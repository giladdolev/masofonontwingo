﻿// Auto generated VisualTree Unit Tests
// Copyright Gizmox Transposition Ltd. ©
// Generated at 04/01/2018 10:53:24
 
 

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System.Threading;

namespace VTTests
{

	[TestClass]
	public partial class PanelElementTests : WebTestBase
	{
	        

		public PanelElementTests():base("Panel", "PanelElement")
		{

		}

		[TestMethod]
		public void BasicPanelTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicPanel");
		}
		[TestMethod]
		public void BasicPanelTestTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicPanelTest");
		}
		[TestMethod]
		public void PanelAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "PanelAccessibleRole");
		}
		[TestMethod]
		public void PanelAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "PanelAnchor");
		}
		[TestMethod]
		public void PanelApplicationTest()
		{
			ExecuteTest("Properties", "Application", "PanelApplication");
		}
		[TestMethod]
		public void PanelApplicationIdTest()
		{
			ExecuteTest("Properties", "ApplicationId", "PanelApplicationId");
		}
		[TestMethod]
		public void PanelAutoScrollTest()
		{
			ExecuteTest("Properties", "AutoScroll", "PanelAutoScroll");
		}
		[TestMethod]
		public void PanelAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "PanelAutoSize");
		}
		[TestMethod]
		public void PanelBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "PanelBackColor");
		}
		[TestMethod]
		public void PanelBackgroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "PanelBackgroundImage");
		}
		[TestMethod]
		public void PanelBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "PanelBackgroundImageLayout");
		}
		[TestMethod]
		public void PanelBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "PanelBorderStyle");
		}
		[TestMethod]
		public void PanelBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "PanelBounds");
		}
		[TestMethod]
		public void PanelChildrenTest()
		{
			ExecuteTest("Properties", "Children", "PanelChildren");
		}
		[TestMethod]
		public void PanelChildrenOrderRevertedTest()
		{
			ExecuteTest("Properties", "ChildrenOrderReverted", "PanelChildrenOrderReverted");
		}
		[TestMethod]
		public void PanelClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "PanelClientID");
		}
		[TestMethod]
		public void PanelClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "PanelClientRectangle");
		}
		[TestMethod]
		public void PanelClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "PanelClientSize");
		}
		[TestMethod]
		public void PanelContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "PanelContextMenuStrip");
		}
		[TestMethod]
		public void PanelControlsTest()
		{
			ExecuteTest("Properties", "Controls", "PanelControls");
		}
		[TestMethod]
		public void PanelCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "PanelCursor");
		}
		[TestMethod]
		public void PanelDockTest()
		{
			ExecuteTest("Properties", "Dock", "PanelDock");
		}
		[TestMethod]
		public void PanelDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "PanelDraggable");
		}
		[TestMethod]
		public void PanelEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "PanelEnabled");
		}
		[TestMethod]
		public void PanelFontTest()
		{
			ExecuteTest("Properties", "Font", "PanelFont");
		}
		[TestMethod]
		public void PanelForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "PanelForeColor");
		}
		[TestMethod]
		public void PanelHandleTest()
		{
			ExecuteTest("Properties", "Handle", "PanelHandle");
		}
		[TestMethod]
		public void PanelHeightTest()
		{
			ExecuteTest("Properties", "Height", "PanelHeight");
		}
		[TestMethod]
		public void PanelIDTest()
		{
			ExecuteTest("Properties", "ID", "PanelID");
		}
		[TestMethod]
		public void PanelIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "PanelIsContainer");
		}
		[TestMethod]
		public void PanelIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "PanelIsDisposed");
		}
		[TestMethod]
		public void PanelLeftTest()
		{
			ExecuteTest("Properties", "Left", "PanelLeft");
		}
		[TestMethod]
		public void PanelLocationTest()
		{
			ExecuteTest("Properties", "Location", "PanelLocation");
		}
		[TestMethod]
		public void PanelMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "PanelMaximumSize");
		}
		[TestMethod]
		public void PanelMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "PanelMinimumSize");
		}
		[TestMethod]
		public void PanelPaddingTest()
		{
			ExecuteTest("Properties", "Padding", "PanelPadding");
		}
		[TestMethod]
		public void PanelParentTest()
		{
			ExecuteTest("Properties", "Parent", "PanelParent");
		}
		[TestMethod]
		public void PanelParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "PanelParentElement");
		}
		[TestMethod]
		public void PanelPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "PanelPixelHeight");
		}
		[TestMethod]
		public void PanelPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "PanelPixelLeft");
		}
		[TestMethod]
		public void PanelPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "PanelPixelTop");
		}
		[TestMethod]
		public void PanelPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "PanelPixelWidth");
		}
		[TestMethod]
		public void PanelRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "PanelRightToLeft");
		}
		[TestMethod]
		public void PanelShowHeaderTest()
		{
			ExecuteTest("Properties", "ShowHeader", "PanelShowHeader");
		}
		[TestMethod]
		public void PanelSizeTest()
		{
			ExecuteTest("Properties", "Size", "PanelSize");
		}
		[TestMethod]
		public void PanelTabIndexTabStopTest()
		{
			ExecuteTest("Properties", "TabIndex", "PanelTabIndexTabStop");
		}
		[TestMethod]
		public void PanelTagTest()
		{
			ExecuteTest("Properties", "Tag", "PanelTag");
		}
		[TestMethod]
		public void PanelTextTest()
		{
			ExecuteTest("Properties", "Text", "PanelText");
		}
		[TestMethod]
		public void PanelToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "PanelToolTipText");
		}
		[TestMethod]
		public void PanelTopTest()
		{
			ExecuteTest("Properties", "Top", "PanelTop");
		}
		[TestMethod]
		public void PanelVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "PanelVisible");
		}
		[TestMethod]
		public void PanelWidthTest()
		{
			ExecuteTest("Properties", "Width", "PanelWidth");
		}
		[TestMethod]
		public void PanelCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "PanelCreateControl");
		}
		[TestMethod]
		public void PanelEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "PanelEquals");
		}
		[TestMethod]
		public void PanelFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "PanelFindForm");
		}
		[TestMethod]
		public void PanelFocusTest()
		{
			ExecuteTest("Methods", "Focus", "PanelFocus");
		}
		[TestMethod]
		public void PanelGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "PanelGetHashCode");
		}
		[TestMethod]
		public void PanelGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "PanelGetKeyPressMasks");
		}
		[TestMethod]
		public void PanelGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "PanelGetType");
		}
		[TestMethod]
		public void PanelHideTest()
		{
			ExecuteTest("Methods", "Hide", "PanelHide");
		}
		[TestMethod]
		public void PanelPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "PanelPerformClick");
		}
		[TestMethod]
		public void PanelPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "PanelPerformControlAdded");
		}
		[TestMethod]
		public void PanelPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "PanelPerformControlRemoved");
		}
		[TestMethod]
		public void PanelPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "PanelPerformDoubleClick");
		}
		[TestMethod]
		public void PanelPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "PanelPerformDragEnter");
		}
		[TestMethod]
		public void PanelPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "PanelPerformDragOver");
		}
		[TestMethod]
		public void PanelPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "PanelPerformEnter");
		}
		[TestMethod]
		public void PanelPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "PanelPerformGotFocus");
		}
		[TestMethod]
		public void PanelPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "PanelPerformKeyDown");
		}
		[TestMethod]
		public void PanelPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "PanelPerformKeyPress");
		}
		[TestMethod]
		public void PanelPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "PanelPerformKeyUp");
		}
		[TestMethod]
		public void PanelPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "PanelPerformLayout");
		}
		[TestMethod]
		public void PanelPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "PanelPerformLeave");
		}
		[TestMethod]
		public void PanelPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "PanelPerformLocationChanged");
		}
		[TestMethod]
		public void PanelPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "PanelPerformMouseClick");
		}
		[TestMethod]
		public void PanelPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "PanelPerformMouseDown");
		}
		[TestMethod]
		public void PanelPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "PanelPerformMouseMove");
		}
		[TestMethod]
		public void PanelPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "PanelPerformMouseUp");
		}
		[TestMethod]
		public void PanelPerformSizeChangedTest()
		{
			ExecuteTest("Methods", "PerformSizeChanged", "PanelPerformSizeChanged");
		}
		[TestMethod]
		public void PanelResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "PanelResetText");
		}
		[TestMethod]
		public void PanelSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "PanelSetBounds");
		}
		[TestMethod]
		public void PanelShowTest()
		{
			ExecuteTest("Methods", "Show", "PanelShow");
		}
		[TestMethod]
		public void PanelToStringTest()
		{
			ExecuteTest("Methods", "ToString", "PanelToString");
		}
		[TestMethod]
		public void PanelClickTest()
		{
			ExecuteTest("Events", "Click", "PanelClick");
		}
		[TestMethod]
		public void PanelDragOverTest()
		{
			ExecuteTest("Events", "DragOver", "PanelDragOver");
		}
		[TestMethod]
		public void PanelEnterTest()
		{
			ExecuteTest("Events", "Enter", "PanelEnter");
		}
		[TestMethod]
		public void PanelGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "PanelGotFocus");
		}
		[TestMethod]
		public void PanelLeaveTest()
		{
			ExecuteTest("Events", "Leave", "PanelLeave");
		}
		[TestMethod]
		public void PanelLoadTest()
		{
			ExecuteTest("Events", "Load", "PanelLoad");
		}
		[TestMethod]
		public void PanelLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "PanelLocationChanged");
		}
		[TestMethod]
		public void PanelLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "PanelLostFocus");
		}
		[TestMethod]
		public void PanelMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "PanelMouseClick");
		}
		[TestMethod]
		public void PanelMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "PanelMouseDown");
		}
		[TestMethod]
		public void PanelMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "PanelMouseEnter");
		}
		[TestMethod]
		public void PanelMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "PanelMouseHover");
		}
		[TestMethod]
		public void PanelMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "PanelMouseLeave");
		}
		[TestMethod]
		public void PanelMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "PanelMouseMove");
		}
		[TestMethod]
		public void PanelMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "PanelMouseUp");
		}
		[TestMethod]
		public void PanelResizeTest()
		{
			ExecuteTest("Events", "Resize", "PanelResize");
		}
		[TestMethod]
		public void PanelSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "PanelSizeChanged");
		}
		[TestMethod]
		public void PanelTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "PanelTextChanged");
		}
		[TestMethod]
		public void PanelValidatingTest()
		{
			ExecuteTest("Events", "Validating", "PanelValidating");
		}
		[TestMethod]
		public void PanelVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "PanelVisibleChanged");
		}
	}

	[TestClass]
	public partial class ToolBarElementTests : WebTestBase
	{
	        

		public ToolBarElementTests():base("Toolbar", "ToolBarElement")
		{

		}

		[TestMethod]
		public void BasicToolBarTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicToolBar");
		}
		[TestMethod]
		public void ToolBarAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "ToolBarAutoSize");
		}
		[TestMethod]
		public void ToolBarBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "ToolBarBackColor");
		}
		[TestMethod]
		public void ToolBarBackgroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "ToolBarBackgroundImage");
		}
		[TestMethod]
		public void ToolBarBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "ToolBarBackgroundImageLayout");
		}
		[TestMethod]
		public void ToolBarBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "ToolBarBorderStyle");
		}
		[TestMethod]
		public void ToolBarBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "ToolBarBounds");
		}
		[TestMethod]
		public void ToolBarClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "ToolBarClientRectangle");
		}
		[TestMethod]
		public void ToolBarClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "ToolBarClientSize");
		}
		[TestMethod]
		public void ToolBarDockTest()
		{
			ExecuteTest("Properties", "Dock", "ToolBarDock");
		}
		[TestMethod]
		public void ToolBarDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "ToolBarDraggable");
		}
		[TestMethod]
		public void ToolBarEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarEnabled");
		}
		[TestMethod]
		public void ToolBarGripStyleTest()
		{
			ExecuteTest("Properties", "GripStyle", "ToolBarGripStyle");
		}
		[TestMethod]
		public void ToolBarHeightTest()
		{
			ExecuteTest("Properties", "Height", "ToolBarHeight");
		}
		[TestMethod]
		public void ToolBarItemsTest()
		{
			ExecuteTest("Properties", "Items", "ToolBarItems");
		}
		[TestMethod]
		public void ToolBarLayoutStyleTest()
		{
			ExecuteTest("Properties", "LayoutStyle", "ToolBarLayoutStyle");
		}
		[TestMethod]
		public void ToolBarLeftTest()
		{
			ExecuteTest("Properties", "Left", "ToolBarLeft");
		}
		[TestMethod]
		public void ToolBarLocationTest()
		{
			ExecuteTest("Properties", "Location", "ToolBarLocation");
		}
		[TestMethod]
		public void ToolBarMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "ToolBarMaximumSize");
		}
		[TestMethod]
		public void ToolBarMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "ToolBarMinimumSize");
		}
		[TestMethod]
		public void ToolBarPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "ToolBarPixelHeight");
		}
		[TestMethod]
		public void ToolBarPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "ToolBarPixelLeft");
		}
		[TestMethod]
		public void ToolBarPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "ToolBarPixelTop");
		}
		[TestMethod]
		public void ToolBarPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "ToolBarPixelWidth");
		}
		[TestMethod]
		public void ToolBarRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "ToolBarRightToLeft");
		}
		[TestMethod]
		public void ToolBarSizeTest()
		{
			ExecuteTest("Properties", "Size", "ToolBarSize");
		}
		[TestMethod]
		public void ToolBarTagTest()
		{
			ExecuteTest("Properties", "Tag", "ToolBarTag");
		}
		[TestMethod]
		public void ToolBarTextDirectionTest()
		{
			ExecuteTest("Properties", "TextDirection", "ToolBarTextDirection");
		}
		[TestMethod]
		public void ToolBarTopTest()
		{
			ExecuteTest("Properties", "Top", "ToolBarTop");
		}
		[TestMethod]
		public void ToolBarVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarVisible");
		}
		[TestMethod]
		public void ToolBarWidthTest()
		{
			ExecuteTest("Properties", "Width", "ToolBarWidth");
		}
		[TestMethod]
		public void ToolBarFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "ToolBarFindForm");
		}
		[TestMethod]
		public void ToolBarGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "ToolBarGetType");
		}
		[TestMethod]
		public void ToolBarHideTest()
		{
			ExecuteTest("Methods", "Hide", "ToolBarHide");
		}
		[TestMethod]
		public void ToolBarSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "ToolBarSetBounds");
		}
		[TestMethod]
		public void ToolBarShowTest()
		{
			ExecuteTest("Methods", "Show", "ToolBarShow");
		}
		[TestMethod]
		public void ToolBarToStringTest()
		{
			ExecuteTest("Methods", "ToString", "ToolBarToString");
		}
		[TestMethod]
		public void ToolBarItemAddedTest()
		{
			ExecuteTest("Events", "ItemAdded", "ToolBarItemAdded");
		}
		[TestMethod]
		public void ToolBarLeaveTest()
		{
			ExecuteTest("Events", "Leave", "ToolBarLeave");
		}
		[TestMethod]
		public void ToolBarSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "ToolBarSizeChanged");
		}
		[TestMethod]
		public void ToolBarVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "ToolBarVisibleChanged");
		}
	}

	[TestClass]
	public partial class ContextMenuStripElementTests : WebTestBase
	{
	        

		public ContextMenuStripElementTests():base("ContextMenu", "ContextMenuStripElement")
		{

		}

		[TestMethod]
		public void ContextMenuStripBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "ContextMenuStripBasic");
		}
	}

	[TestClass]
	public partial class DataViewElementTests : WebTestBase
	{
	        

		public DataViewElementTests():base("DataView", "DataViewElement")
		{

		}

		[TestMethod]
		public void BaiscDataViewTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BaiscDataView");
		}
	}

	[TestClass]
	public partial class DropDownButtonElementTests : WebTestBase
	{
	        

		public DropDownButtonElementTests():base("DropDownButton", "DropDownButtonElement")
		{

		}

		[TestMethod]
		public void DropDownButtonBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "DropDownButtonBasic");
		}
		[TestMethod]
		public void DropDownButtonDropDownClosedTest()
		{
			ExecuteTest("Events", "DropDownClosed", "DropDownButtonDropDownClosed");
		}
		[TestMethod]
		public void DropDownButtonDropDownOpenedTest()
		{
			ExecuteTest("Events", "DropDownOpened", "DropDownButtonDropDownOpened");
		}
		[TestMethod]
		public void DropDownButtonDropDownOpeningTest()
		{
			ExecuteTest("Events", "DropDownOpening", "DropDownButtonDropDownOpening");
		}
	}

	[TestClass]
	public partial class RadioButtonElementTests : WebTestBase
	{
	        

		public RadioButtonElementTests():base("RadioButton", "RadioButtonElement")
		{

		}

		[TestMethod]
		public void BasicRadioButtonTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicRadioButton");
		}
		[TestMethod]
		public void RadioButtonAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "RadioButtonAccessibleRole");
		}
		[TestMethod]
		public void RadioButtonAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "RadioButtonAnchor");
		}
		[TestMethod]
		public void RadioButtonAppearanceTest()
		{
			ExecuteTest("Properties", "Appearance", "RadioButtonAppearance");
		}
		[TestMethod]
		public void RadioButtonApplicationTest()
		{
			ExecuteTest("Properties", "Application", "RadioButtonApplication");
		}
		[TestMethod]
		public void RadioButtonAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "RadioButtonAutoSize");
		}
		[TestMethod]
		public void RadioButtonBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "RadioButtonBackColor");
		}
		[TestMethod]
		public void RadioButtonBackGroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "RadioButtonBackGroundImage");
		}
		[TestMethod]
		public void RadioButtonBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "RadioButtonBackgroundImageLayout");
		}
		[TestMethod]
		public void RadioButtonBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "RadioButtonBounds");
		}
		[TestMethod]
		public void RadioButtonCheckAlignTest()
		{
			ExecuteTest("Properties", "CheckAlign", "RadioButtonCheckAlign");
		}
		[TestMethod]
		public void RadioButtonClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "RadioButtonClientID");
		}
		[TestMethod]
		public void RadioButtonClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "RadioButtonClientRectangle");
		}
		[TestMethod]
		public void RadioButtonClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "RadioButtonClientSize");
		}
		[TestMethod]
		public void RadioButtonContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "RadioButtonContextMenuStrip");
		}
		[TestMethod]
		public void RadioButtonCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "RadioButtonCursor");
		}
		[TestMethod]
		public void RadioButtonDockTest()
		{
			ExecuteTest("Properties", "Dock", "RadioButtonDock");
		}
		[TestMethod]
		public void RadioButtonDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "RadioButtonDraggable");
		}
		[TestMethod]
		public void RadioButtonEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "RadioButtonEnabled");
		}
		[TestMethod]
		public void RadioButtonFlatStyleTest()
		{
			ExecuteTest("Properties", "FlatStyle", "RadioButtonFlatStyle");
		}
		[TestMethod]
		public void RadioButtonFontTest()
		{
			ExecuteTest("Properties", "Font", "RadioButtonFont");
		}
		[TestMethod]
		public void RadioButtonForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "RadioButtonForeColor");
		}
		[TestMethod]
		public void RadioButtonHandleTest()
		{
			ExecuteTest("Properties", "Handle", "RadioButtonHandle");
		}
		[TestMethod]
		public void RadioButtonHeightTest()
		{
			ExecuteTest("Properties", "Height", "RadioButtonHeight");
		}
		[TestMethod]
		public void RadioButtonIDTest()
		{
			ExecuteTest("Properties", "ID", "RadioButtonID");
		}
		[TestMethod]
		public void RadioButtonImageTest()
		{
			ExecuteTest("Properties", "Image", "RadioButtonImage");
		}
		[TestMethod]
		public void RadioButtonImageAlignTest()
		{
			ExecuteTest("Properties", "ImageAlign", "RadioButtonImageAlign");
		}
		[TestMethod]
		public void RadioButtonImageHeightTest()
		{
			ExecuteTest("Properties", "ImageHeight", "RadioButtonImageHeight");
		}
		[TestMethod]
		public void RadioButtonImageWidthTest()
		{
			ExecuteTest("Properties", "ImageWidth", "RadioButtonImageWidth");
		}
		[TestMethod]
		public void RadioButtonIsCheckedTest()
		{
			ExecuteTest("Properties", "IsChecked", "RadioButtonIsChecked");
		}
		[TestMethod]
		public void RadioButtonIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "RadioButtonIsContainer");
		}
		[TestMethod]
		public void RadioButtonIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "RadioButtonIsDisposed");
		}
		[TestMethod]
		public void RadioButtonLeftTest()
		{
			ExecuteTest("Properties", "Left", "RadioButtonLeft");
		}
		[TestMethod]
		public void RadioButtonLocationTest()
		{
			ExecuteTest("Properties", "Location", "RadioButtonLocation");
		}
		[TestMethod]
		public void RadioButtonMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "RadioButtonMaximumSize");
		}
		[TestMethod]
		public void RadioButtonMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "RadioButtonMinimumSize");
		}
		[TestMethod]
		public void RadioButtonParentTest()
		{
			ExecuteTest("Properties", "Parent", "RadioButtonParent");
		}
		[TestMethod]
		public void RadioButtonParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "RadioButtonParentElement");
		}
		[TestMethod]
		public void RadioButtonPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "RadioButtonPixelHeight");
		}
		[TestMethod]
		public void RadioButtonPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "RadioButtonPixelLeft");
		}
		[TestMethod]
		public void RadioButtonPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "RadioButtonPixelTop");
		}
		[TestMethod]
		public void RadioButtonPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "RadioButtonPixelWidth");
		}
		[TestMethod]
		public void RadioButtonRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "RadioButtonRightToLeft");
		}
		[TestMethod]
		public void RadioButtonSizeTest()
		{
			ExecuteTest("Properties", "Size", "RadioButtonSize");
		}
		[TestMethod]
		public void RadioButtonTagTest()
		{
			ExecuteTest("Properties", "Tag", "RadioButtonTag");
		}
		[TestMethod]
		public void RadioButtonTextTest()
		{
			ExecuteTest("Properties", "Text", "RadioButtonText");
		}
		[TestMethod]
		public void RadioButtonTextAlignTest()
		{
			ExecuteTest("Properties", "TextAlign", "RadioButtonTextAlign");
		}
		[TestMethod]
		public void RadioButtonToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "RadioButtonToolTipText");
		}
		[TestMethod]
		public void RadioButtonTopTest()
		{
			ExecuteTest("Properties", "Top", "RadioButtonTop");
		}
		[TestMethod]
		public void RadioButtonVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "RadioButtonVisible");
		}
		[TestMethod]
		public void RadioButtonWidthTest()
		{
			ExecuteTest("Properties", "Width", "RadioButtonWidth");
		}
		[TestMethod]
		public void RadioButtonCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "RadioButtonCreateControl");
		}
		[TestMethod]
		public void RadioButtonEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "RadioButtonEquals");
		}
		[TestMethod]
		public void RadioButtonFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "RadioButtonFindForm");
		}
		[TestMethod]
		public void RadioButtonFocusTest()
		{
			ExecuteTest("Methods", "Focus", "RadioButtonFocus");
		}
		[TestMethod]
		public void RadioButtonGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "RadioButtonGetHashCode");
		}
		[TestMethod]
		public void RadioButtonGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "RadioButtonGetKeyPressMasks");
		}
		[TestMethod]
		public void RadioButtonGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "RadioButtonGetType");
		}
		[TestMethod]
		public void RadioButtonHideTest()
		{
			ExecuteTest("Methods", "Hide", "RadioButtonHide");
		}
		[TestMethod]
		public void RadioButtonPerformCheckedChangedTest()
		{
			ExecuteTest("Methods", "PerformCheckedChanged", "RadioButtonPerformCheckedChanged");
		}
		[TestMethod]
		public void RadioButtonPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "RadioButtonPerformClick");
		}
		[TestMethod]
		public void RadioButtonPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "RadioButtonPerformControlAdded");
		}
		[TestMethod]
		public void RadioButtonPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "RadioButtonPerformControlRemoved");
		}
		[TestMethod]
		public void RadioButtonPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "RadioButtonPerformDoubleClick");
		}
		[TestMethod]
		public void RadioButtonPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "RadioButtonPerformDragEnter");
		}
		[TestMethod]
		public void RadioButtonPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "RadioButtonPerformDragOver");
		}
		[TestMethod]
		public void RadioButtonPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "RadioButtonPerformEnter");
		}
		[TestMethod]
		public void RadioButtonPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "RadioButtonPerformGotFocus");
		}
		[TestMethod]
		public void RadioButtonPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "RadioButtonPerformKeyDown");
		}
		[TestMethod]
		public void RadioButtonPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "RadioButtonPerformKeyPress");
		}
		[TestMethod]
		public void RadioButtonPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "RadioButtonPerformKeyUp");
		}
		[TestMethod]
		public void RadioButtonPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "RadioButtonPerformLayout");
		}
		[TestMethod]
		public void RadioButtonPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "RadioButtonPerformLeave");
		}
		[TestMethod]
		public void RadioButtonPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "RadioButtonPerformLocationChanged");
		}
		[TestMethod]
		public void RadioButtonPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "RadioButtonPerformMouseClick");
		}
		[TestMethod]
		public void RadioButtonPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "RadioButtonPerformMouseDown");
		}
		[TestMethod]
		public void RadioButtonPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "RadioButtonPerformMouseMove");
		}
		[TestMethod]
		public void RadioButtonPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "RadioButtonPerformMouseUp");
		}
		[TestMethod]
		public void RadioButtonPerformSizeChangedTest()
		{
			ExecuteTest("Methods", "PerformSizeChanged", "RadioButtonPerformSizeChanged");
		}
		[TestMethod]
		public void RadioButtonResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "RadioButtonResetText");
		}
		[TestMethod]
		public void RadioButtonSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "RadioButtonSetBounds");
		}
		[TestMethod]
		public void RadioButtonShowTest()
		{
			ExecuteTest("Methods", "Show", "RadioButtonShow");
		}
		[TestMethod]
		public void RadioButtonToStringTest()
		{
			ExecuteTest("Methods", "ToString", "RadioButtonToString");
		}
		[TestMethod]
		public void RadioButtonCheckedChangedTest()
		{
			ExecuteTest("Events", "CheckedChanged", "RadioButtonCheckedChanged");
		}
		[TestMethod]
		public void RadioButtonClickTest()
		{
			ExecuteTest("Events", "Click", "RadioButtonClick");
		}
		[TestMethod]
		public void RadioButtonEnterTest()
		{
			ExecuteTest("Events", "Enter", "RadioButtonEnter");
		}
		[TestMethod]
		public void RadioButtonGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "RadioButtonGotFocus");
		}
		[TestMethod]
		public void RadioButtonKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "RadioButtonKeyDown");
		}
		[TestMethod]
		public void RadioButtonKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "RadioButtonKeyPress");
		}
		[TestMethod]
		public void RadioButtonKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "RadioButtonKeyUp");
		}
		[TestMethod]
		public void RadioButtonLeaveTest()
		{
			ExecuteTest("Events", "Leave", "RadioButtonLeave");
		}
		[TestMethod]
		public void RadioButtonLoadTest()
		{
			ExecuteTest("Events", "Load", "RadioButtonLoad");
		}
		[TestMethod]
		public void RadioButtonLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "RadioButtonLostFocus");
		}
		[TestMethod]
		public void RadioButtonMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "RadioButtonMouseClick");
		}
		[TestMethod]
		public void RadioButtonMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "RadioButtonMouseDown");
		}
		[TestMethod]
		public void RadioButtonMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "RadioButtonMouseEnter");
		}
		[TestMethod]
		public void RadioButtonMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "RadioButtonMouseHover");
		}
		[TestMethod]
		public void RadioButtonMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "RadioButtonMouseLeave");
		}
		[TestMethod]
		public void RadioButtonMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "RadioButtonMouseMove");
		}
		[TestMethod]
		public void RadioButtonMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "RadioButtonMouseUp");
		}
		[TestMethod]
		public void RadioButtonResizeTest()
		{
			ExecuteTest("Events", "Resize", "RadioButtonResize");
		}
		[TestMethod]
		public void RadioButtonSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "RadioButtonSizeChanged");
		}
		[TestMethod]
		public void RadioButtonTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "RadioButtonTextChanged");
		}
		[TestMethod]
		public void RadioButtonValidatingTest()
		{
			ExecuteTest("Events", "Validating", "RadioButtonValidating");
		}
		[TestMethod]
		public void RadioButtonVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "RadioButtonVisibleChanged");
		}
	}

	[TestClass]
	public partial class RadioGroupElementTests : WebTestBase
	{
	        

		public RadioGroupElementTests():base("RadioGroup", "RadioGroupElement")
		{

		}

		[TestMethod]
		public void BasicRadioGroupTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicRadioGroup");
		}
	}

	[TestClass]
	public partial class TextBoxElementTests : WebTestBase
	{
	        

		public TextBoxElementTests():base("TextBox", "TextBoxElement")
		{

		}

		[TestMethod]
		public void BasicTextBoxTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicTextBox");
		}
		[TestMethod]
		public void TextBoxAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "TextBoxAccessibleRole");
		}
		[TestMethod]
		public void TextBoxAllowBlankTest()
		{
			ExecuteTest("Properties", "AllowBlank", "TextBoxAllowBlank");
		}
		[TestMethod]
		public void TextBoxAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "TextBoxAnchor");
		}
		[TestMethod]
		public void TextBoxApplicationTest()
		{
			ExecuteTest("Properties", "Application", "TextBoxApplication");
		}
		[TestMethod]
		public void TextBoxApplicationIdTest()
		{
			ExecuteTest("Properties", "ApplicationId", "TextBoxApplicationId");
		}
		[TestMethod]
		public void TextBoxAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "TextBoxAutoSize");
		}
		[TestMethod]
		public void TextBoxBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "TextBoxBackColor");
		}
		[TestMethod]
		public void TextBoxBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "TextBoxBorderStyle");
		}
		[TestMethod]
		public void TextBoxBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "TextBoxBounds");
		}
		[TestMethod]
		public void TextBoxCharacterCasingTest()
		{
			ExecuteTest("Properties", "CharacterCasing", "TextBoxCharacterCasing");
		}
		[TestMethod]
		public void TextBoxClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "TextBoxClientID");
		}
		[TestMethod]
		public void TextBoxClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "TextBoxClientRectangle");
		}
		[TestMethod]
		public void TextBoxClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "TextBoxClientSize");
		}
		[TestMethod]
		public void TextBoxContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "TextBoxContextMenuStrip");
		}
		[TestMethod]
		public void TextBoxCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "TextBoxCursor");
		}
		[TestMethod]
		public void TextBoxDockTest()
		{
			ExecuteTest("Properties", "Dock", "TextBoxDock");
		}
		[TestMethod]
		public void TextBoxDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "TextBoxDraggable");
		}
		[TestMethod]
		public void TextBoxEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "TextBoxEnabled");
		}
		[TestMethod]
		public void TextBoxFontTest()
		{
			ExecuteTest("Properties", "Font", "TextBoxFont");
		}
		[TestMethod]
		public void TextBoxForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "TextBoxForeColor");
		}
		[TestMethod]
		public void TextBoxHandleTest()
		{
			ExecuteTest("Properties", "Handle", "TextBoxHandle");
		}
		[TestMethod]
		public void TextBoxHeightTest()
		{
			ExecuteTest("Properties", "Height", "TextBoxHeight");
		}
		[TestMethod]
		public void TextBoxIDTest()
		{
			ExecuteTest("Properties", "ID", "TextBoxID");
		}
		[TestMethod]
		public void TextBoxIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "TextBoxIsContainer");
		}
		[TestMethod]
		public void TextBoxIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "TextBoxIsDisposed");
		}
		[TestMethod]
		public void TextBoxIsFocusedTest()
		{
			ExecuteTest("Properties", "IsFocused", "TextBoxIsFocused");
		}
		[TestMethod]
		public void TextBoxLeftTest()
		{
			ExecuteTest("Properties", "Left", "TextBoxLeft");
		}
		[TestMethod]
		public void TextBoxLocationTest()
		{
			ExecuteTest("Properties", "Location", "TextBoxLocation");
		}
		[TestMethod]
		public void TextBoxMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "TextBoxMaximumSize");
		}
		[TestMethod]
		public void TextBoxMaxLengthTest()
		{
			ExecuteTest("Properties", "MaxLength", "TextBoxMaxLength");
		}
		[TestMethod]
		public void TextBoxMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "TextBoxMinimumSize");
		}
		[TestMethod]
		public void TextBoxMultilineTest()
		{
			ExecuteTest("Properties", "Multiline", "TextBoxMultiline");
		}
		[TestMethod]
		public void TextBoxParentTest()
		{
			ExecuteTest("Properties", "Parent", "TextBoxParent");
		}
		[TestMethod]
		public void TextBoxParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "TextBoxParentElement");
		}
		[TestMethod]
		public void TextBoxPasswordCharTest()
		{
			ExecuteTest("Properties", "PasswordChar", "TextBoxPasswordChar");
		}
		[TestMethod]
		public void TextBoxPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "TextBoxPixelHeight");
		}
		[TestMethod]
		public void TextBoxPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "TextBoxPixelLeft");
		}
		[TestMethod]
		public void TextBoxPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "TextBoxPixelTop");
		}
		[TestMethod]
		public void TextBoxPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "TextBoxPixelWidth");
		}
		[TestMethod]
		public void TextBoxReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "TextBoxReadOnly");
		}
		[TestMethod]
		public void TextBoxRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "TextBoxRightToLeft");
		}
		[TestMethod]
		public void TextBoxScrollBarsTest()
		{
			ExecuteTest("Properties", "ScrollBars", "TextBoxScrollBars");
		}
		[TestMethod]
		public void TextBoxSelectedTextTest()
		{
			ExecuteTest("Properties", "SelectedText", "TextBoxSelectedText");
		}
		[TestMethod]
		public void TextBoxSelectionLengthTest()
		{
			ExecuteTest("Properties", "SelectionLength", "TextBoxSelectionLength");
		}
		[TestMethod]
		public void TextBoxSelectionStartTest()
		{
			ExecuteTest("Properties", "SelectionStart", "TextBoxSelectionStart");
		}
		[TestMethod]
		public void TextBoxSelectOnFocusTest()
		{
			ExecuteTest("Properties", "SelectOnFocus", "TextBoxSelectOnFocus");
		}
		[TestMethod]
		public void TextBoxSizeTest()
		{
			ExecuteTest("Properties", "Size", "TextBoxSize");
		}
		[TestMethod]
		public void TextBoxTabIndexTabStopTest()
		{
			ExecuteTest("Properties", "TabIndex", "TextBoxTabIndexTabStop");
		}
		[TestMethod]
		public void TextBoxTagTest()
		{
			ExecuteTest("Properties", "Tag", "TextBoxTag");
		}
		[TestMethod]
		public void TextBoxTextTest()
		{
			ExecuteTest("Properties", "Text", "TextBoxText");
		}
		[TestMethod]
		public void TextBoxTextAlignTest()
		{
			ExecuteTest("Properties", "TextAlign", "TextBoxTextAlign");
		}
		[TestMethod]
		public void TextBoxTextLengthTest()
		{
			ExecuteTest("Properties", "TextLength", "TextBoxTextLength");
		}
		[TestMethod]
		public void TextBoxToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "TextBoxToolTipText");
		}
		[TestMethod]
		public void TextBoxTopTest()
		{
			ExecuteTest("Properties", "Top", "TextBoxTop");
		}
		[TestMethod]
		public void TextBoxVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "TextBoxVisible");
		}
		[TestMethod]
		public void TextBoxWaitMaskDisabledTest()
		{
			ExecuteTest("Properties", "WaitMaskDisabled", "TextBoxWaitMaskDisabled");
		}
		[TestMethod]
		public void TextBoxWidthTest()
		{
			ExecuteTest("Properties", "Width", "TextBoxWidth");
		}
		[TestMethod]
		public void TextBoxClearTest()
		{
			ExecuteTest("Methods", "Clear", "TextBoxClear");
		}
		[TestMethod]
		public void TextBoxCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "TextBoxCreateControl");
		}
		[TestMethod]
		public void TextBoxEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "TextBoxEquals");
		}
		[TestMethod]
		public void TextBoxFocusTest()
		{
			ExecuteTest("Methods", "Focus", "TextBoxFocus");
		}
		[TestMethod]
		public void TextBoxGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "TextBoxGetHashCode");
		}
		[TestMethod]
		public void TextBoxGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "TextBoxGetKeyPressMasks");
		}
		[TestMethod]
		public void TextBoxGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "TextBoxGetType");
		}
		[TestMethod]
		public void TextBoxHideTest()
		{
			ExecuteTest("Methods", "Hide", "TextBoxHide");
		}
		[TestMethod]
		public void TextBoxPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "TextBoxPerformClick");
		}
		[TestMethod]
		public void TextBoxPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "TextBoxPerformControlAdded");
		}
		[TestMethod]
		public void TextBoxPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "TextBoxPerformControlRemoved");
		}
		[TestMethod]
		public void TextBoxPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "TextBoxPerformDoubleClick");
		}
		[TestMethod]
		public void TextBoxPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "TextBoxPerformDragEnter");
		}
		[TestMethod]
		public void TextBoxPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "TextBoxPerformDragOver");
		}
		[TestMethod]
		public void TextBoxPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "TextBoxPerformEnter");
		}
		[TestMethod]
		public void TextBoxPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "TextBoxPerformGotFocus");
		}
		[TestMethod]
		public void TextBoxPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "TextBoxPerformKeyDown");
		}
		[TestMethod]
		public void TextBoxPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "TextBoxPerformKeyPress");
		}
		[TestMethod]
		public void TextBoxPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "TextBoxPerformKeyUp");
		}
		[TestMethod]
		public void TextBoxPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "TextBoxPerformLayout");
		}
		[TestMethod]
		public void TextBoxPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "TextBoxPerformLeave");
		}
		[TestMethod]
		public void TextBoxPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "TextBoxPerformLocationChanged");
		}
		[TestMethod]
		public void TextBoxPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "TextBoxPerformMouseClick");
		}
		[TestMethod]
		public void TextBoxPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "TextBoxPerformMouseDown");
		}
		[TestMethod]
		public void TextBoxPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "TextBoxPerformMouseMove");
		}
		[TestMethod]
		public void TextBoxPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "TextBoxPerformMouseUp");
		}
		[TestMethod]
		public void TextBoxPerformSizeChangedTest()
		{
			ExecuteTest("Methods", "PerformSizeChanged", "TextBoxPerformSizeChanged");
		}
		[TestMethod]
		public void TextBoxResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "TextBoxResetText");
		}
		[TestMethod]
		public void TextBoxSelectAllTest()
		{
			ExecuteTest("Methods", "SelectAll", "TextBoxSelectAll");
		}
		[TestMethod]
		public void TextBoxSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "TextBoxSetBounds");
		}
		[TestMethod]
		public void TextBoxShowTest()
		{
			ExecuteTest("Methods", "Show", "TextBoxShow");
		}
		[TestMethod]
		public void TextBoxToStringTest()
		{
			ExecuteTest("Methods", "ToString", "TextBoxToString");
		}
		[TestMethod]
		public void TextBoxClickTest()
		{
			ExecuteTest("Events", "Click", "TextBoxClick");
		}
		[TestMethod]
		public void TextBoxEnterTest()
		{
			ExecuteTest("Events", "Enter", "TextBoxEnter");
		}
		[TestMethod]
		public void TextBoxGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "TextBoxGotFocus");
		}
		[TestMethod]
		public void TextBoxKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "TextBoxKeyDown");
		}
		[TestMethod]
		public void TextBoxKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "TextBoxKeyPress");
		}
		[TestMethod]
		public void TextBoxKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "TextBoxKeyUp");
		}
		[TestMethod]
		public void TextBoxLeaveTest()
		{
			ExecuteTest("Events", "Leave", "TextBoxLeave");
		}
		[TestMethod]
		public void TextBoxLoadTest()
		{
			ExecuteTest("Events", "Load", "TextBoxLoad");
		}
		[TestMethod]
		public void TextBoxLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "TextBoxLocationChanged");
		}
		[TestMethod]
		public void TextBoxLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "TextBoxLostFocus");
		}
		[TestMethod]
		public void TextBoxMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "TextBoxMouseClick");
		}
		[TestMethod]
		public void TextBoxMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "TextBoxMouseDown");
		}
		[TestMethod]
		public void TextBoxMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "TextBoxMouseEnter");
		}
		[TestMethod]
		public void TextBoxMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "TextBoxMouseHover");
		}
		[TestMethod]
		public void TextBoxMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "TextBoxMouseLeave");
		}
		[TestMethod]
		public void TextBoxMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "TextBoxMouseMove");
		}
		[TestMethod]
		public void TextBoxMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "TextBoxMouseUp");
		}
		[TestMethod]
		public void TextBoxResizeTest()
		{
			ExecuteTest("Events", "Resize", "TextBoxResize");
		}
		[TestMethod]
		public void TextBoxSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "TextBoxSizeChanged");
		}
		[TestMethod]
		public void TextBoxTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "TextBoxTextChanged");
		}
		[TestMethod]
		public void TextBoxValidatingTest()
		{
			ExecuteTest("Events", "Validating", "TextBoxValidating");
		}
		[TestMethod]
		public void TextBoxVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "TextBoxVisibleChanged");
		}
	}

	[TestClass]
	public partial class TimeEditElementTests : WebTestBase
	{
	        

		public TimeEditElementTests():base("TimeEdit", "TimeEditElement")
		{

		}

		[TestMethod]
		public void BasicTimeEditTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicTimeEdit");
		}
	}

	[TestClass]
	public partial class ErrorProviderElementTests : WebTestBase
	{
	        

		public ErrorProviderElementTests():base("ErrorProvider", "ErrorProviderElement")
		{

		}

		[TestMethod]
		public void ErrorProviderBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "ErrorProviderBasic");
		}
		[TestMethod]
		public void ErrorProviderClearTest()
		{
			ExecuteTest("Methods", "Clear", "ErrorProviderClear");
		}
		[TestMethod]
		public void ErrorProviderGetErrorTest()
		{
			ExecuteTest("Methods", "GetError", "ErrorProviderGetError");
		}
		[TestMethod]
		public void ErrorProviderSetErrorTest()
		{
			ExecuteTest("Methods", "SetError", "ErrorProviderSetError");
		}
	}

	[TestClass]
	public partial class GridColumnTests : WebTestBase
	{
	        

		public GridColumnTests():base("GridColumn", "GridColumn")
		{

		}

		[TestMethod]
		public void GridGridColumnIconTest()
		{
			ExecuteTest("Properties", "Icon", "GridGridColumnIcon");
		}
		[TestMethod]
		public void GridGridColumnSortModeTest()
		{
			ExecuteTest("Properties", "SortMode", "GridGridColumnSortMode");
		}
		[TestMethod]
		public void GridGridColumnVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "GridGridColumnVisible");
		}
	}

	[TestClass]
	public partial class GridTextButtonColumnTests : WebTestBase
	{
	        

		public GridTextButtonColumnTests():base("GridTextButtonColumn", "GridTextButtonColumn")
		{

		}

		[TestMethod]
		public void GridGridTextButtonColumnReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "GridGridTextButtonColumnReadOnly");
		}
	}

	[TestClass]
	public partial class GridComboBoxColumnTests : WebTestBase
	{
	        

		public GridComboBoxColumnTests():base("GridComboBoxColumn", "GridComboBoxColumn")
		{

		}

		[TestMethod]
		public void GridGridComboBoxColumnExpandOnFocusTest()
		{
			ExecuteTest("Properties", "ExpandOnFocus", "GridGridComboBoxColumnExpandOnFocus");
		}
		[TestMethod]
		public void GridGridComboBoxColumnReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "GridGridComboBoxColumnReadOnly");
		}
		[TestMethod]
		public void GridGridComboBoxColumnGetItemDataTest()
		{
			ExecuteTest("Methods", "GetItemData", "GridGridComboBoxColumnGetItemData");
		}
		[TestMethod]
		public void GridGridComboBoxColumnSetItemDataTest()
		{
			ExecuteTest("Methods", "SetItemData", "GridGridComboBoxColumnSetItemData");
		}
	}

	[TestClass]
	public partial class GridCellElementTests : WebTestBase
	{
	        

		public GridCellElementTests():base("GridCellElement", "GridCellElement")
		{

		}

		[TestMethod]
		public void GridCellElementSelectedIndexTest()
		{
			ExecuteTest("Properties", "SelectedIndex", "GridCellElementSelectedIndex");
		}
	}

	[TestClass]
	public partial class GridDateTimePickerColumnTests : WebTestBase
	{
	        

		public GridDateTimePickerColumnTests():base("GridDTPickerColumn", "GridDateTimePickerColumn")
		{

		}

		[TestMethod]
		public void GridGridDateTimePickerColumnReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "GridGridDateTimePickerColumnReadOnly");
		}
	}

	[TestClass]
	public partial class HTMLElementTests : WebTestBase
	{
	        

		public HTMLElementTests():base("HTML", "HTMLElement")
		{

		}

		[TestMethod]
		public void BasicHTMLTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicHTML");
		}
	}

	[TestClass]
	public partial class LabelElementTests : WebTestBase
	{
	        

		public LabelElementTests():base("Label", "LabelElement")
		{

		}

		[TestMethod]
		public void BasicLabelTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicLabel");
		}
		[TestMethod]
		public void LabelAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "LabelAccessibleRole");
		}
		[TestMethod]
		public void LabelAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "LabelAnchor");
		}
		[TestMethod]
		public void LabelAppearanceTest()
		{
			ExecuteTest("Properties", "Appearance", "LabelAppearance");
		}
		[TestMethod]
		public void LabelApplicationTest()
		{
			ExecuteTest("Properties", "Application", "LabelApplication");
		}
		[TestMethod]
		public void LabelAutoEllipsisTest()
		{
			ExecuteTest("Properties", "AutoEllipsis", "LabelAutoEllipsis");
		}
		[TestMethod]
		public void LabelAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "LabelAutoSize");
		}
		[TestMethod]
		public void LabelBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "LabelBackColor");
		}
		[TestMethod]
		public void LabelBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "LabelBorderStyle");
		}
		[TestMethod]
		public void LabelBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "LabelBounds");
		}
		[TestMethod]
		public void LabelClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "LabelClientRectangle");
		}
		[TestMethod]
		public void LabelClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "LabelClientSize");
		}
		[TestMethod]
		public void LabelCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "LabelCursor");
		}
		[TestMethod]
		public void LabelDockTest()
		{
			ExecuteTest("Properties", "Dock", "LabelDock");
		}
		[TestMethod]
		public void LabelDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "LabelDraggable");
		}
		[TestMethod]
		public void LabelEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "LabelEnabled");
		}
		[TestMethod]
		public void LabelFontTest()
		{
			ExecuteTest("Properties", "Font", "LabelFont");
		}
		[TestMethod]
		public void LabelForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "LabelForeColor");
		}
		[TestMethod]
		public void LabelHeightTest()
		{
			ExecuteTest("Properties", "Height", "LabelHeight");
		}
		[TestMethod]
		public void LabelImageTest()
		{
			ExecuteTest("Properties", "Image", "LabelImage");
		}
		[TestMethod]
		public void LabelIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "LabelIsContainer");
		}
		[TestMethod]
		public void LabelIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "LabelIsDisposed");
		}
		[TestMethod]
		public void LabelLeftTest()
		{
			ExecuteTest("Properties", "Left", "LabelLeft");
		}
		[TestMethod]
		public void LabelLocationTest()
		{
			ExecuteTest("Properties", "Location", "LabelLocation");
		}
		[TestMethod]
		public void LabelMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "LabelMaximumSize");
		}
		[TestMethod]
		public void LabelMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "LabelMinimumSize");
		}
		[TestMethod]
		public void LabelMousePositionTest()
		{
			ExecuteTest("Properties", "MousePosition", "LabelMousePosition");
		}
		[TestMethod]
		public void LabelPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "LabelPixelHeight");
		}
		[TestMethod]
		public void LabelPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "LabelPixelLeft");
		}
		[TestMethod]
		public void LabelPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "LabelPixelTop");
		}
		[TestMethod]
		public void LabelPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "LabelPixelWidth");
		}
		[TestMethod]
		public void LabelRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "LabelRightToLeft");
		}
		[TestMethod]
		public void LabelSizeTest()
		{
			ExecuteTest("Properties", "Size", "LabelSize");
		}
		[TestMethod]
		public void LabelTagTest()
		{
			ExecuteTest("Properties", "Tag", "LabelTag");
		}
		[TestMethod]
		public void LabelTextTest()
		{
			ExecuteTest("Properties", "Text", "LabelText");
		}
		[TestMethod]
		public void LabelTextAlignTest()
		{
			ExecuteTest("Properties", "TextAlign", "LabelTextAlign");
		}
		[TestMethod]
		public void LabelToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "LabelToolTipText");
		}
		[TestMethod]
		public void LabelTopTest()
		{
			ExecuteTest("Properties", "Top", "LabelTop");
		}
		[TestMethod]
		public void LabelVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "LabelVisible");
		}
		[TestMethod]
		public void LabelWidthTest()
		{
			ExecuteTest("Properties", "Width", "LabelWidth");
		}
		[TestMethod]
		public void LabelWordWrapTest()
		{
			ExecuteTest("Properties", "WordWrap", "LabelWordWrap");
		}
		[TestMethod]
		public void LabelFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "LabelFindForm");
		}
		[TestMethod]
		public void LabelFocusTest()
		{
			ExecuteTest("Methods", "Focus", "LabelFocus");
		}
		[TestMethod]
		public void LabelGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "LabelGetType");
		}
		[TestMethod]
		public void LabelHideTest()
		{
			ExecuteTest("Methods", "Hide", "LabelHide");
		}
		[TestMethod]
		public void LabelResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "LabelResetText");
		}
		[TestMethod]
		public void LabelSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "LabelSetBounds");
		}
		[TestMethod]
		public void LabelShowTest()
		{
			ExecuteTest("Methods", "Show", "LabelShow");
		}
		[TestMethod]
		public void LabelToStringTest()
		{
			ExecuteTest("Methods", "ToString", "LabelToString");
		}
		[TestMethod]
		public void LabelClickTest()
		{
			ExecuteTest("Events", "Click", "LabelClick");
		}
		[TestMethod]
		public void LabelMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "LabelMouseHover");
		}
		[TestMethod]
		public void LabelTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "LabelTextChanged");
		}
		[TestMethod]
		public void LabelVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "LabelVisibleChanged");
		}
	}

	[TestClass]
	public partial class LinkLabelElementTests : WebTestBase
	{
	        

		public LinkLabelElementTests():base("LinkLabel", "LinkLabelElement")
		{

		}

		[TestMethod]
		public void BasicLinkLabelTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicLinkLabel");
		}
		[TestMethod]
		public void LinkLabelMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "LinkLabelMaximumSize");
		}
		[TestMethod]
		public void LinkLabelMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "LinkLabelMinimumSize");
		}
	}

	[TestClass]
	public partial class GridElementTests : WebTestBase
	{
	        

		public GridElementTests():base("Grid", "GridElement")
		{

		}

		[TestMethod]
		public void BasicGridTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicGrid");
		}
		[TestMethod]
		public void BasicGridEventsOrderTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicGridEventsOrder");
		}
		[TestMethod]
		public void GridSelectedRowsColumnsCellsTest()
		{
			ExecuteTest(String.Empty, String.Empty, "GridSelectedRowsColumnsCells");
		}
		[TestMethod]
		public void GridClipboardTest()
		{
			ExecuteTest(String.Empty, String.Empty, "GridClipboard");
		}
		[TestMethod]
		public void GridCustomColumnTest()
		{
			ExecuteTest(String.Empty, String.Empty, "GridCustomColumn");
		}
		[TestMethod]
		public void GridDateTimeColumnFormatTest()
		{
			ExecuteTest(String.Empty, String.Empty, "GridDateTimeColumnFormat");
		}
		[TestMethod]
		public void GridAllowColumnSelectionTest()
		{
			ExecuteTest("Properties", "AllowColumnSelection", "GridAllowColumnSelection");
		}
		[TestMethod]
		public void GridAllowUserToOrderColumnsTest()
		{
			ExecuteTest("Properties", "AllowUserToOrderColumns", "GridAllowUserToOrderColumns");
		}
		[TestMethod]
		public void GridBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "GridBackColor");
		}
		[TestMethod]
		public void GridBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "GridBorderStyle");
		}
		[TestMethod]
		public void GridBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "GridBounds");
		}
		[TestMethod]
		public void GridCanSelectTest()
		{
			ExecuteTest("Properties", "CanSelect", "GridCanSelect");
		}
		[TestMethod]
		public void GridCellBorderStyleTest()
		{
			ExecuteTest("Properties", "CellBorderStyle", "GridCellBorderStyle");
		}
		[TestMethod]
		public void GridClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "GridClientRectangle");
		}
		[TestMethod]
		public void GridClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "GridClientSize");
		}
		[TestMethod]
		public void GridColDataTest()
		{
			ExecuteTest("Properties", "ColData", "GridColData");
		}
		[TestMethod]
		public void GridColumnCountTest()
		{
			ExecuteTest("Properties", "ColumnCount", "GridColumnCount");
		}
		[TestMethod]
		public void GridColumnFilterInfoTest()
		{
			ExecuteTest("Properties", "ColumnFilterInfo", "GridColumnFilterInfo");
		}
		[TestMethod]
		public void GridColumnHeadersVisibleTest()
		{
			ExecuteTest("Properties", "ColumnHeadersVisible", "GridColumnHeadersVisible");
		}
		[TestMethod]
		public void GridColumnFormatTest()
		{
			ExecuteTest("Properties", "Columns", "GridColumnFormat");
		}
		[TestMethod]
		public void BasicGridGridCheckBoxColumnTest()
		{
			ExecuteTest("Properties", "Columns", "BasicGridGridCheckBoxColumn");
		}
		[TestMethod]
		public void GridTextButtonColumnTest()
		{
			ExecuteTest("Properties", "Columns", "GridTextButtonColumn");
		}
		[TestMethod]
		public void BasicGridGridComboBoxColumnTest()
		{
			ExecuteTest("Properties", "Columns", "BasicGridGridComboBoxColumn");
		}
		[TestMethod]
		public void GridGridComboBoxColumnSelectionChangedTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridComboBoxColumnSelectionChanged");
		}
		[TestMethod]
		public void GridGridTextBoxColumnMaxInputLengthTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridTextBoxColumnMaxInputLength");
		}
		[TestMethod]
		public void GridGridColumnCollectionIndexOfStringTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridColumnCollectionIndexOfString");
		}
		[TestMethod]
		public void GridGridColumnCollectionContainsStringTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridColumnCollectionContainsString");
		}
		[TestMethod]
		public void GridGridColumnCollectionAddGridColumnTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridColumnCollectionAddGridColumn");
		}
		[TestMethod]
		public void GridGridColumnDisplayIndexTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridColumnDisplayIndex");
		}
		[TestMethod]
		public void BasicGridGridDateTimePickerColumnTest()
		{
			ExecuteTest("Properties", "Columns", "BasicGridGridDateTimePickerColumn");
		}
		[TestMethod]
		public void GridColumnTest()
		{
			ExecuteTest("Properties", "Columns", "GridColumn");
		}
		[TestMethod]
		public void GridColumnAddRangeTest()
		{
			ExecuteTest("Properties", "Columns", "GridColumnAddRange");
		}
		[TestMethod]
		public void GridColumnCollectionContainsTest()
		{
			ExecuteTest("Properties", "Columns", "GridColumnCollectionContains");
		}
		[TestMethod]
		public void BasicGridWithDynamicColumnsTest()
		{
			ExecuteTest("Properties", "Columns", "BasicGridWithDynamicColumns");
		}
		[TestMethod]
		public void GridGridCheckBoxColumnCheckedChangedTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridCheckBoxColumnCheckedChanged");
		}
		[TestMethod]
		public void GridGridColumnHeaderTextTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridColumnHeaderText");
		}
		[TestMethod]
		public void GridGridColumnMaxWidthTest()
		{
			ExecuteTest("Properties", "Columns", "GridGridColumnMaxWidth");
		}
		[TestMethod]
		public void GridColumnMenuDisabledTest()
		{
			ExecuteTest("Properties", "Columns", "GridColumnMenuDisabled");
		}
		[TestMethod]
		public void GridColumnResizeTest()
		{
			ExecuteTest("Properties", "Columns", "GridColumnResize");
		}
		[TestMethod]
		public void GridContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "GridContextMenuStrip");
		}
		[TestMethod]
		public void GridCurrentCellTest()
		{
			ExecuteTest("Properties", "CurrentCell", "GridCurrentCell");
		}
		[TestMethod]
		public void GridCurrentRowTest()
		{
			ExecuteTest("Properties", "CurrentRow", "GridCurrentRow");
		}
		[TestMethod]
		public void GridDataSourceTest()
		{
			ExecuteTest("Properties", "DataSource", "GridDataSource");
		}
		[TestMethod]
		public void GridCellStyleFontTest()
		{
			ExecuteTest("Properties", "DefaultCellStyle", "GridCellStyleFont");
		}
		[TestMethod]
		public void GridEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "GridEnabled");
		}
		[TestMethod]
		public void GridEnableExportTest()
		{
			ExecuteTest("Properties", "EnableExport", "GridEnableExport");
		}
		[TestMethod]
		public void GridForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "GridForeColor");
		}
		[TestMethod]
		public void GridFrozenColsTest()
		{
			ExecuteTest("Properties", "FrozenCols", "GridFrozenCols");
		}
		[TestMethod]
		public void GridGroupFieldTest()
		{
			ExecuteTest("Properties", "GroupField", "GridGroupField");
		}
		[TestMethod]
		public void GridHeightTest()
		{
			ExecuteTest("Properties", "Height", "GridHeight");
		}
		[TestMethod]
		public void GridIsGroupingGridTest()
		{
			ExecuteTest("Properties", "IsGroupingGrid", "GridIsGroupingGrid");
		}
		[TestMethod]
		public void GridLeftTest()
		{
			ExecuteTest("Properties", "Left", "GridLeft");
		}
		[TestMethod]
		public void GridLocationTest()
		{
			ExecuteTest("Properties", "Location", "GridLocation");
		}
		[TestMethod]
		public void GridMaxRowsTest()
		{
			ExecuteTest("Properties", "MaxRows", "GridMaxRows");
		}
		[TestMethod]
		public void GridPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "GridPixelHeight");
		}
		[TestMethod]
		public void GridPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "GridPixelLeft");
		}
		[TestMethod]
		public void GridPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "GridPixelTop");
		}
		[TestMethod]
		public void GridPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "GridPixelWidth");
		}
		[TestMethod]
		public void GridReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "GridReadOnly");
		}
		[TestMethod]
		public void GridRowCountTest()
		{
			ExecuteTest("Properties", "RowCount", "GridRowCount");
		}
		[TestMethod]
		public void GridRowDataTest()
		{
			ExecuteTest("Properties", "RowData", "GridRowData");
		}
		[TestMethod]
		public void GridRowHeaderCornerIconTest()
		{
			ExecuteTest("Properties", "RowHeaderCornerIcon", "GridRowHeaderCornerIcon");
		}
		[TestMethod]
		public void GridRowHeaderVisibleTest()
		{
			ExecuteTest("Properties", "RowHeaderVisible", "GridRowHeaderVisible");
		}
		[TestMethod]
		public void GridGridRowCollectionAddObjectArrayTest()
		{
			ExecuteTest("Properties", "Rows", "GridGridRowCollectionAddObjectArray");
		}
		[TestMethod]
		public void GridGridRowCollectionRemoveAtTest()
		{
			ExecuteTest("Properties", "Rows", "GridGridRowCollectionRemoveAt");
		}
		[TestMethod]
		public void GridGridRowSelectedTest()
		{
			ExecuteTest("Properties", "Rows", "GridGridRowSelected");
		}
		[TestMethod]
		public void GridRowCollectionIndexerTest()
		{
			ExecuteTest("Properties", "Rows", "GridRowCollectionIndexer");
		}
		[TestMethod]
		public void GridSearchPanelTest()
		{
			ExecuteTest("Properties", "SearchPanel", "GridSearchPanel");
		}
		[TestMethod]
		public void GridSelectedColumnIndexTest()
		{
			ExecuteTest("Properties", "SelectedColumnIndex", "GridSelectedColumnIndex");
		}
		[TestMethod]
		public void GridSelectedRowIndexTest()
		{
			ExecuteTest("Properties", "SelectedRowIndex", "GridSelectedRowIndex");
		}
		[TestMethod]
		public void GridSelectedRowsTest()
		{
			ExecuteTest("Properties", "SelectedRows", "GridSelectedRows");
		}
		[TestMethod]
		public void GridSelectionColorTest()
		{
			ExecuteTest("Properties", "SelectionColor", "GridSelectionColor");
		}
		[TestMethod]
		public void GridSelectionModeTest()
		{
			ExecuteTest("Properties", "SelectionMode", "GridSelectionMode");
		}
		[TestMethod]
		public void GridShowAutoFilterColumnItemTest()
		{
			ExecuteTest("Properties", "ShowAutoFilterColumnItem", "GridShowAutoFilterColumnItem");
		}
		[TestMethod]
		public void GridSizeTest()
		{
			ExecuteTest("Properties", "Size", "GridSize");
		}
		[TestMethod]
		public void GridSortableColumnsTest()
		{
			ExecuteTest("Properties", "SortableColumns", "GridSortableColumns");
		}
		[TestMethod]
		public void GridTagTest()
		{
			ExecuteTest("Properties", "Tag", "GridTag");
		}
		[TestMethod]
		public void GridTextTest()
		{
			ExecuteTest("Properties", "Text", "GridText");
		}
		[TestMethod]
		public void GridTopTest()
		{
			ExecuteTest("Properties", "Top", "GridTop");
		}
		[TestMethod]
		public void GridVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "GridVisible");
		}
		[TestMethod]
		public void GridWidthTest()
		{
			ExecuteTest("Properties", "Width", "GridWidth");
		}
		[TestMethod]
		public void GridChangeColumnPositionTest()
		{
			ExecuteTest("Methods", "ChangeColumnPosition", "GridChangeColumnPosition");
		}
		[TestMethod]
		public void GridEditCellByPositionTest()
		{
			ExecuteTest("Methods", "EditCellByPosition", "GridEditCellByPosition");
		}
		[TestMethod]
		public void GridGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "GridGetType");
		}
		[TestMethod]
		public void GridHideTest()
		{
			ExecuteTest("Methods", "Hide", "GridHide");
		}
		[TestMethod]
		public void GridRefreshDataTest()
		{
			ExecuteTest("Methods", "RefreshData", "GridRefreshData");
		}
		[TestMethod]
		public void GridSelectCellTest()
		{
			ExecuteTest("Methods", "SelectCell", "GridSelectCell");
		}
		[TestMethod]
		public void GridSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "GridSetBounds");
		}
		[TestMethod]
		public void GridSetCellBackgroundColorTest()
		{
			ExecuteTest("Methods", "SetCellBackgroundColor", "GridSetCellBackgroundColor");
		}
		[TestMethod]
		public void GridSetCellStyleTest()
		{
			ExecuteTest("Methods", "SetCellStyle", "GridSetCellStyle");
		}
		[TestMethod]
		public void GridSetRowHeaderBackgroundColorTest()
		{
			ExecuteTest("Methods", "SetRowHeaderBackgroundColor", "GridSetRowHeaderBackgroundColor");
		}
		[TestMethod]
		public void GridSetRowHeaderBackgroundImageTest()
		{
			ExecuteTest("Methods", "SetRowHeaderBackgroundImage", "GridSetRowHeaderBackgroundImage");
		}
		[TestMethod]
		public void GridSetRowVisibilityTest()
		{
			ExecuteTest("Methods", "SetRowVisibility", "GridSetRowVisibility");
		}
		[TestMethod]
		public void GridShowTest()
		{
			ExecuteTest("Methods", "Show", "GridShow");
		}
		[TestMethod]
		public void GridSortTest()
		{
			ExecuteTest("Methods", "Sort", "GridSort");
		}
		[TestMethod]
		public void GridToStringTest()
		{
			ExecuteTest("Methods", "ToString", "GridToString");
		}
		[TestMethod]
		public void GridBeforeContextMenuShowTest()
		{
			ExecuteTest("Events", "BeforeContextMenuShow", "GridBeforeContextMenuShow");
		}
		[TestMethod]
		public void GridBeforeSelectionChangedTest()
		{
			ExecuteTest("Events", "BeforeSelectionChanged", "GridBeforeSelectionChanged");
		}
		[TestMethod]
		public void GridCancelEditTest()
		{
			ExecuteTest("Events", "CancelEdit", "GridCancelEdit");
		}
		[TestMethod]
		public void GridCellBeginEditTest()
		{
			ExecuteTest("Events", "CellBeginEdit", "GridCellBeginEdit");
		}
		[TestMethod]
		public void GridCellClickTest()
		{
			ExecuteTest("Events", "CellClick", "GridCellClick");
		}
		[TestMethod]
		public void GridCellContentClickTest()
		{
			ExecuteTest("Events", "CellContentClick", "GridCellContentClick");
		}
		[TestMethod]
		public void GridCellDoubleClickTest()
		{
			ExecuteTest("Events", "CellDoubleClick", "GridCellDoubleClick");
		}
		[TestMethod]
		public void GridCellEndEditTest()
		{
			ExecuteTest("Events", "CellEndEdit", "GridCellEndEdit");
		}
		[TestMethod]
		public void GridCellMouseDownTest()
		{
			ExecuteTest("Events", "CellMouseDown", "GridCellMouseDown");
		}
		[TestMethod]
		public void GridClickTest()
		{
			ExecuteTest("Events", "Click", "GridClick");
		}
		[TestMethod]
		public void GridColumnDisplayIndexChangedTest()
		{
			ExecuteTest("Events", "ColumnDisplayIndexChanged", "GridColumnDisplayIndexChanged");
		}
		[TestMethod]
		public void GridColumnHeaderMouseClickTest()
		{
			ExecuteTest("Events", "ColumnHeaderMouseClick", "GridColumnHeaderMouseClick");
		}
		[TestMethod]
		public void GridColumnHeaderMouseRightClickTest()
		{
			ExecuteTest("Events", "ColumnHeaderMouseRightClick", "GridColumnHeaderMouseRightClick");
		}
		[TestMethod]
		public void GridCurrentCellChangedTest()
		{
			ExecuteTest("Events", "CurrentCellChanged", "GridCurrentCellChanged");
		}
		[TestMethod]
		public void GridKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "GridKeyDown");
		}
		[TestMethod]
		public void GridLeaveTest()
		{
			ExecuteTest("Events", "Leave", "GridLeave");
		}
		[TestMethod]
		public void GridMouseRightClickTest()
		{
			ExecuteTest("Events", "MouseRightClick", "GridMouseRightClick");
		}
		[TestMethod]
		public void GridRowClickTest()
		{
			ExecuteTest("Events", "RowClick", "GridRowClick");
		}
		[TestMethod]
		public void GridRowHeaderMouseRightClickTest()
		{
			ExecuteTest("Events", "RowHeaderMouseRightClick", "GridRowHeaderMouseRightClick");
		}
		[TestMethod]
		public void GridSelectionChangedTest()
		{
			ExecuteTest("Events", "SelectionChanged", "GridSelectionChanged");
		}
		[TestMethod]
		public void GridSelectionChanged1EditCellInside25206QATest()
		{
			ExecuteTest("Events", "SelectionChanged", "GridSelectionChanged1EditCellInside25206QA");
		}
		[TestMethod]
		public void GridSelectionChanged2TextBoxColumnQATest()
		{
			ExecuteTest("Events", "SelectionChanged", "GridSelectionChanged2TextBoxColumnQA");
		}
		[TestMethod]
		public void GridSelectionChanged3CheckBoxColumnQATest()
		{
			ExecuteTest("Events", "SelectionChanged", "GridSelectionChanged3CheckBoxColumnQA");
		}
		[TestMethod]
		public void GridSelectionChanged4ComboBoxColumnQATest()
		{
			ExecuteTest("Events", "SelectionChanged", "GridSelectionChanged4ComboBoxColumnQA");
		}
		[TestMethod]
		public void GridSelectionChanged5DateTimePickerColumnQATest()
		{
			ExecuteTest("Events", "SelectionChanged", "GridSelectionChanged5DateTimePickerColumnQA");
		}
		[TestMethod]
		public void GridSelectionChanged6TextButtonColumnQATest()
		{
			ExecuteTest("Events", "SelectionChanged", "GridSelectionChanged6TextButtonColumnQA");
		}
	}

	[TestClass]
	public partial class ComboGridElementTests : WebTestBase
	{
	        

		public ComboGridElementTests():base("ComboGrid", "ComboGridElement")
		{

		}

		[TestMethod]
		public void ComboGridBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "ComboGridBasic");
		}
		[TestMethod]
		public void ComboGridDroppedDownTest()
		{
			ExecuteTest("Properties", "DroppedDown", "ComboGridDroppedDown");
		}
		[TestMethod]
		public void ComboGridExpandOnFocusTest()
		{
			ExecuteTest("Properties", "ExpandOnFocus", "ComboGridExpandOnFocus");
		}
		[TestMethod]
		public void ComboGridPreviousValueTest()
		{
			ExecuteTest("Properties", "PreviousValue", "ComboGridPreviousValue");
		}
		[TestMethod]
		public void ComboGridSearchPanelTest()
		{
			ExecuteTest("Properties", "SearchPanel", "ComboGridSearchPanel");
		}
		[TestMethod]
		public void ComboGridSelectedIndexTest()
		{
			ExecuteTest("Properties", "SelectedIndex", "ComboGridSelectedIndex");
		}
		[TestMethod]
		public void ComboGridSelectedTextTest()
		{
			ExecuteTest("Properties", "SelectedText", "ComboGridSelectedText");
		}
		[TestMethod]
		public void ComboGridSelectedValueTest()
		{
			ExecuteTest("Properties", "SelectedValue", "ComboGridSelectedValue");
		}
		[TestMethod]
		public void ComboGridShowHeaderTest()
		{
			ExecuteTest("Properties", "ShowHeader", "ComboGridShowHeader");
		}
		[TestMethod]
		public void ComboGridTextTest()
		{
			ExecuteTest("Properties", "Text", "ComboGridText");
		}
		[TestMethod]
		public void ComboGridSelectedIndexChangedTest()
		{
			ExecuteTest("Events", "SelectedIndexChanged", "ComboGridSelectedIndexChanged");
		}
		[TestMethod]
		public void ComboGridSelectionChangedTest()
		{
			ExecuteTest("Events", "SelectionChanged", "ComboGridSelectionChanged");
		}
		[TestMethod]
		public void ComboGridTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "ComboGridTextChanged");
		}
	}

	[TestClass]
	public partial class MaskedTextBoxElementTests : WebTestBase
	{
	        

		public MaskedTextBoxElementTests():base("MaskedTextBox", "MaskedTextBoxElement")
		{

		}

		[TestMethod]
		public void BasicMaskedTextBoxTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicMaskedTextBox");
		}
		[TestMethod]
		public void MaskedTextBoxMaskTest()
		{
			ExecuteTest("Properties", "Mask", "MaskedTextBoxMask");
		}
		[TestMethod]
		public void MaskedTextBoxMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "MaskedTextBoxMaximumSize");
		}
		[TestMethod]
		public void MaskedTextBoxMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "MaskedTextBoxMinimumSize");
		}
		[TestMethod]
		public void MaskedTextBoxPromptCharTest()
		{
			ExecuteTest("Properties", "PromptChar", "MaskedTextBoxPromptChar");
		}
		[TestMethod]
		public void MaskedTextBoxRegexTest()
		{
			ExecuteTest("Properties", "Regex", "MaskedTextBoxRegex");
		}
		[TestMethod]
		public void MaskedTextBoxSelectOnFocusTest()
		{
			ExecuteTest("Properties", "SelectOnFocus", "MaskedTextBoxSelectOnFocus");
		}
		[TestMethod]
		public void MaskedTextBoxTextTest()
		{
			ExecuteTest("Properties", "Text", "MaskedTextBoxText");
		}
		[TestMethod]
		public void MaskedTextBoxFocusTest()
		{
			ExecuteTest("Methods", "Focus", "MaskedTextBoxFocus");
		}
	}

	[TestClass]
	public partial class TabElementTests : WebTestBase
	{
	        

		public TabElementTests():base("Tab Control", "TabElement")
		{

		}

		[TestMethod]
		public void BasicTabElementTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicTabElement");
		}
		[TestMethod]
		public void TabAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "TabAccessibleRole");
		}
		[TestMethod]
		public void TabAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "TabAnchor");
		}
		[TestMethod]
		public void TabAppearanceTest()
		{
			ExecuteTest("Properties", "Appearance", "TabAppearance");
		}
		[TestMethod]
		public void TabApplicationTest()
		{
			ExecuteTest("Properties", "Application", "TabApplication");
		}
		[TestMethod]
		public void TabApplicationIdTest()
		{
			ExecuteTest("Properties", "ApplicationId", "TabApplicationId");
		}
		[TestMethod]
		public void TabBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "TabBorderStyle");
		}
		[TestMethod]
		public void TabBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "TabBounds");
		}
		[TestMethod]
		public void TabChildrenTest()
		{
			ExecuteTest("Properties", "Children", "TabChildren");
		}
		[TestMethod]
		public void TabClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "TabClientID");
		}
		[TestMethod]
		public void TabClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "TabClientRectangle");
		}
		[TestMethod]
		public void TabClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "TabClientSize");
		}
		[TestMethod]
		public void TabContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "TabContextMenuStrip");
		}
		[TestMethod]
		public void TabControlsTest()
		{
			ExecuteTest("Properties", "Controls", "TabControls");
		}
		[TestMethod]
		public void TabCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "TabCursor");
		}
		[TestMethod]
		public void TabDockTest()
		{
			ExecuteTest("Properties", "Dock", "TabDock");
		}
		[TestMethod]
		public void TabDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "TabDraggable");
		}
		[TestMethod]
		public void TabEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "TabEnabled");
		}
		[TestMethod]
		public void TabHandleTest()
		{
			ExecuteTest("Properties", "Handle", "TabHandle");
		}
		[TestMethod]
		public void TabHeightTest()
		{
			ExecuteTest("Properties", "Height", "TabHeight");
		}
		[TestMethod]
		public void TabImageListTest()
		{
			ExecuteTest("Properties", "ImageList", "TabImageList");
		}
		[TestMethod]
		public void TabLeftTest()
		{
			ExecuteTest("Properties", "Left", "TabLeft");
		}
		[TestMethod]
		public void TabLocationTest()
		{
			ExecuteTest("Properties", "Location", "TabLocation");
		}
		[TestMethod]
		public void TabPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "TabPixelHeight");
		}
		[TestMethod]
		public void TabPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "TabPixelLeft");
		}
		[TestMethod]
		public void TabPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "TabPixelTop");
		}
		[TestMethod]
		public void TabPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "TabPixelWidth");
		}
		[TestMethod]
		public void TabPreviousIndexTest()
		{
			ExecuteTest("Properties", "PreviousIndex", "TabPreviousIndex");
		}
		[TestMethod]
		public void TabRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "TabRightToLeft");
		}
		[TestMethod]
		public void TabSelectedIndexTest()
		{
			ExecuteTest("Properties", "SelectedIndex", "TabSelectedIndex");
		}
		[TestMethod]
		public void SelectedIndexTabElementWithInitSelectedTabTest()
		{
			ExecuteTest("Properties", "SelectedIndex", "SelectedIndexTabElementWithInitSelectedTab");
		}
		[TestMethod]
		public void TabSelectedTabTest()
		{
			ExecuteTest("Properties", "SelectedTab", "TabSelectedTab");
		}
		[TestMethod]
		public void TabSizeTest()
		{
			ExecuteTest("Properties", "Size", "TabSize");
		}
		[TestMethod]
		public void TabTabItemCollectionAddStringTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemCollectionAddString");
		}
		[TestMethod]
		public void TabTabItemForeColorTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemForeColor");
		}
		[TestMethod]
		public void TabTabItemsTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItems");
		}
		[TestMethod]
		public void TabTabItemIndexTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemIndex");
		}
		[TestMethod]
		public void TabTabItemCollectionCountTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemCollectionCount");
		}
		[TestMethod]
		public void TabTabItemTextTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemText");
		}
		[TestMethod]
		public void TabTabItemCollectionRemoveAtTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemCollectionRemoveAt");
		}
		[TestMethod]
		public void TabTabItemBackColorTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemBackColor");
		}
		[TestMethod]
		public void TabTabItemBorderStyleTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemBorderStyle");
		}
		[TestMethod]
		public void TabTabItemBackgroundImageTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemBackgroundImage");
		}
		[TestMethod]
		public void TabTabItemBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemBackgroundImageLayout");
		}
		[TestMethod]
		public void TabTabItemImageIndexTest()
		{
			ExecuteTest("Properties", "TabItems", "TabTabItemImageIndex");
		}
		[TestMethod]
		public void TabTabsPerRowTest()
		{
			ExecuteTest("Properties", "TabsPerRow", "TabTabsPerRow");
		}
		[TestMethod]
		public void TabTagTest()
		{
			ExecuteTest("Properties", "Tag", "TabTag");
		}
		[TestMethod]
		public void TabTopTest()
		{
			ExecuteTest("Properties", "Top", "TabTop");
		}
		[TestMethod]
		public void TabVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "TabVisible");
		}
		[TestMethod]
		public void TabWidthTest()
		{
			ExecuteTest("Properties", "Width", "TabWidth");
		}
		[TestMethod]
		public void TabDeselectTabIntTest()
		{
			ExecuteTest("Methods", "DeselectTab", "TabDeselectTabInt");
		}
		[TestMethod]
		public void TabDeselectTabStringTest()
		{
			ExecuteTest("Methods", "DeselectTab", "TabDeselectTabString");
		}
		[TestMethod]
		public void TabDeselectTabTabItemTest()
		{
			ExecuteTest("Methods", "DeselectTab", "TabDeselectTabTabItem");
		}
		[TestMethod]
		public void TabFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "TabFindForm");
		}
		[TestMethod]
		public void TabGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "TabGetType");
		}
		[TestMethod]
		public void TabHideTest()
		{
			ExecuteTest("Methods", "Hide", "TabHide");
		}
		[TestMethod]
		public void TabResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "TabResetText");
		}
		[TestMethod]
		public void TabSelectTabTest()
		{
			ExecuteTest("Methods", "SelectTab", "TabSelectTab");
		}
		[TestMethod]
		public void TabSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "TabSetBounds");
		}
		[TestMethod]
		public void TabShowTest()
		{
			ExecuteTest("Methods", "Show", "TabShow");
		}
		[TestMethod]
		public void TabToStringTest()
		{
			ExecuteTest("Methods", "ToString", "TabToString");
		}
		[TestMethod]
		public void TabDeselectingTest()
		{
			ExecuteTest("Events", "Deselecting", "TabDeselecting");
		}
		[TestMethod]
		public void TabLeaveTest()
		{
			ExecuteTest("Events", "Leave", "TabLeave");
		}
		[TestMethod]
		public void TabSelectedTest()
		{
			ExecuteTest("Events", "Selected", "TabSelected");
		}
		[TestMethod]
		public void TabSelectedIndexChangedTest()
		{
			ExecuteTest("Events", "SelectedIndexChanged", "TabSelectedIndexChanged");
		}
		[TestMethod]
		public void TabSelectingTest()
		{
			ExecuteTest("Events", "Selecting", "TabSelecting");
		}
		[TestMethod]
		public void TabVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "TabVisibleChanged");
		}
	}

	[TestClass]
	public partial class ToolBarCheckItemTests : WebTestBase
	{
	        

		public ToolBarCheckItemTests():base("ToolBarCheckItem", "ToolBarCheckItem")
		{

		}

		[TestMethod]
		public void ToolBarToolBarCheckItemEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarCheckItemEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarCheckItemVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarCheckItemVisible");
		}
	}

	[TestClass]
	public partial class ToolBarMenuItemTests : WebTestBase
	{
	        

		public ToolBarMenuItemTests():base("ToolBarMenuItem", "ToolBarMenuItem")
		{

		}

		[TestMethod]
		public void BasicToolBarMenuItemTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicToolBarMenuItem");
		}
		[TestMethod]
		public void ToolBarToolBarMenuItemEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarMenuItemEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarMenuItemImageTest()
		{
			ExecuteTest("Properties", "Image", "ToolBarToolBarMenuItemImage");
		}
		[TestMethod]
		public void ToolBarMenuItemImageScalingTest()
		{
			ExecuteTest("Properties", "ImageScaling", "ToolBarMenuItemImageScaling");
		}
		[TestMethod]
		public void ToolBarToolBarMenuItemVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarMenuItemVisible");
		}
	}

	[TestClass]
	public partial class ToolBarTextBoxTests : WebTestBase
	{
	        

		public ToolBarTextBoxTests():base("ToolBarTextBox", "ToolBarTextBox")
		{

		}

		[TestMethod]
		public void ToolBarToolBarTextBoxEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarTextBoxEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarTextBoxVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarTextBoxVisible");
		}
	}

	[TestClass]
	public partial class ComboBoxElementTests : WebTestBase
	{
	        

		public ComboBoxElementTests():base("ComboBox", "ComboBoxElement")
		{

		}

		[TestMethod]
		public void ComboBoxBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "ComboBoxBasic");
		}
		[TestMethod]
		public void ComboBoxApplicationTest()
		{
			ExecuteTest("Properties", "Application", "ComboBoxApplication");
		}
		[TestMethod]
		public void ComboBoxAutoCompleteModeTest()
		{
			ExecuteTest("Properties", "AutoCompleteMode", "ComboBoxAutoCompleteMode");
		}
		[TestMethod]
		public void ComboBoxBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "ComboBoxBackColor");
		}
		[TestMethod]
		public void ComboBoxBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "ComboBoxBounds");
		}
		[TestMethod]
		public void ComboBoxClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "ComboBoxClientRectangle");
		}
		[TestMethod]
		public void ComboBoxClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "ComboBoxClientSize");
		}
		[TestMethod]
		public void ComboBoxComboBoxColumnCollectionAddComboBoxColumnTest()
		{
			ExecuteTest("Properties", "Columns", "ComboBoxComboBoxColumnCollectionAddComboBoxColumn");
		}
		[TestMethod]
		public void ComboBoxCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "ComboBoxCursor");
		}
		[TestMethod]
		public void ComboBoxDataSourceTest()
		{
			ExecuteTest("Properties", "DataSource", "ComboBoxDataSource");
		}
		[TestMethod]
		public void ComboBoxDockTest()
		{
			ExecuteTest("Properties", "Dock", "ComboBoxDock");
		}
		[TestMethod]
		public void ComboBoxDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "ComboBoxDraggable");
		}
		[TestMethod]
		public void ComboBoxDropDownHeightTest()
		{
			ExecuteTest("Properties", "DropDownHeight", "ComboBoxDropDownHeight");
		}
		[TestMethod]
		public void ComboBoxDropDownStyleTest()
		{
			ExecuteTest("Properties", "DropDownStyle", "ComboBoxDropDownStyle");
		}
		[TestMethod]
		public void ComboBoxDropDownWidthTest()
		{
			ExecuteTest("Properties", "DropDownWidth", "ComboBoxDropDownWidth");
		}
		[TestMethod]
		public void ComboBoxDroppedDownTest()
		{
			ExecuteTest("Properties", "DroppedDown", "ComboBoxDroppedDown");
		}
		[TestMethod]
		public void ComboBoxEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ComboBoxEnabled");
		}
		[TestMethod]
		public void ComboBoxEnabledInteractionTest()
		{
			ExecuteTest("Properties", "Enabled", "ComboBoxEnabledInteraction");
		}
		[TestMethod]
		public void ComboBoxFontTest()
		{
			ExecuteTest("Properties", "Font", "ComboBoxFont");
		}
		[TestMethod]
		public void ComboBoxForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "ComboBoxForeColor");
		}
		[TestMethod]
		public void ComboBoxHeightTest()
		{
			ExecuteTest("Properties", "Height", "ComboBoxHeight");
		}
		[TestMethod]
		public void ComboBoxListItemCollectionAddObjectTest()
		{
			ExecuteTest("Properties", "Items", "ComboBoxListItemCollectionAddObject");
		}
		[TestMethod]
		public void ComboBoxListItemCollectionClearTest()
		{
			ExecuteTest("Properties", "Items", "ComboBoxListItemCollectionClear");
		}
		[TestMethod]
		public void ComboBoxItemsTest()
		{
			ExecuteTest("Properties", "Items", "ComboBoxItems");
		}
		[TestMethod]
		public void ComboBoxItemsInsertTest()
		{
			ExecuteTest("Properties", "Items", "ComboBoxItemsInsert");
		}
		[TestMethod]
		public void ComboBoxLeftTest()
		{
			ExecuteTest("Properties", "Left", "ComboBoxLeft");
		}
		[TestMethod]
		public void ComboBoxLocationTest()
		{
			ExecuteTest("Properties", "Location", "ComboBoxLocation");
		}
		[TestMethod]
		public void ComboBoxLockedTest()
		{
			ExecuteTest("Properties", "Locked", "ComboBoxLocked");
		}
		[TestMethod]
		public void ComboBoxMatchComboboxWidthTest()
		{
			ExecuteTest("Properties", "MatchComboboxWidth", "ComboBoxMatchComboboxWidth");
		}
		[TestMethod]
		public void ComboBoxMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "ComboBoxMaximumSize");
		}
		[TestMethod]
		public void ComboBoxMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "ComboBoxMinimumSize");
		}
		[TestMethod]
		public void ComboBoxMultiColumnTest()
		{
			ExecuteTest("Properties", "MultiColumn", "ComboBoxMultiColumn");
		}
		[TestMethod]
		public void ComboBoxPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "ComboBoxPixelHeight");
		}
		[TestMethod]
		public void ComboBoxPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "ComboBoxPixelLeft");
		}
		[TestMethod]
		public void ComboBoxPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "ComboBoxPixelTop");
		}
		[TestMethod]
		public void ComboBoxPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "ComboBoxPixelWidth");
		}
		[TestMethod]
		public void ComboBoxSelectedIndexTest()
		{
			ExecuteTest("Properties", "SelectedIndex", "ComboBoxSelectedIndex");
		}
		[TestMethod]
		public void ComboBoxSelectedItemTest()
		{
			ExecuteTest("Properties", "SelectedItem", "ComboBoxSelectedItem");
		}
		[TestMethod]
		public void ComboBoxSelectedTextTest()
		{
			ExecuteTest("Properties", "SelectedText", "ComboBoxSelectedText");
		}
		[TestMethod]
		public void ComboBoxSelectTextOnFocusTest()
		{
			ExecuteTest("Properties", "SelectTextOnFocus", "ComboBoxSelectTextOnFocus");
		}
		[TestMethod]
		public void ComboBoxShowFilterTest()
		{
			ExecuteTest("Properties", "ShowFilter", "ComboBoxShowFilter");
		}
		[TestMethod]
		public void ComboBoxSizeTest()
		{
			ExecuteTest("Properties", "Size", "ComboBoxSize");
		}
		[TestMethod]
		public void ComboBoxTabStopTest()
		{
			ExecuteTest("Properties", "TabStop", "ComboBoxTabStop");
		}
		[TestMethod]
		public void ComboBoxTagTest()
		{
			ExecuteTest("Properties", "Tag", "ComboBoxTag");
		}
		[TestMethod]
		public void ComboBoxTextTest()
		{
			ExecuteTest("Properties", "Text", "ComboBoxText");
		}
		[TestMethod]
		public void ComboBoxToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "ComboBoxToolTipText");
		}
		[TestMethod]
		public void ComboBoxTopTest()
		{
			ExecuteTest("Properties", "Top", "ComboBoxTop");
		}
		[TestMethod]
		public void ComboBoxVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ComboBoxVisible");
		}
		[TestMethod]
		public void ComboBoxWidthTest()
		{
			ExecuteTest("Properties", "Width", "ComboBoxWidth");
		}
		[TestMethod]
		public void ComboBoxFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "ComboBoxFindForm");
		}
		[TestMethod]
		public void ComboBoxFindStringTest()
		{
			ExecuteTest("Methods", "FindString", "ComboBoxFindString");
		}
		[TestMethod]
		public void ComboBoxFindStringExactTest()
		{
			ExecuteTest("Methods", "FindStringExact", "ComboBoxFindStringExact");
		}
		[TestMethod]
		public void ComboBoxFocusTest()
		{
			ExecuteTest("Methods", "Focus", "ComboBoxFocus");
		}
		[TestMethod]
		public void ComboBoxGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "ComboBoxGetType");
		}
		[TestMethod]
		public void ComboBoxHideTest()
		{
			ExecuteTest("Methods", "Hide", "ComboBoxHide");
		}
		[TestMethod]
		public void ComboBoxResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "ComboBoxResetText");
		}
		[TestMethod]
		public void ComboBoxSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "ComboBoxSetBounds");
		}
		[TestMethod]
		public void ComboBoxShowTest()
		{
			ExecuteTest("Methods", "Show", "ComboBoxShow");
		}
		[TestMethod]
		public void ComboBoxToStringTest()
		{
			ExecuteTest("Methods", "ToString", "ComboBoxToString");
		}
		[TestMethod]
		public void ComboBoxClickTest()
		{
			ExecuteTest("Events", "Click", "ComboBoxClick");
		}
		[TestMethod]
		public void ComboBoxGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "ComboBoxGotFocus");
		}
		[TestMethod]
		public void ComboBoxLeaveTest()
		{
			ExecuteTest("Events", "Leave", "ComboBoxLeave");
		}
		[TestMethod]
		public void ComboBoxMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "ComboBoxMouseHover");
		}
		[TestMethod]
		public void ComboBoxSelectedIndexChangedTest()
		{
			ExecuteTest("Events", "SelectedIndexChanged", "ComboBoxSelectedIndexChanged");
		}
		[TestMethod]
		public void ComboBoxTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "ComboBoxTextChanged");
		}
		[TestMethod]
		public void ComboBoxValidatedTest()
		{
			ExecuteTest("Events", "Validated", "ComboBoxValidated");
		}
		[TestMethod]
		public void ComboBoxVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "ComboBoxVisibleChanged");
		}
	}

	[TestClass]
	public partial class DirectoryListBoxElementTests : WebTestBase
	{
	        

		public DirectoryListBoxElementTests():base("DirectoryListBox", "DirectoryListBoxElement")
		{

		}

		[TestMethod]
		public void DirectoryListBoxBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "DirectoryListBoxBasic");
		}
	}

	[TestClass]
	public partial class DriveListBoxElementTests : WebTestBase
	{
	        

		public DriveListBoxElementTests():base("DriveListBox", "DriveListBoxElement")
		{

		}

		[TestMethod]
		public void DriveListBoxBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "DriveListBoxBasic");
		}
	}

	[TestClass]
	public partial class FileListBoxElementTests : WebTestBase
	{
	        

		public FileListBoxElementTests():base("FileListBox", "FileListBoxElement")
		{

		}

		[TestMethod]
		public void BasicFileSystemDialogTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicFileSystemDialog");
		}
		[TestMethod]
		public void FileListBoxBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "FileListBoxBasic");
		}
	}

	[TestClass]
	public partial class FileUploadElementTests : WebTestBase
	{
	        

		public FileUploadElementTests():base("FileUpload", "FileUploadElement")
		{

		}

		[TestMethod]
		public void BasicFileUploadTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicFileUpload");
		}
	}

	[TestClass]
	public partial class ProgressBarElementTests : WebTestBase
	{
	        

		public ProgressBarElementTests():base("ProgressBar", "ProgressBarElement")
		{

		}

		[TestMethod]
		public void BasicProgressBarTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicProgressBar");
		}
		[TestMethod]
		public void ProgressBarAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "ProgressBarAccessibleRole");
		}
		[TestMethod]
		public void ProgressBarAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "ProgressBarAnchor");
		}
		[TestMethod]
		public void ProgressBarApplicationTest()
		{
			ExecuteTest("Properties", "Application", "ProgressBarApplication");
		}
		[TestMethod]
		public void ProgressBarBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "ProgressBarBackColor");
		}
		[TestMethod]
		public void ProgressBarBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "ProgressBarBounds");
		}
		[TestMethod]
		public void ProgressBarClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "ProgressBarClientID");
		}
		[TestMethod]
		public void ProgressBarClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "ProgressBarClientRectangle");
		}
		[TestMethod]
		public void ProgressBarClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "ProgressBarClientSize");
		}
		[TestMethod]
		public void ProgressBarContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "ProgressBarContextMenuStrip");
		}
		[TestMethod]
		public void ProgressBarCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "ProgressBarCursor");
		}
		[TestMethod]
		public void ProgressBarDockTest()
		{
			ExecuteTest("Properties", "Dock", "ProgressBarDock");
		}
		[TestMethod]
		public void ProgressBarDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "ProgressBarDraggable");
		}
		[TestMethod]
		public void ProgressBarEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ProgressBarEnabled");
		}
		[TestMethod]
		public void ProgressBarForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "ProgressBarForeColor");
		}
		[TestMethod]
		public void ProgressBarHandleTest()
		{
			ExecuteTest("Properties", "Handle", "ProgressBarHandle");
		}
		[TestMethod]
		public void ProgressBarHeightTest()
		{
			ExecuteTest("Properties", "Height", "ProgressBarHeight");
		}
		[TestMethod]
		public void ProgressBarIDTest()
		{
			ExecuteTest("Properties", "ID", "ProgressBarID");
		}
		[TestMethod]
		public void ProgressBarIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "ProgressBarIsContainer");
		}
		[TestMethod]
		public void ProgressBarIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "ProgressBarIsDisposed");
		}
		[TestMethod]
		public void ProgressBarLeftTest()
		{
			ExecuteTest("Properties", "Left", "ProgressBarLeft");
		}
		[TestMethod]
		public void ProgressBarLocationTest()
		{
			ExecuteTest("Properties", "Location", "ProgressBarLocation");
		}
		[TestMethod]
		public void ProgressBarMaxTest()
		{
			ExecuteTest("Properties", "Max", "ProgressBarMax");
		}
		[TestMethod]
		public void ProgressBarMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "ProgressBarMaximumSize");
		}
		[TestMethod]
		public void ProgressBarMinTest()
		{
			ExecuteTest("Properties", "Min", "ProgressBarMin");
		}
		[TestMethod]
		public void ProgressBarMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "ProgressBarMinimumSize");
		}
		[TestMethod]
		public void ProgressBarParentTest()
		{
			ExecuteTest("Properties", "Parent", "ProgressBarParent");
		}
		[TestMethod]
		public void ProgressBarParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "ProgressBarParentElement");
		}
		[TestMethod]
		public void ProgressBarPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "ProgressBarPixelHeight");
		}
		[TestMethod]
		public void ProgressBarPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "ProgressBarPixelLeft");
		}
		[TestMethod]
		public void ProgressBarPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "ProgressBarPixelTop");
		}
		[TestMethod]
		public void ProgressBarPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "ProgressBarPixelWidth");
		}
		[TestMethod]
		public void ProgressBarRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "ProgressBarRightToLeft");
		}
		[TestMethod]
		public void ProgressBarSizeTest()
		{
			ExecuteTest("Properties", "Size", "ProgressBarSize");
		}
		[TestMethod]
		public void ProgressBarTagTest()
		{
			ExecuteTest("Properties", "Tag", "ProgressBarTag");
		}
		[TestMethod]
		public void ProgressBarToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "ProgressBarToolTipText");
		}
		[TestMethod]
		public void ProgressBarTopTest()
		{
			ExecuteTest("Properties", "Top", "ProgressBarTop");
		}
		[TestMethod]
		public void ProgressBarValueTest()
		{
			ExecuteTest("Properties", "Value", "ProgressBarValue");
		}
		[TestMethod]
		public void ProgressBarVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ProgressBarVisible");
		}
		[TestMethod]
		public void ProgressBarWidthTest()
		{
			ExecuteTest("Properties", "Width", "ProgressBarWidth");
		}
		[TestMethod]
		public void ProgressBarCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "ProgressBarCreateControl");
		}
		[TestMethod]
		public void ProgressBarEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "ProgressBarEquals");
		}
		[TestMethod]
		public void ProgressBarFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "ProgressBarFindForm");
		}
		[TestMethod]
		public void ProgressBarGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "ProgressBarGetHashCode");
		}
		[TestMethod]
		public void ProgressBarGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "ProgressBarGetKeyPressMasks");
		}
		[TestMethod]
		public void ProgressBarGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "ProgressBarGetType");
		}
		[TestMethod]
		public void ProgressBarHideTest()
		{
			ExecuteTest("Methods", "Hide", "ProgressBarHide");
		}
		[TestMethod]
		public void ProgressBarPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "ProgressBarPerformClick");
		}
		[TestMethod]
		public void ProgressBarPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "ProgressBarPerformControlAdded");
		}
		[TestMethod]
		public void ProgressBarPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "ProgressBarPerformControlRemoved");
		}
		[TestMethod]
		public void ProgressBarPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "ProgressBarPerformDoubleClick");
		}
		[TestMethod]
		public void ProgressBarPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "ProgressBarPerformDragEnter");
		}
		[TestMethod]
		public void ProgressBarPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "ProgressBarPerformDragOver");
		}
		[TestMethod]
		public void ProgressBarPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "ProgressBarPerformEnter");
		}
		[TestMethod]
		public void ProgressBarPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "ProgressBarPerformGotFocus");
		}
		[TestMethod]
		public void ProgressBarPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "ProgressBarPerformKeyDown");
		}
		[TestMethod]
		public void ProgressBarPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "ProgressBarPerformKeyPress");
		}
		[TestMethod]
		public void ProgressBarPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "ProgressBarPerformKeyUp");
		}
		[TestMethod]
		public void ProgressBarPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "ProgressBarPerformLayout");
		}
		[TestMethod]
		public void ProgressBarPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "ProgressBarPerformLeave");
		}
		[TestMethod]
		public void ProgressBarPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "ProgressBarPerformLocationChanged");
		}
		[TestMethod]
		public void ProgressBarPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "ProgressBarPerformMouseClick");
		}
		[TestMethod]
		public void ProgressBarPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "ProgressBarPerformMouseDown");
		}
		[TestMethod]
		public void ProgressBarPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "ProgressBarPerformMouseMove");
		}
		[TestMethod]
		public void ProgressBarPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "ProgressBarPerformMouseUp");
		}
		[TestMethod]
		public void ProgressBarSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "ProgressBarSetBounds");
		}
		[TestMethod]
		public void ProgressBarShowTest()
		{
			ExecuteTest("Methods", "Show", "ProgressBarShow");
		}
		[TestMethod]
		public void ProgressBarToStringTest()
		{
			ExecuteTest("Methods", "ToString", "ProgressBarToString");
		}
		[TestMethod]
		public void ProgressBarClickTest()
		{
			ExecuteTest("Events", "Click", "ProgressBarClick");
		}
		[TestMethod]
		public void ProgressBarEnterTest()
		{
			ExecuteTest("Events", "Enter", "ProgressBarEnter");
		}
		[TestMethod]
		public void ProgressBarGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "ProgressBarGotFocus");
		}
		[TestMethod]
		public void ProgressBarKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "ProgressBarKeyDown");
		}
		[TestMethod]
		public void ProgressBarKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "ProgressBarKeyPress");
		}
		[TestMethod]
		public void ProgressBarKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "ProgressBarKeyUp");
		}
		[TestMethod]
		public void ProgressBarLeaveTest()
		{
			ExecuteTest("Events", "Leave", "ProgressBarLeave");
		}
		[TestMethod]
		public void ProgressBarLoadTest()
		{
			ExecuteTest("Events", "Load", "ProgressBarLoad");
		}
		[TestMethod]
		public void ProgressBarLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "ProgressBarLocationChanged");
		}
		[TestMethod]
		public void ProgressBarLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "ProgressBarLostFocus");
		}
		[TestMethod]
		public void ProgressBarMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "ProgressBarMouseClick");
		}
		[TestMethod]
		public void ProgressBarMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "ProgressBarMouseDown");
		}
		[TestMethod]
		public void ProgressBarMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "ProgressBarMouseEnter");
		}
		[TestMethod]
		public void ProgressBarMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "ProgressBarMouseHover");
		}
		[TestMethod]
		public void ProgressBarMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "ProgressBarMouseLeave");
		}
		[TestMethod]
		public void ProgressBarMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "ProgressBarMouseMove");
		}
		[TestMethod]
		public void ProgressBarMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "ProgressBarMouseUp");
		}
		[TestMethod]
		public void ProgressBarResizeTest()
		{
			ExecuteTest("Events", "Resize", "ProgressBarResize");
		}
		[TestMethod]
		public void ProgressBarSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "ProgressBarSizeChanged");
		}
		[TestMethod]
		public void ProgressBarValidatingTest()
		{
			ExecuteTest("Events", "Validating", "ProgressBarValidating");
		}
		[TestMethod]
		public void ProgressBarVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "ProgressBarVisibleChanged");
		}
	}

	[TestClass]
	public partial class ToolBarDropDownButtonTests : WebTestBase
	{
	        

		public ToolBarDropDownButtonTests():base("ToolBarDropDownButton", "ToolBarDropDownButton")
		{

		}

		[TestMethod]
		public void ToolBarToolBarDropDownButtonEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarDropDownButtonEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarDropDownButtonVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarDropDownButtonVisible");
		}
	}

	[TestClass]
	public partial class ButtonElementTests : WebTestBase
	{
	        

		public ButtonElementTests():base("Button", "ButtonElement")
		{

		}

		[TestMethod]
		public void AllControlsTest()
		{
			ExecuteTest(String.Empty, String.Empty, "AllControls");
		}
		[TestMethod]
		public void ButtonBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "ButtonBasic");
		}
		[TestMethod]
		public void ButtonAppearanceTest()
		{
			ExecuteTest("Properties", "Appearance", "ButtonAppearance");
		}
		[TestMethod]
		public void ButtonAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "ButtonAutoSize");
		}
		[TestMethod]
		public void ButtonBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "ButtonBackColor");
		}
		[TestMethod]
		public void ButtonBackgroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "ButtonBackgroundImage");
		}
		[TestMethod]
		public void ButtonBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "ButtonBackgroundImageLayout");
		}
		[TestMethod]
		public void ButtonBindingManagerTest()
		{
			ExecuteTest("Properties", "BindingManager", "ButtonBindingManager");
		}
		[TestMethod]
		public void ButtonBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "ButtonBorderStyle");
		}
		[TestMethod]
		public void ButtonBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "ButtonBounds");
		}
		[TestMethod]
		public void ButtonChildrenTest()
		{
			ExecuteTest("Properties", "Children", "ButtonChildren");
		}
		[TestMethod]
		public void ButtonClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "ButtonClientID");
		}
		[TestMethod]
		public void ButtonClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "ButtonClientRectangle");
		}
		[TestMethod]
		public void ButtonClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "ButtonClientSize");
		}
		[TestMethod]
		public void ButtonContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "ButtonContextMenuStrip");
		}
		[TestMethod]
		public void ButtonControlsTest()
		{
			ExecuteTest("Properties", "Controls", "ButtonControls");
		}
		[TestMethod]
		public void ButtonCssClassTest()
		{
			ExecuteTest("Properties", "CssClass", "ButtonCssClass");
		}
		[TestMethod]
		public void ButtonCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "ButtonCursor");
		}
		[TestMethod]
		public void ButtonDataBindingsTest()
		{
			ExecuteTest("Properties", "DataBindings", "ButtonDataBindings");
		}
		[TestMethod]
		public void ButtonDialogResultTest()
		{
			ExecuteTest("Properties", "DialogResult", "ButtonDialogResult");
		}
		[TestMethod]
		public void ButtonDisposingTest()
		{
			ExecuteTest("Properties", "Disposing", "ButtonDisposing");
		}
		[TestMethod]
		public void ButtonDockTest()
		{
			ExecuteTest("Properties", "Dock", "ButtonDock");
		}
		[TestMethod]
		public void ButtonDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "ButtonDraggable");
		}
		[TestMethod]
		public void ButtonEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ButtonEnabled");
		}
		[TestMethod]
		public void ButtonEnableToggleTest()
		{
			ExecuteTest("Properties", "EnableToggle", "ButtonEnableToggle");
		}
		[TestMethod]
		public void ButtonFlatStyleTest()
		{
			ExecuteTest("Properties", "FlatStyle", "ButtonFlatStyle");
		}
		[TestMethod]
		public void ButtonFontTest()
		{
			ExecuteTest("Properties", "Font", "ButtonFont");
		}
		[TestMethod]
		public void ButtonForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "ButtonForeColor");
		}
		[TestMethod]
		public void ButtonHandleTest()
		{
			ExecuteTest("Properties", "Handle", "ButtonHandle");
		}
		[TestMethod]
		public void ButtonHeightTest()
		{
			ExecuteTest("Properties", "Height", "ButtonHeight");
		}
		[TestMethod]
		public void ButtonIDTest()
		{
			ExecuteTest("Properties", "ID", "ButtonID");
		}
		[TestMethod]
		public void ButtonImageTest()
		{
			ExecuteTest("Properties", "Image", "ButtonImage");
		}
		[TestMethod]
		public void ButtonImageAlignTest()
		{
			ExecuteTest("Properties", "ImageAlign", "ButtonImageAlign");
		}
		[TestMethod]
		public void ButtonImageHeightTest()
		{
			ExecuteTest("Properties", "ImageHeight", "ButtonImageHeight");
		}
		[TestMethod]
		public void ButtonImageIndexTest()
		{
			ExecuteTest("Properties", "ImageIndex", "ButtonImageIndex");
		}
		[TestMethod]
		public void ButtonImageWidthTest()
		{
			ExecuteTest("Properties", "ImageWidth", "ButtonImageWidth");
		}
		[TestMethod]
		public void ButtonIndexTest()
		{
			ExecuteTest("Properties", "Index", "ButtonIndex");
		}
		[TestMethod]
		public void ButtonInvokeRequiredTest()
		{
			ExecuteTest("Properties", "InvokeRequired", "ButtonInvokeRequired");
		}
		[TestMethod]
		public void ButtonIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "ButtonIsContainer");
		}
		[TestMethod]
		public void ButtonIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "ButtonIsDisposed");
		}
		[TestMethod]
		public void ButtonIsPressedTest()
		{
			ExecuteTest("Properties", "IsPressed", "ButtonIsPressed");
		}
		[TestMethod]
		public void ButtonLeftTest()
		{
			ExecuteTest("Properties", "Left", "ButtonLeft");
		}
		[TestMethod]
		public void ButtonLocationTest()
		{
			ExecuteTest("Properties", "Location", "ButtonLocation");
		}
		[TestMethod]
		public void ButtonMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "ButtonMaximumSize");
		}
		[TestMethod]
		public void ButtonMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "ButtonMinimumSize");
		}
		[TestMethod]
		public void ButtonMousePositionTest()
		{
			ExecuteTest("Properties", "MousePosition", "ButtonMousePosition");
		}
		[TestMethod]
		public void ButtonPaddingTest()
		{
			ExecuteTest("Properties", "Padding", "ButtonPadding");
		}
		[TestMethod]
		public void ButtonParentTest()
		{
			ExecuteTest("Properties", "Parent", "ButtonParent");
		}
		[TestMethod]
		public void ButtonParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "ButtonParentElement");
		}
		[TestMethod]
		public void ButtonPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "ButtonPixelHeight");
		}
		[TestMethod]
		public void ButtonPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "ButtonPixelLeft");
		}
		[TestMethod]
		public void ButtonPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "ButtonPixelTop");
		}
		[TestMethod]
		public void ButtonPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "ButtonPixelWidth");
		}
		[TestMethod]
		public void ButtonRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "ButtonRightToLeft");
		}
		[TestMethod]
		public void ButtonSizeTest()
		{
			ExecuteTest("Properties", "Size", "ButtonSize");
		}
		[TestMethod]
		public void ButtonTabIndexTabStopTest()
		{
			ExecuteTest("Properties", "TabIndex", "ButtonTabIndexTabStop");
		}
		[TestMethod]
		public void ButtonTagTest()
		{
			ExecuteTest("Properties", "Tag", "ButtonTag");
		}
		[TestMethod]
		public void ButtonTextTest()
		{
			ExecuteTest("Properties", "Text", "ButtonText");
		}
		[TestMethod]
		public void ButtonTextAlignTest()
		{
			ExecuteTest("Properties", "TextAlign", "ButtonTextAlign");
		}
		[TestMethod]
		public void ButtonTextImageRelationTest()
		{
			ExecuteTest("Properties", "TextImageRelation", "ButtonTextImageRelation");
		}
		[TestMethod]
		public void ButtonToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "ButtonToolTipText");
		}
		[TestMethod]
		public void ButtonTopTest()
		{
			ExecuteTest("Properties", "Top", "ButtonTop");
		}
		[TestMethod]
		public void ButtonUseVisualStyleBackColorTest()
		{
			ExecuteTest("Properties", "UseVisualStyleBackColor", "ButtonUseVisualStyleBackColor");
		}
		[TestMethod]
		public void ButtonVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ButtonVisible");
		}
		[TestMethod]
		public void ButtonWidthTest()
		{
			ExecuteTest("Properties", "Width", "ButtonWidth");
		}
		[TestMethod]
		public void ButtonAddTest()
		{
			ExecuteTest("Methods", "Add", "ButtonAdd");
		}
		[TestMethod]
		public void ButtonBeginInvokeTest()
		{
			ExecuteTest("Methods", "BeginInvoke", "ButtonBeginInvoke");
		}
		[TestMethod]
		public void ButtonBringToFrontTest()
		{
			ExecuteTest("Methods", "BringToFront", "ButtonBringToFront");
		}
		[TestMethod]
		public void ButtonCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "ButtonCreateControl");
		}
		[TestMethod]
		public void ButtonCreateGraphicsTest()
		{
			ExecuteTest("Methods", "CreateGraphics", "ButtonCreateGraphics");
		}
		[TestMethod]
		public void ButtonDisposeTest()
		{
			ExecuteTest("Methods", "Dispose", "ButtonDispose");
		}
		[TestMethod]
		public void ButtonEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "ButtonEquals");
		}
		[TestMethod]
		public void ButtonFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "ButtonFindForm");
		}
		[TestMethod]
		public void ButtonFocusTest()
		{
			ExecuteTest("Methods", "Focus", "ButtonFocus");
		}
		[TestMethod]
		public void ButtonGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "ButtonGetHashCode");
		}
		[TestMethod]
		public void ButtonGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "ButtonGetKeyPressMasks");
		}
		[TestMethod]
		public void ButtonGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "ButtonGetType");
		}
		[TestMethod]
		public void ButtonHideTest()
		{
			ExecuteTest("Methods", "Hide", "ButtonHide");
		}
		[TestMethod]
		public void ButtonPerformAfterrenderTest()
		{
			ExecuteTest("Methods", "PerformAfterrender", "ButtonPerformAfterrender");
		}
		[TestMethod]
		public void ButtonPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "ButtonPerformClick");
		}
		[TestMethod]
		public void ButtonPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "ButtonPerformControlAdded");
		}
		[TestMethod]
		public void ButtonPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "ButtonPerformControlRemoved");
		}
		[TestMethod]
		public void ButtonPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "ButtonPerformDoubleClick");
		}
		[TestMethod]
		public void ButtonPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "ButtonPerformDragEnter");
		}
		[TestMethod]
		public void ButtonPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "ButtonPerformDragOver");
		}
		[TestMethod]
		public void ButtonPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "ButtonPerformEnter");
		}
		[TestMethod]
		public void ButtonPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "ButtonPerformGotFocus");
		}
		[TestMethod]
		public void ButtonPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "ButtonPerformKeyDown");
		}
		[TestMethod]
		public void ButtonPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "ButtonPerformKeyPress");
		}
		[TestMethod]
		public void ButtonPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "ButtonPerformKeyUp");
		}
		[TestMethod]
		public void ButtonPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "ButtonPerformLayout");
		}
		[TestMethod]
		public void ButtonPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "ButtonPerformLeave");
		}
		[TestMethod]
		public void ButtonPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "ButtonPerformLocationChanged");
		}
		[TestMethod]
		public void ButtonPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "ButtonPerformMouseClick");
		}
		[TestMethod]
		public void ButtonPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "ButtonPerformMouseDown");
		}
		[TestMethod]
		public void ButtonPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "ButtonPerformMouseUp");
		}
		[TestMethod]
		public void ButtonResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "ButtonResetText");
		}
		[TestMethod]
		public void ButtonSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "ButtonSetBounds");
		}
		[TestMethod]
		public void ButtonShowTest()
		{
			ExecuteTest("Methods", "Show", "ButtonShow");
		}
		[TestMethod]
		public void ButtonToStringTest()
		{
			ExecuteTest("Methods", "ToString", "ButtonToString");
		}
		[TestMethod]
		public void ButtonAfterrenderTest()
		{
			ExecuteTest("Events", "Afterrender", "ButtonAfterrender");
		}
		[TestMethod]
		public void ButtonClickTest()
		{
			ExecuteTest("Events", "Click", "ButtonClick");
		}
		[TestMethod]
		public void ButtonControlAddedTest()
		{
			ExecuteTest("Events", "ControlAdded", "ButtonControlAdded");
		}
		[TestMethod]
		public void ButtonDoubleClickTest()
		{
			ExecuteTest("Events", "DoubleClick", "ButtonDoubleClick");
		}
		[TestMethod]
		public void ButtonEnterTest()
		{
			ExecuteTest("Events", "Enter", "ButtonEnter");
		}
		[TestMethod]
		public void ButtonGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "ButtonGotFocus");
		}
		[TestMethod]
		public void ButtonKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "ButtonKeyDown");
		}
		[TestMethod]
		public void ButtonKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "ButtonKeyPress");
		}
		[TestMethod]
		public void ButtonKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "ButtonKeyUp");
		}
		[TestMethod]
		public void ButtonLeaveTest()
		{
			ExecuteTest("Events", "Leave", "ButtonLeave");
		}
		[TestMethod]
		public void ButtonLoadTest()
		{
			ExecuteTest("Events", "Load", "ButtonLoad");
		}
		[TestMethod]
		public void ButtonLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "ButtonLocationChanged");
		}
		[TestMethod]
		public void ButtonLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "ButtonLostFocus");
		}
		[TestMethod]
		public void ButtonMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "ButtonMouseClick");
		}
		[TestMethod]
		public void ButtonMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "ButtonMouseDown");
		}
		[TestMethod]
		public void ButtonMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "ButtonMouseEnter");
		}
		[TestMethod]
		public void ButtonMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "ButtonMouseHover");
		}
		[TestMethod]
		public void ButtonMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "ButtonMouseLeave");
		}
		[TestMethod]
		public void ButtonMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "ButtonMouseMove");
		}
		[TestMethod]
		public void ButtonMouseTapHoldTest()
		{
			ExecuteTest("Events", "MouseTapHold", "ButtonMouseTapHold");
		}
		[TestMethod]
		public void ButtonMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "ButtonMouseUp");
		}
		[TestMethod]
		public void ButtonPressedChangeTest()
		{
			ExecuteTest("Events", "PressedChange", "ButtonPressedChange");
		}
		[TestMethod]
		public void ButtonResizeTest()
		{
			ExecuteTest("Events", "Resize", "ButtonResize");
		}
		[TestMethod]
		public void ButtonSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "ButtonSizeChanged");
		}
		[TestMethod]
		public void ButtonTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "ButtonTextChanged");
		}
		[TestMethod]
		public void ButtonValidatingTest()
		{
			ExecuteTest("Events", "Validating", "ButtonValidating");
		}
		[TestMethod]
		public void ButtonVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "ButtonVisibleChanged");
		}
	}

	[TestClass]
	public partial class GroupBoxElementTests : WebTestBase
	{
	        

		public GroupBoxElementTests():base("GroupBox", "GroupBoxElement")
		{

		}

		[TestMethod]
		public void BasicGroupBoxTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicGroupBox");
		}
		[TestMethod]
		public void GroupBoxContainRadioButtonsTest()
		{
			ExecuteTest(String.Empty, String.Empty, "GroupBoxContainRadioButtons");
		}
		[TestMethod]
		public void GroupBoxAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "GroupBoxAccessibleRole");
		}
		[TestMethod]
		public void GroupBoxAlignmentTest()
		{
			ExecuteTest("Properties", "Alignment", "GroupBoxAlignment");
		}
		[TestMethod]
		public void GroupBoxAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "GroupBoxAnchor");
		}
		[TestMethod]
		public void GroupBoxAppearanceTest()
		{
			ExecuteTest("Properties", "Appearance", "GroupBoxAppearance");
		}
		[TestMethod]
		public void GroupBoxApplicationTest()
		{
			ExecuteTest("Properties", "Application", "GroupBoxApplication");
		}
		[TestMethod]
		public void GroupBoxApplicationIdTest()
		{
			ExecuteTest("Properties", "ApplicationId", "GroupBoxApplicationId");
		}
		[TestMethod]
		public void GroupBoxAutoScrollTest()
		{
			ExecuteTest("Properties", "AutoScroll", "GroupBoxAutoScroll");
		}
		[TestMethod]
		public void GroupBoxAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "GroupBoxAutoSize");
		}
		[TestMethod]
		public void GroupBoxBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "GroupBoxBackColor");
		}
		[TestMethod]
		public void GroupBoxBackGroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "GroupBoxBackGroundImage");
		}
		[TestMethod]
		public void GroupBoxBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "GroupBoxBackgroundImageLayout");
		}
		[TestMethod]
		public void GroupBoxBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "GroupBoxBorderStyle");
		}
		[TestMethod]
		public void GroupBoxBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "GroupBoxBounds");
		}
		[TestMethod]
		public void GroupBoxChildrenTest()
		{
			ExecuteTest("Properties", "Children", "GroupBoxChildren");
		}
		[TestMethod]
		public void GroupBoxChildrenOrderRevertedTest()
		{
			ExecuteTest("Properties", "ChildrenOrderReverted", "GroupBoxChildrenOrderReverted");
		}
		[TestMethod]
		public void GroupBoxClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "GroupBoxClientID");
		}
		[TestMethod]
		public void GroupBoxClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "GroupBoxClientRectangle");
		}
		[TestMethod]
		public void GroupBoxClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "GroupBoxClientSize");
		}
		[TestMethod]
		public void GroupBoxContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "GroupBoxContextMenuStrip");
		}
		[TestMethod]
		public void GroupBoxControlsTest()
		{
			ExecuteTest("Properties", "Controls", "GroupBoxControls");
		}
		[TestMethod]
		public void GroupBoxCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "GroupBoxCursor");
		}
		[TestMethod]
		public void GroupBoxDockTest()
		{
			ExecuteTest("Properties", "Dock", "GroupBoxDock");
		}
		[TestMethod]
		public void GroupBoxDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "GroupBoxDraggable");
		}
		[TestMethod]
		public void GroupBoxEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "GroupBoxEnabled");
		}
		[TestMethod]
		public void GroupBoxFontTest()
		{
			ExecuteTest("Properties", "Font", "GroupBoxFont");
		}
		[TestMethod]
		public void GroupBoxForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "GroupBoxForeColor");
		}
		[TestMethod]
		public void GroupBoxHandleTest()
		{
			ExecuteTest("Properties", "Handle", "GroupBoxHandle");
		}
		[TestMethod]
		public void GroupBoxHeightTest()
		{
			ExecuteTest("Properties", "Height", "GroupBoxHeight");
		}
		[TestMethod]
		public void GroupBoxIDTest()
		{
			ExecuteTest("Properties", "ID", "GroupBoxID");
		}
		[TestMethod]
		public void GroupBoxIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "GroupBoxIsContainer");
		}
		[TestMethod]
		public void GroupBoxIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "GroupBoxIsDisposed");
		}
		[TestMethod]
		public void GroupBoxLeftTest()
		{
			ExecuteTest("Properties", "Left", "GroupBoxLeft");
		}
		[TestMethod]
		public void GroupBoxLocationTest()
		{
			ExecuteTest("Properties", "Location", "GroupBoxLocation");
		}
		[TestMethod]
		public void GroupBoxMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "GroupBoxMaximumSize");
		}
		[TestMethod]
		public void GroupBoxMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "GroupBoxMinimumSize");
		}
		[TestMethod]
		public void GroupBoxPaddingTest()
		{
			ExecuteTest("Properties", "Padding", "GroupBoxPadding");
		}
		[TestMethod]
		public void GroupBoxParentTest()
		{
			ExecuteTest("Properties", "Parent", "GroupBoxParent");
		}
		[TestMethod]
		public void GroupBoxParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "GroupBoxParentElement");
		}
		[TestMethod]
		public void GroupBoxPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "GroupBoxPixelHeight");
		}
		[TestMethod]
		public void GroupBoxPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "GroupBoxPixelLeft");
		}
		[TestMethod]
		public void GroupBoxPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "GroupBoxPixelTop");
		}
		[TestMethod]
		public void GroupBoxPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "GroupBoxPixelWidth");
		}
		[TestMethod]
		public void GroupBoxRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "GroupBoxRightToLeft");
		}
		[TestMethod]
		public void GroupBoxSizeTest()
		{
			ExecuteTest("Properties", "Size", "GroupBoxSize");
		}
		[TestMethod]
		public void GroupBoxTagTest()
		{
			ExecuteTest("Properties", "Tag", "GroupBoxTag");
		}
		[TestMethod]
		public void GroupBoxTextTest()
		{
			ExecuteTest("Properties", "Text", "GroupBoxText");
		}
		[TestMethod]
		public void GroupBoxToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "GroupBoxToolTipText");
		}
		[TestMethod]
		public void GroupBoxTopTest()
		{
			ExecuteTest("Properties", "Top", "GroupBoxTop");
		}
		[TestMethod]
		public void GroupBoxVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "GroupBoxVisible");
		}
		[TestMethod]
		public void GroupBoxWidthTest()
		{
			ExecuteTest("Properties", "Width", "GroupBoxWidth");
		}
		[TestMethod]
		public void GroupBoxCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "GroupBoxCreateControl");
		}
		[TestMethod]
		public void GroupBoxEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "GroupBoxEquals");
		}
		[TestMethod]
		public void GroupBoxFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "GroupBoxFindForm");
		}
		[TestMethod]
		public void GroupBoxFocusTest()
		{
			ExecuteTest("Methods", "Focus", "GroupBoxFocus");
		}
		[TestMethod]
		public void GroupBoxGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "GroupBoxGetHashCode");
		}
		[TestMethod]
		public void GroupBoxGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "GroupBoxGetKeyPressMasks");
		}
		[TestMethod]
		public void GroupBoxGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "GroupBoxGetType");
		}
		[TestMethod]
		public void GroupBoxHideTest()
		{
			ExecuteTest("Methods", "Hide", "GroupBoxHide");
		}
		[TestMethod]
		public void GroupBoxPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "GroupBoxPerformClick");
		}
		[TestMethod]
		public void GroupBoxPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "GroupBoxPerformControlAdded");
		}
		[TestMethod]
		public void GroupBoxPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "GroupBoxPerformControlRemoved");
		}
		[TestMethod]
		public void GroupBoxPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "GroupBoxPerformDoubleClick");
		}
		[TestMethod]
		public void GroupBoxPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "GroupBoxPerformDragEnter");
		}
		[TestMethod]
		public void GroupBoxPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "GroupBoxPerformDragOver");
		}
		[TestMethod]
		public void GroupBoxPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "GroupBoxPerformEnter");
		}
		[TestMethod]
		public void GroupBoxPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "GroupBoxPerformGotFocus");
		}
		[TestMethod]
		public void GroupBoxPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "GroupBoxPerformKeyDown");
		}
		[TestMethod]
		public void GroupBoxPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "GroupBoxPerformKeyPress");
		}
		[TestMethod]
		public void GroupBoxPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "GroupBoxPerformKeyUp");
		}
		[TestMethod]
		public void GroupBoxPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "GroupBoxPerformLayout");
		}
		[TestMethod]
		public void GroupBoxPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "GroupBoxPerformLeave");
		}
		[TestMethod]
		public void GroupBoxPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "GroupBoxPerformLocationChanged");
		}
		[TestMethod]
		public void GroupBoxPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "GroupBoxPerformMouseClick");
		}
		[TestMethod]
		public void GroupBoxPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "GroupBoxPerformMouseDown");
		}
		[TestMethod]
		public void GroupBoxPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "GroupBoxPerformMouseMove");
		}
		[TestMethod]
		public void GroupBoxPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "GroupBoxPerformMouseUp");
		}
		[TestMethod]
		public void GroupBoxPerformSizeChangedTest()
		{
			ExecuteTest("Methods", "PerformSizeChanged", "GroupBoxPerformSizeChanged");
		}
		[TestMethod]
		public void GroupBoxResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "GroupBoxResetText");
		}
		[TestMethod]
		public void GroupBoxSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "GroupBoxSetBounds");
		}
		[TestMethod]
		public void GroupBoxShowTest()
		{
			ExecuteTest("Methods", "Show", "GroupBoxShow");
		}
		[TestMethod]
		public void GroupBoxToStringTest()
		{
			ExecuteTest("Methods", "ToString", "GroupBoxToString");
		}
		[TestMethod]
		public void GroupBoxEnterTest()
		{
			ExecuteTest("Events", "Enter", "GroupBoxEnter");
		}
		[TestMethod]
		public void GroupBoxGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "GroupBoxGotFocus");
		}
		[TestMethod]
		public void GroupBoxLeaveTest()
		{
			ExecuteTest("Events", "Leave", "GroupBoxLeave");
		}
		[TestMethod]
		public void GroupBoxLoadTest()
		{
			ExecuteTest("Events", "Load", "GroupBoxLoad");
		}
		[TestMethod]
		public void GroupBoxLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "GroupBoxLocationChanged");
		}
		[TestMethod]
		public void GroupBoxLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "GroupBoxLostFocus");
		}
		[TestMethod]
		public void GroupBoxMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "GroupBoxMouseHover");
		}
		[TestMethod]
		public void GroupBoxResizeTest()
		{
			ExecuteTest("Events", "Resize", "GroupBoxResize");
		}
		[TestMethod]
		public void GroupBoxSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "GroupBoxSizeChanged");
		}
		[TestMethod]
		public void GroupBoxTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "GroupBoxTextChanged");
		}
		[TestMethod]
		public void GroupBoxValidatingTest()
		{
			ExecuteTest("Events", "Validating", "GroupBoxValidating");
		}
		[TestMethod]
		public void GroupBoxVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "GroupBoxVisibleChanged");
		}
	}

	[TestClass]
	public partial class ListViewElementTests : WebTestBase
	{
	        

		public ListViewElementTests():base("ListView", "ListViewElement")
		{

		}

		[TestMethod]
		public void BasicListViewTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicListView");
		}
		[TestMethod]
		public void ListViewAddRangeTest()
		{
			ExecuteTest(String.Empty, String.Empty, "ListViewAddRange");
		}
		[TestMethod]
		public void ListViewAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "ListViewAnchor");
		}
		[TestMethod]
		public void ListViewAppearanceTest()
		{
			ExecuteTest("Properties", "Appearance", "ListViewAppearance");
		}
		[TestMethod]
		public void ListViewApplicationTest()
		{
			ExecuteTest("Properties", "Application", "ListViewApplication");
		}
		[TestMethod]
		public void ListViewBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "ListViewBackColor");
		}
		[TestMethod]
		public void ListViewBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "ListViewBounds");
		}
		[TestMethod]
		public void ListViewCheckBoxesTest()
		{
			ExecuteTest("Properties", "CheckBoxes", "ListViewCheckBoxes");
		}
		[TestMethod]
		public void ListViewClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "ListViewClientRectangle");
		}
		[TestMethod]
		public void ListViewClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "ListViewClientSize");
		}
		[TestMethod]
		public void ListViewDockTest()
		{
			ExecuteTest("Properties", "Dock", "ListViewDock");
		}
		[TestMethod]
		public void ListViewEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ListViewEnabled");
		}
		[TestMethod]
		public void ListViewFontTest()
		{
			ExecuteTest("Properties", "Font", "ListViewFont");
		}
		[TestMethod]
		public void ListViewForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "ListViewForeColor");
		}
		[TestMethod]
		public void ListViewFullRowSelectTest()
		{
			ExecuteTest("Properties", "FullRowSelect", "ListViewFullRowSelect");
		}
		[TestMethod]
		public void ListViewHeightTest()
		{
			ExecuteTest("Properties", "Height", "ListViewHeight");
		}
		[TestMethod]
		public void ListViewHotTrackingTest()
		{
			ExecuteTest("Properties", "HotTracking", "ListViewHotTracking");
		}
		[TestMethod]
		public void ListViewItemsTest()
		{
			ExecuteTest("Properties", "Items", "ListViewItems");
		}
		[TestMethod]
		public void ListViewListViewItemCollectionBackColorTest()
		{
			ExecuteTest("Properties", "Items", "ListViewListViewItemCollectionBackColor");
		}
		[TestMethod]
		public void ListViewListViewItemCollectionAdd2StringTest()
		{
			ExecuteTest("Properties", "Items", "ListViewListViewItemCollectionAdd2String");
		}
		[TestMethod]
		public void ListViewLeftTest()
		{
			ExecuteTest("Properties", "Left", "ListViewLeft");
		}
		[TestMethod]
		public void ListViewLocationTest()
		{
			ExecuteTest("Properties", "Location", "ListViewLocation");
		}
		[TestMethod]
		public void ListViewMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "ListViewMaximumSize");
		}
		[TestMethod]
		public void ListViewMenuDisabledTest()
		{
			ExecuteTest("Properties", "MenuDisabled", "ListViewMenuDisabled");
		}
		[TestMethod]
		public void ListViewMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "ListViewMinimumSize");
		}
		[TestMethod]
		public void ListViewPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "ListViewPixelHeight");
		}
		[TestMethod]
		public void ListViewPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "ListViewPixelLeft");
		}
		[TestMethod]
		public void ListViewPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "ListViewPixelTop");
		}
		[TestMethod]
		public void ListViewPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "ListViewPixelWidth");
		}
		[TestMethod]
		public void ListViewSelectedItemsTest()
		{
			ExecuteTest("Properties", "SelectedItems", "ListViewSelectedItems");
		}
		[TestMethod]
		public void ListViewSizeTest()
		{
			ExecuteTest("Properties", "Size", "ListViewSize");
		}
		[TestMethod]
		public void ListViewTagTest()
		{
			ExecuteTest("Properties", "Tag", "ListViewTag");
		}
		[TestMethod]
		public void ListViewTopTest()
		{
			ExecuteTest("Properties", "Top", "ListViewTop");
		}
		[TestMethod]
		public void ListViewViewTest()
		{
			ExecuteTest("Properties", "View", "ListViewView");
		}
		[TestMethod]
		public void ListViewVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ListViewVisible");
		}
		[TestMethod]
		public void ListViewWidthTest()
		{
			ExecuteTest("Properties", "Width", "ListViewWidth");
		}
		[TestMethod]
		public void ListViewClearTest()
		{
			ExecuteTest("Methods", "Clear", "ListViewClear");
		}
		[TestMethod]
		public void ListViewFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "ListViewFindForm");
		}
		[TestMethod]
		public void ListViewFindItemWithText1ParamTest()
		{
			ExecuteTest("Methods", "FindItemWithText", "ListViewFindItemWithText1Param");
		}
		[TestMethod]
		public void ListViewFindItemWithText3ParamsTest()
		{
			ExecuteTest("Methods", "FindItemWithText", "ListViewFindItemWithText3Params");
		}
		[TestMethod]
		public void ListViewFindItemWithText4ParamsTest()
		{
			ExecuteTest("Methods", "FindItemWithText", "ListViewFindItemWithText4Params");
		}
		[TestMethod]
		public void ListViewFocusTest()
		{
			ExecuteTest("Methods", "Focus", "ListViewFocus");
		}
		[TestMethod]
		public void ListViewGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "ListViewGetType");
		}
		[TestMethod]
		public void ListViewHideTest()
		{
			ExecuteTest("Methods", "Hide", "ListViewHide");
		}
		[TestMethod]
		public void ListViewSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "ListViewSetBounds");
		}
		[TestMethod]
		public void ListViewShowTest()
		{
			ExecuteTest("Methods", "Show", "ListViewShow");
		}
		[TestMethod]
		public void ListViewSortTest()
		{
			ExecuteTest("Methods", "Sort", "ListViewSort");
		}
		[TestMethod]
		public void ListViewToStringTest()
		{
			ExecuteTest("Methods", "ToString", "ListViewToString");
		}
		[TestMethod]
		public void ListViewDoubleClickTest()
		{
			ExecuteTest("Events", "DoubleClick", "ListViewDoubleClick");
		}
		[TestMethod]
		public void ListViewLeaveTest()
		{
			ExecuteTest("Events", "Leave", "ListViewLeave");
		}
		[TestMethod]
		public void ListViewSelectionChangedTest()
		{
			ExecuteTest("Events", "SelectionChanged", "ListViewSelectionChanged");
		}
		[TestMethod]
		public void ListViewVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "ListViewVisibleChanged");
		}
	}

	[TestClass]
	public partial class SplitContainerTests : WebTestBase
	{
	        

		public SplitContainerTests():base("SplitContainer", "SplitContainer")
		{

		}

		[TestMethod]
		public void BasicSplitContainerTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicSplitContainer");
		}
		[TestMethod]
		public void SplitContainerExTest()
		{
			ExecuteTest(String.Empty, String.Empty, "SplitContainerEx");
		}
		[TestMethod]
		public void SplitContainerAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "SplitContainerAccessibleRole");
		}
		[TestMethod]
		public void SplitContainerAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "SplitContainerAnchor");
		}
		[TestMethod]
		public void SplitContainerApplicationTest()
		{
			ExecuteTest("Properties", "Application", "SplitContainerApplication");
		}
		[TestMethod]
		public void SplitContainerApplicationIdTest()
		{
			ExecuteTest("Properties", "ApplicationId", "SplitContainerApplicationId");
		}
		[TestMethod]
		public void SplitContainerBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "SplitContainerBackColor");
		}
		[TestMethod]
		public void SplitContainerBackGroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "SplitContainerBackGroundImage");
		}
		[TestMethod]
		public void SplitContainerBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "SplitContainerBorderStyle");
		}
		[TestMethod]
		public void SplitContainerBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "SplitContainerBounds");
		}
		[TestMethod]
		public void SplitContainerChildrenTest()
		{
			ExecuteTest("Properties", "Children", "SplitContainerChildren");
		}
		[TestMethod]
		public void SplitContainerClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "SplitContainerClientID");
		}
		[TestMethod]
		public void SplitContainerClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "SplitContainerClientRectangle");
		}
		[TestMethod]
		public void SplitContainerClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "SplitContainerClientSize");
		}
		[TestMethod]
		public void SplitContainerContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "SplitContainerContextMenuStrip");
		}
		[TestMethod]
		public void SplitContainerCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "SplitContainerCursor");
		}
		[TestMethod]
		public void SplitContainerDockTest()
		{
			ExecuteTest("Properties", "Dock", "SplitContainerDock");
		}
		[TestMethod]
		public void SplitContainerDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "SplitContainerDraggable");
		}
		[TestMethod]
		public void SplitContainerEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "SplitContainerEnabled");
		}
		[TestMethod]
		public void SplitContainerFixedPanelTest()
		{
			ExecuteTest("Properties", "FixedPanel", "SplitContainerFixedPanel");
		}
		[TestMethod]
		public void SplitContainerForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "SplitContainerForeColor");
		}
		[TestMethod]
		public void SplitContainerHandleTest()
		{
			ExecuteTest("Properties", "Handle", "SplitContainerHandle");
		}
		[TestMethod]
		public void SplitContainerHeightTest()
		{
			ExecuteTest("Properties", "Height", "SplitContainerHeight");
		}
		[TestMethod]
		public void SplitContainerIDTest()
		{
			ExecuteTest("Properties", "ID", "SplitContainerID");
		}
		[TestMethod]
		public void SplitContainerIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "SplitContainerIsContainer");
		}
		[TestMethod]
		public void SplitContainerIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "SplitContainerIsDisposed");
		}
		[TestMethod]
		public void SplitContainerLeftTest()
		{
			ExecuteTest("Properties", "Left", "SplitContainerLeft");
		}
		[TestMethod]
		public void SplitContainerLocationTest()
		{
			ExecuteTest("Properties", "Location", "SplitContainerLocation");
		}
		[TestMethod]
		public void SplitContainerMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "SplitContainerMaximumSize");
		}
		[TestMethod]
		public void SplitContainerMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "SplitContainerMinimumSize");
		}
		[TestMethod]
		public void SplitContainerOrientationTest()
		{
			ExecuteTest("Properties", "Orientation", "SplitContainerOrientation");
		}
		[TestMethod]
		public void SplitContainerPanel1Test()
		{
			ExecuteTest("Properties", "Panel1", "SplitContainerPanel1");
		}
		[TestMethod]
		public void SplitContainerPanel1CollapsedTest()
		{
			ExecuteTest("Properties", "Panel1Collapsed", "SplitContainerPanel1Collapsed");
		}
		[TestMethod]
		public void SplitContainerPanel1MinSizeTest()
		{
			ExecuteTest("Properties", "Panel1MinSize", "SplitContainerPanel1MinSize");
		}
		[TestMethod]
		public void SplitContainerPanel2Test()
		{
			ExecuteTest("Properties", "Panel2", "SplitContainerPanel2");
		}
		[TestMethod]
		public void SplitContainerPanel2CollapsedTest()
		{
			ExecuteTest("Properties", "Panel2Collapsed", "SplitContainerPanel2Collapsed");
		}
		[TestMethod]
		public void SplitContainerParentTest()
		{
			ExecuteTest("Properties", "Parent", "SplitContainerParent");
		}
		[TestMethod]
		public void SplitContainerParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "SplitContainerParentElement");
		}
		[TestMethod]
		public void SplitContainerPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "SplitContainerPixelHeight");
		}
		[TestMethod]
		public void SplitContainerPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "SplitContainerPixelLeft");
		}
		[TestMethod]
		public void SplitContainerPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "SplitContainerPixelTop");
		}
		[TestMethod]
		public void SplitContainerPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "SplitContainerPixelWidth");
		}
		[TestMethod]
		public void SplitContainerSizeTest()
		{
			ExecuteTest("Properties", "Size", "SplitContainerSize");
		}
		[TestMethod]
		public void SplitContainerSplitterDistanceTest()
		{
			ExecuteTest("Properties", "SplitterDistance", "SplitContainerSplitterDistance");
		}
		[TestMethod]
		public void SplitContainerTabIndexTabStopTest()
		{
			ExecuteTest("Properties", "TabIndex", "SplitContainerTabIndexTabStop");
		}
		[TestMethod]
		public void SplitContainerTagTest()
		{
			ExecuteTest("Properties", "Tag", "SplitContainerTag");
		}
		[TestMethod]
		public void SplitContainerToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "SplitContainerToolTipText");
		}
		[TestMethod]
		public void SplitContainerTopTest()
		{
			ExecuteTest("Properties", "Top", "SplitContainerTop");
		}
		[TestMethod]
		public void SplitContainerVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "SplitContainerVisible");
		}
		[TestMethod]
		public void SplitContainerWidthTest()
		{
			ExecuteTest("Properties", "Width", "SplitContainerWidth");
		}
		[TestMethod]
		public void SplitContainerEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "SplitContainerEquals");
		}
		[TestMethod]
		public void SplitContainerFocusTest()
		{
			ExecuteTest("Methods", "Focus", "SplitContainerFocus");
		}
		[TestMethod]
		public void SplitContainerGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "SplitContainerGetHashCode");
		}
		[TestMethod]
		public void SplitContainerGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "SplitContainerGetKeyPressMasks");
		}
		[TestMethod]
		public void SplitContainerGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "SplitContainerGetType");
		}
		[TestMethod]
		public void SplitContainerHideTest()
		{
			ExecuteTest("Methods", "Hide", "SplitContainerHide");
		}
		[TestMethod]
		public void SplitContainerPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "SplitContainerPerformClick");
		}
		[TestMethod]
		public void SplitContainerPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "SplitContainerPerformControlAdded");
		}
		[TestMethod]
		public void SplitContainerPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "SplitContainerPerformControlRemoved");
		}
		[TestMethod]
		public void SplitContainerPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "SplitContainerPerformDoubleClick");
		}
		[TestMethod]
		public void SplitContainerPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "SplitContainerPerformDragEnter");
		}
		[TestMethod]
		public void SplitContainerPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "SplitContainerPerformDragOver");
		}
		[TestMethod]
		public void SplitContainerPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "SplitContainerPerformEnter");
		}
		[TestMethod]
		public void SplitContainerPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "SplitContainerPerformGotFocus");
		}
		[TestMethod]
		public void SplitContainerPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "SplitContainerPerformKeyDown");
		}
		[TestMethod]
		public void SplitContainerPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "SplitContainerPerformKeyPress");
		}
		[TestMethod]
		public void SplitContainerPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "SplitContainerPerformKeyUp");
		}
		[TestMethod]
		public void SplitContainerPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "SplitContainerPerformLayout");
		}
		[TestMethod]
		public void SplitContainerPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "SplitContainerPerformLeave");
		}
		[TestMethod]
		public void SplitContainerPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "SplitContainerPerformLocationChanged");
		}
		[TestMethod]
		public void SplitContainerPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "SplitContainerPerformMouseClick");
		}
		[TestMethod]
		public void SplitContainerPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "SplitContainerPerformMouseDown");
		}
		[TestMethod]
		public void SplitContainerPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "SplitContainerPerformMouseMove");
		}
		[TestMethod]
		public void SplitContainerPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "SplitContainerPerformMouseUp");
		}
		[TestMethod]
		public void SplitContainerPerformSizeChangedTest()
		{
			ExecuteTest("Methods", "PerformSizeChanged", "SplitContainerPerformSizeChanged");
		}
		[TestMethod]
		public void SplitContainerResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "SplitContainerResetText");
		}
		[TestMethod]
		public void SplitContainerSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "SplitContainerSetBounds");
		}
		[TestMethod]
		public void SplitContainerShowTest()
		{
			ExecuteTest("Methods", "Show", "SplitContainerShow");
		}
		[TestMethod]
		public void SplitContainerToStringTest()
		{
			ExecuteTest("Methods", "ToString", "SplitContainerToString");
		}
		[TestMethod]
		public void SplitContainerClickTest()
		{
			ExecuteTest("Events", "Click", "SplitContainerClick");
		}
		[TestMethod]
		public void SplitContainerEnterTest()
		{
			ExecuteTest("Events", "Enter", "SplitContainerEnter");
		}
		[TestMethod]
		public void SplitContainerGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "SplitContainerGotFocus");
		}
		[TestMethod]
		public void SplitContainerKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "SplitContainerKeyDown");
		}
		[TestMethod]
		public void SplitContainerKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "SplitContainerKeyPress");
		}
		[TestMethod]
		public void SplitContainerKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "SplitContainerKeyUp");
		}
		[TestMethod]
		public void SplitContainerLeaveTest()
		{
			ExecuteTest("Events", "Leave", "SplitContainerLeave");
		}
		[TestMethod]
		public void SplitContainerLoadTest()
		{
			ExecuteTest("Events", "Load", "SplitContainerLoad");
		}
		[TestMethod]
		public void SplitContainerLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "SplitContainerLocationChanged");
		}
		[TestMethod]
		public void SplitContainerMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "SplitContainerMouseClick");
		}
		[TestMethod]
		public void SplitContainerMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "SplitContainerMouseDown");
		}
		[TestMethod]
		public void SplitContainerMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "SplitContainerMouseEnter");
		}
		[TestMethod]
		public void SplitContainerMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "SplitContainerMouseHover");
		}
		[TestMethod]
		public void SplitContainerMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "SplitContainerMouseLeave");
		}
		[TestMethod]
		public void SplitContainerMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "SplitContainerMouseMove");
		}
		[TestMethod]
		public void SplitContainerMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "SplitContainerMouseUp");
		}
		[TestMethod]
		public void SplitContainerResizeTest()
		{
			ExecuteTest("Events", "Resize", "SplitContainerResize");
		}
		[TestMethod]
		public void SplitContainerSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "SplitContainerSizeChanged");
		}
		[TestMethod]
		public void SplitContainerValidatingTest()
		{
			ExecuteTest("Events", "Validating", "SplitContainerValidating");
		}
		[TestMethod]
		public void SplitContainerVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "SplitContainerVisibleChanged");
		}
	}

	[TestClass]
	public partial class ToolBarButtonTests : WebTestBase
	{
	        

		public ToolBarButtonTests():base("ToolBarButton", "ToolBarButton")
		{

		}

		[TestMethod]
		public void ToolBarToolBarButtonEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarButtonEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarButtonTextImageRelationTest()
		{
			ExecuteTest("Properties", "TextImageRelation", "ToolBarToolBarButtonTextImageRelation");
		}
		[TestMethod]
		public void ToolBarToolBarButtonVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarButtonVisible");
		}
	}

	[TestClass]
	public partial class TreeElementTests : WebTestBase
	{
	        

		public TreeElementTests():base("Tree", "TreeElement")
		{

		}

		[TestMethod]
		public void BasicTreeTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicTree");
		}
		[TestMethod]
		public void TreeBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "TreeBackColor");
		}
		[TestMethod]
		public void TreeBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "TreeBorderStyle");
		}
		[TestMethod]
		public void TreeBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "TreeBounds");
		}
		[TestMethod]
		public void TreeCheckBoxesTest()
		{
			ExecuteTest("Properties", "CheckBoxes", "TreeCheckBoxes");
		}
		[TestMethod]
		public void TreeClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "TreeClientRectangle");
		}
		[TestMethod]
		public void TreeClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "TreeClientSize");
		}
		[TestMethod]
		public void TreeEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "TreeEnabled");
		}
		[TestMethod]
		public void TreeFontTest()
		{
			ExecuteTest("Properties", "Font", "TreeFont");
		}
		[TestMethod]
		public void TreeForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "TreeForeColor");
		}
		[TestMethod]
		public void TreeHeightTest()
		{
			ExecuteTest("Properties", "Height", "TreeHeight");
		}
		[TestMethod]
		public void TreeItemCollectionContainsKeyTest()
		{
			ExecuteTest("Properties", "Items", "TreeItemCollectionContainsKey");
		}
		[TestMethod]
		public void TreeKeyFieldNameTest()
		{
			ExecuteTest("Properties", "KeyFieldName", "TreeKeyFieldName");
		}
		[TestMethod]
		public void TreeLeftTest()
		{
			ExecuteTest("Properties", "Left", "TreeLeft");
		}
		[TestMethod]
		public void TreeLocationTest()
		{
			ExecuteTest("Properties", "Location", "TreeLocation");
		}
		[TestMethod]
		public void TreeParentFieldNameTest()
		{
			ExecuteTest("Properties", "ParentFieldName", "TreeParentFieldName");
		}
		[TestMethod]
		public void TreePixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "TreePixelHeight");
		}
		[TestMethod]
		public void TreePixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "TreePixelLeft");
		}
		[TestMethod]
		public void TreePixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "TreePixelTop");
		}
		[TestMethod]
		public void TreePixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "TreePixelWidth");
		}
		[TestMethod]
		public void TreeSearchPanelTest()
		{
			ExecuteTest("Properties", "SearchPanel", "TreeSearchPanel");
		}
		[TestMethod]
		public void TreeSelectedItemTest()
		{
			ExecuteTest("Properties", "SelectedItem", "TreeSelectedItem");
		}
		[TestMethod]
		public void TreeSizeTest()
		{
			ExecuteTest("Properties", "Size", "TreeSize");
		}
		[TestMethod]
		public void TreeTagTest()
		{
			ExecuteTest("Properties", "Tag", "TreeTag");
		}
		[TestMethod]
		public void TreeTopTest()
		{
			ExecuteTest("Properties", "Top", "TreeTop");
		}
		[TestMethod]
		public void TreeVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "TreeVisible");
		}
		[TestMethod]
		public void TreeWidthTest()
		{
			ExecuteTest("Properties", "Width", "TreeWidth");
		}
		[TestMethod]
		public void TreeAddTest()
		{
			ExecuteTest("Methods", "Add", "TreeAdd");
		}
		[TestMethod]
		public void TreeCollapseAllTest()
		{
			ExecuteTest("Methods", "CollapseAll", "TreeCollapseAll");
		}
		[TestMethod]
		public void TreeGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "TreeGetType");
		}
		[TestMethod]
		public void TreeHideTest()
		{
			ExecuteTest("Methods", "Hide", "TreeHide");
		}
		[TestMethod]
		public void TreeSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "TreeSetBounds");
		}
		[TestMethod]
		public void TreeShowTest()
		{
			ExecuteTest("Methods", "Show", "TreeShow");
		}
		[TestMethod]
		public void TreeToStringTest()
		{
			ExecuteTest("Methods", "ToString", "TreeToString");
		}
		[TestMethod]
		public void TreeAfterCheckTest()
		{
			ExecuteTest("Events", "AfterCheck", "TreeAfterCheck");
		}
		[TestMethod]
		public void TreeBeforeCheckTest()
		{
			ExecuteTest("Events", "BeforeCheck", "TreeBeforeCheck");
		}
		[TestMethod]
		public void TreeLeaveTest()
		{
			ExecuteTest("Events", "Leave", "TreeLeave");
		}
	}

	[TestClass]
	public partial class CompositeElementTests : WebTestBase
	{
	        

		public CompositeElementTests():base("CompositeView", "CompositeElement")
		{

		}

		[TestMethod]
		public void CompositeContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "CompositeContextMenuStrip");
		}
		[TestMethod]
		public void CompositePerformUnloadTest()
		{
			ExecuteTest("Methods", "PerformUnload", "CompositePerformUnload");
		}
	}

	[TestClass]
	public partial class MenuElementTests : WebTestBase
	{
	        

		public MenuElementTests():base("Menu", "MenuElement")
		{

		}

		[TestMethod]
		public void BasicMenuTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicMenu");
		}
		[TestMethod]
		public void MenuItemsTest()
		{
			ExecuteTest("Properties", "Items", "MenuItems");
		}
	}

	[TestClass]
	public partial class TimerElementTests : WebTestBase
	{
	        

		public TimerElementTests():base("Timer", "TimerElement")
		{

		}

		[TestMethod]
		public void BasicTimerTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicTimer");
		}
		[TestMethod]
		public void TimerTickFromLoadTest()
		{
			ExecuteTest(String.Empty, String.Empty, "TimerTickFromLoad");
		}
		[TestMethod]
		public void TimerApplicationTest()
		{
			ExecuteTest("Properties", "Application", "TimerApplication");
		}
		[TestMethod]
		public void TimerApplicationIdTest()
		{
			ExecuteTest("Properties", "ApplicationId", "TimerApplicationId");
		}
		[TestMethod]
		public void TimerClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "TimerClientID");
		}
		[TestMethod]
		public void TimerEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "TimerEnabled");
		}
		[TestMethod]
		public void TimerIDTest()
		{
			ExecuteTest("Properties", "ID", "TimerID");
		}
		[TestMethod]
		public void TimerIntervalTest()
		{
			ExecuteTest("Properties", "Interval", "TimerInterval");
		}
		[TestMethod]
		public void TimerIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "TimerIsContainer");
		}
		[TestMethod]
		public void TimerParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "TimerParentElement");
		}
		[TestMethod]
		public void TimerDisposeTest()
		{
			ExecuteTest("Methods", "Dispose", "TimerDispose");
		}
		[TestMethod]
		public void TimerGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "TimerGetHashCode");
		}
		[TestMethod]
		public void TimerGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "TimerGetType");
		}
		[TestMethod]
		public void TimerStartTest()
		{
			ExecuteTest("Methods", "Start", "TimerStart");
		}
		[TestMethod]
		public void TimerStopTest()
		{
			ExecuteTest("Methods", "Stop", "TimerStop");
		}
		[TestMethod]
		public void TimerToStringTest()
		{
			ExecuteTest("Methods", "ToString", "TimerToString");
		}
		[TestMethod]
		public void TimerTickTest()
		{
			ExecuteTest("Events", "Tick", "TimerTick");
		}
	}

	[TestClass]
	public partial class CheckBoxElementTests : WebTestBase
	{
	        

		public CheckBoxElementTests():base("CheckBox", "CheckBoxElement")
		{

		}

		[TestMethod]
		public void CheckBoxBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "CheckBoxBasic");
		}
		[TestMethod]
		public void CheckBoxArrayBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "CheckBoxArrayBasic");
		}
		[TestMethod]
		public void CheckBoxAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "CheckBoxAccessibleRole");
		}
		[TestMethod]
		public void CheckBoxAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "CheckBoxAnchor");
		}
		[TestMethod]
		public void CheckBoxAppearanceTest()
		{
			ExecuteTest("Properties", "Appearance", "CheckBoxAppearance");
		}
		[TestMethod]
		public void CheckBoxApplicationTest()
		{
			ExecuteTest("Properties", "Application", "CheckBoxApplication");
		}
		[TestMethod]
		public void CheckBoxAutoCheckTest()
		{
			ExecuteTest("Properties", "AutoCheck", "CheckBoxAutoCheck");
		}
		[TestMethod]
		public void CheckBoxAutoSizeTest()
		{
			ExecuteTest("Properties", "AutoSize", "CheckBoxAutoSize");
		}
		[TestMethod]
		public void CheckBoxBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "CheckBoxBackColor");
		}
		[TestMethod]
		public void CheckBoxBackgroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "CheckBoxBackgroundImage");
		}
		[TestMethod]
		public void CheckBoxBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "CheckBoxBackgroundImageLayout");
		}
		[TestMethod]
		public void CheckBoxBindingManagerTest()
		{
			ExecuteTest("Properties", "BindingManager", "CheckBoxBindingManager");
		}
		[TestMethod]
		public void CheckBoxBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "CheckBoxBounds");
		}
		[TestMethod]
		public void CheckBoxCheckAlignTest()
		{
			ExecuteTest("Properties", "CheckAlign", "CheckBoxCheckAlign");
		}
		[TestMethod]
		public void CheckBoxCheckStateTest()
		{
			ExecuteTest("Properties", "CheckState", "CheckBoxCheckState");
		}
		[TestMethod]
		public void CheckBoxChildrenTest()
		{
			ExecuteTest("Properties", "Children", "CheckBoxChildren");
		}
		[TestMethod]
		public void CheckBoxChildrenOrderRevertedTest()
		{
			ExecuteTest("Properties", "ChildrenOrderReverted", "CheckBoxChildrenOrderReverted");
		}
		[TestMethod]
		public void CheckBoxClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "CheckBoxClientID");
		}
		[TestMethod]
		public void CheckBoxClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "CheckBoxClientRectangle");
		}
		[TestMethod]
		public void CheckBoxClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "CheckBoxClientSize");
		}
		[TestMethod]
		public void CheckBoxContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "CheckBoxContextMenuStrip");
		}
		[TestMethod]
		public void CheckBoxControlsTest()
		{
			ExecuteTest("Properties", "Controls", "CheckBoxControls");
		}
		[TestMethod]
		public void CheckBoxCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "CheckBoxCursor");
		}
		[TestMethod]
		public void CheckBoxDataBindingsTest()
		{
			ExecuteTest("Properties", "DataBindings", "CheckBoxDataBindings");
		}
		[TestMethod]
		public void CheckBoxDockTest()
		{
			ExecuteTest("Properties", "Dock", "CheckBoxDock");
		}
		[TestMethod]
		public void CheckBoxDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "CheckBoxDraggable");
		}
		[TestMethod]
		public void CheckBoxEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "CheckBoxEnabled");
		}
		[TestMethod]
		public void CheckBoxFlatStyleTest()
		{
			ExecuteTest("Properties", "FlatStyle", "CheckBoxFlatStyle");
		}
		[TestMethod]
		public void CheckBoxFontTest()
		{
			ExecuteTest("Properties", "Font", "CheckBoxFont");
		}
		[TestMethod]
		public void CheckBoxForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "CheckBoxForeColor");
		}
		[TestMethod]
		public void CheckBoxHandleTest()
		{
			ExecuteTest("Properties", "Handle", "CheckBoxHandle");
		}
		[TestMethod]
		public void CheckBoxHeightTest()
		{
			ExecuteTest("Properties", "Height", "CheckBoxHeight");
		}
		[TestMethod]
		public void CheckBoxIDTest()
		{
			ExecuteTest("Properties", "ID", "CheckBoxID");
		}
		[TestMethod]
		public void CheckBoxImageTest()
		{
			ExecuteTest("Properties", "Image", "CheckBoxImage");
		}
		[TestMethod]
		public void CheckBoxImageAlignTest()
		{
			ExecuteTest("Properties", "ImageAlign", "CheckBoxImageAlign");
		}
		[TestMethod]
		public void CheckBoxImageHeightTest()
		{
			ExecuteTest("Properties", "ImageHeight", "CheckBoxImageHeight");
		}
		[TestMethod]
		public void CheckBoxImageWidthTest()
		{
			ExecuteTest("Properties", "ImageWidth", "CheckBoxImageWidth");
		}
		[TestMethod]
		public void CheckBoxIsCheckedTest()
		{
			ExecuteTest("Properties", "IsChecked", "CheckBoxIsChecked");
		}
		[TestMethod]
		public void CheckBoxIsCheckedIntTest()
		{
			ExecuteTest("Properties", "IsCheckedInt", "CheckBoxIsCheckedInt");
		}
		[TestMethod]
		public void CheckBoxIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "CheckBoxIsContainer");
		}
		[TestMethod]
		public void CheckBoxIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "CheckBoxIsDisposed");
		}
		[TestMethod]
		public void CheckBoxLeftTest()
		{
			ExecuteTest("Properties", "Left", "CheckBoxLeft");
		}
		[TestMethod]
		public void CheckBoxLocationTest()
		{
			ExecuteTest("Properties", "Location", "CheckBoxLocation");
		}
		[TestMethod]
		public void CheckBoxMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "CheckBoxMaximumSize");
		}
		[TestMethod]
		public void CheckBoxMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "CheckBoxMinimumSize");
		}
		[TestMethod]
		public void CheckBoxPaddingTest()
		{
			ExecuteTest("Properties", "Padding", "CheckBoxPadding");
		}
		[TestMethod]
		public void CheckBoxParentTest()
		{
			ExecuteTest("Properties", "Parent", "CheckBoxParent");
		}
		[TestMethod]
		public void CheckBoxParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "CheckBoxParentElement");
		}
		[TestMethod]
		public void CheckBoxPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "CheckBoxPixelHeight");
		}
		[TestMethod]
		public void CheckBoxPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "CheckBoxPixelLeft");
		}
		[TestMethod]
		public void CheckBoxPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "CheckBoxPixelTop");
		}
		[TestMethod]
		public void CheckBoxPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "CheckBoxPixelWidth");
		}
		[TestMethod]
		public void CheckBoxRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "CheckBoxRightToLeft");
		}
		[TestMethod]
		public void CheckBoxSizeTest()
		{
			ExecuteTest("Properties", "Size", "CheckBoxSize");
		}
		[TestMethod]
		public void CheckBoxTabIndexTabStopTest()
		{
			ExecuteTest("Properties", "TabIndex", "CheckBoxTabIndexTabStop");
		}
		[TestMethod]
		public void CheckBoxTagTest()
		{
			ExecuteTest("Properties", "Tag", "CheckBoxTag");
		}
		[TestMethod]
		public void CheckBoxTextTest()
		{
			ExecuteTest("Properties", "Text", "CheckBoxText");
		}
		[TestMethod]
		public void CheckBoxTextAlignTest()
		{
			ExecuteTest("Properties", "TextAlign", "CheckBoxTextAlign");
		}
		[TestMethod]
		public void CheckBoxThreeStateTest()
		{
			ExecuteTest("Properties", "ThreeState", "CheckBoxThreeState");
		}
		[TestMethod]
		public void CheckBoxToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "CheckBoxToolTipText");
		}
		[TestMethod]
		public void CheckBoxTopTest()
		{
			ExecuteTest("Properties", "Top", "CheckBoxTop");
		}
		[TestMethod]
		public void CheckBoxUseVisualStyleBackColorTest()
		{
			ExecuteTest("Properties", "UseVisualStyleBackColor", "CheckBoxUseVisualStyleBackColor");
		}
		[TestMethod]
		public void CheckBoxVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "CheckBoxVisible");
		}
		[TestMethod]
		public void CheckBoxWidthTest()
		{
			ExecuteTest("Properties", "Width", "CheckBoxWidth");
		}
		[TestMethod]
		public void CheckBoxBringToFrontTest()
		{
			ExecuteTest("Methods", "BringToFront", "CheckBoxBringToFront");
		}
		[TestMethod]
		public void CheckBoxCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "CheckBoxCreateControl");
		}
		[TestMethod]
		public void CheckBoxEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "CheckBoxEquals");
		}
		[TestMethod]
		public void CheckBoxFocusTest()
		{
			ExecuteTest("Methods", "Focus", "CheckBoxFocus");
		}
		[TestMethod]
		public void CheckBoxGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "CheckBoxGetHashCode");
		}
		[TestMethod]
		public void CheckBoxGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "CheckBoxGetKeyPressMasks");
		}
		[TestMethod]
		public void CheckBoxGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "CheckBoxGetType");
		}
		[TestMethod]
		public void CheckBoxHideTest()
		{
			ExecuteTest("Methods", "Hide", "CheckBoxHide");
		}
		[TestMethod]
		public void CheckBoxPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "CheckBoxPerformClick");
		}
		[TestMethod]
		public void CheckBoxPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "CheckBoxPerformControlAdded");
		}
		[TestMethod]
		public void CheckBoxPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "CheckBoxPerformControlRemoved");
		}
		[TestMethod]
		public void CheckBoxPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "CheckBoxPerformDoubleClick");
		}
		[TestMethod]
		public void CheckBoxPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "CheckBoxPerformDragEnter");
		}
		[TestMethod]
		public void CheckBoxPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "CheckBoxPerformDragOver");
		}
		[TestMethod]
		public void CheckBoxPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "CheckBoxPerformEnter");
		}
		[TestMethod]
		public void CheckBoxPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "CheckBoxPerformGotFocus");
		}
		[TestMethod]
		public void CheckBoxPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "CheckBoxPerformKeyDown");
		}
		[TestMethod]
		public void CheckBoxPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "CheckBoxPerformKeyPress");
		}
		[TestMethod]
		public void CheckBoxPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "CheckBoxPerformKeyUp");
		}
		[TestMethod]
		public void CheckBoxPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "CheckBoxPerformLayout");
		}
		[TestMethod]
		public void CheckBoxPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "CheckBoxPerformLeave");
		}
		[TestMethod]
		public void CheckBoxPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "CheckBoxPerformLocationChanged");
		}
		[TestMethod]
		public void CheckBoxPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "CheckBoxPerformMouseClick");
		}
		[TestMethod]
		public void CheckBoxPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "CheckBoxPerformMouseDown");
		}
		[TestMethod]
		public void CheckBoxPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "CheckBoxPerformMouseMove");
		}
		[TestMethod]
		public void CheckBoxPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "CheckBoxPerformMouseUp");
		}
		[TestMethod]
		public void CheckBoxResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "CheckBoxResetText");
		}
		[TestMethod]
		public void CheckBoxSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "CheckBoxSetBounds");
		}
		[TestMethod]
		public void CheckBoxShowTest()
		{
			ExecuteTest("Methods", "Show", "CheckBoxShow");
		}
		[TestMethod]
		public void CheckBoxToStringTest()
		{
			ExecuteTest("Methods", "ToString", "CheckBoxToString");
		}
		[TestMethod]
		public void CheckBoxCheckedChangedTest()
		{
			ExecuteTest("Events", "CheckedChanged", "CheckBoxCheckedChanged");
		}
		[TestMethod]
		public void CheckBoxCheckStateChangedTest()
		{
			ExecuteTest("Events", "CheckStateChanged", "CheckBoxCheckStateChanged");
		}
		[TestMethod]
		public void CheckBoxClickTest()
		{
			ExecuteTest("Events", "Click", "CheckBoxClick");
		}
		[TestMethod]
		public void CheckBoxDoubleClickTest()
		{
			ExecuteTest("Events", "DoubleClick", "CheckBoxDoubleClick");
		}
		[TestMethod]
		public void CheckBoxEnterTest()
		{
			ExecuteTest("Events", "Enter", "CheckBoxEnter");
		}
		[TestMethod]
		public void CheckBoxKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "CheckBoxKeyDown");
		}
		[TestMethod]
		public void CheckBoxKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "CheckBoxKeyPress");
		}
		[TestMethod]
		public void CheckBoxKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "CheckBoxKeyUp");
		}
		[TestMethod]
		public void CheckBoxLeaveTest()
		{
			ExecuteTest("Events", "Leave", "CheckBoxLeave");
		}
		[TestMethod]
		public void CheckBoxLoadTest()
		{
			ExecuteTest("Events", "Load", "CheckBoxLoad");
		}
		[TestMethod]
		public void CheckBoxLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "CheckBoxLocationChanged");
		}
		[TestMethod]
		public void CheckBoxLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "CheckBoxLostFocus");
		}
		[TestMethod]
		public void CheckBoxMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "CheckBoxMouseClick");
		}
		[TestMethod]
		public void CheckBoxMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "CheckBoxMouseDown");
		}
		[TestMethod]
		public void CheckBoxMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "CheckBoxMouseEnter");
		}
		[TestMethod]
		public void CheckBoxMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "CheckBoxMouseHover");
		}
		[TestMethod]
		public void CheckBoxMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "CheckBoxMouseLeave");
		}
		[TestMethod]
		public void CheckBoxMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "CheckBoxMouseMove");
		}
		[TestMethod]
		public void CheckBoxMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "CheckBoxMouseUp");
		}
		[TestMethod]
		public void CheckBoxResizeTest()
		{
			ExecuteTest("Events", "Resize", "CheckBoxResize");
		}
		[TestMethod]
		public void CheckBoxTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "CheckBoxTextChanged");
		}
		[TestMethod]
		public void CheckBoxValidatingTest()
		{
			ExecuteTest("Events", "Validating", "CheckBoxValidating");
		}
		[TestMethod]
		public void CheckBoxVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "CheckBoxVisibleChanged");
		}
	}

	[TestClass]
	public partial class ColorDialogTests : WebTestBase
	{
	        

		public ColorDialogTests():base("ColorDialog", "ColorDialog")
		{

		}

		[TestMethod]
		public void ColorDialogBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "ColorDialogBasic");
		}
	}

	[TestClass]
	public partial class CheckedListBoxElementTests : WebTestBase
	{
	        

		public CheckedListBoxElementTests():base("CheckedListBox", "CheckedListBoxElement")
		{

		}

		[TestMethod]
		public void CheckedListBoxBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "CheckedListBoxBasic");
		}
		[TestMethod]
		public void CheckedListBoxAutoScrollTest()
		{
			ExecuteTest("Properties", "AutoScroll", "CheckedListBoxAutoScroll");
		}
		[TestMethod]
		public void CheckedListBoxCheckedItemsTest()
		{
			ExecuteTest("Properties", "CheckedItems", "CheckedListBoxCheckedItems");
		}
		[TestMethod]
		public void CheckedListBoxGetItemCheckedTest()
		{
			ExecuteTest("Methods", "GetItemChecked", "CheckedListBoxGetItemChecked");
		}
		[TestMethod]
		public void CheckedListBoxSetItemCheckedIntBoolTest()
		{
			ExecuteTest("Methods", "SetItemChecked", "CheckedListBoxSetItemCheckedIntBool");
		}
		[TestMethod]
		public void CheckedListBoxItemCheckTest()
		{
			ExecuteTest("Events", "ItemCheck", "CheckedListBoxItemCheck");
		}
	}

	[TestClass]
	public partial class GridButtonColumnTests : WebTestBase
	{
	        

		public GridButtonColumnTests():base("GridButtonColumn", "GridButtonColumn")
		{

		}

		[TestMethod]
		public void BasicGridButtonColumnTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicGridButtonColumn");
		}
	}

	[TestClass]
	public partial class GridCheckBoxColumnTests : WebTestBase
	{
	        

		public GridCheckBoxColumnTests():base("GridCheckBoxColumn", "GridCheckBoxColumn")
		{

		}

		[TestMethod]
		public void GridGridCheckBoxColumnReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "GridGridCheckBoxColumnReadOnly");
		}
	}

	[TestClass]
	public partial class GridTextBoxColumnTests : WebTestBase
	{
	        

		public GridTextBoxColumnTests():base("GridTextBoxColumn", "GridTextBoxColumn")
		{

		}

		[TestMethod]
		public void GridGridTextBoxColumnReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "GridGridTextBoxColumnReadOnly");
		}
	}

	[TestClass]
	public partial class ImageElementTests : WebTestBase
	{
	        

		public ImageElementTests():base("Image", "ImageElement")
		{

		}

		[TestMethod]
		public void BasicImageTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicImage");
		}
		[TestMethod]
		public void ImageBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "ImageBackColor");
		}
		[TestMethod]
		public void ImageBackgroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "ImageBackgroundImage");
		}
		[TestMethod]
		public void ImageBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "ImageBackgroundImageLayout");
		}
		[TestMethod]
		public void ImageBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "ImageBorderStyle");
		}
		[TestMethod]
		public void ImageBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "ImageBounds");
		}
		[TestMethod]
		public void ImageClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "ImageClientRectangle");
		}
		[TestMethod]
		public void ImageClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "ImageClientSize");
		}
		[TestMethod]
		public void ImageEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ImageEnabled");
		}
		[TestMethod]
		public void ImageHeightTest()
		{
			ExecuteTest("Properties", "Height", "ImageHeight");
		}
		[TestMethod]
		public void ImageImageTest()
		{
			ExecuteTest("Properties", "Image", "ImageImage");
		}
		[TestMethod]
		public void ImageLeftTest()
		{
			ExecuteTest("Properties", "Left", "ImageLeft");
		}
		[TestMethod]
		public void ImageLocationTest()
		{
			ExecuteTest("Properties", "Location", "ImageLocation");
		}
		[TestMethod]
		public void ImagePixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "ImagePixelHeight");
		}
		[TestMethod]
		public void ImagePixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "ImagePixelLeft");
		}
		[TestMethod]
		public void ImagePixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "ImagePixelTop");
		}
		[TestMethod]
		public void ImagePixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "ImagePixelWidth");
		}
		[TestMethod]
		public void ImageSizeTest()
		{
			ExecuteTest("Properties", "Size", "ImageSize");
		}
		[TestMethod]
		public void ImageTagTest()
		{
			ExecuteTest("Properties", "Tag", "ImageTag");
		}
		[TestMethod]
		public void ImageTopTest()
		{
			ExecuteTest("Properties", "Top", "ImageTop");
		}
		[TestMethod]
		public void ImageVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ImageVisible");
		}
		[TestMethod]
		public void ImageWidthTest()
		{
			ExecuteTest("Properties", "Width", "ImageWidth");
		}
		[TestMethod]
		public void ImageGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "ImageGetType");
		}
		[TestMethod]
		public void ImageHideTest()
		{
			ExecuteTest("Methods", "Hide", "ImageHide");
		}
		[TestMethod]
		public void ImageSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "ImageSetBounds");
		}
		[TestMethod]
		public void ImageShowTest()
		{
			ExecuteTest("Methods", "Show", "ImageShow");
		}
		[TestMethod]
		public void ImageToStringTest()
		{
			ExecuteTest("Methods", "ToString", "ImageToString");
		}
	}

	[TestClass]
	public partial class ColumnHeaderCollectionTests : WebTestBase
	{
	        

		public ColumnHeaderCollectionTests():base("ColumnHeaderCollection", "ColumnHeaderCollection")
		{

		}

		[TestMethod]
		public void ListViewColumnHeaderCollectionAddInt2StringTest()
		{
			ExecuteTest("Methods", "Add", "ListViewColumnHeaderCollectionAddInt2String");
		}
	}

	[TestClass]
	public partial class ListViewItemTests : WebTestBase
	{
	        

		public ListViewItemTests():base("ListViewItem", "ListViewItem")
		{

		}

		[TestMethod]
		public void ListViewItemCheckedTest()
		{
			ExecuteTest("Properties", "Checked", "ListViewItemChecked");
		}
	}

	[TestClass]
	public partial class ListViewSubitemTests : WebTestBase
	{
	        

		public ListViewSubitemTests():base("ListViewSubitem", "ListViewSubitem")
		{

		}

		[TestMethod]
		public void ListViewSubitemForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "ListViewSubitemForeColor");
		}
	}

	[TestClass]
	public partial class TreeItemTests : WebTestBase
	{
	        

		public TreeItemTests():base("TreeItem", "TreeItem")
		{

		}

		[TestMethod]
		public void TreeItemCheckedTest()
		{
			ExecuteTest("Properties", "Checked", "TreeItemChecked");
		}
	}

	[TestClass]
	public partial class DateTimePickerElementTests : WebTestBase
	{
	        

		public DateTimePickerElementTests():base("DateTimePicker", "DateTimePickerElement")
		{

		}

		[TestMethod]
		public void DateTimePickerBasicTest()
		{
			ExecuteTest(String.Empty, String.Empty, "DateTimePickerBasic");
		}
		[TestMethod]
		public void DateTimePickerCustomFormatTest()
		{
			ExecuteTest("Properties", "CustomFormat", "DateTimePickerCustomFormat");
		}
		[TestMethod]
		public void DateTimePickerEditableTest()
		{
			ExecuteTest("Properties", "Editable", "DateTimePickerEditable");
		}
		[TestMethod]
		public void DateTimePickerShowUpDownTest()
		{
			ExecuteTest("Properties", "ShowUpDown", "DateTimePickerShowUpDown");
		}
		[TestMethod]
		public void DateTimePickerValueTest()
		{
			ExecuteTest("Properties", "Value", "DateTimePickerValue");
		}
	}

	[TestClass]
	public partial class NumericUpDownElementTests : WebTestBase
	{
	        

		public NumericUpDownElementTests():base("NumericUpDown", "NumericUpDownElement")
		{

		}

		[TestMethod]
		public void BasicNumericUpDownTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicNumericUpDown");
		}
		[TestMethod]
		public void NumericUpDownAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "NumericUpDownAccessibleRole");
		}
		[TestMethod]
		public void NumericUpDownAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "NumericUpDownAnchor");
		}
		[TestMethod]
		public void NumericUpDownApplicationTest()
		{
			ExecuteTest("Properties", "Application", "NumericUpDownApplication");
		}
		[TestMethod]
		public void NumericUpDownBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "NumericUpDownBorderStyle");
		}
		[TestMethod]
		public void NumericUpDownBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "NumericUpDownBounds");
		}
		[TestMethod]
		public void NumericUpDownClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "NumericUpDownClientID");
		}
		[TestMethod]
		public void NumericUpDownClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "NumericUpDownClientRectangle");
		}
		[TestMethod]
		public void NumericUpDownClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "NumericUpDownClientSize");
		}
		[TestMethod]
		public void NumericUpDownContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "NumericUpDownContextMenuStrip");
		}
		[TestMethod]
		public void NumericUpDownCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "NumericUpDownCursor");
		}
		[TestMethod]
		public void NumericUpDownDockTest()
		{
			ExecuteTest("Properties", "Dock", "NumericUpDownDock");
		}
		[TestMethod]
		public void NumericUpDownDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "NumericUpDownDraggable");
		}
		[TestMethod]
		public void NumericUpDownEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "NumericUpDownEnabled");
		}
		[TestMethod]
		public void NumericUpDownFontTest()
		{
			ExecuteTest("Properties", "Font", "NumericUpDownFont");
		}
		[TestMethod]
		public void NumericUpDownForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "NumericUpDownForeColor");
		}
		[TestMethod]
		public void NumericUpDownHeightTest()
		{
			ExecuteTest("Properties", "Height", "NumericUpDownHeight");
		}
		[TestMethod]
		public void NumericUpDownHexadecimalTest()
		{
			ExecuteTest("Properties", "Hexadecimal", "NumericUpDownHexadecimal");
		}
		[TestMethod]
		public void NumericUpDownIDTest()
		{
			ExecuteTest("Properties", "ID", "NumericUpDownID");
		}
		[TestMethod]
		public void NumericUpDownIncrementTest()
		{
			ExecuteTest("Properties", "Increment", "NumericUpDownIncrement");
		}
		[TestMethod]
		public void NumericUpDownIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "NumericUpDownIsContainer");
		}
		[TestMethod]
		public void NumericUpDownIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "NumericUpDownIsDisposed");
		}
		[TestMethod]
		public void NumericUpDownLeftTest()
		{
			ExecuteTest("Properties", "Left", "NumericUpDownLeft");
		}
		[TestMethod]
		public void NumericUpDownLocationTest()
		{
			ExecuteTest("Properties", "Location", "NumericUpDownLocation");
		}
		[TestMethod]
		public void NumericUpDownMaximumTest()
		{
			ExecuteTest("Properties", "Maximum", "NumericUpDownMaximum");
		}
		[TestMethod]
		public void NumericUpDownMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "NumericUpDownMaximumSize");
		}
		[TestMethod]
		public void NumericUpDownMinimumTest()
		{
			ExecuteTest("Properties", "Minimum", "NumericUpDownMinimum");
		}
		[TestMethod]
		public void NumericUpDownMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "NumericUpDownMinimumSize");
		}
		[TestMethod]
		public void NumericUpDownParentTest()
		{
			ExecuteTest("Properties", "Parent", "NumericUpDownParent");
		}
		[TestMethod]
		public void NumericUpDownParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "NumericUpDownParentElement");
		}
		[TestMethod]
		public void NumericUpDownPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "NumericUpDownPixelHeight");
		}
		[TestMethod]
		public void NumericUpDownPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "NumericUpDownPixelLeft");
		}
		[TestMethod]
		public void NumericUpDownPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "NumericUpDownPixelTop");
		}
		[TestMethod]
		public void NumericUpDownPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "NumericUpDownPixelWidth");
		}
		[TestMethod]
		public void NumericUpDownRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "NumericUpDownRightToLeft");
		}
		[TestMethod]
		public void NumericUpDownSizeTest()
		{
			ExecuteTest("Properties", "Size", "NumericUpDownSize");
		}
		[TestMethod]
		public void NumericUpDownTabIndexTabStopTest()
		{
			ExecuteTest("Properties", "TabIndex", "NumericUpDownTabIndexTabStop");
		}
		[TestMethod]
		public void NumericUpDownTagTest()
		{
			ExecuteTest("Properties", "Tag", "NumericUpDownTag");
		}
		[TestMethod]
		public void NumericUpDownToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "NumericUpDownToolTipText");
		}
		[TestMethod]
		public void NumericUpDownTopTest()
		{
			ExecuteTest("Properties", "Top", "NumericUpDownTop");
		}
		[TestMethod]
		public void NumericUpDownValueTest()
		{
			ExecuteTest("Properties", "Value", "NumericUpDownValue");
		}
		[TestMethod]
		public void NumericUpDownVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "NumericUpDownVisible");
		}
		[TestMethod]
		public void NumericUpDownWidthTest()
		{
			ExecuteTest("Properties", "Width", "NumericUpDownWidth");
		}
		[TestMethod]
		public void NumericUpDownCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "NumericUpDownCreateControl");
		}
		[TestMethod]
		public void NumericUpDownEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "NumericUpDownEquals");
		}
		[TestMethod]
		public void NumericUpDownGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "NumericUpDownGetHashCode");
		}
		[TestMethod]
		public void NumericUpDownGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "NumericUpDownGetKeyPressMasks");
		}
		[TestMethod]
		public void NumericUpDownGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "NumericUpDownGetType");
		}
		[TestMethod]
		public void NumericUpDownHideTest()
		{
			ExecuteTest("Methods", "Hide", "NumericUpDownHide");
		}
		[TestMethod]
		public void NumericUpDownPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "NumericUpDownPerformClick");
		}
		[TestMethod]
		public void NumericUpDownPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "NumericUpDownPerformControlAdded");
		}
		[TestMethod]
		public void NumericUpDownPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "NumericUpDownPerformControlRemoved");
		}
		[TestMethod]
		public void NumericUpDownPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "NumericUpDownPerformDoubleClick");
		}
		[TestMethod]
		public void NumericUpDownPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "NumericUpDownPerformDragEnter");
		}
		[TestMethod]
		public void NumericUpDownPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "NumericUpDownPerformDragOver");
		}
		[TestMethod]
		public void NumericUpDownPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "NumericUpDownPerformEnter");
		}
		[TestMethod]
		public void NumericUpDownPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "NumericUpDownPerformGotFocus");
		}
		[TestMethod]
		public void NumericUpDownPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "NumericUpDownPerformKeyDown");
		}
		[TestMethod]
		public void NumericUpDownPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "NumericUpDownPerformKeyPress");
		}
		[TestMethod]
		public void NumericUpDownPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "NumericUpDownPerformKeyUp");
		}
		[TestMethod]
		public void NumericUpDownPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "NumericUpDownPerformLayout");
		}
		[TestMethod]
		public void NumericUpDownPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "NumericUpDownPerformLeave");
		}
		[TestMethod]
		public void NumericUpDownPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "NumericUpDownPerformLocationChanged");
		}
		[TestMethod]
		public void NumericUpDownPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "NumericUpDownPerformMouseClick");
		}
		[TestMethod]
		public void NumericUpDownPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "NumericUpDownPerformMouseDown");
		}
		[TestMethod]
		public void NumericUpDownPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "NumericUpDownPerformMouseMove");
		}
		[TestMethod]
		public void NumericUpDownPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "NumericUpDownPerformMouseUp");
		}
		[TestMethod]
		public void NumericUpDownSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "NumericUpDownSetBounds");
		}
		[TestMethod]
		public void NumericUpDownShowTest()
		{
			ExecuteTest("Methods", "Show", "NumericUpDownShow");
		}
		[TestMethod]
		public void NumericUpDownToStringTest()
		{
			ExecuteTest("Methods", "ToString", "NumericUpDownToString");
		}
		[TestMethod]
		public void NumericUpDownClickTest()
		{
			ExecuteTest("Events", "Click", "NumericUpDownClick");
		}
		[TestMethod]
		public void NumericUpDownEnterTest()
		{
			ExecuteTest("Events", "Enter", "NumericUpDownEnter");
		}
		[TestMethod]
		public void NumericUpDownGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "NumericUpDownGotFocus");
		}
		[TestMethod]
		public void NumericUpDownKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "NumericUpDownKeyDown");
		}
		[TestMethod]
		public void NumericUpDownKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "NumericUpDownKeyPress");
		}
		[TestMethod]
		public void NumericUpDownKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "NumericUpDownKeyUp");
		}
		[TestMethod]
		public void NumericUpDownLeaveTest()
		{
			ExecuteTest("Events", "Leave", "NumericUpDownLeave");
		}
		[TestMethod]
		public void NumericUpDownLoadTest()
		{
			ExecuteTest("Events", "Load", "NumericUpDownLoad");
		}
		[TestMethod]
		public void NumericUpDownLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "NumericUpDownLocationChanged");
		}
		[TestMethod]
		public void NumericUpDownLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "NumericUpDownLostFocus");
		}
		[TestMethod]
		public void NumericUpDownMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "NumericUpDownMouseClick");
		}
		[TestMethod]
		public void NumericUpDownMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "NumericUpDownMouseDown");
		}
		[TestMethod]
		public void NumericUpDownMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "NumericUpDownMouseEnter");
		}
		[TestMethod]
		public void NumericUpDownMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "NumericUpDownMouseHover");
		}
		[TestMethod]
		public void NumericUpDownMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "NumericUpDownMouseLeave");
		}
		[TestMethod]
		public void NumericUpDownMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "NumericUpDownMouseMove");
		}
		[TestMethod]
		public void NumericUpDownMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "NumericUpDownMouseUp");
		}
		[TestMethod]
		public void NumericUpDownResizeTest()
		{
			ExecuteTest("Events", "Resize", "NumericUpDownResize");
		}
		[TestMethod]
		public void NumericUpDownSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "NumericUpDownSizeChanged");
		}
		[TestMethod]
		public void NumericUpDownValidatingTest()
		{
			ExecuteTest("Events", "Validating", "NumericUpDownValidating");
		}
		[TestMethod]
		public void NumericUpDownValueChangedTest()
		{
			ExecuteTest("Events", "ValueChanged", "NumericUpDownValueChanged");
		}
		[TestMethod]
		public void NumericUpDownVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "NumericUpDownVisibleChanged");
		}
	}

	[TestClass]
	public partial class RichTextBoxTests : WebTestBase
	{
	        

		public RichTextBoxTests():base("RichTextBox", "RichTextBox")
		{

		}

		[TestMethod]
		public void BasicRichTextBoxTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicRichTextBox");
		}
		[TestMethod]
		public void RichTextBoxAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "RichTextBoxAccessibleRole");
		}
		[TestMethod]
		public void RichTextBoxAnchorTest()
		{
			ExecuteTest("Properties", "Anchor", "RichTextBoxAnchor");
		}
		[TestMethod]
		public void RichTextBoxApplicationTest()
		{
			ExecuteTest("Properties", "Application", "RichTextBoxApplication");
		}
		[TestMethod]
		public void RichTextBoxApplicationIdTest()
		{
			ExecuteTest("Properties", "ApplicationId", "RichTextBoxApplicationId");
		}
		[TestMethod]
		public void RichTextBoxBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "RichTextBoxBackColor");
		}
		[TestMethod]
		public void RichTextBoxBorderStyleTest()
		{
			ExecuteTest("Properties", "BorderStyle", "RichTextBoxBorderStyle");
		}
		[TestMethod]
		public void RichTextBoxBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "RichTextBoxBounds");
		}
		[TestMethod]
		public void RichTextBoxClientIDTest()
		{
			ExecuteTest("Properties", "ClientID", "RichTextBoxClientID");
		}
		[TestMethod]
		public void RichTextBoxClientRectangleTest()
		{
			ExecuteTest("Properties", "ClientRectangle", "RichTextBoxClientRectangle");
		}
		[TestMethod]
		public void RichTextBoxClientSizeTest()
		{
			ExecuteTest("Properties", "ClientSize", "RichTextBoxClientSize");
		}
		[TestMethod]
		public void RichTextBoxContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "RichTextBoxContextMenuStrip");
		}
		[TestMethod]
		public void RichTextBoxCursorTest()
		{
			ExecuteTest("Properties", "Cursor", "RichTextBoxCursor");
		}
		[TestMethod]
		public void RichTextBoxDockTest()
		{
			ExecuteTest("Properties", "Dock", "RichTextBoxDock");
		}
		[TestMethod]
		public void RichTextBoxDraggableTest()
		{
			ExecuteTest("Properties", "Draggable", "RichTextBoxDraggable");
		}
		[TestMethod]
		public void RichTextBoxEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "RichTextBoxEnabled");
		}
		[TestMethod]
		public void RichTextBoxFontTest()
		{
			ExecuteTest("Properties", "Font", "RichTextBoxFont");
		}
		[TestMethod]
		public void RichTextBoxForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "RichTextBoxForeColor");
		}
		[TestMethod]
		public void RichTextBoxHandleTest()
		{
			ExecuteTest("Properties", "Handle", "RichTextBoxHandle");
		}
		[TestMethod]
		public void RichTextBoxHeightTest()
		{
			ExecuteTest("Properties", "Height", "RichTextBoxHeight");
		}
		[TestMethod]
		public void RichTextBoxHideSelectionTest()
		{
			ExecuteTest("Properties", "HideSelection", "RichTextBoxHideSelection");
		}
		[TestMethod]
		public void RichTextBoxIDTest()
		{
			ExecuteTest("Properties", "ID", "RichTextBoxID");
		}
		[TestMethod]
		public void RichTextBoxIsContainerTest()
		{
			ExecuteTest("Properties", "IsContainer", "RichTextBoxIsContainer");
		}
		[TestMethod]
		public void RichTextBoxIsDisposedTest()
		{
			ExecuteTest("Properties", "IsDisposed", "RichTextBoxIsDisposed");
		}
		[TestMethod]
		public void RichTextBoxLeftTest()
		{
			ExecuteTest("Properties", "Left", "RichTextBoxLeft");
		}
		[TestMethod]
		public void RichTextBoxLocationTest()
		{
			ExecuteTest("Properties", "Location", "RichTextBoxLocation");
		}
		[TestMethod]
		public void RichTextBoxMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "RichTextBoxMaximumSize");
		}
		[TestMethod]
		public void RichTextBoxMaxLengthTest()
		{
			ExecuteTest("Properties", "MaxLength", "RichTextBoxMaxLength");
		}
		[TestMethod]
		public void RichTextBoxMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "RichTextBoxMinimumSize");
		}
		[TestMethod]
		public void RichTextBoxMultilineTest()
		{
			ExecuteTest("Properties", "Multiline", "RichTextBoxMultiline");
		}
		[TestMethod]
		public void RichTextBoxParentTest()
		{
			ExecuteTest("Properties", "Parent", "RichTextBoxParent");
		}
		[TestMethod]
		public void RichTextBoxParentElementTest()
		{
			ExecuteTest("Properties", "ParentElement", "RichTextBoxParentElement");
		}
		[TestMethod]
		public void RichTextBoxPixelHeightTest()
		{
			ExecuteTest("Properties", "PixelHeight", "RichTextBoxPixelHeight");
		}
		[TestMethod]
		public void RichTextBoxPixelLeftTest()
		{
			ExecuteTest("Properties", "PixelLeft", "RichTextBoxPixelLeft");
		}
		[TestMethod]
		public void RichTextBoxPixelTopTest()
		{
			ExecuteTest("Properties", "PixelTop", "RichTextBoxPixelTop");
		}
		[TestMethod]
		public void RichTextBoxPixelWidthTest()
		{
			ExecuteTest("Properties", "PixelWidth", "RichTextBoxPixelWidth");
		}
		[TestMethod]
		public void RichTextBoxReadOnlyTest()
		{
			ExecuteTest("Properties", "ReadOnly", "RichTextBoxReadOnly");
		}
		[TestMethod]
		public void RichTextBoxRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "RichTextBoxRightToLeft");
		}
		[TestMethod]
		public void RichTextBoxScrollBarsTest()
		{
			ExecuteTest("Properties", "ScrollBars", "RichTextBoxScrollBars");
		}
		[TestMethod]
		public void RichTextBoxSelectedTextTest()
		{
			ExecuteTest("Properties", "SelectedText", "RichTextBoxSelectedText");
		}
		[TestMethod]
		public void RichTextBoxSelectionColorTest()
		{
			ExecuteTest("Properties", "SelectionColor", "RichTextBoxSelectionColor");
		}
		[TestMethod]
		public void RichTextBoxSelectionFontTest()
		{
			ExecuteTest("Properties", "SelectionFont", "RichTextBoxSelectionFont");
		}
		[TestMethod]
		public void RichTextBoxSelectionLengthTest()
		{
			ExecuteTest("Properties", "SelectionLength", "RichTextBoxSelectionLength");
		}
		[TestMethod]
		public void RichTextBoxSelectionStartTest()
		{
			ExecuteTest("Properties", "SelectionStart", "RichTextBoxSelectionStart");
		}
		[TestMethod]
		public void RichTextBoxSizeTest()
		{
			ExecuteTest("Properties", "Size", "RichTextBoxSize");
		}
		[TestMethod]
		public void RichTextBoxTabIndexTabStopTest()
		{
			ExecuteTest("Properties", "TabIndex", "RichTextBoxTabIndexTabStop");
		}
		[TestMethod]
		public void RichTextBoxTagTest()
		{
			ExecuteTest("Properties", "Tag", "RichTextBoxTag");
		}
		[TestMethod]
		public void RichTextBoxTextTest()
		{
			ExecuteTest("Properties", "Text", "RichTextBoxText");
		}
		[TestMethod]
		public void RichTextBoxTextLengthTest()
		{
			ExecuteTest("Properties", "TextLength", "RichTextBoxTextLength");
		}
		[TestMethod]
		public void RichTextBoxToolTipTextTest()
		{
			ExecuteTest("Properties", "ToolTipText", "RichTextBoxToolTipText");
		}
		[TestMethod]
		public void RichTextBoxTopTest()
		{
			ExecuteTest("Properties", "Top", "RichTextBoxTop");
		}
		[TestMethod]
		public void RichTextBoxValueTest()
		{
			ExecuteTest("Properties", "Value", "RichTextBoxValue");
		}
		[TestMethod]
		public void RichTextBoxVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "RichTextBoxVisible");
		}
		[TestMethod]
		public void RichTextBoxWidthTest()
		{
			ExecuteTest("Properties", "Width", "RichTextBoxWidth");
		}
		[TestMethod]
		public void RichTextBoxClearTest()
		{
			ExecuteTest("Methods", "Clear", "RichTextBoxClear");
		}
		[TestMethod]
		public void RichTextBoxCreateControlTest()
		{
			ExecuteTest("Methods", "CreateControl", "RichTextBoxCreateControl");
		}
		[TestMethod]
		public void RichTextBoxEqualsTest()
		{
			ExecuteTest("Methods", "Equals", "RichTextBoxEquals");
		}
		[TestMethod]
		public void RichTextBoxFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "RichTextBoxFindForm");
		}
		[TestMethod]
		public void RichTextBoxGetHashCodeTest()
		{
			ExecuteTest("Methods", "GetHashCode", "RichTextBoxGetHashCode");
		}
		[TestMethod]
		public void RichTextBoxGetKeyPressMasksTest()
		{
			ExecuteTest("Methods", "GetKeyPressMasks", "RichTextBoxGetKeyPressMasks");
		}
		[TestMethod]
		public void RichTextBoxGetTypeTest()
		{
			ExecuteTest("Methods", "GetType", "RichTextBoxGetType");
		}
		[TestMethod]
		public void RichTextBoxHideTest()
		{
			ExecuteTest("Methods", "Hide", "RichTextBoxHide");
		}
		[TestMethod]
		public void RichTextBoxPerformClickTest()
		{
			ExecuteTest("Methods", "PerformClick", "RichTextBoxPerformClick");
		}
		[TestMethod]
		public void RichTextBoxPerformControlAddedTest()
		{
			ExecuteTest("Methods", "PerformControlAdded", "RichTextBoxPerformControlAdded");
		}
		[TestMethod]
		public void RichTextBoxPerformControlRemovedTest()
		{
			ExecuteTest("Methods", "PerformControlRemoved", "RichTextBoxPerformControlRemoved");
		}
		[TestMethod]
		public void RichTextBoxPerformDoubleClickTest()
		{
			ExecuteTest("Methods", "PerformDoubleClick", "RichTextBoxPerformDoubleClick");
		}
		[TestMethod]
		public void RichTextBoxPerformDragEnterTest()
		{
			ExecuteTest("Methods", "PerformDragEnter", "RichTextBoxPerformDragEnter");
		}
		[TestMethod]
		public void RichTextBoxPerformDragOverTest()
		{
			ExecuteTest("Methods", "PerformDragOver", "RichTextBoxPerformDragOver");
		}
		[TestMethod]
		public void RichTextBoxPerformEnterTest()
		{
			ExecuteTest("Methods", "PerformEnter", "RichTextBoxPerformEnter");
		}
		[TestMethod]
		public void RichTextBoxPerformGotFocusTest()
		{
			ExecuteTest("Methods", "PerformGotFocus", "RichTextBoxPerformGotFocus");
		}
		[TestMethod]
		public void RichTextBoxPerformKeyDownTest()
		{
			ExecuteTest("Methods", "PerformKeyDown", "RichTextBoxPerformKeyDown");
		}
		[TestMethod]
		public void RichTextBoxPerformKeyPressTest()
		{
			ExecuteTest("Methods", "PerformKeyPress", "RichTextBoxPerformKeyPress");
		}
		[TestMethod]
		public void RichTextBoxPerformKeyUpTest()
		{
			ExecuteTest("Methods", "PerformKeyUp", "RichTextBoxPerformKeyUp");
		}
		[TestMethod]
		public void RichTextBoxPerformLayoutTest()
		{
			ExecuteTest("Methods", "PerformLayout", "RichTextBoxPerformLayout");
		}
		[TestMethod]
		public void RichTextBoxPerformLeaveTest()
		{
			ExecuteTest("Methods", "PerformLeave", "RichTextBoxPerformLeave");
		}
		[TestMethod]
		public void RichTextBoxPerformLocationChangedTest()
		{
			ExecuteTest("Methods", "PerformLocationChanged", "RichTextBoxPerformLocationChanged");
		}
		[TestMethod]
		public void RichTextBoxPerformMouseClickTest()
		{
			ExecuteTest("Methods", "PerformMouseClick", "RichTextBoxPerformMouseClick");
		}
		[TestMethod]
		public void RichTextBoxPerformMouseDownTest()
		{
			ExecuteTest("Methods", "PerformMouseDown", "RichTextBoxPerformMouseDown");
		}
		[TestMethod]
		public void RichTextBoxPerformMouseMoveTest()
		{
			ExecuteTest("Methods", "PerformMouseMove", "RichTextBoxPerformMouseMove");
		}
		[TestMethod]
		public void RichTextBoxPerformMouseUpTest()
		{
			ExecuteTest("Methods", "PerformMouseUp", "RichTextBoxPerformMouseUp");
		}
		[TestMethod]
		public void RichTextBoxPerformSizeChangedTest()
		{
			ExecuteTest("Methods", "PerformSizeChanged", "RichTextBoxPerformSizeChanged");
		}
		[TestMethod]
		public void RichTextBoxResetTextTest()
		{
			ExecuteTest("Methods", "ResetText", "RichTextBoxResetText");
		}
		[TestMethod]
		public void RichTextBoxSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "RichTextBoxSetBounds");
		}
		[TestMethod]
		public void RichTextBoxShowTest()
		{
			ExecuteTest("Methods", "Show", "RichTextBoxShow");
		}
		[TestMethod]
		public void RichTextBoxToStringTest()
		{
			ExecuteTest("Methods", "ToString", "RichTextBoxToString");
		}
		[TestMethod]
		public void RichTextBoxClickTest()
		{
			ExecuteTest("Events", "Click", "RichTextBoxClick");
		}
		[TestMethod]
		public void RichTextBoxEnterTest()
		{
			ExecuteTest("Events", "Enter", "RichTextBoxEnter");
		}
		[TestMethod]
		public void RichTextBoxGotFocusTest()
		{
			ExecuteTest("Events", "GotFocus", "RichTextBoxGotFocus");
		}
		[TestMethod]
		public void RichTextBoxKeyDownTest()
		{
			ExecuteTest("Events", "KeyDown", "RichTextBoxKeyDown");
		}
		[TestMethod]
		public void RichTextBoxKeyPressTest()
		{
			ExecuteTest("Events", "KeyPress", "RichTextBoxKeyPress");
		}
		[TestMethod]
		public void RichTextBoxKeyUpTest()
		{
			ExecuteTest("Events", "KeyUp", "RichTextBoxKeyUp");
		}
		[TestMethod]
		public void RichTextBoxLeaveTest()
		{
			ExecuteTest("Events", "Leave", "RichTextBoxLeave");
		}
		[TestMethod]
		public void RichTextBoxLoadTest()
		{
			ExecuteTest("Events", "Load", "RichTextBoxLoad");
		}
		[TestMethod]
		public void RichTextBoxLocationChangedTest()
		{
			ExecuteTest("Events", "LocationChanged", "RichTextBoxLocationChanged");
		}
		[TestMethod]
		public void RichTextBoxLostFocusTest()
		{
			ExecuteTest("Events", "LostFocus", "RichTextBoxLostFocus");
		}
		[TestMethod]
		public void RichTextBoxMouseClickTest()
		{
			ExecuteTest("Events", "MouseClick", "RichTextBoxMouseClick");
		}
		[TestMethod]
		public void RichTextBoxMouseDownTest()
		{
			ExecuteTest("Events", "MouseDown", "RichTextBoxMouseDown");
		}
		[TestMethod]
		public void RichTextBoxMouseEnterTest()
		{
			ExecuteTest("Events", "MouseEnter", "RichTextBoxMouseEnter");
		}
		[TestMethod]
		public void RichTextBoxMouseHoverTest()
		{
			ExecuteTest("Events", "MouseHover", "RichTextBoxMouseHover");
		}
		[TestMethod]
		public void RichTextBoxMouseLeaveTest()
		{
			ExecuteTest("Events", "MouseLeave", "RichTextBoxMouseLeave");
		}
		[TestMethod]
		public void RichTextBoxMouseMoveTest()
		{
			ExecuteTest("Events", "MouseMove", "RichTextBoxMouseMove");
		}
		[TestMethod]
		public void RichTextBoxMouseUpTest()
		{
			ExecuteTest("Events", "MouseUp", "RichTextBoxMouseUp");
		}
		[TestMethod]
		public void RichTextBoxResizeTest()
		{
			ExecuteTest("Events", "Resize", "RichTextBoxResize");
		}
		[TestMethod]
		public void RichTextBoxSizeChangedTest()
		{
			ExecuteTest("Events", "SizeChanged", "RichTextBoxSizeChanged");
		}
		[TestMethod]
		public void RichTextBoxTextChangedTest()
		{
			ExecuteTest("Events", "TextChanged", "RichTextBoxTextChanged");
		}
		[TestMethod]
		public void RichTextBoxValidatingTest()
		{
			ExecuteTest("Events", "Validating", "RichTextBoxValidating");
		}
		[TestMethod]
		public void RichTextBoxVisibleChangedTest()
		{
			ExecuteTest("Events", "VisibleChanged", "RichTextBoxVisibleChanged");
		}
	}

	[TestClass]
	public partial class StatusElementTests : WebTestBase
	{
	        

		public StatusElementTests():base("StatusBar", "StatusElement")
		{

		}

		[TestMethod]
		public void BasicStatusBarTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicStatusBar");
		}
	}

	[TestClass]
	public partial class TablePanelTests : WebTestBase
	{
	        

		public TablePanelTests():base("TablePanel", "TablePanel")
		{

		}

		[TestMethod]
		public void BasicTablePanelTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicTablePanel");
		}
		[TestMethod]
		public void TablePanelBackgroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "TablePanelBackgroundImage");
		}
		[TestMethod]
		public void TablePanelColumnCountTest()
		{
			ExecuteTest("Properties", "ColumnCount", "TablePanelColumnCount");
		}
	}

	[TestClass]
	public partial class ToolBarLabelTests : WebTestBase
	{
	        

		public ToolBarLabelTests():base("ToolBarLabel", "ToolBarLabel")
		{

		}

		[TestMethod]
		public void ToolBarToolBarLabelEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarLabelEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarLabelVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarLabelVisible");
		}
	}

	[TestClass]
	public partial class ToolBarProgressBarTests : WebTestBase
	{
	        

		public ToolBarProgressBarTests():base("ToolBarProgressBar", "ToolBarProgressBar")
		{

		}

		[TestMethod]
		public void ToolBarToolBarProgressBarEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarProgressBarEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarProgressBarVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarProgressBarVisible");
		}
	}

	[TestClass]
	public partial class ToolBarSplitButtonTests : WebTestBase
	{
	        

		public ToolBarSplitButtonTests():base("ToolBarSplitButton", "ToolBarSplitButton")
		{

		}

		[TestMethod]
		public void ToolBarToolBarSplitButtonEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarToolBarSplitButtonEnabled");
		}
		[TestMethod]
		public void ToolBarToolBarSplitButtonVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarToolBarSplitButtonVisible");
		}
	}

	[TestClass]
	public partial class StatusLabelTests : WebTestBase
	{
	        

		public StatusLabelTests():base("StatusLabel", "StatusLabel")
		{

		}

		[TestMethod]
		public void ToolBarStatusLabelEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "ToolBarStatusLabelEnabled");
		}
		[TestMethod]
		public void ToolBarStatusLabelVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "ToolBarStatusLabelVisible");
		}
	}

	[TestClass]
	public partial class RangeElementTests : WebTestBase
	{
	        

		public RangeElementTests():base("Range", "RangeElement")
		{

		}

		[TestMethod]
		public void BasicRangeTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicRange");
		}
	}

	[TestClass]
	public partial class MonthCalendarElementTests : WebTestBase
	{
	        

		public MonthCalendarElementTests():base("MonthCalendar", "MonthCalendarElement")
		{

		}

		[TestMethod]
		public void BasicMonthCalendarTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicMonthCalendar");
		}
		[TestMethod]
		public void MonthCalendarDateChangedTest()
		{
			ExecuteTest("Events", "DateChanged", "MonthCalendarDateChanged");
		}
	}

	[TestClass]
	public partial class ListBoxElementTests : WebTestBase
	{
	        

		public ListBoxElementTests():base("ListBox", "ListBoxElement")
		{

		}

		[TestMethod]
		public void BasicListBoxTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicListBox");
		}
		[TestMethod]
		public void ListBoxItemsTest()
		{
			ExecuteTest("Properties", "Items", "ListBoxItems");
		}
		[TestMethod]
		public void ListBoxMaximumSizeTest()
		{
			ExecuteTest("Properties", "MaximumSize", "ListBoxMaximumSize");
		}
		[TestMethod]
		public void ListBoxMinimumSizeTest()
		{
			ExecuteTest("Properties", "MinimumSize", "ListBoxMinimumSize");
		}
		[TestMethod]
		public void ListBoxSelectedValueChangedTest()
		{
			ExecuteTest("Events", "SelectedValueChanged", "ListBoxSelectedValueChanged");
		}
	}

	[TestClass]
	public partial class MessageBoxTests : WebTestBase
	{
	        

		public MessageBoxTests():base("MessageBox", "MessageBox")
		{

		}

		[TestMethod]
		public void BasicMessageBoxTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicMessageBox");
		}
	}

	[TestClass]
	public partial class WindowElementTests : WebTestBase
	{
	        

		public WindowElementTests():base("Window", "WindowElement")
		{

		}

		[TestMethod]
		public void MDIWindowPartialControlTest()
		{
			ExecuteTest(String.Empty, String.Empty, "MDIWindowPartialControl");
		}
		[TestMethod]
		public void EmployeeInheritanceTest()
		{
			ExecuteTest(String.Empty, String.Empty, "EmployeeInheritance");
		}
		[TestMethod]
		public void BasicWindowTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicWindow");
		}
		[TestMethod]
		public void InheritanceTest()
		{
			ExecuteTest(String.Empty, String.Empty, "Inheritance");
		}
		[TestMethod]
		public void MDIWindowTest()
		{
			ExecuteTest(String.Empty, String.Empty, "MDIWindow");
		}
		[TestMethod]
		public void MDIWindowPanelTest()
		{
			ExecuteTest(String.Empty, String.Empty, "MDIWindowPanel");
		}
		[TestMethod]
		public void MDIWindowContextMenuStripTest()
		{
			ExecuteTest(String.Empty, String.Empty, "MDIWindowContextMenuStrip");
		}
		[TestMethod]
		public void TwoWindowsTest()
		{
			ExecuteTest(String.Empty, String.Empty, "TwoWindows");
		}
		[TestMethod]
		public void WindowAcceptButtonTest()
		{
			ExecuteTest("Properties", "AcceptButton", "WindowAcceptButton");
		}
		[TestMethod]
		public void WindowAccessibleRoleTest()
		{
			ExecuteTest("Properties", "AccessibleRole", "WindowAccessibleRole");
		}
		[TestMethod]
		public void WindowBackColorTest()
		{
			ExecuteTest("Properties", "BackColor", "WindowBackColor");
		}
		[TestMethod]
		public void WindowBackgroundImageTest()
		{
			ExecuteTest("Properties", "BackgroundImage", "WindowBackgroundImage");
		}
		[TestMethod]
		public void WindowBackgroundImageLayoutTest()
		{
			ExecuteTest("Properties", "BackgroundImageLayout", "WindowBackgroundImageLayout");
		}
		[TestMethod]
		public void WindowBoundsTest()
		{
			ExecuteTest("Properties", "Bounds", "WindowBounds");
		}
		[TestMethod]
		public void WindowCancelButtonTest()
		{
			ExecuteTest("Properties", "CancelButton", "WindowCancelButton");
		}
		[TestMethod]
		public void WindowContextMenuStripTest()
		{
			ExecuteTest("Properties", "ContextMenuStrip", "WindowContextMenuStrip");
		}
		[TestMethod]
		public void WindowControlBoxTest()
		{
			ExecuteTest("Properties", "ControlBox", "WindowControlBox");
		}
		[TestMethod]
		public void WindowControlsTest()
		{
			ExecuteTest("Properties", "Controls", "WindowControls");
		}
		[TestMethod]
		public void WindowDesktopBoundsTest()
		{
			ExecuteTest("Properties", "DesktopBounds", "WindowDesktopBounds");
		}
		[TestMethod]
		public void WindowDialogResultTest()
		{
			ExecuteTest("Properties", "DialogResult", "WindowDialogResult");
		}
		[TestMethod]
		public void WindowEnabledTest()
		{
			ExecuteTest("Properties", "Enabled", "WindowEnabled");
		}
		[TestMethod]
		public void WindowEnableEscTest()
		{
			ExecuteTest("Properties", "EnableEsc", "WindowEnableEsc");
		}
		[TestMethod]
		public void WindowFontTest()
		{
			ExecuteTest("Properties", "Font", "WindowFont");
		}
		[TestMethod]
		public void WindowForeColorTest()
		{
			ExecuteTest("Properties", "ForeColor", "WindowForeColor");
		}
		[TestMethod]
		public void WindowHeightTest()
		{
			ExecuteTest("Properties", "Height", "WindowHeight");
		}
		[TestMethod]
		public void WindowIconTest()
		{
			ExecuteTest("Properties", "Icon", "WindowIcon");
		}
		[TestMethod]
		public void WindowLeftTest()
		{
			ExecuteTest("Properties", "Left", "WindowLeft");
		}
		[TestMethod]
		public void WindowLocationTest()
		{
			ExecuteTest("Properties", "Location", "WindowLocation");
		}
		[TestMethod]
		public void WindowRightToLeftTest()
		{
			ExecuteTest("Properties", "RightToLeft", "WindowRightToLeft");
		}
		[TestMethod]
		public void WindowSizeTest()
		{
			ExecuteTest("Properties", "Size", "WindowSize");
		}
		[TestMethod]
		public void WindowSizeGripStyleTest()
		{
			ExecuteTest("Properties", "SizeGripStyle", "WindowSizeGripStyle");
		}
		[TestMethod]
		public void WindowStartPositionTest()
		{
			ExecuteTest("Properties", "StartPosition", "WindowStartPosition");
		}
		[TestMethod]
		public void TabIndexTest()
		{
			ExecuteTest("Properties", "TabIndex", "TabIndex");
		}
		[TestMethod]
		public void WindowTextTest()
		{
			ExecuteTest("Properties", "Text", "WindowText");
		}
		[TestMethod]
		public void WindowTopTest()
		{
			ExecuteTest("Properties", "Top", "WindowTop");
		}
		[TestMethod]
		public void TopMostWindowTest()
		{
			ExecuteTest("Properties", "TopMost", "TopMostWindow");
		}
		[TestMethod]
		public void WindowTransparencyKeyTest()
		{
			ExecuteTest("Properties", "TransparencyKey", "WindowTransparencyKey");
		}
		[TestMethod]
		public void WindowVisibleTest()
		{
			ExecuteTest("Properties", "Visible", "WindowVisible");
		}
		[TestMethod]
		public void WindowWidthTest()
		{
			ExecuteTest("Properties", "Width", "WindowWidth");
		}
		[TestMethod]
		public void WindowWindowBorderStyleTest()
		{
			ExecuteTest("Properties", "WindowBorderStyle", "WindowWindowBorderStyle");
		}
		[TestMethod]
		public void WindowWindowStateTest()
		{
			ExecuteTest("Properties", "WindowState", "WindowWindowState");
		}
		[TestMethod]
		public void WindowActivateTest()
		{
			ExecuteTest("Methods", "Activate", "WindowActivate");
		}
		[TestMethod]
		public void WindowCloseTest()
		{
			ExecuteTest("Methods", "Close", "WindowClose");
		}
		[TestMethod]
		public void WindowDisposeTest()
		{
			ExecuteTest("Methods", "Dispose", "WindowDispose");
		}
		[TestMethod]
		public void WindowFindFormTest()
		{
			ExecuteTest("Methods", "FindForm", "WindowFindForm");
		}
		[TestMethod]
		public void WindowFocusTest()
		{
			ExecuteTest("Methods", "Focus", "WindowFocus");
		}
		[TestMethod]
		public void WindowHideShowTest()
		{
			ExecuteTest("Methods", "Hide", "WindowHideShow");
		}
		[TestMethod]
		public void WindowSetBoundsTest()
		{
			ExecuteTest("Methods", "SetBounds", "WindowSetBounds");
		}
		[TestMethod]
		public void WindowShowDialogTest()
		{
			ExecuteTest("Methods", "ShowDialog", "WindowShowDialog");
		}
		[TestMethod]
		public void WindowDeactivateTest()
		{
			ExecuteTest("Events", "Deactivate", "WindowDeactivate");
		}
		[TestMethod]
		public void WindowLayoutTest()
		{
			ExecuteTest("Events", "Layout", "WindowLayout");
		}
		[TestMethod]
		public void WindowResizeTest()
		{
			ExecuteTest("Events", "Resize", "WindowResize");
		}
		[TestMethod]
		public void WindowClosingTest()
		{
			ExecuteTest("Events", "WindowClosing", "WindowClosing");
		}
	}

	[TestClass]
	public partial class RibbonControlElementTests : WebTestBase
	{
	        

		public RibbonControlElementTests():base("RibbonControl", "RibbonControlElement")
		{

		}

		[TestMethod]
		public void BasicRibbonControlTest()
		{
			ExecuteTest(String.Empty, String.Empty, "BasicRibbonControl");
		}
		[TestMethod]
		public void RibbonImageListTest()
		{
			ExecuteTest("Properties", "ImageList", "RibbonImageList");
		}
	}
}
