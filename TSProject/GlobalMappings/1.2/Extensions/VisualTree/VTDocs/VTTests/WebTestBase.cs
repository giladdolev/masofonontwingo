﻿using ImageMagick;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Safari;
using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Linq;

namespace VTTests
{
    [TestClass]
    [DeploymentItem("chromedriver.exe")]
    [DeploymentItem("geckodriver.exe")]
    [DeploymentItem("operadriver.exe")]
    [DeploymentItem("IEDriverServer.exe")]
    [DeploymentItem("vttests.config")]
    public class WebTestBase
    {
        // The test context
        public TestContext TestContext { get; set; }

        private static IWebDriver _driver;
        
        private static readonly string _baseURL;
        private static readonly string _baselineDir;
        private static readonly string _resultsNewDir;
        private static readonly string _resultsDiffDir;
        private static readonly string _browser;


        protected readonly string _elementName;
        private readonly string _elementClassName;

        private string _currentExampleId = null;

        private TestResult _testResult;
        private string _message;

        #region Internal Processing
        /// <summary>
        /// Initializes the <see cref="WebTestBase"/> class.
        /// </summary>
        static WebTestBase()
        {
            // Load configuration
            ExeConfigurationFileMap map = new ExeConfigurationFileMap();
            map.ExeConfigFilename = "vttests.config";
            Configuration config =
               ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            _baseURL = config.AppSettings.Settings["BaseURL"].Value;
            _baselineDir = config.AppSettings.Settings["BaselineDir"].Value;
            _resultsNewDir = config.AppSettings.Settings["RsesultsNewDir"].Value;
            _resultsDiffDir = config.AppSettings.Settings["ResultsDiffDir"].Value;
            _browser = config.AppSettings.Settings["Browser"].Value;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WebTestBase"/> class.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="elementClassName">Name of the element class.</param>
        public WebTestBase(string elementName, string elementClassName)
        {
            this._elementName = elementName;
            this._elementClassName = elementClassName;
        }

        /// <summary>
        /// Gets the name of the element.
        /// </summary>
        /// <value>
        /// The name of the element.
        /// </value>
        protected string ElementName
        {
            get
            {
                return _elementName;
            }
        }

        /// <summary>
        /// Gets the name of the element class.
        /// </summary>
        /// <value>
        /// The name of the element class.
        /// </value>
        protected string ElementClassName
        {
            get
            {
                return _elementClassName;
            }
        }

        /// <summary>
        /// Gets the driver.
        /// </summary>
        /// <value>
        /// The driver.
        /// </value>
        public IWebDriver Driver
        {
            get { return _driver; }
        }

        public static IWebDriver CurrentWebDriver
        {
            get { return _driver; }
        }

        /// <summary>
        /// Gets the base URL.
        /// </summary>
        /// <value>
        /// The base URL.
        /// </value>
        public string BaseURL
        {
            get { return _baseURL; }
        }

        /// <summary>
        /// Initializes the specified test name.
        /// </summary>
        /// <param name="context">The context.</param>
        internal static void Initialize(TestContext context)
        {

            _driver = GetDriver();


            _driver.Navigate().GoToUrl(_baseURL);

            context.WriteLine("==== Running tests on {0}", _browser);
            context.WriteLine("==== Base URL: {0}", _baseURL);
            context.WriteLine("==== Baseline Dir: {0}", _baselineDir);
            context.WriteLine("==== Results New Dir: {0}", _resultsNewDir);
            context.WriteLine("==== Results Diff Dir: {0}", _resultsDiffDir);
            context.WriteLine("==== Browser: {0}", _browser);

            _driver.Manage().Window.Maximize();

            _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
        }

        /// <summary>
        /// Cleanups this instance.
        /// </summary>
        internal static void Cleanup()
        {
            _driver.Quit();
            _driver.Close();
            _driver.Dispose();
        }

        /// <summary>
        /// Setups the test.
        /// </summary>
        [TestInitialize()]
        public void SetupTest()
        {
            this.Driver.Navigate().GoToUrl(string.Format("{0}?appId={1}", BaseURL, Environment.TickCount));
            //this.Driver.Navigate().Refresh();
            
        }

        [TestCleanup()]
        public void CleanTest()
        {

            var xpath = GetXpathForWindowCloseBtn(_currentExampleId);

            if (this.Driver.FindElements(xpath).Any())
            {
                var closeBtn = this.Driver.WaitForElement(xpath);
                closeBtn.Click();
                this.Driver.WaitWhileElementExistOnPage(closeBtn);
            }       
            
        }

        /// <summary>
        /// Gets the driver.
        /// </summary>
        /// <returns></returns>
        private static IWebDriver GetDriver()
        {
            switch (_browser)
            {
                default:
                case "Chrome":
                    // Set Chrome options
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.AddArgument("test-type");
                    chromeOptions.AddArgument("--start-maximized");
                    chromeOptions.AddArgument("--bwsi");
                    chromeOptions.AddArgument("--disable-infobars");
                    chromeOptions.AddArguments("disable-extensions");

                    //Set driver to chrome web driver 
                    return new OpenQA.Selenium.Chrome.ChromeDriver(chromeOptions);

                case "IE":
                    // Set Internet Explorer options
                    InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
                    internetExplorerOptions.ForceCreateProcessApi = true;
                    internetExplorerOptions.BrowserCommandLineArguments = "-private";
                    internetExplorerOptions.EnsureCleanSession = true;
                    //internetExplorerOptions.BrowserCommandLineArguments = "";

                    //Set driver to Internet Explorer web driver 
                    return new OpenQA.Selenium.IE.InternetExplorerDriver(".",internetExplorerOptions, TimeSpan.FromMinutes(3));

                case "Firefox":
                    // Set Firefox options
                    FirefoxOptions firefoxOptions = new FirefoxOptions();
                    //firefoxOptions.AddArgument("test-type");

                    //Set driver to Firefox Options web driver 
                    return new OpenQA.Selenium.Firefox.FirefoxDriver(firefoxOptions);

                case "Safari":
                    // Set Safari options
                    SafariOptions safariOptions = new SafariOptions();
                    //safariOptions.AddArgument("test-type");;

                    //Set driver to Safari Options web driver 
                    return new OpenQA.Selenium.Safari.SafariDriver(safariOptions);

                case "Opera":
                    // Set Opera options
                    OperaOptions operaOptions = new OperaOptions();
                    //operaOptions.AddArgument("test-type");;

                    //Set driver to Opera Options web driver 
                    return new OpenQA.Selenium.Opera.OperaDriver(operaOptions);
            }
        }
        #endregion

        #region Standard elements retrieval
        protected By GetXpathForSideMenuNode(string nodeText)
        {
            return By.XPath(string.Format("//div[contains(@class, 'side-menu')]//span[contains(@class, 'x-tree-node-text') and text()='{0}']", nodeText));
        }

        protected By GetXpathForExampleTabNode(string nodeText) 
        {
            return By.XPath(string.Format("//div[contains(@class, 'examples-holder')]//a[contains(@class, 'x-tab')]//span[text()='{0}']", nodeText));
        }

        protected By GetXpathForExampleTabItem(string nodeText) 
        {
            return By.XPath(string.Format("//span[text()='{0}']", nodeText));
        }

        protected By GetXpathForMemberItemTitle(string nodeText)
        {
            return By.XPath(string.Format("//div[contains(@class, 'examples-holder')]//div[contains(@class, 'x-tabpanel-child')]//div[contains(@class, 'x-title-text') and text()='{0}']", nodeText));

        }

        protected By GetXpathForPreviewBtn(string membersCategory, string memberName, string exampleId)
        {
            return By.XPath(string.Format("//div[contains(@class, 'examples-holder')]//div[contains(@class, '{0}-{1}')]//a[contains(@class,'x-btn')]//span[contains(@class, 'Example-{2}')]", membersCategory, memberName, exampleId));
        }
        protected By GetXpathForBasicPreviewBtn(string testName)
        {
            return By.XPath(string.Format("//div[contains(@class, 'playground-sandbox')]//div[contains(@class, 'ComponentHeader')]//a[contains(@class, 'x-btn')]//span[contains(@class, 'Example-{0}')]", testName));
        }
        protected By GetXpathForWindowTitle()
        {
            return By.XPath(string.Format("//div[contains(@class, 'x-window')]//div[contains(@class, 'x-window-header-title')]//div[contains(@class, 'x-title-text')]"));
        }

        protected By GetXpathForWindowCloseBtn(string exampleId)
        {
            return By.XPath(string.Format("//div[contains(@class, 'x-window') and contains(@class, '{0}')]//div[contains(@class, 'x-window-header')]//div[contains(@class, 'x-tool-close')]", exampleId));
        }

        protected By GetXpathForTestWindowCloseBtnWithId(string windowId)
        {
            return By.XPath(string.Format("//div[contains(@class, 'x-window') and contains(@id, '{0}')]//div[contains(@class, 'x-window-header')]//div[contains(@class, 'x-tool-close')]", windowId));
        }

        protected By GetXpathForWindowElement(string exampleId)
        {
            return By.XPath(string.Format("//div[contains(@class, 'x-window') and contains(@class, '{0}')]", exampleId));
        }
        
        //XPaths for Button
        protected By GetXPathForButtonWithText(string btnText)
        {
            return By.XPath(string.Format("//a[contains(@class, 'x-btn')]//span[text()='{0}']//ancestor-or-self::a", btnText));
        }

        protected By GetXPathForButtonSpanWithText(string btnText)
        {
            return By.XPath(string.Format("//a[contains(@class, 'x-btn')]//span[text()='{0}']", btnText));
        }

        protected By GetXPathForButtonSpanWithTooltipWithText(string btnText, string toolTipText)
        {
            return By.XPath(string.Format("//label[contains(@class, 'x-btn') and @data-qtip='{1}']//span[text()='{0}']", btnText, toolTipText));
        }

        //..DisabledButton
        protected By GetXPathForDisabledButtonWithText(string btnText)
        {
            return By.XPath(string.Format("//a[contains(@class, 'x-btn') and @aria-disabled='true']//span[text()='{0}']//ancestor-or-self::a", btnText));
        }

        //..EnabledButton
        protected By GetXPathForEnabledButtonWithText(string btnText)
        {
            return By.XPath(string.Format("//a[contains(@class, 'x-btn') and @aria-disabled='false']//span[text()='{0}']//ancestor-or-self::a", btnText));
        }

        protected By GetXPathForButtonAnyWithText(string btnText)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-btn')]//*[text()='{0}']", btnText));
        }

        //XPaths for DropDownButton with css-class
        protected By GetXPathForDropDownButtonWithCssClass(String ddbtnCssClass) //*[contains(@class, "vt-ddbtn-TestedDropDownButton")]//*[contains(@class,"x-btn-arrow-el")]//parent::a
        {
            return By.XPath(string.Format("//*[contains(@class, '{0}')]//*[contains(@class,'x-btn-arrow-el')]//parent::a", ddbtnCssClass));
        }

        protected By GetXPathForDropDownButtonArrowWithCssClass(String ddbtnCssClass) 
        {
            return By.XPath(string.Format("//*[contains(@class, '{0}')]//*[contains(@class,'x-btn-arrow-el')]", ddbtnCssClass));
        }

        //XPath for ToolBarMenuItem 
        protected By GetXPathForToolBarMenuItemWithCssClassAndWithText(string txtCssClass, string tbbtnText)
        {
            return By.XPath(string.Format("//a[contains(@class, '{0}') and contains(@class, 'x-btn')]//span[text()='{1}']//ancestor-or-self::a", txtCssClass, tbbtnText));
        }

        //XPaths for TextBox:

        protected By GetXPathForTextBoxDivWithText(string txtText)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-item-body')]//*[text()='{0}']", txtText));
        }
        protected By GetXPathForEmptyTextBox()
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-empty-field-default')]"));
        }

        protected By GetXPathForTextBoxWithText(string txtText)
        {
            return By.XPath(string.Format("//input[contains(@class, 'x-form-text') and  contains(@value, '{0}')]", txtText));
        }

        protected By GetXPathForMultiLineTextBoxWithText(string txtText) 
        {
            return By.XPath(string.Format("//textarea[contains(@class, 'x-form-text') and  contains(text(), '{0}')]", txtText));
        }

        protected By GetXPathForTextBoxWithCssClass(string txtCssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//input[contains(@class, 'x-form-text')]", txtCssClass));
        }   

        //XPaths for CheckBox:

        protected By GetXPathForCheckBoxWithText(string chkText)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-cb')]//*[text()='{0}']", chkText));
        }

        protected By GetXPathForDisabledCheckBoxByClasses(string txtCheckBox)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-cb') and contains(@class, 'x-item-disabled')]]//*[text()='{0}']", txtCheckBox));
        }

        //..Enabled CheckBox
        protected By GetXPathForEnabledCheckBoxWithText(string chkText, string isEnabled) 
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-cb')]//*[text()='{0}']//parent::div//child::input[@aria-disabled='{1}']", chkText, isEnabled));
        }
        
        //XPaths for GroupBox
        protected By GetXPathForDisabledGroupBoxWithText(string grpText, string isDisabled) //*[contains(@class, 'x-fieldset-with-title') and @aria-disabled="true"]//*[text()="TestedGroupBox"]
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-fieldset-with-title') and @aria-disabled='{1}']//*[text()='{0}']", grpText, isDisabled));
        }

        protected By GetXPathForGroupBoxWithText(string grpText)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-fieldset x-fieldset-with-title x-fieldset-with-legend x-abs-layout-item x-window-item x-fieldset-default')]//*[text()='{0}']", grpText));
        }

        protected By GetXPathForDisabledPanelWithCssClass(string pnlCssClass, string isDisabled) //*[contains(@class, 'vt-test-pnl-2')]//a[contains(@class, 'x-btn') and @aria-disabled='true']
        {
            return By.XPath(string.Format("//*[contains(@class, '{0}')]//a[contains(@class, 'x-btn') and @aria-disabled='{1}']", pnlCssClass, isDisabled));
        }

        protected By GetXPathForGrid()
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-panel x-grid-locked x-abs-layout-item x-window-item x-panel-default x-grid')]"));
        }
        protected By GetXPathForImage()
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-img x-abs-layout-item x-window-item x-img-default')]"));
        }

        protected By GetXPathForLabelWithText(string lblText)
        {
            return By.XPath(string.Format("//label[contains(text(), '{0}')]", lblText));
        }

        protected By GetXPathForEmptyRichTextBox()
        {
            return By.XPath("//*[contains(@class, 'x-htmleditor-iframe')]");
        }

        
        protected By GetXPathForRichTextBox()
        {
            return By.XPath("//div[contains(@class, 'x-html-editor-input')]");
        }

        protected By GetXPathForRichTextBoxWithCssClassAriaDisabled(String rtbCssClass, String isEnabled)
        {
            return By.XPath(String.Format("//div[contains(@class, '{0}')]//div[contains(@class, 'x-html-editor-tb') and @aria-disabled='{1}']", rtbCssClass, isEnabled));
        }
        protected By GetXPathForRichTextBoxWithText()
        {
            return By.XPath("//iframe[contains(@class, 'x-htmleditor-iframe')]");
        }
        protected By GetXPathForRichTextBoxWithCssClass(string richtxtCssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class, 'x-html-editor-tb')]", richtxtCssClass));
        }

        protected By GetXPathForNumericUpDown()
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-item-body x-form-item-body-default x-form-text-field-body x-form-text-field-body-default  ')]"));
        }

        protected By GetXPathForComboBoxWithText(String cmbText)
        {
            //return By.XPath(string.Format("//*[contains(@class, 'x-field' x-form-item x-form-item-default x-form-type-text x-abs-layout-item x-window-item x-field-default x-absolute-form-item x-form-item-no-label')]//*[text()='{0}']", cmbText));
            return By.XPath(string.Format("//*[contains(@class, 'x-form-text')]//span[text()='{0}']", cmbText));
            //*[@id="combobox-1738-inputEl"]
        }

        protected By GetXPathForComboBoxWithCssClass(String cmbCssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//input", cmbCssClass)); //div[contains(@class, "vt-test-cmb-2")]//input
        }

        protected By GetXPathForReadOnlyComboBoxWithCssClass(String cmbCssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//input[@readonly='readonly']", cmbCssClass));   //div[contains(@class, "vt-test-cmb-2")]//input[@readonly='readonly']
        }

        protected By GetXPathForEnabledComboBoxWithCssClass(String cmbCssClass, String isEnabled) //div[contains(@class, "vt-test-cmb-2")]//input[@aria-disabled='true']
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//input[@aria-disabled='{1}']", cmbCssClass, isEnabled));   
        }

        protected By GetXPathForComboBoxArrowWithCssClass(String cmbCssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class,'x-form-arrow-trigger')]", cmbCssClass)); 
        }

        protected By GetXPathForTimeEditArrowWithCssClass(String cmbCssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class,'x-form-time-trigger')]", cmbCssClass));
        }

        protected By GetXPathForControlWithCssClass(String CssClassName)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//input[contains(@class, 'x-form-text')]", CssClassName));
        }

        protected By GetXpathForControlsWithCssClassOnly(String cssClassName)
        {
            return By.XPath(string.Format("//*[contains(@class, '{0}')]", cssClassName));
        }

        protected By GetXPathForComboBoxItemWithText(String cmbItemText) 
        {
            return By.XPath(string.Format("//li[contains(@role, 'option') and contains(@data-recordindex, '0')]"));
            //return By.XPath(string.Format("//li[contains(@value, '{0}')]", cmbItemText));   //li[contains(@value, 'NewItem')]
        }

        protected By GetXPathForComboBoxList()
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-arrow-trigger')]"));
        }
        protected By GetXPathForComboBox()
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-field')]"));
        }
        //XPath for ProgressBar
        protected By GetXPathForProgressBar()
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-progress')]"));
        }
        
        protected By GetXPathForDisabledTreeWithCssClass(String cssClass, string isDisabled) 
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class, 'x-grid-header-ct') and @aria-disabled='{1}']", cssClass, isDisabled));
        }

        protected By GetXPathForTreeItemWithCssClassAndText(String cssClass, string txt)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//span[contains(@class, 'x-tree-node-text') and text()='{1}']", cssClass, txt));
        }

        //XPaths for RadioButton
        protected By GetXPathForRadioButtonWithText(string radioButtonText)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-cb')]//*[text()='{0}']", radioButtonText));
        }

        //ComboBoxItem
        protected By GetXPathForComboBoxItem(String ItemText)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-boundlist')]//*[text()='{0}']", ItemText));
        }
        //Select Tab page
        protected By GetXPathForTabPageWithText(String TapPageText)
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-tab')]//*[text()='{0}']", TapPageText));
        }

        protected By GetXPathForContextMenuItemWithText(string itemTxt)
        {
            return By.XPath(string.Format("//span[contains(@class, 'x-menu-item-text') and text()='{0}']//parent::a", itemTxt));
        }

        protected By GetXPathForExpandedContextMenu(string isExpanded)
        {
            return By.XPath(string.Format("//div[contains(@class, 'x-menu') and @aria-expanded='{0}']", isExpanded));
        }

        protected By GetXPathForDisabledTabWithCssClass(String cssClass, string isDisabled) 
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[@aria-disabled='{1}']", cssClass, isDisabled));
        }

        ////*[contains(@class, "vt-testedGrid1")]//div[contains(@role, 'columnheader')]//span[text()="Column1"]
        protected By GetXPathForGridColumnHeaderSpanWithText(string colText)
        {
            return By.XPath(string.Format("//div[contains(@role, 'columnheader')]//span[text()='{0}']", colText));
        }

        protected By GetXPathForGridRowHeaderWithId(string rowId) 
        {
            return By.XPath(string.Format("//td[contains(@class, 'x-grid-cell-row-numberer')]//parent::tr//parent::tbody//parent::table[contains(@data-recordindex, '{0}')]//td", rowId));
        }

        protected By GetXPathForGridColumnHeaderSpanWithTextAndCssClass(string cssClass, string colText)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@role, 'columnheader')]//span[text()='{1}']", cssClass, colText));
        }

        protected By GetXPathForGridColumnHeaderMenuBtnWithTextAndCssClass(string cssClass, string colText)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@role, 'columnheader')]//span[text()='{1}']//parent::div//parent::div//parent::div//parent::div//div[contains(@class, 'x-column-header-trigger')]", cssClass, colText));
        }
        
        protected By GetXPathForGridColumnHeaderWithColumnCssClass(string cssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]", cssClass));
        }

        protected By GetXPathForGridCellWithIdAndText(string colId, string cellTxt)
        {
            return By.XPath(string.Format("//td[contains(@data-columnid, '{0}')]//div[contains(text(), '{1}')]//parent::td", colId, cellTxt)); 
        }

        protected By GetXPathForGridCellWithColId(string colId) 
        {
            return By.XPath(string.Format("//td[contains(@data-columnid, '{0}')]", colId)); 
        }


        protected By GetXPathForGridEditableCellWithIdAndText(string colId, string cellTxt) //td[contains(@data-columnid, "GridCol2_2")]//div[text()="male"]
        {
            return By.XPath(string.Format("//td[contains(@data-columnid, '{0}')]//div[contains(text(), '{1}')]//parent::td//child::input", colId, cellTxt));
        }


        protected By GetXPathForGridColoredRowHeaderCellWithInd(string rowInd, string color) //table[contains(@data-recordindex, "0")]//td[contains(@class, "x-grid-cell-row-numberer") and contains(@style, "background-color: aquamarine")]
        {
            return By.XPath(string.Format("//table[contains(@data-recordindex, '{0}')]//td[contains(@class, 'x-grid-cell-row-numberer') and contains(@style, 'background-color: {1}')]", rowInd, color));
        }

        protected By GetXPathForGridRowHeaderImageCellWithInd(string rowInd) 
        {
            return By.XPath(string.Format("//table[contains(@data-recordindex, '{0}')]//td[contains(@class, 'x-grid-cell-row-numberer') and contains(@style, 'background')]", rowInd));
        }

        protected By GetXPathForGridCheckColumnWithId(string colId, string rowId) //span[contains(@class, 'x-grid-checkcolumn')]//parent::div//parent::td[contains(@data-columnid, 'GridCol1')]//parent::tr//parent::tbody//parent::table[contains(@data-recordindex, '2')]
        {
            return By.XPath(string.Format("//span[contains(@class, 'x-grid-checkcolumn')]//parent::div//parent::td[contains(@data-columnid, '{0}')]//parent::tr//parent::tbody//parent::table[contains(@data-recordindex, '{1}')]//child::tbody//child::tr//child::td//child::div//child::span", colId, rowId));
        }

        protected By GetXPathForListViewItemWithId(string rowId)
        {
            return By.XPath(string.Format("//td[contains(@class, 'x-grid-cell')]//parent::tr//parent::tbody//parent::table[contains(@data-recordindex, '{0}')]//child::tbody//child::tr//child::td", rowId));
        }

        protected By GetXPathForListViewCheckColumnWithId(string rowId) 
        {
            return By.XPath(string.Format("//td[contains(@class, 'x-grid-cell-special')]//parent::tr//parent::tbody//parent::table[contains(@data-recordindex, '{0}')]//child::tbody//child::tr//child::td", rowId));
        }
        protected By GetXPathForListViewCheckColumnWithIdAndCSSClass(string CSSClass ,string rowId)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//td[contains(@class, 'x-grid-cell-special')]//parent::tr//parent::tbody//parent::table[contains(@data-recordindex, '{1}')]//child::tbody//child::tr//child::td",CSSClass, rowId));
        }

        protected By GetXPathForDisabledControlWithCssClass(string cssClass, string isDisabled)
        {
            return By.XPath(string.Format("//*[contains(@class, '{0}') and @aria-disabled='{1}']", cssClass, isDisabled));
        }

        protected By GetXPathForDisabledTBCheckItemWithCssClass(string cssClass, string isDisabled)
        {
            return By.XPath(string.Format("//*[contains(@class, '{0}')]//*[@aria-disabled='{1}']", cssClass, isDisabled));
        }

        protected By GetXPathForImageWithCssClass(string cssClass) 
        {
            return By.XPath(string.Format("//img[contains(@class, '{0}')]", cssClass));
        }

        protected By GetXPathForDisabledImageWithCssClass(string cssClass) //img[contains(@class, "vt-test-img-2") and contains(@class, 'x-item-disabled')]
        {
            return By.XPath(string.Format("//img[contains(@class, '{0}') and contains(@class, 'x-item-disabled')]", cssClass));
        }

        protected By GetXPathForWindowWithId(string winId)
        {
            return By.XPath(string.Format("//div[contains(@id, '{0}')]", winId));
        }

        protected By GetXPathForWindowCloseButtonWithId(string winId)
        {
            return By.XPath(string.Format("//div[contains(@id, '{0}')]//div[contains(@class, 'x-tool-close')]", winId)); 
        }

        //XPaths for CheckBox
        protected By GetXPathForRadioButtonInputElWithText(string chkText) //*[contains(@class, "x-form-cb")]//*[text()="Male"]//parent::div//child::input
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-cb')]//*[text()='{0}']//parent::div//child::input", chkText));
        }


        protected By GetXpathForControlsWithCssClassAndClass(String cssClassName, String ClassName)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class, '{1}')]",cssClassName, ClassName));
        }

        protected By GetXpathForControlsWithCssClassAnd2Classes(String cssClassName, String ClassName1, String ClassName2)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class, '{1}')]//div[contains(@class, '{2}')]", cssClassName, ClassName1, ClassName2));
        }

        protected By GetXPathForGridWithCssClassAndCellText(string cssClass, string cellText)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//*[text()='{1}']//parent::td", cssClass, cellText));
        }

        protected By GetXPathForGridWithCssClassAndClickedCellText(string cssClass, string cellText)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//*[text()='{1}']//parent::td//child::input", cssClass, cellText));
        }

        protected By GetXPathForNumericUpDownWithCssClass(String nudCssClass)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//input", nudCssClass)); //div[contains(@class, "vt-test-cmb-2")]//input
        }

        protected By GetXpathForNumericUpDownArrowDownWithCssClassAndClass(String cssClassName)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class, 'x-form-spinner-down')]", cssClassName));
        }
        protected By GetXpathForNumericUpDownArrowUpWithCssClassAndClass(String cssClassName)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//div[contains(@class, 'x-form-spinner-up')]", cssClassName));
        }
        protected By GetXpathForLeftSideBar() 
        {
            return By.XPath(string.Format("//div[contains(@class, 'x-grid-item-container')]//parent::div"));
        }

 //When using these three DteTimePicker , make sure you open only your DateTimePicker , otherwise it will find one of the DaeTimepicker and maybe not the one you need

        //DateTimePicker - Select a day from the calendar
        protected By GetXpathForDateTimePickerDay(String day)
        {
            return By.XPath(string.Format("//div[contains(@class, 'x-datepicker-date') and text()='{0}']", day));
        }

        //DateTimePicker - Slide to previous month by clicking the left arrow in the calendar
        protected By GetXpathForDateTimePickerLeftArrowToPreviousMonth()
        {
            return By.XPath(string.Format("//div[contains(@title, 'Previous Month (Control+Left)')]"));
        }

        //DateTimePicker - Slide to next months by clicking the right arrow in the calendar
        protected By GetXpathForDateTimePickerRightArrowToNextMonth()
        {
            return By.XPath(string.Format("//div[contains(@title, 'Next Month (Control+Right)')]"));
        }

        protected By GetXPathForGridTextButtonCellWithIdAndText(string colId, string cellTxt)
        { 
            return By.XPath(string.Format("//div[contains(@class, 'x-box-target') and text()='{0}']//parent::div//parent::div//parent::div//parent::td[contains(@data-columnid, '{1}')]//input", cellTxt, colId));
        }

        //XPaths for tree

        protected By GetXPathForSearchPanel()
        {
            return By.XPath(string.Format("//input[contains(@class, 'x-form-field') and @placeholder='Search']"));
        }


        protected By GetXPathForTreeNodeExpandButtonWithText(string nodeText)
        {
            return By.XPath(string.Format("//td[contains(@class, 'x-grid-cell')]//span[text()='{0}']//parent::div//parent::td//child::div//child::div[contains(@class, 'x-tree-expander')]", nodeText));
        }

        protected By GetXPathForTreeNodeExpandButtonWithTextAndCSSClass(string cssClassName, string nodeText)
        {
            return By.XPath(string.Format("//div[contains(@class, '{0}')]//td[contains(@class, 'x-grid-cell')]//span[text()='{1}']//parent::div//parent::td//child::div//child::div[contains(@class, 'x-tree-expander')]", cssClassName, nodeText));
        }
        

/*
        protected By GetXPathForCheckedRadioButtonWithText(string chkText) 
        {
            return By.XPath(string.Format("//*[contains(@class, 'x-form-cb-checked')]//*[text()='{0}']", chkText));
        }
*/
        //CSSClass

        //protected By GetXpathForSideMenuNode(string nodeText)
        //{
        //    return By.XPath(string.Format("//*[contains(@class, 'vt-test-button1')]"));
        //}

        // .//*[contains(@class, 'x-tab')]//*[text()='{0}']


        //  //*[text()[contains(.,'{0}')]]
        //("//label[contains(text(), '{0}')]", lblText));

        #endregion

        #region UI Helper Methods
        /// <summary>
        /// Scrolls to element.
        /// </summary>
        /// <param name="element">The element.</param>
        protected void ScrollToElement(IWebElement element)
        {
            Actions actions = new Actions(this.Driver);
            actions.MoveToElement(element);
            actions.Perform();

            Thread.Sleep(100);
        }

        /// <summary>
        /// Avoid appearance of tooltip by setting showDelay to 99 sec
        /// If tooltip occurs suddenly snapshot will be different and test become RED
        /// </summary>
        protected void PreventTooltip()
        {
            this.Driver.ExecuteScript(@"
                Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
                    showDelay: 99999
                });
            ");
        }
        #endregion

        #region Test Execution
        /// <summary>
        /// Executes the test.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="exampleId">The example identifier.</param>
        protected void ExecuteTest(string membersCategory, string memberName, string exampleId)
        {
            _currentExampleId = exampleId;
            // Select Element from TreeView
            this.Driver.WaitForElement(GetXpathForSideMenuNode(this.ElementName)).Click();

            if (string.IsNullOrWhiteSpace(membersCategory)) 
            {
                // Generic element test (not attached to member)
                this.Driver.WaitForElement(GetXpathForBasicPreviewBtn(exampleId)).Click();

            }
            else
            {
                // Click Members tab
                this.Driver.WaitForElement(GetXpathForExampleTabNode(membersCategory)).Click();

                // Click member
                this.Driver.WaitForElement(GetXpathForMemberItemTitle(memberName)).Click();

                switch (membersCategory)
                {
                    case "Properties":
                        // Click example preview
                        this.Driver.WaitForElement(GetXpathForPreviewBtn("Property", memberName, exampleId)).Click();
                        break;
                    case "Methods":
                        // Click example preview
                        this.Driver.WaitForElement(GetXpathForPreviewBtn("Method", memberName, exampleId)).Click();
                        break;
                    case "Events":
                        // Click example preview
                        this.Driver.WaitForElement(GetXpathForPreviewBtn("Event", memberName, exampleId)).Click();
                        break;
                }
                
            }
            
            // Wait for example window
            var extWindowElement = this.Driver.WaitForElement(GetXpathForWindowElement(exampleId));

            extWindowElement.WaitForImageResources();

            // Initialize "Not found snapshots" flag
            _testResult = TestResult.Passed;

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId, extWindowElement);

            // Example specific test flow goes here ---------------------------
            ExecuteCustomExampleFlow(membersCategory, memberName, exampleId);

            switch (_testResult)
            {
                case TestResult.Failed:
                    Assert.Fail(_message);
                    break;
                case TestResult.Inconclusive:
                    Assert.Inconclusive(_message);
                    break;
            }
        }

        /// <summary>
        /// Executes the custom example flow.
        /// </summary>
        /// <param name="testName">Name of the test.</param>
        private void ExecuteCustomExampleFlow(string membersCategory, string memberName, string exampleId)
        {
            MethodInfo customTestFlow = this.GetType().GetMethod(exampleId + "Flow", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            if (customTestFlow != null)
            {
                try
                {
                    IWebElement testWindow = this.Driver.WaitForElement(GetXpathForWindowElement(exampleId));

                    this.PreventTooltip();

                    customTestFlow.Invoke(this, new object[] { testWindow, membersCategory, memberName, exampleId });

                }
                catch (TargetInvocationException ex)
                {
                    AssertFailedException failedEX = ex.InnerException as AssertFailedException;
                    if (failedEX != null)
                    {
                        throw failedEX;
                    }
                    AssertInconclusiveException inconclusiveEX = ex.InnerException as AssertInconclusiveException;
                    if (inconclusiveEX != null)
                    { 
                        throw inconclusiveEX;
                    }

                    throw;
                }
            }
        }
        #endregion

        #region Snapshot Processing
        /// <summary>
        /// Compare the snapshot to the baseline.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="snapshotName">Name of the snapshot.</param>
        /// <param name="extWindowElement">The ext window element.</param>
        protected void CompareSnapshot(string membersCategory, string memberName, string snapshotName, IWebElement extWindowElement, string tempSnapshotName)
        {
            string baselineSnapshot = GetSnapshotFileName(_baselineDir, this.ElementName, membersCategory, memberName, snapshotName, false);
            if (!File.Exists(baselineSnapshot))
            {
                PersistSnapshot(_resultsNewDir, this.ElementName, membersCategory, memberName, snapshotName, tempSnapshotName);
                ReportDifference(TestResult.Inconclusive, this.ElementName, membersCategory, memberName, snapshotName);
            }
            else if (!CompareBaselineSnapshots(tempSnapshotName, baselineSnapshot))
            {
                PersistSnapshot(_resultsDiffDir, this.ElementName, membersCategory, memberName, snapshotName, tempSnapshotName);
                PersistSnapshot(_resultsDiffDir, this.ElementName, membersCategory, memberName, snapshotName, tempSnapshotName, true);
                ReportDifference(TestResult.Failed, this.ElementName, membersCategory, memberName, snapshotName);
            }
        }

        /// <summary>
        /// Compares the baseline snapshots.
        /// </summary>
        /// <param name="tempSnapshotName">Name of the temporary snapshot.</param>
        /// <param name="baselineSnapshot">The baseline snapshot.</param>
        /// <param name="optIndes">The opt indes.</param>
        /// <returns></returns>
        private bool CompareBaselineSnapshots(string tempSnapshotName, string baselineSnapshot, int optIndes = 1)
        {
            bool exists1 = File.Exists(tempSnapshotName);
            bool exists2 = File.Exists(baselineSnapshot);

            // If both files not exists, consider equal
            if (!exists1 && !exists2)
            {
                return true;

            }

            if (!exists1 || !exists2)
            {
                return false;
            }
            return CompareBaselineSnapshotsInternal(tempSnapshotName, baselineSnapshot, optIndes);
        }

        /// <summary>
        /// Compares the baseline snapshots internal.
        /// </summary>
        /// <param name="tempSnapshotName">Name of the temporary snapshot.</param>
        /// <param name="baselineSnapshot">The baseline snapshot.</param>
        /// <param name="optIndes">The opt indes.</param>
        /// <returns></returns>
        private bool CompareBaselineSnapshotsInternal(string tempSnapshotName, string baselineSnapshot, int optIndex)
        {
            bool snapshotsEqual = CompareSnapshotFiles(tempSnapshotName, baselineSnapshot);
            if (snapshotsEqual)
            {
                return true;
            }
            string alternativeBaselineSnapshot = GetAlternativeBaseline(baselineSnapshot, optIndex++);
            if (!String.IsNullOrEmpty(alternativeBaselineSnapshot))
            {
                return CompareBaselineSnapshotsInternal(tempSnapshotName, alternativeBaselineSnapshot, optIndex);
            }

            return false;
        }

        /// <summary>
        /// Gets the alternative baseline.
        /// </summary>
        /// <param name="baselineSnapshot">The baseline snapshot.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        private string GetAlternativeBaseline(string baselineSnapshot, int index)
        {
            string extension = Path.GetExtension(baselineSnapshot);
            baselineSnapshot = baselineSnapshot.Substring(0, baselineSnapshot.Length - extension.Length);
            string alternativeName = String.Format("{0}_opt{1}{2}", baselineSnapshot, index, extension);
            if (File.Exists(alternativeName))
            {
                return alternativeName;
            }

            return null;
        }

        /// <summary>
        /// Checks the snapshot.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="snapshotName">Name of the snapshot.</param>
        /// <param name="extWindowElement">The ext window element.</param>
        protected void CheckSnapshot(string membersCategory, string memberName, string snapshotName, IWebElement extWindowElement)
        {
            string tempSnapshotName = TakeSnapshot(extWindowElement);
            CompareSnapshot(membersCategory, memberName, snapshotName, extWindowElement, tempSnapshotName);
        }

        /// <summary>
        /// Checks the full screen snapshot.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="snapshotName">Name of the snapshot.</param>
        /// <param name="extWindowElement">The ext window element.</param>
        /// <param name="exampleSize">Crop snapshot by the Example window size.</param>
        protected void CheckSnapshotExtended(string membersCategory, string memberName, string snapshotName, IWebElement extWindowElement, bool exampleSize=false)
        {
            string tempSnapshotName = TakeSnapshotExtended(extWindowElement,exampleSize);
            CompareSnapshot(membersCategory, memberName, snapshotName, extWindowElement, tempSnapshotName);
        }

        /// <summary>
        /// Checks the snapshot which is taken when the window is closed.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="snapshotName">Name of the snapshot.</param>
        /// <param name="extWindowElement">The ext window element.</param>
        protected void CheckClosedSnapshot(string membersCategory, string memberName, string snapshotName, IWebElement extWindowElement)
        {
            string tempSnapshotName = TakeClosedSnapshot();
            CompareSnapshot(membersCategory, memberName, snapshotName, extWindowElement, tempSnapshotName);
        }

        /// <summary>
        /// Reports the difference.
        /// </summary>
        /// <param name="testResult">The test result.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="snapshotName">Name of the snapshot.</param>
        private void ReportDifference(TestResult testResult, string membersCategory, string elementName, string memberName, string snapshotName)
        {
            _testResult = testResult;
            if (_testResult == TestResult.Inconclusive)
            {
                _message = String.Format("Baseline snapshot for\n\tElement: {0}\n\tCategory: {1}\n\tMember: {2}\n\tSnapsot: {3} not found",
                    elementName, membersCategory, memberName, snapshotName);
            }
            else
            {
                _message = String.Format("Snapshots are different for\n\tElement: {0}\n\tCategory: \n\tMember: {2}\n\tSnapsot: {3}",
                    elementName, membersCategory, memberName, snapshotName);
            }
            TestContext.WriteLine(_message);
        }

        /// <summary>
        /// Takes the snapshot.
        /// </summary>
        /// <param name="extWindowElement">The ext window element.</param>
        /// <returns></returns>
        private string TakeSnapshot(IWebElement extWindowElement)
        {
            using (var img = GetElementScreenShot(this.Driver, extWindowElement))
            {
                string tempSnapshot = Path.GetTempFileName();
                img.Save(tempSnapshot, ImageFormat.Png);

                return tempSnapshot;
            }
        }

        /// <summary>
        /// Takes the full screen snapshot.
        /// </summary>
        /// <param name="extWindowElement">The ext window element.</param>
        /// <returns></returns>
        private string TakeSnapshotExtended(IWebElement extWindowElement, bool exampleSize)
        {
            using (var img = GetElementScreenShotExtended(this.Driver, extWindowElement, exampleSize)) 
            {
                string tempSnapshot = Path.GetTempFileName();
                img.Save(tempSnapshot, ImageFormat.Png);

                return tempSnapshot;
            }
        }

        /// <summary>
        /// Takes the snapshot when the window is closed.
        /// </summary>
        /// <returns></returns>
        private string TakeClosedSnapshot()
        {
            using (var img = GetClosedElementScreenShot(this.Driver))
            {
                string tempSnapshot = Path.GetTempFileName();
                img.Save(tempSnapshot, ImageFormat.Png);

                return tempSnapshot;
            }
        }

        /// <summary>
        /// Gets the element screen shot.
        /// </summary>
        /// <param name="driver">The driver.</param>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static Bitmap GetElementScreenShot(IWebDriver driver, IWebElement element)
        {
            Screenshot sc = ((ITakesScreenshot)driver).GetScreenshot();
            using (var img = Image.FromStream(new MemoryStream(sc.AsByteArray)) as Bitmap)
            {
                var location = new Point()
                {
                    X = Math.Max(0, element.Location.X),
                    Y = Math.Max(0, element.Location.Y),
                };
                var size = new Size()
                {
                    Width = Math.Min(element.Size.Width, img.Width),
                    Height = Math.Min(element.Size.Height, img.Height),
                };

                return img.Clone(new Rectangle(location, size), img.PixelFormat);
            }
           
        }

        /// <summary>
        /// Gets the full screen shot, and crop it.
        /// </summary>
        /// <param name="driver">The driver.</param>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static Bitmap GetElementScreenShotExtended(IWebDriver driver, IWebElement element, bool exampleSize)
        {
            Size s = driver.Manage().Window.Size; 
            int screenWidth = s.Width;
            int screenHeight = s.Height;
            using (var img = new Bitmap(screenWidth, screenHeight) as Bitmap)
            {
                Graphics gfx = Graphics.FromImage((Image)img);
                gfx.CopyFromScreen(0, 0, 0, 0, new Size(screenWidth, screenHeight));

                var location = new Point();
                var size = new Size();
                if (exampleSize)
                {
                    location.X = Math.Max(0, element.Location.X);
                    location.Y = Math.Max(0, element.Location.Y + 66);
                    
                    size.Width = Math.Min(element.Size.Width, img.Width);
                    size.Height = Math.Min(element.Size.Height, img.Height);
                }
                else
                {
                    location.X = 50;
                    location.Y = element.Location.Y + 200;

                    size.Width = element.Size.Width + element.Location.X - 100;
                    size.Height = element.Size.Height - element.Location.Y - 40;
                }               

                return img.Clone(new Rectangle(location, size), img.PixelFormat);
            }

        }

        /// <summary>
        /// Gets the element screen shot when the window is closed.
        /// </summary>
        /// <param name="driver">The driver.</param>
        /// <returns></returns>
        internal static Bitmap GetClosedElementScreenShot(IWebDriver driver)
        {
            Screenshot sc = ((ITakesScreenshot)driver).GetScreenshot();
            using (var img = Image.FromStream(new MemoryStream(sc.AsByteArray)) as Bitmap)
            {
                var location = new Point(577,119);
                var size = new Size(765,735);

                return img.Clone(new Rectangle(location, size), img.PixelFormat);
            }

        }

        /// <summary>
        /// Persists the snapshot.
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="snapshotName">Name of the snapshot.</param>
        /// <param name="tempSnapshotName">Name of the temporary snapshot.</param>
        private static void PersistSnapshot(string folderName, string elementName, string membersCategory, string memberName, string snapshotName, string tempSnapshotName, bool diffFile = false)
        {
            string suffix = diffFile ? ".diff" : String.Empty;
            tempSnapshotName += suffix;
            if (File.Exists(tempSnapshotName))
            {
                string snapshotFileName = GetSnapshotFileName(folderName, elementName, membersCategory, memberName, snapshotName, true);
                    File.Copy(tempSnapshotName, snapshotFileName + suffix, true);
            }
        }

        /// <summary>
        /// Gets the name of the snapshot file.
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="membersCategory">The members category.</param>
        /// <param name="memberName">Name of the member.</param>
        /// <param name="testName">Name of the test.</param>
        /// <param name="createFolder">if set to <c>true</c> create folder if does not exists.</param>
        /// <returns></returns>
        private static string GetSnapshotFileName(string folderName, string elementName, string membersCategory, string memberName, string testName, bool createFolder = false)
        {
            if (!String.IsNullOrEmpty(elementName))
            {
                if (!String.IsNullOrEmpty(membersCategory))
                {
                    folderName = Path.Combine(folderName, _browser, elementName, membersCategory, memberName);
                }
                else
                {
                    folderName = Path.Combine(folderName, _browser, elementName);
                }

            }
            if (createFolder && !Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }

            string snapshotName = Path.Combine(folderName, testName + ".png");
            return snapshotName;
        }

        /// <summary>
        /// Compares the files.
        /// </summary>
        /// <param name="strFile1">The file1.</param>
        /// <param name="strFile2">The file2.</param>
        /// <returns></returns>
        public static bool CompareSnapshots(string strFile1, string strFile2)
        {
            bool exists1 = File.Exists(strFile1);
            bool exists2 = File.Exists(strFile2);

            // If both files not exists, consider equal
            if (!exists1 && !exists2)
            {
                return true;

            }

            if (!exists1 || !exists2)
            {
                return false;
            }

            return CompareSnapshotFiles(strFile1, strFile2);

        }

        /// <summary>
        /// Compares the snapshot files.
        /// </summary>
        /// <param name="strFile1">The string file1.</param>
        /// <param name="strFile2">The string file2.</param>
        /// <returns></returns>
        private static bool CompareSnapshotFiles(string strFile1, string strFile2)
        {
            using (MagickImage image1 = new MagickImage(strFile1))
            using (MagickImage image2 = new MagickImage(strFile2))
            using (MagickImage imageDiff = new MagickImage())
            {

                DateTime startTime = DateTime.Now;
                double compareResult = image1.Compare(image2, ErrorMetric.Fuzz, imageDiff);
                imageDiff.Write(strFile1 + ".diff");
                return (compareResult < 0.0025);
            }
        }

        #endregion
    }
}
