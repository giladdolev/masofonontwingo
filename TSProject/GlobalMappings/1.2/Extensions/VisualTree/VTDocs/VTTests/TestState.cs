﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VTTests
{
    public enum TestResult
    {
        Passed,
        Failed,
        Inconclusive
    }
}
