﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class TreeElementTests
    
    {

        public void TreeCheckBoxesFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Expand the third tree the one with the checkBoxes - sub nodes also contains checkBoxes 
            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithTextAndCSSClass("vt-tree-CheckBoxes", "Node0")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-expandTheTreeWithTheCheckboxes-SubItemsWithCheckBoxes", testWindow);
        }


        //Maybe need to edit the expansion of the tree (first commands) after bug 21853 will be fixed
        public void TreeKeyFieldNameFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Expand all nodes

            //Extend Node "1"
            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithText("1")).Click();
            //Wait for Tree
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ExpandNode1", testWindow);

            //Extend Node "2"
            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithText("2")).Click();

            //Extend Node "6"
            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithText("6")).Click();

            //Wait for Tree
            Thread.Sleep(2000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ExpandRestOfTheNodes-VerifyThereAre8NodesAccordingtoTheOrderWritenInTheExampleController", testWindow);

            //Button Click - Switch To different DataSource (With different TreeKeyFieldName) - Employee records
            testWindow.FindElement(GetXPathForButtonSpanWithText("Switch DataSource >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("switched to Employee records"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-SwitchDataSource-EmployeeRecords", testWindow);

            //Button Click - Switch To different DataSource (With different TreeKeyFieldName) - Profession records
            testWindow.FindElement(GetXPathForButtonSpanWithText("Switch DataSource >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("switched to Profession records"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-buttonClick-SwitchDataSource-ProfessionRecords", testWindow);

        }

        //Maybe need to edit the expansion of the tree (first commands) after bug 21853 will be fixed
        public void TreeParentFieldNameFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Expand all nodes

            //Extend Node "1"
            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithText("1")).Click();
            //Wait for Tree
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ExpandNode1", testWindow);

            //Extend Node "2"
            testWindow. FindElement(GetXPathForTreeNodeExpandButtonWithText("2")).Click();

            //Extend Node "6"
            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithText("6")).Click();

            //Wait for Tree
            Thread.Sleep(2000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ExpandRestOfTheNodes-VerifyThereAre8NodesAccordingtoTheOrderWritenInTheExampleController", testWindow);

            //Button Click - Switch To different DataSource (With different ParentFieldName) - Employee records
            testWindow.FindElement(GetXPathForButtonSpanWithText("Switch DataSource >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("switched to Employee records"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-SwitchDataSource-EmployeeRecords", testWindow);

            //Button Click - Switch To different DataSource (With different ParentFieldName) - Profession records
            testWindow.FindElement(GetXPathForButtonSpanWithText("Switch DataSource >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("switched to Profession records"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-buttonClick-SwitchDataSource-ProfessionRecords", testWindow);
            
        }

        //Search in SearchPanel currently doesn't work - need to extend and explore( expected results) search when bug 18660 will be fixed  
        public void TreeSearchPanelFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Search in Search Panel - type Finance
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys("Finance");
            //Wait for SearchPanel search
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SearchFor-Finance-OneRecord", testWindow);

            //Search on existing search - Type Backspace
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys(Keys.Backspace);
            //Wait for SearchPanel search
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SendKeys-OneBackSpace-SearchFor-Financ-StillOneRecord", testWindow);

            //Delete text
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys(Keys.Control + "a");
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys(Keys.Backspace);
            //Wait for SearchPanel search
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SendKeyToDeleteText-AllRecords", testWindow);

            //Search for 'Mon' (Monterey)
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys("Mon");
            //Wait for SearchPanel search
            Thread.Sleep(2000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SearchFor-Mon-AllRecords", testWindow);

            //Button Click - Change Tree Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change SearchPanel value >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToFalse-SearchPanelIsHidden", testWindow);

            //Button Click - Change Tree Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change SearchPanel value >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToTrue-SearchPanelIsShown", testWindow);

            //Delete text
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys(Keys.Control + "a");
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys(Keys.Backspace);
            //Wait for SearchPanel search
            Thread.Sleep(2000);
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-SendKeyToDeleteText-AllRecords", testWindow);

            //Search after changing SearchPanel value - search should work
            testWindow.FindElement(GetXPathForSearchPanel()).SendKeys("1");
            //Wait for SearchPanel search
            Thread.Sleep(2000);
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-SearchFor-1", testWindow);
           
         }


        public void TreeVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Tree Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToTrue", testWindow);


            //Wait for RichTextBox update 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFalse", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreeHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo80px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("110px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo110px", testWindow);
        }
        public void TreeLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void TreeLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Tree Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tree Location >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft200Top285", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tree Location >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top300", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void TreeSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Size value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300_40", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=140"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo140_110", testWindow);
        }
        public void TreeTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Top value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("285px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
        }
        public void TreeWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Width value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("140px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo140px", testWindow);
        }
        public void TreeBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_140_110", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreeClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_140_110", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreeClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("140,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo140_110", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreeTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".TreeElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTreeElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void TreeBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change NumericUpDown BorderStyle 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void TreeBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
           
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreePixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo80px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("110."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo110px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreePixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreePixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Top value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("285."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TreePixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Width value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("140."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo140px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 21186:VT - Tree.Font doesn't work
        public void TreeFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree Font 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
        }

        public void TreeEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForDisabledTreeWithCssClass("vt-test-tree-1", "false"));
            testWindow.FindElement(GetXPathForDisabledTreeWithCssClass("vt-test-tree-2", "true"));

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if combo Box is enabled 
            testWindow.FindElement(GetXPathForDisabledTreeWithCssClass("vt-test-tree-2", "false"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueSecondTree", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Check if combo is disabled
            testWindow.FindElement(GetXPathForDisabledTreeWithCssClass("vt-test-tree-2", "true"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondTree", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();

        }

        //Bug 23499 - Tree.ForeColor doesn't work
        public void TreeForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Tree ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToBlue", testWindow);

            //Button Click - Change Tree ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);

        }

        public void TreeSelectedItemFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForTreeItemWithCssClassAndText("vt-test-tree-1", "5")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ID: 5"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickToSelectLeftTreeItem-ID5", testWindow);

            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithText("1")).Click();

            testWindow.FindElement(GetXPathForTreeItemWithCssClassAndText("vt-test-tree-1", "2")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ID: 2"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickToSelectLeftTreeItem-ID2", testWindow);

            //Button Click - Select Item ID 5 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Select Item ID 5 >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ID: 5"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectItemID5-LeftTreeItem-ID5-ShouldBeSelected", testWindow);

            //Button Click - Select Item ID 2 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Select Item ID 2 >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ID: 2"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonSelectItemID2-LeftTreeItem-ID2-ShouldBeSelected", testWindow);

            testWindow.FindElement(GetXPathForTreeItemWithCssClassAndText("vt-test-tree-2", "Node1")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name: Node1"));

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickToSelectRightTreeItem-Node1", testWindow);

            testWindow.FindElement(GetXPathForTreeNodeExpandButtonWithText("Node0")).Click();

            testWindow.FindElement(GetXPathForTreeItemWithCssClassAndText("vt-test-tree-2", "Node01")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name: Node01"));

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToSelectRightTreeItem-Node01", testWindow);

            //Button Click - Select Node1 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Select Node1 >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name: Node1"));

            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickButtonSelectNode1-RightTreeItem-Node1-ShouldBeSelected", testWindow);

            //Button Click - Select Node01 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Select Node01 >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name: Node01"));

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickButtonSelectNode01-RightTreeItem-Node01-ShouldBeSelected", testWindow);


        }
    }
}
