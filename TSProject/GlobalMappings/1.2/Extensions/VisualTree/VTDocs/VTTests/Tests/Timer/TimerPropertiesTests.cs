﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class TimerElementTests
    {
        //Bug 19455 - default Interval value is wrong
        public void TimerIntervalFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Wait 20 seconds
            Thread.Sleep(20000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-20Seconds", testWindow);


            //Button Click - Change Interval value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Interval value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: 100"));
           //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeTo100", testWindow);


            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-5seconds", testWindow);


            //Button Click - Change Interval value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Interval value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: 1000"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeTo1000", testWindow);


            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-5seconds", testWindow);

            //Close window
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TimerEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Wait 5 seconds
            Thread.Sleep(5000);
            
            //Button Click - Change Enable value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enable value >>")).Click();
            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Wait5Seconds-ChangeEnableAndWait5Seconds", testWindow);

            //Close window
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

    }
}
