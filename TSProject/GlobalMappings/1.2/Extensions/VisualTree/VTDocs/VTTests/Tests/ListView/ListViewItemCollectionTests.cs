﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ListViewElementTests
    {

        public void ListViewListViewItemCollectionBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change ListView BackColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Item2 BackColor value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToOrange", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Item2 BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        
        //Bug 24314 - ListView.SmallImageList property doesn't work at run time
        //Bug 22132 - server error in ListView.ListViewItemCollection.Add Method (String, String)
        public void ListViewListViewItemCollectionAdd2StringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Add an item with text and image to the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add ListViewItem >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A new item with text and image was added."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-AddNewItemWithTextAndImage", testWindow);

            //Button click - Remove an item with text and image from listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove ListViewItem >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("An item was removed from TestedListView."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-RemoveItem", testWindow);

            //Button click - Add list of items with text and image to the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add list of ListViewItems >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("List of items with text and image was added."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-AddListOfItemsWithTextAndImage", testWindow);

            //Button click - Add an item with text and image to the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add ListViewItem >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A new item with text and image was added."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-AddItemWithTextAndImage", testWindow);

            //Button click - Remove an item with text and image from listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove ListViewItem >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("An item was removed from TestedListView."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-RemoveItem", testWindow);
        }


        
    }
}
