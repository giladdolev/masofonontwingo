﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ToolBarMenuItemTests
    {

        public void ToolBarToolBarMenuItemEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-1-new", "false"));
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-1-open", "false"));
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-new", "true"));
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-open", "true"));

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("Enabled")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-new", "false"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickChangeEnabledValueToEnableNewTbMenuItem", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-open", "false"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickChangeEnabledValueToEnableOpenTbMenuItem", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("Enabled")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-new", "true"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickChangeEnabledValueToDisableNewTbMenuItem", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-open", "true"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickChangeEnabledValueToDisableOpenTbMenuItem", testWindow);
        }

        public void ToolBarToolBarMenuItemVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();
            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("Visible")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("New MenuItem Visible value: True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectItemNew-CheckVisibleChk-ClickChangeVisibleValuebtn-ToShowNewTbMenuItem", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Open MenuItem Visible value: True."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectItemOpen-ClickChangeVisibleValuebtn-ToShowOpenTbMenuItem", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();
            //UnCheck CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("Visible")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("New MenuItem Visible value: False."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectItemNew-UnCheckVisibleChk-ClickChangeVisibleValuebtn-ToHideNewTbMenuItem", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Open MenuItem Visible value: False."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SelectItemOpen-ClickChangeVisibleValuebtn-ToHideOpenTbMenuItem", testWindow);
        }

        public void ToolBarToolBarMenuItemImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //File MenuItem Click
            testWindow.FindElement(GetXPathForToolBarMenuItemWithCssClassAndWithText("vt-test-tbbutton-1-file","File")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-InToolBar1-OpenFileMenu-SeeImages", testWindow);

            //File MenuItem Click
            testWindow.FindElement(GetXPathForToolBarMenuItemWithCssClassAndWithText("vt-test-tbbutton-2-file", "File")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-InToolBar2-OpenFileMenu-SeeImages", testWindow);

            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Image >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("File-New image value: Content/Images/Open.png,"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickChangeImageBtn-ToRplaceNewAndOpenImages", testWindow);

            //File MenuItem Click
            testWindow.FindElement(GetXPathForToolBarMenuItemWithCssClassAndWithText("vt-test-tbbutton-2-file", "File")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-InToolBar2-OpenFileMenu-SeeImagesAfterChange", testWindow);

            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Image >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("File-New image value: Content/Images/New.png,"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickChangeImageBtn-ToResetNewAndOpenImages", testWindow);

            //File MenuItem Click
            testWindow.FindElement(GetXPathForToolBarMenuItemWithCssClassAndWithText("vt-test-tbbutton-2-file", "File")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-InToolBar2-OpenFileMenu-SeeImagesAfterReset", testWindow);

        }
    } 
}
