﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class RadioGroupElementTests
    {
        //Task 19199: VT - Implementation of 'DevExpress.XtraEditors.RadioGroup'
        public void BasicRadioGroupFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Select manually "Option4" radio button
            testWindow.FindElement(GetXPathForRadioButtonWithText("Option4")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Option4 is selected."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectOption4Manually", testWindow);


            //Button Click - Select Option2 (programmatically)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Select Option2 >>")).Click();
            //Wait for log label update. Uncomment the next line after 19199 is fixed.
            //testWindow.WaitForElement(GetXPathForLabelWithText("Option2 is selected."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectOption2Programmatically", testWindow);


            //Select manually "Item1" radio button
            testWindow.FindElement(GetXPathForRadioButtonWithText("Item1")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Item1 is selected."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectItem1Manually", testWindow);


            //Button Click - Select Item4 (programmatically)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Select Item4 >>")).Click();
            //Wait for log label update. Uncomment the next line after 19199 is fixed.
            //testWindow.WaitForElement(GetXPathForLabelWithText("Item4 is selected."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SelectItem4Programmatically", testWindow);
        }

        
    }
}
