﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class ToolBarElementTests
    {
        
        public void ToolBarBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change BackgroundImage
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No background image."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToNoImage", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with background image."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ToolBarHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("40px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ToolBarLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ToolBarLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Location value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft200Top285", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top300", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ToolBarSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change  Size value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=250"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo250_30", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=400"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo400_40", testWindow);
        }
        public void ToolBarTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Top value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("285px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
        }
        public void ToolBarWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Width value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("250px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo250px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("400px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo400px", testWindow);
        }
        //public void ToolBarBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        //{

        //    //Button Click - Change BorderStyle value
        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    //Wait for log Label update
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
        //    //Compare snapshots 1
        //    CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);


        //    //Button Click - Change BorderStyle value
        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    //Wait for log Label update
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
        //    //Compare snapshots 2
        //    CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
        //    //Compare snapshots 3
        //    CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
        //    //Compare snapshots 4
        //    CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
        //    //Compare snapshots 5
        //    CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
        //    //Compare snapshots 6
        //    CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
        //    //Compare snapshots 7
        //    CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
        //    //Compare snapshots 8
        //    CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
        //    //Compare snapshots 9
        //    CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
        //    //Compare snapshots 10
        //    CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
        //    //Compare snapshots 11
        //    CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
        //    //Compare snapshots 12
        //    CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);


        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("None."));
        //    //Compare snapshots 13
        //    CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);


        //    testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        //}
        public void ToolBarBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_400_40", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ToolBarClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_330_400_40", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ToolBarClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("400,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo400_40", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ToolBarVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ToolBarTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ToolBar Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".ToolBarElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToToolBarElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ToolBarBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change ToolBar BorderStyle 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ToolBarPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("40."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ToolBarPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ToolBarPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Top value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("285."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
        }
        public void ToolBarPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Width value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("250."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo250px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("400."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo400px", testWindow);
        }
        public void ToolBarBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.FindElement(GetXPathForLabelWithText("Color [Green]"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ToolBarBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change ToolBar BackgroundImageLayout 
            testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
            //Wait for Label update
            testWindow.FindElement(GetXPathForLabelWithText("Zoom."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
            testWindow.FindElement(GetXPathForLabelWithText("Tile."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
            testWindow.FindElement(GetXPathForLabelWithText("Stretch."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
            testWindow.FindElement(GetXPathForLabelWithText("Center."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
            testWindow.FindElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);
        }

        public void ToolBarEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-toolb-1", "false"));
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-toolb-2", "true"));

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if combo Box is enabled 
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-toolb-2", "false")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueSecondToolBar", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Check if combo is disabled
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-toolb-2", "true"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondToolBar", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        //Ater Bug 22769 fix - add 'AutoSize = false' in the ToolBar in the View    
        public void ToolBarMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=350, Height=100}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo350-100", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=400, Height=50}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo400-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=370, Height=150}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo370-150", testWindow);

            //Button Click - Cancel ToolBar MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        //Ater Bug 22769 fix - add 'AutoSize = false' in the ToolBar in the View    
        public void ToolBarMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=400, Height=50}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo400-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=350, Height=70}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo350-70", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=250, Height=40}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo250-40", testWindow);

            //Button Click - Cancel ToolBar MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }
    }
}
