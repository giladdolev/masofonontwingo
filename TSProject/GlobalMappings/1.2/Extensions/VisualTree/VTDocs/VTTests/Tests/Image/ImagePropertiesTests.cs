﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class ImageElementTests
    {

        public void ImageBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);


            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ImageHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("40px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("90px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo90px", testWindow);
        }
        public void ImageLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ImageLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Image Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Image Location >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft200Top285", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Image Location >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top300", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ImageSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Image Size 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=150"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150_90", testWindow);
        }
        public void ImageTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Image Top 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("285px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
        }
        public void ImageWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Image Width 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }
        public void ImageBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
               
        public void ImageBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_150_90", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ImageClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_150_90", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ImageClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("=200,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("=150,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo150_90", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ImageVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ImageImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - set Button Image 
            testWindow.FindElement(GetXPathForButtonWithText("Change Image Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No image."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Image Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with image."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);

        }
        public void ImageTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Image Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".ImageElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToImageElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ImagePixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("40."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("90."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo90px", testWindow);
        }
        public void ImagePixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ImagePixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Image PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("285."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
        }
        public void ImagePixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Image PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }
        public void ImageBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Set with no background image 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No background image."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetWithNoBackgroundImage", testWindow);

            //Button Click - Set with background image
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with background image."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithBackgroundImage", testWindow);
         
        }

        public void ImageBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change BackgroundImageLayout 
            testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Zoom."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Tile."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Stretch."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Center."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);
        }

        public void ImageEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Check initialize settings
            IWebElement elem1 = testWindow.FindElement(GetXPathForImageWithCssClass("vt-test-img-1"));
            // check disabled by presence of 'x-item-disabled' class
            IWebElement elem2 = testWindow.FindElement(GetXPathForDisabledImageWithCssClass("vt-test-img-2"));
            IWebElement elem3 = testWindow.FindElement(GetXPathForMultiLineTextBoxWithText("Event Log:"));

            // check that top image is enabled
            elem1.Click();
            string s = elem3.GetAttribute("value");
            int i = s.IndexOf("0 - Image:");
            // The assertion fails if the condition is false
            Assert.IsTrue(i > 0, "Top Image Is Disabled");

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickTopImageToCheckIfEnabled", testWindow);

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));

            elem2.Click();

            s = elem3.GetAttribute("value");
            i = s.IndexOf("1 - TestedImage:");
            // The assertion fails if the condition is false
            Assert.IsTrue(i > 0, "Tested Image Is Disabled");

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeEnableToTrue-ClickImage", testWindow);

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));

            // check disabled by presence of 'x-item-disabled' class
            testWindow.FindElement(GetXPathForDisabledImageWithCssClass("vt-test-img-2"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeEnabledToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
            
        }
    }
}
