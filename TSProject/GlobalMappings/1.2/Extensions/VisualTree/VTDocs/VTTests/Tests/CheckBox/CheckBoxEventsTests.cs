﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class CheckBoxElementTests
    {


        public void CheckBoxClickFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Focus CheckBox1 and pressing space key 
            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("CheckBox1")).SendKeys(Keys.Space);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Orange"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-pressingSpaceKeyOnCheckBox1", testWindow);

            //Click CheckBox2
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DeepPink"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickCheckBox2", testWindow);

            //Click CheckBox2
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Indigo"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickCheckBox2", testWindow);
        }

        public void CheckBoxCheckedChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Change Checked property value
            testWindow.FindElement(GetXPathForButtonWithText("Change IsChecked >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeIsCheckedToTrue", testWindow);

            //Click button - Change Checked property value again
            testWindow.FindElement(GetXPathForButtonWithText("Change IsChecked >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeIsCheckedToFalse", testWindow);

           
                      //****Important Uncomment when bug 21036 is fixed (and delete the code after this comment)  
            ////Check CheckBox
            //testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            ////Wait for Log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            ////Compare snapshots 3
            //CheckSnapshot(membersCategory, memberName, exampleId + "3-after-CheckedManually-True", testWindow);

            ////UnCheck CheckBox
            //testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            ////Compare snapshots 4
            //CheckSnapshot(membersCategory, memberName, exampleId + "4-after-UnCheckedManually-False", testWindow);


            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for CheckBox Update
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("TestedCheckBox"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-CheckedManually-True", testWindow);

            //UnCheck CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("TestedCheckBox"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-UnCheckedManually-False", testWindow);

        }


        public void CheckBoxCheckStateChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Click button - Change Checked property value
            testWindow.FindElement(GetXPathForButtonWithText("Change CheckState >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Checked"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeCheckStateToChecked", testWindow);

            //Click button - Change Checked property value again
            testWindow.FindElement(GetXPathForButtonWithText("Change CheckState >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": Indeterminate"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeCheckStateToIndeterminate", testWindow);

            //Click button - Change Checked property value again
            testWindow.FindElement(GetXPathForButtonWithText("Change CheckState >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": Unchecked"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeCheckStateToUnchecked", testWindow);



            //Check\Uncheck  CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Checked"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-CheckedManually-Checked", testWindow);

            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": Unchecked"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-CheckManually-Unchecked", testWindow);

        }
        public void CheckBoxTextChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Some Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSomeText", testWindow);
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void CheckBoxLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Click CheckBox 1
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 1")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-CheckingChkBox1Manully", testWindow);

            //Click button - Click CheckBox 2
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 2")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckBox 1 was Leaved"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-CheckingChkBox2Manully", testWindow);

            //Click button - Click CheckBox 1
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 1")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckBox 2 was Leaved"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-CheckingChkBox1Manully", testWindow);
// check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click button - Click CheckBox 2
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 2")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusChkBox2", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(1000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(1000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();
// end check
// check shifting focus inside the browser window
            //Click button - Click CheckBox 2
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 2")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusChkBox2", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
// end check
        }

        public void CheckBoxVisibleChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke VisibleChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Visible Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeVisibleToFalse", testWindow);
            //Click button - Invoke VisibleChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Visible Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeVisibleToTrue", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
    }
}
