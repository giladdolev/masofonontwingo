﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class GridElementTests
    {

        public void GridChangeColumnPositionFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //************Move Column1

            //Click button - Move Column1 to index 1
            testWindow.FindElement(GetXPathForButtonWithText("Move Column1 in circle >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex: 1"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 DisplayIndex: 0"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 DisplayIndex: 2"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ButtonClick-MoveColumn1ToIndex1", testWindow);

            //Click button - Move Column1 to index 2
            testWindow.FindElement(GetXPathForButtonWithText("Move Column1 in circle >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex: 2"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 DisplayIndex: 0"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 DisplayIndex: 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ButtonClick-MoveColumn1ToIndex2", testWindow);

            //Click button - Move Column1 to index 0
            testWindow.FindElement(GetXPathForButtonWithText("Move Column1 in circle >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex: 0"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 DisplayIndex: 1"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 DisplayIndex: 2"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ButtonClick-MoveColumn1ToIndex0", testWindow);

            //**********Move Colunm3

            //Click button - Move Column3 to index 0
            testWindow.FindElement(GetXPathForButtonWithText("Move Column3 in circle >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex: 1"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 DisplayIndex: 2"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 DisplayIndex: 0"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ButtonClick-MoveColumn3ToIndex0", testWindow);

            //Click button - Move Column3 to index 1
            testWindow.FindElement(GetXPathForButtonWithText("Move Column3 in circle >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex: 0"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 DisplayIndex: 2"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 DisplayIndex: 1"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ButtonClick-MoveColumn3ToIndex1", testWindow);

            //Click button - Move Column3 to index 2
            testWindow.FindElement(GetXPathForButtonWithText("Move Column3 in circle >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex: 0"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 DisplayIndex: 1"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 DisplayIndex: 2"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ButtonClick-MoveColumn3ToIndex2", testWindow);

        }

        public void GridSetCellStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Expected Results:
            /*   .vt-grid-set-cell-style1 {
                  font-weight: bold;
                  color: lightcoral
                 }
   
                .vt-grid-set-cell-style2 {
                 font-family: "Times New Roman";
                 font-size: 250%;
                 background-color: lightgreen;
               }

                 .vt-grid-set-cell-style3 {
                  border: 5px solid red;
                  font-weight:bolder;
               }*/


            //Click button - Invoke Grid SetCellStyle(0, 0, 'vt-grid-set-cell-style1')
            testWindow.FindElement(GetXPathForButtonWithText("Invoke SetCellStyle(0,0,style1) >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("vt-grid-set-cell-style1"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-clickFirstButton", testWindow);

            //Click button - Invoke Grid SetCellStyle(1, 1, 'vt-grid-set-cell-style2')
            testWindow.FindElement(GetXPathForButtonWithText("Invoke SetCellStyle(1,1,style2) >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("vt-grid-set-cell-style2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-clickSencondButton", testWindow);

            //Click button - Invoke Grid SetCellStyle(2, 2, 'vt-grid-set-cell-style3')
            testWindow.FindElement(GetXPathForButtonWithText("Invoke SetCellStyle(2,2,style3) >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("vt-grid-set-cell-style3"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-clickThirdButton", testWindow);
        }

        public void GridSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke Grid SetBounds()
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetBoundsTo100_280_250_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetBoundsTo80_300_285_115", testWindow);
        }

        public void GridHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Grid Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void GridShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Grid Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }
        public void GridGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Grid GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".GridElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        public void GridToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }

        //Task 20606: VT - GridElement - SelectCell Method Implementation
        public void GridSelectCellFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid1Col3", "c")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("values:  (c)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickOnACell", testWindow);

            //Clicking on the button to set CurrenCell problematically
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Select Cell (0,1) >>")).Click();
            Thread.Sleep(1000);
            // *** Sleep should be replaced with WaitForElement when the task will be fixed
            //testWindow.WaitForElement(GetXPathForLabelWithText("values:  (d)");
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnButtonSelectCell(0,1)", testWindow);

            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid1Col2", "h")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("values:  (h)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnACellAgain", testWindow);
        }

        public void GridSetRowHeaderBackgroundColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - first row Aquamarine Background Color
            testWindow.FindElement(GetXPathForButtonSpanWithText("Aquamarine Background Color >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Aquamarine"));

            testWindow.FindElement(GetXPathForGridColoredRowHeaderCellWithInd("0", "aquamarine"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Index0ClickAquamarineColorBtn", testWindow);

            //Open of DropDown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmb-index")).Click();

            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-cmb-index")).SendKeys(Keys.ArrowDown + Keys.Enter);

            //Button Click - second row Fuchsia Background Color
            testWindow.FindElement(GetXPathForButtonSpanWithText("Fuchsia Background Color >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Fuchsia"));

            testWindow.FindElement(GetXPathForGridColoredRowHeaderCellWithInd("1", "fuchsia"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Index1ClickFuchsiaColorBtn", testWindow);

            //Button Click - second row Transparent Background Color
            testWindow.FindElement(GetXPathForButtonSpanWithText("Transparent Background Color >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Transparent"));

            testWindow.FindElement(GetXPathForGridColoredRowHeaderCellWithInd("1", "transparent"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Index1ClickTransparentColorBtn", testWindow);
        }

        public void GridSetRowHeaderBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - first row Aquamarine Background Color
            testWindow.FindElement(GetXPathForButtonSpanWithText("Label Background Image >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Label.png"));

            testWindow.WaitForElement(GetXPathForGridRowHeaderImageCellWithInd("0"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Index0ClickLabelImageBtn", testWindow);

            //Open of DropDown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmb-index")).Click();

            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-cmb-index")).SendKeys(Keys.ArrowDown + Keys.Enter);

            //Button Click - second row Fuchsia Background Color
            testWindow.FindElement(GetXPathForButtonSpanWithText("Button Background Image >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Button.png"));

            testWindow.WaitForElement(GetXPathForGridRowHeaderImageCellWithInd("1"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Index1ClickButtonImageBtn", testWindow);

            //Button Click - second row Transparent Background Color
            testWindow.FindElement(GetXPathForButtonSpanWithText("Clear Background Image >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Null"));

            Thread.Sleep(3000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Index1ClickClearImageBtn", testWindow);
        }

        public void GridSetRowVisibilityFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - call SetRowVisibility
            testWindow.FindElement(GetXPathForButtonWithText("Hide 2nd Row >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide-2ndRow", testWindow);

            //Click button - call SetRowVisibility
            testWindow.FindElement(GetXPathForButtonWithText("Show 2nd Row >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show-2ndRow", testWindow);
        }

        // bug 20569 - When clicking on the 'Sort Column 1' button, only column 1 should be in sorting mode. (Reopened)
        public void GridSortFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Sort Column 1
            testWindow.FindElement(GetXPathForButtonWithText("Sort Column 1 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Column 1 Sort Order: Descending"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickOnSortColumn1BtnDescendingOrder", testWindow);

            //Click button - Sort Column 1
            testWindow.FindElement(GetXPathForButtonWithText("Sort Column 1 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Column 1 Sort Order: Ascending"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnSortColumn1BtnAscendingOrder", testWindow);

            //Column Header Mouse Click
            testWindow.WaitForElement(GetXPathForGridColumnHeaderSpanWithText("Column2")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Grid Column 2 Sort Order: Ascending"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnColumn2AscendingOrder", testWindow);

            //Column Header Mouse Click
            testWindow.WaitForElement(GetXPathForGridColumnHeaderSpanWithText("Column2")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Grid Column 2 Sort Order: Descending"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickOnColumn2DescendingOrder", testWindow);

            //Click button - Sort Column 1
            testWindow.FindElement(GetXPathForButtonWithText("Sort Column 1 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Column 1 Sort Order: Descending"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickOnSortColumn1BtnDescendingOrder", testWindow);

            //Click button - Sort Column 1
            testWindow.FindElement(GetXPathForButtonWithText("Sort Column 1 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Column 1 Sort Order: Ascending"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickOnSortColumn1BtnAscendingOrder", testWindow);
        }
    }
}
