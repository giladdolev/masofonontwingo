﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class PanelElementTests
    {
        public void PanelSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke Panel SetBounds()
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetBoundsTo100_280_250_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetBoundsTo80_300_200_80", testWindow);
        }

        public void PanelHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Panel Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void PanelShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Panel Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }
        public void PanelGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Panel GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".PanelElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        public void PanelToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }
        public void PanelResetTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke ResetText()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ResetText >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is empty"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ResetText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Text >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Text is set"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetText", testWindow);
        }
      
    }
}
