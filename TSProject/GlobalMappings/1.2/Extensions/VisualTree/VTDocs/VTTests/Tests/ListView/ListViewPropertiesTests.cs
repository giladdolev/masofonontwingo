﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ListViewElementTests
    {
        public void ListViewSelectedItemsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            Actions actions = new Actions(CurrentWebDriver);
            actions.KeyDown(Keys.Control).Build().Perform();
            testWindow.FindElement(GetXPathForGridWithCssClassAndCellText("vt-listView-SelectedItems","a")).Click();
            testWindow.FindElement(GetXPathForGridWithCssClassAndCellText("vt-listView-SelectedItems", "g")).Click();
            actions.KeyUp(Keys.Control).Build().Perform();

            //Button click - Change ListView Size 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Get SelectedItems >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToWidth200Height70", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=273"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToWidth273Height99", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ListViewSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Button click - Change ListView Size 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToWidth200Height70", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=273"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToWidth273Height99", testWindow);
            
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ListViewBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change ListView BackColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToOrange", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //public void ListViewBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        //{
        //    //Button click - Change ListView BackgroundImage 
        //    testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();
        //    //Wait for Label update
        //    testWindow.WaitForElement(GetXPathForLabelWithText("image."));
        //    //Compare snapshots 1
        //    CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);


        //    testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));
        //    //Compare snapshots 2
        //    CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);


        //}

        //public void ListViewBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        //{
        //    //Button click - Change ListView BackgroundImageLayout 
        //    testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
        //    //Wait for Label update
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Zoom."));
        //    //Compare snapshots 1
        //    CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

        //    testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Tile."));
        //    //Compare snapshots 2
        //    CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

        //    testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Stretch."));
        //    //Compare snapshots 3
        //    CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

        //    testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Center."));
        //    //Compare snapshots 4
        //    CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

        //    testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
        //    testWindow.WaitForElement(GetXPathForLabelWithText("None."));
        //    //Compare snapshots 5
        //    CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);
        //}

         

         public void ListViewBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Bounds value
             testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_300_50", testWindow);


             testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
             // Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_273_99", testWindow);


         }

         public void ListViewClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ClentSize value
             testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
             //Wait for log Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("300,"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo300_45", testWindow);


             testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("273,"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo273_99", testWindow);
         }

         //public void ListViewHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         //{
         //    //Button Click - Change Height value
         //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
         //    //Wait for log label update
         //    testWindow.WaitForElement(GetXPathForLabelWithText("70px."));
         //    //Compare snapshots 1
         //    CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo70px", testWindow);


         //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
         //    testWindow.WaitForElement(GetXPathForLabelWithText("99px."));
         //    //Compare snapshots 2
         //    CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo99px", testWindow);
         //}

         public void ListViewClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ClientRectangle value
             testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_300_50", testWindow);


             testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_273_99", testWindow);


         }

         public void ListViewLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Left value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
             //Wait for log  label update
             testWindow.WaitForElement(GetXPathForLabelWithText("200px."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
         }

         public void ListViewLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Location value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change ListView Location >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
             //Compare snapshots 1 
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_385", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change ListView Location >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80_300", testWindow);
         }

         public void ListViewTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Top value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText(": 285px"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": 300px"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
         }

         public void ListViewWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Width value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText(": 200px."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": 273px."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo273px", testWindow);
         }

         public void ListViewVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView Visible value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("True."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("False."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void ListViewTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView Tag value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText(".ListViewElement"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToListViewElement", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void ListViewPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change PixelHeight value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("is: 70"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo70px", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("is: 99"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo99px", testWindow);
         }
         public void ListViewPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change PixelLeft Location 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("200."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("80."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void ListViewPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView PixelTop 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("285."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("300."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
         }
         public void ListViewPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView PixelWidth 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("200."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("273."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo273px", testWindow);
         }
         public void ListViewHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView Height 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("70px."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo70px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("99px."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo99px", testWindow);
         }

         //Bug 9503 - ListView.Font Property doesn't work.
         public void ListViewFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView Font 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
         }

         //Bug 9428:VTDocs - ListView.ForeColor property doesn't work at Run time and initializedComponent
         public void ListViewForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView ForeColor 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToBlue", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);
         }
         public void ListViewCheckBoxesFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ListView CheckBoxes value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change CheckBoxes value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("False."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToFalse", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change CheckBoxes value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("True."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToTrue", testWindow);

             // Adding a test to bug 23287 - Exception when unchecking a checkbox
             testWindow.FindElement(GetXPathForListViewCheckColumnWithId("0")).Click();

             Thread.Sleep(2000);

             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-CheckTopRow", testWindow);

             testWindow.FindElement(GetXPathForListViewCheckColumnWithId("0")).Click();

             Thread.Sleep(2000);

             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-UnCheckTopRow", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void ListViewItemsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Remove Item
             testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Item >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Items.Remove"));
             //Wait 2 seconds
             Thread.Sleep(2000);
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-RemoveItem", testWindow);

             //Button Click - Add Item
             testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Items.Add"));
             //Wait 2 seconds
             Thread.Sleep(2000);
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-AddItem", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Item >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Items.Remove"));

             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-2ndRemoveItem", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Items.Add"));
             //Wait 2 seconds
             Thread.Sleep(2000);
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-2ndAddItem", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void ListViewHotTrackingFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Hover the listview with default HotTracking value - 'false'
             IWebElement elem = testWindow.FindElement(GetXpathForControlsWithCssClassAndClass("vt-listview-1", "x-grid-item"));
             Actions actions = new Actions(CurrentWebDriver);
             actions.MoveToElement(elem);
             actions.Perform();
             Thread.Sleep(2000);
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-HoverListViewWithDefaultValueFalse", testWindow);

             //Hover the listview with initialize HotTracking value - 'true'
             IWebElement elem1 = testWindow.FindElement(GetXpathForControlsWithCssClassAndClass("vt-listview-2", "x-grid-item"));
             Actions actions1 = new Actions(CurrentWebDriver);
             actions1.MoveToElement(elem1);
             actions1.Perform();
             Thread.Sleep(2000);
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-HoverListViewWithInitializeValueTrue", testWindow);

         }

         public void ListViewEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Check initialize settings
             testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-lstview-1", "false"));
             testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-lstview-2", "true"));

             //Button Click - Change Enabled value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("True."));
             //Check if ListView is enabled  on the way checking one of the checkBoxes
             testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-lstview-2", "false")).Click();
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueAndCheckOneOftheCheckBoxesOfSecondlstview", testWindow);



             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("False."));
             //Check if ListView is disabled
             testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-lstview-2", "true"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondlstview", testWindow);

             //Button Click - Change Enabled value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("True."));
             //Check if ListView is enabled 
             testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-lstview-2", "false")).Click();
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToTrueSecondlstviewVerifyCheckBoxStillChecked", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }

         //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
         public void ListViewMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button click - Change MaximumSize
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=280, Height=80}"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo280-80", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=310, Height=50}"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310-50", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=350, Height=100}"));
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo350-100", testWindow);

             //Button Click - Cancel ListView MaximumSize, set it to (0, 0). Size is equal to his last size
             testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
         }

         //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
         public void ListViewMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button click - Change MinimumSize
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=360, Height=80}"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo360-80", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=100}"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200-100", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=400, Height=60}"));
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo400-60", testWindow);

             //Button Click - Cancel ListView MinimumSize, set it to (0, 0). Size is equal to his last size
             testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
         }

        //Bug 9533 - View enum of ListView doesn't exist in visual tree
        //When bug 9533 will be fixed, we need to expand the example and add the other View values (LargeIcon, Tile, SmalliCON and List)
         public void ListViewViewFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button click - Change ListView View value 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change View value for headers >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("View value is: LargeIcon."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToLargeIcon", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change View value for headers >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("View value is: Details."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToDetails", testWindow);
         }

         public void ListViewMenuDisabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-listview-1", "Column1")).Click();

             Thread.Sleep(2000);

             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickTopListViewColumn1HeaderNoMenuBtn", testWindow);

             testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-listview-2", "Column1")).Click();

             Thread.Sleep(2000);

             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickBottomLeftListViewColumn1HeaderWithMenuBtn", testWindow);

             testWindow.FindElement(GetXPathForGridColumnHeaderMenuBtnWithTextAndCssClass("vt-listview-2", "Column1")).Click();

             Thread.Sleep(2000);

             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnHeadersMenuBtnShowMenu", testWindow);

             testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-listview-3", "Column1")).Click();

             Thread.Sleep(2000);

             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickBottomRightListViewColumn1HeaderNoMenuBtn", testWindow);

         }
    }
}
