﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ErrorProviderElementTests
    {
        //Task 16508 - VT - Implement SetError(TextBoxElement txtClinic, string p) of ErrorProviderElement
        public void ErrorProviderSetErrorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click Login button
            testWindow.FindElement(GetXPathForButtonWithText("Login")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("on Username field"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickLoginwithEmptyTextboxes", testWindow);

            //Hover the error icon to see the error tool-tip
            //vt-txt-username
            IWebElement elem = testWindow.FindElement(GetXpathForControlsWithCssClassAndClass("vt-txt-username", "x-form-error-msg"));
            Actions actions = new Actions(CurrentWebDriver);
            actions.MoveToElement(elem);
            Thread.Sleep(2000);
            actions.Perform();
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-MouseHoverErrorIcon", testWindow);

            //Enter a username
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-username")).SendKeys("xxx");
            //Leave username textbox
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-password")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Entered Username:  xxx"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-EnterUsernameAndLeave", testWindow);

            //Delete username
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-username")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            testWindow.WaitForElement(GetXPathForTextBoxWithCssClass("vt-txt-username"));
            //Leave username textbox
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-password")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SetError method is invoked on Username field."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-DeleteUsernameAndLeave", testWindow);
        }

        //Task 16507 - VT - Implement GetError(ComboBoxElement cboAnaf) of ErrorProviderElement
        public void ErrorProviderGetErrorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Click in username textbox
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-username")).Click();
            //Leave username textbox
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-password")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Error description: Username"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-LeaveUsernameEmpty", testWindow);

            //Click Login button
            testWindow.FindElement(GetXPathForButtonWithText("Login")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Error description:"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickLoginwithEmptyTextboxes", testWindow);
            
        }

        //Task 16509 - VT - Implement Clear() of ErrorProviderElement
        public void ErrorProviderClearFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Click Login button
            testWindow.FindElement(GetXPathForButtonWithText("Login")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SetError method is invoked on Username field."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickLoginwithEmptyTextboxes", testWindow);

            //Click Login button
            testWindow.FindElement(GetXPathForButtonWithText("Clear Username ErrorProvider >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Clear method is invoked on Username field."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClearUsernameErrorProvider", testWindow);

            }
    }
}
