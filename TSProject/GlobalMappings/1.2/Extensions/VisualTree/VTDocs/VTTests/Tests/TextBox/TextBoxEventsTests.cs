﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class TextBoxElementTests
    {
        public void TextBoxTextChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Change Text
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Some Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSomeText", testWindow);
           
            //Click button - Change Text
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewText", testWindow);
            
            //Verify TextChanged is invoked by manually adding text
            testWindow.FindElement(GetXPathForTextBoxWithText("TestedTextBox")).SendKeys(" Editing");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text Editing"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeManuallyToEditing", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        // Bug 21414 - Calling Focus method programmatically doesn't invoke the GotFocus event
        public void TextBoxGotFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Focus First Name >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ButtonToFocusFirstNameTextBox", testWindow);

             testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).SendKeys(Keys.Tab);
            //Wait for log label update 
             testWindow.WaitForElement(GetXPathForLabelWithText("LastName"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToFocusLastNameTextBox", testWindow);            
            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToFocusFirstNameTextBox", testWindow); 
        }


        public void TextBoxEnterFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickFirstNameTextBox-FirstNameEnterIsInvoked", testWindow);

            testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("LastName"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToEnterLastNameTextBox-LastNameEnterIsInvoked", testWindow);

            //Click button - Change Focus FirstName TextBox
            testWindow.FindElement(GetXPathForButtonWithText("Focus() First Name >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Focus() was invoked on FirstName TextBox"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-clickButton-ToFocusFirstNameTextBox-FirstNameEnterIsInvoked", testWindow);

            //Click on the window's body to verify the text box Enter event isn't ivoked
            testWindow.FindElement(By.XPath(string.Format("//div[contains(@id, 'windowView2-body')]"))).Click();
            testWindow.FindElement(By.XPath(string.Format("//div[contains(@id, 'windowView2-body')]"))).Click();
            testWindow.FindElement(By.XPath(string.Format("//div[contains(@id, 'windowView2-body')]"))).Click();

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-clickingWindow3times-theEnterEventIsNotInvoked", testWindow);

           // By.XPath(string.Format("//div[contains(@id, 'windowView2-innerCt')]")

            //testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name")).SendKeys(Keys.Shift + Keys.Tab);
            ////Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("LastName"));
            ////Compare snapshots 2
            //CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToLeaveLastNameTextBox", testWindow);
            ////Click TextBox - Change Focus
            //testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name")).Click();
            ////Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            ////Compare snapshots 3
            //CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToLeaveFirstNameTextBox", testWindow);
            //// check shifting focus outside the browser window            // Create a new browser window
            //IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            //string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            //// resize the window
            //Size s = ChromeNewWinDriver.Manage().Window.Size;
            //ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            //Thread.Sleep(1000);

            ////Compare snapshots 4 
            //CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            //Thread.Sleep(1000);

            //// Click on header
            //testWindow.FindElement(By.Id("windowView2_header")).Click();

            ////Compare snapshots 5
            //CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            ////Click TextBox - Change Focus
            //testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).Click();

            ////Compare snapshots 6
            //CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusFirstTextBox", testWindow);

            //Thread.Sleep(1000);

            //ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            //Thread.Sleep(1000);

            ////Compare snapshots 7
            //CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            //Thread.Sleep(1000);

            //// click on body
            //testWindow.FindElement(By.Id("windowView2-body")).Click();

            ////Compare snapshots 8
            //CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            //// close the new window
            //ChromeNewWinDriver.Close();
            //// end check
            //// check shifting focus inside the browser window
            ////Click TextBox - Change Focus
            //testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).Click();

            ////Compare snapshots 9
            //CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusFirstTextBox", testWindow);

            ////Click ComboBox - Change Focus
            //testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            ////Compare snapshots 10
            //CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            //Thread.Sleep(1000);

            //// Click on header
            //testWindow.FindElement(By.Id("windowView2_header")).Click();

            ////Compare snapshots 11
            //CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
            //// end check
        }


        // Bug 21500 - leave event is invoked when the window loses focus
        public void TextBoxLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TabToLeaveFirstNameTextBox", testWindow);

            testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name")).SendKeys(Keys.Shift + Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("LastName"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToLeaveLastNameTextBox", testWindow);
            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToLeaveFirstNameTextBox", testWindow);
 // check shifting focus outside the browser window            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4 
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusFirstTextBox", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(1000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(1000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();
// end check
// check shifting focus inside the browser window
            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusFirstTextBox", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
// end check
        }

        // Bug 21413 - TextBox.KeyDown event is invoked twice
        public void TextBoxKeyDownFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement elemFirst = testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name"));
            IWebElement elemLast = testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name"));

            elemFirst.SendKeys(Keys.Tab);
         
            //expected: log is clear - tab does not invoke keydown
            //known bug: 25556 :tab invokes keydown once (should not invoke keydown)
           
            elemLast.SendKeys("1" + Keys.Space + "A");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("1 A."));
            //Compare snapshots 1
            //expected:lastname: 4 keydown (1 + space + shift + 'a')
            //known bug: 21413 :keydown invokes twice when pressing digits\ space and letter (Actual results 6 keydown for lastname)
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-EnterKey-and-1_A-ToLastNameTextBox", testWindow);

            elemLast.SendKeys(Keys.Backspace);
            testWindow.WaitForElement(GetXPathForLabelWithText("1 ."));
            //Compare snapshots 2
            //expected: one more keydown is invoked. => TOTAL: 5 keydown invoked (actual results: 7 keydown invoked - 21413)
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Backspace-1-ToLastNameTextBox", testWindow);
            
            
            elemLast.SendKeys(Keys.Shift + Keys.Tab);
            elemFirst.SendKeys("*");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("*."));
            //Compare snapshots 3
            //expected: one more keydown is invoked. => TOTAL: 6 keydown invoked (actual results: 12 keydown invoked - 21413)
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ShiftTabKey-and-Asterisk-ToFirstNameTextBox", testWindow);            
        }

        // Bug 7708 - The "KeyPress" event invoked after the key is already writen to the textbox
        public void TextBoxKeyPressFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement elem = testWindow.FindElement(GetXPathForTextBoxWithText("0000000000"));
            elem.SendKeys("2");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("2."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Enter-2-ToPhoneNoTextBox", testWindow);

            elem.SendKeys("P");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("2P."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Enter-P-ToPhoneNoTextBox", testWindow);

            elem.SendKeys(Keys.Backspace);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("2."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Backspace-2-ToPhoneNoTextBox", testWindow);

            elem.SendKeys(" *");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("2 *."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Space-And-Asterisk-ToPhoneNoTextBox", testWindow);
        }

        // Bug 21415 - TextBox.KeyDown,TextBox.KeyPress and TextBox.KeyUp events are invoked in wrong order
        public void TextBoxKeyUpFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement elemFirst = testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name"));
            IWebElement elemLast = testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name"));

            elemFirst.SendKeys(Keys.Control + "a");
             //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Control Pressed"));
            //Compare snapshots 1
            //expected:firstname: 2 keydown, 1 keypress, 2 keyup (control doesn't invoke keypress)
            //actual:firstname: 2 keydown
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-CtrlAToMarkFirstNameTextBox", testWindow);

            elemFirst.SendKeys(Keys.Delete);

            //Wait for log label update and click on it to take the focus out from the textbox
            testWindow.WaitForElement(GetXPathForLabelWithText("Empty")).Click();
            //Compare snapshots 2
            //expected:firstname: 1 keydown, 1 keyup (Delete doesn't invoke keypress)
            //actual:firstname: 1 keydown
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-DeleteToEraseFirstNameTextBox", testWindow);

            elemFirst.SendKeys(Keys.Enter);

            elemLast.SendKeys(Keys.Shift + Keys.Tab);
            testWindow.WaitForElement(GetXPathForLabelWithText("LastName"));
            //Compare snapshots 3
            //expected:firstname: 1 keydown, 1 keypress, 1 keyup (Enter key)
            //actual:firstname: 1 keydown, 1 keypress
            //expected:lastname: 1 keydown (Shift key)
            //expected:firstname: 2 keyup (Shift and Tab keys)
            //actual:lastname: 2 keydown (Shift and Tab keys)
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-EnterKey-ToFocusLastName-And-ShiftTab-ToFocusBackToFirstName", testWindow);

            elemFirst.SendKeys("1");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("1.")).Click();
            //Compare snapshots 4
            //expected:firstname: 1 keydown, 1 keypress, 1 keyup (1 key)
            //actual:firstname: 1 keydown, 1 keypress
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Enter-1-ToFirstNameTextBox", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button"));

            elemFirst.SendKeys("S");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("1S.")).Click();
            //Compare snapshots 5
            //expected:firstname: 2 keydown, 1 keypress, 2 keyup (shift doesn't invoke keypress)
            //actual:firstname: 2 keydown, 1 keypress
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClearLog-S-ToFirstNameTextBox", testWindow);

            elemFirst.SendKeys(Keys.Backspace);
            testWindow.WaitForElement(GetXPathForLabelWithText("1.")).Click();
            //Compare snapshots 6
            //expected:firstname: 1 keydown, 1 keypress, 1 keyup (Backspace key)
            //actual:firstname: 1 keydown

            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-Backspace-1-ToFirstNameTextBox", testWindow);

            testWindow.WaitForElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Button"));

            elemLast.SendKeys(" *");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("*.")).Click();
            //Compare snapshots 7
            //expected:lastname: 1 keydown, 1 keypress, 1 keyup (space key)
            //expected:lastname: 1 keydown, 1 keypress, 1 keyup (Asterisk key)
            //actual:lastname: 1 keydown, 1 keypress
            //actual:lastname: 1 keydown, 1 keypress
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClearLog-Space-And-Asterisk-ToLastNameTextBox", testWindow);            
        }
        //Bug 16213 - After fix delete Sleep and uncomment the GetXPathForTextBoxWithText functions
        public void TextBoxLostFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter First Name")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            //Thread.Sleep(1000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TabToLeaveFirstNameTextBox", testWindow);

            testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name")).SendKeys(Keys.Shift + Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("LastName"));
            //Thread.Sleep(1000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ShiftTabToLeaveLastNameTextBox", testWindow);

            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter Last Name")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FirstName"));
            //Thread.Sleep(1000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToLeaveFirstNameTextBox", testWindow);
        }
    }
}
