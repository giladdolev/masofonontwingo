﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class TreeElementTests
    {
        public void TreeItemCollectionContainsKeyFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Open the comboBox list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmb-tree-items")).Click();
            //Select "TreeItem" item from the comboBox
            testWindow.WaitForElement(GetXPathForComboBoxItem("TreeItem")).Click();
            //Button Click  - perform search on tree 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Perform ContainsKey >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": False."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-PerformContainsKey-TreeItem-false", testWindow);
            
            //Open the comboBox list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmb-tree-items")).Click();
            //Select "TreeItem0_0" item from the comboBox
            testWindow.WaitForElement(GetXPathForComboBoxItem("TreeItem0_0")).Click();
            //Button Click  - perform search on tree 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Perform ContainsKey >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": True."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-PerformContainsKey-TreeItem0_0-true", testWindow);

            //Open the comboBox list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmb-tree-items")).Click();
            //Select "TreeItem1_0_0" combo box item
            testWindow.WaitForElement(GetXPathForComboBoxItem("TreeItem1_0_0")).Click();
            //Button Click  - perform search on tree 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Perform ContainsKey >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": True."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-PerformContainsKey-TreeItem1_0_0-true", testWindow);

            //Open the comboBox list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmb-tree-items")).Click();
            //Select "TreeItem1" combo box item
            testWindow.WaitForElement(GetXPathForComboBoxItem("TreeItem1")).Click();
            //Button Click  - perform search on tree 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Perform ContainsKey >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": True."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-buttonClick-PerformContainsKey-TreeItem1-true", testWindow);
        }
    }
}
