﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class RadioButtonElementTests
    {

        public void RadioButtonIsCheckedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Button Click - Change IsChecked value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change IsChecked value >>")).Click();
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value is: False."));
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: False."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-changeToFalse", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change IsChecked value >>")).Click();
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value is: False."));
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: True."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-changeToTrue", testWindow);

            testWindow.FindElement(GetXPathForCheckBoxWithText("RadioButton")).Click();
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value is: True."));
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: False."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ManuallyCheckFirstRadioButton", testWindow);

            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedRadioButton")).Click();
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value is: False."));
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: True."));
            //Compare snapshots 4 Manually
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ManuallyCheckSecondRadioButton", testWindow);




            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("26px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo26px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void RadioButtonLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.FindElement(GetXPathForLabelWithText("180px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void RadioButtonLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Change RadioButton Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft200Top290", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top310", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void RadioButtonSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=150"));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo150_26", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300_80", testWindow);
        }
        public void RadioButtonTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("310px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void RadioButtonWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("150px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }
        public void RadioButtonBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonBackGroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No background image."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);

            //testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage Value >>")).Click();
            ////Wait for Label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("No background image."));
            //// Compare snapshots
            //CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);

            //testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage Value >>")).Click();
            //testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));
            //CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Zoom."));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Tile."));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Stretch."));
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Center."));
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_150_26", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_150_26", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void RadioButtonClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            // Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo150_26", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.WaitForElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Text value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": TestedRadioButton"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToTestedRadioButton", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();

        }
        public void RadioButtonTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RadioButton Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".RadioButtonElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToRadioButtonElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void RadioButtonForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RadioButton ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToBlue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);
        }
        public void RadioButtonPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("26."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo26px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void RadioButtonPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RadioButtonPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RadioButton PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void RadioButtonPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RadioButton PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }

        //Bug 17644:VT - Sencha 6 framework - Can't change Font for RadioButton
        public void RadioButtonFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RadioButton Font 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
        }
        // Bug 7771 - initialized Component - "CheckAlign"property of RadioButton doesn't work
        // Bug 7770 - RunTime - "CheckAlign"property of RadioButton doesn't work
        public void RadioButtonCheckAlignFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change RadioButton Checkbox Alignment 
            testWindow.FindElement(GetXPathForButtonWithText("TopLeft")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TopLeft."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToTopLeft", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("TopCenter")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("TopCenter."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTopCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("TopRight")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("TopRight."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToTopRight", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("MiddleLeft")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("MiddleLeft."));
            //Compare snapshots 4 
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToMiddleLeft", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("MiddleCenter")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("MiddleCenter."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToMiddleCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("MiddleRight")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("MiddleRight."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToMiddleRight", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("BottomLeft")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("BottomLeft."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToBottomLeft", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("BottomCenter")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("BottomCenter."));
            //Compare snapshots 8 
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToBottomCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("BottomRight")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("BottomRight."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToBottomRight", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        //Ater Bug 22769 fix - add 'AutoSize = false' in the RadioButton in the View    
        public void RadioButtonMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=90, Height=110}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo90-110", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=130, Height=60}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo130-60", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=100, Height=70}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo100-70", testWindow);

            //Button Click - Cancel RadioButton MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }


        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        //Ater Bug 22766 fix - add 'AutoSize = false' in the RadioButton in the View 
        public void RadioButtonMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=140, Height=50}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo140-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=90, Height=80}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo90-80", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=60, Height=40}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo60-40", testWindow);

            //Button Click - Cancel RadioButton MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }

    }
}
