﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class SplitContainerTests
    {
        // Bug 21500 - leave event is invoked when the window loses focus
        // Bug 17781 - SplitContainer.Leave event doesn't work
        public void SplitContainerLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Button 1")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button 1 was clicked"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButton1InSplitContainer1", testWindow);

            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Button 2")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button 2 was clicked"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButton2InSplitContainer2", testWindow);

            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Button 1")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button 1 was clicked"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButton1InSplitContainer1", testWindow);
// check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForButtonSpanWithText("Button 2")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusButton2InSplitContainer2", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(2000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(2000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();

// end check
// check shifting focus inside the browser window
            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForButtonSpanWithText("Button 1")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusButton1InSplitContainer1", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
// end check
        }
    }
}
