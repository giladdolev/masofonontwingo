﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class ButtonElementTests
    {

        public void ButtonLostFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Button1")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button1 lost the focus"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-EnterAndTabToLeaveButton1", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Button2")).SendKeys(Keys.Shift + Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button2 lost the focus"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ShiftTabToLeaveButton2AndEnterButton1", testWindow);

            //Click Button - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Button2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button1 lost the focus"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButton2ToLeaveButton1", testWindow);

            //Click on textBox to lose focus from butto2
            testWindow.FindElement(GetXPathForMultiLineTextBoxWithText("Event Log:")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button2 lost the focus"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickLogTextBox2ToLoseFocusFromButton2", testWindow);

        }

        public void ButtonTextChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Some Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSomeText", testWindow);
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        // Bug 21414 - Calling Focus method programmatically doesn't invoke the GotFocus event
        public void ButtonGotFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Focus Button1 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button1 has"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-UseButtonToFocusButton1", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Button1")).SendKeys(Keys.Tab);
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("Button2 has"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToFocusButton2", testWindow);
            
            //Click Button - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Button3")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button3 has"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToFocusButton3", testWindow);
        }

        public void ButtonMouseTapHoldFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
           // testWindow.FindElement(GetXpathForSideMenuNode("Button")).Click();
            
            Actions actions = new Actions(CurrentWebDriver);

            //Click button1 and Hold
            actions.ClickAndHold(testWindow.FindElement(GetXPathForButtonWithText("Button1"))).Build().Perform();
          //  actions.Build().Perform();
            //Wait for a second for long click >> MouseTapHold event should be invoked without click event
            Thread.Sleep(1000);
            actions.Release((testWindow.FindElement(GetXPathForButtonWithText("Button1")))).Build().Perform();
            //Compare snapshots 1
           // actions.MoveToElement(testWindow.FindElement(GetXPathForButtonWithText("Button1"))).Release().Perform();
            Thread.Sleep(1000);
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-LongClickButton1-MouseTapHoldIsInvoked", testWindow);


            //Click button2 and Hold
            actions.ClickAndHold(testWindow.FindElement(GetXPathForButtonWithText("Button2"))).Build().Perform(); ;
            actions.Perform();
            //Wait for a second for long click >> MouseTapHold event should be invoked without click event
            Thread.Sleep(1000);
            actions.Release();
            Thread.Sleep(1000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-LongClickButton2-MouseTapHoldIsInvoked", testWindow);

            //Click button2 and Hold
            actions.ClickAndHold(testWindow.FindElement(GetXPathForButtonWithText("Button3"))).Build().Perform(); ;
            actions.Perform();
            //Wait for a second for long click >> MouseTapHold event should be invoked without click event
            Thread.Sleep(1000);
            actions.Release();
            //Compare snapshots 3
            Thread.Sleep(1000);
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-LongClickButton3-MouseTapHoldIsInvoked", testWindow);

            //Click Button2 to verify MouseTapHold event isn't invoked
            testWindow.FindElement(GetXPathForButtonWithText("Button2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button2 was clicked"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButton2-MouseTapHoldIsNotInvoked", testWindow);

        }

        public void ButtonClickFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Perform Click Button1
            testWindow.FindElement(GetXPathForButtonWithText("Perform Click Button1 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Orange"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-UseButtonToClickButton1", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Button3")).SendKeys(Keys.Enter);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Red"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-EnterKeyToClickButton3", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Button2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DeepPink"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButton2", testWindow);
        }

        // Bug 21500 - leave event is invoked when the window loses focus
        public void ButtonLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Button1")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button1 was leaved"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TabToLeaveButton1", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Button2")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button2 was leaved"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToLeaveButton2", testWindow);
            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Button1")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button3 was leaved"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToLeaveButton3", testWindow);
// check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Button1")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusButton1", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(1000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(1000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();
// end check
// check shifting focus inside the browser window
            //Click TextBox - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Button1")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusButton1", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
// end check
        }

        public void ButtonMouseHoverFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            Actions actions = new Actions(CurrentWebDriver);
            actions.MoveToElement(testWindow.FindElement(GetXPathForButtonWithText("Button1")));
            actions.Perform();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Orange"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-MouseHoverButton1", testWindow);

            actions.MoveToElement(testWindow.FindElement(GetXPathForButtonWithText("Button2")));
            actions.Perform();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DeepPink"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-MouseHoverButton2", testWindow);

            actions.MoveToElement(testWindow.FindElement(GetXPathForButtonWithText("Button1")));
            actions.Perform();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Green"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-MouseHoverButton1", testWindow);
        }

        public void ButtonResizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Size >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Size >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=150"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150_36", testWindow);
        }

        public void ButtonPressedChangeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button IsPressed
            testWindow.FindElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IsPressed = True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ManuallyChangeIsPressedToTrue", testWindow);

            //Button Click - Change Button IsPressed
            testWindow.FindElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IsPressed = False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ManuallyChangeIsPressedToFalse", testWindow);

            //Button Click - Change Button IsPressed
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change IsPressed Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IsPressed = True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ProgrammaticallyChangeIsPressedToTrue", testWindow);

            //Button Click - Change Button IsPressed
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change IsPressed Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IsPressed = False"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ProgrammaticallyChangeIsPressedToFalse", testWindow);
        }

        public void ButtonVisibleChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke VisibleChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Visible Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeVisibleToFalse", testWindow);
            //Click button - Invoke VisibleChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Visible Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeVisibleToTrue", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
    }
}
