﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class TabElementTests
    {
        public void TabSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke Tab SetBounds()
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetBoundsTo100_280_250_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetBoundsTo80_300_300_100", testWindow);
        }

        public void TabHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Tab Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void TabShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Tab Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }
        public void TabGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Tab GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".TabElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        public void TabToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //bug 21362 - VT - Tab - ToString method doesn't return the TabItems.Count and first TabItem Type
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }

        //Bug 21566 - first time invoking Deselect(param) method  the next tab is selected regardless the parameter - add test after fix
       
        public void TabDeselectTabIntFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement txtInput_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Index"));

            ////////Pass abc to the index parameter
            txtInput_elem.Click();
            txtInput_elem.SendKeys("abc");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": abc"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.FindElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Number is required"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Deselect-abc-ResultMessage", testWindow);

            ////////Pass 0 to the index parameter
            txtInput_elem.Click();
            txtInput_elem.SendKeys("0");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": 0"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.FindElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(0)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("Selected is 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Deselect-0-ResultTabItem2Index1", testWindow);

            ////////Pass 3 (out of range) to the index parameter
            txtInput_elem.Click();
            txtInput_elem.SendKeys("3");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": 3"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ArgumentOutOfRangeException was thrown"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Deselect-3-ResultException", testWindow);

            ////////Pass 2 - (not current selected) to the index parameter
            txtInput_elem.Click();
            txtInput_elem.SendKeys("2");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": 2"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(2)"));
            //testWindow.FindElement(GetXPathForLabelWithText("Selected is 0")); - uncomment and edit when 21510 is fixed
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Deselect-2-ResultTabItem2ndex1", testWindow);

            ////////Pass 1 - (currently selected) to the index parameter
            txtInput_elem.Click();
            txtInput_elem.SendKeys("1");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": 1"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(1)"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Deselect-2-ResultTabItem3ndex1", testWindow);

            ////////Pass 2 - (currently selected) to the index parameter
            txtInput_elem.Click();
            txtInput_elem.SendKeys("2");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": 2"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(2)"));
            //testWindow.FindElement(GetXPathForLabelWithText("Selected is 0")); - uncomment and edit when 21510 is fixed
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-Deselect-2-ResultTabItem1ndex0", testWindow);
            
        }

        //Bug 21566 - first time invoking Deselect(param) method  the next tab is selected regardless the parameter - add test after fix
        //Bug 21510 - deselecting the last tab doesn't select the first tab - Edit after fix


        public void TabDeselectTabStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement txtInput_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Enter ID.."));

            ////////Pass tabItem3 (Current selected) to the tabItemID parameter
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("tabItem1");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": tabItem1"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.FindElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(tabItem1)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("tab is tabItem2"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Deselect-tabItem1-ResultabItem2", testWindow);

            ////////Pass non existing tab id
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("tab");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": tab."));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ArgumentNullException was thrown"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Deselect-tab-ResultException", testWindow);

            ////////Pass last item in the tabIems collection which is also not the current selected tab item
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("tabItem3");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ID: tabItem3"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(tabItem3)"));
            //testWindow.FindElement(GetXPathForLabelWithText("tab is tabItem3")); - uncomment and edit when 215110 is fixed
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Deselect-tabItem3-tabItem3", testWindow);


        }

        //Bug 21566 - first time invoking Deselect(param) method  the next tab is selected regardless the parameter - add test after fix
        //Bug 21510 - deselecting the last tab doesn't select the first tab - Edit after fix
        public void TabDeselectTabTabItemFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement txtInput_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Enter Text"));

            ////////Pass tabItem3 (not Currently selected) to the tabItemID parameter
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("TabItem3");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": TabItem3"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(TabItem3)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("Text: TabItem2"));
            //Compare snapshots 1
            //bug id - 21566 - VT - invoking Deselect(param) method  the next tab is selected regardless the parameter
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Deselect-tabItem3-ResultabItem2", testWindow);

            ////////Pass tabItem1 (Currently selected) to the tabItemID parameter
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("TabItem1");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": TabItem1"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(TabItem1)"));
            //testWindow.WaitForElement(GetXPathForLabelWithText("Text: TabItem2"));
            //bug id - 21566 - VT - invoking Deselect(param) method  the next tab is selected regardless the parameter
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Deselect-tabItem1-ResultabItem2", testWindow);


            ////////Pass non existing tab id
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Input tab:"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.FindElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ArgumentNullException was thrown"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Deselect-emptyText-ResultException", testWindow);

            ////////Pass last item in the tabIems collection which is also not the current selected tab item
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("TabItem3");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": TabItem3"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.FindElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(TabItem3)"));
            //testWindow.FindElement(GetXPathForLabelWithText("tab is tabItem3")); - uncomment and edit when 215110 is fixed
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Deselect-tabItem3", testWindow);

            ////////Pass tabItem2 (Currently selected) to the tabItemID parameter
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("TabItem2");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": TabItem2"));
            //Button Click - Invoke DeselectTab(Int)
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("DeselectTab >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Deselect(TabItem2)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem3"));
            //bug id - 21566 - VT - invoking Deselect(param) method  the next tab is selected regardless the parameter
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Deselect-tabItem2-ResultabItem3", testWindow);


        }
    }
}
