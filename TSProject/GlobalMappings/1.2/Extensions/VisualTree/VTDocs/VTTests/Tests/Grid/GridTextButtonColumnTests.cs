﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridTextButtonColumnTests
    {
        
        public void GridGridTextButtonColumnReadOnlyFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement textButtonCellElem = testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridCol1", "c"));
            textButtonCellElem.Click();
            textButtonCellElem.SendKeys("a");

            Thread.Sleep(3000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Type-a-OnTopLeftTextButtonCell", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column1")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickSetColumnReadOnlyToSetColumn1ToTrue", testWindow);
            
            textButtonCellElem.Click();
            textButtonCellElem.SendKeys("a");

            Thread.Sleep(3000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Type-a-OnTopLeftTextButtonCell", testWindow);

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickSetColumnReadOnlyToSetColumn1ToFalse", testWindow);

            textButtonCellElem.Click();
            textButtonCellElem.SendKeys("a");

            Thread.Sleep(3000);

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Type-a-OnTopLeftTextButtonCell", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();            

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column2")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickSetColumnReadOnlyToSetColumn2ToFalse", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column3")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickSetColumnReadOnlyToSetColumn3ToTrue", testWindow);

        }
        
    }
}
