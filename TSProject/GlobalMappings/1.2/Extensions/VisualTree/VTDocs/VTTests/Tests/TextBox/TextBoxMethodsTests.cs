﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class TextBoxElementTests
    {

        public void TextBoxFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            ////Button click - to focus on TextBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Focus method was invoked."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-invokeFocus-buttonClick", testWindow);

            //Button click - Click Lose Focus button to lose focus of the MaskedTextBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Lose Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Control lost focus."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickLoseFocusBtn", testWindow);

            //The following commands are to see if the focus is back to where the cursor were the last time 
            //the textBox had focus
            //For example if the cursor was at the beginning of the  text next time we will focus the textBox, it will be in the same place 
 
            //Focus the textBox - on Specific place inside the textBox - middle of the text 
            testWindow.FindElement(GetXPathForTextBoxWithText("TestedTextBox")).Click();
            testWindow.FindElement(GetXPathForTextBoxWithText("TestedTextBox")).SendKeys(Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft);

            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ManuallyFocusingSpecificPlaceInsideTextBox-inMiddleOfText", testWindow);

            //Button click - Click Focus button 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Focus method was invoked."));
            //Bug 23976 - after fix uncomment to show the return value in log
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("Return value: True."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-FocusByButton-TheFocusInsideTextBoxIsAsInScreenshot3", testWindow);


        }

        public void TextBoxSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button to set new bounds
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetBoundsTo100_280_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetBoundsTo80_300_150_26", testWindow);
        }
        public void TextBoxResetTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Reset text
            testWindow.FindElement(GetXPathForButtonSpanWithText("ResetText >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is empty"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ResetText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Text >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Text is set"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetText", testWindow);
        }

        public void TextBoxHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke TextBox Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void TextBoxShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke TextBox Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }
        public void TextBoxGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke TextBox GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".TextBoxElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        // Bug 21302 - ToString method doesn't return the Text Value
        public void TextBoxToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }

    }
}
