﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridDateTimePickerColumnTests
    {
       
        public void GridGridDateTimePickerColumnReadOnlyFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "06/08/1974")).Click();

            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol1", "06/08/1974")).SendKeys(Keys.Backspace + "5");

            Thread.Sleep(3000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeYearOnTopLeftDateTimePickerTo1975SeeIcon", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column1")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickSetColumnReadOnlyToSetColumn1ToTrue", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "06/08/1975")).Click();

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnTopLeftDateTimePickerCellSeeNoIcon", testWindow);

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickSetColumnReadOnlyToSetColumn1ToFalse", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "06/08/1975")).Click();

            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol1", "06/08/1975")).SendKeys(Keys.Backspace + "6");

            Thread.Sleep(3000);

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ChangeYearOnTopLeftDateTimePickerTo1976SeeIcon", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column2")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickSetColumnReadOnlyToSetColumn2ToFalse", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column3")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickSetColumnReadOnlyToSetColumn3ToTrue", testWindow);

        }
 
    }
}
