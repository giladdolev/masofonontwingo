﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ListViewSubitemTests
    {

        // Bug 25587
        public void ListViewSubitemForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ListViewSubitem ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tested Subitem e ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTestedSubitem-e-ToBlue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tested Subitem e ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Red]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTestedSubitem-e-ToRed", testWindow);
        }

    }
}
