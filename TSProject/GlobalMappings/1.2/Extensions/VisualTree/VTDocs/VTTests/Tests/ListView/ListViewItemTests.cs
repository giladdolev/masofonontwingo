﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ListViewItemTests
    {

        //Bug 27716 - Checked Property isn't updated when checking\unchecking manually
        //Bug 24336 - Checked property doesn't show a checked checkbox when setting it from code
        public void ListViewItemCheckedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check first CheckBox 
            testWindow.FindElement(GetXPathForListViewCheckColumnWithIdAndCSSClass("vt-listviewItem-checked", "0")).Click();
            //Check second CheckBox 
            testWindow.FindElement(GetXPathForListViewCheckColumnWithIdAndCSSClass("vt-listviewItem-checked", "1")).Click();
            //UnCheck third CheckBox 
            testWindow.FindElement(GetXPathForListViewCheckColumnWithIdAndCSSClass("vt-listviewItem-checked", "2")).Click();
            //Bug 27716 - Checked Property isn't updated when checking\unchecking manually
            //After fix uncomment the wait for label methods and delete the sleep 
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("First Item Checked value: True"));
            //testWindow.WaitForElement(GetXPathForLabelWithText("Second Item Checked value: True"));
            //testWindow.WaitForElement(GetXPathForLabelWithText("Third Item Checked value: False"));
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-manuallyClikingAllCheckBoxes", testWindow);
            
            //Button Click - Change ListView CheckBoxes value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Second Item Checked Value >>")).Click();
            //Wait for log label update
            //Bug 27716 After fix uncomment the wait for label methods and delete the sleep
            //testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            Thread.Sleep(1000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeSecondItemChecked-False", testWindow);

            //Button Click - Change ListView CheckBoxes value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Second Item Checked Value >>")).Click();
            //Wait for log label update
            //Bug 27716 After fix uncomment the wait for label methods and delete the sleep
            //testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            Thread.Sleep(1000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-ChangeSecondItemChecked-True", testWindow);

            //UnCheck first CheckBox 
            testWindow.FindElement(GetXPathForListViewCheckColumnWithIdAndCSSClass("vt-listviewItem-checked", "0")).Click();
            //Wait for log label update
            //Bug 27716 After fix uncomment the wait for label methods and delete the sleep
            //testWindow.WaitForElement(GetXPathForLabelWithText("Second Item Checked value: False"));
            Thread.Sleep(2000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-manuallyUnCheckingFirstCheckBox-False", testWindow);

            
        }



    }
}
