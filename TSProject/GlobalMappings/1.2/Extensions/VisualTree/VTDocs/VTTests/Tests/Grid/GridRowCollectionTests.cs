﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridElementTests
    {

        public void GridGridRowCollectionRemoveAtFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement tb_elem = testWindow.FindElement(GetXPathForTextBoxWithText("..."));
            tb_elem.Clear();
            //Set a new value in the textbox
            tb_elem.SendKeys("1");
            //Verify the value was inserted
            testWindow.WaitForElement(GetXPathForLabelWithText("Inserted Value: 1..."));
            //Button Click - Set the new value as rowindex to remove
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Row >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("1 was removed."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-RemoveRowIndex1", testWindow);

        }

        public void GridGridRowCollectionAddObjectArrayFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Add a row
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add a Row >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedGrid.Rows.Add"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-addingARow", testWindow);

            //Button Click - Remove a row
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove a Row >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Row was removed"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-removingARow", testWindow);

            //Button Click - Add a row
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add a Row >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedGrid.Rows.Add"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-addingARowAfterRemove", testWindow);

        }
       
       

    }
}
