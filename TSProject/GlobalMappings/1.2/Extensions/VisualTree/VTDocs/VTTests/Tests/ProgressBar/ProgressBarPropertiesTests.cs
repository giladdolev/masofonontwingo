﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class ProgressBarElementTests
    {

        public void ProgressBarHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("40px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ProgressBarLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ProgressBarLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Change ProgressBar Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=120"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft120Top280", testWindow);
            

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top290", testWindow);

            
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        
        public void ProgressBarSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=250"));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo250_40", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300_80", testWindow);
        }
        public void ProgressBarTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("310px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void ProgressBarWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("500px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo500px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("250px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo250px", testWindow);
        }
        public void ProgressBarBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ProgressBarBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_250_40", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ProgressBarClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_250_40", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ProgressBarClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("250,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo250_40", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ProgressBarVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ProgressBarTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ProgressBar Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".ProgressBarElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToProgressBarElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ProgressBarPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("40."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ProgressBarPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ProgressBarPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ProgressBar PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void ProgressBarPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ProgressBar PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("500."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo500px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("250."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo250px", testWindow);
        }       

        //Bug 17649:VT - ProgressBar.Min property does not work until we change the Value property
        public void ProgressBarMinFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Value to test the ProgressBar reflact
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 60"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeValueTo60", testWindow);

            //Button Click - Change Min value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Min value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Min value = 0"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeMinTo0", testWindow);

            //Button Click - Change Value to test the ProgressBar reflact
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 20"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeValueTo20", testWindow);
        }

        //Bug 9581:VTDocs ProgressBar.Max property does not work until we change the Value property
        public void ProgressBarMaxFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Value to test the ProgressBar reflact
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 50"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeValueTo50", testWindow);

            //Button Click - Change Max value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Max value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Max value = 100"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeMinTo100", testWindow);

            //Button Click - Change Value to test the ProgressBar reflact
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 100"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeValueTo100", testWindow);
        }

        //Bug 21341:VT - ProgressBar.Value property does not work correctly when value is less than 'Min' or greater than 'Max'.
        public void ProgressBarValueFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement tb_elem = testWindow.FindElement(GetXPathForTextBoxWithText("..."));
            tb_elem.Clear();
            //Set a new value in the textbox
            tb_elem.SendKeys("10");
            //Verify the value was inserted
            testWindow.WaitForElement(GetXPathForLabelWithText("Inserted Value - 10..."));
            //Button Click - Set the new value as progressbar.value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 10"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetValueTo10", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("90");
            testWindow.WaitForElement(GetXPathForLabelWithText("Inserted Value - 90..."));
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 90"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetValueTo90", testWindow);

            tb_elem.Clear();
            //Set a value that is less than 'Min'
            tb_elem.SendKeys("3");
            testWindow.WaitForElement(GetXPathForLabelWithText("Inserted Value - 3..."));
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 3"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SetValueTo3", testWindow);

            tb_elem.Clear();
            //Set a value that is greater than 'Max'
            tb_elem.SendKeys("97");
            testWindow.WaitForElement(GetXPathForLabelWithText("Inserted Value - 97..."));
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value property = 97"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetValueTo97", testWindow);
        }

        public void ProgressBarEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-1", "false"));
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-2", "true"));

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if ProgressBar is enabled 
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-2", "false")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueSecondProgressBar", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Check if ProgressBar is disabled
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-2", "true"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondProgressBar", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void ProgressBarMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=150, Height=90}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo150-90", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=230, Height=50}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo230-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=180, Height=70}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo180-70", testWindow);

            //Button Click - Cancel ProgressBar MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }


        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void ProgressBarMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=250, Height=50}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo250-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=100}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200-100", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=160, Height=90}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo160-90", testWindow);

            //Button Click - Cancel ProgressBar MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }
        public void ProgressBarForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change ForeColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToBlue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

    }
}
