﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ComboBoxElementTests
    {

        public void ComboBoxResetTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
           //Click button to Reset ComboBox Text
            testWindow.FindElement(GetXPathForButtonWithText("ResetText >>")).Click();
            //Wait for label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is empty"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ResetText", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Set Text >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("is set"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetText", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("ResetText >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("is empty"));
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ResetText", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ComboBoxSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Change ComboBox Bounds
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            //Compare snapshots 1 
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetBoundsTo100_280_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetBoundsTo80_300_165_25", testWindow);
        }

        public void ComboBoxGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke ComboBox GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".ComboBoxElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }

        public void ComboBoxHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke ComboBox Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void ComboBoxShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke ComboBox Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }

        public void ComboBoxFindStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement tb_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Text Input"));
            tb_elem.Clear();
            //Button Click - Invoke ComboBox FindString() on an empty input value
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 0"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Find-EmptyString-Result0", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("*Item");
            testWindow.WaitForElement(GetXPathForLabelWithText("*Item"));
            //Button Click - Invoke ComboBox FindString() on an "*Item" input value - SubString
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Find-asteriskItem-Result1", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("Item 3");
            testWindow.WaitForElement(GetXPathForLabelWithText("Item 3"));
            //Button Click - Invoke ComboBox FindString() on an "Item 3" input value - full string
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 2"));
            //Compare snapshots 3 
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Find-ItemSpace3-Result2", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("otem");
            testWindow.WaitForElement(GetXPathForLabelWithText("otem"));
            //Button Click - Invoke ComboBox FindString() on an "otem" input value - WrongString
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: -1"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Find-otem-Result-1", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("*IteM");
            testWindow.WaitForElement(GetXPathForLabelWithText("*IteM"));
            //Button Click - Invoke ComboBox FindString() on an "*IteM" input value - CaseSensitive, subString
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 1"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Find-asteriskIteM-Result1", testWindow);
        }

        // Bug 21216 - The search performed by the FindStringExact method is case-sensitive, and causes the search to get stuck
        public void ComboBoxFindStringExactFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement tb_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Text Input"));
            tb_elem.Clear();
            //Button Click - Invoke ComboBox FindStringExact() on an empty input value
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindStringExact >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 0"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Find-EmptyString-Result0", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("*Item");
            testWindow.WaitForElement(GetXPathForLabelWithText("*Item"));
            //Button Click - Invoke ComboBox FindStringExact() on an "*Item" input value - substring
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindStringExact >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: -1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Find-asteriskItem-Result-1", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("Item 3");
            testWindow.WaitForElement(GetXPathForLabelWithText("Item 3"));
            //Button Click - Invoke ComboBox FindStringExact() on an "Item 3" input value - FullStringResult
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindStringExact >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 3"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Find-ItemSpace3-Result3", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("Iyem 3");
            testWindow.WaitForElement(GetXPathForLabelWithText("Iyem 3"));
            //Button Click - Invoke ComboBox FindStringExact() on an "Iyem 3" input value - WrongString
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindStringExact >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: -1"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Find-IyemSpace3-Result-1", testWindow);

            tb_elem.Clear();
            tb_elem.SendKeys("*iteM2");
            testWindow.WaitForElement(GetXPathForLabelWithText("*iteM2"));
            //Button Click - Invoke ComboBox FindStringExact() on an "*iteM2" input value - CaseSensitive, FullString
            testWindow.FindElement(GetXPathForButtonSpanWithText("FindStringExact >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The result"));
            // Bug 21216 - when fixed replace with this line
            //testWindow.FindElement(GetXPathForLabelWithText("is: 2"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Find-asteriskiteM2-Result2", testWindow);

        }
        // Bug 21292 - ToString method doesn't return the Items.count value
        public void ComboBoxToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }

        //Bug 23976 - The type of comboBox.Focus() method is void
        public void ComboBoxFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Choose an item from the ComboBox
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboBox")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxItem("Item2")).Click();
            //Button click - Click Focus button to receive focus on the comboBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Focus method was invoked."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectItem2AndClickFocusBtn", testWindow);

            //Button click - Click Lose Focus button to lose focus of the comboBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Lose Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Control lost focus."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickLoseFocusBtn", testWindow);

            //Button click - Click Focus button to recieve focus on the comboBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Focus method was invoked."));

            //Bug 23976 - after fix, uncomment the below line and delete the  Thread.Sleep line
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("Return value: True."));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickFocusBtn", testWindow);          
        }
    }
}
