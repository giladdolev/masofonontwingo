﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class CheckBoxElementTests
    {


        public void CheckBoxArrayBasicFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click -  checked all CheckBoxes
            testWindow.FindElement(GetXPathForButtonWithText("Check All >>")).Click();
            //Wait for label update
            testWindow.WaitForElement(GetXPathForLabelWithText("All CheckBoxes were checked."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-bottunClick-CheckAllCheckBoxes", testWindow);

            //Button Click -  unchecked all CheckBoxes
            testWindow.FindElement(GetXPathForButtonWithText("UnCheck All >>")).Click();
            //Wait for label update
            testWindow.WaitForElement(GetXPathForLabelWithText("All CheckBoxes were unchecked."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-bottunClick-UnCheckAllCheckBoxes", testWindow);

        }
        public void CheckBoxApplicationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            IWebElement btnActionSpan = testWindow.FindElement(GetXPathForButtonSpanWithText("Action"));
            testWindow.FindElement(GetXPathForButtonWithText("Action")).Click();


            IWebElement txtLogDiv = testWindow.FindElement(GetXPathForTextBoxDivWithText("Log"));

            // In current VisualTree implementation content of button would be replaced
            // So let's wait till current span exist in document
            
            //txtLogDiv.WaitWhileAttachedToPage();

            Thread.Sleep(2000);

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "-after-Action-click", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void CheckBoxAutoCheckFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check TestedCheckBox1
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox")).Click();
            //Get TestedCheckBox2
            IWebElement TestedCheckBox = testWindow.WaitForElement(GetXPathForCheckBoxWithText("TestedCheckBox"));
            //Check TestedCheckBox2
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for CheckBox check
            bool isChecked = TestedCheckBox.Selected;
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-check-CheckBoxes", testWindow);


            //Click button to change AutoCheck value
            testWindow.FindElement(GetXPathForButtonWithText("Change AutoCheck value >>")).Click();
            //Wait for label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check TestedCheckBox2
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for CheckBox check
            isChecked = TestedCheckBox.Selected;
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTrueAndcheck", testWindow);


            testWindow.FindElement(GetXPathForButtonWithText("Change AutoCheck value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            isChecked = TestedCheckBox.Selected;
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFalseAndcheck", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

       
        public void CheckBoxEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForEnabledCheckBoxWithText("CheckBox", "false")).Click();
            testWindow.FindElement(GetXPathForEnabledCheckBoxWithText("TestedCheckBox", "true"));

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if check Box is enabled by clicking it
            testWindow.FindElement(GetXPathForEnabledCheckBoxWithText("TestedCheckBox", "false")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueSecondChk", testWindow);
            

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Check if checkBox is disabled
            testWindow.FindElement(GetXPathForEnabledCheckBoxWithText("TestedCheckBox", "true"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondChk", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("26px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo26px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }

        public void CheckBoxLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);
            

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }

        public void CheckBoxLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=150"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo150_300", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80_330", testWindow);
        }
        public void CheckBoxSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Size value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=150"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo150_26", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300_80", testWindow);
        }

        public void CheckBoxTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Top value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("270px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo270px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }

        public void CheckBoxVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);
        }

        public void CheckBoxWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }
        public void CheckBoxBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No background image."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);
           
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackgroundImage value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);

            //testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage Value >>")).Click();
            ////Wait for Label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("No background image."));
            //// Compare snapshots
            //CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);

            //testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage Value >>")).Click();
            //testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));
            //CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox BackgroundImageLayout 
            testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Zoom."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Tile."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Stretch."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Center."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox Bounds 
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_150_26", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox ClientRectangle 
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_150_26", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void CheckBoxClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox ClientSize
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo150_26", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox Text
            testWindow.FindElement(GetXPathForButtonWithText("Change Text Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Text Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: TestedCheckBox"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToTestedCheckBox", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".CheckBoxElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToCheckBoxElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void CheckBoxForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToBlue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);
        }
        public void CheckBoxPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("26."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo26px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void CheckBoxPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void CheckBoxPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("270."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo270px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void CheckBoxPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }

        //Bug 17600 - Can't change Font for CheckBox and RadioButton
        public void CheckBoxFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox Font 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void CheckBoxMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=120, Height=60}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo120_60", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=170, Height=40}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo170_40", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=190, Height=80}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo190_80", testWindow);

            //Button Click - Cancel CheckBox MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void CheckBoxMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=350, Height=60}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo350_60", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=280, Height=90}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo280_90", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=50}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo200_50", testWindow);

            //Button Click - Cancel CheckBox MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }

        public void CheckBoxIsCheckedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Toggle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Toggle TestedCheckBox1 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedCheckBox1 IsChecked value: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickBtnToToggle-TestedCheckBox1-ToTrue", testWindow);

            //Button Click - Toggle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Toggle TestedCheckBox >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedCheckBox IsChecked value: False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickBtnToToggle-TestedCheckBox-ToFalse", testWindow);

            //Button Click - Toggle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Toggle TestedCheckBox2 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedCheckBox2 IsChecked value: False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickBtnToToggle-TestedCheckBox2-ToFalse", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedCheckBox IsChecked value: True"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-CheckManually-TestedCheckBox-ToTrue", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedCheckBox2 IsChecked value: True"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-CheckManually-TestedCheckBox2-ToTrue", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox1")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedCheckBox1 IsChecked value: False"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-CheckManually-TestedCheckBox1-ToFalse", testWindow);
        }

        //Bug 27451 - delete Sleep and uncomment all GetXPathForLabelWithText after bug fix
        public void CheckBoxThreeStateFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Checked"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-CheckManually-TestedCheckBox1-ToChecked", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Unchecked"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-CheckManually-TestedCheckBox1-ToUnchecked", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Checked."));
            Thread.Sleep(1000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-CheckManually-TestedCheckBox2-ToChecked", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Indeterminate."));
            Thread.Sleep(1000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-CheckManually-TestedCheckBox2-ToIndeterminate", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Unchecked."));
            Thread.Sleep(1000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-CheckManually-TestedCheckBox2-ToUnchecked", testWindow);

            //Button Click - Toggle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ThreeState Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ThreeState value: False."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickBtnToChangeThreeState-TestedCheckBox2-ToFalse", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Checked."));
            Thread.Sleep(1000);
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-CheckManually-TestedCheckBox2-ToChecked", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Unchecked."));
            Thread.Sleep(1000);
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-CheckManually-TestedCheckBox2-ToUnchecked", testWindow);

            //Button Click - Toggle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ThreeState Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ThreeState value: True."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickBtnToChangeThreeState-TestedCheckBox2-ToTrue", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Checked."));
            Thread.Sleep(1000);
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-CheckManually-TestedCheckBox2-ToChecked", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Indeterminate."));
            Thread.Sleep(1000);
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-CheckManually-TestedCheckBox2-ToIndeterminate", testWindow);

            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("TestedCheckBox")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("CheckState value: Unchecked."));
            Thread.Sleep(1000);
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-CheckManually-TestedCheckBox2-ToUnchecked", testWindow);
        }
    }
}
