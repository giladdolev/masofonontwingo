﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class PanelElementTests
    {

        public void PanelSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Button click - Change Panel Size 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToWidth300Height40", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToWidth200Height80", testWindow);
            
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void PanelBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change Panel BackColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToOrange", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void PanelBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change Panel BackgroundImage 
            testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No background image"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);


            testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("with background image."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);


        }

        public void PanelBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change Panel BackgroundImageLayout 
            testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Zoom."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Tile."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Stretch."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Center."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);
        }

         public void PanelBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

         public void PanelBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Bounds value
             testWindow.FindElement(GetXPathForButtonWithText("Change Panel Bounds >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_250_50", testWindow);


             testWindow.FindElement(GetXPathForButtonWithText("Change Panel Bounds >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
             // Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_360_200_80", testWindow);


         }

         public void PanelClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ClentSize value
             testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
             //Wait for log Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("150,"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo150_36", testWindow);


             testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo200_80", testWindow);
         }

         public void PanelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Height value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("40px"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("80px"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
         }

         public void PanelClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change ClientRectangle value
             testWindow.FindElement(GetXPathForButtonWithText("Change Panel Bounds >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("X=120,"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo120_330_300_50", testWindow);


             testWindow.FindElement(GetXPathForButtonWithText("Change Panel Bounds >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_350_200_80", testWindow);


         }

         public void PanelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Left value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
             //Wait for log  label update
             testWindow.WaitForElement(GetXPathForLabelWithText("200px"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("80px"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
         }

         public void PanelLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Location value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Panel Location >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
             //Compare snapshots 1 
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_300", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Panel Location >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80_325", testWindow);
         }

         public void PanelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Top value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText(": 285px"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": 310px"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
         }

         public void PanelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Width value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText(": 300px."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": 200px."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200px", testWindow);
         }

         public void PanelVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Panel Visible value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("True."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("False."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void PanelTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Panel Tag value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText(".PanelElement"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToPanelElement", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void PanelPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change PixelHeight value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight Value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("is: 40"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo40px", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("is: 80"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
         }
         public void PanelPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change PixelLeft Location 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("is: 200"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("is: 80"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }
         public void PanelPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Panel PixelTop 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Value: 285"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Value: 310"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
         }
         public void PanelPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Panel PixelWidth 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("300."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("200."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200px", testWindow);
         }

         //Bug 17712 - Panel.Font doesn't work.
         public void PanelFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change Panel Font 
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             //Wait for Label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
         }

         //Bug 17713:VT - Panel.ForeColor property doesn't work
         public void PanelForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {

             //Button Click - Change ForeColor value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToBlue", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);


             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }

         public void PanelEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Check initialize settings
             testWindow.FindElement(GetXPathForDisabledPanelWithCssClass("vt-test-pnl-1", "false"));
             testWindow.FindElement(GetXPathForDisabledPanelWithCssClass("vt-test-pnl-2", "true"));

             //Button Click - Change Enabled value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("True."));
             //Check if combo Box is enabled 
             testWindow.FindElement(GetXPathForDisabledPanelWithCssClass("vt-test-pnl-2", "false")).Click();
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueSecondPnlAndClickInsidePanel", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("False."));
             //Check if combo is disabled
             testWindow.FindElement(GetXPathForDisabledPanelWithCssClass("vt-test-pnl-2", "true"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondPnl", testWindow);

             testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
         }

         //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
         public void PanelMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button click - Change MaximumSize
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=190, Height=70}"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo190-70", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=210, Height=40}"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo210-40", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=230, Height=60}"));
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo230-60", testWindow);

             //Button Click - Cancel Panel MaximumSize, set it to (0, 0). Size is equal to his last size
             testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
         }


         //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
         public void PanelMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button click - Change MinimumSize
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=360, Height=80}"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo360-80", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=100}"));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200-100", testWindow);

             testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=250, Height=50}"));
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo250-50", testWindow);

             //Button Click - Cancel Panel MinimumSize, set it to (0, 0). Size is equal to his last size
             testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
         }

         // bug 9462 - ContextMenuStrip can't be set at runtime
         public void PanelContextMenuStripFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             Actions actions = new Actions(CurrentWebDriver);
             bool exampleSize = true;

             IWebElement TestedPanel1_elem = testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-test-pnl-1"));
             //Right click TestedButton1
             actions.ContextClick(TestedPanel1_elem);
             actions.Perform();
             //Wait for contextMenu to open
             Thread.Sleep(3000);
             //Compare snapshots 1
             CheckSnapshotExtended(membersCategory, memberName, exampleId + "1-after-rightClickTestedPanel1-DefaultBrowserMenu", testWindow, exampleSize);

             // click on body
             testWindow.FindElement(By.Id("windowView-body")).Click();

             IWebElement TestedPanel2_elem = testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-test-pnl-2"));
             //Right click TestedButton2
             actions.ContextClick(TestedPanel2_elem);
             actions.Perform();
             //Wait for contextMenu to open
             Thread.Sleep(2000);
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-rightClickTestedPanel2-HelloMenuItem", testWindow);

             //Button click - to change contextMenuStrip
             testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip2")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": contextMenuStrip2"));
             //Right click TestedButton2
             actions.ContextClick(TestedPanel2_elem);
             actions.Perform();
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToContextMenuStrip2AndRightClickTestedPanel-HelloWorldMenuItem", testWindow);

             //Button click - to change contextMenuStrip
             testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip1")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": contextMenuStrip1"));
             //Right click TestedButton2
             actions.ContextClick(TestedPanel2_elem);
             actions.Perform();
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToContextMenuStrip1AndRightClickTestedPanel-HelloMenuItem", testWindow);

             //Button click - to Reset contextMenuStrip
             testWindow.FindElement(GetXPathForButtonSpanWithText("Reset")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value is null."));
             //Right click TestedButton2
             actions.ContextClick(TestedPanel2_elem);
             actions.Perform();
             //Compare snapshots 5
             CheckSnapshotExtended(membersCategory, memberName, exampleId + "5-after-clickResetButtonAndRightClickTestedPanel-DefaultBrowserMenu", testWindow, exampleSize);

             // click on body
             testWindow.FindElement(By.Id("windowView-body")).Click();
         }
    }
}
