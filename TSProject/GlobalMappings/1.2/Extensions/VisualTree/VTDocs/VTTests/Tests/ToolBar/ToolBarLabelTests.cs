﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ToolBarLabelTests
    {
        public void ToolBarToolBarLabelEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            //testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-1-new", "false"));
            //testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-1-open", "false"));
            //testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-new", "true"));
            //testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-open", "true"));

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("Enabled")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            //testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-new", "false"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickChangeEnabledValueToEnableNewTbLabel", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            //testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-open", "false"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickChangeEnabledValueToEnableOpenTbLabel", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("Enabled")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            //testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-new", "true"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickChangeEnabledValueToDisableNewTbLabel", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();

            //testWindow.WaitForElement(GetXPathForDisabledControlWithCssClass("vt-test-tbbutton-2-open", "true"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickChangeEnabledValueToDisableOpenTbLabel", testWindow);
        }

        public void ToolBarToolBarLabelVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();
            //Check CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("Visible")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("New Label Visible value: True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectItemNew-CheckVisibleChk-ClickChangeVisibleValuebtn-ToShowNewTbLabel", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Open Label Visible value: True."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectItemOpen-ClickChangeVisibleValuebtn-ToShowOpenTbLabel", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("New")).Click();
            //UnCheck CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("Visible")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("New Label Visible value: False."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectItemNew-UnCheckVisibleChk-ClickChangeVisibleValuebtn-ToHideNewTbLabel", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectItem")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("Open")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible Value >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Open Label Visible value: False."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SelectItemOpen-ClickChangeVisibleValuebtn-ToHideOpenTbLabel", testWindow);
        }

 
    } 
}
