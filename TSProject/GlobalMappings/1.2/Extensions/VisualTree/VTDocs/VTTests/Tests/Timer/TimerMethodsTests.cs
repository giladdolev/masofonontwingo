﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class TimerElementTests
    {
        
        public void TimerGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Timer GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".TimerElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        public void TimerStartFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Invoke Start method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Start >>")).Click();
            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-StartAndWait5Seconds", testWindow);


            //Button Click - Invoke Stop method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Stop >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("stoped,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Stop", testWindow);


            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-5seconds", testWindow);


            //Button Click - Invoke Start method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Start >>")).Click();
            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-StartAgainAndWait5Seconds", testWindow);

            //Close window
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TimerStopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Wait 5 seconds
            Thread.Sleep(5000);
            //Button Click - Invoke Stop method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Stop >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("stoped,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Wait5SecondsAndStop", testWindow);

            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-5seconds", testWindow);

            //Button Click - Invoke Start method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Start >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("started,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Start", testWindow);

            //Wait 5 seconds
            Thread.Sleep(5000);
            //Button Click - Invoke Stop method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Stop >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("stoped,"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Wait5SecondsAndStopAgain", testWindow);

            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-5seconds", testWindow);

            //Close window
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void TimerToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }

    }
}
