﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class ListViewElementTests
    {
        //Task 9790:VT - implement ListViewItemSelectionChangedEvent
        //Bug 21385:VT - ListView items[index].Selected always return false and selectedIndexes return 0 and SelectedITems return 0.
        public void ListViewSelectionChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click Item1
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-TestedListView", "Item1")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Item1 is selected"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectingItem1", testWindow);

            //Multiple selection
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-TestedListView", "Item1")).SendKeys(Keys.Shift + Keys.ArrowDown);
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Item2 is selected"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectingItem1AndItem2", testWindow);

            //Click button - Select Item3
            testWindow.FindElement(GetXPathForButtonWithText("Select Item3 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Item3 is selected"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectingItem3ByButton", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        // Bug 21500 - leave event is invoked when the window loses focus
        // Bug 23286 - ListView.Leave event doesn't work
        public void ListViewLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click a
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-TestedListView-1", "a")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ListView1-L1 is selected"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Click-a-CellInListView1Line1", testWindow);

            //Click e
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-TestedListView-2", "e")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ListView2-L1 is selected"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Click-e-CellInListView2Line1", testWindow);

            //Click c
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-TestedListView-1", "c")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ListView1-L2 is selected"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Click-c-CellInListView1Line1", testWindow);

// check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click c
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-TestedListView-1", "c")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocus-c-CellInListView1Line1", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(2000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(2000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();

// end check
// check shifting focus inside the browser window

            //Click c
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-TestedListView-1", "c")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocus-c-CellInListView1Line1", testWindow);
            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
// end check
        }

    }
}
