﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class MaskedTextBoxElementTests
    {

        public void MaskedTextBoxSelectOnFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            ////Button click - to focus TestedMaskedTextBox1
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus TestedMaskedTextBox1 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedMaskedTextBox1 Focus method was invoked"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-invokeTestedMaskedTextBox1Focus", testWindow);

            ////Button click - to focus MaskedTextBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus TestedMaskedTextBox2 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedMaskedTextBox2 Focus method was invoked"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-invokeTestedMaskedTextBox2Focus", testWindow);

            //Click TestedMaskedTextBox1 - for focus
            testWindow.FindElement(GetXPathForTextBoxWithText("TestedMaskedTextBox1")).Click();
            //Wait before screenShot , for the TestedMaskedTextBox1 Click
            Thread.Sleep(1000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ManuallyFocusingTestedMaskedTextBox1", testWindow);

            //Click TestedMaskedTextBox1 - for focus
            testWindow.FindElement(GetXPathForTextBoxWithText("TestedMaskedTextBox2")).Click();
            //Wait before screenShot , for the TestedMaskedTextBox2 Click
            Thread.Sleep(1000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ManuallyFocusingTestedMaskedTextBox2", testWindow);


        }

        public void BasicMaskedTextBoxFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).Click();
            // writing 'abc' where number is required - the output will be only abc111222
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.Up + "abcabc111222");

            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk2")).Click();
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk2")).SendKeys(Keys.Up + "abcabc111222");

            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk3")).Click();
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk3")).SendKeys(Keys.Up + "abcabc111222");

            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk4")).Click();
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk4")).SendKeys(Keys.Up + "abcabc111222");

           
            Thread.Sleep(3000);
            
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-PopulatingMaskedTextBoxes", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void MaskedTextBoxMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=120, Height=60}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo120-60", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=160, Height=20}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo160-20", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=190, Height=30}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo190-30", testWindow);

            //Button Click - Cancel MaskedTextBox MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void MaskedTextBoxMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=30}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200-30", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=130, Height=50}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo130-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=160, Height=40}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo160-40", testWindow);

            //Button Click - Cancel MaskedTextBox MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }


        //Bug 22753 - retrieving Text wrong
        //22912 -  Mask is gone when setting text through code
        public void MaskedTextBoxTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clear Text
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace + Keys.Backspace + Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-BackspacesToClearText", testWindow);


            //Scenario for entering text in the beginning or middle of the text to see that it pushes the rest of the text 
           
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys("abcd");
            //Move the cursor to the beginning of the text
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.Left + Keys.Left + Keys.Left + Keys.Left + Keys.Left);
            //Enter a character at the beginning of the text
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys("f");
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-enteringTextMovingToTheBeginningOfTheTextAndEntering'f'-'fab-cd_'", testWindow);


            //Scenario: When deleting text, by Backspace key, it should "pull" back the text, by entering blank characters at the end

            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.Right + Keys.Right + Keys.Backspace + Keys.Backspace);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-DeletingCharacters'ab'ByBackspace-'fcd-___'", testWindow);


            //Scenario: When deleting text, by Delete key, it should "pull" back the text, by entering blank characters at the end

            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.Delete + Keys.Delete);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-pressingTheDeleteKeyTwice-'f__-___'", testWindow);


            //Scenario: When the text is marked, the marked text should be deleted and new text should be written instead.
            
            //Enter more text
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys("abcd");
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.Shift + (Keys.Left + Keys.Left + Keys.Left + Keys.Left + Keys.Left) + "g");
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Entering'abcd'-selecting'abcd'andPressing'g'-'fg_-___'", testWindow);

            //Scenario: When the text is marked, the marked text should be deleted and new text should be written instead.

            //Fill all with text
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys("abcd");
            //Select all text and Delete
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.Control + "a");
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys("a");
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-FillMaskWithTextSelectAllAndEnter'a'-'a__-___'", testWindow);


            //Change Text programmatically

            //Enter text in textBox
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-txt1")).SendKeys("abcde");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("textBox: abcde"));
            //Button click - To Change MaskedTextBox Text with user input
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("Text value: abc-de"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-Entering'abcde'InTextBoxAndClickingTheButton-'abc-de", testWindow);


            //Add text to textBox
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-txt1")).SendKeys("fghijk");
            testWindow.WaitForElement(GetXPathForLabelWithText("textBox: abcdefghijk"));
            //Button click - To Change MaskedTextBox Text with user input
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text")).Click();
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("Text value: abc-def"));
            //Wait for log label update
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-Adding'fghijk'InTextBoxAndClickingTheButton-'abc-def'", testWindow);
        }


        public void MaskedTextBoxPromptCharFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button to change PromptChar  - we can see the change in log and also in the maskedTextBoxes
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PromptChar for all >>")).Click();
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("PromptChar value: *,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClickToChangePromptCharToAsterisk", testWindow);

            //Click button to change PromptChar again  - we can see the change in log and also in the maskedTextBoxes
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PromptChar for all >>")).Click();
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("PromptChar value: %,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClickToChangePromptCharTo%", testWindow);

            //Move the cursor to the left and press '5' in Mask1 - this step tests bug 25064
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).Click();
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk1")).SendKeys(Keys.ArrowLeft + "5");
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("Text value: 5"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Press5InMask1", testWindow);

            //Move the cursor to the left and press '56' in Mask2
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk2")).Click();
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk2")).SendKeys(Keys.ArrowLeft + Keys.ArrowLeft + "56");
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("Text value: 56"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Press56InMask2", testWindow);

            //Move the cursor to the left and press '56ef' in Mask3
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk3")).Click();
            testWindow.WaitForElement(GetXPathForControlWithCssClass("vt-Msk3")).SendKeys(Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + "56ef");
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("Text value: 56ef"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Press56efInMask3", testWindow);


            //Click button to change Mask1 Text to 1 - this step tests bug 25064
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Mask1 Text >>")).Click();
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("Text value: 1"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-buttonClickToSetMask1TextTo1", testWindow);

            //Click button to change Mask1 Text to 12 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Mask2 Text >>")).Click();
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("Text value: 12"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-buttonClickToSetMask2TextTo12", testWindow);

            //Click button to change Mask1 Text to 12ab 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Mask3 Text >>")).Click();
            //Wait for the change in log label 
            testWindow.WaitForElement(GetXPathForLabelWithText("Text value: 12ab"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-buttonClickToSetMask3TextTo12ab", testWindow);
            
            
        }
    }
}
