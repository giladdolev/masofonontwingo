﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class CheckedListBoxElementTests
    {

      

        //Task 17553 - after fix add a button (or buttons) that changes checked programatically  to the example and
        //test this button in this automation test 
        public void CheckedListBoxItemCheckFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Check CheckBox2
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 2")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckBox2 is Checked"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-checkingCheckBox2", testWindow);

            //UnCheck CheckBox2
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 2")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckBox2 is Unchecked"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-unCheckingCheckBox2", testWindow);

            // Add here clicking on button (buttons)
        }

    
    }
}
