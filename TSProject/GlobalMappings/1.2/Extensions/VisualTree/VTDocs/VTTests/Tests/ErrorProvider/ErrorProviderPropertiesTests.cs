﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ErrorProviderElementTests
    {
        public void ErrorProviderBasicFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click Login button
            testWindow.FindElement(GetXPathForButtonWithText("Login")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Username can not be empty.."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickLoginwithEmptyFields", testWindow);

            //Hover the username error icon to see the error tool-tip
            //vt-txt-username
            IWebElement elem = testWindow.FindElement(GetXpathForControlsWithCssClassAndClass("vt-txt-username", "x-form-error-msg"));
            Actions actions = new Actions(CurrentWebDriver);
            actions.MoveToElement(elem);
            Thread.Sleep(2000);
            actions.Perform();
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-MouseHoverUsernameErrorIcon", testWindow);

            //Enter a username
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-username")).SendKeys("xxx");
            //Leave username textbox
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-password")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Entered Username:  xxx"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-EnterUsernameAndLeave", testWindow);

            //Delete username
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-username")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            testWindow.WaitForElement(GetXPathForTextBoxWithCssClass("vt-txt-username"));
            //Leave username textbox
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-txt-password")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Username can not be empty.."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-DeleteUsernameAndLeave", testWindow);
        

        //Hover the address error icon to see the error tool-tip
            //vt-txt-username
            IWebElement elem2 = testWindow.FindElement(GetXpathForControlsWithCssClassAndClass("vt-rich-address", "x-form-error-msg"));
            //Actions actions = new Actions(CurrentWebDriver);
            actions.MoveToElement(elem2);
            Thread.Sleep(2000);
            actions.Perform();
            Thread.Sleep(2000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-MouseHoverRichTextBoxErrorIcon", testWindow);
        

        //Enter text in RichTextBox and press Login button
            IWebElement elem3 = testWindow.WaitForElement(GetXPathForRichTextBoxWithText());
            actions.MoveToElement(elem3);
            Thread.Sleep(2000);
            actions.Click().SendKeys("ABcd").Perform();

            Thread.Sleep(2000);

            //Click Login button
            testWindow.WaitForElement(GetXPathForButtonWithText("Login")).Click();

            Thread.Sleep(2000);
            
            //Wait for Label update
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("Entered Username:  xxx"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-EnterTextInRichTextBoxAndPressLogin", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();

        }
    }
}
