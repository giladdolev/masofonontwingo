﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class RadioButtonElementTests
    {
        //Bug 23888  Clicking on RadioButton several times - each time the LostFocus event is invoked
        public void RadioButtonLostFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForRadioButtonWithText("RadioButton1")).Click();
            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("RadioButton1")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton1 lost the focus"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-EnterAndPressTabToLeaveRadioButton1", testWindow);


            testWindow.FindElement(GetXPathForRadioButtonWithText("RadioButton2")).Click();
            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("RadioButton2")).SendKeys(Keys.Up);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton2 lost the focus"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickRadiobutton2AndKeyUpToLeaveRadioButton2AndEnterRadioButton1", testWindow);

            //Click RadioButton - Change Focus
            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("RadioButton2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton1 lost the focus"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickRadioButton2ToLoseFocusFromRadioButton1", testWindow);

            //Click on textBox to lose focus from RadioButto2
            testWindow.FindElement(GetXPathForMultiLineTextBoxWithText("Event Log:")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton2 lost the focus"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickLogTextBox2ToLoseFocusFromRadioButton2", testWindow);

            //Click twice on the RadioButton - to reproduce bug 23888 
            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("RadioButton1")).Click();
            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("RadioButton1")).Click();
            Thread.Sleep(2000);
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickTwiceRadiobutton1-RadioButton1LostFocusLineShouldNotBeAddedToLog", testWindow);

        }


        public void RadioButtonTextChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Some Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSomeText", testWindow);
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 21312 - setting (from code) radio button to 'true', the checkedChanged event is invoked for the checked radio button first
        public void RadioButtonCheckedChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            ////Manually checking radiobuton1 and radiobutton2

            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForCheckBoxWithText("RadioButton1")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton1 IsChecked value is: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-CheckingRb1Manully", testWindow);


            //Click button - Click RadioButton2
            testWindow.FindElement(GetXPathForCheckBoxWithText("RadioButton2")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton2 IsChecked value is: True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-CheckingRb2Manully", testWindow);

            
            ////Checking radiobutton1 and radiobutton2 through code - through IsChecked property 

            //Click button - Change RadioButton1 IsChecked property value  
            testWindow.FindElement(GetXPathForButtonWithText("Toggle Choice >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton1 IsChecked value is: True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeRb1IsCheckedTotrue", testWindow);


            //Click button - Change RadioButton1 IsChecked property value again 
            testWindow.FindElement(GetXPathForButtonWithText("Toggle Choice >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("RadioButton2 IsChecked value is: True"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeRb2IsCheckedTotrue", testWindow);            
        }

        public void RadioButtonGotFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForCheckBoxWithText("Female")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Female RadioButton option"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-CheckingRbFemaleManully", testWindow);

            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("Female")).SendKeys(Keys.ArrowUp);            
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Male RadioButton option"));
            //Wait for focus
            //testWindow.WaitForElement(GetXPathForCheckedRadioButtonWithText("Male"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-UpArrowToFocusRbMale", testWindow);

            testWindow.FindElement(GetXPathForRadioButtonInputElWithText("Male")).SendKeys(Keys.ArrowDown);
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Female RadioButton option"));
            //Wait for focus
            //testWindow.WaitForElement(GetXPathForCheckedRadioButtonWithText("Female"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-DownArrowToFocusRbFemale", testWindow);

            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForButtonWithText("Focus Male Option >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Male RadioButton option"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-FocusingRbMaleProgrammatically", testWindow);
        }

        public void RadioButtonLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForCheckBoxWithText("Female")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-CheckingRbFemaleManully", testWindow);

            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForCheckBoxWithText("Male")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Female RadioButton option"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-CheckingRbMaleManully", testWindow);

            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForCheckBoxWithText("Female")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Male RadioButton option"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-CheckingRbFemaleManully", testWindow);
// check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForCheckBoxWithText("Male")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusRbMale", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(1000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(1000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();
// end check
// check shifting focus inside the browser window            
            //Click button - Click RadioButton1
            testWindow.FindElement(GetXPathForCheckBoxWithText("Male")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusRbMale", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
            // end check
        }
    }
}
