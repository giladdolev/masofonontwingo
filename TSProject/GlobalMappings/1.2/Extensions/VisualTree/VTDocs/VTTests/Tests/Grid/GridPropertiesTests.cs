﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridElementTests
    {

        public void GridColumnCountFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
           //Holds "Input" textBox
            IWebElement textBox_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Input"));

            //Clear "Input" textBox
            textBox_elem.Clear();
            //Enter "5" in "Input" textBox
            textBox_elem.SendKeys("5");
            //Wait for textBox Text to changed 
            Thread.Sleep(1000);
            //Button Click - Change Grid ColumnCount
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ColumnCount >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ColumnCount value: 5"));
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("GridColumnCollection Count: 5"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-ChangeColumnCountTo5", testWindow);

            //Clear "Input" textBox
            textBox_elem.Clear();
            //Enter "2" in "Input" textBox
            textBox_elem.SendKeys("2");
            //Wait for textBox Text to changed 
            Thread.Sleep(1000);
            //Button Click - Change Grid ColumnCount
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ColumnCount >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ColumnCount value: 2"));
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("GridColumnCollection Count: 2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeColumnCountTo2", testWindow);

            //Clear "Input" textBox
            textBox_elem.Clear();
            //Enter "3" in "Input" textBox
            textBox_elem.SendKeys("3");
            //Wait for textBox Text to changed 
            Thread.Sleep(1000);
            //Button Click - Change Grid ColumnCount
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ColumnCount >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ColumnCount value: 3"));
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("GridColumnCollection Count: 3"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeColumnCountTo3", testWindow);

            //Clear "Input" textBox
            textBox_elem.Clear();
            //Enter "5" in "Input" textBox
            textBox_elem.SendKeys("0");
            //Wait for textBox Text to changed 
            Thread.Sleep(1000);
            //Button Click - Change Grid ColumnCount
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ColumnCount >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ColumnCount value: 0"));
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("GridColumnCollection Count: 0"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeColumnCountTo0", testWindow);

            //Clear "Input" textBox
            textBox_elem.Clear();
            //Enter "4" in "Input" textBox
            textBox_elem.SendKeys("4");
            //Wait for textBox Text to changed 
            Thread.Sleep(1000);
            //Button Click - Change Grid ColumnCount
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ColumnCount >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ColumnCount value: 4"));
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("GridColumnCollection Count: 4"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeColumnCountTo4", testWindow);
        }

        public void GridSelectionModeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click on cell (1,1) to verify full row is selected when selecting cells
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col2", "e")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-clicking-Cell-e-fullRowIsSelected", testWindow);

            //Button Click - Change Grid SelectionMode
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change SelectionMode value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectionMode value: Cell"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeSelectionModeToCell", testWindow);

            //Click on cell (0,1) to verify only cell is shown is selected when selecting cells
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col2", "b")).Click();
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-clicking-Cell-b-onlyCellIsSelected", testWindow);

            //Button Click - Change Grid SelectionMode
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change SelectionMode value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectionMode value: Row"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-buttonClick-ChangeSelectionModeToRow", testWindow);

            //Click on cell (1,1) to verify full row is selected when selecting cells
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col1", "g")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-clicking-Cell-g-fullRowIsSelected", testWindow);
        }

        public void GridTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid Text
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": TestedGrid"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTestedGrid", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NewText"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewText", testWindow);
        }

        public void GridColDataFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ColData value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set ColData with columns HeaderText >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: #"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeColDataToColumnsHeadersText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set ColData with values of different types >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeColDataBackToMixTypes", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }


        public void GridRowDataFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ColData value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set RowData with RowsHeader colors >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: Red"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeRowDataToRowsHeaderColors", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set RowData with values of different types >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value: 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeRowDataBackToMixTypes", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //19529 - implement SelectedColumnIndex
        //Bug 9756 - SelectedColumnIndex implementation not full - see bug history
        public void GridSelectedColumnIndexFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click Column1 header
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid", "Column1")).Click();
            //Wait for log label update
            //After bug 19529 fix uncomment this line and delete the "Thread.Sleep(2000)" line
            //testWindow.WaitForElement(GetXPathForLabelWithText("SelectedColumnIndex value: 0"));
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickColumn1header-ExpectingSelectedColumnIndex0", testWindow);

            //Click on cell in Column2
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "b")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedColumnIndex value: 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-CellClickInColumn2-ExpectingSelectedColumnIndex1", testWindow);

            
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }


        //Bug 19240 - edit after fix
        public void GridReadOnlyFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
   
            //Button Click - Change ReadOnly value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ReadOnly value >>")).Click();
            
            Thread.Sleep(5000);
           
            //Take and Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToFalse", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ReadOnly value >>")).Click();
            Thread.Sleep(5000);

            //Take and Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTrue", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void GridHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("70px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo70px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("115px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo115px", testWindow);
        }
        public void GridLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid Left
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void GridLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Grid Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Grid Location >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft200Top285", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Grid Location >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top300", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void GridSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid Size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_70", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=285"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo285_115", testWindow);
        }
        public void GridTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid Top
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("285px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
        }
        public void GridWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid Width
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("285px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo285px", testWindow);
        }
      
        public void GridBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_300_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_285_115", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void GridClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_300_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_285_99", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void GridClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo300_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("285,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo285_115", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void GridVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.FindElement(GetXPathForLabelWithText("False."));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void GridTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".GridElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToGridElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void GridPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("70."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo70px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("115."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo115px", testWindow);
        }
        public void GridPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void GridPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.FindElement(GetXPathForLabelWithText("285."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo285px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300px", testWindow);
        }
        public void GridPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("285."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo285px", testWindow);
        }

        public void GridBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void GridColumnHeadersVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ColumnHeadersVisible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ColumnHeadersVisible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void GridMaxRowsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Click RadioButton MaxRows: 2
            testWindow.FindElement(GetXPathForCheckBoxWithText("MaxRows: 2")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("MaxRows value is: 2,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Checking-rbMax2-Manully", testWindow);

            //Button Click - Add New Row
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add New Row >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("System.ArgumentException"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TryToAddNewRow-ArgumentException", testWindow);

            //Click button - Click RadioButton MaxRows: 4
            testWindow.FindElement(GetXPathForCheckBoxWithText("MaxRows: 4")).Click();
            //Wait for Log label update
            testWindow.FindElement(GetXPathForLabelWithText("MaxRows value is: 4,"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Checking-rbMax4-Manully", testWindow);

            //Button Click - Add New Row
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add New Row >>")).Click();
            //Button Click - Add New Row
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add New Row >>")).Click();

            //Button Click - Add New Row
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add New Row >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("System.ArgumentException"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-TryToAddNewRow-ArgumentException", testWindow);

            //Click button - Click RadioButton MaxRows: 2
            testWindow.FindElement(GetXPathForCheckBoxWithText("MaxRows: 2")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("is bigger than"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-TryToChangeToMaxRows2-ArgumentException", testWindow);
        }

        // Bug 21818 - Initial value doesn't work, Related to Bugs 21816, 21817, 20648
        public void GridFrozenColsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Click RadioButton Frozen Col No. : 2
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Frozen Col No. : 2")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FrozenCols value is: 2"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Checking-rbFrozenCols2-Manually", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col3", "c")).Click();

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col3", "c")).SendKeys(Keys.ArrowRight + Keys.ArrowRight);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col5", "e")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ScrollingRightHorizontallyWithArrows", testWindow);

            //Click button - Click RadioButton Frozen Col No. : 1
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Frozen Col No. : 1")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FrozenCols value is: 1"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Checking-rbFrozenCols1-Manually", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col5", "e")).Click();

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col5", "e")).SendKeys(Keys.ArrowLeft + Keys.ArrowLeft);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col3", "c")).Click();

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ScrollingLeftHorizontallyWithArrows", testWindow);

            //Click button - Click RadioButton Frozen Col No. : 2
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Frozen Col No. : 2")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FrozenCols value is: 2"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Checking-rbFrozenCols2-Manually", testWindow);

            //Click button - Click RadioButton Frozen Col No. : 1
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Frozen Col No. : 1")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FrozenCols value is: 1"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-Checking-rbFrozenCols1-Manually", testWindow);

            //Click button - Click RadioButton Frozen Col No. : 2
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Frozen Col No. : 2")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FrozenCols value is: 2"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-Checking-rbFrozenCols2-Manually", testWindow);

            //Click button - Click RadioButton Frozen Col No. : 0
            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Frozen Col No. : 0")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FrozenCols value is: 0"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-Checking-rbFrozenCols0-Manually", testWindow);
  
        }

        public void GridSelectedRowIndexFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col3", "i")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 2"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickOnRowIndex2", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col2", "b")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 0"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnRowIndex0", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col1", "d")).SendKeys(Keys.ArrowLeft);
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 1"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectRowIndex1HeaderByArrow", testWindow);

        }

        public void GridSelectedRowsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //No row selection. Button Click - Selected Rows Count
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Selected Rows Count >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedRows: 0"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-NoRowSelectionPressToCount", testWindow);
            
            //Selecting one row
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col1", "d")).SendKeys(Keys.ArrowLeft);
            //Button Click - Selected Rows Count
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Selected Rows Count >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedRows: 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-selecting1rowAndPressToCount", testWindow);

            //Selecting all 3 rows
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col1", "a")).SendKeys(Keys.Control + "a");
            //Button Click - Selected Rows Count
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Selected Rows Count >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedRows: 3"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-selecting3rowsAndPressToCount", testWindow);

            IWebElement firstRowElem = testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("0"));
            IWebElement thirdRowElem = testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("2"));           

            //Selecting one row
            firstRowElem.Click();

            //While pressing Ctrl Select third row
            Actions actions = new Actions(CurrentWebDriver);
            actions.KeyDown(Keys.LeftControl)
                //.Click(firstRowElem)
                .Click(thirdRowElem)
                .KeyUp(Keys.LeftControl)
                .Build()
                .Perform();

            //Button Click - Selected Rows Count
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Selected Rows Count >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedRows: 2"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-selecting1stRowAnd3rdRowWithCtrlAndPressBtnToCount", testWindow);
                        
        }

        public void GridCurrentCellFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col2", "h")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 2 , 1"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickOnACell", testWindow);

            //Clickin on the button to set CurrenCell problematically
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change CurrentCell to (1,2) >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 1 , 2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnButtonToSetCurrentCell", testWindow);

            
        }

        public void GridAllowColumnSelectionFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click TestedGrid1 Column1 header
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid1", "Column1")).Click();
            //Click TestedGrid2 Column1 header
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column1")).Click();

            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickingColumn1HeaderOfGrid1AndGrid2", testWindow);

            //Button Click - Change AllowColumnSelection value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change AllowColumnSelection value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            //Click TestedGrid2 Column1 header After changing AllowColumnSelection property to false
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column1")).Click();
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickingGrid2Column1HeaderAfterSetToFalse", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change AllowColumnSelection value >>")).Click();
            testWindow.FindElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToTrue", testWindow);

            //Click TestedGrid2 Column1 header After changing AllowColumnSelection property to false
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column1")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickingGrid2Column1HeaderAfterSetToTrue", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        // Task 19677 - VT - GridElement -  Fix of Column Data Member
        // Bug 25450 - Client data store remains dirty and does not update the Server data store
        public void GridDataSourceFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Empty the DataSource
            testWindow.FindElement(GetXPathForButtonSpanWithText("Empty DataSource >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DataSource has been emptied."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-InGrid1EmptyTheDataSource", testWindow);

            //Set DataSource after DataSource was empty
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set DataSource >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DataSource was set."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-InGrid1SetDataSourceAfterDatSourceWasEmpty", testWindow);

            //Edit a cell and leave
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-testedgrid-1", "Lisa")).Click();
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndClickedCellText("vt-testedgrid-1", "Lisa")).SendKeys("bb");
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-testedgrid-1", "Bart")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("was edited."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-InGrid1EditLisaCellAndLeave", testWindow);

            //Set "NewText" in TextBox Cell(0,1)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Grid1 TextBox Cell(0,1) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Cell (0,1) was set"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickBtnToSetCellRow0Col1WithNewText", testWindow);

            //Uncheck CheckBox Cell(0,2)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Grid1 CheckBox Cell(0,2) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Cell (0,2) was set"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickBtnToSetCellRow0Col2WithFalse", testWindow);

            //Set "female" in ComboBox Cell(2,4)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Grid1 ComboBox Cell(2,4) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Cell (2,4) was set"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickBtnToSetCellRow2Col4Withfemale", testWindow);

            //Copy TestedGrid1 to TestedGrid2
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set TestedGrid1 to TestedGrid2 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("set to TestedGrid2.DataSource."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-CopyGrid1DataSourceToGrid2DataSource", testWindow);
        }

        // Task 19146 - VT - implement filter of grid element
        public void GridColumnFilterInfoFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Filter by value: 1-
            IWebElement tb_elem = testWindow.FindElement(GetXPathForTextBoxWithText("..."));
            tb_elem.Clear();
            //Set a new value in the textbox
            tb_elem.SendKeys("1-");
            //Verify the value was inserted
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter is 1-..."));
            //Button Click - invoke filtering
            testWindow.FindElement(GetXPathForButtonSpanWithText("Filter By Name >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter: 1-"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-FilterByValue1-", testWindow);

            //Filter by value: #
            tb_elem.Clear();
            tb_elem.SendKeys("#");
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter is #..."));
            testWindow.FindElement(GetXPathForButtonSpanWithText("Filter By Name >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter: #"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-FilterByValue#", testWindow);

            ///Filter by value that doesn't exist
            tb_elem.Clear();
            tb_elem.SendKeys("xxx");
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter is xxx..."));
            testWindow.FindElement(GetXPathForButtonSpanWithText("Filter By Name >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter: xxx"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-FilterByValueThatDoesntExist", testWindow);

            //Clear the filter
            testWindow.FindElement(GetXPathForButtonSpanWithText("Clear Filter >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter is ......"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClearingTheFilter", testWindow);

            //Filter by value: Gal
            tb_elem.Clear();
            tb_elem.SendKeys("Gal");
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter is Gal..."));
            testWindow.FindElement(GetXPathForButtonSpanWithText("Filter By Name >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Value to filter: Gal"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-FilterByValueGal", testWindow);
        }

        public void GridRowHeaderCornerIconFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change RowHeader CornerIcon >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("galilcs"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeImage", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change RowHeader CornerIcon >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("gizmox"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeImage", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change RowHeader CornerIcon >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("galilcs"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeImage", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Clear RowHeader CornerIcon >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("null"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClearImages", testWindow);
        }

        public void GridEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-grid-1", "false"));
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-grid-2", "true"));

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if combo Box is enabled 
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-grid-2", "false")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Check if combo is disabled
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-grid-2", "true"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if combo is disabled
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-grid-2", "false"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void GridContextMenuStripFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            Actions actions = new Actions(CurrentWebDriver);

            // Right click on e cell to display the context menu
            IWebElement elem_e = testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e"));
            actions.ContextClick(elem_e);
            actions.Perform();

            // Wait for the ontextMenuStrip Element
            testWindow.WaitForElement(GetXPathForExpandedContextMenu("true"));

            // Check that the context menu stays open until we click on a different cell
            Thread.Sleep(3000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ContextClickMiddleCell-e", testWindow);

            // Click on a different cell to close the context menu
            IWebElement elem_d = testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "d"));
            elem_d.Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickMiddleLeftCell-d-ContextMenu-IsClosed", testWindow);

            // Right click on column header to display the context menu
            IWebElement elem_Col1 = testWindow.WaitForElement(GetXPathForGridColumnHeaderSpanWithText("Column1"));
            actions.ContextClick(elem_Col1);
            actions.Perform();

            // Wait for the ContextMenuStrip Element
            testWindow.WaitForElement(GetXPathForExpandedContextMenu("true"));

            // open ContextMenuStrip SubMenu Element
            testWindow.WaitForElement(GetXPathForContextMenuItemWithText("Item1")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Menu Item1 was Clicked!"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ContextClickColumn1HeaderWithSubMenuExpanded", testWindow);

            // open ContextMenuStrip SubMenu Element
            //testWindow.WaitForElement(GetXPathForContextMenuItemWithText("Item1_1")).Click();
            testWindow.WaitForElement(GetXPathForContextMenuItemWithText("Item1")).SendKeys(Keys.ArrowRight + Keys.Enter);

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Menu Item1_1 was Clicked!"));

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Click-Item1_1-ContextMenu-IsClosed", testWindow);

            // Right click on d cell to display the context menu
            actions.ContextClick(elem_d);
            actions.Perform();

            // Wait for the ContextMenuStrip Element
            testWindow.WaitForElement(GetXPathForExpandedContextMenu("true"));

            bool exampleSize = true;
            //Compare snapshots 6
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "6-after-ContextClickCell-d", testWindow, exampleSize);

            Thread.Sleep(2000);

            // open ContextMenuStrip SubMenu Element
            testWindow.WaitForElement(GetXPathForContextMenuItemWithText("Item2")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Menu Item2 was Clicked!"));

            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-Click-Item2-ContextMenu-IsClosed", testWindow);

            // right click on body
            IWebElement elem_body = testWindow.FindElement(By.Id("windowView-body"));
            actions.ContextClick(elem_body);
            actions.Perform();

            //Compare snapshots 8
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "8-after-ContextClickExampleBody", testWindow, exampleSize);

            Thread.Sleep(3000);

            elem_body.Click();
        }

        public void GridForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change ForeColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToBlue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        // bug 19214 - action Should be reflected on the Grid (colored)
        // bug 19528 - implement setSelection method
        public void GridCurrentRowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "a")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("index: 0"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickOnACell", testWindow);

            //Clickin on the button to set CurrenRow problematically
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Edit Middle Cell >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("index: 1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnButtonToEditMiddleCell", testWindow);

            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Select i >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("index: 2"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnButtonToSelect-i", testWindow);
        }

        // Bug 24264 - Grid RowHeaderVisible property doesn't work at Runtime
        // Currently Cannot Click "Change RowHeaderVisible value >>" a second time, Because of an error message...
        public void GridRowHeaderVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Grid Width
            testWindow.FindElement(GetXPathForButtonSpanWithText("Get Column Index 0 Name >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column Index 0 Name:"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickBtnGetColumnIndex0Name", testWindow);
            
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change RowHeaderVisible Value >>")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("RowHeaderVisible value: True."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickBtnChangeRowHeaderVisiblevalue-toTrue", testWindow);

            //testWindow.FindElement(GetXPathForButtonSpanWithText("Change RowHeaderVisible value >>")).Click();

            //testWindow.WaitForElement(GetXPathForLabelWithText("RowHeaderVisible value: False."));
            //Compare snapshots 3
            //CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickBtnChangeRowHeaderVisiblevalue-toFalse", testWindow);
        }

        public void GridBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change BackColor
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBackColorToOrange", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBackColorToGreen", testWindow);
        }

        public void GridGridColumnHeaderTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change HeaderText value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change column2 HeaderText >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 HeaderText value: NewHeaderText"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeColumn2HeaderTextToNewHeaderText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change column2 HeaderText >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 HeaderText value: Column2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeColumn2HeaderTextToColumn2", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }                   

    }
}
