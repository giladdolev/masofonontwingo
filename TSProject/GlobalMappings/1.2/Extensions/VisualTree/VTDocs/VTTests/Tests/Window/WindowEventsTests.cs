﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class WindowElementTests
    {

        public void WindowClosingFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Open test window
            testWindow.FindElement(GetXPathForButtonWithText("Open Test Window >>")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("State: False."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpeningTestWindow", testWindow);

            //Click button - Open test window
            testWindow.FindElement(GetXPathForButtonWithText("Close This Window >>")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickBtnToCloseTestWindow", testWindow);

            //Click button - Open test window
            testWindow.FindElement(GetXPathForButtonWithText("Open Test Window >>")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("was called"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-OpeningTestWindow", testWindow);

            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Cancel Closing Action")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("State: True."));

            //Click button - Open test window
            testWindow.FindElement(GetXPathForButtonWithText("Close This Window >>")).Click();

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-CheckCancelAndClickBtnToCloseTestWindow", testWindow);

            testWindow.FindElement(GetXpathForTestWindowCloseBtnWithId("ClosingTestWindow")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickCloseBtnIconForClosingTestWindow", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        
    }
}
