﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ComboBoxElementTests
    {

        public void ComboBoxMatchComboboxWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Open drop-down list of TestedComboBox1
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-comboBox1")).Click();
            //Wait For drop-down to open 
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpendCombo1Dropdowns", testWindow);

            //Open drop-down list of TestedComboBox2
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-comboBox2")).Click();
            //Wait For drop-down to open 
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-OpendCombo2Dropdowns", testWindow);
        }

        public void ComboBoxHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("25px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo25px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }

        public void ComboBoxLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);
            

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }

        public void ComboBoxLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_290", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80_310", testWindow);
        }
        public void ComboBoxSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Size value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=165"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo165_25", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300_80", testWindow);
        }

        public void ComboBoxTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Top value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }

        public void ComboBoxVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);
        }

        public void ComboBoxWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("165px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo165px", testWindow);
        }
        public void ComboBoxBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ComboBoxBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox Bounds 
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_165_25", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ComboBoxClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox ClientRectangle 
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_165_25", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ComboBoxClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox ClientSize
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("165,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo165_25", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ComboBoxTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox Text
            testWindow.FindElement(GetXPathForButtonWithText("Change Text Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Text Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: TestedComboBox"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToTestedComboBox", testWindow);

            //Manually adding text
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboBox")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboBox")).SendKeys("xx");
            //When the bug 21183 will be fixed - Sleep command should be replaced with the wait function below.
            Thread.Sleep(3000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("xx"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeManuallyTo-xx", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ComboBoxPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("25."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo25px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void ComboBoxPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ComboBoxPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void ComboBoxPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("165."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo165px", testWindow);
        }
        public void ComboBoxTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".ComboBoxElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToComboBoxElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ComboBoxSelectedIndexFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox SelectIndex value to 2
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change SelectedIndex Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 2."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ButtonClick- ToChangeIndexTo2", testWindow);

            //To test the fix of bug 22252 - use the keyboard down arrow on the dropdown list when the last item is selected
            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboBox")).Click();
            //press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboBox")).SendKeys(Keys.ArrowDown);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-UseKeyboardDownArrowOnDropDownListWhenLastItemIsSelected - NothingShouldHappen", testWindow);

            //Close the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboBox")).Click();

            //Button Click - Change ComboBox SelectIndex value to 0
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change SelectedIndex Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 0."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ButtonClick-ToChangeIndexTo0", testWindow);

            //To test the fix of bug 22252 - use the keyboard up arrow on the dropdown list when the first item is selected
            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboBox")).Click();
            //press the keyboard up arrow
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboBox")).SendKeys(Keys.ArrowUp);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-UseKeyboardUpArrowOnDropDownListWhenFirstItemIsSelected - NothingShouldHappen", testWindow);

            //Close the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboBox")).Click();

            //Choose an item from the ComboBox
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboBox")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxItem("Item2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 1."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-SelectFromComboBoxItem2", testWindow);
            
        }

        public void ComboBoxSelectedItemFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Choose an item from the ComboBox
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboBox")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxItem("Item3")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: Item3."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeManuallyToItem3", testWindow);

            //Button Click - Change ComboBox SelectItem
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change SelectedItem Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Item1."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToItem1", testWindow);
            
        }

        //Bug 9633 - ComboBox.Font Property doesn't work.
        public void ComboBoxFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox Font 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
        }
        public void ComboBoxForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change CheckBox ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToBlue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);
        }

        // Bug 9634 - ComboBox.DropDownStyle Property doesn't work, 20542
        public void ComboBoxDropDownStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Verify TestedComboBox1 is not ReadOnly by Inserting text
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-1")).SendKeys("AbcD123");

            //Check Initialize settings (DropDownList) of DropDownStyle property in TestedComboBox
            testWindow.WaitForElement(GetXPathForReadOnlyComboBoxWithCssClass("vt-test-cmb-2"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SecondComboBoxIsReadOnly+EditFirstComboBox", testWindow);

            //Button Click - Change DropDownStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Simple."));

            //Clicking the arrow for TestedComboBox1 to display the list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb-1")).Click();
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-OpenListOfFirstComboBox-DropDownMode", testWindow);

            //Verify TestedComboBox is not ReadOnly by deleting text
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToSimple+DeleteLettersFromSecondComboBox", testWindow);
           
            //Button Click - Change DropDownStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DropDown."));

            //Verify TestedComboBox is not ReadOnly by adding text
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-2")).SendKeys("Box");
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToDropDown+AddTextToSecondComboBox", testWindow);

            //Clicking the arrow for TestedComboBox to display the list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb-2")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-OpenListOfSecondComboBox-DropDownMode", testWindow);

            //Button Click - Change DropDownStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DropDownList."));

            //Check if TestedComboBox is ReadOnly after setting DropDownStyle = DropDownList
            testWindow.WaitForElement(GetXPathForReadOnlyComboBoxWithCssClass("vt-test-cmb-2"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ChangeToDropDownList-ComboBoxIsReadOnly", testWindow);

            //Clicking the arrow for TestedComboBox to display the list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb-2")).Click();
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-OpenListOfSecondComboBox-DropDownListMode", testWindow);

            testWindow.WaitForElement(GetXpathForWindowCloseBtn(exampleId)).Click();
            //testWindow.SendKeys(Keys.Escape);

            //Compare snapshots 8 - to verify the combobox list doesn't appear in the background after the example is closed (bug 23280)
            CheckClosedSnapshot(membersCategory, memberName, exampleId + "8-after-ClosingTheExampleVerifyTheComboBocDoesntAppearInBackground", testWindow);
        }

        public void ComboBoxEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForEnabledComboBoxWithCssClass("vt-test-cmb-1", "false"));
            testWindow.FindElement(GetXPathForEnabledComboBoxWithCssClass("vt-test-cmb-2", "true"));

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if combo Box is enabled 
            testWindow.FindElement(GetXPathForEnabledComboBoxWithCssClass("vt-test-cmb-2", "false")).Click();
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueSecondCmb", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Check if combo is disabled
            testWindow.FindElement(GetXPathForEnabledComboBoxWithCssClass("vt-test-cmb-2", "true"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondCmb", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void ComboBoxMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=110, Height=90}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo110-90", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=130, Height=30}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo130-30", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=120, Height=50}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo120-50", testWindow);

            //Button Click - Cancel ComboBox MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void ComboBoxMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=150, Height=30}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo150-30", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=100, Height=70}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo100-70", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=30}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo200-30", testWindow);

            //Button Click - Cancel ComboBox MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }

        
         public void ComboBoxDroppedDownFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Button Click - Change DroppedDown value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change DroppedDown value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("False."));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToFalse", testWindow);


             testWindow.FindElement(GetXPathForButtonSpanWithText("Change DroppedDown value >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("True."));
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToTrue", testWindow);
         }

         // Bug 20732 - VT - ComboBoxElement - AutoCompleteMode does not work
         // Bug 21915
         // Bug 27543 - Operation error (until this bu is fixed the test will fail)
         public void ComboBoxAutoCompleteModeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Testing default value 'None' in TestedComboBox1 - autocomplete shouldn't be activated
             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-1")).SendKeys("j");
             
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Insert-j-InComboBox1-None_AutoCompleteShouldNotBeActivated", testWindow);

             //Testing 'Append' mode in TestedComboBox2 - Appends the remainder of the most likely candidate string to the existing characters
             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-2")).SendKeys("m");
             //Wait For appended string
             Thread.Sleep(1000);
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Insert-m-InComboBox2-Appened_ShoulAppend-March-AndNotOpenDropDown", testWindow);

             //Button Click - Change AutoCompleteMode
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change AutoCompleteMode >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Suggest."));

             //Testing 'Suggest' mode in TestedComboBox2 - Displays the auxiliary drop-down list associated with the edit control.
             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-2")).SendKeys("j");
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeModeToSuggestAndInsert-j-InComboBox2-ShouldOpenListAndNotAppend", testWindow);

             //Select 'June' From dropdown nerrowed down list (testing bug 21915 fix)
             testWindow.FindElement(GetXPathForComboBoxItem("June")).Click(); 
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Selected index: 5"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SelectJuneWithClickInComboBox2WhenListIsNerrowedDown-SelectedIndexShouldBe5", testWindow);

             //Button Click - Change AutoCompleteMode
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change AutoCompleteMode >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("SuggestAppend."));

             //Testing 'SuggestAppend' mode in TestedComboBox2 - Applies both Suggest and Append options.
             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-2")).SendKeys("a");
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 5
             CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ChangeModeToSuggestAppendAndInsert-a-InComboBox2-ShouldAppend-April-AndOpenList", testWindow);

             //Button Click - Change AutoCompleteMode
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change AutoCompleteMode >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("None."));

             //Testing 'None' mode (again) in TestedComboBox2 - Aautocomplete shouldn't be activated.
             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-2")).SendKeys("m");
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 6
             CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ChangeModeToSNoneAndInsert-m-InComboBox2-AutoCompleteShouldNotBeActivated", testWindow);
         }

         public void ComboBoxDropDownHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Open DropDown list to verify initialize value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb1")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropDownList1ToVerifyDefaultHeight", testWindow);

             //Open DropDown list to verify initialize value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-OpenDropDownList2ToVerifyInitializeValue", testWindow);

             //Button Click - Change DropDownHeight value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownHeight value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: 80."));

             //Open DropDown list to verify DropDownHeight value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickBtnChangeDropDownHeightTo80AndOpenDropDownList2", testWindow);

             //Button Click - Change DropDownHeight value again
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownHeight value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: 40."));
             //Open DropDown list to verify DropDownHeight value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickBtnChangeDropDownHeightTo40AndOpenDropDownList2", testWindow);

         }

         public void ComboBoxDropDownWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             //Open first combobox DropDown list to verify default value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb1")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropDownList1ToVerifyDefaultWidth-ShouldBeTheSameAsComboBoxWidth", testWindow);

             //Open second combobox DropDown list to verify initialize value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-OpenDropDownList2ToVerifyInitializeValue", testWindow);

             //Close DropDown list
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Button Click - Change DropDownWidth value
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownWidth value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: 250."));
             //Open DropDown list to verify DropDownWidth value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickBtnChangeDropDownWidthTo250AndOpenDropDownList2", testWindow);

             //Close DropDown list
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Button Click - Change DropDownWidth value again
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownWidth value >>")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("value: 200."));
             //Open DropDown list to verify DropDownWidth value
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb2")).Click();
             //Wait For dropdown list
             Thread.Sleep(1000);
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickBtnChangeDropDownWidthTo200AndOpenDropDownList2", testWindow);

         }

         public void ComboBoxLockedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb-1")).Click();
             testWindow.FindElement(GetXPathForComboBoxItem("Item1")).Click();

             //Wait For dropdown list
             Thread.Sleep(1000);

             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectFirsItemInTopComboBoxToVerifyLockedFalse", testWindow);

             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-1")).SendKeys("A");

             //Wait For dropdown list
             Thread.Sleep(1000);

             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TypeAIntoTopComboBoxToVerifyLockedFalse", testWindow);

             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb-2")).Click();
             testWindow.FindElement(GetXPathForComboBoxItem("Item4")).Click();             

             //Wait For dropdown list
             Thread.Sleep(1000);

             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectFirstItemInBottomLeftComboBoxToVerifyLockedTrue", testWindow);

             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-2")).SendKeys("A");

             //Wait For dropdown list
             Thread.Sleep(2000);

             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-TryToTypeAIntoBottomLeftComboBoxToVerifyLockedTrue", testWindow);

             testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb-3")).Click();
             testWindow.FindElement(GetXPathForComboBoxItem("Item7")).Click();

             //Wait For dropdown list
             Thread.Sleep(1000);

             //Compare snapshots 5
             CheckSnapshot(membersCategory, memberName, exampleId + "5-after-SelectFirsItemInBottomRightComboBoxToVerifyLockedFalse", testWindow);

             testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-3")).SendKeys("A");

             //Wait For dropdown list
             Thread.Sleep(1000);

             //Compare snapshots 6
             CheckSnapshot(membersCategory, memberName, exampleId + "6-after-TypeAIntoBottomRightComboBoxToVerifyLockedFalse", testWindow);
         }

    }
}
