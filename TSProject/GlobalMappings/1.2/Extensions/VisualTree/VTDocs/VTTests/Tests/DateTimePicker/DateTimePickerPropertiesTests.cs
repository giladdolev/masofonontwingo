﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;


namespace VTTests
{
    public partial class DateTimePickerElementTests
    {
        //27463 - default empty , format is wrong - after bug fix edit the automation 
        //Bug 23436 -  default time is 00:0:00, should be current time
        //BUg 23435 - the default Format is Short when it should be Long
        public void DateTimePickerValueFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change BackColor
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Value property >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("12/04/2017 12:30:12"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-clickButton-ToChangeDateAndTimeProgrammatically", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Value property >>")).Click();
            //testWindow.WaitForElement(GetXPathForLabelWithText("27/07/2027 06:50:24"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-clickButton-ToChangeDateAndTimeProgrammatically", testWindow);

            //Click to open DateTimePicker calendar
            testWindow.FindElement(GetXpathForControlsWithCssClassAndClass("vt-TestedDateTimePicker", "x-form-date-trigger")).Click();
            Thread.Sleep(1000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-openCalendar", testWindow);

            //Click the left arrow in the calendar to slide to previous month
            testWindow.FindElement(GetXpathForDateTimePickerLeftArrowToPreviousMonth()).Click();
            testWindow.FindElement(GetXpathForDateTimePickerDay("7")).Click();
            //Wait for log label update
            //Bug 23436 After DateTimePicker bugs are fixes delete the following 'WaitForElement' and uncomment the next 'WaitForElement' 
            testWindow.WaitForElement(GetXPathForLabelWithText("07/06/2027 00:00:00"));
            //testWindow.WaitForElement(GetXPathForLabelWithText("07/06/2027 06:50:24"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SelctingMonthAndDayFromCalendarManuallyByMouse", testWindow);

        }
    }
}
