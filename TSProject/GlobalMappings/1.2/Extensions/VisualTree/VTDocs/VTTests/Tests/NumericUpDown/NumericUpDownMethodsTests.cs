﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class NumericUpDownElementTests
    {

        public void NumericUpDownSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button to set new bounds
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetBoundsTo100_280_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetBoundsTo80_300_150_24", testWindow);
        }

        public void NumericUpDownHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke NumericUpDown Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void NumericUpDownShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke NumericUpDown Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }
        public void NumericUpDownGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke NumericUpDown GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".NumericUpDownElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        //Bug 21295 - ToString method doesn't return the Minimum and Maximum values
        public void NumericUpDownToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }


        public void NumericUpDownEqualsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Set TestedNumericUpDown to NumericUpDown1
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set to NumericUpDown1 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedNumericUpDown was set to NumericUpDown1."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNumericUpDown1", testWindow);

            //Button Click - Set TestedNumericUpDown to NumericUpDown2
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set to NumericUpDown2 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedNumericUpDown was set to NumericUpDown2."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetToNumericUpDown2", testWindow);

            //Button Click - Invoked Equals method, Return True if TestedNumericUpDown is equal to NumericUpDown1, otherwise False.
            //On this invokation the return value will be False
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke Equals(NumericUpDown1) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: False."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-InvokeEqualsReturnFalse", testWindow);

            //Button Click - Set TestedNumericUpDown to NumericUpDown1
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set to NumericUpDown1 >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedNumericUpDown was set to NumericUpDown1"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetToNumericUpDown1", testWindow);

            //Button Click - Invoked Equals method, Return True if TestedNumericUpDown is equal to NumericUpDown1, otherwise False.
            //On this invokation the return value will be True
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke Equals(NumericUpDown1) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: True."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-InvokeEqualsReturnTrue", testWindow);






        }
        
    }
}
