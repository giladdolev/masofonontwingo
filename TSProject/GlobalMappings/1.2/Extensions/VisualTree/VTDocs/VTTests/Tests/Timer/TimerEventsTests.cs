﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class TimerElementTests
    {
        public void TimerTickFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Start method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Start >>")).Click();
            //Wait 3 seconds
            Thread.Sleep(3000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-StartAndWait3Seconds", testWindow);


            //Button Click - Invoke Stop method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Stop >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("stoped,"));
            //Wait 3 seconds
            Thread.Sleep(3000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-StopAndWait3Seconds", testWindow);

            //Wait 3 seconds
            Thread.Sleep(3000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-3seconds", testWindow);

            //Button Click - Invoke Start method
            testWindow.FindElement(GetXPathForButtonSpanWithText("Start >>")).Click();
            //Wait 5 seconds
            Thread.Sleep(5000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-StartAgainAndWait5Seconds", testWindow);

            //Close window
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
    }
}
