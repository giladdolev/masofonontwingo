﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ButtonElementTests
    {
        public void ButtonBasicFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement fontBtnSpan = testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Font"));
            testWindow.FindElement(GetXPathForButtonWithText("Change Button Font")).Click();

            // In current VisualTree implementation content of button would be replaced
            // So let's wait till current span exist in document
            fontBtnSpan.WaitWhileAttachedToPage();

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "-after-changeFontBtn-click", testWindow);

            IWebElement colorBtnSpan = testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Colors Change Button Colors Change Button Colors"));

            testWindow.FindElement(GetXPathForButtonWithText("Change Button Colors Change Button Colors Change Button Colors")).Click();

            // In current VisualTree implementation content of button would be replaced
            // So let's wait till current span exist in document
            colorBtnSpan.WaitWhileAttachedToPage();

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "-after-changeColorBtn-click", testWindow);



            IWebElement defaultBtn = testWindow.FindElement(GetXPathForButtonWithText("Defaul Button"));
            defaultBtn.Click();

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "-after-defaultBtn-click", testWindow);



            IWebElement bigRedBtn = testWindow.FindElement(GetXPathForButtonWithText("Big Red Button"));
            //In case if element not in view and we need to scroll to it
            this.Driver.ScrollToElement(bigRedBtn);

            bigRedBtn.Click();
            Thread.Sleep(1000);

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "-after-bigRedBtn-click", testWindow);

            //
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ButtonContextMenuStripFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            Actions actions = new Actions(CurrentWebDriver);

            //Button click - Change BackColor
            IWebElement TestedButton1_elem = testWindow.FindElement(GetXPathForButtonSpanWithText("Button"));
            //Right click TestedButton1
            actions.ContextClick(TestedButton1_elem);
            actions.Perform();
            //Wait for contextMenu to open
            Thread.Sleep(3000);
            //Compare snapshots 1
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "1-after-rightClickTestedButton1", testWindow,true);

            IWebElement TestedButton2_elem = testWindow.FindElement(GetXPathForButtonSpanWithText("TestedButton"));
            //Right click TestedButton2
            actions.ContextClick(TestedButton2_elem);
            actions.Perform();
            //Wait for contextMenu to open
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-rightClickTestedButton2-HelloMenuItem", testWindow);

            //Button click - to change contextMenuStrip
            testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip2")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": contextMenuStrip2"));
            //Right click TestedButton2
            actions.ContextClick(TestedButton2_elem);
            actions.Perform();
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToContextMenuStrip2AndRightClickTestedButton-HelloWorldMenuItem", testWindow);

            //Button click - to change contextMenuStrip
            testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip1")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": contextMenuStrip1"));
            //Right click TestedButton2
            actions.ContextClick(TestedButton2_elem);
            actions.Perform();
            //Compare snapshots 4
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-changeToContextMenuStrip1AndRightClickTestedButton-HelloMenuItem", testWindow, true);

            //Button click - to Reset contextMenuStrip
            testWindow.FindElement(GetXPathForButtonSpanWithText("Reset")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value is null"));
            //Right click TestedButton2
            actions.ContextClick(TestedButton2_elem);
            actions.Perform();
            //Compare snapshots 5
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "5-after-clickResetButtonAndRightClickTestedButton-DefaultBrowserMenu", testWindow, true);

        }

        public void ButtonBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change BackColor
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToOrange", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);


        }

        public void ButtonBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change Button BackgroundImage 
            testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("image."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);


        }

        public void ButtonBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change Button BackgroundImageLayout 
            testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Zoom."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Tile."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Stretch."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Center."));
            //Compare snapshots 4 
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);
        }

        public void ButtonBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Button BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ButtonBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Button Bounds >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Button Bounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_380_150_36", testWindow);


        } 

        public void ButtonClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Button Bounds >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Button Bounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_350_150_36", testWindow);


        }

        public void ButtonClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button ClientSize 
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo150_36", testWindow);
        }

        public void ButtonEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize setting of Enabled property
            testWindow.FindElement(GetXPathForEnabledButtonWithText("Button"));
            testWindow.WaitForElement(GetXPathForDisabledButtonWithText("TestedButton"));
            
            //Button Click - Change Button Enabled 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);
           
            //Check if TestedButton is enabled after setting Enabled = true
            testWindow.FindElement(GetXPathForEnabledButtonWithText("TestedButton"));

            //Click TestedButton - verify it is enabled 
            testWindow.FindElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Changebtn2ToOrange", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToFalse", testWindow);
            
            //Check if TestedTextBox is disabled after setting Enabled = false
            testWindow.FindElement(GetXPathForDisabledButtonWithText("TestedButton"));
         
        }

        public void ButtonFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Font 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
        }

        public void ButtonForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [YellowGreen]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToYellowGreen", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Black]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToBlack", testWindow);
        }

        public void ButtonHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Height 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("60px"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo60px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("40px"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo40px", testWindow);
        }

        public void ButtonImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - set Button Image 
            testWindow.FindElement(GetXPathForButtonWithText("Change Image >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No image."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Image >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);


        }

        public void ButtonLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Left 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }

        public void ButtonLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Location
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Location >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_300", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Location >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80_325", testWindow);
        }

        public void ButtonSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Size >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Size >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=150"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150_36", testWindow);
        }

        public void ButtonTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Text
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Button Text >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": TestedButton"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToTestedButton", testWindow);
        }

        // Bug 21563 - Button TextImageRelation property Overlay value is wrong and also default value is ImageBeforeText instead of Overlay
        //ImageAboveText/ ImageBeforeText/ Overlay/ TextAboveImage/ TextBeforeImage
        public void ButtonTextImageRelationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button TextImageRelation
            testWindow.FindElement(GetXPathForButtonWithText("ImageAboveText >")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ImageAboveText."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToImageAboveText", testWindow);

            //Button Click - Change Button TextImageRelation
            testWindow.FindElement(GetXPathForButtonWithText("ImageBeforeText >")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ImageBeforeText."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToImageBeforeText", testWindow);
            
            testWindow.FindElement(GetXPathForButtonWithText("Overlay >")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Overlay."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToOverlay", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("TextAboveImage >")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("TextAboveImage."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToTextAboveImage", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("TextBeforeImage >")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("TextBeforeImage."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToTextBeforeImage", testWindow);

        }

        public void ButtonTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Top
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": 370px"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo370px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": 350px"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo350px", testWindow);
        }
        public void ButtonVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Visible
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ButtonWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Width
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }
        public void ButtonTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".ButtonElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToButtonElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ButtonPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 60"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo60px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 40"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo40px", testWindow);
        }
        public void ButtonPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void ButtonPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 370"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo370px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 350"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo350px", testWindow);
        }
        public void ButtonPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Button PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("150."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150px", testWindow);
        }
        public void ButtonEnableToggleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Click TestedButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Button")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonOnce", testWindow);

            //Button Click - Click TestedButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Button")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonTwice", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickTestedButtonOnce", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickTestedButtonTwice", testWindow);

            //Button Click - Click "Change EnableToggle value >>"
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change EnableToggle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("EnableToggle = False"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-WhenIsPressedFalse-ClickChangeEnableToggleToFalse", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickTestedButtonOnce", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickTestedButtonTwice", testWindow);

            //Button Click - Click "Change EnableToggle value >>"
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change EnableToggle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("EnableToggle = True,"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickChangeEnableToggleToTrue", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickTestedButtonOnce", testWindow);

            //Button Click - Click "Change EnableToggle value >>"
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change EnableToggle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("EnableToggle = False,"));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-WhenIsPressedTrue-ClickChangeEnableToggleToFalse", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickTestedButtonOnce", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-ClickTestedButtonTwice", testWindow);

            //Button Click - Click "Change EnableToggle value >>"
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change EnableToggle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("EnableToggle = True,"));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-WhenIsPressedTrue-ClickChangeEnableToggleToTrue", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 14
            CheckSnapshot(membersCategory, memberName, exampleId + "14-after-ClickTestedButtonOnce", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 15
            CheckSnapshot(membersCategory, memberName, exampleId + "15-after-ClickTestedButtonTwice", testWindow);

        }
        public void ButtonIsPressedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Click TestedButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Button")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonOnce", testWindow);

            //Button Click - Click TestedButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Button")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonTwice", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickTestedButtonOnce", testWindow);

            //Button Click - Click TestedCheckButton 
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("TestedButton")).Click();
            //Wait for PressedChange event
            Thread.Sleep(3000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickTestedButtonTwice", testWindow);

            //Button Click - Click "Change EnableToggle value >>"
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Toggle IsPressed value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IsPressed = False"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickToggleIsPressedToFalse", testWindow);
            
            //Button Click - Click "Change EnableToggle value >>"
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Toggle IsPressed value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IsPressed = True"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToggleIsPressedToTrue", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void ButtonMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=50, Height=100}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo50-100", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=250, Height=50}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo250-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=100, Height=70}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo100-70", testWindow);

            //Button Click - Cancel Button MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void ButtonMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=100, Height=50}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo100-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=90}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200-90", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=150, Height=100}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo150-100", testWindow);

            //Button Click - Cancel Button MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }

        public void ButtonToolTipTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            // Check that the tooltip text is attached to the button
            testWindow.WaitForElement(GetXPathForButtonSpanWithTooltipWithText("TestedButton1", "MyToolTip1"));
            testWindow.WaitForElement(GetXPathForButtonSpanWithTooltipWithText("TestedButton2", "MyToolTip2"));

            //Button Click - Change Label ToolTipText to "NewToolTip1" and "NewToolTip2"
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ToolTipText value >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedButton1 ToolTipText value: NewToolTip1"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToolTipText-NewToolTip", testWindow);

            // Check that the tooltip text is attached to the button and the text has been changed
            testWindow.WaitForElement(GetXPathForButtonSpanWithTooltipWithText("TestedButton1", "NewToolTip1"));
            testWindow.WaitForElement(GetXPathForButtonSpanWithTooltipWithText("TestedButton2", "NewToolTip2"));

            //Button Click - Change Label ToolTipText back to "MyToolTip1" and "MyToolTip2"
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ToolTipText value >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedButton1 ToolTipText value: MyToolTip1"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBackToolTipText-MyToolTip", testWindow);

            // Check that the tooltip text is attached to the button and the text has been changed
            testWindow.WaitForElement(GetXPathForButtonSpanWithTooltipWithText("TestedButton1", "MyToolTip1"));
            testWindow.WaitForElement(GetXPathForButtonSpanWithTooltipWithText("TestedButton2", "MyToolTip2"));
        }
    }

}
