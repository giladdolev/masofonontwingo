﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class CheckedListBoxElementTests
    {

        public void CheckedListBoxSetItemCheckedIntBoolFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {


            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectIndex")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-cmbSelectIndex")).SendKeys(Keys.Enter);
            //Click CheckBox - check
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckState")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke SetItemChecked(Int32, Boolean) >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("SetItemChecked(0, True)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-InvokingSetItemCheckedWithIndex0AndValueTrue-CheckBox1IsChecked", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectIndex")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-cmbSelectIndex")).SendKeys(Keys.Enter);
            //Click CheckBox - uncheck
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckState")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke SetItemChecked(Int32, Boolean) >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("SetItemChecked(0, False)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-InvokingSetItemCheckedWithIndex0AndValueFalse-CheckBox1IsNotChecked", testWindow);


            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectIndex")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("4")).Click();
            //Click CheckBox - check
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckState")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke SetItemChecked(Int32, Boolean) >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("SetItemChecked(4, True)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ivokingSetItemCheckedWithIndex4AndValueTrue-CheckBox5IsChecked", testWindow);



        }

        public void CheckedListBoxGetItemCheckedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Check CheckBox5
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckBox 5")).Click();
            //Button Click - to Get to log all CheckBoxes CheckState
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke GetItemChecked(index) For All >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("5 CheckState: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-checkingCheckBox5AndClickingtheGetButton", testWindow);


            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectIndex")).Click();
            //Press the keyboard down arrow
            testWindow.WaitForElement(GetXPathForComboBoxItem("2")).Click();
            //Click CheckBox - check
            testWindow.FindElement(GetXPathForCheckBoxWithText("CheckState")).Click();
            //Button Click
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke SetItemChecked(Int32, Boolean) >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("SetItemChecked(2, True)"));

            //Button Click - to Get to log all CheckBoxes CheckState
            testWindow.FindElement(GetXPathForButtonSpanWithText("Invoke GetItemChecked(index) For All >>")).Click();
            //Wait for log label
            testWindow.WaitForElement(GetXPathForLabelWithText("3 CheckState: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-InvokingSetItemChecked(2,True)-CheckBox3IsChecked-ClickingGetButton-AllCheckStateInLog", testWindow);

        }
    
    }
}
