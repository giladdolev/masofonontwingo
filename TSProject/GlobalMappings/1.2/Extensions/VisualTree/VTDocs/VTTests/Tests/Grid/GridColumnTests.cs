﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridColumnTests
    {


       
       

      
        //Bug 21620 -  when initializing a column to false , the column can't to changed to visible at runtime
        public void GridGridColumnVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Column1 Visibility >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column1 Visible value is: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeColumn1ToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Column2 Visibility >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 Visible value is: False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeColumn2ToFalse", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Column3 Visibility >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 Visible value is: False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeColumn3ToFalse", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Column2 Visibility >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 Visible value is: True"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeColumn2ToTrue", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
       


        public void BasicGridGridComboBoxColumnFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Add Column4
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Combo Box Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column4 was added"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-addingColumn4", testWindow);
            
            //Grid cell click and cell input click
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("col4", "female")).Click();
            IWebElement elem_column4Cell = testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("col4", "female"));
            elem_column4Cell.Click();
            //open cell combobox list
            elem_column4Cell.SendKeys(Keys.Down + Keys.Down);
            //Showing the combo list
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ShowColumn4Row2ComboBoxList", testWindow);
            //Select from ComboBox
            elem_column4Cell.SendKeys(Keys.Enter);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-selectFromColumn4Row2ComboBox", testWindow);
            
            //This line is added because of bug 22086
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("col1", "female")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Combo Box Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column4 was removed"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-removingColumn4", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Combo Box Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 was removed"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-removingColumn3", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Combo Box Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column3 was added"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-addingColumn3", testWindow);

            //Grid cell click and cell input click
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("col3", "female")).Click();
            IWebElement elem_column3Cell = testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("col3", "female"));
            elem_column3Cell.Click();
            //open cell combobox list
            elem_column3Cell.SendKeys(Keys.Down + Keys.Down);
            //Showing the combo list
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ShowColumn3Row2ComboBoxList", testWindow);
            //select from combo
            elem_column3Cell.SendKeys(Keys.Enter);
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-selectFromColumn3Row2ComboBox", testWindow);

        }
       

        public void BasicGridGridDateTimePickerColumnFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a cell to see the DateTimePicker icon
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "06/08/1974")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickTheCell", testWindow);

            //SelectingADifferentDate
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol1", "06/08/1974")).SendKeys(Keys.ArrowDown + Keys.ArrowDown + Keys.Enter);
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectingADifferentDate", testWindow);

            //Moving to different cell to verify the date was edited successfully
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "a")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOtherCell", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //8283- after fix edit the automation
        public void BasicGridGridCheckBoxColumnFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking first cell of column3
            testWindow.WaitForElement(By.XPath(string.Format("//td[contains(@data-columnid, 'Col3')]"))).Click();
            //Bug 1956 - after bug fix uncomment the below line and delete the Thread.Sleep line  
            //testWindow.WaitForElement(GetXPathForLabelWithText("Present: Tomer, Yoav, Shimrit, Lital"));
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-CheckingTomersCheckBox", testWindow);

            testWindow.WaitForElement(By.XPath(string.Format("//td[contains(@data-columnid, 'Col3')]"))).Click();
            //Bug 1956 - after bug fix uncomment the below line and delete the Thread.Sleep line    
            //testWindow.WaitForElement(GetXPathForLabelWithText("Absence: Tomer, Shalom, Moran, Haim"));
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-UnCheckingTomersCheckBox", testWindow);
        }        

        public void GridGridColumnIconFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Icon value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change column2 Icon >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 Icon name: icon.jpg"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeColumn2IconToImage-icon", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change column2 Icon >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Column2 Icon name: gizmox.png"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeColumn2IconToImage-gizmox", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 19560 -  After fix the automation
        public void GridGridCheckBoxColumnCheckedChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change CheckBoxes value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change CheckBox cell value >>")).Click();
            //Wait For Log Label update
            //Bug 19560 - after fix uncomment the below line and delete the Thread.Sleep line
            // testWindow.WaitForElement(GetXPathForLabelWithText("Column1 CheckBox is Checked"));
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-checkingAllCheckBoxes", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change CheckBox cell value >>")).Click();
            //Wait For Log Label update
            //Bug 19560 - after fix uncomment the below line and delete the Thread.Sleep line
            // testWindow.WaitForElement(GetXPathForLabelWithText("Column1 CheckBox is UnChecked"));
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-unCheckingAllCheckBoxes", testWindow);

            //Clicking first checkBox cell of Column 2
            testWindow.WaitForElement(By.XPath(string.Format("//td[contains(@data-columnid, 'Col2')]"))).Click();
            //Wait For Log Label update
            //Bug 19560 - after fix uncomment the below line and delete the Thread.Sleep line
            // testWindow.WaitForElement(GetXPathForLabelWithText("Column2 CheckBox is Checked"));
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-checkColumn2Cell", testWindow);

            //Clicking first checkBox cell of Column 2 again
            testWindow.WaitForElement(By.XPath(string.Format("//td[contains(@data-columnid, 'Col2')]"))).Click();
            //Wait For Log Label update
            //Bug 19560 - after fix uncomment the below line and delete the Thread.Sleep line
            // testWindow.WaitForElement(GetXPathForLabelWithText("Column2 CheckBox is UnChecked"));
            Thread.Sleep(2000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-unCheckColumn2Cell", testWindow);
           
        }


        //Bug 20473
        public void GridGridColumnDisplayIndexFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Finding the columns element we want to switch
            IWebElement column4_elem = testWindow.WaitForElement(By.XPath(string.Format("//div[contains(@class, 'vt-GridColumn4')]")));
            IWebElement column2_elem = testWindow.WaitForElement(By.XPath(string.Format("//div[contains(@class, 'vt-GridColumn2')]")));
            IWebElement column1_elem = testWindow.WaitForElement(By.XPath(string.Format("//div[contains(@class, 'vt-GridColumn1')]")));
            IWebElement column3_elem = testWindow.WaitForElement(By.XPath(string.Format("//div[contains(@class, 'vt-GridColumn3')]")));

            //Dragging 'Present' column to 'First Name' column
            Actions actions = new Actions(CurrentWebDriver);
            actions.ClickAndHold(column4_elem).MoveToElement(column2_elem).Release(column1_elem).Build().Perform();
            //wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("PRESENT column was moved from DisplayIndex 3"));
            //Compare snapshot 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-draggingPresentColumnToIndex1", testWindow);

            //Dragging 'Present' column Back
            actions.ClickAndHold(column2_elem).MoveToElement(column4_elem).Release(column4_elem).Build().Perform();
            //After 20473 fix, uncomment the below wait for label two lines and delete the Thread.Sleep line
            //wait for Log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("column was moved from DisplayIndex 3,"));
            //testWindow.WaitForElement(GetXPathForLabelWithText("to DisplayIndex 1"));
            Thread.Sleep(2000);
            //Compare snapshot 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-draggingPresentColumnBackToIndex3", testWindow);


            //Open 'Select Column' ComboBox 
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            //Select 'First Name' item from ComboBox
            testWindow.FindElement(GetXPathForComboBoxItem("#")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Input column: #"));
            //Open 'Select Index' ComboBox 
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectIndex")).Click();
            //Select '2' item from ComboBox
            testWindow.FindElement(GetXPathForComboBoxItem("2")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Input DisplayIndex: 2"));
            //Click 'Set Column DisplayIndex >>' button - move '#' column to index 2
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column DisplayIndex >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("# column DisplayIndex = 2"));
            //Compare snapshot 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Moving#ColumnToIndex2", testWindow);


            //Open 'Select Column' ComboBox 
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            //Select 'First Name' item from ComboBox
            testWindow.FindElement(GetXPathForComboBoxItem("#")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Input column: #"));
            //Open 'Select Index' ComboBox 
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectIndex")).Click();
            //Select '0' item from ComboBox
            testWindow.FindElement(GetXPathForComboBoxItem("0")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Input DisplayIndex: 0"));
            //Click 'Set Column DisplayIndex >>' button - move '#' column back to index 0
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column DisplayIndex >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("# column DisplayIndex = 0"));
            //Compare snapshot 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-Moving#ColumnBackToIndex0", testWindow);
        }

           //Bug 24810, bug 24807, bug 24789
        public void GridGridColumnSortModeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click the button to open a new information window for SortMode property
            testWindow.FindElement(GetXPathForButtonSpanWithText("For more information about SortMode Property click here >>")).Click();
            //wait for window to open
            testWindow.WaitForElement(GetXPathForWindowWithId("InformationWindow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickToOpenInformationWindow", testWindow);

            //Close the window using the close button
            testWindow.FindElement(GetXPathForWindowCloseButtonWithId("InformationWindow")).Click();

            //Click TestedGrid1 Column3 header
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid1", "Column3")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-CloseInfoWin-manuallyClickOnColumn3HeaderAscendingWithSortGlyph", testWindow);

            //Click TestedGrid1 Column3 header
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid1", "Column3")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-manullyClickOnColumn3HeaderDescendingWithSortGlyph", testWindow);

            //Click TestedGrid2 Column6 header - First click to Ascending order
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column6")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-manullyClickColumn6HeaderAscendingSort-notSortableManualy", testWindow);

            //Click TestedGrid2 Column6 header - Second click to Descending order
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column6")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-manullyClickColumn6HeaderDescendingSort-notSortableManualy", testWindow);

            //Bug 25244 - The sorting glyph displays by default when clicking on a column with Programmatic SortMode
            //Click TestedGrid2 Column10 header - First click to Ascending order
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column10")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-manuallyClickColumn10Header-AscendingSort-notSortableManualy", testWindow);

            //Bug 25244 - The sorting glyph displays by default when clicking on a column with Programmatic SortMode
            //Click TestedGrid2 Column10 header - Second click to Descending order
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column10")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-manuallyClickColumn10Header-DescendingSort-notSortableManualy", testWindow);

            //Click 'Sort Column 6' button - to sort column 6 in Ascending order
            testWindow.FindElement(GetXPathForButtonSpanWithText("Sort Column6 - Ascending\\Descending >")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Sort(Column6, SortOrder.Ascending)"));
            //Compare snapshot 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-buttonClickToSortColumn6InAscendingOrder-NoSortGlyph", testWindow);

            //Click 'Sort Column 6' button - to sort column 6 in Descending order
            testWindow.FindElement(GetXPathForButtonSpanWithText("Sort Column6 - Ascending\\Descending >")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Sort(Column6, SortOrder.Descending)"));
            //Compare snapshot 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-buttonClickToSortColumn6InDescendingOrder-NoSortGlyph", testWindow);

            //Click 'Sort Column 10' button - to sort column 10 in Ascending order
            testWindow.FindElement(GetXPathForButtonSpanWithText("Sort Column10 - Ascending\\Descending >")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Sort(Column10, SortOrder.Ascending)"));
            //Compare snapshot 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-buttonClickToSortColumn10InAscendingOrder-NoSortGlyph", testWindow);

            //Click 'Sort Column 10' button - to sort column 10 in Descending order
            testWindow.FindElement(GetXPathForButtonSpanWithText("Sort Column10 - Ascending\\Descending >")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Sort(Column10, SortOrder.Descending)"));
            //Compare snapshot 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-buttonClickToSortColumn10InDescendingOrder-NoSortGlyph", testWindow);


            /////Change SortMode at runtime

            //Button click - to change Column6 SortMode property value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Column6 SortMode >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("is: Programmatic."));
            //Compare snapshot 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-buttonClickToChangeColumn6SortMode-Programmatic", testWindow);

            //Click TestedGrid2 Column6 header - To verify Automatic mode works 
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column6")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-ClickColumn6Header-notSortableManually", testWindow);

            //Button click - to change Column6 SortMode property value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Column6 SortMode >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("is: Automatic."));
            //Compare snapshot 12
            CheckSnapshot(membersCategory, memberName, exampleId + "14-after-buttonClickToChangeColumn6SortMode-Automatic", testWindow);

            //Click TestedGrid2 Column6 header - To verify Automatic mode works 
            testWindow.FindElement(GetXPathForGridColumnHeaderSpanWithTextAndCssClass("vt-testedGrid2", "Column6")).Click();
            Thread.Sleep(2000);
            //Compare snapshots 
            CheckSnapshot(membersCategory, memberName, exampleId + "15-after-ClickColumn6Header-Sortable", testWindow);
        }
             
    }
}
