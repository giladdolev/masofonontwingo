﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ColumnHeaderCollectionTests
    {

        public void ListViewColumnHeaderCollectionAddInt2StringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Add a column with text and key to the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add ColumnHeader >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A New Column was added to the ListView at index 3."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-AddNewColumnWithTextAndKeyToTheEnd", testWindow);

            //Button click - Add a column with text and key to the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add ColumnHeader >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A New Column was added to the ListView at index 4."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-AddAnotherNewColumnToTheEnd", testWindow);

            //Button click - Remove an item with text and key from the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove ColumnHeader >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A Column was removed from the ListView at index 4."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-RemoveLastColumn", testWindow);

            //Button click - Remove an item with text and key from the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove ColumnHeader >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A Column was removed from the ListView at index 3."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-RemoveLastColumn", testWindow);

            //Button click - Remove an item with text and key from the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove ColumnHeader >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A Column was removed from the ListView at index 2."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-RemoveLastColumn", testWindow);

            //Button click - Add a column with text and key to the listview
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add ColumnHeader >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("A New Column was added to the ListView at index 3."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-AddNewColumnWithTextAndKeyToTheEnd", testWindow);

        }

    }
}
