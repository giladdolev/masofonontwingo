﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ComboBoxElementTests
    {
        //Bug 22376 - Fist time clicking combobox
        //Bug 9638 - When clicking on the comboBox and then clicking on the button, the new items isn't shown in the list
        public void ComboBoxListItemCollectionAddObjectFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Open ComboBox list - Bug 22376
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // wait for dropdown list to open
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickToOpenDropDownList", testWindow);

            
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();
            // wait for dropdown list to open
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SecondClickToOpenDropDownList", testWindow);

            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 1"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-AddItem", testWindow);

            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // wait for dropdown list item
            testWindow.FindElement(GetXPathForComboBoxItem("New Item"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickOnArrowToOpenDropDownList", testWindow);

            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 2"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-AddItem", testWindow);

            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // wait for dropdown list item
            testWindow.FindElement(GetXPathForComboBoxItem("New Item"));

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickOnArrowToOpenDropDownList", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

       

        public void ComboBoxListItemCollectionClearFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 1"));
             
            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 2"));

            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 3"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-AddItems", testWindow);

            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // wait for dropdown list item
            testWindow.FindElement(GetXPathForComboBoxItem("New Item"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnArrowToOpenDropDownList", testWindow);

            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Clear >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 0"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClearItems", testWindow);

            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickOnArrowToOpenDropDownList", testWindow);

        }
    }
}
