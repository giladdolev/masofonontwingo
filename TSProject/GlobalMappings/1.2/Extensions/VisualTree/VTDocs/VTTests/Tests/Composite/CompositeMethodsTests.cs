﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class CompositeElementTests
    {

        
        public void CompositePerformUnloadFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
        
             //Button click - to invoke PerformUnload
             testWindow.FindElement(GetXPathForButtonSpanWithText("PerformUnload >>")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("PerformUnload was invoked"));
             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-toInvokePerformUnload", testWindow);
            
         }
    }
}
