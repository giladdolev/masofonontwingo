﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class DropDownButtonElementTests
    {

        //27331 - DropDown Arrow Click event isn't invoked
        public void DropDownButtonBasicFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click 'Drop down button' button
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Drop down button")).Click();
            Thread.Sleep(1000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickingOnMainButton-MenuDoesNotOpen", testWindow);
            
            //Click 'Drop down button' arrow and open the entire menu
            testWindow.WaitForElement(GetXPathForDropDownButtonWithCssClass("vt-ddbtn-TestedDropDownButton")).Click();
            testWindow.WaitForElement(GetXPathForDropDownButtonWithCssClass("vt-ddbtn-TestedDropDownButton")).SendKeys(Keys.Tab + Keys.Enter + (Keys.ArrowDown + Keys.ArrowDown) + Keys.Enter + Keys.ArrowDown);
            Thread.Sleep(1000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickingOnMainButtonAndArrowAndOpenEntireMenu-AllMenuShouldOpen", testWindow);

            //Close the menu by clicking the label to view event log
            testWindow.WaitForElement(GetXPathForLabelWithText("True")).Click();
            Thread.Sleep(1000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClosingMenuToViewEventLog", testWindow);

            //Button click - Change DropDownArrows value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change DropDownArrows value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeDropDownArrowsToFalse-MainButtonWithNoArrow", testWindow);

            //Click 'Clear Event Log'
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Clear Event Log >>")).Click();
            //Click 'Drop down button' button again
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Drop down button")).Click();
            Thread.Sleep(1000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClearEventLogAndClickingOnMainButton-ClickEventIsInvoked", testWindow);
            

          

        }

        
    }

}
