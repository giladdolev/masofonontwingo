﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class WindowElementTests
    {

        public void WindowIconFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Icon 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Icon >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Desert.jpg"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeIconToDesertImage", testWindow);

            //Button Click - Change Icon
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Icon >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Tulips.jpg"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeIconToTulipsImage", testWindow);

            //Button Click - Change Icon
            testWindow.FindElement(GetXPathForButtonSpanWithText("Open New Win >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Window with"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-OpenNewWinWithIconSet", testWindow);
        }


        public void WindowHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("500px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo500px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("705px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo705px", testWindow);
        }

        // Bug 21946 - VT - Window EnableEsc Property - Initial value doesn't work
        // When the bug will be resolved needs to add also - opening Intialize window before clicking "ChangeEnableEsc" button  
        public void WindowEnableEscFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Open Default Window
            testWindow.FindElement(GetXPathForButtonSpanWithText("Open Default Window")).Click();

            //wait for window to open
            testWindow.WaitForElement(GetXPathForWindowWithId("EnableEscDefaultWindow"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickToOpenDefaultWindow", testWindow);

            // try to close window with Esc key
            testWindow.FindElement(GetXPathForWindowWithId("EnableEscDefaultWindow")).SendKeys(Keys.Escape);

            Thread.Sleep(2000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-EscToCloseDefaultWindow", testWindow);

            // close the window using the close button
            testWindow.FindElement(GetXPathForWindowCloseButtonWithId("EnableEscDefaultWindow")).Click();

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToCloseDefaultWindow", testWindow);

            // Initialized Window bug change EnableEsc to true.
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change EnableEsc Value >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": True."));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickToChangeEnableEscToTrue", testWindow);

            //Button Click - Open Initialized Window
            testWindow.FindElement(GetXPathForButtonSpanWithText("Open Initialized Window")).Click();

            //wait for window to open
            testWindow.WaitForElement(GetXPathForWindowWithId("EnableEscInitializedWindow"));

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickToOpenInitializedWindow", testWindow);

            testWindow.FindElement(GetXPathForWindowWithId("EnableEscInitializedWindow")).SendKeys(Keys.Escape);

            Thread.Sleep(2000);

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-EscToCloseInitializedWindow", testWindow);

        }

        // bug 9462 - ContextMenuStrip can't be set at runtime
        public void WindowContextMenuStripFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            Actions actions = new Actions(CurrentWebDriver);
            bool exampleSize = true;

            testWindow.FindElement(GetXPathForButtonSpanWithText("Open Default Window")).Click();

            IWebElement TestedWindow1_elem = testWindow.FindElement(GetXPathForWindowWithId("ContextMenuStripDefaultWindow"));
            //Right click TestedButton1
            actions.ContextClick(TestedWindow1_elem);
            actions.Perform();
            //Wait for contextMenu to open
            Thread.Sleep(3000);
            //Compare snapshots 1
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "1-after-rightClickDefaultWindow-DefaultBrowserMenu", testWindow, exampleSize);

            TestedWindow1_elem.SendKeys(Keys.Escape);

            Thread.Sleep(5000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-EscToCloseDefaultWindow", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Open Initialized Window")).Click();


            IWebElement TestedWindow2_elem = testWindow.FindElement(GetXPathForWindowWithId("ContextMenuStripInitializedWindow"));
            //Right click TestedButton2
            actions.ContextClick(TestedWindow2_elem);
            actions.Perform();
            //Wait for contextMenu to open
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-rightClickInitializedWindow-HelloMenuItem", testWindow);

            //Button click - to change contextMenuStrip
            testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip2")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("contextMenuStrip2."));
            //Right click TestedButton2
            actions.ContextClick(TestedWindow2_elem);
            actions.Perform();
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToContextMenuStrip2AndRightClickInitializedWindow-HelloWorldMenuItem", testWindow);

            //Button click - to change contextMenuStrip
            testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip1")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("contextMenuStrip1."));
            //Right click TestedButton2
            actions.ContextClick(TestedWindow2_elem);
            actions.Perform();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToContextMenuStrip1AndRightClickInitializedWindow-HelloMenuItem", testWindow);

            //Button click - to Reset contextMenuStrip
            testWindow.FindElement(GetXPathForButtonSpanWithText("Reset")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("null."));
            //Right click TestedButton2
            actions.ContextClick(TestedWindow2_elem);
            actions.Perform();
            //Compare snapshots 6
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "6-after-clickResetButtonAndRightClickInitializedWindow-DefaultBrowserMenu", testWindow, exampleSize);

            TestedWindow2_elem.SendKeys(Keys.Escape);
        }
    }
}
