﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class CompositeElementTests
    {

         // bug 9462 - ContextMenuStrip can't be set at runtime
        public void CompositeContextMenuStripFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
         {
             // 26650 - Changing Enable property of timer from inside Timer's Tick event handler, cause exception.
             // in order to verify that there is no exception, we add seep for 4 seconds (interval length + 1 sec) and take first snapshot 
             Thread.Sleep(4000);

             //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-VerifyNoExceptionBug26650", testWindow);

             Actions actions = new Actions(CurrentWebDriver);
             bool exampleSize = true;

             IWebElement TestedCmpst1_elem = testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-test-cmpst-1"));
             //Right click TestedButton1
             actions.ContextClick(TestedCmpst1_elem);
             actions.Perform();
             //Wait for contextMenu to open
             Thread.Sleep(3000);
             //Compare snapshots 2
             CheckSnapshotExtended(membersCategory, memberName, exampleId + "2-after-rightClickTestedCompositeView1-DefaultBrowserMenu", testWindow, exampleSize);

             // click on body
             testWindow.FindElement(By.Id("windowView-body")).Click();

             IWebElement TestedCmpst2_elem = testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-test-cmpst-2"));
             //Right click TestedButton2
             actions.ContextClick(TestedCmpst2_elem);
             actions.Perform();
             //Wait for contextMenu to open
             Thread.Sleep(2000);
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-rightClickTestedCompositeView2-HelloMenuItem", testWindow);

             //Button click - to change contextMenuStrip
             testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip2")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": contextMenuStrip2"));
             //Right click TestedButton2
             actions.ContextClick(TestedCmpst2_elem);
             actions.Perform();
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToContextMenuStrip2AndRightClickTestedCompositeView2-HelloWorldMenuItem", testWindow);

             //Button click - to change contextMenuStrip
             testWindow.FindElement(GetXPathForButtonSpanWithText("contextMenuStrip1")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText(": contextMenuStrip1"));
             //Right click TestedButton2
             actions.ContextClick(TestedCmpst2_elem);
             actions.Perform();
             //Compare snapshots 5
             CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToContextMenuStrip1AndRightClickTestedCompositeView2-HelloMenuItem", testWindow);

             //Button click - to Reset contextMenuStrip
             testWindow.FindElement(GetXPathForButtonSpanWithText("Reset")).Click();
             testWindow.WaitForElement(GetXPathForLabelWithText("value is null."));
             //Right click TestedButton2
             actions.ContextClick(TestedCmpst2_elem);
             actions.Perform();
             //Compare snapshots 6
             CheckSnapshotExtended(membersCategory, memberName, exampleId + "6-after-clickResetButtonAndRightClickTestedCompositeView2-DefaultBrowserMenu", testWindow, exampleSize);

             // click on body
             testWindow.FindElement(By.Id("windowView-body")).Click();
         }
    }
}
