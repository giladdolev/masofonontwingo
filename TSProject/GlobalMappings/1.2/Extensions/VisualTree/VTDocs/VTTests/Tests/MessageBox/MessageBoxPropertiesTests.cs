﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class MessageBoxTests
    {
        public void BasicMessageBoxFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Show MessageBox 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show MessageBox >>")).Click();

            //Wait 2 seconds
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickBtnShowMessageBox", testWindow);

            //Button Click - No
            testWindow.FindElement(GetXPathForButtonSpanWithText("No")).Click();

            //Wait 2 seconds
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickBtnNo", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("OK")).Click();

            //Wait 2 seconds
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickBtnOK", testWindow);
        }
         
    }
}
