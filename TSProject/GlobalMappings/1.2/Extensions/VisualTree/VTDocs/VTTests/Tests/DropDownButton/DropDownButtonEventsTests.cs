﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class DropDownButtonElementTests
    {


        public void DropDownButtonDropDownOpenedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click 'Drop down button' arrow and open the menu
            IWebElement element = testWindow.WaitForElement(GetXPathForDropDownButtonArrowWithCssClass("vt-ddbtn-TestedDropDownButton"));
            Actions action = new Actions(CurrentWebDriver);
            action.MoveToElement(element).Click().Perform();

            Thread.Sleep(1000);

            //Opening And Opened Events Should Invoke in this order
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickingOnTheTestedDropDownButtonArrow-OpenedEventInvoked", testWindow);

            //Click 'menuItem3' arrow and open the sub-menu
            element = testWindow.WaitForElement(GetXPathForDropDownButtonArrowWithCssClass("vt-ddbtn-TestedmenuItem3"));
            action.MoveToElement(element).Click().Perform();

            Thread.Sleep(1000);

            //Opening And Opened Events Should Invoke in this order
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickingOnTheTestedmenuItem3Arrow-OpenedEventInvoked", testWindow);
        }

        public void DropDownButtonDropDownOpeningFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click 'Drop down button' arrow and open the menu
            IWebElement element = testWindow.WaitForElement(GetXPathForDropDownButtonArrowWithCssClass("vt-ddbtn-TestedDropDownButton"));
            Actions action = new Actions(CurrentWebDriver);
            action.MoveToElement(element).Click().Perform();

            Thread.Sleep(1000);

            //Opening And Opened Events Should Invoke in this order
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickingOnTheTestedDropDownButtonArrow-OpeningAndOpenedEventsInvoked", testWindow);

            //Click 'menuItem3' arrow and open the sub-menu
            element = testWindow.WaitForElement(GetXPathForDropDownButtonArrowWithCssClass("vt-ddbtn-TestedmenuItem3"));
            action.MoveToElement(element).Click().Perform();

            Thread.Sleep(1000);

            //Opening And Opened Events Should Invoke in this order
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickingOnTheTestedmenuItem3Arrow-OpeningAndOpenedEventsInvoked", testWindow);
        }

        public void DropDownButtonDropDownClosedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click 'Drop down button' arrow and open the menu
            IWebElement element = testWindow.WaitForElement(GetXPathForDropDownButtonArrowWithCssClass("vt-ddbtn-TestedDropDownButton"));
            Actions action = new Actions(CurrentWebDriver);
            action.MoveToElement(element).Click().Perform();

            Thread.Sleep(1000);

            //Click 'menuItem3' arrow and open the sub-menu
            IWebElement elem = testWindow.WaitForElement(GetXPathForDropDownButtonArrowWithCssClass("vt-ddbtn-TestedmenuItem3"));
            action.MoveToElement(elem).Click().Perform();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickingOnTheTestedDropDownButtonArrowAndOnMenuItem3Arrow-AllMenuShouldOpen", testWindow);

            //Close the menu by clicking 'menuItem3' arrow
            action.MoveToElement(elem).Click().Perform();

            Thread.Sleep(1000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickingOnTheMenuItem3ArrowClosingMenu", testWindow);

            //Close the menu by clicking 'Drop down button' arrow
            action.MoveToElement(element).Click().Perform();

            Thread.Sleep(1000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickingOnTheTestedDropDownButtonArrowClosingMenu", testWindow);

        }

        
    }

}
