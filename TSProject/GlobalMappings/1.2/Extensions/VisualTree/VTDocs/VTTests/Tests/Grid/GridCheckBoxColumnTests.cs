﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridCheckBoxColumnTests
    {

        // Bug 24758
        public void GridGridCheckBoxColumnReadOnlyFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a checkbox cell
            testWindow.WaitForElement(GetXPathForGridCheckColumnWithId("GridCol1","0")).Click();

            Thread.Sleep(3000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-UnCheckTopLeftCheckBoxCell", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column1")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickSetColumnReadOnlyToSetColumn1ToTrue", testWindow);
            
            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickSetColumnReadOnlyToSetColumn1ToFalse", testWindow);

            //Clicking a checkbox cell
            testWindow.WaitForElement(GetXPathForGridCheckColumnWithId("GridCol1", "0")).Click();

            Thread.Sleep(3000);

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-CheckTopLeftCheckBoxCell", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column2")).Click();

            //testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickSetColumnReadOnlyToSetColumn2ToFalse", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();
            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column3")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickSetColumnReadOnlyToSetColumn3ToTrue", testWindow);

        }
 
    }
}
