﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class NumericUpDownElementTests
    {
        //Bug 22220 - ValueChanged event doesn't work so the log isn't updated. After bug fix delete the Sleep code
        //Bug 22225 - The option to enter a value manually to NumericUpDown is not implemented
        public void BasicNumericUpDownFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown1")).Click();

            //Using 5 times the arrow up on the keyboard, increment the value in 5
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown1")).SendKeys(Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The numeric value is: 5."));
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Using5ArrowUpWithKeyboard-click", testWindow);

            //Using 2 times the arrow down on the keyboard, decrement the value in 2
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown1")).SendKeys(Keys.ArrowDown + Keys.ArrowDown);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The numeric value is: 3."));
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Using2ArrowDownWithKeyboard-click", testWindow);

            //Click on the arrow up 
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown1")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The numeric value is: 4."));
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnArrowUp-click", testWindow);

            //Click on the arrow down 
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown1")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The numeric value is: 3."));
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickOnArrowDown-click", testWindow);

            //Button Click - Set numeric value to 2
            testWindow.FindElement(GetXPathForButtonWithText("Set value to 2 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 2."));
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-SetValueTo2-click", testWindow);

            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown1")).SendKeys(Keys.Backspace + "10" + Keys.Enter);
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The numeric value is: 10."));
            Thread.Sleep(2000);
            //Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-EnterValue10-click", testWindow);


        }

        public void NumericUpDownVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Take and Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToTrue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Take and Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFalse", testWindow);
           

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void NumericUpDownLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }

        public void NumericUpDownLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Left value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_290", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80_310", testWindow);
        }

        public void NumericUpDownWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("340px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo340px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("170px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo170px", testWindow);
        }

        public void NumericUpDownTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Top value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void NumericUpDownBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_200_50", testWindow);


            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            // Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_170_24", testWindow);
        }

        public void NumericUpDownClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ClentSize value
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo200_45", testWindow);


            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("170,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo170_24", testWindow);
        }
        
        public void NumericUpDownClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_200_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_170_24", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        
        public void NumericUpDownEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();

        }
        public void NumericUpDownTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change NumericUpDown Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".NumericUpDownElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToNumericUpDownElement", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void NumericUpDownBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change NumericUpDown BorderStyle 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void NumericUpDownPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo80px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("24."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo24px", testWindow);
        }
        public void NumericUpDownPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void NumericUpDownPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change NumericUpDown PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void NumericUpDownPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change NumericUpDown PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("340."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo340px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("170."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo170px", testWindow);
        }


        public void NumericUpDownHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo80px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("24px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo24px", testWindow);
        }


        public void NumericUpDownSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Size value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=170"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo170_24", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300_80", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void NumericUpDownMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=100, Height=120}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo100_120", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=10}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200_10", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=50, Height=80}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo50_80", testWindow);

            //Button Click - Cancel NumericUpDown MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }


        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void NumericUpDownMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=300, Height=70}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo300_70", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=150, Height=90}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo150_90", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=400, Height=50}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo400_50", testWindow);

            //Button Click - Cancel NumericUpDown MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }

        public void NumericUpDownFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Panel Font 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSketchFlow", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Calibri"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToCalibri", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToMiriam", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Font Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Niagara"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToNiagara", testWindow);
        }

        public void NumericUpDownMinimumFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Bug 22257 - after fix, delete this line. This bug is checked in the first snapshot taken.

            //Enter a value less than the minimum which is initially to -10
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace);
            //Entering value to the control, value less than the minimum that is -10
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("-20" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 23879, bug 22225- after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: -10."));
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-DeleteDefaultValueAndEnterValue-20", testWindow);

            //Enter a value between -10 to 10 
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value between -10 to 10
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("5" + Keys.Enter);
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 5."));
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-DeleteValueAndEnterValue5", testWindow);

            //ArrowDown from keyboard
            //Using the arrow down on the keyboard, press until the value is stopped at the minimum (-10)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown +  Keys.ArrowDown);
            //Wait for log label update
            //Bug 22220 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: -10."));
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-UsingArrowDownWithKeyboard11Times", testWindow);

            //Click ArrowDown into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("-9" + Keys.Enter);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-EnterValue-9", testWindow);
            //Using the arrow down into the control, press until the value is stopped at the minimum (-10)
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug 22220 and bug 22225- after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: -10."));
            Thread.Sleep(2000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickArrowDownIntoControloTwice", testWindow);

            //Button Click - Change numeric value to 4
            testWindow.FindElement(GetXPathForButtonWithText("Change Value to 4 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 4."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ChangeValueTo4", testWindow);

            //Click button to change the minimum to 1 and do the same tests as before
            //Button Click - Change minimum value to 1
            testWindow.FindElement(GetXPathForButtonWithText("Change Minimum to 1 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The minimum value is: 1."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ChangeMinimumValueTo1", testWindow);

            //Enter a value less than the new minimum which is 1
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace);
            //Entering value to the control, value less than the new minimum that is 1
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("0" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 23879, bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 1."));
            Thread.Sleep(2000);
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-DeleteValueAndEnterValue0", testWindow);

            //Enter a value between 1 to 10
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace);
            //Entering value to the control, value between 1 to 10
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("7" + Keys.Enter);
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 7."));
            Thread.Sleep(2000);
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-DeleteValueAndEnterValue7", testWindow);

            //ArrowDown from keyboard
            //Using the arrow down on the keyboard, press until the value is stopped at the minimum (1)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.ArrowDown+Keys.ArrowDown+Keys.ArrowDown+Keys.ArrowDown+Keys.ArrowDown+Keys.ArrowDown+Keys.ArrowDown+Keys.ArrowDown);
            //Wait for log label update
            //Bug 22220 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 1."));
            Thread.Sleep(2000);
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-UsingArrowDownWithKeyboard8Times", testWindow);

            //ArrowDown into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("4" + Keys.Enter);
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-EnterValue4", testWindow);
            //Using the arrow down into the control, press until the value is stopped at the minimum (1). Click 4 times until it will stop at the minimum
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 1."));
            Thread.Sleep(2000);
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-DeleteValueAndClickArrowDownIntoControl4Tomes", testWindow);

            //Click button to change the minimum to -15 and do the same tests as before
            //Button Click - Change minimum value to -15
            testWindow.FindElement(GetXPathForButtonWithText("Change Minimum to -15 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The minimum value is: -15."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-ChangeMinimumValueTo-15", testWindow);

            //Enter a value less than the new minimum which is -15
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value less than the new minimum that is -15
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("-30" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 23879, bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: -15."));
            Thread.Sleep(2000);
            //Compare snapshots 14
            CheckSnapshot(membersCategory, memberName, exampleId + "14-after-DeleteVlaueAndEnterValue-30", testWindow);

            //Enter a value between -15 to 10
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value between -15 to 10
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("2" + Keys.Enter);
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 2."));
            Thread.Sleep(2000);
            //Compare snapshots 15
            CheckSnapshot(membersCategory, memberName, exampleId + "15-after-DeleteValueAndEnterValue2", testWindow);

            //ArrowDown from keyboard
            //Using the arrow down on the keyboard, press until the value is stopped at the minimum (-15)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown);
            //Wait for log label update
            //Bug 22220 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: -15."));
            Thread.Sleep(2000);
            //Compare snapshots 16
            CheckSnapshot(membersCategory, memberName, exampleId + "16-after-UsingArrowDownWithKeyboard18Times", testWindow);

            //ArrowDown into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("-12" + Keys.Enter);
            //Compare snapshots 17
            CheckSnapshot(membersCategory, memberName, exampleId + "17-after-EnterValue-12", testWindow);
            //Using the arrow down into the control, press until the value is stopped at the minimum (-15). Click 3 times until it will stop at the minimum
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: -15."));
            Thread.Sleep(2000);
            //Compare snapshots 18
            CheckSnapshot(membersCategory, memberName, exampleId + "18-after-DeleteValueAndClickArrowDown4Times", testWindow);

        }

        public void NumericUpDownMaximumFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Bug 22257 - after fix, delete this line. This bug is checked in the first snapshot taken.

            //ArrowDown from keyboard - To Check bug 25158
            //Using the arrow down on the keyboard, press until the value is stopped at the minimum (1)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.ArrowDown);
            //Wait for log label update
            //Bug 22220, bug 22225, bug 25158 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 1."));
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-UsingArrowDownToCheckValueIsnNotLessThan1", testWindow);

            //Enter a value greater than the maximum which is initially set to 10
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Bug 25158 - after fix delete one backspace from the bottom
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value greater than the maximum that is 10
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("20" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 22225- after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 10."));
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-EnterValue20GreaterThenMaximum", testWindow);

            //Enter a value between 1 to 10 
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value between 1 to 10
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("5" + Keys.Enter);
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 5."));
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-DeleteValueAndEnterValue5", testWindow);

            //ArrowUp from keyboard
            //Using the arrow up on the keyboard, press until the value is stopped at the maximum (10)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp);
            //Wait for log label update
            //Bug 22220, bug 22225 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 10."));
            Thread.Sleep(2000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-UsingArrowUpWithKeyboard6Times", testWindow);

            //Click ArrowUp into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("7" + Keys.Enter);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-DeleteValueAndEnterValue7", testWindow);
            //Using the arrow up into the control, press until the value is stopped at the maximum (10). Click 4 times to make sure it is stopped at the maximum
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug 22220 and bug 22225- after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 10."));
            Thread.Sleep(2000);
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickArrowUpIntoControlUntilTheMaximum4Times", testWindow);

            //Button Click - Change numeric value to 4
            testWindow.FindElement(GetXPathForButtonWithText("Change Value to 4 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 4."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickButtonToChangeValueTo4", testWindow);

            //Click button to change the maximum to 100 and do the same tests as before
            //Button Click - Change maximum value to 100
            testWindow.FindElement(GetXPathForButtonWithText("Change Maximum value to 100 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The maximum value is: 100."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickButtonToChangeMaximumValueTo100", testWindow);

            //Enter a value greater than the new maximum which is 100
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value greater than the new maximum that is 100
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("200" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 100."));
            Thread.Sleep(2000);
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-DeleteValueAndEnterValue200", testWindow);

            //Enter a value between 1 to 100
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value between 1 to 100
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("98" + Keys.Enter);
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 90."));
            Thread.Sleep(2000);
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-DeleteValueAndEnterValue98", testWindow);

            //ArrowUp from keyboard
            //Using the arrow up on the keyboard, press until the value is stopped at the maximum (100)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp);
            //Wait for log label update
            //Bug 22220 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 100."));
            Thread.Sleep(2000);
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-UsingArrowUp3TimesWithKeyboard", testWindow);

            //ArrowUp into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("99" + Keys.Enter);
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-DeleteValueAndEnterValue99", testWindow);
            //Using the arrow up into the control, press until the value is stopped at the maximum (100)
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 100."));
            Thread.Sleep(2000);
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-ClickArrowUpIntoControl2TimesUntilTheMaximum100", testWindow);

            //Click button to change the maximum to 6 and do the same tests as before
            //Button Click - Change maximum value to 6
            testWindow.FindElement(GetXPathForButtonWithText("Change Maximum value to 6 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The maximum value is: 6."));
            //Compare snapshots 14
            CheckSnapshot(membersCategory, memberName, exampleId + "14-after-ChangeMaximumValueTo6", testWindow);

            //Enter a value greater than the new maximum which is 6
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace);
            //Entering value to the control, value greater than the new maximum that is 6
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("10" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 23879, bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 6."));
            Thread.Sleep(2000);
            //Compare snapshots 15
            CheckSnapshot(membersCategory, memberName, exampleId + "15-after-DeleteValueAndEnterValue10", testWindow);

            //Enter a value between 1 to 6
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value between 1 to 6
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("2" + Keys.Enter);
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 2."));
            Thread.Sleep(2000);
            //Compare snapshots 16
            CheckSnapshot(membersCategory, memberName, exampleId + "16-after-DeleteValueAndEnterValue2", testWindow);

            //ArrowUp from keyboard
            //Using the arrow up on the keyboard, press until the value is stopped at the maximum (6)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp);
            //Wait for log label update
            //Bug 22220 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 6."));
            Thread.Sleep(2000);
            //Compare snapshots 17
            CheckSnapshot(membersCategory, memberName, exampleId + "17-after-UsingArrowUp5TimesWithKeyboard", testWindow);

            //ArrowUp into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("5" + Keys.Enter + Keys.Tab);
            //Compare snapshots 18
            CheckSnapshot(membersCategory, memberName, exampleId + "18-after-DeleteValueAndEnterValue5", testWindow);
            //Using the arrow up into the control, press until the value is stopped at the maximum (6). Click once until it will stop at the maximum
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug 22220 and bug 22225 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 6."));
            Thread.Sleep(2000);
            //Compare snapshots 19
            CheckSnapshot(membersCategory, memberName, exampleId + "19-after-ClickArrowUpIntoControlTwiceUntilTheMaximum6", testWindow);

        }

        public void NumericUpDownValueFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Bug 23872 - after fix, delete this line. This bug is checked in the first snapshot taken.

            //Button Click - Change value to 50
            testWindow.FindElement(GetXPathForButtonWithText("Change Value to 50 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 50."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonToChangeValueTo50", testWindow);

            //Enter a value between the default minimum and maximum, between 0 to 100
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value between the default minimum/ maximum values, between 0 to 100
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("70" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 70."));
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-DeleteValueAndEnterValue70", testWindow);

            //Enter a value greater than the default maximum value which is 100
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value greater than 100
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("110" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 22225 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 100."));
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-DeleteValueAndEnterValue110", testWindow);

            //Enter a value smaller than the default minimum value which is 0
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value smaller than 0
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("-1" + Keys.Enter);
            //Wait for log label update
            //Bug 22220, bug 22225, bug 23886, 23879 - after fix, uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 0."));
            Thread.Sleep(2000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-DeleteValueAndEnterValue-1", testWindow);

            //Bug 23886 - After fix, delete one Backspace from the bottom line
            //ArrowUp from keyboard
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value closed to the default maximum value
            //Using the arrow up on the keyboard, press until the value is stopped at the default maximum (100)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("98" + Keys.ArrowUp + Keys.ArrowUp + Keys.ArrowUp);
            //Wait for log label update
            //Bug 22220, bug 23886 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 100."));
            Thread.Sleep(2000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-DeleteAndEnterValue98ArrowUpKeyboard3TimesToValue100", testWindow);

            //ArrowUp into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("98");
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-DeleteValueAndEnterValue98", testWindow);
            //Using the arrow up into the control, press until the value is stopped at the maximum value (100). Click 3 times until it will stop at the maximum
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowUpWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug bug 22220, bug 23886 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 100."));
            Thread.Sleep(2000);
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-DeleteAndEnterValue98ArrowUpIntoControl3TimesToValue100", testWindow);

            //ArrowDown from keyboard
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control, value closed to the minimum default value
            //Using the arrow down on the keyboard, press until the value is stopped at the default minimum value (0)
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("3" + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown + Keys.ArrowDown);
            //Wait for log label update
            //Bug 22220, bug 23886 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 0."));
            Thread.Sleep(2000);
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-DeleteAndEnterValue3ArrowDownKeyboard3TimesToValue0", testWindow);

            //ArrowDown into the control
            //Click on the control - Focus on the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).Click();
            //Bug 23886 - after fix Delete one Backspace from the bottom line
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("3");
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-DeleteValueAndEnterValue3", testWindow);
            //Using the arrow down into the control, press until the value is stopped at the minimum value (0). Click 4 times until it will stop at the minimum
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            testWindow.FindElement(GetXpathForNumericUpDownArrowDownWithCssClassAndClass("vt-TestedNumericUpDown2")).Click();
            //Wait for log label update
            //Bug bug 22220, bug 23886 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 0."));
            Thread.Sleep(2000);
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-DeleteAndEnterValue3ArrowDownIntoControl3TimesToValue0", testWindow);

            //Bug 23886 - after fix Delete one Backspace from the bottom line
            //Enter the character ' + '
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace);
            //Entering the character ' + ' to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("+!@#$%^&*_/\\:;[]()" + Keys.Enter);
            //Wait for log label update
            //Bug bug 23891 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 0."));
            Thread.Sleep(2000);
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-DeleteValueAndEnterVarietyOfCharacters-CharactersDoesn'tAppear", testWindow);

            //Button Click - Change maximum value to 1500
            testWindow.FindElement(GetXPathForButtonWithText("Change maximum value to 1500 >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("The maximum value is: 1500."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-ClickButtonToChangeMaximumValueTo1500", testWindow);
            //Entering number greater than 1000 to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("1,300" + Keys.Enter);
            //Wait for log label update
            //Bug bug 25164 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 1300."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-TypeValue1300WithComma", testWindow);

            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("6.1" + Keys.Enter);
            //Wait for log label update
            //Bug bug 23892, 22220 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 6.1."));
            //Compare snapshots 14
            CheckSnapshot(membersCategory, memberName, exampleId + "14-after-DeleteValueAndEnterValue6.1IntoControlDisplaysValue6", testWindow);

            //Bug 23892 - after fix Delete two Backspace from the bottom line
            //Delete the value in the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys(Keys.Backspace + Keys.Backspace + Keys.Backspace);
            //Entering value to the control
            testWindow.FindElement(GetXPathForNumericUpDownWithCssClass("vt-TestedNumericUpDown2")).SendKeys("6.5" + Keys.Enter);
            //Wait for log label update
            //Bug bug 23892, 22220 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("The value is: 6.5."));
            //Compare snapshots 15
            CheckSnapshot(membersCategory, memberName, exampleId + "15-after-DeleteValueAndEnterValue6.5IntoControlDisplaysValue7", testWindow);
        }

    }
}
