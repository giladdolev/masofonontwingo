﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridComboBoxColumnTests
    {
        //27487 - ExpandOnFocus doesn't work correctly
        public void GridGridComboBoxColumnExpandOnFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //2 steps for bug 26174 - scrolling down  in grid with tab key - verify the comboBox list expanded
            testWindow.FindElement(GetXPathForGridCellWithIdAndText("OptionsColumn1", "Option3")).Click();
            testWindow.FindElement(GetXPathForGridEditableCellWithIdAndText("OptionsColumn1", "Option3")).SendKeys(Keys.Tab + Keys.Tab + Keys.Tab + Keys.Tab + Keys.Tab + Keys.Tab);
            //Compare snapshots 1
             CheckSnapshot(membersCategory, memberName, exampleId + "1-after-StartingFromCell03AndPressing5TimesTab-Cell04ComboListIsexpanded", testWindow);


             //Pressing twice on Tab key to focus 'Gender' cell - verify the list isn't expanded- the default is false..
             testWindow.FindElement(GetXPathForGridCellWithIdAndText("OptionsColumn1", "Option2")).Click();
             testWindow.FindElement(GetXPathForGridEditableCellWithIdAndText("OptionsColumn1", "Option2")).SendKeys(Keys.Tab + Keys.Tab );
             //Compare snapshots 2
             CheckSnapshot(membersCategory, memberName, exampleId + "2-after-FocusingGenderCell-TheListIsNotExpanded", testWindow);


             //Button click - Change Options Column ExpandOnFocus to false
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Options Column ExpandOnFocus value >>")).Click();
             //Click to focus 'Gender' cell (0,2)
             testWindow.FindElement(GetXPathForGridCellWithIdAndText("OptionsColumn1", "Option2")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Options column ExpandOnFocus value: False"));
             //Compare snapshots 3
             CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeOptionsColumnExpandToFalse-FocusingOptionsCell-TheListIsNotExpanded", testWindow);


             //Button click - Change Options Column ExpandOnFocus to false
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Options Column ExpandOnFocus value >>")).Click();
             //Click to focus 'Gender' cell (0,2)
             testWindow.FindElement(GetXPathForGridCellWithIdAndText("OptionsColumn1", "Option2")).Click();
             //Wait for log label update
             testWindow.WaitForElement(GetXPathForLabelWithText("Options column ExpandOnFocus value: True"));
             //Compare snapshots 4
             CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeOptionsColumnExpandToTrue-FocusingOptionsCell-TheListIsExpanded", testWindow);

             //Button click - Change Options Column ExpandOnFocus to false
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Gender Column ExpandOnFocus value >>")).Click();
             //Click to focus 'Gender' cell (0,2)
             testWindow.FindElement(GetXPathForGridCellWithIdAndText("GenderColumn1", "female")).Click();
             //Bug 27487 - after fix delete Sleep
             //Wait for log label update
             //testWindow.WaitForElement(GetXPathForLabelWithText("Options column ExpandOnFocus value: True"));
             Thread.Sleep(1000);
             //Compare snapshots 5
             CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ChangeGenderColumnExpandToTrue-FocusingGenderCell-TheListIsExpanded", testWindow);

             //Button click - Change Options Column ExpandOnFocus to false
             testWindow.FindElement(GetXPathForButtonSpanWithText("Change Gender Column ExpandOnFocus value >>")).Click();
             //Click to focus 'Gender' cell (0,2)
             testWindow.FindElement(GetXPathForGridCellWithIdAndText("GenderColumn1", "female")).Click();
             //Bug 27487 - after fix delete Sleep
             //Wait for log label update
             //testWindow.WaitForElement(GetXPathForLabelWithText("Options column ExpandOnFocus value: False"));
             Thread.Sleep(1000);
             //Compare snapshots 6
             CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ChangeGenderColumnExpandToFalse-FocusingGenderCell-TheListIsNotExpanded", testWindow);

        }


        // Bug 24758
        public void GridGridComboBoxColumnReadOnlyFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Grid cell click and cell input click
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1_2", "male")).Click();

            IWebElement elemComboCell = testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol1_2", "male"));
            elemComboCell.Click();
            elemComboCell.SendKeys("a");

            Thread.Sleep(3000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Type-a-OnTopLeftComboBoxCellSeeArrow", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column1")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickSetColumnReadOnlyToSetColumn1ToTrue", testWindow);

            //Grid cell click and cell input click
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "malea")).Click();

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnTopLeftComboBoxCellSeeNoArrow", testWindow);

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickSetColumnReadOnlyToSetColumn1ToFalse", testWindow);

            //Grid cell click and cell input click
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "malea")).Click();

            elemComboCell = testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol1", "malea"));
            elemComboCell.Click();
            elemComboCell.SendKeys("a");

            Thread.Sleep(3000);

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-Type-a-OnTopLeftComboBoxCellSeeArrow", testWindow);

            // Open dropdown list of the ComboBox Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column2")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickSetColumnReadOnlyToSetColumn2ToFalse", testWindow);

            //Open the dropdown list
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectColumn")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Column3")).Click();

            testWindow.FindElement(GetXPathForCheckBoxWithText("ReadOnly")).Click();

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Column ReadOnly >>")).Click();

            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickSetColumnReadOnlyToSetColumn3ToTrue", testWindow);

        }

        public void GridGridComboBoxColumnGetItemDataFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectPupil")).Click();
            testWindow.FindElement(GetXPathForComboBoxItem("Eli")).Click();

            //Wait For dropdown list
            Thread.Sleep(1000);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Get ItemData >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("c-code: 42"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectEliInComboBoxAndClickGetItemDataBtn", testWindow);

            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectPupil")).Click();
            testWindow.FindElement(GetXPathForComboBoxItem("Sara")).Click();

            //Wait For dropdown list
            Thread.Sleep(1000);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Get ItemData >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("c-code: 54"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectSaraInComboBoxAndClickGetItemDataBtn", testWindow);

        }

        public void GridGridComboBoxColumnSetItemDataFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectPupil")).Click();
            testWindow.FindElement(GetXPathForComboBoxItem("Eli")).Click();

            //Wait For dropdown list
            Thread.Sleep(1000);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Get ItemData >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("c-code: 42"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectEliInComboBoxAndClickGetItemDataBtn", testWindow);

            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbSelectClass")).Click();
            testWindow.FindElement(GetXPathForComboBoxItem("English")).Click();

            //Wait For dropdown list
            Thread.Sleep(1000);

            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-tbClassCode")).SendKeys(Keys.Control + "a" + Keys.Delete);
            testWindow.FindElement(GetXPathForTextBoxWithCssClass("vt-tbClassCode")).SendKeys("88");

            //Wait For dropdown list
            Thread.Sleep(2000);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set ItemData >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("successfully"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectEnglishClassInComboBoxAndType88AndClickSetItemDataBtn", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Get ItemData >>")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("c-code: 88"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickGetItemDataBtnForEliVerify88CCode", testWindow);



        }
    }
}
