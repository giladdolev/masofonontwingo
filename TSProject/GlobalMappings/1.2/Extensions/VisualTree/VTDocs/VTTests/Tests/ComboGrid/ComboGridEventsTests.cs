﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ComboGridElementTests
    {

        // Bug 27409 - when entering text manually , the last letter in the text property is uppercase
        // Bug 23987 -  event now invoked on backspace but Text propert has "gibrish" ch
        public void ComboGridTextChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboBox Text to "Some Text"
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: Some Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSomeText", testWindow);
            //Button Click - Change ComboBox Text to "New Text"
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: New Text"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewText", testWindow);

            //Manually adding text
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).SendKeys("x");

            //When Bug 27409 is fixed, replace this with next code line
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New TextX"));
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("New Textx"));

            //after typing "x" text should change to "NewTextx"
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeManuallyToNewTextx", testWindow);

            // 23987 - When pressing Backspace the event doesn't invoke
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).SendKeys(Keys.Backspace + ".");

            //after typing Backspace + "." text should change to "New Text." and 2 lines should be added to the event log
            //When Bug 23987 is fixed, replace this sleep with next code line
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("New Text."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeManuallyToNewTextDot", testWindow);

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-OpenDropDownPortion", testWindow);

            // select the Item In Index 1 by clicking "Bob" cell, the selected text should change to 2
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "Bob")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: 2"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickOnBobCellToSelectItemInIndex1DisplayMember2", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ComboGridSelectedIndexChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmbgrid")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropDownPortion", testWindow);

            // select the Item In Index 0 by clicking "James" cell, the selected text should change to 1
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "James")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedIndex value: 0"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnJamesCellToSelectItemInIndex0WithDisplayMember1", testWindow);

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmbgrid")).Click();

            // select the Item In Index 1 by clicking "Bob" cell, the selected text should change to 2
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "Bob")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedIndex value: 1"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnBobCellToSelectItemInIndex1WithDisplayMember2", testWindow);

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmbgrid")).Click();

            // select the Item In Index 2 by clicking "Dana" cell, the selected text should change to 3
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "Dana")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedIndex value: 2"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickOnDanaCellToSelectItemInIndex2WithDisplayMember3", testWindow);

            // Click on "Change SelectedIndex Value" button to select Item In Index 0 With DisplayMember 1
            testWindow.FindElement(GetXPathForButtonWithText("Change SelectedIndex Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedIndex value: 0"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickOnChangeSelectedIndexBtnToChooseItemInIndex0WithDisplayMember1", testWindow);

            // Click on "Change SelectedIndex Value" button to select Item In Index 1 With DisplayMember 2
            testWindow.FindElement(GetXPathForButtonWithText("Change SelectedIndex Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedIndex value: 1"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickOnChangeSelectedIndexBtnToChooseItemInIndex1WithDisplayMember2", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

    }
}
