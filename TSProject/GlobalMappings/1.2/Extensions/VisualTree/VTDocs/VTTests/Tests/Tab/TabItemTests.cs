﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class TabElementTests
    {

        
        public void TabTabItemBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("[Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToOrange", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("[Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);
        }
        public void TabTabItemBackgroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change TabItem BackgroundImage 
            testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("No background image."));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BackgroundImage >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Set with"));
            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);
        }

        public void TabTabItemBackgroundImageLayoutFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change TabItem BackgroundImageLayout 
            testWindow.FindElement(GetXPathForButtonWithText("Zoom")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Zoom."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToZoom", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Tile")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Tile."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTile", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Stretch")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Stretch."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToStretch", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Center")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Center."));
            //Compare snapshots 4 
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToCenter", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("None")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToNone", testWindow);
        }

        public void TabTabItemBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change tabItem3 BorderStyle value
            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem1 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);

            //Switch to tabItem2 tab
            testWindow.FindElement(GetXPathForTabPageWithText("tabItem2")).Click();
            
            //Button Click - Change tabItem2 BorderStyle value
            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Dotted."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "14-after-changeToDotted", testWindow);

            //Button Click - Change tabItem2 BorderStyle value
            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "15-after-changeToDouble", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "16-after-changeToFixed3D", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "17-after-changeToFixedSingle", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "18-after-changeToGroove", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "19-after-changeToInset", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "20-after-changeToDashed", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "21-after-changeToNotSet", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "22-after-changeToOutset", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "23-after-changeToRidge", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "24-after-changeToShadowBox", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "25-after-changeToSolid", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "26-after-changeToUnderline", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change BorderStyle of tabItem2 >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "27-after-changeToNone", testWindow);

        }
        public void TabTabItemTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change TabItem Text
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text of tabItem2 to >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToNewText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text of tabItem2 to >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText(": tabItem2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTextTotabItem2", testWindow);
        }

        //Bug 23509 - TabItems.ForeColor property doesn't work
        public void TabTabItemForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Selecting 'tabItem4' by clicking on it
           // testWindow.FindElement(GetXpathForExampleTabItem("tabPage4")).Click();
            //Button Click - Change tabPage4 ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change tabPage4 ForeColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("tabPage4 ForeColor value: Color [Blue]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToBlue", testWindow);
            //Button Click - Change tabPage4 ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change tabPage4 ForeColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("tabPage4 ForeColor value: Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);

            //Selecting 'tabItem5' by clicking on it
            testWindow.FindElement(GetXpathForExampleTabItem("tabPage5")).Click();
            //Button Click - Change tabPage5 ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change tabPage5 ForeColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("tabPage5 ForeColor value: Color [Blue]"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeToBlue", testWindow);
            //Button Click - Change tabPage5 ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change tabPage5 ForeColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("tabPage5 ForeColor value: Color [Green]"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeToGreen", testWindow);


            //Selecting 'tabItem6' by clicking on it
            testWindow.FindElement(GetXpathForExampleTabItem("tabPage6")).Click();
            //Button Click - Change tabPage6 ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change tabPage6 ForeColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("tabPage6 ForeColor value: Color [Blue]"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ChangeToBlue", testWindow);
            //Button Click - Change tabPage6 ForeColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change tabPage6 ForeColor >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("tabPage6 ForeColor value: Color [Green]"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ChangeToGreen", testWindow);
        }

        // Bug 9343 - Tabs of TabElement are in reverse order
        public void TabTabItemIndexFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForTabPageWithText("tabPage3")).Click();

            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Text: tabPage3"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectTabPage3Manually", testWindow);

            testWindow.FindElement(GetXPathForTabPageWithText("tabPage2")).Click();

            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Text: tabPage2"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectTabPage2Manually", testWindow);

            testWindow.FindElement(GetXPathForTabPageWithText("tabPage1")).Click();

            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Text: tabPage1"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectTabPage1Manually", testWindow);

            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Items.Add"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-AddItem", testWindow);

            testWindow.FindElement(GetXPathForTabPageWithText("New Tab")).Click();

            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Text: New Tab"));

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-SelectNewTabManually", testWindow);

        }
       
    }
}
