﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class SplitContainerTests
    {

        public void SplitContainerPanel1CollapsedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Button Click - Change Panel1Collapsed value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Panel1Collapsed value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 1 
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToFalse", testWindow);


            //Button Click - Change Panel1Collapsed value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Panel1Collapsed value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTrue", testWindow);
           

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void SplitContainerHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo80px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("120px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo120px", testWindow);
        }
        public void SplitContainerLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void SplitContainerLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Change SplitContainer Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft200Top290", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top310", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //27464 - When changing splitContainer size half of the SplitContainer disappears
        public void SplitContainerSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - change SplitContainer Size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-ChangeTo200_80", testWindow);

            //Button Click - change SplitContainer Size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeTo300_120", testWindow);

            //Button Click - change SplitContainer Size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-ChangeTo200_80", testWindow);
        }
        public void SplitContainerTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("310px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void SplitContainerWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("400px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo400px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200px", testWindow);
        }

        //27464 - When changing splitContainer size half of the SplitContainer disappears
        public void SplitContainerBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change SplitContainer Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-ChangeBoundsTo100_340_250_50", testWindow);

            //Button Click - Change SplitContainer Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeBoundsTo80_355_200_80", testWindow);

            //Button Click - Change SplitContainer Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-ChangeBoundsTo100_340_250_50", testWindow);
        }
        //27464 - When changing splitContainer size half of the SplitContainer disappears
        public void SplitContainerClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ClentSize value
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-ChangeSizeTo300_45", testWindow);

            //Button Click - Change ClentSize value
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeSizeTo200_80", testWindow);

            //Button Click - Change ClentSize value
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300,"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-ChangeSizeTo300_45", testWindow);
        }
        public void SplitContainerBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change SplitContainer BackColor 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToOrange", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        //27464 - When changing splitContainer size half of the SplitContainer disappears
        public void SplitContainerClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change SplitContainer ClientRectangle 
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-ChangeBoundsTo100_310_300_50", testWindow);

            //Button click - Change SplitContainer ClientRectangle 
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-ChangeBoundsTo80_320_200_80", testWindow);

            //Button click - Change SplitContainer ClientRectangle 
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for Log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-ChangeBoundsTo100_310_300_50", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void SplitContainerVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void SplitContainerEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();

        }
        public void SplitContainerTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change SplitContainer Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".SplitContainer"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSplitContainer", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void SplitContainerPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo80px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("120."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo120px", testWindow);
        }
        public void SplitContainerPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void SplitContainerPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change SplitContainer PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void SplitContainerPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change SplitContainer PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("400."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo400px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("200."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200px", testWindow);
        }
        //Bug 19676 - SplitContainer Background  doesn't work.
        public void SplitContainerBackGroundImageFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change SplitContainer BackgroundImage 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Split BackgroundImage Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("W/O background image."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetToNull", testWindow);
            
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Split BackgroundImage Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("set with background image."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetWithImage", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void SplitContainerBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            //Button Click - Change BorderStyle value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 9879:VT - When setting SplitContainer.Panel2Collapsed to true there is no way back
        public void SplitContainerPanel2CollapsedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Panel1Collapsed value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Panel2Collapsed value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 1 
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToFalse", testWindow);


            //Button Click - Change Panel1Collapsed value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Panel2Collapsed value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToTrue", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 9878:VT - SplitContainer Orientation Property - Orientation.Horizontal is shown as Orientation.Vertical and vice versa
        public void SplitContainerOrientationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Orientation value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Orientation value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Vertical."));
            //Compare snapshots 1 
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToVertical", testWindow);


            //Button Click - Change Orientation value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Orientation value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Horizontal."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToHorizontal", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void SplitContainerForeColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change ForeColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Blue]."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToBlue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ForeColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void SplitContainerFixedPanelFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click Increase Container Width - Panel1 is Fixed
            testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-btn-inc")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickIncreaseBtnPanel1Fixed", testWindow);

            //Button Click reset Width - Panel1 is Fixed
            testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-btn-reset")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickResetBtnPanel1Fixed", testWindow);

            //Button Click Change FixedPanel - Panel2 is Fixed
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change FixedPanel value >>")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickChangeBtnPanel2Fixed", testWindow);

            //Button Click Increase Container Width - Panel2 is Fixed
            testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-btn-inc")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickIncreaseBtnPanel2Fixed", testWindow);

            //Button Click reset Width - Panel2 is Fixed
            testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-btn-reset")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickResetBtnPanel2Fixed", testWindow);

            //Button Click Change FixedPanel to None
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change FixedPanel value >>")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickChangeBtnNoneFixed", testWindow);

            //Button Click Increase Container Width
            testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-btn-inc")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickIncreaseBtnNoneFixed", testWindow);

            //Button Click reset Width
            testWindow.FindElement(GetXpathForControlsWithCssClassOnly("vt-btn-reset")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickResetBtnNoneFixed", testWindow);
        }
    }
}

