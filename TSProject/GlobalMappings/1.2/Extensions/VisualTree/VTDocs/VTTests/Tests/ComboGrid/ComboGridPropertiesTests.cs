﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ComboGridElementTests
    {

        public void ComboGridBasicFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //----------------------------------------------------------------------------------------------------------------
            // Testing Bug 25032 AutoComplete functionality
            //----------------------------------------------------------------------------------------------------------------
            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropdownToCheckInitialValueSelection", testWindow);

            //Manually adding text "34"
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).SendKeys("34");

            Thread.Sleep(2000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Type34TheDropdownOpensAndShowsOnly34512Row", testWindow);

            //Manually adding exact match text "512"
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).SendKeys("512");

            Thread.Sleep(2000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Type512TheDropdownOpensAnd34512RowIsMarkedBlue", testWindow);
            //----------------------------------------------------------------------------------------------------------------
        }

        //Bug 27409
        public void ComboGridTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change ComboGrid Text to "New Text"
            testWindow.FindElement(GetXPathForButtonWithText("Change Text Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-clickButton-ChangeTextToNewText", testWindow);

            //Button Click - Set Text to ""
            testWindow.FindElement(GetXPathForButtonWithText("Empty Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: "));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-clickButton-toEmptyText", testWindow);


            //Button Click - Change ComboGrid Text to "TestedComboGrid"
            testWindow.FindElement(GetXPathForButtonWithText("Change Text Value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: TestedComboGrid"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-clickButton-ChangeTextToTestedComboGrid", testWindow);

            //Manually adding text "xx"
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).SendKeys("xx");
            //Remove comment after bug 27409 fix
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("TestedComboGridxx"));
            Thread.Sleep(1000);
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ChangeTextManuallyToTestedComboGridxx", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ComboGridSelectedIndexFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropdownToCheckInitialValueSelection", testWindow);

            // select the Item In Index 2 by clicking "Dana" cell, the selected text should change to 3
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "Dana")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 2."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnDanaCellToSelectItemInIndex2WithDisplayMember3", testWindow);

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-OpenDropdownToCheckMarkedSelection", testWindow);

            // Click on "Change SelectedIndex Value" button to select Item In Index 0 With DisplayMember 1
            testWindow.FindElement(GetXPathForButtonWithText("Change SelectedIndex Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 0."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickOnChangeSelectedIndexBtnToChooseItemInIndex0WithDisplayMember1", testWindow);

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-OpenDropdownToCheckMarkedSelection", testWindow);

            // Click on "Change SelectedIndex Value" button to select Item In Index 1 With DisplayMember 2
            testWindow.FindElement(GetXPathForButtonWithText("Change SelectedIndex Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is: 1."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickOnChangeSelectedIndexBtnToChooseItemInIndex1WithDisplayMember2", testWindow);
        }

        public void ComboGridDroppedDownFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change DroppedDown value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change DroppedDown value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrue", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change DroppedDown value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalse", testWindow);
        }

        public void ComboGridPreviousValueFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropDownPortion", testWindow);

            // select the Item In Index 0 by clicking "James" cell, the selected text should change to 1
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "James")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("SelectedIndex Value: 0"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnJamesCellToSelectItemInIndex0WithDisplayMember1", testWindow);

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            // select the Item In Index 1 by clicking "Bob" cell, the selected text should change to 2
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "Bob")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("PreviousValue Value: James"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnBobCellToSelectItemInIndex1WithDisplayMember2PreviousValueJames", testWindow);

            // Open dropdown grid of the Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-TestedComboGrid")).Click();

            // select the Item In Index 2 by clicking "Dana" cell, the selected text should change to 3
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Column1", "Dana")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("PreviousValue Value: Bob"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickOnDanaCellToSelectItemInIndex2WithDisplayMember3PreviousValueBob", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
 
        }

        public void ComboGridExpandOnFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            // Click on the default Combogrid Element 
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-ComboGrid")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-FocusOnComboGridDropdownClosed", testWindow);

            // Shift focus to the Tested Combogrid Element 
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-ComboGrid")).SendKeys(Keys.Tab);

            Thread.Sleep(1000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToFocusOnTestedComboGridDropdownExpended", testWindow);

            //Button Click - Change DroppedDown value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ExpandOnFocus value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeExpandOnFocusValueToFalse", testWindow);

            // Click on the Tested Combogrid Element 
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-clickToFocusOnTestedComboGridDropdownClosed", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change ExpandOnFocus value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ChangeExpandOnFocusValueToTrue", testWindow);

            // Click on the Tested Combogrid Element 
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-TestedComboGrid")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-clickToFocusOnTestedComboGridDropdownExpended", testWindow);
        }

        public void ComboGridShowHeaderFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            // Open dropdown grid of the top Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-ComboGrid")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropDownPortionOfTopCombogrid", testWindow);

            // Open dropdown grid of the Bottom Left Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-ComboGrid-1")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-OpenDropDownPortionOfBottomLeftCombogrid", testWindow);

            // Open dropdown grid of the Bottom Right Combogrid Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-ComboGrid-2")).Click();

            Thread.Sleep(1000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-OpenDropDownPortionOfBottomRightCombogrid", testWindow);
        }
    }
}
