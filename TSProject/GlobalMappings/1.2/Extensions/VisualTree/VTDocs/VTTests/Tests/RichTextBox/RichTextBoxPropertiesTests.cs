﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class RichTextBoxTests
    {
        public void RichTextBoxHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change Height value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("45px."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo45px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Height value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80px."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void RichTextBoxLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Left value >>")).Click();

            //Wait for Label update
            testWindow.FindElement(GetXPathForLabelWithText("80px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void RichTextBoxLocationFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Change RichTextBox Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=200"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToLeft200Top290", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Location value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToLeft80Top310", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void RichTextBoxSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=200"));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo200_80", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Size value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Width=300"));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo300_120", testWindow);
        }
        public void RichTextBoxTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Top value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("310px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void RichTextBoxVisibleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change Visible value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update  
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToTrue", testWindow);


            //Wait for RichTextBox update 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Visible value >>")).Click();
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFalse", testWindow);


            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RichTextBoxWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("400px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo400px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Width value >>")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("200px."));

            // Compare snapshots
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo250px", testWindow);
        }

        public void RichTextBoxBorderStyleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Button Click - Change RichTextBox BorderStyle
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Double."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToDouble", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Fixed3D."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToFixed3D", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("FixedSingle."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-changeToFixedSingle", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Groove."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-changeToGroove", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Inset."));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-changeToInset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Dashed."));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-changeToDashed", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("NotSet."));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-changeToNotSet", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Outset."));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-changeToOutset", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Ridge."));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-changeToRidge", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("ShadowBox."));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-changeToShadowBox", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Solid."));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-changeToSolid", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Underline."));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-changeToUnderline", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BorderStyle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("None."));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-changeToNone", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void RichTextBoxBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RichTextBox Bounds
            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_340_300_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change Bounds value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_355_200_80", testWindow);


        }

        public void RichTextBoxClientRectangleFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RichTextBox Bounds value
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("X=100,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeBoundsTo100_310_300_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientRectangle value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("X=80,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeBoundsTo80_320_200_80", testWindow);


        }

        public void RichTextBoxClientSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RichTextBox ClientSize 
            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("300,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeSizeTo300_45", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("Change ClientSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("200,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSizeTo200_80", testWindow);
        }


        //Bug 21154 - VT - RichTextBox.TextChanged event doesn't invoke when inserting text from keyboard
        public void RichTextBoxTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
        

            //Adding text manually '456%'
            testWindow.FindElement(GetXPathForRichTextBoxWithText()).SendKeys(Keys.End + "456%");
            //Wait for log label update 
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("TestedRichTextBox456%"));
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Add456%StringManually", testWindow);
           
            //Delete text manually (4 arrows to the left , backspace X1 , delete X1)
            //Adding text manually - space
            testWindow.FindElement(GetXPathForRichTextBoxWithText()).SendKeys(Keys.Left + Keys.Left + Keys.Left + Keys.Left + Keys.Backspace + Keys.Delete + Keys.Space);
            //Wait for log label update 
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("TestedRichTextBo 56%"));
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-EditBcksDelSpcStringManually", testWindow);
            
            //Button Click - Add Text
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Text >>")).Click();
            //Wait for log label update
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("TestedRichTextBo 56%New String"));
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-AddTextProgramatically", testWindow);


            //Button Click - Clear Text 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Clear Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("RichTextBox is Empty"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClearTextProgramatically", testWindow);

            //Button Click - Change RichTextBox Text
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TestedRichTextBox"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ChangeToNewText", testWindow);

            //Button Click - Change RichTextBox Text
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ChangeToTestedRichTextBox", testWindow);

            //Adding text manually 'AbC*?'
            testWindow.FindElement(GetXPathForRichTextBoxWithText()).SendKeys(Keys.End + "AbC*");
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("AbC*?"));
            Thread.Sleep(2000);
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-AddAbCAsteriksQuestionStringManually", testWindow);

            //Select text and delete
            testWindow.FindElement(GetXPathForRichTextBoxWithText()).SendKeys(Keys.Left + Keys.Left + "7" + Keys.Shift + (Keys.Left + Keys.Left + Keys.Left) + Keys.Delete);
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //Wait for log label update
            Thread.Sleep(2000);
            //testWindow.WaitForElement(GetXPathForLabelWithText("A*?"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-EditMiddleOfStringManually", testWindow);


            

            //Bug 23920 after fix uncomment the below line 
            ////Button Click - Change RichTextBox Text - use \n
            //testWindow.FindElement(GetXPathForButtonSpanWithText("Change Text Value Slash N>>")).Click();
            ////Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("SecondLine"));
            ////Compare snapshots 2
            //CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ChangeToTestedRichTextBoxSlashN", testWindow);



        }
        public void RichTextBoxTagFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RichTextBox Tag value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".RichTextBox"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToRichTextBox", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Tag value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("New Tag."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewTag", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void RichTextBoxBackColorFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change BackColor value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Orange]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-changeToOrange", testWindow);
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change BackColor value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Color [Green]"));
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-changeToGreen", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RichTextBoxPixelHeightFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelHeight value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("45."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo45px", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelHeight value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);
        }
        public void RichTextBoxPixelLeftFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change PixelLeft Location 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("180."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo180px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelLeft value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("80."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo80px", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
        public void RichTextBoxPixelTopFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RichTextBox PixelTop 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("290."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo290px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelTop value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("310."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo310px", testWindow);
        }
        public void RichTextBoxPixelWidthFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Change RichTextBox PixelWidth 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("400."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo400px", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change PixelWidth value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("200."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200px", testWindow);
        }

        public void RichTextBoxEnabledFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Check initialize settings
            testWindow.FindElement(GetXPathForRichTextBoxWithCssClassAriaDisabled("vt-test-rtb-1", "false"));
            testWindow.FindElement(GetXPathForRichTextBoxWithCssClassAriaDisabled("vt-test-rtb-2", "true"));

            //Button Click - Change Enabled value
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));
            //Check if text Box is enabled
            testWindow.FindElement(GetXPathForRichTextBoxWithCssClassAriaDisabled("vt-test-rtb-2", "false"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToTrueSecondRtb", testWindow);


            testWindow.FindElement(GetXPathForButtonSpanWithText("Change Enabled Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False."));
            //Check if text Box is disabled
            testWindow.FindElement(GetXPathForRichTextBoxWithCssClassAriaDisabled("vt-test-rtb-2", "true"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToFalseSecondRtb", testWindow);
            
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void RichTextBoxMaximumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MaximumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=150, Height=90}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo150-90", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=70}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo200-70", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MaximumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=160, Height=100}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo160-100", testWindow);

            //Button Click - Cancel RichTextBox MaximumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MaximumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMaximumSizeTo0_0", testWindow);
        }

        //Bug 9514 - When Changing MinimumSize or MaximumSize the size doesn't change
        public void RichTextBoxMinimumSizeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button click - Change MinimumSize
            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=260, Height=50}"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeTo260-50", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=110, Height=100}"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeTo110-100", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Change MinimumSize value >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=200, Height=70}"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeTo200-70", testWindow);

            //Button Click - Cancel RichTextBox MinimumSize, set it to (0, 0). Size is equal to his last size
            testWindow.FindElement(GetXPathForButtonSpanWithText("Set MinimumSize to (0, 0) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: {Width=0, Height=0}"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-SetMinimumSizeTo0_0", testWindow);
        }

        //Bug 21154 - VT - RichTextBox.TextChanged event doesn't invoke when inserting text from keyboard
        public void RichTextBoxSelectionFontFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Create a text selection
            testWindow.WaitForElement(GetXPathForRichTextBoxWithText()).SendKeys("XX" + Keys.Left + Keys.Left + Keys.Shift + (Keys.Right + Keys.Right + Keys.Right + Keys.Right + Keys.Right + Keys.Right));
            //Wait for log label update 
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("XXTestedRichTextBox"));
            Thread.Sleep(2000);
            //Button Click - Change SelectionFont
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change SelectionFont Value >>")).Click();
            //Wait for log lable update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=SketchFlow"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectTextAndChangeSelectionFont", testWindow);

            //Button Click - Change SelectionFont to the same text selection
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change SelectionFont Value >>")).Click();
            //Wait for log lable update
            testWindow.WaitForElement(GetXPathForLabelWithText("Name=Miriam Fixed"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeSelectionFontToTheSameSelectedText", testWindow);

            //Clear the RichTextBox
            testWindow.WaitForElement(GetXPathForRichTextBoxWithText()).SendKeys(Keys.Control + "a" + Keys.Delete);
            //Button Click - Change SelectionFont
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change SelectionFont Value >>")).Click();

            //Insert Enter and Text 'ABcd'
            testWindow.WaitForElement(GetXPathForRichTextBoxWithText()).SendKeys(Keys.Enter + "ABcd");
            //Wait for log label update 
            //Bug 21154 - after fix uncomment the below line and delete the Thread.Sleep line
            //testWindow.WaitForElement(GetXPathForLabelWithText("ABcd"));
            Thread.Sleep(2000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeSelectionFontAndInsertABcdStringManually", testWindow);

            
        }
    }
}
