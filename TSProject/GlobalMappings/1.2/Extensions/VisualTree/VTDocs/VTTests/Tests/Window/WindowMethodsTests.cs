﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class WindowElementTests
    {

        public void WindowActivateFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //Click on Definition label - need to click free space in the window to bring it the focus 
            testWindow.WaitForElement(GetXPathForLabelWithText("Activates the Window and gives it focus")).Click();
            Thread.Sleep(1000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-clickFreeSpaceInMainWindowToBringItToFront-TestedWindowIsNotShown", testWindow);

            //Click button - to activate TestedWindow
            testWindow.FindElement(GetXPathForButtonWithText("Activate TestedWindow >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Activate() was invoked"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-clickButtonToActivateTestedWindow-TestedWindowIsShown", testWindow);

        }

    }
}
