﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class ProgressBarElementTests
    {
        // Bug 21500 - leave event is invoked when the window loses focus
        public void ProgressBarLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-1", "false")).Click();

            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-2", "false")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ProgressBar 1 was leaved"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TabToLeaveFirstProgressBar", testWindow);

            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-1", "false")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ProgressBar 2 was leaved"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToLeaveSecondProgressBar", testWindow);
            //Click ProgressBar - Change Focus
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-2", "false")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ProgressBar 1 was leaved"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToLeaveFirstProgressBar", testWindow);
            // check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4 
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click ProgressBar - Change Focus
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-2", "false")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusSecondProgressBar", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(1000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(1000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();
            // end check
            // check shifting focus inside the browser window
            //Click ProgressBar - Change Focus
            testWindow.FindElement(GetXPathForDisabledControlWithCssClass("vt-test-pb-2", "false")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusSecondProgressBar", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
            // end check
        }
    }
}
