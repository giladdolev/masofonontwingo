﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class RichTextBoxTests
    {
        // Bug 21154 - event doesn't invoke when inserting text from keyboard
        public void RichTextBoxTextChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Some Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonToChangeTextToSomeText", testWindow);
            //Click button - Invoke TextChanged
            testWindow.WaitForElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonToChangeTextToNewText", testWindow);
            //Verify TextChanged is invoked by adding text
            testWindow.WaitForElement(GetXPathForRichTextBoxWithText()).SendKeys("Editing ");
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("Editing New Text"));

            Thread.Sleep(3000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SendKeys-Editing", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
    }
}
