﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class ListViewElementTests
    {
        public void ListViewFindItemWithText4ParamsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement textBox_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Enter String"));

            //**** Check parameters combinations on level 1 of SubItem

            //Check When "IncludeSubItemsInSearch" and "isPrefixSearch" checkBoxes are not checked - return null

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Yaron Moshe", in textBox
            textBox_elem.SendKeys("Yaron Moshe");
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("0")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Yaron Moshe, False, 0, False)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TextEnter-YaronMoshe-StartIndex0-andButtonClick-Expected-EmptyReturn", testWindow);

            //Check When "IncludeSubItemsInSearch" checkBox is checked - Return the ListViewItem 'Haim Michael'

            //Check  IncludeSubItemsInSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("IncludeSubItemsInSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Haim Michael"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TextEnter-YaronMoshe-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-HaimMichael", testWindow);

            //Check When "IncludeSubItemsInSearch" and "isPrefixSearch" checkBoxes are checked - Return the ListViewItem 'Haim Michael'

            //Check  isPrefixSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("isPrefixSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True, 0, True"));
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Haim Michael"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-TextEnter-YaronMoshe-IncludeSubItemsInSearch-StartIndex0-PrefixSearch-andButtonClick-Expected-HaimMichael", testWindow);

            //Check When "IncludeSubItemsInSearch" is not checked and "isPrefixSearch" checkBoxe is checked - return null 

           
            //UnCheck  IncludeSubItemsInSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("IncludeSubItemsInSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Yaron Moshe, False, 0, True)"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-TextEnter-YaronMoshe-StartIndex0-PrefixSearch-andButtonClick-Expected-EmptyReturn", testWindow);

            //Check When "IncludeSubItemsInSearch" and "isPrefixSearch" checkBoxes are checked and StartIndex is 1 - Return null

         
            //Check IncludeSubItemsInSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("IncludeSubItemsInSearch")).Click();
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("1")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Yaron Moshe, True, 1, True)"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-TextEnter-YaronMoshe-IncludeSubItemsInSearch-StartIndex1-PrefixSearch-andButtonClick-Expected-EmptyReturn", testWindow);

            //Check When "isPrefixSearch" checkBox isn't checked and Searching part of text, 'Yaron' - Return null

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Yaron", in textBox
            textBox_elem.SendKeys("Yaron");
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("0")).Click();
            //UnCheck  isPrefixSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("isPrefixSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Yaron, True, 0, False)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-TextEnter-Yaron-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-EmptyReturn", testWindow);

            //*****Check Some combinations on level 2 of SubItem and also check case sensitive

            //Check When "IncludeSubItemsInSearch" checkBox is checked and "isPrefixSearch" isn't checked - return null 

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "sHIM", in textBox
            textBox_elem.SendKeys("sHIM");
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(sHIM, True, 0, False)"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-TextEnter-sHIM-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-EmptyReturn", testWindow);

            //Check When "IncludeSubItemsInSearch" and "isPrefixSearch" checkBoxes are checked - Return the ListViewItem 'Cohen'

            //Check  isPrefixSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("isPrefixSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Cohen"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-TextEnter-sHIM-IncludeSubItemsInSearch-StartIndex0-PrefixSearch-andButtonClick-Expected-Cohen", testWindow);

            //Check When "IncludeSubItemsInSearch" and "isPrefixSearch" checkBoxes are checked and StartIndex is 2 - Return null

            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("2")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(sHIM, True, 2, True)"));
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-TextEnter-sHIM-IncludeSubItemsInSearch-StartIndex2-PrefixSearch-andButtonClick-Expected-EmptyReturn", testWindow);


            //**Check for  prefix text that doesn't exist

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Shushi", in textBox
            textBox_elem.SendKeys("Shushi");
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("0")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Shushi, True, 0, True)"));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-TextEnter-Shushi-IncludeSubItemsInSearch-StartIndex0-PrefixSearch-andButtonClick-Expected-EmptyReturn", testWindow);

            //**Check searching for ListViewItem parameter (Not SubItem as parameter)

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Itai", in textBox
            textBox_elem.SendKeys("Itai");
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Itai"));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-TextEnter-Itai-IncludeSubItemsInSearch-StartIndex0-PrefixSearch-andButtonClick-Expected-Itai", testWindow);

            //**Check searching for ListViewItem parameter part of text and isPrefixSearch is not checked (Not SubItem , ListViewItem)

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Haim", in textBox
            textBox_elem.SendKeys("Haim");
            //UnCheck  isPrefixSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("isPrefixSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32, Boolean) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Haim, True, 0, False)"));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-TextEnter-Haim-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-EmptyReturn", testWindow);

        }

        public void ListViewFindItemWithText3ParamsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement textBox_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Enter String"));
            
            //**** Check parameters combinations on level 1 of subitem

            //Check When "IncludeSubItemsInSearch" checkbox isn't checked - return null

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Yaron", in textBox
            textBox_elem.SendKeys("Yaron");
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("0")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Yaron, False, 0)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TextEnter-Yaron-StartIndex0-andButtonClick-Expected-EmptyReturn", testWindow);

            //Check When "IncludeSubItemsInSearch" checkbox is checked - Return the ListViewItem 'Haim Michael'

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Yaron", in textBox
            textBox_elem.SendKeys("Yaron");
            //Check  IncludeSubItemsInSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("IncludeSubItemsInSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Haim Michael"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TextEnter-Yaron-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-HaimMichael", testWindow);

            //Check When "IncludeSubItemsInSearch" checkbox is checked and StartIndex is 1 - return null

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Yaron", in textBox
            textBox_elem.SendKeys("Yaron");
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("1")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Yaron, True, 1)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-TextEnter-Yaron-IncludeSubItemsInSearch-StartIndex1-andButtonClick-Expected-EmptyReturn", testWindow);

            //*****Check parameters combinations on level 2 of subitem and also check case sensitive

            //Check When "IncludeSubItemsInSearch" checkbox isn't checked - return null 

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "sHIM", in textBox
            textBox_elem.SendKeys("sHIM");
            //UnCheck  IncludeSubItemsInSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("IncludeSubItemsInSearch")).Click();
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("0")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(sHIM, False, 0)"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-TextEnter-sHIM-StartIndex0-andButtonClick-Expected-EmptyReturn", testWindow);

            //Check When "IncludeSubItemsInSearch" checkbox is checked - Return the ListViewItem 'Cohen'

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "sHIM", in textBox
            textBox_elem.SendKeys("sHIM");
            //Check  IncludeSubItemsInSearch CheckBox
            testWindow.FindElement(GetXPathForCheckBoxWithText("IncludeSubItemsInSearch")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Cohen"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-TextEnter-sHIM-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-Cohen", testWindow);

            //Check When "IncludeSubItemsInSearch" checkbox is checked and StartIndex is 2 - return null

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "sHIM", in textBox
            textBox_elem.SendKeys("sHIM");
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("2")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(sHIM, True, 2)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-TextEnter-sHIM-IncludeSubItemsInSearch-StartIndex2-andButtonClick-Expected-EmptyReturn", testWindow);


            //**Check searching for ListViewItem parameter (Not Subitem as parameter)

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "sHIM", in textBox
            textBox_elem.SendKeys("Itai");
            //Click to combo down arrow
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-cmbStartIndex")).Click();
            //Select combo item "0" 
            testWindow.WaitForElement(GetXPathForComboBoxItem("0")).Click();
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Itai"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-TextEnter-Itai-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-Itai", testWindow);

            //**Check for  prefix text that doesn't exist

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Shushi", in textBox
            textBox_elem.SendKeys("Shushi");
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String, Boolean, Int32) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("FindItemWithText(Shushi, True, 0)"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-TextEnter-Shushi-IncludeSubItemsInSearch-StartIndex0-andButtonClick-Expected-EmptyReturn", testWindow);

        
        }

        public void ListViewFindItemWithText1ParamFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement textBox_elem = testWindow.FindElement(GetXPathForTextBoxWithText("Enter String"));
            //Send parameter listViewItem

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text - "Haim", in "Enter String" textBox 
            textBox_elem.SendKeys("Haim");
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Haim Michael"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TextEnter-Haim-andButtonClick-Expected-HaimMichael", testWindow);

            //Send parameter SubItem from level 1

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text - "Ti", in "Enter String" textBox 
            textBox_elem.SendKeys("Ti");
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Gali"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TextEnter-Ti-andButtonClick-Expected-Gali", testWindow);

            //Send parameter SubItem from level 2

            //Clear TextBox
            textBox_elem.Clear();
            //Enter Text, "Rachel Emenu", in textBox 
            textBox_elem.SendKeys("Rachel Emenu");
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String) >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Return value: Itai"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-TextEnter-RachelEmenu-andButtonClick-Expected-Itai", testWindow);

            //Send parameter text that doesn't match prefix of any item 

            //Clear TextBox
            testWindow.FindElement(GetXPathForTextBoxWithText("Enter String")).Clear();
            //Enter Text, "Emenu", in textBox 
            textBox_elem.SendKeys("Emenu");
            //Wait for TextBox Text to be set
            Thread.Sleep(1000);
            //Click "FindItemWithTex(String) >>" button
            testWindow.FindElement(GetXPathForButtonWithText("FindItemWithText(String) >>")).Click();
            Thread.Sleep(1000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-TextEnter-Emenu-andButtonClick-Expected-NoReturn-Null", testWindow);
        }
        public void ListViewSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke ListView SetBounds()
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SetBoundsTo100_280_250_50", testWindow);

            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetBoundsTo80_300_273_99", testWindow);
        }

        public void ListViewHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke ListView Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void ListViewShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke ListView Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }
        public void ListViewGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke ListView GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".ListViewElement"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        // Bug 21294 - ToString method doesn't return the Items.Count and first Item Type
        public void ListViewToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }

        public void ListViewClearFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Clear >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 0"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClickClear-VerifyListViewIsEmpty", testWindow);


            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Add2Items-ListViewContains2Items", testWindow);


            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Clear >>")).Click();
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Items Count value: 0"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClickClear-VerifyListViewIsEmpty", testWindow);


        }
    }
}
