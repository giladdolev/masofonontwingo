﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class TabElementTests
    {
        public void TabTabItemCollectionAddStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Remove Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Item >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Items.Remove"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-RemoveItem", testWindow);

            //Button Click - Add Item
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Items.Add"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-AddItem", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Item >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Items.Remove"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-2ndRemoveItem", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Item >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Items.Add"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-2ndAddItem", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void TabTabItemCollectionRemoveAtFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement textBox_elem = testWindow.FindElement(GetXPathForTextBoxWithText("..."));
            textBox_elem.Clear();
            //Set a new value in the textBox - Insert TabItem index
            textBox_elem.SendKeys("2");
            //Verify the value was inserted
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem index: 2."));
            //Button Click - Removes the selected TabItem
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove TabItem >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem in index 2 was removed."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-RemoveTabItemIndex2", testWindow);

            textBox_elem.Clear();
            //Set a new value in the textBox - Insert TabItem index
            textBox_elem.SendKeys("1");
            //Verify the value was inserted
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem index: 1."));
            //Button Click - Removes the selected TabItem
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove TabItem >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem in index 1 was removed."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-RemoveTabItemIndex1", testWindow);

            //Button Click - Reloads all the TabItems
            testWindow.FindElement(GetXPathForButtonSpanWithText("Reload TabItems >>")).Click();

            Thread.Sleep(3000);

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ReloadTabItems", testWindow);

        }

        public void TabTabItemCollectionCountFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement textBox_elem = testWindow.FindElement(GetXPathForTextBoxWithText("..."));
            textBox_elem.Clear();
            //Set a new value in the textBox - Insert TabItem index
            textBox_elem.SendKeys("1");
            //Verify the value was inserted
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem index: 1."));
            //Button Click - Removes the selected TabItem
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove TabItem >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem in index 1 was removed."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-RemoveTabItemIndex2", testWindow);

            //Button Click - Adds set of new TabItems
            testWindow.FindElement(GetXPathForButtonSpanWithText("AddRange TabItems >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Set of 3 TabItems were added."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-AddSetOfTabItems", testWindow);

            textBox_elem.Clear();
            //Set a new value in the textBox - Insert TabItem index
            textBox_elem.SendKeys("4");
            //Verify the value was inserted
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem index: 4."));
            //Button Click - Removes the selected TabItem
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove TabItem >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TabItem in index 4 was removed."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-RemoveTabItemIndex4", testWindow);

        }
        
    }
}
