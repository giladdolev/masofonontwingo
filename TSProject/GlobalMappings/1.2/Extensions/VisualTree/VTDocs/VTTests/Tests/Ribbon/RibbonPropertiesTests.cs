﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class RibbonControlElementTests
    {
        public void BasicRibbonControlFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Selecting second ribbon page
            testWindow.WaitForElement(GetXPathForTabPageWithText("Page1")).Click();
            
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectingSecondRibbonPage", testWindow);

        }

        public void RibbonImageListFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Selecting second ribbon page
            testWindow.WaitForElement(GetXPathForTabPageWithText("Page1")).Click();

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectingSecondRibbonPage", testWindow);

            //Open the dropdown button
            testWindow.WaitForElement(GetXpathForControlsWithCssClassOnly("vt-dropdown-page1")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-OpeningDropDownButton", testWindow);

        }
        
    }
}
