﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridElementTests
    {

        public void GridGridColumnCollectionContainsStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement txtInput_elem = testWindow.FindElement(GetXPathForTextBoxWithText("ColumnID"));

            ////////Pass Col to Contains(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col");
            //Wait for keys to be sent
            Thread.Sleep(1000);
            //Button Click - Invoke Contains(Col)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Contains >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Contains(Col) : False"));
            //testWindow.FindElement(GetXPathForLabelWithText("False."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-InvokeContains-Col-ResultFalse", testWindow);

            ////////Pass Col1 to Contains(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col1");
            //Wait for keys to be sent
            Thread.Sleep(1000);
            //Button Click - Invoke Contains(Col1)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Contains >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Contains(Col1) : True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-InvokeContains-Col1-ResultTrue", testWindow);

            ////////Pass Col2 to Contains(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col2");
            //Wait for keys to be sent
            Thread.Sleep(1000);
            //Button Click - Invoke Contains(Col2)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Contains >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Contains(Col2) : True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-InvokeContains-Col2-ResultTrue", testWindow);

            ////////Pass Col3 to Contains(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col3");
            //Wait for keys to be sent
            Thread.Sleep(1000);
            //Button Click - Invoke Contains(Col3)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Contains >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Contains(Col3) : True"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-InvokeContains-Col3-ResultTrue", testWindow);
        }
        public void GridGridColumnCollectionIndexOfStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            IWebElement txtInput_elem = testWindow.FindElement(GetXPathForTextBoxWithText("ColumnID"));

            ////////Pass Col to IndexOf(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Col"));
            //Button Click - Invoke IndexOf(Col)
            testWindow.FindElement(GetXPathForButtonSpanWithText("IndexOf >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IndexOf(Col)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("-1."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-InvokeIndexOf-Col-Result-1", testWindow);

            ////////Pass Col1 to IndexOf(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col1");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Col1"));
            //Button Click - Invoke IndexOf(Col1)
            testWindow.FindElement(GetXPathForButtonSpanWithText("IndexOf >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IndexOf(Col1)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("0."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-InvokeIndexOf-Col1-Result0", testWindow);

            ////////Pass Col2 to IndexOf(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col2");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Col2"));
            //Button Click - Invoke IndexOf(Col2)
            testWindow.FindElement(GetXPathForButtonSpanWithText("IndexOf >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IndexOf(Col2)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("1."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-InvokeIndexOf-Col2-Result1", testWindow);

            ////////Pass Col3 to IndexOf(String) method
            txtInput_elem.Clear();
            txtInput_elem.SendKeys("Col3");
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(": Col3"));
            //Button Click - Invoke IndexOf(Col3)
            testWindow.FindElement(GetXPathForButtonSpanWithText("IndexOf >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("IndexOf(Col3)"));
            testWindow.WaitForElement(GetXPathForLabelWithText("2."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-InvokeIndexOf-Col3-Result2", testWindow);

        }

        public void GridGridColumnCollectionAddGridColumnFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            // bug 25228 - Adding test for exception when clicking on an empty cell. No Snapshots, If throw exception, test will fail.
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol1")).Click();
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol2")).Click();
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol3")).Click();
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol4")).Click();
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol5")).Click();
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol6")).Click();

            //Remove all six Columns
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Last Column >>")).Click();
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Last Column >>")).Click();
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Last Column >>")).Click();
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Last Column >>")).Click();
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Last Column >>")).Click();
            testWindow.FindElement(GetXPathForButtonSpanWithText("Remove Last Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("removed - ButtonCol."));

            //Button Click - Invoke Add(GridButtonColumn)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Button Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Button Column was added"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-RemoveAllColumnsAndInvokeAddButtonColumn", testWindow);

            //Button Click - Invoke Add(GridCheckBoxColumn)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add CheckBox Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckBox Column was added"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-AddCheckBoxColumn", testWindow);

            //Button Click - Invoke Add(GridComboBoxColumn)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add ComboBox Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBox Column was added"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-AddComboBoxColumn", testWindow);

            //Button Click - Invoke Add(GridImageColumn)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Image Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Image Column was added"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-AddImageColumn", testWindow);

            //Button Click - Invoke Add(GridLinkColumn)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add Link Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Link Column was added"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-AddLinkColumn", testWindow);

            //Button Click - Invoke Add(GridTextBoxColumn)
            testWindow.FindElement(GetXPathForButtonSpanWithText("Add TextBox Column >>")).Click();
            //Wait for log Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TextBox Column was added"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-AddTextBoxColumn", testWindow);
            

            

        }
       
       

    }
}
