﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class MaskedTextBoxElementTests
    {

        // Bug 24396 - when focusing the cursor always at the end of the text
        public void MaskedTextBoxFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            ////Button click - to focus MaskedTextBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Focus method was invoked."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-invokeFocus-buttonClick", testWindow);

            //Button click - Click Lose Focus button to lose focus of the MaskedTextBox
            testWindow.FindElement(GetXPathForButtonSpanWithText("Lose Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Control lost focus."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickLoseFocusBtn", testWindow);
           
            //The following commands are to see if the focus is back to where the cursor were the last time 
            //the MaskedTextBox had focus
            //For example if the cursor was at the beginning of the  text next time we will focus the MaskedTextBox, it will be in the same place 

            //Focus the MaskedTextBox - on Specific place inside the MaskedTextBox - middle of the text 
            testWindow.FindElement(GetXPathForTextBoxWithText("TestedMaskedTextBox")).Click();
            testWindow.FindElement(GetXPathForTextBoxWithText("TestedMaskedTextBox")).SendKeys(Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft + Keys.ArrowLeft);
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ManuallyFocusingSpecificPlaceInsideMaskedTextBox-inMiddleOfText", testWindow);

            
            //Button click - Click Focus button 
            testWindow.FindElement(GetXPathForButtonSpanWithText("Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Focus method was invoked."));
            //Bug 23976 - after fix, uncomment to show the return value in log
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("Return value: True."));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-FocusByButton-TheFocusInsideMaskedTextBoxIsAsInScreenshot3", testWindow);


        }

    }
}
