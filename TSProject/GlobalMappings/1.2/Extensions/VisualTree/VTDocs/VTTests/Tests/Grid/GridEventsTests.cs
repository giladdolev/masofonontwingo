﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridElementTests
    {
        public void GridColumnHeaderMouseClickFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Column Header Mouse Click
            testWindow.WaitForElement(GetXPathForGridColumnHeaderSpanWithText("Column1")).Click();
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("You clicked on column header number : 1"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickOnColumn1", testWindow);

            //Column Header Mouse Click
            testWindow.WaitForElement(GetXPathForGridColumnHeaderSpanWithText("Column2")).Click();
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("You clicked on column header number : 2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnColumn2", testWindow);

            //Column Header Mouse Click
            testWindow.WaitForElement(GetXPathForGridColumnHeaderSpanWithText("Column3")).Click();
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("You clicked on column header number : 3"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickOnColumn3", testWindow);
        }

        
        public void GridCellBeginEditFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: e."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickAndEditMiddleCell-e", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "a")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: a."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickAndEditTopLeftCell-a", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol3", "i")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: i."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickAndEditButtomRightCell-i", testWindow);
        }

        // Bug 21859 - CellEndEdit event is invoked when the window loses focus, 21858, 21052
        // Bug 27677 - When Adding an Empty Row the RowHeader disappear
        public void GridCellEndEditFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a checkbox cell
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol1")).Click();

            // Click on 1st row's header
            testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("0")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("CheckBoxCol,"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-UnCheckTopCheckBoxCellAndClickOn1stRowHeader", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2_2", "male")).Click();
            IWebElement elemComboCell = testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2_2", "male"));
            elemComboCell.Click();
            elemComboCell.SendKeys(Keys.ArrowDown + Keys.ArrowDown + Keys.Enter);

            // Click on 1st row's header
            testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("0")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBoxCol,"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectFemaleOnTopComboBoxCellAndClickOn1stRowHeader", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol3", "a")).Click();

            // Click on 1st row's header
            testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("0")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TextBoxCol,"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickAndEditTextBoxCell-a-AndClickOn1stRowHeader", testWindow);

            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridCol4", "d")).SendKeys("d");

            // Click on 1st row's header
            testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("0")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("TextButtonCol,"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickAndEditTextButtonCell-d-AndClickOn1stRowHeader", testWindow);

            //Clicking a checkbox cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol5", "06/08/1974")).Click();

            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol5", "06/08/1974")).SendKeys(Keys.ArrowDown + Keys.ArrowDown + Keys.Enter + Keys.Enter);

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("DateTimePickerCol,"));

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-SelectDateOnDateTimePickerCellAndPressEnter", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Add Empty Row >>")).Click();

            //Clicking a checkbox cell
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol1")).SendKeys(Keys.ArrowRight + Keys.ArrowRight + Keys.Enter + "k");

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();            

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Edited value: k"));

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-AddAndEditEmptyRowAndClearEventLog", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol3", "a")).Click();

            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-ClickAndEditTextBoxCell-a", testWindow);

            // check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 8
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "8-after-OpeningNewWindowAndFocusOnItEventShouldNotInvoke", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickHeaderToReFocusOnExampleWindowTextBoxCell-a-inEditMode", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();


        }

        public void GridMouseRightClickFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //RightClick Grid panel
            IWebElement elem = testWindow.FindElement(GetXpathForControlsWithCssClassAndClass("vt-tested-Grid", "x-tab-guard-after"));
            Actions actions = new Actions(CurrentWebDriver);
            actions.ContextClick(elem);
            actions.Perform();
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Panel was right clicked"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-RightClickGridContainer", testWindow);

            //RightClick RowHeader - verify Grid MouseRightClick event is not invoked when clicking the row header
            //elem = testWindow.WaitForElement(GetXpathForControlsWithCssClassAndClass("vt-tested-Grid", "x-grid-inner-locked"));
            // Click on 1st row's header
            elem = testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("0"));
            actions.ContextClick(elem);
            actions.Perform();
            testWindow.WaitForElement(GetXPathForLabelWithText("Row Header was right clicked"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-RightClickRowHeader", testWindow);

            //RightClick Column Header - verify Grid MouseRightClick event is not invoked when clicking the column header
            elem = testWindow.WaitForElement(GetXpathForControlsWithCssClassAndClass("vt-tested-Grid", "x-column-header-inner"));
            actions.ContextClick(elem);
            actions.Perform();
            testWindow.WaitForElement(GetXPathForLabelWithText("Column Header was right clicked"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-RightClickColumnHeader", testWindow);

            //RightClick on grid cell 
            elem = testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col1", "d"));
            actions.ContextClick(elem);
            actions.Perform();
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Cell was right clicked")); // uncomment after 22027 fix

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-RightClickcell", testWindow);
        }

        public void GridBeforeSelectionChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke SelectionChanged
            testWindow.FindElement(GetXPathForButtonWithText("Select i >>")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("(i)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonSelect-i", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "a")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("(a)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectTopLeftCell-a", testWindow);

            testWindow.WaitForElement(GetXPathForCheckBoxWithText("Cancel selection")).Click();

            Thread.Sleep(2000);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "d")).Click();

            Thread.Sleep(3000);
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectMiddleLeftCell-d-WhenCancelSelectionIsChecked", testWindow);
        }

        //Bug 24750 - VT - GridElement - Selected property doesn't work for a grid column
        //Bug 24767 - VT - GridElement - When selecting a row by Selected property the e.SelectedCells are empty.
        public void GridSelectionChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Select middle TextBox Cell
            //b Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select b >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(b)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonSelect-b-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select middle CheckBox Cell
            //Middle CheckBox Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle CheckBox >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(False)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonSelectMiddleCheckBox-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select middle ComboBox Cell
            //Middle ComboBox Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle ComboBox >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(male)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectMiddleComboBox-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonClearEventLog", testWindow);

            //Click button - Select Row[0]
            //First Row Cells Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select Row[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(a)"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickButtonSelectRow[0]-FirstRowShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select Column[0]
            //First Column Cells Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select Column[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24750 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(a) (b) (c)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickButtonSelectColumn[0]-FirstColumnShouldBeSelectedAndEventIsInvoked", testWindow);

            //Manualy select all cells
            //All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn1", "a")).SendKeys(Keys.Control + "a");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(c)"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-SelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Manualy select MiddleRow
            //Middle Row Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn1", "b")).SendKeys(Keys.ArrowLeft);

            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(male)"));

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-SelectMiddleRow-MiddleRowShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickButtonClearEventLog", testWindow);

            //Click Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-tbCol")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickColumn1Header-EventIsInvoked", testWindow);

            // iterate first row with arrows
            //On First Row Move From a cell to CheckBox cell with the arrow
            //First CheckBox Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn1", "a")).SendKeys(Keys.ArrowRight);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(False)"));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-MoveFromAToFirstCheckBoxWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //On First Row Move From CheckBox cell to ComboBox cell with the arrow
            //First ComboBox Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCheckBoxColumn1")).SendKeys(Keys.ArrowRight);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(female)"));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-MoveFromFirstCheckBoxToFirstComboBoxWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            // iterate first row with tabs (?)

        }

        public void GridSelectionChanged2TextBoxColumnQAFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Left Grid Select d middle TextBox Cell
            //d Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select d >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(d)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonSelect-d-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Right Grid Select r bottom right TextBox Cell
            //r Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select r >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(r)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonSelect-r-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select Row[0]
            //In both Grids First Row Cells Should Be Selected And Event Is Invoked twice
            testWindow.FindElement(GetXPathForButtonWithText("Select Row[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(r)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectRow[0]-InBothGridsFirstRowShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Select Column[0]
            //In both Grids First Column Cells Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select Column[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24750 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(p)"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonSelectColumn[0]-InBothGridsFirstColumnShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickButtonClearEventLog", testWindow);

            //Left Grid Manualy select all cells
            //Left Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn1", "a")).SendKeys(Keys.Control + "a");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(i)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-LeftGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid Manualy select all cells
            //Right Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn4", "j")).SendKeys(Keys.Control + "a");
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(r)"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-RightGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Left Grid Manualy select MiddleRow
            //Left Grid Middle Row Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn1", "d")).SendKeys(Keys.ArrowLeft);

            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(f)"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-LeftGridSelectMiddleRow-MiddleRowShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickButtonClearEventLog", testWindow);

            //Click Left Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-tbCol-1")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-LeftGridClickColumn1Header-EventIsInvoked", testWindow);

            //Click Right Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-tbCol-4")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-RightGridClickColumn1Header-EventIsInvoked", testWindow);

            // iterate first row with arrows
            //Left Grid On First Row Move From a cell to b cell with the arrow
            //b Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn1", "a")).SendKeys(Keys.ArrowRight);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(b)"));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-LeftGridMoveFromAToBWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid On First Row Move From j cell to k cell with the arrow
            //k Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridTextBoxColumn4", "j")).SendKeys(Keys.ArrowRight);
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(k)"));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-RightGridMoveFromJToKWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            // iterate first row with tabs (?)

        }

        public void GridSelectionChanged3CheckBoxColumnQAFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Left Grid Select middle CheckBox Cell
            //Left Grid middle CheckBox Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle cell (Left) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(False)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonSelectMiddleCellLeft-ShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Right Grid Select middle CheckBox Cell
            //Right Grid middle CheckBox Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle cell (Right) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(False)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonSelectMiddleCellRight-ShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select Row[0]
            //In both Grids First Row Cells Should Be Selected And Event Is Invoked twice
            testWindow.FindElement(GetXPathForButtonWithText("Select Row[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(False)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectRow[0]-InBothGridsFirstRowShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Select Column[0]
            //In both Grids First Column Cells Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select Column[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24750 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(False)"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonSelectColumn[0]-InBothGridsFirstColumnShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickButtonClearEventLog", testWindow);

            //Left Grid Manualy select all cells
            //Left Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCheckBoxColumn1")).SendKeys(Keys.Control + "a");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(True)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-LeftGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid Manualy select all cells
            //Right Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCheckBoxColumn4")).SendKeys(Keys.Control + "a");
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(True)"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-RightGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Left Grid Manualy select First Row
            //Left Grid First Row Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCheckBoxColumn1")).SendKeys(Keys.ArrowLeft);

            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(True)"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-LeftGridSelectFirstRow-FirstRowShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickButtonClearEventLog", testWindow);

            //Click Left Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-chbCol-1")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-LeftGridClickColumn1Header-EventIsInvoked", testWindow);

            //Click Right Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-chbCol-4")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-RightGridClickColumn1Header-EventIsInvoked", testWindow);

            // iterate first row with arrows
            //Left Grid On First Row Move From left cell to middle cell with the arrow
            //middle Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCheckBoxColumn1")).SendKeys(Keys.ArrowRight);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(True)"));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-LeftGridFirstRowMoveFromLeftCellToMiddleCellWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid On First Row Move From left cell to middle cell with the arrow
            //middle Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCheckBoxColumn4")).SendKeys(Keys.ArrowRight);
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(True)"));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-RightGridFirstRowMoveFromLeftCellToMiddleCellWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            // iterate first row with tabs (?)

        }

        public void GridSelectionChanged4ComboBoxColumnQAFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Left Grid Select middle ComboBox Cell
            //Left Grid middle ComboBox Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle cell (Left) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(female)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonSelectMiddleCellLeft-ShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Right Grid Select middle ComboBox Cell
            //Right Grid middle ComboBox Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle cell (Right) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(female)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonSelectMiddleCellRight-ShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select Row[0]
            //In both Grids First Row Cells Should Be Selected And Event Is Invoked twice
            testWindow.FindElement(GetXPathForButtonWithText("Select Row[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(male)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectRow[0]-InBothGridsFirstRowShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Select Column[0]
            //In both Grids First Column Cells Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select Column[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24750 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(male)"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonSelectColumn[0]-InBothGridsFirstColumnShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickButtonClearEventLog", testWindow);

            //Left Grid Manualy select all cells
            //Left Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridComboBoxColumn1")).SendKeys(Keys.Control + "a");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(female)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-LeftGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid Manualy select all cells
            //Right Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridComboBoxColumn4")).SendKeys(Keys.Control + "a");
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(female)"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-RightGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Left Grid Manualy select First Row
            //Left Grid First Row Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridComboBoxColumn1")).SendKeys(Keys.ArrowLeft);

            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(female)"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-LeftGridSelectFirstRow-FirstRowShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickButtonClearEventLog", testWindow);
/*
            //Click Left Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-cbCol-1")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-LeftGridClickColumn1Header-EventIsInvoked", testWindow);

            //Click Right Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-cbCol-4")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-RightGridClickColumn1Header-EventIsInvoked", testWindow);
*/
            // iterate first row with arrows
            //Left Grid On First Row Move From left cell to middle cell with the arrow
            //middle Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridComboBoxColumn1")).SendKeys(Keys.ArrowRight);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(male)"));
            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-LeftGridFirstRowMoveFromLeftCellToMiddleCellWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid On First Row Move From left cell to middle cell with the arrow
            //middle Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridComboBoxColumn4")).SendKeys(Keys.ArrowRight);
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(male)"));
            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-RightGridFirstRowMoveFromLeftCellToMiddleCellWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            // iterate first row with tabs (?)

        }

        public void GridSelectionChanged5DateTimePickerColumnQAFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Left Grid Select middle DateTimePicker Cell
            //Left Grid middle DateTimePicker Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle cell (Left) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(10/8/1974 12:00:00 AM)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonSelectMiddleCellLeft-ShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Right Grid Select middle DateTimePicker Cell
            //Right Grid middle DateTimePicker Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select middle cell (Right) >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(19/8/1974 12:00:00 AM)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonSelectMiddleCellRight-ShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select Row[0]
            //In both Grids First Row Cells Should Be Selected And Event Is Invoked twice
            testWindow.FindElement(GetXPathForButtonWithText("Select Row[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(17/8/1974 12:00:00 AM)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectRow[0]-InBothGridsFirstRowShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Select Column[0]
            //In both Grids First Column Cells Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select Column[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24750 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(21/8/1974 12:00:00 AM)"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonSelectColumn[0]-InBothGridsFirstColumnShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickButtonClearEventLog", testWindow);

            //Left Grid Manualy select all cells
            //Left Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridDateTimePickerColumn1")).SendKeys(Keys.Control + "a");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(14/8/1974 12:00:00 AM)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-LeftGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid Manualy select all cells
            //Right Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridDateTimePickerColumn4")).SendKeys(Keys.Control + "a");
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(23/8/1974 12:00:00 AM)"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-RightGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Left Grid Manualy select First Row
            //Left Grid First Row Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridDateTimePickerColumn1")).SendKeys(Keys.ArrowLeft);

            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(8/8/1974 12:00:00 AM)"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-LeftGridSelectFirstRow-FirstRowShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickButtonClearEventLog", testWindow);

            //Click Left Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-dtpCol-1")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-LeftGridClickColumn1Header-EventIsInvoked", testWindow);

            //Click Right Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-dtpCol-4")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-RightGridClickColumn1Header-EventIsInvoked", testWindow);

            // iterate first row with arrows
            //Left Grid On First Row Move From left cell to middle cell with the arrow
            //middle Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridDateTimePickerColumn1")).SendKeys(Keys.ArrowRight);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(7/8/1974 12:00:00 AM)"));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-LeftGridFirstRowMoveFromLeftCellToMiddleCellWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid On First Row Move From left cell to middle cell with the arrow
            //middle Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridDateTimePickerColumn4")).SendKeys(Keys.ArrowRight);
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(16/8/1974 12:00:00 AM)"));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-RightGridFirstRowMoveFromLeftCellToMiddleCellWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            // iterate first row with tabs (?)

        }

        public void GridSelectionChanged6TextButtonColumnQAFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Left Grid Select d middle TextBox Cell
            //d Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select d >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(d)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonSelect-d-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Right Grid Select r bottom right TextBox Cell
            //r Cell Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select r >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(r)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonSelect-r-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Select Row[0]
            //In both Grids First Row Cells Should Be Selected And Event Is Invoked twice
            testWindow.FindElement(GetXPathForButtonWithText("Select Row[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(r)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectRow[0]-InBothGridsFirstRowShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Select Column[0]
            //In both Grids First Column Cells Should Be Selected And Event Is Invoked
            testWindow.FindElement(GetXPathForButtonWithText("Select Column[0] >>")).Click();
            //Sleep should be replaced with the follow commandline when the bug 24750 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.FindElement(GetXPathForLabelWithText("(p)"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonSelectColumn[0]-InBothGridsFirstColumnShouldBeSelectedAndEventIsInvokedTwice", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickButtonClearEventLog", testWindow);

            //Left Grid Manualy select all cells
            //Left Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn1", "a")).Click();
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn1", "a")).SendKeys(Keys.Escape + Keys.Control + "a");
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(i)"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-LeftGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid Manualy select all cells
            //Right Grid All Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn4", "j")).Click();
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn4", "j")).SendKeys(Keys.Escape + Keys.Control + "a");
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(r)"));
            //Compare snapshots 7
            CheckSnapshot(membersCategory, memberName, exampleId + "7-after-RightGridSelectAllCells-AllCellsShouldBeSelectedAndEventIsInvoked", testWindow);

            //Left Grid Manualy select MiddleRow
            //Left Grid Middle Row Cells Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn1", "d")).Click();
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn1", "d")).SendKeys(Keys.Escape + Keys.ArrowLeft);

            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(f)"));
            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-LeftGridSelectMiddleRow-MiddleRowShouldBeSelectedAndEventIsInvoked", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear Event Log >>")).Click();
            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickButtonClearEventLog", testWindow);

            //Click Left Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-tbnCol-1")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-LeftGridClickColumn1Header-EventIsInvoked", testWindow);

            //Click Right Grid Column1 header
            //Event Is Invoked
            testWindow.FindElement(GetXPathForGridColumnHeaderWithColumnCssClass("vt-tbnCol-4")).Click();

            Thread.Sleep(2000);

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-RightGridClickColumn1Header-EventIsInvoked", testWindow);

            // iterate first row with arrows
            //Left Grid On First Row Move From a cell to b cell with the arrow
            //b Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn1", "a")).Click();
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn1", "a")).SendKeys(Keys.Escape + Keys.ArrowRight);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(b)"));
            //Compare snapshots 12
            CheckSnapshot(membersCategory, memberName, exampleId + "12-after-LeftGridMoveFromAToBWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            //Right Grid On First Row Move From j cell to k cell with the arrow
            //k Cell Should Be Selected And Event Is Invoked
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn4", "j")).Click();
            testWindow.WaitForElement(GetXPathForGridTextButtonCellWithIdAndText("GridTextButtonColumn4", "j")).SendKeys(Keys.Escape + Keys.ArrowRight);
            //Sleep should be replaced with the follow commandline when the bug 25434 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(k)"));
            //Compare snapshots 13
            CheckSnapshot(membersCategory, memberName, exampleId + "13-after-RightGridMoveFromJToKWithArrow-CellShouldBeSelectedAndEventIsInvoked", testWindow);

            // iterate first row with tabs (?)

        }

        public void GridCancelEditFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //THE TEST FAILS BECAUSE OF GRID INDEX BUG - 26339 - VT - GridElement - index of first column should be 0 and not 1 (row\column headers index should be -1)

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "a")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol1", "a")).SendKeys("aa" + Keys.Escape);

            testWindow.WaitForElement(GetXPathForLabelWithText("value: a."));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickAndEditTopLeftCell-aa-andEsc", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2", "e")).SendKeys("ee" + Keys.Escape);

            testWindow.WaitForElement(GetXPathForLabelWithText("value: e."));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickAndEditMiddleCell-ee-andEsc", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol3", "i")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol3", "i")).SendKeys("ii" + Keys.Escape);

            testWindow.WaitForElement(GetXPathForLabelWithText("value: i."));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickAndEditButtomRightCell-ii-andEsc", testWindow);


        }

        public void GridKeyDownFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2", "e")).SendKeys("a");

            testWindow.WaitForElement(GetXPathForLabelWithText("KeyCode-A"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickAndEditMiddleCell-a", testWindow);

            //testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2", "e")).SendKeys("1");

            testWindow.WaitForElement(GetXPathForLabelWithText("KeyCode-D1"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickAndEditMiddleCell-1", testWindow);

            //testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2", "e")).SendKeys(" ");

            testWindow.WaitForElement(GetXPathForLabelWithText("KeyCode-Space"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickAndEditMiddleCell-Space", testWindow);

            //testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2", "e")).SendKeys("*");

            testWindow.WaitForElement(GetXPathForLabelWithText("KeyCode-D8"));
            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickAndEditMiddleCell-Asterisk", testWindow);

            //testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2", "e")).SendKeys(Keys.Backspace);

            testWindow.WaitForElement(GetXPathForLabelWithText("KeyCode-Back"));
            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickAndEditMiddleCell-Backspace", testWindow);

            //testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e")).Click();
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol2", "e")).SendKeys(Keys.Shift + Keys.Tab);

            testWindow.WaitForElement(GetXPathForLabelWithText("KeyCode-Tab"));
            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickAndEditMiddleCell-ShiftTab", testWindow);

            

        }

        public void GridBeforeContextMenuShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            Actions actions = new Actions(CurrentWebDriver);

            IWebElement elem_e = testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol2", "e"));
            actions.ContextClick(elem_e);
            actions.Perform();

            testWindow.WaitForElement(GetXPathForLabelWithText("[Aqua]"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ContextClickMiddleCell-e", testWindow);

            IWebElement elem_d = testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "d"));
            actions.ContextClick(elem_d);
            actions.Perform();

            testWindow.WaitForElement(GetXPathForLabelWithText("[Fuchsia]"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ContextClickMiddleLeftCell-d", testWindow);
        }

        public void GridCellMouseDownFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //THE TEST FAILS BECAUSE OF BUG 26349 - VT - GridElement - selection - selection extends instead of being switched

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col2", "e")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("(e)"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-SelectMiddleCell-e", testWindow);

            IWebElement aElem = testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col1", "a"));
            aElem.Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("(a)"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SelectTopLeftCell-a", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col3", "i")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("(i)"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-SelectButtomRightCell-i", testWindow);

            //Check if the mouse move after click release causes selection of cells, bug (20456)
            Actions actions = new Actions(CurrentWebDriver);
            actions.MoveToElement(aElem);
            actions.Perform();

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-MouseMoveToTopLeftCell-a", testWindow);
        }
        public void GridCurrentCellChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("Grid2Col2", "h")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 2 , 1"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickOnACell", testWindow);

            //Clicking on the button to set CurrenCell programmatically
            testWindow.WaitForElement(GetXPathForButtonSpanWithText("Change CurrentCell to (1,2) >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 1 , 2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickOnButtonToSetCurrentCell", testWindow);


        }

        public void BasicGridEventsOrderFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "a")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 0 , 0"));

            //editing a cell
            testWindow.WaitForElement(GetXPathForGridEditableCellWithIdAndText("GridCol1", "a")).SendKeys("b");

            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "b")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 1 , 0"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickAndEditOnACellAndMoveToBCell", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear log >>")).Click();

            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 0 , 1"));

            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "ab")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClearAndClickAndEditOnCheckBoxCellAndMoveToACell", testWindow);

            //Click button - Clear log
            testWindow.FindElement(GetXPathForButtonWithText("Clear log >>")).Click();

            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol3")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Index : 0 , 2"));

            testWindow.WaitForElement(GetXPathForGridCellWithColId("GridCol3")).SendKeys(Keys.Enter + Keys.ArrowDown + Keys.ArrowDown + Keys.Enter);

            //Clicking a cell
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "ab")).Click();

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClearAndClickAndEditOnComboBoxCellAndMoveToA", testWindow);


        }

        // Bug 21500 - leave event is invoked when the window loses focus
        // Bug 23390 - Grid.Leave event doesn't work
        public void GridLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click a
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid-1", "a")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(a)"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Click-a-CellInGrid1Line1", testWindow);

            //Click e
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid-2", "d")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(d)"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Click-d-CellInGrid2Line1", testWindow);

            //Click c
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid-1", "c")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(c)"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Click-c-CellInGrid1Line1", testWindow);

            // check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click c
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid-1", "c")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocus-c-CellInGrid1Line1", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(2000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(2000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();

            // end check
            // check shifting focus inside the browser window

            //Click c
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid-1", "c")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocus-c-CellInGrid1Line1", testWindow);
            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
            // end check
        }

        // bug 18540 - RowClick event shouldn't invoke when clicking on an editable cell. (Reopened)
        public void GridRowClickFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click a
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid", "a")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(a)"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Click-a-CellInEditableColumn1", testWindow);

            // Click on 2nd row's header
            testWindow.WaitForElement(GetXPathForGridRowHeaderWithId("1")).Click();

            //Sleep should be replaced with the follow commandline when the bug 24767 will be fixed
            Thread.Sleep(2000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("(f)"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Click-secondRowHeader", testWindow);

            //Click i
            testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid", "i")).Click();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("(i)"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Click-i-CellInColumn3", testWindow);
        }

        //Bug 20378 - implement grid Column Move event
        //Bug 25178 - Moving a column in Grid the DisplayIndex causes to server error
        //public void GridColumnMoveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        //{
        //    IWebElement column1_elem = testWindow.WaitForElement(By.XPath(string.Format("//div[contains(@class, 'vt-GridColumn1')]")));
        //    IWebElement column2_elem = testWindow.WaitForElement(By.XPath(string.Format("//div[contains(@class, 'vt-GridColumn2')]")));
        //    IWebElement column3_elem = testWindow.WaitForElement(By.XPath(string.Format("//div[contains(@class, 'vt-GridColumn3')]")));

        //    //Dragging 'Column1' column to 'Column3' column
        //    Actions actions = new Actions(CurrentWebDriver);
        //    actions.ClickAndHold(column1_elem).MoveToElement(column3_elem).Release(column3_elem).Build().Perform();
        //    //wait for Log label update
        //    testWindow.WaitForElement(GetXPathForLabelWithText("Column in index 0 was moved to Column in index 2"));
        //    //Compare snapshot 1
        //    CheckSnapshot(membersCategory, memberName, exampleId + "1-after-DraggingColumn1ColumnToIndex2", testWindow);

        //    //Dragging 'Column1' column to 'Column2' column
        //    actions.ClickAndHold(column1_elem).MoveToElement(column2_elem).Release(column2_elem).Build().Perform();
        //    //wait for Log label update
        //    //testWindow.WaitForElement(GetXPathForLabelWithText("Column in index 2 was moved to Column in index 0"));
        //    Thread.Sleep(2000);
        //    //Compare snapshot 2
        //    CheckSnapshot(membersCategory, memberName, exampleId + "2-after-DraggingColumn1Back", testWindow);

        //    //Button Click - Change column1 index programmatically
        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change DisplayIndex value >>")).Click();
        //    //After 25178 fix, uncomment the below wait for label and delete the Thread.Sleep line
        //    //Wait for log label update
        //    //testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex is 1"));
        //    Thread.Sleep(2000);
        //    //Compare snapshots 3
        //    CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonToChangeColumn1ToIndex1", testWindow);

        //    //Button Click - Change column1 back to his index programmatically 
        //    testWindow.FindElement(GetXPathForButtonSpanWithText("Change DisplayIndex value >>")).Click();
        //    //After 25178 fix, uncomment the below wait for label and delete the Thread.Sleep line
        //    //Wait for log label update
        //    //testWindow.WaitForElement(GetXPathForLabelWithText("Column1 DisplayIndex is 0"));
        //    Thread.Sleep(2000);
        //    //Compare snapshots 4
        //    CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickButtonToChangeColumn1BackToIndex0", testWindow);

        //}

        public void GridCellDoubleClickFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {

            //THE TEST FAILS BECAUSE OF GRID INDEX BUG - 26339 - VT - GridElement - index of first column should be 0 and not 1 (row\column headers index should be -1)

            IWebElement CurrentCell1 = testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid", "a"));
            IWebElement CurrentCell3 = testWindow.WaitForElement(GetXPathForGridWithCssClassAndCellText("vt-test-grid", "i"));
            IWebElement CurrentCell2 = testWindow.WaitForElement(By.XPath(string.Format("//td[contains(@data-columnid, 'GridCol2')]")));

            Actions actions = new Actions(CurrentWebDriver);
            //DoubleClick 'a' that founds in cell index (0,0)
            actions.DoubleClick(CurrentCell1);
            actions.Perform();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Current Cell Index : 0 , 0"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-DoubleClickOn-a-InIndex(0,0)", testWindow);

            //DoubleClick 'i' that founds in cell index (2,2)
            actions.DoubleClick(CurrentCell3);
            actions.Perform();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Current Cell Index : 2 , 2"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-DoubleClickOn-i-InIndex(2,2)", testWindow);

            //DoubleClick 'i' that founds in cell index (0,1)
            actions.DoubleClick(CurrentCell2);
            actions.Perform();

            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Grid Current Cell Index : 0 , 1"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-DoubleClickOn-TheFirstCheckBoxCell-InIndex(0,1)", testWindow);
        }
    }
}