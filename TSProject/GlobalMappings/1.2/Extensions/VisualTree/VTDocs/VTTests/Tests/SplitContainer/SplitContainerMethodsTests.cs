﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;

namespace VTTests
{
    public partial class SplitContainerTests
    {
        //27464 - When changing splitContainer size half of the SplitContainer disappears
        public void SplitContainerSetBoundsFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button to set new bounds
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-buttonClick-SetBoundsTo100_280_250_50", testWindow);

            //Click button to set new bounds
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  80px,"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-buttonClick-SetBoundsTo80_300_200_80", testWindow);

            //Click button to set new bounds
            testWindow.FindElement(GetXPathForButtonWithText("SetBounds >>")).Click();
            //Wait for Log label
            testWindow.WaitForElement(GetXPathForLabelWithText("Left  100px,"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-buttonClick-SetBoundsTo100_280_250_50", testWindow);
        }

        public void SplitContainerHideFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke SplitContainer Hide()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Hide", testWindow);
        }

        public void SplitContainerShowFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke SplitContainer Show()
            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-Show", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Hide >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("False"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Hide", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Show >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-Show", testWindow);
        }
        public void SplitContainerGetTypeFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke SplitContainer GetType()
            testWindow.FindElement(GetXPathForButtonSpanWithText("GetType >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText(".SplitContainer"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-GetType", testWindow);
        }
        public void SplitContainerToStringFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Invoke Button ToString()
            testWindow.FindElement(GetXPathForButtonSpanWithText("ToString >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Component"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ToString", testWindow);
        }

        public void SplitContainerResetTextFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Button Click - Reset text
            testWindow.FindElement(GetXPathForButtonSpanWithText("ResetText >>")).Click();
            //Wait for Label update
            testWindow.WaitForElement(GetXPathForLabelWithText("is empty"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ResetText", testWindow);

            testWindow.FindElement(GetXPathForButtonSpanWithText("Set Text >>")).Click();
            testWindow.WaitForElement(GetXPathForLabelWithText("Text is set"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-SetText", testWindow);
        }
    }
}
