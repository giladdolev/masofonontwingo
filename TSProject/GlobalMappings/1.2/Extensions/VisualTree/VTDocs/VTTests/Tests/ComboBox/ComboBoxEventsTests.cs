﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Drawing;

namespace VTTests
{
    public partial class ComboBoxElementTests
    {       
        //Bug 21183 - event doesn't invoke, text from the keyboard
        public void ComboBoxTextChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Some Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeToSomeText", testWindow);
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Text >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("New Text"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeToNewText", testWindow);
 
            
            //Verify TextChanged is invoked by manually adding text
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboBox")).Click();
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-TestedComboBox")).SendKeys("xx");
            //When the bug will be fixed - Sleep command should be replaced with the wait function below.
            Thread.Sleep(3000);
            //Wait for log label update
            //testWindow.WaitForElement(GetXPathForLabelWithText("xx"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ChangeManuallyTo-xx", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

        public void ComboBoxValidatedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            // Bug 21650 - ComboBox CausesValidation Property default value should be set to true

            // A test when the CausesValidation value is false:
            // Focus on ComboBox Element
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-test-cmb")).Click();

            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Shift Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Lost Focus!"));

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeFocusToButtonValidatedNotInvoked", testWindow);

            // Change CausesValidation value to true:
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Toggle CausesValidation Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("True."));

            // Focus on ComboBox Element
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-test-cmb")).Click();

            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Shift Focus >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Not a valid value!"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeFocusToButtonInvalidText", testWindow);

            // Open dropdown list of the ComboBox Element
            testWindow.FindElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItemWithText("Item1")).Click();
/*
            //Wait for log label update
            testWindow.FindElement(GetXPathForLabelWithText("Item1"));
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("Valid value!"));
*/
            //Click button - Invoke TextChanged
            testWindow.FindElement(GetXPathForButtonWithText("Shift Focus >>")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeFocusToButtonValidText", testWindow);            
        }

        // Bug 21500 - leave event is invoked when the window loses focus
        public void ComboBoxLeaveFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-test-cmb1")).SendKeys(Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBox 1 was leaved"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-TabToLeaveFirstComboBox", testWindow);

            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-test-cmb2")).SendKeys(Keys.Shift + Keys.Tab);
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBox 2 was leaved"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToLeaveSecondComboBox", testWindow);
            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-test-cmb2")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBox 1 was leaved"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToLeaveFirstComboBox", testWindow);
// check shifting focus outside the browser window
            // Create a new browser window
            IWebDriver ChromeNewWinDriver = new OpenQA.Selenium.Chrome.ChromeDriver();

            string ChromeNewWinHandle = ChromeNewWinDriver.WindowHandles[0];

            // resize the window
            Size s = ChromeNewWinDriver.Manage().Window.Size;
            ChromeNewWinDriver.Manage().Window.Size = new Size(testWindow.Location.X + 20, s.Height);

            Thread.Sleep(1000);

            //Compare snapshots 4 
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "4-after-OpeningNewWindowAndFocusOnIt", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 5
            CheckSnapshot(membersCategory, memberName, exampleId + "5-after-ClickHeaderToReFocusOnExampleWindow", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-test-cmb2")).Click();

            //Compare snapshots 6
            CheckSnapshot(membersCategory, memberName, exampleId + "6-after-ClickToFocusSecondComboBox", testWindow);

            Thread.Sleep(1000);

            ChromeNewWinDriver.SwitchTo().Window(ChromeNewWinHandle);

            Thread.Sleep(1000);

            //Compare snapshots 7
            CheckSnapshotExtended(membersCategory, memberName, exampleId + "7-after-ReFocusingOnTheNewWindow", testWindow);

            Thread.Sleep(1000);

            // click on body
            testWindow.FindElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 8
            CheckSnapshot(membersCategory, memberName, exampleId + "8-after-ClickBodyToReFocusOnExampleWindow", testWindow);

            // close the new window
            ChromeNewWinDriver.Close();
// end check
// check shifting focus inside the browser window
            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXPathForComboBoxWithCssClass("vt-test-cmb2")).Click();

            //Compare snapshots 9
            CheckSnapshot(membersCategory, memberName, exampleId + "9-after-ClickToFocusSecondComboBox", testWindow);

            //Click ComboBox - Change Focus
            testWindow.FindElement(GetXpathForLeftSideBar()).Click();

            //Compare snapshots 10
            CheckSnapshot(membersCategory, memberName, exampleId + "10-after-ClickInsideMainWindowOutsideExampleWindow", testWindow);

            Thread.Sleep(1000);

            // Click on header
            testWindow.FindElement(By.Id("windowView2_header")).Click();

            //Compare snapshots 11
            CheckSnapshot(membersCategory, memberName, exampleId + "11-after-ClickHeaderToReFocusOnExampleWindow", testWindow);
// end check
        }

        public void ComboBoxSelectedIndexChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click ComboBox - Change Focus
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb")).Click();

            // click on body
            testWindow.WaitForElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickComboboxAndClickBody", testWindow);
            
            //Click ComboBox - Change Focus
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // click on body
            testWindow.WaitForElement(By.Id("windowView2-body")).Click();

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-OpenComboboxAndClickBody", testWindow);

            //Click ComboBox - Change Focus
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Item1")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: 0"));

            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-OpenComboboxAndSelectItem1", testWindow);

            //Click ComboBox - Change Focus
            testWindow.WaitForElement(GetXPathForComboBoxArrowWithCssClass("vt-test-cmb")).Click();

            // Choose an Item from the ComboBox Element
            testWindow.WaitForElement(GetXPathForComboBoxItem("Item2")).Click();

            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: 1"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-OpenComboboxAndSelectItem2", testWindow);
        }

        // Bug 21414 - Calling Focus method programmatically doesn't invoke the GotFocus event
        public void ComboBoxGotFocusFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Change Focus
            testWindow.FindElement(GetXPathForButtonWithText("Focus Top ComboBox >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBox 1 has the Focus"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ButtonToFocusTopComboBox", testWindow);

            //Click ComboBox - Change Focus
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-1")).SendKeys(Keys.Tab);
            //Wait for log label update 
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBox 2 has the Focus"));
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-TabToFocusButtomComboBox", testWindow);

            //Click ComboBox - Change Focus
            testWindow.WaitForElement(GetXPathForComboBoxWithCssClass("vt-test-cmb-1")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("ComboBox 1 has the Focus"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickToFocusTopComboBox", testWindow);
        }

        public void ComboBoxVisibleChangedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Invoke VisibleChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Visible Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: False"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ChangeVisibleToFalse", testWindow);
            //Click button - Invoke VisibleChanged
            testWindow.FindElement(GetXPathForButtonWithText("Change Visible Value >>")).Click();
            //Wait for log label update
            testWindow.WaitForElement(GetXPathForLabelWithText("value: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ChangeVisibleToTrue", testWindow);

            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }
    }
}
