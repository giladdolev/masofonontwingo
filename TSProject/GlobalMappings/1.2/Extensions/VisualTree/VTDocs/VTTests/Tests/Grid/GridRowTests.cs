﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class GridElementTests
    {
        //Bug 20866 
        public void GridGridRowSelectedFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            //Click button - Select First Row
            testWindow.FindElement(GetXPathForButtonWithText("Select/UnSelect first row >>")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("0, selected value: True"));
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-ClickButtonToSelectFirstRow", testWindow);

            //Click button - Select First Row
            testWindow.FindElement(GetXPathForButtonWithText("Select/UnSelect first row >>")).Click();

            Thread.Sleep(3000);
            //testWindow.WaitForElement(GetXPathForLabelWithText("0, selected value: False"));

            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-ClickButtonToUnSelectFirstRow", testWindow);

            //Click button - Select Last Row
            testWindow.FindElement(GetXPathForButtonWithText("Select/UnSelect last row >>")).Click();

            testWindow.WaitForElement(GetXPathForLabelWithText("2, selected value: True"));
            //Compare snapshots 3
            CheckSnapshot(membersCategory, memberName, exampleId + "3-after-ClickButtonSelectLastRow", testWindow);

            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "d")).Click();
            testWindow.WaitForElement(GetXPathForGridCellWithIdAndText("GridCol1", "d")).SendKeys(Keys.ArrowLeft);

            Thread.Sleep(3000);
            //testWindow.WaitForElement(GetXPathForLabelWithText("1, selected value: True"));

            //Compare snapshots 4
            CheckSnapshot(membersCategory, memberName, exampleId + "4-after-ClickHeaderToSelectMiddleRowLastRowShouldBeUnSelected", testWindow);
        }

        

    }
}
