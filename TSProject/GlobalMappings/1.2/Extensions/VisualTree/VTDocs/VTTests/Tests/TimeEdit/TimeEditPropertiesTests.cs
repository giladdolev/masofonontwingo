﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System;
using System.Collections.ObjectModel;

namespace VTTests
{
    public partial class TimeEditElementTests
    {

        public void BasicTimeEditFlow(IWebElement testWindow, string membersCategory, string memberName, string exampleId)
        {
            
            //Button Click - Change Enable value
            testWindow.FindElement(GetXPathForTimeEditArrowWithCssClass("vt-test-te")).Click();
            //Wait 2 seconds
            Thread.Sleep(2000);
            //Compare snapshots 1
            CheckSnapshot(membersCategory, memberName, exampleId + "1-after-OpenDropdown-Wait2Sec", testWindow);

            testWindow.FindElement(GetXPathForControlWithCssClass("vt-test-te")).SendKeys(Keys.ArrowDown + Keys.Enter);

            //Wait 2 seconds
            Thread.Sleep(2000);
            //Compare snapshots 2
            CheckSnapshot(membersCategory, memberName, exampleId + "2-after-Choose2ndItem-Wait2Sec", testWindow);

            //Close window
            testWindow.FindElement(GetXpathForWindowCloseBtn(exampleId)).Click();
        }

    }
}
