﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTTests
{
    [TestClass]
    public class TestUtils
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            WebTestBase.Initialize(context);
        }
        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            WebTestBase.Cleanup();
        }
    }
}
