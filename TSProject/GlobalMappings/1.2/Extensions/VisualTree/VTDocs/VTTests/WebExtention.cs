﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace VTTests
{
    static class WebExtention
    {

        public static IWebElement WaitForElement(this IWebDriver driver, By by)
        {
            var timeOut = TimeSpan.FromSeconds(30);

            var webDriverWait = new WebDriverWait(driver, timeOut);

            var element = webDriverWait.Until(ExpectedConditions.ElementIsVisible(by));

            driver.ScrollToElement(element);
            
            return element;
        }

        public static IWebElement WaitForElement(this IWebElement parentElement, By by)
        {
            var element = WebTestBase.CurrentWebDriver.WaitForElement(by);

            return parentElement.FindElement(by);
        }

        public static void WaitWhileElementExistOnPage (this IWebDriver driver, IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.StalenessOf(element));

            Thread.Sleep(100);
        }

        public static void WaitWhileAttachedToPage (this IWebElement element)
        {
            WebTestBase.CurrentWebDriver.WaitWhileElementExistOnPage(element);
        }

        public static void ScrollToElement(this IWebDriver driver, IWebElement element)
        {
            driver.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(100); 
        }

        public static object ExecuteScript(this IWebDriver driver, string script, params object[] args)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            return js.ExecuteScript(script, args);
        }

        public static void WaitForImageResources(this IWebElement element)
        {
            string imgCache = "imgCache" + DateTime.UtcNow.Ticks.ToString();
            string script = @"
Ext.imgCache = Ext.imgCache || {};

function queueImage(imageUrl){
	if(!imageUrl || Ext.imgCache[imageUrl] === 1){
		return true;		
	}

	if(Ext.imgCache[imageUrl] !== 0){
		var img = new Image();
		
		img.onload = function () {
			Ext.imgCache[imageUrl] = 1;
		};

        img.onerror = function () {
			Ext.imgCache[imageUrl] = 1;
		};

		img.src = imageUrl;
		Ext.imgCache[imageUrl] = 0;
	}
	
	
	return false;
}

return ([].slice.call(arguments[0].querySelectorAll('*'), 0)).reduce(function(prevVal, elem){
	var style = elem.currentStyle || window.getComputedStyle(elem, false),
		imageUrl = style.backgroundImage && style.backgroundImage.slice(4, -1).replace(/[""']/g, '');
	
	if(elem.tagName === 'IMG'){
		prevVal = prevVal && queueImage(elem.src);
	}
    prevVal = prevVal && queueImage(imageUrl);
    return prevVal;
}, true);

".Replace("imgCache", imgCache);

            WebDriverWait wait = new WebDriverWait(WebTestBase.CurrentWebDriver, TimeSpan.FromSeconds(30));
            wait.Until(wd => (bool)wd.ExecuteScript(script, element) == true);

            //Clear mess 
            WebTestBase.CurrentWebDriver.ExecuteScript(string.Format( "delete Ext['{0}']", imgCache));
            Thread.Sleep(100);
        }
    }
}
