﻿using System.Web.Mvc;

namespace VTDocs.App_Start
{
    public class ExampleViewEngine : WebFormViewEngine
    {

        public ExampleViewEngine()
        {
            var viewLocations = new[]
            {
                "~/Examples/%1/Views/{1}/{0}.aspx",
                "~/Examples/%1/Views/{1}/{0}.ascx",
                "~/Examples/%1/Views/{0}.ascx",
                "~/Examples/%1/Views/{0}.aspx",
                "~/CustomPages//Views/{0}.aspx",
                "~/Views/Home/{0}.aspx"
            };

            this.PartialViewLocationFormats = viewLocations;
            this.ViewLocationFormats = viewLocations;
        }


        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            partialPath = FormatExamplePath(controllerContext, partialPath);
            return base.CreatePartialView(controllerContext, partialPath);
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            masterPath = FormatExamplePath(controllerContext, masterPath);
            viewPath = FormatExamplePath(controllerContext, viewPath);
            return base.CreateView(controllerContext, viewPath, masterPath);
        }

        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            virtualPath = FormatExamplePath(controllerContext, virtualPath);
            return base.FileExists(controllerContext, virtualPath);
        }

        private static string FormatExamplePath(ControllerContext controllerContext, string inputPath)
        {
            if (controllerContext != null)
            {
                var requestContext = controllerContext.RequestContext;
                if (requestContext != null)
                {
                    var routeData = requestContext.RouteData;
                    if (routeData != null)
                    {
                        var routeValuesDic = routeData.Values;
                        if (routeValuesDic != null && routeValuesDic.Count > 0)
                        {
                            string elementName = "";
                            string exampleName = "";
                            //TODO: Member Example support

                            string replaceString = "";
                            if (routeValuesDic.ContainsKey("ElementName"))
                            {
                                elementName = routeValuesDic["ElementName"].ToString();
                            }
                            if (routeValuesDic.ContainsKey("ExampleName"))
                            {
                                exampleName = routeValuesDic["ExampleName"].ToString();
                            }
                            //TODO: Member Example support

                            if (!string.IsNullOrEmpty(elementName) && !string.IsNullOrEmpty(exampleName))
                            {
                                //TODO: Member Example support
                                replaceString = elementName + "/" + exampleName;
                                inputPath = inputPath.Replace("%1", replaceString);
                            }
                        }
                    }
                }
            }
            return inputPath;
        }
    }
}