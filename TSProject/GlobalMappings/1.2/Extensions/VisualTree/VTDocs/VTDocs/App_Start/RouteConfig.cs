﻿using System.Web.Mvc;
using System.Web.Routing;

namespace VTDocs
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("GetPictureHandler");

            routes.MapRoute(
                "Examples",
                "Examples/{ElementName}/{ExampleName}/{controller}/{action}"
                );
            routes.MapRoute(
                "CustomPages",
                "CustomPages/{controller}/{action}/{id}",
                new { controller = "Article", action = "Article", id = UrlParameter.Optional },
                new string[] { "VTDocs.Controllers" }
                );

            //Route to VTFocs/Html/RegexCharacters.html file
            routes.MapPageRoute("RegexCharacters", "RegexCharactersURL", "~/Html/RegexCharacters.html");

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new string[] { "VTDocs.Controllers" }
                );

        }
    }
}
