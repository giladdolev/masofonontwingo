<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Text="" ID="articleCompositeView" Dock="Fill">
    <vt:Label runat="server" Text='Article....'  Height="26" BorderStyle="Solid" Dock="Top" />
</vt:CompositeView>
