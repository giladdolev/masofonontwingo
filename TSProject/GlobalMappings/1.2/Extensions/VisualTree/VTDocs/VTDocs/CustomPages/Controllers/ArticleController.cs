using System.Web.Mvc;

namespace VTDocs.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ArticleController : Controller
    {

        /// <summary>
        /// Components the specified element identifier.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <returns></returns>
        public ActionResult Article(string elementID)
        {
            return this.View();
        }

    }
}