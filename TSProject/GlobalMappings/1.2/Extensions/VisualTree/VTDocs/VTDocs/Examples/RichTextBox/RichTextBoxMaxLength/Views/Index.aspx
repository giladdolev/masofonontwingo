<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox MaxLength Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px" LoadAction="RichTextBoxMaxLength\Load">

        <vt:Label runat="server" Text="MaxLength Property is initially set to --> " Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="240px"></vt:Label>

        <vt:RichTextBox runat="server" Text="Hello World" Top="125px" Left="140px" ID="rtfMaxLength" Height="100px" Width="150px" MaxLength="5"></vt:RichTextBox>

        <vt:Button runat="server" Text="Set MaxLength" Top="300px" Left="100px" ID="btnSetMaxLength" Height="30px" Width="150px" ClickAction="RichTextBoxMaxLength\btnSetMaxLength_Click"></vt:Button>

        <vt:Button runat="server" Text="Get MaxLength" Top="300px" Left="260px" ID="btnGetMaxLength" Height="30px" Width="150px" ClickAction="RichTextBoxMaxLength\btnGetMaxLength_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
