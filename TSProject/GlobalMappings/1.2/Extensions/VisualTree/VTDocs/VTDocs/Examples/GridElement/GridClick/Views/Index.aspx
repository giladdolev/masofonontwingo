﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"  %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Click event" ID="Form1"  LoadAction="GridClick\OnLoad">
         <vt:Label runat="server" SkinID="Title" Text="Click" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when any part of the grid is clicked, including borders and padding.
            
            Syntax: public event EventHandler Click" 
            Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can click on any part of the grid with the mouse. 
            Each invocation of the 'Click' event should add a line to the 'Event Log'."
            Top="240px" ID="lblExp2">
        </vt:Label>
              
         <vt:Grid runat="server" Top="320px" Width="300px" ID="TestedGrid" CssClass="vt-test-grid" ClickAction="GridClick\TestedGrid_Click">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridCheckBoxColumn>
                <vt:GridButtonColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridButtonColumn>
            </Columns>
        </vt:Grid>
        
         <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog" Text="GridClick event"></vt:Label>

         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="500px" ID="txtEventLog"></vt:TextBox>
         
    </vt:WindowView>
</asp:Content>
