using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxCharacterCasingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCharacterCasing_Click(object sender, EventArgs e)
        {
            TextBoxElement txtCharacterCasingChange = this.GetVisualElementById<TextBoxElement>("txtCharacterCasingChange");

            TextBoxElement TextBox1 = this.GetVisualElementById<TextBoxElement>("TextBox1");

            if (txtCharacterCasingChange.CharacterCasing == CharacterCasing.Lower)
            {
                txtCharacterCasingChange.CharacterCasing = CharacterCasing.Upper;
                TextBox1.Text = "CharacterCasing value:" + txtCharacterCasingChange.CharacterCasing;
            }
            else if (txtCharacterCasingChange.CharacterCasing == CharacterCasing.Upper)
            {
                txtCharacterCasingChange.CharacterCasing = CharacterCasing.Normal;
                TextBox1.Text = "CharacterCasing value:" + txtCharacterCasingChange.CharacterCasing;
            }
            else
            {
                txtCharacterCasingChange.CharacterCasing = CharacterCasing.Lower;
                TextBox1.Text = "CharacterCasing value:" + txtCharacterCasingChange.CharacterCasing;
            }
           
        }

    }
}