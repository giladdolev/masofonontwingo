<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown PixelWidth Property" ID="windowView1" LoadAction="NumericUpDownPixelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels.
            
            Syntax: public int PixelWidth { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this NumericUpDown is initially set to: 170px" Top="235px" ID="lblExp1"></vt:Label>

        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="295px" ID="TestedNumericUpDown"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="335px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelWidth value >>" Top="400px" ID="btnChangePixelWidth" TabIndex="3" Width="180px" ClickAction="NumericUpDownPixelWidth\btnChangePixelWidth_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
