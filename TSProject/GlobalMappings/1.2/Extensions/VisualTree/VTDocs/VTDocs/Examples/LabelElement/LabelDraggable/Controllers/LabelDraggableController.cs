﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelDraggableController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnDraggable_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");
            TextBoxElement txtDraggable = this.GetVisualElementById<TextBoxElement>("txtDraggable");

            if (label.Draggable == false)
            {
                label.Draggable = true;
                txtDraggable.Text = label.Draggable.ToString();
            }
            else
            {
                label.Draggable = false;
                txtDraggable.Text = label.Draggable.ToString();
            }
        }
    }
}