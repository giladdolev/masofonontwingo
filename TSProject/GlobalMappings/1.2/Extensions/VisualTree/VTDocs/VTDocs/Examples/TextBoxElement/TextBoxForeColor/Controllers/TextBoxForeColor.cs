using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
      
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedTextBox1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedTextBox2.ForeColor + '.';
        }

        public void btnChangeTextBoxForeColor_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTextBox2.ForeColor == Color.Green)
            {
                TestedTextBox2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedTextBox2.ForeColor + '.';
            }
            else
            {
                TestedTextBox2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedTextBox2.ForeColor + '.';
            }
        }

    }
}