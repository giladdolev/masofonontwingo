﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckedListBox GetItemChecked Method (Int32)" Height="750px" ID="windowView1" LoadAction="CheckedListBoxGetItemChecked\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="GetItemChecked" Left="270px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Returns a value indicating whether the specified item is checked.

            Syntax: public bool GetItemChecked(int selectedItemIndex)
            Property Value : 'true' if the item is checked; otherwise, 'false'.
            
            Remarks: GetItemChecked returns true if the value of CheckState is Checked or Indeterminate for the item.
             To determine the specific state the item is in, use the GetItemCheckState method."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="235px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Use the button below to invoke GetItemChecked(Index) on all CheckListBox items or to invoke  
             SetCheckedState method with the index and CheckState inputs.
             "
            Top="275px" ID="lblExp2">
        </vt:Label>

        <vt:CheckedListBox runat="server" ID="testedCheckedListBox" Height="120px" Top="335px" >
            <Items>
                <vt:ListItem runat="server" ID="listItem1" Text="CheckBox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem2" Text="CheckBox 2"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem3" Text="CheckBox 3"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem4" Text="CheckBox 4"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem5" Text="CheckBox 5"></vt:ListItem>
            </Items>
        </vt:CheckedListBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Height="80px" Top="470px"></vt:Label>

        <vt:Button runat="server" Text="Invoke GetItemChecked(index) For All >>" Top="580px" ID="btnGetItemCheckedForAll" Width="250px" ClickAction="CheckedListBoxGetItemChecked\btnGetItemCheckedForAll_Click"></vt:Button>

        <vt:Label runat="server" Text="Select Index:" Left="80px"  ID="lblSelectIndex" Height="80px" Top="635px"></vt:Label>

         <vt:ComboBox runat="server" Width="110px" CssClass="vt-cmbSelectIndex" Text="Select Index" Top="655px" ID="cmbSelectIndex" SelectedIndexChangedAction="CheckedListBoxGetItemChecked\cmbSelectIndex_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="0"></vt:ListItem>
                <vt:ListItem runat="server" Text="1"></vt:ListItem>
                <vt:ListItem runat="server" Text="2"></vt:ListItem>
                <vt:ListItem runat="server" Text="3"></vt:ListItem>
                <vt:ListItem runat="server" Text="4"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:CheckBox runat="server" Top="655px" Left="230px" ID="chkCheckState" Text="CheckState"></vt:CheckBox>

        <vt:Button runat="server" Text="Invoke SetItemChecked(Int32, Boolean) >>" Top="655px" Left="370px"  ID="btnSetItemChecked" Width="250px" ClickAction="CheckedListBoxGetItemChecked\btnSetItemChecked_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
