using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownTabIndexTabStopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnChangeTabIndex_Click(object sender, EventArgs e)
        {
            NumericUpDownElement btnChangeTabIndex = this.GetVisualElementById<NumericUpDownElement>("btnChangeTabIndex");
            NumericUpDownElement NumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown1");
            NumericUpDownElement NumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown2");
            NumericUpDownElement NumericUpDown4 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown4");
            NumericUpDownElement NumericUpDown5 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown5");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (NumericUpDown1.TabIndex == 1)
            {
                NumericUpDown5.TabIndex = 1;
                NumericUpDown4.TabIndex = 2;
                NumericUpDown2.TabIndex = 4;
                NumericUpDown1.TabIndex = 5;

                textBox1.Text = "\\r\\nNumericUpDown1 TabIndex: " + NumericUpDown1.TabIndex + "\\r\\nNumericUpDown2 TabIndex: " + NumericUpDown2.TabIndex + "\\r\\nNumericUpDown4 TabIndex: " + NumericUpDown4.TabIndex + "\\r\\nNumericUpDown5 TabIndex: " + NumericUpDown5.TabIndex;       

            }
            else
            { 
                NumericUpDown1.TabIndex = 1;
                NumericUpDown2.TabIndex = 2;
                NumericUpDown4.TabIndex = 4;
                NumericUpDown5.TabIndex = 5;

                textBox1.Text = "\\r\\nNumericUpDown1 TabIndex: " + NumericUpDown1.TabIndex + "\\r\\nNumericUpDown2 TabIndex: " + NumericUpDown2.TabIndex + "\\r\\nNumericUpDown4 TabIndex: " + NumericUpDown4.TabIndex + "\\r\\nNumericUpDown5 TabIndex: " + NumericUpDown5.TabIndex;
            }
        }
        private void btnChangeTabStop_Click(object sender, EventArgs e)
        {
            NumericUpDownElement btnChangeTabStop = this.GetVisualElementById<NumericUpDownElement>("btnChangeTabStop");
            NumericUpDownElement NumericUpDown3 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown3");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");


            if (NumericUpDown3.TabStop == false)
            {
                NumericUpDown3.TabStop = true;
                textBox2.Text = "NumericUpDown3 TabStop: \\r\\n" + NumericUpDown3.TabStop;

            }
            else
            {
                NumericUpDown3.TabStop = false;
                textBox2.Text = "NumericUpDown3 TabStop: \\r\\n" + NumericUpDown3.TabStop;

            }
        }
    }
}