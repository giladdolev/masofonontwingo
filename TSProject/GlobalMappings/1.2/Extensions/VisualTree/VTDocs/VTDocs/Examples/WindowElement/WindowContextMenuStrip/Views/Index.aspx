<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window ContextMenuStrip Property" EnableEsc="true" ID="windowView2" LoadAction="WindowContextMenuStrip\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ContextMenuStrip" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the ContextMenuStrip associated with this control.
            
            Syntax:  public ContextMenuStripElement ContextMenuStrip { get; set; }
            Property Value: The ContextMenuStrip for this control, or null if there is no ContextMenuStrip.
            The default is null." Top="75px"  ID="Label2"></vt:Label>
               
        <vt:Button runat="server" SkinID="Wide" Text="Open Default Window" Top="190px" ID="TestedButton1" ClickAction="WindowContextMenuStrip\TestedButton1_Click"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="Label3"></vt:Label>
        
        <vt:Label runat="server" Text="In the following example, You can click the 'Open Initialized Window' button to open the 'TestedWindow'. 
            The ContextMenuStrip property of this 'TestedWindow' is initially set to 'contextMenuStrip1' with 
            menu item 'Hello'. Inside the 'TestedWindow' you can use the buttons to set the ContextMenuStrip 
            property to a different value" Top="350px"  ID="Label4"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="Open Initialized Window" Top="480px" ID="TestedButton2" ClickAction="WindowContextMenuStrip\TestedButton2_Click"></vt:Button>

        <%--<vt:Label runat="server" SkinID="Log" Top="530px" ID="lblLog2"></vt:Label>--%>

 <%--       <vt:Button runat="server" Text="contextMenuStrip1" Top="575px"  ID="btnChangeToContextMenuStrip1" ClickAction="WindowContextMenuStrip\btnChangeToContextMenuStrip1_Click"></vt:Button>

        <vt:Button runat="server" Text="contextMenuStrip2" Top="575px" Left="270px" ID="btnChangeToContextMenuStrip2" ClickAction="WindowContextMenuStrip\btnChangeToContextMenuStrip2_Click"></vt:Button>

        <vt:Button runat="server" Text="Reset" Top="575px" Left="460px" ID="btnReset" ClickAction="WindowContextMenuStrip\btnReset_Click"></vt:Button>--%>

    </vt:WindowView>
</asp:Content>
