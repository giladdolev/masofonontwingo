using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.IO;


namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewListViewItemCollectionAdd2StringController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "Press on one of the buttons...";
        }

        //Adds one item with text and image on each clicking
        public void btnAddListViewItem_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            populate1();
            lblLog1.Text = "A new item with text and image was added.";
            
        }

        //Adds list of items with text and image on each clicking
        public void btnAddListOfListViewItem_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            populate2();
            lblLog1.Text = "List of items with text and image was added.";

        }

        private void populate1()
        {

            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");

            //Imagelist to hold images
            ImageList imgs = new ImageList();
            imgs.ImageSize = new Size(50, 50);

            //Load images from file
            string[] paths ;
            string contentPathExtension = "Content/Images/ListView";
            paths = Directory.GetFiles(Path.Combine(System.Web.HttpRuntime.AppDomainAppPath,contentPathExtension));


            try
            {
                foreach (string path in paths)
                {
                    imgs.Images.Add(Image.FromFile(path));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            //Bind imgs to listview
            TestedListView.SmallImageList = imgs;
            TestedListView.Items.Add("0", "Cute");

        }

        private void populate2()
        {

            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");

            //Imagelist to hold images
            ImageList imgs = new ImageList();
            imgs.ImageSize = new Size(50, 50);

            //Load images from file
            string[] paths;
            string contentPathExtension = "Content/Images/ListViewList";
            paths = Directory.GetFiles(Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, contentPathExtension));

            try
            {
                foreach (string path in paths)
                {
                    imgs.Images.Add(Image.FromFile(path));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            //Bind imgs to listview
            TestedListView.SmallImageList = imgs;
            TestedListView.Items.Add("0", "Cute");
            TestedListView.Items.Add("1", "Desert");
            TestedListView.Items.Add("2", "Chrysanthemum");

        }


        public void btnRemoveListViewItem_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedListView.Items.Count > 0)
            TestedListView.Items.RemoveAt(TestedListView.Items.Count - 1);

            lblLog1.Text = "An item was removed from TestedListView.";
        }

    }
} 