using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedTextBox1.Enabled.ToString();

            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedTextBox2.Enabled.ToString() + ".";

        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedTextBox2.Enabled = !TestedTextBox2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedTextBox2.Enabled.ToString() + ".";
        }
      
    }
}