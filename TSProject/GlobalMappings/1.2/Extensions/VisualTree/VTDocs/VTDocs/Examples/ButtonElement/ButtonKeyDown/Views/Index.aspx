<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button KeyDown event" ID="windowView2" LoadAction="ButtonKeyDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="KeyDown" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when a key is pressed while the control has focus.

            Syntax: public event KeyEventHandler KeyDown"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can change the background color of the Button which has the focus
            by pressing one of the following keys:
            1. 'G' for Green color.
            2. 'r' for Red color.
            3. '*' for Transparent color.
            4. Space for Pink color
             Each invocation of the 'KeyDown' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="395px" Height="50px" Width="505px">
            <vt:Button runat="server" Top="15px" Left="15px" Text="Button1" ID="btnButton1" TabIndex="1" KeyDownAction="ButtonKeyDown\btnButton1_KeyDown"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="175px" Text="Button2" ID="btnButton2" TabIndex="2" KeyDownAction="ButtonKeyDown\btnButton2_KeyDown"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="335px" Text="Button3" ID="btnButton3" TabIndex="3" KeyDownAction="ButtonKeyDown\btnButton3_KeyDown"></vt:Button>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Height="40px" Top="460px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="515px" Width="360px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
