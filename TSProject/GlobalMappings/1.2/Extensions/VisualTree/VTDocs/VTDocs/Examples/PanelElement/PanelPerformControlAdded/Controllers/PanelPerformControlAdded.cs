using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformControlAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlAdded_Click(object sender, EventArgs e)
        {
            
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");

            ControlEventArgs args = new ControlEventArgs(testedPanel);

            testedPanel.PerformControlAdded(args);
        }

        public void testedPanel_ControlAdded(object sender, EventArgs e)
        {
            MessageBox.Show("TestedPanel ControlAdded event method is invoked");
        }

    }
}