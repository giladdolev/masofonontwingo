﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Compatibility;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    public class FileListBoxBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void FileUploaded(object sender, EventArgs e)
        {
            FileListBoxElement flb1 = this.GetVisualElementById<FileListBoxElement>("flb1");

            MessageBox.Show(flb1.FileName, "File Uploaded");
        }
        public void SelectedItemChanged(object sender, EventArgs e)
        {
            FileListBoxElement flb1 = this.GetVisualElementById<FileListBoxElement>("flb1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = String.Format("{0} file selected", flb1.FileName);
        }
    }
}