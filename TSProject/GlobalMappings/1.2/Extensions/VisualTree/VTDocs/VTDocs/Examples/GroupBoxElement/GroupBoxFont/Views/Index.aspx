<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Font Property" ID="windowView1" LoadAction="GroupBoxFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:GroupBox runat="server" Text="GroupBox" Top="185px" ID="TestedGroupBox1"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="280px" Width="400px" Height="55px" ID="lblLog1"></vt:Label>
 
 


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this GroupBox is initially set to:
             Font-Names='Niagara Engraved', Font-Italic='true' and Font-Size='50'" Top="400px"  ID="lblExp1"></vt:Label>     
        
      
        <vt:GroupBox runat="server" Text="TestedGroupBox" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="50" Top="455px" ID="TestedGroupBox2"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="550px" Width="400px" Height="55px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font value >>" Top="630px" ID="btnChangeGroupBoxFont" ClickAction="GroupBoxFont/btnChangeGroupBoxFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>