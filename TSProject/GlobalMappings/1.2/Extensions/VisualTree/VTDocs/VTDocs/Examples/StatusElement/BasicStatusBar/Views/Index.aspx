<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Status Bar" ID="windowView1">

        <vt:Label runat="server" SkinID="Title" Text="Status Bar" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="In this example we will demonstrate the use of Status Bar.
            This status bar has a few items within, like :
            - ProgressBar
            - DropDownButton
            - Label 
            
            Any ToolbarItem can be added to the Status bar as well." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Status runat="server" ID="statusBar">
            <vt:DropDownButton runat="server" ID="statusBarBtn"></vt:DropDownButton>
            <vt:ToolBarLabel runat="server" Text="Label" ID="statusBarLabel"></vt:ToolBarLabel>
            <vt:ToolBarProgressBar runat="server" ID="statusBarProgressBar"></vt:ToolBarProgressBar>
        </vt:Status>
        

    </vt:WindowView>
</asp:Content>
