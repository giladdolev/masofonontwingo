using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new RadioButtonCreateControl());

        }

        private RadioButtonCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as RadioButtonCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement newRadioButton = new RadioButtonElement();

            newRadioButton.CreateControl();
            newRadioButton.Width = 100;
            newRadioButton.Height = 20;


            this.ViewModel.Controls.Add(newRadioButton);
            

            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}