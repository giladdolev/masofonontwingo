using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabAppearanceController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TabAlignment
        public void btnChangeAppearance_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabRuntimeAppearance = this.GetVisualElementById<TabElement>("tabRuntimeAppearance");

            if (tabRuntimeAppearance.Appearance == AppearanceConstants.None)
            {
                tabRuntimeAppearance.Appearance = AppearanceConstants.ccFlat;
                textBox1.Text = "AppearanceConstants.ccFlat";
            }
            else if (tabRuntimeAppearance.Appearance == AppearanceConstants.ccFlat)
            {
                tabRuntimeAppearance.Appearance = AppearanceConstants.cc3D;
                textBox1.Text = "AppearanceConstants.cc3D";
            }
            else if (tabRuntimeAppearance.Appearance == AppearanceConstants.cc3D)
            {
                tabRuntimeAppearance.Appearance = AppearanceConstants.None;
                textBox1.Text = "AppearanceConstants.None";
            }
        }

    }
}