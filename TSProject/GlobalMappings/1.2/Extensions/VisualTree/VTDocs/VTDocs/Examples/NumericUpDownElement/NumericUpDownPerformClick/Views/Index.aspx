<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:NumericUpDown runat="server" Text="Tested NumericUpDown" Top="150px" Left="200px" ID="TestedNumericUpDown" Height="20px" Width="100px" ClickAction="NumericUpDownPerformClick\TestedNumericUpDown_Click"></vt:NumericUpDown>

        <vt:Button runat="server" Text="PerformClick of 'Tested NumericUpDown'" Top="45px" Left="140px" ID="btnPerformClick" Height="36px" Width="300px" ClickAction="NumericUpDownPerformClick\btnPerformClick_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
