using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedCheckBox.ClientSize;

        }
        public void btnChangeCheckBoxClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            if (TestedCheckBox.Width == 200)
            {
                TestedCheckBox.ClientSize = new Size(150, 26);
                lblLog.Text = "ClientSize value: " + TestedCheckBox.ClientSize;

            }
            else
            {
                TestedCheckBox.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedCheckBox.ClientSize;
            }

        }

    }
}