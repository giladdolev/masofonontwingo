<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView DoubleClick event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewDoubleClick/Form_Load">
  
        <vt:Label runat="server" Text="Run Time : Click Double click on the ListView" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ListView runat="server" ID="listView1" Top="160px" Left="50px" Height="90px" Width="250px" DoubleClickAction="ListViewDoubleClick\btnDoubleClick_DoubleClick"/>
        <vt:Label runat="server" Text="" Top="112px" Left="150px" ID="lblEvent" Width="350px" Font-Bold="true"></vt:Label>

    </vt:WindowView>
</asp:Content>
