﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelAutoEllipsisController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void button1_Click(object sender, EventArgs e)
        {

            LabelElement label = this.GetVisualElementById<LabelElement>("Label1");
            label.AutoSize = !label.AutoSize;
            label.Text = "Label1AutoSize=" + label.AutoSize.ToString() + " Label1AutoEllipsis=" + label.AutoEllipsis.ToString();
        }
        public void button2_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("Label1");
            label.AutoEllipsis = !label.AutoEllipsis;
            label.Text = "Label1AutoSize=" + label.AutoSize.ToString()+" Label1AutoEllipsis=" + label.AutoEllipsis.ToString();
        }
    }
}