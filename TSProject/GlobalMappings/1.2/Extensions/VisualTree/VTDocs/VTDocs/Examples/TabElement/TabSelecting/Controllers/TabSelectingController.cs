using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSelectingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "Please select a tab...";
        }
        public void TestedTab_Deselecting(object sender, TabViewCancelEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTabControl 'Deselecting' event is invoked";
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void TestedTab_Deselected(object sender, TabControlEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTabControl 'Deselected' event is invoked";
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void TestedTab_Selecting(object sender, TabViewCancelEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTabControl 'Selecting' event is invoked";
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void TestedTab_Selected(object sender, TabControlEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTabControl 'Selected' event is invoked";
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnSelecttabItem1_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabItem tabItem1 = this.GetVisualElementById<TabItem>("tabItem1");

            TestedTab.SelectedTab = tabItem1;
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnSelecttabItem2_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            //TabItem tabItem2 = this.GetVisualElementById<TabItem>("tabItem2");

            TestedTab.SelectedIndex = 1;
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnSelecttabItem3_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabItem tabItem3 = this.GetVisualElementById<TabItem>("tabItem3");

            TestedTab.SelectedTab = tabItem3;
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnClearEventLog_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();
            txtEventLog.Text = "Event Log:";
        }
    }
}

