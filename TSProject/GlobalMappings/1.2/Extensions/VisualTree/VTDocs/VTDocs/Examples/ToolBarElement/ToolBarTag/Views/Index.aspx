
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Tag Property" ID="windowView1" LoadAction="ToolBarTag\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Tag" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the object that contains data about the control.
            
            Syntax: public object Tag { get; set; }
            Value: An Object that contains data about the control. The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:ToolBar runat="server" Text="ToolBar" Dock="None" Top="170px" ID="TestedToolBar1">
            <vt:ToolBarButton runat="server" id="ToolBar1Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar1Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar1Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="225px" Width="430" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Tag property of this ToolBar is initially set to the string 'New Tag'." Top="330px"  ID="lblExp1"></vt:Label>     
        
        <vt:ToolBar runat="server" Text="TestedToolBar" Dock="None" Tag="New Tag." Top="380px" ID="TestedToolBar2">
            <vt:ToolBarButton runat="server" id="ToolBar2Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar2Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar2Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="435px" Width="430" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Tag value >>" Top="490px" ID="btnChangeBackColor" ClickAction="ToolBarTag\btnChangeTag_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

