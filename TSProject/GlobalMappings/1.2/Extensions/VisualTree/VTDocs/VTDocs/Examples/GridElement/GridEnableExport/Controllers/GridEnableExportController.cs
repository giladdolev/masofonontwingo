using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
using System.Drawing;

namespace HelloWorldApp
{
    public class GridEnableExportController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            gridElement.TextButtonList.Add("TextButtonColumn");
            //gridElement.AddToComboBoxList("ComboBoxColumn", "male|1,female|2");
            gridElement.AddToComboBoxList("ComboBoxColumn", "male,female");

            gridElement.Rows.Add("male", "male","" , "24/09/2017" ,"female" ,666);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("gridElement");
            GridImageColumn a = new GridImageColumn();
 
            //Create column
            GridColumn col;
            col = new GridColumn("name");
            col.Width = 50;
            dgvCalendario.MaxRows = 1;
            //Create row          
            GridRow gridRow = new GridRow();
            GridContentCell gridCellElement1 = new GridContentCell();
            gridCellElement1.Value = "new Content";
            gridRow.Cells.Add(gridCellElement1);


            //Add to grid
            dgvCalendario.Columns.Add(col);
            dgvCalendario.Columns.Add(a);
            dgvCalendario.Rows.Add(gridRow);



            GridRow gridRow1 = new GridRow();
            GridContentCell gridCellElement11 = new GridContentCell();
            gridCellElement11.Value = "new Content1";
            gridRow.Cells.Add(gridCellElement11);


            //Add to grid
            dgvCalendario.Columns.Add(col);
            dgvCalendario.Columns.Add(a);
            dgvCalendario.Rows.Add(gridRow1);
            dgvCalendario.RefreshData();
            dgvCalendario.Refresh();

        }

    
    }
}
