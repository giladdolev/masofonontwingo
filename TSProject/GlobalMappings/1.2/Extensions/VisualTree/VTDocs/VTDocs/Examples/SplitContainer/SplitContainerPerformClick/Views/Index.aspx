<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:SplitContainer runat="server" Text="Tested SplitContainer" Top="150px" Left="200px" ID="TestedSplitContainer" Height="70px" Width="200px" ClickAction="SplitContainerPerformClick\TestedSplitContainer_Click"></vt:SplitContainer>

        <vt:Button runat="server" Text="PerformClick of 'Tested SplitContainer'" Top="45px" Left="140px" ID="btnPerformClick" Height="36px" Width="300px" ClickAction="SplitContainerPerformClick\btnPerformClick_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
