<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

     <vt:WindowView runat="server" Text="SplitContainer Orientation Property" ID="windowView1" LoadAction="SplitContainerOrientation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="250px" Text="Orientation" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating the horizontal or vertical orientation of the SplitContainer panels.

            Syntax: public Orientation Orientation { get; set; }
            Value: One of the Orientation values. The default is Vertical."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested SplitContainer1 -->
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="170px" ID="TestedSplitContainer1"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Orientation property of this SplitContainer is initially set to 'Horizontal'." Top="390px" ID="lblExp1"></vt:Label>

        <!-- Tested SplitContainer2 -->
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Orientation="Horizontal" Top="450px" ID="TestedSplitContainer2"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="545px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Orientation value >>" Top="620px" Width="200px" ID="btnChangeSplitContainerOrientation" ClickAction="SplitContainerOrientation\btnChangeOrientation_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
