using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedListView1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedListView1);
            lblLog2.Text = "Font value: " + TestedListView2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedListView2);
        }

        private void btnChangeListViewFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            if (TestedListView2.Font.Underline == true)
            {
                TestedListView2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedListView2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedListView2);
            }
            else if (TestedListView2.Font.Bold == true)
            {
                TestedListView2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedListView2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedListView2);
            }
            else if (TestedListView2.Font.Italic == true)
            {
                TestedListView2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedListView2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedListView2);
            }
            else
            {
                TestedListView2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedListView2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedListView2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }

    }
}