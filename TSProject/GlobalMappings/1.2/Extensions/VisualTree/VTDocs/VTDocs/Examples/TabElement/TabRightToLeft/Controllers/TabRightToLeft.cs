using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabRightToLeft = this.GetVisualElementById<TabElement>("tabRightToLeft");

            if (tabRightToLeft.RightToLeft == RightToLeft.Inherit)
            {
                tabRightToLeft.RightToLeft = RightToLeft.No;
                textBox1.Text = tabRightToLeft.RightToLeft.ToString();
            }
            else if (tabRightToLeft.RightToLeft == RightToLeft.No)
            {
                tabRightToLeft.RightToLeft = RightToLeft.Yes;
                textBox1.Text = tabRightToLeft.RightToLeft.ToString();
            }
            else
            {
                tabRightToLeft.RightToLeft = RightToLeft.Inherit;
                textBox1.Text = tabRightToLeft.RightToLeft.ToString();
            }
        }

    }
}