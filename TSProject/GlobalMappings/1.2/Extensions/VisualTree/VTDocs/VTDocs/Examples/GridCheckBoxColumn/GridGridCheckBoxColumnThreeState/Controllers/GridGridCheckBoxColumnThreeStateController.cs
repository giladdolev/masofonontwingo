using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridGridCheckBoxColumnThreeStateController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void Page_load(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedGrid.Rows.Add(CheckState.Indeterminate, false, true);
            TestedGrid.Rows.Add(false, true, true);
            TestedGrid.Rows.Add(null, true, false);

            /* lblLog1.Text = "Column1 ThreeState: " + TestedGrid.Columns[0].ThreeState +
                 "\r\nColumn2 ThreeState: " + TestedGrid.Columns[1].ThreeState +
                 "\r\nColumn3 ThreeState: " + TestedGrid.Columns[2].ThreeState;
            
           
             DataTable source = new DataTable();
             source.Columns.Add("�������� ()", typeof(string));
             source.Columns.Add("TextButtonColumn", typeof(string));
             source.Columns.Add("CheckBoxColumn", typeof(bool));
             source.Columns.Add("DateTimeColumn", typeof(DateTime));
             source.Columns.Add("ComboBoxColumn", typeof(string));
             source.Columns.Add("Currency", typeof(double));

             source.Rows.Add("galilcs", @"c:\dir\", CheckState.Indeterminate, "20/11/2015 05:05:00", "male", 100);
             source.Rows.Add("Bart", @"c:\dir\file", null, "20/10/2015 00:00:00", "male", 10);
             source.Rows.Add("Homer", @"c:\dir\file", true, "15/10/2017 00:00:00", "female", 0.99);
             source.Rows.Add("Lisa", @"c:\dir\file", false, "01/02/2016 00:00:00", "male", 99);
             source.Rows.Add("Marge", @"c:\dir\file", CheckState.Indeterminate, "08/07/2015 00:00:00", "female", 8);
             source.Rows.Add("Galilcs", @"c:\dir\file", CheckState.Checked, "15/8/2015 00:00:00", "male", 234324);
             source.Rows.Add("Gizmox", @"c:\file", CheckState.Unchecked, "01/12/2015 00:00:00", "female", 34234);

             TestedGrid.DataSource = source;*/
        }

        private void btnSetColumnThreeState_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            TestedGrid.Rows[0].Cells[0].Value = null ;

        }

    }
}
