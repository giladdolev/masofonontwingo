using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage1 = this.GetVisualElementById<ImageElement>("TestedImage1");
            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedImage1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
        }
        
        public void btnChangeImageBorderStyle_Click(object sender, EventArgs e)
        {
            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedImage2.BorderStyle == BorderStyle.None)
            {
                TestedImage2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Dotted)
            {
                TestedImage2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Double)
            {
                TestedImage2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedImage2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedImage2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Groove)
            {
                TestedImage2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Inset)
            {
                TestedImage2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Dashed)
            {
                TestedImage2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.NotSet)
            {
                TestedImage2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Outset)
            {
                TestedImage2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Ridge)
            {
                TestedImage2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedImage2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else if (TestedImage2.BorderStyle == BorderStyle.Solid)
            {
                TestedImage2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
            else
            {
                TestedImage2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedImage2.BorderStyle + '.';
            }
        }
    }
}