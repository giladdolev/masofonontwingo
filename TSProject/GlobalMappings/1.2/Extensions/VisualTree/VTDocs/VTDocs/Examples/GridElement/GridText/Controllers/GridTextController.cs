using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace MvcApplication9.Controllers
{
    public class GridTextController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "Text value: " + TestedGrid1.Text.ToString();

            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "Text value: " + TestedGrid2.Text.ToString();

        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.Text == "TestedGrid")
            {
                TestedGrid2.Text = "NewText";
                lblLog2.Text = "Text value: " + TestedGrid2.Text.ToString();
            }
            else
            {
                TestedGrid2.Text = "TestedGrid";
                lblLog2.Text = "Text value: " + TestedGrid2.Text.ToString();

            }
        }


	}
}
