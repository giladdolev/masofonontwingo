<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox ClientSize Property" ID="windowView" LoadAction="GroupBoxClientSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the client area of the control.

            Syntax: public Size ClientSize { get; set; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus."
            Top="75px" ID="Label2">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The ClientSize property of this 'TestedGroupBox' is initially set with 
            Width: 200px, Height: 80px"
            Top="280px" ID="lblExp1">
        </vt:Label>

        <!-- TestedGroupBox -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="345px" ID="TestedGroupBox"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="440px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientSize value >>" Top="520px" Width="180px" ID="btnChangeGroupBoxSize" ClickAction="GroupBoxClientSize\btnChangeGroupBoxSize_Click"></vt:Button>



    </vt:WindowView>
</asp:Content>
