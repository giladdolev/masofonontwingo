<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox BackColor Property" ID="windowView1" LoadAction="RichTextBoxBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:RichTextBox runat="server" Text="RichTextBox" Top="140px" ID="TestedRichTextBox1"></vt:RichTextBox>
        <vt:Label runat="server" SkinID="Log" Top="235px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this 'TestedRichTextBox' is initially set to Gray" Top="350px"  ID="lblExp1"></vt:Label>     
        
        <vt:RichTextBox runat="server" Text="TestedRichTextBox" BackColor ="Gray" Top="400px" ID="TestedRichTextBox2"></vt:RichTextBox>
        <vt:Label runat="server" SkinID="Log" Top="495px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Top="560px" Width="180px" ID="btnChangeBackColor" ClickAction="RichTextBoxBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
