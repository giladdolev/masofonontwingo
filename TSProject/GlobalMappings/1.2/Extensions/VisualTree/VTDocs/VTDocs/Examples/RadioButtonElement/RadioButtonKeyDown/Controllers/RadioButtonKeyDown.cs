using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonKeyDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }



        public void rdoKeyDown_KeyDown(object sender, KeyEventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "RadioButton KeyDown event is invoked\\r\\n";

        }



    }
}