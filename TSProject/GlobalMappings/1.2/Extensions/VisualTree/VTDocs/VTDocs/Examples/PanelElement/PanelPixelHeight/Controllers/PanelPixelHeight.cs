using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Panel PixelHeight is: " + TestedPanel.PixelHeight;

        }
        public void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.PixelHeight == 80)
            {
                TestedPanel.PixelHeight = 40;
                lblLog.Text = "Panel PixelHeight is: " + TestedPanel.PixelHeight;

            }
            else
            {
                TestedPanel.PixelHeight = 80;
                lblLog.Text = "Panel PixelHeight is: " + TestedPanel.PixelHeight;
            }

        }
    }
}