<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox AutoSize Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="GroupBoxAutoSize\Form_Load">
        
        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:GroupBox runat="server" Text="AutoSize is set to true" AutoSize ="true" Top="55px"  Left="140px" ID="grpAutoSize" Height="70px" Width="150px">
            <vt:Button runat="server" Text="Button1" Top="43px" Left="-20px" ID="Button2" Height="36px" Width="100px" ></vt:Button>
        </vt:GroupBox>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:GroupBox runat="server" Text="RuntimeGroupBox" Top="165px" Left="140px" ID="grpRuntimeAutoSize" Height="70px" TabIndex="1" Width="150px" >
            <vt:Button runat="server" Text="Button1" Top="43px" Left="-20px" ID="Button1" Height="36px" Width="100px"></vt:Button>
        </vt:GroupBox>

        <vt:Button runat="server" Text="Change GroupBox AutoSize" Top="195px" Left="420px" ID="btnChangeAutoSize" Height="36px" TabIndex="1" Width="200px" ClickAction="GroupBoxAutoSize\btnChangeAutoSize_Click"></vt:Button>


        <vt:TextBox runat="server" ScrollBars="Both" ReadOnly="True" Multiline="True" Top="255px" Left="100px" ID="txtAutoSizeValue" BackColor="White" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="50px"  Width="300px">
		</vt:TextBox>

    </vt:WindowView>
</asp:Content>
