﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridRow RowData" ID="windowView1" LoadAction="GridRowData\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="RowData" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Returns or sets a user-defined variant associated with the given row.
            
            Syntax: public Dictionary<int, object> RowData {get , private set}"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="180px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example shows a use of RowData property with two possible data collections.
            The collections contains few data types." Top="230px" ID="Label1"></vt:Label>

       <vt:Grid runat="server" CssClass="vt-testedgrid" Top="300px" Height="160px" Width="300px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Wikdth="75px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="480px" Width="450px" Height="100px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Set RowData with RowsHeader colors >>" Top="600px" Width ="250px" ID="btnChangeRowData" ClickAction="GridRowData\btnChangeRowData_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
