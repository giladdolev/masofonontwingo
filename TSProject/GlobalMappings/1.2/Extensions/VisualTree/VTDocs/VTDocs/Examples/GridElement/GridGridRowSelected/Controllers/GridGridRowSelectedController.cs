using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridGridRowSelectedController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void Page_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));
            source.Columns.Add(")", typeof(bool));

            source.Rows.Add("Lisa", "lisa@simpsons.com", true);
            source.Rows.Add("Bart", "Bart@simpsons.com", true);
            source.Rows.Add("Homer", "Homer@simpsons.com", true);
            source.Rows.Add("Lisa", "Lisa@simpsons.com", false);
            source.Rows.Add("Marge", "Marge@simpsons.com", true);
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", true);
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", false);
            gridElement.DataSource = source;
            gridElement.CellEndEdit += moFlex_AfterEdit;
            gridElement.SelectionChanged += gridElement_SelectionChanged;
            gridElement.RefreshData();

            lblLog1.Text = "The Selected row is: Row: " + gridElement.SelectedRowIndex;
        }

        void gridElement_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("SelectionChanged");

        }

        private void moFlex_AfterEdit(object sender, EventArgs e)
        {

            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            int col = gridElement.SelectedColumnIndex == -1 ? 0 : gridElement.SelectedColumnIndex;// e.ColumnIndex - 1;
            int row = gridElement.SelectedRowIndex == -1 ? 0 : gridElement.SelectedRowIndex;// e.RowIndex;


           // MessageBox.Show("Values[" + row + "]" + "[" + col + "]= " + gridElement.Rows[row].Cells[col].Value);
        }

        protected void Select_First_Row(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));
            source.Columns.Add(")", typeof(bool));

            source.Rows.Add("Lisa", "lisa@simpsons.com", true);
            source.Rows.Add("Bart", "Bart@simpsons.com", true);
            source.Rows.Add("Homer", "Homer@simpsons.com", true);
            source.Rows.Add("Lisa", "Lisa@simpsons.com", false);
            source.Rows.Add("Marge", "Marge@simpsons.com", true);
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", true);
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", false);
            gridElement.DataSource = source;
            gridElement.CellEndEdit += moFlex_AfterEdit;
            gridElement.SelectionChanged += gridElement_SelectionChanged;
            gridElement.RefreshData();

            lblLog1.Text = "The Selected row is: Row: " + gridElement.SelectedRowIndex;

        }

        void gridElement_CellBeginEdit(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            gridElement.Rows[gridElement.SelectedCells[0].RowIndex].Selected = true;

            lblLog1.Text = "The Selected row is: Row: " + gridElement.SelectedRowIndex;

        }

        public void Select_Last_Row(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            gridElement.Rows[gridElement.Rows.Count - 1].Selected = true;

            lblLog1.Text = "The Selected row is: Row: " + gridElement.SelectedRowIndex;
        }

        public void Select_New_Row(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

          //  gridElement.Rows.Add("Elias", "elias@simpsons.com", true);
        //    gridElement.Rows.Add((object)DBNull.Value, (object)DBNull.Value, (object)DBNull.Value);

            var gridRow = new GridRow();
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridElement.Rows.Insert(0, gridRow);

            gridElement.RefreshData();
            gridElement.Refresh();

            lblLog1.Text = "The Selected row is: Row: " + gridElement.SelectedRowIndex;
        }

        public void Checked_Changed(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");

            lblLog1.Text = "The Selected row is: Row: " + gridElement.CurrentRow.Index;
        }


        
    }
}
