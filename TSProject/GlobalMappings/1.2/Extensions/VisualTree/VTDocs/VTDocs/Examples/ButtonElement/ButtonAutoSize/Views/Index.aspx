<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button AutoSize Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:Button runat="server" Text="button AutoSize is set to trueeeeeeeeeeeeeeeeeeeee" AutoSize ="true" Top="45px" Left="140px" ID="btnAutoSize" Height="36px" TabIndex="2" ></vt:Button>    

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:Button runat="server" Text="Tested Button" Top="115px"  AutoSize ="false" Left="140px" ID="btnTestedButton" Height="36px" Width="60px" TabIndex="3"  ></vt:Button>

        <vt:Button runat="server" Text="AutoSize = False" Top="115px" Left="250px" ID="btnChangeAutoSize" Height="36px"  TabIndex="3" Width="100px" ClickAction="ButtonAutoSize\btnChangeAutoSize_Click"></vt:Button>

         <vt:Button runat="server" Text="Change Text" Top="115px" Left="350px" ID="btnChangeText" Height="36px"  TabIndex="3" Width="100px" ClickAction="ButtonAutoSize\btnChangeText_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
