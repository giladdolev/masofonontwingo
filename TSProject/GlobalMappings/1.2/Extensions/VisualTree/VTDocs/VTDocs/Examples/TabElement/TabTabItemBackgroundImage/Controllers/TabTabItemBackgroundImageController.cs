using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemBackgroundImageController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            TabItem Tab1Item1 = TestedTab1.TabItems[0];
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (Tab1Item1.BackgroundImage == null)
            {
                lblLog1.Text = "No background image";
            }
            else
            {
                lblLog1.Text = "Set with background image";
            }


            if (Tab1Item2.BackgroundImage == null)
            {
                lblLog2.Text = "No background image.";
            }
            else
            {
                lblLog2.Text = "Set with background image.";
            }
        }



        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (Tab1Item2.BackgroundImage == null)
            {
                Tab1Item2.BackgroundImage = new UrlReference("/Content/Images/galilcs.png");
                lblLog2.Text = "Set with background image.";
            }
            else
            {
                Tab1Item2.BackgroundImage = null;
                lblLog2.Text = "No background image.";

            }
        }

    }
}