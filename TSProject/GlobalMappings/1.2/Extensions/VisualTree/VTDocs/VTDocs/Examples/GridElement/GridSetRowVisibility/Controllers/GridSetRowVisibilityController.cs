using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridSetRowVisibilityController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog.Text = "Row 2 visibility value: " + TestedGrid.Rows[1].Visible;
        }

        public void btnShow_Click(object sender, EventArgs e)
        {

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.SetRowVisibility(1, true);
            lblLog.Text = "Row 2 visibility value: " + TestedGrid.Rows[1].Visible;
        }

        public void btnHide_Click(object sender, EventArgs e)
        {

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.SetRowVisibility(1, false);
            lblLog.Text = "Row 2 visibility value: " + TestedGrid.Rows[1].Visible;
        }
    }
}