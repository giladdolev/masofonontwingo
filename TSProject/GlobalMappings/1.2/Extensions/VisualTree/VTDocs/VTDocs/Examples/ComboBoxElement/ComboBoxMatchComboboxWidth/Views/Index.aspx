<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox MatchComboboxWidth property" ID="windowView1" LoadAction="ComboBoxMatchComboboxWidth\OnLoad">
        <vt:Label runat="server" SkinID="Title" Left="250px" Text="MatchComboboxWidth" ID="title" >
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the width of the ComboBox dropdowns  
            matches the width of the ComboBox or the ComboBox dropdowns is resizes according to
            its content
           
            Syntax: public bool MatchComboboxWidth { get; set; }
            Property value: 'true' whether the dropdowns width should be explicitly set to match the width of
             the ComboBox, otherwise 'false' .
            The default value is 'false'.
            
            Note: This property has only initialize effect, that means you can change his property
             from the aspx file only."
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="327px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In following example TestedComboBox1 is set to MatchComboboxWidth false
            and TestedComboBox2 is set to MatchComboboxWidth true.
            " Top="377px"  ID="lblExp1"></vt:Label>

        <vt:ComboBox runat="server" ID="testedComboBox1" CssClass="vt-comboBox1" Text="TestedComboBox1" Top="470px" MatchComboboxWidth="false">
            <Items>
                <vt:ListItem runat="server" Text="If the length of the field is too small"></vt:ListItem>
                <vt:ListItem runat="server" Text="content"></vt:ListItem>
                <vt:ListItem runat="server" Text="content"></vt:ListItem>
                <vt:ListItem runat="server" Text="content"></vt:ListItem>
            </Items>
        </vt:ComboBox>
       
        

         <vt:ComboBox runat="server" ID="testedComboBox2" CssClass="vt-comboBox2" Text="TestedComboBox2" Top="470px" Left="300px" MatchComboboxWidth="true" >
            <Items>
                <vt:ListItem runat="server" Text="If the length of the field is too small "></vt:ListItem>
                <vt:ListItem runat="server" Text="content"></vt:ListItem>
                <vt:ListItem runat="server" Text="content"></vt:ListItem>
                <vt:ListItem runat="server" Text="content"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Height="30px" Width="385px"  Top="510px" ID="lblLog1"></vt:Label>
    </vt:WindowView>
</asp:Content>
