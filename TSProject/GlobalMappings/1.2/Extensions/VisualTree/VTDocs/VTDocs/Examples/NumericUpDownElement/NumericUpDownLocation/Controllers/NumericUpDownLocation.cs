using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownLocationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Location value: " + TestedNumericUpDown.Location;
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedNumericUpDown.Location == new Point(80, 310))
            {
                TestedNumericUpDown.Location = new Point(200, 290);
                lblLog.Text = "Location value: " + TestedNumericUpDown.Location;
            }
            else
            {
                TestedNumericUpDown.Location = new Point(80, 310);
                lblLog.Text = "Location value: " + TestedNumericUpDown.Location;
            }
        }
    }
}