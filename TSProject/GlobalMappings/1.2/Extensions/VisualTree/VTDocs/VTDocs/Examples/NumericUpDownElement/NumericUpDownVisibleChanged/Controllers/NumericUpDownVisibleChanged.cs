using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


       
        public void btnChangeVisibility_Click(object sender, EventArgs e)
        {
            NumericUpDownElement testedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("testedNumericUpDown");
            testedNumericUpDown.Visible = !testedNumericUpDown.Visible;

        }


        public void testedNumericUpDown_VisibleChanged(object sender, EventArgs e)
        {

            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            if (txtEventTrack != null)
                txtEventTrack.Text += "NumericUpDown VisibleChanged event is invoked\\r\\n";




        }
    }
}