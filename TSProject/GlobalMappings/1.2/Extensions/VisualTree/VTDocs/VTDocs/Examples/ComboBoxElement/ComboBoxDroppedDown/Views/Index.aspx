<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox DroppedDown Property" ID="windowView2"  LoadAction="ComboBoxDroppedDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="DroppedDown" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the combo box is displaying its drop-down portion.

            Syntax: public bool DroppedDown { get; set; }
            
            Value: true if the drop-down portion is displayed; otherwise, false. The default is false."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox" Top="180px" ID="TestedComboBox1"></vt:ComboBox>
        
        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="275px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="DroppedDown property of this ComboBox is initially set to 'true'.
             The drop-down portion be displayed." Top="325px" ID="lblExp1"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" DroppedDown="true" Top="390px" ID="TestedComboBox2" ></vt:ComboBox>
        
        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change DroppedDown value >>" Top="495px" ID="btnChangeDroppedDown" Width="200px" ClickAction ="ComboBoxDroppedDown\btnChangeDroppedDown_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        