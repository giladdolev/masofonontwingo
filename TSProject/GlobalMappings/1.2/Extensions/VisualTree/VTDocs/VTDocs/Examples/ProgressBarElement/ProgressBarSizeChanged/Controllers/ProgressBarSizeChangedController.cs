using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //ProgressBarAlignment
        public void btnChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ProgressBarElement prgSize = this.GetVisualElementById<ProgressBarElement>("prgSize");

            if (prgSize.Size == new Size(200,50)) //Get
            {
                prgSize.Size = new Size(250, 25); //Set
                textBox1.Text = "The Size property value is: " + prgSize.Size;
            }
            else  
            {
                prgSize.Size = new Size(200, 50); 
                textBox1.Text = "The Size property value is: " + prgSize.Size;
            }
             
        }
        
        public void prgSize_SizeChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "SizeChanged Event is invoked\\r\\n";

        }
    }
}