<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Visible Property" ID="windowView2"  LoadAction="ToolBarVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ToolBar runat="server" Text="ToolBar" Dock="None" Top="155px" ID="TestedToolBar1">
            <vt:ToolBarButton runat="server" id="ToolBar1Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar1Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar1Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>
        
        <vt:Label runat="server" SkinID="Log" Top="205px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="270px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this ToolBar is initially set to 'false'
             The ToolBar should be invisible" Top="320px" ID="lblExp1"></vt:Label>

        <vt:ToolBar runat="server" Text="TestedToolBar" Dock="None" Visible="false" Top="385px" ID="TestedToolBar2">
            <vt:ToolBarButton runat="server" id="ToolBar2Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar2Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar2Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>
        
        <vt:Label runat="server" SkinID="Log" Top="435px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="500px" ID="Button1" Width="200px" ClickAction ="ToolBarVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        