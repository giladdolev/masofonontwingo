using System;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TablePanelColumnCountController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
         
        //public void btnRowCol_Click(object sender, EventArgs e)
        private void btnAddRow_Click(object sender, EventArgs e)
        {
            ButtonElement button = new ButtonElement();
            TablePanel tablePanel = this.GetVisualElementById<TablePanel>("tablePanel");
            tablePanel.RowCount++;
        }
        

        public void btnAddCol_Click(object sender, EventArgs e)
        {
            ButtonElement button = new ButtonElement();
            TablePanel tablePanel = this.GetVisualElementById<TablePanel>("tablePanel");
            tablePanel.ColumnCount++;
            tablePanel.Controls.Add(button);
        }

    }
}