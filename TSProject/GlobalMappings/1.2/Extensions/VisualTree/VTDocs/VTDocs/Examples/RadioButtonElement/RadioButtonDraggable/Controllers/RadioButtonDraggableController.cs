using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement rdoRuntimeDraggable = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeDraggable");

            if (rdoRuntimeDraggable.Draggable == false)
            {
                rdoRuntimeDraggable.Draggable = true;
                textBox1.Text = rdoRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                rdoRuntimeDraggable.Draggable = false;
                textBox1.Text = rdoRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}