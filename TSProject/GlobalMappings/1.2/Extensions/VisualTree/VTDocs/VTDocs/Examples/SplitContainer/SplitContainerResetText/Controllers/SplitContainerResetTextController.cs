using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerResetTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedSplitContainer.Panel1.Text != "")
            {
                lblLog.Text = "SplitterPanel1 Text is set to " + TestedSplitContainer.Panel1.Text + "\\r\\n";
            }
            else
            {
                lblLog.Text = "SplitterPanel1 Text is empty\\r\\n";
            }
            if (TestedSplitContainer.Panel2.Text != "")
            {
                lblLog.Text += "SplitterPanel2 Text is set to " + TestedSplitContainer.Panel2.Text;
            }
            else
            {
                lblLog.Text += "SplitterPanel2 Text is empty";
            }
        }

        //RichSplitContainerAlignment
        public void btnResetText_Click(object sender, EventArgs e)
        {

            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedSplitContainer.Panel1.ResetText();
            TestedSplitContainer.Panel2.ResetText();
            if (TestedSplitContainer.Panel1.Text != "")
            {
                lblLog.Text = "SplitterPanel1 Text is set to " + TestedSplitContainer.Panel1.Text + "\\r\\n";
            }
            else
            {
                lblLog.Text = "SplitterPanel1 Text is empty\\r\\n";
            }
            if (TestedSplitContainer.Panel2.Text != "")
            {
                lblLog.Text += "SplitterPanel2 Text is set to " + TestedSplitContainer.Panel2.Text;
            }
            else
            {
                lblLog.Text += "SplitterPanel2 Text is empty";
            }
        }

        public void btnSetText_Click(object sender, EventArgs e)
        {

            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedSplitContainer.Panel1.Text = "TestedSplitPanel1";
            TestedSplitContainer.Panel2.Text = "TestedSplitPanel2";

            if (TestedSplitContainer.Panel1.Text != "")
            {
                lblLog.Text = "SplitterPanel1 Text is set to " + TestedSplitContainer.Panel1.Text + "\\r\\n";
            }
            else
            {
                lblLog.Text = "SplitterPanel1 Text is empty\\r\\n";
            }
            if (TestedSplitContainer.Panel2.Text != "")
            {
                lblLog.Text += "SplitterPanel2 Text is set to " + TestedSplitContainer.Panel2.Text;
            }
            else
            {
                lblLog.Text += "SplitterPanel2 Text is empty";
            }
        }

    }
}