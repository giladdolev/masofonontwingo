using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedLabel1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedLabel2.ForeColor + '.';
        }
         public void btnChangeLabelForeColor_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel2.ForeColor == Color.Green)
            {
                TestedLabel2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedLabel2.ForeColor + '.';
            }
            else
            {
                TestedLabel2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedLabel2.ForeColor + '.';
            }
        }
       
    }
}