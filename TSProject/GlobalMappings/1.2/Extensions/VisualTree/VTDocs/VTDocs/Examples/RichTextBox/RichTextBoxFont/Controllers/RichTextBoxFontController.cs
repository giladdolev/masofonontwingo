using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox1 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox1");
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedRichTextBox1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRichTextBox1);
            lblLog2.Text = "Font value: " + TestedRichTextBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRichTextBox2);
        }

        private void btnChangeRichTextBoxFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            if (TestedRichTextBox2.Font.Underline == true)
            {
                TestedRichTextBox2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedRichTextBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRichTextBox2);
            }
            else if (TestedRichTextBox2.Font.Bold == true)
            {
                TestedRichTextBox2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedRichTextBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRichTextBox2);
            }
            else if (TestedRichTextBox2.Font.Italic == true)
            {
                TestedRichTextBox2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedRichTextBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRichTextBox2);
            }
            else
            {
                TestedRichTextBox2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedRichTextBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRichTextBox2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}