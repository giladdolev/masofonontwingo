using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "TextBox MinimumSize value: " + TestedTextBox.MinimumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
        }

        public void btnChangeTxtMinimumSize_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedTextBox.MinimumSize == new Size(260, 30))
            {
                TestedTextBox.MinimumSize = new Size(200, 80);
                lblLog1.Text = "TextBox MinimumSize value: " + TestedTextBox.MinimumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
            }
            else if (TestedTextBox.MinimumSize == new Size(200, 80))
            {
                TestedTextBox.MinimumSize = new Size(240, 50);
                lblLog1.Text = "TextBox MinimumSize value: " + TestedTextBox.MinimumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
            }
            else
            {
                TestedTextBox.MinimumSize = new Size(260, 30);
                lblLog1.Text = "TextBox MinimumSize value: " + TestedTextBox.MinimumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
            }

        }

        public void btnSetToZeroTxtMinimumSize_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedTextBox.MinimumSize = new Size(0, 0);
            lblLog1.Text = "TextBox MinimumSize value: " + TestedTextBox.MinimumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;

        }


    }
}