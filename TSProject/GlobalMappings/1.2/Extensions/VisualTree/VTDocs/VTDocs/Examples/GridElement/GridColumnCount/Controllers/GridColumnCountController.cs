using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridColumnCountController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "ColumnCount value: " + TestedGrid1.ColumnCount.ToString();

            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "ColumnCount value: " + TestedGrid2.ColumnCount.ToString() + "\\r\\nGridColumnCollection Count: " + TestedGrid2.Columns.Count;

        }

        public void btnChangeColumnCount_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TextBoxElement txtColumnCount = this.GetVisualElementById<TextBoxElement>("txtColumnCount");
            int result;
            if (Int32.TryParse(txtColumnCount.Text, out result))
            {
                if (result >= 0)
                {
                    TestedGrid2.ColumnCount = result;
                    lblLog2.Text = "Input text: " + result + "\\r\\nColumnCount value: " + TestedGrid2.ColumnCount.ToString() + "\\r\\nGridColumnCollection Count: " + TestedGrid2.Columns.Count;
                }
                else
                    lblLog2.Text = "Input: Enter a number to be set to ColumnCount";
            }
            else
                lblLog2.Text = "Input: Enter a number to be set to ColumnCount";
        }


    }
}
