using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridCellStyleFontController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

		private void Grid_Load(object sender, EventArgs e)
		{
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            GridColumn col1 = new GridTextBoxColumn();
            GridColumn col2 = new GridTextBoxColumn();
            GridColumn col3 = new GridTextBoxColumn();

            //add columns
            col1.HeaderText = "Name";
            col1.Visible = true;
            col2.HeaderText = "Last Name";
            col2.Visible = true;
            col3.HeaderText = "ID";
            col3.Visible = true;
            gridElement.Columns.Add(col1.ID, col1.HeaderText);
            gridElement.Columns.Add(col2.ID, col2.HeaderText);
            gridElement.Columns.Add(col3.ID, col3.HeaderText);

            //Add rows
            gridElement.Rows.Add(new object[] {
				"David",
				"Raz",
				"1",
			});
            gridElement.Rows.Add(new object[] {
				"Natan",
				"Biton",
				"2",
			});
            gridElement.Rows.Add(new object[] {
				"Tamar",
				"Levi",
				"3",
			});

            gridElement.RefreshData();
            gridElement.Refresh();
		}


        public void btnFont_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            if (gridElement.DefaultCellStyle.Font.Italic == true) //get
            {
                gridElement.DefaultCellStyle.Font = new Font("Miriam", 9, FontStyle.Bold); //set
            }
            else if (gridElement.DefaultCellStyle.Font.Bold == true)
            {
                gridElement.DefaultCellStyle.Font = new Font("SketchFlow Print", 9, FontStyle.Strikeout);
            }
            else if (gridElement.DefaultCellStyle.Font.Strikeout == true)
            {
                gridElement.DefaultCellStyle.Font = new Font("SketchFlow Print", 11, FontStyle.Underline);
            }
            else if (gridElement.DefaultCellStyle.Font.Underline == true)
            {
                gridElement.DefaultCellStyle.Font = new Font("Miriam", 10, FontStyle.Italic);
            }
            else
                gridElement.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Italic);
        }


	}
}
