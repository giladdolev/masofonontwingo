<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="Grid UseFixedHeaders Property"  ID="windowView1" Height="800px" LoadAction="GridUseFixedHeaders\OnLoad">


        <vt:Label runat="server" SkinID="Title" Text="UseFixedHeaders" Left="220px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Specifies whether the fixed-headers feature is enabled. 
            Fixed-headers feature lets you fix (freeze) columns so they do not scroll when the grid 
            is scrolled horizontally.

            Syntax: public bool UseFixedHeaders { get; set; }
            'true' if the fixed-headers feature is enabled; otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Grid1 -->
        <vt:Grid runat="server" Top="200px" ID="TestedGrid1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="33px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="33px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="33px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>


        <vt:Label runat="server" SkinID="Log" Top="330px" ID="lblLog1"></vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="385px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="UseFixedHeaders property of this Grid is initially set to 'true'
             The fixed-headers feature is enabled." Top="430px" ID="lblExp1"></vt:Label>

        <!-- Tested Grid2 -->
        <vt:Grid runat="server" UseFixedHeaders="true" Top="485px" Height="140px" Width="430px" ID="TestedGrid2">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="33px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="33px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="33px" Width="85px"></vt:GridColumn>
               <vt:GridColumn  runat="server" ID="Grid2Col4" HeaderText="Column4" Height="33px" Width="85px"></vt:GridColumn>
               <vt:GridColumn  runat="server" ID="Grid2Col5" HeaderText="Column5" Height="33px" Width="85px"></vt:GridColumn>
               <vt:GridColumn  runat="server" ID="Grid2Col6" HeaderText="Column6" Height="33px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="640px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change UseFixedHeaders value >>" Top="710px" Width="220px" ID="btnChangeUseFixedHeaders" ClickAction="GridUseFixedHeaders\btnChangeUseFixedHeaders_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
        