
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Tag Property" ID="windowView1" LoadAction="SplitContainerTag\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Tag" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the object that contains data about the control.
            
            Syntax: public object Tag { get; set; }
            Value: An Object that contains data about the control. The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="175px" ID="TestedSplitContainer1"></vt:SplitContainer>
        <vt:Label runat="server" SkinID="Log" Top="270px" Width="410" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Tag property of this SplitContainer is initially set to the string 'New Tag'." Top="375px"  ID="lblExp1"></vt:Label>     
        
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="420px" Tag="New Tag." ID="TestedSplitContainer2"></vt:SplitContainer>
        <vt:Label runat="server" SkinID="Log" Top="515px" Width="410" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Tag value >>" Top="585px" ID="btnChangeBackColor" ClickAction="SplitContainerTag\btnChangeTag_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

