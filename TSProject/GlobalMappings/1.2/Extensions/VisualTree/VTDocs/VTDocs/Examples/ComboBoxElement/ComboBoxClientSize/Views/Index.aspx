<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox ClientSize Property" ID="windowView" LoadAction="ComboBoxClientSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the client area of the control.

            Syntax: public Size ClientSize { get; set; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="lblDefinition" ></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="235px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The ClientSize property of this 'TestedComboBox' is initially set with 
            Width: 165px, Height: 25px" Top="275px"  ID="lblExp1"></vt:Label>     

        <!-- TestedComboBox -->
        <vt:ComboBox runat="server" Text="TestedComboBox" Top="345px" ID="TestedComboBox"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="395px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientSize value >>" Top="460px" Width="180px" ID="btnChangeComboBoxClientSize" ClickAction="ComboBoxClientSize\btnChangeComboBoxClientSize_Click"></vt:Button>
              
    </vt:WindowView>
</asp:Content>
