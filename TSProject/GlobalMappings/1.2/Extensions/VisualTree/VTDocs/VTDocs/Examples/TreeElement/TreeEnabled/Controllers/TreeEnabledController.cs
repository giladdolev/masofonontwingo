using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedTree1.Enabled.ToString();

            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedTree2.Enabled.ToString() + ".";
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedTree2.Enabled = !TestedTree2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedTree2.Enabled.ToString() + ".";
        }
    }
}