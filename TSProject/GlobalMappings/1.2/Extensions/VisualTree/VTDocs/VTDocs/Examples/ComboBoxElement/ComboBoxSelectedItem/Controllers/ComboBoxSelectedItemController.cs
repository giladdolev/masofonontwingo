using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxSelectedItemController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox1.Items.Add("Item1");
            TestedComboBox1.Items.Add("Item2");
            TestedComboBox1.Items.Add("Item3");

            if (TestedComboBox1.SelectedItem == null)
            {
                lblLog1.Text = "SelectedItem Value: null";
            }
            else
            {
                lblLog1.Text = "SelectedItem Value: " + Convert.ToString(TestedComboBox1.SelectedItem);
            }

            TestedComboBox2.Items.Add("Item1");
            TestedComboBox2.Items.Add("Item2");
            TestedComboBox2.Items.Add("Item3");
            TestedComboBox2.SelectedItem = "Item2";

            lblLog2.Text = "SelectedItem Value: " + Convert.ToString(TestedComboBox2.SelectedItem) + ".";
        }

        public void TestedComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "The SelectedItem is: " + Convert.ToString(TestedComboBox1.SelectedItem);
        }

        public void TestedComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "The SelectedItem is: " + Convert.ToString(TestedComboBox2.SelectedItem) + ".";
        }

        private void ChooseNext_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            
            if (TestedComboBox2.SelectedItem.ToString() == "Item3")
            {
                TestedComboBox2.SelectedItem = "Item1";
            }
            else if (TestedComboBox2.SelectedItem.ToString() == "Item1")
            {
                TestedComboBox2.SelectedItem = "Item2";
            }
            else
            {
                TestedComboBox2.SelectedItem = "Item3";
            }
        }
    }
}
