﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class ButtonEquals : WindowElement
    {

        ButtonElement _TestedButton;

        public ButtonEquals()
        {
            _TestedButton = new ButtonElement();
        }

        public ButtonElement TestedButton
        {
            get { return this._TestedButton; }
            set { this._TestedButton = value; }
        }
    }
}