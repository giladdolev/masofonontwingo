using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowEnableEsc3Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index3()
        {
            return View(new WindowEnableEsc3());
        }

    }
}