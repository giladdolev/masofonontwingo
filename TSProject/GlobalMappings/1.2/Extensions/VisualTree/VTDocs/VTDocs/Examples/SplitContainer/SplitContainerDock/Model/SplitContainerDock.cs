﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class SplitContainerDock : WindowElement
    {

        Form2DockFillAndResizing _TestedWin2;
        Form3DockLeftAndRight _TestedWin3;
        Form4DockTopAndBottom _TestedWin4;

        public SplitContainerDock()
        {

        }

        public Form2DockFillAndResizing TestedWin2
        {
            get { return this._TestedWin2; }
            set { this._TestedWin2 = value; }
        }


        public Form3DockLeftAndRight TestedWin3
        {
            get { return this._TestedWin3; }
            set { this._TestedWin3 = value; }
        }


        public Form4DockTopAndBottom TestedWin4
        {
            get { return this._TestedWin4; }
            set { this._TestedWin4 = value; }
        }

    }
}