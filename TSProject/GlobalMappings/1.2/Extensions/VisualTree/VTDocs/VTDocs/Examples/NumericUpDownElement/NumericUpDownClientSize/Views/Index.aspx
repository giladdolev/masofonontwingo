<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown ClientSize Property" ID="windowView" LoadAction="NumericUpDownClientSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the client area of the control.

            Syntax: public Size ClientSize { get; set; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="lblDefinition" ></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="235px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The ClientSize property of this 'TestedNumericUpDown' is initially set with 
            Width: 170px, Height: 24px" Top="275px"  ID="lblExp1"></vt:Label>     

        <!-- TestedNumericUpDown -->
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="345px" ID="TestedNumericUpDown"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="400px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientSize value >>" Top="460px" Width="175px" ID="btnChangeNumericUpDownClientSize" ClickAction="NumericUpDownClientSize\btnChangeNumericUpDownClientSize_Click"></vt:Button>
              
    </vt:WindowView>
</asp:Content>
