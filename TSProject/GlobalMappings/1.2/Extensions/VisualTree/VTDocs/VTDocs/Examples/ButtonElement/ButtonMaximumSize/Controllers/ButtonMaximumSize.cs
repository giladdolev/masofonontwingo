using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //private void btnChangeMaximumSize_Click(object sender, EventArgs e)
        //{
        //    ButtonElement btnChangeMaximumSize = this.GetVisualElementById<ButtonElement>("btnChangeMaximumSize");

        //    if (btnChangeMaximumSize.MaximumSize == new Size(0, 0)) 
        //    {
        //        btnChangeMaximumSize.MaximumSize = new Size(100, 30);
        //        btnChangeMaximumSize.Text = "MaximumSize is set to: " + btnChangeMaximumSize.MaximumSize;
        //    }
        //    else 
        //    {
        //        btnChangeMaximumSize.MaximumSize = new Size(0, 0);
        //        btnChangeMaximumSize.Text = "MaximumSize is back to : " + btnChangeMaximumSize.MaximumSize;

        //    }
        //}

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button MaximumSize value: " + btnTestedButton.MaximumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;

        }
        public void btnChangeButtonMaximumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            if (btnTestedButton.MaximumSize == new Size(50, 100))
            {
                btnTestedButton.MaximumSize = new Size(250, 50);
                lblLog.Text = "Button MaximumSize value: " + btnTestedButton.MaximumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;

            }
            else if (btnTestedButton.MaximumSize == new Size(250, 50))
            {
                btnTestedButton.MaximumSize = new Size(100, 70);
                lblLog.Text = "Button MaximumSize value: " + btnTestedButton.MaximumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;
            }

            else
            {
                btnTestedButton.MaximumSize = new Size(50, 100);
                lblLog.Text = "Button MaximumSize value: " + btnTestedButton.MaximumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;
            }

        }

        public void btnSetToZeroBtnMaximumSize_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            btnTestedButton.MaximumSize = new Size(0, 0);
            lblLog.Text = "Button MaximumSize value: " + btnTestedButton.MaximumSize + "\\r\\nButton Size value: " + btnTestedButton.Size;

        }
      
    }
}