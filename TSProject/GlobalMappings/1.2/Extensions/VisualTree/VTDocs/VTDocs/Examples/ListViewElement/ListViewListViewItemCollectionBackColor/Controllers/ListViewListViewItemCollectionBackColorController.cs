using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewListViewItemCollectionBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Item2 BackColor value: " + TestedListView1.Items[1].BackColor.ToString();

            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Item2 BackColor value: " + TestedListView2.Items[1].BackColor.ToString();

        }

        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedListView2.Items[1].BackColor == Color.Orange)
            {
                TestedListView2.Items[1].BackColor = Color.Green;
                lblLog2.Text = "Item2 BackColor value: " + TestedListView2.Items[1].BackColor.ToString();
            }
            else
            {
                TestedListView2.Items[1].BackColor = Color.Orange;
                lblLog2.Text = "Item2 BackColor value: " + TestedListView2.Items[1].BackColor.ToString();

            }
        }

       
    }
} 