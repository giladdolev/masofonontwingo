using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeSize = this.GetVisualElementById<ButtonElement>("btnChangeSize");


            if (btnChangeSize.Size == new Size(250, 36))
            {
                btnChangeSize.Size = new Size(300, 20);
                btnChangeSize.Text = "Set to Height: " + btnChangeSize.Height + ", Width: " + btnChangeSize.Width;
            }
            else
            {
                btnChangeSize.Size = new Size(250, 36);
                btnChangeSize.Text = "Back to Height: " + btnChangeSize.Height + ", Width: " + btnChangeSize.Width;
            }
        }

        private void btnChangeSize_SizeChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "SizeChanged event is invoked\\r\\n";
        }
      
    }
}