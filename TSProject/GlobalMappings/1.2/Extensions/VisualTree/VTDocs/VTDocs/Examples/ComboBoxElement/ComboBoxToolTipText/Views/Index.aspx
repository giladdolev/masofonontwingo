<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox ToolTipText property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxToolTipText/Form_Load">
        <vt:Label runat="server" Text="Initialized Component : Hover with the mouse on the comboBox" Top="300px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="comboBox1" DisplayMember="text" ValueMember="value" Left="50" Top="360" ToolTipText="ComboBox for initialized">
            <Items>
                <vt:ListItem runat="server" Text="aa"></vt:ListItem>
                <vt:ListItem runat="server" Text="bb"></vt:ListItem>
                <vt:ListItem runat="server" Text="cc"></vt:ListItem>
                <vt:ListItem runat="server" Text="55"></vt:ListItem>
            </Items>
        </vt:ComboBox>
        <vt:Label runat="server" Text="Run Time : Hover with the mouse on the comboBox" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="160" Left="50"/>
    </vt:WindowView>
</asp:Content>
