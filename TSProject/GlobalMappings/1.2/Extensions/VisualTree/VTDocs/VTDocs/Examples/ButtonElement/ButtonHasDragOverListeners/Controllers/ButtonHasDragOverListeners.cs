using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonHasDragOverListenersController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnHasDragOverListeners_Click(object sender, EventArgs e)
        {
            ButtonElement btnHasDragOverListeners = this.GetVisualElementById<ButtonElement>("btnHasDragOverListeners");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            textBox1.Text = "HasDragOverListeners value:\\r\\n" + btnHasDragOverListeners.HasDragOverListeners;
        }

    }
}