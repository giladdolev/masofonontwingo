using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarDockController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnChangeDock_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
			ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            if (toolStrip1.Dock == Dock.Bottom)
            {
                toolStrip1.Dock = Dock.Fill;
                textBox1.Text = toolStrip1.Dock.ToString();
            }
            else if (toolStrip1.Dock == Dock.Fill)
            {
                toolStrip1.Dock = Dock.Left;
                textBox1.Text = toolStrip1.Dock.ToString();
            }
            else if (toolStrip1.Dock == Dock.Left)
            {
                toolStrip1.Dock = Dock.Right;
                textBox1.Text = toolStrip1.Dock.ToString();
            }
            else if (toolStrip1.Dock == Dock.Right)
            {
                toolStrip1.Dock = Dock.Top;
                textBox1.Text = toolStrip1.Dock.ToString();
            }
            else if (toolStrip1.Dock == Dock.Top)
            {
                toolStrip1.Dock = Dock.None;
                textBox1.Text = toolStrip1.Dock.ToString();
            }
            else if (toolStrip1.Dock == Dock.None)
            {
                toolStrip1.Dock = Dock.Bottom;
                textBox1.Text = toolStrip1.Dock.ToString();
            }  
                
		}

    }
}