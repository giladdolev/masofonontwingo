using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedProgressBar.Size;
        }

        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.Size == new Size(300, 80))   //Get
            {
                TestedProgressBar.Size = new Size(250, 40);    //Set
                lblLog.Text = "Size value: " + TestedProgressBar.Size;
            }
            else
            {
                TestedProgressBar.Size = new Size(300, 80);
                lblLog.Text = "Size value: " + TestedProgressBar.Size;
            }
        }

    }
}