using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TimerEnabled());
        }

        TimerEnabled TimerEnabledModel;
        private TimerEnabled ViewModel
        {
            get { return this.GetRootVisualElement() as TimerEnabled; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TimerElement TestedTimer1 = this.GetVisualElementById<TimerElement>("TestedTimer1");
            TimerElement TestedTimer2 = this.GetVisualElementById<TimerElement>("TestedTimer2");

            lblLog1.Text = "Enabled value: " + TestedTimer1.Enabled;
            lblLog2.Text = "Enabled value: " + TestedTimer2.Enabled + '.';
        }

        public void btnChangeTimerEnable_Click(object sender, EventArgs e)
        {
            TimerElement TestedTimer2 = this.GetVisualElementById<TimerElement>("TestedTimer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedTimer2.Enabled = !TestedTimer2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedTimer2.Enabled + '.';
        }

        public void TestedTimer1_Tick(object sender, EventArgs e)
        {
            TextBoxElement timer1TextBox = this.GetVisualElementById<TextBoxElement>("timer1TextBox");
            // TODO: Type 'System.Int32' does not have a 'ToString' member. (CODE=30002)
            timer1TextBox.Text = ViewModel.duration1Property++.ToString();
        }

        public void TestedTimer2_Tick(object sender, EventArgs e)
        {
            TextBoxElement timer2TextBox = this.GetVisualElementById<TextBoxElement>("timer2TextBox");
            // TODO: Type 'System.Int32' does not have a 'ToString' member. (CODE=30002)
            timer2TextBox.Text = ViewModel.duration2Property++.ToString();
        }
    }
}