<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree ForeColor property" ID="windowView1" LoadAction="TreeForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Tree1 -->
        <vt:Tree runat="server" Text="Tree" Top="165px" ID="TestedTree1">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="285px" BorderStyle="None" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of this Tree is initially set to Green" Top="385px" ID="lblExp1"></vt:Label>

        <!-- Tested Tree2 -->
        <vt:Tree runat="server" Text="TestedTree" Top="420px" ForeColor="Green" ID="TestedTree2">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree2Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="540px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="605px" Width="180px" ID="btnChangeTreeForeColor" ClickAction="TreeForeColor\btnChangeTreeForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
