using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "GroupBox MaximumSize value: " + TestedGroupBox.MaximumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;

        }
        public void btnChangeGrpMaximumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            if (TestedGroupBox.MaximumSize == new Size(150, 100))
            {
                TestedGroupBox.MaximumSize = new Size(200, 50);
                lblLog.Text = "GroupBox MaximumSize value: " + TestedGroupBox.MaximumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;

            }
            else if (TestedGroupBox.MaximumSize == new Size(200, 50))
            {
                TestedGroupBox.MaximumSize = new Size(170,40);
                lblLog.Text = "GroupBox MaximumSize value: " + TestedGroupBox.MaximumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;
            }

            else
            {
                TestedGroupBox.MaximumSize = new Size(150, 100);
                lblLog.Text = "GroupBox MaximumSize value: " + TestedGroupBox.MaximumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;
            }

        }

        public void btnSetToZeroGrpMaximumSize_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGroupBox.MaximumSize = new Size(0, 0);
            lblLog.Text = "GroupBox MaximumSize value: " + TestedGroupBox.MaximumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;

        }

    }
}