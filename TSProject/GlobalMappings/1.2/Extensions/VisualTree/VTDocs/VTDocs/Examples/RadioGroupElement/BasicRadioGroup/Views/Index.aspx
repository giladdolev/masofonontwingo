<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic RadioGroup" Top="57px" Left="11px" ID="windowView1" LoadAction="BasicRadioGroup\OnLoad">

         <vt:Label runat="server" SkinID="Title" Text="Basic RadioGroup" Left="280px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Combines multiple options (radio buttons) into a group that supports selecting one of several options.
            Selecting an item deselects the one previously selected, so only one item can be selected at a time." Top="75px" ID="Label1" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="170px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The following example shows 2 RadioGroups. 
            In each group only one item can be selected at a time." Top="210px"  ID="lblExp1"></vt:Label>   

		<vt:RadioGroup runat="server" Top="300px" Text= "RadioGroup1" ID="RadioGroup1" ChangeAction="BasicRadioGroup\Group1_Change">
			<vt:RadioGroupItem runat="server" Text="Option1" Top="20px" ID="RadioGroupItem1" TabIndex="5"></vt:RadioGroupItem>
            <vt:RadioGroupItem runat="server" Text="Option2" Top="60px" ID="RadioGroupItem2" TabIndex="6" ></vt:RadioGroupItem>
            <vt:RadioGroupItem runat="server" Text="Option3" Top="100px" ID="RadioGroupItem3" TabIndex="7" ></vt:RadioGroupItem>
            <vt:RadioGroupItem runat="server" Text="Option4" Top="140px" ID="RadioGroupItem4" TabIndex="8" ></vt:RadioGroupItem>
		</vt:RadioGroup>
        <vt:Label runat="server" SkinID="Log" Top="490px" Width="200px" ID="lblLog1"></vt:Label>

        <vt:Button runat="server" Text="Select Option2 >>" Top="530px" ID="btnSelectOption2" ClickAction="BasicRadioGroup\btnSelectOption2_Click"></vt:Button>
        
        
        <vt:RadioGroup runat="server" Top="300px" Text="RadioGroup2" Left="350px" ID="RadioGroup2" ChangeAction="BasicRadioGroup\Group2_Change">
			<vt:RadioGroupItem runat="server" Text="Item1" Top="20px"  ID="RadioGroupItem5" TabIndex="5"></vt:RadioGroupItem>
            <vt:RadioGroupItem runat="server" Text="Item2" Top="60px"  ID="RadioGroupItem6" TabIndex="6" ></vt:RadioGroupItem>
            <vt:RadioGroupItem runat="server" Text="Item3" Top="100px"  ID="RadioGroupItem7" TabIndex="7" ></vt:RadioGroupItem>
            <vt:RadioGroupItem runat="server" Text="Item4" Top="140px"  ID="RadioGroupItem8" TabIndex="8" ></vt:RadioGroupItem>
		</vt:RadioGroup>
        <vt:Label runat="server" SkinID="Log" Top="490px" Width="200px" Left="350px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Select Item4 >>" Top="530px" Left="350px" ID="btnSelectItem4" ClickAction="BasicRadioGroup\btnSelectItem4_Click"></vt:Button>
        
       
		

    </vt:WindowView>
</asp:Content>
