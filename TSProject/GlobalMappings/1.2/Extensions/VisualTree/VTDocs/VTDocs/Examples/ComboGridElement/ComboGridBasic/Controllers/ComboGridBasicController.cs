using System;
using System.Data;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(int));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(string));
            source.Rows.Add(12345, "James", "Dean");
            source.Rows.Add(23451, "Bob", "Marley");
            source.Rows.Add(34512, "Dana", "International");
            source.Rows.Add(45123, "Brad", "Pitt");

            TestedComboGrid.ValueMember = "Column2";
            TestedComboGrid.DisplayMember = "Column1";

            TestedComboGrid.DataSource = source;

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ComboGrid SelectedIndex value: " + TestedComboGrid.SelectedIndex +
                "\\r\\nComboGrid SelectedValue value: " + TestedComboGrid.SelectedValue +
                "\\r\\nComboGrid SelectedItem value: " + TestedComboGrid.SelectedItem +
                "\\r\\nComboGrid SelectedText value: " + TestedComboGrid.SelectedText;

        }

        public void TestedComboGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ComboGrid SelectedIndex value: " + TestedComboGrid.SelectedIndex +
                "\\r\\nComboGrid SelectedValue value: " + TestedComboGrid.SelectedValue.ToString() +
                "\\r\\nComboGrid SelectedItem value: " + TestedComboGrid.SelectedItem.ToString() +
                "\\r\\nComboGrid SelectedText value: " + TestedComboGrid.SelectedText.ToString();
        }

        private void btnSelect_click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            if (TestedComboGrid.SelectedIndex == 3)
            {
                TestedComboGrid.SelectedIndex = 0;
            }
            else
            {
                TestedComboGrid.SelectedIndex++;
            }
        }

        public void btnClearSelection_click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            TestedComboGrid.Text = "";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ComboGrid SelectedIndex value: " + TestedComboGrid.SelectedIndex +
                "\\r\\nComboGrid SelectedValue value: " + TestedComboGrid.SelectedValue.ToString() +
                "\\r\\nComboGrid SelectedItem value: " + TestedComboGrid.SelectedItem.ToString() +
                "\\r\\nComboGrid SelectedText value: " + TestedComboGrid.SelectedText.ToString();
        }

    }
}