<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel MinimumSize Property" ID="windowView" LoadAction="PanelMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedPanel' is width: 300px and height: 90px. 
            Its MinimumSize is initially set to width: 320px and height: 100px.
            To cancel 'TestedPanel' MinimumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label>     

        <vt:Panel runat="server" Left="80px" Top="360px" Height="90px" Width="300px" Text="TestedPanel" ID="TestedPanel" MinimumSize="320, 100"></vt:Panel>


        <vt:Label runat="server" SkinID="Log" Top="470px" ID="lblLog" Width="400px" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="555px" Width="200px" ID="btnChangePnlMinimumSize" ClickAction="PanelMinimumSize\btnChangePnlMinimumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="555px" width="200px" Left="310px" ID="btnSetToZeroPnlMinimumSize" ClickAction="PanelMinimumSize\btnSetToZeroPnlMinimumSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>









