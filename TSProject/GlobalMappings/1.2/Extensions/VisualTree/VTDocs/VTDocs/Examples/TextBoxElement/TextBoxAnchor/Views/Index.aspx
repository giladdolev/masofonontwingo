<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Anchor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Top="50px" Left="140px"  ID="panel1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="72px" Width="300px">
        <vt:TextBox runat="server" Text="TextBox Anchor is set to 'Bottom'" Anchor ="Bottom" Top="18px" Left="50px" ID="txtAnchor" Height="36px"  Width="200px"></vt:TextBox>           
            </vt:Panel>


        <vt:Label runat="server" Text="RunTime" Top="150px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Top="170px" Left="140px" ID="panel2" Height="72px" Width="300px" BorderStyle="Solid">
        <vt:TextBox runat="server"  Text="Change TextBox Anchor" Top="18px" Left="50px" ID="txtAnchorChange" Height="36px"  Width="200px"></vt:TextBox>
            </vt:Panel>
         <vt:button runat="server"  Text="Change TextBox Anchor" Top="188px" Left="550px" ID="btnChangeAnchor" Height="36px"  Width="150px" ClickAction="TextBoxAnchor\btnChangeAnchor_Click"></vt:button>
            

    </vt:WindowView>
</asp:Content>
