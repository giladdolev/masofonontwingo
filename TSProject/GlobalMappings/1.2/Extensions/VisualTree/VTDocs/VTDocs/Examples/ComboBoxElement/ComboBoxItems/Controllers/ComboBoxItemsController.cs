using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxItemsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ComboBoxElement cmb = this.GetVisualElementById<ComboBoxElement>("cmb1");
            cmb.Items.Add("aaa");
            cmb.Items.Add("bbb");
        }

        private void btnItems_Click(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");

            ListItem newitem1 = new ListItem("Item1");
            cmb1.Items.Add(newitem1);
        }
            
    }
}