﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;

namespace MvcApplication9.Controllers
{
    public class GridCellClickController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
/*
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
 */ 

            TestedGrid.Rows.Add("a", "false", "delete");
            TestedGrid.Rows.Add("b", "true", "delete");
            TestedGrid.Rows.Add("c", "false", "delete");

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            //lblLog.Text = "Grid Selected values: Empty";
            lblLog.Text = "Grid Current Cell Index : None";

        }

        public void TestedGrid_CellClick(object sender, GridElementCellEventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nGrid CellClick event is invoked";

            //lblLog.Text = "Grid Selected values: ";

            if ((e.RowIndex >= 0) && (e.ColumnIndex >= 0))
            {
                //lblLog.Text += " (" + TestedGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value + ") ";
                lblLog.Text = "Grid Current Cell Index : " + e.RowIndex + " , " + e.ColumnIndex;
            }

        }
    }
}
