
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Height Property" ID="windowView2" LoadAction="ButtonHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Height" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control.
            
            Syntax: public int Height { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Height property of this 'TestedButton' is initially set to 40." Top="300px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Height=40px Top="350px" ID="btnTestedButton"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Height >>" Top="475px" ID="btnChangeHeight" ClickAction="ButtonHeight\btnChangeHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
