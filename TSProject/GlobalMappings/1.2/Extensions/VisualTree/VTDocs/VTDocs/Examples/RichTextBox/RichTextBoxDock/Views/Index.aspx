<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Dock Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
       
         <vt:Label runat="server" Text="The RichTextBox is initialliy docked to the bottom" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 

        <vt:RichTextBox runat="server" Text="RichTextBox" Top="165px" Left="140px" ID="rtfDock" Dock="Bottom" Height="150px" TabIndex="1" Width="200px"></vt:RichTextBox>

        <vt:Button runat="server" Text="Change Dock style" Top="195px" Left="320px" ID="btnDock" Height="30px" TabIndex="1" Width="200px" 
            ClickAction="RichTextBoxDock\btnDock_Click"></vt:Button>
        
        <vt:TextBox runat="server" Text="" Top="325px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>


    </vt:WindowView>
</asp:Content>
        