<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem ImageIndex Property" ID="windowView2" LoadAction="TabTabItemImageIndex\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ImageIndex" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the index to the image displayed on this tab.
            
            Syntax: public int ImageIndex { get; set; }
            The zero-based index to the image in the TabControl.ImageList that appears on the tab. 
            The default is -1, which signifies no image.
            The ImageIndex points to an image in the associated ImageList of the TabControl." Top="75px" ID="lblDefinition"></vt:Label>
               
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="In the following example, an ImageList is defined for the Tab Control and the ImageIndex property 
            of the Tab Items is set. You can change the ImageIndex property by pressing on the buttons" Top="280px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tab runat="server" ImageListID="imglst" Top="345px" ID="TestedTab">
            <TabItems>
                <vt:TabItem runat="server" ImageIndex="4" Text="tabPage1" ID="tabPage1" Top="22px" Left="4px" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" ImageIndex="5" Text="tabPage2" ID="tabPage2" Top="22px" Left="4px" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Height ="35px" Top="475px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change tabPage1 Image >>" Width="180px" Top="575px" ID="btnChangetabPage1Imgn" ClickAction="TabTabItemImageIndex\btnChangetabPage1Imgn_Click"></vt:Button>

        <vt:Button runat="server" Text="Change tabPage2 Image >>" Left="290px" Width="180px" Top="575px" ID="btnChangetabPage2Img" ClickAction="TabTabItemImageIndex\btnChangetabPage2Img_Click"></vt:Button>

        <vt:ComponentManager runat="server" Visible="False">
            <vt:ImageList runat="server" TransparentColor="Transparent" ID="imglst">
                <Images>
                    <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream0.png"></vt:UrlReference>
                    <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream1.png"></vt:UrlReference>
                    <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream2.png"></vt:UrlReference>
                    <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream3.png"></vt:UrlReference>
                    <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream4.png"></vt:UrlReference>
                    <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream5.png"></vt:UrlReference>
                    <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream6.png"></vt:UrlReference>
                </Images>
                <Keys>
                    <vt:KeyItem runat="server" Key="Content/Images/Cute.png"></vt:KeyItem>
                    <vt:KeyItem runat="server" Key="Content/Images/galilcs.png" ImageIndex="1"></vt:KeyItem>
                    <vt:KeyItem runat="server" Key="Content/Images/gizmox.png" ImageIndex="2"></vt:KeyItem>
                    <vt:KeyItem runat="server" Key="Content/Images/icon.jpg" ImageIndex="3"></vt:KeyItem>
                    <vt:KeyItem runat="server" Key="Content/Images/New.png" ImageIndex="4"></vt:KeyItem>
                    <vt:KeyItem runat="server" Key="Content/Images/Open.png" ImageIndex="5"></vt:KeyItem>
                    <vt:KeyItem runat="server" Key="Content/Images/sleepy.jpg" ImageIndex="6"></vt:KeyItem>
                </Keys>
            </vt:ImageList>
        </vt:ComponentManager>

    </vt:WindowView>
</asp:Content>

