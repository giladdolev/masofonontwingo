<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button EnableToggle property" ID="windowView" LoadAction="ButtonEnableToggle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="EnableToggle" Top="15px" ID="lblTitle" Left="300px">
        </vt:Label>
        <vt:Label runat="server" Text="Allows this button to have the pressed state toggled via user interaction.
                       
            syntax : public bool EnableToggle { get; set; }

            If 'EnableToggle' is set to true the 'IsPressed' property becomes relevant. When the button is clicked, 
            the 'IsPressed' property will change it's state and the 'PressedChange' event will be invoked. 
            The default value is false."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Button default -->
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="230px" ID="TestedButton1" PressedChangeAction="ButtonEnableToggle\TestedButton1_PressedChange"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="280px" Width="350px" Height="40px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="345px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the initial 'EnableToggle' property value of 'TestedButton' is set to true.
            You can change the 'EnableToggle' property value by clicking on the 'Change EnableToggle value' button.
            When the 'EnableToggle' value is true and the 'TestedButton' is pressed, the 'IsPressed' value toggles." 
            Top="395px" ID="Label2">
        </vt:Label>

        <!-- Tested CheckButtons -->
        <vt:Button runat="server" EnableToggle="true" SkinID="Wide" Text="TestedButton" Top="490px" ID="TestedButton2" PressedChangeAction="ButtonEnableToggle\TestedButton2_PressedChange"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="540px" Width="330px" Height="40px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change EnableToggle value >>" Top="600px" Width="180px" ID="btnChangeEnableToggle" ClickAction="ButtonEnableToggle\btnChangeEnableToggle_Click"></vt:Button>

        
    </vt:WindowView>

</asp:Content>
