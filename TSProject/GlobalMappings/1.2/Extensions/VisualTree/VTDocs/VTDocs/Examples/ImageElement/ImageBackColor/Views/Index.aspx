<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image BackColor Property" ID="windowView1" LoadAction="ImageBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Image runat="server" Text="Image" ImageReference="" BorderStyle="Solid" Top="150px" ID="TestedImage1"></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="250px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="315px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this 'TestedImage' is initially set to Gray" Top="365px"  ID="lblExp1"></vt:Label>     
        
        <vt:Image runat="server" Text="TestedImage" BorderStyle="Solid" BackColor ="Gray" Top="405px" ID="TestedImage2" ImageReference=""></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Top="575px" Width="160" ID="btnChangeBackColor" ClickAction="ImageBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
