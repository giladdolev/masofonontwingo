using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridSelectionModeController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");

            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");

            lblLog1.Text = "SelectionMode value: " + TestedGrid1.SelectionMode.ToString();
            lblLog2.Text = "SelectionMode value: " + TestedGrid2.SelectionMode.ToString();
        }

        public void btnChangeSelectionMode_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2"); 
            
            if (TestedGrid2.SelectionMode == CellSelectionMode.Cell)
                TestedGrid2.SelectionMode = CellSelectionMode.Row;
            else
                TestedGrid2.SelectionMode = CellSelectionMode.Cell;

            lblLog2.Text = "SelectionMode value: " + TestedGrid2.SelectionMode.ToString();
        }


	}
}
