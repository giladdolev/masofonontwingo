<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox TextAlign" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="20px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:TextBox runat="server" Text="Some Text .. " Top="150px" Left="20px" ID="textBox19" Height="20px" Width="180px"></vt:TextBox>
        <vt:Button runat="server" Text="Get TextAlign" Top="150px" Left="300px" ID="button1" Height="36px" TabIndex="1" Width="200px" ClickAction="TextBoxTextAlign\btnGetTextAlign_Click"></vt:Button>
        <vt:Button runat="server" Text="Set TextAlign" Top="200px" Left="300px" ID="button2" Height="36px" TabIndex="1" Width="200px" ClickAction="TextBoxTextAlign\btnChangeAlignment_Click"></vt:Button>
        <vt:Label runat="server" Text="Initialized Component : TextBox TextAlign is initialy set to Center" Top="400px" Left="20px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:TextBox runat="server" Text="Some Text .. " Top="440px" Left="20px" ID="textBox2" Height="20px" Width="180px" TextAlign="Center"></vt:TextBox>
    </vt:WindowView>
</asp:Content>
