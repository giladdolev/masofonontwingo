using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("Item2");
            TestedComboBox.Items.Add("Item3");
            lblLog.Text = "Click on the button...";
        }

        public void Focus_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.Focus();
            lblLog.Text = "Focus method was invoked.";
           //Bug 23976 - after fix, uncomment the below line
           //lblLog.Text = "The return value is: " + TestedComboBox.Focus() + ".";
        }

        public void btnLoseFocus_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Control lost focus.";
        }

     


    }
}