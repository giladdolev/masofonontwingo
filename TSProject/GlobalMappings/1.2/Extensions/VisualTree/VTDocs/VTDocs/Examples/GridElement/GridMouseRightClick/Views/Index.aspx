<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid MouseRightClick Event" ID="windowView1" LoadAction="GridMouseRightClick\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MouseRightClick" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Fires when the container is right clicked.

            Syntax: public event EventHandler<EventArgs> MouseRightClick"
            Top="75px" ID="lblDefinition">
        </vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following you can invoke the 'MouseRightClick' example by right clicking the grid panel.
            Each invocation of the 'MouseRightClick' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

         <vt:Grid runat="server" Text="TestedGrid" CssClass="vt-tested-Grid" BackColor ="Gray" Height="140px" Width="300" Top="300px" ID="TestedGrid" MouseRightClickAction="GridMouseRightClick\TestedGrid_MouseRightClick" RowHeaderMouseRightClickAction="GridMouseRightClick\TestedGrid_RowHeaderMouseRightClick" ColumnHeaderMouseRightClickAction="GridMouseRightClick\TestedGrid_ColumnHeaderMouseRightClick" CellClickAction="GridMouseRightClick\TestedGrid_CellClick"  RowHeaderVisible="false">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log"  Height="40px" ID="lblLog" Top="455px"></vt:Label>

         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Width ="400px" Height="120px" Top="530px" ID="txtEventLog"></vt:TextBox>

         <vt:Button runat="server" Text="Reset Log" Width="150px" Left="530px" Top="560px" ID="btnResetLog" ClickAction="GridMouseRightClick\btnResetLog_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
