using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeCheckBoxesController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "CheckBoxes value: " + TestedTree1.CheckBoxes.ToString();

            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckBoxes value: " + TestedTree2.CheckBoxes.ToString();

            TreeElement TestedTree3 = this.GetVisualElementById<TreeElement>("TestedTree3");
            LabelElement lblLog3 = this.GetVisualElementById<LabelElement>("lblLog3");
            lblLog3.Text = "CheckBoxes value: " + TestedTree3.CheckBoxes.ToString();

        }


    }
}