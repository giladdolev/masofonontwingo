<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Top Property" ID="windowView1" LoadAction="LabelTop\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Top" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the top edge of the control
             and the top edge of its container's client area.
            
            Syntax: public int Top { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Top property of this Label is initially set to: 310px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Top="310px" ID="TestedLabel" TabIndex="2"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="340px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Top value >>" Top="405px" ID="btnChangeTop" TabIndex="1" Width="180px" ClickAction="LabelTop\btnChangeTop_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
