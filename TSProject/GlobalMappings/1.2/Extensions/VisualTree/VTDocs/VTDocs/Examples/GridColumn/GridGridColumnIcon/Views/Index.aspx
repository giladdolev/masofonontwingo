﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumn Icon Property" ID="windowView1" LoadAction="GridGridColumnIcon\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Icon" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the icon displayed in the top left column header.
            
            Syntax: public string Icon { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="In the following example the Icon property of the first two columns is set with iamge" Top="240px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server"  Top="285px" Width="500px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="Col1" Icon = "~/Content/Images/gizmox.png" HeaderText="Column1" Height="20px" Width="110px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Col2" Icon = "~/Content/Images/gizmox.png" HeaderText="Column2" Height="20px" Width="110px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Col3" HeaderText="Column3" Height="20px" Width="110px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Col4" HeaderText="Column4" Height="20px" Width="110px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Height ="35px" Top="415px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change column1 Icon >>" Width="180px" Top="515px" ID="btnChangeColumn1Icon" ClickAction="GridGridColumnIcon\btnChangeColumn1Icon_Click"></vt:Button>

        <vt:Button runat="server" Text="Change column2 Icon >>" Left="290px" Width="180px" Top="515px" ID="btnChangeColumn2Icon" ClickAction="GridGridColumnIcon\btnChangeColumn2Icon_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
