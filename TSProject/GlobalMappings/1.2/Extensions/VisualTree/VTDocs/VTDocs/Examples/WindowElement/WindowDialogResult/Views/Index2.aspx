<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Dialog Window" Top="57px" Left="11px" ID="windowView2" Height="600px" Width="600px">
		
		<vt:Button runat="server" TextAlign="MiddleCenter" Text="Ok" Top="300px" Left="50px" ClickAction="WindowDialogResult\btnOk_Click" ID="btnOk" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="200px">
		</vt:Button>
        
        
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Cancel" Top="300px" Left="350px" ClickAction="WindowDialogResult\btnCancel_Click" ID="btnCancel" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="200px">
		</vt:Button>
		

        </vt:WindowView>
</asp:Content>