﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectionChanged event" ID="windowView2" Height="800px" LoadAction="GridSelectionChanged6TextButtonColumnQA\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current selection changes.

            Syntax: public event GridElementCellEventHandler SelectionChanged"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells from the grid by clicking with the mouse or by pressing 
            the buttons. Each invocation of the 'BeforeSelectionChanged' and the 'SelectionChanged' events should  
            add a line to the 'Event Log'."
            Top="215px" ID="Label4">
        </vt:Label>

         <vt:Grid runat="server" Top="300px" Height="150px" ID="TestedGrid" BeforeSelectionChangedAction="GridSelectionChanged6TextButtonColumnQA\TestedGrid_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged6TextButtonColumnQA\TestedGrid_SelectionChanged">
            <Columns>
                <vt:GridTextButtonColumn runat="server" ID="GridTextButtonColumn1"  HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-tbnCol-1"></vt:GridTextButtonColumn>
                <vt:GridTextButtonColumn runat="server" ID="GridTextButtonColumn2"  HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-tbnCol-2"></vt:GridTextButtonColumn>
                <vt:GridTextButtonColumn runat="server" ID="GridTextButtonColumn3"  HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-tbnCol-3"></vt:GridTextButtonColumn>
            </Columns>
        </vt:Grid>

        <vt:Grid runat="server" Top="300px" Left="380px" Height="150px" RowHeaderVisible="false" ID="TestedGrid2" BeforeSelectionChangedAction="GridSelectionChanged6TextButtonColumnQA\TestedGrid2_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged6TextButtonColumnQA\TestedGrid2_SelectionChanged">
            <Columns>
                <vt:GridTextButtonColumn runat="server" ID="GridTextButtonColumn4"  HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-tbnCol-4"></vt:GridTextButtonColumn>
                <vt:GridTextButtonColumn runat="server" ID="GridTextButtonColumn5"  HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-tbnCol-5"></vt:GridTextButtonColumn>
                <vt:GridTextButtonColumn runat="server" ID="GridTextButtonColumn6"  HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-tbnCol-6"></vt:GridTextButtonColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="480px" height="40px" Width="550px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="540px" Width="550px" Height="140px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select d >>" Top="705px" ID="btnSelectd" ClickAction="GridSelectionChanged6TextButtonColumnQA\btnSelectd_Click"></vt:Button>

        <vt:Button runat="server" Text="Select r >>" Top="705px" Left="250px" ID="btnSelectr" ClickAction="GridSelectionChanged6TextButtonColumnQA\btnSelectr_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Row[0] >>" Top="745px" ID="btnSelectRow0" ClickAction="GridSelectionChanged6TextButtonColumnQA\btnSelectRow0_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Column[0] >>" Top="745px" Left="250px" ID="btnSelectColumn0" ClickAction="GridSelectionChanged6TextButtonColumnQA\btnSelectColumn0_Click"></vt:Button>

        <vt:Button runat="server" Text="Clear Event Log >>" Top="745px" Left="450px" ID="btnClear" ClickAction="GridSelectionChanged6TextButtonColumnQA\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

