using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerIntervalController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TimerInterval());
        }

        TimerInterval TimerIntervalModel;
        private TimerInterval ViewModel
        {
            get { return this.GetRootVisualElement() as TimerInterval; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TimerElement TestedTimer1 = this.GetVisualElementById<TimerElement>("TestedTimer1");
            TimerElement TestedTimer2 = this.GetVisualElementById<TimerElement>("TestedTimer2");

            lblLog1.Text = "Interval value: " + TestedTimer1.Interval;
            lblLog2.Text = "Interval value: " + TestedTimer2.Interval + '.';

        }

        public void TestedTimer1_Tick(object sender, EventArgs e)
        {
            TextBoxElement timer1TextBox = this.GetVisualElementById<TextBoxElement>("timer1TextBox");

            if (ViewModel.durationTimer1Property < 9999)
                timer1TextBox.Text = ViewModel.durationTimer1Property++.ToString();
            
            else
                ViewModel.durationTimer1Property = 0;

        }

        public void TestedTimer2_Tick(object sender, EventArgs e)
        {
            TextBoxElement timer2TextBox = this.GetVisualElementById<TextBoxElement>("timer2TextBox");
            if (ViewModel.durationTimer2Property < 9999)
                timer2TextBox.Text = ViewModel.durationTimer2Property++.ToString();

            else
                ViewModel.durationTimer2Property = 0;
        }


        public void btnChangeInterval_Click(object sender, EventArgs e)
        {
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TimerElement TestedTimer2 = this.GetVisualElementById<TimerElement>("TestedTimer2");

            if (TestedTimer2.Interval == 1000)
            {
                TestedTimer2.Interval = 100;
                lblLog2.Text = "Interval value: " + TestedTimer2.Interval + '.';
            }

            else
            {
                TestedTimer2.Interval = 1000;
                lblLog2.Text = "Interval value: " + TestedTimer2.Interval + '.';
            }

        }
    }
}