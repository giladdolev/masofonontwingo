using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");
            LayoutEventArgs args = new LayoutEventArgs(testedCheckBox, "Some String");

            testedCheckBox.PerformLayout(args);
        }

        public void testedCheckBox_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedCheckBox Layout event method is invoked");
        }

    }
}