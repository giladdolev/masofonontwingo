using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (TestedSplitContainer1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedSplitContainer1.Tag;
            }

            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            if (TestedSplitContainer2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedSplitContainer2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedSplitContainer2.Tag == "New Tag.")
            {
                TestedSplitContainer2.Tag = TestedSplitContainer2;
                lblLog2.Text = "Tag value: " + TestedSplitContainer2.Tag;
            }
            else
            {
                TestedSplitContainer2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedSplitContainer2.Tag;

            }
        }

    }
}