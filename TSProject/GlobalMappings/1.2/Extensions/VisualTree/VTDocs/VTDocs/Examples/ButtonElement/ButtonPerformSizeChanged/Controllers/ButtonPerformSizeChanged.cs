using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPerformSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformSizeChanged_Click(object sender, EventArgs e)
        {
            
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            
            MouseEventArgs args = new MouseEventArgs();

            btnTestedButton.PerformSizeChanged(args);
        }

        public void btnTestedButton_SizeChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TestedButton SizeChanged event method is invoked");
        }

    }
}