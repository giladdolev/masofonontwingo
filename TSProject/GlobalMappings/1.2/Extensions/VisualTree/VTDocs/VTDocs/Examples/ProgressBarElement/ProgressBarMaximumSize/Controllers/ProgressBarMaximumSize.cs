using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ProgressBar MaximumSize value: " + TestedProgressBar.MaximumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
        }

        public void btnChangePrgMaximumSize_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedProgressBar.MaximumSize == new Size(150, 90))
            {
                TestedProgressBar.MaximumSize = new Size(230, 50);
                lblLog1.Text = "ProgressBar MaximumSize value: " + TestedProgressBar.MaximumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
            }
            else if (TestedProgressBar.MaximumSize == new Size(230, 50))
            {
                TestedProgressBar.MaximumSize = new Size(180, 70);
                lblLog1.Text = "ProgressBar MaximumSize value: " + TestedProgressBar.MaximumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
            }
            else
            {
                TestedProgressBar.MaximumSize = new Size(150, 90);
                lblLog1.Text = "ProgressBar MaximumSize value: " + TestedProgressBar.MaximumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
            }

        }

        public void btnSetToZeroPrgMaximumSize_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedProgressBar.MaximumSize = new Size(0, 0);
            lblLog1.Text = "ProgressBar MaximumSize value: " + TestedProgressBar.MaximumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;

        }


    }
}