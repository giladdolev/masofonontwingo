using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedImage.ClientSize;

        }
        public void btnChangeImageClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            if (TestedImage.Width == 200)
            {
                TestedImage.ClientSize = new Size(150, 90);
                lblLog.Text = "ClientSize value: " + TestedImage.ClientSize;

            }
            else
            {
                TestedImage.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedImage.ClientSize;
            }

        }

    }
}