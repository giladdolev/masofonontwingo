using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxLocationChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            GroupBoxElement testedGroupBox = this.GetVisualElementById<GroupBoxElement>("testedGroupBox");

            if (testedGroupBox.Location == new Point(140, 90))
            {
                testedGroupBox.Location = new Point(200, 500);
            }
            else
            {
                testedGroupBox.Location = new Point(140, 90);
            }
        }

        public void testedGroupBox_LocationChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "GroupBox LocationChanged event is invoked\\r\\n";
        }

    }
}