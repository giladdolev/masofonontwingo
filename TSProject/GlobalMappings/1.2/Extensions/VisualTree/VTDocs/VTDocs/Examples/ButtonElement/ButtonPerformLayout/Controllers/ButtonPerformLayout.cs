using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LayoutEventArgs args = new LayoutEventArgs(btnTestedButton, "Some String");

            btnTestedButton.PerformLayout(args);
        }

        public void btnTestedButton_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedButton Layout event method is invoked");
        }

    }
}