using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "GroupBox bounds are: \\r\\n" + TestedGroupBox.Bounds;

        }
        public void btnChangeBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            if (TestedGroupBox.Bounds == new Rectangle(100, 340, 250, 50))
            {
                TestedGroupBox.Bounds = new Rectangle(80, 360, 200, 80);
                lblLog.Text = "GroupBox bounds are: \\r\\n" + TestedGroupBox.Bounds;
            }
            else
            {
                TestedGroupBox.Bounds = new Rectangle(100, 340, 250, 50);
                lblLog.Text = "GroupBox bounds are: \\r\\n" + TestedGroupBox.Bounds;
            }

        }
    }
}