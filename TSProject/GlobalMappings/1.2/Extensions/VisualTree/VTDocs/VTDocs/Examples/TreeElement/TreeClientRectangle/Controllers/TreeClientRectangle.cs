using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    
    public class TreeClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTree.ClientRectangle;

        }
        public void btnChangeTreeClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            if (TestedTree.Left == 100)
            {
                TestedTree.SetBounds(80, 320, 140, 110);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTree.ClientRectangle;

            }
            else
            {
                TestedTree.SetBounds(100, 310, 200, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTree.ClientRectangle;
            }

        }

    }
}