<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer PixelHeight Property" ID="windowView1" LoadAction="SplitContainerPixelHeight\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelHeight" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int PixelHeight { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this SplitContainer is initially set to: 120px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="310px" ID="TestedSplitContainer" Height="120px"></vt:SplitContainer>    

        <vt:Label runat="server" SkinID="Log" Top="445px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelHeight value >>" Top="510px" ID="btnChangePixelHeight" TabIndex="3" Width="180px" ClickAction="SplitContainerPixelHeight\btnChangePixelHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
