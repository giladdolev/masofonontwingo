<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
      
        <vt:CheckBox runat="server" Text="CheckBox1" BackColor="Yellow" Top="90px" Left="140px" ID="CheckBox1" Height="36px"  Width="100px"></vt:CheckBox>     
        
        <vt:CheckBox runat="server" Text="CheckBox2" BackColor="RosyBrown" Top="90px" Left="100px" ID="CheckBox2" Height="36px"  Width="100px"></vt:CheckBox>  
        
        <vt:Button runat="server" Text="Bring CheckBox1 To Fromt" Top="70px" Left="300px" ID="btnBringCheckBox1ToFromt" Height="36px"  Width="200px" ClickAction="CheckBoxBringToFront\btnBringCheckBox1ToFront_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Bring CheckBox2 To Fromt" Top="112px" Left="300px" ID="btnBringCheckBox2ToFromt" Height="36px"  Width="200px" ClickAction="CheckBoxBringToFront\btnBringCheckBox2ToFront_Click"></vt:Button>        

        <vt:TextBox runat="server" Text="" Top="200px" Left="100px" ID="textBox1" Height="36px" Width="140px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
