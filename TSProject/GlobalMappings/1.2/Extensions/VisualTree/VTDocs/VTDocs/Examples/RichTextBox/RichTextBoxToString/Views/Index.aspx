 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox ToString Method" ID="windowView2" LoadAction="RichTextBoxToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a string that represents the TextBox control.

            Syntax: public override string ToString()
            
            Return Value: The string includes the type and the Text property of the control." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this RichTextBox." 
            Top="265px" ID="lblExp2"></vt:Label>

        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="330px" ID="TestedRichTextBox"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="425px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="530px" ID="btnToString" ClickAction="RichTextBoxToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>