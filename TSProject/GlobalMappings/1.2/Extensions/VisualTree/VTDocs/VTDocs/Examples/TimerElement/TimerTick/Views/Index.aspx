<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer Tick Event" ID="windowView1" LoadAction="TimerTick\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Tick" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the specified timer interval has elapsed and the timer is enabled.

            Syntax: public event EventHandler Tick"
            Top="75px" ID="lblDefinition">
        </vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="170px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following Tested Timer interval is 1 sec. Each invocation of the 'Tick'event 
            should add a line in the 'Event Log'.
            You can use the 'Start' and 'Stop' buttons to start and stop the timer."
            Top="215px" ID="Label1">
        </vt:Label>
       
        <vt:Label runat="server" SkinID="TimerLabel" Top="310px" ID="timerLabel"></vt:Label>

        <vt:TextBox runat="server" SkinID="TimerTextBox" Top="305px" ID="timerTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="335px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Height="180" Text="Event Log:" Top="380px" ID="txtEventLog"></vt:TextBox>
        
        <vt:Button runat="server" Text="Start >>" Top="590px" ID="btnStart" ClickAction="TimerTick\btnStart_Click"></vt:Button>

        <vt:Button runat="server" Text="Stop >>" Left="300" Top="590px" ID="btnStop" ClickAction="TimerTick\btnStop_Click"></vt:Button>

    </vt:WindowView>

    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="TestedTimer" Enabled="false" Interval="1000" TickAction="TimerTick\TestedTimer_Tick"></vt:Timer>
	</vt:ComponentManager>

</asp:Content>
        