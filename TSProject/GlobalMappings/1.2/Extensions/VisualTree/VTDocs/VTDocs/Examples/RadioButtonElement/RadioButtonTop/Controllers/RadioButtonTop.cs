using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Top value: " + TestedRadioButton.Top + '.';
        }

        public void btnChangeTop_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRadioButton.Top == 310)
            {
                TestedRadioButton.Top = 290;
                lblLog.Text = "Top value: " + TestedRadioButton.Top + '.';
            }
            else
            {
                TestedRadioButton.Top = 310;
                lblLog.Text = "Top value: " + TestedRadioButton.Top + '.';
            }

        }

    }
}