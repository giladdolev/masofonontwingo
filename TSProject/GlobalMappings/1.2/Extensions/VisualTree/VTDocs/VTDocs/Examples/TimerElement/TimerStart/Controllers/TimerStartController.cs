using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerStartController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TimerStart());
        }

        private TimerStart ViewModel
        {
            get { return this.GetRootVisualElement() as TimerStart; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");

            lblLog.Text = "Press the 'Start' button...";
        }

        public void TestedTimer_Tick(object sender, EventArgs e)
        {
            TextBoxElement timerTextBox = this.GetVisualElementById<TextBoxElement>("timerTextBox");
            timerTextBox.Text = ViewModel.durationProperty++.ToString();
        }

        public void btnStart_Click(object sender, EventArgs e)
        {
            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            //timer1.Enabled = true;
            TestedTimer.Start();
            lblLog.Text = "Timer has started, Enabled value: " + TestedTimer.Enabled;
        }

        public void btnStop_Click(object sender, EventArgs e)
        {
            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTimer.Stop();
            lblLog.Text = "Timer has stoped, Enabled value: " + TestedTimer.Enabled;
        }

    }
}