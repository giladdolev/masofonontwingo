using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelBackgroundImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel1.BackgroundImage == null)
            {
                lblLog1.Text = "No background image";
            }
            else
            {
                lblLog1.Text = "Set with background image";
            }


            if (TestedLabel2.BackgroundImage == null)
            {
                lblLog2.Text = "No background image.";
            }
            else
            {
                lblLog2.Text = "Set with background image.";
            }
        }

        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel2.BackgroundImage == null)
            {
                TestedLabel2.BackgroundImage = new UrlReference("/Content/Elements/Image.png");
                lblLog2.Text = "Set with background image.";
            }
            else
            {
                TestedLabel2.BackgroundImage = null ;
                lblLog2.Text = "No background image.";

            }
        }
    }
}