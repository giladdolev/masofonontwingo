<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Bounds Property" ID="windowView" LoadAction="GroupBoxBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Bounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size and location of the control including its nonclient elements, 
            in pixels, relative to the parent control.

            Syntax: public Rectangle Bounds { get; set; }
            
            The bounds of the control include the nonclient elements such as scroll bars, 
            borders, title bars, and menus." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The bounds of this 'TestedGroupBox' is initially set with 
            Left: 80px, Top: 360px, Width: 200px, Height: 80px" Top="295px"  ID="lblExp1"></vt:Label>     

        <!-- TestedGroupBox -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="360px" ID="TestedGroupBox"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="455px" Height="40" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change GroupBox Bounds >>"  Top="535px" Width="180px" ID="btnChangeBounds" ClickAction="GroupBoxBounds\btnChangeBounds_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>
