using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonUseVisualStyleBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeUseVisualStyleBackColor_Click(object sender, EventArgs e)
        {
            ButtonElement btnUseVisualStyleBackColor = this.GetVisualElementById<ButtonElement>("btnUseVisualStyleBackColor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            

            if (btnUseVisualStyleBackColor.UseVisualStyleBackColor == true)
            {
                btnUseVisualStyleBackColor.UseVisualStyleBackColor = false;
                textBox1.Text = "UseVisualStyleBackColor value is:\\r\\n" + btnUseVisualStyleBackColor.UseVisualStyleBackColor;

            }

            else
            {
                btnUseVisualStyleBackColor.UseVisualStyleBackColor = true;
                textBox1.Text = "UseVisualStyleBackColor value is:\\r\\n" + btnUseVisualStyleBackColor.UseVisualStyleBackColor;
            }
        }

    }
}