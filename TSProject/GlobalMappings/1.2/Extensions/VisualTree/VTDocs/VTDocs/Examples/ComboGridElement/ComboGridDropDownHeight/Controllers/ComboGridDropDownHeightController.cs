using System;
using System.Data;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridDropDownHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement ComboGrid = this.GetVisualElementById<ComboGridElement>("ComboGrid");
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(int));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(string));
            source.Rows.Add(12345, "James", "Dean");
            source.Rows.Add(23451, "Bob", "Marley");
            source.Rows.Add(34512, "Dana", "International");
            source.Rows.Add(45123, "Brad", "Pitt");

            ComboGrid.ValueMember = "Column2";
            ComboGrid.DisplayMember = "Column1";

            ComboGrid.DataSource = source;           

            lblLog1.Text = "ComboGrid DropDownHeight value: " + ComboGrid.DropDownHeight;

            DataTable source1 = new DataTable();
            source1.Columns.Add("Column1", typeof(int));
            source1.Columns.Add("Column2", typeof(string));
            source1.Columns.Add("Column3", typeof(string));
            source1.Rows.Add(12345, "James", "Dean");
            source1.Rows.Add(23451, "Bob", "Marley");
            source1.Rows.Add(34512, "Dana", "International");
            source1.Rows.Add(45123, "Brad", "Pitt");

            TestedComboGrid.ValueMember = "Column2";
            TestedComboGrid.DisplayMember = "Column1";

            TestedComboGrid.DataSource = source;

            lblLog2.Text = "ComboGrid DropDownHeight value: " + TestedComboGrid.DropDownHeight + '.';   

        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedComboGrid.DropDownHeight == 30)
            {
                TestedComboGrid.DropDownHeight = 90;
            }
            else
            {
                TestedComboGrid.DropDownHeight = 30;
            }

            lblLog2.Text = "ComboGrid DropDownHeight value: " + TestedComboGrid.DropDownHeight + '.';      
        }

    }
}