using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListBoxMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ListBox MinimumSize value: " + TestedListBox.MinimumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
        }

        public void btnChangeLstMinimumSize_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedListBox.MinimumSize == new Size(350, 60))
            {
                TestedListBox.MinimumSize = new Size(300, 80);
                lblLog1.Text = "ListBox MinimumSize value: " + TestedListBox.MinimumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
            }
            else if (TestedListBox.MinimumSize == new Size(300, 80))
            {
                TestedListBox.MinimumSize = new Size(200, 40);
                lblLog1.Text = "ListBox MinimumSize value: " + TestedListBox.MinimumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
            }
            else
            {
                TestedListBox.MinimumSize = new Size(350, 60);
                lblLog1.Text = "ListBox MinimumSize value: " + TestedListBox.MinimumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
            }

        }

        public void btnSetToZeroLstMinimumSize_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedListBox.MinimumSize = new Size(0, 0);
            lblLog1.Text = "ListBox MinimumSize value: " + TestedListBox.MinimumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;

        }


    }
}