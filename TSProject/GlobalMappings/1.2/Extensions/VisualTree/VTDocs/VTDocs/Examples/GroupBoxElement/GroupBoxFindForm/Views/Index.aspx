<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox FindForm() Method" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:GroupBox runat="server" Text="Tested GroupBox" Top="100px" Left="140px" ID="grpFindForm" Height="150px" Width="200px" >           
        </vt:GroupBox>

        <vt:Button runat="server" Text="Invoke GroupBox FindForm() method" Top="150px" Left="420px" ID="btnInvokeFindForm" Height="36px" Width="200px" ClickAction="GroupBoxFindForm\btnInvokeFindForm_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="300px" Left="140px" ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        