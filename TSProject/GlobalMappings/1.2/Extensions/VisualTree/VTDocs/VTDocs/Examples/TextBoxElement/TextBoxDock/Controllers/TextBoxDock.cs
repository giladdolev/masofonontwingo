using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeDock_Click(object sender, EventArgs e)
        {
            TextBoxElement txtDockChange = this.GetVisualElementById<TextBoxElement>("txtDockChange");

            if (txtDockChange.Dock == Dock.Bottom)
            {
                txtDockChange.Dock = Dock.Fill; 
            }
            else if (txtDockChange.Dock == Dock.Fill)
            {
                txtDockChange.Dock = Dock.Left; 
            }
            else if (txtDockChange.Dock == Dock.Left)
            {
                txtDockChange.Dock = Dock.Right; 
            }
            else if (txtDockChange.Dock == Dock.Right)
            {
                txtDockChange.Dock = Dock.Top; 
            }
            else if (txtDockChange.Dock == Dock.Top)
            {
                txtDockChange.Dock = Dock.None;
            }
            else if (txtDockChange.Dock == Dock.None)
            {
                txtDockChange.Dock = Dock.Bottom;
            }       
        }

    }
}