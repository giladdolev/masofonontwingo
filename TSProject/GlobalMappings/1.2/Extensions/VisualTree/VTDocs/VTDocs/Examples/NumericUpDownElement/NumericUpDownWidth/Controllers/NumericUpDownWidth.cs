using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Width value: " + TestedNumericUpDown.Width + '.';
        }

        private void btnChangeWidth_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedNumericUpDown.Width == 170)
            {
                TestedNumericUpDown.Width = 340;
                lblLog.Text = "Width value: " + TestedNumericUpDown.Width + '.';
            }
            else
            {
                TestedNumericUpDown.Width = 170;
                lblLog.Text = "Width value: " + TestedNumericUpDown.Width + '.';
            }
        }
      
    }
}