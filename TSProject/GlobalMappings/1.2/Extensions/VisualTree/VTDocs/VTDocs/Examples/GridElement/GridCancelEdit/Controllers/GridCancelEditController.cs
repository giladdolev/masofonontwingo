using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridCancelEditController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "The Selected cell is: Row: " + TestedGrid.SelectedRowIndex + ", " + "Column: " + TestedGrid.SelectedColumnIndex + '.';
        }

        public void TestedGrid_CancelEdit(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            int selectedRow = TestedGrid.SelectedRowIndex;
            int selectedColumn = TestedGrid.SelectedColumnIndex;

            lblLog.Text = "The Edited cell was: Row: " + selectedRow + ", " + TestedGrid.Columns[selectedColumn].HeaderText + ", Edited value: " + TestedGrid.Rows[selectedRow].Cells[selectedColumn].Value + '.';

            txtEventLog.Text += "\\r\\nFinished editing at Cell (" + selectedRow + "," + selectedColumn + "), CancelEdit event is invoked.";
        }
    }
}
