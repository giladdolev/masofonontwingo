<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar BackColor Property" ID="windowView1" LoadAction="ProgressBarBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:ProgressBar runat="server" Text="ProgressBar" Top="140px" ID="TestedProgressBar1"></vt:ProgressBar>
        <vt:Label runat="server" SkinID="Log" Top="195px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="260px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this 'TestedProgressBar' is initially set to Gray" Top="310px"  ID="lblExp1"></vt:Label>     
        
        <vt:ProgressBar runat="server" Text="TestedProgressBar" BackColor ="Gray" Top="380px" ID="TestedProgressBar2"></vt:ProgressBar>
        <vt:Label runat="server" SkinID="Log" Top="435px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Width="180px" Top="500px" ID="btnChangeBackColor" ClickAction="ProgressBarBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
