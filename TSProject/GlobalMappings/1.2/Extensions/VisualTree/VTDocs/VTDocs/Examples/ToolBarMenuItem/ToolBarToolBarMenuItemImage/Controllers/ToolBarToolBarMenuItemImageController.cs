using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarToolBarMenuItemImageController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            ToolBarDropDownItem FileMenuItems = TestedToolBar1.Items[0] as ToolBarDropDownItem;

            lblLog.Text = "File image value: " + FileMenuItems.Image.Source + "., File-New image value: " + FileMenuItems.DropDownItems[0].Image.Source +
                ".,\\r\\n File-Open image value: " + FileMenuItems.DropDownItems[1].Image.Source + ".," +
                "\\r\\nNew image value: " + TestedToolBar1.Items[2].Image.Source + "., Open image value: " + TestedToolBar1.Items[3].Image.Source + ".";

            ToolBarDropDownItem FileMenuItems2 = TestedToolBar2.Items[0] as ToolBarDropDownItem;

            lblLog1.Text = "File image value: " + FileMenuItems2.Image.Source + ", File-New image value: " + FileMenuItems2.DropDownItems[0].Image.Source +
                ",\\r\\n File-Open image value: " + FileMenuItems2.DropDownItems[1].Image.Source + "." +
                "\\r\\nNew image value: " + TestedToolBar2.Items[2].Image.Source + ", Open image value: " + TestedToolBar2.Items[3].Image.Source;
        }

        private void btnChangeImage_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            ToolBarDropDownItem FileMenuItems2 = TestedToolBar2.Items[0] as ToolBarDropDownItem;

            if (FileMenuItems2.DropDownItems[0].Image != null)
            {
                if (FileMenuItems2.DropDownItems[0].Image.Source.IndexOf("Open.png") > 0)
                {
                    FileMenuItems2.DropDownItems[0].Image = new UrlReference(@"Content/Images/New.png");
                }
                else
                {
                    FileMenuItems2.DropDownItems[0].Image = new UrlReference(@"Content/Images/Open.png");
                }
            }

            if (TestedToolBar2.Items[2].Image != null)
            {
                if (TestedToolBar2.Items[2].Image.Source.IndexOf("Open.png") > 0)
                {
                    TestedToolBar2.Items[2].Image = new UrlReference(@"Content/Images/New.png");
                }
                else
                {
                    TestedToolBar2.Items[2].Image = new UrlReference(@"Content/Images/Open.png");
                }
            }

            if (FileMenuItems2.DropDownItems[1].Image != null)
            {
                if (FileMenuItems2.DropDownItems[1].Image.Source.IndexOf("Open.png") > 0)
                {
                    FileMenuItems2.DropDownItems[1].Image = new UrlReference(@"Content/Images/New.png");
                }
                else
                {
                    FileMenuItems2.DropDownItems[1].Image = new UrlReference(@"Content/Images/Open.png");
                }
            }

            if (TestedToolBar2.Items[3].Image != null)
            {
                if (TestedToolBar2.Items[3].Image.Source.IndexOf("Open.png") > 0)
                {
                    TestedToolBar2.Items[3].Image = new UrlReference(@"Content/Images/New.png");
                }
                else
                {
                    TestedToolBar2.Items[3].Image = new UrlReference(@"Content/Images/Open.png");
                }
            }

            lblLog1.Text = "File image value: " + FileMenuItems2.Image.Source + ", File-New image value: " + FileMenuItems2.DropDownItems[0].Image.Source +
                ",\\r\\n File-Open image value: " + FileMenuItems2.DropDownItems[1].Image.Source + "." +
                "\\r\\nNew image value: " + TestedToolBar2.Items[2].Image.Source + ", Open image value: " + TestedToolBar2.Items[3].Image.Source;

        }
    }
}