using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ClientSize value: " + TestedListView.ClientSize;

        }
        public void btnChangeListViewClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            if (TestedListView.Width == 300)
            {
                TestedListView.ClientSize = new Size(273, 99);
                lblLog.Text = "ClientSize value: " + TestedListView.ClientSize;

            }
            else
            {
                TestedListView.ClientSize = new Size(300, 45);
                lblLog.Text = "ClientSize value: " + TestedListView.ClientSize;
            }

        }

    }
}