using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //CheckBoxAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            CheckBoxElement chkRuntimeDraggable = this.GetVisualElementById<CheckBoxElement>("chkRuntimeDraggable");

            if (chkRuntimeDraggable.Draggable == false)
            {
                chkRuntimeDraggable.Draggable = true;
                textBox1.Text = chkRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                chkRuntimeDraggable.Draggable = false;
                textBox1.Text = chkRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}