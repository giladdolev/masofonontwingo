<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab DeselectTab Method (TabItem)" ID="windowView" LoadAction="TabDeselectTabTabItem\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="DeselectTab" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Makes the tab following the tab with the specified ID the current tab.

            Syntax: public void DeselectTab(TabItem tabItem)
            tabItem - The tabItem to deselect.
            
            ArgumentNullException - index is less than 0 or greater than the number of TabItem controls
                                   in the TabItems collection minus 1 or tabItem is not in the TabItems collection. 
            ArgumentNullException - tabItem is null"
            Top="75px" ID="lblDefinition"></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="285px"  ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="In the following example you can invoke the DeselectTab(TabItem) by entering 
            one of the tab items text and clicking the DeselectTab button." Top="335px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tab runat="server" Text="TestedTab" Top="415px" ID="TestedTab">
            <TabItems>
                <vt:TabItem runat="server" Text="TabItem1" Top="22px" Left="4px" ID="tabItem1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="TabItem2" Top="22px" Left="4px" ID="tabItem2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="TabItem3" Top="22px" Left="4px" ID="tabItem3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="530px" Height="40" Width="460" ID="lblLog"></vt:Label>

        <vt:Label runat="server" Text="Enter tab item:" Left="80px" ID="lblInput" Top="595px"></vt:Label>

        <vt:TextBox runat="server" Text="Enter Text" Top="595px" Left="185px" Width="90px" ID="txtInput" ForeColor="Gray"  TextChangedAction="TabDeselectTabTabItem\txtInput_TextChanged" >
         </vt:TextBox>

        <vt:Button runat="server" Text="DeselectTab >>" Left="330px" Top="595px" ID="btnDeselectTab" ClickAction="TabDeselectTabTabItem\btnDeselectTab_Click"></vt:Button>

             
    </vt:WindowView>
</asp:Content>


