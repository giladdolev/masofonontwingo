using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridColumnDisplayIndexChangedController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void load_view(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            TestedGrid.AddToComboBoxList("Column4", "male,female");
            TestedGrid.Rows.Add("1", "Tomer", false, "male");
            TestedGrid.Rows.Add("2", "Yoav", true, "male");
            TestedGrid.Rows.Add("3", "Shalom", false, "male");
            TestedGrid.Rows.Add("4", "Shimrit", true, "female");
            TestedGrid.Rows.Add("5", "Moran", false, "female");


            lblLog.Text = "Move columns...";
        }

        public void TestedGrid_ColumnDisplayIndexChanged(object sender, GridColumnMoveEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            txtEventLog.Text += "\\r\\nGrid: column, ColumnDisplayIndexChanged event is invoked";
            lblLog.Text = "Column at index " + e.FromIndex + " was moved to index " + e.ToIndex + ".";
            PrintToLog();
        }

        private void btnChangeDisplayIndex_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGrid.Columns[0].DisplayIndex == 0)
                TestedGrid.Columns[0].DisplayIndex = 1;
            else if (TestedGrid.Columns[0].DisplayIndex == 1)
                TestedGrid.Columns[0].DisplayIndex = 2;
            else if (TestedGrid.Columns[0].DisplayIndex == 2)
                TestedGrid.Columns[0].DisplayIndex = 3;
            else 
                TestedGrid.Columns[0].DisplayIndex = 0;
            
            lblLog.Text = "Column1 DislayIndex was set to : " + TestedGrid.Columns[0].DisplayIndex;
            PrintToLog();
        }
        public void PrintToLog()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text += "\\r\\nColumn1: Index = " + TestedGrid.Columns[0].Index + ", DisplayIndex = " + TestedGrid.Columns[0].DisplayIndex + "\\r\\nColumn2: Index = " + TestedGrid.Columns[1].Index + ", DisplayIndex = " + TestedGrid.Columns[1].DisplayIndex + "\\r\\nColumn3: Index = " + TestedGrid.Columns[2].Index + ", DisplayIndex = " + TestedGrid.Columns[2].DisplayIndex + "\\r\\nColumn4: Index = " + TestedGrid.Columns[3].Index + ", DisplayIndex = " + TestedGrid.Columns[3].DisplayIndex;
        }




    }
}
