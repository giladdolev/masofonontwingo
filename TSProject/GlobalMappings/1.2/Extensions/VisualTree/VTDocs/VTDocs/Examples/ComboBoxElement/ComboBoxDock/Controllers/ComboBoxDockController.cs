using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");
            cmb1.Items.Add("aaa");
            cmb1.Items.Add("bbb");
        }

        private void btnDock_Click(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");

            if (cmb1.Dock == Dock.None)
            {
                cmb1.Dock = Dock.Fill;
            }
            else if (cmb1.Dock == Dock.Fill)
            {
                cmb1.Dock = Dock.Left;
            }
            else if (cmb1.Dock == Dock.Left)
            {
                cmb1.Dock = Dock.Right;
            }
            else if (cmb1.Dock == Dock.Right)
            {
                cmb1.Dock = Dock.Top;
            }
            else if (cmb1.Dock == Dock.Top)
            {
                cmb1.Dock = Dock.Bottom;
            }
            else if (cmb1.Dock == Dock.Bottom)
            {
                cmb1.Dock = Dock.None;
            }
        }

    }
}