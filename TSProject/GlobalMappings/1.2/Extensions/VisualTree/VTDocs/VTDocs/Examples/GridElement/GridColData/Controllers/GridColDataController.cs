using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace MvcApplication9.Controllers
{
    public class GridColDataController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

		private void OnLoad(object sender, EventArgs e)
		{
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

          
            TestedGrid.Rows.Add("1", "Shir", "0524244541", true);
            TestedGrid.Rows.Add("2", "Michal", "0523454344", false);
            TestedGrid.Rows.Add("3", "Maayan", "0526734512", true);
            TestedGrid.Rows.Add("3", "Shalom", "0526734512", false);
            TestedGrid.Rows.Add("4", "David", "0524144663", true);
            TestedGrid.Rows.Add("5", "Eti", "0523548975", true);
            TestedGrid.Rows.Add("6", "Haim", "0523434225", true);

            TestedGrid.ColData.Add(1, 1);
            TestedGrid.ColData.Add(2, "Two");
            TestedGrid.ColData.Add(3, this);
            TestedGrid.ColData.Add(4, new String [2]{"Four", "Column"});

            lblLog.Text = "Grid ColData collection";
            for (int i = 1; i <= TestedGrid.ColData.Count; i++)
            {

                lblLog.Text += "\\r\\nKey:" + i + ", Value: " + TestedGrid.ColData[i].ToString() + ", Type: " + TestedGrid.ColData[i].GetType();

            } 
          
		}

        private void btnChangeColData_Click(object sender, EventArgs e)
		{
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement btnChangeColData = this.GetVisualElementById<ButtonElement>("btnChangeColData");

            if(TestedGrid.ColData[1] == "#")
            {
                TestedGrid.ColData.Clear();
                TestedGrid.ColData.Add(1, 1);
                TestedGrid.ColData.Add(2, "Two");
                TestedGrid.ColData.Add(3, this);
                TestedGrid.ColData.Add(4, new String[2] { "Four", "Column" });

                btnChangeColData.Text = "Set ColData with columns HeaderText >>";

            }
            else
            {
                TestedGrid.ColData.Clear();
                TestedGrid.ColData.Add(1, "#");
                TestedGrid.ColData.Add(2, "First Name");
                TestedGrid.ColData.Add(3, "Telephone No.");
                TestedGrid.ColData.Add(4, "Present");

                btnChangeColData.Text = "Set ColData with values of different types >>";

            }


            PrintToLog();
        }


        private void PrintToLog()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid ColData collection";
            for (int i = 1; i <= TestedGrid.ColData.Count; i++)
            {

                lblLog.Text += "\\r\\nKey:" + i + ", Value: " + TestedGrid.ColData[i].ToString() + ", Type: " + TestedGrid.ColData[i].GetType();

            }
        }

	}
}
