using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerExController : Controller
    {
        private static GridElement grid1, grid2;
        private static SplitContainer split1, split2;
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        public ActionResult Index()
        {
            return View();
        }

        public void load_view(object sender, EventArgs e)
        {

            grid1 = new GridElement();
            grid1.Columns.Add(new GridColumn("1"));
            grid1.Columns.Add(new GridColumn("2"));
            grid1.Rows.Add("r1", "r1");
            grid1.Rows.Add("r2", "r2");

            grid2 = new GridElement();
            grid2.Columns.Add(new GridColumn("1"));
            grid2.Columns.Add(new GridColumn("2"));
            grid2.Rows.Add("r1", "r1");
            grid2.Rows.Add("r2", "r2");

            split1 = this.GetVisualElementById<SplitContainer>("splitContainer1");
            split2 = this.GetVisualElementById<SplitContainer>("splitContainer2");

            split1.Panel2.Controls.Add(grid2);
            split1.Panel1.Controls.Add(grid1);
            

            split2.Panel1.Controls.Add(split1);
        }

    }
}