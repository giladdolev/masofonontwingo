using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton1 = this.GetVisualElementById<ButtonElement>("TestedButton1");
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedButton1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
        }
        
        public void btnChangeButtonBorderStyle_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedButton2.BorderStyle == BorderStyle.None)
            {
                TestedButton2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Dotted)
            {
                TestedButton2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Double)
            {
                TestedButton2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedButton2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedButton2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Groove)
            {
                TestedButton2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Inset)
            {
                TestedButton2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Dashed)
            {
                TestedButton2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.NotSet)
            {
                TestedButton2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Outset)
            {
                TestedButton2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Ridge)
            {
                TestedButton2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedButton2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else if (TestedButton2.BorderStyle == BorderStyle.Solid)
            {
                TestedButton2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
            else
            {
                TestedButton2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedButton2.BorderStyle + '.';
            }
        }
    }
}
