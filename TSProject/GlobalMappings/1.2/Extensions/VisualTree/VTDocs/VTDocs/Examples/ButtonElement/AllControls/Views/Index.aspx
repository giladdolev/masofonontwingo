<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" EnableTheming="true" StylesheetTheme="ThemeRed" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="All Controls" ID="windowView1" Height="905px" Width="768px">
        <vt:SplitContainer runat="server" Dock="Fill">
            <vt:SplitterPanel runat="server" ID="Panel1" Dock="Fill">
                <vt:NumericUpDown runat="server" ID="TextBox1" Top="10" />
                <vt:ProgressBar runat="server" ID="NumericUpDown1" Value="50" Top="60" />
                <vt:GroupBox runat="server" ID="Panel2" Text="GroupBox" Top="90" Height="80">
                    <vt:RadioButton runat="server" ID="Label4" Text="fff" />
                    <vt:RadioButton runat="server" ID="RadioButton1" Text="ggg" Top="40" />
                </vt:GroupBox>
                <vt:RichTextBox runat="server" ID="TextBox2" Text="RichTextBox" Top="200" Height="80" />
                <vt:TextBox runat="server" ID="TextBox3" Text="TextBox" Top="310" />
                <vt:MaskedTextBox runat="server" ID="TextBox4" Text="MaskedTextBox" Top="340" />
                <vt:Tree runat="server" Top="370" Height="180">
                    <Items>
                        <vt:TreeItem runat="server" Text="t0">
                            <Items>
                                <vt:TreeItem runat="server" Text="t01" />
                                <vt:TreeItem runat="server" Text="t02" />
                                <vt:TreeItem runat="server" Text="t03">
                                    <Items>
                                        <vt:TreeItem runat="server" Text="t031" />
                                        <vt:TreeItem runat="server" Text="t032" />
                                    </Items>
                                </vt:TreeItem>
                            </Items>
                        </vt:TreeItem>
                    </Items>
                </vt:Tree>
                <vt:Tab runat="server" ID="Tab1" Top="580" Height="80">
                    <TabItems>
                        <vt:TabItem runat="server" Text="Tab1"></vt:TabItem>
                        <vt:TabItem runat="server" Text="Tab2"></vt:TabItem>
                    </TabItems>
                </vt:Tab>
                <vt:Grid runat="server" Top="690" ContextMenuStripID="contextMenuStripCells" RowCount="3">
                    <Columns>
                        <vt:GridTextBoxColumn ID="txtclm" runat="server" HeaderText="TextBox Column" DataMember="TextColumn"></vt:GridTextBoxColumn>
                        <vt:GridTextButtonColumn ID="txtbtnclm" runat="server" HeaderText="TextButton Column" DataMember="TextButtonColumn"></vt:GridTextButtonColumn>
                        <vt:GridCheckBoxColumn ID="cehckclm" runat="server" HeaderText="CheckBox Column" DataMember="CheckBoxColumn"></vt:GridCheckBoxColumn>
                        <vt:GridDateTimePickerColumn ID="dataclm" runat="server" HeaderText="Date Column" DataMember="DateTimeColumn" Format="Short"></vt:GridDateTimePickerColumn>
                        <vt:GridComboBoxColumn ID="comboclm" runat="server" HeaderText="ComboBox Column" DataMember="ComboBoxColumn" Width="90" Format="Short"></vt:GridComboBoxColumn>
                        <vt:GridTextBoxColumn ID="GridTextBoxColumn1" runat="server" HeaderText="Currency" DataMember="Currency" Format="currency"></vt:GridTextBoxColumn>
                    </Columns>
                </vt:Grid>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" ID="p1" Dock="Left" Width="400px">
                <vt:Button runat="server" ID="ButtonMessageBox" Text="MessageBox" Top="10" ClickAction="AllControls/ButtonMessageBox_Click" />
                <vt:Button runat="server" ID="ButtonOpenFile" Text="OpenFile" Top="10" Left="150" ClickAction="AllControls/ButtonOpenFile_Click" />
                <vt:CheckBox runat="server" ID="Label3" Text="CheckBox" Top="60" />
                <vt:CheckedListBox runat="server" ID="ComboBox1" Top="90" Height="80">
                    <Items>
                        <vt:ListItem runat="server" Text="xxx" />
                        <vt:ListItem runat="server" Text="yyy" />
                        <vt:ListItem runat="server" Text="ccc" />
                    </Items>
                </vt:CheckedListBox>
                <vt:ComboBox runat="server" ID="Label2" Text="ComboBox" Top="200">
                    <Items>
                        <vt:ListItem runat="server" Text="xxx" />
                        <vt:ListItem runat="server" Text="yyy" />
                        <vt:ListItem runat="server" Text="ccc" />
                    </Items>
                </vt:ComboBox>
                <vt:DateTimePicker runat="server" ID="Label5" Top="230" />
                <vt:Label runat="server" ID="l1" Text="Label" Top="260" />
                <vt:LinkLabel runat="server" ID="Label6" Text="LinkLabel" Top="290" />
                <vt:ListBox runat="server" ID="CheckedListBox1" Top="320" Height="80">
                    <Items>
                        <vt:ListItem runat="server" Text="xxx" />
                        <vt:ListItem runat="server" Text="yyy" />
                        <vt:ListItem runat="server" Text="ccc" />
                    </Items>
                </vt:ListBox>
                <vt:ListView runat="server" ID="ListBox1" Top="430" Height="80">
                    <Columns>
                        <vt:ColumnHeader runat="server" Text="col0" Width="100"></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" Text="col1" Width="100"></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" Text="col2" Width="100"></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" Text="col3" Width="100"></vt:ColumnHeader>
                    </Columns>
                    <Items>
                        <vt:ListViewItem runat="server" Text="xxx">
                            <Subitems>
                                <vt:ListViewSubitem runat="server" Text="xxx1"></vt:ListViewSubitem>
                                <vt:ListViewSubitem runat="server" Text="xxx2"></vt:ListViewSubitem>
                                <vt:ListViewSubitem runat="server" Text="xxx3"></vt:ListViewSubitem>
                            </Subitems>
                        </vt:ListViewItem>
                        <vt:ListViewItem runat="server" Text="yyy">
                            <Subitems>
                                <vt:ListViewSubitem runat="server" Text="yyy1"></vt:ListViewSubitem>
                                <vt:ListViewSubitem runat="server" Text="yyy2"></vt:ListViewSubitem>
                                <vt:ListViewSubitem runat="server" Text="yyy3"></vt:ListViewSubitem>
                            </Subitems>
                        </vt:ListViewItem>
                        <vt:ListViewItem runat="server" Text="zzz">
                            <Subitems>
                                <vt:ListViewSubitem runat="server" Text="zzz1"></vt:ListViewSubitem>
                                <vt:ListViewSubitem runat="server" Text="zzz2"></vt:ListViewSubitem>
                                <vt:ListViewSubitem runat="server" Text="zzz3"></vt:ListViewSubitem>
                            </Subitems>
                        </vt:ListViewItem>
                    </Items>
                </vt:ListView>
                <vt:MonthCalendar runat="server" ID="Label1" Top="540" />
            </vt:SplitterPanel>
        </vt:SplitContainer>
        <vt:ToolBar runat="server" ID="MainMenu" Dock="Top">
            <vt:ToolBarMenuItem runat="server" Text="File">
                <vt:ToolBarMenuItem runat="server" Text="New..." Image="Content/Images/New.png" />
                <vt:ToolBarMenuItem runat="server" Text="Open..." Image="Content/Images/Open.png" />
                <vt:ToolBarMenuSeparator runat="server" />
                <vt:ToolBarMenuItem runat="server" Text="Exit" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Edit">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="View">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Tools">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Window">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Help">
                <vt:ToolBarMenuItem runat="server" Text="About" />
            </vt:ToolBarMenuItem>
        </vt:ToolBar>
    </vt:WindowView>
</asp:Content>
