using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelHeight value: " + TestedRadioButton.PixelHeight + '.';
        }

        private void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRadioButton.PixelHeight == 80)
            {
                TestedRadioButton.PixelHeight = 26;
                lblLog.Text = "PixelHeight value: " + TestedRadioButton.PixelHeight + '.';
            }
            else
            {
                TestedRadioButton.PixelHeight = 80;
                lblLog.Text = "PixelHeight value: " + TestedRadioButton.PixelHeight + '.';
            }
        }
      
    }
}