<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer TabStop and TabIndex Properties" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="1000px">
        
        <vt:Label runat="server" Text="SplitContainer3 TabStop is set to false, SplitContaineres TabIndex are set from left to right" Top="30px" Left="100px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" ></vt:Label>

        <vt:Label runat="server" Text="1" Top="60px" Left="30px" ID="Label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>
        
        <vt:SplitContainer runat="server" Text="SplitContainer1" Top="60px" Left="40px" ID="SplitContainer1" Height="50px" TabIndex="1" Width="150px">
           
        </vt:SplitContainer>    

        <vt:Label runat="server" Text="2" Top="60px" Left="200px" ID="Label3" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>

        <vt:SplitContainer runat="server" Text="SplitContainer2" Top="60px" Left="210px" ID="SplitContainer2" Height="50px" TabIndex="2"  Width="150px">
          
        </vt:SplitContainer>

        <vt:Label runat="server" Text="3" Top="60px" Left="380px" ID="Label4" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>

        <vt:SplitContainer runat="server" Text="SplitContainer3" Top="60px" Left="390px" ID="SplitContainer3" Height="50px" TabIndex="3" TabStop="false" Width="150px">
          
        </vt:SplitContainer>    

        <vt:Label runat="server" Text="4" Top="60px" Left="560px" ID="Label5" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>

        <vt:SplitContainer runat="server" Text="SplitContainer4" Top="60px" Left="570px" ID="SplitContainer4" Height="50px"  TabIndex="4" Width="150px">
            
        </vt:SplitContainer>
       

         <vt:Label runat="server" Text="5" Top="60px" Left="740px" ID="Label6" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>

        <vt:SplitContainer runat="server" Text="SplitContainer5" Top="60px" Left="750px" ID="SplitContainer5" Height="50px" TabIndex="5" Width="150px">
      
        </vt:SplitContainer> 
        
         <vt:button runat="server" Text="Change SplitContainers TabIndex" Top="150px" Left="100px" TabStop ="false" ID="btnChangeTabIndex" Height="36px" Width="210px" ClickAction="SplitContainerTabIndexTabStop\btnChangeTabIndex_Click"></vt:button> 
        
         <vt:button runat="server" Text="Change SplitContainer3 TabStop" Top="150px" Left="340px" ID="btnChangeTabStop" Height="36px"  TabStop ="false" Width="210px" ClickAction="SplitContainerTabIndexTabStop\btnChangeTabStop_Click"></vt:button>    


        <vt:TextBox runat="server" Text="" Top="220px" Left="130px"  ID="textBox1" Multiline="true"  Height="80px" Width="170px"> 
            </vt:TextBox>

        <vt:TextBox runat="server" Text="" Top="220px" Left="340px"  ID="textBox2" Multiline="true"  Height="80px" Width="170px"> 
            </vt:TextBox>

     </vt:WindowView>
</asp:Content>
