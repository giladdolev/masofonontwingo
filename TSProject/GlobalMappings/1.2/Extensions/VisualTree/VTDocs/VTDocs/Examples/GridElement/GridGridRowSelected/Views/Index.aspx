﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridRow Selected" LoadAction="GridGridRowSelected\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="Selected" Top="15px" ID="lblTitle" Left="300"> </vt:Label>
        <vt:Label runat="server" Text="Gets or sets a value indicating whether the row is selected. 
            
            Syntax : public override bool Selected { get; set; }            
            Value: true if the row is selected; otherwise, false."
            Top="65px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="195px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, clicking on one of the grid rows will display the current SelectedRowIndex  
            property. You can also Select/UnSelect one of the rows by using the buttons." Top="255px" ID="Label2">
        </vt:Label>
        <vt:Grid runat="server" Top="340px" ID="TestedGrid" SelectionChangedAction="GridGridRowSelected\RowSelectionChanged_Click">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="470px" Height="60" ID="lblLog1"></vt:Label>

        <vt:Button runat="server" Text="Select/UnSelect first row >>" Top="580px" Width="180px" ID="btnSelectFirstRow" ClickAction="GridGridRowSelected\btnSelectFirstRow_Click"></vt:Button>
        <vt:Button runat="server" Text="Select/UnSelect second row >>" Top="580px" Width="180px" Left="280px" ID="btnSelectSecondRow" ClickAction="GridGridRowSelected\btnSelectSecondRow_Click"></vt:Button>
        <vt:Button runat="server" Text="Select/UnSelect last row >>" Top="580px" Width="180px" Left="480px" ID="btnSelectLastRow" ClickAction="GridGridRowSelected\btnSelectLastRow_Click"></vt:Button>
        
                
    </vt:WindowView>

</asp:Content>
