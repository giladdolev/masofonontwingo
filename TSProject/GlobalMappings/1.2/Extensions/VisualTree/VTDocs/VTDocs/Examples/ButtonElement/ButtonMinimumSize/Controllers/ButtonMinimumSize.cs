using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
   
        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button MinimumSize value: " + btnTestedButton.MinimumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;

        }
        public void btnChangeButtonMinimumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            if (btnTestedButton.MinimumSize == new Size(100, 50))
            {
                btnTestedButton.MinimumSize = new Size(200, 90);
                lblLog.Text = "Button MinimumSize value: " + btnTestedButton.MinimumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;

            }
            else if (btnTestedButton.MinimumSize == new Size(200, 90))
            {
                btnTestedButton.MinimumSize = new Size(150, 100);
                lblLog.Text = "Button MinimumSize value: " + btnTestedButton.MinimumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;
            }

            else
            {
                btnTestedButton.MinimumSize = new Size(100, 50);
                lblLog.Text = "Button MinimumSize value: " + btnTestedButton.MinimumSize + "\\r\\nButton Size value:" + btnTestedButton.Size;
            }

        }

        public void btnSetToZeroBtnMinimumSize_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            btnTestedButton.MinimumSize = new Size(0, 0);
            lblLog.Text = "Button MinimumSize value: " + btnTestedButton.MinimumSize + "\\r\\nButton Size value: " + btnTestedButton.Size;

        }

    }
}