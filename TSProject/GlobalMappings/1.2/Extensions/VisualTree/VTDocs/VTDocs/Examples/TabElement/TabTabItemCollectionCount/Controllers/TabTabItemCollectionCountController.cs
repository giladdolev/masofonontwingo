using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemCollectionCountController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Count value is: " + TestedTab1.TabItems.Count + "\\r\\n" + "Insert index number of TabItem and press the button ";
        }

        public void RemoveTabItem_Click(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

           int removeIndex;

            if (int.TryParse(txtInput.Text, out removeIndex))
            {
                if (removeIndex > -1 && removeIndex < TestedTab1.TabItems.Count)
                {
                    TestedTab1.TabItems.RemoveAt(removeIndex);
                    lblLog.Text = "TabItem in index " + removeIndex + " was removed." + "\\r\\n" + "Count value is: " + TestedTab1.TabItems.Count;
                }
                else
                {
                    lblLog.Text = "Index is out of range";

                }
            }
            else
            {
                lblLog.Text = "Number is required - Enter one of the tab indexes";
            }

        }

    

        private void AddRangeTabItems_Click(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabItem[] tabItems = new TabItem[3];
            tabItems[0] = new TabItem("New1");
            tabItems[1] = new TabItem("New2");
            tabItems[2] = new TabItem("New3");

            TestedTab1.TabItems.AddRange(tabItems);

            lblLog.Text = "Set of 3 TabItems were added." + "\\r\\n" + "Count value is: " + TestedTab1.TabItems.Count;

        }

        public void txtInput_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

            lblLog.Text = "TabItem index: " + txtInput.Text + '.';
        }


    }
}