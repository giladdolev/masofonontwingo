<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window ShowDialog Method" Top="57px" Left="0px" ID="windowView1" Height="800px" Width="700px">
		
        <vt:Label runat="server" Text="**Click the Open \ Show button to open another form as a modal dialog box**" Top="50px" Left="100px" ID="label" Width="350px" Font-Bold="true"></vt:Label>

		<vt:Button runat="server" TextAlign="MiddleCenter" Text="Open \ Show Tested Window as a modal dialog box" Top="100px" Left="160px" ClickAction="WindowShowDialog\btnShowDialog_Click" ID="btnShowDialog" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="350px">
		</vt:Button>
      

        <vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="350px" Left="200px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="250px">
		</vt:TextBox>

        </vt:WindowView>
</asp:Content>