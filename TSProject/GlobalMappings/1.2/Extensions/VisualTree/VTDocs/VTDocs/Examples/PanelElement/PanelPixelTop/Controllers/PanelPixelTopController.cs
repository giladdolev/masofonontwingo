using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "PixelTop Value: " + TestedPanel.PixelTop;

        }

        public void btnChangePixelTop_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.PixelTop == 285)
            {
                TestedPanel.PixelTop = 310;
                lblLog.Text = "PixelTop Value: " + TestedPanel.PixelTop;

            }
            else
            {
                TestedPanel.PixelTop = 285;
                lblLog.Text = "PixelTop Value: " + TestedPanel.PixelTop;
            }

        }
    }
}