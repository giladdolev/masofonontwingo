<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="6, 13" Text="MDIWindow" ID="mdi" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="600px" Width="777px" BackColor="Green" Resizable="false">
		<vt:Button runat="server" ImageIndex="-1" TextAlign="MiddleCenter" AutoSize="True" Text="open" Top="64px" Left="32px" ClickAction="MDIWindow\Command1_Click" ID="Command1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="57px" Width="193px"></vt:Button>
        <%--<vt:Button runat="server" ImageIndex="-1" TextAlign="MiddleCenter" AutoSize="True" Text="close" Top="300" Left="32px" ClickAction="MDIWindow\Command2_Click" ID="Button1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="57px" Width="193px"></vt:Button>--%>
	</vt:WindowView>
</asp:Content>
