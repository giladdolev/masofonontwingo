<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button MinimumSize Property" ID="windowView" LoadAction="ButtonMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedButton' is width: 50px and height: 70px. 
            Its MinimumSize is initially set to width: 70px and height: 80px.
            To cancel 'TestedButton' MinimumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label>     


        <vt:Button runat="server" SkinID="Wide"  Text="TestedButton" Top="370px" Width="50" MinimumSize="70, 80" height="70" ID="btnTestedButton"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="480px" ID="lblLog" Width="400px" Height="40px"></vt:Label>
        <vt:Button runat="server" Text="Change MinimumSize value >>"  Top="570px" Width="200px" ID="btnChangeButtonMinimumSize" ClickAction="ButtonMinimumSize\btnChangeButtonMinimumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="570px" width="200px" Left="310px" ID="btnSetToZeroBtnMinimumSize" ClickAction="ButtonMinimumSize\btnSetToZeroBtnMinimumSize_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>