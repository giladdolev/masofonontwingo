 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown ToString Method" ID="windowView2" LoadAction="NumericUpDownToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a string that represents the NumericUpDown control.

            Syntax: public override string ToString()
            
            Return Value: The return string includes the type and the values for the Minimum and Maximum properties." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this NumericUpDown." 
            Top="265px" ID="lblExp2"></vt:Label>

        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="330px" ID="TestedNumericUpDown"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Width="380px" Height="40px" ID="lblLog" Top="370px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="475px" ID="btnToString" ClickAction="NumericUpDownToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>