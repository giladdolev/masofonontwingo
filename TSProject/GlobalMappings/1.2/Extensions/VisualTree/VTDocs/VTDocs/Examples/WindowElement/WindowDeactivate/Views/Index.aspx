<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window Deactivate event" Top="57px" Left="11px" ID="windowView1" Height="900px" Width="768px" DeactivateAction="WindowDeactivate\wndDeactivate_Deactivate">
      

         <vt:Label runat="server" SkinID="Title" Text="Deactivate" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the window loses focus and is no longer the active window.

            Syntax: public event EventHandler Deactivate"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Invoke Deactivate event method by changing focus by using keyboard or mouse. Each invocation of the 'Deactivate' event should  
            add a line to the 'Event Log'."
            Top="215px" ID="lblExp2">
        </vt:Label>

        <vt:Button runat="server" Text="TestedButton" Top="290px" Left="200px" ID="btnDeactivate" Height="36px" Width="100px" ClickAction="WindowDeactivate\btndClick_Click" ></vt:Button>          


        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="340px" Width="450px"></vt:Label>

        <vt:TextBox runat="server" Text="" Top="400px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
