<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button LostFocus event" ID="windowView2" LoadAction="ButtonLostFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="LostFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control loses focus.

            Syntax: public event EventHandler LostFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out of one of the buttons,
             a 'LostFocus' event will be invoked.
             Each invocation of the 'LostFocus' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:GroupBox runat="server" Top="335px" Height="120px" Width="200px">

        <!--Tested Button1 -->
        <vt:Button runat="server"  Top="25px" Left="25px" Text="Button1" ID="button1" TabIndex="1" LostFocusAction="ButtonLostFocus\button1_LostFocus"></vt:Button>

        <!--Tested Button2 -->
        <vt:Button runat="server"  Top="70px" Left="25px" Text="Button2"  ID="button2" TabIndex="2" LostFocusAction="ButtonLostFocus\button2_LostFocus"></vt:Button>
        
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="340px" ID="lblLog" Top="470px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="520px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Top="640px" Text="Clear 'Event Log'" ID="btnClear" ClickAction="ButtonLostFocus\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
