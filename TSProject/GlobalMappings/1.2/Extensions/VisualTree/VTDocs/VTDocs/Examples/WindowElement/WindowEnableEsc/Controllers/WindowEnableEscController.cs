using System.Collections.Generic;
using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowEnableEscController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new WindowEnableEsc());
        }

        private WindowEnableEsc ViewModel
        {
            get { return this.GetRootVisualElement() as WindowEnableEsc; }
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowEnableEsc");
            return argsDictionary;
        }

        public void OnLoad(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = GetArgsDictionary();
            ViewModel.frm2 = VisualElementHelper.CreateFromView<WindowEnableEsc2>("WindowEnableEsc2", "Index2", null, argsDictionary, null);
            ViewModel.frm3 = VisualElementHelper.CreateFromView<WindowEnableEsc3>("WindowEnableEsc3", "Index3", null, argsDictionary, null);

            ButtonElement TestedButton1 = this.GetVisualElementById<ButtonElement>("TestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "EnableEsc value: " + ViewModel.frm2.EnableEsc.ToString();

            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "EnableEsc value: " + ViewModel.frm3.EnableEsc.ToString() + ".";

        }

        public void TestedButton1_Click(object sender, EventArgs e)
        {
            ViewModel.frm2.Show();
        }

        public void TestedButton2_Click(object sender, EventArgs e)
        {
            ViewModel.frm3.Show();
        }

        private void btnChangeEnableEscValue_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            ViewModel.frm3.EnableEsc = !ViewModel.frm3.EnableEsc;
            lblLog2.Text = "EnableEsc value: " + ViewModel.frm3.EnableEsc.ToString() + ".";
        }
       
    }
}