﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridSelectionChanged5DateTimePickerColumnQAController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows.Add("6/8/1974 12:00:00 AM", "7/8/1974 12:00:00 AM", "8/8/1974 12:00:00 AM");
            testedGrid.Rows.Add("9/8/1974 12:00:00 AM", "10/8/1974 12:00:00 AM", "11/8/1974 12:00:00 AM");
            testedGrid.Rows.Add("12/8/1974 12:00:00 AM", "13/8/1974 12:00:00 AM", "14/8/1974 12:00:00 AM");

            GridElement testedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");

            testedGrid2.Rows.Add("15/8/1974 12:00:00 AM", "16/8/1974 12:00:00 AM", "17/8/1974 12:00:00 AM");
            testedGrid2.Rows.Add("18/8/1974 12:00:00 AM", "19/8/1974 12:00:00 AM", "20/8/1974 12:00:00 AM");
            testedGrid2.Rows.Add("21/8/1974 12:00:00 AM", "22/8/1974 12:00:00 AM", "23/8/1974 12:00:00 AM");

            lblLog.Text = "Left and Right Grids: SelectedCells values: Empty";
        }

        public void TestedGrid_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nLeft Grid: BeforeSelectionChanged event is invoked";
        }

        public void TestedGrid2_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nRight Grid: BeforeSelectionChanged event is invoked";
        }

        public void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nLeft Grid: SelectionChanged event is invoked";

            lblLog.Text = "Left Grid: SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }

        public void TestedGrid2_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nRight Grid: SelectionChanged event is invoked";

            lblLog.Text = "Right Grid: SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }

        public void btnSelectMiddleCellLeft_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[1].Cells[1].Selected = true;

            lblLog.Text = "Left Grid: SelectedCells values: (" + testedGrid.Rows[1].Cells[1].Value + ") ";
        }

        public void btnSelectMiddleCellRight_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[1].Cells[1].Selected = true;

            lblLog.Text = "Right Grid: SelectedCells values: (" + testedGrid.Rows[1].Cells[1].Value + ") ";
        }

        public void btnClear_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();

            txtEventLog.Text = "Event Log:";
        }

        public void btnSelectRow0_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            GridElement testedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[0].Selected = true;
            testedGrid2.Rows[0].Selected = true;
        }

        public void btnSelectColumn0_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            GridElement testedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Columns[0].Selected = true;
            testedGrid2.Columns[0].Selected = true;
        }
    }
}
