<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Leave event" ID="windowView2" LoadAction="TextBoxLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the text boxes,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="265px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="345px" Height="120px" Width="340px">
            <vt:Label runat="server" Text="First Name :" Left="15px" Top="30px" ID="Label1"></vt:Label>
                
            <vt:TextBox runat="server" Top="30px" Left="160px" Text="Enter First Name" Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtFirstName" TabIndex="1" 
                    LeaveAction="TextBoxLeave\txtFirstName_Leave" TextChangedAction="TextBoxGotFocus\txtFirstName_TextChanged"></vt:TextBox>

            <vt:Label runat="server" Text="Last Name :" Left="15px" Top="75px" ID="Label2"></vt:Label>

            <vt:TextBox runat="server" Top="75px" Left="160px" Text="Enter Last Name" Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtLastName" TabIndex="2" 
                    LeaveAction="TextBoxLeave\txtLastName_Leave" TextChangedAction="TextBoxGotFocus\txtLastName_TextChanged"></vt:TextBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="480px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="520px" Width="360px" Height="140px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

