using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeDock_Click(object sender, EventArgs e)
        {
            ButtonElement btnDockChange = this.GetVisualElementById<ButtonElement>("btnDockChange");
           
            if (btnDockChange.Dock == Dock.Bottom) //Get
            {
                btnDockChange.Dock = Dock.Fill; //Set
            }
            else if (btnDockChange.Dock == Dock.Fill)
            {
                btnDockChange.Dock = Dock.Left; 
            }
            else if (btnDockChange.Dock == Dock.Left)
            {
                btnDockChange.Dock = Dock.Right; 
            }
            else if (btnDockChange.Dock == Dock.Right)
            {
                btnDockChange.Dock = Dock.Top; 
            }
            else if (btnDockChange.Dock == Dock.Top)
            {
                btnDockChange.Dock = Dock.None;
            }
            else 
            {
                btnDockChange.Dock = Dock.Bottom;
            }       
        }

    }
}