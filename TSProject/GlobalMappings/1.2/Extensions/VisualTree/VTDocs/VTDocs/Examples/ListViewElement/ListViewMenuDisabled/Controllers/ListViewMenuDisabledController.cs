using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewMenuDisabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            ListViewElement TestedListView3 = this.GetVisualElementById<ListViewElement>("TestedListView3");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            LabelElement lblLog3 = this.GetVisualElementById<LabelElement>("lblLog3");

            lblLog1.Text = "MenuDisabled value: " + TestedListView1.MenuDisabled;
            lblLog2.Text = "MenuDisabled value: " + TestedListView2.MenuDisabled + '.';
            lblLog3.Text = "MenuDisabled value: " + TestedListView3.MenuDisabled + '.';

        }

    }
}