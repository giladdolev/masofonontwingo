<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox PerformKeyDown() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformKeyDown of 'Tested RichTextBox'" Top="45px" Left="140px" ID="btnPerformKeyDown" Height="36px" Width="300px" ClickAction="RichTextBoxPerformKeyDown\btnPerformKeyDown_Click"></vt:Button>


        <vt:RichTextBox runat="server" Text="Tested RichTextBox" Top="150px" Left="200px" ID="testedRichTextBox" Height="70px"  Width="200px" KeyDownAction="RichTextBoxPerformKeyDown\testedRichTextBox_KeyDown"></vt:RichTextBox>           

        

    </vt:WindowView>
</asp:Content>
