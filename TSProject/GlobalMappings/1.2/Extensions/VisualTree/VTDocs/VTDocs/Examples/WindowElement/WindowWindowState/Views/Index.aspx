<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   
     <vt:WindowView runat="server" Text="Window WindowState Property" WindowState = "Maximized" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="700px">
		
		<vt:Button runat="server" TextAlign="MiddleCenter" Text="WindowState Minimized" Top="110px" Left="44px" ClickAction="WindowWindowState\btnWindowStateMinimized_Click" ID="btnWindowStateMinimized" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button>

		
         <vt:Button runat="server" TextAlign="MiddleCenter" Text="WindowState Normal" Top="110px" Left="250px" ClickAction="WindowWindowState\btnWindowStateNormal_Click" ID="btnWindowStateNormal" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="150px">
		</vt:Button>

         <vt:Button runat="server" TextAlign="MiddleCenter" Text="WindowState Maximized" Top="110px" Left="405px" ClickAction="WindowWindowState\btnWindowStateMaximized_Click" ID="btnWindowStateMaximized" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="150px">
		</vt:Button>

		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="230px" Left="200px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="150px">
		</vt:TextBox>

		<vt:Label runat="server" Text="WindowState is initially set to 'Maximized'" Top="9px" Left="41px" ID="label1" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="9.75pt" Height="16px" TabIndex="4" Width="682px">
		</vt:Label>

        </vt:WindowView>
</asp:Content>