﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Basic Example" Height="800px" Width="1200px" LoadAction="GridDateTimeColumnFormat\page_load">
        

        <vt:Grid runat="server" Top="100px" Left="10px" ID="gridElement" Height="200px" Width="782px">
            <Columns>
                <vt:GridTextBoxColumn ID="txtclm" runat="server" HeaderText="TextBox Column" DataMember="TextColumn"></vt:GridTextBoxColumn>

                <vt:GridTextButtonColumn ID="txtbtnclm" runat="server" HeaderText="TextButton Column" DataMember="TextButtonColumn"></vt:GridTextButtonColumn>

                <vt:GridCheckBoxColumn ID="cehckclm" runat="server" HeaderText="CheckBox Column" DataMember="CheckBoxColumn"></vt:GridCheckBoxColumn>

                <vt:GridDateTimePickerColumn ID="dataclm" runat="server" HeaderText="Date Column" DataMember="DateTimeColumn"  Format="Short"></vt:GridDateTimePickerColumn>

                <vt:GridComboBoxColumn ID="comboclm" runat="server" HeaderText="ComboBox Column" DataMember="ComboBoxColumn" Width="90" Format="Short"></vt:GridComboBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Fill the Grid" Top="100px" Left="820px" ID="button1" ClickAction="GridDateTimeColumnFormat\button1_Click" Height="45px" Width="236px">
        </vt:Button>
        
      
    </vt:WindowView>

</asp:Content>
