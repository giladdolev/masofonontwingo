﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree GetType Method" ID="windowView2" LoadAction="TreeGetType\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="GetType" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the Type of the current instance.

            Syntax: public Type GetType()" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the GetType button will invoke the 
            GetType method of this Tree." Top="250px" ID="lblExp2"></vt:Label>

        <vt:Tree runat="server" Text="TestedTree" Top="340px" ID="TestedTree">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="TreeItem1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="TreeItem2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="TreeItem3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="465"></vt:Label>

        <vt:Button runat="server" Text="GetType >>" Top="545px" ID="btnGetType" ClickAction="TreeGetType\GetType_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
