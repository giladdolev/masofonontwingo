using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Data;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeCollapseAllController : Controller
    {
        
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {

            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            DataTable source = new DataTable();
            source.Columns.Add("ID", typeof(string));
            source.Columns.Add("ParentID", typeof(string));
          
            source.Rows.Add("1", ""); 
            source.Rows.Add("2", "1");
            source.Rows.Add("3", "2");
            source.Rows.Add("4", "2");
            source.Rows.Add("5", "");
            source.Rows.Add("6", "5");
            source.Rows.Add("7", "5");
            source.Rows.Add("8", "5");


            TestedTree1.ParentFieldName = "ParentID";
            TestedTree1.KeyFieldName = "ID";
            TestedTree1.DataSource = source;

            lblLog.Text = "Click the button...";
        }

      

        public void btnCollapseAll_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTree1.CollapseAll();
            TestedTree2.CollapseAll();

            lblLog.Text = "CollapseAll() was invoked on both trees";

        }

    }
}