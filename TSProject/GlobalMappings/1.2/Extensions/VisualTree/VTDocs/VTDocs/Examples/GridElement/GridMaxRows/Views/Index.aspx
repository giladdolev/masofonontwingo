﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid MaxRows Property" LoadAction="GridMaxRows\OnLoad" >

        <vt:Label runat="server" SkinID="Title" Text="MaxRows" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Gets or Sets the maximum number of rows allowed.

         Syntax: public int MaxRows { get; set; }
            
            The default is maximum value of Int32.
            Exceptions: ArgumentException - Occur to prevent the user from adding rows or reduce the MaxRows    
            value, when the GridRowCollection will violate the MaxRows constrants as a consequence."
            Top="75px" ID="Label1">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the initial MaxRows value is set to 4.
            You can add or remove rows by clicking on the buttons, and also to change the MaxRows value to 2."
            Top="280px" ID="lblExp2">
        </vt:Label>

       <vt:Grid runat="server" Top="350px" Height="165px" MaxRows="4" ID="TestedGrid1">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>
        <vt:RadioButton runat="server" Top="390px" left="420px" Text="MaxRows: 4" ID="rbMax4" CheckedChangedAction="GridMaxRows\rbMax4_CheckedChanged"></vt:RadioButton>
        <vt:RadioButton runat="server" Top="440px" left="420px" Text="MaxRows: 2" ID="rbMax2" CheckedChangedAction="GridMaxRows\rbMax2_CheckedChanged"></vt:RadioButton>

        <vt:Label runat="server" SkinID="Log" Top="530px" Width="500px" Height="50px" ID="lblLog1"></vt:Label>
        <vt:Button runat="server" Top="620px" Text="Add New Row >>" ID="button2" ClickAction="GridMaxRows\Add_New_Row"></vt:Button>
        <vt:Button runat="server" Top="620px" left="250px" Text="Delete Row >>" ID="button1" ClickAction="GridMaxRows\Delete_Row"></vt:Button>

    </vt:WindowView>
</asp:Content>
