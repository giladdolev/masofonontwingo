<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Visible Property" ID="windowView1"  LoadAction="LabelVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="Label" Top="175px" ID="TestedLabel1"></vt:Label>
        
        <vt:Label runat="server" SkinID="Log" Top="205px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="270px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this Label is initially set to 'false'
             The Label should be invisible" Top="320px" ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Visible="false" Top="395px" ID="TestedLabel2"></vt:Label>
        
        <vt:Label runat="server" SkinID="Log" Top="425px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="490px" ID="btnChangeVisible" Width="200px" ClickAction ="LabelVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>