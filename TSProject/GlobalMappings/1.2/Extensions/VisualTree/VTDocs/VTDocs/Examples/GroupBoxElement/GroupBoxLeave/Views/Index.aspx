<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Leave event" ID="windowView2" LoadAction="GroupBoxLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the groups,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Text="Group 1" Height="50px" Width="180px" LeaveAction="GroupBoxLeave\grpButton1_Leave">
            <vt:Button runat="server" Top="15px" Left="15px" Text="Button 1" ID="btnButton1" TabIndex="1" ClickAction="GroupBoxLeave\btnButton1_Click"></vt:Button>
        </vt:GroupBox>
        <vt:GroupBox runat="server" Top="400px" Text="Group 2" Height="50px" Width="180px" LeaveAction="GroupBoxLeave\grpButton2_Leave">
            <vt:Button runat="server" Top="15px" Left="15px" Text="Button 2" ID="btnButton2" TabIndex="1" ClickAction="GroupBoxLeave\btnButton2_Click"></vt:Button>
        </vt:GroupBox>
        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="465px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="505px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

