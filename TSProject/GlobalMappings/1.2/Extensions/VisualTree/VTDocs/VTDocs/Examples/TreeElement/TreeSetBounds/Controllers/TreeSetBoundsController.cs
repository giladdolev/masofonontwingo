using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //TreeAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested Tree bounds are set to: \\r\\nLeft  " + TestedTree.Left + ", Top  " + TestedTree.Top + ", Width  " + TestedTree.Width + ", Height  " + TestedTree.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            if (TestedTree.Left == 100)
            {
                TestedTree.SetBounds(80, 300, 140, 110);
                lblLog.Text = "The Tested Tree bounds are set to: \\r\\nLeft  " + TestedTree.Left + ", Top  " + TestedTree.Top + ", Width  " + TestedTree.Width + ", Height  " + TestedTree.Height;

            }
            else
            {
                TestedTree.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested Tree bounds are set to: \\r\\nLeft  " + TestedTree.Left + ", Top  " + TestedTree.Top + ", Width  " + TestedTree.Width + ", Height  " + TestedTree.Height;
            }
        }

    }
}