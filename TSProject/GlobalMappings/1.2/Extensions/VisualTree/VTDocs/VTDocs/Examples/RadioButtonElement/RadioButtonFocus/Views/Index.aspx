<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton Focus() Method" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:RadioButton runat="server" Text="Tested RadioButton" Top="100px" Left="140px" ID="rdoFocus" Height="36px" Width="200px" >           
        </vt:RadioButton>

        <vt:Button runat="server" Text="Invoke RadioButton Focus() method" Top="110px" Left="350px" ID="btnInvokeFocus" Height="30px" Width="200px" ClickAction="RadioButtonFocus\btnInvokeFocus_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="200px" Left="140px" ID="textBox1" Multiline="true"  Height="50px" Width="150px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        