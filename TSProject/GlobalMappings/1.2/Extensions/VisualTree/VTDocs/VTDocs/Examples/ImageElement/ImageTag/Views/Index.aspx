
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image Tag Property" ID="windowView1" LoadAction="ImageTag\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Tag" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the object that contains data about the control.
            
            Syntax: public object Tag { get; set; }
            Value: An Object that contains data about the control. The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:Image runat="server" Text="Image" Top="175px" ID="TestedImage1"></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="280px" Width="410" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Tag property of this Image is initially set to the string 'New Tag'." Top="375px"  ID="lblExp1"></vt:Label>     
        
        <vt:Image runat="server" Text="TestedImage" Top="425px" Tag="New Tag." ID="TestedImage2"></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="530px" Width="410" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Tag value >>" Top="585px" ID="btnChangeBackColor" ClickAction="ImageTag\btnChangeTag_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

