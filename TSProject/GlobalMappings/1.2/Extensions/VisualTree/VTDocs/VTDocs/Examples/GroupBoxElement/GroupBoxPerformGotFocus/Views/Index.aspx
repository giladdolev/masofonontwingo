<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox PerformGotFocus() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformGotFocus of 'Tested GroupBox'" Top="45px" Left="140px" ID="btnPerformGotFocus" Height="36px" Width="300px" ClickAction="GroupBoxPerformGotFocus\btnPerformGotFocus_Click"></vt:Button>


        <vt:GroupBox runat="server" Text="Tested GroupBox" Top="150px" Left="200px" ID="testedGroupBox" Height="70px"  Width="200px" GotFocusAction="GroupBoxPerformGotFocus\testedGroupBox_GotFocus"></vt:GroupBox>           

        

    </vt:WindowView>
</asp:Content>
