<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer ForeColor property" ID="windowView1" LoadAction="SplitContainerForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested SplitContainer1 -->
        <vt:SplitContainer runat="server" Text="SplitContainer" SplitterDistance="150" Width="300px" Top="165px" ID="TestedSplitContainer1">
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel1" ID="SplitterPanel1">
            <vt:Label runat="server" Text="Panel1" Top="10px" ID="Panel1Label"></vt:Label>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel2" ID="SplitterPanel2">
            <vt:Label runat="server" Text="Panel2" Top="10px" ID="Panel2Label"></vt:Label>
            </vt:SplitterPanel>
        </vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="255px" BorderStyle="None" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of this SplitContainer is initially set to Green" Top="385px" ID="lblExp1"></vt:Label>

        <!-- Tested SplitContainer2 -->
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" SplitterDistance="150" Width="300px" Top="420px" ForeColor="Green" ID="TestedSplitContainer2">
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel3" ID="SplitterPanel3">
               <vt:Label runat="server" Text="Panel3" Top="10px" ID="Panel3Label"></vt:Label>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel4" ID="SplitterPanel4">
               <vt:Label runat="server" Text="Panel4" Top="10px" ID="Panel4Label"></vt:Label>
            </vt:SplitterPanel>
        </vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="590px" Width="180px" ID="btnChangeSplitContainerForeColor" ClickAction="SplitContainerForeColor\btnChangeSplitContainerForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
