<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic ComboGrid" Top="57px" Left="11px" Height="800px" ID="windowView1" LoadAction="ComboGridBasic\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ComboGrid" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="The ComboGrid is a special ComboBox that embeds a grid in it's dropdowm portion.
            It has the ability to change the ComboBox value by selecting a row-record from the dropdown grid.            
            The dropdown grid displays data in a traditional tabular format. Each data source record is represented 
            as a row, and all data source data fields have related columns. Data portions are stored within individual
            cells. Some of the features that can be employed for the dropdown grid are: hiding column headers, 
            filtering, sorting, and more.
            
            You should add a data source to the ComboGrid control and define it's ValueMember and 
            DisplayMember properties.             
            The ValueMember will be set with a column name that matches the data field which represent the value 
            of each data source record, and after selecting the SelectedValue property will be set with this value.            
            The DisplayMember will be set with a column name that matches the data field which represent the text
            that will be displayed in the editable portion of the ComboGrid, and after selecting the SelectedItem 
            property will be set with this text."
            Top="75px" ID="lblDefinition">
        </vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="375px" ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="The following example shows basic usage of ComboGrid control.
            you can change the combogrid SelectedIndex value by selecting a line or a cell from the dropdown grid
            or by pressing the 'Change SelectedIndex Value' button. pressing the button will set the SelectedIndex 
            property, each click selects a different item, in a cyclic order." Top="425px" ID="lblExp2">
        </vt:Label>  

        <vt:ComboGrid runat="server" Top="525px" ID="TestedComboGrid" CssClass="vt-TestedComboGrid" SelectedIndexChangeAction="ComboGridBasic\TestedComboGrid_SelectedIndexChanged"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Left="80px" Height="60px" Top="565px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change SelectedIndex Value >>" Top="660px" Width="200px" ID="btnSelect" ClickAction="ComboGridBasic\btnSelect_click"></vt:Button>

        <vt:Button runat="server" Text="Clear Selection >>" Top="660px" Width="200px" Left="300px" ID="btnClearSelection" ClickAction="ComboGridBasic\btnClearSelection_click"></vt:Button>

    </vt:WindowView>
</asp:Content>
