using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedSplitContainer.ClientRectangle;

        }
        public void btnChangeSplitContainerClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            if (TestedSplitContainer.Left == 100)
            {
                TestedSplitContainer.SetBounds(80, 320, 200, 80);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedSplitContainer.ClientRectangle;

            }
            else
            {
                TestedSplitContainer.SetBounds(100, 310, 300, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedSplitContainer.ClientRectangle;
            }

        }

    }
}