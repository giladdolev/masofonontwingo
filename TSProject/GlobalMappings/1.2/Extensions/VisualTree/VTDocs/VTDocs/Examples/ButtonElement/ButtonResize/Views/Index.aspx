
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Resize Property" ID="windowView" LoadAction="ButtonResize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Resize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control is resized.

            Syntax: public event EventHandler Resize." Top="75px" ID="Label2" ></vt:Label>
               
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the Size property by clicking the 'Change Button Size' button. 
            Each invocation of the 'Resize' event will add a line to the 'Event Log'." Top="250px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide"  Text="TestedButton" Top="320px" ID="btnTestedButton" ResizeAction="ButtonResize\btnTestedButton_Resize"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="390px" Width="350" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="430px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change Button Size >>"  Top="590px" Width="180px" ID="btnChangeButtonSize" ClickAction="ButtonResize\btnChangeButtonSize_Click"></vt:Button>
              
    </vt:WindowView>
</asp:Content>




