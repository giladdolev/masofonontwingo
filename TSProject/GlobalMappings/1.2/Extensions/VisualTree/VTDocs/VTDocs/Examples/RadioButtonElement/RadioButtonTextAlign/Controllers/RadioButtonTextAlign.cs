using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonTextAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Set TextAlign property
        private void btnSetTextAlign_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoTextAlign = this.GetVisualElementById<RadioButtonElement>("rdoTextAlign");

            if (rdoTextAlign.TextAlign == ContentAlignment.MiddleLeft) //Get
            {
                rdoTextAlign.TextAlign = ContentAlignment.MiddleRight;//Set
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else if (rdoTextAlign.TextAlign == ContentAlignment.MiddleRight)
            {
                rdoTextAlign.TextAlign = ContentAlignment.MiddleCenter;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else if (rdoTextAlign.TextAlign == ContentAlignment.MiddleCenter)
            {
                rdoTextAlign.TextAlign = ContentAlignment.BottomCenter;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else if (rdoTextAlign.TextAlign == ContentAlignment.BottomCenter)
            {
                rdoTextAlign.TextAlign = ContentAlignment.BottomLeft;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else if (rdoTextAlign.TextAlign == ContentAlignment.BottomLeft)
            {
                rdoTextAlign.TextAlign = ContentAlignment.BottomRight;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else if (rdoTextAlign.TextAlign == ContentAlignment.BottomRight)
            {
                rdoTextAlign.TextAlign = ContentAlignment.TopCenter;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else if (rdoTextAlign.TextAlign == ContentAlignment.TopCenter)
            {
                rdoTextAlign.TextAlign = ContentAlignment.TopLeft;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else if (rdoTextAlign.TextAlign == ContentAlignment.TopLeft)
            {
                rdoTextAlign.TextAlign = ContentAlignment.TopRight;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }
            else
            {
                rdoTextAlign.TextAlign = ContentAlignment.MiddleLeft;
                rdoTextAlign.Text = "TextAlign - " + rdoTextAlign.TextAlign.ToString();
            }

            
            
           

        }

        //Get TextAlign property
        private void btnGetTextAlign_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoTextAlign = this.GetVisualElementById<RadioButtonElement>("rdoTextAlign");
            MessageBox.Show("TextAlign mode: " + rdoTextAlign.TextAlign.ToString());
        }
    }
}