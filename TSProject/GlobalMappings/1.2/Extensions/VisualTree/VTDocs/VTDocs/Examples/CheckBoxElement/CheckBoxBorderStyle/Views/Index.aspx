<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox BorderStyle Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

         
        <vt:CheckBox runat="server" Text="CheckBox BorderStyle is initially set to 'Dotted'" BorderStyle="Dotted" Top="45px"  Left="140px" ID="chkBorderStyle" Height="36px" Font-Names=""  TabIndex="1" Width="220px"></vt:CheckBox>           

        <vt:Button runat="server" Text="Change CheckBox BorderStyle" Top="45px" Left="400px" ID="btnChangeBorderStyle" Height="36px" TabIndex="1" Width="220px" ClickAction="CheckBoxBorderStyle\btnChangeBorderStyle_Click"></vt:Button>

         <vt:TextBox runat="server" Text="" Top="115px" Left="140px" ID="textBox1" Height="36px" TabIndex="1" Width="220px" ></vt:TextBox>

    </vt:WindowView>
</asp:Content>
