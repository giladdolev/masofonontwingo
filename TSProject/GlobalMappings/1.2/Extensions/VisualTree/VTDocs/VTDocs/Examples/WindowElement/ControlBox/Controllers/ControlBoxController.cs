using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Rendering.ExtJS;

namespace MvcApplication9.Controllers
{

    /// <summary>
    /// Home Controller
    /// </summary>
    public class ControlBoxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private WindowElement ViewModel
        {
            get { return this.GetRootVisualElement() as WindowElement; }
        }

        public void btnMinimizeBox_Click(object sender, EventArgs e)
        {
            if (this.ViewModel.MinimizeBox == true) //get
            {
                this.ViewModel.MinimizeBox = false; //set
            }
            else
                this.ViewModel.MinimizeBox = true;
        }

        public void btnMaximizeBox_Click(object sender, EventArgs e)
        {
            if (this.ViewModel.MaximizeBox == true) //get
            {
                this.ViewModel.MaximizeBox = false; //set
            }
            else
                this.ViewModel.MaximizeBox = true;
        }

        public void btnControlBox_Click(object sender, EventArgs e)
        {
            this.ViewModel.MaximizeBox = true;

            if (this.ViewModel.ControlBox == true) //get
            {
                this.ViewModel.ControlBox = false; //set
            }
            else
                this.ViewModel.ControlBox = true;
        }

       
    }
}