using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPerformKeyUpController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformKeyUp_Click(object sender, EventArgs e)
        {
            Keys keys = new Keys();
           KeyDownEventArgs args = new KeyDownEventArgs(keys);
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");

            testedRichTextBox.PerformKeyUp(args);
        }

        public void testedRichTextBox_KeyUp(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRichTextBox KeyUp event method is invoked");
        }

    }
}