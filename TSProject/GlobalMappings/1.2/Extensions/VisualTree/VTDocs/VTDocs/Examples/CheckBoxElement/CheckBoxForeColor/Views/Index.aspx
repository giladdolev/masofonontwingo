<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox ForeColor Property" ID="windowView1" LoadAction="CheckBoxForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>
         
        <vt:CheckBox runat="server" Text="CheckBox" Top="175px" ID="TestedCheckBox1"></vt:CheckBox>           

        <vt:Label runat="server" SkinID="Log" Top="215px" BorderStyle="None" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px" ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="ForeColor property of this CheckBox is initially set to Green" Top="330px" ID="lblExp1"></vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox" ForeColor="Green" Top="390px" ID="TestedCheckBox2"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="495px" ID="btnChangeForeColor" TabIndex="1" Width="180px" ClickAction="CheckBoxForeColor\btnChangeForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
