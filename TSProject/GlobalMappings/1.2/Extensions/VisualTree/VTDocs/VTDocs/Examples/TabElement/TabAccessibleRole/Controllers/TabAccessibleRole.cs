using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            TabElement tabAccessibleRole = this.GetVisualElementById<TabElement>("tabAccessibleRole");

            MessageBox.Show(tabAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            TabElement tabAccessibleRole = this.GetVisualElementById<TabElement>("tabAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (tabAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else if (tabAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                tabAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = tabAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}