using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformKeyDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformKeyDown_Click(object sender, EventArgs e)
        {
            Keys keys = new Keys();
            KeyDownEventArgs args = new KeyDownEventArgs(keys);
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");

            testedTextBox.PerformKeyDown(args);
        }

        public void testedTextBox_KeyDown(object sender, EventArgs e)
        {
            MessageBox.Show("TestedTextBox KeyDown event method is invoked");
        }

    }
}