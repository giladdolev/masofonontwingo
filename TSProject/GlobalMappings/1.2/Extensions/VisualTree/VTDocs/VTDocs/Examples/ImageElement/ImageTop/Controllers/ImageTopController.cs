using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             lblLog.Text = "Top Value: " + TestedImage.Top + '.';

         }
         public void btnChangeTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
             if (TestedImage.Top == 285)
             {
                 TestedImage.Top = 300;
                 lblLog.Text = "Top Value: " + TestedImage.Top + '.';

             }
             else
             {
                 TestedImage.Top = 285;
                 lblLog.Text = "Top Value: " + TestedImage.Top + '.';
             }

         }

    }
}