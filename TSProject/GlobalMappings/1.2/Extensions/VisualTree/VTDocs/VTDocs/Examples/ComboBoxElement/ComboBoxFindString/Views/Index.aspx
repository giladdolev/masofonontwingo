<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox FindString Method" ID="windowView1" LoadAction="ComboBoxFindString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="FindString" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Returns the index of the first item in the ComboBox that starts with the specified string.

            Syntax: public int FindString(string s)
            
            Return Value: The zero-based index of the first item found; returns -1 if no match is found,
            or 0 if the s parameter specifies Empty.
            The search performed by this method is not case-sensitive."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Insert a string into the TextBox bellow and press the FindString button." 
            Top="280px" ID="lblExp2">
        </vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" Top="355px" ID="TestedComboBox"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Text="Insert a String to find..." ID="lblLog" Top="395px"></vt:Label>

        <vt:Label runat="server" Text="Search String in ComboBox:" ID="Label2" Top="470px"></vt:Label>

        <vt:TextBox runat="server" Top="470px" Left="250px" ID="txtInput" Text="Text Input" TextChangedAction="ComboBoxFindString\TextBox_TextChanged" WaitMaskDisabled="true"></vt:TextBox>

        <vt:Button runat="server" Text="FindString >>" Left="430px" Top="470px" ID="btnFindString" ClickAction="ComboBoxFindString\btnFindString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
