using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MDIWindowController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        
        /// <summary>
        /// Handles the Click event of the Command1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        /// <returns></returns>
        public async Task Command1_Click(object sender, EventArgs e)
        {
            MDIWindow1 f2 = null;
            Dictionary<string, object> argsDictionary = GetArgsDictionary();
            if (f2 == null)
                f2 = VisualElementHelper.CreateFromView<MDIWindow1>("MDIWindow1", "MDIWindow1", null, argsDictionary, null);
           await f2.ShowDialog();
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "MDIWindow");
            return argsDictionary;
        }

        public void Command2_Click(object sender, EventArgs e)
        {
        }

    }
}