﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView1" Text="Grid GridRowCollection Add method (Object[])" LoadAction="GridGridRowCollectionAddObjectArray\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="Add" Top="15px" ID="lblTitle" Left="300">
        </vt:Label>
        <vt:Label runat="server" Text="Adds a new row to the collection, and populates the cells with the specified objects. 

            syntax :  public int Add(params object[] values)"
            Top="65px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="180px" ID="lblSubTitle"></vt:Label>

        <vt:Label runat="server" Text="You can add add remove rows using the bellow buttons." Top="220px" ID="lblExp1">
        </vt:Label>
        <vt:Grid runat="server" Height="150px" Top="295px" ID="testedGrid" >
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server"  SkinID="Log" Width="330px" Top="460px" ID="lblLog" Text=""></vt:Label>
        
        
        <vt:Button runat="server" Top="525px" Left="80px" Text="Add a Row >>" ClickAction="GridGridRowCollectionAddObjectArray\btnAddItems_Click">
        </vt:Button>
        <vt:Button runat="server" Top="525px" Left="280px" Text="Remove a Row >>" ClickAction="GridGridRowCollectionAddObjectArray\btnRemoveRow_Click">
        </vt:Button>
    </vt:WindowView>

</asp:Content>
