<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox ForeColor property" ID="windowView2" LoadAction="ComboBoxForeColor/OnLoad">
        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color ForeColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:ComboBox runat="server" ID="TestedComboBox1" Text="ComboBox" Top="140px"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="180px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ForeColor property of this 'TestedRadioButton' is initially set to Green" Top="295px"  ID="lblExp1"></vt:Label>     
        
        <vt:ComboBox runat="server" ID="TestedComboBox2" Text="TestedComboBox" ForeColor="Green" Top="345px"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="385px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="450px" Width="180px" ID="btnChangeForeColor" ClickAction="ComboBoxForeColor\btnChangeForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

