using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridGridColumnMaxWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "default Column1 MaxWidth: " + TestedGrid.Columns[0].MaxWidth + ", Column1 Width: " + TestedGrid.Columns[0].Width +
                "\\r\\ninitialized Column2 MaxWidth: " + TestedGrid.Columns[1].MaxWidth + ", Column2 Width: " + TestedGrid.Columns[1].Width +
                "\\r\\ninitialized Column3 MaxWidth: " + TestedGrid.Columns[2].MaxWidth + ", Column3 Width: " + TestedGrid.Columns[2].Width;

        }

        private void cmbSelectColumn_SelectedIndexChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbSelectColumn = this.GetVisualElementById<ComboBoxElement>("cmbSelectColumn");

            lblLog.Text = "Input Column Name: " + cmbSelectColumn.Text;
        }

        private void cmbSelectWidth_SelectedWidthChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbSelectWidth = this.GetVisualElementById<ComboBoxElement>("cmbSelectWidth");

            lblLog.Text = "Input MaxWidth: " + cmbSelectWidth.Text;
        }

        private void btnSetColumnMaxWidth_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            ComboBoxElement cmbSelectColumn = this.GetVisualElementById<ComboBoxElement>("cmbSelectColumn");
            ComboBoxElement cmbSelectWidth = this.GetVisualElementById<ComboBoxElement>("cmbSelectWidth");

            int inputWidth;
            if (int.TryParse(cmbSelectWidth.Text, out inputWidth))
            {
                switch (cmbSelectColumn.Text)
                {
                    case "Column1":
                        TestedGrid.Columns[0].MaxWidth = inputWidth;
                        PrintAllColumnsMaxWidth();
                        break;
                    case "Column2":
                        TestedGrid.Columns[1].MaxWidth = inputWidth;
                        PrintAllColumnsMaxWidth();
                        break;
                    case "Column3":
                        TestedGrid.Columns[2].MaxWidth = inputWidth;
                        PrintAllColumnsMaxWidth();
                        break;
                    default:
                        lblLog.Text = "Please Select a column from the first ComboBox";
                        break;
                }
            }
            else
                lblLog.Text = "Please select a Column and MaxWidth from below \\r\\nComboBoxes";

        }

        private void PrintAllColumnsMaxWidth()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Column1 MaxWidth: " + TestedGrid.Columns[0].MaxWidth + ", Column1 Width: " + TestedGrid.Columns[0].Width +
                "\\r\\nColumn2 MaxWidth: " + TestedGrid.Columns[1].MaxWidth + ", Column2 Width: " + TestedGrid.Columns[1].Width +
                "\\r\\nColumn3 MaxWidth: " + TestedGrid.Columns[2].MaxWidth + ", Column3 Width: " + TestedGrid.Columns[2].Width;
        }
    }
}