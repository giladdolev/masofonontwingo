using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonCheckedChangedController : Controller
    {
        RadioButtonElement rd1;
        RadioButtonElement rd2;
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void WriteChoice()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            rd1 = this.GetVisualElementById<RadioButtonElement>("radioButton1");
            rd2 = this.GetVisualElementById<RadioButtonElement>("radioButton2");

            if (rd1.IsChecked)
            {
                lblLog.Text = "RadioButton1 IsChecked value is: " + rd1.IsChecked;
                lblLog.Text += "\\r\\nRadioButton2 IsChecked value is: " + rd2.IsChecked;
            }
            else if (rd2.IsChecked)
            {
                lblLog.Text = "RadioButton1 IsChecked value is: " + rd1.IsChecked;
                lblLog.Text += "\\r\\nRadioButton2 IsChecked value is: " + rd2.IsChecked;
            }
            else
            {
                lblLog.Text = "No RadioButton IsChecked. Select a RadioButton";
            }
        }

        private void ToggleChoice()
        {
            rd1 = this.GetVisualElementById<RadioButtonElement>("radioButton1");
            rd2 = this.GetVisualElementById<RadioButtonElement>("radioButton2");

            if (rd1.IsChecked)
            {
                //rd1.IsChecked = false;
                rd2.IsChecked = true;
            }
            else if (rd2.IsChecked)
            {
                //rd2.IsChecked = false;
                rd1.IsChecked = true;
            }
        }

        private void ResetChoice()
        {
            rd1 = this.GetVisualElementById<RadioButtonElement>("radioButton1");
            rd2 = this.GetVisualElementById<RadioButtonElement>("radioButton2");

            if (rd1.IsChecked)
            {
                rd1.IsChecked = false;
            }
            if (rd2.IsChecked)
            {
                rd2.IsChecked = false;
            }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            WriteChoice();
        }

        public void rd1_checkedChanged(object sender, EventArgs e)
        {          
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nRadioButton1 CheckedChanged event is invoked";

            WriteChoice();
        }

        public void rd2_checkedChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            
            txtEventLog.Text += "\\r\\nRadioButton2 CheckedChanged event is invoked";

            WriteChoice();
        }

        public void btnChangeChecked_Click(object sender, EventArgs e)
        {
            ToggleChoice();
        }
        public void btnResetChecked_Click(object sender, EventArgs e)
        {
            ResetChoice();
        }
    }
}