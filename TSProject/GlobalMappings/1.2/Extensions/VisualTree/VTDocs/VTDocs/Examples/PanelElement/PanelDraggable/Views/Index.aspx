<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Draggable Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Text="Draggable is set to 'true'" Top="55px" Draggable="true" Left="140px" ID="pnlDraggable" Height="70px" Width="200px"></vt:Panel>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Text="Runtime Draggable" Top="165px" Left="140px" ID="pnlRuntimeDraggable" Height="70px" TabIndex="1" Width="200px" ></vt:Panel>

        <vt:Button runat="server" Text="Change Panel Draggable" Top="195px" Left="420px" ID="btnChangeDraggable" Height="36px" TabIndex="1" Width="200px" ClickAction="PanelDraggable\btnChangeDraggable_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="275px" Left="110px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        