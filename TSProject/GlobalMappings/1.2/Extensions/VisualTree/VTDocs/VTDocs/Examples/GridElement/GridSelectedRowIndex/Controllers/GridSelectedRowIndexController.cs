using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridSelectedRowIndexController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

	    public void Form_load(object sender, EventArgs e)
	    {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            
            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");

            lblLog1.Text = "Grid Selected Row Index : " + gridElement.SelectedRowIndex.ToString();
	    }

        public void RowSelectionChanged_Click(object sender, GridElementSelectionChangeEventArgs e)
	    {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
	        lblLog1.Text = "Grid Selected Row Index : " + gridElement.SelectedRowIndex.ToString();
	    }
        //public void SelectedRowIndex_Click(object sender, EventArgs e)
        //{
        //    GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
        //    ComboBoxElement cmbIndexes = this.GetVisualElementById<ComboBoxElement>("cmbIndexes");
        //    LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

        //    int index = Int32.Parse(cmbIndexes.Text);

        //    gridElement.FirstDisplayedScrollingRowIndex = index;
        //    gridElement.Refresh();
        //    gridElement.CurrentCell = gridElement.Rows[index].Cells[0];
        //    gridElement.Rows[index].Selected = true;
            
        //    lblLog1.Text = "Grid Selected Row Index : " + gridElement.SelectedRowIndex.ToString();

        //}
	}
}
