using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImagePixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Image PixelLeft Value is: " + TestedImage.PixelLeft + '.';
        }
        public void ChangePixelLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            if (TestedImage.PixelLeft == 200)
            {
                TestedImage.PixelLeft = 80;
                lblLog.Text = "Image PixelLeft Value is: " + TestedImage.PixelLeft + '.';

            }
            else
            {
                TestedImage.PixelLeft = 200;
                lblLog.Text = "Image PixelLeft Value is: " + TestedImage.PixelLeft + '.';
            }
        }

    }
}