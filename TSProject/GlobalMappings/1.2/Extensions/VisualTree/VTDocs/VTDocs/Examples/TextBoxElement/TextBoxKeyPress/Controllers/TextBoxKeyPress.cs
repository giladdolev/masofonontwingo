using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxKeyPressController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Enter Phone No.";
        }

        public void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {            
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else if ((e.KeyCode == Keys.Back) || (e.KeyCode == Keys.ShiftKey))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

            txtEventLog.Text += "\\r\\nTextBox: Phone No, KeyPress event is invoked";

        }

        public void txtPhone_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtPhone = this.GetVisualElementById<TextBoxElement>("txtPhone");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            string key = "";

            txtPhone.ForeColor = Color.Black;

            // in order to simulate placeholder text:
            // the dimed text is changed to black and the initial text is cleared
            if (txtPhone.Text.IndexOf("0000000000") == 0)
            {
                key = txtPhone.Text.Substring(10);
                txtPhone.Clear();
                txtPhone.Text = key;
            }
            else if (txtPhone.Text.IndexOf("000000000") == 0)
            {
                txtPhone.Clear();
            }

            if (txtPhone.Text != "")
            {
                lblLog.Text = "Phone No. input: " + txtPhone.Text + '.';
            }
            else
            {
                lblLog.Text = "Phone No. TextBox is empty";
            }
        }
    }
}