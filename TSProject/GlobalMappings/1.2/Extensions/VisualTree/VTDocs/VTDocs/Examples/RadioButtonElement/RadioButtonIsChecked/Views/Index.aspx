<%@Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="RadioButton IsChecked Property" ID="windowView1" LoadAction="RadioButtonIsChecked\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="IsChecked" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control is checked.

            Syntax: public bool IsChecked { get; set; }

            'true' if the check box is checked; otherwise, 'false'. The default value is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:RadioButton runat="server" Text="RadioButton" Top="200px" ID="TestedRadioButton1"  CheckedChangedAction="RadioButtonIsChecked\rarioButton0_CheckChanged"></vt:RadioButton>

        <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="IsChecked property of this RadioButton is initially set to 'true'"
             Top="370px" ID="lblExp2">
        </vt:Label>

        <vt:RadioButton runat="server" Text="TestedRadioButton" IsChecked="true" Top="420px" ID="TestedRadioButton2" CheckedChangedAction="RadioButtonIsChecked\rarioButton1_CheckChanged"></vt:RadioButton>

        <vt:Label runat="server" SkinID="Log" Top="460px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change IsChecked value >>" Width="180px" Top="530px" ID="btnChangeIsChecked" ClickAction="RadioButtonIsChecked\btnChangeIsChecked_Click"></vt:Button>



    </vt:WindowView>


</asp:Content>
