using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TabAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabRuntimeDraggable = this.GetVisualElementById<TabElement>("tabRuntimeDraggable");

            if (tabRuntimeDraggable.Draggable == false)
            {
                tabRuntimeDraggable.Draggable = true;
                textBox1.Text = tabRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                tabRuntimeDraggable.Draggable = false;
                textBox1.Text = tabRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}