<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton GotFocus event" ID="windowViewRadioButtonGotFocus" LoadAction="RadioButtonGotFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="GotFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control receives focus.

            Syntax: public event EventHandler GotFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted between the radio button
             options, a 'GotFocus' event will be invoked.
             Each invocation of the 'GotFocus' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Height="120px" Width="150px">
             <vt:RadioButton runat="server" Top="25px" Left="15px" Text="Male" ID="radioMale" TabIndex="1" GotFocusAction="RadioButtonGotFocus\radioMale_GotFocus"></vt:RadioButton>
            <vt:RadioButton runat="server" Top="70px" Left="15px" Text="Female" ID="radioFemale" TabIndex="2" GotFocusAction="RadioButtonGotFocus\radioFemale_GotFocus"></vt:RadioButton>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="420px" ID="lblLog" Top="470px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="510px" Width="420px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Focus Male Option >>" Width="180px" Top="640px" TabIndex="3" ID="btnChangeFocus" GotFocusAction="RadioButtonGotFocus\btnChangeFocus_GotFocus" ClickAction="RadioButtonGotFocus\btnChangeFocus_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>


