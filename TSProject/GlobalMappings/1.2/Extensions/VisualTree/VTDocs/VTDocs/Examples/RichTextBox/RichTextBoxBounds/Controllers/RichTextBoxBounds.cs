using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "RichTextBox bounds are: \\r\\n" + TestedRichTextBox.Bounds;

        }
        public void btnChangeRichTextBoxBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.Bounds == new Rectangle(100, 340, 300, 50))
            {
                TestedRichTextBox.Bounds = new Rectangle(80, 355, 200, 80);
                lblLog.Text = "RichTextBox bounds are: \\r\\n" + TestedRichTextBox.Bounds;

            }
            else
            {
                TestedRichTextBox.Bounds = new Rectangle(100, 340, 300, 50);
                lblLog.Text = "RichTextBox bounds are: \\r\\n" + TestedRichTextBox.Bounds;
            }

        }

    }
}