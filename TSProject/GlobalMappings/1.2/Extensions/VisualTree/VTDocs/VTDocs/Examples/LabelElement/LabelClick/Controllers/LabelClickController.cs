using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void LabelClick_Click(object sender, EventArgs e)
        {
            LabelElement lblEvent = this.GetVisualElementById<LabelElement>("lblEvent");
            lblEvent.Text = "Click event for label is invoked";
        }
 
    }
}