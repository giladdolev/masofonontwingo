<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Appearance Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:GroupBox runat="server" Text="Appearance is set to 'cc3D'" Top="55px" Appearance="cc3D" Left="140px" ID="grpAppearance" Height="70px" Width="200px"></vt:GroupBox>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:GroupBox runat="server" Text="RuntimeAppearance" Top="165px" Left="140px" ID="grpRuntimeAppearance" Height="70px" TabIndex="1" Width="200px" ></vt:GroupBox>

        <vt:Button runat="server" Text="Change GroupBox Appearance" Top="195px" Left="420px" ID="btnChangeAppearance" Height="36px" TabIndex="1" Width="200px" ClickAction="GroupBoxAppearance\btnChangeAppearance_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="275px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        