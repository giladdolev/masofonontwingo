﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBarItem ImageScaling  Property" ID="windowView1" LoadAction="ToolBarItemImageScaling\OnLoad">

       <vt:Label runat="server" SkinID="Title" Text="ImageScaling" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the image that is displayed on a ToolBarMenuItem.           

            Syntax: public ResourceReference Image { get; set; }
            Property Values: The Image to be displayed." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:ToolBar runat="server" Text="ToolBar" Top="185px" Dock="None" ID="TestedToolBar1" CssClass="vt-test-toolb-1">
            <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item1" Text="New" Image="Content/Images/New.png" CssClass="vt-test-tbbutton-1-new"></vt:ToolBarMenuItem>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item2" Text="Open" Image="Content/Images/Open.png" CssClass="vt-test-tbbutton-1-open"></vt:ToolBarMenuItem>
        </vt:ToolBar>           

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Image of the New ToolBarMenuItem is initially set to 'New.png'.
            The Image of the New ToolBarMenuItem will change to the Image of the Open ToolBarMenuItem
             by clicking the 'Change Image' button" Top="375px" ID="lblExp1"></vt:Label> 

        <vt:ToolBar runat="server" Text="TestedToolBar" Top="445px" Dock="None" ID="TestedToolBar2" CssClass="vt-test-toolb-2">
            <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item1" Text="New" Image="Content/Images/New.png" CssClass="vt-test-tbbutton-2-new"></vt:ToolBarMenuItem>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item2" Text="Open" Image="Content/Images/Open.png" CssClass="vt-test-tbbutton-2-open"></vt:ToolBarMenuItem>
        </vt:ToolBar>

        <vt:Button runat="server" Text="Change Image >>" Top="585px" Left="430px" ID="btnChangeImage" Width="180px" ClickAction="ToolBarItemImageScaling\btnChangeImage_Click"></vt:Button>
       
    </vt:WindowView>
</asp:Content>