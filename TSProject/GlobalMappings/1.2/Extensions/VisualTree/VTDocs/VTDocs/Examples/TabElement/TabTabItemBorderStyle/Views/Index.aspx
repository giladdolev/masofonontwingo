<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem BorderStyle Property" ID="windowView1" LoadAction="TabTabItemBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Indicates the border style for the TabItem.
            
            Syntax: public BorderStyle BorderStyle { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Tab runat="server" Text="Tab" Top="150px" ID="TestedTab1">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="Tab1Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Height="40px" Top="260px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="345px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, BorderStyle property of 'tabItem1' is initially set to 'Dotted' 
            In addition BorderStyle property of 'tabItem2' is initially set to 'None'" Top="395px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tab runat="server" Text="TestedTab" Top="455px" ID="TestedTab2" TabReference="">
            <TabItems>
                <vt:TabItem runat="server" Text="tabItem1" Top="22px" Left="4px" BorderStyle ="Dotted" ID="Tab2Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem2" Top="22px" Left="4px" BorderStyle ="None" ID="Tab2Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem3" Top="22px" Left="4px"  ID="Tab2Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="565px" Height="40px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BorderStyle of tabItem1 >>" Width="200" Top="630px" ID="btnChangeBorderStyleTab2Item1" ClickAction="TabTabItemBorderStyle\btnChangeBorderStyleTab2Item1_Click"></vt:Button>

        <vt:Button runat="server" Text="Change BorderStyle of tabItem2 >>" left="300" Width="200" Top="630px" ID="btnChangeBorderStyleTab2Item2" ClickAction="TabTabItemBorderStyle\btnChangeBorderStyleTab2Item2_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

