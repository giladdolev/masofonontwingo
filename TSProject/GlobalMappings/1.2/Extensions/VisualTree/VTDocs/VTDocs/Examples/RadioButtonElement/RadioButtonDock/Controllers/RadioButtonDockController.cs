using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnDock_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoDock = this.GetVisualElementById<RadioButtonElement>("rdoDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (rdoDock.Dock == Dock.Bottom)
            {
                rdoDock.Dock = Dock.Fill;
                textBox1.Text = rdoDock.Dock.ToString();
            }
            else if (rdoDock.Dock == Dock.Fill)
            {
                rdoDock.Dock = Dock.Left;
                textBox1.Text = rdoDock.Dock.ToString();
            }
            else if (rdoDock.Dock == Dock.Left)
            {
                rdoDock.Dock = Dock.Right;
                textBox1.Text = rdoDock.Dock.ToString();
            }
            else if (rdoDock.Dock == Dock.Right)
            {
                rdoDock.Dock = Dock.Top;
                textBox1.Text = rdoDock.Dock.ToString();
            }
            else if (rdoDock.Dock == Dock.Top)
            {
                rdoDock.Dock = Dock.None;
                textBox1.Text = rdoDock.Dock.ToString();
            }
            else if (rdoDock.Dock == Dock.None)
            {
                rdoDock.Dock = Dock.Bottom;
                textBox1.Text = rdoDock.Dock.ToString();
            }  
        }

    }
}