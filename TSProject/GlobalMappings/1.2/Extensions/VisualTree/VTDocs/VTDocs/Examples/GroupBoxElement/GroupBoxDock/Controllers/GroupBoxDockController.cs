using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnDock_Click(object sender, EventArgs e)
        {
            GroupBoxElement grpDock = this.GetVisualElementById<GroupBoxElement>("grpDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (grpDock.Dock == Dock.Bottom)
            {
                grpDock.Dock = Dock.Fill;
                textBox1.Text = grpDock.Dock.ToString();
            }
            else if (grpDock.Dock == Dock.Fill)
            {
                grpDock.Dock = Dock.Left;
                textBox1.Text = grpDock.Dock.ToString();
            }
            else if (grpDock.Dock == Dock.Left)
            {
                grpDock.Dock = Dock.Right;
                textBox1.Text = grpDock.Dock.ToString();
            }
            else if (grpDock.Dock == Dock.Right)
            {
                grpDock.Dock = Dock.Top;
                textBox1.Text = grpDock.Dock.ToString();
            }
            else if (grpDock.Dock == Dock.Top)
            {
                grpDock.Dock = Dock.None;
                textBox1.Text = grpDock.Dock.ToString();
            }
            else if (grpDock.Dock == Dock.None)
            {
                grpDock.Dock = Dock.Bottom;
                textBox1.Text = grpDock.Dock.ToString();
            }  
        }

    }
}