using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedListView.Size;
        }
        private void btnChangeListViewSize_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedListView.Size == new Size(273, 99)) //Get
            {
                TestedListView.Size = new Size(200, 70); //Set
                lblLog.Text = "Size value: " + TestedListView.Size;
            }
            else
            {
                TestedListView.Size = new Size(273, 99);
                lblLog.Text = "Size value: " + TestedListView.Size;
            }

        }
    }
}