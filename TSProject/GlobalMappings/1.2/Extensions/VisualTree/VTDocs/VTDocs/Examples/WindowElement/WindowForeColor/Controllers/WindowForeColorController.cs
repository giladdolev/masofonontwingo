using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowForeColorController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowForeColor());
        }
        private WindowForeColor ViewModel
        {
            get { return this.GetRootVisualElement() as WindowForeColor; }
        }

        public void btnChangeForeColor_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.ForeColor == Color.Pink)
			{
                this.ViewModel.ForeColor = Color.RoyalBlue;
                textBox1.Text = this.ViewModel.ForeColor.ToString();
			}
			else
            {
                this.ViewModel.ForeColor = Color.Pink;
                textBox1.Text = this.ViewModel.ForeColor.ToString();
            }
                
		}

    }
}