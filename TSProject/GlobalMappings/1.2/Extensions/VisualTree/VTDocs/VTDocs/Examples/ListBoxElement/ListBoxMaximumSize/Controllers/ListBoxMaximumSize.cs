using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListBoxMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ListBox MaximumSize value: " + TestedListBox.MaximumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
        }

        public void btnChangeLstMaximumSize_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedListBox.MaximumSize == new Size(180, 90))
            {
                TestedListBox.MaximumSize = new Size(230, 70);
                lblLog1.Text = "ListBox MaximumSize value: " + TestedListBox.MaximumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
            }
            else if (TestedListBox.MaximumSize == new Size(230, 70))
            {
                TestedListBox.MaximumSize = new Size(150, 100);
                lblLog1.Text = "ListBox MaximumSize value: " + TestedListBox.MaximumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
            }
            else
            {
                TestedListBox.MaximumSize = new Size(180, 90);
                lblLog1.Text = "ListBox MaximumSize value: " + TestedListBox.MaximumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;
            }

        }

        public void btnSetToZeroLstMaximumSize_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedListBox.MaximumSize = new Size(0, 0);
            lblLog1.Text = "ListBox MaximumSize value: " + TestedListBox.MaximumSize + "\\r\\nListBox Size value: " + TestedListBox.Size;

        }


    }
}