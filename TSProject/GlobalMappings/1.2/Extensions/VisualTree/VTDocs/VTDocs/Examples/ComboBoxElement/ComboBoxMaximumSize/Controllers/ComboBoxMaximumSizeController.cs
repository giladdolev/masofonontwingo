using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ComboBox MaximumSize value: " + TestedComboBox.MaximumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;

        }
        public void btnChangeCmbMaximumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            if (TestedComboBox.MaximumSize == new Size(110, 90))
            {
                TestedComboBox.MaximumSize = new Size(130, 30);
                lblLog.Text = "ComboBox MaximumSize value: " + TestedComboBox.MaximumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;

            }
            else if (TestedComboBox.MaximumSize == new Size(130, 30))
            {
                TestedComboBox.MaximumSize = new Size(120, 50);
                lblLog.Text = "ComboBox MaximumSize value: " + TestedComboBox.MaximumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;
            }

            else
            {
                TestedComboBox.MaximumSize = new Size(110, 90);
                lblLog.Text = "ComboBox MaximumSize value: " + TestedComboBox.MaximumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;
            }

        }

        public void btnSetToZeroCmbMaximumSize_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.MaximumSize = new Size(0, 0);
            lblLog.Text = "ComboBox MaximumSize value: " + TestedComboBox.MaximumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;

        }

    }
}