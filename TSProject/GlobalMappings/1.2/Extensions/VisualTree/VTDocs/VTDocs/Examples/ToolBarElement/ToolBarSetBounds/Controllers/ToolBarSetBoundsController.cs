using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarSetBoundsController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested ToolBar bounds are set to: \\r\\nLeft  " + TestedToolBar.Left + ", Top  " + TestedToolBar.Top + ", Width  " + TestedToolBar.Width + ", Height  " + TestedToolBar.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            if (TestedToolBar.Left == 100)
            {
                TestedToolBar.SetBounds(80, 300, 400, 40);
                lblLog.Text = "The Tested ToolBar bounds are set to: \\r\\nLeft  " + TestedToolBar.Left + ", Top  " + TestedToolBar.Top + ", Width  " + TestedToolBar.Width + ", Height  " + TestedToolBar.Height;

            }
            else
            {
                TestedToolBar.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested ToolBar bounds are set to: \\r\\nLeft  " + TestedToolBar.Left + ", Top  " + TestedToolBar.Top + ", Width  " + TestedToolBar.Width + ", Height  " + TestedToolBar.Height;
            }
        }
    }
}