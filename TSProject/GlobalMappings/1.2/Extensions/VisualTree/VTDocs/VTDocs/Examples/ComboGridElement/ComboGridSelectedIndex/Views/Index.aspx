<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid SelectedIndex Property" ID="windowView1" LoadAction="ComboGridSelectedIndex\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="SelectedIndex" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the index specifying the currently selected item.

            Syntax: public override int SelectedIndex { get; set; }
            
            A value of negative one (-1) is returned if no item is selected." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:ComboGrid runat="server" Text="ComboGrid" ID="TestedComboGrid1" SelectedIndexChangeAction="ComboGridSelectedIndex\TestedComboGrid1_SelectedIndexChanged" Top="180px"> </vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="305px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the SelectedIndex property by selecting an item 
              from the ComboGrid or by pressing the 'Change SelectedIndex Value' button.
              pressing the button will set the SelectedIndex property, each click selects a different item, in a cyclic order.
              The initial SelectedIndex value is 1." 
            Top="355px" ID="lblExp2"></vt:Label>

        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-TestedComboGrid" Top="465px" ID="TestedComboGrid2" SelectedIndex="1" SelectedIndexChangeAction="ComboGridSelectedIndex\TestedComboGrid2_SelectedIndexChanged"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog2" Top="505px"></vt:Label>

        <vt:Button runat="server" Text="Change SelectedIndex Value >>" Top="595px" ID="btnChooseNext" Width="180px" ClickAction="ComboGridSelectedIndex\ChooseNext_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

