<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Font Property" ID="windowView2" LoadAction="LabelFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Label runat="server" SkinID="TestedLabel" Text="Label" Top="185px" ID="TestedLabel1" ></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="215px" Width="400px" Height="55px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="320px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedLabel is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="360px"  ID="lblExp1"></vt:Label>     
        
      
        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="425px" ID="TestedLabel2" Height="45px" Width="150px"></vt:Label>
        <vt:Label runat="server" SkinID="Log" Top="485px" Width="400px" Height="55px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="590px" ID="btnChangeLabelFont" ClickAction="LabelFont/btnChangeLabelFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
