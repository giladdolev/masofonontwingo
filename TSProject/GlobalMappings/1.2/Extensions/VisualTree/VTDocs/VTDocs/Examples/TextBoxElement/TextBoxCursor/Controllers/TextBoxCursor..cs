using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            TextBoxElement txtRuntimeCursor = this.GetVisualElementById<TextBoxElement>("txtRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (txtRuntimeCursor.Cursor == CursorsElement.Default)
            {
                txtRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                txtRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                txtRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                txtRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.Help)
            {
                txtRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                txtRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                txtRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.No)
            {
                txtRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                txtRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                txtRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;

            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                txtRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                txtRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                txtRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                txtRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                txtRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                txtRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                txtRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                txtRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else if (txtRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                txtRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
            else 
            {
                txtRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + txtRuntimeCursor.Cursor;
            }
        }

    }
}