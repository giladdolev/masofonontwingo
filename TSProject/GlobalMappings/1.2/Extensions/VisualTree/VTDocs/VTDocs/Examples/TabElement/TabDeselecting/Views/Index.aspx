<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Deselecting Example" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px" >
        
         <vt:Label runat="server" Text="Tab SelectedIndex is set to 3" Top="130px" Left="300px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 
        
        <vt:Tab runat="server"  SelectedIndex ="3" Top="180px" Left="230px" ID="tabControl1" Height="100px" Width="500" DeselectingAction = "TabDeselecting\tabControl1_Deselecting">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="lbl1" Text="Tab Item 1"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px">
                     <vt:Label runat="server" ID="Label2" Text="Tab Item 2"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px">
                     <vt:Label runat="server" ID="Label3" Text="Tab Item 3"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage4" Top="22px" Left="4px" ID="tabPage4" Height="74px" Width="192px">
                     <vt:Label runat="server" ID="Label4" Text="Tab Item 4"></vt:Label>
                </vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Button runat="server" Text="Change selectedIndex" Top="180px" Left="10px" ClickAction="TabDeselecting\ChangeSelectedIndex_Click" ID="ChangeTabsText" Height="23px" Width="170px"></vt:Button>
        <vt:TextBox runat="server" Text="" Top="180px" Left="185px"  ID="textBox1" Height="23px" MaxLength="1" Width="30px"></vt:TextBox>
        <vt:TextBox runat="server" Text="" Top="300px" Left="350px"  ID="txtExplanation" Multiline="true"  Height="40px" Width="250px"> 
            </vt:TextBox>

        <vt:Label runat="server" Text="Event Log" Top="400px" Left="350px"  ID="label5"   Height="15px" Width="200px"> 
            </vt:Label>

        <vt:TextBox runat="server" Text="" Top="420px" Left="350px"  ID="txtEventLog" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>

        