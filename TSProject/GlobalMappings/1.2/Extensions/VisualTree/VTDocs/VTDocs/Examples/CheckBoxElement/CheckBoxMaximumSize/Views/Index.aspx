<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="CheckBox MaximumSize" ID="windowView1" LoadAction="CheckBoxMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedCheckBox' is width: 200px 
            and height: 60px. Its MaximumSize is initially set to width: 150px and height: 50px.
            To cancel 'TestedCheckBox' MaximumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>
        
        <vt:CheckBox runat="server" SkinID="BackColorCheckBox" Text="TestedCheckBox" Top="360px" ID="TestedCheckBox" TabIndex="1" Width="200px" Height="60" MaximumSize="150, 50"></vt:CheckBox> 

        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="505px" width="200px" Left="80px" ID="btnChangeChkMaximumSize" ClickAction="CheckBoxMaximumSize\btnChangeChkMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="505px" width="200px" Left="310px" ID="btnSetToZeroChkMaximumSize" ClickAction="CheckBoxMaximumSize\btnSetToZeroChkMaximumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>







