

<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Items Property" ID="windowView2" LoadAction="ListViewItems\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Items" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets a collection containing all items in the control.

            Syntax: public ListView.ListViewItemCollection Items { get; }" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the 'Add Item' button will invoke the Items.Add property 
            on this ListView. Pressing the 'Remove Item' button will invoke the Items.Remove property 
            on this ListView." Top="230px" ID="lblExp2"></vt:Label>

        <vt:ListView runat="server" Text="TestedListView" Top="310px" Height="170px" ID="TestedListView" DoubleClickAction ="ListViewItems\AddItems_Click">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Width="355px" ID="lblLog" Top="490"></vt:Label>

        <vt:Button runat="server" Text="Remove Item >>" Top="560px" Left="300px" ID="btnRemoveItem" ClickAction="ListViewItems\RemoveItems_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Item >>" Top="560px"  ID="btnAddItem" ClickAction="ListViewItems\AddItems_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
