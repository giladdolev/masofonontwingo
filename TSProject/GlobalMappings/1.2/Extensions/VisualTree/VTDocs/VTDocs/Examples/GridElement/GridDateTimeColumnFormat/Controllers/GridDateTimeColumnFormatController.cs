using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridDateTimeColumnFormatController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void page_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            gridElement.TextButtonList.Add("TextButtonColumn");
            gridElement.AddToComboBoxList("ComboBoxColumn", "male,female");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            DataTable source = new DataTable();
            source.Columns.Add("TextColumn", typeof(string));
            source.Columns.Add("TextButtonColumn", typeof(string));
            source.Columns.Add("CheckBoxColumn", typeof(bool));
            source.Columns.Add("DateTimeColumn", typeof(DateTime));
            source.Columns.Add("ComboBoxColumn", typeof(string));

            source.Rows.Add("galilcs", @"c:\dir\", true, "20/12/2015 05:05:00", "male");
            source.Rows.Add("Bart", @"c:\dir\file", true, "20/12/2015 00:00:00", "male");
            source.Rows.Add("Homer", @"c:\dir\file", true, "15/10/2017 00:00:00", "female");
            source.Rows.Add("Lisa", @"c:\dir\file", true, "1/2/2016 00:00:00", "male");
            source.Rows.Add("Marge", @"c:\dir\file", true, "8/7/2015 00:00:00", "female");
            source.Rows.Add("Galilcs", @"c:\dir\file", true, "15/8/2015 00:00:00", "male");
            source.Rows.Add("Gizmox", @"c:\file", true, "1/12/2015 00:00:00", "female");
            gridElement.DataSource = source;

            (gridElement.Columns[3] as GridDateTimePickerColumn).Format = DateTimePickerFormat.Custom;
            (gridElement.Columns[3] as GridDateTimePickerColumn).CustomFormat = "d-m-Y";

        }
    }
}
