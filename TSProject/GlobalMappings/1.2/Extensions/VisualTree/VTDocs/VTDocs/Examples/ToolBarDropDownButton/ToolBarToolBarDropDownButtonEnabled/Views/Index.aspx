<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar ToolBarDropDownButton Enabled Property" ID="windowView1" LoadAction="ToolBarToolBarDropDownButtonEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the ToolBarDropDownButton can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the ToolBarDropDownButton can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:ToolBar runat="server" Text="ToolBar" Top="185px" Dock="None" ID="TestedToolBar1" CssClass="vt-test-toolb-1">
            <vt:ToolBarDropDownButton runat="server" id="ToolBar1Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="New" CssClass="vt-test-tbbutton-1-new"></vt:ToolBarDropDownButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarDropDownButton runat="server" id="ToolBar1Item2" Image="Content/Images/Open.png" Text="Open" CssClass="vt-test-tbbutton-1-open"></vt:ToolBarDropDownButton>
        </vt:ToolBar>           

        <vt:Label runat="server" SkinID="Log" Top="240px" Height="40px" Width="400px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of both tested ToolBarDropDownButtons is initially set to 'false'.
            You can change the Enabled value by clicking the 'Change Enabled Value' button" Top="375px" ID="lblExp1"></vt:Label> 

        <vt:ToolBar runat="server" Text="TestedToolBar" Top="445px" Dock="None" ID="TestedToolBar2" CssClass="vt-test-toolb-2">
            <vt:ToolBarDropDownButton runat="server" id="ToolBar2Item1" Image="Content/Images/New.png" Enabled ="false" DisplayStyle="Image" Text="New" CssClass="vt-test-tbbutton-2-new"></vt:ToolBarDropDownButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarDropDownButton runat="server" id="ToolBar2Item2" Image="Content/Images/Open.png" Enabled ="false" Text="Open" CssClass="vt-test-tbbutton-2-open"></vt:ToolBarDropDownButton>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="500px" Height="40px" Width="400px" ID="lblLog2"></vt:Label>

        <vt:ComboBox runat="server" Width="155px" CssClass="vt-cmbSelectItem" Text="Select Item" Top="585px" ID="cmbSelectItem" SelectedIndexChangedAction="ToolBarToolBarDropDownButtonEnabled\cmbSelectItem_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="New"></vt:ListItem>
                <vt:ListItem runat="server" Text="Open"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:CheckBox runat="server" Top="585px" Left="270px" ID="chkEnabled" Text="Enabled"></vt:CheckBox>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="585px" Left="430px" ID="btnChangeEnabled" Width="180px" ClickAction="ToolBarToolBarDropDownButtonEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
