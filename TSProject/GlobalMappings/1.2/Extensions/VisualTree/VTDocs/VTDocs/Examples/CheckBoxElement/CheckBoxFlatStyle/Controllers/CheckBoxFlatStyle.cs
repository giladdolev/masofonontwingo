using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxFlatStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeFlatStyle_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkCheckFlatStyle = this.GetVisualElementById<CheckBoxElement>("chkCheckFlatStyle");

            if (chkCheckFlatStyle.FlatStyle ==  FlatStyle.Flat)
            {
                chkCheckFlatStyle.FlatStyle = FlatStyle.Popup;
            }
            else if (chkCheckFlatStyle.FlatStyle == FlatStyle.Popup)
            {
                chkCheckFlatStyle.FlatStyle = FlatStyle.Standard;
            }
            else if (chkCheckFlatStyle.FlatStyle == FlatStyle.Standard)
            {
                chkCheckFlatStyle.FlatStyle = FlatStyle.System;
            }
            else if (chkCheckFlatStyle.FlatStyle == FlatStyle.System)
            {
                chkCheckFlatStyle.FlatStyle = FlatStyle.Flat;
            }
           
        }

    }
}