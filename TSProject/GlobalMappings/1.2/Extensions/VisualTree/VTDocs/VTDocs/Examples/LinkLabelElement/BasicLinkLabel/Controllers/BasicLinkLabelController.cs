using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicLinkLabelController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeLinkText_Click(object sender, EventArgs e)
        {
            LinkLabelElement lnklabel = this.GetVisualElementById<LinkLabelElement>("lnklabel");
            lnklabel.Text = "Server Time is: " + DateTime.Now.ToLongTimeString();
        }
        public void btnChangeLink_Click(object sender, EventArgs e)
        {
           LinkLabelElement lnklabel = this.GetVisualElementById<LinkLabelElement>("lnklabel");
           lnklabel.LinkData = "http://www.ynet.co.il/home/0,7340,L-8,00.html";

        }
        public void button3_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label3");
            label.BorderStyle = System.Web.VisualTree.BorderStyle.ShadowBox;

            LabelElement label4 = this.GetVisualElementById<LabelElement>("label4");
            label4.BorderStyle = System.Web.VisualTree.BorderStyle.Double;
        }

        private void btnAutoEllipsis_Click(object sender, EventArgs e)
        {
            LabelElement label1 = this.GetVisualElementById<LabelElement>("label5");
            if (label1.AutoEllipsis == false)
            {
                label1.AutoEllipsis = true;
            }
            else
            {
                label1.AutoEllipsis = false;
            }
        }

    }
}