using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabsTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabsText = this.GetVisualElementById<TabElement>("tabsText");

            if (tabsText.TabItems["tabPage1"].Text == "tabPage1")
            {
                tabsText.TabItems["tabPage1"].Text = "NewText";
                tabsText.TabItems["tabPage2"].Text = "NewText";
                textBox1.Text = "tabPage1: " + tabsText.TabItems["tabPage1"].Text.ToString();
                textBox1.Text += "\\r\\ntabPage2: " + tabsText.TabItems["tabPage2"].Text.ToString();
            }
            else
            {
                tabsText.TabItems["tabPage1"].Text = "tabPage1";
                tabsText.TabItems["tabPage2"].Text = "tabPage2";
                textBox1.Text = "tabPage1: " + tabsText.TabItems["tabPage1"].Text.ToString();
                textBox1.Text += "\\r\\ntabpage2: " + tabsText.TabItems["tabPage2"].Text.ToString();
            }
        }

    }
}