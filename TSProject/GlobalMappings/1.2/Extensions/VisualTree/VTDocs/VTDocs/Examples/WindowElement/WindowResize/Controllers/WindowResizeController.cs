using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowResizeController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return this.View(new WindowResize());
        }

        private WindowResize ViewModel
        {
            get { return this.GetRootVisualElement() as WindowResize; }
        }

        public void btn1_changeSize(object sender, EventArgs e)
        {
            this.ViewModel.Width = 300;
            
        }

        public void Window_Resize(object sender, ResizeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "Resize Event fired";
        }
    }
}

