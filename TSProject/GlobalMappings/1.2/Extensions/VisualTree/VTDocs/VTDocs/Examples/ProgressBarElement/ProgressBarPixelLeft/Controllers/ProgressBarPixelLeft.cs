using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelLeft value: " + TestedProgressBar.PixelLeft + '.';
        }

        public void btnChangePixelLeft_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedProgressBar.PixelLeft == 80)
            {
                TestedProgressBar.PixelLeft = 180;
                lblLog.Text = "PixelLeft value: " + TestedProgressBar.PixelLeft + '.';
            }
            else
            {
                TestedProgressBar.PixelLeft = 80;
                lblLog.Text = "PixelLeft value: " + TestedProgressBar.PixelLeft + '.';
            }
        }
    }
}