using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ErrorProviderSetErrorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Please enter Username and Password";
        }

        public void txtUsername_leave(object sender, EventArgs e)
        {
            ErrorProviderElement epUsername = this.GetVisualElementById<ErrorProviderElement>("epUsername");
            TextBoxElement txtUsername = this.GetVisualElementById<TextBoxElement>("txtUsername");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtUsername.Text == "")
            {
                epUsername.SetError(txtUsername, "Username can not be empty...");
                lblLog.Text = "SetError method is invoked on Username field.";
            }
            else
            {
                epUsername.Clear();
                lblLog.Text = "Entered Username:  " + txtUsername.Text.ToString();
            }

        }

        public void txtPassword_leave(object sender, EventArgs e)
        {
            ErrorProviderElement epPassword = this.GetVisualElementById<ErrorProviderElement>("epPassword");
            TextBoxElement txtPassword = this.GetVisualElementById<TextBoxElement>("txtPassword");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtPassword.Text == "")
            {
                epPassword.SetError(txtPassword, "Password can not be empty...");
                lblLog.Text = "SetError method is invoked on Password field.";
            }
            else
            {
                epPassword.Clear();
                lblLog.Text += "\\r\\nEntered Password:  " + txtPassword.Text.ToString();
            }

        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            ErrorProviderElement epUsername = this.GetVisualElementById<ErrorProviderElement>("epUsername");
            TextBoxElement txtUsername = this.GetVisualElementById<TextBoxElement>("txtUsername"); 
            ErrorProviderElement epPassword = this.GetVisualElementById<ErrorProviderElement>("epPassword");
            TextBoxElement txtPassword = this.GetVisualElementById<TextBoxElement>("txtPassword");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtUsername.Text == "")
            {
                epUsername.SetError(txtUsername, "Username can not be empty...");
                lblLog.Text = "SetError method is invoked on Username field.";
            }
            else
            {
                epUsername.SetError(txtUsername, null);
                lblLog.Text = "Entered Username:  " + txtUsername.Text.ToString();
            }

            if (txtPassword.Text == "")
            {
                epPassword.SetError(txtPassword, "Password can not be empty...");
                lblLog.Text += "\\r\\nSetError method is invoked on Password field.";
            }
            else
            {
                epPassword.SetError(txtPassword, null);
                lblLog.Text += "\\r\\nEntered Password:  " + txtPassword.Text.ToString();
            }

        }


        //public void btnGetError_Click(object sender, EventArgs e)
        //{
        //    ErrorProviderElement errorP = this.GetVisualElementById<ErrorProviderElement>("errorP");
        //    TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
        //    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

        //    lblLog.Text = "TextBox Error is  = " + errorP.GetError(TestedTextBox);
        //}

        //public void btnSetError_Click(object sender, EventArgs e)
        //{
        //    ErrorProviderElement errorP = this.GetVisualElementById<ErrorProviderElement>("errorP");
        //    TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
        //    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
        //    errorP.SetError(TestedTextBox, "control error");
        //    lblLog.Text = "TextBox Error is =" + errorP.GetError(TestedTextBox);
        //}

    }
}