
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem Index" ID="windowView2" LoadAction="TabTabItemIndex\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Index" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the index of an object in a collection. Read-only at run time.
 
            Syntax: public override int Index { get; }" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, use the 'Add Item' button to add a Tab and the 'Remove Item' button 
            to remove a Tab from the end of the list. You can select a Tab with the mouse 
            to see it's index value" Top="230px" ID="lblExp2"></vt:Label>

        <vt:Tab runat="server" Text="Tab" Top="320px" Width="400px" ID="TestedTab1" SelectedIndexChangedAction="TabTabItemIndex\TestedTab1_SelectedIndexChanged">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="Tab1Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Width="400px" ID="lblLog" Top="435"></vt:Label>

        <vt:Button runat="server" Text="Remove Item >>" Top="510px" Left="300px" ID="btnRemoveItem" ClickAction="TabTabItemIndex\RemoveItems_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Item >>" Top="510px"  ID="btnAddItem" ClickAction="TabTabItemIndex\AddItems_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
