<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox ResetText Method" ID="windowView1" LoadAction="TextBoxResetText\OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="ResetText" ID="lblTitle"  >
        </vt:Label>

        <vt:Label runat="server" Text="Resets the Text property to its default value.

            Syntax: public virtual void ResetText()"
            Top="75px" ID="lblDefinition">
        </vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the TextBox Text property is initially set with some text. 
            When invoking ResetText(), the Text property returns to its default value - empty string." Top="250px" ID="lblExp2">
        </vt:Label>

        <vt:TextBox runat="server" Text="TestedTextBox" Top="320px" ID="TestedTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="360"></vt:Label>

        <vt:Button runat="server" Text="ResetText >>" Width="180px" Top="420px" ID="btnResetText" ClickAction="TextBoxResetText\btnResetText_Click"></vt:Button>

        <vt:Button runat="server" Text="Set Text >>" Width="180px" Left="300" Top="420px" ID="btnSetText" ClickAction="TextBoxResetText\btnSetText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
