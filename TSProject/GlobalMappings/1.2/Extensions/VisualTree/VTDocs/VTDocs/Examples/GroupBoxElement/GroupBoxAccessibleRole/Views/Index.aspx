<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox AccessibleRole Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="AccessibleRole is initially set to 'ToolBar'" Top="25px" Left="50px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="50px" AccessibleRole="ToolBar" Left="50px" ID="grpAccessibleRole" Height="60px" Width="200px"></vt:GroupBox>           

        <vt:Button runat="server" Text="Change AccessibleRole" Top="60px" Left="350px" ID="btnChangeAccessibleRole" Height="36px" TabIndex="1" Width="200px" ClickAction="GroupBoxAccessibleRole\btnChangeAccessibleRole_Click"></vt:Button>

        <vt:Button runat="server" Text="Get AccessibleRole" Top="100px" Left="350px" ID="btnGetAccessibleRole" Height="36px" TabIndex="1" Width="200px" ClickAction="GroupBoxAccessibleRole\btnGetAccessibleRole_Click"></vt:Button>

        <vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="160px" Left="50px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="200px">
		</vt:TextBox>

    </vt:WindowView>
</asp:Content>
