using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splRuntimeDraggable = this.GetVisualElementById<SplitContainer>("splRuntimeDraggable");

            if (splRuntimeDraggable.Draggable == false)
            {
                splRuntimeDraggable.Draggable = true;
                textBox1.Text = splRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                splRuntimeDraggable.Draggable = false;
                textBox1.Text = splRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}