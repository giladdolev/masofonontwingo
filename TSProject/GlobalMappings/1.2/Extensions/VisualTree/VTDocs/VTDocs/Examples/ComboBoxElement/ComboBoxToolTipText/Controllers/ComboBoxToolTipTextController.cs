using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxToolTipTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");
            cmb1.Items.Add("aaa");
            cmb1.Items.Add("bbb");

            cmb1.ToolTipText = "ComboBox for run time";
        }

    }
}