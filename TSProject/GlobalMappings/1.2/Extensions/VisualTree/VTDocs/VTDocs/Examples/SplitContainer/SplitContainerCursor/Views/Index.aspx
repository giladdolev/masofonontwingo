<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Cursor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent - Cursor is set to .. bug" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" ></vt:Label>     

        <vt:SplitContainer runat="server" Text="" Cursor="System.Web.VisualTree.Elements.CursorElement" Top="45px" Left="140px" ID="splCursor" Height="70px" Width="200px"></vt:SplitContainer>           

        <vt:Label runat="server" Text="SplitContainer Cursor - runtime" Top="150px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" ></vt:Label>

        <vt:SplitContainer runat="server" Text="" Top="165px" Left="140px" ID="splRuntimeCursor" Height="70px" TabIndex="1" Width="200px"></vt:SplitContainer>

        <vt:Button runat="server" Text="Change SplitContainer Cursor" Top="185px" Left="400px" ID="btnChangeCursor" Height="36px" TabIndex="1" Width="200px" ClickAction="SplitContainerCursor\btnChangeCursor_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="260px" Left="110px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
