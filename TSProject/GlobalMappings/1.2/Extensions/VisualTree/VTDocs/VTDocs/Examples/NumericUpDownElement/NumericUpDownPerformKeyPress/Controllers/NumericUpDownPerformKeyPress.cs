using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownPerformKeyPressController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformKeyPress_Click(object sender, EventArgs e)
        {
            Keys keys = new Keys();
            KeyPressEventArgs args = new KeyPressEventArgs(keys);
            NumericUpDownElement testedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("testedNumericUpDown");

            testedNumericUpDown.PerformKeyPress(args);
        }

        public void testedNumericUpDown_KeyPress(object sender, EventArgs e)
        {
            MessageBox.Show("TestedNumericUpDown KeyPress event method is invoked");
        }

    }
}