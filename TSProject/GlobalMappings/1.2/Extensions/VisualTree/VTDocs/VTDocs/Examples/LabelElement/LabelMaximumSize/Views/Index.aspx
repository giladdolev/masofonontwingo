<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label MaximumSize Property" ID="windowView" LoadAction="LabelMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedLabel' is width: 200px and height: 90px. 
            Its MaximumSize is initially set to width: 150px and height: 70px.
            To cancel 'TestedLabel' MaximumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label>     

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" ID="TestedLabel" Width="200px" Height="90px" BorderStyle="Solid" Left="80px" MaximumSize="150, 70" Top="370px"></vt:Label> 

        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog" Width="400px" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="510px" Width="200px" ID="btnChangeLblMaximumSize" ClickAction="LabelMaximumSize\btnChangeLblMaximumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="510px" width="200px" Left="310px" ID="btnSetToZeroLblMaximumSize" ClickAction="LabelMaximumSize\btnSetToZeroLblMaximumSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>









