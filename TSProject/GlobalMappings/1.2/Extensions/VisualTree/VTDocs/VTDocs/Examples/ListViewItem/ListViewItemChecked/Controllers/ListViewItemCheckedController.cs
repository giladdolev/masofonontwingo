using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewItemCheckedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "First Item Checked value: " + TestedListView1.Items[0].Checked + "\\r\\nSecond Item Checked value: " + TestedListView1.Items[1].Checked + "\\r\\nThird Item Checked value: " + TestedListView1.Items[2].Checked;

            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "First Item Checked value: " + TestedListView2.Items[0].Checked + "\\r\\nSecond Item Checked value: " + TestedListView2.Items[1].Checked + "\\r\\nThird Item Checked value: " + TestedListView2.Items[2].Checked;

        }

        public void btnChangeItem2Checked_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

         
            TestedListView2.Items[1].Checked = !TestedListView2.Items[1].Checked;
            lblLog2.Text = "First Item Checked value: " + TestedListView2.Items[0].Checked + "\\r\\nSecond Item Checked value: " + TestedListView2.Items[1].Checked + "\\r\\nThird Item Checked value: " + TestedListView2.Items[2].Checked;

            
        }

        public void TestedListView2_ItemCheck(object sender, EventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            lblLog2.Text = "First Item Checked value: " + TestedListView2.Items[0].Checked + "\\r\\nSecond Item Checked value: " + TestedListView2.Items[1].Checked + "\\r\\nThird Item Checked value: " + TestedListView2.Items[2].Checked;


        }
    }
}