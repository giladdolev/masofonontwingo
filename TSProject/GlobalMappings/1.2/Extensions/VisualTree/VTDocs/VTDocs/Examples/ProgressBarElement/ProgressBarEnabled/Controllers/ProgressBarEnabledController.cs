using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar1 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedProgressBar1.Enabled.ToString();

            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedProgressBar2.Enabled.ToString() + ".";
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedProgressBar2.Enabled = !TestedProgressBar2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedProgressBar2.Enabled.ToString() + ".";
        }
    }
}