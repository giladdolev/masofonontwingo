using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "Enabled value: " + TestedGrid1.Enabled.ToString();

            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "Enabled value: " + TestedGrid2.Enabled.ToString() + ".";
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGrid2.Enabled = !TestedGrid2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedGrid2.Enabled.ToString() + ".";
        }
    }
}