using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        public void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement rdoRuntimeRightToLeft = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeRightToLeft");

            if (rdoRuntimeRightToLeft.RightToLeft == RightToLeft.Inherit) //Get
            {
                rdoRuntimeRightToLeft.RightToLeft = RightToLeft.No; //Set
                textBox1.Text = "The RightToLeft property value is: " + rdoRuntimeRightToLeft.RightToLeft;
            }
            else if (rdoRuntimeRightToLeft.RightToLeft == RightToLeft.No)
            {
                rdoRuntimeRightToLeft.RightToLeft = RightToLeft.Yes; 
                textBox1.Text = "The RightToLeft property value is: " + rdoRuntimeRightToLeft.RightToLeft;
            }
            else  
            {
                rdoRuntimeRightToLeft.RightToLeft = RightToLeft.Inherit; 
                textBox1.Text = "The RightToLeft property value is: " + rdoRuntimeRightToLeft.RightToLeft;
            }
             
        }

    }
}