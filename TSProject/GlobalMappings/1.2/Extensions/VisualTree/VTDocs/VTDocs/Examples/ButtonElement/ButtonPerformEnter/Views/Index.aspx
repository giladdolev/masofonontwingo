<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button PerformEnter() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformEnter of 'Tested Button'" Top="45px" Left="140px" ID="btnPerformEnter" Height="36px" Width="220px" ClickAction="ButtonPerformEnter\btnPerformEnter_Click"></vt:Button>


        <vt:Button runat="server" Text="Tested Button" Top="150px" Left="200px" ID="btnTestedButton" Height="36px"  Width="100px" EnterAction="ButtonPerformEnter\btnTestedButton_Enter"></vt:Button>           

        

    </vt:WindowView>
</asp:Content>
