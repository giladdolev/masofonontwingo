using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPerformEnterController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformEnter_Click(object sender, EventArgs e)
        {
            
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");

            testedRichTextBox.PerformEnter(e);
        }

        public void testedRichTextBox_Enter(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRichTextBox Enter event method is invoked");
        }

    }
}