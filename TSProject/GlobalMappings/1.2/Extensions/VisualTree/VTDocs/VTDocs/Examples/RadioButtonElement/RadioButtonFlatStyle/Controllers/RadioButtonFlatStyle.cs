using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonFlatStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeFlatStyle_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoCheckFlatStyle = this.GetVisualElementById<RadioButtonElement>("rdoCheckFlatStyle");

            if (rdoCheckFlatStyle.FlatStyle ==  FlatStyle.Flat)
            {
                rdoCheckFlatStyle.FlatStyle = FlatStyle.Popup;
            }
            else if (rdoCheckFlatStyle.FlatStyle == FlatStyle.Popup)
            {
                rdoCheckFlatStyle.FlatStyle = FlatStyle.Standard;
            }
            else if (rdoCheckFlatStyle.FlatStyle == FlatStyle.Standard)
            {
                rdoCheckFlatStyle.FlatStyle = FlatStyle.System;
            }
            else if (rdoCheckFlatStyle.FlatStyle == FlatStyle.System)
            {
                rdoCheckFlatStyle.FlatStyle = FlatStyle.Flat;
            }
           
        }

    }
}