<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid SelectedText property"  ID="windowView1" LoadAction="ComboGridSelectedText/OnLoad">
        <vt:Label runat="server" SkinID="Title" Text="SelectedText" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the selected text.
            
            public string SelectedText { get; set; }
            Property value: A string value representing the selected text.
            
            Remarks:
            Read the SelectedText property value to obtain the currently selected text. If no text is presently selected,
            the property returns an empty string. Assign a string to the SelectedText property to replace the currently
            selected text with the string specified. If no text is selected presently, assigning a value will insert the
            specified text to the current caret position. Note that changing the SelectedText property value removes 
            the selection.
            
            To select text via code, use the SelectionStart and SelectionLength properties." Top="75px"  ID="lblDefinition"></vt:Label>
              
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In ComboGrid controls by default all text is selected. Hover with the mouse on buttons
            below to try the Get and Set of the SelectedText property of this ComboGrid." Top="410px"  ID="lblExp1"></vt:Label>     
        
        <vt:ComboGrid runat="server" ID="TestedComboGrid" Text="TestedComboGrid" Top="475px"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="515px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Set SelectedText value >>" Width="180px" Top="590px" ID="btnSetSelectedText" ClickAction="ComboGridSelectedText\btnSetSelectedText_Click" MouseHoverAction="ComboGridSelectedText\btnSetSelectedText_Click"></vt:Button>

          <vt:Button runat="server" Text="Get SelectedText value >>" Left="300px" Width="180px" Top="590px" ID="btnGetSelectedText" ClickAction="ComboGridSelectedText\btnGetSelectedText_Click" MouseHoverAction="ComboGridSelectedText\btnGetSelectedText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
