<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox AutoCheck Property" ID="windowView1" LoadAction="CheckBoxAutoCheck\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="AutoCheck" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the Checked or CheckState value and the CheckBox's
              appearance are automatically changed when the CheckBox is clicked. 

            Syntax: public bool AutoCheck { get; set; }

            The default value is true."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:CheckBox runat="server" Text="CheckBox" Top="200px" ID="TestedCheckBox1"></vt:CheckBox>
       
        <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" Text="When the AutoCheck is set to false the CheckBox cannot be checked or unchecked"
            Top="270px" ID="lblExp1">
        </vt:Label>
   


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="AutoCheck value is set to 'false'
            You should not be able to check or uncheck the CheckBox." Top="370px" ID="lblExp2">
        </vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox" AutoCheck="false" Top="440px" ID="TestedCheckBox2"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Top="480px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change AutoCheck value >>" Width="180px" Top="530px" ID="btnChangeAutoCheck" ClickAction="CheckBoxAutoCheck\btnChangeAutoCheck_Click"></vt:Button>



    </vt:WindowView>
</asp:Content>
