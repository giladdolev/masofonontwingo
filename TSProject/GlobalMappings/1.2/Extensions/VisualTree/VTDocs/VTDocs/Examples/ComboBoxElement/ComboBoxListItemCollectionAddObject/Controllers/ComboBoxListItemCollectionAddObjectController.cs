using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxListItemCollectionAddObjectController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press the Add button...";
        }

        private void AddItems_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            ListItem newitem = new ListItem("New Item");
            TestedComboBox.Items.Add(newitem);

            lblLog.Text = "New Item was added to the ComboBox, Items Count value: " + TestedComboBox.Items.Count + '.';

        }

        // Bug 8719 - Transposition to MVC -function removeAt in comboBox doesn't work
        //public void RemoveItems_Click(object sender, EventArgs e)
        //{
        //    ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
        //    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

        //    if (TestedComboBox.Items.Count > 0)
        //    {
        //        TestedComboBox.Items.RemoveAt(TestedComboBox.Items.Count - 1);

        //        lblLog.Text = "An Item was removed from the ComboBox.";
        //    }
        //    else
        //    {
        //        lblLog.Text = "ComboBox is empty.";
        //    }

        //}
    }
}