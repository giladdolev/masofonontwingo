<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox CheckStateChanged event" ID="windowView1" LoadAction="CheckBoxCheckStateChanged\OnLoad" >
      

        <vt:Label runat="server" SkinID="Title" Text="CheckStateChanged" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the value of the CheckState property changes.

            Syntax: public event EventHandler CheckStateChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>
   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the CheckState property by
             clicking the 'Change CheckState' button or by manually checking or unchecking the TestedCheckBox.
            Each invocation of the 'CheckStateChanged' event should add a line in the 'Event Log'." Top="250px" ID="lblExp2">
        </vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox" ThreeState="true" Top="320px" ID="TestedCheckBox" CheckStateChangedAction="CheckBoxCheckStateChanged\TestedCheckBox_CheckStateChanged"></vt:CheckBox>

        
        <vt:Label runat="server" SkinID="Log" Left="80px" Top="360px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="400px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change CheckState >>" Width="180px" Top="550px" ID="btnChangeCheckState" ClickAction="CheckBoxCheckStateChanged\btnChangeCheckState_Click"></vt:Button>



    </vt:WindowView>
</asp:Content>
