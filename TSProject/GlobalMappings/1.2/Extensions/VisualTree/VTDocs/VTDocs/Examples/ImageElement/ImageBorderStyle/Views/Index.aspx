<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image BorderStyle Property" ID="windowView1" LoadAction="ImageBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is None.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Image runat="server" Text="Image" Top="170px" ID="TestedImage1"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Top="275px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this Image is initially set to 'Dotted'" Top="380px"  ID="lblExp1"></vt:Label>     
        
        <vt:Image runat="server"  Text="TestedImage" BorderStyle="Dotted" Top="420px" ID="TestedImage2"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Top="520px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="575px" ID="btnChangeImageBorderStyle" ClickAction="ImageBorderStyle\btnChangeImageBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
