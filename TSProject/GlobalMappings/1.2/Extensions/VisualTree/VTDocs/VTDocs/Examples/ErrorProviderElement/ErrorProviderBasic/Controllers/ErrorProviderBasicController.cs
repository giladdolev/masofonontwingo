using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ErrorProviderBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbGender = this.GetVisualElementById<ComboBoxElement>("cmbGender");
            
            lblLog.Text = "Please fill all fields.";
            cmbGender.Items.Add("Male");
            cmbGender.Items.Add("Female");
        }

        public void txtUsername_leave(object sender, EventArgs e)
        {
            ErrorProviderElement epUsername = this.GetVisualElementById<ErrorProviderElement>("epUsername");
            TextBoxElement txtUsername = this.GetVisualElementById<TextBoxElement>("txtUsername");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtUsername.Text == "")
            {
                epUsername.SetError(txtUsername, "Username can not be empty...");
                lblLog.Text = "Error description: " + epUsername.GetError(txtUsername);
            }
            else
            {
                epUsername.Clear();
                //epUsername.SetError(txtUsername, "");
                lblLog.Text = "Entered Username:  " + txtUsername.Text.ToString();
            }

        }

        public void txtPassword_leave(object sender, EventArgs e)
        {
            ErrorProviderElement epPassword = this.GetVisualElementById<ErrorProviderElement>("epPassword");
            TextBoxElement txtPassword = this.GetVisualElementById<TextBoxElement>("txtPassword");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtPassword.Text == "")
            {
                epPassword.SetError(txtPassword, "Password can not be empty...");
                lblLog.Text = "Error description: " + epPassword.GetError(txtPassword);
            }
            else
            {
                epPassword.Clear();
                //epPassword.SetError(txtPassword, "");
                lblLog.Text = "Entered Password:  " + txtPassword.Text.ToString();
            }

        }

            public void cmbGender_leave(object sender, EventArgs e)
        {
            ErrorProviderElement epGender = this.GetVisualElementById<ErrorProviderElement>("epGender");
            ComboBoxElement cmbGender = this.GetVisualElementById<ComboBoxElement>("cmbGender");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (cmbGender.Text == "")
            {
                epGender.SetError(cmbGender, "Gender can not be empty...");
                lblLog.Text = "Error description: " + epGender.GetError(cmbGender);
            }
            else
            {
                epGender.Clear();
                //epGender.SetError(cmbGender, "");
                lblLog.Text = "Entered gender:  " + cmbGender.Text.ToString();
            }

        }

            public void RichtbAddress_leave(object sender, EventArgs e)
            {
                ErrorProviderElement epAddress = this.GetVisualElementById<ErrorProviderElement>("epAddress");
                RichTextBox RichtbAddress = this.GetVisualElementById<RichTextBox>("RichtbAddress");
                LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

                if (RichtbAddress.Text == "")
                {
                    epAddress.SetError(RichtbAddress, "Address can not be empty...");
                    lblLog.Text = "Error description: " + epAddress.GetError(RichtbAddress);
                }
                else
                {
                    epAddress.Clear();
                    //epAddress.SetError(RichtbAddress, "");
                    lblLog.Text = "Entered address:  " + RichtbAddress.Text.ToString();
                }

            }
        public void btnLogin_Click(object sender, EventArgs e)
        {
            ErrorProviderElement epUsername = this.GetVisualElementById<ErrorProviderElement>("epUsername");
            TextBoxElement txtUsername = this.GetVisualElementById<TextBoxElement>("txtUsername");
            ErrorProviderElement epPassword = this.GetVisualElementById<ErrorProviderElement>("epPassword");
            TextBoxElement txtPassword = this.GetVisualElementById<TextBoxElement>("txtPassword");
            ErrorProviderElement epGender = this.GetVisualElementById<ErrorProviderElement>("epGender");
            ComboBoxElement cmbGender = this.GetVisualElementById<ComboBoxElement>("cmbGender");
            ErrorProviderElement epAddress = this.GetVisualElementById<ErrorProviderElement>("epAddress");
            RichTextBox RichtbAddress = this.GetVisualElementById<RichTextBox>("RichtbAddress");
                
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtUsername.Text == "")
            {
                epUsername.SetError(txtUsername, "Username can not be empty...");
                lblLog.Text = "Error description: " + epUsername.GetError(txtUsername);
            }
            else
            {
                //epUsername.SetError(txtUsername, "");
                epUsername.Clear();
                lblLog.Text = "Entered Username:  " + txtUsername.Text.ToString();
            }

            if (txtPassword.Text == "")
            {
                epPassword.SetError(txtPassword, "Password can not be empty...");
                lblLog.Text += "\\r\\nError description: " + epPassword.GetError(txtPassword);
            }
            else
            {
                //epPassword.SetError(txtPassword, "");
                epPassword.Clear();
                lblLog.Text += "\\r\\nEntered Password:  " + txtPassword.Text.ToString();
            }

            //if (cmbGender.Text == "")
            //{
            //    epGender.SetError(cmbGender, "Gender can not be empty...");
            //    lblLog.Text += "\\r\\n\\r\\nError description: " + epGender.GetError(cmbGender);
            //}
            //else
            //{
            //    //epGender.SetError(cmbGender, null);
            //    epGender.Clear();
            //    lblLog.Text += "\\r\\n\\r\\nEntered gender:  " + cmbGender.Text.ToString();
            //}

            if (RichtbAddress.Text == "")
            {
                epAddress.SetError(RichtbAddress, "Address can not be empty...");
                lblLog.Text += "\\r\\n\\r\\n\\r\\nError description: " + epAddress.GetError(RichtbAddress);
            }
            else
            {
                //epAddress.SetError(RichtbAddress, null);
                epAddress.Clear();
                lblLog.Text += "\\r\\n\\r\\n\\r\\nEntered address:  " + RichtbAddress.Text.ToString();
            }

        }
      
    }
}