<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar SetBounds() Method" ID="windowView" LoadAction="ProgressBarSetBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SetBounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the bounds of the control to the specified location and size.

            Syntax: public void SetBounds(int left, int top, int width, int height)" Top="75px" ID="Label1" ></vt:Label>
                 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="SetBounds Method of this 'TestedProgressBar' is initially set with 
            Left: 80px, Top: 300px, Width: 250px, Height: 40px" Top="235px"  ID="lblExp1"></vt:Label>     
        
        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="300px" ID="TestedProgressBar"></vt:ProgressBar>
        <vt:Label runat="server" SkinID="Log" Top="355px" Height="40" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="SetBounds >>"  Top="460px" Width="180px" ID="btnSetBounds" ClickAction="ProgressBarSetBounds\btnSetBounds_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

