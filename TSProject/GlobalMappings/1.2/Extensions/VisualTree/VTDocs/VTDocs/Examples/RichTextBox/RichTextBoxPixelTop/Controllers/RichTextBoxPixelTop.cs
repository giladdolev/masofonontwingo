using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelTop value: " + TestedRichTextBox.PixelTop + '.';
        }

        public void btnChangePixelTop_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRichTextBox.PixelTop == 310)
            {
                TestedRichTextBox.PixelTop = 290;
                lblLog.Text = "PixelTop value: " + TestedRichTextBox.PixelTop + '.';
            }
            else
            {
                TestedRichTextBox.PixelTop = 310;
                lblLog.Text = "PixelTop value: " + TestedRichTextBox.PixelTop + '.';
            }

        }
    }
}