<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox SelectionLength Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="SelectionLength Property" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="240px"></vt:Label>

        <vt:TextBox runat="server" Text="Hello World" Top="125px" Left="140px" ID="txtSelectionLength" Height="26px"  Width="150px"></vt:TextBox>

        <vt:Button runat="server" Text="Set" Top="115px" Left="320px" ID="btnSetSelectionLength" Height="36px"  Width="60px" ClickAction="TextBoxSelectionLength\btnSetSelectionLength_Click"></vt:Button>

        <vt:Button runat="server" Text="Get" Top="115px" Left="400px" ID="btnGetSelectionLength" Height="36px"  Width="60px" ClickAction="TextBoxSelectionLength\btnGetSelectionLength_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
