﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;


namespace  MvcApplication9.Controllers
{
    public class ButtonToolTipTextController : Controller
    {
        // GET: ToolTipText
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton1 = this.GetVisualElementById<ButtonElement>("TestedButton1");
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "TestedButton1 ToolTipText value: " + TestedButton1.ToolTipText;
            lblLog.Text += "\\r\\nTestedButton2 ToolTipText value: " + TestedButton2.ToolTipText;
        }

        public void btnChangeToolTipText_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton1 = this.GetVisualElementById<ButtonElement>("TestedButton1");
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedButton1.ToolTipText == "MyToolTip1")
            {
                TestedButton1.ToolTipText = "NewToolTip1";
            }
            else
            {
                TestedButton1.ToolTipText = "MyToolTip1";
            }

            if (TestedButton2.ToolTipText == "MyToolTip2")
            {
                TestedButton2.ToolTipText = "NewToolTip2";
            }
            else
            {
                TestedButton2.ToolTipText = "MyToolTip2";
            }

            lblLog.Text = "TestedButton1 ToolTipText value: " + TestedButton1.ToolTipText;
            lblLog.Text += "\\r\\nTestedButton2 ToolTipText value: " + TestedButton2.ToolTipText;
        }
    }

}