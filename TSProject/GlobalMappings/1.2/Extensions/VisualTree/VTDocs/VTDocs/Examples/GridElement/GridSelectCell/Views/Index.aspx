﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server"  Text="Grid SelectCell method" ID="Form1" LoadAction="GridSelectCell\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectCell" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Select a cell by Row index and Column index.
            
            public void SelectCell(int row, int col)" Top="75px"  ID="lblDefinition"></vt:Label>
        
  
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text=" In the following example, clicking on 'Select Cell (0,1)' button will invoke the SelectCell method 
            on row index 0 and column index 1." Top="205px"  ID="lblExp1"></vt:Label>     
              
        <vt:Grid runat="server" Top="265px" ID="TestedGrid" SelectionChangedAction="GridSelectCell\TestedGrid_SelectionChanged">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Text="" Top="390px" ID="lblLog"></vt:Label>

         <vt:Button runat="server"  Text="Select Cell (0,1) >>" Top="440px" ID="button2" ClickAction="GridSelectCell\SelectCell_Click" >
        </vt:Button>
               
    </vt:WindowView>

</asp:Content>

<%--<vt:Button runat="server" Text="Click Me!" Top="330px"  ID="button1" ClickAction="GridSelectCell\button1_Click"> </vt:Button>--%>
