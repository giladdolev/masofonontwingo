﻿ <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="27367" ID="windowView2" LoadAction="GridGridColumnVisible1WrongIndexes27367QA\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Left="70px" Text="Testing Grid with Non-Visible columns has wrong indexes" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Test:" Top="70px" ID="lblTest"></vt:Label>

        <vt:Label runat="server" Text="Bug description: When the Grid has a Non-Visible Column0, When we Check/UnCheck Column2 
            the value is updated on Column1, because the column indexes are wrong.
            
            In the following example, Column0.Visible is initially set to 'false'.
            Check/UnCheck Column2 Cells and view the values changes on Column1. 
            
            Use the button below to change the grid column0 visibility" 
            Top="120px" ID="lblDefinition"></vt:Label>

        <vt:Grid runat="server" Top="300px" ID="TestedGrid" Height="150px"></vt:Grid>

        <vt:Label runat="server" SkinID="Log" Width="350px"  ID="lblLog" Top="470px"></vt:Label>

        <vt:Button runat="server" Text="Change Column0 Visibility >>" Width="180px" Top="530px" ID="btnChangeCol0Visibility" ClickAction="GridGridColumnVisible1WrongIndexes27367QA\btnChangeCol0Visibility_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>




