<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Cursor property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxCursor/Form_Load">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="190" Left="150px" Height="50" Width="200"/>
        <vt:Button runat="server" Text="Change the cursor of the comboBox" Top="112px" Left="130px" ID="btnCursor" TabIndex="1" Width="300px" ClickAction="ComboBoxCursor\btnCursor_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
