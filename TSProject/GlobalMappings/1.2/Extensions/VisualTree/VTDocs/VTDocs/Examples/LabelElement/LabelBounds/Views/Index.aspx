<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Bounds Property" ID="windowView" LoadAction="LabelBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Bounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size and location of the control including its nonclient elements, 
            in pixels, relative to the parent control.

            Syntax: public Rectangle Bounds { get; set; }
            
            The bounds of the control include the nonclient elements such as scroll bars, 
            borders, title bars, and menus." Top="75px" ID="Label3" ></vt:Label>
                 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The bounds of this 'TestedLabel' is initially set with 
            Left: 80px, Top: 380px, Width: 150px, Height: 36px" Top="290px"  ID="lblExp1"></vt:Label>     

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Top="380px" Width="150px" Height="36px" ID="TestedLabel"></vt:Label>
        <vt:Label runat="server" SkinID="Log" Top="430px" Height="40" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Bounds value >>" Top="510px" Width="180px" ID="btnChangeLabelBounds" ClickAction="LabelBounds\btnChangeLabelBounds_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>