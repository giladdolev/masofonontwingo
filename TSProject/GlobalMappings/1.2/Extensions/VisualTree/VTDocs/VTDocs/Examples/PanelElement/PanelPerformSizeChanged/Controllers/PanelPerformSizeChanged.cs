using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformSizeChanged_Click(object sender, EventArgs e)
        {
            
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");
            
            MouseEventArgs args = new MouseEventArgs();

            testedPanel.PerformSizeChanged(args);
        }

        public void testedPanel_SizeChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TestedPanel SizeChanged event method is invoked");
        }

    }
}