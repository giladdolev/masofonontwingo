﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Grid GridComboBoxColumn"  Top="57px" ID="windowView1"  LoadAction="BasicGridGridComboBoxColumn\OnLoad">
       
         <vt:Label runat="server" SkinID="Title" Text="GridComboBoxColumn" Left="250px" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Represents a column of combo box in a Grid control"
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example shows basic usage of GridComboBoxColumn class;
            Use the buttons bellow to add or remove Combo Box columns with items from Grid." Top="205px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Text="Grid" Height="130px" Top="280px" Width ="500px" ID="TestedGrid" CellEndEditAction="BasicGridGridComboBoxColumn\cellEndEdit">
            <Columns>
                <vt:GridComboBoxColumn  runat="server" ID="col1" HeaderText="Column1" Height="20px" Width="75px" ></vt:GridComboBoxColumn>
                <vt:GridComboBoxColumn  runat="server" ID="col2" HeaderText="Column2" Height="20px" Width="75px"></vt:GridComboBoxColumn>
                <vt:GridComboBoxColumn  runat="server" ID="col3" HeaderText="Column3" Height="20px" Width="75px"></vt:GridComboBoxColumn>
            </Columns>
        </vt:Grid>

         <vt:Label runat="server" SkinID="Log" Top="425px" ID="lblLog" Text ="Click the buttons.."></vt:Label>
        
        <vt:Button runat="server" Text="Add Combo Box Column >>" Width="200px" Top="490px" ID="btnAddComboBoxColumn" ClickAction="BasicGridGridComboBoxColumn\btnAddComboBoxColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Remove Combo Box Column >>" Width="200px" Left="340px" Top="490px" ID="btnRemoveLastComboBoxColumn" ClickAction="BasicGridGridComboBoxColumn\btnRemoveLastComboBoxColumn_Click"></vt:Button>
        

    </vt:WindowView>
</asp:Content>
