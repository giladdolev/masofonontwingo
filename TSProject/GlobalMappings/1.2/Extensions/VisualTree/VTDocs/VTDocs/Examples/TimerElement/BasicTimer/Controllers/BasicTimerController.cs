using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicTimerController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        

        public ActionResult Index()
        {
            return View();
        }

        public void btnTxtCleaner1_Click(object sender, EventArgs e)
        {
            TextBoxElement txtOutput = this.GetVisualElementById<TextBoxElement>("textBox1");
            txtOutput.Text = String.Empty;
        }

        public void btnTxtCleaner2_Click(object sender, EventArgs e)
        {
            TextBoxElement txtOutput = this.GetVisualElementById<TextBoxElement>("textBox2");
            txtOutput.Text = String.Empty;

        }
        public void btnTimerToggle1_Click(object sender, EventArgs e)
        {
            ButtonElement btnTimerToggle = this.GetVisualElementById<ButtonElement>("btnToggleTimer1");
            TimerElement exampleTimer = this.GetVisualElementById<TimerElement>("timer1");
            TextBoxElement txtOutput = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (exampleTimer.Enabled == false)
            {
                btnTimerToggle.Text = "Stop timer 1";
                exampleTimer.Enabled = true;
                txtOutput.Text = String.Format("{0}\nTimer started", txtOutput.Text);
            }
            else
            {
                btnTimerToggle.Text = "Start timer 1";
                exampleTimer.Enabled = false;
                txtOutput.Text = String.Format("{0}\nTimer stopped", txtOutput.Text);
            }

        }

        public void btnTimerToggle2_Click(object sender, EventArgs e)
        {
            ButtonElement btnTimerToggle = this.GetVisualElementById<ButtonElement>("btnToggleTimer2");
            TimerElement exampleTimer = this.GetVisualElementById<TimerElement>("timer2");
            TextBoxElement txtOutput = this.GetVisualElementById<TextBoxElement>("textBox2");

            if (exampleTimer.Enabled == false)
            {
                btnTimerToggle.Text = "Stop timer 2";
                exampleTimer.Enabled = true;
                txtOutput.Text = String.Format("{0}\nTimer started", txtOutput.Text);
            }
            else
            {
                btnTimerToggle.Text = "Start timer 2";
                exampleTimer.Enabled = false;
                txtOutput.Text = String.Format("{0}\nTimer stopped", txtOutput.Text);
            }

        }

        /// <summary>
        /// Handles the Tick event of the Timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        /// <returns></returns>
        public void timer1_Tick(object sender, EventArgs e)
        {
            TextBoxElement txtOutput = this.GetVisualElementById<TextBoxElement>("textBox1");
            txtOutput.Text = String.Format("{0}\nTimer message: {1}", txtOutput.Text, DateTime.Now.ToLongTimeString());
        }

        public void timer2_Tick(object sender, EventArgs e)
        {
            TextBoxElement txtOutput = this.GetVisualElementById<TextBoxElement>("textBox2");
            txtOutput.Text = String.Format("{0}\nTimer message: {1}", txtOutput.Text, DateTime.Now.ToLongTimeString());
        }
        public void WindowClosed(object sender, EventArgs e)
        {
            TimerElement timer1 = this.GetVisualElementById<TimerElement>("timer1");
            TimerElement timer2 = this.GetVisualElementById<TimerElement>("timer2");

            timer1.Stop();
            timer2.Stop();
        }
        
    }
}