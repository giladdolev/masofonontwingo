<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton PerformKeyDown() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformKeyDown of 'Tested RadioButton'" Top="45px" Left="140px" ID="btnPerformKeyDown" Height="36px" Width="300px" ClickAction="RadioButtonPerformKeyDown\btnPerformKeyDown_Click"></vt:Button>


        <vt:RadioButton runat="server" Text="Tested RadioButton" Top="150px" Left="200px" ID="testedRadioButton" Height="36px"  Width="200px" KeyDownAction="RadioButtonPerformKeyDown\testedRadioButton_KeyDown"></vt:RadioButton>           

        

    </vt:WindowView>
</asp:Content>
