using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeBorderStyle_Click(object sender, EventArgs e)
        {
            //textBox1
            CheckBoxElement chkBorderStyle = this.GetVisualElementById<CheckBoxElement>("chkBorderStyle");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (chkBorderStyle.BorderStyle == BorderStyle.None)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Dotted;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Dotted)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Double;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Double)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Fixed3D;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Fixed3D)
            {
                chkBorderStyle.BorderStyle = BorderStyle.FixedSingle;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.FixedSingle)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Groove;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Groove)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Inset;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Inset)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Dashed;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Dashed)
            {
                chkBorderStyle.BorderStyle = BorderStyle.NotSet;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.NotSet)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Outset;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Outset)
            {   
                chkBorderStyle.BorderStyle = BorderStyle.Ridge;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Ridge)
            {
                chkBorderStyle.BorderStyle = BorderStyle.ShadowBox;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.ShadowBox)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Solid;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Solid)
            {
                chkBorderStyle.BorderStyle = BorderStyle.Underline;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            else if (chkBorderStyle.BorderStyle == BorderStyle.Underline)
            {
                chkBorderStyle.BorderStyle = BorderStyle.None;
                textBox1.Text = "Check BorderStyle: " + chkBorderStyle.BorderStyle.ToString();
            }
            
        }

    }
}