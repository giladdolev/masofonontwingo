﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView FindItemWithText Method (String, Boolean, Int32)" Height="840px" ID="windowView2" LoadAction="ListViewFindItemWithText3Params\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="280px" Text="FindItemWithText" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Finds the first ListViewItem or ListViewItem.ListViewSubItem, if indicated, that begins with the specified
             text value. The search starts at the specified index.
            
           Syntax: public ListViewItem FindItemWithText(string text, bool includeSubItemsInSearch,
             int startIndex, bool isPrefixSearch)

            Parameters:
            'text': The text to search for.
            'includeSubItemsInSearch': true to include subitems in the search; otherwise, false. 
            'startIndex': The index of the item at which to start the search.
            Return Value: The first ListViewItem that begins with the specified text value or null if the list is empty
             or there is no matching item." Top="75px"  ID="lblDefinition"></vt:Label>
      
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Invoke the FindItemWithText(String, Boolean, Int32) method using the controls below" Top="380px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedListView -->
        <vt:ListView runat="server" Text="TestedListView" Top="430px" Height="125px" ID="TestedListView">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="Haim Michael">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Yaron Moshe"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="David"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Cohen">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Michael"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="Shimon Horowitz"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Gali">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Tiqva Cohen"> <vt:ListViewSubitem runat="server" Text="Tiqva Cohen"></vt:ListViewSubitem></vt:ListViewSubitem>
                       
                        <vt:ListViewSubitem runat="server" Text="Tirzha"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                 <vt:ListViewItem runat="server" Text="Itai">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Tiki Halifa"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="Rachel Emenu"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="570px" Width="490px" Height="50px" ID="lblLog"></vt:Label>
          
         <vt:Label runat="server" Text="Enter String: " Top="660px" Left="80px" ID="lblEnterString" ClickAction="ListViewFindItemWithText3Params\ChangePixelLeft_Click"></vt:Label>

        <vt:TextBox runat="server" Text="Enter String" Top="685px"  ID="txtEnterString" ClickAction=" ListViewFindItemWithText3Params\ChangePixelLeft_Click"></vt:TextBox>

         <vt:CheckBox runat="server" Top="685px" Left="260px" ID="chkIncludeSubItemsInSearch" Width="170px" Text="IncludeSubItemsInSearch"></vt:CheckBox>

        <vt:ComboBox runat="server" Width="95px" Left="460px" CssClass="vt-cmbStartIndex" Text="StartIndex" Top="685px" ID="cmbStartIndex" >
            <Items>
                <vt:ListItem runat="server" Text="0"></vt:ListItem>
                <vt:ListItem runat="server" Text="1"></vt:ListItem>
                <vt:ListItem runat="server" Text="2"></vt:ListItem>
            </Items>
        </vt:ComboBox>

       

        <vt:Button runat="server" Text="FindItemWithText(String, Boolean, Int32) >>" Width="270px" Top="735px" ID="FindItemWithText" ClickAction="ListViewFindItemWithText3Params\FindItemWithText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>