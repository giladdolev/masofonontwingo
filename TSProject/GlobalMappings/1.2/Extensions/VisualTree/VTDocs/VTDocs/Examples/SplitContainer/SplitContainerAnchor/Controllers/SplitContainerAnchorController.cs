using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splRunTimeAnchor = this.GetVisualElementById<SplitContainer>("splRunTimeAnchor");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");


            if (splRunTimeAnchor.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                splRunTimeAnchor.Anchor = AnchorStyles.Top; //button keeps the top gap from the SplitContainer
                panel2.Size = new Size(400, 171);
                textBox1.Text = "AnchorStyles.Top, ";
                textBox1.Text += "SplitContainer Size: (400, 171)";

            }

            else if (splRunTimeAnchor.Anchor == AnchorStyles.Top)
            {
                splRunTimeAnchor.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the SplitContainer
                panel2.Size = new Size(315, 190);
                textBox1.Text = "AnchorStyles.Left, ";
                textBox1.Text += "SplitContainer Size: (315, 190)";
            }
            else if (splRunTimeAnchor.Anchor == AnchorStyles.Left)
            {
                splRunTimeAnchor.Anchor = AnchorStyles.Right; //button keeps the Right gap from the SplitContainer
                panel2.Size = new Size(200, 190);
                textBox1.Text = "AnchorStyles.Right, ";
                textBox1.Text += "SplitContainer Size: (200, 190)";
            }
            else if (splRunTimeAnchor.Anchor == AnchorStyles.Right)
            {
                splRunTimeAnchor.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the SplitContainer
                panel2.Size = new Size(200, 160);
                textBox1.Text = "AnchorStyles.Bottom, ";
                textBox1.Text += "SplitContainer Size: (200, 160)";
            }
            else if (splRunTimeAnchor.Anchor == AnchorStyles.Bottom)
            {
                splRunTimeAnchor.Anchor = AnchorStyles.None; //Resizing SplitContainer doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the SplitContainer size
                textBox1.Text = "AnchorStyles.None, ";
                textBox1.Text += "SplitContainer Size: (315, 171)";
            }
            else if (splRunTimeAnchor.Anchor == AnchorStyles.None)
            {
                //Restore SplitContainer1 and btnChangeAnchor settings
                splRunTimeAnchor.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the SplitContainer

                panel2.Size = new Size(300, 72);//Set  back the SplitContainer size
                splRunTimeAnchor.Location = new Point(75, 18);
                textBox1.Text = "AnchorStyles.Left | AnchorStyles.Top, ";
                textBox1.Text += "SplitContainer Size is back to : (300, 72)";
            }       
        }

    }
}