using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownHexadecimalController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnGetHexadecimalValue_Click(object sender, EventArgs e)
        {
            NumericUpDownElement numericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("numericUpDown1");
            MessageBox.Show("numericUpDown1 Hexadecimal value is: " + numericUpDown1.Hexadecimal.ToString());

        }

        private void btnChangeHexadecimal_Click(object sender, EventArgs e)
        {
            NumericUpDownElement numericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("numericUpDown1");
            if (numericUpDown1.Hexadecimal == false)
                numericUpDown1.Hexadecimal = true;
            else
                numericUpDown1.Hexadecimal = false;


        }
    }
}