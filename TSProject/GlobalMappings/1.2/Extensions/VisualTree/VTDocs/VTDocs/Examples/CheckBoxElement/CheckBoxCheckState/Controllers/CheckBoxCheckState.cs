using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxCheckStateController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCheckState_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkCheckCheckState = this.GetVisualElementById<CheckBoxElement>("chkCheckCheckState");

            if (chkCheckCheckState.CheckState == CheckState.Checked)
            {
                chkCheckCheckState.CheckState = CheckState.Indeterminate;
            }
            else if (chkCheckCheckState.CheckState == CheckState.Indeterminate)
            {
                chkCheckCheckState.CheckState = CheckState.Unchecked;
            }
            else if (chkCheckCheckState.CheckState == CheckState.Unchecked)
            {
                chkCheckCheckState.CheckState = CheckState.Checked;
            }
            
        }

    }
}