
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItemCollection RemoveAt Method (Int32)" ID="windowView2" LoadAction="TabTabItemCollectionRemoveAt\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="RemoveAt (Int32)" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Removes the tab page at the specified index from the collection.

            Syntax: public void RemoveAt (int index)" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="170px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the 'Remove TabItem' button will remove TabItem, 
            which is specified by its index. Pressing the 'Add TabItem' button will add a new TabItem." Top="210px" ID="lblExp2"></vt:Label>

        <vt:Tab runat="server" Text="Tab" Top="290px" Width="400px" ID="TestedTab1">
           <%-- <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="Tab1Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>--%>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="405" Width="380px"></vt:Label>

        <vt:Label runat="server" Text="Enter TabItem index to remove:" Left="80px" ID="lblInput" Top="470px"></vt:Label>
        <vt:TextBox runat="server" Text="..." Top="470px" Left="290px" Width="50px" ID="txtInput" ForeColor="Gray"  TextChangedAction="TabTabItemCollectionRemoveAt\txtInput_TextChanged"></vt:TextBox>

        <vt:Button runat="server" Text="Remove TabItem >>" Top="470px" Left="360px" ID="btnRemoveTabItem" ClickAction="TabTabItemCollectionRemoveAt\RemoveTabItem_Click"></vt:Button>

        <vt:Button runat="server" Text="Add TabItem >>" Top="470px" Left="520px" ID="btnAddTabItem" ClickAction="TabTabItemCollectionRemoveAt\btnAddTabItem_Click"></vt:Button>

        <vt:Button runat="server" Text="Reload TabItems >>" Top="525px" ID="btnReload" ClickAction="TabTabItemCollectionRemoveAt\btnReload_Click"></vt:Button>

        

    </vt:WindowView>
</asp:Content>
