using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformClick_Click(object sender, EventArgs e)
        {

            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");


            TestedPanel.PerformClick(e);
        }

        public void TestedPanel_Click(object sender, EventArgs e)
        {

            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");

            MessageBox.Show("TestedPanel Click event method is invoked");
        }

    }
}