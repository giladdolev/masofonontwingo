using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxLocationChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");

            if (testedTextBox.Location == new Point(140, 90))
            {
                testedTextBox.Location = new Point(200, 500);
            }
            else
            {
                testedTextBox.Location = new Point(140, 90);
            }
        }

        public void testedTextBox_LocationChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "TextBox LocationChanged event is invoked\\r\\n";
        }

    }
}