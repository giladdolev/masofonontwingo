
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label ImageAlign Property" ID="windowView1"  LoadAction="LabelImageAlign\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ImageAlign" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the alignment of an image that is displayed in the control.

            Syntax: public ContentAlignment ImageAlign { get; set; }
            Value: One of the ContentAlignment values. The default is ContentAlignment.MiddleCenter."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="Label" Height="80px" Width="300px" Top="175px" ID="TestedLabel1"></vt:Label>
        
        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="320px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ImageAlign property of this Label is initially set to 'MiddleCenter'.
             Use the buttons below to change the Tested Label ImageAlign value." Top="350px" ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" ImageAlign="MiddleCenter" Height="100px" Width="300px" Top="415px" ID="TestedLabel2" Image="/Content/Images/icon.jpg"></vt:Label>
        
        <vt:Label runat="server" SkinID="Log" Top="520px" ID="lblLog2"></vt:Label>
 
        <vt:Button runat="server" Text="TopLeft" width="100px"  Top="550px" Left="80px" ID="btnTopLeft" TabIndex="1" ClickAction="LabelImageAlign\btnTopLeft_Click"></vt:Button>  
        <vt:Button runat="server" Text="TopCenter" width="100px" Top="550px" Left="240px" ID="btnTopCenter" TabIndex="1" ClickAction="LabelImageAlign\btnTopCenter_Click"></vt:Button>  
        <vt:Button runat="server" Text="TopRight"  width="100px" Top="550px" Left="400px" ID="btnTopRight" TabIndex="1" ClickAction="LabelImageAlign\btnTopRight_Click"></vt:Button>  
        <vt:Button runat="server" Text="MiddleLeft" width="100px"  Top="590px" Left="80px" ID="btnMiddleLeft" TabIndex="1" ClickAction="LabelImageAlign\btnMiddleLeft_Click"></vt:Button>  
        <vt:Button runat="server" Text="MiddleCenter" width="100px" Top="590px" Left="240px" ID="btnMiddleCenter" TabIndex="1" ClickAction="LabelImageAlign\btnMiddleCenter_Click"></vt:Button>  
        <vt:Button runat="server" Text="MiddleRight"  width="100px" Top="590px" Left="400px" ID="btnMiddleRight" TabIndex="1" ClickAction="LabelImageAlign\btnMiddleRight_Click"></vt:Button>  
        <vt:Button runat="server" Text="BottomLeft" width="100px"  Top="630px" Left="80px" ID="btnBottomLeft" TabIndex="1" ClickAction="LabelImageAlign\btnBottomLeft_Click"></vt:Button>   
        <vt:Button runat="server" Text="BottomCenter" width="100px" Top="630px" Left="240px" ID="btnBottomCenter" TabIndex="1" ClickAction="LabelImageAlign\btnBottomCenter_Click"></vt:Button>  
        <vt:Button runat="server" Text="BottomRight"  width="100px" Top="630px" Left="400px" ID="btnBottomRight" TabIndex="1" ClickAction="LabelImageAlign\btnBottomRight_Click"></vt:Button>  

        
    </vt:WindowView>
</asp:Content>