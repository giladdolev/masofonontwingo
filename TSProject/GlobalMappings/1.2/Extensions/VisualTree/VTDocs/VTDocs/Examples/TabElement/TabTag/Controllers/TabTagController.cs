using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (TestedTab1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedTab1.Tag;
            }

            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            if (TestedTab2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedTab2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedTab2.Tag == "New Tag.")
            {
                TestedTab2.Tag = TestedTab2;
                lblLog2.Text = "Tag value: " + TestedTab2.Tag;
            }
            else
            {
                TestedTab2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedTab2.Tag;

            }
        }

    }
}