﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridCheckBoxColumn CheckedChange event" ID="windowView1" LoadAction="GridGridCheckBoxColumnCheckedchanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CheckedChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the value of the IsChecked property changes.

            Syntax: public event EventHandler CheckedChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can invoke the CheckedChange event by clicking the button or by manually 
            Check uncheck Grid Columns CheckBoxes
            Each invocation of the CheckBox Column 'CheckedChanged' event should add a line in the 'Event Log'."
            Top="200px" ID="lblExp2">
        </vt:Label>

         <vt:Grid runat="server" Top="290px" ID="TestedGrid">
            <Columns>
                <vt:GridCheckBoxColumn  runat="server" ID="Col1" HeaderText="Column1" Height="20px" Width="85px"
                    CheckedChangeAction="GridGridCheckBoxColumnCheckedchanged\coulmns_CheckedChange" ></vt:GridCheckBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="Col2" HeaderText="Column2" Height="20px" Width="85px"
                    CheckedChangeAction="GridGridCheckBoxColumnCheckedchanged\coulmns_CheckedChange" ></vt:GridCheckBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="Col3" HeaderText="Column3" Height="20px" Width="85px" CheckedChangeAction="GridGridCheckBoxColumnCheckedchanged\coulmns_CheckedChange"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="420px" Height="50px" ID="lblLog"></vt:Label>

          <vt:Button runat="server" Text="Change CheckBox cell value >>"  Top="510px" Width="185px" ID="btnChangeCheckBoxValue" ClickAction="GridGridCheckBoxColumnCheckedchanged\btnChangeCheckBoxValue_Click"></vt:Button>

         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="555px" ID="txtEventLog"></vt:TextBox>
    </vt:WindowView>
</asp:Content>
