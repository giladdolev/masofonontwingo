using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ListView Left Value is: " + TestedListView.Left + '.';
        }

        public void ChangeLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            if (TestedListView.Left == 200)
            {
                TestedListView.Left = 80;
                lblLog.Text = "ListView Left Value is: " + TestedListView.Left + '.';

            }
            else
            {
                TestedListView.Left = 200;
                lblLog.Text = "ListView Left Value is: " + TestedListView.Left + '.';
            }
        }
    }
}