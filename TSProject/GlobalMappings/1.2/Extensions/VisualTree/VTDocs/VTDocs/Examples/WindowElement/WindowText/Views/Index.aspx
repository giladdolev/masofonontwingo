<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   
     <vt:WindowView runat="server" Text="Window Text Property"  Top="57px" Left="11px" ID="windowView1" Height="800px" Width="700px">
		
		<vt:Button runat="server" UseVisualStyleText="True" TextAlign="MiddleCenter" Text="Change Text" Top="110px" Left="44px" ClickAction="WindowText\btnChangeText_Click" ID="btnChangeText" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button>

		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="170px" Left="44px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="500px">
		</vt:TextBox>	

        <vt:Label runat="server" Text="Click the button to open Window2" Top="270px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        
        <vt:Button runat="server" UseVisualStyleText="True" TextAlign="MiddleCenter" Text="Open Form2" Top="300px" Left="44px" ClickAction="WindowText\btnOpenWindow2_Click" ID="Button1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button> 

        </vt:WindowView>
</asp:Content>