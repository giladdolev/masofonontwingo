﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid RowClick event" ID="windowView2" LoadAction="GridRowClick\OnLoad">

         <vt:Label runat="server" SkinID="Title" Text="RowClick" ID="lblTitle" Top="15px">
        </vt:Label>

        <vt:Label runat="server" Text="The RowClick event is invoked when clicking on any element of a grid row (cell, cell border, 
            row indicator, etc).

            Syntax: public event EventHandler GridRowClickEventArgs RowClick;
            The event handler receives an argument of type GridRowClickEventArgs containing data related to 
            this event.
            When data editing of cells is enabled, the event will not invoke when clicking on an editable cell 
            with the mouse."
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, The first column is editable.
            You can click on any element of a grid row (cell, cell border, row indicator, etc). 
            Each invocation of the 'RowClick' event should add a line in the 'Event Log'."
            Top="300px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" CssClass ="vt-test-grid" Top="380px" ID="TestedGrid" RowClickAction="GridRowClick\TestedGrid_RowClick" SelectionChangedAction="GridRowClick\TestedGrid_SelectionChanged">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridColumn  runat="server" ID="col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        
        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="510px" Width="450px"></vt:Label> 

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="550px" ID="txtEventLog"></vt:TextBox>
         
    </vt:WindowView>
</asp:Content>
