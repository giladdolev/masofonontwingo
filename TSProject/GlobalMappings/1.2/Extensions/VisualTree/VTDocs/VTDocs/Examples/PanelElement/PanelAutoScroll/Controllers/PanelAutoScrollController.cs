using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelAutoScrollController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeAutoScroll_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement pnlRuntimeAutoScroll = this.GetVisualElementById<PanelElement>("pnlRuntimeAutoScroll");

            if (pnlRuntimeAutoScroll.AutoScroll == false)
            {
                pnlRuntimeAutoScroll.AutoScroll = true;
                textBox1.Text = "AutoScroll -> true";
            }
            else
            {
                pnlRuntimeAutoScroll.AutoScroll = false;
                textBox1.Text = "AutoScroll -> false";
            }
            
        }

    }
}