<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="DateTimePicker Value Property" ID="windowView1" LoadAction="DateTimePickerValue\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Value" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the date/time value assigned to the control.
            
        public System.DateTime Value { get; set; }
            If the Value property has not been changed in code or by the user, it is set to the current date and time" Top="75px"  ID="lblDefinition"></vt:Label>
      
       <!-- Tested DateTimePicker1 -->
        <vt:DateTimePicker runat="server" Text="DateTimePicker" Left="80px" Height="20px" Width="200px" Top="170px" ID="TestedDateTimePicker1"></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Height="40px" Top="205px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="290px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Value property of this 'TestedDateTimePicker' is initially set to '2021-01-01'" Top="340px"  ID="lblExp1"></vt:Label>     
        
        <!-- Tested DateTimePicker2 -->
        <vt:DateTimePicker runat="server"  Text="TestedDateTimePicker" CssClass="vt-TestedDateTimePicker" Left ="80px" Height="20px" Width="200px" Value="2021-01-01" Top="390px" ID="TestedDateTimePicker2" ValueChangedAction="DateTimePickerValue\TestedDateTimePicker2_ValueChanged"></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Height="40px" Top="425px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Value property >>" Top="510px" ID="btnChangeValue" Width="185px" ClickAction="DateTimePickerValue\btnChangeValue_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Format property >>" Top="510px" Left="300px" ID="btnChangeFormat" Width="185px" ClickAction="DateTimePickerValue\btnChangeFormat_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
