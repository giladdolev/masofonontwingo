using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicLabelController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void button1_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label1");

      
            label.Text = "Wordone WordTwo WordThree WordFour WordFive REALLYLONGWORDTHATDOESNTEXIST";
            label.MaxWidth = 60;
            label.MaxHeight = 500;
            LabelElement label2 = this.GetVisualElementById<LabelElement>("label2");
            label2.Text = "Assigned by server only at: " + DateTime.Now.ToLongTimeString();
        }
        public void button2_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label1");
            label.Font = new System.Drawing.Font(label.Font, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline);

        }
        public void button3_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label3");
            label.BorderStyle = System.Web.VisualTree.BorderStyle.ShadowBox;

            LabelElement label4 = this.GetVisualElementById<LabelElement>("label4");
            label4.BorderStyle = System.Web.VisualTree.BorderStyle.Double;
        }

        private void btnAutoEllipsis_Click(object sender, EventArgs e)
        {
            LabelElement label1 = this.GetVisualElementById<LabelElement>("label5");
            if (label1.AutoEllipsis == false)
            {
                label1.AutoEllipsis = true;
            }
            else
            {
                label1.AutoEllipsis = false;
            }
        }

    }
}