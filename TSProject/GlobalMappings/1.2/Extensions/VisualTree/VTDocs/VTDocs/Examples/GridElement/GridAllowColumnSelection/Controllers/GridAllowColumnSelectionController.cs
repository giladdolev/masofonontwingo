using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridAllowColumnSelectionController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");



            for (int i = 0; i < 3; i++)
            {
                //   ApplicationElement.ExecuteOnRespone(func, null, new GridCellEventArgs(i, i), 100);
            }


        }


        private void dataChanged(object sender, EventArgs e)
        {
            var h = e as GridCellEventArgs;
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            TestedGrid1.SetCellStyle(0, 0, "cellcss");
            TestedGrid1.SetCellStyle(0, 1, "cellcss");
            TestedGrid1.SetCellStyle(0, 2, "cellcss");
        }
        private void EditChangedAction(object sender, EventArgs e)
        {

        }

        public void btnhangeAllowColumnSelection_click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid1");
            for (int i = 0; i < 100; i++)
            {
                gridElement.Rows[1].Cells[1].Value = "ad" + i.ToString();
                gridElement.Rows[1].Cells[2].Value = "ad";
            }

        }

    }
}
