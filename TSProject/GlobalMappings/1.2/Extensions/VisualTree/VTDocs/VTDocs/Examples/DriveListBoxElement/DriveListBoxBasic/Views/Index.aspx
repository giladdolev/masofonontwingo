<%@ Page Title="BasicFileSystemDialog" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Drive ListBox Property" ID="windowView1">

        <vt:Label runat="server" SkinID="Title" Text="Basic Drive ListBox" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Show behavior of compatibility Drive ListBox" Top="75px"  ID="lblDefinition"></vt:Label>
      
        <!-- Actual sample element(s) should be placed instead of button below -->

        <vt:DriveListBox runat="server" ID="dlb1"  Top="100px" ChangedAction="DriveListBoxBasic\SelectedItemChanged"></vt:DriveListBox> 

        <vt:Label runat="server" SkinID="Log" Top="150px" ID="lblLog1"></vt:Label>
 
    </vt:WindowView>
</asp:Content>
