using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new ButtonCreateControl());

        }

        private ButtonCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as ButtonCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ButtonElement newButton = new ButtonElement();

            newButton.CreateControl();
            newButton.Text = "newButton";

            this.ViewModel.Controls.Add(newButton);
            

            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}