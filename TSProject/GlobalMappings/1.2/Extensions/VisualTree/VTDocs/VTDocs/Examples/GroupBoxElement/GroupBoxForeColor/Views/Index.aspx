<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox ForeColor property" ID="windowView1" LoadAction="GroupBoxForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested GroupBox1 -->
        <vt:GroupBox runat="server" Text="GroupBox" Top="165px" ID="TestedGroupBox1"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="255px" BorderStyle="None" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of this GroupBox is initially set to Green" Top="385px" ID="lblExp1"></vt:Label>

        <!-- Tested GroupBox2 -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="420px" ForeColor="Green" ID="TestedGroupBox2"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="590px" Width="180px" ID="btnChangeGroupBoxForeColor" ClickAction="GroupBoxForeColor\btnChangeGroupBoxForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
