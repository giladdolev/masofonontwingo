using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridRowCountController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

		private void button1_Click(object sender, EventArgs e)
		{
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            gridElement.RowCount = 2;
            gridElement.RefreshData();
            gridElement.Refresh();
		}


	}
}
