<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel RightToLeft Property" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="RightToLeft is set to 'Yes'" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Text="" Top="55px" RightToLeft="Yes" Left="140px" ID="pnlRightToLeft" Height="150px" Width="200px">
            <vt:Label runat="server" AutoSize="false" Text="Label1" Top="10px" BackColor="Yellow" Left="10px" ID="Label4" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="100px"></vt:Label>
        </vt:Panel>           

        <vt:Label runat="server" Text="RunTime" Top="220px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Text="RuntimeRightToLeft" Top="235px" Left="140px" ID="pnlRuntimeRightToLeft" Height="150px" Width="200px" >      
            <vt:Label runat="server" AutoSize="false" Text="Label1" Top="10px" BackColor="Yellow" Left="10px" ID="Label3" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="100px"></vt:Label>
                 
        </vt:Panel>

        <vt:Button runat="server" Text="Change Panel RightToLeft" Top="265px" Left="420px" ID="btnChangeRightToLeft" Height="36px" Width="200px" ClickAction="PanelRightToLeft\btnChangeRightToLeft_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="400px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        