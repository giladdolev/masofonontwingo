<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="NumericUpDown Equals" ID="windowView1" LoadAction="NumericUpDownEquals\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Equals" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Determines whether the specified object is equal to the current object. 

            Syntax: public virtual bool Equals(object obj)
            Parameters: obj - The object to compare with the current object.                 
            Return Value: true if the specified object is equal to the current object; otherwise, false." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Here are two NumericUpDown:" Top="240px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="In the following example we will use this TestedNumericUpDown to invoke the Equals method on.
            Use 'Set' buttons to assign NumericUpDown1 / NumericUpDown2 to TestedNumericUpDown.
            Use 'Invoke Equals(NumericUpDown1)' button to return True if TestedNumericUpDown is equal to 
            NumericUpDown1, otherwise, False." Top="360px" ID="Label2"></vt:Label>

        <vt:NumericUpDown runat="server" Left="80px" Height="24px" Width="170px" Text="NumericUpDown1" Top="310px" ID="NumericUpDown1"></vt:NumericUpDown>

        <vt:NumericUpDown runat="server" Left="280px" Height="24px" Width="170px" Text="NumericUpDown2" Top="310px" ID="NumericUpDown2"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="525px" ID="lblLog1" Height="40px" Width="400px"></vt:Label>

        <vt:Label runat="server" AutoSize="false" Left="80px" Font-Names="Calibri Light" Font-Size="12pt" Text="TestedNumericUpDown" Top="470px"></vt:Label>  
        <vt:Label runat="server" AutoSize="false" Left="80px" Font-Names="Calibri Light" Font-Size="12pt" Text ="NumericUpDown1" Top="290px"></vt:Label>  
        <vt:Label runat="server" AutoSize="false" Left="280px" Font-Names="Calibri Light" Font-Size="12pt" Text ="NumericUpDown2" Top="290px"></vt:Label>  

        <vt:Button runat="server" Text="Set to NumericUpDown1 >>" Top="490px" width="180px" Left="510px" ID="btnSetToNumericUpDown1" ClickAction="NumericUpDownEquals\btnSetToNumericUpDown1_Click"></vt:Button>

         <vt:Button runat="server" Text="Set to NumericUpDown2 >>" Top="520px" width="180px" Left="510px" ID="btnSetToNumericUpDown2" ClickAction="NumericUpDownEquals\btnSetToNumericUpDown2_Click"></vt:Button>

        <vt:Button runat="server" Text="Invoke Equals(NumericUpDown1) >>" Top="610px" width="200px" Left="80px" ID="btnInvokeEquals" ClickAction="NumericUpDownEquals\btnInvokeEquals_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>




