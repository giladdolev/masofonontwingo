<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Validated event" ID="windowView2" LoadAction="ComboBoxValidated\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Validated" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control is validating.

            Syntax: public event EventHandler Validating
            
            If the CausesValidation property is set to false, the Validating and Validated events are suppressed."
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can type text or choose a value from the combobox dropdown list. 
            When you shift the focus to the 'Shift Focus' button, the combobox text will be checked and  
            if it doesn't match one of the items from the combobox dropdown list, an error message will be  
             written to the log. Each invocation of the 'Validated' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:ComboBox runat="server" CssClass="vt-test-cmb" Text="TestedComboBox" Top="365px" ID="TestedComboBox"  ValidatedAction="ComboBoxValidated\TestedComboBox_Validated" TextChangedAction="ComboBoxValidated\TestedComboBox_TextChanged" LostFocusAction="ComboBoxValidated\TestedComboBox_LostFocus"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="405px" Width="340px" Height="50px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="470px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Shift Focus >>" Top="615px" ID="btnShiftFocus"></vt:Button> 
        
        <vt:Button runat="server" Text="Toggle CausesValidation Value >>" Left="300px" width="200px" Top="615px" ID="btnToggleCausesValidation" ClickAction="ComboBoxValidated\btnToggleCausesValidation_Click"></vt:Button>         
       
    </vt:WindowView>
</asp:Content>
