using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerOrientationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedSplitContainer1.Orientation == System.Web.VisualTree.Orientation.Vertical)
            {
                lblLog1.Text = "Orientation value: Vertical";
            }
            else if (TestedSplitContainer1.Orientation == System.Web.VisualTree.Orientation.Horizontal)
            {
                lblLog1.Text = "Orientation value: Horizontal";
            }

            if (TestedSplitContainer2.Orientation == System.Web.VisualTree.Orientation.Vertical)
            {
                lblLog2.Text = "Orientation value: Vertical.";
            }
            else if (TestedSplitContainer2.Orientation == System.Web.VisualTree.Orientation.Horizontal)
            {
                lblLog2.Text = "Orientation value: Horizontal.";
            }
        }

        public void btnChangeOrientation_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedSplitContainer2.Orientation == System.Web.VisualTree.Orientation.Vertical)
            {
                TestedSplitContainer2.Orientation = System.Web.VisualTree.Orientation.Horizontal;
                lblLog2.Text = "Orientation value: Horizontal.";
            }
            else if (TestedSplitContainer2.Orientation == System.Web.VisualTree.Orientation.Horizontal)
            {
                TestedSplitContainer2.Orientation = System.Web.VisualTree.Orientation.Vertical;
                lblLog2.Text = "Orientation value: Vertical.";
            }
        }
    }
}