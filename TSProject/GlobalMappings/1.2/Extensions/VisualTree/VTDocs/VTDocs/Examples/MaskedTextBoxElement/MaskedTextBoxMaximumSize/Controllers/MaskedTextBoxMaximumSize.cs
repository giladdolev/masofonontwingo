﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MaskedTextBoxMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "MaskedTextBox MaximumSize value: " + TestedMaskedTextBox.MaximumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
        }

        public void btnChangeMskMaximumSize_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedMaskedTextBox.MaximumSize == new Size(120, 60))
            {
                TestedMaskedTextBox.MaximumSize = new Size(160, 20);
                lblLog1.Text = "MaskedTextBox MaximumSize value: " + TestedMaskedTextBox.MaximumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
            }
            else if (TestedMaskedTextBox.MaximumSize == new Size(160, 20))
            {
                TestedMaskedTextBox.MaximumSize = new Size(190, 30);
                lblLog1.Text = "MaskedTextBox MaximumSize value: " + TestedMaskedTextBox.MaximumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
            }
            else
            {
                TestedMaskedTextBox.MaximumSize = new Size(120, 60);
                lblLog1.Text = "MaskedTextBox MaximumSize value: " + TestedMaskedTextBox.MaximumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
            }

        }

        public void btnSetToZeroMskMaximumSize_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedMaskedTextBox.MaximumSize = new Size(0, 0);
            lblLog1.Text = "MaskedTextBox MaximumSize value: " + TestedMaskedTextBox.MaximumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;

        }


    }
}