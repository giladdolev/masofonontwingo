<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image Height Property" ID="windowView2" LoadAction="ImageHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Height" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int Height { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this 'TestedImage' is initially set to 90px" Top="250px"  ID="lblExp1"></vt:Label>     

        <!-- TestedImage -->
        <vt:Image runat="server" Text="TestedImage" Top="300px" ID="TestedImage"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Height value >>" Top="460px" ID="btnChangeHeight" ClickAction="ImageHeight\btnChangeHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
