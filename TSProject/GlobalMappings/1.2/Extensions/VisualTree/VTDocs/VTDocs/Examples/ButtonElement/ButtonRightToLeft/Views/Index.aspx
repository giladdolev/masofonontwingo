<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic button" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

     
        <vt:button runat="server"  Text="RightToLeft is set to'yes'" RightToLeft="Yes" Top="45px" Left="140px" ID="btnRightToLeft" Height="36px" TabIndex="2" Width="200px"></vt:button>
      

         <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>
     
        <vt:button runat="server"  Text="Change button RightToLeft" Top="115px" Left="140px" ID="btnChangeRightToLeft" Height="36px"  TabIndex="3" Width="200px" ClickAction="buttonRightToLeft\btnChangeRightToLeft_Click" ></vt:button>

    </vt:WindowView>
</asp:Content>
