<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Text Property" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent: Verify the Panel below is shown without Text" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Text="Text is set to - This is a recursion" ShowHeader="true" Top="55px"  Left="140px" ID="pnlText" Height="150px" Width="200px"></vt:Panel>           

        <vt:Label runat="server" Text="RunTime" Top="220px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Text="RuntimeText" Top="235px" ShowHeader="true" Left="140px" ID="pnlRuntimeText" Height="150px" Width="200px" >           
        </vt:Panel>

        <vt:Button runat="server" Text="Change Panel Text" Top="265px" Left="420px" ID="btnChangeText" Height="36px" Width="200px" ClickAction="PanelText\btnChangeText_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="400px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        