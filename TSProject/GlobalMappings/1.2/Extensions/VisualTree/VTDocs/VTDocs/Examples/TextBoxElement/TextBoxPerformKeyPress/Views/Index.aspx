<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox PerformKeyPress() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformKeyPress of 'Tested TextBox'" Top="45px" Left="140px" ID="btnPerformKeyPress" Height="36px" Width="300px" ClickAction="TextBoxPerformKeyPress\btnPerformKeyPress_Click"></vt:Button>


        <vt:TextBox runat="server" Text="Tested TextBox" Top="150px" Left="200px" ID="testedTextBox" Height="36px"  Width="200px" KeyPressAction="TextBoxPerformKeyPress\testedTextBox_KeyPress"></vt:TextBox>           

        

    </vt:WindowView>
</asp:Content>
