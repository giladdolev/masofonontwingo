<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PixelWidth Property" ID="windowView1" LoadAction="CheckBoxPixelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels.
            
            Syntax: public int PixelWidth { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="175px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this CheckBox is initially set to: 150px" Top="225px" ID="lblExp1"></vt:Label>

        <vt:CheckBox runat="server" SkinID="BackColorCheckBox" Text="TestedCheckBox" Top="285px" ID="TestedCheckBox" TabIndex="3"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Top="325px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelWidth value >>" Top="390px" ID="btnChangePixelWidth" TabIndex="3" Width="180px" ClickAction="CheckBoxPixelWidth\btnChangePixelWidth_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
