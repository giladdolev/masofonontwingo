using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformKeyPressController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformKeyPress_Click(object sender, EventArgs e)
        {
            Keys keys = new Keys();
            KeyPressEventArgs args = new KeyPressEventArgs(keys);
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");

            testedPanel.PerformKeyPress(args);
        }

        public void testedPanel_KeyPress(object sender, EventArgs e)
        {
            MessageBox.Show("TestedPanel KeyPress event method is invoked");
        }

    }
}