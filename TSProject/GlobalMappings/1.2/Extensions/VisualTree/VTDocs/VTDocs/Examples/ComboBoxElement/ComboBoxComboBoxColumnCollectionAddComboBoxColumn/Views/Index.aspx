 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox ComboBoxColumnCollection Add Method (ComboBoxColumn)" ID="windowView2" LoadAction="ComboBoxComboBoxColumnCollectionAddComboBoxColumn\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Add(ComboBoxColumn)" left="200px" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Adds the given column to the collection.

            Syntax: public void Collection<ComboboxColumn>.Add(ComboboxColumn item)
            Parameters: item - The column to add." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can press the bellow buttons to add and remove any type
             of ComboBoxColumn to this MultiColumn ComboBox" 
            Top="255px" ID="lblExp2"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" MultiColumn="true"  ID="TestedComboBox" CssClass="vt-test-cmb" Top="320px"> </vt:ComboBox>   

        <vt:Label runat="server" SkinID="Log" Width="350px" ID="lblLog" Top="480px"></vt:Label>

        <vt:Button runat="server" Text="Add Integer Column >>" Width="160px" Top="545px" ID="btnAddIntegerColumn" ClickAction="ComboBoxComboBoxColumnCollectionAddComboBoxColumn\btnAddIntegerColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Add String Column >>" Width="160px" Left="270px" Top="545px" ID="btnAddStringColumn" ClickAction="ComboBoxComboBoxColumnCollectionAddComboBoxColumn\btnAddStringColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Remove Last Column >>" BackColor="Green" Width="160px" Left="270px" Top="650px" ID="btnRemoveColumn" ClickAction="ComboBoxComboBoxColumnCollectionAddComboBoxColumn\btnRemoveColumn_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>