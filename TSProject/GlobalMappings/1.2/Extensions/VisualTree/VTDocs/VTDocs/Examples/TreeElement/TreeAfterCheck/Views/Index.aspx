<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree AfterCheck event" ID="windowView2" LoadAction="TreeAfterCheck\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="AfterCheck" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs after the tree node check box is checked.

            Syntax: public event EventHandler <TreeEventArgs>  AfterCheck"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="175px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can invoke the AfterCheck event by checking the
             Tree CheckBoxes either by buttons bellow or manually."
            Top="225px" ID="lblExp2">
        </vt:Label>

        <!-- TestedTree -->
        <vt:Tree runat="server" Text="TestedTree" Top="295px" ID="TestedTree" CssClass="vt-test-tree-1" CheckBoxes="true" AfterCheckAction="TreeAfterCheck\TestedTree_AfterCheck">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="TreeItem1">
                    <Items>
                        <vt:TreeItem runat="server" Text="Node00" ID="TreeItem11" Checked="true"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="TreeItem2" Checked="true"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="TreeItem3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="420px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="460px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

         <vt:Button runat="server" Text="Change Node1 Checked Value >>" Top="610px" Width="230px" ID="btnChangeNode1Checked" ClickAction="TreeAfterCheck\btnChangeNode1Checked_Click"></vt:Button>

         <vt:Button runat="server" Text="Change Child Node00 Checked Value >>" Top="610px" Width="230px" Left="350px" ID="btnChangeChildNode00Checked" ClickAction="TreeAfterCheck\btnChangeChildNode00Checked_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

