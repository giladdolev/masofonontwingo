<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Equals Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">


        <vt:Button runat="server" Text="Button1" Top="45px" Left="140px" ID="button1" Height="36px" Width="220px" ></vt:Button>           

         <vt:Button runat="server" Text="Button2" Top="80px" Left="140px" ID="button2" Height="36px" Width="220px" ></vt:Button>


        <vt:Button runat="server" Text="Invoke Equals" Top="80px" Left="400px" ID="btnEquals" Height="36px" Width="220px" ClickAction ="ButtonEquals\btnEquals_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="220px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="220px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
