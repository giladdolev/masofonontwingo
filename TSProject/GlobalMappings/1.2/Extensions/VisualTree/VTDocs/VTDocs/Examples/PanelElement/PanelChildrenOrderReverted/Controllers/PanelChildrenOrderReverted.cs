using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelChildrenOrderRevertedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnGetChildrenOrderReverted_Click(object sender, EventArgs e)
        {
            PanelElement pnlChildrenOrderReverted = this.GetVisualElementById<PanelElement>("pnlChildrenOrderReverted");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            textBox1.Text = "ChildrenOrderReverted value:\\r\\n" + pnlChildrenOrderReverted.ChildrenOrderReverted;

            
        }

    }
}