using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: " + TestedPanel.ClientRectangle;

        }
        public void btnChangePanelBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.Left == 120)
            {
                TestedPanel.SetBounds(80, 350, 200, 80);
                lblLog.Text = "ClientRectangle value: " + TestedPanel.ClientRectangle;

            }
            else
            {
                TestedPanel.SetBounds(120, 330, 300, 50);
                lblLog.Text = "ClientRectangle value: " + TestedPanel.ClientRectangle;
            }

        }

    }
}