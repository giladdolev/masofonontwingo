using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownPixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelTop value: " + TestedNumericUpDown.PixelTop + '.';
        }

        public void btnChangePixelTop_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedNumericUpDown.PixelTop == 310)
            {
                TestedNumericUpDown.PixelTop = 290;
                lblLog.Text = "PixelTop value: " + TestedNumericUpDown.PixelTop + '.';
            }
            else
            {
                TestedNumericUpDown.PixelTop = 310;
                lblLog.Text = "PixelTop value: " + TestedNumericUpDown.PixelTop + '.';
            }

        }
      
    }
}