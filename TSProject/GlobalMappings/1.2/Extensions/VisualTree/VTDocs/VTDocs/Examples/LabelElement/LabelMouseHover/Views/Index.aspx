<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label MouseHover event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Hovering Hover the label below" Top="110px" Left="160px" ID="label1" Height="13px" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Label runat="server" Text="Hover me" Top="200px" Left="160px" ID="label2" Height="13px" Width="350px" MouseHoverAction="LabelMouseHover\btn_MouseHover"></vt:Label>
    </vt:WindowView>
</asp:Content>
