<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CompositeView ContextMenuStrip Property" ID="windowView" SkinID="WindowForCompositeView" LoadAction="CompositeContextMenuStrip\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ContextMenuStrip" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the ContextMenuStrip associated with this control.
                        
            Syntax:  public ContextMenuStripElement ContextMenuStrip { get; set; }
            Property Value: The ContextMenuStrip for this control, or null if there is no ContextMenuStrip.
            The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:PartialControl runat="server" Controller="Composite1" Action="CompositeIndex" Top="190px" Left="80px" ID="TestedCompositeIndex1" Height="185px" Width="540px" CssClass="vt-test-cmpst-1">
		</vt:PartialControl> 

        <vt:Label runat="server" SkinID="Log" Width="400px" Top="390px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="430px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ContextMenuStrip property of this 'TestedPanel' is initially set to 
           'contextMenuStrip1' with menu item 'Hello'. Use the buttons below to set TestesPanel 
            ContextMenuStrip property to a different value" Top="480px"  ID="lblExp1"></vt:Label>
             
        <vt:PartialControl runat="server" Controller="Composite1" Action="CompositeIndex" Top="565px" Left="80px" ID="TestedCompositeIndex2" ContextMenuStripID ="contextMenuStrip1" Height="185px" Width="540px" CssClass="vt-test-cmpst-2">
		</vt:PartialControl> 

        <vt:Label runat="server" SkinID="Log" Top="765px" Width="400px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="contextMenuStrip1" Top="830px"  ID="btnChangToContextMenuStrip1" ClickAction="CompositeContextMenuStrip\btnChangToContextMenuStrip1_Click"></vt:Button>

        <vt:Button runat="server" Text="contextMenuStrip2" Top="830px" Left="270px" ID="btnChangToContextMenuStrip2" ClickAction="CompositeContextMenuStrip\btnChangToContextMenuStrip2_Click"></vt:Button>

        <vt:Button runat="server" Text="Reset" Top="830px" Left="460px" ID="btnReset" ClickAction="CompositeContextMenuStrip\btnReset_Click"></vt:Button>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip1" >
            <vt:ToolBarMenuItem runat ="server" ID="ToolBarMenuItem1" Text="Hello"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip2" >
            <vt:ToolBarMenuItem runat ="server" ID="toolBarMenuItem2" Text="Hello World"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

    </vt:WindowView>
</asp:Content>

