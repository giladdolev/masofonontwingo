<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Visible Property" ID="windowView2"  LoadAction="ProgressBarVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ProgressBar runat="server" Text="ProgressBar" Dock="None" Top="155px" ID="TestedProgressBar1"></vt:ProgressBar>
        
        <vt:Label runat="server" SkinID="Log" Top="205px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="270px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this ProgressBar is initially set to 'false'
             The ProgressBar should be invisible" Top="320px" ID="lblExp1"></vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Dock="None" Visible="false" Top="385px" ID="TestedProgressBar2"></vt:ProgressBar>
        
        <vt:Label runat="server" SkinID="Log" Top="435px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="500px" ID="Button1" Width="200px" ClickAction ="ProgressBarVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        