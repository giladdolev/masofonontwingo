<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer works from Load event" Top="57px" Left="11px" ID="windowView1" Height="480px" Width="640px" LoadAction="TimerTickFromLoad\Form_Load">

        <vt:Label runat="server" Text="Timer Tick event creating in Load event: Verify Timer Tick event starts running" Top="80px" Left="50px" ID="Label3" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="4" Width="450px" Font-Bold="true">
		</vt:Label>

        <vt:TextBox runat="server" Text="" Top="200px" Left="220px" ID="textBox2" Multiline="true"  Height="100px" Width="200px"></vt:TextBox>

        <vt:TextBox runat="server" PasswordChar="" Text="" Top="150px" Left="180px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="20px" TabIndex="2" Width="300px">
		</vt:TextBox>
    </vt:WindowView>

    <vt:ComponentManager runat="server" >
        <vt:Timer runat="server" ID="timer1" Enabled="false" Interval="1000">
		</vt:Timer>
	</vt:ComponentManager>
</asp:Content>