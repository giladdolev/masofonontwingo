<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ControlBox" Top="57px" Left="11px" ID="windowView12" Height="905px" Width="768px" BackColor="Yellow">
              
    <vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="MaximizeBox" Top="75px" Left="145px" ClickAction="ControlBox\btnMaximizeBox_Click" ID="btnMaximizeBox" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="51px" TabIndex="28" Width="157px">
		</vt:Button>
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="MinimizeBox" Top="162px" Left="145px" ClickAction="ControlBox\btnMinimizeBox_Click" ID="btnMinimizeBox" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="51px" TabIndex="29" Width="157px">
		</vt:Button>
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="ControlBox" Top="246px" Left="145px" ClickAction="ControlBox\btnControlBox_Click" ID="btnControlBox" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="51px" TabIndex="30" Width="157px">
		</vt:Button>
    </vt:WindowView>
</asp:Content>
    