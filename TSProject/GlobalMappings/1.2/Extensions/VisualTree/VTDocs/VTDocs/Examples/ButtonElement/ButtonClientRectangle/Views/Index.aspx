<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button ClientRectangle Property" ID="windowView" LoadAction="ButtonClientRectangle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientRectangle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets the rectangle that represents the client area of the control.

            Syntax: public Rectangle ClientRectangle { get; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="Label2" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ClientRectangle values of this 'TestedButton' is initially set with 
            Left: 80px, Top: 350px, Width: 150px, Height: 36px" Top="260px"  ID="lblExp1"></vt:Label>     

        <!-- TestedButton -->
        <vt:Button runat="server" SkinID="Wide"  Text="TestedButton" Top="350px" ID="TestedButton"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="400px" Height="40" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Button Bounds >>"  Top="480px" Width="180px" ID="btnChangeButtonBounds" ClickAction="ButtonClientRectangle\btnChangeButtonBounds_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>




