using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridGridColumnHeaderTextController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog.Text = "Column1 HeaderText value: " + TestedGrid.Columns[0].HeaderText + "\\r\\nColumn2 HeaderText value: " + TestedGrid.Columns[1].HeaderText + "\\r\\nColumn3 HeaderText value: " + TestedGrid.Columns[2].HeaderText;

        }


        private void btnChangeColumn1HeaderText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            if (TestedGrid.Columns[0].HeaderText == "Column1")
            {
                TestedGrid.Columns[0].HeaderText = "NewHeaderText";
            }
            else
            {
                TestedGrid.Columns[0].HeaderText = "Column1";
            }

            lblLog.Text = "Column1 HeaderText value: " + TestedGrid.Columns[0].HeaderText + "\\r\\nColumn2 HeaderText value: " + TestedGrid.Columns[1].HeaderText + "\\r\\nColumn3 HeaderText value: " + TestedGrid.Columns[2].HeaderText;
        }

        private void btnChangeColumn2HeaderText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            if (TestedGrid.Columns[1].HeaderText == "Column2")
            {
                TestedGrid.Columns[1].HeaderText = "NewHeaderText";
            }
            else
            {
                TestedGrid.Columns[1].HeaderText = "Column2";
            }

            lblLog.Text = "Column1 HeaderText value: " + TestedGrid.Columns[0].HeaderText + "\\r\\nColumn2 HeaderText value: " + TestedGrid.Columns[1].HeaderText + "\\r\\nColumn3 HeaderText value: " + TestedGrid.Columns[2].HeaderText;
        }

        private void btnChangeColumn3HeaderText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            if (TestedGrid.Columns[2].HeaderText == "Column3")
            {
                TestedGrid.Columns[2].HeaderText = "NewHeaderText";
            }
            else
            {
                TestedGrid.Columns[2].HeaderText = "Column3";
            }

            lblLog.Text = "Column1 HeaderText value: " + TestedGrid.Columns[0].HeaderText + "\\r\\nColumn2 HeaderText value: " + TestedGrid.Columns[1].HeaderText + "\\r\\nColumn3 HeaderText value: " + TestedGrid.Columns[2].HeaderText;
        }


}
}
