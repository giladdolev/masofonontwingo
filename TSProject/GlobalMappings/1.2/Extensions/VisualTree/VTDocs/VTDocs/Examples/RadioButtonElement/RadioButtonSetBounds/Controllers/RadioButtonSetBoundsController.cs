using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested RadioButton bounds are set to: \\r\\nLeft  " + TestedRadioButton.Left + ", Top  " + TestedRadioButton.Top + ", Width  " + TestedRadioButton.Width + ", Height  " + TestedRadioButton.Height;

        }
        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            if (TestedRadioButton.Left == 100)
            {
                TestedRadioButton.SetBounds(80, 300, 150, 26);
                lblLog.Text = "The Tested RadioButton bounds are set to: \\r\\nLeft  " + TestedRadioButton.Left + ", Top  " + TestedRadioButton.Top + ", Width  " + TestedRadioButton.Width + ", Height  " + TestedRadioButton.Height;

            }
            else
            {
                TestedRadioButton.SetBounds(100, 280, 200, 50);
                lblLog.Text = "The Tested RadioButton bounds are set to: \\r\\nLeft  " + TestedRadioButton.Left + ", Top  " + TestedRadioButton.Top + ", Width  " + TestedRadioButton.Width + ", Height  " + TestedRadioButton.Height;
            }
        }

    }
}