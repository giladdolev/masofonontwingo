<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab PreviousIndex Property" ID="windowView2" LoadAction="TabPreviousIndex\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="PreviousIndex" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the Previous selected Tab item index.

            Syntax: public int PreviousIndex { get }
            
            A negative initial value (-1) is returned if no selection has been made yet." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Tab runat="server" Top="190px" ID="TestedTabControl1" Height="100px" Width="350" SelectedIndexChangedAction="TabPreviousIndex\tabSelectedIndex_SelectedIndexChanged">
            <TabItems>
                <vt:TabItem runat="server" Text="tabItem1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="lbl1" Text="Tab Item 1"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label2" Text="Tab Item 2"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label3" Text="Tab Item 3"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem4" Top="22px" Left="4px" ID="tabPage4" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label4" Text="Tab Item 4"></vt:Label>
                </vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="300px" height="32px" Width="350" ID="lblLog1"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following TabControl the initial selected tab index is 3.
             You can manually select any tab item from the TabControl or press the 'Select Tab Index 0' button." 
            Top="400px" ID="lblExp2"></vt:Label>

        <vt:Tab runat="server" SelectedIndex="3" Top="460px" ID="TestedTabControl2" Height="100px" Width="350" SelectedIndexChangedAction="TabPreviousIndex\tabSelectedIndex_SelectedIndexChanged">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label1" Text="Tab Page 1"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label5" Text="Tab Page 2"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label6" Text="Tab Page 3"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage4" Top="22px" Left="4px" ID="TabItem4" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label7" Text="Tab Page 4"></vt:Label>
                </vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" ID="lblLog2" height="32px" Width="350" Top="570px"></vt:Label>

        <vt:Button runat="server" Text="Select Tab Index 0 >>" Top="625px" ID="btnSelectTabIndex0" ClickAction="TabPreviousIndex\btnSelectTabIndex0_Click"></vt:Button>


        
    </vt:WindowView>
</asp:Content>




<%--<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab PreviousIndex Property" ID="windowView2" LoadAction="TabPreviousIndex\load_view">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the PreviousIndex property." Top="75px" ID="Label6"></vt:Label>

        <vt:Tab runat="server" SelectedIndex="3" Top="180px" Left="230px" ID="tabControl1" Height="100px" Width="500" SelectedIndexChangedAction="TabPreviousIndex\tabSelectedIndex_SelectedIndexChanged">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="lbl1" Text="Tab Item 1"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label2" Text="Tab Item 2"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label3" Text="Tab Item 3"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage4" Top="22px" Left="4px" ID="tabPage4" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label4" Text="Tab Item 4"></vt:Label>
                </vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" Text="Log" Top="280px" Left="350px" ID="label5">
        </vt:Label>

          <vt:TextBox runat="server"  SkinID="EventLog" Text="Log:" Top="300" Height="600" ID="txtEventLog"></vt:TextBox>
    </vt:WindowView>
</asp:Content>
--%>
