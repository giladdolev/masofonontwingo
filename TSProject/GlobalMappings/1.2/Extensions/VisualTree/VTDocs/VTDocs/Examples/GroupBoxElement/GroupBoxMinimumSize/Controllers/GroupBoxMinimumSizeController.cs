using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "GroupBox MinimumSize value: " + TestedGroupBox.MinimumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;

        }
        public void btnChangeGrpMinimumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            if (TestedGroupBox.MinimumSize == new Size(230, 80))
            {
                TestedGroupBox.MinimumSize = new Size(160, 120);
                lblLog.Text = "GroupBox MinimumSize value: " + TestedGroupBox.MinimumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;

            }
            else if (TestedGroupBox.MinimumSize == new Size(160, 120))
            {
                TestedGroupBox.MinimumSize = new Size(150, 50);
                lblLog.Text = "GroupBox MinimumSize value: " + TestedGroupBox.MinimumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;
            }

            else
            {
                TestedGroupBox.MinimumSize = new Size(230, 80);
                lblLog.Text = "GroupBox MinimumSize value: " + TestedGroupBox.MinimumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;
            }

        }

        public void btnSetToZeroGrpMinimumSize_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGroupBox.MinimumSize = new Size(0, 0);
            lblLog.Text = "GroupBox MinimumSize value: " + TestedGroupBox.MinimumSize + "\\r\\nGroupBox Size value: " + TestedGroupBox.Size;

        }

    }
}