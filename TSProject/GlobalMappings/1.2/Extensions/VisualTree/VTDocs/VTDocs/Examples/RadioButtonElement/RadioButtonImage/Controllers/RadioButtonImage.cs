using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement btnTestedRadioButton1 = this.GetVisualElementById<RadioButtonElement>("btnTestedRadioButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (btnTestedRadioButton1.Image == null)
            {
                lblLog1.Text = "No image";
            }
            else
            {
                lblLog1.Text = "Set with image";
            }


            RadioButtonElement btnTestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("btnTestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (btnTestedRadioButton2.Image == null)
            {
                lblLog2.Text = "No image.";
            }
            else
            {
                lblLog2.Text = "Set with image.";
            }
        }



        public void btnChangeImage_Click(object sender, EventArgs e)
        {
            RadioButtonElement btnTestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("btnTestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (btnTestedRadioButton2.Image == null)
            {
                btnTestedRadioButton2.Image = new UrlReference("/Content/Elements/Button.png");
                lblLog2.Text = "Set with image.";
            }
            else
            {
                btnTestedRadioButton2.Image = null;
                lblLog2.Text = "No image.";

            }
        }
    }
}