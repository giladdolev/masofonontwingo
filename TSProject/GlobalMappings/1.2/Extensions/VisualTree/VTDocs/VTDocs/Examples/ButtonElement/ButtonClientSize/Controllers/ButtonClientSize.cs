using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedButton.ClientSize;

        }
        public void btnChangeButtonSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.Width == 200)
            {
                TestedButton.ClientSize = new Size(150, 36);
                lblLog.Text = "ClientSize value: " + TestedButton.ClientSize;

            }
            else
            {
                TestedButton.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedButton.ClientSize;
            }

        }

    }
}