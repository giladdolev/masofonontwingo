﻿ <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridTextBoxColumn MaxInputLength Property" ID="windowView2" LoadAction="GridGridTextBoxColumnMaxInputLength\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="MaxInputLength" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the maximum number of characters that can be entered into the column cells.
            
            Syntax: public int MaxInputLength { get; set; }            
            Property Value: The maximum number of characters that can be entered into the text box;
            the default value is 32767.
            
             The MaxInputLength property does not affect the length of text entered programmatically through 
            the cell's value or through cell formatting. It affects only what the user can input and edit."
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="265px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example Column1.MaxInputLength is initially set to 3. You can also examine the behavior
              by using the below button to change the max input length of column1 and the Text of cell(0,0)." 
            Top="315px" ID="lblExp2"></vt:Label>

        <vt:Grid runat="server" Top="380px" ID="TestedGrid" >
            <Columns>
                <vt:GridTextBoxColumn  runat="server" MaxInputLength="3" ID="GridCol1"  HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>


        <vt:Label runat="server" SkinID="Log"  Height="40px" Width="550px"  ID="lblLog" Top="510px"></vt:Label>


        <vt:Button runat="server" Text="Change Column1 MaxInputLength >>" Width ="220px" Left="80px" Top="580px" ID="btnChangeColumn1MaxInputLength" ClickAction="GridGridTextBoxColumnMaxInputLength\btnChangeColumn1MaxInputLength_Click"></vt:Button>



        <vt:label runat="server" Text="Set text to cell (0,0):" Font-Size="11pt" Left="80px" Top="635px" ID="lblInput2" ></vt:label>

        <vt:TextBox runat="server" Text="Enter Text.." ForeColor="Gray" Width="85px" Left="205px"  Top="633px" ID="txtInputColumnText" TextChangedAction="GridGridTextBoxColumnMaxInputLength\txtInput_TextChanged"  ></vt:TextBox>

        <vt:Button runat="server" Text="Change Column1 Text >>" Left="330px" Width ="150px" Top="635px" ID="btnChangeColumn1Text" ClickAction="GridGridTextBoxColumnMaxInputLength\btnChangeColumn1Text_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>




