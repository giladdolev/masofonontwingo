
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Error Provider" ID="windowView1" LoadAction="ErrorProviderBasic\OnLoad">
      
       
        <vt:Label runat="server" SkinID="Title" Text="ErrorProvider" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Provides a user interface for indicating that a control on a form has an error associated with it."
            Top="75px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="130px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example there are 2 textboxes, a combobox and a richtextbox in a simple 
            login dialog.
            When one of fields is left empty, an error icon is displayed using SetError method, and the 
            error description is written to the log using GetError method." Top="170px" ID="lblExp2">
        </vt:Label>

        <vt:panel runat="server" Text="" Top="280px" ID="panel1" Width="350px" Height="250">
        <vt:label runat="server" Text="Username:" Top="40px" Left="50px" ID="lblUsername"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-username" Top="40px" Left="145px" ID="txtUsername" LeaveAction="ErrorProviderBasic\txtUsername_leave"></vt:textbox>

        <vt:label runat="server" Text="Password:" Top="70px" Left="50px" ID="lblPassword"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-password" Top="70px" Left="145px" ID="txtPassword" LeaveAction="ErrorProviderBasic\txtPassword_leave"></vt:textbox>

        <vt:label runat="server" Text="Gender:" Top="100px" Left="50px" ID="lblGender"></vt:label>
        <vt:ComboBox runat="server" Text="" DroppedDown="true" Top="100px" CssClass="vt-ep-gender" Left="145px" Width="150px"  ID="cmbGender" LeaveAction="ErrorProviderBasic\cmbGender_leave"></vt:ComboBox>

        <vt:label runat="server" Text="Address:" Top="130px" Left="50px" ID="lblAddress"></vt:label>
        <vt:RichTextBox runat="server" Text="" Top="130px" CssClass="vt-rich-address" Left="145px" Height="50px" Width="150px" ID="RichtbAddress" LeaveAction="ErrorProviderBasic\RichtbAddress_leave"></vt:RichTextBox>

        <vt:Button runat="server" Text="Login" Width="100px" Height="30px" Top="200px" Left="130px" ID="btnLogin" ClickAction="ErrorProviderBasic\btnLogin_Click"></vt:Button>
            </vt:panel>
        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="540" Height="80px" Width="350px"></vt:Label>

       
    </vt:WindowView>
    <vt:ComponentManager runat="server" >
		<vt:ErrorProvider runat="server" CssClass="vt-ep-username" ID="epUsername" ></vt:ErrorProvider>
	    <vt:ErrorProvider runat="server" CssClass="vt-ep-password" ID="epPassword" ></vt:ErrorProvider>
        <vt:ErrorProvider runat="server" CssClass="vt-ep-gender" ID="emGender" ></vt:ErrorProvider>
	    <vt:ErrorProvider runat="server" CssClass="vt-ep-address" ID="epAddress" ></vt:ErrorProvider>

	</vt:ComponentManager>s
</asp:Content>