using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSelectedIndexController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        
        public void ChangeTabsChanged_Click(object sender, EventArgs e)
        {
            TextBoxElement txtExplanation = this.GetVisualElementById<TextBoxElement>("txtExplanation");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabelement = this.GetVisualElementById<TabElement>("tabControl1");
            
            
            int index;
            
            if (int.TryParse(textBox1.Text, out index) && index < tabelement.TabItems.Count)
            {
                tabelement.SelectedIndex = index;
                txtExplanation.Text = "The selected TabIndex is: " + tabelement.SelectedIndex;
            }
            else
                txtExplanation.Text = "Enter index number to be selected";
                
        }
    }
}