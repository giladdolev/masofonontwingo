using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new CheckBoxEquals());
        }

        private CheckBoxEquals ViewModel
        {
            get { return this.GetRootVisualElement() as CheckBoxEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            CheckBoxElement CheckBox1 = this.GetVisualElementById<CheckBoxElement>("CheckBox1");
            CheckBoxElement CheckBox2 = this.GetVisualElementById<CheckBoxElement>("CheckBox2");


            if (this.ViewModel.TestedCheckBox.Equals(CheckBox1))
            {
                this.ViewModel.TestedCheckBox = CheckBox2;
                textBox1.Text = "TestedCheckBox Equals CheckBox2";
            }
            else
            {
                this.ViewModel.TestedCheckBox = CheckBox1;
                textBox1.Text = "TestedCheckBox Equals CheckBox1";
            }

            
        }
        

        public void Load(object sender, EventArgs e)
        {
            this.ViewModel.Controls.Add(this.ViewModel.TestedCheckBox);
        }



    }
}