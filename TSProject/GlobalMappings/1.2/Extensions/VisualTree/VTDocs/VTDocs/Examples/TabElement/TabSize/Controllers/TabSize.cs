using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //TabAlignment
        private void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedTab.Size;
        }
        private void btnChangeTabSize_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTab.Size == new Size(300, 100)) //Get
            {
                TestedTab.Size = new Size(200, 70); //Set
                lblLog.Text = "Size value: " + TestedTab.Size;
            }
            else
            {
                TestedTab.Size = new Size(300, 100);
                lblLog.Text = "Size value: " + TestedTab.Size;
            }
        }

    }
}