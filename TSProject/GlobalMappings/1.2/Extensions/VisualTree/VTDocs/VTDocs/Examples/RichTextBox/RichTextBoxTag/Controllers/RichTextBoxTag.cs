using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox1 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (TestedRichTextBox1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedRichTextBox1.Tag;
            }

            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            if (TestedRichTextBox2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedRichTextBox2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedRichTextBox2.Tag == "New Tag.")
            {
                TestedRichTextBox2.Tag = TestedRichTextBox2;
                lblLog2.Text = "Tag value: " + TestedRichTextBox2.Tag;
            }
            else
            {
                TestedRichTextBox2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedRichTextBox2.Tag;

            }
        }

    }
}