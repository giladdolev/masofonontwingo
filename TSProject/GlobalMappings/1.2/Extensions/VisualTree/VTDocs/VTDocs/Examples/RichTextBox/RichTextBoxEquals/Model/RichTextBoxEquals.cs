﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class RichTextBoxEquals : WindowElement
    {

        RichTextBox _TestedRichTextBox;

        public RichTextBoxEquals()
        {
            _TestedRichTextBox = new RichTextBox();
        }

        public RichTextBox TestedRichTextBox
        {
            get { return this._TestedRichTextBox; }
            set { this._TestedRichTextBox = value; }
        }
    }
}