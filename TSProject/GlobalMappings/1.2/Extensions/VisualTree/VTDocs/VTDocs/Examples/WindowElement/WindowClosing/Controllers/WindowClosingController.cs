using System.Collections.Generic;
using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowClosingController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowClosing());
        }
        private WindowClosing ViewModel
        {
            get { return this.GetRootVisualElement() as WindowClosing; }
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowClosing");
            return argsDictionary;
        }

        public void OnLoad(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = GetArgsDictionary();
            ViewModel.frm2 = VisualElementHelper.CreateFromView<WindowClosing2>("WindowClosing2", "Index2", null, argsDictionary, null);
            /*
                        CheckBoxElement chkCancelClosing = this.GetVisualElementById<CheckBoxElement>("chkCancelClosing");
                        LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

                        lblLog.Text = "Cancel Closing Action State: " + chkCancelClosing.IsChecked + '.';
             */
        }

        public void btnOpenTestWindow_Click(object sender, EventArgs e)
        {
            ViewModel.frm2.Show();
        }
        /*
                public void closing(object sender, WindowClosingEventArgs e)
                {
                    CheckBoxElement chkCancelClosing = this.GetVisualElementById<CheckBoxElement>("chkCancelClosing");
                    e.Cancel = chkCancelClosing.IsChecked;

                    TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
                    txtEventLog.Text += "\\r\\nWindow: closing event is invoked";
                }

                public void closed(object sender, WindowClosedEventArgs e)
                {
                    TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
                    txtEventLog.Text += "\\r\\nWindow: closed event is invoked";
                }

                public void btnCloseWindow_Click(object sender, EventArgs e)
                {
                   WindowElement ClosingWindowView = this.GetVisualElementById<WindowElement>("ClosingWindowView");
                   LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

                   ClosingWindowView.Close();

                   lblLog.Text = "Window Close method was called";
                }

                public void chkCancelClosing_CheckedChanged(object sender, EventArgs e)
                {
                    CheckBoxElement chkCancelClosing = this.GetVisualElementById<CheckBoxElement>("chkCancelClosing");
                    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

                    lblLog.Text = "Cancel Closing Action State: " + chkCancelClosing.IsChecked + '.';
                }
        */
    }
}