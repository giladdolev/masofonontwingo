<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GetActiveControl" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="TextBoxGetActiveControl\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="GetActiveControl Method" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the current active control.

            Syntax: public Task &lt;ControlElement&gt; GetActiveControl()
            
            Return Value: ControlElement object if the focused component is typeof ControlElement; otherwise, null.
            
            Remarks: Make sure not to activate this method using button click because the focus will change,
            and the expected result will be always wrong.
            Do not use debbuger mode in VisualStudio or Browser debugger so the focus won't switch
            to another component.
            
            In the example below, first click on one of the textboxes so it get focus, then hover over the button
            'Hover Over Me'.You should see the ID of the TextBox in the logger." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="400px" ID="lblExample"></vt:Label>

        <vt:TextBox runat="server" Text="My ID is 'textBox1'" Top="450px"  Left="20px" ID="textBox1"  Height="20px" Width="180px"></vt:TextBox>
        <vt:TextBox runat="server" Text="My ID is 'textBox2'" Top="500px"  Left="20px" ID="textBox2"  Height="20px" Width="180px"></vt:TextBox>
        <vt:TextBox runat="server" Text="My ID is 'textBox3'" Top="550px"  Left="20px" ID="textBox3"  Height="20px" Width="180px"></vt:TextBox>
        <vt:TextBox runat="server" Text="My ID is 'textBox4'" Top="600px"  Left="20px" ID="textBox4"  Height="20px" Width="180px"></vt:TextBox>
        <vt:Button runat="server" Text="Hover Over Me" Top="500px" Left="300px" ID="button1" Height="36px" Width="200px" MouseHoverAction="TextBoxGetActiveControl\Mouse_Hover"></vt:Button>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="650px" Height="40px"></vt:Label>
        
    </vt:WindowView>
</asp:Content>
