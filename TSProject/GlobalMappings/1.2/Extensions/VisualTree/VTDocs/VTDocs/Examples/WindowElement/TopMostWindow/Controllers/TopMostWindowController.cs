using System.Collections.Generic;
using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TopMostWindowController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TopMostWindow());
        }
        private TopMostWindow ViewModel
        {
            get { return this.GetRootVisualElement() as TopMostWindow; }
        }
        public void Form1_Load(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = GetArgsDictionary();
            ViewModel.frm2 = VisualElementHelper.CreateFromView<TopMostWindow2>("TopMostWindow2", "Index2", null, argsDictionary, null);
            ViewModel.frm3 = VisualElementHelper.CreateFromView<TopMostWindow3>("TopMostWindow3", "Index3", null, argsDictionary, null);
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "TopMostWindow");
            return argsDictionary;
        }

        public void button1_Click(object sender, EventArgs e)
        {
            ViewModel.frm2.Show();
            ViewModel.frm3.Show();
        }

        public void btnTopMost_Click(object sender, EventArgs e)
        {
            if (ViewModel.frm2.TopMost)
            {
                ViewModel.frm2.TopMost = false;
                ViewModel.frm3.TopMost = true;
            }
            else
            {
                ViewModel.frm3.TopMost = false;
                ViewModel.frm2.TopMost = true;
            }

        }
    }
}