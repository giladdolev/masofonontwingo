using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformRenderController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformRender_Click(object sender, EventArgs e)
        {
            
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");
            
            MouseEventArgs args = new MouseEventArgs();

            //testedCheckBox.PerformRender(args);
        }

        public void testedCheckBox_Render(object sender, EventArgs e)
        {
            MessageBox.Show("TestedCheckBox Render event method is invoked");
        }

    }
}