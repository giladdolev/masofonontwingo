using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ComboBox MinimumSize value: " + TestedComboBox.MinimumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;

        }
        public void btnChangeCmbMinimumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            if (TestedComboBox.MinimumSize == new Size(150, 30))
            {
                TestedComboBox.MinimumSize = new Size(100, 70);
                lblLog.Text = "ComboBox MinimumSize value: " + TestedComboBox.MinimumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;

            }
            else if (TestedComboBox.MinimumSize == new Size(100, 70))
            {
                TestedComboBox.MinimumSize = new Size(200, 30);
                lblLog.Text = "ComboBox MinimumSize value: " + TestedComboBox.MinimumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;
            }

            else
            {
                TestedComboBox.MinimumSize = new Size(150, 30);
                lblLog.Text = "ComboBox MinimumSize value: " + TestedComboBox.MinimumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;
            }

        }

        public void btnSetToZeroCmbMinimumSize_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.MinimumSize = new Size(0, 0);
            lblLog.Text = "ComboBox MinimumSize value: " + TestedComboBox.MinimumSize + "\\r\\nComboBox Size value: " + TestedComboBox.Size;

        }

    }
}