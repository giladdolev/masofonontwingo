using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxArrayBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        

        private void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Click the buttons...";
        }

        private void btnCheckAll_Click(object sender, EventArgs e)
        {
            List<CheckBoxElement> CheckBoxArr = this.GetVisualElementsById<CheckBoxElement>("CheckBoxArr");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            for(int i = 0; i < CheckBoxArr.Count; i++)
            {
                CheckBoxArr[i].CheckState = CheckState.Checked;
            }

            lblLog.Text = "All CheckBoxes were checked.";

        }
     



        private void btnUnCheckAll_Click(object sender, EventArgs e)
        {
            List<CheckBoxElement> CheckBoxArr = this.GetVisualElementsById<CheckBoxElement>("CheckBoxArr");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

             for(int i = 0; i < CheckBoxArr.Count; i++)
            {
                CheckBoxArr[i].CheckState = CheckState.Unchecked;
            }

             lblLog.Text = "All CheckBoxes were unchecked.";
        }
      
       
  
    }
}