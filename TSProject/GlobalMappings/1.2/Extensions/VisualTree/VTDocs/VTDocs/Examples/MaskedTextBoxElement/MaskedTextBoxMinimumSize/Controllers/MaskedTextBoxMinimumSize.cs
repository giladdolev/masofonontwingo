﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MaskedTextBoxMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "MaskedTextBox MinimumSize value: " + TestedMaskedTextBox.MinimumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
        }

        public void btnChangeMskMinimumSize_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedMaskedTextBox.MinimumSize == new Size(200, 30))
            {
                TestedMaskedTextBox.MinimumSize = new Size(130, 50);
                lblLog1.Text = "MaskedTextBox MinimumSize value: " + TestedMaskedTextBox.MinimumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
            }
            else if (TestedMaskedTextBox.MinimumSize == new Size(130, 50))
            {
                TestedMaskedTextBox.MinimumSize = new Size(160, 40);
                lblLog1.Text = "MaskedTextBox MinimumSize value: " + TestedMaskedTextBox.MinimumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
            }
            else
            {
                TestedMaskedTextBox.MinimumSize = new Size(200, 30);
                lblLog1.Text = "MaskedTextBox MinimumSize value: " + TestedMaskedTextBox.MinimumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;
            }

        }

        public void btnSetToZeroMskMinimumSize_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedMaskedTextBox.MinimumSize = new Size(0, 0);
            lblLog1.Text = "MaskedTextBox MinimumSize value: " + TestedMaskedTextBox.MinimumSize + "\\r\\nMaskedTextBox Size value: " + TestedMaskedTextBox.Size;

        }


    }
}