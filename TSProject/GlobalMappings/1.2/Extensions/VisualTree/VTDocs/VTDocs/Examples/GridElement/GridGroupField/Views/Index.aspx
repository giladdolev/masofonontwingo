﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupField Example" ID="Form1" Height="800px" Width="1000px" LoadAction="GridGroupField\View_load">

        <vt:Label runat="server" SkinID="SubTitle" Text="GroupField" Top="15px" ID="Label1" Left="300" Font-Size="19pt" Font-Bold="true"></vt:Label>

        <vt:Label runat="server" Text=" Get, set Column GroupField ." Top="65px" ID="lblDefinition"></vt:Label>


        <vt:Grid runat="server" AutoSize="true" Top="120px" Left="85px" ID="gridElement" Height="299px" Width="782px" IsGroupingGrid="True" GroupField="name">
            <Columns>
                <vt:GridColumn ID="name" runat="server" HeaderText="Name" DataMember="colNumeroCuota" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="email" runat="server" HeaderText="Email" DataMember="colTipoCuota" Width="80"></vt:GridColumn>
                <vt:GridColumn ID="phone" runat="server" HeaderText="Phone" DataMember="colFechaInicio" Width="90"></vt:GridColumn>
            </Columns>
        </vt:Grid>



        <vt:Label runat="server" Text="When the GroupField is filled you should see the grid records grouped by the given column id in this example the grid is grouped by name column "
            Top="480px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Font-Size="13pt" Top="520px" ID="Label4"></vt:Label>

        <vt:Label runat="server" Text="GroupField value is set to nothing you should see all the datasource record"
            Top="560px" ID="Label5">
        </vt:Label>


    </vt:WindowView>

</asp:Content>
