﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar GetKeyPressMasks Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
      
        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="170px" Left="140px" ID="testedProgressBar" Height="36px" Width="200px"></vt:ProgressBar>

        <vt:Button runat="server" Text="Invoke GetKeyPressMasks()" Top="45px" Left="140px" ID="btnInvokeGetKeyPressMasks" Height="36px" Width="220px" ClickAction="ProgressBarGetKeyPressMasks\btnGetKeyPressMasks_Click"></vt:Button>

         <vt:TextBox runat="server" Text="" Top="250px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
