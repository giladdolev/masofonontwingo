using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpRunTimeAnchor = this.GetVisualElementById<GroupBoxElement>("grpRunTimeAnchor");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");


            if (grpRunTimeAnchor.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                grpRunTimeAnchor.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                panel2.Size = new Size(400, 171);
                textBox1.Text = "AnchorStyles.Top, ";
                textBox1.Text += "Panel Size: (400, 171)";

            }

            else if (grpRunTimeAnchor.Anchor == AnchorStyles.Top)
            {
                grpRunTimeAnchor.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                panel2.Size = new Size(315, 190);
                textBox1.Text = "AnchorStyles.Left, ";
                textBox1.Text += "Panel Size: (315, 190)";
            }
            else if (grpRunTimeAnchor.Anchor == AnchorStyles.Left)
            {
                grpRunTimeAnchor.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                panel2.Size = new Size(200, 190);
                textBox1.Text = "AnchorStyles.Right, ";
                textBox1.Text += "Panel Size: (200, 190)";
            }
            else if (grpRunTimeAnchor.Anchor == AnchorStyles.Right)
            {
                grpRunTimeAnchor.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                panel2.Size = new Size(200, 160);
                textBox1.Text = "AnchorStyles.Bottom, ";
                textBox1.Text += "Panel Size: (200, 160)";
            }
            else if (grpRunTimeAnchor.Anchor == AnchorStyles.Bottom)
            {
                grpRunTimeAnchor.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the panel size
                textBox1.Text = "AnchorStyles.None, ";
                textBox1.Text += "Panel Size: (315, 171)";
            }
            else if (grpRunTimeAnchor.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and btnChangeAnchor settings
                grpRunTimeAnchor.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                panel2.Size = new Size(300, 200);//Set  back the panel size
                grpRunTimeAnchor.Location = new Point(75, 18);
                textBox1.Text = "AnchorStyles.Left | AnchorStyles.Top, ";
                textBox1.Text += "Panel Size is back to : (300, 200)";
            }       
        }

    }
}