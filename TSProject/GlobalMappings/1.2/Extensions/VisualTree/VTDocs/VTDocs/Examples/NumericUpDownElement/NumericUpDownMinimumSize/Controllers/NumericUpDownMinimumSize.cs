using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "NumericUpDown MinimumSize value: " + TestedNumericUpDown.MinimumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;
        }

        public void btnChangeNudMinimumSize_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedNumericUpDown.MinimumSize == new Size(300, 70))
            {
                TestedNumericUpDown.MinimumSize = new Size(150, 90);
                lblLog1.Text = "NumericUpDown MinimumSize value: " + TestedNumericUpDown.MinimumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;
            }
            else if (TestedNumericUpDown.MinimumSize == new Size(150, 90))
            {
                TestedNumericUpDown.MinimumSize = new Size(400, 50);
                lblLog1.Text = "NumericUpDown MinimumSize value: " + TestedNumericUpDown.MinimumSize + "\\r\\NumericUpDown Size value: " + TestedNumericUpDown.Size;
            }
            else
            {
                TestedNumericUpDown.MinimumSize = new Size(300, 70);
                lblLog1.Text = "NumericUpDown MinimumSize value: " + TestedNumericUpDown.MinimumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;
            }

        }

        public void btnSetToZeroNudMinimumSize_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedNumericUpDown.MinimumSize = new Size(0, 0);
            lblLog1.Text = "NumericUpDown MinimumSize value: " + TestedNumericUpDown.MinimumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;

        }

    }
}