using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonBackgroundImageLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btn1Image = this.GetVisualElementById<ButtonElement>("TestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackgroundImageLayout value: " + btn1Image.BackgroundImageLayout.ToString();
                      
            ButtonElement btn2Image = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
            
        }
        private void btnNone_Click(object sender, EventArgs e)
        {
            ButtonElement btn2Image = this.GetVisualElementById<ButtonElement>("TestedButton2");
            btn2Image.BackgroundImageLayout = ImageLayout.None;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
        private void btnTile_Click(object sender, EventArgs e)
        {
            ButtonElement btn2Image = this.GetVisualElementById<ButtonElement>("TestedButton2");
            btn2Image.BackgroundImageLayout = ImageLayout.Tile;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
        private void btnCenter_Click(object sender, EventArgs e)
        {
           ButtonElement btn2Image = this.GetVisualElementById<ButtonElement>("TestedButton2");
            btn2Image.BackgroundImageLayout = ImageLayout.Center;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
        private void btnStretch_Click(object sender, EventArgs e)
        {
           ButtonElement btn2Image = this.GetVisualElementById<ButtonElement>("TestedButton2");
            btn2Image.BackgroundImageLayout = ImageLayout.Stretch;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
         private void btnZoom_Click(object sender, EventArgs e)
        {
            ButtonElement btn2Image = this.GetVisualElementById<ButtonElement>("TestedButton2");
            btn2Image.BackgroundImageLayout = ImageLayout.Zoom;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
    }
}