using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Panel MinimumSize value: " + TestedPanel.MinimumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;

        }
        public void btnChangePnlMinimumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.MinimumSize == new Size(360, 80))
            {
                TestedPanel.MinimumSize = new Size(200, 100);
                lblLog.Text = "Panel MinimumSize value: " + TestedPanel.MinimumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;

            }
            else if (TestedPanel.MinimumSize == new Size(200, 100))
            {
                TestedPanel.MinimumSize = new Size(250, 50);
                lblLog.Text = "Panel MinimumSize value: " + TestedPanel.MinimumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;
            }

            else
            {
                TestedPanel.MinimumSize = new Size(360, 80);
                lblLog.Text = "Panel MinimumSize value: " + TestedPanel.MinimumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;
            }

        }

        public void btnSetToZeroPnlMinimumSize_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedPanel.MinimumSize = new Size(0, 0);
            lblLog.Text = "Panel MinimumSize value: " + TestedPanel.MinimumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;

        }

    }
}