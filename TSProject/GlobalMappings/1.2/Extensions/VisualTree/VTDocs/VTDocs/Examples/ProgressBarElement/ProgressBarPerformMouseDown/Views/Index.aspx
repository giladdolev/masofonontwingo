<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar PerformMouseDown() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseDown of 'Tested ProgressBar'" Top="45px" Left="140px" ID="btnPerformMouseDown" Height="36px" Width="300px" ClickAction="ProgressBarPerformMouseDown\btnPerformMouseDown_Click"></vt:Button>


        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="150px" Left="200px" ID="testedProgressBar" Height="36px"  Width="200px" MouseDownAction="ProgressBarPerformMouseDown\testedProgressBar_MouseDown"></vt:ProgressBar>           

        

    </vt:WindowView>
</asp:Content>
