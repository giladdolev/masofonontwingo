<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="MonthCalendar DateChanged Event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:MonthCalendar runat="server" Date="11/5/12" Top="106px" Left="140px" ID="monthCalendar1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" TabIndex="1" DateChangedAction="MonthCalendarDateChanged\monthCalendar1_DateChanged">
		</vt:MonthCalendar>
        
        <vt:Textbox runat="server" Text="" Top="370px" Left="130px" ID="Textbox1" Height="60px" Multiline="true" TabIndex="1" Width="200px"></vt:Textbox>


        <vt:Label runat="server" Text="Event Log" Top="350px" Left="130px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px"  Width="150px"></vt:Label>     


    </vt:WindowView>
</asp:Content>
