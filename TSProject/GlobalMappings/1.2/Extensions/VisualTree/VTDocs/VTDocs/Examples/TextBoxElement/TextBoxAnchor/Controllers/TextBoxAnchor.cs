using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            TextBoxElement txtAnchorChange = this.GetVisualElementById<TextBoxElement>("txtAnchorChange");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");


            if (txtAnchorChange.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                txtAnchorChange.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                panel2.Size = new Size(400, 171);
            }

            else if (txtAnchorChange.Anchor == AnchorStyles.Top)
            {
                txtAnchorChange.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                panel2.Size = new Size(315, 190);
            }
            else if (txtAnchorChange.Anchor == AnchorStyles.Left)
            {
                txtAnchorChange.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                panel2.Size = new Size(200, 190);
            }
            else if (txtAnchorChange.Anchor == AnchorStyles.Right)
            {
                txtAnchorChange.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                panel2.Size = new Size(200, 160);
            }
            else if (txtAnchorChange.Anchor == AnchorStyles.Bottom)
            {
                txtAnchorChange.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the panel size
            }
            else if (txtAnchorChange.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and btnChangeAnchor settings
                txtAnchorChange.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                panel2.Size = new Size(300, 72);//Set  back the panel size
               
            }       
        }

    }
}