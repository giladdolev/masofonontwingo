using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    
    public class TabClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTab.ClientRectangle;

        }
        public void btnChangeTabClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            if (TestedTab.Left == 100)
            {
                TestedTab.SetBounds(80, 320, 300, 100);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTab.ClientRectangle;

            }
            else
            {
                TestedTab.SetBounds(100, 310, 200, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTab.ClientRectangle;
            }

        }

    }
}