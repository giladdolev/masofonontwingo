<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Leave event" ID="windowView2" LoadAction="ComboBoxLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the combo boxes,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="265px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="345px" Height="120px" Width="200px">
                
            <vt:ComboBox runat="server" Top="30px" Left="15px" Text="ComboBox 1" ID="TestedComboBox1" CssClass="vt-test-cmb1" TabIndex="1" LeaveAction="ComboBoxLeave\TestedComboBox1_Leave"></vt:ComboBox>

            <vt:ComboBox runat="server" Top="75px" Left="15px" Text="ComboBox 2" ID="TestedComboBox2" CssClass="vt-test-cmb2" TabIndex="2" LeaveAction="ComboBoxLeave\TestedComboBox2_Leave"></vt:ComboBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="480px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="520px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

