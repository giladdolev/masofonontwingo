using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonKeyDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new ButtonKeyDown());
        }

        private ButtonKeyDown ViewModel
        {
            get { return this.GetRootVisualElement() as ButtonKeyDown; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Press the Bottons...";
            ButtonElement btnButton1 = this.GetVisualElementById<ButtonElement>("btnButton1");
        }

        public void btnButton1_KeyDown(object sender, KeyEventArgs e)
        {
            ButtonElement btnButton1 = this.GetVisualElementById<ButtonElement>("btnButton1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "A key was typed into Button1";
            txtEventLog.Text += "\\r\\nButton: Button1, KeyDown event is invoked";

            if (ViewModel.isCtrlPressedProperty == true)
            {
                lblLog.Text += " Control Pressed";
                ViewModel.isCtrlPressedProperty = false;
            }

            if (e.KeyCode == Keys.ControlKey)
            {
                ViewModel.isCtrlPressedProperty = true;
            }

            if (ViewModel.isShiftPressedProperty == true)
            {
                lblLog.Text += " Shift Pressed";
                ViewModel.isShiftPressedProperty = false;

                if (e.KeyChar == 'g')
                {
                    lblLog.Text += " + g";
                    btnButton1.BackColor = Color.Green;
                    lblLog.Text += "\\r\\nBackColor value: " + btnButton1.BackColor.ToString();
                }
            }

            if (e.KeyCode == Keys.ShiftKey)
            {
                ViewModel.isShiftPressedProperty = true;
            }

            if (e.KeyChar == 'r')
            {
                btnButton1.BackColor = Color.Red;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton1.BackColor.ToString();
            } 
            else if (e.KeyChar == '*')
            {
                btnButton1.BackColor = Color.Transparent;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton1.BackColor.ToString();
            }
            else if (e.KeyChar == ' ')
            {
                btnButton1.BackColor = Color.DeepPink;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton1.BackColor.ToString();
            }
        }

        public void btnButton2_KeyDown(object sender, KeyEventArgs e)
        {
            ButtonElement btnButton2 = this.GetVisualElementById<ButtonElement>("btnButton2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "A key was typed into Button2";
            txtEventLog.Text += "\\r\\nButton: Button2, KeyDown event is invoked";

            if (ViewModel.isCtrlPressedProperty == true)
            {
                lblLog.Text += " Control Pressed";
                ViewModel.isCtrlPressedProperty = false;
            }

            if (e.KeyCode == Keys.ControlKey)
            {
                ViewModel.isCtrlPressedProperty = true;
            }

            if (ViewModel.isShiftPressedProperty == true)
            {
                lblLog.Text += " Shift Pressed";
                ViewModel.isShiftPressedProperty = false;

                if (e.KeyChar == 'g')
                {
                    lblLog.Text += " + g";
                    btnButton2.BackColor = Color.Green;
                    lblLog.Text += "\\r\\nBackColor value: " + btnButton2.BackColor.ToString();
                }
            }

            if (e.KeyCode == Keys.ShiftKey)
            {
                ViewModel.isShiftPressedProperty = true;
            }

            if (e.KeyChar == 'r')
            {
                btnButton2.BackColor = Color.Red;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton2.BackColor.ToString();
            }
            else if (e.KeyChar == '*')
            {
                btnButton2.BackColor = Color.Transparent;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton2.BackColor.ToString();
            }
            else if (e.KeyChar == ' ')
            {
                btnButton2.BackColor = Color.DeepPink;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton2.BackColor.ToString();
            }
        }

        public void btnButton3_KeyDown(object sender, KeyEventArgs e)
        {
            ButtonElement btnButton3 = this.GetVisualElementById<ButtonElement>("btnButton3");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "A key was typed into Button3";
            txtEventLog.Text += "\\r\\nButton: Button3, KeyDown event is invoked";

            if (ViewModel.isCtrlPressedProperty == true)
            {
                lblLog.Text += " Control Pressed";
                ViewModel.isCtrlPressedProperty = false;
            }

            if (e.KeyCode == Keys.ControlKey)
            {
                ViewModel.isCtrlPressedProperty = true;
            }

            if (ViewModel.isShiftPressedProperty == true)
            {
                lblLog.Text += " Shift Pressed";
                ViewModel.isShiftPressedProperty = false;

                if (e.KeyChar == 'g')
                {
                    lblLog.Text += " + g";
                    btnButton3.BackColor = Color.Green;
                    lblLog.Text += "\\r\\nBackColor value: " + btnButton3.BackColor.ToString();
                }
            }

            if (e.KeyCode == Keys.ShiftKey)
            {
                ViewModel.isShiftPressedProperty = true;
            }

            if (e.KeyChar == 'r')
            {
                btnButton3.BackColor = Color.Red;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton3.BackColor.ToString();
            }
            else if (e.KeyChar == '*')
            {
                btnButton3.BackColor = Color.Transparent;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton3.BackColor.ToString();
            }
            else if (e.KeyChar == ' ')
            {
                btnButton3.BackColor = Color.DeepPink;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton3.BackColor.ToString();
            }
        }
    }
}