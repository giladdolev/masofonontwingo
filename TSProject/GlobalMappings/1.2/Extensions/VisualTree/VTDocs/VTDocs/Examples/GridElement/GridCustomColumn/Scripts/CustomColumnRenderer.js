﻿function renderCellWidget(val, meta, rec) {
    console.log(meta.rowIndex);
    // generate unique id for an element
    var id = Ext.id();
    if (meta.rowIndex == 0) {
        Ext.defer(function () {
            Ext.widget('button', {
                renderTo: id,
                text: 'button',
                scale: 'small',
                handler: function () {
                    Ext.Msg.alert("Hello from button")
                }
            });
        }, 50);
    } else if (meta.rowIndex == 1) {
        var itemPath = meta.column.itemPath;
        Ext.defer(function () {

            Ext.widget('checkbox', {
                renderTo: id,
                text: 'checkbox',
                scale: 'small',


                handler: function () {
                    VT_raiseEvent(itemPath, "ColumnAction", { newValue: this.value })
                }
            });
        }, 50);
    } else if (meta.rowIndex == 2) {
        Ext.defer(function () {
            Ext.widget('combo', {
                renderTo: id,
                text: 'combo',
                scale: 'small',
                handler: function () {
                    Ext.Msg.alert("Hello from combo")
                }
            });
        }, 50);
    }
    return Ext.String.format('<div id="{0}"></div>', id);
}