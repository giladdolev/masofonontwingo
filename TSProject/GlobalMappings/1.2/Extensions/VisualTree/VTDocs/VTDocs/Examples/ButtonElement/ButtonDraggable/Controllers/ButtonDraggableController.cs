using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //ButtonAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ButtonElement btnRuntimeDraggable = this.GetVisualElementById<ButtonElement>("btnRuntimeDraggable");

            if (btnRuntimeDraggable.Draggable == false)
            {
                btnRuntimeDraggable.Draggable = true;
                textBox1.Text = btnRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                btnRuntimeDraggable.Draggable = false;
                textBox1.Text = btnRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}