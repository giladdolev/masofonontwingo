using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedPanel1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
        }

        public void btnChangeBorderStyle_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedPanel2.BorderStyle == BorderStyle.None)
            {
                TestedPanel2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Dotted)
            {
                TestedPanel2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Double)
            {
                TestedPanel2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedPanel2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedPanel2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Groove)
            {
                TestedPanel2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Inset)
            {
                TestedPanel2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Dashed)
            {
                TestedPanel2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.NotSet)
            {
                TestedPanel2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Outset)
            {
                TestedPanel2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Ridge)
            {
                TestedPanel2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedPanel2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else if (TestedPanel2.BorderStyle == BorderStyle.Solid)
            {
                TestedPanel2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
            else
            {
                TestedPanel2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedPanel2.BorderStyle + ".";
            }
        }

    }
}