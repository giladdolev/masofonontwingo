<%--<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TabItem BackgroundImageLayout Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Tab runat="server" Text="BackgroundImageLayout is set to Coral" Top="50px"  Left="140px" ID="tabBackgroundImageLayout" Height="200px" Width="250px">
             <TabItems>
                <vt:TabItem runat="server"  BackgroundImageLayout ="Stretch" BackgroundImage ="~/Content/Elements/Tab.png" Text="tabPage1" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" BackgroundImage ="~/Content/Elements/Tab.png" BackgroundImageLayout ="Stretch" Text="tabPage2" Top="22px" Left="4px" ID="TabItem4" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>          

        <vt:Label runat="server" Text="RunTime" Top="300px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Tab runat="server" Text="Runtime BackgroundImageLayout" Top="320px" Left="140px" ID="tabRuntimeBackgroundImageLayout" Height="200px" TabIndex="1" Width="250px" >
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" BackgroundImage ="~/Content/Elements/Tab.png" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" BackgroundImage ="~/Content/Elements/Tab.png" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
            
        </vt:Tab>

        <vt:Button runat="server"  Text="Change Tab BackgroundImageLayout" Top="410px" Left="470px" ID="btnChangeBackgroundImageLayout" Height="36px" TabIndex="1" Width="200px" ClickAction="TabItemBackgroundImageLayout\btnChangeBackgroundImageLayout_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="570px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>--%>

<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem BackgroundImageLayout Property" ID="windowView" LoadAction="TabTabItemBackgroundImageLayout\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImageLayout" Left="240px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image position on the control (Center , None, Stretch, Tile or Zoom).
            Tile is the default value.
            
            Syntax: public virtual ImageLayout BackgroundImageLayout { get; set; }" Top="75px"  ID="lblDefinition1"></vt:Label>
      
        
        <vt:Tab runat="server" Text="Tab" Top="160px" ID="TestedTab1">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" BackgroundImage ="~/Content/Images/icon.jpg" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3"  Top="22px" Left="4px" ID="Tab1Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="270px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" Text="BackgroundImageLayout takes effect only if the BackgroundImage property is set." Top="295px"  ID="lblDefinition2"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, the initial BackgroundImageLayout of 'tabPage1' is set to 'Center'.
            Use the buttons below (None/ Stretch/ Center/ Zoom/ Tile) to change the TestedButton 
            Background Image Layout." Top="380px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tab runat="server" Text="TestedTab" Top="445px" ID="TestedTab2" TabReference="">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" BackgroundImage ="~/Content/Images/icon.jpg" BackgroundImageLayout="Center" ID="Tab2Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab2Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px"  ID="Tab2Item3"  Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="555px" ID="lblLog2"></vt:Label>

         <vt:Button runat="server" Text="Tile"  Top="605px" Left="560px" ID="btnTile" Height="30px" TabIndex="1" Width="60px" ClickAction="TabTabItemBackgroundImageLayout\btnTile_Click"></vt:Button>  
         <vt:Button runat="server" Text="Center"   Top="605px" Left="340px" ID="btnCenter" Height="30px" TabIndex="1" Width="60px" ClickAction="TabTabItemBackgroundImageLayout\btnCenter_Click"></vt:Button>  
         <vt:Button runat="server" Text="Stretch"   Top="605px" Left="230px" ID="btnStretch" Height="30px" TabIndex="1" Width="60px" ClickAction="TabTabItemBackgroundImageLayout\btnStretch_Click"></vt:Button>  
         <vt:Button runat="server" Text="Zoom"  Top="605px" Left="450px" ID="btnZoom" Height="30px" TabIndex="1" Width="60px" ClickAction="TabTabItemBackgroundImageLayout\btnZoom_Click"></vt:Button>  
         <vt:Button runat="server" Text="None" Top="605px" Left="120px" ID="btnNone" Height="30px" TabIndex="1" Width="60px" ClickAction="TabTabItemBackgroundImageLayout\btnNone_Click"></vt:Button> 
 
    </vt:WindowView>
</asp:Content>



