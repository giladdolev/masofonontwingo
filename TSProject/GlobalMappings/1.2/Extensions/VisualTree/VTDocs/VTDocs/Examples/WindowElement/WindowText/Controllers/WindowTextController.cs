using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowTextController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowText());
        }
        private WindowText ViewModel
        {
            get { return this.GetRootVisualElement() as WindowText; }
        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.Text == "Window Text Property")
            {
                this.ViewModel.Text = "The new Text :  ***************Window Text Property*************";
                textBox1.Text = this.ViewModel.Text.ToString();
            }
            else
            {
                this.ViewModel.Text = "Window Text Property";
                textBox1.Text = this.ViewModel.Text.ToString();
            }
        }

        //btnOpenWindow2
        public void btnOpenWindow2_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowText");

            ViewModel.TestedWin2 = VisualElementHelper.CreateFromView<Window2TextChanged>("Window2TextChanged", "Index2", null, argsDictionary, null);
            this.ViewModel.TestedWin2.Show();
        }


    }
}