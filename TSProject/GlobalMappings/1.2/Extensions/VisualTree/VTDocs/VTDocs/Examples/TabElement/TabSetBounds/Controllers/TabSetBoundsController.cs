using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //TabAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested Tab bounds are set to: \\r\\nLeft  " + TestedTab.Left + ", Top  " + TestedTab.Top + ", Width  " + TestedTab.Width + ", Height  " + TestedTab.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            if (TestedTab.Left == 100)
            {
                TestedTab.SetBounds(80, 300, 300, 100);
                lblLog.Text = "The Tested Tab bounds are set to: \\r\\nLeft  " + TestedTab.Left + ", Top  " + TestedTab.Top + ", Width  " + TestedTab.Width + ", Height  " + TestedTab.Height;

            }
            else
            {
                TestedTab.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested Tab bounds are set to: \\r\\nLeft  " + TestedTab.Left + ", Top  " + TestedTab.Top + ", Width  " + TestedTab.Width + ", Height  " + TestedTab.Height;
            }
        }

    }
}