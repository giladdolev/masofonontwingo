using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowCancelButtonController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowCancelButton());
        }
        private WindowCancelButton ViewModel
        {
            get { return this.GetRootVisualElement() as WindowCancelButton; }
        }

        public void btnChangeCancelButton_Click(object sender, EventArgs e)
		{
            ButtonElement btnCancelButton = this.GetVisualElementById<ButtonElement>("btnCancelButton");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.CancelButton == null)
			{
                this.ViewModel.CancelButton = btnCancelButton;
                textBox1.Text = this.ViewModel.CancelButton.ToString();
			}
			else
            {
                this.ViewModel.CancelButton = null;
                textBox1.Text = "CancelButton Property is not set";
            }
		}
        public void btnCancelButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("CancelButton Click method is invoked");
        }

    }
    
}