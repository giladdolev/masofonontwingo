using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedGrid.ClientRectangle;

        }
        public void btnChangeGridClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            if (TestedGrid.Left == 100)
            {
                TestedGrid.SetBounds(80, 320, 285, 115);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedGrid.ClientRectangle;

            }
            else
            {
                TestedGrid.SetBounds(100, 310, 300, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedGrid.ClientRectangle;
            }

        }

    }
}