 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer ToString Method" ID="windowView2" LoadAction="SplitContainerToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a String containing the name of the Component, if any.

            Syntax: public override string ToString()
            
            Return Value: A String containing the name of the Component, if any, or null if the Component is unnamed." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this SplitContainer." 
            Top="265px" ID="lblExp2"></vt:Label>

        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="330px" ID="TestedSplitContainer"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="425px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="530px" ID="btnToString" ClickAction="SplitContainerToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>