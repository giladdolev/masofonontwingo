using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage1 = this.GetVisualElementById<ImageElement>("TestedImage1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedImage1.BackColor.ToString();

            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedImage2.BackColor.ToString();
            
        }

   
        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedImage2.BackColor == Color.Orange)
            {
                TestedImage2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedImage2.BackColor.ToString();
            }
            else
            {
                TestedImage2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedImage2.BackColor.ToString();

            }
        }

    }
}