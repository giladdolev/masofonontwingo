using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BaseViewController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult BaseView()
        {
            return View();
        }

        public void buttonOK_Click(object sender, EventArgs e)
        {
            MessageBox.Show("OK button clicked", "Base View");
        }
    }
}