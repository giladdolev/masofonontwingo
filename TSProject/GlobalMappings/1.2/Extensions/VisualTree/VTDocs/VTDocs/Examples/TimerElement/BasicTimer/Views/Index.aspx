<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Timer" Top="57px" Left="11px" ID="windowView1" Height="480px" Width="640px" WindowClosedAction="BasicTimer\WindowClosed">
		
        <vt:TextBox runat="server" ID="textBox1" Multiline="true" Width="300px" Height="200px" AutoSize="false" Top="12px" Left="12px"></vt:TextBox>
        <vt:Button runat="server" ID="btnToggleTimer1" Text="Start Timer 1" ClickAction="BasicTimer\btnTimerToggle1_Click"
            TextAlign="MiddleCenter" Top="212px" Left="12px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" 
            ForeColor="Black" Height="41px" Width="135px">
		</vt:Button>


        <vt:Button runat="server" ID="btnTxtCleaner1" Text="Clear TextBox 2" ClickAction="BasicTimer\btnTxtCleaner1_Click"
            TextAlign="MiddleCenter" Top="212px" Left="150px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" 
            ForeColor="Black" Height="41px" Width="135px">
		</vt:Button>


        <vt:TextBox runat="server" ID="textBox2" Multiline="true" Width="300px" Height="200px" AutoSize="false" Top="12px" Left="312px"></vt:TextBox>
        <vt:Button runat="server" ID="btnToggleTimer2" Text="Start Timer 2" ClickAction="BasicTimer\btnTimerToggle2_Click"
            TextAlign="MiddleCenter" Top="212px" Left="312px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" 
            ForeColor="Black" Height="41px" Width="135px">
		</vt:Button>

        <vt:Button runat="server" ID="btnTxtCleaner2" Text="Clear TextBox 2" ClickAction="BasicTimer\btnTxtCleaner2_Click"
            TextAlign="MiddleCenter" Top="212px" Left="450px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" 
            ForeColor="Black" Height="41px" Width="135px">
		</vt:Button>
     </vt:WindowView>



    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="timer1" Enabled="false" Interval="1000" TickAction="BasicTimer\timer1_Tick">
		</vt:Timer>
        <vt:Timer runat="server" ID="timer2" Enabled="false" Interval="3000" TickAction="BasicTimer\timer2_Tick">
		</vt:Timer>
	</vt:ComponentManager>
</asp:Content>