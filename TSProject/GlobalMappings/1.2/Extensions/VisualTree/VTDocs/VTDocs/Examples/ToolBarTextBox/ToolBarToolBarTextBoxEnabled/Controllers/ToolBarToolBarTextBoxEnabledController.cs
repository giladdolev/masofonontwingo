using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarToolBarTextBoxEnabledController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "New TextBox Enabled value: " + TestedToolBar1.Items[0].Enabled.ToString();
            lblLog1.Text += "\\r\\nOpen TextBox Enabled value: " + TestedToolBar1.Items[2].Enabled.ToString();

            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "New TextBox Enabled value: " + TestedToolBar2.Items[0].Enabled.ToString();
            lblLog2.Text += "\\r\\nOpen TextBox Enabled value: " + TestedToolBar2.Items[2].Enabled.ToString();
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ComboBoxElement cmbSelectItem = this.GetVisualElementById<ComboBoxElement>("cmbSelectItem");
            CheckBoxElement chkEnabled = this.GetVisualElementById<CheckBoxElement>("chkEnabled");

            switch (cmbSelectItem.Text)
            {
                case "New":
                    TestedToolBar2.Items[0].Enabled = chkEnabled.IsChecked;
                    PrintAllColumnsReadOnly();
                    break;
                case "Open":
                    TestedToolBar2.Items[2].Enabled = chkEnabled.IsChecked;
                    PrintAllColumnsReadOnly();
                    break;
                default:
                    lblLog2.Text = "Please Select a TextBox from the first ComboBox";
                    break;
            }

        }

        private void PrintAllColumnsReadOnly()
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "New TextBox Enabled value: " + TestedToolBar2.Items[0].Enabled.ToString();
            lblLog2.Text += "\\r\\nOpen TextBox Enabled value: " + TestedToolBar2.Items[2].Enabled.ToString();

        }

    }
}