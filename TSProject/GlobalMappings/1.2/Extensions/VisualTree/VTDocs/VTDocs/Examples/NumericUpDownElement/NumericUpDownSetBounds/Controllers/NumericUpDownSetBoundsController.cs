using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested NumericUpDown bounds are set to: \\r\\nLeft  " + TestedNumericUpDown.Left + ", Top  " + TestedNumericUpDown.Top + ", Width  " + TestedNumericUpDown.Width + ", Height  " + TestedNumericUpDown.Height;

        }
        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            if (TestedNumericUpDown.Left == 100)
            {
                TestedNumericUpDown.SetBounds(80, 300, 150, 24);
                lblLog.Text = "The Tested NumericUpDown bounds are set to: \\r\\nLeft  " + TestedNumericUpDown.Left + ", Top  " + TestedNumericUpDown.Top + ", Width  " + TestedNumericUpDown.Width + ", Height  " + TestedNumericUpDown.Height;

            }
            else
            {
                TestedNumericUpDown.SetBounds(100, 280, 200, 50);
                lblLog.Text = "The Tested NumericUpDown bounds are set to: \\r\\nLeft  " + TestedNumericUpDown.Left + ", Top  " + TestedNumericUpDown.Top + ", Width  " + TestedNumericUpDown.Width + ", Height  " + TestedNumericUpDown.Height;
            }
        }

    }
}