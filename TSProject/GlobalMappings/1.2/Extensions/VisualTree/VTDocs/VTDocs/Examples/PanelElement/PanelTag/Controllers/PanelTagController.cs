using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (TestedPanel1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedPanel1.Tag;
            }

            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            if (TestedPanel2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedPanel2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedPanel2.Tag == "New Tag.")
            {
                TestedPanel2.Tag = TestedPanel2;
                lblLog2.Text = "Tag value: " + TestedPanel2.Tag;
            }
            else
            {
                TestedPanel2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedPanel2.Tag;

            }
        }

    }
}