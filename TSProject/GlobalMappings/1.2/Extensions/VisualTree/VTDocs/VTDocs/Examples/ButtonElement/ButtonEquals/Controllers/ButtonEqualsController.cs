using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new ButtonEquals());
        }

        private ButtonEquals ViewModel
        {
            get { return this.GetRootVisualElement() as ButtonEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ButtonElement button1 = this.GetVisualElementById<ButtonElement>("button1");
            ButtonElement button2 = this.GetVisualElementById<ButtonElement>("button2");


            if (this.ViewModel.TestedButton.Equals(button1))
            {
                this.ViewModel.TestedButton = button2;
                textBox1.Text = "TestedButton Equals button2";
            }
            else
            {
                this.ViewModel.TestedButton = button1;
                textBox1.Text = "TestedButton Equals button1";
            }

            
        }

    }
}