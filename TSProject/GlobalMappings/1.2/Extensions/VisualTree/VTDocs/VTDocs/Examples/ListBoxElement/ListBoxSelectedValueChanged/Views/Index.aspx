<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListBox SelectedValueChanged Event" ID="windowView2" Height="800px" LoadAction="ListBoxSelectedValueChanged\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="SelectedValueChanged" Left="250px" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Occurs when the SelectedValue property changes.

            Syntax: public event EventHandler SelectedValueChanged
            If the ValueMember is empty, an InvalidOperationException will be thrown when trying to set the 
            SelectedValue." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="180px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, The middle and Right ListBox uses a DataSource. In The middle ListBox 
            ValueMember property is not defined. You can try to change the SelectedValue by clicking the buttons,
            or by manually clicking on an item from the ListBox. 
            Each invocation of the 'SelectedValueChanged' event should add a line in the 'Event Log'." Top="220px" ID="lblExp2"></vt:Label>

        <vt:ListBox runat="server" Text="TestedListBox" Top="340px" Height="120px" ID="TestedListBox" SelectedValueChangedAction="ListBoxSelectedValueChanged\TestedListBox_SelectedValueChanged">
           <Items>
                <vt:ListItem runat="server" ID="It1" Text="It1" Value="It1"></vt:ListItem>
                <vt:ListItem runat="server" ID="It2" Text="It2" Value="It2"></vt:ListItem>
            </Items>
        </vt:ListBox>

        <vt:ListBox runat="server" Text="TestedListBoxDataSourceNoValueMember" Top="340px" Left="270px" Height="120px" ID="TestedListBoxDataSourceNoValueMember" SelectedValueChangedAction="ListBoxSelectedValueChanged\TestedListBoxDataSourceNoValueMember_SelectedValueChanged"></vt:ListBox>

        <vt:ListBox runat="server" Text="TestedListBoxDataSource" Top="340px" Left="460px" Height="120px" ID="TestedListBoxDataSource" SelectedValueChangedAction="ListBoxSelectedValueChanged\TestedListBoxDataSource_SelectedValueChanged"></vt:ListBox>

        <vt:Label runat="server" SkinID="Log" Width="450px" ID="lblLog" Top="485px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="525px" Width="450px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select It3 >>" Top="675px"  ID="btnSelectIt3" ClickAction="ListBoxSelectedValueChanged\btnSelectIt3_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Item3 >>" Top="675px" left="270px" ID="btnSelectItem3" ClickAction="ListBoxSelectedValueChanged\btnSelectItem3_Click"></vt:Button>

        <vt:Button runat="server" Text="Select DisplayItem3 >>" Top="675px" left="460px" ID="Button1" ClickAction="ListBoxSelectedValueChanged\btnSelectDisplayItem3_Click"></vt:Button>

        <vt:Button runat="server" Text="Clear log >>" Top="730px" ID="btnClearLog" ClickAction="ListBoxSelectedValueChanged\btnClearLog_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

