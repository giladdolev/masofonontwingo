<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="NumericUpDown MaximumSize" ID="windowView1" LoadAction="NumericUpDownMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the maximum size of the spin box (also known as an up-down control). 

            Syntax: public virtual Size MaximumSize { get; set; } 

            Value: The Size, which is the maximum size of the spin box." Top="75px" ID="lblDefinition"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedNumericUpDown' is width: 250px 
            and height: 50px. Its MaximumSize is initially set to width: 150px and height: 70px.
            To cancel 'TestedNumericUpDown' MaximumSize, set width and height values to 0px." Top="230px" ID="Label1"></vt:Label>
        
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="320px" ID="TestedNumericUpDown" Height="50px" Width="250px" MaximumSize="150, 70" TabIndex="1"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="390px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="450px" width="200px" Left="80px" ID="btnChangeNudMaximumSize" ClickAction="NumericUpDownMaximumSize\btnChangeNudMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="450px" width="200px" Left="310px" ID="btnSetToZeroNudMaximumSize" ClickAction="NumericUpDownMaximumSize\btnSetToZeroNudMaximumSize_Click"></vt:Button>

    
    </vt:WindowView>
</asp:Content>
