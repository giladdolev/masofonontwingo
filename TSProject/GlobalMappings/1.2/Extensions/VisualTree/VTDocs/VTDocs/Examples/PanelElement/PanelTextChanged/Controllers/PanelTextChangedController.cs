using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelTextChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //PanelAlignment
        public void btnChangeText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement pnlText = this.GetVisualElementById<PanelElement>("pnlText");

            if (pnlText.Text == "Tested Panel") //Get
            {
                pnlText.Text = "NewText"; //Set
                textBox1.Text = "The Text property value is set to: " + pnlText.Text;
            }
            else 
            {
                pnlText.Text = "Tested Panel";
                textBox1.Text = "The Text property value is set back to: " + pnlText.Text;
            }
        }

      
        public void pnlText_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "TextChanged Event is invoked\\r\\n";
        }
    }
}