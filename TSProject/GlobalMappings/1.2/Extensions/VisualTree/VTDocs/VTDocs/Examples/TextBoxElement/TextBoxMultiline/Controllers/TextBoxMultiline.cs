using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxMultilineController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeMultiline_Click(object sender, EventArgs e)
        {
            TextBoxElement txtMultilineChange = this.GetVisualElementById<TextBoxElement>("txtMultilineChange");

            if (txtMultilineChange.Multiline == true)
            {
                txtMultilineChange.Multiline = false;
            }
            else
            {
                txtMultilineChange.Multiline = true;
            }
            
            
        }

    }
}