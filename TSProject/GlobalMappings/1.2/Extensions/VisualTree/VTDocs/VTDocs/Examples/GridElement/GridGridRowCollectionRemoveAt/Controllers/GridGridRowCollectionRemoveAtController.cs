﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridGridRowCollectionRemoveAtController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void Form_load(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            lblLog1.Text = "Insert a row index and press the button.";
        }

        
        public void RemoveAt_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            int val;
            if (int.TryParse(txtInput.Text, out val))
            {
                if (val > -1 && val < gridElement.Rows.Count)
                {
                    gridElement.Rows.RemoveAt(val);
                    lblLog1.Text = "Row index " + val + " was removed.";

                }
                else
                {
                    lblLog1.Text = "Index is out of range";
                }
            }
            else
            {
                lblLog1.Text = "Please enter a row index number";
            }

        }

        public void TextBox_TextChanged(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "Inserted Value: " + txtInput.Text + "...";

        }
    }
}
