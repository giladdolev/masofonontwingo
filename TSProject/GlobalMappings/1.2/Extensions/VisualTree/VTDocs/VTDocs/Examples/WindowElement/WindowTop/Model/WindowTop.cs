﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowTop : WindowElement
    {
         TestedWindowTop _TestedWin;

         public TestedWindowTop TestedWin
        {
            get { return this._TestedWin; }
            set { this._TestedWin = value; }
        }
        public WindowTop()
        {

        }

    }
}