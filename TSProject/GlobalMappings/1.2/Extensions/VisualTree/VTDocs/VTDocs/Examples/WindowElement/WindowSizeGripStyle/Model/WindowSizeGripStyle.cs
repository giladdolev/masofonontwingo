﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowSizeGripStyle : WindowElement
    {

        SizeGripStyleTestedWindow _TestedWin;

        public WindowSizeGripStyle()
        {

        }

        public SizeGripStyleTestedWindow TestedWin
        {
            get { return this._TestedWin; }
            set { this._TestedWin = value; }
        }
       

    }
}