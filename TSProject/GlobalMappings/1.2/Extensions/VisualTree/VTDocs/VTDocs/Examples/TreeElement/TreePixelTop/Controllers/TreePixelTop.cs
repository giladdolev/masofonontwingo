using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreePixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             lblLog.Text = "PixelTop Value: " + TestedTree.PixelTop + '.';

         }
         public void btnChangePixelTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
             if (TestedTree.PixelTop == 285)
             {
                 TestedTree.PixelTop = 300;
                 lblLog.Text = "PixelTop Value: " + TestedTree.PixelTop + '.';

             }
             else
             {
                 TestedTree.PixelTop = 285;
                 lblLog.Text = "PixelTop Value: " + TestedTree.PixelTop + '.';
             }

         }

    }
}