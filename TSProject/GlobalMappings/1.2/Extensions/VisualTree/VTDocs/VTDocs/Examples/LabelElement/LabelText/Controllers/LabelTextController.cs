using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Text  property value: " + TestedLabel.Text;

        }

        public void btnChangeLabelText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            if (TestedLabel.Text == "Tested<>?\"{}[]789456123\'0-Label")
            {
                TestedLabel.Text = "New!@#$%^&*()_+ Text";
                lblLog.Text = "Text  property value: " + TestedLabel.Text;

            }
            else
            {
                TestedLabel.Text = "Tested<>?\"{}[]789456123\'0-Label";
                lblLog.Text = "Text  property value: " + TestedLabel.Text;
            }

        }
 
    }
}