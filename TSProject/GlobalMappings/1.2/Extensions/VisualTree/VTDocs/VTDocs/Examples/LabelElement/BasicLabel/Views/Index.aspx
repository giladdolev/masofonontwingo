<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Label" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Text set in designer" Top="110px" Left="160px" ID="label1" Height="13px" Width="350px" Font-Italic="true" AutoSize="true"></vt:Label>
        <vt:Button runat="server" Text="Change Label text" Top="44px" Left="140px" ID="button1" Height="36px" TabIndex="1" Width="150px" ClickAction="BasicLabel\button1_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Label style" Top="44px" Left="310px" ID="button2" Height="36px" TabIndex="1" Width="150px" ClickAction="BasicLabel\button2_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Label border" Top="44px" Left="480px" ID="button3" Height="36px" TabIndex="1" Width="150px" ClickAction="BasicLabel\button3_Click"></vt:Button>
        <vt:Label runat="server" Top="150px" Left="160px" ID="label3" Height="20px" Text="Label with border" Width="200" BorderStyle="Solid"></vt:Label>
        <vt:Label runat="server" Top="150px" Left="400px" ID="label4" Height="20px" Text="Label with dotted border" BorderStyle="Dotted"></vt:Label>
        <vt:Label runat="server" Top="190px" Left="160px" ID="label2" Height="13px" Width="0px" ></vt:Label>
        <vt:Label runat="server" AutoEllipsis="True" Text="label11111111111111111111111111111111111
            111111" Top="230px" Left="160px" ID="label5" BackColor="Yellow" BorderStyle="Solid" Height="23px" Width="100px"></vt:Label>
        <vt:Button runat="server" Text="Change Label AutoEllipsis" Top="230px" Left="480px" ID="button4" Height="36px" TabIndex="1" Width="150px" ClickAction="BasicLabel\btnAutoEllipsis_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
