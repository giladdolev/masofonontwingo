using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarBackColorController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedToolBar1.BackColor.ToString();

            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedToolBar2.BackColor.ToString();

        }


        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedToolBar2.BackColor == Color.Orange)
            {
                TestedToolBar2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedToolBar2.BackColor.ToString();
            }
            else
            {
                TestedToolBar2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedToolBar2.BackColor.ToString();

            }
        }
        //public void btnChangeBackColor_Click(object sender, EventArgs e)
        //{
        //    TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
        //    ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
        //    if (toolStrip1.BackColor == Color.Yellow)
        //    {
        //        toolStrip1.BackColor = Color.Red;
        //        textBox1.Text = toolStrip1.BackColor.ToString();
        //    }
        //    else
        //    {
        //        toolStrip1.BackColor = Color.Yellow;
        //        textBox1.Text = toolStrip1.BackColor.ToString();
        //    }
                
        //}

    }
}