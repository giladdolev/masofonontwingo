<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Cursor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent - Cursor is set to .. bug" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" ></vt:Label>     

        <vt:Tab runat="server" Text="" Cursor="System.Web.VisualTree.Elements.CursorElement" Top="45px" Left="140px" ID="tabCursor" Height="150px" Width="200px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem4" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>           

        <vt:Label runat="server" Text="Tab Cursor - runtime" Top="230px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" ></vt:Label>

        <vt:Tab runat="server" Text="" Top="245px" Left="140px" ID="tabRuntimeCursor" Height="150px" TabIndex="1" Width="200px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Button runat="server" Text="Change Tab Cursor" Top="260px" Left="400px" ID="btnChangeCursor" Height="36px" TabIndex="1" Width="200px" ClickAction="TabCursor\btnChangeCursor_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="430px" Left="110px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
