using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedListView1.BorderStyle + '.';
            lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
        }

        public void btnChangeListViewBorderStyle_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedListView2.BorderStyle == BorderStyle.None)
            {
                TestedListView2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Dotted)
            {
                TestedListView2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Double)
            {
                TestedListView2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedListView2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedListView2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Groove)
            {
                TestedListView2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Inset)
            {
                TestedListView2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Dashed)
            {
                TestedListView2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.NotSet)
            {
                TestedListView2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Outset)
            {
                TestedListView2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Ridge)
            {
                TestedListView2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedListView2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else if (TestedListView2.BorderStyle == BorderStyle.Solid)
            {
                TestedListView2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
            else
            {
                TestedListView2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedListView2.BorderStyle;
            }
        }
    }
}