using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowTransparencyKeyController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowTransparencyKey());
        }
        private WindowTransparencyKey ViewModel
        {
            get { return this.GetRootVisualElement() as WindowTransparencyKey; }
        }

        public void btnChangeTransparencyKey_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.TransparencyKey == Color.Blue) //Get
            {
                this.ViewModel.TransparencyKey = Color.Yellow; //Set
                textBox1.Text = "TransparencyKey value: " + this.ViewModel.TransparencyKey;
            }
            else if (this.ViewModel.TransparencyKey == Color.Yellow)
            {
                this.ViewModel.TransparencyKey = Color.Red;
                textBox1.Text = "TransparencyKey value:" + this.ViewModel.TransparencyKey;
            }
            else
            {
                this.ViewModel.TransparencyKey = Color.Blue;
                textBox1.Text = "TransparencyKey value:" + this.ViewModel.TransparencyKey;
            }
        }
    }
}