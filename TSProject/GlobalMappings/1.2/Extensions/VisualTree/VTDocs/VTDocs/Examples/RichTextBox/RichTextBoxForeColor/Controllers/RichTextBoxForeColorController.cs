using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox1 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox1");
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedRichTextBox1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedRichTextBox2.ForeColor + '.';
        }

        public void btnChangeRichTextBoxForeColor_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedRichTextBox2.ForeColor == Color.Green)
            {
                TestedRichTextBox2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedRichTextBox2.ForeColor + '.';
            }
            else
            {
                TestedRichTextBox2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedRichTextBox2.ForeColor + '.';
            }
        }

    }
}