<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="TextBox MinimumSize" ID="windowView1" LoadAction="TextBoxMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedTextBox' is width: 70px and height: 40px. 
            Its MinimumSize is initially set to width: 210px and height: 50px.
            To cancel 'TestedTextBox' MinimumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>

        <vt:TextBox runat="server" Left="80px" Height="40px" Width="70px" Top="350px" Text="TestedTextBox" ID="TestedTextBox" MinimumSize="210, 50"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="435px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="520px" width="200px" Left="80px" ID="btnChangeTxtMinimumSize" ClickAction="TextBoxMinimumSize\btnChangeTxtMinimumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="520px" width="200px" Left="310px" ID="btnSetToZeroTxtMinimumSize" ClickAction="TextBoxMinimumSize\btnSetToZeroTxtMinimumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>
























