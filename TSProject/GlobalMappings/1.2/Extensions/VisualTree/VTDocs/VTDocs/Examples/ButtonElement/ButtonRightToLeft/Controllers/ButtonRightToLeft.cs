using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        // Change RightToLeft property
        /// <summary>
        /// Handles the Click event of the btnChangeRightToLeft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeRightToLeft = this.GetVisualElementById<ButtonElement>("btnChangeRightToLeft");
            if (btnChangeRightToLeft.RightToLeft == RightToLeft.No)
            {
                btnChangeRightToLeft.RightToLeft = RightToLeft.Yes;
            }
            else if (btnChangeRightToLeft.RightToLeft == RightToLeft.Yes)
            {
                btnChangeRightToLeft.RightToLeft = RightToLeft.Inherit;
            }
            else if (btnChangeRightToLeft.RightToLeft == RightToLeft.Inherit)
            {
                btnChangeRightToLeft.RightToLeft = RightToLeft.No;
            }
        }

    }
}