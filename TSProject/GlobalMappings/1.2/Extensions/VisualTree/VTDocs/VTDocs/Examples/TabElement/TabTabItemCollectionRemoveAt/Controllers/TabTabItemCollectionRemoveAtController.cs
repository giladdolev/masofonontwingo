using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemCollectionRemoveAtController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Insert index number of TabItem and press the button ";

            InsertTabs();
        }

        public void InsertTabs()
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");

            TestedTab1.TabItems.Clear();

            int count = 0;
            while (count < 3)
            {
                TestedTab1.TabItems.Add(new TabItem());
                count = TestedTab1.TabItems.Count;
                TestedTab1.TabItems[count-1].Text = "tabPage" + count;
            }

            TestedTab1.SelectedIndex = 0;
        }

        public void btnReload_Click(object sender, EventArgs e)
        {
            InsertTabs();
        }

        public void RemoveTabItem_Click(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

            int removeIndex;

            if (int.TryParse(txtInput.Text, out removeIndex))
            {
                if (removeIndex > -1 && removeIndex < TestedTab1.TabItems.Count)
                {
                    TestedTab1.TabItems.RemoveAt(removeIndex);
                    lblLog.Text = "TabItem in index " + removeIndex + " was removed.";
                }
                else
                {
                    lblLog.Text = "Index is out of range";

                }
            }
            else
            {
                lblLog.Text = "Number is required - Enter one of the tab indexes";
            }

        }

        private void btnAddTabItem_Click(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTab1.TabItems.Add("New TabItem");
            lblLog.Text = "A new TabItem was added";

        }

        public void txtInput_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

            lblLog.Text = "TabItem index: " + txtInput.Text + '.';
        }


    }
}