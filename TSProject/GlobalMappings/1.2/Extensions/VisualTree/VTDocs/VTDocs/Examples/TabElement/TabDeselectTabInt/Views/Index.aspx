<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab DeselectTab Method (Int)" ID="windowView" LoadAction="TabDeselectTabInt\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="DeselectTab" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Makes the tab following the tab with the specified index the current tab.

            If there are at least two tabs in the control, the tab following the specified tab becomes the current tab.
             If the specified tab is the last tab in the control, the first tab becomes the current tab.

            Syntax: public void DeselectTab(int index)
            index - The index in the TabPages collection of the tab to deselect.
            
            Exceptions: ArgumentOutOfRangeException - Occur when
            	 index is less than 0 or greater than the number of TabItem controls in the TabItems collection minus 1." 
            Top="75px" ID="lblDefinition"></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="285px"  ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="In the following example you can invoke the DeselectTab(Int) by entering tab index
             and clicking the DeselectTab button." Top="335px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tab runat="server" Text="TestedTab" Top="415px" ID="TestedTab">
            <TabItems>
                <vt:TabItem runat="server" Text="TabItem1" Top="22px" Left="4px" ID="tabItem1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="TabItem2" Top="22px" Left="4px" ID="tabItem2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="TabItem3" Top="22px" Left="4px" ID="tabItem3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="530px" Height="40" Width="450" ID="lblLog"></vt:Label>

         <vt:Label runat="server" Text="Enter tab index: " Left="80px" ID="lblInput" Top="595px"></vt:Label>

        <vt:TextBox runat="server" Top="595px" Left="190px" Width="45px" ID="txtInput" ForeColor="Gray" Text="Index" TextChangedAction="TabDeselectTabInt\txtInput_TextChanged" GotFocusAction="TabDeselectTabInt\txtInput_GotFocus"></vt:TextBox>

        <vt:Button runat="server" Text="DeselectTab >>" Left="280px" Top="595px" ID="btnDeselectTab" ClickAction="TabDeselectTabInt\btnDeselectTab_Click"></vt:Button>

             
    </vt:WindowView>
</asp:Content>


