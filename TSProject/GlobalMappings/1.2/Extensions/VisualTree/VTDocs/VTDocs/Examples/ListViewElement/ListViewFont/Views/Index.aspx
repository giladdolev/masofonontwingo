<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Font Property" ID="windowView2" LoadAction="ListViewFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:ListView runat="server" Text="ListView" Top="180px" ID="TestedListView1" >
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="290px" Width="430px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedListView is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="400px"  ID="lblExp1"></vt:Label>     
              
        <vt:ListView runat="server" Text="TestedListView" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="455px" ID="TestedListView2" >
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>
        <vt:Label runat="server" SkinID="Log" Top="565px" Width="430px" Height="45px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="640px" ID="btnChangeListViewFont" ClickAction="ListViewFont/btnChangeListViewFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
