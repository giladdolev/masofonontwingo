using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxDropDownStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox1.Items.Add("Item1");
            TestedComboBox1.Items.Add("Item2");
            TestedComboBox1.Items.Add("Item3");
            lblLog1.Text = "DropDownStyle Value: " + TestedComboBox1.DropDownStyle.ToString();
            TestedComboBox2.Items.Add("Item1");
            TestedComboBox2.Items.Add("Item2");
            TestedComboBox2.Items.Add("Item3");
            lblLog2.Text = "DropDownStyle Value: " + TestedComboBox2.DropDownStyle.ToString() + ".";
        }

        private void btnChangeDropDownStyle_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedComboBox2.DropDownStyle == ComboBoxStyle.DropDown)
            {
                TestedComboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            else if (TestedComboBox2.DropDownStyle == ComboBoxStyle.DropDownList)
            {
                TestedComboBox2.DropDownStyle = ComboBoxStyle.Simple;
            }
            else if (TestedComboBox2.DropDownStyle == ComboBoxStyle.Simple)
            {
                TestedComboBox2.DropDownStyle = ComboBoxStyle.DropDown;
            }
            lblLog2.Text = "DropDownStyle Value: " + TestedComboBox2.DropDownStyle.ToString() + ".";
        }
    }
}