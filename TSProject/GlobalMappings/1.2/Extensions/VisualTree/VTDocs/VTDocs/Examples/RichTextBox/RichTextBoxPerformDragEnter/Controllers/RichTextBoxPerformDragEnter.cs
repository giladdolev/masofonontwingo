using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPerformDragEnterController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformDragEnter_Click(object sender, EventArgs e)
        {
            DragEventArgs args = new DragEventArgs();
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");

            testedRichTextBox.PerformDragEnter(args);
        }

        public void testedRichTextBox_DragEnter(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRichTextBox DragEnter event method is invoked");
        }

    }
}