using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridCurrentCellChangedController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}


        //private void Page_load(object sender, EventArgs e)
        //{
        //    GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
        //    LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
        //    TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
        //    TestedGrid1.Rows.Add("a", "b", "c");
        //    TestedGrid1.Rows.Add("d", "e", "f");
        //    TestedGrid1.Rows.Add("g", "h", "i");
        //    lblLog1.Text = "Current Cell Text : ";
        //}

        //public void Cell_Changed(object sender, EventArgs e)
        //{
        //    GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
        //    LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
        //    TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

        //    lblLog1.Text = "Current Cell Text : " + TestedGrid1.CurrentCell.Value;

        //    txtEventLog.Text += "\\r\\nCurrentCellChanged event is invoked.";

        //}

        public void Form_load(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");

            lblLog1.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog1.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;

            txtEventLog.Text += "\\r\\nCurrentCellChanged event is invoked.";

        }

        public void btnChangeCurrentCell_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbIndexes = this.GetVisualElementById<ComboBoxElement>("cmbIndexes");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            gridElement.CurrentCell = gridElement.Rows[1].Cells[2];

            lblLog1.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;

        }
	}
}
