using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreePixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Tree PixelLeft Value is: " + TestedTree.PixelLeft + '.';
        }
        public void ChangePixelLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            if (TestedTree.PixelLeft == 200)
            {
                TestedTree.PixelLeft = 80;
                lblLog.Text = "Tree PixelLeft Value is: " + TestedTree.PixelLeft + '.';

            }
            else
            {
                TestedTree.PixelLeft = 200;
                lblLog.Text = "Tree PixelLeft Value is: " + TestedTree.PixelLeft + '.';
            }
        }

    }
}