using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        // Change RightToLeft property
        /// <summary>
        /// Handles the Click event of the btnChangeRightToLeft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (testedProgressBar.RightToLeft == RightToLeft.No)
            {
                testedProgressBar.RightToLeft = RightToLeft.Yes;
                textBox1.Text = "testedProgressBar RightToLeft: " + testedProgressBar.RightToLeft;
            }
            else if (testedProgressBar.RightToLeft == RightToLeft.Yes)
            {
                testedProgressBar.RightToLeft = RightToLeft.Inherit;
                textBox1.Text = "testedProgressBar RightToLeft: " + testedProgressBar.RightToLeft;

            }
            else 
            {
                testedProgressBar.RightToLeft = RightToLeft.No;
                textBox1.Text = "testedProgressBar RightToLeft: " + testedProgressBar.RightToLeft;

            }
        }

    }
}