using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedCheckBox1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedCheckBox1);
            lblLog2.Text = "Font value: " + TestedCheckBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedCheckBox2);
        }

        private void btnChangeCheckBoxFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            if (TestedCheckBox2.Font.Underline == true)
            {
                TestedCheckBox2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedCheckBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedCheckBox2);
            }
            else if (TestedCheckBox2.Font.Bold == true)
            {
                TestedCheckBox2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedCheckBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedCheckBox2);
            }
            else if (TestedCheckBox2.Font.Italic == true)
            {
                TestedCheckBox2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedCheckBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedCheckBox2);
            }
            else
            {
                TestedCheckBox2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedCheckBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedCheckBox2);
            }
        }


        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
        
      
    }
}