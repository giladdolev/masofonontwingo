using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller   
    /// </summary>
    public class ButtonFindFormController : Controller
    {
        /// <summary>
        /// Returns The View For Index   
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnFindForm_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            textBox1.Text = btnTestedButton.FindForm().ToString();
        }


    }
}

        

