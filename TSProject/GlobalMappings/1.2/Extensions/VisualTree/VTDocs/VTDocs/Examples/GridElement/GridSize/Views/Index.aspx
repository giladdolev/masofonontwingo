<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="Grid Size Property" ID="windowView1" LoadAction="GridSize/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control.
            
            Syntax: public Size Size { get; set; }
            Property Value: The Size that represents the height and width of the control in pixels." Top="75px"  ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Size property of this Grid is initially set to:  Width='285px' Height='115px'" Top="250px" ID="lblExp"></vt:Label>

        <!-- TestedGrid -->
        <vt:Grid runat="server" Top="300px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="430px" Width="360px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Width="180px" Top="495px" ID="btnChangeGridSize" ClickAction="GridSize/btnChangeGridSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
