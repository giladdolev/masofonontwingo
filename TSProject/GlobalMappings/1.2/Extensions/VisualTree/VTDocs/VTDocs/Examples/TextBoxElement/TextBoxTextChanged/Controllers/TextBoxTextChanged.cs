using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxTextChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");

            lblLog.Text = "TextBox Text value: " + TestedTextBox.Text;

        }

        public void TestedTextBox_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTextBox TextChanged event is invoked";
            lblLog.Text = "TextBox Text value: " + TestedTextBox.Text;
        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");

            if (TestedTextBox.Text == "Some Text")
                TestedTextBox.Text = "New Text";
            else
                TestedTextBox.Text = "Some Text";
        }
    }
}