﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelFindFormController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnFindForm_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");
            WindowElement myForm = this.GetRootVisualElement() as WindowElement; //<WindowElement>("windowView1");

            myForm.Text = "The Form before FindForm";
            // Get the form that the Label control is contained within.
            myForm = label.FindForm();
            // Set the text and color of the form containing the Label.
            myForm.Text = "The Form after FindForm";
            myForm.BackColor = Color.Blue;
        }
      
    }
}