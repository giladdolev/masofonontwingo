using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowContextMenuStrip3Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index3()
        {
            return View(new WindowContextMenuStrip3());
        }

        public void OnLoad(object sender, EventArgs e)
        {
            WindowElement win = this.GetVisualElementById<WindowElement>("ContextMenuStripInitializedWindow");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "ContextMenuStrip value is:\\r\\n" + win.ContextMenuStrip.ID + ".";
        }

        private void btnChangeToContextMenuStrip1_Click()
        {
            WindowElement win = this.GetVisualElementById<WindowElement>("ContextMenuStripInitializedWindow");
            ContextMenuStripElement contextMenuStrip1 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            win.ContextMenuStrip = contextMenuStrip1;
            lblLog2.Text = "ContextMenuStrip value is:\\r\\n" + win.ContextMenuStrip.ID + ".";

        }

        private void btnChangeToContextMenuStrip2_Click()
        {
            WindowElement win = this.GetVisualElementById<WindowElement>("ContextMenuStripInitializedWindow");
            ContextMenuStripElement contextMenuStrip2 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            win.ContextMenuStrip = contextMenuStrip2;
            lblLog2.Text = "ContextMenuStrip value is:\\r\\n" + win.ContextMenuStrip.ID + ".";

        }

        private void btnReset_Click()
        {
            WindowElement win = this.GetVisualElementById<WindowElement>("ContextMenuStripInitializedWindow");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            win.ContextMenuStrip = null;
            lblLog2.Text = "ContextMenuStrip value is:\\r\\nnull.";
        }
    }
}