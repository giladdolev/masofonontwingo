using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxGotFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Fill up the form...";

            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");

            TestedComboBox1.Items.Add("Item1");
            TestedComboBox1.Items.Add("Item2");
            TestedComboBox1.Items.Add("Item3");

            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");

            TestedComboBox2.Items.Add("Item1");
            TestedComboBox2.Items.Add("Item2");
            TestedComboBox2.Items.Add("Item3");
        }

        public void TestedComboBox1_GotFocus(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nComboBox 1: GotFocus event is invoked";
            lblLog.Text = "ComboBox 1 has the Focus";
        }

        public void TestedComboBox2_GotFocus(object sender, EventArgs e)
        {
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nComboBox 2: GotFocus event is invoked";
            lblLog.Text = "ComboBox 2 has the Focus";            
        }

        public void btnChangeFocus_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Button has the Focus"; 
        }

        public void btnChangeFocus_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox1.Focus();
            lblLog.Text = " ComboBox 1 has the Focus";
        }

    }
}