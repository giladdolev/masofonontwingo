
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Text Property" ID="windowView2" LoadAction="GroupBoxText/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with this control.
            
            Syntax: public override string Text { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Text property of the GroupBox is initially set to 'TestedGroupBox'" Top="235px"  ID="lblExp1"></vt:Label>     
      
        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="285px" ID="btnTestedGroupBox"></vt:GroupBox>
        <vt:Label runat="server" SkinID="Log" Top="380px" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Text Value >>"  Top="445px" Width="180px" ID="btnChangeGroupBoxSize" ClickAction="GroupBoxText\btnChangeGroupBoxText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>