<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

       <vt:WindowView runat="server" Text="GroupBox BackColor Property" ID="windowView1" LoadAction="GroupBoxBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control" Top="75px"  ID="lblDefinition"></vt:Label>
      
         <!-- TestedGroupBox1 -->
        <vt:GroupBox runat="server" Text="GroupBox" Top="110px" ID="TestedGroupBox1"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="205px" ID="lblLog1"></vt:Label>
 


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="270px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="BackColor property of this GroupBox is initially set to Gray" Top="320px"  ID="lblExp1"></vt:Label>     

        <!-- TestedGroupBox2 -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" BackColor ="Gray" Top="370px" ID="TestedGroupBox2"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="465px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change BackColor >>" Top="530px" ID="btnChangeBackColor" ClickAction="GroupBoxBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
        