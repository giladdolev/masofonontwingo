<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="ListBox MaximumSize" ID="windowView1" LoadAction="ListBoxMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedListBox' is width: 270px and height: 110px. 
            Its MaximumSize is initially set to width: 200px and height: 80px.
            To cancel 'TestedListBox' MaximumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>

         <vt:ListBox runat="server" Top="350px" Left="80px" ID="TestedListBox" Text="TestedListBox" Height="110px" Width="270px" MaximumSize="200, 80">
            <Items>
                <vt:ListItem runat="server" Text="Helo World"></vt:ListItem>
                <vt:ListItem runat="server" Text="Welcome"></vt:ListItem>
                <vt:ListItem runat="server" Text="Good Morning"></vt:ListItem>
                <vt:ListItem runat="server" Text="Good Evening"></vt:ListItem>
            </Items>
        </vt:ListBox>

        <vt:Label runat="server" SkinID="Log" Top="445px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="540px" width="200px" Left="80px" ID="btnChangeLstMaximumSize" ClickAction="ListBoxMaximumSize\btnChangeLstMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="540px" width="200px" Left="310px" ID="btnSetToZeroLstMaximumSize" ClickAction="ListBoxMaximumSize\btnSetToZeroLstMaximumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>






