﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectionChanged event" ID="windowView3" Height="750px" LoadAction="GridSelectionChanged5DateTimePickerColumnQA\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current selection changes.

            Syntax: public event GridElementCellEventHandler SelectionChanged"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells from the grid by clicking with the mouse or by pressing 
            the buttons. Each invocation of the 'BeforeSelectionChanged' and the 'SelectionChanged' events should  
            add a line to the 'Event Log'."
            Top="215px" ID="Label4">
        </vt:Label>

         <vt:Grid runat="server" Top="300px" ID="TestedGrid" BeforeSelectionChangedAction="GridSelectionChanged5DateTimePickerColumnQA\TestedGrid_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged5DateTimePickerColumnQA\TestedGrid_SelectionChanged">
            <Columns>
                <vt:GridDateTimePickerColumn runat="server" ID="GridDateTimePickerColumn1" HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-dtpCol-1"></vt:GridDateTimePickerColumn>
                <vt:GridDateTimePickerColumn runat="server" ID="GridDateTimePickerColumn2" HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-dtpCol-2"></vt:GridDateTimePickerColumn>
                <vt:GridDateTimePickerColumn runat="server" ID="GridDateTimePickerColumn3" HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-dtpCol-3"></vt:GridDateTimePickerColumn>
            </Columns>
        </vt:Grid>

        <vt:Grid runat="server" Top="300px" Left="380px" RowHeaderVisible="false" ID="TestedGrid2" BeforeSelectionChangedAction="GridSelectionChanged5DateTimePickerColumnQA\TestedGrid2_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged5DateTimePickerColumnQA\TestedGrid2_SelectionChanged">
            <Columns>
                <vt:GridDateTimePickerColumn runat="server" ID="GridDateTimePickerColumn4" HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-dtpCol-4"></vt:GridDateTimePickerColumn>
                <vt:GridDateTimePickerColumn runat="server" ID="GridDateTimePickerColumn5" HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-dtpCol-5"></vt:GridDateTimePickerColumn>
                <vt:GridDateTimePickerColumn runat="server" ID="GridDateTimePickerColumn6" HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-dtpCol-6"></vt:GridDateTimePickerColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="430px" height="50px" Width="590px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="500px" Width="590px" Height="140px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select middle cell (Left) >>" Top="665px" Width="180px" ID="btnSelectMiddleCellLeft" ClickAction="GridSelectionChanged5DateTimePickerColumnQA\btnSelectMiddleCellLeft_Click"></vt:Button>

        <vt:Button runat="server" Text="Select middle cell (Right) >>" Top="665px" Width="180px" Left="280px" ID="btnSelectMiddleCellRight" ClickAction="GridSelectionChanged5DateTimePickerColumnQA\btnSelectMiddleCellRight_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Row[0] >>" Top="705px" ID="btnSelectRow0" Width="180px" ClickAction="GridSelectionChanged5DateTimePickerColumnQA\btnSelectRow0_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Column[0] >>" Top="705px" Left="280px" Width="180px" ID="btnSelectColumn0" ClickAction="GridSelectionChanged5DateTimePickerColumnQA\btnSelectColumn0_Click"></vt:Button>

        <vt:Button runat="server" Text="Clear Event Log >>" Top="705px" Left="480px" ID="btnClear" ClickAction="GridSelectionChanged5DateTimePickerColumnQA\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

