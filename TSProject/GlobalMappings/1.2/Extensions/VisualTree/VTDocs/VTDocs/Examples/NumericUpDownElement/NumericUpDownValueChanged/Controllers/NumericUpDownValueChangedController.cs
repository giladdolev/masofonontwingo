using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownValueChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnGetValue_Click(object sender, EventArgs e)
        {
            NumericUpDownElement numericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("numericUpDown1");
            MessageBox.Show("numericUpDown1 value is: " + numericUpDown1.Value.ToString());

        }

        private void btnChangeValue_Click(object sender, EventArgs e)
        {
            NumericUpDownElement numericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("numericUpDown1");
            if (numericUpDown1.Value > 10)
                numericUpDown1.Value -= 2;
            else
                numericUpDown1.Value += 2;
        }

        private void NumericUpDownValueChanged_ValueChanged(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            textBox1.Text += "ValueChanged event is invoked"; 
        }
    }
}