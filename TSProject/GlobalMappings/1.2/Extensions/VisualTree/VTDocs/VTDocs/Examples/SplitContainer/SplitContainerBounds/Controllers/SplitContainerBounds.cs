using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "SplitContainer bounds are: \\r\\n" + TestedSplitContainer.Bounds;

        }
        public void btnChangeSplitContainerBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            if (TestedSplitContainer.Bounds == new Rectangle(100, 340, 300, 50))
            {
                TestedSplitContainer.Bounds = new Rectangle(80, 355, 200, 80);
                lblLog.Text = "SplitContainer bounds are: \\r\\n" + TestedSplitContainer.Bounds;

            }
            else
            {
                TestedSplitContainer.Bounds = new Rectangle(100, 340, 300, 50);
                lblLog.Text = "SplitContainer bounds are: \\r\\n" + TestedSplitContainer.Bounds;
            }

        }

    }
}