using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            SplitContainer splAccessibleRole = this.GetVisualElementById<SplitContainer>("splAccessibleRole");

            MessageBox.Show(splAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            SplitContainer splAccessibleRole = this.GetVisualElementById<SplitContainer>("splAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (splAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else if (splAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                splAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = splAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}