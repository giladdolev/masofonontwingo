using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelBackgroundImageLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement btn1Image = this.GetVisualElementById<PanelElement>("TestedPanel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackgroundImageLayout value: " + btn1Image.BackgroundImageLayout.ToString();

            PanelElement btn2Image = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";

        }
        private void btnNone_Click(object sender, EventArgs e)
        {
            PanelElement btn2Image = this.GetVisualElementById<PanelElement>("TestedPanel2");
            btn2Image.BackgroundImageLayout = ImageLayout.None;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
        private void btnTile_Click(object sender, EventArgs e)
        {
            PanelElement btn2Image = this.GetVisualElementById<PanelElement>("TestedPanel2");
            btn2Image.BackgroundImageLayout = ImageLayout.Tile;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
        private void btnCenter_Click(object sender, EventArgs e)
        {
            PanelElement btn2Image = this.GetVisualElementById<PanelElement>("TestedPanel2");
            btn2Image.BackgroundImageLayout = ImageLayout.Center;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
        private void btnStretch_Click(object sender, EventArgs e)
        {
            PanelElement btn2Image = this.GetVisualElementById<PanelElement>("TestedPanel2");
            btn2Image.BackgroundImageLayout = ImageLayout.Stretch;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
        private void btnZoom_Click(object sender, EventArgs e)
        {
            PanelElement btn2Image = this.GetVisualElementById<PanelElement>("TestedPanel2");
            btn2Image.BackgroundImageLayout = ImageLayout.Zoom;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2Image.BackgroundImageLayout.ToString() + ".";
        }
    }
}