<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Panel1MinSize" ID="windowView1" LoadAction="SplitContainerPanel1MinSize/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SplitContainer Panel1MinSize Property" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the minimum distance in pixels of the splitter from the left or top edge of Panel1.
            Syntax : public int Panel1MinSize { get; set; }" Top="75px"  ID="Label2"></vt:Label> 

        <vt:SplitContainer runat="server" SplitterDistance="50" Top="170px" Left="50px" Width="640px" Height="350px" ID="splitContainer1" >

            <vt:SplitterPanel runat="server" Dock="Fill" ID="s2" PanelMinSize="20">
                <vt:Panel  runat="server" ID="pan1" Dock="Fill" BackColor="Purple">
                </vt:Panel>
            </vt:SplitterPanel>

            <vt:SplitterPanel runat="server" Dock="Left" Width="400px" ID="s1">
                <vt:Panel  runat="server" ID="Panel1" Dock="Fill" BackColor="Black">
                </vt:Panel>
            </vt:SplitterPanel>

        </vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="550px" ID="lblLog1"></vt:Label> 

        <vt:Button  runat="server" ID="Button1" Width="200" Height="25" Text="Change  Panel1MinSize >>" Top="600" ClickAction="SplitContainerPanel1MinSize/btnChangePanel1MinSize_Click"></vt:Button>

        <vt:Button  runat="server" ID="Button2" Width="200" Left="300px" Height="25" Text="Change  Panel2MinSize >>" Top="600" ClickAction="SplitContainerPanel1MinSize/btnChangePanel2MinSize_Click"></vt:Button>
        
    </vt:WindowView>
</asp:Content>
