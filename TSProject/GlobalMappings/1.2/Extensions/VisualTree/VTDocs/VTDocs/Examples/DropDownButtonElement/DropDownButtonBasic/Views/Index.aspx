<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic DropDownButton" ID="windowView2" LoadAction="DropDownButtonBasic\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="DropDownButton Basic" ID="lblTitle" Left="250px" ></vt:Label>

        <vt:Label runat="server" Text="The Drop-Down Button consists of a button that when clicked displays a drop-down list of mutually
          exclusive items." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="175px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example shows basic usage of DropDownButton class;
            DropDownButton DropDownArrows property is initially set to 'true'.
             You can click on the DropDownButton, it's arrow and it's items to invoke their click events." Top="215px" ID="lblExp"></vt:Label>
        
        
             <vt:DropDownButton runat="server" Image="Content/Images/New.png" Top="305px" Left="80px"  ID="TestedDropDownButton" CssClass="vt-ddbtn-TestedDropDownButton" DropDownArrows="true"  Height="50px" Width="180px" ArrowButtonClickAction="DropDownButtonBasic\TestedDropDownButtonArrow_Click"  Text="Drop down button" ClickAction="DropDownButtonBasic\TestedDropDownButton_Click">
                 <vt:ToolbarButton runat="server" Top="10px"  ID="item1"  Height="30px" Width="120px"  Text="menuItem1" ClickAction="DropDownButtonBasic\menuItem_Click"></vt:ToolbarButton>
                 <vt:ToolbarButton runat="server" Top="10px"  ID="item2"  Height="30px" Width="120px"  Text="menuItem2" ClickAction="DropDownButtonBasic\menuItem_Click"></vt:ToolbarButton>
                 <vt:DropDownButton runat="server" Top="10px" ID="item3" DropDownArrows="true"  Height="30px" Width="120px"  Text="menuItem3" CssClass="vt-ddbtn-TestedmenuItem3" ArrowButtonClickAction="DropDownButtonBasic\menuItemArrow_Click" ClickAction="DropDownButtonBasic\menuItem_Click"> 
                     <vt:ToolbarButton runat="server" Top="10px"  ID="Button1"  Height="30px" Width="120px"  Text="Item3_1" ClickAction="DropDownButtonBasic\menuItem_Click"></vt:ToolbarButton>
                     <vt:ToolbarButton runat="server" Top="10px"  ID="Button2"  Height="30px" Width="120px"  Text="Item3_2" ClickAction="DropDownButtonBasic\menuItem_Click"></vt:ToolbarButton>
  
                 </vt:DropDownButton>
            </vt:DropDownButton>

         <vt:Label runat="server" SkinID="Log" Top="370px" Width="440px" ID="lblLog"></vt:Label>
       
         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="410px" Width="440px" Height="130px" ID="txtEventLog"></vt:TextBox>
       
        <vt:Button runat="server" Text="Change DropDownArrows value >>" Width="195px" Top="580px" ID="btnChangeDropDownArrowsproperty" ClickAction="DropDownButtonBasic\btnChangeDropDownArrowsproperty_Click"></vt:Button>
        <vt:Button runat="server" Text="Clear Event Log >>" Width="195px" Top="580px" Left="320px" ID="btnClearEventLog" ClickAction="DropDownButtonBasic\btnClearEventLog_Click"></vt:Button>

        
    </vt:WindowView>
</asp:Content>

