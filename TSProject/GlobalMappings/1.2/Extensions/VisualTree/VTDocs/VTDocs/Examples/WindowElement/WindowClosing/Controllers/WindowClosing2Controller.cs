using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowClosing2Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index2()
        {
            return View(new WindowClosing2());
        }

        public void OnLoad(object sender, EventArgs e)
        {

            CheckBoxElement chkCancelClosing = this.GetVisualElementById<CheckBoxElement>("chkCancelClosing");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Cancel Closing Action State: " + chkCancelClosing.IsChecked + '.';

        }

        public void closing(object sender, WindowClosingEventArgs e)
        {
            CheckBoxElement chkCancelClosing = this.GetVisualElementById<CheckBoxElement>("chkCancelClosing");
            e.Cancel = chkCancelClosing.IsChecked;

            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nWindow: closing event is invoked";
        }

        public void closed(object sender, WindowClosedEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nWindow: closed event is invoked";
        }

        public void btnCloseWindow_Click(object sender, EventArgs e)
        {
            WindowElement ClosingTestWindow = this.GetVisualElementById<WindowElement>("ClosingTestWindow");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            ClosingTestWindow.Close();

            lblLog.Text = "Window Close method was called";
        }

        public void chkCancelClosing_CheckedChanged(object sender, EventArgs e)
        {
            CheckBoxElement chkCancelClosing = this.GetVisualElementById<CheckBoxElement>("chkCancelClosing");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Cancel Closing Action State: " + chkCancelClosing.IsChecked + '.';
        }
    }
}