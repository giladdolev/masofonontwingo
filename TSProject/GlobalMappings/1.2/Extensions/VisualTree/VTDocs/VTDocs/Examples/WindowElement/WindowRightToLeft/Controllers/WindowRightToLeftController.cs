using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowRightToLeftController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowRightToLeft());
        }
        private WindowRightToLeft ViewModel
        {
            get { return this.GetRootVisualElement() as WindowRightToLeft; }
        }

        public void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            
            if (this.ViewModel.RightToLeft == RightToLeft.Inherit) //Get
            {
                this.ViewModel.RightToLeft = RightToLeft.No; //Set
                textBox1.Text = "RightToLeft value: " + this.ViewModel.RightToLeft;
            }
            else if (this.ViewModel.RightToLeft == RightToLeft.No)
            {
                this.ViewModel.RightToLeft = RightToLeft.Yes;
                textBox1.Text = "RightToLeft value:" + this.ViewModel.RightToLeft;
            }
            else
            {
                this.ViewModel.RightToLeft = RightToLeft.Inherit;
                textBox1.Text = "RightToLeft value:" + this.ViewModel.RightToLeft;
            }
        }
    }
}