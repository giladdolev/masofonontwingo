
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Text Property" ID="windowView2" LoadAction="CheckBoxText/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with this control.
            
            Syntax: public override string Text { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Text property of the CheckBox is initially set to 'TestedCheckBox'" Top="235px"  ID="lblExp1"></vt:Label>     
      
        <vt:CheckBox runat="server" Text="TestedCheckBox" Top="285px" ID="btnTestedCheckBox"></vt:CheckBox>
        <vt:Label runat="server" SkinID="Log" Top="325px" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Text Value >>"  Top="390px" Width="180px" ID="btnChangeCheckBoxSize" ClickAction="CheckBoxText\btnChangeCheckBoxText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>