<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox PixelHeight Property" ID="windowView2" LoadAction="GroupBoxPixelHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelHeight" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels..
            
            Syntax: public int PixelHeight { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this GroupBox is initially set to 80." Top="235px"  ID="lblExp1"></vt:Label>     

        <!-- TestedGroupBox -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="285px" ID="TestedGroupBox"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="380px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelHeight Value >>" Top="445px" ID="btnChangePixelHeight" Width="180px" ClickAction="GroupBoxPixelHeight\btnChangePixelHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
