using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
using System.Collections.Generic;

namespace MvcApplication9.Controllers
{
    public class GridRowDataController : Controller
	{         
		public ActionResult Index()
		{
			return this.View();
		}

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");           
            gridElement.Rows.Add("j", "k", "l");

            gridElement.RowData.Add(0, 1);
            gridElement.RowData.Add(1, "Two");
            gridElement.RowData.Add(2, this);
            gridElement.RowData.Add(3, new String[2] { "Four", "Row" });

            lblLog.Text = "Grid RowData collection";

            for (int i = 0; i < gridElement.RowData.Count; i++)
            {
                lblLog.Text += "\\r\\nKey:" + i + ", Value: " + gridElement.RowData[i].ToString() + ", Type: " + gridElement.RowData[i].GetType();
            } 
        }

        private void btnChangeRowData_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement btnChangeRowData = this.GetVisualElementById<ButtonElement>("btnChangeRowData");

            if (TestedGrid.RowData[0] == "Red")
            {
                TestedGrid.SetRowHeaderBackgroundColor(0, Color.Transparent);
                TestedGrid.SetRowHeaderBackgroundColor(1, Color.Transparent);
                TestedGrid.SetRowHeaderBackgroundColor(2, Color.Transparent);

                TestedGrid.RowData.Clear();
                TestedGrid.RowData.Add(0, 1);
                TestedGrid.RowData.Add(1, "Two");
                TestedGrid.RowData.Add(2, this);
                TestedGrid.RowData.Add(3, new String[2] { "Four", "Column" });

                btnChangeRowData.Text = "Set RowData with RowsHeader colors >>";

            }
            else
            {
                TestedGrid.SetRowHeaderBackgroundColor(0, Color.Red);
                TestedGrid.SetRowHeaderBackgroundColor(1, Color.Green);
                TestedGrid.SetRowHeaderBackgroundColor(2, Color.Blue);

                TestedGrid.RowData.Clear();
                TestedGrid.RowData.Add(0, "Red");
                TestedGrid.RowData.Add(1, "Green");
                TestedGrid.RowData.Add(2, "Blue");
                TestedGrid.RowData.Add(3, "White");

                btnChangeRowData.Text = "Set RowData with values of different types >>";

            }

            PrintToLog();
        }

        private void PrintToLog()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid RowData collection";
            for (int i = 0; i < TestedGrid.RowData.Count; i++)
            {

                lblLog.Text += "\\r\\nKey:" + i + ", Value: " + TestedGrid.RowData[i].ToString() + ", Type: " + TestedGrid.RowData[i].GetType();

            }
        }
    }
}