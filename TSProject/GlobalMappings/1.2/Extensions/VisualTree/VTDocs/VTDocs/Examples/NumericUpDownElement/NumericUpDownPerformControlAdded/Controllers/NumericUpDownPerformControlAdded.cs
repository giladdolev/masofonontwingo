using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownPerformControlAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlAdded_Click(object sender, EventArgs e)
        {
            
            NumericUpDownElement testedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("testedNumericUpDown");

            ControlEventArgs args = new ControlEventArgs(testedNumericUpDown);

            testedNumericUpDown.PerformControlAdded(args);
        }

        public void testedNumericUpDown_ControlAdded(object sender, EventArgs e)
        {
            MessageBox.Show("TestedNumericUpDown ControlAdded event method is invoked");
        }

    }
}