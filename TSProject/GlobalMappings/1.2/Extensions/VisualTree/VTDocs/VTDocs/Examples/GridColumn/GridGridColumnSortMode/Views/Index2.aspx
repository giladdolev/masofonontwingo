﻿
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GridColumn SortMode Property" ID="InformationWindow" Width="665px" Height="700px" LoadAction="GridColumnSortModeRemarks\OnLoad">

         <vt:Label runat="server" SkinID="Title" Top="30px" Text="SortMode property Information" Left="140px" ID="Label1"></vt:Label>

          <vt:Label runat="server" Text="Gets or sets the sort mode for the column.
              public GridColumnSortMode SortMode { get; set; }

              Property Value: A GridColumnSortMode that specifies the criteria used to order the rows 
              based on the cell values in a column.

              Grid columns have three sort modes. The sort mode for each column is specified through 
              the SortMode property of the column, which can be set to one of the following 
              GridColumnSortMode enumeration values: Automatic, Programmatic, NotSortable.

              Automatic: The user can sort the column by clicking the column header unless the column 
              headers are used for selection. A sorting glyph will be displayed automatically.

              NotSortable: The column can only be sorted programmatically, but it is not intended for 
              sorting, so the column header will not include space for a sorting glyph.

              Programmatic: The column can only be sorted programmatically, and the column header 
              will include space for a sorting glyph." Top="80px" ID="lblDefinition"></vt:Label>

         <vt:Label runat="server" Top="460px" Text=" Remarks:" Font-Bold="true" ID="Label2"></vt:Label>

         <vt:Label runat="server" Top="500px" Text=" * When a Grid control is sorted using a column with a SortMode property value
               of Automatic, a sorting glyph is automatically displayed in the column header.
              *When the control is sorted using a column with a SortMode property value of Programmatic,
               you must display the sorting glyph yourself through the SortGlyphDirection property.
              *The default sort mode of a GridTextBoxColumn is Automatic. The default sort
               mode for other column types is NotSortable.
              *A SortMode property value of NotSortable will prevent the header from changing its
               appearance when it is clicked."  ID="Label3"></vt:Label>
          

        
    </vt:WindowView>
</asp:Content>

