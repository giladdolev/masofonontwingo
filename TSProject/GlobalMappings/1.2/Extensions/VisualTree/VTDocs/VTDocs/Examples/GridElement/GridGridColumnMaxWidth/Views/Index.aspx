<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumn MaxWidth Property" ID="windowView" LoadAction="GridGridColumnMaxWidth\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaxWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the column's maximum width

            Syntax: public int MaxWidth { get; set; }

            The default is 0, meaning MaxWidth is neutralized and doesn't affect column width"
             Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, The Columns are initialized to Width='75'.
            Column1 has a default MaxWidth value, Column2 is initialized to MaxWidth='0', 
            and Column2 is initialized to MaxWidth='150'. you can change the MaxWidth property of the columns
              by using the button 'Set Column MaxWidth' and change column Width by manually dragging columns edges" Top="255px"  ID="lblExp1"></vt:Label>     

        <vt:Grid runat="server" Top="365px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px" ></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px" MaxWidth="0"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px" MaxWidth="150"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="500px" Height="40" Width="400" ID="lblLog"></vt:Label>      
        
        <vt:ComboBox runat="server" Width="155px" CssClass="vt-cmbSelectColumn" Text="Select Column" Top="635px" ID="cmbSelectColumn" SelectedIndexChangedAction="GridGridColumnMaxWidth\cmbSelectColumn_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="Column1"></vt:ListItem>
                <vt:ListItem runat="server" Text="Column2"></vt:ListItem>
                <vt:ListItem runat="server" Text="Column3"></vt:ListItem>
            </Items>
        </vt:ComboBox>


        <vt:ComboBox runat="server" Width="155px"  CssClass="vt-cmbSelectWidth" Text="Select Width" Left="250px" Top="635px" ID="cmbSelectWidth" SelectedIndexChangedAction="GridGridColumnMaxWidth\cmbSelectWidth_SelectedWidthChanged"> 
            <Items>
                <vt:ListItem runat="server" Text="25"></vt:ListItem>
                <vt:ListItem runat="server" Text="50"></vt:ListItem>
                <vt:ListItem runat="server" Text="75"></vt:ListItem>
                <vt:ListItem runat="server" Text="100"></vt:ListItem>
                <vt:ListItem runat="server" Text="125"></vt:ListItem>
                <vt:ListItem runat="server" Text="150"></vt:ListItem>
                <vt:ListItem runat="server" Text="175"></vt:ListItem>
                <vt:ListItem runat="server" Text="200"></vt:ListItem>
            </Items>
        </vt:ComboBox>
        
        <vt:Button runat="server" Text="Set Column MaxWidth >>" Left="430px" Width="170px" Top="635px"  ID="btnSetColumnMaxWidth" ClickAction="GridGridColumnMaxWidth\btnSetColumnMaxWidth_Click" ></vt:Button>

  
    </vt:WindowView>
</asp:Content>
