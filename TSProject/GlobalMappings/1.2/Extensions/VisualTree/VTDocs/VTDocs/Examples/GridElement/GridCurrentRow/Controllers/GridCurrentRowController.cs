using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridCurrentRowController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            if (testedGrid.CurrentRow == null)
            {
                lblLog.Text = "Current row value: null";
            }
            else
            {
                lblLog.Text = "Current row index: " + testedGrid.CurrentRow.Index;
            }
        }

        public void TestedGrid_RowSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (testedGrid.CurrentRow == null)
            {
                lblLog.Text = "Current row value: null";
            }
            else
            {
                lblLog.Text = "Current row index: " + testedGrid.CurrentRow.Index;
            }
        }

        public void btnEditMiddleCell_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.CurrentRow = testedGrid.Rows[1];
            testedGrid.CurrentCell = testedGrid.Rows[1].Cells[1];

            testedGrid.CurrentCell.Value = "x";
            
            // enter edit mode
            //testedGrid.BeginEdit(false);

            if (testedGrid.CurrentRow == null)
            {
                lblLog.Text = "Current row value: null";
            }
            else
            {
                lblLog.Text = "Current row index: " + testedGrid.CurrentRow.Index;
            }

        }

        public void btnSelecti_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.CurrentRow = testedGrid.Rows[2];
            testedGrid.CurrentCell = testedGrid.Rows[2].Cells[2];

            //testedGrid.Rows[2].Selected = true;

            if (testedGrid.CurrentRow == null)
            {
                lblLog.Text = "Current row value: null";
            }
            else
            {
                lblLog.Text = "Current row index: " + testedGrid.CurrentRow.Index;
            }

        }
	}
}
