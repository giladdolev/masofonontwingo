<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PerformControlRemoved() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:button runat="server" Text="PerformControlRemoved of 'Tested CheckBox'" Top="45px" Left="140px" ID="btnPerformControlRemoved" Height="36px" Width="220px" ClickAction="CheckBoxPerformControlRemoved\btnPerformControlRemoved_Click"></vt:button>


        <vt:CheckBox runat="server" Text="Tested CheckBox" Top="150px" Left="200px" ID="testedCheckBox" Height="36px"  Width="100px" ControlRemovedAction="CheckBoxPerformControlRemoved\testedCheckBox_ControlRemoved"></vt:CheckBox>           


    </vt:WindowView>
</asp:Content>
