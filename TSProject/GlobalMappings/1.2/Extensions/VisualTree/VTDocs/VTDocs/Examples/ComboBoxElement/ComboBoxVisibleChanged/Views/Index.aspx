<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox VisibleChanged event" ID="windowView2" LoadAction="ComboBoxVisibleChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="VisibleChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the Visible property value changes.

            Syntax: public event EventHandler VisibleChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the Visible property by clicking the 'Change Visible Value' button. 
            Each invocation of the 'VisibleChanged' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:ComboBox runat="server" ID="TestedComboBox" Top="325px" VisibleChangedAction="ComboBoxVisibleChanged\TestedComboBox_VisibleChanged"/>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="375px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="415px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change Visible Value >>" Top="560px" ID="btnChangeVisible" ClickAction ="ComboBoxVisibleChanged\btnChangeVisible_Click" ></vt:Button>          
       
    </vt:WindowView>
</asp:Content>


