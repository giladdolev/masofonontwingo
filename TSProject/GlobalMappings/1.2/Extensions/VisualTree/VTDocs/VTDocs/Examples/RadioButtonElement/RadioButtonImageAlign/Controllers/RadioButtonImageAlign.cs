using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonImageAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageAlign_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoRuntimeImageAlign = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeImageAlign");

            if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.MiddleLeft) //Get
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.MiddleRight;//Set
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.MiddleRight)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.MiddleCenter;
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.MiddleCenter)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.BottomCenter;
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.BottomCenter)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.BottomLeft;
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.BottomLeft)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.BottomRight;
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.BottomRight)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.TopCenter;
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.TopCenter)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.TopLeft;
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.TopLeft)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.TopRight;
            }
            else if (rdoRuntimeImageAlign.ImageAlign == ContentAlignment.TopRight)
            {
                rdoRuntimeImageAlign.ImageAlign = ContentAlignment.MiddleLeft;
            }
           
        }
      
    }
}