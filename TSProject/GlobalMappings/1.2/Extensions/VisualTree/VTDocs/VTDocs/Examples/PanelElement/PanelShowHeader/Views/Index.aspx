<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel ShowHeader Property" Top="57px" Left="11px" ID="windowView1" Height="750px" Width="768px">
        
        <vt:Label runat="server" Text="Panel ShowHeader is set to true" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45"></vt:Label>

        <vt:Panel runat="server" Text="TestedPanel" Top="115px" ShowHeader="true" Left="140px" ID="testedPanel" Height="70px"  TabIndex="3" Width="200px"></vt:Panel>

        <vt:Button runat="server" Text="Change Panel ShowHeader" Top="140px" Left="400px" ID="btnChangeShowHeader" Height="36px"  TabIndex="3" Width="200px" ClickAction="PanelShowHeader\btnChangeShowHeader_Click"></vt:Button>

         <vt:TextBox runat="server" Text="" Top="220px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
