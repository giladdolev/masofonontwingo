<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TreeView CollapseAll Method ()" Height="800px" ID="windowView" LoadAction="TreeCollapseAll\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CollapseAll" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Collapses all the tree nodes.

            Syntax: public void CollapseAll()

            Remarks:
            The CollapseAll method collapses all the TreeItem objects, which includes all the child tree nodes,
             that are in the TreeElement."
            Top="75px" ID="Label3">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="260px" ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example the right tree is filled through Items property and the left tree is filled through 
            DataSource property. Click the button to try the CollapseAll() method on these trees"
            Top="310px" ID="lblExp1">
        </vt:Label>

        <vt:Tree runat="server" ID="TestedTree1" Width="210px" Height="200px" Top="390px"></vt:Tree>

        <vt:Tree runat="server" Text="TestedTree2" Left="380px" Width="210px" Height="200px" Top="390px" ID="TestedTree2">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1">
                    <Items>
                        <vt:TreeItem runat="server" Text="Node01" ID="Tree2Item2">
                            <Items>
                                <vt:TreeItem runat="server" Text="Node011" ID="TreeItem3">
                                    <Items>
                                        <vt:TreeItem runat="server" Text="Node0111" ID="TreeItem4"></vt:TreeItem>
                                    </Items>
                                </vt:TreeItem>
                            </Items>
                        </vt:TreeItem>
                    </Items>
                </vt:TreeItem>

                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item5">
                    <Items>
                        <vt:TreeItem runat="server" Text="Node11" ID="Tree2Item6"></vt:TreeItem>
                        <vt:TreeItem runat="server" Text="Node12" ID="Tree2Item7"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree2Item9"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="615px" Width="400px"  ID="lblLog"></vt:Label>
        

        <vt:Button runat="server" Text="CollapseAll >>" Top="670px" ID="btnCollapseAll" ClickAction="TreeCollapseAll\btnCollapseAll_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
