using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class EmployeeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult EmployeeView()
        {
            return View();
        }

        
        

        //btnShowTasks_Click
        public void btnShowTasks_Click(object sender, EventArgs e)
        {
            LabelElement lblAttention2 = this.GetVisualElementById<LabelElement>("lblAttention2");
            TextBoxElement txtList = this.GetVisualElementById<TextBoxElement>("txtList");

            txtList.Text = "Deliver Reprot\\r\\n\\r\\nWrite Tests\\r\\n\\r\\nShare Work";
            lblAttention2.Text = "3 Personal Tasks";

        }
        public void btnClearTasks_Click(object sender, EventArgs e)
        {
            TextBoxElement txtList = this.GetVisualElementById<TextBoxElement>("txtList");
            txtList.Text = "";
        }
    }
}