<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Size Property" ID="windowView1" LoadAction="ProgressBarSize/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control in pixels.
            
            Syntax: public Size Size { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="Size property of this ProgressBar is initially set to:  Width='300px' Height='80px'" Top="250px"  ID="lblExp"></vt:Label>     

        <vt:ProgressBar runat="server" Top="310px" ID="TestedProgressBar" Height="80px" Width="300px"></vt:ProgressBar>           

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Top="450px" ID="btnChangeSize" TabIndex="1" Width="180px" ClickAction="ProgressBarSize\btnChangeSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
        