﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumn DisplayIndex " LoadAction="GridGridColumnDisplayIndex\OnLoad">
        
         <vt:Label runat="server" SkinID="Title" Text="DisplayIndex" Top="15px" ID="lblTitle">
        </vt:Label>
        <vt:Label runat="server" Text="Gets or sets the display order of the column relative to the currently displayed columns.
            
            Syntax: public int DisplayIndex { get; set; }"
            Top="75px" ID="lblDefinition"></vt:Label>

         <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px" ID="lblExample"></vt:Label>
         <vt:Label runat="server" Text="In the following example you can change the DisplayIndex property of this 4 columns
              by using the button 'Set Column DisplayIndex' or manually dragging columns" Top="200px" ID="lblExp"></vt:Label>
        
        <vt:Grid runat="server" Top="265px" Height="230px" Width="380px" ID="testedGrid" ColumnDisplayIndexChangedAction="GridGridColumnDisplayIndex\testedGrid_ColumnMove">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="idColumn" CssClass="vt-GridColumn1" HeaderText="#" Height="20px" Width="50px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="nameColumn" CssClass="vt-GridColumn2" HeaderText="First Name" Height="20px" Width="90px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="telephoneColumn" CssClass="vt-GridColumn3" HeaderText="Telephone No." Height="20px" Width="110px"></vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="presentColumn" CssClass="vt-GridColumn4" HeaderText="Present" Height="20px" Width="90px"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Height="100px" ID="lblLog" Width="520px" Top="510px"></vt:Label>
       
        <vt:ComboBox runat="server" Width="155px" CssClass="vt-cmbSelectColumn" Text="Select Column" Top="635px" ID="cmbSelectColumn" SelectedIndexChangedAction="GridGridColumnDisplayIndex\cmbSelectColumn_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="#"></vt:ListItem>
                <vt:ListItem runat="server" Text="First Name"></vt:ListItem>
                <vt:ListItem runat="server" Text="Telephone No."></vt:ListItem>
                <vt:ListItem runat="server" Text="Present"></vt:ListItem>
            </Items>
        </vt:ComboBox>


        <vt:ComboBox runat="server" Width="155px"  CssClass="vt-cmbSelectIndex" Text="Select DisplayIndex" Left="250px" Top="635px" ID="cmbSelectIndex" SelectedIndexChangedAction="GridGridColumnDisplayIndex\cmbSelectIndex_SelectedIndexChanged"> 
            <Items>
                <vt:ListItem runat="server" Text="0"></vt:ListItem>
                <vt:ListItem runat="server" Text="1"></vt:ListItem>
                <vt:ListItem runat="server" Text="2"></vt:ListItem>
                <vt:ListItem runat="server" Text="3"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:Button runat="server" Text="Set Column DisplayIndex >>" Left="430px" Width="170px" Top="635px"  ID="btnSetColumnDisplayIndex" ClickAction="GridGridColumnDisplayIndex\btnSetColumnDisplayIndex_Click" ></vt:Button>

       
       
        
    </vt:WindowView>

</asp:Content>
