using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxValidatedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("Item2");
            TestedComboBox.Items.Add("Item3");
            
            lblLog.Text = "Text value: " + TestedComboBox.Text + 
                "\nCausesValidation Property Value: " + TestedComboBox.CausesValidation.ToString() + '.';           
        }


        public void TestedComboBox_Validated(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nComboBox Validated event is invoked";

            int index = TestedComboBox.FindStringExact(TestedComboBox.Text); 
            if (index < 0)
            {
                lblLog.Text = "Text value: " + TestedComboBox.Text + ",\\r\\nNot a valid value!" +
                    "\\r\\nCausesValidation Property Value: " + TestedComboBox.CausesValidation.ToString() + '.';
            }
            else
            {
                lblLog.Text = "Text value: " + TestedComboBox.Text + ",\\r\\Valid value!" +
                   "\\r\\nCausesValidation Property Value: " + TestedComboBox.CausesValidation.ToString() + '.';
            }

        }

        public void TestedComboBox_LostFocus(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Text value: " + TestedComboBox.Text + ",\\r\\Lost Focus!" +
                "\\r\\nCausesValidation Property Value: " + TestedComboBox.CausesValidation.ToString() + '.';
        }

        public void TestedComboBox_TextChanged(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Text value: " + TestedComboBox.Text +
                "\\r\\nCausesValidation Property Value: " + TestedComboBox.CausesValidation.ToString() + '.';
        }

        public void btnToggleCausesValidation_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedComboBox.CausesValidation == true)
            {
                TestedComboBox.CausesValidation = false;
            }
            else
            {
                TestedComboBox.CausesValidation = true;
            }

            lblLog.Text = "Text value: " + TestedComboBox.Text +
                "\\r\\nCausesValidation Property Value: " + TestedComboBox.CausesValidation.ToString() + '.';
        }
    }
}