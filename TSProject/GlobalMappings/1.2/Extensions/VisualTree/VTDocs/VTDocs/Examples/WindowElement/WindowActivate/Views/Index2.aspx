<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TestedWindow" ID="EnableEscDefaultWindow" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleDimensions="6, 13" Height="262px" Width="284px">
	</vt:WindowView>
</asp:Content>
