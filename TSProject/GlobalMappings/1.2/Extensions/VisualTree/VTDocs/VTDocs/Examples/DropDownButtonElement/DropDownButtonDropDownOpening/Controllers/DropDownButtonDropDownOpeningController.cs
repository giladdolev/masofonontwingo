using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class DropDownButtonDropDownOpeningController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Click arrow to open the dropdown";
        }
       
        public void TestedDropDownButton_DropDownOpening(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nDropDownOpening event was invoked";
        }

        public void TestedDropDownButton_DropDownOpened(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nDropDownOpened event was invoked";
        }

        public void item3_DropDownOpening(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            ToolBarItem menuItem = (ToolBarItem)sender;
            txtEventLog.Text += "\\r\\n" + menuItem.Text + " DropDownOpening event was invoked";
        }

        public void item3_DropDownOpened(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            ToolBarItem menuItem = (ToolBarItem)sender;
            txtEventLog.Text += "\\r\\n" + menuItem.Text + " DropDownOpened event was invoked";
        }

    }
}