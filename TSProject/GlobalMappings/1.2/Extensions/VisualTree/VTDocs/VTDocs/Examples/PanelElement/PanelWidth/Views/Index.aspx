<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Width Property" ID="windowView1" LoadAction="PanelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="Width" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this Panel is initially set to: 200px" Top="205px" ID="lblExp1"></vt:Label>

         <!-- TestedPanel -->
        <vt:Panel runat="server" Text="TestedPanel" Top="265px" ID="TestedPanel" TabIndex="3"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="360px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Width value >>" Top="425px" ID="btnChangeWidth" TabIndex="3" Width="180px" ClickAction="PanelWidth\btnChangeWidth_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>