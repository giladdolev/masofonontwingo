<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Draggable Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:ProgressBar runat="server" Text="Draggable is set to 'true'" Top="55px" Draggable="true" Left="140px" ID="prgDraggable" Height="50px" Width="200px"></vt:ProgressBar>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:ProgressBar runat="server" Text="Runtime Draggable" Top="165px" Left="140px" ID="prgRuntimeDraggable" Height="50px" TabIndex="1" Width="200px" ></vt:ProgressBar>

        <vt:Button runat="server" Text="Change ProgressBar Draggable" Top="195px" Left="420px" ID="btnChangeDraggable" Height="36px" TabIndex="1" Width="200px" ClickAction="ProgressBarDraggable\btnChangeDraggable_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="275px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        