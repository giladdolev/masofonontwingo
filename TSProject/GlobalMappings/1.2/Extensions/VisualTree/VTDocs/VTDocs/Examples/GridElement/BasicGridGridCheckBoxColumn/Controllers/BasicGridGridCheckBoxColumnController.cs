﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace MvcApplication9.Controllers
{
    public class BasicGridGridCheckBoxColumnController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            DataTable source = new DataTable();
            source.Columns.Add("value", typeof(int));
            source.Columns.Add("text", typeof(string));
            source.Columns.Add("Field3", typeof(bool));
            source.Rows.Add(1, "James", true);
            source.Rows.Add(2, "Bob", false);
            source.Rows.Add(3, "Dana", false);
            source.Rows.Add(4, "Sara", true);
          
            //TestedGrid.Rows.Add("1","Tomer", false);
            //TestedGrid.Rows.Add("2", "Yoav", true);
            //TestedGrid.Rows.Add("3", "Shalom", false);
            //TestedGrid.Rows.Add("4", "Shimrit", true);
            //TestedGrid.Rows.Add("5", "Moran", false);
            //TestedGrid.Rows.Add("6", "Lital", true);
            //TestedGrid.Rows.Add("7", "Haim", false);
            TestedGrid.DataSource = source;
            PrintToLogFromOnLoad();

            
        }

       

      
                 
        public void btnSetCheckBoxesValueFalse_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            for (int i = 0; i < TestedGrid.Rows.Count; i++)
            {
                TestedGrid.Rows[i].Cells[2].SetValue(false); 
            }
            lblLog.Text = "All gymnasts are absence..";

        }

        public void btnSetCheckBoxesValueTrue_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            for (int i = 0; i < TestedGrid.Rows.Count; i++)
            {
                TestedGrid.Rows[i].Cells[2].SetValue(true);
            }

            lblLog.Text = "All gymnasts are present..";
        }


        public void coulmns_CheckedChange(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            PrintToLog();
        }

        public void PrintToLog()
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            string present = "";
            string absence = "";

           
            for (int i = 0; i < TestedGrid.Rows.Count; i++)
            {
                if(TestedGrid.Rows[i].Cells[2].Value.ToString() == "True")
                {
                    if(present=="")
                        present +=TestedGrid.Rows[i].Cells[1].Value;
                    else
                      present += ", " + TestedGrid.Rows[i].Cells[1].Value;
                }
                else 
                {
                    if (absence == "")
                        absence += TestedGrid.Rows[i].Cells[1].Value;
                    else
                        absence += ", " + TestedGrid.Rows[i].Cells[1].Value;
                }
            }

            lblLog.Text = "Attendance summary:\\r\\nPresent: " + present + "\\r\\nAbsence: " + absence;


        }

        private void PrintToLogFromOnLoad()
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            string present = "";
            string absence = "";

            //Setting variables -  first row is without comma
            for (int i = 0; i < TestedGrid.Rows.Count; i++)
            {
                if (TestedGrid.Rows[i].Cells[2].Value.ToString() == "True")
                {
                    if (present == "")
                        present += TestedGrid.Rows[i].Cells[1].Value;
                    else
                        present += ", " + TestedGrid.Rows[i].Cells[1].Value;
                }
                else
                {
                    if (absence == "")
                        absence += TestedGrid.Rows[i].Cells[1].Value;
                    else
                        absence += ", " + TestedGrid.Rows[i].Cells[1].Value;
                }
            }

            lblLog.Text = "Attendance summary:\\r\\nPresent: " + present + "\\r\\nAbsence: " + absence;
        } 
    }
}
