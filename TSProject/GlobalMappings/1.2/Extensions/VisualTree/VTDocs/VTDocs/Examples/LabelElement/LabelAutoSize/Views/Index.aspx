<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label AutoSize property" ID="windowView2" LoadAction="LabelAutoSize\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="AutoSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control is automatically resized to display its entire contents.
            
             public virtual bool AutoSize { get; set;}
             'true' if the control adjusts its width to closely fit its contents; otherwise, 'false'.

             Note: When added through, the default value is true. When
             instantiated from code, the default value is 'false'." Top="75px"  ID="lblDefinition"></vt:Label>
         
        <!--TestedLabel1-->
        <vt:Label runat="server"  Text="This is the Text of TestedLabel1" Top="230px" ID="TestedLabel1" Height="15px" Width="80px" BorderStyle="Solid" Left="80px"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Height="30px" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="AutoSize property of this 'TestedLabel' is initially set to 'false'" Top="380px"  ID="lblExp1"></vt:Label>     

        <!--TestedLabel-->
        <vt:Label runat="server"  Text="This is the Text of TestedLabel2" Top="430px" ID="TestedLabel2" AutoSize="false" Height="15px" Width="80px" BorderStyle="Solid" Left="80px"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Height="30px" Top="480px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change AutoSize value >>" Top="545px" Left="50px" ID="btnChangeAutoSize" Width="200px" ClickAction="LabelAutoSize\btnChangeAutoSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Size to (80,15) >>" Top="545px" Left="280px" ID="btnChangeSize1" Width="200px" ClickAction="LabelAutoSize\btnChangeSizeTo80_15_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Size to (200,30) >>" Top="545px" Left="510px" ID="btnChangeSize2" Width="200px" ClickAction="LabelAutoSize\btnChangeSizeTo200_30_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>


