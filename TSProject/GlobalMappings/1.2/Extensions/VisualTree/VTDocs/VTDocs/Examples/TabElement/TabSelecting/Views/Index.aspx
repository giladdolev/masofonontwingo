<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Selecting Event" ID="windowView1" LoadAction="TabSelecting\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Selecting" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Occurs before a tab is selected, enabling a handler to cancel the tab change.
            
            Syntax: public event TabControlCancelEventHandler Selecting

            When the current tab changes in a TabControl, the following events occur in the following order:
            1-Deselecting 2-Deselected 3-Selecting 4-Selected"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can Select different Tabs using the selection buttons below 
            or by manually selecting a different tab.
            Each invocation of the 'Selecting' event should add a line in the 'Event Log'."
            Top="245px" ID="Label4">
        </vt:Label>
         <!-- TestedTab -->
        <vt:Tab runat="server" Text="TestedTab" Top="315px" ID="TestedTab" DeselectingAction="TabSelecting/TestedTab_Deselecting" DeselectedAction="TabSelecting/TestedTab_Deselected" SelectingAction="TabSelecting/TestedTab_Selecting" SelectedAction="TabSelecting/TestedTab_Selected">
            <TabItems>
                <vt:TabItem runat="server" Text="tabItem1" Top="22px" Left="4px" ID="tabItem1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem2" Top="22px" Left="4px" ID="tabItem2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem3" Top="22px" Left="4px" ID="tabItem3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Height="40px" Top="435px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="490px" Width="360px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select tabItem1 >>" Top="630px" width="130px" Left="80px" ID="btnSelecttabItem1" TabIndex="1" ClickAction="TabSelecting\btnSelecttabItem1_Click"></vt:Button>   
        <vt:Button runat="server" Text="Select tabItem2 >>" Top="630px" width="130px" Left="235px" ID="btnSelecttabItem2" TabIndex="2" ClickAction="TabSelecting\btnSelecttabItem2_Click"></vt:Button>  
        <vt:Button runat="server" Text="Select tabItem3 >>" Top="630px" width="130px" Left="390px" ID="btnSelecttabItem3" TabIndex="3" ClickAction="TabSelecting\btnSelecttabItem3_Click"></vt:Button>  
        <vt:Button runat="server" Text="Clear Event Log >>" Top="630px" width="130px" Left="545px" ID="btnClearEventLog" TabIndex="3" ClickAction="TabSelecting\btnClearEventLog_Click"></vt:Button>  


    </vt:WindowView>
</asp:Content>


        