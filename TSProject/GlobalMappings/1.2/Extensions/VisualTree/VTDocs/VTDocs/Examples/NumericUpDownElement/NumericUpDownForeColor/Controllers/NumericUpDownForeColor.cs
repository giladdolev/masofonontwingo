using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedNumericUpDown1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedNumericUpDown2.ForeColor + '.';
        }

        public void btnChangeNumericUpDownForeColor_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedNumericUpDown2.ForeColor == Color.Green)
            {
                TestedNumericUpDown2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedNumericUpDown2.ForeColor + '.';
            }
            else
            {
                TestedNumericUpDown2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedNumericUpDown2.ForeColor + '.';
            }
        }

    }
}