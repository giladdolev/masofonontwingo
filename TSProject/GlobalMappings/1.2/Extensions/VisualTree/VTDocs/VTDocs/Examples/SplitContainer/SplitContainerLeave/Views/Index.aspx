<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Leave event" ID="windowView2" LoadAction="SplitContainerLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the Split Containers,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:SplitContainer runat="server" Top="335px" Height="50px" Width="180px" ID="TestedSplitContainer1" LeaveAction ="SplitContainerLeave\TestedSplitContainer1_Leave">
            <vt:SplitterPanel runat="server" Dock="Fill">
                <vt:Button runat="server" ID="btn1" Top="10px" Left="10px" Text="Button 1" ClickAction="SplitContainerLeave\btnButton1_Click"></vt:Button>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" Dock="Left">
            </vt:SplitterPanel>
        </vt:SplitContainer>
        <vt:SplitContainer runat="server" Top="400px" Height="50px" Width="180px" ID="TestedSplitContainer2" LeaveAction ="SplitContainerLeave\TestedSplitContainer2_Leave">  
            <vt:SplitterPanel runat="server" Dock="Fill">
                <vt:Button runat="server" ID="btn2" Top="10px" Left="10px" Text="Button 2" ClickAction="SplitContainerLeave\btnButton2_Click"></vt:Button>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" Dock="Left">
            </vt:SplitterPanel>
        </vt:SplitContainer>
        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="465px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="505px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

