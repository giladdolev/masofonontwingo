using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowBackgroundImageController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowBackgroundImage());
        }
        private WindowBackgroundImage ViewModel
        {
            get { return this.GetRootVisualElement() as WindowBackgroundImage; }
        }

        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            //if (groupBox.BackgroundImage.Source == "Content/Images/gizmox.png")
            //    groupBox.BackgroundImage = new UrlReference(@"Content/Images/galilcs.png");
            //else
            //    groupBox.BackgroundImage = new UrlReference(@"Content/Images/gizmox.png");

            if (this.ViewModel.BackgroundImage == null)
			{
                this.ViewModel.BackgroundImage = new UrlReference("/Content/Images/sleepy.jpg");
                textBox1.Text = this.ViewModel.BackgroundImage.Source.ToString();
			}
			else
            {
                this.ViewModel.BackgroundImage = null;
                textBox1.Text = "BackgroundImage is set to null";
            }
                
		}

    }
}