﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="ThemeRed" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="8, 16" Text="Hello World" Margin-All="0" Padding-All="0" ID="Form1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="7.8pt" Height="330px" TabIndex="-1" Width="1000px">

         <vt:Label runat="server" Text="Basic RangeTrackBar" Top="15px" ID="lblTitle" Left="300" Font-Size="19pt" Font-Bold="true">
        </vt:Label>
        <vt:Label runat="server" Text="Example of RangeTrackBar ,initialize thumbs to [0,0]"
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Range runat="server" id="range" MinValue="0" MaxValue="100" Increment="1" Width="200" Height="30" Top="150" Left="80"></vt:Range>

        <vt:Label runat="server" Text="Try to move the thumbs " Top="300px" ID="Label2">
        </vt:Label>

    </vt:WindowView>

</asp:Content>
