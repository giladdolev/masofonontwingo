<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox CheckAlign Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="150px"></vt:Label>     

        <vt:CheckBox runat="server" Text="CheckAlign is set to TopRight" CheckAlign="BottomCenter" Top="45px"   Left="140px" ID="chkCheckAlign" Height="36px" Width="150px"></vt:CheckBox>           

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:CheckBox runat="server" Text="CheckBox CheckAlign Change" Top="115px" Left="140px" ID="chkCheckAlignChange" Height="36px" Width="150px"></vt:CheckBox>

        <vt:Button runat="server" Text="Change CheckBox CheckAlign" Top="115px" Left="400px" ID="btnChangeCheckAlign" Height="36px" Width="200px" ClickAction="CheckBoxCheckAlign\btnChangeCheckAlign_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
