using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox1 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedRichTextBox1.BackColor.ToString();

            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedRichTextBox2.BackColor.ToString();

        }

        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedRichTextBox2.BackColor == Color.Orange)
            {
                TestedRichTextBox2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedRichTextBox2.BackColor.ToString();
            }
            else
            {
                TestedRichTextBox2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedRichTextBox2.BackColor.ToString();

            }
        }

    }
}