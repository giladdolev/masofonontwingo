using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowWidthController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowWidth());
        }
        private WindowWidth ViewModel
        {
            get { return this.GetRootVisualElement() as WindowWidth; }
        }

        public void btnChangeWidth_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            
           if(this.ViewModel.Width == 700)
           {
               this.ViewModel.Width = 400;
               textBox1.Text = this.ViewModel.Width.ToString();
           }
           else
           {
               this.ViewModel.Width = 700;
               textBox1.Text = this.ViewModel.Width.ToString();

           }
        }
    }
}