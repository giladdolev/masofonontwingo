<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree Enabled Property" ID="windowView1" LoadAction="TreeEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Tree runat="server" Text="Tree" Top="185px" ID="TestedTree1" CssClass="vt-test-tree-1">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>           

        <vt:Label runat="server" SkinID="Log" Top="305px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedTree' is initially set to 'false'." Top="405px" ID="lblExp1"></vt:Label> 

        <vt:Tree runat="server" Text="TestedTree" Top="455px" Enabled ="false" ID="TestedTree2" CssClass="vt-test-tree-2">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree2Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="575px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="640px" ID="btnChangeEnabled" Width="180px" ClickAction="TreeEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
