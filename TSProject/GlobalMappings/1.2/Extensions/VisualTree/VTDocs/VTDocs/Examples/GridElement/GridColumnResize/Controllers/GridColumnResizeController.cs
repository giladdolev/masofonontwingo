using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
using System.ComponentModel;
namespace HelloWorldApp
{
    public class GridColumnResizeController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void load_view(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));
            source.Columns.Add("phone", typeof(bool));

            source.Rows.Add("�����#", "lisa@simpsons.com", true);
            source.Rows.Add("Bart", "Bart@simpsons.com", true);
            source.Rows.Add("Homer", "Homer@simpsons.com", true);
            source.Rows.Add("Lisa", "Lisa@simpsons.com", false);
            source.Rows.Add("Marge", "Marge@simpsons.com", true);
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", true);
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", false);
            gridElement.DataSource = source;
            gridElement.Columns[0].Resize+=column_resize;
        }
        private void column_resize(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\ncolumn_resize event is invoked";
        }
    }
}
