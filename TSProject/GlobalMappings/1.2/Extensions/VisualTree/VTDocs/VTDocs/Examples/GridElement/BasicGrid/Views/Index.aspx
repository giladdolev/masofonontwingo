﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Basic Example" Height="1200px" Width="1200px" LoadAction="BasicGrid\page_load" AfterrenderAction="BasicGrid\afterrenderform">
     
   <%--     <vt:WidgetGrid runat="server" Top="100px" Left="10px" ID="WidgetGrid1" Height="299px" Width="782px" WidgetTemplate="spgridcomponent" WidgetTemplateItemsCount="3" WidgetControlEditChangedAction="BasicGrid/EditChangedAction" WidgetControlItemChangedAction="BasicGrid/ItemChangedAction" WidgetControlFocusEnterAction="BasicGrid/FocusEnterAction" WidgetControlDoubleClickAction="BasicGrid/DoubleClickAction" RowHeaderVisible="false" SelectionChangedAction="BasicGrid/selection" BeforeSelectionChangedAction="BasicGrid/beforeselection" WidgetControlClickAction="BasicGrid/ClickAction" CellkeyDownAction="BasicGrid/grdkeydown" AfterrenderAction="BasicGrid\afterrendergrid" >
        </vt:WidgetGrid>--%>

        <%-- <vt:Grid runat="server" Top="34px" Left="10px" ID="gridgrid" Height="299px" Width="782px" WidgetTemplate="spgridcomponent" WidgetTemplateItemsCount="3">

        </vt:Grid>--%>

        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Fill the Grid" Top="100px" Left="820px" ID="button1" ClickAction="BasicGrid\button1_Click" Height="45px" Width="236px">
        </vt:Button>
       <%--  <vt:Button runat="server" UseVisualStyleBackColor="True" Text="focus" Top="200px" Left="820px" ID="button2" ClickAction="BasicGrid\button2_Click" Height="45px" Width="236px">
        </vt:Button>
         <vt:Button runat="server" UseVisualStyleBackColor="True" Text="enable" Top="300px" Left="820px" ID="button3" ClickAction="BasicGrid\button3_Click" Height="45px" Width="236px">
        </vt:Button>--%>


        <vt:WidgetGrid runat="server" Top="500px" Left="10px" ID="gridElement" Height="299px" Width="782px" WidgetTemplate="spgridcomponent" WidgetTemplateItemsCount="3" WidgetControlEditChangedAction="BasicGrid/EditChangedAction" WidgetControlItemChangedAction="BasicGrid/ItemChangedAction" WidgetControlFocusEnterAction="BasicGrid/FocusEnterAction" WidgetControlDoubleClickAction="BasicGrid/DoubleClickAction" RowHeaderVisible="false" SelectionChangedAction="BasicGrid/selection" BeforeSelectionChangedAction="BasicGrid/beforeselection" WidgetControlClickAction="BasicGrid/ClickAction" CellkeyDownAction="BasicGrid/grdkeydown" AfterrenderAction="BasicGrid\afterrendergrid" >
        </vt:WidgetGrid>
        

        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Add" Top="150px" Left="820px" ID="button4" ClickAction="BasicGrid\button1_Add" Height="45px" Width="236px">
        </vt:Button>
        
        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Delete row number 1" Top="200px" Left="820px" ID="button5" ClickAction="BasicGrid\button1_Delete" Height="45px" Width="236px">
        </vt:Button>
        
        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Focus Last Row" Top="250px" Left="820px" ID="button6" ClickAction="BasicGrid\button1_Focus" Height="45px" Width="236px">
        </vt:Button>



        <vt:ContextMenuStrip runat="server" ID="contextMenuStripCells">
        </vt:ContextMenuStrip>

    </vt:WindowView>

</asp:Content>
