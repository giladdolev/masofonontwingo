﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Custom Column" Height="800px" Width="1200px">
        
         <vt:Label runat="server" SkinID="SubTitle" Text="CustomColumn" Top="15px" ID="lblTitle" Left="300" Font-Size="19pt" Font-Bold="true">
        </vt:Label>
        <vt:Label runat="server" Text="How to use custom columnin grid ,initialize ."
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Grid runat="server" Top="100px" Left="10px" ID="dgvTest" Height="200px" Width="782px" RowCount="2" AutoGenerateColumns="false" SelectionChangedAction="GridCustomColumn\SelectionChanged" CellBeginEditAction="GridCustomColumn\cell_beginEdit" > 
            <Columns>
                <vt:GridColumn ID="name" runat="server" HeaderText="Name" DataMember="name" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="email" runat="server" HeaderText="Email" DataMember="email" Width="80"></vt:GridColumn>
                <vt:GridCustomColumn ID="GridCustomColumn1" runat="server" HeaderText="Custom" Width="300"  DataMember="someValue" BeforeCellRenderingAction="GridCustomColumn\BeforeCellRendering" ColumnActionAction1="GridCustomColumn\ColumnAction" ></vt:GridCustomColumn>
            </Columns>
        </vt:Grid>
       
        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Fill the Grid" Top="100px" Left="820px" ID="button1" ClickAction="GridCustomColumn\button1_Click" Height="45px" Width="236px">
        </vt:Button>
        
      
        
          <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Font-Size="13pt" Top="350px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="after click on Fill the Grid button we fiiled the grid with new datasource  " Top="380px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog"></vt:Label>
    </vt:WindowView>

</asp:Content>
