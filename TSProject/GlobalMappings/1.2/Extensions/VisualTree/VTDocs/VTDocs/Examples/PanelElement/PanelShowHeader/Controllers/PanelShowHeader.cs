using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelShowHeaderController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeShowHeader_Click(object sender, EventArgs e)
        {
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (testedPanel.ShowHeader == true)
            {
                testedPanel.ShowHeader = false;
                textBox1.Text = "ShowHeader value:\\r\\n" + testedPanel.ShowHeader;

            }
            else
            {
                testedPanel.ShowHeader = true;
                textBox1.Text = "ShowHeader value:\\r\\n" + testedPanel.ShowHeader;
            }
        }
      
    }
}