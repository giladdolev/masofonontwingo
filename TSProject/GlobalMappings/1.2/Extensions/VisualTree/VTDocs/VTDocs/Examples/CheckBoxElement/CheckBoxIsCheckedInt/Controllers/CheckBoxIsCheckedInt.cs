using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxIsCheckedIntController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeIsCheckedInt_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkCheckIsCheckedInt = this.GetVisualElementById<CheckBoxElement>("chkCheckIsCheckedInt");

            if (chkCheckIsCheckedInt.IsCheckedInt == 1)
            {
                chkCheckIsCheckedInt.IsCheckedInt = 0;
            }
            else
            {
                chkCheckIsCheckedInt.IsCheckedInt = 1;
            }
            
        }

    }
}