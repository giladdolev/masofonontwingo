using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowTopController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowTop());
        }
        private WindowTop ViewModel
        {
            get { return this.GetRootVisualElement() as WindowTop; }
        }

        public void btnChangeTop_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            
           if(this.ViewModel.Top == 57)
           {
               this.ViewModel.Top = 100;
               textBox1.Text = this.ViewModel.Top.ToString();
           }
           else
           {
               this.ViewModel.Top = 57;
               textBox1.Text = this.ViewModel.Top.ToString();

           }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowTop");

            ViewModel.TestedWin = VisualElementHelper.CreateFromView<TestedWindowTop>("TestedWindowTop", "Index2", null, argsDictionary, null);

            this.ViewModel.TestedWin.Show();
            this.ViewModel.TestedWin.Left = 400;
            this.ViewModel.TestedWin.Top = 200;
            //this.ViewModel.TestedWin.Height = 400;
            //this.ViewModel.TestedWin.Width = 900;
        }

    }
}