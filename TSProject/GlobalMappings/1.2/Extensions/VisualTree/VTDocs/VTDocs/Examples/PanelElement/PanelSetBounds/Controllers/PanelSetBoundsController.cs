using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //PanelAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested Panel bounds are set to: \\r\\nLeft  " + TestedPanel.Left + ", Top  " + TestedPanel.Top + ", Width  " + TestedPanel.Width + ", Height  " + TestedPanel.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.Left == 100)
            {
                TestedPanel.SetBounds(80, 300, 200, 80);
                lblLog.Text = "The Tested Panel bounds are set to: \\r\\nLeft  " + TestedPanel.Left + ", Top  " + TestedPanel.Top + ", Width  " + TestedPanel.Width + ", Height  " + TestedPanel.Height;

            }
            else
            {
                TestedPanel.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested Panel bounds are set to: \\r\\nLeft  " + TestedPanel.Left + ", Top  " + TestedPanel.Top + ", Width  " + TestedPanel.Width + ", Height  " + TestedPanel.Height;
            }
        }

    }
}