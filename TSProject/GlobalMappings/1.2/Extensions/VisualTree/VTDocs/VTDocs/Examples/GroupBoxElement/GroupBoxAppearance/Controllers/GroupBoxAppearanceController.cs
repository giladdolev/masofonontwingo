using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxAppearanceController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeAppearance_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpRuntimeAppearance = this.GetVisualElementById<GroupBoxElement>("grpRuntimeAppearance");

            if (grpRuntimeAppearance.Appearance == AppearanceConstants.None)
            {
                grpRuntimeAppearance.Appearance = AppearanceConstants.ccFlat;
                textBox1.Text = "AppearanceConstants.ccFlat";
            }
            else if (grpRuntimeAppearance.Appearance == AppearanceConstants.ccFlat)
            {
                grpRuntimeAppearance.Appearance = AppearanceConstants.cc3D;
                textBox1.Text = "AppearanceConstants.cc3D";
            }
            else if (grpRuntimeAppearance.Appearance == AppearanceConstants.cc3D)
            {
                grpRuntimeAppearance.Appearance = AppearanceConstants.None;
                textBox1.Text = "AppearanceConstants.None";
            }
        }

    }
}