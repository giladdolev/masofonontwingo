﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class RadioButtonEquals : WindowElement
    {

        RadioButtonElement _TestedRadioButton;

        public RadioButtonEquals()
        {
            _TestedRadioButton = new RadioButtonElement();
        }

        public RadioButtonElement TestedRadioButton
        {
            get { return this._TestedRadioButton; }
            set { this._TestedRadioButton = value; }
        }
    }
}