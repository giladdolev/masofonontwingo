using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicProgressBarController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void Command1_Click(object sender, EventArgs e)
        {
            ProgressBarElement ProgressBar1 = this.GetVisualElementById<ProgressBarElement>("ProgressBar1");

            ProgressBar1.Max = 1000;
            ProgressBar1.Value = (ProgressBar1.Value + 100 ) % 1000;

        }


    }
}