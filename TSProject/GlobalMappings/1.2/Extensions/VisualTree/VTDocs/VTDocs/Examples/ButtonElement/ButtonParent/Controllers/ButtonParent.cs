using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonParentController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnParent_Click(object sender, EventArgs e)
        {
            ButtonElement btnParent = this.GetVisualElementById<ButtonElement>("btnParent");

            MessageBox.Show("Parent property value:\\r\\n" + btnParent.Parent);
        }

    }
}