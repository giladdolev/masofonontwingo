using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonGotFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Press the Bottons...";
        }

        public void btnButton1_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "Button1 has the Focus";
            txtEventLog.Text += "\\r\\nButton: Button1, GotFocus event is invoked";
        }

        public void btnButton2_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "Button2 has the Focus";
            txtEventLog.Text += "\\r\\nButton: Button2, GotFocus event is invoked";
        }

        public void btnButton3_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "Button3 has the Focus";
            txtEventLog.Text += "\\r\\nButton: Button3, GotFocus event is invoked";
        }

        public void btnChangeFocus_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Focus Button1 >> has the Focus";
        }

        public void btnChangeFocus_Click(object sender, EventArgs e)
        {
            ButtonElement btnButton1 = this.GetVisualElementById<ButtonElement>("btnButton1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            btnButton1.Focus();
            lblLog.Text = "Button1 has the Focus";
        }
    }
}