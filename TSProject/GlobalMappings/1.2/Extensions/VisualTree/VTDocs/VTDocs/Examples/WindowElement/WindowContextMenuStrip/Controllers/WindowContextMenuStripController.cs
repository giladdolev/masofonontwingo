using System.Collections.Generic;
using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowContextMenuStripController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new WindowContextMenuStrip());
        }

        private WindowContextMenuStrip ViewModel
        {
            get { return this.GetRootVisualElement() as WindowContextMenuStrip; }
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowContextMenuStrip");
            return argsDictionary;
        }

        public void OnLoad(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = GetArgsDictionary();
            ViewModel.frm2 = VisualElementHelper.CreateFromView<WindowContextMenuStrip2>("WindowContextMenuStrip2", "Index2", null, argsDictionary, null);
            ViewModel.frm3 = VisualElementHelper.CreateFromView<WindowContextMenuStrip3>("WindowContextMenuStrip3", "Index3", null, argsDictionary, null);
            
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            //LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            //Write to log lblLog1
            if (ViewModel.frm2.ContextMenuStrip == null)
                lblLog1.Text = "ContextMenuStrip value is null";
            else
                lblLog1.Text = "ContextMenuStrip value is: " + ViewModel.frm2.ContextMenuStrip.ID;

            ViewModel.frm2.EnableEsc = true;
            ViewModel.frm3.EnableEsc = true;
            //Write to log lblLog2
            //if (ViewModel.frm3.ContextMenuStrip == null)
            //    lblLog2.Text = "ContextMenuStrip value is null.";
            //else
            //    lblLog2.Text = "ContextMenuStrip value is: " + ViewModel.frm3.ContextMenuStrip.ID + ".";
        }

        public void TestedButton1_Click(object sender, EventArgs e)
        {
            ViewModel.frm2.Show();
        }

        public void TestedButton2_Click(object sender, EventArgs e)
        {
            ViewModel.frm3.Show();
        }

        //private void btnChangeToContextMenuStrip1_Click()
        //{
        //    ContextMenuStripElement contextMenuStrip1 = ViewModel.frm2.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip1");
        //    LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

        //    ViewModel.frm2.ContextMenuStrip = contextMenuStrip1;
        //    lblLog2.Text = "ContextMenuStrip value is: " + ViewModel.frm2.ContextMenuStrip.ID + ".";

        //}

        //private void btnChangeToContextMenuStrip2_Click()
        //{
        //    ContextMenuStripElement contextMenuStrip2 = ViewModel.frm2.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip2");
        //    LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

        //    ViewModel.frm2.ContextMenuStrip = contextMenuStrip2;
        //    lblLog2.Text = "ContextMenuStrip value is: " + ViewModel.frm2.ContextMenuStrip.ID + ".";

        //}

        //private void btnReset_Click()
        //{
        //    LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

        //    ViewModel.frm2.ContextMenuStrip = null;
        //    lblLog2.Text = "ContextMenuStrip value is null.";
        //}
      
    }
}