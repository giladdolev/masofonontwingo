﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Height="720px" Width="830px" ID="windowView2" Text="GridColumn SortMode Property" LoadAction="GridGridColumnSortMode\OnLoad">
        <vt:Label runat="server" SkinID="Title" Text="SortMode" ID="lblTitle"></vt:Label>

        <vt:Button runat="server" Text="For more information about SortMode Property click here >>" Top="70px" width="350px" Left="80px" ID="btnSortModesInformation" ClickAction="GridGridColumnSortMode\btnSortModesInformation_Click"></vt:Button>
                
        <vt:Grid runat="server" Top="110px" Left="80px" cssclass="vt-testedGrid1" ID="TestedGrid1" Width="470px" Height="140px">
             <Columns>
                <vt:GridColumn  runat="server" ID="GridColumn1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn4" HeaderText="Column4" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn5" HeaderText="Column5" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>      
         </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="260px" ID="lblLog1" Width="470px"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px" ID="lblExample"></vt:Label>
           
        <vt:Label runat="server" Text="In the following example Column6 is set to SortMode 'NotSortable' and Column10 SortMode is set to 'Programmatic'. 
            When set to 'Programmatic' it is possible to display sorting glyph through the SortGlyphDirection property
             as explained in the link above. 
            Use the RadioButtons bellow to set Column10 SortGlyphDirection with one of the SortOrder values.
             Use the buttons bellow to sort Column6, Column10 and to change Column6 SortMode" Top="330px"></vt:Label>

        <vt:Grid runat="server" Top="445px" cssclass="vt-testedGrid2" Left="80px" ID="TestedGrid2" Width="470px" Height="135px">
             <Columns>
                <vt:GridColumn  runat="server" ID="GridColumn6" HeaderText="Column6" Height="20px" Width="85px" SortMode="NotSortable"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn7" HeaderText="Column7" Height="20px" Width="85px" SortMode="NotSortable"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn8" HeaderText="Column8" Height="20px" Width="85px" SortMode="NotSortable"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn9" HeaderText="Column9" Height="20px" Width="85px" SortMode="Automatic"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn10" HeaderText="Column10" Height="20px" Width="85px" SortMode="Programmatic"></vt:GridColumn>
            </Columns>      
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="595px" ID="lblLog2" Width="470px" Height="50px"></vt:Label>

        <vt:Button runat="server" ID="btnSortColumn6" Text="Sort Column6 - Ascending\Descending >" ClickAction="GridGridColumnSortMode\btnSortColumn6_click" Top="450px" Width="225px" Left="570px"></vt:Button>

        <vt:Button runat="server" ID="btnSortColumn10" Text="Sort Column10 - Ascending\Descending >" ClickAction="GridGridColumnSortMode\btnSortColumn10_click" Top="490px" Width="225px" Left="570px"></vt:Button>

        <vt:GroupBox runat="server" ID="GroupBox1" Font-Bold="true" Text="Column10 SortGlyphDirection" Top="525px" Height="105px" Width="160px" Left="595px">
            <vt:RadioButton runat="server" ID="rdoAscending" Text="Ascending" Top="15px" Left="20px" CheckedChangedAction="GridGridColumnSortMode\rdoAscending_CheckedChanged"></vt:RadioButton>
            <vt:RadioButton runat="server" ID="rdoDescending" Text="Descending" Top="35px" Left="20px" CheckedChangedAction="GridGridColumnSortMode\rdoDescending_CheckedChanged"></vt:RadioButton>
            <vt:RadioButton runat="server" ID="rdoNone" Text="None" Top="55px" Left="20px" CheckedChangedAction="GridGridColumnSortMode\rdoNone_CheckedChanged"></vt:RadioButton>


        </vt:GroupBox>

        <vt:Button runat="server" ID="btnChangeColumn6SortMode" Text="Change Column6 SortMode >>" ClickAction="GridGridColumnSortMode\btnChangeColumn6SortMode_click" Top="665px" Width="200px" ></vt:Button>
    </vt:WindowView>

 </asp:Content>
