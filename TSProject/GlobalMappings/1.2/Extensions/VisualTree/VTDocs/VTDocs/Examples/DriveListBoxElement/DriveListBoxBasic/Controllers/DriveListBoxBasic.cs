﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Compatibility;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    public class DriveListBoxBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void SelectedItemChanged(object sender, EventArgs e)
        {
            DriveListBoxElement flb1 = this.GetVisualElementById<DriveListBoxElement>("dlb1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = String.Format("{0} drive selected", flb1.Drive.Name);
        }
    }
}