using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridEditCellByPositionController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid2");

            //Create row          
            GridRow gridRow = new GridRow();
            GridContentCell gridCellElement1 = new GridContentCell();
            gridCellElement1.Value = "new Content";

            //Add to grid
            gridElement.Rows.Add(gridRow);  
            gridElement.RefreshData();
            gridElement.Refresh();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid2");
            gridElement.EditCellByPosition(0, 0);
            lblLog1.Text = "Method: EditCellByPosition";
            txtEventLog.Text += "\nEdit Cell: ( " + gridElement.SelectedColumnIndex + " ," + gridElement.SelectedRowIndex + " )";

        }
        private void button3_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid2");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Method: stop EditCellByPosition";
            txtEventLog.Text += "\nStop edit Cell";
        }
        
    }
}
