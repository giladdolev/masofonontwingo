using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformLocationChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLocationChanged_Click(object sender, EventArgs e)
        {
            
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");
            testedTextBox.PerformLocationChanged(e);
        }

        public void testedTextBox_LocationChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TestedTextBox LocationChanged event method is invoked");
        }

    }
}