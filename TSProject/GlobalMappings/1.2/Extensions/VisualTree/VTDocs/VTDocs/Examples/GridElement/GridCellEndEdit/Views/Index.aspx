﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CellEndEdit event" ID="windowView2" LoadAction="GridCellEndEdit\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CellEndEdit" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when edit mode ends for the selected cell.

         Syntax: public event EventHandler CellEndEdit"
            Top="75px" ID="Label1">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can edit the cell content by selecting it with the mouse,
            and then end editing by clicking Enter (on keyboard) or selecting another cell (with the mouse).
            Each invocation of the 'CellEndEdit' event should add a line in the 'Event Log'."
            Top="205px" ID="lblExp2">
        </vt:Label>
        <vt:Grid runat="server" Top="285px" Width="550px" Height="160px" ID="TestedGrid" CellEndEditAction="GridCellEndEdit/TestedGrid_CellEndEdit">
             <Columns>
                
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol1" HeaderText="CheckBoxCol" Height="20px" Width="90px"></vt:GridCheckBoxColumn>
              
                  <vt:GridComboBoxColumn  runat="server" ID="GridCol2" HeaderText="ComboBoxCol" Height="20px" Width="100px"></vt:GridComboBoxColumn>
               
                <vt:GridTextBoxColumn  runat="server" ID="GridCol3" HeaderText="TextBoxCol" Height="20px" Width="90px"></vt:GridTextBoxColumn>

                 <vt:GridTextButtonColumn  runat="server" ID="GridCol4" HeaderText="TextButtonCol" Height="20px" Width="100px" DataMember="TextButtonColumn"></vt:GridTextButtonColumn>

                 <vt:GridDateTimePickerColumn  runat="server" ID="GridCol5" HeaderText="DateTimePickerCol" Height="20px" Width="125px"></vt:GridDateTimePickerColumn>
            </Columns>
        </vt:Grid>
        
        <vt:Label runat="server" SkinID="Log" Top="460px" Width ="550px" Height="40px" ID="lblLog1"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Width ="550px" Top="525px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Clear Event Log >>" Top="635px" ID="btnClear" ClickAction="GridCellEndEdit\btnClear_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Empty Row >>" Top="635px" left="300px" ID="btnAddEmptyRow" ClickAction="GridCellEndEdit\btnAddEmptyRow_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
