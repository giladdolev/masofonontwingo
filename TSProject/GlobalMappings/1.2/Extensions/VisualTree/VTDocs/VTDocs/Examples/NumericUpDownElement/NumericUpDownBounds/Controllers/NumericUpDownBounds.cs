using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "NumericUpDown bounds are: \\r\\n" + TestedNumericUpDown.Bounds;

        }
        public void btnChangeNumericUpDownBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            if (TestedNumericUpDown.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedNumericUpDown.Bounds = new Rectangle(80, 355, 170, 24);
                lblLog.Text = "NumericUpDown bounds are: \\r\\n" + TestedNumericUpDown.Bounds;

            }
            else
            {
                TestedNumericUpDown.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "NumericUpDown bounds are: \\r\\n" + TestedNumericUpDown.Bounds;
            }

        }

    }
}