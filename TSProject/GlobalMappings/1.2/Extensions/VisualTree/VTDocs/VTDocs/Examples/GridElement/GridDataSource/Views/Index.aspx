﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Data Source" Height="800px"  Width="770px" LoadAction="GridDataSource\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="DataSource" Top="15px" ID="lblTitle" Left="300"> </vt:Label>
        <vt:Label runat="server" Text="Gets or sets the data source that the Grid is displaying data for. 
            
            syntax: public virtual object DataSource { get; set; }"

           Top="65px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the Grid is initially set with a DataSource.
            You can edit the data in the Grid, clear the DataSource by the 'Empty DataSource' button or 
            set it with the DataSource again. " Top="200px" ID="Label2">
        </vt:Label>
       
         <vt:Label runat="server" Text="TestedGrid1" Left="80px" Top="280px" ID="lblGrid1Title" ></vt:Label>
         <vt:Grid runat="server" CssClass="vt-testedgrid-1" Top="300px" Height="150px" ID="TestedGrid1" Width="530px" CellEndEditAction="GridDataSource\TestedGrid1_CellEndEdit">
        </vt:Grid>


        <vt:Label runat="server" Text="TestedGrid2" Top="460px" Left="80px" ID="lblGrid2Title" ></vt:Label>
        <vt:Grid runat="server" CssClass="vt-testedgrid-2" Top="480px" Height="150px" ID="TestedGrid2" Width="530px" CellEndEditAction="GridDataSource\TestedGrid2_CellEndEdit">
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Width="530px" Top="660px" ID="lblLog" ></vt:Label>
      
        <vt:Button runat="server" Text="Empty DataSource >>" Left ="50px" Width="220px" Top="695px" ID="btnEmptyDataSource" ClickAction="GridDataSource\btnEmptyDataSource_Click"></vt:Button>

        <vt:Button runat="server" Text="Set DataSource >>" Width="220px" Top="695px" Left="280px" ID="btnSetDataSource" ClickAction="GridDataSource\btnSetDataSource_Click"></vt:Button>

         <vt:Button runat="server" Text="Set TestedGrid1 to TestedGrid2 >>" Width="220px"  Top="695px" Left="510px" ID="Button1" ClickAction="GridDataSource\btnSetTestedGrid1ToTestedGrid2_Click"></vt:Button>

        <vt:Button runat="server" Text="Set Grid1 TextBox Cell(0,1) >>" Left ="50px" Width="220px" Top="725px" ID="btnSetGrid1TextBoxCell" ClickAction="GridDataSource\btnSetGrid1TextBoxCell_Click"></vt:Button>

        <vt:Button runat="server" Text="Set Grid1 CheckBox Cell(0,2) >>" Width="220px" Top="725px" Left="280px" ID="btnSetGrid1CheckBoxCell" ClickAction="GridDataSource\btnSetGrid1CheckBoxCell_Click"></vt:Button>

         <vt:Button runat="server" Text="Set Grid1 ComboBox Cell(2,4) >>" Width="220px"  Top="725px" Left="510px" ID="btnSetGrid1ComboBoxCell" ClickAction="GridDataSource\btnSetGrid1ComboBoxCell_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>

