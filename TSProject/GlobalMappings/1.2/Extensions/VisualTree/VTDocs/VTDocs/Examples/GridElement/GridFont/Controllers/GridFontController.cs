using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "Font value: " + TestedGrid1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGrid1);
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "Font value: " + TestedGrid2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGrid2);
        }

        private void btnChangeGridFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            if (TestedGrid2.Font.Underline == true)
            {
                TestedGrid2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedGrid2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGrid2);
            }
            else if (TestedGrid2.Font.Bold == true)
            {
                TestedGrid2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedGrid2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGrid2);
            }
            else if (TestedGrid2.Font.Italic == true)
            {
                TestedGrid2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedGrid2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGrid2);
            }
            else
            {
                TestedGrid2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedGrid2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGrid2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}