<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton Visible Property" ID="windowView2"  LoadAction="RadioButtonVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:RadioButton runat="server" Text="RadioButton" Top="155px" ID="TestedRadioButton1"></vt:RadioButton>
        
        <vt:Label runat="server" SkinID="Log" Top="195px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="260px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this RadioButton is initially set to 'false'
             The RadioButton should be invisible" Top="310px" ID="lblExp1"></vt:Label>

        <vt:RadioButton runat="server" Text="TestedRadioButton" Visible="false" Top="375px" ID="TestedRadioButton2"></vt:RadioButton>
        
        <vt:Label runat="server" SkinID="Log" Top="415px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="480px" ID="Button1" Width="200px" ClickAction ="RadioButtonVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        