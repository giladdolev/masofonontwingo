<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox GotFocus event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

         <vt:Label runat="server" Text="Focus the GroupBox by using the keyboard or Mouse to invoke GotFocus event method" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" ></vt:Label>

        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="50px" Left="200px" ID="grpGotFocus" Height="70px"  Width="200px" GotFocusAction ="GroupBoxGotFocus\grpGotFocus_GotFocus">
            <vt:Button runat="server" Text="button1" Top="30px" Left="40px" ID="button1" Height="26px" Width="100px"></vt:Button>
        </vt:GroupBox>          

        <vt:TextBox runat="server" Text="" Top="150px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
