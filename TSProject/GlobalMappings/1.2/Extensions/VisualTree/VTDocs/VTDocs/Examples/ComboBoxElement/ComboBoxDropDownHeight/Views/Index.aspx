<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox DropDownHeight Property" ID="windowView1" LoadAction="ComboBoxDropDownHeight\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Left="270px" Text="DropDownHeight" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height in pixels of the drop-down portion of the ComboBox.
            
            Syntax: public int DropDownHeight { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox" Top="160px" ID="ComboBox" CssClass="vt-test-cmb1">
            <Items>
                <vt:ListItem runat="server" Text="Item1"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item2"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item3"></vt:ListItem>
            </Items>
        </vt:ComboBox>    

        <vt:Label runat="server" SkinID="Log" Top="250px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="DropDownHeight property of this ComboBox is initially set to 40." Top="350px" ID="lblExp1"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" Top="410px" ID="TestedComboBox" CssClass="vt-test-cmb2" DropDownHeight="40">
            <Items>
                <vt:ListItem runat="server" Text="Item1"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item2"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item3"></vt:ListItem>
            </Items>
        </vt:ComboBox>    

        <vt:Label runat="server" SkinID="Log" Top="520px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change DropDownHeight value >>" Top="590px" ID="btnChangeDropDownHeight" Width="190px" ClickAction="ComboBoxDropDownHeight\btnChangeDropDownHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

