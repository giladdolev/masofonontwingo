<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:CheckBox runat="server" Text="Tested CheckBox" Top="150px" Left="200px" ID="TestedCheckBox" Height="36px" Width="100px" ClickAction="CheckBoxPerformClick\TestedCheckBox_Click"></vt:CheckBox>

        <vt:Button runat="server" Text="PerformClick of 'Tested CheckBox'" Top="45px" Left="140px" ID="btnPerformClick" Height="36px" Width="220px" ClickAction="CheckBoxPerformClick\btnPerformClick_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
