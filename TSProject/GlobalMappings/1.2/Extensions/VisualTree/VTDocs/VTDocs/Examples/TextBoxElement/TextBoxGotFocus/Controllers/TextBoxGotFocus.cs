using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxGotFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Fill up the form...";
        }

        public void txtFirstName_GotFocus(object sender, EventArgs e)
        {
            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTextBox: FirstName, GotFocus event is invoked";
            lblLog.Text = "FirstName TextBox has the Focus";
        }

        public void txtFirstName_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            string key = "";
            
            txtFirstName.ForeColor = Color.Black;

            if (txtFirstName.Text.IndexOf("Enter First Name") == 0)
            {
                key = txtFirstName.Text.Substring(17);
                txtFirstName.Clear();
                txtFirstName.Text = key;
            } 
            else if (txtFirstName.Text.IndexOf("Enter First Nam") == 0)
            {
                txtFirstName.Clear();
            }
        }

        public void txtLastName_GotFocus(object sender, EventArgs e)
        {
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTextBox: LastName, GotFocus event is invoked";            
            lblLog.Text = "LastName TextBox has the Focus";            
        }

        public void txtLastName_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            string key = "";

            txtLastName.ForeColor = Color.Black; 

            if (txtLastName.Text.IndexOf("Enter Last Name") == 0)
            {
                key = txtLastName.Text.Substring(15);
                txtLastName.Clear();
                txtLastName.Text = key;
            }
            else if (txtLastName.Text.IndexOf("Enter Last Nam") == 0)
            {
                txtLastName.Clear();
            }
        }

        public void btnChangeFocus_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Button has the Focus"; 
        }

        public void btnChangeFocus_Click(object sender, EventArgs e)
        {
            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtFirstName.Focus();
            lblLog.Text = "FirstName TextBox has the Focus";
        }

    }
}