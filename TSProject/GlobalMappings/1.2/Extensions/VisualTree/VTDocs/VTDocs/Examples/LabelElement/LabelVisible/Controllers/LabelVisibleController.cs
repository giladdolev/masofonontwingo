﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelVisibleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Visible value: " + TestedLabel1.Visible;
            lblLog2.Text = "Visible value: " + TestedLabel2.Visible + '.';

        }

        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedLabel2.Visible = !TestedLabel2.Visible;
            lblLog2.Text = "Visible value: " + TestedLabel2.Visible + '.';
        }
    }
}