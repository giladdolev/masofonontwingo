using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Resources;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BackGroundImageGroupBoxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void ChangeBackGroundImage_Click(object sender, EventArgs e)
        {
            GroupBoxElement groupBox = this.GetVisualElement<GroupBoxElement>("grb1");
            if (groupBox.BackgroundImage.Source == "Content/Images/gizmox.png")
                groupBox.BackgroundImage = new UrlReference(@"Content/Images/galilcs.png");
            else
                groupBox.BackgroundImage = new UrlReference(@"Content/Images/gizmox.png");
        }
    }
}