using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformMouseUpController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseUp_Click(object sender, EventArgs e)
        {
            
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");
            
            MouseEventArgs args = new MouseEventArgs();

            testedPanel.PerformMouseUp(args);
        }

        public void testedPanel_MouseUp(object sender, EventArgs e)
        {
            MessageBox.Show("TestedPanel MouseUp event method is invoked");
        }

    }
}