using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPerformCheckedChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformCheckedChanged_Click(object sender, EventArgs e)
        {

            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");

             ValueChangedArgs<bool> eval = new ValueChangedArgs<bool>(true);

             TestedRadioButton.PerformCheckedChanged(eval);
        }

       
        public void TestedRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRadioButton Click event method is invoked");
        }

    }
}