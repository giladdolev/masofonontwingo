<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme = "Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="ToolBar BackgroundImage Property" ID="windowView1" LoadAction="ToolBarBackgroundImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImage" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image displayed in the control" Top="75px" ID="lblDefinition" ></vt:Label>
         
         <!-- Tested ToolBar1 -->
        <vt:ToolBar runat="server" Top="120px" ID="TestedToolBar1" Dock="None">

            <vt:ToolBarButton runat="server" id="ToolBarItem1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel1" Text="ToolBarLabel1" ></vt:ToolBarLabel>

        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="170px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackgroundImage property of this 'ToolBar' is initially set with image" Top="330px"  ID="lblExp1"></vt:Label>   

        <!-- Tested ToolBar2 -->
        <vt:ToolBar runat="server" BackgroundImage ="~/Content/Images/icon.jpg" Top="380px" Dock="None" ID="TestedToolBar2">

            <vt:ToolBarButton runat="server" id="ToolBarButton1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarButton2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel2" Text="ToolBarLabel1" ></vt:ToolBarLabel>

        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change BackgroundImage >>"  Top="490px" Width="180px" ID="btnChangeBackgroundImage" ClickAction="ToolBarBackgroundImage\btnChangeBackgroundImage_Click"></vt:Button>

              

    </vt:WindowView>

</asp:Content>