using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonTextAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeTextAlign_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeTextAlign = this.GetVisualElementById<ButtonElement>("btnChangeTextAlign");

            if (btnChangeTextAlign.TextAlign == ContentAlignment.MiddleLeft) //Get
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.MiddleRight;//Set
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.MiddleRight)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.MiddleCenter;
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.MiddleCenter)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.BottomCenter;
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.BottomCenter)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.BottomLeft;
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.BottomLeft)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.BottomRight;
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.BottomRight)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.TopCenter;
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.TopCenter)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.TopLeft;
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.TopLeft)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.TopRight;
            }
            else if (btnChangeTextAlign.TextAlign == ContentAlignment.TopRight)
            {
                btnChangeTextAlign.TextAlign = ContentAlignment.MiddleLeft;
            }

        }
      
    }
}