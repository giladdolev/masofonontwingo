﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class ComboBoxComboBoxColumnCollectionAddComboBoxColumn : WindowElement
    {

        public ComboBoxComboBoxColumnCollectionAddComboBoxColumn()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        int intCounter;
        int stringCounter;


        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>

        public int intCounterProperty
        {
            get { return this.intCounter; }
            set { this.intCounter = value; }
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>

        public int stringCounterProperty
        {
            get { return this.stringCounter; }
            set { this.stringCounter = value; }
        }

    }
}