using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonAutoSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeAutoSize_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeAutoSize = this.GetVisualElementById<ButtonElement>("btnChangeAutoSize");
            RadioButtonElement rdoAutoSizeChange = this.GetVisualElementById<RadioButtonElement>("rdoAutoSizeChange");

            if (rdoAutoSizeChange.AutoSize == false)
            {
                rdoAutoSizeChange.AutoSize = true;
                btnChangeAutoSize.Text = "AutoSize = True";
            }
            else
            {
                rdoAutoSizeChange.AutoSize = false;
                btnChangeAutoSize.Text = "AutoSize = False";
            }
        }
        //btnChangeText_Click
        private void btnChangeText_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoAutoSizeChange = this.GetVisualElementById<RadioButtonElement>("rdoAutoSizeChange");
            if (rdoAutoSizeChange.Text == "Tested Button")
            {
                rdoAutoSizeChange.Text = "Some great new Text";

            }
            else
                rdoAutoSizeChange.Text = "Tested Button";
        }

    }
}