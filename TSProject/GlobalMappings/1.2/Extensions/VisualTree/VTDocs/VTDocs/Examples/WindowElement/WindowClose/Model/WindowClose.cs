﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowClose : WindowElement
    {

        CloseTestedWindow _TestedWin;

        public WindowClose()
        {

        }

        public CloseTestedWindow TestedWin
        {
            get { return this._TestedWin; }
            set { this._TestedWin = value; }
        }
       

    }
}