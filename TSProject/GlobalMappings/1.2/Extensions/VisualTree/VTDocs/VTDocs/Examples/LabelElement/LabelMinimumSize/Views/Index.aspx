<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label MinimumSize Property" ID="windowView" LoadAction="LabelMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedLabel' is width: 100px and height: 80px. 
            Its MinimumSize is initially set to width: 200px and height: 100px.
            To cancel 'TestedLabel' MinimumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label>     

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" ID="TestedLabel" Width="100px" Height="80px" BorderStyle="Solid" Left="80px" MinimumSize="200, 100" Top="370px"></vt:Label> 

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog" Width="400px" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="560px" Width="200px" ID="btnChangeLblMinimumSize" ClickAction="LabelMinimumSize\btnChangeLblMinimumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="560px" width="200px" Left="310px" ID="btnSetToZeroLblMinimumSize" ClickAction="LabelMinimumSize\btnSetToZeroLblMinimumSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>









