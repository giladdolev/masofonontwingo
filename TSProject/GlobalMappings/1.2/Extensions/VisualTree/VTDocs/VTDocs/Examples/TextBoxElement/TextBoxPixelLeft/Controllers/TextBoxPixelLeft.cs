using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelLeft value: " + TestedTextBox.PixelLeft + '.';
        }

        public void btnChangePixelLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedTextBox.PixelLeft == 80)
            {
                TestedTextBox.PixelLeft = 180;
                lblLog.Text = "PixelLeft value: " + TestedTextBox.PixelLeft + '.';
            }
            else
            {
                TestedTextBox.PixelLeft = 80;
                lblLog.Text = "PixelLeft value: " + TestedTextBox.PixelLeft + '.';
            }
        }
      
    }
}