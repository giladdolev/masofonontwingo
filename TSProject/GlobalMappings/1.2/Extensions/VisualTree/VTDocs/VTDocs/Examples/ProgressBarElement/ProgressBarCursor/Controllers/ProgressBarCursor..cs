using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            ProgressBarElement prgRuntimeCursor = this.GetVisualElementById<ProgressBarElement>("prgRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (prgRuntimeCursor.Cursor == CursorsElement.Default)
            {
                prgRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                prgRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                prgRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                prgRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.Help)
            {
                prgRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                prgRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                prgRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.No)
            {
                prgRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                prgRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                prgRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;

            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                prgRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                prgRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                prgRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                prgRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                prgRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                prgRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                prgRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                prgRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else if (prgRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                prgRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
            else 
            {
                prgRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + prgRuntimeCursor.Cursor;
            }
        }

    }
}