<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Visible Property" ID="windowView2"  LoadAction="ComboBoxVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox" Top="180px" ID="TestedComboBox1"></vt:ComboBox>
        
        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="275px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this ComboBox is initially set to 'false'
             The ComboBox should be invisible" Top="325px" ID="lblExp1"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" Visible="false" Top="390px" ID="TestedComboBox2"></vt:ComboBox>
        
        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="495px" ID="Button1" Width="200px" ClickAction ="ComboBoxVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        