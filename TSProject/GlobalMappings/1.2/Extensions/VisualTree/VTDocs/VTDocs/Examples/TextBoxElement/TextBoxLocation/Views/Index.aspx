<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Location Property" ID="windowView1" LoadAction="TextBoxLocation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Location" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the coordinates of the upper-left corner of the control relative to the 
            upper-left corner of its container.

            Syntax: public Point Location { get; set; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="Location property of this TextBox is initially set to: Left='80px' Top='300px'" Top="250px" ID="lblExp1"></vt:Label>

        <vt:TextBox runat="server" Text="TestedTextBox" Top="300px" ID="TestedTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="340px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Location value >>" Top="405px" ID="btnChangeLocation" Width="180px" ClickAction="TextBoxLocation\btnChangeLocation_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
