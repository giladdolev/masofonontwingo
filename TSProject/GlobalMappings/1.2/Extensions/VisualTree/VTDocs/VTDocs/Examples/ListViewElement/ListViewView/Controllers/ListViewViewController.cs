using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewViewController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "View value is: " + TestedListView1.View; //The default value
            lblLog2.Text = "Click the button to change the visibility of the columns headers...";

        }
        private void ChangeHeaderVisibility_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedListView2.View == System.Web.VisualTree.ItemsView.Details)
                TestedListView2.View = System.Web.VisualTree.ItemsView.LargeIcon;
            else
                TestedListView2.View = System.Web.VisualTree.ItemsView.Details;

            lblLog2.Text = "View value is: " + TestedListView2.View + ".";
        }
    }
}