using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPerformDragEnterController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformDragEnter_Click(object sender, EventArgs e)
        {
            DragEventArgs args = new DragEventArgs();
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");

            testedProgressBar.PerformDragEnter(args);
        }

        public void testedProgressBar_DragEnter(object sender, EventArgs e)
        {
            MessageBox.Show("TestedProgressBar DragEnter event method is invoked");
        }

    }
}