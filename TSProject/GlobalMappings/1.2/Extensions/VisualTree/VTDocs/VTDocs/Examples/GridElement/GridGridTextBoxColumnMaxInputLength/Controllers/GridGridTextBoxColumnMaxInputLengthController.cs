using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridGridTextBoxColumnMaxInputLengthController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}


        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog.Text = "Column1 MaxInputLength value is: " + TestedGrid.Columns[0].MaxInputLength;
        }


        public void btnChangeColumn1MaxInputLength_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridTextBoxColumn Column1 = this.GetVisualElementById<GridTextBoxColumn>("GridCol1");
            TextBoxElement txtInputColumnText = this.GetVisualElementById<TextBoxElement>("txtInputColumnText");
            

            
            
            if (TestedGrid.Columns[0].MaxInputLength == 3)
            {
                TestedGrid.Columns[0].MaxInputLength = 5;
                lblLog.Text = "Column1 maxInputLength value: " + Column1.MaxInputLength + "; Column1 cell(0,0) value: " + TestedGrid.Rows[0].Cells[0].Value + "\\r\\nInput Text: " + txtInputColumnText.Text;
            }
            else 
            {
                TestedGrid.Columns[0].MaxInputLength = 3;
                lblLog.Text = "Column1 maxInputLength value: " + Column1.MaxInputLength + "; Column1 cell(0,0) value: " + TestedGrid.Rows[0].Cells[0].Value + "\\r\\nInput Text: " + txtInputColumnText.Text;
            }
           
        }

        public void btnChangeColumn1Text_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridTextBoxColumn Column1 = this.GetVisualElementById<GridTextBoxColumn>("GridCol1");
            TextBoxElement txtInputColumnText = this.GetVisualElementById<TextBoxElement>("txtInputColumnText");

            //GridContentCell FirstCellContent = new GridContentCell();
            //FirstCellContent.Value = txtInputColumnText.Text;
            TestedGrid.Rows[0].Cells[0].Value = txtInputColumnText.Text;
            TestedGrid.RefreshData();
            TestedGrid.Refresh();


            lblLog.Text = "Column1 maxInputLength value: " + Column1.MaxInputLength + "; Column1 cell(0,0) value: " + TestedGrid.Rows[0].Cells[0].Value + "\\r\\nInput Text: " + txtInputColumnText.Text;
        }
    

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInputColumnText = this.GetVisualElementById<TextBoxElement>("txtInputColumnText");

            if (txtInputColumnText.Text.Contains("Enter Text") && txtInputColumnText.Text.Length > 12)
            {
                txtInputColumnText.Text = txtInputColumnText.Text.Substring(12);
                txtInputColumnText.ForeColor = Color.Black;
               
            }
            else if (txtInputColumnText.Text.Contains("Enter Text"))
            {
                txtInputColumnText.Text = "";
                txtInputColumnText.ForeColor = Color.Black;
            }

            string temp = lblLog.Text;
            string[] lines = temp.Split(new string[] { "\\r\\n" }, StringSplitOptions.None);
            lblLog.Text = lines[0];
            lblLog.Text += "\\r\\nInput Text: " + txtInputColumnText.Text;

        }
       
        
	}
}
