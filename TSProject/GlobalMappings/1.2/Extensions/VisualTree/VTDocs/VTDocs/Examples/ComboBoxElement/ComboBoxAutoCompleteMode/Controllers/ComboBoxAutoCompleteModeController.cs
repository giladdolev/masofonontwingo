using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxAutoCompleteModeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "AutoCompleteMode is " + TestedComboBox1.AutoCompleteMode.ToString();
            lblLog2.Text = "AutoCompleteMode is " + TestedComboBox2.AutoCompleteMode.ToString() + ".";
        
        }

        public void btnChangeAutoCompleteMode_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedComboBox2.AutoCompleteMode == AutoCompleteMode.None)
            {
                TestedComboBox2.AutoCompleteMode = AutoCompleteMode.Append;
                lblLog2.Text = "AutoCompleteMode is " + TestedComboBox2.AutoCompleteMode.ToString() + ".";
            }
            else if (TestedComboBox2.AutoCompleteMode == AutoCompleteMode.Append)
            {
                TestedComboBox2.AutoCompleteMode = AutoCompleteMode.Suggest;
                lblLog2.Text = "AutoCompleteMode is " + TestedComboBox2.AutoCompleteMode.ToString() + ".";
            }
            else if (TestedComboBox2.AutoCompleteMode == AutoCompleteMode.Suggest)
            {
                TestedComboBox2.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                lblLog2.Text = "AutoCompleteMode is " + TestedComboBox2.AutoCompleteMode.ToString() + ".";
            }
            else
            {
                TestedComboBox2.AutoCompleteMode = AutoCompleteMode.None;
                lblLog2.Text = "AutoCompleteMode is " + TestedComboBox2.AutoCompleteMode.ToString() + ".";
            }
        }

        public void TestedComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            
            lblLog1.Text = "AutoCompleteMode is " + TestedComboBox1.AutoCompleteMode.ToString() + "\\r\\nSelectedItem: " + Convert.ToString(TestedComboBox1.SelectedItem) + ", Selected index: " + TestedComboBox1.SelectedIndex.ToString();
            }

        public void TestedComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            
            lblLog2.Text = "AutoCompleteMode is " + TestedComboBox2.AutoCompleteMode.ToString() + ".\\r\\nSelectedItem: " + Convert.ToString(TestedComboBox2.SelectedItem) + ", Selected index: " + TestedComboBox2.SelectedIndex.ToString();
             }


        //private void a(object sender, EventArgs e)
        //{
        //    ComboBoxElement comboBox1 = this.GetVisualElementById<ComboBoxElement>("comboBox1");
        //    object s = comboBox1.SelectedItem;
        //}

    }
}