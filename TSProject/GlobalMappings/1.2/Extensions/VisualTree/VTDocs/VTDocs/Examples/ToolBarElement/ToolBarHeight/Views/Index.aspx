<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Height Property" ID="windowView1" LoadAction="ToolBarHeight\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="Height" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int Height { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this ToolBar is initially set to: 80px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:ToolBar runat="server" Text="TestedToolBar"  Top="300px" ID="TestedToolBar" Dock="None" Height="80px">
            <vt:ToolBarButton runat="server" id="ToolBarItem1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>    

        <vt:Label runat="server" SkinID="Log" Top="395px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Height value >>" Top="460px" ID="btnChangeHeight" TabIndex="3" Width="180px" ClickAction="ToolBarHeight\btnChangeHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>