using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ListView MinimumSize value: " + TestedListView.MinimumSize + "\\r\\nListView Size value: " + TestedListView.Size;
        }

        public void btnChangeLvwMinimumSize_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedListView.MinimumSize == new Size(360, 80))
            {
                TestedListView.MinimumSize = new Size(200, 100);
                lblLog1.Text = "ListView MinimumSize value: " + TestedListView.MinimumSize + "\\r\\nListView Size value: " + TestedListView.Size;
            }
            else if (TestedListView.MinimumSize == new Size(200, 100))
            {
                TestedListView.MinimumSize = new Size(400, 60);
                lblLog1.Text = "ListView MinimumSize value: " + TestedListView.MinimumSize + "\\r\\nListView Size value: " + TestedListView.Size;
            }
            else
            {
                TestedListView.MinimumSize = new Size(360, 80);
                lblLog1.Text = "ListView MinimumSize value: " + TestedListView.MinimumSize + "\\r\\nListView Size value: " + TestedListView.Size;
            }

        }

        public void btnSetToZeroLvwMinimumSize_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedListView.MinimumSize = new Size(0, 0);
            lblLog1.Text = "ListView MinimumSize value: " + TestedListView.MinimumSize + "\\r\\nListView Size value: " + TestedListView.Size;

        }


    }
}