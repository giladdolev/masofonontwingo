using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridGroupFieldController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void View_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));
            source.Columns.Add("phone", typeof(string));

            source.Rows.Add("Lisa", "lisa@simpsons.com", "555-111-1224");
            source.Rows.Add("Bart", "Bart@simpsons.com", "555-111-1224");
            source.Rows.Add("Homer", "Homer@simpsons.com", "555-111-1224");
            source.Rows.Add("Lisa", "Lisa@simpsons.com", "555-111-1224");
            source.Rows.Add("Marge", "Marge@simpsons.com", "555-111-1224");
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", "555-111-1224");
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", "555-111-1224");

            gridElement.DataSource = source;
            gridElement.GroupField = "email";
        }


    }
}
