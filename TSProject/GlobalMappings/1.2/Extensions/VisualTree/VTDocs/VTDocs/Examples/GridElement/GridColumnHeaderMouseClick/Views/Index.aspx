﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Text="Grid ColumnHeaderMouseClick event" ID="windowView1" LoadAction="GridColumnHeaderMouseClick\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ColumnHeaderMouseClick" Left="200px" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the user clicks on a column header.

            Syntax: public event GridElementCellEventHandler ColumnHeaderMouseClick
            
            The default behavior is to order the grid rows based on the clicked column, or to reverse the sort order 
            if the grid is already sorted by the clicked column."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can click on the grid column header.
            Each invocation of the 'ColumnHeaderMouseClick' event should add a line in the 'Event Log'."
            Top="280px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="345px" ID="TestedGrid" ColumnHeaderMouseClickAction="GridColumnHeaderMouseClick\TestedGrid_HeaderClick"  >
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>    
         
        <vt:Label runat="server" SkinID="Log" Left="80px" Top="475px" Width="400px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="515px" Width="400px" ID="txtEventLog"></vt:TextBox>


    </vt:WindowView>

</asp:Content>
