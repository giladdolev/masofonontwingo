<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Panel" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Tab runat="server" Top="124px" Left="264px" ID="tabControl1" Height="100px" Width="400px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Button runat="server" Text="SelectedTab.Index" Top="100px" Left="109px" ClickAction="BasicTabElement\SelectedTabIndex_Click" ID="Button1" Height="23px" Width="117px"></vt:Button>
        <vt:Button runat="server" Text="Change tabs text" Top="165px" Left="109px" ClickAction="BasicTabElement\ChangeTabsText_Click" ID="ChangeTabsText" Height="23px" Width="117px">
        </vt:Button>
    </vt:WindowView>
</asp:Content>
