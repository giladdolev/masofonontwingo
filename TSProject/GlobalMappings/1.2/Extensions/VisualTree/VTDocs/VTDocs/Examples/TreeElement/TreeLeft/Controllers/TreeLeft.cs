using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Tree Left Value is: " + TestedTree.Left + '.';
        }
        public void ChangeLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            if (TestedTree.Left == 200)
            {
                TestedTree.Left = 80;
                lblLog.Text = "Tree Left Value is: " + TestedTree.Left + '.';

            }
            else
            {
                TestedTree.Left = 200;
                lblLog.Text = "Tree Left Value is: " + TestedTree.Left + '.';
            }
        }

    }
}