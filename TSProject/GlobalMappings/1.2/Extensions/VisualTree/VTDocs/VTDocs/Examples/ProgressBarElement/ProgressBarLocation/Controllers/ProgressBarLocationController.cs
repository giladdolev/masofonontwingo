using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarLocationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Location value: " + TestedProgressBar.Location;

        }
        //GroupBoxAlignment
        public void btnChangeProgressBarLocation_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.Location == new Point(80, 290))
            {
                TestedProgressBar.Location = new Point(120, 280);
                lblLog.Text = "Location value: " + TestedProgressBar.Location;
            }
            else
            {
                TestedProgressBar.Location = new Point(80, 290);
                lblLog.Text = "Location value: " + TestedProgressBar.Location;
            }
        }

    }
}