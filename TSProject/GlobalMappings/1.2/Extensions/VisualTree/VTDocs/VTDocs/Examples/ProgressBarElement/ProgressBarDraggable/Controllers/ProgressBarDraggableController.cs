using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //ProgressBarAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ProgressBarElement prgRuntimeDraggable = this.GetVisualElementById<ProgressBarElement>("prgRuntimeDraggable");

            if (prgRuntimeDraggable.Draggable == false)
            {
                prgRuntimeDraggable.Draggable = true;
                textBox1.Text = prgRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                prgRuntimeDraggable.Draggable = false;
                textBox1.Text = prgRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}