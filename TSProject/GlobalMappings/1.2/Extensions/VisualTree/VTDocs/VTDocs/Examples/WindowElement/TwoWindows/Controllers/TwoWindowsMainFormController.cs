﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using MvcApplication9.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MvcApplication9.Controllers
{
    public class TwoWindowsMainFormController : Controller
    {       
        public ActionResult Index()
        {
            return View(new TwoWindowsMainForm());
        }



        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "TwoWindows");
            return argsDictionary;
        }


        private async Task button1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < 5; i++)
            {
                var x = VisualElementHelper.CreateFromView<TwoWindowsForm1>("TwoWindowsForm1", "TwoWindowsForm1", null, GetArgsDictionary(), null);
                await x.ShowDialog();
            }



        }
    }
}
