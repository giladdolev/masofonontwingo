using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TextBoxAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TextBoxElement txtRuntimeDraggable = this.GetVisualElementById<TextBoxElement>("txtRuntimeDraggable");

            if (txtRuntimeDraggable.Draggable == false)
            {
                txtRuntimeDraggable.Draggable = true;
                textBox1.Text = txtRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                txtRuntimeDraggable.Draggable = false;
                textBox1.Text = txtRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}