﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class CheckBoxEquals : WindowElement
    {

        CheckBoxElement _TestedCheckBox;

        public CheckBoxEquals()
        {
            _TestedCheckBox = new CheckBoxElement();
        }

        public CheckBoxElement TestedCheckBox
        {
            get { return this._TestedCheckBox; }
            set { this._TestedCheckBox = value; }
        }
    }
}