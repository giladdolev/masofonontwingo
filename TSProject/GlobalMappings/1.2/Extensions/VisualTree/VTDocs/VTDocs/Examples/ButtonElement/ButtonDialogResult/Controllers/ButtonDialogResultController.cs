using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonDialogResultController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new ButtonDialogResult());
        }
        private ButtonDialogResult ViewModel
        {
            get { return this.GetRootVisualElement() as ButtonDialogResult; }
        }

        public async void btnShowDialog_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "ButtonElement");
            argsDictionary.Add("ExampleName", "ButtonDialogResult");

            ViewModel.frmDialog = VisualElementHelper.CreateFromView<ButtonWindowDialog>("ButtonWindowDialog", "Index2", null, argsDictionary, null);
            DialogResult dr = await ViewModel.frmDialog.ShowDialog();
            if (dr == DialogResult.OK)
                textBox1.Text = "User clicked OK button";
            else
                textBox1.Text = "User clicked Cancel button";
            


		}

    }
}