<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Padding Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Button runat="server" Text="button Padding is set to Padding-Top = 30" Padding-Top ="30"  Top="45px" Left="140px" ID="btnPadding" Height="36px" Font-Names=""  TabIndex="1" Width="300px"></vt:Button>           

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Button runat="server" Text="Change Button Padding" Top="115px" Left="140px" ID="btnChangePadding" Height="36px" TabIndex="1" Width="300px" ClickAction="ButtonPadding\btnChangePadding_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
