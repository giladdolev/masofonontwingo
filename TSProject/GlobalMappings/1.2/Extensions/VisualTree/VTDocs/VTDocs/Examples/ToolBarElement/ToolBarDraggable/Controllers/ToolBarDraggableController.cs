using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarDraggableController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void Load_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            ToolBarMenuItem menuItem2 = new ToolBarMenuItem();
            toolStrip1.Items.Add(menuItem2);
            menuItem2.Text = "menuItem2";
            menuItem2.Click += new EventHandler(btnChangeDraggable_Click);

        }

        public void btnChangeDraggable_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
			ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");

            toolStrip1.Draggable = !toolStrip1.Draggable;
            textBox1.Text = toolStrip1.Draggable.ToString();
                
		}

    }
}