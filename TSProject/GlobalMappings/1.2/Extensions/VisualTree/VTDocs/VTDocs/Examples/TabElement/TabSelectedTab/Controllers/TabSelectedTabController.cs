using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSelectedTabController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "SelectedTab value: " + TestedTab1.SelectedTab.Text;
            lblLog2.Text = "SelectedTab value: " + TestedTab2.SelectedTab.Text + '.';

        }

        public void cmbInput_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem tabPage1 = this.GetVisualElementById<TabItem>("tab2Page1");
            TabItem tabPage2 = this.GetVisualElementById<TabItem>("tab2Page2");
            TabItem tabPage3 = this.GetVisualElementById<TabItem>("tab2Page3");

            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ComboBoxElement cmbInput = this.GetVisualElementById<ComboBoxElement>("cmbInput");

            if (cmbInput.SelectedIndex == 0)
            {
                TestedTab2.SelectedTab = tabPage1;
                lblLog2.Text = "SelectedTab value: " + TestedTab2.SelectedTab.Text + '.';
            }
            else if (cmbInput.SelectedIndex == 1)
            {
                TestedTab2.SelectedTab = tabPage2;
                lblLog2.Text = "SelectedTab value: " + TestedTab2.SelectedTab.Text + '.';
            }
            else if (cmbInput.SelectedIndex == 2)
            {
                TestedTab2.SelectedTab = tabPage3;
                lblLog2.Text = "SelectedTab value: " + TestedTab2.SelectedTab.Text + '.';
            }
            else
                cmbInput.Text = "Select...";
          
        }

        public void TestedTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ComboBoxElement cmbInput = this.GetVisualElementById<ComboBoxElement>("cmbInput");

            lblLog2.Text = "SelectedTab value: " + TestedTab2.SelectedTab.Text + '.';
            cmbInput.Text = TestedTab2.SelectedTab.Text;
        }

        public void TestedTab1_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "SelectedTab value: " + TestedTab1.SelectedTab.Text;

        }
    
    }
}