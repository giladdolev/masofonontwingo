<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Leave event" ID="windowView2" LoadAction="CheckBoxLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the check boxes,
             a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Height="120px" Width="150px">
             <vt:CheckBox runat="server" Top="25px" Left="15px" Text="CheckBox 1" ID="chk1" TabIndex="1" LeaveAction="CheckBoxLeave\chk1_Leave"></vt:CheckBox>
            <vt:CheckBox runat="server" Top="70px" Left="15px" Text="CheckBox 2" ID="chk2" TabIndex="2" LeaveAction="CheckBoxLeave\chk2_Leave"></vt:CheckBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="420px" ID="lblLog" Top="470px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="510px" Width="420px" Height="140px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>


