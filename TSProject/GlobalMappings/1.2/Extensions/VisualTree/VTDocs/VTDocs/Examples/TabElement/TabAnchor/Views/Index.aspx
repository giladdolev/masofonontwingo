<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Anchor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Top="55px" Left="140px" Margin-All="0" Padding-All="0" ID="panel1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="300px">
        <vt:Tab runat="server" Text="Anchor is set to true" AutoSize ="true" Top="10px"  Left="20px" ID="tabAnchor" Height="200px" Width="250px">
             <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px" ImageIndex="0" >
                    <vt:Button runat="server" Text="Button1" Top="43px" Left="-20px" ID="Button1" Height="36px" Width="100px"></vt:Button>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem4" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>          
            </vt:Panel>

        <vt:Label runat="server" Text="RunTime" Top="400px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>




        <vt:Panel runat="server" Top="420px" Left="140px" Margin-All="0" Padding-All="0" ID="panel2" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="300px">
        <vt:Tab runat="server" Text="RuntimeTab" Top="10px" Left="20px" ID="tabRunTimeAnchor" Height="200px" TabIndex="1" Width="250px" >
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px" ImageIndex="0" >
                    <vt:Button runat="server" Text="Button1" Top="43px" Left="-20px" ID="Button2" Height="36px" Width="100px"></vt:Button>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
            
        </vt:Tab>
            </vt:Panel>

        <vt:Button runat="server"  Text="Change Tab Anchor" Top="550px" Left="470px" ID="btnChangeAnchor" Height="36px" TabIndex="1" Width="150px" ClickAction="TabAnchor\btnChangeAnchor_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="750px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>

