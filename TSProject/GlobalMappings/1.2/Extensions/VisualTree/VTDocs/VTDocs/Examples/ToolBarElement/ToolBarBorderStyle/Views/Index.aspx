<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar BorderStyle Property" ID="windowView1" LoadAction="ToolBarBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:ToolBar runat="server" Text="ToolBar" Top="170px" Dock="None" ID="TestedToolBar1">
            <vt:ToolBarButton runat="server" id="ToolBar1Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar1Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar1Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="225px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="290px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this ToolBar is initially set to 'Dotted'" Top="330px"  ID="lblExp1"></vt:Label>     
        
        <vt:ToolBar runat="server"  Text="TestedToolBar" Dock="None" BorderStyle="Dotted" Top="370px" ID="TestedToolBar2">
            <vt:ToolBarButton runat="server" id="ToolBar2Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar2Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar2Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="485px" ID="btnChangeToolBarBorderStyle" ClickAction="ToolBarBorderStyle\btnChangeToolBarBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
