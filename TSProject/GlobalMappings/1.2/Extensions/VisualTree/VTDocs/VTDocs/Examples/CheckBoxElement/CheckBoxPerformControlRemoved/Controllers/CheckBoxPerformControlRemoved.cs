using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformControlRemovedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlRemoved_Click(object sender, EventArgs e)
        {
            
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");

            ControlEventArgs args = new ControlEventArgs(testedCheckBox);

            testedCheckBox.PerformControlRemoved(args);
        }

        public void testedCheckBox_ControlRemoved(object sender, EventArgs e)
        {
            MessageBox.Show("TestedCheckBox ControlRemoved event method is invoked");
        }

    }
}