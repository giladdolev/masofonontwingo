using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            ButtonElement btnAccessibleRole = this.GetVisualElementById<ButtonElement>("btnAccessibleRole");

            MessageBox.Show(btnAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            ButtonElement btnAccessibleRole = this.GetVisualElementById<ButtonElement>("btnAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (btnAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else if (btnAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                btnAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = btnAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}