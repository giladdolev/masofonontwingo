using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridColumnHeaderMouseRightClickController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void btn1_raiseEvent(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "GridColumnHeaderMouseRightClick Event fired";
        }

        public void Window_Load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));
            source.Columns.Add("phone", typeof(DateTime));
            source.Columns.Add("boolt", typeof(bool));

            source.Rows.Add("Lisa", "lisa@simpsons.com", "10/10/2014", true);
            source.Rows.Add("Bart", "Bart@simpsons.com", "10/10/2014", false);
            source.Rows.Add("Homer", "Homer@simpsons.com", "10/10/2014", true);
            source.Rows.Add("Lisa", "Lisa@simpsons.com", "10/10/2014", false);
            source.Rows.Add("Marge", "Marge@simpsons.com", "10/10/2014", true);
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", "10/10/2014", false);
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", "10/10/2014", true);
            gridElement.DataSource = source;
        }

	    public void Grid_HeaderRightClick(object sender, EventArgs e)
	    {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text = "you clicked on column header number : " + ((GridElementCellEventArgs)e).ColumnIndex.ToString();

	    }
       
	}
}
