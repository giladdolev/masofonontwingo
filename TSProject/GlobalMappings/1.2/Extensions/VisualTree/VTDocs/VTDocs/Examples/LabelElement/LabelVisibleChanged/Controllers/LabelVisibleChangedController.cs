using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnTestedLabel_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");

            label.Visible = !label.Visible;
        }

        public void btnTestedLabel_VisibleChanged(object sender, EventArgs e)
        {
            LabelElement lblEvent = this.GetVisualElementById<LabelElement>("lblEvent");
            if (lblEvent != null)
                lblEvent.Text = "Label VisibleChanged event is invoked";
              
        }
 
    }
}