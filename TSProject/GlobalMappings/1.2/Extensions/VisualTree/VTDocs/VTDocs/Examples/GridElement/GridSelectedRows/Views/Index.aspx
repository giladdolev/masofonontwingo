﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectedRows" ID="windowView1" LoadAction="GridSelectedRows\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="SelectedRows" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets the collection of rows selected by the user.
            
            Syntax: public IList-GridRow- SelectedRows { get; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="180px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example presseng the 'Selected Rows Count' button will invoke the SelectedRows.count 
            property on this Grid.
            You can select a couple of row headers and press the button." Top="210px" ID="Label1"></vt:Label>

       <vt:Grid runat="server" CssClass="vt-testedgrid" Top="300px" ID="TestedGrid" SelectionChangedAction="GridSelectedRows\RowSelectionChanged_Click">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="430px" Width="320px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Selected Rows Count >>" Top="490px" Height="20" ID="btn_SelectedRowsCount" ClickAction="GridSelectedRows/SelectedRowsCount_click"></vt:Button>

    </vt:WindowView>
</asp:Content>
