using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonImageWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageWidth_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeImageWidth = this.GetVisualElementById<ButtonElement>("btnChangeImageWidth");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (btnChangeImageWidth.ImageWidth == 150) //Get
            {
                btnChangeImageWidth.ImageWidth = 300;//Set
                textBox1.Text = "ImageWidth value: " + btnChangeImageWidth.ImageWidth;
            }
            else 
            {
                btnChangeImageWidth.ImageWidth = 150;
                textBox1.Text = "ImageWidth value: " + btnChangeImageWidth.ImageWidth;

            }                    
        }
      
    }
}