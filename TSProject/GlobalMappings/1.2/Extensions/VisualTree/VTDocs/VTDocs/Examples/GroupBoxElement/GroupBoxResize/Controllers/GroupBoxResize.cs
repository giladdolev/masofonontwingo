using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxResizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            GroupBoxElement testedGroupBox = this.GetVisualElementById<GroupBoxElement>("testedGroupBox");


            if (testedGroupBox.Size == new Size(200, 70))
            {
                testedGroupBox.Size = new Size(300, 100);
                testedGroupBox.Text = "Set to Height: " + testedGroupBox.Height + ", Width: " + testedGroupBox.Width;
            }
            else
            {
                testedGroupBox.Size = new Size(200, 70);
                testedGroupBox.Text = "Back to Height: " + testedGroupBox.Height + ", Width: " + testedGroupBox.Width;
            }
        }

        private void testedGroupBox_Resize(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "GroupBox Resize event is invoked\\r\\n";
        }
      
    }
}