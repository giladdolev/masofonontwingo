using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxTabIndexTabStopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnChangeTabIndex_Click(object sender, EventArgs e)
        {
            TextBoxElement btnChangeTabIndex = this.GetVisualElementById<TextBoxElement>("btnChangeTabIndex");
            TextBoxElement TextBox1 = this.GetVisualElementById<TextBoxElement>("TextBox1");
            TextBoxElement TextBox2 = this.GetVisualElementById<TextBoxElement>("TextBox2");
            TextBoxElement TextBox4 = this.GetVisualElementById<TextBoxElement>("TextBox4");
            TextBoxElement TextBox5 = this.GetVisualElementById<TextBoxElement>("TextBox5");
            TextBoxElement logTextBox1 = this.GetVisualElementById<TextBoxElement>("logTextBox1");


            if (TextBox1.TabIndex == 1)
            {
                TextBox5.TabIndex = 1;
                TextBox4.TabIndex = 2;
                TextBox2.TabIndex = 4;
                TextBox1.TabIndex = 5;

                logTextBox1.Text = "\\r\\nTextBox1 TabIndex: " + TextBox1.TabIndex + "\\r\\nTextBox2 TabIndex: " + TextBox2.TabIndex + "\\r\\nTextBox4 TabIndex: " + TextBox4.TabIndex + "\\r\\nTextBox5 TabIndex: " + TextBox5.TabIndex;       

            }
            else
            { 
                TextBox1.TabIndex = 1;
                TextBox2.TabIndex = 2;
                TextBox4.TabIndex = 4;
                TextBox5.TabIndex = 5;

                logTextBox1.Text = "\\r\\nTextBox1 TabIndex: " + TextBox1.TabIndex + "\\r\\nTextBox2 TabIndex: " + TextBox2.TabIndex + "\\r\\nTextBox4 TabIndex: " + TextBox4.TabIndex + "\\r\\nTextBox5 TabIndex: " + TextBox5.TabIndex;
            }
        }
        private void btnChangeTabStop_Click(object sender, EventArgs e)
        {
            TextBoxElement btnChangeTabStop = this.GetVisualElementById<TextBoxElement>("btnChangeTabStop");
            TextBoxElement TextBox3 = this.GetVisualElementById<TextBoxElement>("TextBox3");
            TextBoxElement logTextBox2 = this.GetVisualElementById<TextBoxElement>("logTextBox2");


            if (TextBox3.TabStop == false)
            {
                TextBox3.TabStop = true;
                logTextBox2.Text = "TextBox3 TabStop: \\r\\n" + TextBox3.TabStop;

            }
            else
            {
                TextBox3.TabStop = false;
                logTextBox2.Text = "TextBox3 TabStop: \\r\\n" + TextBox3.TabStop;

            }
        }
    }
}