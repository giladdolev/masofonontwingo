<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

     <vt:WindowView runat="server" Text="SplitContainer Panel1Collapsed Property" ID="windowView1" LoadAction="SplitContainerPanel1Collapsed\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="250px" Text="Panel1Collapsed" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value determining whether Panel1 is collapsed or expanded.

            Syntax: public bool Panel1Collapsed { get; set; }
            'true' if Panel1 is collapsed; otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested SplitContainer1 -->
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="170px" ID="TestedSplitContainer1"></vt:SplitContainer>


        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Panel1Collapsed property of this SplitContainer is initially set to 'true'" Top="390px" ID="lblExp1"></vt:Label>

        <!-- Tested SplitContainer2 -->
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Panel1Collapsed="true" Top="450px" ID="TestedSplitContainer2"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="545px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Panel1Collapsed value >>" Top="620px" Width="200px" ID="btnChangeSplitContainerPanel1Collapsed" ClickAction="SplitContainerPanel1Collapsed\btnChangePanel1Collapsed_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
