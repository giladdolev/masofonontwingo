using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement btnTestedImage1 = this.GetVisualElementById<ImageElement>("btnTestedImage1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (btnTestedImage1.Image == null)
            {
                lblLog1.Text = "No image";
            }
            else
            {
                lblLog1.Text = "Set with image";
            }


            ImageElement btnTestedImage2 = this.GetVisualElementById<ImageElement>("btnTestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (btnTestedImage2.Image == null)
            {
                lblLog2.Text = "No image.";
            }
            else
            {
                lblLog2.Text = "Set with image.";
            }
        }



        public void btnChangeImage_Click(object sender, EventArgs e)
        {
            ImageElement btnTestedImage2 = this.GetVisualElementById<ImageElement>("btnTestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (btnTestedImage2.Image == null)
            {
                btnTestedImage2.Image = new UrlReference("/Content/Images/icon.jpg");
                lblLog2.Text = "Set with image.";
            }
            else
            {
                btnTestedImage2.Image = null;
                lblLog2.Text = "No image.";

            }
        }
    }
}