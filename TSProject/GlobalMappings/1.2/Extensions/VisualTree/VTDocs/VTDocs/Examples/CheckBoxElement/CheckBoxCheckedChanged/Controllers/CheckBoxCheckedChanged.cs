using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxCheckedChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");

            lblLog.Text = "CheckBox IsChecked value: " + TestedCheckBox.IsChecked;

        }

        public void TestedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nCheckBox CheckedChanged event is invoked";
            lblLog.Text = "CheckBox IsChecked value: " + TestedCheckBox.IsChecked;
        }

        public void btnChangeIsChecked_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedCheckBox.IsChecked == true)
            {
                TestedCheckBox.IsChecked = false;
                lblLog.Text = "CheckBox IsChecked value: " + TestedCheckBox.IsChecked;
            }
            else
            {
                TestedCheckBox.IsChecked = true;
                lblLog.Text = "CheckBox IsChecked value: " + TestedCheckBox.IsChecked;
            }

        }     

    }
}