using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowControlBoxController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowControlBox());
        }
        private WindowControlBox ViewModel
        {
            get { return this.GetRootVisualElement() as WindowControlBox; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "this.ControlBox value: " + this.ViewModel.ControlBox + '.';
        }


        public void btnChangeControlBox_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (this.ViewModel.ControlBox == true)
            {
                this.ViewModel.ControlBox = false;
                lblLog.Text = "this.ControlBox value: " + this.ViewModel.ControlBox + '.';
            }
            else
            {
                this.ViewModel.ControlBox = true;
                lblLog.Text = "this.ControlBox value: " + this.ViewModel.ControlBox + '.';

            }
        }
    }
}