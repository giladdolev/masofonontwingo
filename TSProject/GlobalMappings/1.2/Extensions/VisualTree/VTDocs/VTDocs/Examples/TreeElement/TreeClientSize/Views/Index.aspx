<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree ClientSize Property" ID="windowView" LoadAction="TreeClientSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientSize" ID="lblTitle"></vt:Label>
            
        <vt:Label runat="server" Text="Gets or sets the height and width of the client area of the control.

            Syntax: public Size ClientSize { get; set; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="lblDefinition" ></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="235px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The ClientSize property of this 'TestedTree' is initially set with 
            Width: 140px, Height: 110px" Top="275px"  ID="lblExp1"></vt:Label>     

        <!-- TestedTree -->
        <vt:Tree runat="server" Text="TestedTree" Top="345px" ID="TestedTree">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="TreeItem1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="TreeItem2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="TreeItem3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="470px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientSize value >>"  Top="535px" Width="180px" ID="btnChangeTreeClientSize" ClickAction="TreeClientSize\btnChangeTreeClientSize_Click"></vt:Button>
              
    </vt:WindowView>
</asp:Content>

