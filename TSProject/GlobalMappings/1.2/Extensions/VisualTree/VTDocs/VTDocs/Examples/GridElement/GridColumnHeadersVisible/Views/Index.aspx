<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="Grid ColumnHeadersVisible Property"  ID="windowView1" LoadAction="GridColumnHeadersVisible\OnLoad">


        <vt:Label runat="server" SkinID="Title" Text="ColumnHeadersVisible" Left="220px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the column header row is displayed.

            Syntax: public bool ColumnHeadersVisible { get; set; }
            'true' if the column headers are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Grid1 -->
        <vt:Grid runat="server" Top="170px" ID="TestedGrid1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>


        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ColumnHeadersVisible property of this Grid is initially set to 'false'
             The header row should be invisible" Top="400px" ID="lblExp1"></vt:Label>

        <!-- Tested Grid2 -->
        <vt:Grid runat="server" ColumnHeadersVisible="false" Top="455px" ID="TestedGrid2">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="590px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ColumnHeadersVisible value >>" Top="640px" Width="220px" ID="btnChangeGridColumnHeadersVisible" ClickAction="GridColumnHeadersVisible\btnChangeGridColumnHeadersVisible_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
        