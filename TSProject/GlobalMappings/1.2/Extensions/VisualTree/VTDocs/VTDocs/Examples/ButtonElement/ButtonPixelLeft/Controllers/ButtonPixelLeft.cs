using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button PixelLeft Value is: " + TestedButton.PixelLeft;

        }

        public void btnChangePixelLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.PixelLeft == 200)
            {
                TestedButton.PixelLeft = 80;
                lblLog.Text = "Button PixelLeft Value is: " + TestedButton.PixelLeft;

            }
            else
            {
                TestedButton.PixelLeft = 200;
                lblLog.Text = "Button PixelLeft Value is: " + TestedButton.PixelLeft;
            }
        }
    }
}