using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpVisible = this.GetVisualElementById<GroupBoxElement>("grpVisible");

            if (grpVisible.Visible == true) //Get
            {
                grpVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + grpVisible.Visible;
            }
            else
            {
                grpVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + grpVisible.Visible;
            }
        }

      //  grpText_TextChanged
        public void grpVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}