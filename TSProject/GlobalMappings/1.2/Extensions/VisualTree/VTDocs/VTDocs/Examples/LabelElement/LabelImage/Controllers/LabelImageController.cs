using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Change BackColor
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            
            lblLog1.Text = "Image default value: ResourceReference.EmptyArray"; 

            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "Image Is initially set to " + TestedLabel2.Image.Source.Substring(16);
        }


        public void btnChangeImage_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel2.Image.Source == "/Content/Images/open-book.png")
                TestedLabel2.Image = new ResourceReference("/Content/Images/icon.jpg");
            else
                TestedLabel2.Image = new ResourceReference("/Content/Images/open-book.png");

            lblLog2.Text = "Image was changed to " + TestedLabel2.Image.Source.Substring(16);
        }
    }
}