using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "RichTextBox MinimumSize value: " + TestedRichTextBox.MinimumSize + "\\r\\nRichTextBox  Size value: " + TestedRichTextBox.Size;

        }
        public void btnChangeRtfMinimumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.MinimumSize == new Size(260, 50))
            {
                TestedRichTextBox.MinimumSize = new Size(110, 100);
                lblLog.Text = "RichTextBox MinimumSize value: " + TestedRichTextBox.MinimumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;

            }
            else if (TestedRichTextBox.MinimumSize == new Size(110, 100))
            {
                TestedRichTextBox.MinimumSize = new Size(200, 70);
                lblLog.Text = "RichTextBox MinimumSize value: " + TestedRichTextBox.MinimumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;
            }

            else
            {
                TestedRichTextBox.MinimumSize = new Size(260, 50);
                lblLog.Text = "RichTextBox MinimumSize value: " + TestedRichTextBox.MinimumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;
            }

        }

        public void btnSetToZeroRtfMinimumSize_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedRichTextBox.MinimumSize = new Size(0, 0);
            lblLog.Text = "RichTextBox MinimumSize value: " + TestedRichTextBox.MinimumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;

        }

    }
}