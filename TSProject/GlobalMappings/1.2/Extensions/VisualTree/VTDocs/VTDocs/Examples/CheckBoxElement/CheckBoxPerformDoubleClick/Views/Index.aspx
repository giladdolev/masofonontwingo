<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PerformDoubleClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformDoubleClick of 'Tested CheckBox'" Top="45px" Left="140px" ID="btnPerformDoubleClick" Height="36px" Width="220px" ClickAction="CheckBoxPerformDoubleClick\btnPerformDoubleClick_Click"></vt:Button>


        <vt:CheckBox runat="server" Text="Tested CheckBox" Top="150px" Left="200px" ID="testedCheckBox" Height="36px"  Width="100px" DoubleClickAction="CheckBoxPerformDoubleClick\testedCheckBox_DoubleClick"></vt:CheckBox>           

        

    </vt:WindowView>
</asp:Content>
