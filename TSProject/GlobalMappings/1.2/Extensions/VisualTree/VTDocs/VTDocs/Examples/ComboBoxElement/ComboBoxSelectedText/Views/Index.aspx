<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox SelectedText property" ID="windowView1" LoadAction="ComboBoxSelectedText/OnLoad">
        <vt:Label runat="server" SkinID="Title" Text="SelectedText" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text that is selected in the editable portion of a ComboBox.
            
            public string SelectedText { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:ComboBox runat="server" ID="TestedComboBox1" Text="ComboBox" Top="145px"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="185px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In ComboGrid controls by default all text is selected. Hover with the mouse on buttons
            below to try the Get and Set of the SelectedText property of this ComboGrid." Top="295px"  ID="lblExp1"></vt:Label>     
        
        <vt:ComboBox runat="server" ID="TestedComboBox2" Text="TestedComboBox" Top="360px"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="400px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Set SelectedText value >>" Width="180px" Top="465px" ID="btnSetSelectedText" ClickAction="ComboBoxSelectedText\btnSetSelectedText_Click" MouseHoverAction="ComboBoxSelectedText\btnSetSelectedText_Click"></vt:Button>

          <vt:Button runat="server" Text="Get SelectedText value >>" Left="300px" Width="180px" Top="465px" ID="btnGetSelectedText" ClickAction="ComboBoxSelectedText\btnGetSelectedText_Click" MouseHoverAction="ComboBoxSelectedText\btnGetSelectedText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
