using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarVisibleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Visible value: " + TestedToolBar1.Visible;
            lblLog2.Text = "Visible value: " + TestedToolBar2.Visible + '.';

        }

        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedToolBar2.Visible = !TestedToolBar2.Visible;
            lblLog2.Text = "Visible value: " + TestedToolBar2.Visible + '.';
        }

    }
}