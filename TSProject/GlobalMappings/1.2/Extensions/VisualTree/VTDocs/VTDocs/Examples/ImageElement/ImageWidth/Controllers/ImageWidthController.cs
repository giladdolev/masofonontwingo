using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Width value: " + TestedImage.Width + '.';
        }

        private void btnChangeWidth_Click(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedImage.Width == 150)
            {
                TestedImage.Width = 200;
                lblLog.Text = "Width value: " + TestedImage.Width + '.';
            }
            else
            {
                TestedImage.Width = 150;
                lblLog.Text = "Width value: " + TestedImage.Width + '.';
            }
        }
      
    }
}