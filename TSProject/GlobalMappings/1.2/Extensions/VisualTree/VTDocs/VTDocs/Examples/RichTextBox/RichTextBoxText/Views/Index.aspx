
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Text Property" ID="windowView2" LoadAction="RichTextBoxText/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with this control.
            
            Syntax: public override string Text { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Text property of the RichTextBox is initially set to 'TestedRichTextBox'" Top="235px"  ID="lblExp1"></vt:Label>     
      
        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="285px" ID="TestedRichTextBox" TextChangedAction="RichTextBoxText\TestedRichTextBox_TextChanged"></vt:RichTextBox>
        <vt:Label runat="server" SkinID="Log" Top="380px" Width="550" Height="100" ID="lblLog"></vt:Label>
         <vt:Button runat="server" Text="Add Text >>"  Top="520px" Width="150px" ID="btnAddText" ClickAction="RichTextBoxText\btnAddText_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Text Value >>"  Top="520px" Width="150px" Left= "280px"  ID="btnChangeRichTextBoxSize" ClickAction="RichTextBoxText\btnChangeRichTextBoxText_Click"></vt:Button>
         <vt:Button runat="server" Text="Change Text Value Slash N>>"  Top="570px" Width="200px" Left= "255px"  ID="btnChangeRichTextBoxSizeSlashN" ClickAction="RichTextBoxText\btnChangeRichTextBoxText2_Click"></vt:Button>
        <vt:Button runat="server" Text="Clear Text >>"  Top="520px" Width="150px" Left= "480px" ID="btnClearText" ClickAction="RichTextBoxText\btnClearText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>