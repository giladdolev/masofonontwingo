<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel ForeColor property" ID="windowView1" LoadAction="PanelForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Panel1 -->
        <vt:Panel runat="server" ShowHeader="true" Text="Panel" Top="165px" Height="110px" Width="430px" ID="TestedPanel1">
            <vt:Label runat="server" Text="This Panel contains a few controls." Top="5px" Left="20px" ID="Label1"></vt:Label>
            <vt:Button runat="server" Text="Button" Top="35px" Left="20px" Height="23px" Width="100px" ID="Button1" ></vt:Button>
            <vt:CheckBox runat="server" Text="CheckBox" Top="35px" Left="160px" Height="20px" Width="100px" ID="CheckBox1"></vt:CheckBox>
        
            <vt:TextBox runat="server" Text="TextBox" Top="35px" Left="300px" Height="20px" Width="100px" ID="textBox1" > </vt:TextBox>
        </vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="285px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of this TestedPanel is initially set to Green.
             " Top="385px" ID="lblExp1"></vt:Label>

        <!-- Tested Panel2 -->
        <vt:Panel runat="server" ShowHeader="true" Text="TestedPanel" Top="420px" Height="110px" Width="430px" ForeColor="Green" ID="TestedPanel2">
            <vt:Label runat="server" Text="This Panel contains a few controls." Top="5px" Left="20px" ID="Label2"></vt:Label>
            <vt:Button runat="server" Text="Button" Top="35px" Left="20px" Height="23px" Width="100px" ID="Button2" ></vt:Button>
            <vt:CheckBox runat="server" Text="CheckBox" Top="35px" Left="160px" Height="20px" Width="100px" ID="CheckBox2"></vt:CheckBox>
        
            <vt:TextBox runat="server" Text="TextBox" Top="35px" Left="300px" Height="20px" Width="100px" ID="textBox2" > </vt:TextBox>
        </vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="540px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="590px" Width="180px" ID="btnChangePanelForeColor" ClickAction="PanelForeColor\btnChangePanelForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
