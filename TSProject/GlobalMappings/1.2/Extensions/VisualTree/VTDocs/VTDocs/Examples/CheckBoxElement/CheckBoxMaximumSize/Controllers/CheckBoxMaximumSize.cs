using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "CheckBox MaximumSize value: " + TestedCheckBox.MaximumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
        }

        public void btnChangeChkMaximumSize_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedCheckBox.MaximumSize == new Size(120, 60))
            {
                TestedCheckBox.MaximumSize = new Size(170, 40);
                lblLog1.Text = "CheckBox MaximumSize value: " + TestedCheckBox.MaximumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
            }
            else if (TestedCheckBox.MaximumSize == new Size(170, 40))
            {
                TestedCheckBox.MaximumSize = new Size(190, 80);
                lblLog1.Text = "CheckBox MaximumSize value: " + TestedCheckBox.MaximumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
            }
            else
            {
                TestedCheckBox.MaximumSize = new Size(120, 60);
                lblLog1.Text = "CheckBox MaximumSize value: " + TestedCheckBox.MaximumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
            }

        }

        public void btnSetToZeroChkMaximumSize_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedCheckBox.MaximumSize = new Size(0, 0);
            lblLog1.Text = "CheckBox MaximumSize value: " + TestedCheckBox.MaximumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;

        }


    }
}