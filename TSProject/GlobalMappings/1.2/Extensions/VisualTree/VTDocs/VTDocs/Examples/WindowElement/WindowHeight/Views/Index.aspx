<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window Height Property" Left="5px" ID="windowView1" LoadAction="WindowHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Height" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int Height { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Height property of this window is initially set to 705px" Top="295px"  ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Height value >>" Top="410px" Width="160px" ID="btnChangeHeight" ClickAction="WindowHeight\btnChangeHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>