using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonBringToFrontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnBringButton1ToFront_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ButtonElement button1 = this.GetVisualElementById<ButtonElement>("button1");

            button1.BringToFront();
            textBox1.Text = "button1 is on front";
        }

        public void btnBringButton2ToFront_Click(object sender, EventArgs e)
        {

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ButtonElement button2 = this.GetVisualElementById<ButtonElement>("button2");

            button2.BringToFront();
            textBox1.Text = "button2 is on front";
        }

    }
}