using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridCellElementSelectedIndexController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GridAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.AddToComboBoxList("OptionsColumn1", "Option1,Option2,Option3, Option4");
            TestedGrid.AddToComboBoxList("GenderColumn1", "male,female");


            TestedGrid.Rows.Add("Option4", "46203", "male", "Tomer", false);
            TestedGrid.Rows.Add("Option4", "19203", "male", "Yoav", true);
            TestedGrid.Rows.Add("Option2", "94203", "male", "Shalom", false);
            TestedGrid.Rows.Add();


            lblLog.Text = "Cell (0,0) SelctedIndex value: " + TestedGrid.Rows[0].Cells[0].SelectedIndex + "\\r\\nCell (0,2) SelctedIndex value: " + TestedGrid.Rows[0].Cells[2].SelectedIndex + "\\r\\nCell (3,0) SelctedIndex value: " + TestedGrid.Rows[3].Cells[0].SelectedIndex;
           
        }


       //Change this event to GridComboBoxColumn SelectionChanged event 
        public void TestedGrid_CellEndEdit(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Cell (0,0) SelctedIndex value: " + TestedGrid.Rows[0].Cells[0].SelectedIndex + "\\r\\nCell (0,2) SelctedIndex value: " + TestedGrid.Rows[0].Cells[2].SelectedIndex + "\\r\\nCell (3,0) SelctedIndex value: " + TestedGrid.Rows[3].Cells[0].SelectedIndex;
        }

        public void btnChangeSelectedIndexOfCell0_0_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGrid.Rows[0].Cells[0].SelectedIndex == 0)
                TestedGrid.Rows[0].Cells[0].SelectedIndex = 1;
            else if (TestedGrid.Rows[0].Cells[0].SelectedIndex == 1)
                TestedGrid.Rows[0].Cells[0].SelectedIndex = 2;
            else
                TestedGrid.Rows[0].Cells[0].SelectedIndex = 0;



            lblLog.Text = "Cell (0,0) SelctedIndex value: " + TestedGrid.Rows[0].Cells[0].SelectedIndex + "\\r\\nCell (0,2) SelctedIndex value: " + TestedGrid.Rows[0].Cells[2].SelectedIndex + "\\r\\nCell (3,0) SelctedIndex value: " + TestedGrid.Rows[3].Cells[0].SelectedIndex;
        }


        public void btnChangeSelectedIndexOfCell0_2_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGrid.Rows[0].Cells[2].SelectedIndex == 0)
                TestedGrid.Rows[0].Cells[2].SelectedIndex = 1;
            else if (TestedGrid.Rows[0].Cells[2].SelectedIndex == 1)
                TestedGrid.Rows[0].Cells[2].SelectedIndex = 2;
            else
                TestedGrid.Rows[0].Cells[2].SelectedIndex = 0;



            lblLog.Text = "Cell (0,0) SelctedIndex value: " + TestedGrid.Rows[0].Cells[0].SelectedIndex + "\\r\\nCell (0,2) SelctedIndex value: " + TestedGrid.Rows[0].Cells[2].SelectedIndex + "\\r\\nCell (3,0) SelctedIndex value: " + TestedGrid.Rows[3].Cells[0].SelectedIndex;
        }

        public void btnChangeSelectedIndexOfCell3_0_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGrid.Rows[3].Cells[0].SelectedIndex == 0)
                TestedGrid.Rows[3].Cells[0].SelectedIndex = 1;
            else if (TestedGrid.Rows[3].Cells[0].SelectedIndex == 1)
                TestedGrid.Rows[3].Cells[0].SelectedIndex = 2;
            else
                TestedGrid.Rows[3].Cells[0].SelectedIndex = 0;



            lblLog.Text = "Cell (0,0) SelctedIndex value: " + TestedGrid.Rows[0].Cells[0].SelectedIndex + "\\r\\nCell (0,2) SelctedIndex value: " + TestedGrid.Rows[0].Cells[2].SelectedIndex + "\\r\\nCell (3,0) SelctedIndex value: " + TestedGrid.Rows[3].Cells[0].SelectedIndex;
        }

       

    }
}