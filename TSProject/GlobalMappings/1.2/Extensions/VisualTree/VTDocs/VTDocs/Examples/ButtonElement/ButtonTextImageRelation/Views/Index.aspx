
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button TextImageRelation Property" ID="windowView" LoadAction="ButtonTextImageRelation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="TextImageRelation" Left="260px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the position of text and image relative to each other.
            The default value is Overly (the image and text share the same space on the control).

            Syntax: public TextImageRelation TextImageRelation { get; set; }

            Values: ImageAboveText/ ImageBeforeText/ Overlay/ TextAboveImage/ TextBeforeImage" Top="75px"  ID="lblDefinition1"></vt:Label>
      
        
        <vt:Button runat="server" SkinID="Wide" Height="70" Text="Button" Top="210px" Image="~/Content/Elements/Button.png" ID="btnTestedButton1"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>
 
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, the initial TextImageRelation of TestedButton is set to TextBeforeImage.
            Use the buttons below to change the TestedButton Text-Image Relationt." Top="380px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Image="~/Content/Elements/Button.png" TextImageRelation ="TextBeforeImage" Height="70" Top="460px" ID="btnTestedButton2"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="540px" ID="lblLog2"></vt:Label>

         <vt:Button runat="server" Text="ImageAboveText >"  Top="600px" Left="80px" ID="btnImageAboveText" Height="23px" Width="112px" ClickAction="ButtonTextImageRelation\btnImageAboveText_Click"></vt:Button>  
         <vt:Button runat="server" Text="ImageBeforeText >"   Top="600px" Left="200px" ID="btnImageBeforeText" Height="23px" Width="112px" ClickAction="ButtonTextImageRelation\btnImageBeforeText_Click"></vt:Button>  
         <vt:Button runat="server" Text="Overlay >"   Top="600px" Left="320px" ID="btnOverlay" Height="23px" Width="112px" ClickAction="ButtonTextImageRelation\btnOverlay_Click"></vt:Button>  
         <vt:Button runat="server" Text="TextAboveImage >"  Top="600px" Left="440px" ID="btnTextAboveImage" Height="23px" Width="112px" ClickAction="ButtonTextImageRelation\btnTextAboveImage_Click"></vt:Button>  
         <vt:Button runat="server" Text="TextBeforeImage >" Top="600px" Left="560px" ID="btnNone" Height="23px" Width="112px" ClickAction="ButtonTextImageRelation\btnTextBeforeImage_Click"></vt:Button> 
           
    </vt:WindowView>
</asp:Content>
