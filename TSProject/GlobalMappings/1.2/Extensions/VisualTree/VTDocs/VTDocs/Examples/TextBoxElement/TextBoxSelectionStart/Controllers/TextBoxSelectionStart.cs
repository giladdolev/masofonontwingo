using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxSelectionStartController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnSetSelectionStart_Click(object sender, EventArgs e)
        {
            TextBoxElement txtSelectionStart = this.GetVisualElementById<TextBoxElement>("txtSelectionStart");

            txtSelectionStart.SelectionStart = 5;
        }
        public void btnGetSelectionStart_Click(object sender, EventArgs e)
        {
            TextBoxElement txtSelectionStart = this.GetVisualElementById<TextBoxElement>("txtSelectionStart");
            
            MessageBox.Show(txtSelectionStart.SelectionStart.ToString());
        }


    }
}