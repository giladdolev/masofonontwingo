<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox TextAlign" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
        <vt:RadioButton runat="server" Text="TextAlign - BottomCenter" Top="44px" Left="20px" TextAlign="BottomCenter" ID="rdoTextAlign" Height="20px" Width="180px"></vt:RadioButton>

        <vt:Button runat="server" Text="Get TextAlign" Top="44px" Left="300px" ID="btnGetTextAlign" Height="36px"  Width="200px" ClickAction="RadioButtonTextAlign\btnGetTextAlign_Click"></vt:Button>
        <vt:Button runat="server" Text="Set TextAlign" Top="100px" Left="300px" ID="btnSetTextAlign" Height="36px"  Width="200px" ClickAction="RadioButtonTextAlign\btnSetTextAlign_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
