<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox IsChecked Property" ID="windowView2" LoadAction="CheckBoxIsChecked\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="IsChecked" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or set a value indicating whether the CheckBox is in the checked state.
            
            Syntax: public bool IsChecked { get; set; }
            The default value is false." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:CheckBox runat="server" Text="CheckBox" Top="170px" ID="TestedCheckBox1"></vt:CheckBox>
        <vt:Label runat="server" SkinID="Log" Top="210px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="275px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="IsChecked property of 'TestedCheckBox' and 'TestedCheckBox2' is initially set to True.
            IsChecked property of 'TestedCheckBox1' is initially set to False." Top="325px"  ID="lblExp1"></vt:Label>     
        
        <vt:CheckBox runat="server" Text="TestedCheckBox" IsChecked ="True" Top="395px" ID="TestedCheckBox2" CheckStateChangedAction="CheckBoxIsChecked\TestedCheckBoxes_CheckStateChanged"></vt:CheckBox>
        <vt:CheckBox runat="server" Text="TestedCheckBox1" IsChecked ="False" Top="395px" Left="280px" ID="TestedCheckBox3" CheckStateChangedAction="CheckBoxIsChecked\TestedCheckBoxes_CheckStateChanged"></vt:CheckBox>
        <vt:CheckBox runat="server" Text="TestedCheckBox2" IsChecked ="True" Top="395px" Left="480px" ID="TestedCheckBox4" CheckStateChangedAction="CheckBoxIsChecked\TestedCheckBoxes_CheckStateChanged"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Top="435px" Height="50px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Toggle TestedCheckBox >>" Top="550px" ID="btnToggleTestedCheckBox" Width="185px" ClickAction="CheckBoxIsChecked\btnToggleTestedCheckBox_Click"></vt:Button>
        <vt:Button runat="server" Text="Toggle TestedCheckBox1 >>" Top="550px" Left="280px" ID="btnToggleTestedCheckBox1" Width="185px" ClickAction="CheckBoxIsChecked\btnToggleTestedCheckBox1_Click"></vt:Button>
        <vt:Button runat="server" Text="Toggle TestedCheckBox2 >>" Top="550px" Left="480px" ID="btnToggleTestedCheckBox2" Width="185px" ClickAction="CheckBoxIsChecked\btnToggleTestedCheckBox2_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
