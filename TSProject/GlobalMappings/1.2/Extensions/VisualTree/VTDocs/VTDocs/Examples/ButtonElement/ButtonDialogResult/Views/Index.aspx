<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button DialogResult Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="700px">
		
		<vt:Button runat="server" TextAlign="MiddleCenter" Text="Show Dialog" Top="110px" Left="200px" ClickAction="ButtonDialogResult\btnShowDialog_Click" ID="btnShowDialog" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="200px">
		</vt:Button>

		
		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="230px" Left="200px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="200px">
		</vt:TextBox>

        </vt:WindowView>
</asp:Content>