using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonLocationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
 
        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button Location is: " + btnTestedButton.Location;

        }
        public void btnChangeButtonLocation_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            if (btnTestedButton.Location == new Point(200, 300))
            {
                btnTestedButton.Location = new Point(80, 325);
                lblLog.Text = "Button Location is: " + btnTestedButton.Location;

            }
            else
            {
                btnTestedButton.Location = new Point(200, 300);
                lblLog.Text = "Button Location is: " + btnTestedButton.Location;
            }

        }

    }
}