using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPaddingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangePadding_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangePadding = this.GetVisualElementById<ButtonElement>("btnChangePadding");
           
            if (btnChangePadding.Padding == new Padding(0, 0, 0, 0)) //Get
            {
                btnChangePadding.Padding = new Padding(0, 0, 0, 30); //Set
            }
            else if (btnChangePadding.Padding == new Padding(0, 0, 0, 30))
            {
                btnChangePadding.Padding = new Padding(0, 0, 30, 0); //Set
            }
            else if (btnChangePadding.Padding == new Padding(0, 0, 30, 0)) //Get
            {
                btnChangePadding.Padding = new Padding(0, 30, 0, 0); //Set
            }
            else if (btnChangePadding.Padding == new Padding(0, 30, 0, 0)) //Get
            {
                btnChangePadding.Padding = new Padding(30, 0, 0, 0); //Set
            }
            else if (btnChangePadding.Padding == new Padding(30, 0, 0, 0)) //Get
            {
                btnChangePadding.Padding = new Padding(0, 0, 0, 0); //Set
            }
          
        }

    }
}