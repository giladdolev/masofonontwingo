<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Left Property" ID="windowView2" LoadAction="ButtonLeft\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Left" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the left edge 
            of the control and the left edge of its container's client area.
            
            Syntax: public int Left { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Left property of this 'TestedButton' is initially set to 80." Top="300px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Top="350px" ID="btnTestedButton"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Left Value >>" Top="475px" ID="btnChangeLeft" ClickAction="ButtonLeft\btnChangeLeft_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>