using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarMinController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar1 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar1");
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog1.Text = "ProgressBar Min value is " + TestedProgressBar1.Min.ToString() + " ; " + "Value property is " + TestedProgressBar1.Value.ToString();
            lblLog.Text = "ProgressBar Min value = " + TestedProgressBar.Min.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
        }

        public void btnChangeMin_Click(object sender, EventArgs e)
        {

            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.Min == 0)
            {
                TestedProgressBar.Min = 20;
                lblLog.Text = "ProgressBar Min value = " + TestedProgressBar.Min.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }
            else
            {
                TestedProgressBar.Min = 0;
                lblLog.Text = "ProgressBar Min value = " + TestedProgressBar.Min.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }

        }

        public void btnChangeValue_Click(object sender, EventArgs e)
        {

            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.Value == 60)
            {
                TestedProgressBar.Value = 20;
                lblLog.Text = "ProgressBar Min value = " + TestedProgressBar.Min.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }
            else
            {
                TestedProgressBar.Value = 60;
                lblLog.Text = "ProgressBar Min value = " + TestedProgressBar.Min.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }
        }

        
    }
}