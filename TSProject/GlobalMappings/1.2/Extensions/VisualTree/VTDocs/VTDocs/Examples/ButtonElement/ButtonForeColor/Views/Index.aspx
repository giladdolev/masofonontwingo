
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button ForeColor Property" ID="windowView2" LoadAction="ButtonForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.
            
            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="170px" ID="btnTestedButton1"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="ForeColor property of this 'TestedButton' is initially set to Gray" Top="330px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" ForeColor ="Gray" Top="400px" ID="btnTestedButton2"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change ForeColor >>" Top="505px" ID="btnChangeForeColor" ClickAction="ButtonForeColor\btnChangeForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
