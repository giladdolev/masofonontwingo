using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
       
        public void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement pnlRuntimeRightToLeft = this.GetVisualElementById<PanelElement>("pnlRuntimeRightToLeft");

            if (pnlRuntimeRightToLeft.RightToLeft == RightToLeft.Inherit) //Get
            {
                pnlRuntimeRightToLeft.RightToLeft = RightToLeft.No; //Set
                textBox1.Text = "The RightToLeft property value is: " + pnlRuntimeRightToLeft.RightToLeft;
            }
            else if (pnlRuntimeRightToLeft.RightToLeft == RightToLeft.No)
            {
                pnlRuntimeRightToLeft.RightToLeft = RightToLeft.Yes; 
                textBox1.Text = "The RightToLeft property value is: " + pnlRuntimeRightToLeft.RightToLeft;
            }
            else  
            {
                pnlRuntimeRightToLeft.RightToLeft = RightToLeft.Inherit; 
                textBox1.Text = "The RightToLeft property value is: " + pnlRuntimeRightToLeft.RightToLeft;
            }
             
        }

    }
}