﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Controllers
{
    public class BaiscDataView : WindowElement
    {
        public List<Employee> employeesList = new List<Employee>();
        public BaiscDataView()
        {
            List<Employee> employeesList = new List<Employee>();
        }

        public List<Employee> GetEmployeesList()
        {
            employeesList.Add(new Employee() { ID = 1, FirstName = "james", LastName = "ashton", Options = "1"});
            employeesList.Add(new Employee() { ID = 2, FirstName = "david", LastName = "ashton", Options = "2" });
            employeesList.Add(new Employee() { ID = 3, FirstName = "haim", LastName = "ashton", Options = "3" });

            return employeesList;
        }

    }
}