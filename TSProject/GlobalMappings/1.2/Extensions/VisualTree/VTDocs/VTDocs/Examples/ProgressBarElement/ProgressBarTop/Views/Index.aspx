<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Top Property" Top="57px" Left="11px" ID="windowView1" LoadAction="ProgressBarTop\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Top" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the top edge of the control
             and the top edge of its container's client area.
            
            Syntax: public int Top { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Top property of this ProgressBar is initially set to: 310px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="310px" ID="TestedProgressBar" TabIndex="3"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Top="365px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Top value >>" Top="430px" ID="btnChangeTop" TabIndex="1" Width="180px" ClickAction="ProgressBarTop\btnChangeTop_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
