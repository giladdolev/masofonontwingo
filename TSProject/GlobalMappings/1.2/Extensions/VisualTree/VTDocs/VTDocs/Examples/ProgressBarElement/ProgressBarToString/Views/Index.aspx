 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar ToString Method" ID="windowView2" LoadAction="ProgressBarToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a string that represents the ProgressBar control.

            Syntax: public override string ToString()
            
            Return Value: The return string includes the type and the values for the Minimum, Maximum, 
            and Value properties." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this ProgressBar." 
            Top="280px" ID="lblExp2"></vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="345px" ID="TestedProgressBar"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="400px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="505px" ID="btnToString" ClickAction="ProgressBarToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>