using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPerformMouseMoveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseMove_Click(object sender, EventArgs e)
        {
            
            RadioButtonElement testedRadioButton = this.GetVisualElementById<RadioButtonElement>("testedRadioButton");

            MouseEventArgs args = new MouseEventArgs();

            testedRadioButton.PerformMouseMove(args);
        }

        public void testedRadioButton_MouseMove(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRadioButton MouseMove event method is invoked");
        }

    }
}