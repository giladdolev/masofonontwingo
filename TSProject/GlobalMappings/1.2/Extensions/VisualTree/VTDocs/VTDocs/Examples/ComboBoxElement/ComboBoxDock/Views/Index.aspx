<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Enabled property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxDock/Form_Load">
        <vt:Label runat="server" Text="Initialized Component : ComboBox Dock is set to Bottom" Top="300px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="comboBox1" DisplayMember="text" ValueMember="value" Left="50" Top="360" Dock="Bottom">
            <Items>
                <vt:ListItem runat="server" Text="aa"></vt:ListItem>
                <vt:ListItem runat="server" Text="bb"></vt:ListItem>
                <vt:ListItem runat="server" Text="cc"></vt:ListItem>
                <vt:ListItem runat="server" Text="55"></vt:ListItem>
            </Items>
        </vt:ComboBox>
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="160" Left="50"/>
        <vt:Button runat="server" Text="Change comboBox Docking" Top="110px" Left="140px" ID="btnDock" Height="26px" Width="300px" ClickAction="ComboBoxDock\btnDock_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
