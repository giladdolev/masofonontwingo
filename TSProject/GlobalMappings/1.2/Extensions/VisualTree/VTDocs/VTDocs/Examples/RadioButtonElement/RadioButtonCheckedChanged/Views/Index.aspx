<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton CheckedChanged event" ID="windowView1" LoadAction="RadioButtonCheckedChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CheckedChanged" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the value of the Checked property changes.

            Syntax: public event EventHandler CheckedChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the Checked property by
             clicking the 'Toggle Choise' button or by manually checking the radioButton1 or radioButton2.
            Each invocation of the 'CheckedChanged' event should add 2 lines in the 'Event Log' -
           The order is: first line for the unchecked radiobutton and second line for the checked radio button."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:RadioButton runat="server" Text="RadioButton1" ID="radioButton1" Top="325px" CheckedChangedAction="RadioButtonCheckedChanged\rd1_checkedChanged"></vt:RadioButton>
        <vt:RadioButton runat="server" Text="RadioButton2" ID="radioButton2" Top="355px" CheckedChangedAction="RadioButtonCheckedChanged\rd2_checkedChanged"></vt:RadioButton>
                  
        <vt:Label runat="server" SkinID="Log" Top="395px" Width="340px" Height="40px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Top="450px" Height="120px" ID="txtEventLog" Text="Event Log:"></vt:TextBox>

        <vt:Button runat="server" Text="Toggle Choice >>" Top="610px" Width="180px" ID="btnChangeCheckState" ClickAction="RadioButtonCheckedChanged\btnChangeChecked_Click"></vt:Button>

        <vt:Button runat="server" Text="Reset Choice >>" Top="610px" Width="180px" Left="270px" ID="btnResetCheckState" ClickAction="RadioButtonCheckedChanged\btnResetChecked_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
