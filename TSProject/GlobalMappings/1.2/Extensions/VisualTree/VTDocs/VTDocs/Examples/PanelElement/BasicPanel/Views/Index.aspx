<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Panel" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" >
        <vt:Panel runat="server" ShowHeader="False" Border="true" AutoScroll="True" Top="0px" Left="0px" ID="panel1" BackColor="Lime" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="262px" Width="284px"></vt:Panel>
		<vt:Button runat="server" TextAlign="MiddleCenter" Text="Change Background" Top="20px" Left="400px" ID="ChangeColors" Height="49px" TabIndex="1" Width="169px" ClickAction="BasicPanel\ChangeColors_Click"></vt:Button>
		<vt:Button runat="server" TextAlign="MiddleCenter" Text="Add BoxShadow" Top="80px" Left="400px" ID="AddBoxShadow" Height="49px" TabIndex="1" Width="169px" ClickAction="BasicPanel\AddBoxShow_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
