using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridMaxRowsController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}


        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement rbMax4 = this.GetVisualElementById<RadioButtonElement>("rbMax4");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("a", "b", "c");

            rbMax4.IsChecked = true;

            lblLog1.Text = "MaxRows value is: " + TestedGrid1.MaxRows + ", Rows count value is: " + TestedGrid1.Rows.Count;
        }

        private void Add_New_Row(object sender, EventArgs e)
		{
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");

            try
            {
                TestedGrid1.Rows.Add("a", "b", "c");
                TestedGrid1.Refresh();
                TestedGrid1.RefreshData();
                lblLog1.Text = "MaxRows value is: " + TestedGrid1.MaxRows + ", Rows count value is: " + TestedGrid1.Rows.Count;
            } 
            catch (ArgumentException exc)
            {
                lblLog1.Text = "MaxRows value is: " + TestedGrid1.MaxRows + ", Rows count value is: " + TestedGrid1.Rows.Count +
                    "\\r\\nSystem.ArgumentException was thrown:\\r\\n" + exc.Message;
            }
		}

        private void Delete_Row(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");

            if (TestedGrid1.Rows.Count > 0)
            {
                TestedGrid1.Rows.RemoveAt(TestedGrid1.Rows.Count - 1);
                TestedGrid1.Refresh();
                TestedGrid1.RefreshData();
                lblLog1.Text = "MaxRows value is: " + TestedGrid1.MaxRows + ", Rows count value is: " + TestedGrid1.Rows.Count;
            }

        }

        private void rbMax4_CheckedChanged(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");

            TestedGrid1.MaxRows = 4;
            lblLog1.Text = "MaxRows value is: " + TestedGrid1.MaxRows + ", Rows count value is: " + TestedGrid1.Rows.Count;
        }

        private void rbMax2_CheckedChanged(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");

            try
            {
                TestedGrid1.MaxRows = 2;
                lblLog1.Text = "MaxRows value is: " + TestedGrid1.MaxRows + ", Rows count value is: " + TestedGrid1.Rows.Count;

            }
            catch (ArgumentException exc)
            {
                RadioButtonElement rbMax4 = this.GetVisualElementById<RadioButtonElement>("rbMax4");
                lblLog1.Text = "MaxRows value is: " + TestedGrid1.MaxRows + ", Rows count value is: " + TestedGrid1.Rows.Count +
                    "\\r\\nSystem.ArgumentException was thrown:\\r\\n" + exc.Message;
                rbMax4.IsChecked = true;
            }
        }
	}
}
