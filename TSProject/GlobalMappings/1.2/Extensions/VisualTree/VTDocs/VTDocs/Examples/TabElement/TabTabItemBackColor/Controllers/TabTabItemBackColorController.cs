using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            TabItem Tab1Item1 = TestedTab1.TabItems[0];
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + Tab1Item1.BackColor.ToString();

            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + Tab1Item2.BackColor.ToString();

        }


        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (Tab1Item2.BackColor == Color.Orange)
            {
                Tab1Item2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + Tab1Item2.BackColor.ToString();
            }
            else
            {
                Tab1Item2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + Tab1Item2.BackColor.ToString();

            }
        }

    }
}