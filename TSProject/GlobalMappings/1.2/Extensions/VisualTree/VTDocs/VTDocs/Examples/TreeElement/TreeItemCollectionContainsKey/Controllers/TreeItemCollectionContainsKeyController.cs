using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeItemCollectionContainsKeyController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            ComboBoxElement cmbTreeItems = this.GetVisualElementById<ComboBoxElement>("cmbTreeItems");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            cmbTreeItems.Items.Add("TreeItem");

            for (int i = 0; i < TestedTree.Items.Count; i++)
            {
                cmbTreeItems.Items.Add(TestedTree.Items[i].Text);
                for (int j = 0; j < TestedTree.Items[i].Items.Count; j++)
                {
                    cmbTreeItems.Items.Add(TestedTree.Items[i].Items[j].Text);
                    for (int k = 0; k < TestedTree.Items[i].Items[j].Items.Count; k++)
                    {
                        cmbTreeItems.Items.Add(TestedTree.Items[i].Items[j].Items[k].Text);
                    }
                }
            }

            lblLog1.Text = "Choose key and press button...";
        }

        private bool searchKey(string key)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");


            if (TestedTree.Items.ContainsKey(key))
            {
                return true;
            }

            for (int i = 0; i < TestedTree.Items.Count; i++)
            {
                if (TestedTree.Items[i].Items.ContainsKey(key))
                {
                    return true;
                }

                for (int j = 0; j < TestedTree.Items[i].Items.Count; j++)
                {
                    if (TestedTree.Items[i].Items[j].Items.ContainsKey(key))
                    {
                        return true;
                    } 

                    for (int k = 0; k < TestedTree.Items[i].Items[j].Items.Count; k++)
                    {
                        if (TestedTree.Items[i].Items[j].Items[k].Items.ContainsKey(key))
                        {
                            return true;
                        } 
                    }
                }
            }

            return false;
        }

        public void btnPerformContainsKey_Click(object sender, EventArgs e)
        {
            ComboBoxElement cmbTreeItems = this.GetVisualElementById<ComboBoxElement>("cmbTreeItems");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (cmbTreeItems.SelectedValue != null)
            {
                lblLog1.Text = "ContainsKey return value : " + searchKey(cmbTreeItems.Text) + '.';
            }
        }
    }
}