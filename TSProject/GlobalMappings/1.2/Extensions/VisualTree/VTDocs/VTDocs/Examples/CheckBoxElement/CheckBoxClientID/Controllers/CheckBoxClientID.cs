using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxClientIDController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //public void btnClientID_Click(object sender, EventArgs e)
        //{
        //    CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");
        //    TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

        //    textBox1.Text = "ClientID value:\\r\\n" + testedCheckBox.ClientID;
        //}
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press the 'Get ClientID' Button...";
        }

        public void GetClientID_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            
            lblLog.Text = "The element ClientID is: \\r\\n" + TestedCheckBox.ClientID;
        }

    }
}