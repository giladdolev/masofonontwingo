<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic SplitContainer" ID="windowView1">

        <vt:Label runat="server" SkinID="Title" Text="Basic SplitContainer control" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Show behavior of SplitContainer control" Top="75px" ID="lblDefinition"></vt:Label>

        <!-- Actual sample element(s) should be placed instead of button below -->
        <vt:SplitContainer runat="server" SplitterDistance="145" Top="100px" Left="50px" Width="650px" Height="350px" ID="splitContainer1">

            <vt:SplitterPanel runat="server" Dock="Fill">
                <vt:Panel  runat="server" ID="pan1" Dock="Fill" BackColor="Yellow">
                    <vt:Button  runat="server" ID="btn1" Width="40" Height="25" Text="button1 "></vt:Button>
                </vt:Panel>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" Dock="Right" Width="200px" ID="sp">
            </vt:SplitterPanel>
        </vt:SplitContainer>
        <vt:Panel runat="server" Top="455px" Left="50px" Width="650px" Height="350px">
                <vt:Panel  runat="server" Dock="Fill" BackColor="Yellow">
                    <vt:Button  runat="server" ID="Button1" Width="40" Height="25" Text="button1 "></vt:Button>
                </vt:Panel>
            <vt:Panel runat="server" Dock="Right" Width="200px" ID="p">
            </vt:Panel>
        </vt:Panel>

<%--        <vt:Panel runat="server" --%>
    </vt:WindowView>
</asp:Content>
