<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window Icon Property" Icon = "/Content/Images/Tulips.jpg" ID="windowView1" LoadAction="WindowIcon\OnLoad" >
		
         <vt:Label runat="server" SkinID="Title" Text="Icon" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the window's icon.

            Syntax: public ResourceReference Icon {get; set;}
            
            Remarks: This property will have no effect if WindowBorderStyle is set to FixedDialog. In that case,
             the window will not display an icon." Top="75px"  ID="lblDefinition" ></vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:"  Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Icon Property of this window is initially set with icon.
            In the following example you can change this window's icon or open a new window initialized
            with icon, using the buttons below." Top="280px"   ID="lblExp1" ></vt:Label> 
            
		<vt:Button runat="server" Text="Change Icon >>" Top="370px"  ClickAction="WindowIcon\btnChangeIcon_Click" ID="btnChangeIcon"  Width="150px" ></vt:Button>

        <vt:Button runat="server" Text="Open New Win >>" Top="370px" Left="250px"  ClickAction="WindowIcon\btnOpenNewWinWithIcon_Click" ID="btnOpenNewWinWithIcon"  Width="150px" ></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="420px"  ID="lblLog"></vt:Label>

        </vt:WindowView>
</asp:Content>