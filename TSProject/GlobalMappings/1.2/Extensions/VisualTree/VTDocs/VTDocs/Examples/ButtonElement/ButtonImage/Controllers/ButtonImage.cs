using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //private void btnChangeImage_Click(object sender, EventArgs e)
        //{
        //    ButtonElement btnChangeImage = this.GetVisualElementById<ButtonElement>("btnChangeImage");

        //    if (btnChangeImage.Image == null)
        //    {
        //        btnChangeImage.Image = new UrlReference("/Content/Elements/Button.png");
        //    }
        //    else
        //    {
        //        btnChangeImage.Image = null;
        //    }
        //}
        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton1 = this.GetVisualElementById<ButtonElement>("btnTestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (btnTestedButton1.Image == null)
            {
                lblLog1.Text = "No image";
            }
            else
            {
                lblLog1.Text = "Set with image";
            }


            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (btnTestedButton2.Image == null)
            {
                lblLog2.Text = "No image.";
            }
            else
            {
                lblLog2.Text = "Set with image.";
            }
        }



        public void btnChangeImage_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (btnTestedButton2.Image == null)
            {
                btnTestedButton2.Image = new UrlReference("/Content/Elements/Button.png");
                lblLog2.Text = "Set with image.";
            }
            else
            {
                btnTestedButton2.Image = null;
                lblLog2.Text = "No image.";

            }
        }
      
    }
}