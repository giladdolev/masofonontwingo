using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new ImageEnabled());
        }

        private ImageEnabled ViewModel
        {
            get { return this.GetRootVisualElement() as ImageEnabled; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage1 = this.GetVisualElementById<ImageElement>("TestedImage1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedImage1.Enabled.ToString();

            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedImage2.Enabled.ToString() + ".";
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedImage2.Enabled = !TestedImage2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedImage2.Enabled.ToString() + ".";
        }

        private void TestedImage1_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\n" + ViewModel.clickcounterProperty++.ToString() + " - Image: Click event is invoked";
        }

        private void TestedImage2_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\n" + ViewModel.clickcounterProperty++.ToString() + " - TestedImage: Click event is invoked";
        }
    }
}