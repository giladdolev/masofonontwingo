<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Click event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Label runat="server" Text="Click Run Time" Top="160px" Left="50px" ID="label" Width="350px" ClickAction="LabelClick\LabelClick_Click"></vt:Label>

        <vt:Label runat="server" Text="" Top="112px" Left="450px" ID="lblEvent" Width="350px" Font-Bold="true"></vt:Label>
    </vt:WindowView>
</asp:Content>
