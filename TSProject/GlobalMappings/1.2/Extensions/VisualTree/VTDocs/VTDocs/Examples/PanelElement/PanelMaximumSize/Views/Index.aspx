<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel MaximumSize Property" ID="windowView" LoadAction="PanelMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedPanel' is width: 260px and height: 100px. 
            Its MaximumSize is initially set to width: 240px and height: 80px.
            To cancel 'TestedPanel' MaximumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label>     

        <vt:Panel runat="server" Left="80px" Top="360px" Height="100px" Width="260px" Text="TestedPanel" ID="TestedPanel" MaximumSize="240, 80"></vt:Panel>


        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog" Width="400px" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="535px" Width="200px" ID="btnChangePnlMaximumSize" ClickAction="PanelMaximumSize\btnChangePnlMaximumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="535px" width="200px" Left="310px" ID="btnSetToZeroPnlMaximumSize" ClickAction="PanelMaximumSize\btnSetToZeroPnlMaximumSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>









