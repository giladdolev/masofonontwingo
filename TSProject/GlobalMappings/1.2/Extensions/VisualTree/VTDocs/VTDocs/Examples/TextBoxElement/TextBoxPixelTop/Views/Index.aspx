<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox PixelTop Property" ID="windowView1" LoadAction="TextBoxPixelTop\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelTop" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the top edge of the control
             and the top edge of its container's client area.
            
            Syntax: public int PixelTop { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Top property of this TextBox is initially set to: 300px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:TextBox runat="server" Text="TestedTextBox" Top="300px" ID="TestedTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="340px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelTop value >>" Top="405px" ID="btnChangePixelTop" Width="180px" ClickAction="TextBoxPixelTop\btnChangePixelTop_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
