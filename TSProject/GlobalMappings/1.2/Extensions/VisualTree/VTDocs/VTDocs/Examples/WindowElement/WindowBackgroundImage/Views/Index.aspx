<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window BackgroundImage Property" BackgroundImage ="~/Content/Images/sleepy.jpg" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="1000px">
		
		<vt:Button runat="server"  TextAlign="MiddleCenter" Text="Change BackgroundImage" Top="110px" Left="44px" ClickAction="WindowBackgroundImage\btnChangeBackgroundImage_Click" ID="btnChangeBackgroundImage" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button>

		
		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="230px" Left="44px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="200px">
		</vt:TextBox>

	

        </vt:WindowView>
</asp:Content>