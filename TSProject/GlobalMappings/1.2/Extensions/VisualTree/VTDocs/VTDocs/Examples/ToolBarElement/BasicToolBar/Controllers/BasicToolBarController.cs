using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicToolBarController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 


        public ActionResult Index()
        {
            return View();
        }


        public void but_click(object sender, EventArgs e)
        {
            ToolBarElement tbl = this.GetVisualElementById<ToolBarElement>("ToolBar1");
            ResourceReference resource = new ResourceReference(@"Content\Images\Cute.png");
            tbl.Add(new ToolBarMenuItem("new Item",resource,null,System.Web.VisualTree.Keys.Alt));
            tbl.Refresh();
        }


        public void BasicToolBar_Load(object sender, EventArgs e)
        {
            ToolBarElement ToolBar1 = this.GetVisualElementById<ToolBarElement>("ToolBar1");
            ToolBar1.Items["Edit"].Enabled = false;
        }

        public void Button1_click(object sender, EventArgs e)
        {
            ToolBarElement ToolBar1 = this.GetVisualElementById<ToolBarElement>("ToolBar1");
            ToolBar1.Items["Edit"].Enabled = !ToolBar1.Items["Edit"].Enabled;
        }

    }
}