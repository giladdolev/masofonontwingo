using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowSetBoundsController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowSetBounds());
        }
        private WindowSetBounds ViewModel
        {
            get { return this.GetRootVisualElement() as WindowSetBounds; }
        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.Left == 11)
            {
                this.ViewModel.SetBounds(100, 400, 400, 400);
                textBox1.Text = "The Tested Window bounds are set to: Left " + this.ViewModel.Left + ", Top " + this.ViewModel.Top + ", Width " + this.ViewModel.Width + ", Height " + this.ViewModel.Height;

            }
            else
            {
                this.ViewModel.SetBounds(11, 57, 700, 800);
                textBox1.Text = "The Tested Window bounds are set to: Left " + this.ViewModel.Left + ", Top " + this.ViewModel.Top + ", Width " + this.ViewModel.Width + ", Height " + this.ViewModel.Height;
            }
        }
    }
}