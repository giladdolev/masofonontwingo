﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Label MinimumSize value: " + TestedLabel.MinimumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;

        }
        public void btnChangeLblMinimumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            if (TestedLabel.MinimumSize == new Size(250, 90))
            {
                TestedLabel.MinimumSize = new Size(230, 120);
                lblLog.Text = "Label MinimumSize value: " + TestedLabel.MinimumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;

            }
            else if (TestedLabel.MinimumSize == new Size(230, 120))
            {
                TestedLabel.MinimumSize = new Size(300, 70);
                lblLog.Text = "Label MinimumSize value: " + TestedLabel.MinimumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;
            }

            else
            {
                TestedLabel.MinimumSize = new Size(250, 90);
                lblLog.Text = "Label MinimumSize value: " + TestedLabel.MinimumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;
            }

        }

        public void btnSetToZeroLblMinimumSize_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedLabel.MinimumSize = new Size(0, 0);
            lblLog.Text = "Label MinimumSize value: " + TestedLabel.MinimumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;

        }

    }
}