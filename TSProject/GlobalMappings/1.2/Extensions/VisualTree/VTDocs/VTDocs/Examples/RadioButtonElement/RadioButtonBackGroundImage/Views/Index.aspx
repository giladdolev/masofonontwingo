<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton BackgroundImage Property" ID="windowView1" LoadAction="RadioButtonBackgroundImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImage" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image displayed in the control.
            
            public virtual Image BackgroundImage { get; set; }" Top="75px" ID="lblDefinition" ></vt:Label>
         
        <vt:RadioButton runat="server" Text="RadioButton" Top="140px" ID="TestedRadioButton1" ></vt:RadioButton>
        <vt:Label runat="server" SkinID="Log" Top="180px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackgroundImage property of this 'TestedRadioButton' is initially set to Image.png" Top="295px"  ID="lblExp1"></vt:Label>     
        
        <vt:RadioButton runat="server" Text="TestedRadioButton" BackgroundImage ="~/Content/Elements/Image.png" Top="345px" ID="TestedRadioButton2" ImageReference=""></vt:RadioButton>
        <vt:Label runat="server" SkinID="Log" Top="385px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackgroundImage Value >>"  Top="450px" Width="200px" ID="btnChangeBackgroundRadioButton" ClickAction="RadioButtonBackgroundImage\btnChangeBackgroundImage_Click"></vt:Button>
             
    </vt:WindowView>
</asp:Content>
