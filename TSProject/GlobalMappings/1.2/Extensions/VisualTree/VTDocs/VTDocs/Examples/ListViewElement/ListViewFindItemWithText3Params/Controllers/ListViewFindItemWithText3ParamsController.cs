using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewFindItemWithText3ParamsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Search for ListViewItem";
        }

        public void FindItemWithText_Click(object sender, EventArgs e)
        {
            //ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            //LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEnterString = this.GetVisualElementById<TextBoxElement>("txtEnterString");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            ListViewItem itemFounded = new ListViewItem();
            ComboBoxElement cmbStartIndex = this.GetVisualElementById<ComboBoxElement>("cmbStartIndex");
            CheckBoxElement chkIncludeSubItemsInSearch = this.GetVisualElementById<CheckBoxElement>("chkIncludeSubItemsInSearch");
           
            int output;

            if (int.TryParse(cmbStartIndex.Text, out output) && (output >= 0 && output < 3))
            {
                if (TestedListView.FindItemWithText(txtEnterString.Text, chkIncludeSubItemsInSearch.IsChecked, output) == null)

                    lblLog.Text = "FindItemWithText(" + txtEnterString.Text + ", " + chkIncludeSubItemsInSearch.IsChecked + ", " + output + ") was invoked.\\r\\nReturn value: " + TestedListView.FindItemWithText(txtEnterString.Text, chkIncludeSubItemsInSearch.IsChecked, output);
                else
                    lblLog.Text = "FindItemWithText(" + txtEnterString.Text + ", " + chkIncludeSubItemsInSearch.IsChecked + ", " + output + ") was invoked.\\r\\nReturn value: " + TestedListView.FindItemWithText(txtEnterString.Text, chkIncludeSubItemsInSearch.IsChecked, output).Text;
            }
            else 
                lblLog.Text = "Select from the Start Index from the comboBox";


            
        }
    }
}