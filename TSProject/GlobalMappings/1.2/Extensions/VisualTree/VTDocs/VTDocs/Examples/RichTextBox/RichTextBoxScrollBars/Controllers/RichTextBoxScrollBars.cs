using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxScrollBarsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeScrollBars_Click(object sender, EventArgs e)
        {
            RichTextBox rtfScrollBars = this.GetVisualElementById<RichTextBox>("rtfScrollBars");

            if (rtfScrollBars.ScrollBars == ScrollBars.Both)
                rtfScrollBars.ScrollBars = ScrollBars.Horizontal;

            else if (rtfScrollBars.ScrollBars == ScrollBars.Horizontal)
                rtfScrollBars.ScrollBars = ScrollBars.LargeChange;

            else if (rtfScrollBars.ScrollBars == ScrollBars.LargeChange)
                rtfScrollBars.ScrollBars = ScrollBars.Maximum;

            else if (rtfScrollBars.ScrollBars == ScrollBars.Maximum)
                rtfScrollBars.ScrollBars = ScrollBars.Minimum;

            else if (rtfScrollBars.ScrollBars == ScrollBars.Minimum)
                rtfScrollBars.ScrollBars = ScrollBars.None;

            else if (rtfScrollBars.ScrollBars == ScrollBars.None)
                rtfScrollBars.ScrollBars = ScrollBars.SmallChange;

            else if (rtfScrollBars.ScrollBars == ScrollBars.SmallChange)
                rtfScrollBars.ScrollBars = ScrollBars.Value;

            else if (rtfScrollBars.ScrollBars == ScrollBars.Value)
                rtfScrollBars.ScrollBars = ScrollBars.Vertical;

            else if (rtfScrollBars.ScrollBars == ScrollBars.Vertical)
                rtfScrollBars.ScrollBars = ScrollBars.Both;
            
        }

    }
}