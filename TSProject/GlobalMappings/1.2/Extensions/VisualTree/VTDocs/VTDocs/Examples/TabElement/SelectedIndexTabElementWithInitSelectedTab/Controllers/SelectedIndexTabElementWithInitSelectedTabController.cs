using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SelectedIndexTabElementWithInitSelectedTabController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void loadAction(object sender, EventArgs e)
        {
            TabElement tabControl1 = this.GetVisualElementById<TabElement>("tabControl1");
            if (tabControl1 != null)
            {
                tabControl1.SelectedTab = tabControl1.TabItems[0];
            }
        }


        public void AddTab_Click(object sender, EventArgs e)
        {
            TabElement tabelement = this.GetVisualElementById<TabElement>("tabControl1");
            TabItem tb = new TabItem("tab ");
            tabelement.TabItems.Add(tb);
        }


        public void Removetab_Click(object sender, EventArgs e)
        {
            TabElement tabelement = this.GetVisualElementById<TabElement>("tabControl1");
            tabelement.TabItems.Clear();
        }


        public void selectedtab_Click(object sender, EventArgs e)
        {
            TabElement tabelement = this.GetVisualElementById<TabElement>("tabControl1");
            if (tabelement.SelectedIndex != null)
            {
                MessageBox.Show("selectedtab is ==> " + tabelement.SelectedIndex);
            }
        }
    }
}