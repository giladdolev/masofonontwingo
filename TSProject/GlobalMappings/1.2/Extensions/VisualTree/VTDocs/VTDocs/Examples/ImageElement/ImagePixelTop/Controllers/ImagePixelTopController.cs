using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImagePixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             lblLog.Text = "PixelTop Value: " + TestedImage.PixelTop + '.';

         }
         public void btnChangePixelTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
             if (TestedImage.PixelTop == 285)
             {
                 TestedImage.PixelTop = 300;
                 lblLog.Text = "PixelTop Value: " + TestedImage.PixelTop + '.';

             }
             else
             {
                 TestedImage.PixelTop = 285;
                 lblLog.Text = "PixelTop Value: " + TestedImage.PixelTop + '.';
             }

         }

    }
}