<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar FindForm() Method" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="100px" Left="140px" ID="prgFindForm" Height="50px" Width="200px" >           
        </vt:ProgressBar>

        <vt:Button runat="server" Text="Invoke ProgressBar FindForm() method" Top="110px" Left="420px" ID="btnInvokeFindForm" Height="36px" Width="250px" ClickAction="ProgressBarFindForm\btnInvokeFindForm_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="200px" Left="140px" ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        