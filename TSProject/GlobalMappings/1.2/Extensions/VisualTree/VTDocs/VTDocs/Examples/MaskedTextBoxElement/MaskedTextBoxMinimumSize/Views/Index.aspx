﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="MaskedTextBox MinimumSize" ID="windowView1" LoadAction="MaskedTextBoxMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedMaskedTextBox' is width: 90px and height: 20px. 
            Its MinimumSize is initially set to width: 150px and height: 40px.
            To cancel 'TestedMaskedTextBox' MinimumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>

        <vt:MaskedTextBox runat="server" ID="TestedMaskedTextBox" Mask="(AAA) - (000) - (999) - (###)" Top="350px" Left="80px" Height="20px" Width="90px" MinimumSize="150, 40"></vt:MaskedTextBox>

        <vt:Label runat="server" SkinID="Log" Top="415px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="500px" width="200px" Left="80px" ID="btnChangeMskMinimumSize" ClickAction="MaskedTextBoxMinimumSize\btnChangeMskMinimumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="500px" width="200px" Left="310px" ID="btnSetToZeroMskMinimumSize" ClickAction="MaskedTextBoxMinimumSize\btnSetToZeroMskMinimumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>






