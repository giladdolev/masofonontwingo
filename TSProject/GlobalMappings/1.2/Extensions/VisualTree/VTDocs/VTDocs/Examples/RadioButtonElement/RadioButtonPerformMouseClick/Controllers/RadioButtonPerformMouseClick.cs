using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPerformMouseClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseClick_Click(object sender, EventArgs e)
        {
            
            RadioButtonElement testedRadioButton = this.GetVisualElementById<RadioButtonElement>("testedRadioButton");
            
            TreeItem t = new TreeItem();
            MouseButtons m = new MouseButtons();
            TreeItemMouseClickEventArgs args = new TreeItemMouseClickEventArgs(t,m,0,0,0);

            testedRadioButton.PerformMouseClick(args);
        }

        public void testedRadioButton_MouseClick(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRadioButton MouseClick event method is invoked");
        }

    }
}