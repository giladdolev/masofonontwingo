using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            lblLog.Text = "Tab Left Value is: " + TestedTab.Left + '.';
        }
        public void ChangeLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            if (TestedTab.Left == 200)
            {
                TestedTab.Left = 80;
                lblLog.Text = "Tab Left Value is: " + TestedTab.Left + '.';

            }
            else
            {
                TestedTab.Left = 200;
                lblLog.Text = "Tab Left Value is: " + TestedTab.Left + '.';
            }
        }

    }
}