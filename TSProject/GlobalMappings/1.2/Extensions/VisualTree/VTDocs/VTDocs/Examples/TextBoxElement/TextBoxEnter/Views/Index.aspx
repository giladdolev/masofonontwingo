<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Enter event" Height="750px" ID="windowView2" LoadAction="TextBoxEnter\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enter" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control is entered.

            Syntax: public event EventHandler Enter
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is on one of the 2 textBoxes, either by mouse,            keyboard
             or the Focus() the Enter event is invoked. 
             Each invocation of the 'Enter' event should add a line in the 'Event Log'."
            Top="250px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="330px" Height="105px" Width="340px">
            <vt:Label runat="server" Text="First Name :" Left="15px" Top="15px" ID="Label1"></vt:Label>

            <vt:TextBox runat="server" Top="15px" Left="160px" Text="Enter First Name" Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtFirstName" TabIndex="1"
                EnterAction="TextBoxEnter\txtFirstName_Enter" TextChangedAction="TextBoxGotFocus\txtFirstName_TextChanged">
            </vt:TextBox>

            <vt:Label runat="server" Text="Last Name :" Left="15px" Top="60px" ID="Label2"></vt:Label>

            <vt:TextBox runat="server" Top="60px" Left="160px" Text="Enter Last Name" Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtLastName" TabIndex="2"
                EnterAction="TextBoxEnter\txtLastName_Enter" TextChangedAction="TextBoxGotFocus\txtLastName_TextChanged">
            </vt:TextBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="450px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="490px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Focus() First Name >>" Top="645px" ID="btnFocusFirstName" ClickAction="TextBoxEnter\btnFocusFirstName_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

