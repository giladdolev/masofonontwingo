using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxResizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");


            if (testedCheckBox.Size == new Size(250, 36))
            {
                testedCheckBox.Size = new Size(300, 20);
                testedCheckBox.Text = "Set to Height: " + testedCheckBox.Height + ", Width: " + testedCheckBox.Width;
            }
            else
            {
                testedCheckBox.Size = new Size(250, 36);
                testedCheckBox.Text = "Back to Height: " + testedCheckBox.Height + ", Width: " + testedCheckBox.Width;
            }
        }

        private void testedCheckBox_Resize(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "Resize event is invoked\\r\\n";
        }
      
    }
}