using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            lblLog.Text = "Tab PixelLeft Value is: " + TestedTab.PixelLeft + '.';
        }
        public void ChangePixelLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            if (TestedTab.PixelLeft == 200)
            {
                TestedTab.PixelLeft = 80;
                lblLog.Text = "Tab PixelLeft Value is: " + TestedTab.PixelLeft + '.';

            }
            else
            {
                TestedTab.PixelLeft = 200;
                lblLog.Text = "Tab PixelLeft Value is: " + TestedTab.PixelLeft + '.';
            }
        }

    }
}