using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewFindItemWithText1ParamController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Search for ListViewItem";
        }

        public void FindItemWithText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEnterString = this.GetVisualElementById<TextBoxElement>("txtEnterString");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            ListViewItem itemFounded = new ListViewItem();
            //string textFounded = "";


            if (TestedListView.FindItemWithText(txtEnterString.Text) == null)

                lblLog.Text = "FindItemWithText(" + txtEnterString.Text + ") was invoked.\\r\\nReturn value: " + TestedListView.FindItemWithText(txtEnterString.Text);
            else
                lblLog.Text = "FindItemWithText(" + txtEnterString.Text + ") was invoked.\\r\\nReturn value: " + TestedListView.FindItemWithText(txtEnterString.Text).Text;
        }
    }
}