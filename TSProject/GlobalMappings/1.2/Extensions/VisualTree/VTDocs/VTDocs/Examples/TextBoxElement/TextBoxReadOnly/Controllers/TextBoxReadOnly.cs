using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxReadOnlyController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeReadOnly_Click(object sender, EventArgs e)
        {
            TextBoxElement txtReadOnlyChange = this.GetVisualElementById<TextBoxElement>("txtReadOnlyChange");

            if (txtReadOnlyChange.ReadOnly == true)
            {
                txtReadOnlyChange.ReadOnly = false;
            }
            else
            {
                txtReadOnlyChange.ReadOnly = true;
            }
            LabelElement lbl = this.GetVisualElementById<LabelElement>("Label2");
            lbl.Text = "ReadOnly Property is initially set to " + txtReadOnlyChange.ReadOnly.ToString();

        }

    }
}