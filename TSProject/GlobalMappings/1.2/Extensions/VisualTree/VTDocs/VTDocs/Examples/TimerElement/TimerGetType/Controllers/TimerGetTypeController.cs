using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerGetTypeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TimerGetType());
        }

     //   TimerGetType TimerGetTypeModel;
        private TimerGetType ViewModel
        {
            get { return this.GetRootVisualElement() as TimerGetType; }
        }

        public void TestedTimer_Tick(object sender, EventArgs e)
        {
            TextBoxElement timerTextBox = this.GetVisualElementById<TextBoxElement>("timerTextBox");
            timerTextBox.Text = ViewModel.durationProperty++.ToString();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press the GetType Button...";
        }

        public void GetType_Click(object sender, EventArgs e)
        {
            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "The element Type is: \\r\\n" + TestedTimer.GetType().ToString();
        }
    }
}