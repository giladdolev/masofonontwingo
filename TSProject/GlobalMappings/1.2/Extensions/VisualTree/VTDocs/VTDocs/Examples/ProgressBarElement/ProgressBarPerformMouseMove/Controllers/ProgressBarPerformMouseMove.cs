using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPerformMouseMoveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseMove_Click(object sender, EventArgs e)
        {
            
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");

            MouseEventArgs args = new MouseEventArgs();

            testedProgressBar.PerformMouseMove(args);
        }

        public void testedProgressBar_MouseMove(object sender, EventArgs e)
        {
            MessageBox.Show("TestedProgressBar MouseMove event method is invoked");
        }

    }
}