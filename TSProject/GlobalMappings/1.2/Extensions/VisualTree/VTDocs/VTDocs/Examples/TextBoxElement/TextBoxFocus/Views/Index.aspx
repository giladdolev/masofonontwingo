<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Focus Method" ID="windowView2" LoadAction="TextBoxFocus\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Focus" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Sets input focus to the control.

            Syntax: public bool Focus()
            
            Return Value: true if the input focus request was successful; otherwise, false.
            
            Remarks: The Focus method returns true if the control successfully received input focus. 
                     The control can have the input focus while not displaying any visual cues of having the focus." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the Focus button will invoke the 
            Focus method of this TextBox." Top="290px" ID="lblExp2"></vt:Label>

        <vt:TextBox runat="server" Text="TestedTextBox" Top="370px" ID="TestedTextBox" CssClass="vt-TestedTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="410" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Focus >>" Top="500px" ID="btnFocus" ClickAction="TextBoxFocus\Focus_Click"></vt:Button>

        <vt:Button runat="server" Text="Lose Focus >>" Top="500px" Left="250px" ID="btnLoseFocus" ClickAction="TextBoxFocus\btnLoseFocus_Click"></vt:Button>    

    </vt:WindowView>
</asp:Content>
