using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");
            LayoutEventArgs args = new LayoutEventArgs(testedTextBox, "Some String");

            testedTextBox.PerformLayout(args);
        }

        public void testedTextBox_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedTextBox Layout event method is invoked");
        }

    }
}