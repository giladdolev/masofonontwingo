using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnDock_Click(object sender, EventArgs e)
        {
            ProgressBarElement prgDock = this.GetVisualElementById<ProgressBarElement>("prgDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (prgDock.Dock == Dock.Bottom)
            {
                prgDock.Dock = Dock.Fill;
                textBox1.Text = prgDock.Dock.ToString();
            }
            else if (prgDock.Dock == Dock.Fill)
            {
                prgDock.Dock = Dock.Left;
                textBox1.Text = prgDock.Dock.ToString();
            }
            else if (prgDock.Dock == Dock.Left)
            {
                prgDock.Dock = Dock.Right;
                textBox1.Text = prgDock.Dock.ToString();
            }
            else if (prgDock.Dock == Dock.Right)
            {
                prgDock.Dock = Dock.Top;
                textBox1.Text = prgDock.Dock.ToString();
            }
            else if (prgDock.Dock == Dock.Top)
            {
                prgDock.Dock = Dock.None;
                textBox1.Text = prgDock.Dock.ToString();
            }
            else if (prgDock.Dock == Dock.None)
            {
                prgDock.Dock = Dock.Bottom;
                textBox1.Text = prgDock.Dock.ToString();
            }  
        }

    }
}