﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelSetBoundsController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested Label bounds are set to: \\r\\nLeft  " + TestedLabel.Left + ", Top  " + TestedLabel.Top + ", Width  " + TestedLabel.Width + ", Height  " + TestedLabel.Height;

        }
        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            if (TestedLabel.Left == 100)
            {
                TestedLabel.SetBounds(80, 300, 80, 15);
                lblLog.Text = "The Tested Label bounds are set to: \\r\\nLeft  " + TestedLabel.Left + ", Top  " + TestedLabel.Top + ", Width  " + TestedLabel.Width + ", Height  " + TestedLabel.Height;

            }
            else
            {
                TestedLabel.SetBounds(100, 280, 200, 50);
                lblLog.Text = "The Tested Label bounds are set to: \\r\\nLeft  " + TestedLabel.Left + ", Top  " + TestedLabel.Top + ", Width  " + TestedLabel.Width + ", Height  " + TestedLabel.Height;
            }
        }
    }
}