<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox SizeChanged event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxSizeChanged/Form_Load">
  
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="160px" Left="50px" Height="90px" Width="250px" SizeChangedAction="ComboBoxSizeChanged\btnTestedComboBox_SizeChanged"/>

        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change ComboBox SizeChanged" Top="112px" Left="130px" ID="btnTestedComboBox" Height="30px" TabIndex="1" Width="300px" ClickAction="ComboBoxSizeChanged\btnTestedComboBox_Click"></vt:Button>
        <vt:Label runat="server" Text="" Top="112px" Left="450px" ID="lblEvent" Width="350px" Font-Bold="true"></vt:Label>

    </vt:WindowView>
</asp:Content>
