using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxSelectionColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void clickBtn1(object sender, EventArgs e)
        {
            RichTextBox richTextBox1 = this.GetVisualElementById<RichTextBox>("richTextBox1");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (richTextBox1.SelectionColor == Color.Black)
            {
                richTextBox1.SelectionColor = Color.Red;
                textBox1.Text = richTextBox1.SelectionColor.ToString();
            }
            else if (richTextBox1.SelectionColor == Color.Red)
            {
                richTextBox1.SelectionColor = Color.Blue;
                textBox1.Text = richTextBox1.SelectionColor.ToString();
            }
            else
            {
                richTextBox1.SelectionColor = Color.Black;
                textBox1.Text = richTextBox1.SelectionColor.ToString();
            }
        }
    }
}