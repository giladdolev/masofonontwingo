using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "RadioButton bounds are: \\r\\n" + TestedRadioButton.Bounds;

        }
        public void btnChangeRadioButtonBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            if (TestedRadioButton.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedRadioButton.Bounds = new Rectangle(80, 355, 150, 26);
                lblLog.Text = "RadioButton bounds are: \\r\\n" + TestedRadioButton.Bounds;

            }
            else
            {
                TestedRadioButton.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "RadioButton bounds are: \\r\\n" + TestedRadioButton.Bounds;
            }

        }

    }
}