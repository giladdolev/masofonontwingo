<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton LostFocus event" ID="windowView2" LoadAction="RadioButtonLostFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="LostFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control loses focus.

            Syntax: public event EventHandler LostFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out of one of the RadioButtons,
             a 'LostFocus' event will be invoked.
             Each invocation of the 'LostFocus' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:GroupBox runat="server" Top="335px" Height="100px" Width="180px">

        <!--Tested RadioButton1 -->
        <vt:RadioButton runat="server"  Top="15px" Left="15px" Text="RadioButton1" ID="RadioButton1" TabIndex="1" LostFocusAction="RadioButtonLostFocus\RadioButton1_LostFocus"></vt:RadioButton>

        <!--Tested RadioButton2 -->
        <vt:RadioButton runat="server"  Top="56px" Left="15px" Text="RadioButton2"  ID="RadioButton2" TabIndex="2" LostFocusAction="RadioButtonLostFocus\RadioButton2_LostFocus"></vt:RadioButton>
        
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="350px"  ID="lblLog" Top="450px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Width="350px" Height="120px" Text="Event Log:" Top="500px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Top="645px" Text="Clear 'Event Log'" ID="btnClear" ClickAction="RadioButtonLostFocus\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
