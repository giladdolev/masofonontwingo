using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckedListBoxCheckedItemsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Check\\Uncheck CheckedListBoxItemCheck CheckBoxes";
        }


        public void testedCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckedListBoxElement testedCheckedListBox = this.GetVisualElementById<CheckedListBoxElement>("testedCheckedListBox");

            
            if (testedCheckedListBox.CheckedItems.Count > 0)
            {
                lblLog.Text = "CheckedItems - collection values:\\r\\n" + testedCheckedListBox.CheckedItems[0].Text;

                for (int i = 1; i < testedCheckedListBox.CheckedItems.Count; i++)
                {
                    lblLog.Text += ",\\r\\n " + testedCheckedListBox.CheckedItems[i].Text;
                }
            }
            else
            {
                lblLog.Text = "CheckedItems - collection values:\\r\\n";
            }
            
        }

    }
}