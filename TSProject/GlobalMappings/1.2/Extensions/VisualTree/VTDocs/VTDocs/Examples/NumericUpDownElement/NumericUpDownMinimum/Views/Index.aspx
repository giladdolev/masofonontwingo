<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="NumericUpDown Minimum" ID="windowView1" LoadAction="NumericUpDownMinimum\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Minimum" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the minimum allowed value for the spin box (also known as an up-down control).

            Syntax: public decimal Minimum { get; set; }
            The default value is 0.
                                    
            When the Minimum property is set, the Maximum property is evaluated and the UpdateEditText 
            method is called. If the new Minimum property value is greater than the Maximum property value,
            the Maximum value is set equal to the Minimum value. If the Value is less than the new Minimum value,
            the Value property is also set equal to the Minimum value. " Top="75px" ID="lblDefinition"></vt:Label>

        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown1" Top="270px" ID="TestedNumericUpDown1" Height="20px" TabIndex="1" Width="200px"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1" Width="500px"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the TestedNumericUpDown2 has minimum value:-10, maximum value:10.
            You can enter a minimum value using keyboard or using the up and down buttons or using 
            'Change minimum value' buttons. " Top="380px" ID="Label1"></vt:Label>
        
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown2" Top="470px" ID="TestedNumericUpDown2" Height="20px" TabIndex="1" Width="200px" Minimum="-10" Maximum="10" ValueChangedAction="NumericUpDownMinimum\TestedNumericUpDown2_ValueChanged" CssClass="vt-TestedNumericUpDown2"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog2" Height="70" Width="400px"></vt:Label>

        <vt:Button runat="server" Text="Change Minimum to -15 >>" Top="590px" width="180px" Left="290px" ID="btnChangeMinimumValueToMinus15" ClickAction="NumericUpDownMinimum\btnChangeMinimumValueToMinus15_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Minimum to 1 >>" Top="590px" width="180px" ID="btnChangeMinimumValueTo1" ClickAction="NumericUpDownMinimum\btnChangeMinimumValueTo1_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Value to 4 >>" Top="590px" width="180px" Left="500px" ID="btnChangeValueTo4" ClickAction="NumericUpDownMinimum\btnChangeValueTo4_Click"></vt:Button>
    
    </vt:WindowView>
</asp:Content>
