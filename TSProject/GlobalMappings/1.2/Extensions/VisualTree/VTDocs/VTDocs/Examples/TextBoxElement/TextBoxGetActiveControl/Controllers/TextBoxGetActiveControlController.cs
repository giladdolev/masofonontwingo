using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxGetActiveControlController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog");
            lbl.Text = "Active Control ID = '' ";
        }

        public async void Mouse_Hover(object sender, EventArgs e)
        {
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog");
            WindowElement w = this.GetRootVisualElement() as WindowElement;
            ControlElement ctr = await w.GetActiveControl();
            lbl.Text = "Active Control ID = " + ctr.ID;
        }
    }
}