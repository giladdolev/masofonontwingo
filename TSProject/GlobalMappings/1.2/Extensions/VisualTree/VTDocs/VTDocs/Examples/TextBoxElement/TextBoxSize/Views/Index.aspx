<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Size Property" ID="windowView1" LoadAction="TextBoxSize/OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control in pixels.
            
            Syntax: public Size Size { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="Size property of this TextBox is initially set to:  Width='300px' Height='80px'" Top="250px" ID="lblExp"></vt:Label>     
 
        <vt:TextBox runat="server" Text="TestedTextBox" Top="300px" ID="TestedTextBox" Height="80px"  Width="300px"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="395px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Top="440px" ID="btnChangeSize" Width="180px" ClickAction="TextBoxSize\btnChangeSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
