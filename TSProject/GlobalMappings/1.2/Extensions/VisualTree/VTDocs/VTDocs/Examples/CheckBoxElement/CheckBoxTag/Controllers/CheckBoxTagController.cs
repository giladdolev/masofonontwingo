using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
     

        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (TestedCheckBox1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedCheckBox1.Tag;
            }

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            if (TestedCheckBox2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedCheckBox2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedCheckBox2.Tag == "New Tag.")
            {
                TestedCheckBox2.Tag = TestedCheckBox2;
                lblLog2.Text = "Tag value: " + TestedCheckBox2.Tag;
            }
            else
            {
                TestedCheckBox2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedCheckBox2.Tag;

            }
        }
    }
}