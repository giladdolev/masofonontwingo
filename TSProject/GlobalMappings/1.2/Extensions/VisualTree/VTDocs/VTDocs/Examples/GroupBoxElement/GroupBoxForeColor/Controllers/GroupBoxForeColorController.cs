using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox1 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox1");
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedGroupBox1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedGroupBox2.ForeColor + '.';
        }

        public void btnChangeGroupBoxForeColor_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGroupBox2.ForeColor == Color.Green)
            {
                TestedGroupBox2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedGroupBox2.ForeColor + '.';
            }
            else
            {
                TestedGroupBox2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedGroupBox2.ForeColor + '.';
            }
        }

    }
}