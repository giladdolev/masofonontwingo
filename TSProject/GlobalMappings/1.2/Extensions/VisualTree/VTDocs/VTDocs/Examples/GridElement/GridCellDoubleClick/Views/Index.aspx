﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"  %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CellDoubleClick event" ID="Form1"  LoadAction="GridCellDoubleClick\OnLoad">
         <vt:Label runat="server" SkinID="Title" Text="GridCellDoubleClick" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the user double-clicks anywhere in a cell.
            
             public event DataGridViewCellEventHandler CellDoubleClick" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="180px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells in the TestedGrid by double clicking the mouse. 
            Each double clicking will invoke the 'CellDoubleClick' event, each invocation should add a line to the 
            'Event Log' and the current index of the selected cell is wrriten in the log."
            Top="230px" ID="lblExp2">
        </vt:Label>
              
        <vt:Grid runat="server" Top="310px" ID="TestedGrid" CssClass="vt-test-grid" CellDoubleClickAction="GridCellDoubleClick\TestedGrid_CellDoubleClick">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridCheckBoxColumn>
                <vt:GridButtonColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridButtonColumn>
            </Columns>
        </vt:Grid>
        
         <vt:Label runat="server" SkinID="Log" Top="440px" ID="lblLog" Text="GridCellDoubleClick event"></vt:Label>

         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="490px" ID="txtEventLog"></vt:TextBox>
         
    </vt:WindowView>
</asp:Content>
