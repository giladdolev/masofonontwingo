using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
using System.Drawing;

namespace HelloWorldApp
{
    public class GridSearchPanelController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void page_load(object sender, EventArgs e)
        {
            GridElement gridElement1 = this.GetVisualElementById<GridElement>("gridElement1");
            GridElement gridElement2 = this.GetVisualElementById<GridElement>("grid2");
            gridElement1.TextButtonList.Add("TextButtonColumn");
            gridElement1.AddToComboBoxList("ComboBoxColumn", "male|1,female|2");
            gridElement2.TextButtonList.Add("TextButtonColumn");
            gridElement2.AddToComboBoxList("ComboBoxColumn", "male|1,female|2");

            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "ShowFilter value: " + gridElement1.SearchPanel.ToString();

            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ShowFilter value: " + gridElement2.SearchPanel.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("grid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (dgvCalendario.SearchPanel == true)
            {
                dgvCalendario.SearchPanel = false;
                lblLog2.Text = "ShowFilter value: " + dgvCalendario.SearchPanel.ToString();
            }
            else
            {
                dgvCalendario.SearchPanel = true;
                lblLog2.Text = "ShowFilter value: " + dgvCalendario.SearchPanel.ToString();

            }
           
        
        }

      


    }
}
