using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowLayoutController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowLayout());
        }
        private WindowLayout ViewModel
        {
            get { return this.GetRootVisualElement() as WindowLayout; }
        }

        public void windowView1_Layout(object sender, LayoutEventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            this.ViewModel.Width = 100;
            this.ViewModel.Height = 100;

            textBox1.Text += "The Layout event is invoked\\r\\n";
            
        }

    
    }
}