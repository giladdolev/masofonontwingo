using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowFocusController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowFocus());
        }
        private WindowFocus ViewModel
        {
            get { return this.GetRootVisualElement() as WindowFocus; }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
        {

            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowFocus()");

            ViewModel.TestedWin = VisualElementHelper.CreateFromView<FocusTestedWindow>("FocusTestedWindow", "Index2", null, argsDictionary, null);
            this.ViewModel.TestedWin.Show();
        }

        public void btnChangeFocus_Click(object sender, EventArgs e)
        {

            if (this.ViewModel.TestedWin != null)
            {
                TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
                this.ViewModel.TestedWin.Focus();
            }
            else
                MessageBox.Show("Open Testes window First");

        }

    }
}