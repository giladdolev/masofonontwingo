using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonTextChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");

            lblLog.Text = "Button Text value: " + TestedButton.Text;

        }

        public void TestedButton_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nButton TextChanged event is invoked";
            lblLog.Text = "Button Text value: " + TestedButton.Text;
        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");

            if (TestedButton.Text == "Some Text")
                TestedButton.Text = "New Text";
            else
                TestedButton.Text = "Some Text";
        }
    }
}