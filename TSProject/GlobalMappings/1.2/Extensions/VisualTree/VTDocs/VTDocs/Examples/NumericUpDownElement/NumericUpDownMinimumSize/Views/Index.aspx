<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="NumericUpDown MinimumSize" ID="windowView1" LoadAction="NumericUpDownMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the minimum size of the spin box (also known as an up-down control). 

            Syntax: public virtual Size MinimumSize { get; set; } 
            
            Value: The Size, which is the minimum size of the spin box." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedNumericUpDown' is width: 100px 
            and height: 50px. Its MinimumSize is initially set to width: 250px and height: 40px.
            To cancel 'TestedNumericUpDown' MinimumSize, set width and height values to 0px." Top="230px" ID="Label1"></vt:Label>
        
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="320px" ID="TestedNumericUpDown" Width="100px" Height="50px" MinimumSize="250, 40" TabIndex="1"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="415px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="480px" width="200px" Left="80px" ID="btnChangeNudMinimumSize" ClickAction="NumericUpDownMinimumSize\btnChangeNudMinimumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="480px" width="200px" Left="310px" ID="btnSetToZeroNudMinimumSize" ClickAction="NumericUpDownMinimumSize\btnSetToZeroNudMinimumSize_Click"></vt:Button>

    
    </vt:WindowView>
</asp:Content>
