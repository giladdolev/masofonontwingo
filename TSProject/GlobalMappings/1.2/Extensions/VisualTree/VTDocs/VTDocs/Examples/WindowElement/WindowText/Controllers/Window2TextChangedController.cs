using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class Window2TextChangedController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index2()
        {
            return View(new Window2TextChanged());
        }
        private Window2TextChanged ViewModel
        {
            get { return this.GetRootVisualElement() as Window2TextChanged; }
        }

        public void btnWindow2ChangeText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.Text == "Window2 Text Property")
            {
                this.ViewModel.Text = "The new Text : Window2 Text Property changed";
                textBox1.Text = this.ViewModel.Text.ToString();
            }
            else
            {
                this.ViewModel.Text = "Window2 Text Property";
                textBox1.Text = this.ViewModel.Text.ToString();
            }
        }
    }
}