<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox SelectedIndexChanged event" ID="windowView2" LoadAction="ComboBoxSelectedIndexChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectedIndexChanged" Left="250px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the SelectedIndex property has changed.

            Syntax: public event EventHandler SelectedIndexChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can change the combobox SelectedIndex value by selecting an item 
            from the dropdown list. 
            Each invocation of the 'TextChanged' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" CssClass="vt-test-cmb" Top="325px" ID="TestedComboBox" SelectedIndexChangedAction="ComboBoxSelectedIndexChanged\TestedComboBox_SelectedIndexChanged"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="365px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="405px" ID="txtEventLog"></vt:TextBox>       
       
    </vt:WindowView>
</asp:Content>
