using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicGroupBoxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void ChangeForeColor_Click(object sender, EventArgs e)
        {
            GroupBoxElement groupBox = this.GetVisualElementById<GroupBoxElement>("Frame1");
            groupBox.ForeColor = Color.Fuchsia;
            groupBox.Font = new Font("Algerian", 12, FontStyle.Italic);
        }
        public void ChangeColors_Click(object sender, EventArgs e)
        {
            GroupBoxElement groupBox = this.GetVisualElementById<GroupBoxElement>("GroupBoxColors");
            groupBox.BackColor = Color.Lime;
        }
        public void ChangeSize_Click(object sender, EventArgs e)
        {
            GroupBoxElement groupBox0 = this.GetVisualElementById<GroupBoxElement>("Frame1");
            if (groupBox0.PixelHeight == 100 && groupBox0.PixelWidth == 200) //Get
            {
                groupBox0.PixelHeight = 200;
                groupBox0.PixelWidth = 400;
            }
            else
            {
                groupBox0.PixelHeight = 100;
                groupBox0.PixelWidth = 200;
            }
            GroupBoxElement groupBox1 = this.GetVisualElementById<GroupBoxElement>("GroupBoxColors");
            groupBox1.Size = new Size(300, 80);
            GroupBoxElement groupBox2 = this.GetVisualElementById<GroupBoxElement>("GroupBoxBorder");
            groupBox2.Size = new Size(450, 150);
        }
    }
}