using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarHideController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Visible value is: " + TestedToolBar.Visible;
        }

        public void btnShow_Click(object sender, EventArgs e)
        {

            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedToolBar.Show();
            lblLog.Text = "Visible value is: " + TestedToolBar.Visible;
        }

        public void btnHide_Click(object sender, EventArgs e)
        {

            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedToolBar.Hide();
            lblLog.Text = "Visible value is: " + TestedToolBar.Visible;
        }
    }
}