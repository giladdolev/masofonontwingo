using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;

namespace HelloWorldApp
{
    public class GridSetRowHeaderBackgroundColorController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");

            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            cmbInd.Items.Add("0");
            cmbInd.Items.Add("1");
            cmbInd.Items.Add("2");

            cmbInd.SelectedIndex = 0;

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundColor: Transparent";  

        }

        private void btnAquamarineRowHeaderBackgroundColor_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.SetRowHeaderBackgroundColor(cmbInd.SelectedIndex, Color.Aquamarine);

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundColor: Aquamarine"; 
        }

        private void btnFuchsiaRowHeaderBackgroundColor_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.SetRowHeaderBackgroundColor(cmbInd.SelectedIndex, Color.Fuchsia);

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundColor: Fuchsia"; 
        }

        private void btnTransparentRowHeaderBackgroundColor_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.SetRowHeaderBackgroundColor(cmbInd.SelectedIndex, Color.Transparent);

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundColor: Transparent";
        }
    }
}
