<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox PasswordChar Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
     
        
        <vt:Label runat="server" Text="TextBox PasswordChar is set to 140px" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="400px"></vt:Label>

        <vt:TextBox runat="server" Text="TestedTextBox" PasswordChar='b' Top="115px" Left="140px" ID="testedTextBox" Height="36px"  TabIndex="3" Width="200px"></vt:TextBox>

        <vt:Button runat="server" Text="Change TextBox PasswordChar" Top="115px" Left="400px" ID="btnChangePasswordChar" Height="36px"  TabIndex="3" Width="200px" ClickAction="TextBoxPasswordChar\btnChangePasswordChar_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="200px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>


    </vt:WindowView>
</asp:Content>
