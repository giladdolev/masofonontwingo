<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid ShowHeader property" ID="windowView1" LoadAction="ComboGridShowHeader/OnLoad">
        <vt:Label runat="server" SkinID="Title" Text="ShowHeader" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the ShowHeader property of the control.
            
            Syntax: public bool ShowHeader{ get; set; }
            Values: 'false' if column headers of the ComboGrid control are shown; otherwise, 'true'.
            The default is 'false'.
            
            Notice - this property is not intended to be changed at run time." Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:ComboGrid runat="server" ID="TestedComboGrid1" Text="ComboGrid" CssClass="vt-test-ComboGrid" Top="220px"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="260px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ShowHeader property of the left ComboGrid is initially set to 'true'.
            Therefore, the columns headers are shown.
            ShowHeader property of the right ComboGrid is initially set to 'false'." Top="375px"  ID="lblExp1"></vt:Label>     
        
        <vt:ComboGrid runat="server" ID="TestedComboGrid2" Text="TestedComboGrid1" CssClass="vt-test-ComboGrid-1" ShowHeader="true" Top="460px"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog2"></vt:Label>

        <vt:ComboGrid runat="server" ID="TestedComboGrid3" Text="TestedComboGrid2" CssClass="vt-test-ComboGrid-2" ShowHeader="false" Top="460px" Left="400px"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog3" Left="400px"></vt:Label>

    </vt:WindowView>
</asp:Content>
