using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabDeselectTabTabItemController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Current selected tab item: \\r\\n" + TestedTab.SelectedTab + ", " + TestedTab.SelectedTab.Text;
        }

        public void btnDeselectTab_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");



            try
            {

                switch (txtInput.Text)
                {
                    case "TabItem1":
                        for (int i = 0; i < TestedTab.TabItems.Count; i++)
                        {
                            if (TestedTab.TabItems[i].Text == "TabItem1")
                            {
                                TestedTab.DeselectTab(TestedTab.TabItems[i]);
                                lblLog.Text = "After Deselect(" + txtInput.Text + ")" + ", Current Selected tab is: \\r\\n" + TestedTab.SelectedTab + ", " + TestedTab.SelectedTab.Text;
                            }
                        }
                        break;

                    case "TabItem2":

                        for (int i = 0; i < TestedTab.TabItems.Count; i++)
                        {
                            if (TestedTab.TabItems[i].Text == "TabItem2")
                            {
                                TestedTab.DeselectTab(TestedTab.TabItems[i]);
                                lblLog.Text = "After Deselect(" + txtInput.Text + ")" + ", Current Selected tab is: \\r\\n" + TestedTab.SelectedTab + ", " + TestedTab.SelectedTab.Text;
                            }
                        }

                        break;

                    case "TabItem3":

                        for (int i = 0; i < TestedTab.TabItems.Count; i++)
                        {
                            if (TestedTab.TabItems[i].Text == "TabItem3")
                            {
                                TestedTab.DeselectTab(TestedTab.TabItems[i]);
                                lblLog.Text = "After Deselect(" + txtInput.Text + ")" + ", Current Selected tab is: \\r\\n" + TestedTab.SelectedTab + ", Text: " + TestedTab.SelectedTab.Text;
                            }
                        }
                        break;

                    default :
                        if (txtInput.Text == "")
                        {
                            TabItem newTab = null;
                            TestedTab.DeselectTab(newTab);
                        }
                        else
                        {
                            lblLog.Text = "To invoke Deselect(TabItem tabItem) enter one of the tab items text";
                        }
                        break;

                }
            }

           
            catch (ArgumentException ex)
            {
                lblLog.Text = ex.GetType().ToString() + " was thrown\\r\\nEnter one of the tab items";
            }

        }

        public void txtInput_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

            lblLog.Text = "Input tab: " + txtInput.Text + '.';
        }

    }
}