<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic ComboBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="BasicComboBox/Form_Load">

        <vt:ComboGrid runat="server" ID="cmgrd" SelectedIndexChangeAction="BasicComboBox\select"  TextChangedAction="BasicComboBox\textChanged"></vt:ComboGrid>

          <%--<vt:ComboGrid runat="server" Left="300" ID="cmgrd2"  ShowFilter="true"></vt:ComboGrid>--%>

        <vt:Button runat="server" Text="Add Item" Top="120px" Left="500px" ID="addItem" ClickAction="BasicComboBox\addItem_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
