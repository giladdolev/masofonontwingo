<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" StartPosition="WindowsDefaultLocation" Text="Home" Top="57px" Left="11px" ID="windowView1" BorderStyle="None" Font-Bold="True" Font-Names="Tahoma" Font-Size="9pt" Height="905px" TabIndex="-1" Width="768px" LoadAction="TreeAdd\loadAction">
      <vt:Tree runat="server" ID="tree1"></vt:Tree>
        <vt:Button runat="server" ID="btn1" Top="200" Text="click to add TreeItem" ClickAction="TreeAdd\Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
