using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ContextMenuStripBasicController : Controller
    {
          /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void Load(object sender, EventArgs e)
        {
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            ToolBarItem menuItem = (ToolBarItem)sender;
            MessageBox.Show(menuItem.Text);
        }
        private void menuItem2_Click(object sender, EventArgs e)
        {
            ToolBarItem menuItem = (ToolBarItem)sender;
            MessageBox.Show(menuItem.Text);
        }
        private void menuItem3_Click(object sender, EventArgs e)
        {
            ToolBarItem menuItem = (ToolBarItem)sender;
            MessageBox.Show(menuItem.Text);
        }
        private void menuItem4_Click(object sender, EventArgs e)
        {
            ToolBarItem menuItem = (ToolBarItem)sender;
            MessageBox.Show(menuItem.Text);
        }
        private void menuItem5_Click(object sender, EventArgs e)
        {
            ToolBarItem menuItem = (ToolBarItem)sender;
            MessageBox.Show(menuItem.Text);
        }
    }
}