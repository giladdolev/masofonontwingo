using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkAccessibleRole = this.GetVisualElementById<CheckBoxElement>("chkAccessibleRole");

            MessageBox.Show(chkAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkAccessibleRole = this.GetVisualElementById<CheckBoxElement>("chkAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (chkAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else if (chkAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                chkAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = chkAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}