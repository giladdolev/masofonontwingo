<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic GroupBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:GroupBox runat="server" Text="Frame1" Top="60px" Left="128px" ID="Frame1" BorderStyle="Inset" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Blue" Height="100px" Width="400px">
            <vt:Label runat="server" Text="Some Content"></vt:Label>
        </vt:GroupBox>
        <vt:GroupBox runat="server" Text="Frame1" Top="180px" Left="128px" ID="GroupBoxColors" BackColor="Yellow" BorderStyle="Inset" Height="100px" Width="400px">
            <vt:Label runat="server" Text="Some Content"></vt:Label>
        </vt:GroupBox>
        <vt:GroupBox runat="server" Text="Frame1" Top="300px" Left="128px" ID="GroupBoxBorder" BackColor="White" BorderStyle="None" Height="100px" Width="400px">
            <vt:Label runat="server" Text="GroupBox without border"></vt:Label>
        </vt:GroupBox>
        <vt:GroupBox runat="server" ShowHeader="True" AutoScroll="True" TabStop="False" Text="groupBox1" Top="420px" Left="128px" ID="groupBox1" Height="315px" Width="284px">
            <vt:GroupBox runat="server" ShowHeader="False" AutoScroll="True" TabStop="False" Dock="Top" Text="groupBox2" Top="16px" Left="3px" ID="groupBox2" BorderStyle="None" Height="100px" Width="278px">
                <vt:Button runat="server" Text="button1" Top="71px" Left="9px" ID="button2" BorderStyle="None" Height="23px" Width="75px"></vt:Button>
            </vt:GroupBox>
            <vt:GroupBox runat="server" ShowHeader="False" AutoScroll="True" TabStop="False" Text="groupBox3" Top="122px" Left="3px" ID="groupBox3" BorderStyle="None" Height="143px" TabIndex="1" Width="278px">
                <vt:Button runat="server" Text="button2" Top="71px" Left="9px" ID="button3" BorderStyle="None" Height="23px" Width="75px"></vt:Button>
                <vt:Button runat="server" Text="button4" Top="19px" Left="6px" ID="button4" BorderStyle="None" Height="23px" Width="75px"></vt:Button>
                <vt:GroupBox runat="server" ShowHeader="False" AutoScroll="True" TabStop="False" Text="groupBox4" Top="48px" Left="3px" ID="groupBox4" BorderStyle="None" Height="133px" Width="278px"></vt:GroupBox>
            </vt:GroupBox>
            <vt:Button runat="server" Text="button3" Top="261px" Left="12px" ID="button5" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="23px" Width="75px"></vt:Button>
        </vt:GroupBox>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change Title Style" Top="70px" Left="600px" ID="ChangeFrame1ForeColor" Height="49px" Width="169px" ClickAction="BasicGroupBox\ChangeForeColor_Click">
        </vt:Button>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change Background" Top="190px" Left="600px" ID="ChangeColors" Height="49px" Width="169px" ClickAction="BasicGroupBox\ChangeColors_Click">
        </vt:Button>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change Size" Top="310px" Left="600px" ID="Button1" Height="49px" Width="169px" ClickAction="BasicGroupBox\ChangeSize_Click">
        </vt:Button>
    </vt:WindowView>
</asp:Content>
