
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Size Property" ID="windowView" LoadAction="ButtonSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control in pixels.

            Syntax: public Size Size { get; set; }." Top="75px" ID="Label2" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The size of this 'TestedButton' is initially set with 
            Width: 150px, Height: 36px" Top="260px"  ID="lblExp1"></vt:Label>     

        
        <vt:Button runat="server" SkinID="Wide"  Text="TestedButton" Top="330px" ID="btnTestedButton"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="400px" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Button Size >>"  Top="480px" Width="180px" ID="btnChangeButtonSize" ClickAction="ButtonSize\btnChangeButtonSize_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>




