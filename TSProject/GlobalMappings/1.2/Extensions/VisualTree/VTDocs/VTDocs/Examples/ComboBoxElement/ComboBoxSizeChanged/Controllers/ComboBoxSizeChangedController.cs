using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");
            cmb1.Items.Add("aaa");
            cmb1.Items.Add("bbb");
        }

        public void btnTestedComboBox_Click(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");

            if (cmb1.Size == new Size(250, 90))
            {
                cmb1.Size = new Size(75, 100);
            }
            else
            {
                cmb1.Size = new Size(250, 90);
            }
        }

        public void btnTestedComboBox_SizeChanged(object sender, EventArgs e)
        {
            LabelElement lblEvent = this.GetVisualElementById<LabelElement>("lblEvent");
            if (lblEvent != null)
                lblEvent.Text = "ComboBox SizeChanged event is invoked";
        }

      

    }
}