using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxAutoSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnAction1_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            TextBoxElement txtLog1 = this.GetVisualElementById<TextBoxElement>("txtLog1");

            if (TestedCheckBox1.AutoSize == false)
            {
                TestedCheckBox1.AutoSize = true;
                txtLog1.Text = "AutoSize value: " + TestedCheckBox1.AutoSize;
            }
            else
            {
                TestedCheckBox1.AutoSize = false;
                txtLog1.Text = "AutoSize value: " + TestedCheckBox1.AutoSize;
            }
        }

        private void btnAction2_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            TextBoxElement txtLog2 = this.GetVisualElementById<TextBoxElement>("txtLog2");

            if (TestedCheckBox2.AutoSize == false)
            {
                TestedCheckBox2.AutoSize = true;
                txtLog2.Text = "AutoSize value: " + TestedCheckBox2.AutoSize;
            }
            else
            {
                TestedCheckBox2.AutoSize = false;
                txtLog2.Text = "AutoSize value: " + TestedCheckBox2.AutoSize;
            }
        }
    }
}