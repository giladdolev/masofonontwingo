﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CurrentCell" LoadAction="GridCurrentCell\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="CurrentCell" Top="15px" ID="lblTitle" Left="300"> </vt:Label>
        <vt:Label runat="server" Text="Gets or sets the currently active cell. 
            
            syntax : public GridCellElement CurrentCell { get; set; }
            
             The default is the first cell in the first column or null if there are no cells in the control."

           Top="65px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can change the CurrentCell value by clicking on one of the grid  
           cells or by clicking the 'Change CurrentCell to (1,2)' button. " Top="270px" ID="Label2">
        </vt:Label>
        <vt:Grid runat="server" Top="340px" ID="TestedGrid" SelectionChangedAction="GridCurrentCell\CellSelectionChanged_Click">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="470px" ID="lblLog1"></vt:Label>
      
          
        <vt:Button runat="server" Text="Change CurrentCell to (1,2) >>" Top="550px" Width="180px" ID="btnChangeCurrentCell" ClickAction="GridCurrentCell\btnChangeCurrentCell_Click"></vt:Button>

                
    </vt:WindowView>

</asp:Content>
