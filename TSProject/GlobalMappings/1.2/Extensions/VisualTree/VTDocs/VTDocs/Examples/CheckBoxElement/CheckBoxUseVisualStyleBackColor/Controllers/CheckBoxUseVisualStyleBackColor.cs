using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxUseVisualStyleBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeUseVisualStyleBackColor_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkUseVisualStyleBackColor = this.GetVisualElementById<CheckBoxElement>("chkUseVisualStyleBackColor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            

            if (chkUseVisualStyleBackColor.UseVisualStyleBackColor == true)
            {
                chkUseVisualStyleBackColor.UseVisualStyleBackColor = false;
                textBox1.Text = "UseVisualStyleBackColor value is:\\r\\n" + chkUseVisualStyleBackColor.UseVisualStyleBackColor;

            }

            else
            {
                chkUseVisualStyleBackColor.UseVisualStyleBackColor = true;
                textBox1.Text = "UseVisualStyleBackColor value is:\\r\\n" + chkUseVisualStyleBackColor.UseVisualStyleBackColor;
            }
        }

    }
}