﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server"  Height="800px" Width="1300px" Text="Grid SetCellBackGroundColor" LoadAction="GridSetCellBackgroundColor\Page_load">

        <vt:Label runat="server" SkinID="Title" Text="SetCellBackgroundColor Method" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Sets the background color for a specific cell.

         Syntax: public void SetCellBackgroundColor(int rowIndex, int colIndex, Color color)"
            Top="75px" ID="Label1">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the cell at rowIndex = 1 and columnIndex = 1 to colors :
            Red, White, Purple, Orange, Gray.
            We use the Method SetCellBackgroundColor(rowIndex, columnIndex, Color) to get the desired result for example :
            SetCellBackgroundColor(1, 1, Color.Red)
             "
            Top="200px" ID="lblExp2">
        </vt:Label>
        <vt:Grid runat="server" Top="265px" ID="gridElement" Height="150px" Width="300px">
            <Columns>
                
            </Columns>         
        </vt:Grid>
        
        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog1"></vt:Label>

        <vt:Button runat="server" Text="Red" ClickAction="GridSetCellBackgroundColor/Grid_SetCellBackgroundColorToRed" Top="500px"></vt:Button>
        <vt:Button runat="server" Text="White" ClickAction="GridSetCellBackgroundColor/Grid_SetCellBackgroundColorToWhite" Left="300px" Top="500px"></vt:Button>
        <vt:Button runat="server" Text="Purple" ClickAction="GridSetCellBackgroundColor/Grid_SetCellBackgroundColorToPurple" Left="500px" Top="500px"></vt:Button>
        <vt:Button runat="server" Text="Orange" ClickAction="GridSetCellBackgroundColor/Grid_SetCellBackgroundColorToOrange" Left="700px" Top="500px"></vt:Button>
        <vt:Button runat="server" Text="Gray" ClickAction="GridSetCellBackgroundColor/Grid_SetCellBackgroundColorToGray" Left="900px" Top="500px"></vt:Button>
    </vt:WindowView>

</asp:Content>

