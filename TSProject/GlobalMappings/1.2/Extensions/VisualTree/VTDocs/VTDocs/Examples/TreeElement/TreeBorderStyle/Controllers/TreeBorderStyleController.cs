using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedTree1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
        }
        
        public void btnChangeTreeBorderStyle_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTree2.BorderStyle == BorderStyle.None)
            {
                TestedTree2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Dotted)
            {
                TestedTree2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Double)
            {
                TestedTree2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedTree2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedTree2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Groove)
            {
                TestedTree2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Inset)
            {
                TestedTree2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Dashed)
            {
                TestedTree2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.NotSet)
            {
                TestedTree2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Outset)
            {
                TestedTree2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Ridge)
            {
                TestedTree2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedTree2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else if (TestedTree2.BorderStyle == BorderStyle.Solid)
            {
                TestedTree2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
            else
            {
                TestedTree2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedTree2.BorderStyle + '.';
            }
        }
    }
}