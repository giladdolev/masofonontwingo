<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Size Property" ID="windowView1" LoadAction="RichTextBoxSize/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control in pixels.
            
            Syntax: public Size Size { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="Size property of this RichTextBox is initially set to:  Width='300px' Height='120px'" Top="250px"  ID="lblExp"></vt:Label>     

        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="310px" ID="TestedRichTextBox" Height="120px" Width="300px" >           
        </vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="445px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Top="510px" ID="btnChangeSize" Width="180px" ClickAction="RichTextBoxSize\btnChangeSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
        