using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelTop value: " + TestedProgressBar.PixelTop + '.';
        }

        public void btnChangePixelTop_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.PixelTop == 310)
            {
                TestedProgressBar.PixelTop = 290;
                lblLog.Text = "PixelTop value: " + TestedProgressBar.PixelTop + '.';
            }
            else
            {
                TestedProgressBar.PixelTop = 310;
                lblLog.Text = "PixelTop value: " + TestedProgressBar.PixelTop + '.';
            }

        }
      
    }
}