<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown ValueChanged Property" Top="57px" Left="11px" ID="windowView1" Height="700px" Width="768px">
     

        <vt:NumericUpDown runat="server" Value="3" Top="97px" Left="250px" ID="numericUpDown1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="20px" Width="100px" ValueChangedAction="NumericUpDownValueChanged\NumericUpDownValueChanged_ValueChanged">
        </vt:NumericUpDown>

        <vt:Button runat="server" Text="Change Value" Top="70px" Left="20px" ID="btnChangeValue" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="NumericUpDownValueChanged\btnChangeValue_Click"></vt:Button>

        <vt:Button runat="server" Text="Get Value" Top="110px" Left="20px" ID="btnGetValue" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="NumericUpDownValueChanged\btnGetValue_Click"></vt:Button>

        <vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="200px" Left="144px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="67px" TabIndex="3" Width="184px">
		</vt:TextBox>

		<vt:Label runat="server" Text="Event Log" Top="180px" Left="141px" ID="label2" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Font-Bold="true" Height="13px" TabIndex="4" Width="56px">
		</vt:Label>

    </vt:WindowView>
</asp:Content>
