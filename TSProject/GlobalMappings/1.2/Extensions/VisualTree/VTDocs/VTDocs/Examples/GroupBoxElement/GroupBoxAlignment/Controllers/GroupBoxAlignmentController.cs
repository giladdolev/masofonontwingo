using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxAlignmentController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeAlignment_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpRuntimeAlignment = this.GetVisualElementById<GroupBoxElement>("grpRuntimeAlignment");

            if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.BottomOfCaption)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.CenterBottom;
                textBox1.Text = "GroupBoxAlignment.CenterBottom";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.CenterBottom)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.CenterMiddle;
                textBox1.Text = "GroupBoxAlignment.CenterMiddle";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.CenterMiddle)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.CenterTop;
                textBox1.Text = "GroupBoxAlignment.CenterTop";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.CenterTop)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.LeftBottom;
                textBox1.Text = "GroupBoxAlignment.LeftBottom";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.LeftBottom)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.LeftMiddle;
                textBox1.Text = "GroupBoxAlignment.LeftMiddle";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.LeftMiddle)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.LeftOfCaption;
                textBox1.Text = "GroupBoxAlignment.LeftOfCaption";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.LeftOfCaption)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.LeftTop;
                textBox1.Text = "GroupBoxAlignment.LeftTop";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.LeftTop)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.RightBottom;
                textBox1.Text = "GroupBoxAlignment.RightBottom";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.RightBottom)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.RightMiddle;
                textBox1.Text = "GroupBoxAlignment.RightMiddle";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.RightMiddle)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.RightOfCaption;
                textBox1.Text = "GroupBoxAlignment.RightOfCaption";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.RightOfCaption)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.RightTop;
                textBox1.Text = "GroupBoxAlignment.RightTop";
            }
            else if (grpRuntimeAlignment.Alignment == GroupBoxAlignment.RightTop)
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.TopOfCaption;
                textBox1.Text = "GroupBoxAlignment.TopOfCaption";
            }
            else
            {
                grpRuntimeAlignment.Alignment = GroupBoxAlignment.BottomOfCaption;
                textBox1.Text = "GroupBoxAlignment.BottomOfCaption";
           
            }
        }

    }
}