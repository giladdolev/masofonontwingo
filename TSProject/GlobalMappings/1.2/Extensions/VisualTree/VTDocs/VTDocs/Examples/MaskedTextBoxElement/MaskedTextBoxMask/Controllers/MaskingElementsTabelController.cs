﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MaskingElementsTabelController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index2()
        {
            return View(new MaskingElementsTabel());
        }

        //public void OnLoad(object sender, EventArgs e)
        //{

        //    //GridTristate WrapMode GridCellStyle
        //    GridElement ElementsGrid = this.GetVisualElementById<GridElement>("ElementsGrid");
        //    GridTextBoxColumn Grid1Col1 = this.GetVisualElementById<GridTextBoxColumn>("Grid1Col1");
        //    GridTextBoxColumn Grid1Col2 = this.GetVisualElementById<GridTextBoxColumn>("Grid1Col2");

        //    ElementsGrid.DefaultCellStyle.WrapMode = GridTristate.True;

        //    Grid1Col1.AutoSizeMode = GridAutoSizeColumnMode.AllCells;
        //    Grid1Col1.DefaultCellStyle.WrapMode = GridTristate.True;

        //    Grid1Col2.AutoSizeMode = GridAutoSizeColumnMode.AllCells;
        //    Grid1Col2.DefaultCellStyle.WrapMode = GridTristate.True;

        //    //ElementsGrid.Columns[1].AutoSizeMode = GridAutoSizeColumnMode.DisplayedCells;
        //    //ElementsGrid.Columns[1].DefaultCellStyle.WrapMode = GridTristate.True;
        //    //ElementsGrid.Columns[0].AutoSizeMode = GridAutoSizeColumnMode.DisplayedCells;
        //    //ElementsGrid.Columns[0].DefaultCellStyle.WrapMode = GridTristate.True;
           
            
        //    ElementsGrid.AutoSize = true;
        //    ElementsGrid.Rows.Add("0", "Digit, required. This element will accept any single digit between 0 and 9.");
        //    ElementsGrid.Rows.Add("9", "Digit or space, optional.");
        //    ElementsGrid.Rows.Add("#", "Digit or space, optional. If this position is blank in the mask, it will be rendered as a space in the Text property.Plus (+) and minus (-) signs are allowed.");
        //    ElementsGrid.Rows.Add("L", "Letter, required. Restricts input to the ASCII letters a-z and A-Z. This mask element is equivalent to [a-zA-Z] in regular expressions.");
        //    ElementsGrid.Rows.Add("?", "Letter, optional. Restricts input to the ASCII letters a-z and A-Z. This mask element is equivalent to [a-zA-Z]? in regular expressions.");
        //    ElementsGrid.Rows.Add("&", "Character, required. If the AsciiOnly property is set to true, this element behaves like the 'L' element.");
        //    ElementsGrid.Rows.Add("C", "Character, optional. Any non-control character. If the AsciiOnly property is set to true, this element behaves like the '?' element.");
        //    ElementsGrid.Rows.Add("A", "Alphanumeric, required. If the AsciiOnly property is set to true, the only characters it will accept are the ASCII letters a-z and A-Z. This mask element behaves like the 'a' element.");
        //    ElementsGrid.Rows.Add("a", "Alphanumeric, optional. If the AsciiOnly property is set to true, the only characters it will accept are the ASCII letters a-z and A-Z. This mask element behaves like the 'A' element.");
        //    ElementsGrid.Rows.Add(".", "Decimal placeholder. The actual display character used will be the decimal symbol appropriate to the format provider, as determined by the control's FormatProvider property.");
        //    ElementsGrid.Rows.Add(",", "Thousands placeholder. The actual display character used will be the thousands placeholder appropriate to the format provider, as determined by the control's FormatProvider property.");
        //    ElementsGrid.Rows.Add(":", "Time separator. The actual display character used will be the time symbol appropriate to the format provider, as determined by the control's FormatProvider property.");
        //    ElementsGrid.Rows.Add("/", "Date separator. The actual display character used will be the date symbol appropriate to the format provider, as determined by the control's FormatProvider property.");
        //    ElementsGrid.Rows.Add("$", "Currency symbol. The actual character displayed will be the currency symbol appropriate to the format provider, as determined by the control's FormatProvider property.");
        //    ElementsGrid.Rows.Add("<", "Shift down. Converts all characters that follow to lowercase.");
        //    ElementsGrid.Rows.Add(">", "Shift up. Converts all characters that follow to uppercase.");
        //    ElementsGrid.Rows.Add("|", "Disable a previous shift up or shift down.");
        //    ElementsGrid.Rows.Add("\\", "Escape. Escapes a mask character, turning it into a literal. '\\' is the escape sequence for a backslash.");
        //    ElementsGrid.Rows.Add("All other characters", "Literals. All non-mask elements will appear as themselves within MaskedTextBox. Literals always occupy a static position in the mask at run time, and cannot be moved or deleted by the user.");
        //    ElementsGrid.Rows[3].DefaultCellStyle.WrapMode = GridTristate.True;
        //}
    }
}