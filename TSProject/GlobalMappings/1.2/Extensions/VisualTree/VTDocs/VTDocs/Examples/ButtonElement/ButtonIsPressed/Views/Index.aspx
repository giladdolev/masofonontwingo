<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button IsPressed property" ID="windowView" LoadAction="ButtonIsPressed\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="IsPressed" Top="15px" ID="lblTitle" Left="300px">
        </vt:Label>
        <vt:Label runat="server" Text="Sets or Gets the pressed state of the button.
                       
            syntax : public bool IsPressed { get; set; }

            If 'EnableToggle' is set to true the 'IsPressed' property becomes relevant. When the button is clicked, 
            the 'IsPressed' property will change it's state and the 'PressedChange' event will be invoked. 
            The default value is false."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Button default -->
        <vt:Button runat="server" SkinID="Wide" EnableToggle="true" Text="Button" Top="230px" ID="TestedButton1" PressedChangeAction="ButtonIsPressed\TestedButton1_PressedChange"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="280px" Width="350px" Height="40px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="345px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the initial 'IsPressed' property value of 'TestedButton' is set to true.             
            You can change the 'IsPressed' property value by clicking on 'TestedButton' or 'Change IsPressed value'
             buttons. (the 'EnableToggle' property value is set to true)" 
            Top="395px" ID="Label2">
        </vt:Label>

        <!-- Tested CheckButtons -->
        <vt:Button runat="server" EnableToggle="true" IsPressed="true" SkinID="Wide" Text="TestedButton" Top="490px" ID="TestedButton2" PressedChangeAction="ButtonIsPressed\TestedButton2_PressedChange"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="540px" Width="330px" Height="40px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Toggle IsPressed value >>" Top="600px" Width="180px" ID="btnChangeIsPressed" ClickAction="ButtonIsPressed\btnToggleIsPressed_Click"></vt:Button>
        
    </vt:WindowView>

</asp:Content>
