﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="AutoScroll" Height="800px" Width="1200px">
        
         <vt:Label runat="server" SkinID="SubTitle" Text="AutoScroll" Top="15px" ID="lblTitle" Left="300" Font-Size="19pt" Font-Bold="true">
        </vt:Label>
        <vt:Label runat="server" Text="How to use AutoScroll ,initialize ."
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:CheckedListBox runat="server" ID="CheckedListBox1" Width="200px" Height="100px" AutoScroll="true" Top="150" Left="200">
            <Items>
                <vt:ListItem runat="server" ID="check1" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem1" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem2" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem3" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem4" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem5" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem6" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem7" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem8" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem9" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem10" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem11" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem12" Text="checkbox 1"></vt:ListItem>
            </Items>
        </vt:CheckedListBox>
       
          <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Font-Size="13pt" Top="350px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="AutoScroll = true  " Top="380px" ID="Label2">
        </vt:Label>
    </vt:WindowView>

</asp:Content>
