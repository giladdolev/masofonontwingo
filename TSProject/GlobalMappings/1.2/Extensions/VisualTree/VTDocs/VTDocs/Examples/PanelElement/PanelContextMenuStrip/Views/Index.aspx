<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel ContextMenuStrip Property" ID="windowView"  LoadAction="PanelContextMenuStrip\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ContextMenuStrip" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the ContextMenuStrip associated with this control.
            
            Syntax:  public ContextMenuStripElement ContextMenuStrip { get; set; }
            Property Value: The ContextMenuStrip for this control, or null if there is no ContextMenuStrip.
            The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Panel runat="server" Top="190px" ID="TestedPanel1" CssClass="vt-test-pnl-1" Height="70px" Width="220px"></vt:Panel>  

        <vt:Label runat="server" SkinID="Log" Width="400px" Top="275px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="315px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ContextMenuStrip property of this 'TestedPanel' is initially set to 
           'contextMenuStrip1' with menu item 'Hello'. Use the buttons below to set TestesPanel 
            ContextMenuStrip property to a different value" Top="365px"  ID="lblExp1"></vt:Label>
             
        <vt:Panel runat="server" Top="450px" ID="TestedPanel2" CssClass="vt-test-pnl-2" Height="70px" Width="220px" ContextMenuStripID ="contextMenuStrip1"></vt:Panel>  

        <vt:Label runat="server" SkinID="Log" Top="535px" Width="400px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="contextMenuStrip1" Top="595px"  ID="btnChangToContextMenuStrip1" ClickAction="PanelContextMenuStrip\btnChangeToContextMenuStrip1_Click"></vt:Button>

        <vt:Button runat="server" Text="contextMenuStrip2" Top="595px" Left="270px" ID="btnChangToContextMenuStrip2" ClickAction="PanelContextMenuStrip\btnChangeToContextMenuStrip2_Click"></vt:Button>

        <vt:Button runat="server" Text="Reset" Top="595px" Left="460px" ID="btnReset" ClickAction="PanelContextMenuStrip\btnReset_Click"></vt:Button>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip1" >
            <vt:ToolBarMenuItem runat ="server" ID="ToolBarMenuItem1" Text="Hello"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip2" >
            <vt:ToolBarMenuItem runat ="server" ID="toolBarMenuItem2" Text="Hello World"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

    </vt:WindowView>
</asp:Content>

