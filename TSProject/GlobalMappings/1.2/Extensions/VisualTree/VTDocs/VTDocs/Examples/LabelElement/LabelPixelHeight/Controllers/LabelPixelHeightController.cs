using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelPixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelHeight value: " + TestedLabel.PixelHeight + '.';
        }

        private void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedLabel.PixelHeight == 80)
            {
                TestedLabel.PixelHeight = 18;
                lblLog.Text = "PixelHeight value: " + TestedLabel.PixelHeight + '.';
            }
            else
            {
                TestedLabel.PixelHeight = 80;
                lblLog.Text = "PixelHeight value: " + TestedLabel.PixelHeight + '.';
            }
        }
    }
}