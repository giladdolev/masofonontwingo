using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

             TestedGrid.Rows.Add("a", "b", "c");
             TestedGrid.Rows.Add("d", "e", "f");
             TestedGrid.Rows.Add("g", "h", "i");

             lblLog.Text = "Top Value: " + TestedGrid.Top + '.';

         }
         public void btnChangeTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
             if (TestedGrid.Top == 285)
             {
                 TestedGrid.Top = 300;
                 lblLog.Text = "Top Value: " + TestedGrid.Top + '.';

             }
             else
             {
                 TestedGrid.Top = 285;
                 lblLog.Text = "Top Value: " + TestedGrid.Top + '.';
             }

         }

    }
}