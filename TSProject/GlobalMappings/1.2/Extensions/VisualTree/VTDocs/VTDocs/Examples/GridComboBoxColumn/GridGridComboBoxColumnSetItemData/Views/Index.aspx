﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridComboBoxColumn SetItemData"  Top="57px" ID="windowView1"  LoadAction="GridGridComboBoxColumnSetItemData\OnLoad">
       
         <vt:Label runat="server" SkinID="Title" Text="SetItemData" Left="250px" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Sets an integer data stored for each item in a ComboBox
            
            Syntax: public void SetItemData(int index, int value)"
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Each line in the Grid bellow displays information about a pupil. 
            The first TextBox Column presents the pupil name. The Second ComboBox Column provides gender 
            options and also stores their g-code as an ItemData (1-male, 2-female). 
            The third ComboBox Column provides class options and also stores their c-code as an ItemData. 
            The last CheckBox column indicates success or failure. You can use the comboboxes and buttons below 
            to display a pupil information or to change a c-code." 
            Top="215px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Height="130px" Top="360px" Width="350px" ID="TestedGrid">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="col1" HeaderText="Name" Height="20px" Width="75px" ></vt:GridTextBoxColumn>
                <vt:GridComboBoxColumn  runat="server" ID="col2" HeaderText="Gender" Height="20px" Width="75px"></vt:GridComboBoxColumn>
                <vt:GridComboBoxColumn  runat="server" ID="col3" HeaderText="Class" Height="20px" Width="75px"></vt:GridComboBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="col4" HeaderText="Passed" Height="20px" Width="75px"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="505px" Width="350px" Height="40px" ID="lblLog" Text ="Select name or class and click the button.."></vt:Label>

        <vt:ComboBox runat="server" Width="155px" CssClass="vt-cmbSelectPupil" Text="Select Pupil" Top="580px" ID="cmbSelectPupil" SelectedIndexChangedAction="GridGridComboBoxColumnSetItemData\cmbSelectPupil_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="Eli"></vt:ListItem>
                <vt:ListItem runat="server" Text="Sara"></vt:ListItem>
                <vt:ListItem runat="server" Text="Yaakov"></vt:ListItem>
            </Items>
        </vt:ComboBox>
                
        <vt:Button runat="server" Text="Get ItemData >>" left="270px" Width="200px" Top="580px" ID="btnGetItemData" ClickAction="GridGridComboBoxColumnSetItemData\btnGetItemData_Click"></vt:Button>    

        <vt:ComboBox runat="server" Width="155px" CssClass="vt-cmbSelectClass" Text="Select Class" Top="630px" ID="cmbSelectClass" SelectedIndexChangedAction="GridGridComboBoxColumnSetItemData\cmbSelectClass_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="Math"></vt:ListItem>
                <vt:ListItem runat="server" Text="English"></vt:ListItem>
                <vt:ListItem runat="server" Text="History"></vt:ListItem>
                <vt:ListItem runat="server" Text="Chemistry"></vt:ListItem>
                <vt:ListItem runat="server" Text="Biology"></vt:ListItem>
            </Items>
        </vt:ComboBox>

         <vt:TextBox runat="server" Width="155px" CssClass="vt-tbClassCode" Top="630px" left="270px" ID="ClassCode"></vt:TextBox>

        <vt:Button runat="server" Text="Set ItemData >>" left="460px" Width="200px" Top="630px" ID="btnSetItemData" ClickAction="GridGridComboBoxColumnSetItemData\btnSetItemData_Click"></vt:Button>    


    </vt:WindowView>
</asp:Content>
