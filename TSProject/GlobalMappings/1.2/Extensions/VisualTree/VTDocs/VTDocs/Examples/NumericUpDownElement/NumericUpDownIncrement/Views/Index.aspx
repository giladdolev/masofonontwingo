<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Increment Property" Top="57px" Left="11px" ID="windowView1" Height="700px" Width="768px">

        <vt:Label runat="server" Text="Increment is initially set to 10" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:NumericUpDown runat="server" Increment="10" Top="97px" Left="250px" ID="numericUpDown1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="20px" Width="100px">
        </vt:NumericUpDown>

        <vt:Button runat="server" Text="Change Increment" Top="70px" Left="20px" ID="btnChangeIncrement" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="NumericUpDownIncrement\btnChangeIncrement_Click"></vt:Button>

        <vt:Button runat="server" Text="Get Increment" Top="110px" Left="20px" ID="btnGetIncrement" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="NumericUpDownIncrement\btnGetIncrementValue_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
