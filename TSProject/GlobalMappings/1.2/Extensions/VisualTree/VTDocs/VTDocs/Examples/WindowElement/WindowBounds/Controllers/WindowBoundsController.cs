using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowBoundsController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowBounds());
        }
        private WindowBounds ViewModel
        {
            get { return this.GetRootVisualElement() as WindowBounds; }
        }

        public void btnChangeBounds_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.Bounds == new Rectangle(11, 57, 700, 800))
			{
                this.ViewModel.Bounds = new Rectangle(0, 100, 1000, 600);
                textBox1.Text = this.ViewModel.Bounds.ToString();
			}
			else
            {
                this.ViewModel.Bounds = new Rectangle(11, 57, 700, 800);
                textBox1.Text = this.ViewModel.Bounds.ToString();
            }
                
		}

    }
}