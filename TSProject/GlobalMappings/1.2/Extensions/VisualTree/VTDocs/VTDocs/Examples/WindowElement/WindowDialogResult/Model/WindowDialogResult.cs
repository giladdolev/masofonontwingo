﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowDialogResult : WindowElement
    {

        WindowDialog _frmDialog;

        public WindowDialogResult()
        {

        }

        public WindowDialog frmDialog
        {
            get { return this._frmDialog; }
            set { this._frmDialog = value; }
        }
    }
}