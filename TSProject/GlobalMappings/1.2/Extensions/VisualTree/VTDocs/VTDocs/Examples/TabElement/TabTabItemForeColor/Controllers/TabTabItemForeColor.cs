using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //TabAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "tabPage1 ForeColor value: " + TestedTab1.TabItems[0].ForeColor + "\\r\\ntabPage2 ForeColor value: " + TestedTab1.TabItems[1].ForeColor + "\\r\\ntabPage3 ForeColor value: " + TestedTab1.TabItems[2].ForeColor;
            lblLog2.Text = "tabPage4 ForeColor value: " + TestedTab2.TabItems[0].ForeColor + "\\r\\ntabPage5 ForeColor value: " + TestedTab2.TabItems[1].ForeColor + "\\r\\ntabPage6 ForeColor value: " + TestedTab2.TabItems[2].ForeColor;
        }

        public void btnChangeTabPage4ForeColor_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTab2.TabItems[0].ForeColor == Color.Green)
            {
                TestedTab2.TabItems[0].ForeColor = Color.Blue;
                lblLog2.Text = "tabPage4 ForeColor value: " + TestedTab2.TabItems[0].ForeColor + "\\r\\ntabPage5 ForeColor value: " + TestedTab2.TabItems[1].ForeColor + "\\r\\ntabPage6 ForeColor value: " + TestedTab2.TabItems[2].ForeColor;
            }
            else
            {
                TestedTab2.TabItems[0].ForeColor = Color.Green;
                lblLog2.Text = "tabPage4 ForeColor value: " + TestedTab2.TabItems[0].ForeColor + "\\r\\ntabPage5 ForeColor value: " + TestedTab2.TabItems[1].ForeColor + "\\r\\ntabPage6 ForeColor value: " + TestedTab2.TabItems[2].ForeColor;
            }
        }

        public void btnChangeTabPage5ForeColor_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTab2.TabItems[1].ForeColor == Color.Green)
            {
                TestedTab2.TabItems[1].ForeColor = Color.Blue;
                lblLog2.Text = "tabPage4 ForeColor value: " + TestedTab2.TabItems[0].ForeColor + "\\r\\ntabPage5 ForeColor value: " + TestedTab2.TabItems[1].ForeColor + "\\r\\ntabPage6 ForeColor value: " + TestedTab2.TabItems[2].ForeColor;
            }
            else
            {
                TestedTab2.TabItems[1].ForeColor = Color.Green;
                lblLog2.Text = "tabPage4 ForeColor value: " + TestedTab2.TabItems[0].ForeColor + "\\r\\ntabPage5 ForeColor value: " + TestedTab2.TabItems[1].ForeColor + "\\r\\ntabPage6 ForeColor value: " + TestedTab2.TabItems[2].ForeColor;
            }
        }

        public void btnChangeTabPage6ForeColor_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTab2.TabItems[2].ForeColor == Color.Green)
            {
                TestedTab2.TabItems[2].ForeColor = Color.Blue;
                lblLog2.Text = "tabPage4 ForeColor value: " + TestedTab2.TabItems[0].ForeColor + "\\r\\ntabPage5 ForeColor value: " + TestedTab2.TabItems[1].ForeColor + "\\r\\ntabPage6 ForeColor value: " + TestedTab2.TabItems[2].ForeColor;
            }
            else
            {
                TestedTab2.TabItems[2].ForeColor = Color.Green;
                lblLog2.Text = "tabPage4 ForeColor value: " + TestedTab2.TabItems[0].ForeColor + "\\r\\ntabPage5 ForeColor value: " + TestedTab2.TabItems[1].ForeColor + "\\r\\ntabPage6 ForeColor value: " + TestedTab2.TabItems[2].ForeColor;
            }
        }
    }
}