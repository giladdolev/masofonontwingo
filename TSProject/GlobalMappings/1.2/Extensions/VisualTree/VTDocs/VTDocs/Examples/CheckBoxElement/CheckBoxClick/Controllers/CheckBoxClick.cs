using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Press the CheckBoxs...";
        }

        public void btnCheckBox1_Click(object sender, EventArgs e)
        {
            CheckBoxElement btnCheckBox1 = this.GetVisualElementById<CheckBoxElement>("btnCheckBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "CheckBox1 was clicked";
            txtEventLog.Text += "\\r\\nCheckBox: CheckBox1, Click event is invoked";

            if (btnCheckBox1.BackColor == Color.Orange)
            {
                btnCheckBox1.BackColor = Color.Green;
                lblLog.Text += "\\r\\nBackColor value: " + btnCheckBox1.BackColor.ToString();
            }
            else
            {
                btnCheckBox1.BackColor = Color.Orange;
                lblLog.Text += "\\r\\nBackColor value: " + btnCheckBox1.BackColor.ToString();
            }
        }

        public void btnCheckBox2_Click(object sender, EventArgs e)
        {
            CheckBoxElement btnCheckBox2 = this.GetVisualElementById<CheckBoxElement>("btnCheckBox2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "CheckBox2 was clicked";
            txtEventLog.Text += "\\r\\nCheckBox: CheckBox2, Click event is invoked";

            if (btnCheckBox2.BackColor == Color.DeepPink)
            {
                btnCheckBox2.BackColor = Color.Indigo;
                lblLog.Text += "\\r\\nBackColor value: " + btnCheckBox2.BackColor.ToString();
            }
            else
            {
                btnCheckBox2.BackColor = Color.DeepPink;
                lblLog.Text += "\\r\\nBackColor value: " + btnCheckBox2.BackColor.ToString();
            }
        }

        public void btnCheckBox3_Click(object sender, EventArgs e)
        {
            CheckBoxElement btnCheckBox3 = this.GetVisualElementById<CheckBoxElement>("btnCheckBox3");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "CheckBox3 was clicked";
            txtEventLog.Text += "\\r\\nCheckBox: CheckBox3, Click event is invoked";

            if (btnCheckBox3.BackColor == Color.Red)
            {
                btnCheckBox3.BackColor = Color.Blue;
                lblLog.Text += "\\r\\nBackColor value: " + btnCheckBox3.BackColor.ToString();
            }
            else
            {
                btnCheckBox3.BackColor = Color.Red;
                lblLog.Text += "\\r\\nBackColor value: " + btnCheckBox3.BackColor.ToString();
            }
        }


    }
}