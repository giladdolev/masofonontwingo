using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPerformClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformClick_Click(object sender, EventArgs e)
        {

            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");


            TestedGroupBox.PerformClick(e);
        }

        public void TestedGroupBox_Click(object sender, EventArgs e)
        {

            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");

            MessageBox.Show("TestedGroupBox Click event method is invoked");
        }

    }
}