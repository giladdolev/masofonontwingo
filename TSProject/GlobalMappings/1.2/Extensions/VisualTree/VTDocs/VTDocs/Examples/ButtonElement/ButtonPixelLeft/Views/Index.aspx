<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button PixelLeft Property" ID="windowView2" LoadAction="ButtonPixelLeft\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelLeft" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the Left edge 
            of the control and the Left edge of its container's client area.
            
            Syntax: public int PixelLeft { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Left property of this Button is initially set to 80." Top="300px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Top="350px" ID="TestedButton"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change PixelLeft Value >>" Top="475px" ID="btnChangePixelLeft" ClickAction="ButtonPixelLeft\btnChangePixelLeft_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>