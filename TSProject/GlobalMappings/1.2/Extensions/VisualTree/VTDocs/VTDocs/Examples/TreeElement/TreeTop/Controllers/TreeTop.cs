using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             lblLog.Text = "Top Value: " + TestedTree.Top + '.';

         }
         public void btnChangeTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
             if (TestedTree.Top == 285)
             {
                 TestedTree.Top = 300;
                 lblLog.Text = "Top Value: " + TestedTree.Top + '.';

             }
             else
             {
                 TestedTree.Top = 285;
                 lblLog.Text = "Top Value: " + TestedTree.Top + '.';
             }

         }

    }
}