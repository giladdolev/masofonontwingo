<%--<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SelectedIndex changed Example" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px" >
        
         <vt:Label runat="server" Text="Tab SelectedIndex is set to 3" Top="130px" Left="300px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 
        
        <vt:Tab runat="server"  SelectedIndex ="3" Top="180px" Left="230px" ID="tabControl1" Height="100px" Width="500" SelectedIndexChangedAction="TabSelectedIndexChanged\tabSelectedIndex_SelectedIndexChanged">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="lbl1" Text="Tab Item 1"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px">
                     <vt:Label runat="server" ID="Label2" Text="Tab Item 2"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px">
                     <vt:Label runat="server" ID="Label3" Text="Tab Item 3"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage4" Top="22px" Left="4px" ID="tabPage4" Height="74px" Width="192px">
                     <vt:Label runat="server" ID="Label4" Text="Tab Item 4"></vt:Label>
                </vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Button runat="server" Text="Change selectedIndex" Top="180px" Left="10px" ClickAction="TabSelectedIndexChanged\ChangeSelectedIndex_Click" ID="ChangeTabsText" Height="23px" Width="170px"></vt:Button>
        <vt:TextBox runat="server" Text="" Top="180px" Left="185px"  ID="textBox1" Height="23px" MaxLength="1" Width="30px"></vt:TextBox>
        <vt:TextBox runat="server" Text="" Top="300px" Left="350px"  ID="txtExplanation" Multiline="true"  Height="40px" Width="250px"> 
            </vt:TextBox>

        <vt:Label runat="server" Text="Event Log" Top="400px" Left="350px"  ID="label5"   Height="15px" Width="200px"> 
            </vt:Label>

        <vt:TextBox runat="server" Text="" Top="420px" Left="350px"  ID="txtEventLog" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>--%>


<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab SelectedIndexChanged Event" ID="windowView1" LoadAction="TabSelectedIndexChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectedIndexChanged" ID="Label1" Left="250px"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the SelectedIndex property has changed.
            
            Syntax: public event EventHandler SelectedIndexChanged"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can Select different Tabs using the selection buttons below 
            or by manually selecting a different tab.
            Each invocation of the 'SelectedIndexChanged' event should add a line in the 'Event Log'."
            Top="225px" ID="Label4">
        </vt:Label>
         <!-- TestedTab -->
        <vt:Tab runat="server" Text="TestedTab" Top="295px" ID="TestedTab" SelectedIndexChangedAction="TabSelectedIndexChanged/TestedTab_SelectedIndexChanged" >
            <TabItems>
                <vt:TabItem runat="server" Text="tabItem1" Top="22px" Left="4px" ID="tabItem1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem2" Top="22px" Left="4px" ID="tabItem2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem3" Top="22px" Left="4px" ID="tabItem3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Width="400px" ID="lblLog" Top="415px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="450px" Width="400px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select tabItem1 >>" Top="620px" width="130px" Left="80px" ID="btnSelecttabItem1" TabIndex="1" ClickAction="TabSelectedIndexChanged\btnSelecttabItem1_Click"></vt:Button>   
        <vt:Button runat="server" Text="Select tabItem2 >>" Top="620px" width="130px" Left="235px" ID="btnSelecttabItem2" TabIndex="2" ClickAction="TabSelectedIndexChanged\btnSelecttabItem2_Click"></vt:Button>  
        <vt:Button runat="server" Text="Select tabItem3 >>" Top="620px" width="130px" Left="390px" ID="btnSelecttabItem3" TabIndex="3" ClickAction="TabSelectedIndexChanged\btnSelecttabItem3_Click"></vt:Button>  
        <vt:Button runat="server" Text="Clear Event Log >>" Top="620px" width="130px" Left="545px" ID="btnClearEventLog" TabIndex="3" ClickAction="TabSelectedIndexChanged\btnClearEventLog_Click"></vt:Button>  


    </vt:WindowView>
</asp:Content>

        