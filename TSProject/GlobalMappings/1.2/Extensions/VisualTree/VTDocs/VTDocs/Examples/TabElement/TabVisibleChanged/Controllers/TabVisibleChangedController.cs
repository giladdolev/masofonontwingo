using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TabAlignment
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabVisible = this.GetVisualElementById<TabElement>("tabVisible");

            if (tabVisible.Visible == true) //Get
            {
                tabVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + tabVisible.Visible;
            }
            else
            {
                tabVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + tabVisible.Visible;
            }
        }

      //  tabText_TextChanged
        public void tabVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}