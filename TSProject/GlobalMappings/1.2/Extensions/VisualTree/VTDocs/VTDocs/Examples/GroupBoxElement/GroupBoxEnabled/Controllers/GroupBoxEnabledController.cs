using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox1 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedGroupBox1.Enabled.ToString();

            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedGroupBox2.Enabled.ToString() + ".";
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGroupBox2.Enabled = !TestedGroupBox2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedGroupBox2.Enabled.ToString() + ".";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ButtonElement button1 = this.GetVisualElementById<ButtonElement>("button1");

            if(button1.BackColor == Color.Yellow)
            {
                button1.BackColor = Color.Green;
            }
            else
            {
                button1.BackColor = Color.Yellow;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonElement button2 = this.GetVisualElementById<ButtonElement>("button2");

            if (button2.BackColor == Color.Yellow)
            {
                button2.BackColor = Color.Green;
            }
            else
            {
                button2.BackColor = Color.Yellow;
            }
        }
    }
}