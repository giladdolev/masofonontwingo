<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Show Method" ID="windowView1" LoadAction="ListViewShow\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Show" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Displays the control to the user.

            Syntax: public void Show()
            
            Showing the control is equivalent to setting the Visible property to 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the Visible property is initially set to 'false'. 
            When invoking Show() method, the Visible property value is changed to 'true'." Top="290px" ID="lblExp2">
        </vt:Label>

        <vt:ListView runat="server" Text="TestedListView" Top="360px" Visible="false" ID="TestedListView">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="475px"></vt:Label>

        <vt:Button runat="server" Text="Show >>" Top="550px" ID="btnShow" ClickAction="ListViewShow\btnShow_Click"></vt:Button>

        <vt:Button runat="server" Text="Hide >>" Left="300" Top="550px" ID="btnHide" ClickAction="ListViewShow\btnHide_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
