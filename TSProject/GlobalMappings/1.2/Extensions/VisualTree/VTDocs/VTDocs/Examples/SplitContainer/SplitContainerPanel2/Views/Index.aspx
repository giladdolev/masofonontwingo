<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Panel2 Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Panel2 is initially set to 'false'" Top="30px" Left="128px" ID="label1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Font-Bold="true" Height="13px" TabIndex="4" Width="56px">
		</vt:Label>
        
        <vt:SplitContainer runat="server" Panel2-Enabled="false" Top="80px" Left="260px" ID="splitContainer1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="173px" TabIndex="5" Width="299px">

			<Panel1 RoundedCorners="False" FloodShowPct="False" AutoSize="True" ClientSize="0, 0" Font="Microsoft Sans Serif, 8.25pt" AllowDrop="False" Bounds="0, 0, 0, 0" AllowColumnReorder="False" CausesValidation="False" ScaleHeight="0" HelpContextID="0" Dock="Left" ForeColor=""  Width="" Height="" Size="0, 0" Top="" Left="" Visible="True" Enabled="True">
			</Panel1>

			<Panel2 RoundedCorners="False" FloodShowPct="False" AutoSize="True" ClientSize="0, 0" Font="Microsoft Sans Serif, 8.25pt" AllowDrop="False" Bounds="0, 0, 0, 0" AllowColumnReorder="False" CausesValidation="False" ScaleHeight="0" HelpContextID="0" Dock="Fill" ForeColor="" Width="" Height="" Size="0, 0" Top="" Left="" Visible="True" Enabled="True">
			</Panel2>
		</vt:SplitContainer>

        <vt:Button runat="server" Text="Change Panel2 Enabled" Top="120px" Left="20px" ID="btnChangePanel2Enabled" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="SplitContainerPanel2\btnChangePanel2Enabled_Click"></vt:Button>

        <vt:Button runat="server" Text="Get Panel2 Enabled" Top="165px" Left="20px" ID="btnGetPanel2Enabled" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="SplitContainerPanel2\btnGetPanel2EnabledValue_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
