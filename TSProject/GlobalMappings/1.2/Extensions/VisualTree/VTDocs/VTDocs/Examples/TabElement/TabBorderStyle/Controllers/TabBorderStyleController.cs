using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TabAlignment
        public void btnChangeBorderStyle_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabRuntimeBorderStyle = this.GetVisualElementById<TabElement>("tabRuntimeBorderStyle");


            if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.None)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Dotted;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Dotted;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Dotted)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Double;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Double;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Double)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Fixed3D;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Fixed3D;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Fixed3D)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.FixedSingle;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.FixedSingle;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.FixedSingle)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Groove;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Groove;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Groove)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Inset;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Inset;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Inset)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Dashed;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Dashed;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Dashed)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.NotSet;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.NotSet;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.NotSet)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Outset;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Outset;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Outset)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Ridge;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Ridge;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Ridge)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.ShadowBox;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.ShadowBox;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.ShadowBox)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Solid;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Solid;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else if (tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle == BorderStyle.Solid)
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.Underline;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.Underline;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            else
            {
                tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle = BorderStyle.None;
                tabRuntimeBorderStyle.TabItems["TabItem2"].BorderStyle = BorderStyle.None;
                textBox1.Text = "Tabpage1: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
                textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBorderStyle.TabItems["TabItem1"].BorderStyle.ToString();
            }
            
        }

    }
}