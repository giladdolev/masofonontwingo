<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox SelectOnFocus Property" ID="windowView1" LoadAction="TextBoxSelectOnFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectOnFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control text is selected when focused

            Property value: 'true' if the entire text will be selected when focused or false if
             only the cursor is shown when focused. The default false.  
            
            Syntax: public bool SelectOnFocus { get; set; }
            
            Notice - this property is not intended to be changed at run time." Top="75px"  ID="lblDefinition"></vt:Label>

        
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="SelectOnFocus property of 'TestedTextBox2' is initially set to 'true'.
            Use the buttons bellow to examine the behavior" Top="330px"  ID="lblExp1"></vt:Label>     
      

         <!--TestedTextBox1-->
        <vt:TextBox runat="server" Text="TestedTextBox1" Top="395px" ID="TestedTextBox1" LostFocusAction="TextBoxSelectOnFocus\TestedTextBox1_LostFocus"></vt:TextBox>  
        
        <vt:Label runat="server" SkinID="Log" Width="225px" Height="50px" Top="436px" ID="lblLog1"></vt:Label>   

        <vt:Button runat="server" Text="Focus TestedTextBox1 >>" Width="225px" Top="521px" ID="btnFocus1" ClickAction="TextBoxSelectOnFocus\btnFocus1_Click"></vt:Button>
 
      
         <!--TestedTextBox2-->
        <vt:TextBox runat="server"  Text="TestedTextBox2" Left="430px" Top="395px" ID="TestedTextBox2" LostFocusAction="TextBoxSelectOnFocus\TestedTextBox2_LostFocus" SelectOnFocus="true"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="436px" Width="225px" Height="50px" Left="430px" ID="lblLog2" ></vt:Label>

        <vt:Button runat="server" Text="Focus TestedTextBox2 >>" Top="521px" Width="225px" Left="430px" ID="btnFocus2" ClickAction="TextBoxSelectOnFocus\btnFocus2_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
