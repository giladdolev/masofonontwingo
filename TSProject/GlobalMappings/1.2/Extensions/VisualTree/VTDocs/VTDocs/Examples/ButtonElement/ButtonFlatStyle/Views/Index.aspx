<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button FlatStyle Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:Button runat="server" Text="FlatStyle is set to 'Popup'" Top="45px" FlatStyle="Popup" Left="140px" ID="FlatStyle" Height="36px"  Width="150px"></vt:Button>    

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Button runat="server" Text="Change Button FlatStyle" Top="115px" Left="140px" ID="btnFlatStyle" Height="36px" Width="150px" ClickAction="ButtonFlatStyle\btnFlatStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
