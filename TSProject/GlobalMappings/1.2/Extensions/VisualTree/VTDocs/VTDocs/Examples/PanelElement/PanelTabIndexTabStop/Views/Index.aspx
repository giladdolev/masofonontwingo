<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel TabStop and TabIndex Properties" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Panel3 TabStop is set to false, Paneles TabIndex are set from left to right" Top="30px" Left="100px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="200px"></vt:Label>

        <vt:Label runat="server" Text="1" Top="60px" Left="30px" ID="Label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>
        
        <vt:Panel runat="server" Text="Panel1" Top="60px" Left="40px" ID="Panel1" Height="50px" TabIndex="1" Width="100px">
            <vt:Button runat="server" Text="button1" Top="10px" Left="10px" ID="button1" Height="20px" Width="50px"></vt:Button>

        </vt:Panel>    

        <vt:Label runat="server" Text="2" Top="60px" Left="150px" ID="Label3" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>
        <vt:Panel runat="server" Text="Panel2" Top="60px" Left="160px" ID="Panel2" Height="50px" TabIndex="2"  Width="100px">
            <vt:Button runat="server" Text="button1" Top="10px" Left="10px" ID="button2" Height="20px" Width="50px"></vt:Button>
        </vt:Panel>

        <vt:Label runat="server" Text="3" Top="60px" Left="270px" ID="Label4" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>
        <vt:Panel runat="server" Text="Panel3" Top="60px" Left="280px" ID="Panel3" Height="50px" TabIndex="3" TabStop="false" Width="100px">
            <vt:Button runat="server" Text="button1" Top="10px" Left="10px" ID="button3" Height="20px" Width="50px"></vt:Button>
        </vt:Panel>    

        <vt:Label runat="server" Text="4" Top="60px" Left="390px" ID="Label5" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>
        <vt:Panel runat="server" Text="Panel4" Top="60px" Left="400px" ID="Panel4" Height="50px"  TabIndex="4" Width="100px">
            <vt:Button runat="server" Text="button1" Top="10px" Left="10px" ID="button4" Height="20px" Width="50px"></vt:Button>
        </vt:Panel>
       
         <vt:Label runat="server" Text="5" Top="60px" Left="510px" ID="Label6" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="5px"></vt:Label>


        <vt:Panel runat="server" Text="Panel5" Top="60px" Left="520px" ID="Panel5" Height="50px" TabIndex="5" Width="100px">
            <vt:Button runat="server" Text="button1" Top="10px" Left="10px" ID="button5" Height="20px" Width="50px"></vt:Button>
        </vt:Panel> 
        
         <vt:button runat="server" Text="Change Panels TabIndex" Top="130px" Left="100px" TabStop ="false" ID="btnChangeTabIndex" Height="36px" Width="210px" ClickAction="PanelTabIndexTabStop\btnChangeTabIndex_Click"></vt:button> 
        
         <vt:button runat="server" Text="Change Panel3 TabStop" Top="130px" Left="340px" ID="btnChangeTabStop" Height="36px"  TabStop ="false" Width="210px" ClickAction="PanelTabIndexTabStop\btnChangeTabStop_Click"></vt:button>    


        <vt:TextBox runat="server" Text="" Top="200px" Left="130px"  ID="textBox1" Multiline="true"  Height="80px" Width="170px"> 
            </vt:TextBox>

        <vt:TextBox runat="server" Text="" Top="200px" Left="340px"  ID="textBox2" Multiline="true"  Height="80px" Width="170px"> 
            </vt:TextBox>

     </vt:WindowView>
</asp:Content>
