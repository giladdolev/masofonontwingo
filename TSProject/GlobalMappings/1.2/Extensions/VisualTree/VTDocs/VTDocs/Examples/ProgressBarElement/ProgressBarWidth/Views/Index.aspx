<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Width Property" ID="windowView1" LoadAction="ProgressBarWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="Width" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels.
            
            Syntax: public int Width { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this ProgressBar is initially set to: 250px" Top="235px" ID="lblExp1"></vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="295px" ID="TestedProgressBar" TabIndex="3"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Width value >>" Top="415px" ID="btnChangeWidth" TabIndex="1" Width="180px" ClickAction="ProgressBarWidth\btnChangeWidth_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
