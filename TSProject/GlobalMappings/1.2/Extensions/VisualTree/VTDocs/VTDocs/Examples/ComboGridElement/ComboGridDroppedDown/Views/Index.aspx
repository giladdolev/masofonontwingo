<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid DroppedDown Property" ID="windowView2"  LoadAction="ComboGridDroppedDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="DroppedDown" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the combogrid is displaying its drop-down portion.

            Syntax: public bool DroppedDown { get; set; }
            
            Value: true if the drop-down portion is displayed; otherwise, false. The default is false."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboGrid runat="server" Text="ComboGrid" Top="180px" ID="TestedComboGrid1"></vt:ComboGrid>
        
        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="275px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="The DroppedDown property doesn't have initial value.
             You can click on the button to toggle the value at runtime." Top="325px" ID="lblExp1"></vt:Label>

        <vt:ComboGrid runat="server" Text="TestedComboGrid" Top="390px" ID="TestedComboGrid2" ></vt:ComboGrid>
        
        <vt:Label runat="server" SkinID="Log" Top="555px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change DroppedDown value >>" Top="605px" ID="btnChangeDroppedDown" Width="200px" ClickAction ="ComboGridDroppedDown\btnChangeDroppedDown_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        