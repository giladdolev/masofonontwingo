using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.IO;


namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewColumnHeaderCollectionAddInt2StringController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "Press on one of the buttons...";
        }

        //Adds one item with text and image on each clicking
        public void btnAddColumnHeader_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            ColumnHeader res = TestedListView.Columns.Add(0, "New Column", "New Column");
            
            TestedListView.Refresh();
            lblLog1.Text = "A New Column was added to the ListView at index " + res.Index + ".";        
        }


        public void btnRemoveColumnHeader_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedListView.Columns.Count > 0)
            {
                int i = TestedListView.Columns.Count - 1;
                TestedListView.Columns.RemoveAt(i);
                TestedListView.Refresh();

                lblLog1.Text = "A Column was removed from the ListView at index " + i + ".";
            }
        }

    }
} 