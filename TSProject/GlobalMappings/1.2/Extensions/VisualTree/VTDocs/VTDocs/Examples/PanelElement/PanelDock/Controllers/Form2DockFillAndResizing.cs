using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class Form2DockFillAndResizingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index2()
        {
            return View(new Form2DockFillAndResizing());
        }
        private Form2DockFillAndResizing ViewModel
        {
            get { return this.GetRootVisualElement() as Form2DockFillAndResizing; }
        }

        //Resize Form2
        public void btnDockFillForm2_Click(object sender, EventArgs e)
        {
            PanelElement pnlDock = this.GetVisualElementById<PanelElement>("pnlDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (pnlDock.Dock == Dock.Bottom)
            {
                pnlDock.Dock = Dock.Fill;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Fill)
            {
                pnlDock.Dock = Dock.Left;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Left)
            {
                pnlDock.Dock = Dock.Right;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Right)
            {
                pnlDock.Dock = Dock.Top;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Top)
            {
                pnlDock.Dock = Dock.None;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.None)
            {
                pnlDock.Dock = Dock.Bottom;
                textBox1.Text = pnlDock.Dock.ToString();
            }
        }

       
    }
}