using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ListView MaximumSize value: " + TestedListView.MaximumSize + "\\r\\nListView Size value: " + TestedListView.Size;
        }

        public void btnChangeLvwMaximumSize_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedListView.MaximumSize == new Size(280, 80))
            {
                TestedListView.MaximumSize = new Size(310, 50);
                lblLog1.Text = "ListView MaximumSize value: " + TestedListView.MaximumSize + "\\r\\nListView Size value: " + TestedListView.Size;
            }
            else if (TestedListView.MaximumSize == new Size(310, 50))
            {
                TestedListView.MaximumSize = new Size(350, 100);
                lblLog1.Text = "ListView MaximumSize value: " + TestedListView.MaximumSize + "\\r\\nListView Size value: " + TestedListView.Size;
            }
            else
            {
                TestedListView.MaximumSize = new Size(280, 80);
                lblLog1.Text = "ListView MaximumSize value: " + TestedListView.MaximumSize + "\\r\\nListView Size value: " + TestedListView.Size;
            }

        }

        public void btnSetToZeroLvwMaximumSize_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedListView.MaximumSize = new Size(0, 0);
            lblLog1.Text = "ListView MaximumSize value: " + TestedListView.MaximumSize + "\\r\\nListView Size value: " + TestedListView.Size;

        }


    }
}