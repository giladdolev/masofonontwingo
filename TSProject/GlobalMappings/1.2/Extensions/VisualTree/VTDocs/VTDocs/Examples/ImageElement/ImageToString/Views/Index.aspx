 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image ToString Method" ID="windowView2" LoadAction="ImageToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a string that represents the current Image control.

            Syntax: public override string ToString()
            
            Return Value: A string that includes the type and the SizeMode property of the control." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this Image." 
            Top="265px" ID="lblExp2"></vt:Label>

        <vt:Image runat="server" Text="TestedImage" Top="330px" ID="TestedImage"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="435px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="540px" ID="btnToString" ClickAction="ImageToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>