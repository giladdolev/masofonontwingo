<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Enabled Property" ID="windowView1" LoadAction="TextBoxEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:TextBox runat="server" Text="TextBox" Top="195px" ID="TestedTextBox1"></vt:TextBox>           

        <vt:Label runat="server" SkinID="Log" Top="235px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this TextBox is initially set to 'false'." Top="350px" ID="lblExp1"></vt:Label> 

        <vt:TextBox runat="server" Text="TestedTextBox" Top="410px" Enabled ="false" ID="TestedTextBox2"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="515px" ID="btnChangeEnabled" Width="180px" ClickAction="TextBoxEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
