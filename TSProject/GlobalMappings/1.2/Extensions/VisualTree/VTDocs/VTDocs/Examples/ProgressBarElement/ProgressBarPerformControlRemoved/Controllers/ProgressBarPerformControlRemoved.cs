using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPerformControlRemovedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlRemoved_Click(object sender, EventArgs e)
        {
            
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");

            ControlEventArgs args = new ControlEventArgs(testedProgressBar);

            testedProgressBar.PerformControlRemoved(args);
        }

        public void testedProgressBar_ControlRemoved(object sender, EventArgs e)
        {
            MessageBox.Show("TestedProgressBar ControlRemoved event method is invoked");
        }

    }
}