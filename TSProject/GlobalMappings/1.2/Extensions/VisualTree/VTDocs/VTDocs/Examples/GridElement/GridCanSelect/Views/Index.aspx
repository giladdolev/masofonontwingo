﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="8, 16" Text="Hello World" Margin-All="0" Padding-All="0" ID="Form1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="7.8pt" Height="330px" TabIndex="-1" Width="1000px">

        <vt:Grid runat="server" AutoSize="true" ClientRowTemplate="" ReadOnly="True" AllowUserToAddRows="False" AllowUserToDeleteRows="False" AllowUserToResizeColumns="False" AllowUserToResizeRows="False" ColumnHeadersHeightSizeMode="AutoSize" RowHeadersVisible="True" AutoScroll="True" ToolTipText="" Top="34px" Left="10px" ID="gridElement" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="299px" Width="782px">
            <Columns>
                <vt:GridColumn ID="colNumeroCuota" runat="server" HeaderText="# Cuota" DataMember="colNumeroCuota" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="colTipoCuota" runat="server" HeaderText="Tipo Cuota" DataMember="colTipoCuota" Width="80"></vt:GridColumn>
                <vt:GridColumn ID="colFechaInicio" runat="server" HeaderText="Fecha Inicio" DataMember="colFechaInicio" Width="90"></vt:GridColumn>
                <vt:GridColumn ID="colFechaVencimiento" runat="server" HeaderText="Fecha Vcto" DataMember="colFechaVencimiento" Width="90"></vt:GridColumn>
                <vt:GridColumn ID="colPlazo" runat="server" HeaderText="Plazo" DataMember="colPlazo" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="colSaldoCapital" runat="server" HeaderText="Saldo Capital" DataMember="colSaldoCapital" Width="120"></vt:GridColumn>
                <vt:GridColumn ID="colAmortizacion" runat="server" HeaderText="Amortización" DataMember="colAmortizacion" Width="120"></vt:GridColumn>
                <vt:GridColumn ID="colInteres" runat="server" HeaderText="Interes" DataMember="colInteres" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="colSubtotal" runat="server" HeaderText="Sub Total" DataMember="colSubtotal" Width="120"></vt:GridColumn>
                <vt:GridColumn ID="colTotalIgv" runat="server" HeaderText="IGV" DataMember="colTotalIgv" Width="100"></vt:GridColumn>
                <vt:GridColumn ID="colTotalCuota" runat="server" HeaderText="Total Cuota" DataMember="colTotalCuota" Width="120"></vt:GridColumn>
            </Columns>
            
        </vt:Grid>


        <vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Click Me!" Top="155px" Left="820px" Margin-All="0" Padding-All="0" ID="button1" ClickAction="GridCanSelect\button1_Click" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="7.8pt" Height="45px" Width="236px">
        </vt:Button>
        <vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Change" Top="190px" Left="820px" Margin-All="0" Padding-All="0" ID="button2" ClickAction="GridCanSelect\button2_Click" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="7.8pt" Height="45px" Width="236px">
        </vt:Button>
    </vt:WindowView>

</asp:Content>
