using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerTabIndexTabStopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnChangeTabIndex_Click(object sender, EventArgs e)
        {
            SplitContainer btnChangeTabIndex = this.GetVisualElementById<SplitContainer>("btnChangeTabIndex");
            SplitContainer SplitContainer1 = this.GetVisualElementById<SplitContainer>("SplitContainer1");
            SplitContainer SplitContainer2 = this.GetVisualElementById<SplitContainer>("SplitContainer2");
            SplitContainer SplitContainer4 = this.GetVisualElementById<SplitContainer>("SplitContainer4");
            SplitContainer SplitContainer5 = this.GetVisualElementById<SplitContainer>("SplitContainer5");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (SplitContainer1.TabIndex == 1)
            {
                SplitContainer5.TabIndex = 1;
                SplitContainer4.TabIndex = 2;
                SplitContainer2.TabIndex = 4;
                SplitContainer1.TabIndex = 5;

                textBox1.Text = "\\r\\nSplitContainer1 TabIndex: " + SplitContainer1.TabIndex + "\\r\\nSplitContainer2 TabIndex: " + SplitContainer2.TabIndex + "\\r\\nSplitContainer4 TabIndex: " + SplitContainer4.TabIndex + "\\r\\nSplitContainer5 TabIndex: " + SplitContainer5.TabIndex;       

            }
            else
            { 
                SplitContainer1.TabIndex = 1;
                SplitContainer2.TabIndex = 2;
                SplitContainer4.TabIndex = 4;
                SplitContainer5.TabIndex = 5;

                textBox1.Text = "\\r\\nSplitContainer1 TabIndex: " + SplitContainer1.TabIndex + "\\r\\nSplitContainer2 TabIndex: " + SplitContainer2.TabIndex + "\\r\\nSplitContainer4 TabIndex: " + SplitContainer4.TabIndex + "\\r\\nSplitContainer5 TabIndex: " + SplitContainer5.TabIndex;
            }
        }
        private void btnChangeTabStop_Click(object sender, EventArgs e)
        {
            SplitContainer btnChangeTabStop = this.GetVisualElementById<SplitContainer>("btnChangeTabStop");
            SplitContainer SplitContainer3 = this.GetVisualElementById<SplitContainer>("SplitContainer3");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");


            if (SplitContainer3.TabStop == false)
            {
                SplitContainer3.TabStop = true;
                textBox2.Text = "SplitContainer3 TabStop: \\r\\n" + SplitContainer3.TabStop;

            }
            else
            {
                SplitContainer3.TabStop = false;
                textBox2.Text = "SplitContainer3 TabStop: \\r\\n" + SplitContainer3.TabStop;

            }
        }
    }
}