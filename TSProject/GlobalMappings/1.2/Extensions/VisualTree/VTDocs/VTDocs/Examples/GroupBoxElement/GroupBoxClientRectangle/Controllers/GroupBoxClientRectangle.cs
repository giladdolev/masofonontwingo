using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: " + TestedGroupBox.ClientRectangle;

        }
        public void btnChangeGroupBoxBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            if (TestedGroupBox.Left == 120)
            {
                TestedGroupBox.SetBounds(80, 345, 200, 80);
                lblLog.Text = "ClientRectangle value: " + TestedGroupBox.ClientRectangle;

            }
            else
            {
                TestedGroupBox.SetBounds(120, 325, 300, 50);
                lblLog.Text = "ClientRectangle value: " + TestedGroupBox.ClientRectangle;
            }

        }

    }
}