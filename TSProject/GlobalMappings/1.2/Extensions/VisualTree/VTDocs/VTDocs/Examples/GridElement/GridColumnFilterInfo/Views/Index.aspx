﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid ColumnFilterInfo Property" ID="windowView1" LoadAction="GridColumnFilterInfo\View_load">

        <vt:Label runat="server" SkinID="Title" Top="15px" ID="lblTitle" Left="300" Text="ColumnFilterInfo"></vt:Label>

        <vt:Label runat="server" Text="  Gets or sets column filter settings. 
            
            syntax : public FilterInfo ColumnFilterInfo { get; set;}"
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can insert the text box a value to filter the Grid 'Name' column. 
            Clicking the 'Filter By Name' will activate the ColumnFilterInfo Property. " Top="195px" ID="Label1">
        </vt:Label>

        <vt:Grid runat="server" Top="270px" ID="TestedGrid" Height="230px" Width="400px">
            <Columns>
                <vt:GridColumn  runat="server" ID="Name" HeaderText="Name" Height="20px" Width="100px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Email" HeaderText="Email" Height="20px" Width="100px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Number" HeaderText="Number" Height="20px" Width="100px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Width="400px" Top="510"></vt:Label>

         <vt:Label runat="server" Text="Value to filter:" ID="lbl2" Width="150px" Left="80px" Top="560"></vt:Label>

        <vt:TextBox runat="server" Top="560px" ID="TextBoxFilter" Text="..." Left="250px" Height="25px" Width="100px" TextChangedAction="GridColumnFilterInfo\TextBox_TextChanged">
        </vt:TextBox>

       
          <vt:Button runat="server" Text="Filter By Name >>" Top="600px" ID="buttonFilter" ClickAction="GridColumnFilterInfo\buttonFilter_Click" >
        </vt:Button>

        <vt:Button runat="server" Text="Clear Filter >>" Top="600px" Left="250px" ID="buttonClearFilter" ClickAction="GridColumnFilterInfo\buttonClearFilter_Click" >
        </vt:Button>

    </vt:WindowView>

</asp:Content>
