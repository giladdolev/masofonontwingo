using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelDragOverController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void panel1_dragOver(object sender, DragEventArgs e)
        {
            LabelElement lblx = this.GetVisualElementById<LabelElement>("lblx");
            LabelElement lbly = this.GetVisualElementById<LabelElement>("lbly");

            lblx.Text = e.X.ToString();
            lbly.Text = e.Y.ToString();
        }
    }
}