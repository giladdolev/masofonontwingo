﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic MaskedTextBox" Top="57px" ID="windowView1"  LoadAction="BasicMaskedTextBox\OnLoad">
       
         <vt:Label runat="server" SkinID="Title" Text="MaskedTextBox" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Uses a mask to distinguish between proper and improper user input."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example shows basic usage of MaskedTextBox class
             The example includes using Mask and TextMaskFormat properties.

             The Mask property of all 4 MaskedTextBoxes bellow is set to '(AAA) - (000) - (999) - (###)'
            The possible values of MaskedTextFormat are the members of MaskFormat Enumeration: 
            ExcludePromptAndLiterals, IncludeLiterals, IncludePrompt and IncludePromptAndLiterals " Top="205px" ID="lblExp2">
        </vt:Label>
        
        
        <vt:Label runat="server" Text="IncludeLiterals" Left="80px" Font-Size="11pt" Top="380px" ></vt:Label>
        <vt:MaskedTextBox runat="server" ID="maskedTextBox1" CssClass="vt-Msk1" Mask="(AAA) - (000) - (999) - (###)" Top="400px" Left="80px"  Height="20px" Width="180px" TextMaskFormat="IncludeLiterals"
            TextChangedAction="BasicMaskedTextBox\maskedTextBox1_TextChanged" KeyDownAction="BasicMaskedTextBox\keydown"></vt:MaskedTextBox>
        
        <vt:TextBox runat="server" ID="textBox1" Top="400px"  Left="280px" Height="20px" Width="180px"  ></vt:TextBox>
        
        <vt:Label runat="server" Text="ExcludePromptAndLiterals" Left="80px" Font-Size="11pt" Top="435px" ></vt:Label>
         <vt:MaskedTextBox runat="server" ID="maskedTextBox2" CssClass="vt-Msk2" Mask="(AAA) - (000) - (999) - (###)" Top="455px" Left="80px"  Height="20px" Width="180px" TextMaskFormat="ExcludePromptAndLiterals"
            TextChangedAction="BasicMaskedTextBox\maskedTextBox2_TextChanged" ></vt:MaskedTextBox>

        <vt:TextBox runat="server" ID="textBox2" Top="455px"  Left="280px" Height="20px" Width="180px"  ></vt:TextBox>
        
        <vt:Label runat="server" Text="IncludePrompt" Left="80px" Font-Size="11pt" Top="490px" ></vt:Label>
        <vt:MaskedTextBox runat="server" ID="maskedTextBox3" CssClass="vt-Msk3" Mask="(AAA) - (000) - (999) - (###)" Top="510px"  Height="20px" Width="180px" TextMaskFormat="IncludePrompt"
            TextChangedAction="BasicMaskedTextBox\maskedTextBox3_TextChanged" ></vt:MaskedTextBox>

        <vt:TextBox runat="server"  ID="textBox3" Top="510px" Left="280px" Height="20px" Width="180px"  ></vt:TextBox>

        <vt:Label runat="server" Text="IncludePromptAndLiterals" Left="80px" Font-Size="11pt" Top="545px" ></vt:Label>
        <vt:MaskedTextBox runat="server" ID="maskedTextBox4" CssClass="vt-Msk4" Mask="(AAA) - (000) - (999) - (###)" Top="565px"  Height="20px" Width="180px" TextMaskFormat="IncludePromptAndLiterals"
         TextChangedAction="BasicMaskedTextBox\maskedTextBox4_TextChanged" ></vt:MaskedTextBox>

        <vt:TextBox runat="server" ID="textBox4"  Top="565px" CssClass="vt-txt4"  Left="280px" Height="20px" Width="180px"  ></vt:TextBox>
       

    </vt:WindowView>
</asp:Content>
