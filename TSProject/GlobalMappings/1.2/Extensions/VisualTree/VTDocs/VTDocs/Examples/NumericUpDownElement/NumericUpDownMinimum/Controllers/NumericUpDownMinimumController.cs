using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownMinimumController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "The default minimum value: " + "[" + TestedNumericUpDown1.Minimum.ToString() + "]";
            lblLog2.Text = "The minimum value is: " + TestedNumericUpDown2.Minimum + "." + "\\r\\n" + "The maximum value is: " + TestedNumericUpDown2.Maximum + "." + "\\r\\n" + "The value is: " + TestedNumericUpDown2.Value.ToString() + "." + "\\r\\n" + "You can enter a value or change the minimum value.";

        }


        private void btnChangeMinimumValueToMinus15_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedNumericUpDown2.Minimum = -15;

            TestedNumericUpDown2.Value = Convert.ToDecimal(TestedNumericUpDown2.Minimum.ToString());

            lblLog2.Text = "The minimum value is: " + TestedNumericUpDown2.Minimum + "." + "\\r\\n" + "The maximum value is: " + TestedNumericUpDown2.Maximum + "." + "\\r\\n" + "The value is: " + TestedNumericUpDown2.Value.ToString() + ".";

        }

        private void btnChangeMinimumValueTo1_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedNumericUpDown2.Minimum = 1;

            TestedNumericUpDown2.Value = Convert.ToDecimal(TestedNumericUpDown2.Minimum.ToString());

            lblLog2.Text = "The minimum value is: " + TestedNumericUpDown2.Minimum + "." + "\\r\\n" + "The maximum value is: " + TestedNumericUpDown2.Maximum + "." + "\\r\\n" + "The value is: " + TestedNumericUpDown2.Value.ToString() + ".";

        }

        //Change programmatically the value of TestedNumericUpDown2 (value between minimum and maximum range, -10 to 10) 
        private void btnChangeValueTo4_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedNumericUpDown2.Value = 4;

            lblLog2.Text = "The minimum value is: " + TestedNumericUpDown2.Minimum + "." + "\\r\\n" + "The maximum value is: " + TestedNumericUpDown2.Maximum + "." + "\\r\\n" + "The value is: " + TestedNumericUpDown2.Value.ToString() + ".";
        }

        public void TestedNumericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "The value is: " + TestedNumericUpDown2.Value.ToString() + ".";
        }

    }
}