﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="GridComboBoxColumn ReadOnly Property" LoadAction="GridGridComboBoxColumnReadOnly\Page_load">

        <vt:Label runat="server" SkinID="Title" Text="ReadOnly" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the user can edit the cells of the GridColumn control.

           Syntax: public bool ReadOnly { get; set; }
            'true' if the user cannot edit the cells of the GridColumn control; otherwise, 'false'.
            The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the ReadOnly property of Column1 is the default ('false'),
            the ReadOnly property of column2 is initially set to 'true', and the ReadOnly property of column3 
            is initially set to 'false'. You can change the ReadOnly property of the columns by using the 
            button 'Set Column ReadOnly'" Top="255px"  ID="Label1"></vt:Label>

        <vt:Grid runat="server" Top="365px" Width="360px" Height="120px" ID="TestedGrid">
            <Columns>              
                <vt:GridComboBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="100px"></vt:GridComboBoxColumn>
              
                <vt:GridComboBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" ReadOnly="true" Height="20px" Width="100px"></vt:GridComboBoxColumn>

                <vt:GridComboBoxColumn  runat="server" ID="GridCol3" HeaderText="Column3" ReadOnly="false" Height="20px" Width="100px"></vt:GridComboBoxColumn>
            </Columns> 
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="505px" Height="50" Width="400" ID="lblLog1"></vt:Label>

        <vt:ComboBox runat="server" Width="155px" CssClass="vt-cmbSelectColumn" Text="Select Column" Top="605px" ID="cmbSelectColumn" SelectedIndexChangedAction="GridGridComboBoxColumnReadOnly\cmbSelectColumn_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="Column1"></vt:ListItem>
                <vt:ListItem runat="server" Text="Column2"></vt:ListItem>
                <vt:ListItem runat="server" Text="Column3"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:CheckBox runat="server" Top="605px" Left="270px" ID="chkReadOnly" Text="ReadOnly" CheckedChangedAction="GridGridComboBoxColumnReadOnly/checkchange"></vt:CheckBox>

        <vt:Button runat="server" Text="Set Column ReadOnly >>" Left="430px" Width="170px" Top="605px"  ID="btnSetColumnReadOnly" ClickAction="GridGridComboBoxColumnReadOnly\btnSetColumnReadOnly_Click" ></vt:Button>

    </vt:WindowView>


</asp:Content>
