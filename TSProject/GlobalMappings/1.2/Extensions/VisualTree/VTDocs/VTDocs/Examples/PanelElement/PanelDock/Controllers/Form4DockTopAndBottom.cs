using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class Form4DockTopAndBottomController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index4()
        {
            return View(new Form4DockTopAndBottom());
        }
        private Form4DockTopAndBottom ViewModel
        {
            get { return this.GetRootVisualElement() as Form4DockTopAndBottom; }
        }

        //Resize Form2
        public void btnDockTopAndBottomForm4_Click(object sender, EventArgs e)
        {
            PanelElement pnlDock = this.GetVisualElementById<PanelElement>("pnlDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (pnlDock.Dock == Dock.Bottom)
            {
                pnlDock.Dock = Dock.Fill;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Fill)
            {
                pnlDock.Dock = Dock.Left;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Left)
            {
                pnlDock.Dock = Dock.Right;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Right)
            {
                pnlDock.Dock = Dock.Top;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Top)
            {
                pnlDock.Dock = Dock.None;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.None)
            {
                pnlDock.Dock = Dock.Bottom;
                textBox1.Text = pnlDock.Dock.ToString();
            }
        }

    }
}