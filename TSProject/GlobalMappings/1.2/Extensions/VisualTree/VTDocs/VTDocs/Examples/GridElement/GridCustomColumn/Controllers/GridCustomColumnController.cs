using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
using System.ComponentModel;
namespace HelloWorldApp
{
    public class GridCustomColumnController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void BeforeCellRendering(object sender, CellRenderingEventArgs args)
        {
            if (args.RowIndex < 2)
            {
                args.CellType = GridColumnType.CheckBox;
            }
            else if (args.RowIndex < 3)
            {
                args.CellType = GridColumnType.ComboBox;
                args.ListItems = "Moshe|1,David|2";
            }
            else if (args.RowIndex < 4)
            {
                args.CellType = GridColumnType.ComboBox;
                args.ListItems = "Blue|2,Red|3,Green|7";
            }
            else
            {
                args.CellType = GridColumnType.DateTimePicker;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridElement dgvTest = this.GetVisualElementById<GridElement>("dgvTest");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));
            source.Columns.Add("someValue", typeof(string));

            source.Rows.Add("�����#", "lisa@simpsons.com", "True");
            source.Rows.Add("Bart", "Bart@simpsons.com", "False");
            source.Rows.Add("Homer", "Homer@simpsons.com", "1");
            source.Rows.Add("Lisa", "Lisa@simpsons.com", "2");
            source.Rows.Add("Marge", "Marge@simpsons.com", "3");
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", "7");
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", "2");
            dgvTest.DataSource = source;
        }

        private void ColumnAction(object sender, EventArgs e)
        {
        }
        public void button3_Click(object sender, EventArgs e)
        {
            GridElement dgvTest = this.GetVisualElementById<GridElement>("dgvTest");
            // dgvTest.Columns[0].Visible = false;
            //   dgvTest.Refresh();
        }

        public void button2_Click(object sender, EventArgs e)
        {
            GridElement dgvTest = this.GetVisualElementById<GridElement>("dgvTest");

            var parameters = new object[]
                        {
                            "newRow",
                            "newRow",
                             true
                        };
            dgvTest.Rows.Add(parameters);
        }


        public void SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = String.Format("Row {0} selected", e.SelectedRows[0]);

        }
        private void frmFlexColumnsController_CheckedChange(object sender, EventArgs e)
        {
        }


        public void cell_beginEdit(object sender, EventArgs e)
        {
        }
        public void frmFlexColumnsController_beforeCheckedChange(object sender, CancelEventArgs e)
        {
            //e.Cancel = true;
        }

        public void button4_Click(object sender, EventArgs e)
        {
            GridElement dgvTest = this.GetVisualElementById<GridElement>("dgvTest");
            MessageBox.Show("Current indexes =  Row Index  --> " + dgvTest.SelectedRowIndex.ToString() + " , Column index -- > " + dgvTest.SelectedColumnIndex.ToString() + " , Value --> " + dgvTest.Rows[dgvTest.SelectedRowIndex].Cells[dgvTest.SelectedColumnIndex].Value);
        }
    }



}
