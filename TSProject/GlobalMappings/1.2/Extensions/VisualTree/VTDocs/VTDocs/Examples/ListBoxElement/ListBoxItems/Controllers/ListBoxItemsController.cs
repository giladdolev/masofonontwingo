using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListBoxItemsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press one of the buttons...";
        }

        public void RemoveItems_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedListBox.Items.Count > 0)
                TestedListBox.Items.RemoveAt(TestedListBox.Items.Count - 1);

            lblLog.Text = "Item was removed";
        }

        private void AddItems_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListItem newitem = new ListItem();
            newitem.Text = "New Item";
            TestedListBox.Items.Add(newitem);

            lblLog.Text = "Item was added";

        }
    }
} 