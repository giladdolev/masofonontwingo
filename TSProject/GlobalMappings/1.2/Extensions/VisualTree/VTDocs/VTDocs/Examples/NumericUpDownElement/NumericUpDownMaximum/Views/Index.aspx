<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="NumericUpDown Maximum" ID="windowView1" LoadAction="NumericUpDownMaximum\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Maximum" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the maximum value for the spin box (also known as an up-down control). 

            Syntax: public decimal Maximum { get; set; }
            The default value is 100.
                                    
            When the Maximum property is set, the Minimum property is evaluated and the UpdateEditText 
            method is called (this function displays the current value into the control). 
            If the Minimum property is greater than the new Maximum property, the Minimum property value is 
            set equal to the Maximum value. If the current Value is greater 
            than the new Maximum value the Value property value is set equal to the Maximum value. " Top="75px" ID="lblDefinition"></vt:Label>

        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown1" Top="280px" ID="TestedNumericUpDown1" Height="20px" TabIndex="1" Width="200px"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="310px" ID="lblLog1" Width="400px"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the TestedNumericUpDown2 has maximum value:10, minimum value:1.
            You can enter a maximum value using keyboard or using the up and down buttons or using 
            'Change maximum value' buttons. " Top="390px" ID="Label1"></vt:Label>
        
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown2" Top="470px" ID="TestedNumericUpDown2" Height="20px" TabIndex="1" Width="200px" Minimum="1" Maximum="10" ValueChangedAction="NumericUpDownMaximum\TestedNumericUpDown2_ValueChanged" CssClass="vt-TestedNumericUpDown2"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog2" Height="60" Width="400px"></vt:Label>

        <vt:Button runat="server" Text="Change Maximum value to 6 >>" Top="590px" width="180px" Left="300px" ID="btnChangeMaximumValueTo6" ClickAction="NumericUpDownMaximum\btnChangeMaximumValueTo6_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Maximum value to 100 >>" Top="590px" width="200px" ID="btnChangeMaximumValueTo100" ClickAction="NumericUpDownMaximum\btnChangeMaximumValueTo100_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Value to 4 >>" Top="590px" width="200px" Left="500" ID="btnChangeValueTo4" ClickAction="NumericUpDownMaximum\btnChangeValueTo4_Click"></vt:Button>
    
    </vt:WindowView>
</asp:Content>
