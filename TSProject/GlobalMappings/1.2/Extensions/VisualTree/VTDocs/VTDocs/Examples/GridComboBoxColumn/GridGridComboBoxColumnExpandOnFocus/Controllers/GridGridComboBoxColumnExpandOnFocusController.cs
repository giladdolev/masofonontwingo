using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridGridComboBoxColumnExpandOnFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GridAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.AddToComboBoxList("OptionsColumn1", "Option1,Option2,Option3, Option4");
            TestedGrid.AddToComboBoxList("GenderColumn1", "male,female");


            TestedGrid.Rows.Add("Option4", "46203", "male", "Tomer", false);
            TestedGrid.Rows.Add("Option4", "19203", "male", "Yoav", true);
            TestedGrid.Rows.Add("Option2", "94203", "male", "Shalom", false);
            TestedGrid.Rows.Add("Option3", "55203", "female", "Shimrit", true);
            TestedGrid.Rows.Add("Option1", "23203", "male", "Moran", false);

            lblLog.Text = "'Options' column ExpandOnFocus value: " + ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus + '.' + "\\r\\n'Gender' column ExpandOnFocus value: " + ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus + '.';
           
        }

        public void btnChangeOptionsColExpandOnFocus_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus = !((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus;
            lblLog.Text = "Options column ExpandOnFocus value: " + ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus + '.' + "\\r\\nGender column ExpandOnFocus value: " + ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus + '.';
        }

        public void btnChangeGenderColExpandOnFocus_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus = !((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus;
            lblLog.Text = "Options column ExpandOnFocus value: " + ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus + '.' + "\\r\\nGender column ExpandOnFocus value: " + ((GridComboBoxColumn)TestedGrid.Columns[0]).ExpandOnFocus + '.';
        }

    }
}