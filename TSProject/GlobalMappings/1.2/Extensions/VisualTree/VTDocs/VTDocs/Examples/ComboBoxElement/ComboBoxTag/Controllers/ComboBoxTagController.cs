using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
     

        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedComboBox1.Items.Add("Item1");
            TestedComboBox1.Items.Add("Item2");
            TestedComboBox1.Items.Add("Item3");
            if (TestedComboBox1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedComboBox1.Tag;
            }

            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox2.Items.Add("Item1");
            TestedComboBox2.Items.Add("Item2");
            TestedComboBox2.Items.Add("Item3");
            if (TestedComboBox2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedComboBox2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedComboBox2.Tag == "New Tag.")
            {
                TestedComboBox2.Tag = TestedComboBox2;
                lblLog2.Text = "Tag value: " + TestedComboBox2.Tag;
            }
            else
            {
                TestedComboBox2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedComboBox2.Tag;

            }
        }
    }
}