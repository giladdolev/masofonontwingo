using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new CheckBoxCreateControl());

        }

        private CheckBoxCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as CheckBoxCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            CheckBoxElement newCheckBox = new CheckBoxElement();

            newCheckBox.CreateControl();
            newCheckBox.Text = "newCheckBox";

            this.ViewModel.Controls.Add(newCheckBox);
            

            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}