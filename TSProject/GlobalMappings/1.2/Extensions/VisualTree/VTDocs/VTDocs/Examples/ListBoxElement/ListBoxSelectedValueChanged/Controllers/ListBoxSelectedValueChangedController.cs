using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Collections;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListBoxSelectedValueChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            ListBoxElement TestedListBoxDataSourceNoValueMember = this.GetVisualElementById<ListBoxElement>("TestedListBoxDataSourceNoValueMember");
            ListBoxElement TestedListBoxDataSource = this.GetVisualElementById<ListBoxElement>("TestedListBoxDataSource");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedListBox.Items.Add("It3");
            TestedListBox.Items.Add("It4");

            List<string> values1 = new List<string>();
            values1.Add("Item1");
            values1.Add("Item2");
            values1.Add("Item3");

            TestedListBoxDataSourceNoValueMember.DataSource = values1;

            ArrayList values = new ArrayList();
            values.Add(new ListItem("ValueItem1", "DisplayItem1"));
            values.Add(new ListItem("ValueItem2", "DisplayItem2"));
            values.Add(new ListItem("ValueItem3", "DisplayItem3"));
            values.Add(new ListItem("ValueItem4", "DisplayItem4"));

            TestedListBoxDataSource.ValueMember = "Value";
            TestedListBoxDataSource.DisplayMember = "Display";
            TestedListBoxDataSource.DataSource = values;

            lblLog.Text = "Select items from the ListBox...";
        }

        private void TestedListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTestedListBoxListBox SelectedValueChanged event is invoked";

            lblLog.Text = "TestedListBox SelectedValue: " + TestedListBox.SelectedValue;

        }

        private void TestedListBoxDataSourceNoValueMember_SelectedValueChanged(object sender, EventArgs e)
        {
            ListBoxElement TestedListBoxDataSourceNoValueMember = this.GetVisualElementById<ListBoxElement>("TestedListBoxDataSourceNoValueMember");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTestedListBoxDataSourceNoValueMember SelectedValueChanged event is invoked";

            lblLog.Text = "TestedListBoxDataSourceNoValueMember SelectedValue: " + TestedListBoxDataSourceNoValueMember.SelectedValue;

        }

        private void TestedListBoxDataSource_SelectedValueChanged(object sender, EventArgs e)
        {
            ListBoxElement TestedListBoxDataSource = this.GetVisualElementById<ListBoxElement>("TestedListBoxDataSource");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTestedListBoxDataSource SelectedValueChanged event is invoked";

            lblLog.Text = "TestedListBoxDataSource SelectedValue: " + TestedListBoxDataSource.SelectedValue;

        }

        public void btnSelectIt3_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBox = this.GetVisualElementById<ListBoxElement>("TestedListBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedListBox.SelectedValue = "It3";
        }

        public void btnSelectItem3_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBoxDataSourceNoValueMember = this.GetVisualElementById<ListBoxElement>("TestedListBoxDataSourceNoValueMember");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            try
            {
                TestedListBoxDataSourceNoValueMember.SelectedValue = "Item3";
            }
            catch (InvalidOperationException exc)
            {
                lblLog.Text = "Cannot set SelectedValue when ValueMember is empty." +
                    "\\r\\nSystem.InvalidOperationException was thrown:\\r\\n" + exc.Message;
            }
        }

        public void btnSelectDisplayItem3_Click(object sender, EventArgs e)
        {
            ListBoxElement TestedListBoxDataSource = this.GetVisualElementById<ListBoxElement>("TestedListBoxDataSource");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedListBoxDataSource.SelectedValue = "ValueItem3";
        }

        public void btnClearLog_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Clear();
            txtEventLog.Text = "Event Log:";
        }

        public class ListItem
        {
            private string myValue;
            private string myDisplay;

            public ListItem(string Value, string Display)
            {
                this.myValue = Value;
                this.myDisplay = Display;
            }

            public string Value
            {
                get
                {
                    return myValue;
                }
            }

            public string Display
            {

                get
                {
                    return myDisplay;
                }
            }

        }
    }
}