<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Height="800px" Text="ListView FullRowSelect Property" ID="windowView1" LoadAction="ListViewFullRowSelect\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="FullRowSelect" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether clicking an item selects all its subitems.

            Syntax: public bool FullRowSelect { get; set; }
            Property Value: 'true' if clicking an item selects the item and all its subitems;
             'false' if clicking an item selects only the item itself. The default is 'false'.
            
            Remarks: The FullRowSelect property has no effect unless the View property of the ListView control
             is set to Details. The FullRowSelect property is typically used when a ListView displays items with
             many subitems and it is important to be able to see selected items when the item text is not
             visible due to horizontal scrolling of the control's contents."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested ListView1 -->
        <vt:ListView runat="server" Text="ListView" Top="280px" ID="TestedListView1">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="390px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="445px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="FullRowSelect property of this ListView is initially set to 'true'
             The ListView should be inFullRowSelect" Top="485px" ID="lblExp1"></vt:Label>

        <!-- Tested ListView2 -->
        <vt:ListView runat="server" Text="TestedListView" FullRowSelect="true" Top="545px" ID="TestedListView2">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="655px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change FullRowSelect value >>" Top="720px" Width="200px" ID="btnChangeListViewFullRowSelect" ClickAction="ListViewFullRowSelect\btnChangeListViewFullRowSelect_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
        