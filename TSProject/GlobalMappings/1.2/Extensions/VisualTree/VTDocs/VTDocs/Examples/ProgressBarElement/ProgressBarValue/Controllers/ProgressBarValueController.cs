using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarValueController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar1 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "ProgressBar Min value is " + TestedProgressBar1.Min.ToString() + " ; " + "Max value is " + TestedProgressBar1.Max.ToString() + " ; " + "Value property is " + TestedProgressBar1.Value.ToString();
           
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ProgressBar Min value = " + TestedProgressBar2.Min.ToString() + " ; " + "Max value = " + TestedProgressBar2.Max.ToString() + " ; " + "Value property = " + TestedProgressBar2.Value.ToString();
           
        }

        public void btnChangeValue_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            int anInteger;

            if (int.TryParse(txtInput.Text, out anInteger))
            {
                anInteger = int.Parse(txtInput.Text);

                try
               {                
                   TestedProgressBar2.Value = anInteger;

                   lblLog2.Text = "ProgressBar Min value = " + TestedProgressBar2.Min.ToString() + " ; " + "Max value = " + TestedProgressBar2.Max.ToString() + " ; " + "Value property = " + TestedProgressBar2.Value.ToString();
               }
                catch (ArgumentOutOfRangeException ex)
               {
                    if (ex != null)
                    {
                        txtInput.Text = ex.ToString();

                    }
               }
            }

            else
            {
                lblLog2.Text = "Value must be a number.";
            }

        }

        public void TextBox_TextChanged(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "ProgressBar Min value = " + TestedProgressBar2.Min.ToString() + " ; " + "Max value = " + TestedProgressBar2.Max.ToString() + " ; " + "Inserted Value - " + txtInput.Text + "...";
             
         }
        
       
    }
}