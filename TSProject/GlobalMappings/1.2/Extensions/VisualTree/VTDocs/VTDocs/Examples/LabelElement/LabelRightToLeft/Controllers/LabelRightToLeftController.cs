﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelRightToLeftController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnRightToLeft_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");
            TextBoxElement txtRight2Left = this.GetVisualElementById<TextBoxElement>("txtRight2Left");

            if (label.RightToLeft == RightToLeft.Inherit) //Get
            {
                label.RightToLeft = RightToLeft.No; //Set
                txtRight2Left.Text = "The RightToLeft property value is: " + label.RightToLeft;
            }
            else if (label.RightToLeft == RightToLeft.No)
            {
                label.RightToLeft = RightToLeft.Yes;
                txtRight2Left.Text = "The RightToLeft property value is: " + label.RightToLeft;
            }
            else
            {
                label.RightToLeft = RightToLeft.Inherit;
                txtRight2Left.Text = "The RightToLeft property value is: " + label.RightToLeft;
            }
        }
      
    }
}