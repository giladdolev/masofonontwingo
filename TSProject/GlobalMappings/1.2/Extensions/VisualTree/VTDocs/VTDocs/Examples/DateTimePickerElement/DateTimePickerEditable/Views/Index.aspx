<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="DateTimePicker Editable Property" Width="770px" Height="750px" ID="windowView1" LoadAction="DateTimePickerEditable\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Editable" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the DateTimePicker box can respond to user interaction
            
           Syntex: public bool Editable { get; set; }
          
            Remarks: When the Editable property is set to true the date/time value can be updated.
             When the Editable property is set to false the date/time value is unable to be changed. 
            The default is true" Top="75px"  ID="lblDefinition"></vt:Label>
      
       <!-- Tested DateTimePicker1 -->
        <vt:DateTimePicker runat="server" Text="DateTimePicker" Left="80px" Height="20px" Width="200px" Top="230px" ID="TestedDateTimePicker1"></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Editable property of this DateTimePicker is initially set to 'false'" Top="380px"  ID="lblExp1"></vt:Label>     
        
        <!-- Tested DateTimePicker2 -->
        <vt:DateTimePicker runat="server"  Text="TestedDateTimePicker" CssClass="vt-TestedDateTimePicker" Left ="80px" Height="20px" Width="200px" Editable="false" Top="430px" ID="TestedDateTimePicker" ></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Height="50px" Top="465px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Editable value >>" Top="560px" ID="btnChangeEditable" Width="200px" ClickAction="DateTimePickerEditable\btnChangeEditable_Click"></vt:Button>

        <vt:Button runat="server" Text="Set Value >>" Top="560px" Left="300px" ID="btnSetValue" Width="200px" ClickAction="DateTimePickerEditable\btnSetValue_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Format value >>" Top="560px" Left="520px" ID="btnChangeFormat" Width="200px" ClickAction="DateTimePickerEditable\btnChangeFormat_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
