using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewFindFormController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form_Load(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            ListViewItem newReportNode = new ListViewItem("xxxxx");
            newReportNode.Subitems.Add("1");
            listView.Items.Add(newReportNode);
            newReportNode = new ListViewItem("yyyy");
            newReportNode.Subitems.Add("2");
            listView.Items.Add(newReportNode);
            ColumnHeader header1 = new ColumnHeader();
            header1.Text = "Col1";
            listView.Columns.Add(header1);
        }

        private void btnFindForm_Click(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            WindowElement myForm = this.GetRootVisualElement() as WindowElement;//<WindowElement>("windowView1");
            myForm.Text = "The Form before FindForm";
            // Get the form that the ListView control is contained within.
            myForm = listView.FindForm();
            // Set the text and color of the form containing the ListView.
            myForm.Text = "The Form after FindForm";
            myForm.BackColor = Color.Red;
        }
    }
}