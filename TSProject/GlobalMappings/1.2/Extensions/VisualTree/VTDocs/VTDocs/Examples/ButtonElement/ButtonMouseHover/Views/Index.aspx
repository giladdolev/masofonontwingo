<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button MouseHover event" ID="windowView2" LoadAction="ButtonMouseHover\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MouseHover" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the mouse pointer rests on the control.

            Syntax: public event EventHandler MouseHover
            
            The pause required for this event to be raised is specified in milliseconds by the MouseHoverTime 
            property."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time one of the Buttons is being hovered over, it's background color  
              changes. Each invocation of the 'MouseHover' event should add a line in the 'Event Log'."
            Top="265px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Height="50px" Width="505px">
            <vt:Button runat="server" Top="15px" Left="15px" Text="Button1" ID="btnButton1" TabIndex="1" MouseHoverAction="ButtonMouseHover\btnButton1_MouseHover"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="175px" Text="Button2" ID="btnButton2" TabIndex="2" MouseHoverAction="ButtonMouseHover\btnButton2_MouseHover"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="335px" Text="Button3" ID="btnButton3" TabIndex="3" MouseHoverAction="ButtonMouseHover\btnButton3_MouseHover"></vt:Button>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Height="40px" Top="400px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="455px" Width="360px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
