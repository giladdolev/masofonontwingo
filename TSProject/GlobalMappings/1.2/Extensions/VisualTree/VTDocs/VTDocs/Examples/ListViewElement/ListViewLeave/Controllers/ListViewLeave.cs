using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewLeaveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Click on the listview items...";
        }

        public void TestedListView1_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nListView 1: Leave event is invoked";

            lblLog.Text = "ListView 1 was leaved";
        }

        public void TestedListView1_SelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "";

            for (int i = 0; i < e.SelectedIndexes.Length; i++)
            {

                lblLog.Text += TestedListView1.Items[e.SelectedIndexes[i]].Text + " is selected \\r\\n";

            }
        }

        public void TestedListView2_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nListView 2: Leave event is invoked";

            lblLog.Text = "ListView 2 was leaved";
        }

        public void TestedListView2_SelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "";

            for (int i = 0; i < e.SelectedIndexes.Length; i++)
            {

                lblLog.Text += TestedListView2.Items[e.SelectedIndexes[i]].Text + " is selected \\r\\n";

            }
        }

    }
}