﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;

namespace MvcApplication9.Controllers
{
    public class BasicGridGridComboBoxColumnController : Controller
    {

        int i;
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.AddToComboBoxList("col1", "male,female");
            TestedGrid.AddToComboBoxList("col2", "male,female");
            TestedGrid.AddToComboBoxList("col3", "male,female");

            TestedGrid.Rows.Add("male", "male", "male");
            TestedGrid.Rows.Add("female", "female", "female");
            TestedGrid.Rows.Add("male", "male", "male");
        }


        public void btnAddComboBoxColumn_Click(object sender, EventArgs e)
        {

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            GridComboBoxColumn newColumn = new GridComboBoxColumn();
            i = TestedGrid.Columns.Count + 1;
            newColumn.ID = "col" + i.ToString();
            TestedGrid.AddToComboBoxList(newColumn.ID, "male,female");

            newColumn.HeaderText = "Column" + i.ToString();
            TestedGrid.Columns.Add(newColumn);
            lblLog.Text = newColumn.HeaderText + " was added";

            TestedGrid.Rows[0].Cells[TestedGrid.Columns.Count - 1].Value = "male";
            TestedGrid.Rows[1].Cells[TestedGrid.Columns.Count - 1].Value = "female";
            TestedGrid.Rows[2].Cells[TestedGrid.Columns.Count - 1].Value = "male";

        }

        public void btnRemoveLastComboBoxColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGrid.Columns.Count > 0)
            {
                int colToRemove = TestedGrid.Columns.Count;
                TestedGrid.Columns.RemoveAt(TestedGrid.Columns.Count - 1);

                lblLog.Text = "Column" + colToRemove.ToString() + " was removed";
            }
        }

        public void cellEndEdit(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

        }
    }
}