using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPixelWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelWidth value: " + TestedButton.PixelWidth + '.';
        }

        private void btnChangePixelWidth_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedButton.PixelWidth == 150)
            {
                TestedButton.PixelWidth = 300;
                lblLog.Text = "PixelWidth value: " + TestedButton.PixelWidth + '.';
            }
            else
            {
                TestedButton.PixelWidth = 150;
                lblLog.Text = "PixelWidth value: " + TestedButton.PixelWidth + '.';
            }
        }
    }
}