<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic TablePanel" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Initialized Component : TablePanel backColor is set to Lime" Top="520px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:TablePanel runat="server" Text="" Top="550px" Left="130px" ID="tablePanelInitialized" Width="500px" Height="350">

        </vt:TablePanel>


        <vt:Label runat="server" Text="Run Time :" Top="70px" Left="50px" ID="TablePanelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:TablePanel runat="server" Text="" Top="140px" Left="130px" ID="tablePanel" Width="500px" Height="350"></vt:TablePanel>
        <vt:Button runat="server" Text="Add col" Top="70px" Left="140px" ID="btnAddCol" Height="36px" TabIndex="1" Width="200px" ClickAction="TablePanelColumnCount\btnAddCol_Click"></vt:Button>

        <vt:Button runat="server" Text="Add row" Top="70px" Left="350px" ID="btnAddRow" Height="36px" TabIndex="1" Width="200px" ClickAction="TablePanelColumnCount\btnAddRow_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
