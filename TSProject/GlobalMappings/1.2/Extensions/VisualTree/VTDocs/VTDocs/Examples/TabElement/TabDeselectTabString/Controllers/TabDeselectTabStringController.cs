using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabDeselectTabStringController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Current tab selected ID is: " + TestedTab.SelectedTab.ID.ToString();

        }
 
        public void btnDeselectTab_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

            try
            {
                TestedTab.DeselectTab(txtInput.Text);
                lblLog.Text = "After Deselect(" + txtInput.Text + ")" + ", Current Selected tab is " + TestedTab.SelectedTab.ID;
            }
            catch (ArgumentException ex)
            {
                lblLog.Text = ex.GetType().ToString() + " was thrown\\r\\nEnter one of the tab item IDs";
            }
            
        }

     

        public void txtInput_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

            lblLog.Text = "Input tab ID: " + txtInput.Text + '.';
        }

       


        ////tabPageName example
        //public void ChangeTab2_Click(object sender, EventArgs e)
        //{
        //    TabElement tab = this.GetVisualElementById<TabElement>("tabControl1");
        //    tab.DeselectTab(tab.SelectedTab.ID);
        //}

        ////tabPage example
        //public void ChangeTab3_Click(object sender, EventArgs e)
        //{
        //    TabElement tab = this.GetVisualElementById<TabElement>("tabControl1");
        //    tab.DeselectTab(tab.SelectedTab);
        //}
    }
}