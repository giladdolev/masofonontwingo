﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="Grid ReadOnly Property" LoadAction="GridReadOnly\Page_load">


        <vt:Label runat="server" SkinID="Title" Text="ReadOnly" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the user can edit the cells of the DataGridView control.

           Syntax: public bool ReadOnly { get; set; }
            'true' if the user cannot edit the cells of the DataGridView control; otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Grid runat="server" Top="175px" Left="80px" ID="TestedGrid1" >
             <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns>      
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="305px" ID="lblLog1"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ReadOnly property of this Grid is initialy set to 'true'
            You should not be able to edit Grid cells" Top="390px" ID="lblExp1"></vt:Label>

        <vt:Grid runat="server" Top="450px" ReadOnly="true" Left="80px" ID="TestedGrid2">
            <Columns>
                
                <vt:GridTextBoxColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns> 
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="580px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ReadOnly value >>" Width="180px" Top="640px" ID="btnChangeReadOnly" ClickAction="GridReadOnly\btnChangeReadOnly_Click"></vt:Button>



    </vt:WindowView>


</asp:Content>
