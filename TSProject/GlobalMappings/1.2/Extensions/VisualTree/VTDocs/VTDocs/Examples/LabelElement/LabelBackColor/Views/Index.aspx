<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label BackColor property" ID="windowView1" LoadAction="LabelBackColor\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
         
        <vt:Label runat="server" SkinID="TestedLabel" Text="Label" Top="185px" ID="TestedLabel1"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="215px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="BackColor property of this 'TestedLabel' is initially set to Gray" Top="330px"  ID="lblExp1"></vt:Label>     

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Top="380px" ID="TestedLabel2" BackColor="Gray"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="410px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change BackColor value >>" Top="475px" ID="btnBackColor" Width="185px" ClickAction="LabelBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
