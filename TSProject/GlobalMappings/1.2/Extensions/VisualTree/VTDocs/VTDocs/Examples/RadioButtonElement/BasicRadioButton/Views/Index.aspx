<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic RichTextBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="radioButton1" Top="36px" Left="22px" ID="radioButton1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" Width="85px">
		</vt:RadioButton>
		<vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="radioButton2" Top="71px" Left="22px" ID="radioButton2" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" TabIndex="1" Width="85px">
		</vt:RadioButton>
		<vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="radioButton3" Top="111px" Left="22px" ID="radioButton3" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" TabIndex="2" Width="85px">
		</vt:RadioButton>
		<vt:GroupBox runat="server" TabStop="False" Text="groupBox1" Top="12px" Left="147px" ID="groupBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="150px" TabIndex="3" Width="157px">
			<vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="radioButton4" Top="24px" Left="24px" ID="radioButton4" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" Width="85px">
			</vt:RadioButton>
			<vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="radioButton5" Top="59px" Left="24px" ID="radioButton5" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" TabIndex="2" Width="85px">
			</vt:RadioButton>
			<vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="radioButton6" Top="99px" Left="24px" ID="radioButton6" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" TabIndex="4" Width="85px">
			</vt:RadioButton>
		</vt:GroupBox>
		<vt:GroupBox runat="server" TabStop="False" Text="groupBox2" Top="201px" Left="150px" ID="groupBox2" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="160px" TabIndex="4" Width="106px">
			<vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="radioButton7" Top="24px" Left="6px" ID="radioButton7" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" TabIndex="5" Width="85px">
			</vt:RadioButton>
		</vt:GroupBox>
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Check\Uncheck" Top="241px" Left="278px" ClickAction="BasicRadioButton\btnIsChecked_Click" ID="btnIsChecked" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="33px" TabIndex="5" Width="106px">
		</vt:Button>
		<vt:Label runat="server" Text="This label is
spreading
to 3 lines" Top="54px" Left="550px" ID="label1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="39px" TabIndex="6" Width="62px">
		</vt:Label>
		<vt:Label runat="server" Text="Vey special label:" Top="12px" Left="542px" ID="label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="7" Width="107px">
		</vt:Label>
    </vt:WindowView>
</asp:Content>
