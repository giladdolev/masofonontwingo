using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridRowHeaderCornerIconController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (gridElement.RowHeaderCornerIcon == null)
            {
                lblLog.Text = "RowHeaderCornerIcon source: null";
            }
            else
            {
                lblLog.Text = "RowHeaderCornerIcon source: \\r\\n" + gridElement.RowHeaderCornerIcon.Source;
            }
        }


        private void btnChangeRowHeaderCornerIcon_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            int ind = -1;

            if (gridElement.RowHeaderCornerIcon != null)
            {
                ind = gridElement.RowHeaderCornerIcon.Source.IndexOf("gizmox");
            }


            if (ind > 0)
            {
                gridElement.RowHeaderCornerIcon = new UrlReference("/Content/Images/galilcs.png");
            }
            else
            {
                gridElement.RowHeaderCornerIcon = new UrlReference("/Content/Images/gizmox.png");
            }


            //grid.SetRowHeaderBackgroundImage(0, new ResourceReference("/Content/Images/gizmox.png"));

            lblLog.Text = "RowHeaderCornerIcon source: " + gridElement.RowHeaderCornerIcon.Source;
        }

        private void btnClearRowHeaderCornerIcon_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.RowHeaderCornerIcon = null;

            if (gridElement.RowHeaderCornerIcon == null)
            {
                lblLog.Text = "RowHeaderCornerIcon source: null";
            }
            else
            {
                lblLog.Text = "RowHeaderCornerIcon source: " + gridElement.RowHeaderCornerIcon.Source;
            }
        }
    }
}
