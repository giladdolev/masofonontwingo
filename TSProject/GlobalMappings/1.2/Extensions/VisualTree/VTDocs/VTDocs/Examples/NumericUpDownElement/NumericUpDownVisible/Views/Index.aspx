<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Visible Property" ID="windowView1" LoadAction="NumericUpDownVisible\OnLoad">


        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested NumericUpDown1 -->
        <vt:NumericUpDown runat="server" Text="NumericUpDown" Top="170px" ID="TestedNumericUpDown1"></vt:NumericUpDown>


        <vt:Label runat="server" SkinID="Log" Top="205px" ID="lblLog1"></vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this NumericUpDown is initially set to 'false'
             The NumericUpDown should be invisible" Top="330px" ID="lblExp1"></vt:Label>

        <!-- Tested NumericUpDown2 -->
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Visible="false" Top="390px" ID="TestedNumericUpDown2"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="425px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Visible value >>" Top="500px" Width="200px" ID="btnChangeNumericUpDownVisible" ClickAction="NumericUpDownVisible\btnChangeNumericUpDownVisible_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
