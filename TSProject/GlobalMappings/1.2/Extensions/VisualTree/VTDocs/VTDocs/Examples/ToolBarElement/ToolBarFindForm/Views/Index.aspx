<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar FindForm Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="1000px">
		
        <vt:ToolBar runat="server" Text="toolStrip1" Dock="None" Top="102px" Left="276px" ID="toolStrip1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="25px" Width="466px">
		</vt:ToolBar>

		<vt:Button runat="server" TextAlign="MiddleCenter" Text="Invoke FindForm()" Top="100px" Left="44px" ClickAction="ToolBarFindForm\btnInvokeFindForm_Click" ID="btnInvokeFindForm" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button>

		
		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="230px" Left="400px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="198px">
		</vt:TextBox>
		

        </vt:WindowView>
</asp:Content>