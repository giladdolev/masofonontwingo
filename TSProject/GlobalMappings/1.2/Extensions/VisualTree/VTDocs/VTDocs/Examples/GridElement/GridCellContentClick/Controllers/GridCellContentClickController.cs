﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;

namespace MvcApplication9.Controllers
{
    public class GridCellContentClickController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");


            TestedGrid.Rows.Add("a", "false", "delete");
            TestedGrid.Rows.Add("b", "true", "delete");
            TestedGrid.Rows.Add("c", "false", "delete");

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            lblLog.Text = "Grid Current Cell Index : None";
        }

        public void TestedGrid_CellContentClick(object sender, GridElementCellEventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nGrid CellContentClick event is invoked";

            if ((e.RowIndex >= 0) && (e.ColumnIndex >= 0))
            {
                lblLog.Text = "Grid Current Cell Index : " + e.RowIndex + " , " + e.ColumnIndex;
            }
        }
    }
}
