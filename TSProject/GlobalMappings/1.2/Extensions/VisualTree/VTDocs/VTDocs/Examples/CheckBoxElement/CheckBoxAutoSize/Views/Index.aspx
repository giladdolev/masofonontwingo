<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox AutoSize Property" Top="57px" Left="11px" ID="windowView1" Height="880px" Width="800px">
        
         <vt:Label runat="server" Text="Gets or sets a value that indicates whether the control resizes based on its contents.
              true if the control automatically resizes based on its contents; otherwise, false. The default is true." Top="32px" width="760px" Left="50px" Font-Names="Calibri" ID="lblDefinition" Font-Size="13pt"></vt:Label>
         
        <vt:Label runat="server" Text="" Width="700px" Font-Names="Calibri Light" BackColor="Black"  Height="1px" Top="105px" Left="50px"  ID="lblLine"></vt:Label>
        
        <vt:Label runat="server" Text="AutoSize = Default value
            " Font-Names="Calibri" Font-Size="15pt" Font-Bold="true" Top="170px" Left="50px" AutoSize="true" ID="lblExp1"></vt:Label>
        
        <vt:Label runat="server" Text="In following example the AutoSize value is set to Default. 
            You should be able to see all the CheckBox text in one line: 'TestedCheckBox1 TestedCheckBox1 TestedCheckBox1'.
            
            To change the AutoSize value to 'false', press Action1 button.
            Now only part of the text is shown (according to the CheckBox width attribute)" Font-Names="Calibri Light"  Top="215px" Font-Size="12pt" Left="50px" AutoSize="true" ID="lblExp11"></vt:Label> 

        <vt:CheckBox runat="server" Text="TestedCheckBox1 TestedCheckBox1 TestedCheckBox1" Width="120"  Top="335px" Left="180px" ID="TestedCheckBox1" Height="26px"  ></vt:CheckBox>
        
        <vt:Button runat="server" Text="Action1" Top="335px" Left="80px" ID="Button1" Height="36px" Width="70px" ClickAction="CheckBoxAutoSize\btnAction1_Click"></vt:Button>  
                 
        <vt:TextBox runat="server" Text="" Top="410px" Left="80px" ID="txtLog1" Height="20px" Width="300px" Font-Size="8pt" Font-Names="Courier New" Multiline="true" BackColor ="LightYellow"></vt:TextBox>


         
        <vt:Label runat="server" Font-Names="Calibri" Text="AutoSize = false" Font-Size="15pt" Font-Bold="true" Top="475px" Left="50px" ID="lblExp2"   AutoSize="true"></vt:Label>
        
        <vt:Label runat="server" Font-Names="Calibri Light" Text="In following example the AutoSize value is set to 'false'. You should be able to see part of 
            CheckBox text (According to the checkBox Width settings).
            
            To change the AutoSize value to 'true', press Action1 button.
            Now you are able to see the all CheckBox text in one line." AutoSize="true" Font-Size="12pt" Top="520px" Left="50px"  ID="lblAction2"></vt:Label>     

        <vt:CheckBox runat="server"  Text="TestedCheckBox2 TestedCheckBox2 TestedCheckBox2" Top="640px" Left="180px" ID="TestedCheckBox2" AutoSize="false" Height="26px" Width="120px" ></vt:CheckBox>
        
        <vt:Button runat="server" Text="Action2" Top="640px" Left="80px" ID="Button2" Height="36px" Width="70px" ClickAction="CheckBoxAutoSize\btnAction2_Click"></vt:Button> 

        <vt:TextBox runat="server" Text="" Top="715px" Left="80px" ID="txtLog2" Height="20px" Width="300px" Font-Size="8pt" Font-Names="Courier New" BackColor ="LightYellow"></vt:TextBox>


        <vt:Label runat="server" Font-Names="Calibri Light" Text="TestedCheckBox2 TestedCheckBox2 TestedCheckBox2" width="120" Font-Size="12pt" Top="800px" Left="50px" height="15" ID="Label90"></vt:Label>  

    </vt:WindowView>
</asp:Content>
