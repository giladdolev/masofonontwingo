using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedTab1.Enabled.ToString();

            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedTab2.Enabled.ToString() + ".";
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedTab2.Enabled = !TestedTab2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedTab2.Enabled.ToString() + ".";
        }
    }
}