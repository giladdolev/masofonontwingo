<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button BackColor Property" ID="windowView" LoadAction="ButtonBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control" Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="120px" ID="TestedButton1"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="170px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this 'TestedButton' is initially set to Gray" Top="330px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" BackColor ="Gray" Top="400px" ID="TestedButton2"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor >>" Top="505px" ID="btnChangeBackColor" ClickAction="ButtonBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
