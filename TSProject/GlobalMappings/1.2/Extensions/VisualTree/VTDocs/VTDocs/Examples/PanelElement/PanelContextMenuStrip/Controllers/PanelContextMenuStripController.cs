using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.WebControls;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelContextMenuStripController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new PanelContextMenuStrip());
        }
        private PanelContextMenuStrip ViewModel
        {
            get { return this.GetRootVisualElement() as PanelContextMenuStrip; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");

            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            //Write to log lblLog1
            if (TestedPanel1.ContextMenuStrip == null)
                lblLog1.Text = "ContextMenuStrip value is null";
            else
                lblLog1.Text = "ContextMenuStrip value is: " + TestedPanel1.ContextMenuStrip.ID;

            //Write to log lblLog2
            if (TestedPanel2.ContextMenuStrip == null)
                lblLog2.Text = "ContextMenuStrip value is null.";
            else
                lblLog2.Text = "ContextMenuStrip value is: " + TestedPanel2.ContextMenuStrip.ID + ".";
        }


        private void btnChangeToContextMenuStrip1_Click()
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            ContextMenuStripElement contextMenuStrip1 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedPanel2.ContextMenuStrip = contextMenuStrip1;
            lblLog2.Text = "ContextMenuStrip value is: " + TestedPanel2.ContextMenuStrip.ID + ".";

        }

        private void btnChangeToContextMenuStrip2_Click()
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            ContextMenuStripElement contextMenuStrip2 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedPanel2.ContextMenuStrip = contextMenuStrip2;
            lblLog2.Text = "ContextMenuStrip value is: " + TestedPanel2.ContextMenuStrip.ID + ".";

        }

        private void btnReset_Click()
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedPanel2.ContextMenuStrip = null;
            lblLog2.Text = "ContextMenuStrip value is null.";

        }
/*
        private void CreateContextMenu()
        {
            PanelElement ContextMenuStrip2 = this.GetVisualElementById<PanelElement>("ContextMenuStrip2");

            if (ContextMenuStrip2.ContextMenuStrip == null)
            {
                ContextMenuStripElement menuStrip = new ContextMenuStripElement();
                // menuStrip.ID = "cnt";
                menuStrip.ID = "RunTimeCnt";

                // TODO: Method '.ctor' of type 'System.Windows.Forms.ToolStripMenuItem' is unmapped. (CODE=1005)
                ToolBarMenuItem menuItem = new ToolBarMenuItem("Exit");
                menuItem.Click += new EventHandler(menuItem_Click);
                menuItem.ID = "Exit";
                menuStrip.Items.Add(menuItem);

                //  this.Controls.Add(menuStrip);

                  
                this.ViewModel.Controls.Add(menuStrip);
                ContextMenuStrip2.ContextMenuStrip = menuStrip;
            }
            else 
            {
                ContextMenuStrip2.ContextMenuStrip = null;
            }
           
        }
        public void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolBarMenuItem menuItem = (ToolBarMenuItem)sender;
            if (object.Equals(menuItem.Text, "Exit"))
            {
                this.ViewModel.Close();
            }
        }

        private void menuItem_Click(object sender, EventArgs e)
        {
            ToolBarMenuItem menuItem = (ToolBarMenuItem)sender;
            if (object.Equals(menuItem.Text, "Exit"))
            {
                this.ViewModel.Close();
            }
        }

        public void btnSetContextMenuStrip_Click(object sender, EventArgs e)
        {
            CreateContextMenu();
        }
*/        
      
    }
}