using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxMultiColumnController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement testedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            testedComboBox1.MultiColumn = true;
            DataTable source = new DataTable();
            source.Columns.Add("Field1", typeof(int));
            source.Columns.Add("Field2", typeof(string));
            source.Columns.Add("Field3", typeof(string));

            source.Rows.Add(1, "James", "Dean");
            source.Rows.Add(2, "Bob", "Marley");
            source.Rows.Add(3, "Dana", "International");
            source.Rows.Add(4, "Sara", "");
            testedComboBox1.DisplayMember = "Field2";
            testedComboBox1.ValueMember = "Field1";
            testedComboBox1.DataSource = source;
        }
    }
}