<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SetBounds() Method" ID="windowView" LoadAction="GridSetBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SetBounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the bounds of the control to the specified location and size.

            Syntax: public void SetBounds(int left, int top, int width, int height)" Top="75px" ID="Label1" ></vt:Label>
                 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="SetBounds Method of this 'TestedGrid' is initially set with 
            Left: 80px, Top: 300px, Width: 285px, Height: 115px" Top="235px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" Top="300px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="430px" Height="40" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="SetBounds >>"  Top="495px" Width="180px" ID="btnSetBounds" ClickAction="GridSetBounds\btnSetBounds_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

