<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox DropDownWidth Property" ID="windowView1" LoadAction="ComboBoxDropDownWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Left="270px" Text="DropDownWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the of the drop-down portion of a combo box.
            
            Syntax: public int DropDownWidth { get; set; }
            
            If a value has not been set for the DropDownWidth, this property returns the Width of the combo box.
            The width of the drop-down cannot be smaller than the ComboBox width." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox" Top="220px" ID="ComboBox" CssClass="vt-test-cmb1">
            <Items>
                <vt:ListItem runat="server" Text="Item1"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item2"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item3"></vt:ListItem>
            </Items>
        </vt:ComboBox>    

        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The width of this ComboBox is 165px. 
            DropDownWidth property of this ComboBox is initially set to 200." Top="400px" ID="lblExp1"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" Top="470px" ID="TestedComboBox" CssClass="vt-test-cmb2" DropDownWidth="200">
            <Items>
                <vt:ListItem runat="server" Text="Item1"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item2"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item3"></vt:ListItem>
            </Items>
        </vt:ComboBox>    

        <vt:Label runat="server" SkinID="Log" Top="570px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change DropDownWidth value >>" Top="620px" ID="btnChangeDropDownWidth" Width="190px" ClickAction="ComboBoxDropDownWidth\btnChangeDropDownWidth_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

