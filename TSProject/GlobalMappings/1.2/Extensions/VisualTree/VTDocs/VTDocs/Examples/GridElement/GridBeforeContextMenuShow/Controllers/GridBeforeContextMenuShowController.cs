using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.ComponentModel;
using System.Drawing;

namespace MvcApplication9.Controllers
{
    public class GridBeforeContextMenuShowController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Right-Click on the Grid...";
        }


        public void TestedGrid_BeforeContextMenuShow(object sennder, CancelEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nGrid: BeforeContextMenuShow event is invoked";
            ContextMenuStripElement contextMenu = this.GetVisualElementById<ContextMenuStripElement>("contextMenu");

            if (contextMenu.Items[0].BackColor == Color.Aqua)
            {
                contextMenu.Items[0].BackColor = Color.Fuchsia;
                contextMenu.Items[1].BackColor = Color.Fuchsia;
            }
            else
            {
                contextMenu.Items[0].BackColor = Color.Aqua;
                contextMenu.Items[1].BackColor = Color.Aqua;
            }

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Context Menu Items BackColor=" + contextMenu.Items[0].BackColor;
           
        }
    }
}
