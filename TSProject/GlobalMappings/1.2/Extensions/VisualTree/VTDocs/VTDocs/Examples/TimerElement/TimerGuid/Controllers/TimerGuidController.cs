using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerGuidController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TimerGuid());
        }

     //   TimerGuid TimerGuidModel;
        private TimerGuid ViewModel
        {
            get { return this.GetRootVisualElement() as TimerGuid; }
        }

        public void timer1_Tick(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            textBox1.Text =  ViewModel.durationProperty++.ToString();

        }

        public void btnStart_Click(object sender, EventArgs e)
        {
            TimerElement timer1 = this.GetVisualElementById<TimerElement>("timer1");
            timer1.Enabled = true;
            timer1.Start();
        }

        public void btnStop_Click(object sender, EventArgs e)
        {
            TimerElement timer1 = this.GetVisualElementById<TimerElement>("timer1");
            timer1.Stop();
        }

        public void btnGuid_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");
            TimerElement timer1 = this.GetVisualElementById<TimerElement>("timer1");

            textBox2.Text = "Guid value: " + timer1.ID.ToString();
        }
    }
}