using System;
using System.Data;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridExpandOnFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid1 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(int));
            source.Rows.Add("James", "Dean", 1);
            source.Rows.Add("Bob", "Marley", 2);
            source.Rows.Add("Dana", "International", 3);

            TestedComboGrid1.ValueMember = "Column1";
            TestedComboGrid1.DisplayMember = "Column3";

            TestedComboGrid1.DataSource = source;
            lblLog1.Text = "ExpandOnFocus Value: " + TestedComboGrid1.ExpandOnFocus.ToString();

            DataTable source1 = new DataTable();
            source1.Columns.Add("Column1", typeof(string));
            source1.Columns.Add("Column2", typeof(string));
            source1.Columns.Add("Column3", typeof(int));
            source1.Rows.Add("James", "Dean", 1);
            source1.Rows.Add("Bob", "Marley", 2);
            source1.Rows.Add("Dana", "International", 3);

            TestedComboGrid2.ValueMember = "Column1";
            TestedComboGrid2.DisplayMember = "Column3";

            TestedComboGrid2.DataSource = source1;
            TestedComboGrid2.DataSource = source1;

            lblLog2.Text = "ExpandOnFocus Value: " + TestedComboGrid2.ExpandOnFocus.ToString() + ".";


        }

        private void btnChangeExpandOnFocus_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboGrid2.ExpandOnFocus = !TestedComboGrid2.ExpandOnFocus;

            lblLog2.Text = "ExpandOnFocus Value: " + TestedComboGrid2.ExpandOnFocus.ToString() + ".";
        }
    }
}