<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic btnContextMenuStrip" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" ContextMenuStripID="cnt">
        <vt:ContextMenuStrip runat="server" ID="cnt" Height="125">
            <vt:ToolBarMenuItem runat="server" ID="tlb1" ClickAction="ContextMenuStripBasic\menuItem1_Click" Text="menu 1"></vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" ID="ToolBarMenuItem1"  ClickAction="ContextMenuStripBasic\menuItem2_Click" Text="menu 2"></vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" ID="ToolBarMenuItem2" ClickAction="ContextMenuStripBasic\menuItem3_Click" Text="menu 3"></vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" ID="ToolBarMenuItem3" ClickAction="ContextMenuStripBasic\menuItem4_Click" Text="menu 4"></vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" ID="ToolBarMenuItem4" ClickAction="ContextMenuStripBasic\menuItem5_Click" Text="menu 5"></vt:ToolBarMenuItem>
        </vt:ContextMenuStrip>
    </vt:WindowView>
</asp:Content>
