using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonResetTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button Text is set";
        }

        public void btnResetText_Click(object sender, EventArgs e)
        {

            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedButton.ResetText();
            lblLog.Text = "Button Text is empty";
        }

        public void btnSetText_Click(object sender, EventArgs e)
        {

            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedButton.Text = "TestedButton";
            lblLog.Text = "Button Text is set";
        }
    }
}