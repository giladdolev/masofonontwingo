using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new PanelDock());
        }

        private PanelDock ViewModel
        {
            get { return this.GetRootVisualElement() as PanelDock; }
        }

        public void btnDock_Click(object sender, EventArgs e)
        {
            PanelElement pnlDock = this.GetVisualElementById<PanelElement>("pnlDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (pnlDock.Dock == Dock.Bottom)
            {
                pnlDock.Dock = Dock.Fill;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Fill)
            {
                pnlDock.Dock = Dock.Left;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Left)
            {
                pnlDock.Dock = Dock.Right;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Right)
            {
                pnlDock.Dock = Dock.Top;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.Top)
            {
                pnlDock.Dock = Dock.None;
                textBox1.Text = pnlDock.Dock.ToString();
            }
            else if (pnlDock.Dock == Dock.None)
            {
                pnlDock.Dock = Dock.Bottom;
                textBox1.Text = pnlDock.Dock.ToString();
            }  
        }

        //btnOpenForm2
        public void btnOpenForm2_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "PanelElement");
            argsDictionary.Add("ExampleName", "PanelDock");

            ViewModel.TestedWin2 = VisualElementHelper.CreateFromView<Form2DockFillAndResizing>("Form2DockFillAndResizing", "Index2", null, argsDictionary, null);
            this.ViewModel.TestedWin2.Show();
        }


        //btnOpenForm3
        public void btnOpenForm3_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "PanelElement");
            argsDictionary.Add("ExampleName", "PanelDock");

            ViewModel.TestedWin3 = VisualElementHelper.CreateFromView<Form3DockLeftAndRight>("Form3DockLeftAndRight", "Index3", null, argsDictionary, null);
            this.ViewModel.TestedWin3.Show();
        }

        //btnOpenForm4
        public void btnOpenForm4_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "PanelElement");
            argsDictionary.Add("ExampleName", "PanelDock");

            ViewModel.TestedWin4 = VisualElementHelper.CreateFromView<Form4DockTopAndBottom>("Form4DockTopAndBottom", "Index4", null, argsDictionary, null);
            this.ViewModel.TestedWin4.Show();
        }
    }
}