﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class GroupBoxEquals : WindowElement
    {

        GroupBoxElement _TestedGroupBox;

        public GroupBoxEquals()
        {
            _TestedGroupBox = new GroupBoxElement();
        }

        public GroupBoxElement TestedGroupBox
        {
            get { return this._TestedGroupBox; }
            set { this._TestedGroupBox = value; }
        }
    }
}