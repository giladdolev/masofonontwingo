<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Image property" ID="windowView1" LoadAction="LabelImage\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="Image" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the image that is displayed on a Label.
            
            Syntax: public ResourceReference Image { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
         
        <vt:Label runat="server" SkinID="TestedLabel" Height="70px" width="120" Text="Label" Top="145px" ID="TestedLabel1"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="230px" ID="lblLog1" Width="400"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example this label Image is initially set with image, you can change the Image by clicking
             the 'Change Image'  button " Top="350px"  ID="lblExp1"></vt:Label>     

        <vt:Label runat="server" SkinID="TestedLabel"  Text="TestedLabel" Image= "/Content/Images/icon.jpg"  Top="415px" ID="TestedLabel2" Height="70px" width="120" ></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog2" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change Image >>" Top="565px" ID="btnImage" Width="225px" ClickAction="LabelImage\btnChangeImage_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
