using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxSelectOnFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "SelectOnFocus value is: " + TestedTextBox1.SelectOnFocus;
            lblLog2.Text = "SelectOnFocus value is: " + TestedTextBox2.SelectOnFocus;

        }


        public void btnFocus1_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedTextBox1.Focus();
            lblLog1.Text = "SelectOnFocus value is: " + TestedTextBox1.SelectOnFocus + "\\r\\nTestedTextBox1 Focus method was invoked";
        }

        public void btnFocus2_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedTextBox2.Focus();
            lblLog2.Text = "SelectOnFocus value is: " + TestedTextBox2.SelectOnFocus + "\\r\\nTestedTextBox2 Focus method was invoked";
        }


        public void TestedTextBox1_LostFocus(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "SelectOnFocus value is: " + TestedTextBox1.SelectOnFocus;
        }

        public void TestedTextBox2_LostFocus(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            
            lblLog2.Text = "SelectOnFocus value is: " + TestedTextBox2.SelectOnFocus;
        }

    }
}