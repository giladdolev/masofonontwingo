<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox VisibleChanged Event" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:TextBox runat="server" Text="" Top="400px" Left="140px"  ID="txtEventLog" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

        <vt:TextBox runat="server" Text="Tested TextBox" Top="140px" Left="140px" ID="txtVisible" Height="36px" Width="200px" VisibleChangedAction="TextBoxVisibleChanged\txtVisible_VisibleChanged" >          
        </vt:TextBox>
        
        <vt:Button runat="server" Text="Change TextBox Visible" Top="145px" Left="420px" ID="btnChangeVisible" Height="30px" Width="200px" ClickAction="TextBoxVisibleChanged\btnChangeVisible_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="300px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

        <vt:Label runat="server" Text="Event Log" Top="370px" Left="140px"  ID="label1"   Height="15px" Width="200px"> 
            </vt:Label>

        

    </vt:WindowView>
</asp:Content>
        