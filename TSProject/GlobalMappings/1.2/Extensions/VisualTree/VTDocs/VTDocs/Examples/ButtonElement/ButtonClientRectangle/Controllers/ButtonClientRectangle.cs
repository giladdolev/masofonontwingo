using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedButton.ClientRectangle;

        }
        public void btnChangeButtonBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.Left == 100)
            {
                TestedButton.SetBounds(80, 350, 150, 36);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedButton.ClientRectangle;

            }
            else
            {
                TestedButton.SetBounds(100, 310, 200, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedButton.ClientRectangle;
            }

        }

      
    }
}