<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="6, 13" Text="BasicWindow" ID="BasicWindow" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="262px" Width="777px" LoadAction="BasicWindow\loadAction">
		<vt:Keyboard runat="server" ID="customKeyboard" Top="100px" Left="50px" Height="80px" Width="200px" CssClass="keyboardButton"></vt:Keyboard>
       <vt:TextBox runat="server" ID="text1" NumericKeyboardId="customKeyboard"></vt:TextBox>
              <vt:TextBox runat="server" ID="text2" Top="50px" NumericKeyboardId="customKeyboard"></vt:TextBox>

       <vt:Button runat="server" Left="200px" ClickAction="BasicWindow\click"></vt:Button>
	</vt:WindowView>
</asp:Content>
