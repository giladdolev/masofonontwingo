<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel AutoScroll Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Text="AutoScroll is set to 'true'" Top="55px"  AutoScroll="false" Left="140px" ID="pnlAutoScroll" Height="70px" 
            Width="200px">
            <vt:label runat="server" Text="label10" Top="10px" Left="335px" ID="label3" Height="15px"  Width="100px" ></vt:label>
        </vt:Panel>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2"  Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Text="PanelAutoScroll" Top="165px" Left="140px" ID="pnlRuntimeAutoScroll" Height="70px" TabIndex="1" Width="200px" >
            <vt:label runat="server" Text="label10" Top="10px" Left="335px" ID="label10" Height="15px"  Width="100px" ></vt:label>
        </vt:Panel>

        <vt:Button runat="server" Text="Change Panel AutoScroll" Top="195px" Left="420px" ID="btnChangeAutoScroll" Height="36px" TabIndex="1" Width="200px" ClickAction="PanelAutoScroll\btnChangeAutoScroll_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="275px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        