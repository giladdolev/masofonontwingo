<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar PixelWidth Property" ID="windowView1" LoadAction="ProgressBarPixelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels.
            
            Syntax: public int PixelWidth { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this ProgressBar is initially set to: 250px" Top="235px" ID="lblExp1"></vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="295px" ID="TestedProgressBar" TabIndex="3"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelWidth value >>" Top="415px" ID="btnChangePixelWidth" TabIndex="1" Width="180px" ClickAction="ProgressBarPixelWidth\btnChangePixelWidth_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
