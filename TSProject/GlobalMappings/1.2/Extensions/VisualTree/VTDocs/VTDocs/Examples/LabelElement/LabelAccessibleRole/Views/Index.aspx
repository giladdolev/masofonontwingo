<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label AccessibleRole property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Label runat="server" Text="AccessibleRole testing Run Time" Top="190px" Left="130px" ID="label" Width="350px"></vt:Label>
        <vt:Button runat="server" Text="Get Label AccessibleRole value" Top="112px" Left="130px" ID="btnAccessibleRole" TabIndex="1" Width="220px" ClickAction="LabelAccessibleRole\btnAccessibleRole_Click"></vt:Button>
         <vt:TextBox runat="server" Text="" Top="250px" Left="130px" ID="txtAccessibleRole" Multiline="true"  Height="100px" Width="250px"> 
            </vt:TextBox>
    </vt:WindowView>
</asp:Content>