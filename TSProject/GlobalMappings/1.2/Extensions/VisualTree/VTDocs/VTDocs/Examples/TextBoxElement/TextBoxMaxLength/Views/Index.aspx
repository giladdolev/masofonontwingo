<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox MaxLength Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="TextBoxMaxLength\Load">

        <vt:Label runat="server" Text="MaxLength Property is initially set to --> " Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="240px"></vt:Label>

        <vt:TextBox runat="server" Text="Hello World" Top="125px" Left="140px" ID="txtMaxLength" Height="26px" Width="150px" MaxLength="5"></vt:TextBox>

        <vt:Button runat="server" Text="Set MaxLength" Top="115px" Left="350px" ID="btnSetMaxLength" Height="36px" Width="150px" ClickAction="TextBoxMaxLength\btnSetMaxLength_Click"></vt:Button>

        <vt:Button runat="server" Text="Get MaxLength" Top="115px" Left="540px" ID="btnGetMaxLength" Height="36px" Width="150px" ClickAction="TextBoxMaxLength\btnGetMaxLength_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
