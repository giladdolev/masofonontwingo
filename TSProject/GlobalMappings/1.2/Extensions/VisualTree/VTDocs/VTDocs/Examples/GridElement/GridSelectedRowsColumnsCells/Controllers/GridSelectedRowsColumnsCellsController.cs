﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridSelectedRowsColumnsCellsController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void Load_View(object sender, EventArgs e)
        {

            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");

            DataTable source = new DataTable();
            source.Columns.Add("Field1", typeof(int));
            source.Columns.Add("Field2", typeof(string));


            source.Rows.Add(1, "James");
            source.Rows.Add(2, "Bob");
            source.Rows.Add(3, "Dana");
            source.Rows.Add(4, "Sara");

            gridElement.DataSource = source;



        }

        public void Grid_SelectionChangedAction(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nSelectedRows -- > " + gridElement.SelectedRows.Count;
            txtEventLog.Text += "\\r\\nSelectedColumns -- > " + gridElement.SelectedColumns.Count;
            txtEventLog.Text += "\\r\\nSelectedCells -- > " + gridElement.SelectedCells.Count;
            txtEventLog.Text += "\\r\\ ---------------------------------- ";
        }


        public void clcik(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            gridElement.RefreshData();
        }
    }
}
