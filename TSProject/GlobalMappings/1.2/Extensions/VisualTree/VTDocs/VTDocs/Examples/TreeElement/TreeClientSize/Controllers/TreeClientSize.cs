using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>

        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedTree.ClientSize;

        }
        public void btnChangeTreeClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            if (TestedTree.Width == 200)
            {
                TestedTree.ClientSize = new Size(140, 110);
                lblLog.Text = "ClientSize value: " + TestedTree.ClientSize;

            }
            else
            {
                TestedTree.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedTree.ClientSize;
            }

        }

    }
}