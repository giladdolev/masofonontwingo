<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox MouseHover event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxMouseHover/Form_Load">
  
        <vt:Label runat="server" Text="Hovering Hover the comboBox below" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="160px" Left="50px" Height="90px" Width="250px" MouseHoverAction="ComboBoxMouseHover\btnTestedComboBox_MouseHover"/>
    </vt:WindowView>
</asp:Content>
