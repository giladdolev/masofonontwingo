<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Left Property" ID="windowView1" LoadAction="RichTextBoxLeft\OnLoad">          

        <vt:Label runat="server" SkinID="Title" Text="Left" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the left edge of the control 
            and the left edge of its container's client area.
            
            Syntax: public int Left { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Left property of this RichTextBox is initially set to: 80px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="310px" ID="TestedRichTextBox" TabIndex="1"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Left value >>" Top="470px" ID="btnChangeLeft" TabIndex="1" Width="180px" ClickAction="RichTextBoxLeft\btnChangeLeft_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
