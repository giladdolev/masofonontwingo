<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image BackgroundImage Property" ID="windowView1" LoadAction="ImageBackgroundImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImage" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image displayed in the control.
            
            public virtual Image BackgroundImage { get; set; }" Top="75px" ID="lblDefinition" ></vt:Label>
         
        <vt:Image runat="server" Text="Image" ImageReference="" BorderStyle="Solid" Top="140px" ID="TestedImage1" ></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="245px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="310px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackgroundImage property of this 'TestedImage' is initially set to Image.png" Top="360px"  ID="lblExp1"></vt:Label>     
        
        <vt:Image runat="server" Text="TestedImage" ImageReference="~/Content/Images/icon.jpg" BorderStyle="Solid" BackgroundImage ="~/Content/Elements/Image.png" Top="410px" ID="TestedImage2" ></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="515px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackgroundImage Value >>"  Top="580px" Width="200px" ID="btnChangeBackgroundImage" ClickAction="ImageBackgroundImage\btnChangeBackgroundImage_Click"></vt:Button>
             
    </vt:WindowView>
</asp:Content>

