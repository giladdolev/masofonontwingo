﻿ <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumnCollection IndexOf  Method (String)" ID="windowView1" LoadAction="GridGridColumnCollectionIndexOfString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="IndexOf(String)" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the index of the specified ID in the collection.

            Syntax: public int IndexOf(string colID)
            colID - The column ID to return the index of.

            Return Value: The index of the given column id, or -1 if the column ID does not match the ID
             of any column in the collection."
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="260px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can get column index by entering the column ID and clicking the
             IndexOf button. 
            Column1 ID is Col1
            Column2 ID is Col2
            Column3 ID is Col3"
            Top="310px" ID="lblExp2"></vt:Label>

        <vt:Grid runat="server" Top="420px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Width="410px" Height="40px" ID="lblLog" Top="550px"></vt:Label>

        <vt:label runat="server" Text="Enter Column ID: " Left="80px" Font-Size="11pt" Width="120px" Top="617px" ID="lblInput" ></vt:label>

        <vt:TextBox runat="server" Text="ColumnID" ForeColor="Gray" Width="80px" Left="190"  Top="615px" ID="txtInput" TextChangedAction="GridGridColumnCollectionIndexOfString\txtInput_TextChanged"></vt:TextBox>

        <vt:Button runat="server" Text="IndexOf >>" Left="330px" Top="615px" ID="btnIndexOf" ClickAction="GridGridColumnCollectionIndexOfString\btnIndexOf_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

