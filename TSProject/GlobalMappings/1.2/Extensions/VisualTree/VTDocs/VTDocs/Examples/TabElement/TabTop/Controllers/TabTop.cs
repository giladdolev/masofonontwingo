using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             lblLog.Text = "Top Value: " + TestedTab.Top + '.';

         }
         public void btnChangeTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
             if (TestedTab.Top == 515)
             {
                 TestedTab.Top = 300;
                 lblLog.Text = "Top Value: " + TestedTab.Top + '.';

             }
             else
             {
                 TestedTab.Top = 515;
                 lblLog.Text = "Top Value: " + TestedTab.Top + '.' + " Look Down :)";
             }

         }

    }
}