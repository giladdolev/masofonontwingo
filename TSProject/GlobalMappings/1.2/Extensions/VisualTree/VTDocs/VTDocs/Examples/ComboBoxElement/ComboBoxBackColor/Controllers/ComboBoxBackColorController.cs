using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedComboBox1.Items.Add("Item1");
            TestedComboBox1.Items.Add("Item2");
            TestedComboBox1.Items.Add("Item3");
            lblLog1.Text = "BackColor value: " + TestedComboBox1.BackColor.ToString();

            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox2.Items.Add("Item1");
            TestedComboBox2.Items.Add("Item2");
            TestedComboBox2.Items.Add("Item3");
            lblLog2.Text = "BackColor value: " + TestedComboBox2.BackColor.ToString(); 

        }


        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedComboBox2.BackColor == Color.Orange)
            {
                TestedComboBox2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedComboBox2.BackColor.ToString();
            }
            else
            {
                TestedComboBox2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedComboBox2.BackColor.ToString();

            }
        }
    }
}