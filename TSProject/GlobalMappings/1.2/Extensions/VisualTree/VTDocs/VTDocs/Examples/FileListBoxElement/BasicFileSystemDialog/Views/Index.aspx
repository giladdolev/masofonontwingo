<%@ Page Title="BasicFileSystemDialog" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic FileSystem Dialog" ID="windowView1">

        <vt:Label runat="server" SkinID="Title" Text="Basic FileSystem Dialog" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Show behavior of compatibility FileSystem controls" Top="75px" ID="lblDefinition"></vt:Label>

        <!-- Actual sample element(s) should be placed instead of button below -->
        <vt:SplitContainer runat="server" SplitterDistance="145" Top="100px" Left="50px" Width="650px" Height="350px" ID="splitContainer1">

            <vt:SplitterPanel runat="server" Dock="Fill">
                <vt:DriveListBox runat="server" ID="dlb1" Dock="Top" ChangedAction="BasicFileSystemDialog\SelectedDriveChanged"></vt:DriveListBox>
                <vt:DirectoryListBox runat="server" ID="dirlb1" Dock="Fill" ChangedAction="BasicFileSystemDialog\SelectedDirectoryChanged" ></vt:DirectoryListBox>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" Dock="Left" Width="400px">
                <vt:FileListBox runat="server" ID="flb1" Dock="Fill" EnableUpload="false" SelectedItemChangedAction="BasicFileSystemDialog\SelectedFileChanged"></vt:FileListBox>
            </vt:SplitterPanel>
        </vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog1"></vt:Label>

    </vt:WindowView>
</asp:Content>
