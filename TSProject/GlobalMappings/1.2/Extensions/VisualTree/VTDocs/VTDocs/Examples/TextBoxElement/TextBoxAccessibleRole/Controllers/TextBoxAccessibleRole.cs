using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            TextBoxElement txtAccessibleRole = this.GetVisualElementById<TextBoxElement>("txtAccessibleRole");

            MessageBox.Show(txtAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            TextBoxElement txtAccessibleRole = this.GetVisualElementById<TextBoxElement>("txtAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (txtAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else if (txtAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                txtAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = txtAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}