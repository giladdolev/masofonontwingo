<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Closing Test Window" ID="ClosingTestWindow" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleDimensions="6, 13" Height="262px" Width="384px" LoadAction="WindowClosing2\OnLoad" WindowClosingAction="WindowClosing2\closing" WindowClosedAction="WindowClosing2\Closed">
        
        <vt:Label runat="server" SkinID="Log" Top="10px" Left="10px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="50px" Left="10px" Width="360px" Height="100px" ID="txtEventLog"></vt:TextBox>

        <vt:CheckBox runat="server" Text="Cancel Closing Action" Top="210px" Left="230px" ID="chkCancelClosing" CheckedChangedAction="WindowClosing2\chkCancelClosing_CheckedChanged"></vt:CheckBox>

        <vt:Button runat="server" Text="Close This Window >>" Top="210px" Left="10px" Width="160px" ID="btnChangeHeight" ClickAction="WindowClosing2\btnCloseWindow_Click"></vt:Button>

	</vt:WindowView>
</asp:Content>
