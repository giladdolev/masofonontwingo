<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Location Property" ID="windowView1" LoadAction="TabLocation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Location" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the coordinates of the upper-left corner of the control 
            relative to the upper-left corner of its container.

            Syntax: public Point Location { get; set; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The Location of this Tab is initially set with Left: 80px and Top: 285px" 
                Top="235px"  ID="lblExp1"></vt:Label>

        <!-- Tested Tab -->
        <vt:Tab runat="server" Text="TestedTab" Top="285px" ID="TestedTab">
             <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab> 
        
        <vt:Label runat="server" SkinID="Log" Top="400px" ID="lblLog"></vt:Label>         

        <vt:Button runat="server"  Text="Change Location value >>" Top="465px" ID="btnChangeLocation" Width="150px" ClickAction="TabLocation\btnChangeLocation_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

