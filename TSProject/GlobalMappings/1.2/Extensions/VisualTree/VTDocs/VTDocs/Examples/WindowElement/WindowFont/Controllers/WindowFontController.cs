using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowFontController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowFont());
        }
        private WindowFont ViewModel
        {
            get { return this.GetRootVisualElement() as WindowFont; }
        }

        public void btnChangeFont_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.Font.Underline == true)
            {
                this.ViewModel.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                textBox1.Text = "Font(Niagara Engraved, 30, FontStyle.Italic)";
            }
            else if (this.ViewModel.Font.Bold == true)
            {
                this.ViewModel.Font = new Font("Miriam Fixed", 20, FontStyle.Underline);
                textBox1.Text = "Font(Miriam Fixed, 20, FontStyle.Underline)";
            }
            else if (this.ViewModel.Font.Italic == true)
            {
                this.ViewModel.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                textBox1.Text = "Font(SketchFlow Print, 15, FontStyle.Strikeout)";
            }
            else
            {
                this.ViewModel.Font = new Font("SketchFlow Print", 15, FontStyle.Bold);
                textBox1.Text = "Font(SketchFlow Print, 15, FontStyle.Bold)";
            }
        }
    }
}