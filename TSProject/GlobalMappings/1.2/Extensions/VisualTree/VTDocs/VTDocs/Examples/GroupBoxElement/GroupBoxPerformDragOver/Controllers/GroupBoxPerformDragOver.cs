using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPerformDragOverController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformDragOver_Click(object sender, EventArgs e)
        {
            DragEventArgs args = new DragEventArgs();
            GroupBoxElement testedGroupBox = this.GetVisualElementById<GroupBoxElement>("testedGroupBox");

            testedGroupBox.PerformDragOver(args);
        }

        public void testedGroupBox_DragOver(object sender, EventArgs e)
        {
            MessageBox.Show("TestedGroupBox DragOver event method is invoked");
        }

    }
}