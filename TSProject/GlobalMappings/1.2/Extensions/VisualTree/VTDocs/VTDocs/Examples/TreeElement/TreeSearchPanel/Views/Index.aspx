<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree SearchPanel Property" ID="windowView" LoadAction="TreeSearchPanel\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SearchPanel" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets a value indicating whether the search field is shown.

            Syntax: public bool SearchPanel { get; set; }
            Property value: 'true' if the property is shown otherwise 'false'. The default is 'false'.
             
            The SearchPanel provides an easy way to search against multiple columns" Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="SearchPanel property of this Tree is initially set to 'true'. 
             You can also show or hide the SearchPanel using the button below" Top="280px"  ID="lblExp1"></vt:Label>     

        <vt:Tree runat="server" ID="TestedTree" SkinID="TreeList" Top="345px" SearchPanel="true"></vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="560px" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change SearchPanel value >>" Top="625px" Width="180px" ID="btnChangeTreeSearchPanel" ClickAction="TreeSearchPanel\btnChangeTreeSearchPanel_Click"></vt:Button>
             
    </vt:WindowView>
</asp:Content>