﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridRowCollection RemoveAt method" ID="windowView1" LoadAction="GridGridRowCollectionRemoveAt\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="RemoveAt" Top="15px" ID="lblTitle" Left="300">
        </vt:Label>
        <vt:Label runat="server" Text="Removes the row at the specified position from the collection. 

            syntax : public void RemoveAt(int index);"
            Top="65px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="180px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can insert a row index number to the text box and click  
           on the 'Remove Row' button. The row with the specified index will be removed." Top="220px" ID="Label2">
        </vt:Label>
        <vt:Grid runat="server"  Top="295px" ID="TestedGrid" >
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog1" Text=""></vt:Label>
        
        <vt:Label runat="server" Top="495px" Left="80px" ID="lblinstruction" Text="Insert a row index to remove:"></vt:Label>
        
        <vt:TextBox runat="server" Top="490px" Left="290px" Width="60px" Text="..." TextChangedAction="GridGridRowCollectionRemoveAt\TextBox_TextChanged" ID="txtInput"></vt:TextBox>
        <vt:Button runat="server" Top="490px" Left="400px" Text="Remove Row >>" ClickAction="GridGridRowCollectionRemoveAt\RemoveAt_Click">
        </vt:Button>
    </vt:WindowView>

</asp:Content>
