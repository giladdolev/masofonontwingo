<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ClipBorad" Height="800px" Width="1200px" LoadAction="GridClipboard\view_load" >
        
         <vt:Label runat="server" SkinID="SubTitle" Text="ClipBorad" Top="15px" ID="lblTitle" Left="300" Font-Size="19pt" Font-Bold="true">
        </vt:Label>
        <vt:Label runat="server" Text="How to use ClipBorad"
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Grid runat="server" Top="100px" Left="10px" ID="gridElement" Height="200px" Width="782px" ReadOnly="false"> 
            <Columns>
                <vt:GridColumn ID="name" runat="server" HeaderText="Name" DataMember="name" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="email" runat="server" HeaderText="Email" DataMember="email" Width="80"></vt:GridColumn>
                <vt:GridCheckBoxColumn ID="phone" runat="server" HeaderText="Phone" DataMember="phone" Width="90" ></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>
       
        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="copy to " Top="100px" Left="820px" ID="button1" ClickAction="GridClipboard\button1_Click" Height="45px" Width="236px">
        </vt:Button>
        
        
          <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Font-Size="13pt" Top="350px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="after click on copy to the selected cell will copied to the clipboard. " Top="380px" ID="Label2">
        </vt:Label>
    </vt:WindowView>

</asp:Content>
