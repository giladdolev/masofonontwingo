<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Anchor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="Initialize - NumericUpDown Anchor is set to 'Bottom'" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px"  Width="150px"></vt:Label>     

        <vt:Panel runat="server" Top="50px" Left="140px" Margin-All="0" Padding-All="0" ID="panel1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="72px"  Width="300px">
        <vt:NumericUpDown runat="server" Text="" Anchor ="Bottom" Top="18px" Left="75px" ID="nudAnchor" Height="20px"  Width="100px"></vt:NumericUpDown>           
            </vt:Panel>


        <vt:Label runat="server" Text="RunTime" Top="150px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Top="170px" Left="140px" Margin-All="0" Padding-All="0"  ID="panel2" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="72px"  Width="300px">
        <vt:NumericUpDown runat="server"  Text="NumericUpDown Anchor change" Top="18px" Left="75px" ID="nudAnchorChange" Height="20px"  Width="100px" ></vt:NumericUpDown>
            </vt:Panel>

        <vt:Button runat="server" Text="Change Anchor" Top="188px" Left="555px" ID="btnChangeAnchor" Height="36px"   Width="120px" ClickAction="NumericUpDownAnchor\btnChangeAnchor_Click"></vt:Button>
        
        
        <vt:TextBox runat="server" Text="" Top="400px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
