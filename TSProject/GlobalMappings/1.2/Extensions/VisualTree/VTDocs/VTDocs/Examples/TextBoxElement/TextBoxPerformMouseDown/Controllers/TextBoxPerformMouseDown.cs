using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformMouseDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseDown_Click(object sender, EventArgs e)
        {
            
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");

            MouseEventArgs args = new MouseEventArgs();

            testedTextBox.PerformMouseDown(args);
        }

        public void testedTextBox_MouseDown(object sender, EventArgs e)
        {
            MessageBox.Show("TestedTextBox MouseDown event method is invoked");
        }

    }
}