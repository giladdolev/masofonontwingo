﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="MaskedTextBox Mask" ID="windowView1" LoadAction="MaskedTextBoxMask\OnLoad">

         <vt:Label runat="server" SkinID="Title" Text="Mask" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the input mask to use at run time. 

            Syntax: public string Mask { get; set; }
            Value: A String representing the current mask. The default value is the empty string
             which allows any input.
            Mask is the default property for the MaskedTextBox class. Mask must be a string composed of one
             or more of the masking element. Click the following button See all masking elements." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:MaskedTextBox runat="server" ID="MaskedTextBox" Top="230px" Left="80px" ></vt:MaskedTextBox>

        <vt:Label runat="server" SkinID="Log" Top="270px" ID="lblLog1" Width="450px" ></vt:Label>
          
        <vt:Button runat="server" Text="Open MaskingElement Table >>" Top="230px" width="200px" Left="330px" ID="btnMaskingElementTable" ClickAction="MaskedTextBoxMask\btnMaskingElementTable_Click"></vt:Button>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="305px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can examine the Mask property by selecting a predefine mask from 
            the comboBox list below or by defining a custom mask by writing it in the comboBox.
            This MaskedTextBox mask is initially set to '00/00/0000' (A date (day, numeric month, year) in
             international date format. The '/' character is a logical date separator, and will appear
             to the user as the date separator appropriate to the application's current culture)." Top="345px" ID="Label3"></vt:Label>

        <vt:MaskedTextBox runat="server" ID="TestedMaskedTextBox" Mask="00/00/0000" Top="465px" Left="80px" ></vt:MaskedTextBox>

        <vt:Label runat="server" SkinID="Log" Top="505px" ID="lblLog" Width="450px" Height="90px" ></vt:Label>
         
         <vt:Label runat="server" Top="620px" Text="Select a Mask: " ID="Label1"></vt:Label>

        <vt:ComboBox runat="server" ID="combo1" Text="" SelectedIndexChangedAction = "MaskedTextBoxMask\combo1_SelectedIndexChanged" Left="150px" Top="620px">
          <Items>
                <vt:ListItem runat="server" Text="00->L<LL-0000"></vt:ListItem>
                <vt:ListItem runat="server" Text="00/00/0000"></vt:ListItem>
                <vt:ListItem runat="server" Text="(999)-000-0000"></vt:ListItem>
                <vt:ListItem runat="server" Text="$999,999.00"></vt:ListItem>
                <vt:ListItem runat="server" Text="\a. LLL, b. LLL"></vt:ListItem>
                <vt:ListItem runat="server" Text="00:00"></vt:ListItem>
                <vt:ListItem runat="server" Text="90:00"></vt:ListItem>
                <vt:ListItem runat="server" Text="00/00/0000 90:00"></vt:ListItem>
                <vt:ListItem runat="server" Text="00000"></vt:ListItem>               
          </Items> </vt:ComboBox>

        <vt:Button runat="server" ID="btnSetMask" Left="380px" Text="Set Mask" Top="620px" ClickAction="MaskedTextBoxMask\btnSetMask_Click"></vt:Button>

        <vt:Label runat="server" Top="650px" Text="Enter Text: " ID="Label2"></vt:Label>

         <vt:TextBox runat="server" ID="textBox1" Width="165px" Text="" SelectedIndexChangedAction = "MaskedTextBoxMask\combo1_SelectedIndexChanged" Left="150px" Top="650px"> </vt:TextBox>

        <vt:Button runat="server" ID="btnSetText" Left="380px" Text="Set Text" Top="650px" ClickAction="MaskedTextBoxMask\btnSetText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
