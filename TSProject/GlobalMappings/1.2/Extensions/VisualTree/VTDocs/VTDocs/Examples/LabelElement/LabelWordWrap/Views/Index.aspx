<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label WordWrap property" ID="windowView2" LoadAction="LabelWordWrap\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="WordWrap" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether a Label control with its AutoSize property set to True 
            expands vertically or horizontally to fit the text specified in its Text property.
            
             Syntax: public bool WordWrap { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
         
        <!--TestedLabel1-->
        <vt:Label runat="server"  Text="This is the Text of TestedLabel1" Top="180px" Left="80px" ID="TestedLabel1" AutoSize="true" Height="15px" Width="80px" BorderStyle="Solid" BackColor="LimeGreen"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Height="45px" Top="215px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="AutoSize property is initially set to 'true' in both tested labels. 
            WordWrap property of this 'TestedLabel' is initially set to 'true'.
            You can toggle the value of the WordWrap property by pressing the button." Top="330px"  ID="lblExp1"></vt:Label>     

        <!--TestedLabel-->
        <vt:Label runat="server"  Text="This is the Text of TestedLabel2" Top="420px" Left="80px" ID="TestedLabel2" AutoSize="true" WordWrap="true" Height="15px" Width="80px" BorderStyle="Solid" BackColor="LimeGreen"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Height="45px" Top="470px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Toggle WordWrap value >>" Top="555px" ID="btnToggleWordWrap" Width="200px" ClickAction="LabelWordWrap\btnToggleWordWrap_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>


