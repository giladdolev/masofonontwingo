<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server" >

    <vt:CompositeView runat="server" Text="Composite" Height="250" Top="0" ID="Composite" LoadAction="CompositeWindow\cmp_load" BackColor="Yellow" Dock="Fill" UnloadAction="CompositeWindow\cmp_unload">
     
        <vt:Button runat="server" Text="close" Top="0" ID="Button1" Height="40" Width="80" BackColor="Green" ClickAction="CompositeWindow\rmove" ZIndex="1"></vt:Button>

        <vt:Button runat="server" Text="change color" Top="10" ID="Button2" Height="40" Width="80"  ClickAction="CompositeWindow\change_btn_color"></vt:Button>

    </vt:CompositeView>

    


</asp:Content>
