﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelAppearanceController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnAppearance_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");

            if (label.Appearance == AppearanceConstants.cc3D)
            {
                label.Appearance = AppearanceConstants.ccFlat;
            }
            else if (label.Appearance == AppearanceConstants.ccFlat)
            {
                label.Appearance = AppearanceConstants.None;
            }
            else if (label.Appearance == AppearanceConstants.None)
            {
                label.Appearance = AppearanceConstants.cc3D;
            }
        }
      
    }
}