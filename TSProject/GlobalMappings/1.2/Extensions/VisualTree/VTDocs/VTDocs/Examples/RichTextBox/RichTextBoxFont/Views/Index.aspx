<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Font Property" ID="windowView2" LoadAction="RichTextBoxFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:RichTextBox runat="server" Text="RichTextBox" Top="185px" Width="160px" ID="TestedRichTextBox1" ></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="275px" Width="430px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedRichTextBox is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="410px"  ID="lblExp1"></vt:Label>     
        
      
        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="460px" Width="160px" ID="TestedRichTextBox2" ></vt:RichTextBox>
        <vt:Label runat="server" SkinID="Log" Top="555px" Width="430px" Height="45px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="620px" ID="btnChangeRichTextBoxFont" ClickAction="RichTextBoxFont/btnChangeRichTextBoxFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>