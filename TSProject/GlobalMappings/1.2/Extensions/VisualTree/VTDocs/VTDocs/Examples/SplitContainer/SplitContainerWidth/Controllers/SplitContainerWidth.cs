using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Width value: " + TestedSplitContainer.Width + '.';
        }

        private void btnChangeWidth_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedSplitContainer.Width == 200)
            {
                TestedSplitContainer.Width = 400;
                lblLog.Text = "Width value: " + TestedSplitContainer.Width + '.';
            }
            else
            {
                TestedSplitContainer.Width = 200;
                lblLog.Text = "Width value: " + TestedSplitContainer.Width + '.';
            }
        }
      
    }
}