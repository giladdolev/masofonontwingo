<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic HTML" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" >
        <vt:Panel runat="server" ShowHeader="False" AutoScroll="True" Top="0px" Left="0px" ID="panel1" BackColor="Lime" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="900px" Width="500px">
               <vt:HTML runat="server" Top="0" Left="0" ID="frm"  ForeColor="Red" Url="http://www.walla.co.il/"></vt:HTML>
        </vt:Panel>
    </vt:WindowView>
</asp:Content>
