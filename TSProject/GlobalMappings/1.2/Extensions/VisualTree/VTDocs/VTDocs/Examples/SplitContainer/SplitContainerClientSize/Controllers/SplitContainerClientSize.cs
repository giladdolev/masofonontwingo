using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedSplitContainer.ClientSize;

        }
        public void btnChangeSplitContainerClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            if (TestedSplitContainer.Width == 300)
            {
                TestedSplitContainer.ClientSize = new Size(200, 80);
                lblLog.Text = "ClientSize value: " + TestedSplitContainer.ClientSize;

            }
            else
            {
                TestedSplitContainer.ClientSize = new Size(300, 45);
                lblLog.Text = "ClientSize value: " + TestedSplitContainer.ClientSize;
            }

        }

    }
}