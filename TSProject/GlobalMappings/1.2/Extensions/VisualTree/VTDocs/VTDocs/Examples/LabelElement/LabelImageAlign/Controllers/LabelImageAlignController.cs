using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelImageAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ImageAlign value: " + TestedLabel1.ImageAlign;
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + '.';

        }

        private void btnTopLeft_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.TopLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        private void btnTopCenter_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.TopCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        private void btnTopRight_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.TopRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }


        private void btnMiddleLeft_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.MiddleLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        private void btnMiddleCenter_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.MiddleCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        private void btnMiddleRight_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.MiddleRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        private void btnBottomLeft_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.BottomLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        private void btnBottomCenter_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.BottomCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        private void btnBottomRight_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.ImageAlign = ContentAlignment.BottomRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ImageAlign value: " + TestedLabel2.ImageAlign + ".";
        }

        
    }
}