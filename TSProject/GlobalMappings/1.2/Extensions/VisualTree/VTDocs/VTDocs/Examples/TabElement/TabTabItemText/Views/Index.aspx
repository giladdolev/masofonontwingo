<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem Text Property" ID="windowView1" LoadAction="TabTabItemText\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text to display on the TabItem.
            
            Syntax: public Virtual string Text { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
       
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, Text property of tabItem2 is initially set to 'tabItem2'" Top="215px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tab runat="server" Text="TestedTab" Top="256px" ID="TestedTab1" TabReference="">
            <TabItems>
                <vt:TabItem runat="server" Text="tabItem1" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem2" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabItem3" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="366px" Height="20px" ID="lblLog2" Width="350px"></vt:Label>
        <vt:Button runat="server" Text="Change Text of tabItem2 to >>" Width="200" Top="421px" ID="btnChangeText" ClickAction="TabTabItemText\btnChangeText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

