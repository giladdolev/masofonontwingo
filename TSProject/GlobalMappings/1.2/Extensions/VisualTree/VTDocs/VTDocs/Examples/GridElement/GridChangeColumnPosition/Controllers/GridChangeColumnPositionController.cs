using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridChangeColumnPositionController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //GridAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Move the grid columns...";

            PrintToLog();

        }

        public void btnMoveColumn1_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            if (TestedGrid.Columns[0].DisplayIndex == 0)
            {
                TestedGrid.ChangeColumnPosition(0, 1);
                lblLog.Text = "ChangeColumnPosition(0, 1) was invoked";
            }

            else if (TestedGrid.Columns[0].DisplayIndex == 1)
            {
                TestedGrid.ChangeColumnPosition(1, 2);
                lblLog.Text = "ChangeColumnPosition(1, 2) was invoked";
            }
            else
            {
                TestedGrid.ChangeColumnPosition(2, 0);
                lblLog.Text = "ChangeColumnPosition(2, 0) was invoked";
            }

            PrintToLog();
        }

        public void btnMoveColumn2_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            if (TestedGrid.Columns[1].DisplayIndex == 0)
            {
                TestedGrid.ChangeColumnPosition(0, 1);
                lblLog.Text = "ChangeColumnPosition(0, 1) was invoked";
            }

            else if (TestedGrid.Columns[1].DisplayIndex == 1)
            {
                TestedGrid.ChangeColumnPosition(1, 2);
                lblLog.Text = "ChangeColumnPosition(1, 2) was invoked";
            }
            else
            {
                TestedGrid.ChangeColumnPosition(2, 0);
                lblLog.Text = "ChangeColumnPosition(2, 0) was invoked";
            }

            PrintToLog();
        }

        public void btnMoveColumn3_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            if (TestedGrid.Columns[2].DisplayIndex == 0)
            {
                TestedGrid.ChangeColumnPosition(0, 1);
                lblLog.Text = "ChangeColumnPosition(0, 1) was invoked";
            }
            else if (TestedGrid.Columns[2].DisplayIndex == 1)
            {
                TestedGrid.ChangeColumnPosition(1, 2);
                lblLog.Text = "ChangeColumnPosition(1, 2) was invoked";
            }
            else
            {
                TestedGrid.ChangeColumnPosition(2, 0);
                lblLog.Text = "ChangeColumnPosition(2, 0) was invoked";
            }

            PrintToLog();
        }

        private void PrintToLog()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text += "\\r\\n\\r\\nColumn1 DisplayIndex: " + TestedGrid.Columns[0].DisplayIndex + "\\r\\nColumn2 DisplayIndex: " + TestedGrid.Columns[1].DisplayIndex + "\\r\\nColumn3 DisplayIndex: " + TestedGrid.Columns[2].DisplayIndex;
        }

    }
}