<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic ListView" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="BasicListView/Form_Load">

        <vt:Label runat="server" SkinID="Title" Text="List View Basic Example" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the list column header visibility by clickin on
            Change Header Visibility button. The current visibility will be displayed in the log.
            ListView.View : Details - Shows the headers, otherwise it will be hidden"
            Top="150px" ID="lblExp2">
        </vt:Label>

        <vt:ListView runat="server" ID="listView2" Top="230px" Height="100px" Width="300px" ItemClickAction="BasicListView/click" ItemCheckAction="BasicListView/check" SelectionChangedAction="BasicListView/chamge" CheckBoxes="true">
            <Columns>
                <vt:ColumnHeader runat="server" Text="�����" ID="test1" Width="50"></vt:ColumnHeader>
            </Columns>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="350px"  ID="lblLog1"></vt:Label>
        
        <vt:Button runat="server" Text="Change Header Visibility" ClickAction="BasicListView\Change_Header_Visibility" Top="380"> </vt:Button>
    </vt:WindowView>
</asp:Content>
