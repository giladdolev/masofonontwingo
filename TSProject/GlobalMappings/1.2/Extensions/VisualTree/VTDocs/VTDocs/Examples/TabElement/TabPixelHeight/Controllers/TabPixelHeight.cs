using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabPixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Tab PixelHeight is: " + TestedTab.PixelHeight + '.';

        }
        public void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            if (TestedTab.PixelHeight == 100)
            {
                TestedTab.PixelHeight = 60;
                lblLog.Text = "Tab PixelHeight is: " + TestedTab.PixelHeight + '.';

            }
            else
            {
                TestedTab.PixelHeight = 100;
                lblLog.Text = "Tab PixelHeight is: " + TestedTab.PixelHeight + '.';
            }

        }
      
    }
}