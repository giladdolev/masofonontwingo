using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "TextBox bounds are: \\r\\n" + TestedTextBox.Bounds;

        }
        public void btnChangeTextBoxBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            if (TestedTextBox.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedTextBox.Bounds = new Rectangle(80, 355, 150, 26);
                lblLog.Text = "TextBox bounds are: \\r\\n" + TestedTextBox.Bounds;

            }
            else
            {
                TestedTextBox.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "TextBox bounds are: \\r\\n" + TestedTextBox.Bounds;
            }

        }

    }
}