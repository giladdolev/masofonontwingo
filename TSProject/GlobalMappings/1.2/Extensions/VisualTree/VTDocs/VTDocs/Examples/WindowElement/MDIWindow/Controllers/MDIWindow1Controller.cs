using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MDIWindow1Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult MDIWindow1()
        {
            return View(new MDIWindow1());
        }

        public void btnControlBox_Click(object sender, EventArgs e)
        {
            var x = (sender as ControlElement).Parent as WindowElement;
            WindowElement win = this.GetRootVisualElement() as WindowElement;
            win.Close();// PerformWindowClosed(new WindowClosedEventArgs());
        }

    }
}