using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");
            LayoutEventArgs args = new LayoutEventArgs(testedRichTextBox, "Some String");

            testedRichTextBox.PerformLayout(args);
        }

        public void testedRichTextBox_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRichTextBox Layout event method is invoked");
        }

    }
}