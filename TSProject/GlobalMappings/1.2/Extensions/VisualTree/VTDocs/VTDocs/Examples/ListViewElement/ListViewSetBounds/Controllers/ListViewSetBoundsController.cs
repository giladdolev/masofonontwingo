using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "The Tested ListView bounds are set to: \\r\\nLeft  " + TestedListView.Left + ", Top  " + TestedListView.Top + ", Width  " + TestedListView.Width + ", Height  " + TestedListView.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            if (TestedListView.Left == 100)
            {
                TestedListView.SetBounds(80, 300, 273, 99);
                lblLog.Text = "The Tested ListView bounds are set to: \\r\\nLeft  " + TestedListView.Left + ", Top  " + TestedListView.Top + ", Width  " + TestedListView.Width + ", Height  " + TestedListView.Height;

            }
            else
            {
                TestedListView.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested ListView bounds are set to: \\r\\nLeft  " + TestedListView.Left + ", Top  " + TestedListView.Top + ", Width  " + TestedListView.Width + ", Height  " + TestedListView.Height;
            }
        }
    }
}