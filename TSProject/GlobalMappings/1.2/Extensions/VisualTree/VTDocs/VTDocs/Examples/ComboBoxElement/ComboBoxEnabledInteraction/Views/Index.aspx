<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Enabled property" ID="windowView1" LoadAction="ComboBoxEnabledInteraction\OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Left="200px" Text="Testing interaction when disabled" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Test:" Top="100px" ID="lblTest"></vt:Label>

        <vt:Label runat="server" Text="Enabled value is set to 'false'
            the ComboBox should not respond to user interaction
            
            Click the 'Change BackColor' button"
            Top="140px" ID="lblExp2">
        </vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" Enabled="false" Top="250px" ID="TestedComboBox"> </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="300px" Height="70px" Width="360px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled value >>" Width="180px" Top="420px" ID="btnChangeEnabled" ClickAction="ComboBoxEnabledInteraction\btnChangeEnabled_Click"></vt:Button>

        <vt:Button runat="server" Text="Change BackColor value >>" Width="180px" Top="420px" Left="300" ID="btnChangeBackColor" ClickAction="ComboBoxEnabledInteraction\btnChangeBackColor_Click"></vt:Button>

        <vt:Label runat="server" Text="Verify when the ComboBox is disabled, and changing the comboBox BackColor, 
            the new BackColor is not shown till the ComboBox is enabled"
            Top="470px" ID="Label1">
        </vt:Label>

    </vt:WindowView>
</asp:Content>
