using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");
            LayoutEventArgs args = new LayoutEventArgs(testedProgressBar, "Some String");

            testedProgressBar.PerformLayout(args);
        }

        public void testedProgressBar_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedProgressBar Layout event method is invoked");
        }

    }
}