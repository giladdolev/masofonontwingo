﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectedRowIndex" LoadAction="GridSelectedRowIndex\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="SelectedRowIndex" Top="15px" ID="lblTitle" Left="300"> </vt:Label>
        <vt:Label runat="server" Text="  Gets the current row index. 
            
            syntax : public int SelectedRowIndex { get; }
            
            If no row has been selected the SelectedRowIndex value is -1."
            Top="65px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="195px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, clicking on one of the grid rows will get the index of the selected 
            row using SelectedRowIndex property. " Top="225px" ID="Label2">
        </vt:Label>
        <vt:Grid runat="server" Top="300px" ID="TestedGrid" SelectionChangedAction="GridSelectedRowIndex\RowSelectionChanged_Click">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog1"></vt:Label>
        
                
    </vt:WindowView>

</asp:Content>
