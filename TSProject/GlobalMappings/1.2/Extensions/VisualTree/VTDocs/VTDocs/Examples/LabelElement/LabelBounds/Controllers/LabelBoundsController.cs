﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelBoundsController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Label bounds are: \\r\\n" + TestedLabel.Bounds;

        }
        public void btnChangeLabelBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            if (TestedLabel.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedLabel.Bounds = new Rectangle(80, 380, 150, 36);
                lblLog.Text = "Label bounds are: \\r\\n" + TestedLabel.Bounds;

            }
            else
            {
                TestedLabel.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "Label bounds are: \\r\\n" + TestedLabel.Bounds;
            }

        }
      
    }
}