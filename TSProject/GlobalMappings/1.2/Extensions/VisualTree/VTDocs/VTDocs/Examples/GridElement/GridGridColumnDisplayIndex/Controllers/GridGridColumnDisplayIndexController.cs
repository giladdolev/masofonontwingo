using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
using System.ComponentModel;
namespace MvcApplication9.Controllers
{
    public class GridGridColumnDisplayIndexController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("testedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            testedGrid.Rows.Add("1", "Shir", "0524244541", true);
            testedGrid.Rows.Add("2", "Michal", "0523454344", false);
            testedGrid.Rows.Add("3", "Maayan", "0526734512", true);
            testedGrid.Rows.Add("3", "Shalom", "0526734512", false);
            testedGrid.Rows.Add("4", "David", "0524144663", true);
            testedGrid.Rows.Add("5", "Eti", "0523548975", true);
            testedGrid.Rows.Add("6", "Dan", "0523434225", false);


            lblLog.Text = "Use the button below to assign a DisplayIndex to one of the columns";
            lblLog.Text += "\\r\\n";

            for (int i = 0; i < testedGrid.Columns.Count; i++)
            {
                lblLog.Text += "\\r\\n" + testedGrid.Columns[i].HeaderText.ToUpper() + " column DisplayIndex = " + testedGrid.Columns[i].DisplayIndex;
            }

        }


        private void btnSetColumnDisplayIndex_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("testedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            ComboBoxElement cmbSelectColumn = this.GetVisualElementById<ComboBoxElement>("cmbSelectColumn");
            ComboBoxElement cmbSelectIndex = this.GetVisualElementById<ComboBoxElement>("cmbSelectIndex");

            GridTextBoxColumn idColumn = this.GetVisualElementById<GridTextBoxColumn>("idColumn");
            GridTextBoxColumn nameColumn = this.GetVisualElementById<GridTextBoxColumn>("nameColumn");
            GridTextBoxColumn telephoneColumn = this.GetVisualElementById<GridTextBoxColumn>("telephoneColumn");
            GridCheckBoxColumn presentColumn = this.GetVisualElementById<GridCheckBoxColumn>("presentColumn");
            
            int inputIndex;
            if (int.TryParse(cmbSelectIndex.Text, out inputIndex))
            {
                if (inputIndex >= 0 && inputIndex < testedGrid.Columns.Count)
                {
                    switch (cmbSelectColumn.Text)
                    {
                        case "#":
                            idColumn.DisplayIndex = inputIndex;
                            lblLog.Text = "Input Column: # (Index=" + idColumn.Index + ");Input DisplayIndex: " + inputIndex ;
                            PrintAllColumnsDisplayIndex();
                            break;
                        case "First Name":
                            nameColumn.DisplayIndex = inputIndex;
                            lblLog.Text = "Input Column: First Name (Index=" + nameColumn.Index + ");Input DisplayIndex: " + inputIndex ;
                            PrintAllColumnsDisplayIndex();
                            break;
                        case "Telephone No.":
                            telephoneColumn.DisplayIndex = inputIndex;
                            lblLog.Text = "Input Column: Telephone No.(Index=" + nameColumn.Index + ");Input DisplayIndex: " + inputIndex ;
                            PrintAllColumnsDisplayIndex();
                            break;
                        case "Present":
                            presentColumn.DisplayIndex = inputIndex;
                            lblLog.Text = "Input Column: Present (Index=" + presentColumn.Index + ");Input DisplayIndex: " + inputIndex ;
                            PrintAllColumnsDisplayIndex();
                            break;
                        default:
                            lblLog.Text = "Please Select a column from the first ComboBox";
                            break;
                    }
                }

                else
                    lblLog.Text = "Please select a Column and DisplayIndex from below ComboBoxes";
            }
            else
                lblLog.Text = "Please select a Column and DisplayIndex from below ComboBoxes";
                 
            
           
        }


        private void testedGrid_ColumnDisplayIndexChanged(object sender, GridColumnMoveEventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement testedGrid = this.GetVisualElementById<GridElement>("testedGrid");

            lblLog.Text = "" + testedGrid.Columns[e.FromIndex].HeaderText.ToUpper() + " column was moved from DisplayIndex " + e.FromIndex + ", \\r\\nto DisplayIndex " + e.ToIndex;
            PrintAllColumnsDisplayIndex();
            
        }

        private void PrintAllColumnsDisplayIndex()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement testedGrid = this.GetVisualElementById<GridElement>("testedGrid");

            lblLog.Text += "\\r\\n";

            for(int i = 0; i < testedGrid.Columns.Count; i++)
            {
                   lblLog.Text += "\\r\\n" + testedGrid.Columns[i].HeaderText.ToUpper() + " column DisplayIndex = " + testedGrid.Columns[i].DisplayIndex;
            }
        }
       
        private void cmbSelectIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbSelectIndex = this.GetVisualElementById<ComboBoxElement>("cmbSelectIndex");

            lblLog.Text = "Input DisplayIndex: " + cmbSelectIndex.Text;
        }

        private void cmbSelectColumn_SelectedIndexChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbSelectColumn = this.GetVisualElementById<ComboBoxElement>("cmbSelectColumn");

            lblLog.Text = "Input column: " + cmbSelectColumn.Text;
        }

    }
}
