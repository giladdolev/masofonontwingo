<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox BackColor Property" ID="windowView1" LoadAction="TextBoxBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:TextBox runat="server" Text="TextBox" Top="140px" ID="TestedTextBox1"></vt:TextBox>
        <vt:Label runat="server" SkinID="Log" Top="180px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this 'TestedTextBox' is initially set to Gray" Top="295px"  ID="lblExp1"></vt:Label>     
        
        <vt:TextBox runat="server" Text="TestedTextBox" BackColor ="Gray" Top="345px" ID="TestedTextBox2"></vt:TextBox>
        <vt:Label runat="server" SkinID="Log" Top="385px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Top="450px" Width="180px" ID="btnChangeBackColor" ClickAction="TextBoxBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
