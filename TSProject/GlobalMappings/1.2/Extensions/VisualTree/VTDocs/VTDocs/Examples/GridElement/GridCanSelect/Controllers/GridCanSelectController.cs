using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridCanSelectController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

		private void button1_Click(object sender, EventArgs e)
		{
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            GridImageColumn a = new GridImageColumn();
            //a.Image = 
            //Create column
            GridColumn col;
            col = new GridColumn("name");
            col.Width = 50;

            //Create row          
            GridRow gridRow = new GridRow();
            GridContentCell gridCellElement1 = new GridContentCell();
            gridCellElement1.Value = "new Content";
            gridRow.Cells.Add(gridCellElement1);

            //Add to grid
            gridElement.Columns.Add(col);
            gridElement.Columns.Add(a);
            gridElement.Rows.Add(gridRow);

            gridElement.RefreshData();
            gridElement.Refresh();
             
		}


        private void button2_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");

            gridElement.CanSelect = !gridElement.CanSelect;

        }


	}
}
