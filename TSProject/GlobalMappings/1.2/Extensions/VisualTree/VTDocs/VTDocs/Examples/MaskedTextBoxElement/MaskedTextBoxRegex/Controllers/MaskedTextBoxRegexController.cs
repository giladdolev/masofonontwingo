using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;
using MvcApplication9.Models;
using System.Web.VisualTree.Utilities;

namespace MvcApplication9.Controllers
{
    public class MaskedTextBoxRegexController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new MaskedTextBoxMask());
        }
        private MaskedTextBoxMask ViewModel
        {
            get { return this.GetRootVisualElement() as MaskedTextBoxMask; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");


            lblLog.Text = "Regex value: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nThis Regex matches all strings that begin with {A and ends with Z}\\r\\n\\r\\nFor example: {A fef# r %fg Z} ";
        }

        public void btnSpecialCharacterTable_Click(object sender, EventArgs e)
        {
            BrowserUtils.OpenWindow("/RegexCharactersURL");
        }

        public void btnSetMask_Click(object sender, EventArgs e)
        {
            ComboBoxElement combo1 = this.GetVisualElementById<ComboBoxElement>("combo1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>
               ("TestedMaskedTextBox");

            switch (combo1.Text)
            {
                case "\\w{3,25}":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches string with 3 to 25 word characters. Can represent a UserName\\r\\n\\r\\nFor example: Ab3";
                    break;
                case "[A-Za-z0-9]+": 
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches strings with one and unlimited times word characters\\r\\n\\r\\nFor example: 7";
                    break;
                case "[0-9A-Za-z]{8,25}":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches string with 8 to 25 word characters. Can represent a password.\\r\\n\\r\\nFor example: 012345abcD";
                    break;
                case "\\d{3,25}":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches strings with 3 to 25 digits\\r\\n\\r\\nFor example: 2574";
                    break;
                case "[cmf]an":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches 'can', 'man' or 'fan'  ";
                    break;
                case "[^ghd]an":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches 3 character string that ends with 'an' and doesn't start with 'g', 'h' or 'd'\\r\\n\\r\\nFor example: ban";
                    break;
                case "waz{3,10}up":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches strings that starts with 'waz' follow by 3 to 10 'z' and ends with 'up'\\r\\n\\r\\nFor example: wazzzzzup";
                    break;
                case "(\\d+)x(\\d+)":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nMatches string that starts with one or more digits follow by the literal 'x' and ends with one or more digits\\r\\n\\r\\nFor example: 52x89";
                    break;
                case "(\\d+)x(\\1)":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nThis Regex contains 2 groups. It Matches string that starts with one or more digits follow by the literal 'x' and ends with same characters of group 1\\r\\n\\r\\nFor example: 52x52";
                    break;
                case "0[xX][0-9a-fA-F]+":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nHexadecimal regular expression\\r\\n\\r\\nFor example: 0x4ab, 0xfff ";
                    break;
                case "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nEmail Address regular expression\\r\\n\\r\\nFor example: d@d.dd";
                    break;
                case "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$":
                    lblLog.Text = "The following predefine Regex was selected: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nURL regular expression.\\r\\n\\r\\nFor example: https://msdn.microsoft.com or msdn.microsoft";
                    break;
                default:
                    lblLog.Text = "Regex property was set with user regular expression: " + TestedMaskedTextBox.Regex + "\\r\\n\\r\\nClick the above button to view regex special characters explanation";
                    break;
            }
        }



        public void btnSetText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>
               ("TestedMaskedTextBox");

            TestedMaskedTextBox.Text = textBox1.Text;
        }

    }
}