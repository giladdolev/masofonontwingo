<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   
     <vt:WindowView runat="server" Text="Window Font Property" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="700px">
		
		<vt:Button runat="server"  TextAlign="MiddleCenter" Text="Change Font" Top="110px" Left="44px" ClickAction="WindowFont\btnChangeFont_Click" ID="btnChangeFont" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button>

		
		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="230px" Left="44px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="200px">
		</vt:TextBox>

		<vt:Label runat="server" Text="Font is initially set with Font-Names='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="9px" Left="41px" ID="label1" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="9.75pt" Height="16px" TabIndex="4" Width="682px">
		</vt:Label>

        </vt:WindowView>
</asp:Content>