using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            ProgressBarElement prgAccessibleRole = this.GetVisualElementById<ProgressBarElement>("prgAccessibleRole");

            MessageBox.Show(prgAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            ProgressBarElement prgAccessibleRole = this.GetVisualElementById<ProgressBarElement>("prgAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (prgAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else if (prgAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                prgAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = prgAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}