using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelLeft value: " + TestedGroupBox.PixelLeft + '.';
        }

        public void btnChangePixelLeft_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedGroupBox.PixelLeft == 80)
            {
                TestedGroupBox.PixelLeft = 180;
                lblLog.Text = "PixelLeft value: " + TestedGroupBox.PixelLeft + '.';
            }
            else
            {
                TestedGroupBox.PixelLeft = 80;
                lblLog.Text = "PixelLeft value: " + TestedGroupBox.PixelLeft + '.';
            }
        }
      
    }
}