using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxLockedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ComboBoxElement TestedComboBox3 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox3");
            LabelElement lblLog3 = this.GetVisualElementById<LabelElement>("lblLog3");

            TestedComboBox1.Items.Add("Item1");
            TestedComboBox1.Items.Add("Item2");
            TestedComboBox1.Items.Add("Item3");
            lblLog1.Text = "Locked Value: " + TestedComboBox1.Locked;
            TestedComboBox2.Items.Add("Item4");
            TestedComboBox2.Items.Add("Item5");
            TestedComboBox2.Items.Add("Item6");
            lblLog2.Text = "Locked Value: " + TestedComboBox2.Locked + ".";
            TestedComboBox3.Items.Add("Item7");
            TestedComboBox3.Items.Add("Item8");
            TestedComboBox3.Items.Add("Item9");
            lblLog3.Text = "Locked Value: " + TestedComboBox3.Locked + ".";
        }     

    }
}