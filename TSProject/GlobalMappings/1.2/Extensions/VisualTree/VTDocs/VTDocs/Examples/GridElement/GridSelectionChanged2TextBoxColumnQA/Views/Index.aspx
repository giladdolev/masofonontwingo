﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectionChanged event" ID="windowView2" Height="750px" LoadAction="GridSelectionChanged2TextBoxColumnQA\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current selection changes.

            Syntax: public event GridElementCellEventHandler SelectionChanged"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells from the grid by clicking with the mouse or by pressing 
            the buttons. Each invocation of the 'BeforeSelectionChanged' and the 'SelectionChanged' events should  
            add a line to the 'Event Log'."
            Top="215px" ID="Label4">
        </vt:Label>

         <vt:Grid runat="server" Top="300px" ID="TestedGrid" BeforeSelectionChangedAction="GridSelectionChanged2TextBoxColumnQA\TestedGrid_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged2TextBoxColumnQA\TestedGrid_SelectionChanged">
            <Columns>
                <vt:GridTextBoxColumn runat="server" ID="GridTextBoxColumn1"  HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-tbCol-1"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridTextBoxColumn2"  HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-tbCol-2"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridTextBoxColumn3"  HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-tbCol-3"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Grid runat="server" Top="300px" Left="380px" RowHeaderVisible="false" ID="TestedGrid2" BeforeSelectionChangedAction="GridSelectionChanged2TextBoxColumnQA\TestedGrid2_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged2TextBoxColumnQA\TestedGrid2_SelectionChanged">
            <Columns>
                <vt:GridTextBoxColumn runat="server" ID="GridTextBoxColumn4"  HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-tbCol-4"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridTextBoxColumn5"  HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-tbCol-5"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridTextBoxColumn6"  HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-tbCol-6"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="430px" height="40px" Width="550px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="490px" Width="550px" Height="140px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select d >>" Top="655px" ID="btnSelectd" ClickAction="GridSelectionChanged2TextBoxColumnQA\btnSelectd_Click"></vt:Button>

        <vt:Button runat="server" Text="Select r >>" Top="655px" Left="250px" ID="btnSelectr" ClickAction="GridSelectionChanged2TextBoxColumnQA\btnSelectr_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Row[0] >>" Top="695px" ID="btnSelectRow0" ClickAction="GridSelectionChanged2TextBoxColumnQA\btnSelectRow0_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Column[0] >>" Top="695px" Left="250px" ID="btnSelectColumn0" ClickAction="GridSelectionChanged2TextBoxColumnQA\btnSelectColumn0_Click"></vt:Button>

        <vt:Button runat="server" Text="Clear Event Log >>" Top="655px" Left="450px" ID="btnClear" ClickAction="GridSelectionChanged2TextBoxColumnQA\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

