using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarBackColorController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar1 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedProgressBar1.BackColor.ToString();

            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedProgressBar2.BackColor.ToString();

        }


        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedProgressBar2.BackColor == Color.Orange)
            {
                TestedProgressBar2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedProgressBar2.BackColor.ToString();
            }
            else
            {
                TestedProgressBar2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedProgressBar2.BackColor.ToString();

            }
        }

    }
}