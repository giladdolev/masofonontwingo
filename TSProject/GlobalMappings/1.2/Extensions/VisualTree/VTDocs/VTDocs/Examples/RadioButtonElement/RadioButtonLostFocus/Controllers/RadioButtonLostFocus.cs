using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonLostFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }



        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Focus and lose focus from the RadioButtons above";
        }

        public void RadioButton1_LostFocus(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nRadioButton1 LostFocus event is invoked";
            lblLog.Text = "RadioButton1 lost the focus";
        }

        public void RadioButton2_LostFocus(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nRadioButton2 LostFocus event is invoked";
            lblLog.Text = "RadioButton2 lost the focus";
        }


        public void btnClear_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text = "Event Log:";
        }


    }
}