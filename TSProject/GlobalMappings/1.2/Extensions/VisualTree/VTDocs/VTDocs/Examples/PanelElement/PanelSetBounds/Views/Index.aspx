<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel SetBounds() Method" ID="windowView" LoadAction="PanelSetBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SetBounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the bounds of the control to the specified location and size.

            Syntax: public void SetBounds(int left, int top, int width, int height)" Top="75px" ID="Label1" ></vt:Label>
                 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="SetBounds Method of this 'TestedPanel' is initially set with 
            Left: 80px, Top: 300px, Width: 200px, Height: 80px" Top="235px"  ID="lblExp1"></vt:Label>     
        
        <vt:Panel runat="server" Text="TestedPanel" Top="300px" ID="TestedPanel"></vt:Panel>
        <vt:Label runat="server" SkinID="Log" Top="395px" Height="40" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="SetBounds >>"  Top="460px" Width="180px" ID="btnSetBounds" ClickAction="PanelSetBounds\btnSetBounds_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

