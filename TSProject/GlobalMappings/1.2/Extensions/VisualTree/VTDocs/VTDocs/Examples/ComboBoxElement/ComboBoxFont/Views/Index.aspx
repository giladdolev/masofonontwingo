<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Font Property" ID="windowView2" LoadAction="ComboBoxFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:ComboBox runat="server" Text="ComboBox" Font-Size="Large" Top="195px" ID="TestedComboBox1" ></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="245px" Width="400px" Height="55px" ID="lblLog1"></vt:Label>
 
 


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedComboBox is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="400px"  ID="lblExp1"></vt:Label>     
        
      
        <vt:ComboBox runat="server" Text="TestedComboBox" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="Small" Top="465px" ID="TestedComboBox2" ></vt:ComboBox>
        <vt:Label runat="server" SkinID="Log" Top="515px" Width="400px" Height="55px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="620px" ID="btnChangeComboBoxFont" ClickAction="ComboBoxFont/btnChangeComboBoxFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
