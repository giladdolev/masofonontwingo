﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowEnableEsc : WindowElement
    {
        WindowEnableEsc2 _frm2;
        WindowEnableEsc3 _frm3;

        public WindowEnableEsc()
		{
		}

        public WindowEnableEsc2 frm2
        {
            get { return this._frm2; }
            set { this._frm2 = value; }
        }
        public WindowEnableEsc3 frm3
        {
            get { return this._frm3; }
            set { this._frm3 = value; }
        }
    }
}