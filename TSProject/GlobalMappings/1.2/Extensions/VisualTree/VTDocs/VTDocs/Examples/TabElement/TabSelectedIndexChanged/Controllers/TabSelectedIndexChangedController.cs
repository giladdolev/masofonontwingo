using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSelectedIndexChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }
        public void TestedTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTabControl 'SelectedIndexChanged' event is invoked";
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnSelecttabItem1_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabItem tabItem1 = this.GetVisualElementById<TabItem>("tabItem1");

            TestedTab.SelectedTab = tabItem1;
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnSelecttabItem2_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            //TabItem tabItem2 = this.GetVisualElementById<TabItem>("tabItem2");

            TestedTab.SelectedIndex = 1;
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnSelecttabItem3_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabItem tabItem3 = this.GetVisualElementById<TabItem>("tabItem3");

            TestedTab.SelectedTab = tabItem3;
            lblLog.Text = "The selected tab is: " + TestedTab.SelectedTab.Text;
        }

        public void btnClearEventLog_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();
            txtEventLog.Text = "Event Log:";
        }
        //public void ChangeSelectedIndex_Click(object sender, EventArgs e)
        //{
        //    TextBoxElement txtExplanation = this.GetVisualElementById<TextBoxElement>("txtExplanation");
        //    TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
        //    TabElement tabSelectedIndex = this.GetVisualElementById<TabElement>("tabControl1");


        //    int index;

        //    if (int.TryParse(textBox1.Text, out index) && index < tabSelectedIndex.TabItems.Count)
        //    {
        //        tabSelectedIndex.SelectedIndex = index;
        //        txtExplanation.Text = "The selected TabIndex is: " + tabSelectedIndex.SelectedIndex;
        //    }
        //    else
        //        txtExplanation.Text = "Enter index number to be selected";

        //}

        //public void tabSelectedIndex_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
        //    txtEventLog.Text += "SelectedIndexChanged Event is invoked\\r\\n";

        //}
    }
}

