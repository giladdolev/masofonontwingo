using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarLeaveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Click on the progress bars...";
        }

        public void TestedProgressBar1_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nProgressBar 1: Leave event is invoked";

            lblLog.Text = "ProgressBar 1 was leaved";
        }

        public void TestedProgressBar2_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nProgressBar 2: Leave event is invoked";

            lblLog.Text = "ProgressBar 2 was leaved";
        }

    }
}