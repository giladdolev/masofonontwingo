﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class ProgressBarEquals : WindowElement
    {

        ProgressBarElement _TestedProgressBar;

        public ProgressBarEquals()
        {
            _TestedProgressBar = new ProgressBarElement();
        }

        public ProgressBarElement TestedProgressBar
        {
            get { return this._TestedProgressBar; }
            set { this._TestedProgressBar = value; }
        }
    }
}