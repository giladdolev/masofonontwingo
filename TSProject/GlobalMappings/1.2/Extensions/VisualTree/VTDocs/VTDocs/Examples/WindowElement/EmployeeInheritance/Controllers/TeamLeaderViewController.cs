using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TeamLeaderViewController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamLeaderView()
        {
            return View();
        }
         public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TestedGrid1.Rows.Add("Einam Cohen", "0508-555-636", "Eni123@gmail.com", "6/8/1974 12:00:00 AM");
            

        }

         public void btnShowTasks_Click(object sender, EventArgs e)
         {
             LabelElement lblAttention2 = this.GetVisualElementById<LabelElement>("lblAttention2");
             TextBoxElement txtList = this.GetVisualElementById<TextBoxElement>("txtList");

             txtList.Text += "\\r\\n\\r\\nMeeting\\r\\n\\r\\nDeliver Final Report\\r\\n\\r\\nCoffee";
             lblAttention2.Text = "6 Team Tasks";

         }

         public void btnShowEinamDesktop_Click(object sender, EventArgs e)
         {
             Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
             argsDictionary.Add("ElementName", "WindowElement");
             argsDictionary.Add("ExampleName", "EmployeeInheritance");
             WindowElement baseView = VisualElementHelper.CreateFromView<WindowElement>("Employee", "EmployeeView", null, argsDictionary, null);
             baseView.Show();
         }
       

        //public void buttonOK_Click(object sender, EventArgs e)
        //{
        //    LabelElement label = this.GetVisualElementById<LabelElement>("description");
        //    label.Text = "Changed by inherited view";
        //}

        //public void ShowBase_Click(object sender, EventArgs e)
        //{
        //    Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
        //    argsDictionary.Add("ElementName", "WindowElement");
        //    argsDictionary.Add("ExampleName", "EmployeeInheritance");
        //    WindowElement baseView = VisualElementHelper.CreateFromView<WindowElement>("EmployeeView", "EmployeeView", null, argsDictionary, null);
        //    baseView.ShowDialog();
          
        //}     

    }
}