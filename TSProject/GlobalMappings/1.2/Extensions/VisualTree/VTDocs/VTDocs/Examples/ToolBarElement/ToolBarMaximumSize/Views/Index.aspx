<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="ToolBar MaximumSize" ID="windowView1" LoadAction="ToolBarMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedToolBar' is width: 400px and height: 80px. 
            Its MaximumSize is initially set to width: 380px and height: 60px.
            To cancel 'TestedToolBar' MaximumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>

        <vt:ToolBar runat="server" Dock="None" Left="80px" Height="80px" Width="400px" Text="TestedToolBar" ID="TestedToolBar" Top="360px" MaximumSize="380, 60"></vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="455px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="540px" width="200px" Left="80px" ID="btnChangeTlbMaximumSize" ClickAction="ToolBarMaximumSize\btnChangeTlbMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="540px" width="200px" Left="310px" ID="btnSetToZeroTlbMaximumSize" ClickAction="ToolBarMaximumSize\btnSetToZeroTlbMaximumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>
























