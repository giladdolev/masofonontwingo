<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ParentController="BaseView" ParentArguments="ElementName|WindowElement|ExampleName|Inheritance" MaximizeBox="True" MinimizeBox="True" Text="Inherited View" ID="WindowView1" Top="57px" Left="11px" Height="800px" Width="700px">
        <vt:Panel runat="server" ID="content">
            <vt:Label runat="server" ID="description" Text="Inherited Label" Top="20" Left="30" Font-Bold="true"></vt:Label>
            <vt:TextBox runat="server" Top="200" Left="30" Width="300"></vt:TextBox>
            <vt:Button runat="server" Top="250" Left="30" Text="Show base view" ClickAction="InheritedView\ShowBase_Click"></vt:Button>
        </vt:Panel>
        <vt:Button runat="server" ID="buttonOK" Text="Execute" ClickAction="InheritedView\buttonOK_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
