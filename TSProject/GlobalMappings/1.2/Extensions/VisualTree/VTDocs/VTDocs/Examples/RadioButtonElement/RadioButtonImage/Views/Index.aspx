<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton Image Property" ID="windowView2" LoadAction="RadioButtonImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Image" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the image that is displayed on a RadioButton control.
            The default value is null.
            
            Syntax: public Image Image { get; set; }" Top="75px" ID="lblDefinition" ></vt:Label>                

        <vt:RadioButton runat="server" Text="RadioButton" Top="155px" ID="btnTestedRadioButton1" ></vt:RadioButton>
        <vt:Label runat="server" SkinID="Log" Top="195px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="260px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Image property of this 'TestedRadioButton' is initially set to Button.png" Top="310px"  ID="lblExp1"></vt:Label>     
        
        <vt:RadioButton runat="server" Text="TestedRadioButton" Image ="~/Content/Elements/Button.png" Top="360px" ID="btnTestedRadioButton2"></vt:RadioButton>
        <vt:Label runat="server" SkinID="Log" Top="400px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Image Value >>"  Top="465px" Width="180px" ID="btnChangeImage" ClickAction="RadioButtonImage\btnChangeImage_Click"></vt:Button>
             
    </vt:WindowView>
</asp:Content>

