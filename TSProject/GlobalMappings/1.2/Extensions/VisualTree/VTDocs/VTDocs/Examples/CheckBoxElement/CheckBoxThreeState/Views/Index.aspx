<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox ThreeState Property" ID="windowView1" LoadAction="CheckBoxThreeState\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ThreeState" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the CheckBox will allow three check states rather than two.           

            Syntax: public bool ThreeState { get; set; }
            Property Values: true if the CheckBox is able to display three check states; otherwise, false. 
                             The default value is false."
            Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:CheckBox runat="server" Text="CheckBox" Top="195px" ID="TestedCheckBox1" CheckStateChangedAction="CheckBoxThreeState\CheckBox_CheckStateChanged"></vt:CheckBox>           

        <vt:Label runat="server" SkinID="Log" Top="235px" Height="40px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="ThreeState property of this 'TestedCheckBox' is initially set to 'true'.
            When ThreeState property sets to true we have 3 states :
            Checked/Indeterminate/Unchecked." Top="375px" ID="lblExp1"></vt:Label> 

        <vt:CheckBox runat="server" Text="TestedCheckBox" Top="460px"  ID="TestedCheckBox2" ThreeState="true" CheckStateChangedAction="CheckBoxThreeState\TestedCheckBox_CheckStateChanged"></vt:CheckBox>
        <vt:Label runat="server" SkinID="Log" Top="495px" Height="40px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ThreeState Value >>" Top="580px" ID="btnChangeThreeState" Width="180px" ClickAction="CheckBoxThreeState\btnChangeThreeState_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
