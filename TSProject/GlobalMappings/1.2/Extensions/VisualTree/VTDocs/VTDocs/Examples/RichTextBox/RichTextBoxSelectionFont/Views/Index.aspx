<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox SelectionFont Property" ID="windowView2" LoadAction="RichTextBoxSelectionFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionFont" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the current text selection or insertion point.
            
            Syntax: public Font SelectionFont { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="In the following RichTexBox you can change the SelectionFont value to text selection or to text 
             entered after the insertion point by pressing the 'Change SelectionFont Value' button." Top="290px"  ID="lblExp1"></vt:Label>     
        
      
        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="360px" Width="430px" ID="TestedRichTextBox" SelectionFont="Miriam Fixed" TextChangedAction="RichTextBoxSelectionFont/TestedRichTextBox_TextChanged"></vt:RichTextBox>
        <vt:Label runat="server" SkinID="Log" Top="455px" Width="430px" Height="80px" ID="lblLog"></vt:Label>
       
         <vt:Button runat="server" Text="Change SelectionFont Value >>" Top="590px" Width="180px" ID="btnChangeSelectionFont" ClickAction="RichTextBoxSelectionFont/btnChangeSelectionFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

