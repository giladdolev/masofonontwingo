using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowWindowStateController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowWindowState());
        }
        private WindowWindowState ViewModel
        {
            get { return this.GetRootVisualElement() as WindowWindowState; }
        }

        public void btnWindowStateNormal_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            this.ViewModel.WindowState = WindowState.Normal;
            textBox1.Text = "WindowState value:" + this.ViewModel.WindowState;
        }
        public void btnWindowStateMinimized_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            this.ViewModel.WindowState = WindowState.Minimized;
            textBox1.Text = "WindowState value:" + this.ViewModel.WindowState;
        }
        public void btnWindowStateMaximized_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            this.ViewModel.WindowState = WindowState.Maximized;
            textBox1.Text = "WindowState value:" + this.ViewModel.WindowState;
        }
    }
}