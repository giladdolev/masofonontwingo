using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        private void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedRadioButton.Size;
        }

        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRadioButton.Size == new Size(300, 80))   //Get
            {
                TestedRadioButton.Size = new Size(150, 26);    //Set
                lblLog.Text = "Size value: " + TestedRadioButton.Size;
            }
            else
            {
                TestedRadioButton.Size = new Size(300, 80);
                lblLog.Text = "Size value: " + TestedRadioButton.Size;
            }
        }

    }
}