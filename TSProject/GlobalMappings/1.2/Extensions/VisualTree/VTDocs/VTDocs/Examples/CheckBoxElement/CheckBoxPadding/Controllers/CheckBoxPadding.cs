using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPaddingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangePadding_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkRuntimePadding = this.GetVisualElementById<CheckBoxElement>("chkRuntimePadding");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (chkRuntimePadding.Padding == new Padding(0, 0, 0, 0)) //Get
            {
                chkRuntimePadding.Padding = new Padding(0, 0, 0, 30); //Set
                textBox1.Text = "TestedCheckBox1 Padding" + chkRuntimePadding.Padding;
            }
            else if (chkRuntimePadding.Padding == new Padding(0, 0, 0, 30))
            {
                chkRuntimePadding.Padding = new Padding(0, 0, 30, 0); //Set
                textBox1.Text = "TestedCheckBox1 Padding" + chkRuntimePadding.Padding;
            }
            else if (chkRuntimePadding.Padding == new Padding(0, 0, 30, 0)) //Get
            {
                chkRuntimePadding.Padding = new Padding(0, 30, 0, 0); //Set
                textBox1.Text = "TestedCheckBox1 Padding" + chkRuntimePadding.Padding;
            }
            else if (chkRuntimePadding.Padding == new Padding(0, 30, 0, 0)) //Get
            {
                chkRuntimePadding.Padding = new Padding(30, 0, 0, 0); //Set
                textBox1.Text = "TestedCheckBox1 Padding" + chkRuntimePadding.Padding;
            }
            else if (chkRuntimePadding.Padding == new Padding(30, 0, 0, 0)) //Get
            {
                chkRuntimePadding.Padding = new Padding(0, 0, 0, 0); //Set
                textBox1.Text = "TestedCheckBox1 Padding" + chkRuntimePadding.Padding;
            }

        }

        public void btnGetPadding_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkPadding = this.GetVisualElementById<CheckBoxElement>("chkPadding");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            textBox1.Text = "CheckBox2 Padding" + chkPadding.Padding;
        }

    }
}