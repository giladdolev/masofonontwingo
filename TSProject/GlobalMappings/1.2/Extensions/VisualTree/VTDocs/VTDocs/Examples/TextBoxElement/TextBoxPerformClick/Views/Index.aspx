<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:TextBox runat="server" Text="Tested TextBox" Top="150px" Left="200px" ID="TestedTextBox" Height="36px" Width="200px" ClickAction="TextBoxPerformClick\TestedTextBox_Click"></vt:TextBox>

        <vt:Button runat="server" Text="PerformClick of 'Tested TextBox'" Top="45px" Left="140px" ID="btnPerformClick" Height="36px" Width="300px" ClickAction="TextBoxPerformClick\btnPerformClick_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
