<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="LinkLabel MaximumSize" ID="windowView1" LoadAction="LinkLabelMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedLinkLabel' is width: 300px and height: 40px. 
            Its MaximumSize is initially set to width: 200px and height: 30px.
            To cancel 'TestedLinkLabel' MaximumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>

        <vt:LinkLabel runat="server" SkinID="BackColorLinkLabel" Left="80px" Height="40px" Width="300px" BackColor="#f2f2f2" Text="TestedLinkLabel" ID="TestedLinkLabel" MaximumSize="200, 30" Top="360px" AutoSize="false"></vt:LinkLabel>

        <vt:Label runat="server" SkinID="Log" Top="415px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="495px" width="200px" Left="80px" ID="btnChangeMaximumSize" ClickAction="LinkLabelMaximumSize\btnChangeMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="495px" width="200px" Left="310px" ID="btnSetToZeroMaximumSize" ClickAction="LinkLabelMaximumSize\btnSetToZeroMaximumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>
























