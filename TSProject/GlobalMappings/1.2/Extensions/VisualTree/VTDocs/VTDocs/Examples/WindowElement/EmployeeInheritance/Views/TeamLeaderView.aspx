<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" LoadAction="TeamLeaderView\OnLoad" ParentController="Employee" ParentAction="EmployeeView" ParentArguments="ElementName|WindowElement|ExampleName|EmployeeInheritance" Text="Team Leader" BackColor="Purple" ID="WindowView1" >

        <vt:Label runat="server" Text="TeamLeader Desktop" ID="lblTitle"></vt:Label>

        <vt:Tab runat="server" ID="TabControl">
            
                          
                <TabItems>
                <vt:TabItem runat="server" Text="Management" ID="ManagementTab">

                    <vt:Label runat="server" Font-Bold="true" Left="380px" Top="20px" Font-Size="19pt" Text="Employee List" ID="lblEmployeeList"></vt:Label> 

                    <vt:Grid runat="server" Left="25px" Width="620px" Height="70px" Top="60px" ID="TestedGrid1">
                        <Columns>
                            <vt:GridTextBoxColumn runat="server" ID="FullNameCol" HeaderText="Full Name" Height="20px" Width="130px"></vt:GridTextBoxColumn>
                            <vt:GridTextBoxColumn runat="server" ID="PhoneCol" HeaderText="Phone" Height="20px" Width="130px"></vt:GridTextBoxColumn>
                            <vt:GridTextBoxColumn runat="server" ID="EmailCol" HeaderText="Email" Height="20px" Width="200px"></vt:GridTextBoxColumn>
                            <vt:GridDateTimePickerColumn runat="server" ID="DateOfBirthCol" HeaderText="Date Of Birth" Height="20px" Width="130px"></vt:GridDateTimePickerColumn>
                            
                            
                        </Columns>
                    </vt:Grid>
                    <vt:Button runat="server" ID="btnShowEinamDesktop" SkinID="Wide"  Width="130px" Top="95px" Left="660px" Text="Show Desktop" ClickAction="TeamLeaderView\btnShowEinamDesktop_Click"></vt:Button>
                </vt:TabItem>

            </TabItems>
            
                   

        </vt:Tab>

         <vt:Button runat="server" ID="btnShowTasks" Text="Show All Tasks" ClickAction="TeamLeaderView\btnShowTasks_Click"></vt:Button>

                    <vt:Label runat="server" ID="lblAttention2" Text="6 Team Tasks" BackColor="Purple"></vt:Label>
    </vt:WindowView>
</asp:Content>
