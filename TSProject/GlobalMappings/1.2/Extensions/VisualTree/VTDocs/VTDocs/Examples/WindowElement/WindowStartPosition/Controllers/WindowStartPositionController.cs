using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowStartPositionController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowStartPosition());
        }
        private WindowStartPosition ViewModel
        {
            get { return this.GetRootVisualElement() as WindowStartPosition; }
        }

        public void WindowLoad()
        {

            this.ViewModel.i = 1;

           

        }

        public void btnOpenForm2_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowStartPosition");

            switch (this.ViewModel.i)
            {

                //WindowStartPosition is notr implemented

                case 1:
                    ViewModel.TestedWin1 = VisualElementHelper.CreateFromView<Win1>("Win1", "Index1", null, argsDictionary, null);
                    ViewModel.TestedWin1.StartPosition = System.Web.VisualTree.WindowStartPosition.CenterParent;
                    ViewModel.TestedWin1.Show();
                    textBox1.Text = "StartPosition value:" + ViewModel.TestedWin1.StartPosition;
                    increment_i();
                    break;
                case 2:
                    ViewModel.TestedWin2 = VisualElementHelper.CreateFromView<Win2>("Win2", "Index2", null, argsDictionary, null);
                    ViewModel.TestedWin2.StartPosition = System.Web.VisualTree.WindowStartPosition.CenterScreen;
                    ViewModel.TestedWin2.Show();
                    textBox1.Text = "StartPosition value:" + ViewModel.TestedWin2.StartPosition;
                    increment_i();
                    break;
                case 3:
                    ViewModel.TestedWin3 = VisualElementHelper.CreateFromView<Win3>("Win3", "Index3", null, argsDictionary, null);
                    ViewModel.TestedWin3.StartPosition = System.Web.VisualTree.WindowStartPosition.Manual;
                    ViewModel.TestedWin3.Show();
                    textBox1.Text = "StartPosition value:" + ViewModel.TestedWin3.StartPosition;
                    increment_i();
                    break;
                case 4:
                    ViewModel.TestedWin4 = VisualElementHelper.CreateFromView<Win4>("Win4", "Index4", null,argsDictionary, null);
                    ViewModel.TestedWin4.StartPosition = System.Web.VisualTree.WindowStartPosition.WindowsDefaultBounds;
                    ViewModel.TestedWin4.Show();
                    textBox1.Text = "StartPosition value:" + ViewModel.TestedWin4.StartPosition;
                    increment_i();
                    break;
                case 5:
                    ViewModel.TestedWin5 = VisualElementHelper.CreateFromView<Win5>("Win5", "Index5", null, argsDictionary, null);
                    ViewModel.TestedWin5.StartPosition = System.Web.VisualTree.WindowStartPosition.WindowsDefaultLocation;
                    ViewModel.TestedWin5.Show();
                    textBox1.Text = "StartPosition value:" + ViewModel.TestedWin5.StartPosition;
                    increment_i();
                    break;
                default:
                    MessageBox.Show("Default case");
                    break;
            }
        }

        private void increment_i()
        {
            if (this.ViewModel.i == 5)
                this.ViewModel.i = 1;
            else
                this.ViewModel.i++;

        }
    }
}