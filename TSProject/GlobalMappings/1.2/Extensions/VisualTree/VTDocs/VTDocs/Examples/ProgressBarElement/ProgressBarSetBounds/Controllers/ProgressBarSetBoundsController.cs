using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //ProgressBarAlignment
        
        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested ProgressBar bounds are set to: \\r\\nLeft  " + TestedProgressBar.Left + ", Top  " + TestedProgressBar.Top + ", Width  " + TestedProgressBar.Width + ", Height  " + TestedProgressBar.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            if (TestedProgressBar.Left == 100)
            {
                TestedProgressBar.SetBounds(80, 300, 250, 40);
                lblLog.Text = "The Tested ProgressBar bounds are set to: \\r\\nLeft  " + TestedProgressBar.Left + ", Top  " + TestedProgressBar.Top + ", Width  " + TestedProgressBar.Width + ", Height  " + TestedProgressBar.Height;

            }
            else
            {
                TestedProgressBar.SetBounds(100, 280, 300, 50);
                lblLog.Text = "The Tested ProgressBar bounds are set to: \\r\\nLeft  " + TestedProgressBar.Left + ", Top  " + TestedProgressBar.Top + ", Width  " + TestedProgressBar.Width + ", Height  " + TestedProgressBar.Height;
            }
        }
    }
}