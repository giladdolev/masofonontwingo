using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonResetTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "RadioButton Text is set";
        }

        //RadioButtonAlignment
        public void btnResetText_Click(object sender, EventArgs e)
        {

            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedRadioButton.ResetText();
            lblLog.Text = "RadioButton Text is empty";
        }

        public void btnSetText_Click(object sender, EventArgs e)
        {

            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedRadioButton.Text = "TestedRadioButton";
            lblLog.Text = "RadioButton Text is set";
        }

    }
}