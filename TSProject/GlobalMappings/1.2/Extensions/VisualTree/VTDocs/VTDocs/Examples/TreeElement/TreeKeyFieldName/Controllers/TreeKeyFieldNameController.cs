using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Data;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeKeyFieldNameController : Controller
    {
        
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            
            TreeElement tree1 = this.GetVisualElementById<TreeElement>("tree1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            DataTable source = new DataTable();
            source.Columns.Add("ID", typeof(string));
            source.Columns.Add("ParentID", typeof(string));
            source.Columns.Add("Department", typeof(string));
            source.Columns.Add("Location", typeof(string));
            source.Columns.Add("Phone", typeof(string));

            source.Rows.Add("1", "", "Corporate Headquarters", "Monterey", "(408) 555-1234");
            source.Rows.Add("2", "1", "Sales and Marketing", "San Francisco", "(415) 555-1234");
            source.Rows.Add("3", "2", "Field Office: Canada", "Toronto", "(416) 677-1000");
            source.Rows.Add("4", "2", "Field Office: East Coast", "Boston", "(617) 555-4234");
            source.Rows.Add("5", "", "Finance", "Monterey", "(408) 555-1234");
            source.Rows.Add("6", "", "Engineering", "Monterey", "(408) 555-1234");
            source.Rows.Add("7", "6", "Software Products Div.", "Monterey", "(408) 555-1234");
            source.Rows.Add("8", "6", "Software Development", "Monterey", "(408) 555-1234");

            tree1.ParentFieldName = "ParentID";
            tree1.KeyFieldName = "ID";
            tree1.DataSource = source;

            lblLog.Text = "Click the button...";
        }


        public void btnSwitchDataSource_Click(object sender, EventArgs e)
        {

            TreeElement tree1 = this.GetVisualElementById<TreeElement>("tree1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (tree1.KeyFieldName == "ID")
            {
                DataTable source = new DataTable();
                source.Columns.Add("IDCol", typeof(string));
                source.Columns.Add("ParentIDCol", typeof(string));
                source.Columns.Add("Full Name", typeof(string));
                source.Columns.Add("Job Title", typeof(string));
                source.Columns.Add("City", typeof(string));


                source.Rows.Add("1", "", "Sara Ragas", "Information Services Manager", "Tulsa");
                source.Rows.Add("2", "1", "Nicholas Kleinsorge", "Database Administrator", "Aurora");
                source.Rows.Add("3", "1", "Declan Kears", "Application Specialist", "West");
                source.Rows.Add("4", "1", "Armando Ordway", "Application Specialist", "Longview");
                source.Rows.Add("5", "", "Randy Pollom", "Marketing Manager", "Martinez");
                source.Rows.Add("6", "5", "Julia Lagrave", "Marketing Specialist", "Fresno");
                source.Rows.Add("7", "5", "Huyen Trinklein", "Marketing Specialist", "Fresno");
                source.Rows.Add("8", "5", "Lyric Margaris", "Marketing Specialist", "Mantua");
                source.Rows.Add("9", "5", "Nihal Leino", "Marketing Assistant", "Gahanna");
                source.Rows.Add("10", "", "Harshita Kerl", "Chief Financial Officer", "Atlanta");
                source.Rows.Add("11", "10", "Molla Conneely", "Human Resources Manager", "Union");
                source.Rows.Add("12", "11", "Groves Lindos", "Human Resources Administrative Assistant", "Granby");
                source.Rows.Add("13", "11", "Jacoby Golombecki", "Recruiter", "Medford");
                source.Rows.Add("14", "11", "Brooks Orsburn", "Recruiter", "Way");

                tree1.ParentFieldName = "ParentIDCol";
                tree1.KeyFieldName = "IDCol";
                tree1.DataSource = source;

                lblLog.Text = "DataSource was switched to Employee records";
            }
            else
            {
                DataTable source = new DataTable();
                source.Columns.Add("ID", typeof(string));
                source.Columns.Add("ParentID", typeof(string));
                source.Columns.Add("Department", typeof(string));
                source.Columns.Add("Location", typeof(string));
                source.Columns.Add("Phone", typeof(string));

                source.Rows.Add("1", "", "Corporate Headquarters", "Monterey", "(408) 555-1234");
                source.Rows.Add("2", "1", "Sales and Marketing", "San Francisco", "(415) 555-1234");
                source.Rows.Add("3", "2", "Field Office: Canada", "Toronto", "(416) 677-1000");
                source.Rows.Add("4", "2", "Field Office: East Coast", "Boston", "(617) 555-4234");
                source.Rows.Add("5", "", "Finance", "Monterey", "(408) 555-1234");
                source.Rows.Add("6", "", "Engineering", "Monterey", "(408) 555-1234");
                source.Rows.Add("7", "6", "Software Products Div.", "Monterey", "(408) 555-1234");
                source.Rows.Add("8", "6", "Software Development", "Monterey", "(408) 555-1234");

                tree1.ParentFieldName = "ParentID";
                tree1.KeyFieldName = "ID";
                tree1.DataSource = source;

                lblLog.Text = "DataSource was switched to Profession records";
            }
            

        }

      
    }
}