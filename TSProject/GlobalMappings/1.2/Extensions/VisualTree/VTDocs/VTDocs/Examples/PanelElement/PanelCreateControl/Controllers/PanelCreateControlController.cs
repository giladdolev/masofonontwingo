using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new PanelCreateControl());

        }

        private PanelCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as PanelCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement newPanel = new PanelElement();

            newPanel.CreateControl();
            this.ViewModel.Controls.Add(newPanel);
            
            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}