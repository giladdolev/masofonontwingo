using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");
            LayoutEventArgs args = new LayoutEventArgs(testedPanel, "Some String");

            testedPanel.PerformLayout(args);
        }

        public void testedPanel_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedPanel Layout event method is invoked");
        }

    }
}