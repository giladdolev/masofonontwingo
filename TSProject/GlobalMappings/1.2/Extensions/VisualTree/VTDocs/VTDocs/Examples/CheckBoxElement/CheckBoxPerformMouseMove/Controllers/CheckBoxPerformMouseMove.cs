using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformMouseMoveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseMove_Click(object sender, EventArgs e)
        {
            
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");

            MouseEventArgs args = new MouseEventArgs();

            testedCheckBox.PerformMouseMove(args);
        }

        public void testedCheckBox_MouseMove(object sender, EventArgs e)
        {
            MessageBox.Show("TestedCheckBox MouseMove event method is invoked");
        }

    }
}