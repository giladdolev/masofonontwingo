<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="Grid ExpandOnFocus Property" ID="windowView1" Height="780px" LoadAction="GridGridComboBoxColumnExpandOnFocus\OnLoad">


        <vt:Label runat="server" SkinID="Title" Text="ExpandOnFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or Sets a value indicating whether the control is expanded on focus.

            Syntax: public bool ExpandOnFocus { get; set; }
            'true' if the control  is expanded on focus; otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="In the following example the columns 'Options' and 'Gender' are comboBox columns.
              'Options' column ExpandOnFocus property is set to 'true'.
              'Gender' column ExpandOnFocus property isn't set. " Top="250px" ID="lblExp1"></vt:Label>

        <!-- Tested Grid -->
        <vt:Grid runat="server" Top="335px" Height="140px" Width="480px" ID="TestedGrid">
              <Columns>
                 <vt:GridComboBoxColumn  runat="server" ID="OptionsColumn1" ExpandOnFocus="true" HeaderText="Options" Height="20px" Width="90px" ></vt:GridComboBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="IDColumn1" HeaderText="ID" Height="20px" Width="60px"> </vt:GridTextBoxColumn>
                 <vt:GridComboBoxColumn  runat="server" ID="GenderColumn1" HeaderText="Gender" ExpandOnFocus="true" Height="20px" Width="90px" ></vt:GridComboBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="NameColumn1" HeaderText="Name" Height="20px" Width="80px"> </vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="AttendanceColumn1" HeaderText="Attendance" Height="20px" Width="110px" ></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log"  Width="480px" Top="530px" Height="40px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Options Column ExpandOnFocus value >>" Top="620px" Width="270px" ID="btnChangeOptionsColExpandOnFocus" ClickAction="GridGridComboBoxColumnExpandOnFocus\btnChangeOptionsColExpandOnFocus_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Gender Column ExpandOnFocus value >>" Top="620px" Left="390px" Width="270px" ID="btnChangeGenderColExpandOnFocus" ClickAction="GridGridComboBoxColumnExpandOnFocus\btnChangeGenderColExpandOnFocus_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
        