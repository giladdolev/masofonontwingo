 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton ToString Method" ID="windowView2" LoadAction="RadioButtonToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a A string representation of the RadioButton control.

            Syntax: public override string ToString()
            
            Return Value: A string representation of the RadioButton that indicates whether it is checked" 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this RadioButton." 
            Top="265px" ID="lblExp2"></vt:Label>

        <vt:RadioButton runat="server" Text="TestedRadioButton" Top="330px" ID="TestedRadioButton"></vt:RadioButton>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="370px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="475px" ID="btnToString" ClickAction="RadioButtonToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>