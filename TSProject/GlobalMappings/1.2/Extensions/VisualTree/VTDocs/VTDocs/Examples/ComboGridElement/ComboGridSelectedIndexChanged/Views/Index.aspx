<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid SelectedIndexChanged event" ID="windowView1" LoadAction="ComboGridSelectedIndexChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectedIndexChanged" Left="250px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the SelectedIndex property has changed.

            Syntax: public event EventHandler SelectedIndexChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can change the combogrid SelectedIndex value by selecting 
            a line or a cell from the dropdown grid or by pressing the 'Change SelectedIndex Value' button.
            pressing the button will set the SelectedIndex property, each click selects a different item, in a cyclic order.
            Each invocation of the 'SelectedIndexChanged' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-test-cmbgrid" Top="355px" ID="TestedComboGrid" SelectedIndexChangeAction="ComboGridSelectedIndexChanged\TestedComboGrid_SelectedIndexChanged"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Left="80px" Height="60px" Top="395px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="485px" Width="360px" Height="130px" ID="txtEventLog"></vt:TextBox>  
        
        <vt:Button runat="server" Text="Change SelectedIndex Value >>" Top="645px" ID="btnChooseNext" Width="180px" ClickAction="ComboGridSelectedIndexChanged\ChooseNext_Click"></vt:Button>     
       
    </vt:WindowView>
</asp:Content>
