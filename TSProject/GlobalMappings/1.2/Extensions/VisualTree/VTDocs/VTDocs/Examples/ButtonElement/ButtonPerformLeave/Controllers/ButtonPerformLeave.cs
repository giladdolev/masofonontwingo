using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPerformLeaveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLeave_Click(object sender, EventArgs e)
        {
            
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");

            btnTestedButton.PerformLeave(e);
        }

        public void btnTestedButton_Leave(object sender, EventArgs e)
        {
            MessageBox.Show("TestedButton Leave event method is invoked");
        }

    }
}