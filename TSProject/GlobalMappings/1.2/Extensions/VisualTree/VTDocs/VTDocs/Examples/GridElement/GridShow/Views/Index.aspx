<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Show Method" ID="windowView1" LoadAction="GridShow\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Show" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Displays the control to the user.

            Syntax: public void Show()
            
            Showing the control is equivalent to setting the Visible property to 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the Visible property is initially set to 'false'. 
            When invoking Show() method, the Visible property value is changed to 'true'." Top="290px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="360px" Visible="false" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="490px"></vt:Label>

        <vt:Button runat="server" Text="Show >>" Top="565px" ID="btnShow" ClickAction="GridShow\btnShow_Click"></vt:Button>

        <vt:Button runat="server" Text="Hide >>" Left="300" Top="565px" ID="btnHide" ClickAction="GridShow\btnHide_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
