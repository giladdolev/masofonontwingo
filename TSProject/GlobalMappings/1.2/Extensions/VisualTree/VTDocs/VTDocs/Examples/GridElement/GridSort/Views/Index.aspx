﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="Grid Sort Method" LoadAction="GridSort\Page_load">
        <vt:Label runat="server" SkinID="Title" Text="Sort" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sorts the contents of the GridElement control in ascending or descending order based on the contents 
            of the specified column.         

            Syntax : public void Sort(GridColumn grdColumn, SortOrder sortOrder)
            grdColumn - The main column to sort by 
            sortOrder - The order you want to sort by, ascending or descending order"
            Top="75px" ID="lblDefinition">
        </vt:Label>
                
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px" ID="lblExample"></vt:Label>
           
        <vt:Label runat="server" Text="In the following example, you can click on one of the columns header to enter a sorting mode 
            and than each click will toggle between ascending and descending order. 
            You can also press the 'Sort Column 1' button to toggle the first column sort order."
            Top="270px"></vt:Label>

        <vt:Grid runat="server" Top="350px" Left="80px" ID="TestedGrid1" SortableColumns="true" ColumnHeaderMouseClickAction="GridSort\TestedGrid_HeaderClick">
             <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px" DataMember="column1"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>      
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="480px" ID="lblLog1"></vt:Label>

        <vt:Button runat="server" ID="btn1" Text="Sort Column 1 >>" ClickAction="GridSort\btn_click" Top="545px"></vt:Button>
    </vt:WindowView>

</asp:Content>
