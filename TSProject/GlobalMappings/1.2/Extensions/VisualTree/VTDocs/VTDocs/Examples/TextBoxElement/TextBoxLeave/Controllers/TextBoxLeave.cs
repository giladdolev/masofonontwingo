using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxLeaveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Fill up the form...";
        }

        public void txtFirstName_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTextBox: FirstName, Leave event is invoked";

            lblLog.Text = "FirstName TextBox was leaved";
        }

        public void txtFirstName_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            string key = "";

            txtFirstName.ForeColor = Color.Black;

            if (txtFirstName.Text.IndexOf("Enter First Name") == 0)
            {
                key = txtFirstName.Text.Substring(16);
                txtFirstName.Clear();
                txtFirstName.Text = key;
            }
            else if (txtFirstName.Text.IndexOf("Enter First Nam") == 0)
            {
                txtFirstName.Clear();
            }
        }

        public void txtLastName_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTextBox: LastName, Leave event is invoked";

            lblLog.Text = "LastName TextBox was leaved";
        }

        public void txtLastName_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            string key = "";

            txtLastName.ForeColor = Color.Black;

            if (txtLastName.Text.IndexOf("Enter Last Name") == 0)
            {
                key = txtLastName.Text.Substring(14);
                txtLastName.Clear();
                txtLastName.Text = key;
            }
            else if (txtLastName.Text.IndexOf("Enter Last Nam") == 0)
            {
                txtLastName.Clear();
            }
        }
    }
}