<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer GotFocus event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

         <vt:Label runat="server" Text="Focus the SplitContainer by using the keyboard or Mouse to invoke GotFocus event method" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="500px"></vt:Label>

        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="50px" Left="200px" ID="splGotFocus" Height="36px"  Width="200px" GotFocusAction ="SplitContainerGotFocus\splGotFocus_GotFocus"></vt:SplitContainer>          


        <vt:Label runat="server" Text="Event Log" Top="135px" Left="140px" Font-Bold="true" Multiline="true" ID="Label2" Height="15px" Width="250px"></vt:Label>

        <vt:TextBox runat="server" Text="" Top="150px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
