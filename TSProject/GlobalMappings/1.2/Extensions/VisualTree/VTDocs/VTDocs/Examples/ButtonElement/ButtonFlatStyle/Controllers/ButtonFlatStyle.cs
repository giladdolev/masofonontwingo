using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonFlatStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnFlatStyle_Click(object sender, EventArgs e)
        {
            ButtonElement button1 = this.GetVisualElementById<ButtonElement>("btnFlatStyle");

            if (button1.FlatStyle == FlatStyle.Standard)
            {
                button1.FlatStyle = FlatStyle.Flat;
            }
            else if (button1.FlatStyle == FlatStyle.Flat)
            {
                button1.FlatStyle = FlatStyle.Popup;
            }
            else if (button1.FlatStyle == FlatStyle.Popup)
            {
                button1.FlatStyle = FlatStyle.System;
            }
            else if (button1.FlatStyle == FlatStyle.System)
            {
                button1.FlatStyle = FlatStyle.Standard;
            }
        }

    }
}