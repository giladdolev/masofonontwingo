using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarMaxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar1 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar1");
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog1.Text = "ProgressBar Max value is " + TestedProgressBar1.Max.ToString() + " ; " + "Value property is " + TestedProgressBar1.Value.ToString();
            lblLog.Text = "ProgressBar Max value = " + TestedProgressBar.Max.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
        }

        public void btnChangeMax_Click(object sender, EventArgs e)
        {

            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.Max == 100)
            {
                TestedProgressBar.Max = 1000;
                lblLog.Text = "ProgressBar Max value = " + TestedProgressBar.Max.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }
            else
            {
                TestedProgressBar.Max = 100;
                lblLog.Text = "ProgressBar Max value = " + TestedProgressBar.Max.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }

        }

        public void btnChangeValue_Click(object sender, EventArgs e)
        {

            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.Value == 100)
            {
                TestedProgressBar.Value = 50;
                lblLog.Text = "ProgressBar Max value = " + TestedProgressBar.Max.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }
            else
            {
                TestedProgressBar.Value = 100;
                lblLog.Text = "ProgressBar Max value = " + TestedProgressBar.Max.ToString() + " ; " + "Value property = " + TestedProgressBar.Value.ToString();
            }
        }

  
    }
}