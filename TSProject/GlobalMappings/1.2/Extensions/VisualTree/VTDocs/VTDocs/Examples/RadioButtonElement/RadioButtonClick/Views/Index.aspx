<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton Click event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      
        <vt:RadioButton runat="server" Text="TestedRadioButton" Top="50px" Left="140px" ID="TestedRadioButton" Height="36px"  Width="200px"  ClickAction="RadioButtonClick\TestedRadioButton_Click" ></vt:RadioButton>          

        <vt:TextBox runat="server" Text="" Top="50px" Left="400px" Multiline="true" ID="textBox1" Height="100px" Width="250px"></vt:TextBox>
       
    </vt:WindowView>
</asp:Content>
