<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Show Method" ID="windowView1" LoadAction="LabelShow\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Show" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Displays the control to the user.

            Syntax: public void Show()
            
            Showing the control is equivalent to setting the Visible property to 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the Visible property is initially set to 'false'. 
            When invoking Show() method, the Visible property value is changed to 'true'." Top="290px" ID="lblExp2">
        </vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Top="360px" Visible="false" ID="TestedLabel"></vt:Label>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="390px"></vt:Label>

        <vt:Button runat="server" Text="Show >>" Top="465px" ID="btnShow" ClickAction="LabelShow\btnShow_Click"></vt:Button>

        <vt:Button runat="server" Text="Hide >>" Left="300" Top="465px" ID="btnHide" ClickAction="LabelShow\btnHide_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
