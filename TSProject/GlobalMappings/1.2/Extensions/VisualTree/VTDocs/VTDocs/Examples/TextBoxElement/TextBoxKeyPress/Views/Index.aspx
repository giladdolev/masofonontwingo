<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox KeyPress event" ID="windowView2" LoadAction="TextBoxKeyPress\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="KeyPress" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when a character, space or backspace key is pressed while the control has focus.

            Syntax: public event KeyPressEventHandler  KeyPress"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example uses the KeyPress event to prevent insertion of characters other than  
            digits into the Phone textbox. 
            Each invocation of the 'KeyPress' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:Label runat="server" Text="Phone No. :" Left="80px" Top="330px" ID="Label1"></vt:Label>

        <vt:TextBox runat="server" Top="330px" Left="200px" Text="0000000000" Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtPhone" 
            KeyPressAction="TextBoxKeyPress\txtPhone_KeyPress" TextChangedAction="TextBoxKeyPress\txtPhone_TextChanged"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="380px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="415px" Width="360px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>


