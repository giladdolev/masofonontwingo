﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="8, 16" Text="GridColumn Format" Margin-All="0" Padding-All="0" ID="Form1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="7.8pt" Height="330px" TabIndex="-1" Width="1000px" LoadAction="GridColumnFormat\load">

        <vt:Grid runat="server" AutoSize="true" ClientRowTemplate="" ReadOnly="True" AllowUserToAddRows="False" AllowUserToDeleteRows="False" AllowUserToResizeColumns="False" AllowUserToResizeRows="False" ColumnHeadersHeightSizeMode="AutoSize" RowHeadersVisible="True" AutoScroll="True" ToolTipText="" Top="34px" Left="10px" ID="gridElement" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="299px" Width="782px">
            <Columns>
                <vt:GridColumn ID="colNumeroCuota" runat="server" HeaderText="# Cuota" Format="currency" DataMember="colNumeroCuota" Width="110"></vt:GridColumn>
            </Columns>
            
        </vt:Grid>


        <vt:Button runat="server" UseVisualStyleBackColor="True" SkinID="Wide" TextAlign="MiddleCenter" Text="Change Currency Format" Top="155px" Left="820px" ID="button1" ClickAction="GridColumnFormat\button1_Click" >
        </vt:Button>
    </vt:WindowView>

</asp:Content>
