using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridSelectionColorController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {

            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");

        }

        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.Rows[0].Cells[0].BackColor == Color.Black)
            {
               TestedGrid2.Rows[0].Cells[0].BackColor= Color.Green;
               lblLog2.Text = "Selection Color value: " + TestedGrid2.Rows[0].Cells[0].BackColor.ToString();
            }
            else
            {
                TestedGrid2.Rows[0].Cells[0].BackColor = Color.Black;
                lblLog2.Text = "Selection Color value: " + TestedGrid2.Rows[0].Cells[0].BackColor.ToString();

            }
        }


	}
}
