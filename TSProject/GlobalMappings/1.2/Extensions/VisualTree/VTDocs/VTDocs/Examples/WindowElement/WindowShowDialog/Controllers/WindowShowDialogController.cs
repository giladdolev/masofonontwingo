using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowShowDialogController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowShowDialog());
        }
        private WindowShowDialog ViewModel
        {
            get { return this.GetRootVisualElement() as WindowShowDialog; }
        }


        //btnOpenForm2
        public async void btnShowDialog_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowShowDialog()");

            ViewModel.TestedWin = VisualElementHelper.CreateFromView<ShowDialogTestedWindow>("ShowDialogTestedWindow", "Index2", null, argsDictionary, null);
            DialogResult result = await this.ViewModel.TestedWin.ShowDialog();
            textBox1.Text = "TestedWindow is Shown as a modal dialog box";
            await MessageBox.Show(string.Format("The dialog was closed with the following result {0}", result));
        }

    }
}