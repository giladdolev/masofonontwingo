<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView ListViewItemCollection Add Method (String, String)" ID="windowView1" LoadAction="ListViewListViewItemCollectionAdd2String\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Add (String, String)" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Creates an item with the specified text and image and adds it to the collection.
            
            Syntax: public virtual ListViewItem Add(string text, string imageKey)

            Return Value: The ListViewItem that was added to the collection.

            If the ListView is not sorted, the item is added to the end of the collection." Top="75px" ID="lblDefinition"></vt:Label>
       
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following ListView pressing the 'Add ListViewItem' button will invoke 
            the Items.Add method on this ListView control. Pressing the 'Remove ListViewItem' button
             will invoke the Items.Remove method on this ListView control. " Top="260px" ID="lblExp1"></vt:Label>     
        
        <vt:ListView runat="server" Text="TestedListView" Top="340px" width="400px" Height="200px" ID="TestedListView" View="SmallIcon">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="120"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
              <Items>
                <vt:ListViewItem runat="server" Text="Item1">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="subitem1"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="subitem11"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Item2">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="subitem2"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="subitem22"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Item3">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="subitem3"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="subitem33"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="550px" ID="lblLog1" Width="400px"></vt:Label>

        <vt:Button runat="server" Text="Add ListViewItem >>" Width="160px" Top="600px" ID="btnAddListViewItem" ClickAction="ListViewListViewItemCollectionAdd2String\btnAddListViewItem_Click"></vt:Button>

        <vt:Button runat="server" Text="Remove ListViewItem >>" Width="160px" Left="480px" Top="600px" ID="btnRemoveListViewItem" ClickAction="ListViewListViewItemCollectionAdd2String\btnRemoveListViewItem_Click"></vt:Button>

         <vt:Button runat="server" Text="Add list of ListViewItems >>" Width="160px" Left="280px" Top="600px" ID="btnAddListOfListViewItem" ClickAction="ListViewListViewItemCollectionAdd2String\btnAddListOfListViewItem_Click"></vt:Button>
    </vt:WindowView>
     
</asp:Content>


