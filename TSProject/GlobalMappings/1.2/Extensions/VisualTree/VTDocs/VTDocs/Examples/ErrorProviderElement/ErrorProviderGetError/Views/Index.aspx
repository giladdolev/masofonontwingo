<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ErrorProvider GetError Method" ID="windowView1" LoadAction="ErrorProviderGetError\OnLoad">
      
       
        <vt:Label runat="server" SkinID="Title" Text="GetError(control)" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Returns the current error description string for the specified control.

            Syntax:  public string GetError(ControlElement control)
            
            'control' is the item to get the error description string for."
            Top="75px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example there are 2 textboxes in a simple login dialog.
            When one of the text boxes is left empty, an error icon is displayed, the GetError method
            is invoked, and the error description is written to the log." Top="290px" ID="lblExp2">
        </vt:Label>

        <vt:panel runat="server" Text="" Top="380px" ID="panel1" Width="320px" Height="150">
        <vt:label runat="server" Text="Username:" Top="30px" Left="35px" ID="lblUsername"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-username" Top="30px" Left="130px" ID="txtUsername" LeaveAction="ErrorProviderGetError\txtUsername_leave"></vt:textbox>

        <vt:label runat="server" Text="Password:" Top="60px" Left="35px" ID="lblPassword"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-password" Top="60px" Left="130px" ID="txtPassword" LeaveAction="ErrorProviderGetError\txtPassword_leave"></vt:textbox>

        <vt:Button runat="server" Text="Login" Width="100px" Height="30px" Top="100px" Left="110px" ID="btnLogin" ClickAction="ErrorProviderGetError\btnLogin_Click"></vt:Button>
</vt:panel>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="540" Height="40px" Width="350px"></vt:Label>

       
    </vt:WindowView>
    <vt:ComponentManager runat="server" >
		<vt:ErrorProvider runat="server" CssClass="vt-ep-username" ID="epUsername" ></vt:ErrorProvider>
	    <vt:ErrorProvider runat="server" CssClass="vt-ep-password" ID="epPassword" ></vt:ErrorProvider>
	</vt:ComponentManager>s
</asp:Content>