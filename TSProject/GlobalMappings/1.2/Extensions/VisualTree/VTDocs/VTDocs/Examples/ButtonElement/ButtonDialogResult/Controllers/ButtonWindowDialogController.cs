using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonWindowDialogController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index2()
        {
            return View(new ButtonWindowDialog());
        }

        private ButtonWindowDialog ViewModel
        {
            get { return this.GetRootVisualElement() as ButtonWindowDialog; }
        }

        public void btnOk_Click(object sender, EventArgs e)
        {
            ButtonElement btnOk = this.GetVisualElementById<ButtonElement>("btnOk");
            btnOk.DialogResult = DialogResult.OK;
        }

        public void btnCancel_Click(object sender, EventArgs e)
        {
            ButtonElement btnCancel = this.GetVisualElementById<ButtonElement>("btnCancel");
            btnCancel.DialogResult = DialogResult.Cancel;
        }

        public void btnSwitchButtons_Click(object sender, EventArgs e)
        {
            ButtonElement btnCancel = this.GetVisualElementById<ButtonElement>("btnCancel");
            ButtonElement btnOk = this.GetVisualElementById<ButtonElement>("btnOk");
            btnCancel.DialogResult = DialogResult.OK;
            btnOk.DialogResult = DialogResult.Cancel;
        }


        

    }
}