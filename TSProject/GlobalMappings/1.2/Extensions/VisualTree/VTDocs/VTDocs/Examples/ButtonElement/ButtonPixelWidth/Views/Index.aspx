<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button PixelWidth Property" ID="windowView1" LoadAction="ButtonPixelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the Width of the control in pixels.
            
            Syntax: public int PixelWidth { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this Button is initially set to: 150px" Top="235px" ID="lblExp1"></vt:Label>

        <vt:Button runat="server"  SkinID="Wide" Text="TestedButton" Top="285px" ID="TestedButton" TabIndex="3"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="335px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelWidth value >>" Top="400px" ID="btnChangePixelWidth" TabIndex="3" Width="180px" ClickAction="ButtonPixelWidth\btnChangePixelWidth_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
