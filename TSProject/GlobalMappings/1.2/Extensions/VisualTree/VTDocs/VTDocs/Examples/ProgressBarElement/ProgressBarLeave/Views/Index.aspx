<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Leave event" ID="windowView2" LoadAction="ProgressBarLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the progress bars,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:ProgressBar runat="server" Top="335px" Height="50px" ID="TestedProgressBar1" CssClass="vt-test-pb-1" LeaveAction="ProgressBarLeave\TestedProgressBar1_Leave"></vt:ProgressBar>

        <vt:ProgressBar runat="server" Top="400px" Height="50px" ID="TestedProgressBar2" CssClass="vt-test-pb-2" LeaveAction="ProgressBarLeave\TestedProgressBar2_Leave"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="465px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="505px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>


