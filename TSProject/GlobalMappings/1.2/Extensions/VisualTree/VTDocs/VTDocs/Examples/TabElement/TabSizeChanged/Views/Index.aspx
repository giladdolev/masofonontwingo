<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab SizeChanged Event" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        
        <vt:Tab runat="server" Text="Tested Tab" Top="100px" Left="140px" ID="tabSize" Height="200px" Width="250px" SizeChangedAction="TabSizeChanged\tabSize_SizeChanged" >  
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px">
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px">
                </vt:TabItem>
            </TabItems>         
        </vt:Tab>

        <vt:Button runat="server" Text="Change Tab Size" Top="150px" Left="420px" ID="btnChangeSize" Height="36px" Width="200px" ClickAction="TabSizeChanged\btnChangeSize_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="330px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

        <vt:Label runat="server" Text="Event Log" Top="400px" Left="140px"  ID="label3"   Height="15px" Width="200px"> 
            </vt:Label>

        <vt:TextBox runat="server" Text="" Top="420px" Left="140px"  ID="txtEventLog" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>
    </vt:WindowView>
</asp:Content>
        