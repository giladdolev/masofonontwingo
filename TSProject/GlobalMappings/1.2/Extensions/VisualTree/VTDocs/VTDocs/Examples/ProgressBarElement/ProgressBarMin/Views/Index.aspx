
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Min Method" ID="windowView1" LoadAction="ProgressBarMin\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Min" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets the minimum value of the range of the control.

            Syntax: public int Min { get; set; }
            Value: The minimum value of the range. The default is 0.
            
            This property specifies the lower limit of the Value property. When the value of the Minimum property 
            is changed, the ProgressBar control is redrawn to reflect the new range of the control. When the value 
            of the Value property is equal to the value of the Minimum property, the progress bar is empty."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:ProgressBar runat="server" Text="ProgressBar" Top="250px" ID="TestedProgressBar1"></vt:ProgressBar>
        
        <vt:Label runat="server" SkinID="Log" Top="300px" Width="380px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the ProgressBar 'Min' property is set to 20; 'Value' property is set to 40." Top="415px" ID="lblExp2">
        </vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="460px" Min="20" Value="20" ID="TestedProgressBar"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="510px" Width="380px" ></vt:Label>

        <vt:Button runat="server" Text="Change Min value >>" Top="580px" ID="btnMin" ClickAction="ProgressBarMin\btnChangeMin_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Value >>" Left="300" Top="580px" ID="btnValue" ClickAction="ProgressBarMin\btnChangeValue_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
