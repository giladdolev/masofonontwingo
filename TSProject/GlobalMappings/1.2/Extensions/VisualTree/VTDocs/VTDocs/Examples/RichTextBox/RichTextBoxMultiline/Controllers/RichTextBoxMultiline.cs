using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxMultilineController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeMultiline_Click(object sender, EventArgs e)
        {
            RichTextBox rtfMultilineChange = this.GetVisualElementById<RichTextBox>("rtfMultilineChange");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (rtfMultilineChange.Multiline == true)
            {
                rtfMultilineChange.Multiline = false;
                textBox1.Text = rtfMultilineChange.Multiline.ToString();
            }
            else
            {
                rtfMultilineChange.Multiline = true;
                textBox1.Text = rtfMultilineChange.Multiline.ToString();
            }
            
            
        }

    }
}