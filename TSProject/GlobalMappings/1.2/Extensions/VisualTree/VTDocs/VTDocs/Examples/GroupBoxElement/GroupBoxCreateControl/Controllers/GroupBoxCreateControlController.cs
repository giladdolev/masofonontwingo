using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new GroupBoxCreateControl());

        }

        private GroupBoxCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as GroupBoxCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement newGroupBox = new GroupBoxElement();

            newGroupBox.CreateControl();
            
            this.ViewModel.Controls.Add(newGroupBox);
            

            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}