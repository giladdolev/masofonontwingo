using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxBringToFrontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnBringCheckBox1ToFront_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            CheckBoxElement CheckBox1 = this.GetVisualElementById<CheckBoxElement>("CheckBox1");

            CheckBox1.BringToFront();
            textBox1.Text = "CheckBox1 is on front";
        }

        public void btnBringCheckBox2ToFront_Click(object sender, EventArgs e)
        {

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            CheckBoxElement CheckBox2 = this.GetVisualElementById<CheckBoxElement>("CheckBox2");

            CheckBox2.BringToFront();
            textBox1.Text = "CheckBox2 is on front";
        }

    }
}