using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button bounds are: \\r\\n" + TestedButton.Bounds;

        }
        public void btnChangeButtonBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedButton.Bounds = new Rectangle(80, 380, 150, 36);
                lblLog.Text = "Button bounds are: \\r\\n" + TestedButton.Bounds;

            }
            else
            {
                TestedButton.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "Button bounds are: \\r\\n" + TestedButton.Bounds;
            }

        }


    }
}