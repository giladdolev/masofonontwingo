<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PerformMouseDown() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseDown of 'Tested CheckBox'" Top="45px" Left="140px" ID="btnPerformMouseDown" Height="36px" Width="220px" ClickAction="CheckBoxPerformMouseDown\btnPerformMouseDown_Click"></vt:Button>


        <vt:CheckBox runat="server" Text="Tested CheckBox" Top="150px" Left="200px" ID="testedCheckBox" Height="36px"  Width="100px" MouseDownAction="CheckBoxPerformMouseDown\testedCheckBox_MouseDown"></vt:CheckBox>           

        

    </vt:WindowView>
</asp:Content>
