<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window Resize event" ID="windowView1" Resizable="true" ResizeAction="WindowResize\Window_Resize">

        <vt:Label runat="server" SkinID="Title" Text="Window Resize" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the size of the weindow changes.
            Syntax: public event EventHandler CheckedChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the size of the window."
            Top="250px" ID="lblExp2">
        </vt:Label>

        <vt:Button runat="server" Text="Change window size" Top="320px" ID="btn1" ClickAction="WindowResize\btn1_changeSize"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="360px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="400px" ID="txtEventLog"></vt:TextBox>


    </vt:WindowView>
</asp:Content>
