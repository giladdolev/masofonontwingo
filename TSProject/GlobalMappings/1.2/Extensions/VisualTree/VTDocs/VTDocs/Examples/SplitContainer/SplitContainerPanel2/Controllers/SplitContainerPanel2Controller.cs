using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>

    public class SplitContainerPanel2Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnGetPanel2EnabledValue_Click(object sender, EventArgs e)
        {
            SplitContainer splitContainer1 = this.GetVisualElementById<SplitContainer>("splitContainer1");
            MessageBox.Show("Panel2 Enabled value is: " + splitContainer1.Panel2.Enabled.ToString());

        }

        private void btnChangePanel2Enabled_Click(object sender, EventArgs e)
        {
            SplitContainer splitContainer1 = this.GetVisualElementById<SplitContainer>("splitContainer1");
            if (splitContainer1.Panel2.Enabled == false)
                splitContainer1.Panel2.Enabled = true;
            else
                splitContainer1.Panel2.Enabled = false;


        }
    }
}