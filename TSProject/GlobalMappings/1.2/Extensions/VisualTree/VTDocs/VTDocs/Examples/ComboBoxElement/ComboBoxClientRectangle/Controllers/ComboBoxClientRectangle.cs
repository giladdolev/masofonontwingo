using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("Item2");
            TestedComboBox.Items.Add("Item3");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedComboBox.ClientRectangle;

        }
        public void btnChangeComboBoxClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            if (TestedComboBox.Left == 100)
            {
                TestedComboBox.SetBounds(80, 320, 165, 25);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedComboBox.ClientRectangle;

            }
            else
            {
                TestedComboBox.SetBounds(100, 310, 200, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedComboBox.ClientRectangle;
            }

        }

    }
}