using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPerformClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformClick_Click(object sender, EventArgs e)
        {

            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");


            TestedRichTextBox.PerformClick(e);
        }

        public void TestedRichTextBox_Click(object sender, EventArgs e)
        {

            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");

            MessageBox.Show("TestedRichTextBox Click event method is invoked");
        }

    }
}