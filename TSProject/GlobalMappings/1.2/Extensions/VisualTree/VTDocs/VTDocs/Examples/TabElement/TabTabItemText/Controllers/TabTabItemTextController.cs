using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabItem TabItem2 = this.GetVisualElementById<TabItem>("TabItem2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Text property value for tabItem2: " + TabItem2.Text + ".";
        }


        public void btnChangeText_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab1");
            TabItem TabItem2 = this.GetVisualElementById<TabItem>("TabItem2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TabItem2.Text == "tabItem2")
            {
                TabItem2.Text = "New Text";
                lblLog2.Text = "Text property value for tabItem2: " + TabItem2.Text + ".";
            }
            else
            {
                TabItem2.Text = "tabItem2";
                lblLog2.Text = "Text property value for tabItem2: " + TabItem2.Text + ".";
            }


        }
    }
}