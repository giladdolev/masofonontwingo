using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridColumnHeaderMouseClickController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void btn1_raiseEvent(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            //txtEventLog.Text += "GridColumnHeaderMouseClick Event fired";
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Click on the Grid Column headers...";
        }


        public void TestedGrid_HeaderClick(object sender, GridElementCellEventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "You clicked on column header number : " + e.ColumnIndex.ToString();
            txtEventLog.Text += "\\r\\nGrid ColumnHeaderMouseClick event is invoked";
        }
    }
}
