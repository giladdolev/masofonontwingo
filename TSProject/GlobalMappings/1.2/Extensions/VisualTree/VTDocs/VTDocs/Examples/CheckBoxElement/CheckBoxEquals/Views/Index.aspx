<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Equals Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction ="CheckBoxEquals\Load">


        <vt:CheckBox runat="server" Text="CheckBox1" Top="45px" Left="140px" ID="CheckBox1" Height="36px" Width="220px" ></vt:CheckBox>           

         <vt:CheckBox runat="server" Text="CheckBox2" Top="80px" Left="140px" ID="CheckBox2" Height="36px" Width="220px" ></vt:CheckBox>


        <vt:Button runat="server" Text="Invoke Equals" Top="80px" Left="400px" ID="btnEquals" Height="36px" Width="220px" ClickAction ="CheckBoxEquals\btnEquals_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="220px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="220px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
