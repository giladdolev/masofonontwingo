<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label VisibleChanged event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Label runat="server" Text="VisibleChanged Run Time" Top="160px" Left="50px" ID="label" Width="350px" VisibleChangedAction="LabelVisibleChanged\btnTestedLabel_VisibleChanged"></vt:Label>

        <vt:Button runat="server" Text="Change Label VisibleChanged" Top="112px" Left="130px" ID="btnVisibleChanged" TabIndex="1" Width="200px" ClickAction="LabelVisibleChanged\btnTestedLabel_Click"></vt:Button>
        <vt:Label runat="server" Text="" Top="112px" Left="450px" ID="lblEvent" Width="350px" Font-Bold="true"></vt:Label>
    </vt:WindowView>
</asp:Content>
