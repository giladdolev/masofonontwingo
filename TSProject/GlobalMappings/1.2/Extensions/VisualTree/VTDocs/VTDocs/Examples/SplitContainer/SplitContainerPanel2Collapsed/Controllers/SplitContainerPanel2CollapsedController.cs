using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>

    public class SplitContainerPanel2CollapsedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Panel2Collapsed value: " + TestedSplitContainer1.Panel2Collapsed;
            lblLog2.Text = "Panel2Collapsed value: " + TestedSplitContainer2.Panel2Collapsed + '.';
        }

        public void btnChangePanel2Collapsed_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedSplitContainer2.Panel2Collapsed = !TestedSplitContainer2.Panel2Collapsed;
            lblLog2.Text = "Panel2Collapsed value: " + TestedSplitContainer2.Panel2Collapsed + '.';
        }
    }
}