using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonCssClassController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton1 = this.GetVisualElementById<ButtonElement>("btnTestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if ( String.IsNullOrEmpty( btnTestedButton1.CssClass) )
            {
                lblLog1.Text = "No IconCss class";
            }
            else
            {
                lblLog1.Text = "Set with " + btnTestedButton1.CssClass + " class";
            }


            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (String.IsNullOrEmpty(btnTestedButton2.CssClass))
            {
                lblLog2.Text = "No IconCss class";
            }
            else
            {
                lblLog2.Text = "Set with " + btnTestedButton2.CssClass + " class";
            }
        }

    }
}