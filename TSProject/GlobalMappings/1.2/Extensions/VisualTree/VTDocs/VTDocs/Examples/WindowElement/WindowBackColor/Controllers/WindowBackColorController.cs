using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowBackColorController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowBackColor());
        }
        private WindowBackColor ViewModel
        {
            get { return this.GetRootVisualElement() as WindowBackColor; }
        }

        public void btnChangeBackColor_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.BackColor == Color.Pink)
			{
                this.ViewModel.BackColor = Color.RoyalBlue;
                textBox1.Text = this.ViewModel.BackColor.ToString();
			}
			else
            {
                this.ViewModel.BackColor = Color.Pink;
                textBox1.Text = this.ViewModel.BackColor.ToString();
            }
                
		}

    }
}