using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformControlAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlAdded_Click(object sender, EventArgs e)
        {
            
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");

            ControlEventArgs args = new ControlEventArgs(testedCheckBox);

            testedCheckBox.PerformControlAdded(args);
        }

        public void testedCheckBox_ControlAdded(object sender, EventArgs e)
        {
            MessageBox.Show("TestedCheckBox ControlAdded event method is invoked");
        }

    }
}