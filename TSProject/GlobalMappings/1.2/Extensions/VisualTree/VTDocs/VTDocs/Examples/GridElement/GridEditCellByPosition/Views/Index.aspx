﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Edit Cell By Position"  ID="Form1" >

     <vt:Label runat="server"  Text="EditCellByPosition" ID="Label1" Font-Size="18px"></vt:Label>

            <vt:Label runat="server" Text="Use position to edit cell.(in here it to a defaule (0 , 0)
                Syntax : public method Grid.EditCellByPotion"
            Top="75px" ID="lblDefinition">
        </vt:Label>
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="140px" ID="lblExample"></vt:Label>
          <vt:Label runat="server" Text="Fill the grid with 'Add Row' and than click 'Edit Cell' to stat editind"
            Top="175px" ID="Label2">
        </vt:Label>
       <vt:Grid runat="server" BackColor ="Gray" Top="230px" ID="TestedGrid2" LostFocusAction="GridEditCellByPosition\button3_Click" GotFocusAction="GridEditCellByPosition\button2_Click">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

             <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog1" Left="10px" Text="Method"></vt:Label>
        <vt:Button runat="server" Text="Add row" Top="420px" Left="10px" ID="button1" ClickAction="GridEditCellByPosition\button1_Click">
        </vt:Button>
         <vt:Button runat="server" Text="Cell Edit" Top="420px" Left="250px" ID="button2" ClickAction="GridEditCellByPosition\button2_Click">
        </vt:Button>
                        <vt:TextBox runat="server" SkinID="EventLog" Text="Updated Display Indexes:" Top="490px" ID="txtEventLog" ></vt:TextBox> 
    </vt:WindowView>

</asp:Content>
