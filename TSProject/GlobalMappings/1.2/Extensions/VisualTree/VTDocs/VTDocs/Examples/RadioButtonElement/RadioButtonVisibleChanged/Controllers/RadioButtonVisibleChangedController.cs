using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement rdoVisible = this.GetVisualElementById<RadioButtonElement>("rdoVisible");

            if (rdoVisible.Visible == true) //Get
            {
                rdoVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + rdoVisible.Visible;
            }
            else
            {
                rdoVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + rdoVisible.Visible;
            }
        }

      //  rdoText_TextChanged
        public void rdoVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}