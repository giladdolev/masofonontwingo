using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedCheckBox1.Enabled.ToString();

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedCheckBox2.Enabled.ToString() + ".";

        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedCheckBox2.Enabled = !TestedCheckBox2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedCheckBox2.Enabled.ToString() + ".";
        }
     
    }
}