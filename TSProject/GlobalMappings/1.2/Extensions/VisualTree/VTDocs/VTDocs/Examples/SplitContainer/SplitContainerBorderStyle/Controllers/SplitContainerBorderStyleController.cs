using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedSplitContainer1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
        }

        public void btnChangeSplitContainerBorderStyle_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedSplitContainer2.BorderStyle == BorderStyle.None)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Dotted)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Double)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Groove)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Inset)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Dashed)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.NotSet)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Outset)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Ridge)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else if (TestedSplitContainer2.BorderStyle == BorderStyle.Solid)
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
            else
            {
                TestedSplitContainer2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedSplitContainer2.BorderStyle + '.';
            }
        }

    }
}