<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="6, 13" TabIndex="3" Text="TabIndex" ID="WindowWithControls" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="800px" Width="777px" BackColor="lightBlue">

        <vt:Button runat="server" ID="Button0" Text="TabIndex 1 " Left="10px" Top="10px" Width="80px" Height="25px" TabIndex="1"></vt:Button>
        <vt:Button runat="server" ID="Button1" Text="TabIndex 2 " Left="10px" Top="40px" Width="80px" Height="25px" TabIndex="2"></vt:Button>
       <%-- <vt:Button runat="server" ID="Button2" Text="TabIndex 3 " Left="10px" Top="70px" Width="80px" Height="25px" TabIndex="3"></vt:Button>--%>
        <vt:Button runat="server" ID="Button3" Text="TabIndex 4 " Left="10px" Top="100px" Width="80px" Height="25px" TabIndex="4"></vt:Button>
        <vt:Button runat="server" ID="Button4" Text="TabIndex 5 " Left="10px" Top="130px" Width="80px" Height="25px" TabIndex="5"></vt:Button>
        <vt:Button runat="server" ID="Button5" Text="TabIndex 6 " Left="10px" Top="160px" Width="80px" Height="25px" TabIndex="6"></vt:Button>
        <vt:Button runat="server" ID="Button6" Text="TabIndex 7 " Left="10px" Top="190px" Width="80px" Height="25px" TabIndex="7"></vt:Button>
        <vt:Button runat="server" ID="Button7" Text="TabIndex 8 " Left="10px" Top="220px" Width="80px" Height="25px" TabIndex="8"></vt:Button>
        <vt:Button runat="server" ID="Button8" Text="TabIndex 9 " Left="10px" Top="250px" Width="80px" Height="25px" TabIndex="9"></vt:Button>
        <vt:Button runat="server" ID="Button9" Text="TabIndex 10 " Left="10px" Top="280px" Width="80px" Height="25px" TabIndex="10"></vt:Button>
        <vt:Button runat="server" ID="Button10" Text="TabIndex 11 " Left="10px" Top="310px" Width="80px" Height="25px" TabIndex="11"></vt:Button>
        <vt:Button runat="server" ID="Button11" Text="TabIndex 12 " Left="10px" Top="340px" Width="80px" Height="25px" TabIndex="12"></vt:Button>
        <vt:Button runat="server" ID="Button12" Text="TabIndex 13 " Left="10px" Top="370px" Width="80px" Height="25px" TabIndex="13"></vt:Button>
        <vt:Button runat="server" ID="Button13" Text="TabIndex 14 " Left="10px" Top="400px" Width="80px" Height="25px" TabIndex="14"></vt:Button>
        <vt:Button runat="server" ID="Button14" Text="TabIndex 15 " Left="10px" Top="430px" Width="80px" Height="25px" TabIndex="15"></vt:Button>
        <vt:Button runat="server" ID="Button15" Text="TabIndex 16 " Left="10px" Top="460px" Width="80px" Height="25px" TabIndex="16"></vt:Button>
        <vt:Button runat="server" ID="Button16" Text="TabIndex 17 " Left="10px" Top="490px" Width="80px" Height="25px" TabIndex="17"></vt:Button>
        <vt:Button runat="server" ID="Button17" Text="TabIndex 18 " Left="10px" Top="520px" Width="80px" Height="25px" TabIndex="18"></vt:Button>

        <vt:TextBox runat="server" ID="Button18" Text="TabIndex 19 " Left="100px" Top="10px" Width="80px" Height="25px" TabIndex="19"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button19" Text="TabIndex 20 " Left="100px" Top="40px" Width="80px" Height="25px" TabIndex="20"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button20" Text="TabIndex 21 " Left="100px" Top="70px" Width="80px" Height="25px" TabIndex="21"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button21" Text="TabIndex 22 " Left="100px" Top="100px" Width="80px" Height="25px" TabIndex="22"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button22" Text="TabIndex 23 " Left="100px" Top="130px" Width="80px" Height="25px" TabIndex="23"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button23" Text="TabIndex 24 " Left="100px" Top="160px" Width="80px" Height="25px" TabIndex="24"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button24" Text="TabIndex 25 " Left="100px" Top="190px" Width="80px" Height="25px" TabIndex="25"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button25" Text="TabIndex 26 " Left="100px" Top="220px" Width="80px" Height="25px" TabIndex="26"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button26" Text="TabIndex 27 " Left="100px" Top="250px" Width="80px" Height="25px" TabIndex="27"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button27" Text="TabIndex 28 " Left="100px" Top="280px" Width="80px" Height="25px" TabIndex="28"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button28" Text="TabIndex 29 " Left="100px" Top="310px" Width="80px" Height="25px" TabIndex="29"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button29" Text="TabIndex 30 " Left="100px" Top="340px" Width="80px" Height="25px" TabIndex="30"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button30" Text="TabIndex 31 " Left="100px" Top="370px" Width="80px" Height="25px" TabIndex="31"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button31" Text="TabIndex 32 " Left="100px" Top="400px" Width="80px" Height="25px" TabIndex="32"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button32" Text="TabIndex 33 " Left="100px" Top="430px" Width="80px" Height="25px" TabIndex="33"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button33" Text="TabIndex 34 " Left="100px" Top="460px" Width="80px" Height="25px" TabIndex="34"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button34" Text="TabIndex 35 " Left="100px" Top="490px" Width="80px" Height="25px" TabIndex="35"></vt:TextBox>
        <vt:TextBox runat="server" ID="Button35" Text="TabIndex 36 " Left="100px" Top="520px" Width="80px" Height="25px" TabIndex="36"></vt:TextBox>

        <vt:GroupBox runat="server" ID="grb1" Width="300px" Height="200px" Left="200px" Top="20px" Text="TabIndex 37 " TabIndex="37">
           
            <vt:Button runat="server" ID="Button36" Text="TabIndex 38 " Left="10px" Top="25px" Width="80px" Height="25px" TabIndex="38"></vt:Button>
            <vt:Button runat="server" ID="Button37" Text="TabIndex 39 " Left="10px" Top="50px" Width="80px" Height="25px" TabIndex="39"></vt:Button>
            <vt:Button runat="server" ID="Button38" Text="TabIndex 40 " Left="10px" Top="75px" Width="80px" Height="25px" TabIndex="40"></vt:Button>
             <vt:TextBox runat="server" ID="TextBox1" Text="TabIndex 41 " Left="150px" Top="25px" Width="80px" Height="25px" TabIndex="41"></vt:TextBox>
            <vt:TextBox runat="server" ID="TextBox2" Text="TabIndex 42 " Left="150px" Top="50px" Width="80px" Height="25px" TabIndex="42"></vt:TextBox>
            <vt:TextBox runat="server" ID="TextBox3" Text="TabIndex 43 " Left="150px" Top="75px" Width="80px" Height="25px" TabIndex="43"></vt:TextBox>

        </vt:GroupBox>

        <vt:Button runat="server" ID="TextBox4" Text="TabIndex 44 " Left="200px" Top="260px" Width="80px" Height="25px" TabIndex="44"></vt:Button>
        <vt:Button runat="server" ID="TextBox5" Text="TabIndex 45 " Left="280px" Top="260px" Width="80px" Height="25px" TabIndex="45"></vt:Button>
        <vt:Button runat="server" ID="TextBox6" Text="TabIndex 46 " Left="360px" Top="260px" Width="80px" Height="25px" TabIndex="46"></vt:Button>

    </vt:WindowView>
</asp:Content>
