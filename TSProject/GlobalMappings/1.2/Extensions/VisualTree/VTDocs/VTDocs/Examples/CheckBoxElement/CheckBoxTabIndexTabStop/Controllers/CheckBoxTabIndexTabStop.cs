using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxTabIndexTabStopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnChangeTabIndex_Click(object sender, EventArgs e)
        {
            CheckBoxElement btnChangeTabIndex = this.GetVisualElementById<CheckBoxElement>("btnChangeTabIndex");
            CheckBoxElement CheckBox1 = this.GetVisualElementById<CheckBoxElement>("CheckBox1");
            CheckBoxElement CheckBox2 = this.GetVisualElementById<CheckBoxElement>("CheckBox2");
            CheckBoxElement CheckBox4 = this.GetVisualElementById<CheckBoxElement>("CheckBox4");
            CheckBoxElement CheckBox5 = this.GetVisualElementById<CheckBoxElement>("CheckBox5");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (CheckBox1.TabIndex == 1)
            {
                CheckBox5.TabIndex = 1;
                CheckBox4.TabIndex = 2;
                CheckBox2.TabIndex = 4;
                CheckBox1.TabIndex = 5;

                textBox1.Text = "\\r\\nCheckBox1 TabIndex: " + CheckBox1.TabIndex + "\\r\\nCheckBox2 TabIndex: " + CheckBox2.TabIndex + "\\r\\nCheckBox4 TabIndex: " + CheckBox4.TabIndex + "\\r\\nCheckBox5 TabIndex: " + CheckBox5.TabIndex;       

            }
            else
            { 
                CheckBox1.TabIndex = 1;
                CheckBox2.TabIndex = 2;
                CheckBox4.TabIndex = 4;
                CheckBox5.TabIndex = 5;

                textBox1.Text = "\\r\\nCheckBox1 TabIndex: " + CheckBox1.TabIndex + "\\r\\nCheckBox2 TabIndex: " + CheckBox2.TabIndex + "\\r\\nCheckBox4 TabIndex: " + CheckBox4.TabIndex + "\\r\\nCheckBox5 TabIndex: " + CheckBox5.TabIndex;
            }
        }
        private void btnChangeTabStop_Click(object sender, EventArgs e)
        {
            CheckBoxElement btnChangeTabStop = this.GetVisualElementById<CheckBoxElement>("btnChangeTabStop");
            CheckBoxElement CheckBox3 = this.GetVisualElementById<CheckBoxElement>("CheckBox3");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");


            if (CheckBox3.TabStop == false)
            {
                CheckBox3.TabStop = true;
                textBox2.Text = "CheckBox3 TabStop: \\r\\n" + CheckBox3.TabStop;

            }
            else
            {
                CheckBox3.TabStop = false;
                textBox2.Text = "CheckBox3 TabStop: \\r\\n" + CheckBox3.TabStop;

            }
        }
    }
}