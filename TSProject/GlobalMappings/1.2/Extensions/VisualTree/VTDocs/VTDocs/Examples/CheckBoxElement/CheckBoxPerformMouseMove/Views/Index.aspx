<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PerformMouseMove() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseMove of 'Tested CheckBox'" Top="45px" Left="140px" ID="btnPerformMouseMove" Height="36px" Width="220px" ClickAction="CheckBoxPerformMouseMove\btnPerformMouseMove_Click"></vt:Button>


        <vt:CheckBox runat="server" Text="Tested CheckBox" Top="150px" Left="200px" ID="testedCheckBox" Height="36px"  Width="100px" MouseMoveAction="CheckBoxPerformMouseMove\testedCheckBox_MouseMove"></vt:CheckBox>           

        

    </vt:WindowView>
</asp:Content>
