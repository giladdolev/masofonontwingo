using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new RichTextBoxEquals());
        }

        private RichTextBoxEquals ViewModel
        {
            get { return this.GetRootVisualElement() as RichTextBoxEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RichTextBox RichTextBox1 = this.GetVisualElementById<RichTextBox>("RichTextBox1");
            RichTextBox RichTextBox2 = this.GetVisualElementById<RichTextBox>("RichTextBox2");


            if (this.ViewModel.TestedRichTextBox.Equals(RichTextBox1))
            {
                this.ViewModel.TestedRichTextBox = RichTextBox2;
                textBox1.Text = "TestedRichTextBox Equals RichTextBox2";
            }
            else
            {
                this.ViewModel.TestedRichTextBox = RichTextBox1;
                textBox1.Text = "TestedRichTextBox Equals RichTextBox1";
            }

            
        }
        

        public void Load(object sender, EventArgs e)
        {
            this.ViewModel.Controls.Add(this.ViewModel.TestedRichTextBox);
        }



    }
}