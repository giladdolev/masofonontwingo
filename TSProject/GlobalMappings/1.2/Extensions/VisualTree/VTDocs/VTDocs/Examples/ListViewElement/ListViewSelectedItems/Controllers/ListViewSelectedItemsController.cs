using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewSelectedItemsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedListView.SelectedItems==null)
                lblLog.Text = "SelectedListViewItemCollection is empty";
            else
            {
                lblLog.Text = "SelectedListViewItemCollection contains: ";
                foreach (ListViewItem selectedItem in TestedListView.SelectedItems)
                {
                    if (selectedItem.Text == "a")
                        lblLog.Text += "\\r\\nFirst ListViewItem - " + selectedItem.Text;
                    else if (selectedItem.Text == "d")
                        lblLog.Text += "\\r\\nSecond ListViewItem - " + selectedItem.Text;
                    else if (selectedItem.Text == "g")
                        lblLog.Text += "\\r\\nThird ListViewItem - " + selectedItem.Text;

                }
            }

        }
        public void btnGetSelectedItems_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");


            if (TestedListView.SelectedItems == null)
                lblLog.Text = "SelectedListViewItemCollection is empty";
            else
            {
                lblLog.Text = "SelectedListViewItemCollection contains: ";
                foreach (ListViewItem selectedItem in TestedListView.SelectedItems)
                {
                    if (selectedItem.Text == "a")
                        lblLog.Text += "\\r\\nFirst ListViewItem - " + selectedItem.Text;
                    else if (selectedItem.Text == "d")
                        lblLog.Text += "\\r\\nSecond ListViewItem - " + selectedItem.Text;
                    else if (selectedItem.Text == "g")
                        lblLog.Text += "\\r\\nThird ListViewItem - " + selectedItem.Text;

                }
            }
            
            
        }

    }
}