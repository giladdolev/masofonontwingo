using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class DateTimePickerEditableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker1 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Editable value: " + TestedDateTimePicker1.Editable.ToString();

            DateTimePickerElement TestedDateTimePicker = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Editable value: " + TestedDateTimePicker.Editable.ToString();

        }


        public void btnChangeEditable_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedDateTimePicker.Editable = !TestedDateTimePicker.Editable;
            lblLog2.Text = "Editable value: " + TestedDateTimePicker.Editable + "\\r\\nFormat value: " + TestedDateTimePicker.Format;

        }

        public void btnSetValue_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker");
            TestedDateTimePicker.Value = new DateTime(2016, 11, 8, 1, 0, 1, 0);
            
        }
        
        public void btnChangeFormat_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedDateTimePicker.Format == DateTimePickerFormat.Default)
                TestedDateTimePicker.Format = DateTimePickerFormat.Custom;
            else if (TestedDateTimePicker.Format == DateTimePickerFormat.Custom)
                TestedDateTimePicker.Format = DateTimePickerFormat.Long;
            else if (TestedDateTimePicker.Format == DateTimePickerFormat.Long)
                TestedDateTimePicker.Format = DateTimePickerFormat.Short;
            else if (TestedDateTimePicker.Format == DateTimePickerFormat.Short)
                TestedDateTimePicker.Format = DateTimePickerFormat.Time;
            else if (TestedDateTimePicker.Format == DateTimePickerFormat.Time)
                TestedDateTimePicker.Format = DateTimePickerFormat.Line;
            else
                TestedDateTimePicker.Format = DateTimePickerFormat.Default;


            lblLog2.Text = "Editable value: " + TestedDateTimePicker.Editable + "\\r\\nFormat value: " + TestedDateTimePicker.Format;

        }

    }
}