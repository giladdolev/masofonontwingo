﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelDockController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnDock_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");

            if (label.Dock == Dock.None)
            {
                label.Dock = Dock.Fill;
            }
            else if (label.Dock == Dock.Fill)
            {
                label.Dock = Dock.Left;
            }
            else if (label.Dock == Dock.Left)
            {
                label.Dock = Dock.Right;
            }
            else if (label.Dock == Dock.Right)
            {
                label.Dock = Dock.Top;
            }
            else if (label.Dock == Dock.Top)
            {
                label.Dock = Dock.Bottom;
            }
            else if (label.Dock == Dock.Bottom)
            {
                label.Dock = Dock.None;
            }
        }
      
    }
}