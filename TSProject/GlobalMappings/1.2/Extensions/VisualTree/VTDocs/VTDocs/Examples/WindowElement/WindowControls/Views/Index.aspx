<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window Controls Property" Controls = "Pink" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="1000px">
		
        <vt:Label runat="server" Text="Click the 'Add Controls' button and verify the dynamic controls that shown are not causing an exception" Top="30px" Left="70px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     


		<vt:Button runat="server" UseVisualStyleControls="True" TextAlign="MiddleCenter" Text="Add Controls" Top="100px" Left="200px" ClickAction="WindowControls\btnAddControls_Click" ID="btnAddControls" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="50px" TabIndex="1" Width="200px">
		</vt:Button>

	

        </vt:WindowView>
</asp:Content>