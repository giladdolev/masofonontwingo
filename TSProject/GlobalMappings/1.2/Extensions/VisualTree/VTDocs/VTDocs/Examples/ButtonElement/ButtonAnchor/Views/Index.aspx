<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Anchor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Top="50px" Left="140px" Margin-All="0" Padding-All="0" ID="panel1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="72px" TabIndex="56" Width="300px">
        <vt:Button runat="server" Text="button Anchor is set to 'Bottom'" Anchor ="Bottom" Top="18px" Left="75px" ID="btnAnchor" Height="36px" Font-Names=""  TabIndex="1" Width="150px"></vt:Button>           
            </vt:Panel>


        <vt:Label runat="server" Text="RunTime" Top="150px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Top="170px" Left="140px" Margin-All="0" Padding-All="0"  ID="panel2" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="72px" TabIndex="56" Width="300px">
        <vt:Button runat="server"  Text="Change Button Anchor" Top="18px" Left="75px" ID="btnChangeAnchor" Height="36px" TabIndex="1" Width="150px" ClickAction="ButtonAnchor\btnChangeAnchor_Click"></vt:Button>
            </vt:Panel>

    </vt:WindowView>
</asp:Content>
