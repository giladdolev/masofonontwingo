using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Image bounds are: \\r\\n" + TestedImage.Bounds;

        }
        public void btnChangeImageBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            if (TestedImage.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedImage.Bounds = new Rectangle(80, 355, 150, 90);
                lblLog.Text = "Image bounds are: \\r\\n" + TestedImage.Bounds;

            }
            else
            {
                TestedImage.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "Image bounds are: \\r\\n" + TestedImage.Bounds;
            }

        }


    }
}