<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Load event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
              
          <vt:Label runat="server" Text="Event Log" Top="160px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>


        <vt:TextBox runat="server" Text="" Top="150px" Left="180px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>



        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="50px" Left="200px" ID="rtfLoad" Height="70px"  Width="200px" LoadAction ="RichTextBoxLoad\rtfLoad_Load"></vt:RichTextBox>          


      

    </vt:WindowView>
</asp:Content>
