using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonTabIndexTabStopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnChangeTabIndex_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeTabIndex = this.GetVisualElementById<ButtonElement>("btnChangeTabIndex");
            ButtonElement button1 = this.GetVisualElementById<ButtonElement>("button1");
            ButtonElement button2 = this.GetVisualElementById<ButtonElement>("button2");
            ButtonElement button4 = this.GetVisualElementById<ButtonElement>("button4");
            ButtonElement button5 = this.GetVisualElementById<ButtonElement>("button5");

            if (button1.TabIndex == 1)
            {
                button5.TabIndex = 1;
                button4.TabIndex = 2;
                button2.TabIndex = 4;
                button1.TabIndex = 5;
            }
            else
            { 
                button1.TabIndex = 1;
                button2.TabIndex = 2;
                button4.TabIndex = 4;
                button5.TabIndex = 5;
            }
        }
        private void btnChangeTabStop_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeTabStop = this.GetVisualElementById<ButtonElement>("btnChangeTabStop");
            ButtonElement button3 = this.GetVisualElementById<ButtonElement>("button3");
            
            if (button3.TabStop == false)
            {
                button3.TabStop = true;
            }
            else
                button3.TabStop = false;
        }
    }
}