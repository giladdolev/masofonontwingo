using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarResizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");


            if (testedProgressBar.Size == new Size(200, 36))
            {
                testedProgressBar.Size = new Size(300, 20);
                testedProgressBar.Text = "Set to Height: " + testedProgressBar.Height + ", Width: " + testedProgressBar.Width;
            }
            else
            {
                testedProgressBar.Size = new Size(200, 36);
                testedProgressBar.Text = "Back to Height: " + testedProgressBar.Height + ", Width: " + testedProgressBar.Width;
            }
        }

        private void testedProgressBar_Resize(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "ProgressBar Resize event is invoked\\r\\n";
        }
      
    }
}