using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //PanelAlignment
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement pnlVisible = this.GetVisualElementById<PanelElement>("pnlVisible");

            if (pnlVisible.Visible == true) //Get
            {
                pnlVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + pnlVisible.Visible;
            }
            else
            {
                pnlVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + pnlVisible.Visible;
            }
        }

      //  grpText_TextChanged
        public void pnlVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}