<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox BackGroundImage" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

         <vt:GroupBox runat="server" AutoScroll="True" Dock="Fill" Top="0px" Left="0px" ID="grb1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="50px" Width="50px" BackgroundImage="Content/Images/gizmox.png"  Text="Frame1" ></vt:GroupBox>
       
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change groupbox background" Top="70px" Left="600px" ID="ChangeBackGroundImage" Height="49px" Width="169px" ClickAction="BackGroundImageGroupBox\ChangeBackGroundImage_Click">
        </vt:Button>
       
    </vt:WindowView>
</asp:Content>
