using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class AllControlsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void ButtonMessageBox_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello from all VT controls", "All Controls", System.Web.VisualTree.MessageBoxIcon.Information);
            
        }
        public void ButtonOpenFile_Click(object sender, EventArgs e)
        {
        }

        
    }
}