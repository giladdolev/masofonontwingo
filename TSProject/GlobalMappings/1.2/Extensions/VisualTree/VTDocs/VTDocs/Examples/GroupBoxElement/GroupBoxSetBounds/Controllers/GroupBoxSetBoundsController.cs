using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested GroupBox bounds are set to: \\r\\nLeft  " + TestedGroupBox.Left + ", Top  " + TestedGroupBox.Top + ", Width  " + TestedGroupBox.Width + ", Height  " + TestedGroupBox.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            if (TestedGroupBox.Left == 100)
            {
                TestedGroupBox.SetBounds(80, 300, 200, 80);
                lblLog.Text = "The Tested GroupBox bounds are set to: \\r\\nLeft  " + TestedGroupBox.Left + ", Top  " + TestedGroupBox.Top + ", Width  " + TestedGroupBox.Width + ", Height  " + TestedGroupBox.Height;

            }
            else
            {
                TestedGroupBox.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested GroupBox bounds are set to: \\r\\nLeft  " + TestedGroupBox.Left + ", Top  " + TestedGroupBox.Top + ", Width  " + TestedGroupBox.Width + ", Height  " + TestedGroupBox.Height;
            }
        }

    }
}