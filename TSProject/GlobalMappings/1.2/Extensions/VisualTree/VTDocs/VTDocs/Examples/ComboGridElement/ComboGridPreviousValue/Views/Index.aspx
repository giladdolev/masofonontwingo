
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid PreviousValue Property" ID="windowView2" LoadAction="ComboGridPreviousValue/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PreviousValue" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the PreviousValue that holds the previous selection, when a new selection occur.
            The value of the member property specified by the ValueMember property.
            
            Syntax: public object PreviousValue { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can change the combogrid selection by selecting a line or a cell
            from the dropdown grid, and as a result the PreviousValue Property will be updated." 
            Top="255px"  ID="lblExp1"></vt:Label>     
      
        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-TestedComboGrid" Top="345px" ID="TestedComboGrid" SelectedIndexChangeAction="ComboGridPreviousValue\TestedComboGrid_SelectedIndexChanged"></vt:ComboGrid>
        
        <vt:Label runat="server" SkinID="Log" Top="500px" Width="350" Height="40px" ID="lblLog"></vt:Label>

    </vt:WindowView>
</asp:Content>