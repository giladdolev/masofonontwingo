<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton Anchor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Top="50px" Left="140px" Margin-All="0" Padding-All="0" ID="panel1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="300px">
        <vt:RadioButton runat="server" Text="RadioButton Anchor is set to 'Bottom'" BackColor ="yellow" Anchor ="Bottom" Top="50px" Left="75px" ID="rdoAnchor" Height="36px" Font-Names=""  TabIndex="1" Width="150px"></vt:RadioButton>           
            </vt:Panel>


        <vt:Label runat="server" Text="RunTime" Top="270px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Top="290px" Left="140px" Margin-All="0" Padding-All="0"  ID="panel2" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" TabIndex="56" Width="300px">
        <vt:RadioButton runat="server" BackColor ="yellow" Text="RunTime Anchor" Top="50px" Left="75px" ID="rdoRunTimeAnchor" Height="36px" TabIndex="1" Width="150px"></vt:RadioButton>
            </vt:Panel>

        <vt:Button runat="server"  Text="Change RadioButton Anchor" Top="290px" Left="460px" ID="btnChangeAnchor" Height="36px" TabIndex="1" Width="200px" ClickAction="RadioButtonAnchor\btnChangeAnchor_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="530px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
