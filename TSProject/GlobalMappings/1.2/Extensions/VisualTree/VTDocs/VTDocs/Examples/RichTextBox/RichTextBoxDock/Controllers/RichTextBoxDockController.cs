using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnDock_Click(object sender, EventArgs e)
        {
            RichTextBox rtfDock = this.GetVisualElementById<RichTextBox>("rtfDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (rtfDock.Dock == Dock.Bottom)
            {
                rtfDock.Dock = Dock.Fill;
                textBox1.Text = rtfDock.Dock.ToString();
            }
            else if (rtfDock.Dock == Dock.Fill)
            {
                rtfDock.Dock = Dock.Left;
                textBox1.Text = rtfDock.Dock.ToString();
            }
            else if (rtfDock.Dock == Dock.Left)
            {
                rtfDock.Dock = Dock.Right;
                textBox1.Text = rtfDock.Dock.ToString();
            }
            else if (rtfDock.Dock == Dock.Right)
            {
                rtfDock.Dock = Dock.Top;
                textBox1.Text = rtfDock.Dock.ToString();
            }
            else if (rtfDock.Dock == Dock.Top)
            {
                rtfDock.Dock = Dock.None;
                textBox1.Text = rtfDock.Dock.ToString();
            }
            else if (rtfDock.Dock == Dock.None)
            {
                rtfDock.Dock = Dock.Bottom;
                textBox1.Text = rtfDock.Dock.ToString();
            }  
        }

    }
}