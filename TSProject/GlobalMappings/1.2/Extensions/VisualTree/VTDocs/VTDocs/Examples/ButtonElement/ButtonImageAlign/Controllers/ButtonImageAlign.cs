using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonImageAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageAlign_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeImageAlign = this.GetVisualElementById<ButtonElement>("btnChangeImageAlign");

            if (btnChangeImageAlign.ImageAlign == ContentAlignment.MiddleLeft) //Get
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.MiddleRight;//Set
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.MiddleRight)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.MiddleCenter;
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.MiddleCenter)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.BottomCenter;
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.BottomCenter)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.BottomLeft;
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.BottomLeft)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.BottomRight;
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.BottomRight)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.TopCenter;
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.TopCenter)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.TopLeft;
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.TopLeft)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.TopRight;
            }
            else if (btnChangeImageAlign.ImageAlign == ContentAlignment.TopRight)
            {
                btnChangeImageAlign.ImageAlign = ContentAlignment.MiddleLeft;
            }
           
        }
      
    }
}