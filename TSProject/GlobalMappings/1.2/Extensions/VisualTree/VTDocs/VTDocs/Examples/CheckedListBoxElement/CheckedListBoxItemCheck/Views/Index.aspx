<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckedListBox ItemCheck event" ID="windowView1" LoadAction="CheckedListBoxItemCheck\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ItemCheck" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the checked state of an item changes.

            Syntax: public event ItemCheckEventHandler ItemCheck"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Invoke the ItemCheck event by checking or unchecking CheckedListBox CheckBoxes."
            Top="235px" ID="lblExp2">
        </vt:Label>

          <vt:CheckedListBox runat="server" ID="testedCheckedListBox"  Top="285px" ItemCheckAction="CheckedListBoxItemCheck\testedCheckedListBox_ItemCheck">
             <Items>
                <vt:ListItem runat="server" ID="listItem1" Text="CheckBox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem2" Text="CheckBox 2"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem3" Text="CheckBox 3"></vt:ListItem>
            </Items>
        </vt:CheckedListBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Height="20px" Top="390px" Width="450px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="460px" Width="450px" Height="140px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>