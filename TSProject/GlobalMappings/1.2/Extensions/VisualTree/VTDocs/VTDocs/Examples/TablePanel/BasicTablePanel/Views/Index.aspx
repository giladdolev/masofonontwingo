<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic TablePanel" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Initialized Component : TablePanel backColor is set to Lime" Top="520px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:TablePanel runat="server" Text="" Top="550px" Left="130px" ID="tablePanelInitialized" Width="500px" Height="350" BackColor="Lime">
            <ColumnStyles>
                <vt:ColumnStyle runat="server" Text="Col1" Left="130px" ID="Column1" BackColor="Red"> </vt:ColumnStyle>
                <vt:ColumnStyle runat="server" Text="Col2" Left="130px" ID="Column2">
                    <vt:TextBox runat="server" Text="" ID="TextBox1" Width="200px" BackColor="Yellow"></vt:TextBox>
                </vt:ColumnStyle>
            </ColumnStyles>
            <RowStyles>
<%--                <vt:RowStyle runat="server" Text="Row1" Left="130px" ID="Row1" BackColor="Yellow"></vt:RowStyle>
                <vt:RowStyle runat="server" Text="Row2" Left="130px" ID="RowStyle1" BackColor="Brown" ></vt:RowStyle>
                <vt:RowStyle runat="server" Text="Row3" Left="130px" ID="RowStyle2"></vt:RowStyle>
                <vt:RowStyle runat="server" Text="Row4" Left="130px" ID="RowStyle3"></vt:RowStyle>--%>
               <%-- <vt:TextBox runat="server" Text="" ID="TextBox11" Width="200px" BackColor="Yellow"></vt:TextBox>
                <vt:CheckBox runat="server" Text="" ID="CheckBox22" Width="200px" BackColor="Brown"></vt:CheckBox>--%>
            </RowStyles>
        </vt:TablePanel>


        <vt:Label runat="server" Text="Run Time :" Top="70px" Left="50px" ID="TablePanelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:TablePanel runat="server" Text="" Top="140px" Left="130px" ID="tablePanel" Width="500px" Height="350"></vt:TablePanel>
        <vt:Button runat="server" Text="Change BackColor" Top="70px" Left="140px" ID="btnChangeBackgroundColor" Height="36px" TabIndex="1" Width="200px" ClickAction="BasicTablePanel\btnChangeBackgroundColor_Click"></vt:Button>

        <vt:Button runat="server" Text="Add rows and columns" Top="70px" Left="350px" ID="btnRowCol" Height="36px" TabIndex="1" Width="200px" ClickAction="BasicTablePanel\btnRowCol_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
