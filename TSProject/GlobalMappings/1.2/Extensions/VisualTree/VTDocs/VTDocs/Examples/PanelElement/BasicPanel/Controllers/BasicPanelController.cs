using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicPanelController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void ChangeColors_Click(object sender, EventArgs e)
        {
            PanelElement panel = this.GetVisualElementById<PanelElement>("panel1");
           panel.BackColor = Color.Yellow;
           panel.BackColor = Color.White;
           //BoxShadow="0 0 5px rgba(0, 0, 0, 0.3)" BorderRadius="4px 4px 4px 4px"
            
        }

        public void AddBoxShow_Click(object sender, EventArgs e)
        {
            PanelElement panel = this.GetVisualElementById<PanelElement>("panel1");
            var styles = new Dictionary<string, string>()
            {
                {"box-shadow", "0 0 5px rgba(0, 0, 0, 0.3)"},
                {"border-radius", "4px 4px 4px 4px"}
            };
            panel.SetNativeStyles(styles);

            //or
            //panel.SetNativeStyle("box-shadow", "0 0 5px rgba(0, 0, 0, 0.3)");
        }
    }
}