using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press one of the buttons...";
        }

        public void RemoveItems_Click(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTab1.TabItems.Count > 0)
                TestedTab1.TabItems.RemoveAt(TestedTab1.TabItems.Count - 1);

            lblLog.Text = "TestedTab1.TabItems.Items.RemoveAt was invoked";
        }

        private void AddItems_Click(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
           
            TestedTab1.TabItems.Add("New Tab");

            lblLog.Text = "TestedTab1.TabItems.Add was invoked.";

        }

    }
}