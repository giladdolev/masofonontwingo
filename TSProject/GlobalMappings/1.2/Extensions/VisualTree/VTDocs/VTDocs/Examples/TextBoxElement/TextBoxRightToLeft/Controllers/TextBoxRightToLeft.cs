using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        // Change RightToLeft property
        /// <summary>
        /// Handles the Click event of the btnChangeRightToLeft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (testedTextBox.RightToLeft == RightToLeft.No)
            {
                testedTextBox.RightToLeft = RightToLeft.Yes;
                textBox1.Text = "testedTextBox RightToLeft: " + testedTextBox.RightToLeft;
            }
            else if (testedTextBox.RightToLeft == RightToLeft.Yes)
            {
                testedTextBox.RightToLeft = RightToLeft.Inherit;
                textBox1.Text = "testedTextBox RightToLeft: " + testedTextBox.RightToLeft;

            }
            else 
            {
                testedTextBox.RightToLeft = RightToLeft.No;
                textBox1.Text = "testedTextBox RightToLeft: " + testedTextBox.RightToLeft;

            }
        }

    }
}