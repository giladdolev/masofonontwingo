using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Utilities;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridClipboardController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void view_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            DataTable source = new DataTable();
            source.Columns.Add("name/ds", typeof(string));
            source.Columns.Add("c�c", typeof(string));
            source.Columns.Add("(c�c", typeof(bool));

            source.Rows.Add("�����#", "lisa@simpsons.com", true);
            source.Rows.Add("Bart", "Bart@simpsons.com", true);
            source.Rows.Add("Homer", "Homer@simpsons.com", true);
            source.Rows.Add("Lisa", "Lisa@simpsons.com", false);
            source.Rows.Add("Marge", "Marge@simpsons.com", true);
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", true);
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", false);
            gridElement.DataSource = source;
        }
        public void button1_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            ClipboardUtils.CopyText(gridElement.CurrentRow.Cells[gridElement.SelectedColumnIndex].Value.ToString());

        }

    }
}