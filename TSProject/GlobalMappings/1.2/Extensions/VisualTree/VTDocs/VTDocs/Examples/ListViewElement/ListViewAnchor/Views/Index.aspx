<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Anchor" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewAnchor/Form_Load">
        <vt:Label runat="server" Text="Initialized Component :ListView Anchor is set to Bottom" Top="400px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:Panel runat="server" Top="450px" Left="50px" ID="panel2" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" TabIndex="56" Width="500px"></vt:Panel>
        <vt:ListView runat="server" ID="listView2" Top="500" Left="150" Height="100px" Width="300px" Anchor="Bottom">
            <Columns>
                <vt:ColumnHeader runat="server" Text="h1" Width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="h2" Width="200"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="h3" Width="200"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="aaaa">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="11111"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="22222"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="bbbbb">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="33333"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="44444"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Panel runat="server" Top="160px" Left="50px" ID="Panel1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" TabIndex="56" Width="500px"></vt:Panel>
        <vt:ListView runat="server" ID="listView1" Top="200" Left="150" Height="100px" Width="100px"/>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change ListView1 Anchor" Top="112px" Left="130px" ID="btnAnchor" Height="30px" TabIndex="1" Width="250px" ClickAction="ListViewAnchor\btnAnchor_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
