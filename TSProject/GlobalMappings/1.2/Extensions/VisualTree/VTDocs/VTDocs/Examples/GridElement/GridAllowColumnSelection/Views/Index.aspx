﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="Grid AllowColumnSelection Property" LoadAction="GridAllowColumnSelection\OnLoad">


        <vt:Label runat="server" Left="250px" SkinID="Title" Text="AllowColumnSelection" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether users can select an entire column

           Syntax: public bool AllowColumnSelection { get; set; }

            'true' if users can select entire columns; otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Grid runat="server" Text="Grid" Top="185px" CssClass="vt-testedGrid1" Left="80px" ID="TestedGrid1" dataChangedAction="GridAllowColumnSelection\dataChanged" ClientEventAction="GridAllowColumnSelection/EditChangedAction">
            <Columns>
                <vt:GridColumn runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="315px" ID="lblLog1"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="AllowColumnSelection property of this Grid is initially set to 'true',
            You should be able to select an entire column."
            Top="405px" ID="lblExp1">
        </vt:Label>

        <%--   <vt:Grid runat="server" Text="TestedGrid" Top="470px" CssClass="vt-testedGrid2"  Left="80px" ID="TestedGrid2" AllowColumnSelection="true"  AfterrenderAction="GridAllowColumnSelection\test">
            <Columns>
                
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns> 
        </vt:Grid>--%>

        <vt:Label runat="server" SkinID="Log" Top="600px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change AllowColumnSelection value >>" Top="650px" Width="240px" ID="btnhangeAllowColumnSelection" ClickAction="GridAllowColumnSelection/btnhangeAllowColumnSelection_click"></vt:Button>
    </vt:WindowView>

</asp:Content>
