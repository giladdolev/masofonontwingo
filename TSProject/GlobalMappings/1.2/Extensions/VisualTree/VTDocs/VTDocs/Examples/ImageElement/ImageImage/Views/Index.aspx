<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image Image Property" ID="windowView2" LoadAction="ImageImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Image" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the image that is displayed on a Image control.
            The default value is null.
            
            Syntax: public Image Image { get; set; }" Top="75px" ID="lblDefinition" ></vt:Label>                

        <vt:Image runat="server" Text="Image" Top="160px" ID="btnTestedImage1" ImageReference="" BorderStyle="Solid"></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Image property of this 'TestedImage' is initially with image" Top="375px"  ID="lblExp1"></vt:Label>     
        
        <vt:Image runat="server" Text="TestedImage" ImageReference="~/Content/Images/icon.jpg" BorderStyle="Solid" Top="425px" ID="btnTestedImage2"></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="530px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Image Value >>"  Top="595px" Width="180px" ID="btnChangeImage" ClickAction="ImageImage\btnChangeImage_Click"></vt:Button>
             
    </vt:WindowView>
</asp:Content>

