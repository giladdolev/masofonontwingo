using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LinkLabelMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LinkLabelElement TestedLinkLabel = this.GetVisualElementById<LinkLabelElement>("TestedLinkLabel");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "LinkLabel MaximumSize value: " + TestedLinkLabel.MaximumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
        }

        public void btnChangeMaximumSize_Click(object sender, EventArgs e)
        {
            LinkLabelElement TestedLinkLabel = this.GetVisualElementById<LinkLabelElement>("TestedLinkLabel");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedLinkLabel.MaximumSize == new Size(180, 50))
            {
                TestedLinkLabel.MaximumSize = new Size(230, 20);
                lblLog1.Text = "LinkLabel MaximumSize value: " + TestedLinkLabel.MaximumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
            }
            else if (TestedLinkLabel.MaximumSize == new Size(230, 20))
            {
                TestedLinkLabel.MaximumSize = new Size(210, 60);
                lblLog1.Text = "LinkLabel MaximumSize value: " + TestedLinkLabel.MaximumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
            }
            else
            {
                TestedLinkLabel.MaximumSize = new Size(180, 50);
                lblLog1.Text = "LinkLabel MaximumSize value: " + TestedLinkLabel.MaximumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
            }

        }

        public void btnSetToZeroMaximumSize_Click(object sender, EventArgs e)
        {
            LinkLabelElement TestedLinkLabel = this.GetVisualElementById<LinkLabelElement>("TestedLinkLabel");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedLinkLabel.MaximumSize = new Size(0, 0);
            lblLog1.Text = "LinkLabel MaximumSize value: " + TestedLinkLabel.MaximumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;

        }


    }
}