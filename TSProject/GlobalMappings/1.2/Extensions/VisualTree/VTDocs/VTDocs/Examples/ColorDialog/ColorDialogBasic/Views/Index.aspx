<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="6, 13" Text="Basic ColorDialog" ID="Window1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="700px" Width="800px">
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Select Color" Top="72px" Left="171px" ClickAction="ColorDialogBasic\btnSelectColor_Click" ID="btnSelectColor" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="54px" Width="225px">
		</vt:Button>
		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="189px" Left="62px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="228px" TabIndex="1" Width="460px">
		</vt:TextBox>
        
	</vt:WindowView>

   <%-- <vt:ComponentManager runat="server" >
		<vt:ColorDialog runat="server" ID="exampleTimer">
		</vt:ColorDialog>
	</vt:ComponentManager>--%>

</asp:Content>
