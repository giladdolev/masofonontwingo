<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer GetType Method" ID="windowView2" LoadAction="TimerGetType\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="GetType" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the Type of the current instance.

            Syntax: public Type GetType()" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the GetType button will invoke the 
            GetType method of this Timer." Top="250px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" SkinID="TimerLabel" Top="345px" ID="timerLabel"></vt:Label>

        <vt:TextBox runat="server" SkinID="TimerTextBox" Top="340px" ID="timerTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="380"></vt:Label>

        <vt:Button runat="server" Text="GetType >>" Top="460px" ID="btnGetType" ClickAction="TimerGetType\GetType_Click"></vt:Button>

    </vt:WindowView>

    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="TestedTimer" Enabled="true" Interval="1000" TickAction="TimerGetType\TestedTimer_Tick"></vt:Timer>
	</vt:ComponentManager>
</asp:Content>
