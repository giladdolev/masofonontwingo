<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Enabled Property" ID="windowView1" LoadAction="SplitContainerEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:SplitContainer runat="server" Text="SplitContainer" Top="195px" ID="TestedSplitContainer1"></vt:SplitContainer>           

        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedSplitContainer' is initially set to 'false'." Top="405px" ID="lblExp1"></vt:Label> 

        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="465px" Enabled ="false" ID="TestedSplitContainer2"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="560px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="625px" ID="btnChangeEnabled" Width="180px" ClickAction="SplitContainerEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
