<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic CheckBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="CheckAlign:" Top="66px" Left="32px" ID="lblCheckAlign" Height="13px" Width="67px"></vt:Label>
        <vt:Label runat="server" Text="CheckState:" Top="130px" Left="33px" ID="lblcCheckState" Height="13px" Width="66px"></vt:Label>
        <vt:Label runat="server" Text="Appearance:" Top="200px" Left="32px" ID="lblAppearance" Height="13px" Width="68px"></vt:Label>
        <vt:CheckBox runat="server" CheckAlign="MiddleRight" Text="checkBox1" Top="65px" Left="153px" ID="chkCheckAlign" Height="17px" Width="80px"></vt:CheckBox>
        <vt:CheckBox runat="server" CheckState="Indeterminate" IsChecked="True" Text="checkBox2" Top="129px" Left="153px" ID="chkCheckState" Height="17px" Width="80px"></vt:CheckBox>
        <vt:CheckBox runat="server" Text="checkBox3" Top="200px" Left="153px" ID="chkAppearance" Height="23px" Width="71px"></vt:CheckBox>
        <vt:Label runat="server" Text="InitializeComponent" Top="15px" Left="150px" ID="label1" Height="13px" Width="98px"></vt:Label>
        <vt:Label runat="server" Text="RunTime" Top="15px" Left="335px" ID="label2" Height="13px" Width="50px"></vt:Label>
        <vt:CheckBox runat="server" Text="checkBox4" Top="65px" Left="318px" ID="chkCheckAlignRunTime" Height="17px" Width="80px"></vt:CheckBox>
        <vt:CheckBox runat="server" Text="checkBox5" Top="130px" Left="318px" ID="chkCheckStateRunTime" Height="17px" Width="80px"></vt:CheckBox>
        <vt:CheckBox runat="server" Text="checkBox6" Top="201px" Left="318px" ID="chkAppearanceRunTime" Height="17px" Width="80px"></vt:CheckBox>
        <vt:Button runat="server" Text="Change CheckAlign" Top="56px" Left="494px" ID="btnChangeCheckAlign" Height="23px" Width="186px" ClickAction="CheckBoxBasic/btnChangeCheckAlign_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Appearance" Top="200px" Left="494px" ID="btnAppearance" Height="23px" Width="186px"></vt:Button>
        <vt:Button runat="server" Text="Change CheckState" Top="130px" Left="494px" ID="btnCheckState" Height="23px" Width="186px" ClickAction="CheckBoxBasic/btnCheckState_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Font Style" Top="259px" Left="494px" ID="btnChangeFontStyle" Height="23px" Width="186px" ClickAction="CheckBoxBasic/btnChangeFontStyle_Click"></vt:Button>
        <vt:CheckBox runat="server" Text="checkBox7" Top="265px" Left="318px" ID="chkFontStyle" Height="17px" Width="80px"></vt:CheckBox>
        <vt:Label runat="server" Text="FontStyle:" Top="265px" Left="33px" ID="lblFontStyle" Height="13px" Width="54px"></vt:Label>
        <vt:CheckBox runat="server" Text="checkBox8" Top="318px" Left="318px" ID="chkLocation" Height="17px" Width="80px"></vt:CheckBox>
        <vt:Button runat="server" Text="Change Location" Top="312px" Left="494px" ID="btnChangeLocation" Height="23px" Width="186px" ClickAction="CheckBoxBasic/btnChangeLocation_Click"></vt:Button>
        <vt:Label runat="server" Text="Location:" Top="322px" Left="32px" ID="lblLocation" Height="13px" Width="51px"></vt:Label>
    </vt:WindowView>
</asp:Content>
