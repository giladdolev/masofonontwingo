<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Anchor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:TextBox runat="server" Text="some text" Top="18px" Left="50px" ID="txt1" Height="26px"  Width="200px"></vt:TextBox>
        <vt:Button runat="server" ID="btn1" Text="Clear Text" Top="45px" Left="50px" Height="25px" Width="80px" ClickAction="TextBoxClear\btn1_click"></vt:Button>
    </vt:WindowView>
</asp:Content>
