using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedListView1.BackColor.ToString();

            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedListView2.BackColor.ToString();

        }

        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedListView2.BackColor == Color.Orange)
            {
                TestedListView2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedListView2.BackColor.ToString();
            }
            else
            {
                TestedListView2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedListView2.BackColor.ToString();

            }
        }
    }
}