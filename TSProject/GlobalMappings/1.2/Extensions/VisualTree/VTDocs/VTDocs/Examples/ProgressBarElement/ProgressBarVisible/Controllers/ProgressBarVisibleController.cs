using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarVisibleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //ProgressBarAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar1 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar1");
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Visible value: " + TestedProgressBar1.Visible;
            lblLog2.Text = "Visible value: " + TestedProgressBar2.Visible + '.';

        }

        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedProgressBar2.Visible = !TestedProgressBar2.Visible;
            lblLog2.Text = "Visible value: " + TestedProgressBar2.Visible + '.';
        }

    }
}