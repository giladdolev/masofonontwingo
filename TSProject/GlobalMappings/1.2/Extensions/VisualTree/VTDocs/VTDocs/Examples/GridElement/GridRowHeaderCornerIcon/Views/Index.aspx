﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid RowHeaderCornerIcon property" ID="Form1" LoadAction="GridRowHeaderCornerIcon\OnLoad">
         <vt:Label runat="server" SkinID="Title" Text="RowHeaderCornerIcon" Left="230px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the icon at the top left RowHeader cell of the control.
                        
            Syntax: public ResourceReference RowHeaderCornerIcon { get; set; }" Top="75px"  ID="Label2"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="Label3"></vt:Label>
        <vt:Label runat="server" Text="In the following example, the RowHeaderCornerIcon property is initially set with an image.
            You can change or clear the image by pressing the buttons." 
            Top="240px"  ID="Label4"></vt:Label> 
              
        <vt:Grid runat="server" Top="300px" ID="TestedGrid" RowHeaderCornerIcon ="~/Content/Images/gizmox.png">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="430px" Width="400px" Height="40px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change RowHeader CornerIcon >>" Top="530px" Width="200px" ID="btnChangeRowHeaderCornerIcon" ClickAction="GridRowHeaderCornerIcon\btnChangeRowHeaderCornerIcon_Click"></vt:Button>
        <vt:Button runat="server" Text="Clear RowHeader CornerIcon >>" Top="530px" Left="290px" Width="200px" ID="btnClearRowHeaderCornerIcon" ClickAction="GridRowHeaderCornerIcon\btnClearRowHeaderCornerIcon_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
