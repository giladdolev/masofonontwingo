using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicTextBoxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void Onload(object sender, EventArgs e)
        {
           

           
        }

        public void ChangeFont_Click(object sender, EventArgs e)
        {
            TextBoxElement text = this.GetVisualElementById<TextBoxElement>("textBox1");
            text.Font = new System.Drawing.Font(text.Font, text.Font.Style | System.Drawing.FontStyle.Underline | System.Drawing.FontStyle.Italic);
        }
        public void ChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement text1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            text1.Size = new System.Drawing.Size(100, 12);
            TextBoxElement text2 = this.GetVisualElementById<TextBoxElement>("textBox2");
            text2.Size = new System.Drawing.Size(200, 40);
        }

        public void Keyd(object sender, KeyEventArgs e)
        {

        }

        public void aaa(object sender, EventArgs e)
        {

        }

        public void keydown(object sender, KeyDownEventArgs e)
        {

        }
        public void keypress(object sender, EventArgs e)
        {

        }
        public void keyppress(object sender, EventArgs e)
        {

        }

        public void dirtycahnge(object sender, EventArgs e)
        {
        }
    }
}