using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button PixelTop is: " + TestedButton.PixelTop;

        }

        public void btnChangePixelTop_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.PixelTop == 370)
            {
                TestedButton.PixelTop = 350;
                lblLog.Text = "Button PixelTop is: " + TestedButton.PixelTop;

            }
            else
            {
                TestedButton.PixelTop = 370;
                lblLog.Text = "Button PixelTop is: " + TestedButton.PixelTop;
            }

        }
    }
}