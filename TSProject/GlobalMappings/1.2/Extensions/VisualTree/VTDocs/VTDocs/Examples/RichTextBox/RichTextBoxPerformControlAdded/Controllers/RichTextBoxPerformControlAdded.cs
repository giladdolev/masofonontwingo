using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPerformControlAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlAdded_Click(object sender, EventArgs e)
        {
            
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");

            ControlEventArgs args = new ControlEventArgs(testedRichTextBox);

            testedRichTextBox.PerformControlAdded(args);
        }

        public void testedRichTextBox_ControlAdded(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRichTextBox ControlAdded event method is invoked");
        }

    }
}