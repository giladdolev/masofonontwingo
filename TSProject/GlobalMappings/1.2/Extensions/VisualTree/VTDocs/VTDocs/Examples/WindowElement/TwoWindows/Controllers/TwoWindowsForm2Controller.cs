﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    public class TwoWindowsForm2Controller : Controller
    {
        public ActionResult TwoWindowsForm2()
        {
            return View(new TwoWindowsForm2());
        }
    }
}
