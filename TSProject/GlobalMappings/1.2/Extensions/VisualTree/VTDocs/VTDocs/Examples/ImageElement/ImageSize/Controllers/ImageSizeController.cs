using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedImage.Size;
        }
        private void btnChangeImageSize_Click(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedImage.Size == new Size(150, 90)) //Get
            {
                TestedImage.Size = new Size(200, 50); //Set
                lblLog.Text = "Size value: " + TestedImage.Size;
            }
            else  
            {
                TestedImage.Size = new Size(150, 90);
                lblLog.Text = "Size value: " + TestedImage.Size;
            }
             
        }

    }
}