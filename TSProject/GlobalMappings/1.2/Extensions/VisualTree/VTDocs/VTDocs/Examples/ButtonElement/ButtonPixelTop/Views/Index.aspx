<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button PixelTop Property" ID="windowView2" LoadAction="ButtonPixelTop\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelTop" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the Top edge of the control
             and the Top edge of its container's client area.
            
            Syntax: public int PixelTop { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Top property of this Button is initially set to 350px." Top="300px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedButton -->
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Top="350px" ID="TestedButton"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelTop value >>" Top="475px" ID="btnChangePixelTop" ClickAction="ButtonPixelTop\btnChangePixelTop_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
