using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarBoundsController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ToolBar bounds are: \\r\\n" + TestedToolBar.Bounds;

        }
        public void btnChangeToolBarBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            if (TestedToolBar.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedToolBar.Bounds = new Rectangle(80, 355, 400, 40);
                lblLog.Text = "ToolBar bounds are: \\r\\n" + TestedToolBar.Bounds;

            }
            else
            {
                TestedToolBar.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "ToolBar bounds are: \\r\\n" + TestedToolBar.Bounds;
            }

        }

    }
}