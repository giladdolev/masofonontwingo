﻿
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button GetType Method" ID="windowView2" LoadAction="ButtonGetType\OnLoad">
      
       
        <vt:Label runat="server" SkinID="Title" Text="GetType" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the Type of the current instance.

            Syntax: public Type GetType()" Top="75px" ID="lblDefinition"></vt:Label>
   


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the GetType button will invoke the 
            GetType method of this button." 
            Top="250px" ID="lblExp2"></vt:Label>

        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Top="340px" ID="TestedButton"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="390"></vt:Label>

        <vt:Button runat="server" Text="GetType >>" Top="470px" ID="btnGetType" ClickAction="ButtonGetType\GetType_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
