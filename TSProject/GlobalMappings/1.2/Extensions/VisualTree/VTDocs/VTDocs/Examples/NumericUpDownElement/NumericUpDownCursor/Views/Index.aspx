<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Cursor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:NumericUpDown runat="server" Text="Cursor is set to .. bug" Cursor="System.Web.VisualTree.Elements.CursorElement" Top="45px" Left="140px" ID="nudCursor" Height="20px" Width="100px"></vt:NumericUpDown>           

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:NumericUpDown runat="server" Text="NumericUpDown Cursor - runtime" Top="115px" Left="140px" ID="nudRuntimeCursor" Height="20px" TabIndex="1" Width="100px"></vt:NumericUpDown>

        <vt:Button runat="server" Text="Change NumericUpDown Cursor" Top="115px" Left="400px" ID="btnChangeCursor" Height="36px" TabIndex="1" Width="200px" ClickAction="NumericUpDownCursor\btnChangeCursor_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="180px" Left="110px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
