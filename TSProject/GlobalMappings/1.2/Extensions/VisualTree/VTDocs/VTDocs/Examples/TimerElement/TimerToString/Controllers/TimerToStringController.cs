using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerToStringController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TimerToString());
        }

     //   TimerToString TimerToStringModel;        
        private TimerToString ViewModel
        {
            get { return this.GetRootVisualElement() as TimerToString; }
        }

        public void TestedTimer_Tick(object sender, EventArgs e)
        {
            TextBoxElement timerTextBox = this.GetVisualElementById<TextBoxElement>("timerTextBox");
            timerTextBox.Text = ViewModel.durationProperty++.ToString();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press the 'ToString >>' button...";
        }

        public void ToString_Click(object sender, EventArgs e)
        {

            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "The Component name is: \\r\\n" + TestedTimer.ToString();
        }
    }
}