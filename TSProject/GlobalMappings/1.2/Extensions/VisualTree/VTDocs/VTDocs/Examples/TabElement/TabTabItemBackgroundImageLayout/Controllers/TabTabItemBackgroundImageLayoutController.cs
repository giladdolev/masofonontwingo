using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemBackgroundImageLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            TabItem Tab1Item1 = TestedTab1.TabItems[0];
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackgroundImageLayout value: " + Tab1Item1.BackgroundImageLayout.ToString();

            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + Tab1Item2.BackgroundImageLayout.ToString() + ".";

        }
        private void btnNone_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            Tab1Item2.BackgroundImageLayout = ImageLayout.None;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + Tab1Item2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnTile_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            Tab1Item2.BackgroundImageLayout = ImageLayout.Tile;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + Tab1Item2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnCenter_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            Tab1Item2.BackgroundImageLayout = ImageLayout.Center;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + Tab1Item2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnStretch_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            Tab1Item2.BackgroundImageLayout = ImageLayout.Stretch;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + Tab1Item2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnZoom_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab1Item2 = TestedTab2.TabItems[0];
            Tab1Item2.BackgroundImageLayout = ImageLayout.Zoom;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + Tab1Item2.BackgroundImageLayout.ToString() + ".";
        }
        //TabAlignment
        //public void btnChangeBackgroundImageLayout_Click(object sender, EventArgs e)
        //{
        //    TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
        //    TabElement tabRuntimeBackgroundImageLayout = this.GetVisualElementById<TabElement>("tabRuntimeBackgroundImageLayout");

        //    if (tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout == ImageLayout.Center)
        //    {
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout = ImageLayout.None;
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem2"].BackgroundImageLayout = ImageLayout.None;
        //        textBox1.Text = "Tabpage1: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //        textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //    }
        //    else if (tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout == ImageLayout.None)
        //    {
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout = ImageLayout.Stretch;
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem2"].BackgroundImageLayout = ImageLayout.Stretch;
        //        textBox1.Text = "Tabpage1: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //        textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //    }
        //    else if (tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout == ImageLayout.Stretch)
        //    {
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout = ImageLayout.Tile;
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem2"].BackgroundImageLayout = ImageLayout.Tile;
        //        textBox1.Text = "Tabpage1: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //        textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //    }
        //    else if (tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout == ImageLayout.Tile)
        //    {
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout = ImageLayout.Zoom;
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem2"].BackgroundImageLayout = ImageLayout.Zoom;
        //        textBox1.Text = "Tabpage1: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //        textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //    }
        //    else 
        //    {
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout = ImageLayout.Center;
        //        tabRuntimeBackgroundImageLayout.TabItems["TabItem2"].BackgroundImageLayout = ImageLayout.Center;
        //        textBox1.Text = "Tabpage1: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //        textBox1.Text += "\\r\\nTabpage2: " + tabRuntimeBackgroundImageLayout.TabItems["TabItem1"].BackgroundImageLayout.ToString();
        //    }
        //}

    }
}