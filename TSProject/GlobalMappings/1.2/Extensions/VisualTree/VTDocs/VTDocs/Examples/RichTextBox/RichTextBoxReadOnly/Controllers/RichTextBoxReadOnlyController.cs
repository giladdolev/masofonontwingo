using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxReadOnlyController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void clickBtn1(object sender, EventArgs e)
        {
            RichTextBox lbxEventos = this.GetVisualElementById<RichTextBox>("richTextBox1");
            lbxEventos.ReadOnly = true; ;
        }
        public void clickBtn2(object sender, EventArgs e)
        {
            RichTextBox lbxEventos = this.GetVisualElementById<RichTextBox>("richTextBox1");
            lbxEventos.ReadOnly = false; ;
        }
    }
}