﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelWordWrapController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Label AutoSize Value: " + TestedLabel1.AutoSize + "\\r\\nLabel WordWrap Value: " + TestedLabel1.WordWrap + 
                "\\r\\nLabel Size Value: " + TestedLabel2.Size;
            lblLog2.Text = "Label AutoSize Value: " + TestedLabel2.AutoSize + '.' + "\\r\\nLabel WordWrap Value: " + TestedLabel2.WordWrap + '.' + 
                "\\r\\nLabel Size Value: " + TestedLabel2.Size;
        }

        private void btnToggleWordWrap_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedLabel2.WordWrap = !TestedLabel2.WordWrap;

            lblLog2.Text = "Label AutoSize Value: " + TestedLabel2.AutoSize + '.' + "\\r\\nLabel WordWrap Value: " + TestedLabel2.WordWrap + '.' +
                "\\r\\nLabel Size Value: " + TestedLabel2.Size;
        }
      
    }
}