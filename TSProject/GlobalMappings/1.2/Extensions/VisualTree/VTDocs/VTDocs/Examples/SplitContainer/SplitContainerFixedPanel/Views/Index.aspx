<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

     <vt:WindowView runat="server" Text="SplitContainer FixedPanel Property" ID="windowView1" LoadAction="SplitContainerFixedPanel\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="250px" Text="FixedPanel" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets which SplitContainer panel remains the same size when the container is resized.

            Syntax: public FixedPanel FixedPanel { get; set; }
            FixedPanel Enum: None, Panel1 or Panel2. The default is None."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested SplitContainer1 -->
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="170px" ID="TestedSplitContainer1">
            <vt:SplitterPanel runat="server">
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" Width="50px">
            </vt:SplitterPanel>
        </vt:SplitContainer>


        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Button runat="server" Text="Increase Container Width >>" Top="300px" Width="200px" ID="btnDoubleWidthButton1" ClickAction="SplitContainerFixedPanel\btnDoubleWidthButton1_Click"></vt:Button>
        <vt:Button runat="server" Text="Reset Width >>" Top="300px" Left="300px" Width="200px" ID="btnResetWidthButton1" ClickAction="SplitContainerFixedPanel\btnResetWidthButton1_Click"></vt:Button>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="FixedPanel property of this SplitContainer is initially set to 'Panel1'. Panel1 is the left Panel.
             Use the Increase and Reset buttons to see the behavior. You can change the FixedPanel property
             using the 'Change FixedPanel value' button." Top="390px" ID="lblExp1"></vt:Label>

        <!-- Tested SplitContainer2 -->
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" FixedPanel="Panel1" Top="460px" ID="TestedSplitContainer2">
            <vt:SplitterPanel runat="server">
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" Width="50px">
            </vt:SplitterPanel>
        </vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="555px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Increase Container Width >>" CssClass="vt-btn-inc" Top="590px" Width="200px" ID="btnDoubleWidthButton2" ClickAction="SplitContainerFixedPanel\btnDoubleWidthButton2_Click"></vt:Button>
        <vt:Button runat="server" Text="Reset Width >>" CssClass="vt-btn-reset" Top="590px" Left="300px" Width="200px" ID="btnResetWidthButton2" ClickAction="SplitContainerFixedPanel\btnResetWidthButton2_Click"></vt:Button>

        <vt:Button runat="server" Text="Change FixedPanel value >>" Top="630px" Width="200px" ID="btnChangeSplitContainerFixedPanel" ClickAction="SplitContainerFixedPanel\btnChangeFixedPanel_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
