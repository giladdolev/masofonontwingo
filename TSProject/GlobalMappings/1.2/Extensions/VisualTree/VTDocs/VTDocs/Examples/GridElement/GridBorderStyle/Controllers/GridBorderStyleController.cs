using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "BorderStyle value: " + TestedGrid1.BorderStyle;
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
        }

        public void btnChangeGridBorderStyle_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.BorderStyle == BorderStyle.None)
            {
                TestedGrid2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Dotted)
            {
                TestedGrid2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Double)
            {
                TestedGrid2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedGrid2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedGrid2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Groove)
            {
                TestedGrid2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Inset)
            {
                TestedGrid2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Dashed)
            {
                TestedGrid2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.NotSet)
            {
                TestedGrid2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Outset)
            {
                TestedGrid2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Ridge)
            {
                TestedGrid2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedGrid2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else if (TestedGrid2.BorderStyle == BorderStyle.Solid)
            {
                TestedGrid2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
            else
            {
                TestedGrid2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedGrid2.BorderStyle + '.';
            }
        }

    }
}