using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridReadOnlyController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}


        public void Page_load(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "ReadOnly value: " + TestedGrid1.ReadOnly;

            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "ReadOnly value: " + TestedGrid2.ReadOnly;
        }

		private void btnChangeReadOnly_Click(object sender, EventArgs e)
		{
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.ReadOnly == true)
            {
                TestedGrid2.ReadOnly = false;
                lblLog2.Text = "ReadOnly value: " + TestedGrid2.ReadOnly;

            }
            else
            {
                TestedGrid2.ReadOnly = true;
                lblLog2.Text = "ReadOnly value: " + TestedGrid2.ReadOnly;
            }
		}
    
	}
}
