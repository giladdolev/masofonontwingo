<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Enabled Property" ID="windowView1" LoadAction="RichTextBoxEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:RichTextBox runat="server" Text="RichTextBox" Top="195px" CssClass="vt-test-rtb-1" ID="TestedRichTextBox1"></vt:RichTextBox>           

        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedRichTextBox' is initially set to 'false'." Top="405px" ID="lblExp1"></vt:Label> 

        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="465px" Enabled ="false" CssClass="vt-test-rtb-2" ID="TestedRichTextBox2"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="560px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="625px" ID="btnChangeEnabled" Width="180px" ClickAction="RichTextBoxEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
