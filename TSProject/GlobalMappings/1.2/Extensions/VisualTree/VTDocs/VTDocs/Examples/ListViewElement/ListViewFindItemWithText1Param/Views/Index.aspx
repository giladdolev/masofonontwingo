<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView FindItemWithText Method (String)" ID="windowView2" LoadAction="ListViewFindItemWithText1Param\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="280px" Text="FindItemWithText" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Finds the first ListViewItem that begins with the specified text value.
            
            Syntax: public ListViewItem FindItemWithText(string text)
            Parameter 'text': The text to search for.
            Return Value: The first ListViewItem that begins with the specified text value or null if the list is empty 
            or there is no matching item." Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Invoke the FindItemWithText(String) method using the textBox and button below" Top="270px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedListView -->
        <vt:ListView runat="server" Text="TestedListView" Top="320px" Height="125px" ID="TestedListView">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="Haim Michael">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Yaron Moshe"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="David"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Cohen">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Michael"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="Shimon Horowitz"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Gali">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Tiqva Cohen"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="Tirzha"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Itai">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="Tiki Halifa"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="Rachel Emenu"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="460px" Width="490px" Height="50px" ID="lblLog"></vt:Label>
          
         <vt:Label runat="server" Text="Enter String: " Top="555px" ID="lblEnterString" ClickAction="ListViewFindItemWithText1Param\ChangePixelLeft_Click"></vt:Label>

        <vt:TextBox runat="server" Text="Enter String" Top="555px" Left="140px" ID="txtEnterString" ClickAction=" ListViewFindItemWithText1Param\ChangePixelLeft_Click"></vt:TextBox>

        <vt:Button runat="server" Text="FindItemWithText(String) >>" Width="220px" Left="350px" Top="555px" ID="FindItemWithText" ClickAction="ListViewFindItemWithText1Param\FindItemWithText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>