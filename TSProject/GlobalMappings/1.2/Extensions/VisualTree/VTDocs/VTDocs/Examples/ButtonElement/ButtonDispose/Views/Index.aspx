<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Dispose Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Button runat="server" Text="Tested Button" Top="45px" Left="140px" ID="btnTestedButton" Height="36px" Width="220px" ClickAction ="ButtonDispose\btnTestedButton_Click"></vt:Button> 

         <vt:Button runat="server" Text="Invoke Dispose" Top="45px" Left="400px" ID="btnDispose" Height="36px" Width="220px" ClickAction ="ButtonDispose\btnDispose_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
