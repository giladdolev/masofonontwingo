<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Application" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewApplication/Form_Load">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ListView runat="server" ID="listView1" Top="200" Left="150" Height="100px" Width="300px"/>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Click to show the Application value of ListView1" Top="112px" Left="150px" ID="btnApplication" Height="30px" TabIndex="1" Width="350px" ClickAction="ListViewApplication\btnApplication_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
