using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedPanel1.BackColor.ToString();

            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedPanel2.BackColor.ToString();

        }


        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedPanel2.BackColor == Color.Orange)
            {
                TestedPanel2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedPanel2.BackColor.ToString();
            }
            else
            {
                TestedPanel2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedPanel2.BackColor.ToString();

            }
        }


    }
}