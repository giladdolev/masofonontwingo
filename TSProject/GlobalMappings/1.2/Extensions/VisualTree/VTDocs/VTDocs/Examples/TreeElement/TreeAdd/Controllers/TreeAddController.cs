using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeAddController : Controller
    {

        static private int i = 0;
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void loadAction(object sender, EventArgs e)
        {
            TreeElement tree1 = this.GetVisualElementById<TreeElement>("tree1");
            TreeItem tree2 = new TreeItem((++i).ToString());
            tree1.Add(tree2, false);
        }

        public void Click(object sender, EventArgs e)
        {
            TreeElement tree1 = this.GetVisualElementById<TreeElement>("tree1");
            TreeItem tree2 = new TreeItem((++i).ToString());
            tree1.Add(tree2, false);
            tree1.Refresh();
        }
    }
}