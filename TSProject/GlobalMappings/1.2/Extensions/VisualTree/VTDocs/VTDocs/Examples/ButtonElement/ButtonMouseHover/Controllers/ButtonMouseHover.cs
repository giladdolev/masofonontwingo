using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonMouseHoverController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Press the Bottons...";
        }

        public void btnButton1_MouseHover(object sender, EventArgs e)
        {
            ButtonElement btnButton1 = this.GetVisualElementById<ButtonElement>("btnButton1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "Button1 was Hovered";
            txtEventLog.Text += "\\r\\nButton: Button1, MouseHover event is invoked";

            if (btnButton1.BackColor == Color.Orange)
            {
                btnButton1.BackColor = Color.Green;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton1.BackColor.ToString();
            }
            else
            {
                btnButton1.BackColor = Color.Orange;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton1.BackColor.ToString();
            }
        }

        public void btnButton2_MouseHover(object sender, EventArgs e)
        {
            ButtonElement btnButton2 = this.GetVisualElementById<ButtonElement>("btnButton2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "Button2 was Hovered";
            txtEventLog.Text += "\\r\\nButton: Button2, MouseHover event is invoked";

            if (btnButton2.BackColor == Color.DeepPink)
            {
                btnButton2.BackColor = Color.Indigo;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton2.BackColor.ToString();
            }
            else
            {
                btnButton2.BackColor = Color.DeepPink;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton2.BackColor.ToString();
            }
        }

        public void btnButton3_MouseHover(object sender, EventArgs e)
        {
            ButtonElement btnButton3 = this.GetVisualElementById<ButtonElement>("btnButton3");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "Button3 was Hovered";
            txtEventLog.Text += "\\r\\nButton: Button3, MouseHover event is invoked";

            if (btnButton3.BackColor == Color.Red)
            {
                btnButton3.BackColor = Color.Blue;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton3.BackColor.ToString();
            }
            else
            {
                btnButton3.BackColor = Color.Red;
                lblLog.Text += "\\r\\nBackColor value: " + btnButton3.BackColor.ToString();
            }
        }
    }
}