using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowSizeController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowSize());
        }
        private WindowSize ViewModel
        {
            get { return this.GetRootVisualElement() as WindowSize; }
        }

        public void btnChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            
           if(this.ViewModel.Size == new Size(700, 800))
           {
               this.ViewModel.Size = new Size(1000, 400);
               textBox1.Text = this.ViewModel.Size.ToString();
           }
           else
           {
               this.ViewModel.Size = new Size(700, 800);
               textBox1.Text = this.ViewModel.Size.ToString();

           }
        }
    }
}