using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             lblLog.Text = "Top Value: " + TestedPanel.Top;

         }
         public void btnChangeTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
             if (TestedPanel.Top == 285)
             {
                 TestedPanel.Top = 310;
                 lblLog.Text = "Top Value: " + TestedPanel.Top;

             }
             else
             {
                 TestedPanel.Top = 285;
                 lblLog.Text = "Top Value: " + TestedPanel.Top;
             }

         }

    }
}