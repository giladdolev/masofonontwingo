using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;

namespace HelloWorldApp
{
    public class BasicGridController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void page_load(object sender, EventArgs e)
        {

            WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
            gridElement.WidgetTemplateWidth = 300;
            gridElement.WidgetTemplateModel = "vm-spviewmodelPalletManual";
            gridElement.WidgetTemplate = "spgridcomponentPalletManual";
            gridElement.WidgetTemplateDataAttachMethod = "VT_WidgetColumnAttachPalletManual";
            gridElement.WidgetTemplateDataMembers = "row_no,barcode,material_name,expected_material_quantity,order_quantity,invoice_pack_quantity,doc_no_en,doc_no,declines,invoice_data";

            //WidgetGridElement WidgetGrid1 = this.GetVisualElementById<WidgetGridElement>("WidgetGrid1");
            //WidgetGrid1.WidgetTemplateWidth = 300;
            //WidgetGrid1.WidgetTemplateModel = "vm-spviewmodelReturnsDetails";
            //WidgetGrid1.WidgetTemplate = "spgridcomponentReturnsDetails";
            //WidgetGrid1.WidgetTemplateDataAttachMethod = "VT_WidgetColumnAttachReturnsDetails";

            //WidgetGrid1.WidgetTemplateDataMembers = "row_no,barcode,material_name,material_quantity,supplier_number,declines,invoice_data";
          
            //DataTable cmbsource = new DataTable();
            //cmbsource.Columns.Add("value", typeof(string));
            //cmbsource.Columns.Add("text", typeof(string));

            //cmbsource.Rows.Add("James", "Dean");
            //cmbsource.Rows.Add("Bob", "Marley");
            //string JsonData1 = WidgetColumnMultiComboBoxElement.GetComboBoxData("asdsad", cmbsource);

            //DataTable source2 = new DataTable();
            //source2.Columns.Add("materials_material_name", typeof(string));
            //source2.Columns.Add("inv_pack_quantity", typeof(string));
            //source2.Columns.Add("mesh3aref", typeof(string));
            //source2.Columns.Add("expected_material_quantity", typeof(string));
            //source2.Columns.Add("invoice_number", typeof(string));
            //source2.Columns.Add("invoice_data", typeof(string));
            //for (int i = 0; i < 200; i++)
            //{
            //    source2.Rows.Add("asdasd", "dddddd", i.ToString(), "asdsadsad", "asdas", JsonData1);

            //}

            //gridElement.DataSource = source2;

        }
        void button1_Add(object sender, EventArgs e)
        {
            WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
            GridRow r = new GridRow();
            foreach (var col in gridElement.Columns)
            {
                r.Cells.Add(new GridContentCell());
            }
            gridElement.Rows.Add(r);

        }

        void button1_Delete(object sender, EventArgs e)
        {
            WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
            gridElement.Rows.RemoveAt(1);
        }

        void button1_Focus(object sender, EventArgs e)
        {
            WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
            gridElement.SetFocus(gridElement.Rows.Count - 1, "barcode");
        }


        private void button1_Click(object sender, EventArgs e)
        {

            /*  GridElement grd = this.GetVisualElementById<GridElement>("gridgrid");
              WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
              grd.DataSource = null;
              DataTable source = new DataTable();
              source.Columns.Add("value", typeof(string));
              source.Columns.Add("text", typeof(string));

              source.Rows.Add("James", "Dean");
              source.Rows.Add("Bob", "Marley");


              WidgetColumn col = new WidgetColumn() { ID = "wid1" };

              col.Items.Add(new LabelElement() { Top = 0, Left = 295, Width = 105, Height = 22, DataMember = "label" });
              WidgetColumnTextBoxElement txt = new WidgetColumnTextBoxElement() { Top = 0, Left = 187, Width = 105, Height = 22, DataMember = "txt" };
              txt.HasEditChange = true;
              txt.HasItemChanged = true;
              txt.HasFocusEnter = true;
              txt.HasDoubleClick = true;
              txt.ClientKeyPress = "console.log('asdasdsadasdasdasd');";
              col.Items.Add(txt);
              col.Items.Add(new WidgetColumnTextBoxElement() { Top = 25, Left = 34, Width = 105, Height = 22, DataMember = "txt1", HasDoubleClick = true, HasClick = true, HasEditChange = true, HasFocusEnter = true, HasItemChanged = true });
              col.Items.Add(new WidgetColumnMultiComboBoxElement() { Top = 0, Left = 34, Width = 105, Height = 22, DataMember = "combo", DataSource = source, ValueMember = "value", DisplayMember = "text", HasItemChanged = true, HasEditChange = true });

              grd.Columns.Add(col);
              grd.Columns.Add(new GridColumn("label", "label") { Visible = false });
              grd.Columns.Add(new GridColumn("txt", "txt") { Visible = false });
              grd.Columns.Add(new GridColumn("txt1", "txt1") { Visible = false });
              grd.Columns.Add(new GridColumn("combo", "combo") { Visible = false });
              for (int i = 0; i < 100; i++)
              {
                  string JsonData = WidgetColumnMultiComboBoxElement.GetComboBoxData("asdsad", source);
                  grd.Rows.Add("", "3", "Homer", "txt1", JsonData);
                  grd.Rows.Add("", "4", "Lisa", "txt1", JsonData);
                  grd.Rows.Add("", "5", "Marge", "txt1", JsonData);

              }
              grd.ClientCellMouseDown += "var clm = view.ownerGrid.getColumns()[cellIndex];if (clm != null) {if (clm.xtype == 'SPGridColumnwidgetcolumn') {var row = clm.Rows[rowIndex];for (int i = 0; i < row.length; i++) {if (row[i].xtype != 'label' ) {row[i].focus();}}}}";
              */
          //  WidgetGridElement WidgetGrid1 = this.GetVisualElementById<WidgetGridElement>("WidgetGrid1");

            WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
          //  gridElement.WidgetTemplateDataMembers = "materials_material_name,inv_pack_quantity,mesh3aref,expected_material_quantity,invoice_number,invoice_data";
            DataTable cmbsource = new DataTable();
            cmbsource.Columns.Add("value", typeof(string));
            cmbsource.Columns.Add("text", typeof(string));

            cmbsource.Rows.Add("James", "Dean");
            cmbsource.Rows.Add("Bob", "Marley");
            string JsonData1 = WidgetColumnMultiComboBoxElement.GetComboBoxData("asdsad", cmbsource);
           
            DataTable source2 = new DataTable();
            source2.Columns.Add("row_no", typeof(string));
            source2.Columns.Add("barcode", typeof(string));
            source2.Columns.Add("material_name", typeof(string));
            source2.Columns.Add("expected_material_quantity", typeof(string));
            source2.Columns.Add("order_quantity", typeof(string));
            source2.Columns.Add("invoice_pack_quantity", typeof(string));
            source2.Columns.Add("doc_no_en", typeof(string));
            source2.Columns.Add("doc_no", typeof(string));
            source2.Columns.Add("declines", typeof(string));
            source2.Columns.Add("invoice_data", typeof(string));

            for (int i = 0; i < 5; i++)
            {
                source2.Rows.Add(i.ToString(), "1121212121212", "yyy", "asdsadsad", "asdas", "asdasd", "asdasd", "asdasd","11232", JsonData1);
            }

            gridElement.DataSource = source2;

            //DataTable source3 = new DataTable();
            //source3.Columns.Add("row_no", typeof(string));
            //source3.Columns.Add("barcode", typeof(string));
            //source3.Columns.Add("material_name", typeof(string));
            //source3.Columns.Add("material_quantity", typeof(string));
            //source3.Columns.Add("supplier_number", typeof(string));
            //source3.Columns.Add("declines", typeof(string));
            //source3.Columns.Add("invoice_data", typeof(string));

            //for (int i = 0; i < 200; i++)
            //{
            //    source3.Rows.Add(i.ToString(), "1121212121212", "yyy", "asdsadsad", "asdas", "11232", JsonData1);
            //}
          //  WidgetGrid1.DataSource = source3;
        }

        void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            throw new NotImplementedException();
        }

        void button2_Click1(object sender, EventArgs e)
        {
            //DataTable source = new DataTable();
            //source.Columns.Add("value", typeof(string));
            //source.Columns.Add("text", typeof(string));
            //source.Rows.Add("1111", "2222");
            //source.Rows.Add("33333", "4444");

            // change value of datatindex txt line 3
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            //GridMultiComboBoxElement combo = (gridElement.Columns[0] as WidgetColumn).Items[3] as GridMultiComboBoxElement;
            //WidgetColumn clm = gridElement.Columns[0] as WidgetColumn;

            //if (combo != null)
            //{
            //    combo.SelectedText = "sadsadsad";
            //    combo.DataSource = source;
            //    string JsonData = GridMultiComboBoxElement.GetComboBoxData("14545", source);
            //    gridElement.Rows[0].Cells[4].Value = JsonData;
            //    gridElement.Refresh();
            //}

            //  gridElement.WidgetControlFocus(1, "combo");// Rows[0].WidgetControls[1] as WidgetColumnTextBoxElement.Focus();// = new Font("Niagara Engraved", 30, FontStyle.Italic);
            //gridElement.WidgetControlEnabled(0, "txt", false);
            gridElement.WidgetControlFocus(1, "txt1");
            //gridElement.WidgetControlBackColor(2, "txt", Color.Aqua);
            //gridElement.WidgetControlBackColor(2, "combo", Color.Aqua);
            gridElement.SetCellStyle(1, 0, "cellcss1");
            //gridElement.WidgetControlBackColor(0, "txt",Color.Blue);

            //string comboData2 = ComboGridElement.GetComboBoxData("33454", source);
            //gridElement.Rows[0].Cells[4].Value = comboData2;

        }



        void button3_Click3(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            //gridElement.Refresh();
            ApplicationElement.ExecuteOnRespone((object s, EventArgs e1) => { gridElement.WidgetControlFocus(0, "txt1"); }, delay: 5000);

            //gridElement.WidgetControlFocus(1, "txt");
            //gridElement.WidgetControlBackColor(2, "txt", Color.Aqua);
        }

        public void EditChangedAction(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            // txtEventLog.Text += "\r\nLeft Grid: EditChangedAction event is invoked";
            GridCellEventArgs args = e as GridCellEventArgs;
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            // MessageBox.Show("newValue -> " + args.Value + " --- dataIndex --> " + args.DataIndex);
        }

        public void ItemChangedAction(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            //txtEventLog.Text += "\r\nLeft Grid: ItemChangedAction event is invoked";
            GridCellEventArgs args = e as GridCellEventArgs;
            System.Diagnostics.Debug.WriteLine("ItemChangedAction");
            // MessageBox.Show("newValue -> " + args.Value + " --- dataIndex --> " + args.DataIndex);
            // DialogResult ds = await func();

        }

        private async Task<DialogResult> func()
        {
            return await MessageBox.Show("asdasd", "sadsadas");

        }



        public void FocusEnterAction(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            //  txtEventLog.Text += "\r\nLeft Grid: FocusEnterAction event is invoked";
            GridCellEventArgs args = e as GridCellEventArgs;

            // MessageBox.Show("newValue -> " + args.Value + " --- dataIndex --> " + args.DataIndex);
        }
        public void DoubleClickAction(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            // txtEventLog.Text += "\r\nLeft Grid: DoubleClickAction event is invoked";
            GridCellEventArgs args = e as GridCellEventArgs;
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            // MessageBox.Show("newValue -> " + args.Value + " --- dataIndex --> " + args.DataIndex);
        }
        public void selection(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            // MessageBox.Show("newValue -> " + args.Value + " --- dataIndex --> " + args.DataIndex);
            GridRow row = gridElement.CurrentRow;
            System.Diagnostics.Debug.WriteLine("selection");
        }
        public void beforeselection(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            // MessageBox.Show("newValue -> " + args.Value + " --- dataIndex --> " + args.DataIndex);
            System.Diagnostics.Debug.WriteLine("beforeselection");
        }
        public void ClickAction(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            //txtEventLog.Text += "\r\nLeft Grid: ClickAction event is invoked";
            GridCellEventArgs args = e as GridCellEventArgs;
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            // MessageBox.Show("newValue -> " + args.Value + " --- dataIndex --> " + args.DataIndex);
        }


        public void grdkeydown(object sende, EventArgs e)
        {

        }

        public void cellcontentclick(object sende, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
            DataTable cmbsource = new DataTable();
            cmbsource.Columns.Add("value", typeof(string));
            cmbsource.Columns.Add("text", typeof(string));

            cmbsource.Rows.Add("123123123", "33333");
            cmbsource.Rows.Add("21312323", "111111111");
            string JsonData1 = WidgetColumnMultiComboBoxElement.GetComboBoxData("asdsad", cmbsource);
            gridElement.Rows[1].Cells[8].Value = JsonData1;

        }


        private void button3_Click(object sender, EventArgs e)
        {
            WidgetGridElement gridElement = this.GetVisualElementById<WidgetGridElement>("gridElement");
            gridElement.SetEnabled(1, "barcode",false);
        }

        private void afterrenderform(object sender, EventArgs e)
        {
           
        }

        private void afterrendergrid(object sender, EventArgs e)
        {
           
        }

    }
}

