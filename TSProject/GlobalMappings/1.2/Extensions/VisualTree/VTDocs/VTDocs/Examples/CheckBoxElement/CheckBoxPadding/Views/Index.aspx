<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Padding Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent CheckBox Padding is set to Padding-Top = 30" Top="30px" Left="100px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:CheckBox runat="server" Text="TestedCheckBox1" BackColor="LightGray" Padding-Top ="30"  Top="45px" Left="100px" ID="chkPadding" Height="36px" Font-Names=""  TabIndex="1" Width="300px"></vt:CheckBox>

        <vt:Button runat="server" Text="Get CheckBox Padding" Padding-Top ="30"  Top="45px" Left="450px" ID="btnGetPadding" Height="36px" Font-Names=""  TabIndex="1" Width="200px" ClickAction="CheckBoxPadding\btnGetPadding_Click"></vt:Button>           

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="100px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox2" Top="115px" BackColor="LightGray" Left="100px" ID="chkRuntimePadding" Height="36px" TabIndex="1" Width="300px" ></vt:CheckBox>

        <vt:Button runat="server" Text="Change CheckBox Padding" Top="115px" Left="450px" ID="btnChangePadding" Height="36px" TabIndex="1" Width="200px" ClickAction="CheckBoxPadding\btnChangePadding_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="170px" Left="100px" ID="textBox1" Height="36px"  TabIndex="3" Width="350px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

