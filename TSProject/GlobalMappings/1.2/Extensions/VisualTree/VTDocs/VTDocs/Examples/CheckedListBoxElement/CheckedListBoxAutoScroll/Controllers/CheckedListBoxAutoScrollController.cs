using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
using System.ComponentModel;
namespace HelloWorldApp
{
    public class CheckedListBoxAutoScrollController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("dgvCalendario");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));
            source.Columns.Add("phone", typeof(bool));

            source.Rows.Add("�����#", "lisa@simpsons.com", true);
            source.Rows.Add("Bart", "Bart@simpsons.com", true);
            source.Rows.Add("Homer", "Homer@simpsons.com", true);
            source.Rows.Add("Lisa", "Lisa@simpsons.com", false);
            source.Rows.Add("Marge", "Marge@simpsons.com", true);
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", true);
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", false);
            dgvCalendario.DataSource = source;
            (dgvCalendario.Columns[2] as GridCheckBoxColumn).BeforeChecked += frmFlexColumnsController_beforeCheckedChange;
            (dgvCalendario.Columns[2] as GridCheckBoxColumn).CheckedChange += frmFlexColumnsController_CheckedChange;
        }

        public void button3_Click(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("dgvCalendario");
            // dgvCalendario.Columns[0].Visible = false;
            //   dgvCalendario.Refresh();
        }

        public void button2_Click(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("dgvCalendario");

            var parameters = new object[]
                        {
                            "newRow",
                            "newRow",
                             true
                        };
            dgvCalendario.Rows.Add(parameters);
        }


        public void SelectionChanged(object sender, GridElementCellEventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("dgvCalendario");

        }
        private void frmFlexColumnsController_CheckedChange(object sender, EventArgs e)
        {
        }


        public void cell_beginEdit(object sender, EventArgs e)
        {
        }
        public void frmFlexColumnsController_beforeCheckedChange(object sender, CancelEventArgs e)
        {
            //e.Cancel = true;
        }

        public void cell_EndEdit(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("dgvCalendario");
            MessageBox.Show("end edit event =  Row Index  --> " + dgvCalendario.SelectedRowIndex.ToString() + " , Column index -- > " + dgvCalendario.SelectedColumnIndex.ToString() + " , Value --> " + dgvCalendario.Rows[dgvCalendario.SelectedRowIndex].Cells[dgvCalendario.SelectedColumnIndex].Value);
        }

        public void button4_Click(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("dgvCalendario");
            MessageBox.Show("Current indexes =  Row Index  --> " + dgvCalendario.SelectedRowIndex.ToString() + " , Column index -- > " + dgvCalendario.SelectedColumnIndex.ToString() + " , Value --> " + dgvCalendario.Rows[dgvCalendario.SelectedRowIndex].Cells[dgvCalendario.SelectedColumnIndex].Value);
        }
    }



}
