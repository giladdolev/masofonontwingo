<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SetRowVisibility Method" ID="windowView1" LoadAction="GridSetRowVisibility\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="SetRowVisibility" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Determine if the grid row is shown or hidden.

            Syntax: public void SetRowVisibility(int index, bool visibility)
            
            This method action is equivalent to setting the Visible property of the grid row accordingly.
            The index property determine which row will be effected."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can show or hide the second grid row by pressing the buttons.
            " Top="290px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="360px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="490px"></vt:Label>

        <vt:Button runat="server" Text="Show 2nd Row >>" Top="550px" ID="btnShow" ClickAction="GridSetRowVisibility\btnShow_Click"></vt:Button>

        <vt:Button runat="server" Text="Hide 2nd Row >>" Left="300" Top="550px" ID="btnHide" ClickAction="GridSetRowVisibility\btnHide_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
