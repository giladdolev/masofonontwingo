using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedNumericUpDown.ClientSize;

        }
        public void btnChangeNumericUpDownClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            if (TestedNumericUpDown.Width == 200)
            {
                TestedNumericUpDown.ClientSize = new Size(170, 24);
                lblLog.Text = "ClientSize value: " + TestedNumericUpDown.ClientSize;

            }
            else
            {
                TestedNumericUpDown.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedNumericUpDown.ClientSize;
            }

        }

    }
}