using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RichTextBoxAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RichTextBox rtfRuntimeDraggable = this.GetVisualElementById<RichTextBox>("rtfRuntimeDraggable");

            if (rtfRuntimeDraggable.Draggable == false)
            {
                rtfRuntimeDraggable.Draggable = true;
                textBox1.Text = rtfRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                rtfRuntimeDraggable.Draggable = false;
                textBox1.Text = rtfRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}