﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid events order" ID="windowView1" LoadAction="BasicGridEventsOrder\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Grid events order" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="75px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the event log is used to show the events invokation order."
            Top="120px" ID="lblExp2">
        </vt:Label>

         <vt:Grid runat="server" Top="160px" ID="TestedGrid" CellBeginEditAction="BasicGridEventsOrder\TestedGrid_CellBeginEdit" 
              CellEndEditAction="BasicGridEventsOrder\TestedGrid_CellEndEdit" BeforeSelectionChangedAction="BasicGridEventsOrder\TestedGrid_BeforeSelectionChanged" 
             SelectionChangedAction="BasicGridEventsOrder\TestedGrid_SelectionChanged" CellMouseDownAction="BasicGridEventsOrder\TestedGrid_CellMouseDown" 
             CellClickAction="BasicGridEventsOrder\TestedGrid_CellClick" RowClickAction="BasicGridEventsOrder\TestedGrid_RowClick" 
             CurrentCellChangedAction="BasicGridEventsOrder\TestedGrid_CurrentCellChanged" CellValueChangedAction="BasicGridEventsOrder\TestedGrid_CellValueChanged"
             CurrentCellDirtyStateChangedAction="BasicGridEventsOrder\TestedGrid_CurrentCellDirtyStateChanged">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridCheckBoxColumn>
                <vt:GridComboBoxColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridComboBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="295px" Width="450px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="335px" Width="450px" Height="300px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Clear log >>" Top="650px" ID="btnClear" ClickAction="BasicGridEventsOrder\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
