<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer MouseUp event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

         <vt:Label runat="server" Text="MouseUp event method is invoked when the mouse pointer is over the control and a mouse SplitContainer is released." Top="30px" Left="110px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="500px"></vt:Label>

        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="50px" Left="200px" ID="splMouseUp" Height="60px"  Width="200px" MouseUpAction ="SplitContainerMouseUp\splMouseUp_MouseUp"></vt:SplitContainer>          


        <vt:Label runat="server" Text="Event Log" Top="150px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>


        <vt:TextBox runat="server" Text="" Top="170px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
