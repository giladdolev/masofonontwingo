using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonImageWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageWidth_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoRuntimeImageWidth = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeImageWidth");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (rdoRuntimeImageWidth.ImageWidth == 150) //Get
            {
                rdoRuntimeImageWidth.ImageWidth = 300;//Set
                textBox1.Text = "ImageWidth value: " + rdoRuntimeImageWidth.ImageWidth;
            }
            else 
            {
                rdoRuntimeImageWidth.ImageWidth = 150;
                textBox1.Text = "ImageWidth value: " + rdoRuntimeImageWidth.ImageWidth;

            }                    
        }
      
    }
}