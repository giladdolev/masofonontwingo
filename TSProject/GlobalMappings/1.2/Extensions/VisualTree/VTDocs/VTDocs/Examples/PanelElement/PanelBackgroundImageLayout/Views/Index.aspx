<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme ="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
  <vt:WindowView runat="server" Text="Panel BackgroundImageLayout Property" ID="windowView" LoadAction="PanelBackgroundImageLayout\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImageLayout" Left="240px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image position on the control (Center , None, Stretch, Tile or Zoom).
            Tile is the default value." Top="75px"  ID="lblDefinition1"></vt:Label>
      
        <!-- TestedPanel1 -->
        <vt:Panel runat="server" BackgroundImage ="~/Content/Images/icon.jpg" Height="70" Text="Panel" Top="140px" ID="TestedPanel1"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="230px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" Text="BackgroundImageLayout takes effect only if the BackgroundImage property is set." Top="270px"  ID="lblDefinition2"></vt:Label>
      


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the initial BackgroundImageLayout of TestedPanel is set to 'Center'.
            Use the buttons below (None/ Stretch/ Center/ Zoom/ Tile) to change the TestedPanel 
            Background Image Layout." Top="380px"  ID="lblExp1"></vt:Label>     
        
       <!-- TestedPanel2 -->
        <vt:Panel runat="server" Text="TestedPanel" BackgroundImage ="~/Content/Images/icon.jpg" BackgroundImageLayout ="Center" Height="70" Top="460px" ID="TestedPanel2"></vt:Panel>
        <vt:Label runat="server" SkinID="Log" Top="550px" ID="lblLog2"></vt:Label>

         <vt:Button runat="server" Text="Tile"  Top="600px" Left="560px" ID="btnTile" Height="30px" TabIndex="1" Width="60px" ClickAction="PanelBackgroundImageLayout\btnTile_Click"></vt:Button>  
         <vt:Button runat="server" Text="Center"   Top="600px" Left="340px" ID="btnCenter" Height="30px" TabIndex="1" Width="60px" ClickAction="PanelBackgroundImageLayout\btnCenter_Click"></vt:Button>  
         <vt:Button runat="server" Text="Stretch"   Top="600px" Left="230px" ID="btnStretch" Height="30px" TabIndex="1" Width="60px" ClickAction="PanelBackgroundImageLayout\btnStretch_Click"></vt:Button>  
         <vt:Button runat="server" Text="Zoom"  Top="600px" Left="450px" ID="btnZoom" Height="30px" TabIndex="1" Width="60px" ClickAction="PanelBackgroundImageLayout\btnZoom_Click"></vt:Button>  
         <vt:Button runat="server" Text="None" Top="600px" Left="120px" ID="btnNone" Height="30px" TabIndex="1" Width="60px" ClickAction="PanelBackgroundImageLayout\btnNone_Click"></vt:Button> 
 
    </vt:WindowView>
</asp:Content>
        