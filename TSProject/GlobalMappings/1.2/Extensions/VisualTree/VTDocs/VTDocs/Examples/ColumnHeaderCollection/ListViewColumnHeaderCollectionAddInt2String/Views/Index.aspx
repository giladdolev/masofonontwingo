<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView ColumnHeaderCollection Add Method (int, String, String)" ID="windowView1" LoadAction="ListViewColumnHeaderCollectionAddInt2String\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="220px" Text="Add (int, String, String)" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Creates and adds a column to the collection, at index with the specified text and key.
            
            Syntax: public ColumnHeader Add(int index , string key , string text )
            Return Value: The ColumnHeader that was added to the collection.
            If the index value is 0, the item is added to the end of the collection." Top="75px" ID="lblDefinition"></vt:Label>
       
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, pressing the 'Add ColumnHeader' button will invoke 
            the Columns.Add method on this ListView control. Pressing the 'Remove ColumnHeader' button
            will invoke the Columns.RemoveAt method on this ListView control. " Top="260px" ID="lblExp1"></vt:Label>     
        
        <vt:ListView runat="server" Text="TestedListView" Top="340px" width="420px" Height="150px" ID="TestedListView" View="Details">
            <Columns>
                <vt:ColumnHeader runat="server" Text="Column1" width="75"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column2" width="75"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column3" width="75"></vt:ColumnHeader>
            </Columns>
              <Items>
                <vt:ListViewItem runat="server" Text="Item1">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="subitem1"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="subitem11"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Item2">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="subitem2"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="subitem22"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Item3">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="subitem3"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="subitem33"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog1" Width="420px"></vt:Label>

        <vt:Button runat="server" Text="Add ColumnHeader >>" Width="200px" Top="600px" ID="btnAddColumnHeader" ClickAction="ListViewColumnHeaderCollectionAddInt2String\btnAddColumnHeader_Click"></vt:Button>

        <vt:Button runat="server" Text="Remove ColumnHeader >>" Width="200px" Left="300px" Top="600px" ID="btnRemoveColumnHeader" ClickAction="ListViewColumnHeaderCollectionAddInt2String\btnRemoveColumnHeader_Click"></vt:Button>

    </vt:WindowView>
     
</asp:Content>


