using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new GroupBoxEquals());
        }

        private GroupBoxEquals ViewModel
        {
            get { return this.GetRootVisualElement() as GroupBoxEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement GroupBox1 = this.GetVisualElementById<GroupBoxElement>("GroupBox1");
            GroupBoxElement GroupBox2 = this.GetVisualElementById<GroupBoxElement>("GroupBox2");


            if (this.ViewModel.TestedGroupBox.Equals(GroupBox1))
            {
                this.ViewModel.TestedGroupBox = GroupBox2;
                textBox1.Text = "TestedGroupBox Equals GroupBox2";
            }
            else
            {
                this.ViewModel.TestedGroupBox = GroupBox1;
                textBox1.Text = "TestedGroupBox Equals GroupBox1";
            }

            
        }
        

        public void Load(object sender, EventArgs e)
        {
            this.ViewModel.Controls.Add(this.ViewModel.TestedGroupBox);
        }



    }
}