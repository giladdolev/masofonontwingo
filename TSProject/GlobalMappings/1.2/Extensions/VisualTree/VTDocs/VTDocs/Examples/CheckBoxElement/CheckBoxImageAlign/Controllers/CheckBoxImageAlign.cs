using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxImageAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageAlign_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkRuntimeImageAlign = this.GetVisualElementById<CheckBoxElement>("chkRuntimeImageAlign");

            if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.MiddleLeft) //Get
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.MiddleRight;//Set
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.MiddleRight)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.MiddleCenter;
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.MiddleCenter)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.BottomCenter;
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.BottomCenter)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.BottomLeft;
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.BottomLeft)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.BottomRight;
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.BottomRight)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.TopCenter;
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.TopCenter)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.TopLeft;
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.TopLeft)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.TopRight;
            }
            else if (chkRuntimeImageAlign.ImageAlign == ContentAlignment.TopRight)
            {
                chkRuntimeImageAlign.ImageAlign = ContentAlignment.MiddleLeft;
            }
           
        }
      
    }
}