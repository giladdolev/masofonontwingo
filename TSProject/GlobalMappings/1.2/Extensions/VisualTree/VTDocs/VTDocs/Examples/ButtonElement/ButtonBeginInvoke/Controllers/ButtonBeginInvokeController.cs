using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller   
    /// </summary>
    public class ButtonBeginInvokeController : Controller
    {
        /// <summary>
        /// Returns The View For Index   
        /// </summary>
        /// <returns></returns>
        /// 

        public delegate void MyDelegate(LabelElement myControl, string myArg2);

        public delegate void InvokeDelegate();

        public ActionResult Index()
        {
            return View(new ButtonBeginInvoke());
        }

        private ButtonBeginInvoke ViewModel
        {
            get { return this.GetRootVisualElement() as ButtonBeginInvoke; }
        }

        public void btnBeginInvoke_Click(object sender, EventArgs e)
        {
            ButtonElement btnBeginInvoke = this.GetVisualElementById<ButtonElement>("btnBeginInvoke");

            btnBeginInvoke.BeginInvoke(new InvokeDelegate(InvokeMethod));
        }

        public void InvokeMethod()
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            ButtonElement btnBeginInvoke = this.GetVisualElementById<ButtonElement>("btnBeginInvoke");

            btnBeginInvoke.Text = "Executed the given delegate";

            textBox1.Text = "BeginInvoke method invokes 'InvokeMethod' that changes the button text"; 
        }

        public void btnBeginInvokeWithParam_Click(object sender, EventArgs e)
        {
            ButtonElement btnBeginInvokeWithParams = this.GetVisualElementById<ButtonElement>("btnBeginInvokeWithParams");

            Object[] myArray = new object[2];
            myArray[0] = new LabelElement();
            myArray[1] = "Enter a Value";
            

            btnBeginInvokeWithParams.BeginInvoke(new MyDelegate(DelegateMethod), myArray);
        }

        public void DelegateMethod(LabelElement myControl, string myCaption)
        {

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            myControl.Location = new Point(16, 16);
            myControl.Size = new Size(80, 25);
            myControl.Text = myCaption;
            this.ViewModel.Controls.Add(myControl);
            textBox1.Text = "BeginInvoke method invokes 'DelegateMethod' with Label Aans String as parameter The label is added to Window"; 
        }
    }
}

        

