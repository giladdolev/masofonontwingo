<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image BackgroundImageLayout Property" ID="windowView" LoadAction="ImageBackgroundImageLayout\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImageLayout" Left="260px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image position on the control (Center , None, Stretch, Tile or Zoom).
            Tile is the default value.            
            public virtual ImageLayout BackgroundImageLayout { get; set; }" Top="75px"  ID="lblDefinition1"></vt:Label>
      
        
        <vt:Image runat="server" BackgroundImage ="~/Content/Images/icon.jpg" Text="Image" Top="140px" ID="TestedImage1" ImageReference=""></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="245px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" Text="BackgroundImageLayout takes effect only if the BackgroundImage property is set." Top="270px"  ID="lblDefinition2"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, the initial BackgroundImageLayout of TestedImage is set to 'Center'.
            Use the Images below (None/ Stretch/ Center/ Zoom/ Tile) to change the TestedImage 
            Background Image Layout." Top="380px"  ID="lblExp1"></vt:Label>     
        
        <vt:Image runat="server" Text="TestedImage" BackgroundImage ="~/Content/Images/icon.jpg" BackgroundImageLayout ="Center" Top="460px" ID="TestedImage2" ImageReference=""></vt:Image>
        <vt:Label runat="server" SkinID="Log" Top="565px" ID="lblLog2"></vt:Label>

         <vt:Button runat="server" Text="Tile"  Top="600px" Left="560px" ID="btnTile" Height="30px" TabIndex="1" Width="60px" ClickAction="ImageBackgroundImageLayout\btnTile_Click"></vt:Button>  
         <vt:Button runat="server" Text="Center"   Top="600px" Left="340px" ID="btnCenter" Height="30px" TabIndex="1" Width="60px" ClickAction="ImageBackgroundImageLayout\btnCenter_Click"></vt:Button>  
         <vt:Button runat="server" Text="Stretch"   Top="600px" Left="230px" ID="btnStretch" Height="30px" TabIndex="1" Width="60px" ClickAction="ImageBackgroundImageLayout\btnStretch_Click"></vt:Button>  
         <vt:Button runat="server" Text="Zoom"  Top="600px" Left="450px" ID="btnZoom" Height="30px" TabIndex="1" Width="60px" ClickAction="ImageBackgroundImageLayout\btnZoom_Click"></vt:Button>  
         <vt:Button runat="server" Text="None" Top="600px" Left="120px" ID="btnNone" Height="30px" TabIndex="1" Width="60px" ClickAction="ImageBackgroundImageLayout\btnNone_Click"></vt:Button> 
 
    </vt:WindowView>
</asp:Content>


