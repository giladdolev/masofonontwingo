﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Label MaximumSize value: " + TestedLabel.MaximumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;

        }
        public void btnChangeLblMaximumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            if (TestedLabel.MaximumSize == new Size(120, 80))
            {
                TestedLabel.MaximumSize = new Size(140, 40);
                lblLog.Text = "Label MaximumSize value: " + TestedLabel.MaximumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;

            }
            else if (TestedLabel.MaximumSize == new Size(140, 40))
            {
                TestedLabel.MaximumSize = new Size(250, 20);
                lblLog.Text = "Label MaximumSize value: " + TestedLabel.MaximumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;
            }

            else
            {
                TestedLabel.MaximumSize = new Size(120, 80);
                lblLog.Text = "Label MaximumSize value: " + TestedLabel.MaximumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;
            }

        }

        public void btnSetToZeroLblMaximumSize_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedLabel.MaximumSize = new Size(0, 0);
            lblLog.Text = "Label MaximumSize value: " + TestedLabel.MaximumSize + "\\r\\nLabel Size value: " + TestedLabel.Size;

        }

    }
}