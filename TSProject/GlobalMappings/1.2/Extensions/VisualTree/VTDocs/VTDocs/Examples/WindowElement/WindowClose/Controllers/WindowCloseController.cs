using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowCloseController : Controller
    {

        
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowClose());
        }
        private WindowClose ViewModel
        {
            get { return this.GetRootVisualElement() as WindowClose; }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowClose()");

            ViewModel.TestedWin = VisualElementHelper.CreateFromView<CloseTestedWindow>("CloseTestedWindow", "Index2", null, argsDictionary, null);
            this.ViewModel.TestedWin.Show();
            textBox1.Text = "TestedWindow is Open";



        }

        public void btnClose_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (this.ViewModel.TestedWin != null)
            {
                this.ViewModel.TestedWin.Close();
                textBox1.Text = "TestedWindow is Closed";
            }

        }

    }
}