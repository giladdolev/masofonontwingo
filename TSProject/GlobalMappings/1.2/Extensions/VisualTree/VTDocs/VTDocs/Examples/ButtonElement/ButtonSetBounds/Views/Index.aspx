<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button SetBounds() Method" ID="windowView" LoadAction="ButtonSetBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SetBounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the bounds of the control to the specified location and size.
            The SetBounds function has 4 overloads functions:

            Syntax: public void SetBounds(int left)
            Syntax: public void SetBounds(int left, int top)
            Syntax: public void SetBounds(int left, int top, int width)
            Syntax: public void SetBounds(int left, int top, int width, int height)" Top="75px" ID="Label1" ></vt:Label>
         
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="SetBounds Method of this 'TestedButton' is initially set with 
            Left: 80px, Top: 390px, Width: 150px, Height: 36px 
            Use the buttons bellow to invoke SetBounds functions" Top="295px"  ID="lblExp1"></vt:Label>     

         <!--TestedButton-->
        <vt:Button runat="server" SkinID="Wide"  Text="TestedButton" Top="390px" ID="TestedButton"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="460px" Height="60px" Width="370px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="SetBounds(int) >" Left="50px"  Top="580px" Width="155px" ID="btnSetBounds1Param" ClickAction="ButtonSetBounds\btnSetBounds1Param_Click"></vt:Button>
        <vt:Button runat="server" Text="SetBounds(int, int) >" Left="215px" Top="580px" Width="155px" ID="btnSetBounds2Params" ClickAction="ButtonSetBounds\btnSetBounds2Params_Click"></vt:Button>
        <vt:Button runat="server" Text="SetBounds(int, int, int) >" Left="380px" Top="580px" Width="155px" ID="btnSetBounds3Params" ClickAction="ButtonSetBounds\btnSetBounds3Params_Click"></vt:Button>
        <vt:Button runat="server" Text="SetBounds(int, int, int, int) >" Left="545px"  Top="580px" Width="155px" ID="btnSetBounds4Params" ClickAction="ButtonSetBounds\btnSetBounds4Params_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>

