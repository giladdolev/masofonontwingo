using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonImageHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageHeight_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoRuntimeImageHeight = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeImageHeight");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (rdoRuntimeImageHeight.ImageHeight == 100) //Get
            {
                rdoRuntimeImageHeight.ImageHeight = 50;//Set
                textBox1.Text = "ImageHeight value: " + rdoRuntimeImageHeight.ImageHeight;
            }
            else 
            {
                rdoRuntimeImageHeight.ImageHeight = 100;
                textBox1.Text = "ImageHeight value: " + rdoRuntimeImageHeight.ImageHeight;

            }                    
        }
      
    }
}