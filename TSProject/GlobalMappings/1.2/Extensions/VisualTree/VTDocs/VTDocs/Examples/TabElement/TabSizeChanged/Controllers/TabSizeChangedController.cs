using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void btnChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabSize = this.GetVisualElementById<TabElement>("tabSize");

            if (tabSize.Size == new Size(250,200)) //Get
            {
                tabSize.Size = new Size(200, 100); //Set
                textBox1.Text = "The Size property value is: " + tabSize.Size;
            }
            else  
            {
                tabSize.Size = new Size(250, 200); 
                textBox1.Text = "The Size property value is: " + tabSize.Size;
            }
             
        }
        
        public void tabSize_SizeChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "SizeChanged Event is invoked\\r\\n";

        }
    }
}