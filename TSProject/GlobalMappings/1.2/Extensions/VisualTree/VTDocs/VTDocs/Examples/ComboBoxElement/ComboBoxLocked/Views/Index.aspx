<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Locked property" ID="windowView1" LoadAction="ComboBoxLocked/OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="Locked" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Determines whether a control can be edited. 

            Syntax: public bool Locked { get; set; }
            Values: false if the ComboBox can be edited; otherwise, true. The default value is false.
            
            Notice - this property is not intended to be changed at run time."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox"  ID="TestedComboBox1" CssClass="vt-test-cmb-1" Top="200px"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="305px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Locked property of the left ComboBox is initially set to 'true'.
            Therefore, it is not possible to edit the text or select a value from the dropdown list.
            Locked property of the right ComboBox is initially set to 'false'."
            Top="355px" ID="Label1">
        </vt:Label>
                 
        <vt:ComboBox runat="server" Text="TestedComboBox1"  Locked="true" CssClass="vt-test-cmb-2" Top="470px" ID="TestedComboBox2"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog2"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox2"  Locked="false" CssClass="vt-test-cmb-3" Top="470px" Left="400px" ID="TestedComboBox3"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="510px" Left="400px" ID="lblLog3"></vt:Label>

    </vt:WindowView>
</asp:Content>
