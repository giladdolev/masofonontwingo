
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem BackgroundImage Property" ID="windowView1" LoadAction="TabTabItemBackgroundImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImage" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image displayed in the control
            
            Syntax: public virtual Image BackgroundImage { get; set; }" Top="75px" ID="lblDefinition" ></vt:Label>
         
        <vt:Tab runat="server" Text="Tab" Top="160px" ID="TestedTab1">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="Tab1Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="270px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, BackgroundImage property of 'tabPage1' is initially set to 'galilcs.png'." Top="375px"  ID="lblExp1"></vt:Label>     

        
        <vt:Tab runat="server" Text="TestedTab" Top="415px" ID="TestedTab2" TabReference="">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" BackgroundImage="Content/Images/galilcs.png" ID="Tab2Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab2Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px"  ID="Tab2Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="525px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackgroundImage >>"  Top="575px" Width="180px" ID="btnChangeBackgroundImage" ClickAction="TabTabItemBackgroundImage\btnChangeBackgroundImage_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>

