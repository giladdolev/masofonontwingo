using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownIncrementController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnGetIncrementValue_Click(object sender, EventArgs e)
        {
            NumericUpDownElement numericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("numericUpDown1");
            MessageBox.Show("numericUpDown1 Increment value is: " + numericUpDown1.Increment.ToString());

        }

        private void btnChangeIncrement_Click(object sender, EventArgs e)
        {
            NumericUpDownElement numericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("numericUpDown1");
            if (numericUpDown1.Increment == 1)
                numericUpDown1.Increment = 10;
            else
                numericUpDown1.Increment = 1;


        }
    }
}