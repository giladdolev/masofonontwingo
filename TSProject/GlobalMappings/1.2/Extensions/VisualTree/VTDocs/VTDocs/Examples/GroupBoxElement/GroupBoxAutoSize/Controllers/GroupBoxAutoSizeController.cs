using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxAutoSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void Form_Load(object sender, EventArgs e)
        {
            TextBoxElement txtAutoSizeValue = this.GetVisualElementById<TextBoxElement>("txtAutoSizeValue");
            GroupBoxElement grpRuntimeAutoSize = this.GetVisualElementById<GroupBoxElement>("grpRuntimeAutoSize");
            txtAutoSizeValue.Text = "RuntimeGroupBox is set to AutoSize " + grpRuntimeAutoSize.AutoSize.ToString(); 

        }
        public void btnChangeAutoSize_Click(object sender, EventArgs e)
        {
            TextBoxElement txtAutoSizeValue = this.GetVisualElementById<TextBoxElement>("txtAutoSizeValue");
            GroupBoxElement grpRuntimeAutoSize = this.GetVisualElementById<GroupBoxElement>("grpRuntimeAutoSize");

            if (grpRuntimeAutoSize.AutoSize == false)
            {
                grpRuntimeAutoSize.AutoSize = true;
                txtAutoSizeValue.Text = "RuntimeGroupBox is set to AutoSize " + grpRuntimeAutoSize.AutoSize;
            }
            else
            {
                grpRuntimeAutoSize.AutoSize = false;
                txtAutoSizeValue.Text = "RuntimeGroupBox is set to AutoSize " + grpRuntimeAutoSize.AutoSize;
            }
        }
    }
}