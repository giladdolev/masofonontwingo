﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridGridRowCollectionAddObjectArrayController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void Form_load(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("testedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog");
            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            lblLog1.Text = "Click the buttons...";
        }

        
        public void btnRemoveRow_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("testedGrid");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            
            if (testedGrid.Rows.Count > 0)
            {
                testedGrid.Rows.RemoveAt(testedGrid.Rows.Count - 1);
                lblLog.Text = "Row was removed";
            }
           
        }

        private void btnAddItems_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("testedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows.Add(new object[] {"cellVal1", "cellVal2", "cellVal3"});

            lblLog.Text = "TestedGrid.Rows.Add (Object []) was invoked.";

        }

    }
}
