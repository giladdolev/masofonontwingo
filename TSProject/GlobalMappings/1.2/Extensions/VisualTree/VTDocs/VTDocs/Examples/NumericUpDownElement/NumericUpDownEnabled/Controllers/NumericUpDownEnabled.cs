using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedNumericUpDown1.Enabled.ToString();

            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedNumericUpDown2.Enabled.ToString() + ".";

        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedNumericUpDown2.Enabled = !TestedNumericUpDown2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedNumericUpDown2.Enabled.ToString() + ".";
        }
      
    }
}