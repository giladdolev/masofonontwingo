using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowEnabledController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowEnabled());
        }
        private WindowEnabled ViewModel
        {
            get { return this.GetRootVisualElement() as WindowEnabled; }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
        {

            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowEnabled");

            ViewModel.TestedWin = VisualElementHelper.CreateFromView<TestedWindow>("TestedWindow", "Index2", null, argsDictionary, null);
            this.ViewModel.TestedWin.Show();
        }

        public void btnChangeEnabled_Click(object sender, EventArgs e)
        {

            if (this.ViewModel.TestedWin != null)
            {
                TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
                if (this.ViewModel.TestedWin.Enabled)
                {
                    this.ViewModel.TestedWin.Enabled = false;
                    textBox1.Text = "Enbaled = " + this.ViewModel.TestedWin.Enabled.ToString();
                }
                    
                else
                {
                    this.ViewModel.TestedWin.Enabled = true;
                    textBox1.Text = "Enbaled = " + this.ViewModel.TestedWin.Enabled.ToString();

                }
            }

        }

    }
}