using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnDock_Click(object sender, EventArgs e)
        {
            TabElement tabDock = this.GetVisualElementById<TabElement>("tabDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (tabDock.Dock == Dock.Bottom)
            {
                tabDock.Dock = Dock.Fill;
                textBox1.Text = tabDock.Dock.ToString();
            }
            else if (tabDock.Dock == Dock.Fill)
            {
                tabDock.Dock = Dock.Left;
                textBox1.Text = tabDock.Dock.ToString();
            }
            else if (tabDock.Dock == Dock.Left)
            {
                tabDock.Dock = Dock.Right;
                textBox1.Text = tabDock.Dock.ToString();
            }
            else if (tabDock.Dock == Dock.Right)
            {
                tabDock.Dock = Dock.Top;
                textBox1.Text = tabDock.Dock.ToString();
            }
            else if (tabDock.Dock == Dock.Top)
            {
                tabDock.Dock = Dock.None;
                textBox1.Text = tabDock.Dock.ToString();
            }
            else if (tabDock.Dock == Dock.None)
            {
                tabDock.Dock = Dock.Bottom;
                textBox1.Text = tabDock.Dock.ToString();
            }  
        }

    }
}