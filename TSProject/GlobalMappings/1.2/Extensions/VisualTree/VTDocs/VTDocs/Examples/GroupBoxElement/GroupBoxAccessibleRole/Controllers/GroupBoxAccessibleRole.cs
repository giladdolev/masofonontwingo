using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            GroupBoxElement grpAccessibleRole = this.GetVisualElementById<GroupBoxElement>("grpAccessibleRole");

            MessageBox.Show(grpAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            GroupBoxElement grpAccessibleRole = this.GetVisualElementById<GroupBoxElement>("grpAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (grpAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else if (grpAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                grpAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = grpAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}