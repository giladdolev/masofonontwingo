using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "CheckBox MinimumSize value: " + TestedCheckBox.MinimumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
        }

        public void btnChangeChkMinimumSize_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedCheckBox.MinimumSize == new Size(350, 60))
            {
                TestedCheckBox.MinimumSize = new Size(280, 90);
                lblLog1.Text = "CheckBox MinimumSize value: " + TestedCheckBox.MinimumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
            }
            else if (TestedCheckBox.MinimumSize == new Size(280, 90))
            {
                TestedCheckBox.MinimumSize = new Size(200, 50);
                lblLog1.Text = "CheckBox MinimumSize value: " + TestedCheckBox.MinimumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
            }
            else
            {
                TestedCheckBox.MinimumSize = new Size(350, 60);
                lblLog1.Text = "CheckBox MinimumSize value: " + TestedCheckBox.MinimumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;
            }

        }

        public void btnSetToZeroChkMinimumSize_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedCheckBox.MinimumSize = new Size(0, 0);
            lblLog1.Text = "CheckBox MinimumSize value: " + TestedCheckBox.MinimumSize + "\\r\\nCheckBox Size value: " + TestedCheckBox.Size;

        }


    }
}