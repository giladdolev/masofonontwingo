using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowFindFormController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowFindForm());
        }
        private WindowFindForm ViewModel
        {
            get { return this.GetRootVisualElement() as WindowFindForm; }
        }

        public void btnFindForm_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            this.ViewModel.FindForm();
            textBox1.Text = this.ViewModel.FindForm().ToString();
        }
    }
}