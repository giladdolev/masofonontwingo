<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="ListView MinimumSize" ID="windowView1" LoadAction="ListViewMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedListView' is width: 270px and height: 70px. 
            Its MinimumSize is initially set to width: 310px and height: 90px.
            To cancel 'TestedListView' MinimumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>
        

        <vt:ListView runat="server" Top="350px" Left="80px" Height="70px" Width="270px" Text="TestedListView" ID="TestedListView" MinimumSize="310, 90">
            <Columns>
                <vt:ColumnHeader runat="server" Text="Col1" Width="80"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Col2" Width="80"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Col3" width="80"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="Item1">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="SubItem1"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="SubItem2"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Item2">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="SubItem3"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="SubItem4"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="455px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="540px" width="200px" Left="80px" ID="btnChangeLvwMinimumSize" ClickAction="ListViewMinimumSize\btnChangeLvwMinimumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="540px" width="200px" Left="310px" ID="btnSetToZeroLvwMinimumSize" ClickAction="ListViewMinimumSize\btnSetToZeroLvwMinimumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>






