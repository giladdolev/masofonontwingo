﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic SplitContainer" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="SplitContainerEx\load_view">
        
        <vt:Label runat="server" Text="Simple SplitContainer control" Top="30px" Left="128px" ID="label1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Font-Bold="true" Height="13px" TabIndex="4" Width="56px">
		</vt:Label>
        
        <vt:SplitContainer runat="server" SplitterDistance="145" Top="67px" Left="60px" ID="splitContainer1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="120px" TabIndex="5" Width="250px">

			<Panel1 >

			</Panel1>

			<Panel2 >
			</Panel2>
		</vt:SplitContainer>

                <vt:SplitContainer runat="server" SplitterDistance="145" Top="300px" Left="60px" ID="splitContainer2" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="400px" TabIndex="6" Width="400px" Orientation="Vertical">

			<Panel1 >
			</Panel1>

			<Panel2 >
			</Panel2>
		</vt:SplitContainer>
        
    </vt:WindowView>
</asp:Content>
