﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelHideController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Visible value is: " + TestedLabel.Visible;
        }

        public void btnShow_Click(object sender, EventArgs e)
        {

            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedLabel.Show();
            lblLog.Text = "Visible value is: " + TestedLabel.Visible;
        }

        public void btnHide_Click(object sender, EventArgs e)
        {

            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedLabel.Hide();
            lblLog.Text = "Visible value is: " + TestedLabel.Visible;
        }
    }
}