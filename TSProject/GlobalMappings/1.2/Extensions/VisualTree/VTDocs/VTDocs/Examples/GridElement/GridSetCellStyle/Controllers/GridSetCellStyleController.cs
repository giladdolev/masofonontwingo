using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridSetCellStyleController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
           

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Click the buttons below...";
        }

        public void btnInvokeSetCellStyle1_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
          
            TestedGrid.SetCellStyle(0, 0, "vt-grid-set-cell-style1");
            lblLog.Text = "SetCellStyle(0, 0, 'vt-grid-set-cell-style1') was invoked";
        }

        public void btnInvokeSetCellStyle2_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            TestedGrid.SetCellStyle(1, 1, "vt-grid-set-cell-style2");
            lblLog.Text = "SetCellStyle(0, 0, 'vt-grid-set-cell-style2') was invoked";
        }

        public void btnInvokeSetCellStyle3_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            
            TestedGrid.SetCellStyle(2, 2, "vt-grid-set-cell-style3");
            lblLog.Text = "SetCellStyle(0, 0, 'vt-grid-set-cell-style3') was invoked";
        }



    }
}
