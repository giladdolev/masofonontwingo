using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarLayoutStyleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnChangeLayoutStyle_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
			ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            
            
            if (toolStrip1.LayoutStyle == ToolBarLayoutStyle.Flow)
            {
                toolStrip1.LayoutStyle = ToolBarLayoutStyle.HorizontalStackWithOverflow;
                textBox1.Text = toolStrip1.LayoutStyle.ToString();
            }
            else if (toolStrip1.LayoutStyle == ToolBarLayoutStyle.HorizontalStackWithOverflow)
            {
                toolStrip1.LayoutStyle = ToolBarLayoutStyle.StackWithOverflow;
                textBox1.Text = toolStrip1.LayoutStyle.ToString();
            }
            else if (toolStrip1.LayoutStyle == ToolBarLayoutStyle.StackWithOverflow)
            {
                toolStrip1.LayoutStyle = ToolBarLayoutStyle.Table;
                textBox1.Text = toolStrip1.LayoutStyle.ToString();
            }
            else if (toolStrip1.LayoutStyle == ToolBarLayoutStyle.Table)
            {
                toolStrip1.LayoutStyle = ToolBarLayoutStyle.VerticalStackWithOverflow;
                textBox1.Text = toolStrip1.LayoutStyle.ToString();
            }
            else 
            {
                toolStrip1.LayoutStyle = ToolBarLayoutStyle.Flow;
                textBox1.Text = toolStrip1.LayoutStyle.ToString();
            }
                
		}

    }
}