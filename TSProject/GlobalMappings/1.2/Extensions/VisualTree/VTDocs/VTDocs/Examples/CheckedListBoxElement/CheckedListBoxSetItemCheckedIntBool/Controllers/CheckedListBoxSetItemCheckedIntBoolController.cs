using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckedListBoxSetItemCheckedIntBoolController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            CheckedListBoxElement testedCheckedListBox = this.GetVisualElementById<CheckedListBoxElement>("testedCheckedListBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            for (int i = 0; i < testedCheckedListBox.Items.Count; i++)
            {
                lblLog.Text +=  testedCheckedListBox.Items[i].ToString()
                    + " CheckState: " + testedCheckedListBox.GetItemChecked(i) + "\\r\\n";
            }
        }


        private void btnSetItemChecked_Click(object sender, EventArgs e)
        {
            CheckedListBoxElement testedCheckedListBox = this.GetVisualElementById<CheckedListBoxElement>("testedCheckedListBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbSelectIndex = this.GetVisualElementById<ComboBoxElement>("cmbSelectIndex");
            CheckBoxElement chkCheckState = this.GetVisualElementById<CheckBoxElement>("chkCheckState");

            testedCheckedListBox.SetItemChecked(cmbSelectIndex.SelectedIndex, chkCheckState.IsChecked);
            lblLog.Text = "SetItemChecked(" + int.Parse(cmbSelectIndex.Text)+", "+chkCheckState.IsChecked+ ") was invoked\\r\\n";

            for (int i = 0; i < testedCheckedListBox.Items.Count; i++)
            {
                lblLog.Text += testedCheckedListBox.Items[i].ToString()
                    + " CheckState: " + testedCheckedListBox.GetItemChecked(i) + "\\r\\n";
            }
           
        }


    }
}