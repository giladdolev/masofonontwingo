using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton1 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton1");
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedRadioButton1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRadioButton1);
            lblLog2.Text = "Font value: " + TestedRadioButton2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRadioButton2);
        }

        private void btnChangeRadioButtonFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            if (TestedRadioButton2.Font.Underline == true)
            {
                TestedRadioButton2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedRadioButton2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRadioButton2);
            }
            else if (TestedRadioButton2.Font.Bold == true)
            {
                TestedRadioButton2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedRadioButton2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRadioButton2);
            }
            else if (TestedRadioButton2.Font.Italic == true)
            {
                TestedRadioButton2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedRadioButton2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRadioButton2);
            }
            else
            {
                TestedRadioButton2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedRadioButton2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedRadioButton2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}