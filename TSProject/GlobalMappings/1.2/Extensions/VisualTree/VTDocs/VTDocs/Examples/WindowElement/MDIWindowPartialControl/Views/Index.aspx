<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" MaximizeBox="True" MinimizeBox="True" Text="MainPage - Index" ID="MainPage" Height="600px" Width="777px">

        <vt:Button runat="server" Text="add composite " Top="300" Left="32px" ID="Button1" Height="57px" Width="193px" ClickAction="MainPag\btn1_click"></vt:Button>

        <vt:Button runat="server"  Text="close" Top="400" Left="32px" ID="Button2"  Height="57px" Width="193px" ClickAction="MainPag\btn2_click"></vt:Button>

        <vt:Panel runat="server" ID="Panel1" Width="300" Height="300" >

        </vt:Panel>

    </vt:WindowView>
</asp:Content>
