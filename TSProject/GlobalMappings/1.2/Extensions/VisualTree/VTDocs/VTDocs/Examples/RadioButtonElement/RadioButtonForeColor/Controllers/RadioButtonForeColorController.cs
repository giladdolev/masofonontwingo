using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton1 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedRadioButton1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedRadioButton2.ForeColor + '.';
        }

        public void btnChangeForeColor_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedRadioButton2.ForeColor == Color.Green)
            {
                TestedRadioButton2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedRadioButton2.ForeColor + '.';
            }
            else
            {
                TestedRadioButton2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedRadioButton2.ForeColor + '.';

            }
        }

    }
}