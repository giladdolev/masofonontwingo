using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class DateTimePickerShowUpDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker1 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "ShowUpDown value: " + TestedDateTimePicker1.ShowUpDown.ToString();

            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ShowUpDown value: " + TestedDateTimePicker2.ShowUpDown.ToString();

        }


        public void btnChangeShowUpDown_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedDateTimePicker2.ShowUpDown = !TestedDateTimePicker2.ShowUpDown;
            lblLog2.Text = "ShowUpDown value: " + TestedDateTimePicker2.ShowUpDown + "\\r\\nFormat value: " + TestedDateTimePicker2.Format;

        }

        public void btnChangeFormat_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedDateTimePicker2.Format == DateTimePickerFormat.Default)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Custom;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Custom)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Long;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Long)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Short;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Short)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Time;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Time)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Line;
            else
                TestedDateTimePicker2.Format = DateTimePickerFormat.Default;


            lblLog2.Text = "ShowUpDown value: " + TestedDateTimePicker2.ShowUpDown + "\\r\\nFormat value: " + TestedDateTimePicker2.Format;

        }

    }
}