using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewCheckedIndicesController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PrintToLog();
        }
        public void btnChangeCheckStateOfFirstCheckBox_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");

            TestedListView.Items[0].Checked = !TestedListView.Items[0].Checked;

            PrintToLog();
        }

        public void btnChangeCheckStateOfSecondCheckBox_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");

            TestedListView.Items[1].Checked = !TestedListView.Items[1].Checked;

            PrintToLog();
        }

        public void btnChangeCheckStateOfThirdCheckBox_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");

            TestedListView.Items[2].Checked = !TestedListView.Items[2].Checked;

            PrintToLog();
        }

        public void TestedListView_ItemChecked(object sender, EventArgs e)
        {
            PrintToLog();
        }

        public void TestedListView_ItemUnChecked(object sender, EventArgs e)
        {
            PrintToLog();
        }

        private void PrintToLog()
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");

            if (TestedListView.CheckedIndices != null)
            {
                lblLog.Text = "CheckedIndices list values: ";

                for (int i = 0; i < TestedListView.CheckedIndices.Count; i++)
                {
                    lblLog.Text += "\\r\\n" + TestedListView.CheckedIndices[i];
                }
            }
            else
                lblLog.Text = "CheckedIndices list is empty";
            
        }


    }
}