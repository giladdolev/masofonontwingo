<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown GotFocus Event" ID="windowView1" LoadAction="NumericUpDownGotFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="GotFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control receives focus. 

            Syntax: public event EventHandler GotFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse. " Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="In the following example, each time the focus is shifted into the control,
             a 'GotFocus' event will be invoked. Each invocation of the 'GotFocus' event should add 
             a line in the 'Event Log'." Top="250px" ID="lblExp1"></vt:Label>

        <vt:GroupBox runat="server" Top="335px" Height="55px" Width="580px">
            <vt:NumericUpDown runat="server" Text="TestedNumericUpDown1" Top="15px" TabIndex="1" Left="15px" CssClass="vt-NumericUpDown1" ID="TestedNumericUpDown1" 
              GotFocusAction="NumericUpDownGotFocus\TestedNumericUpDown1_GotFocus" ></vt:NumericUpDown>
            <vt:NumericUpDown runat="server" Text="TestedNumericUpDown2" Top="15px" TabIndex="2" Left="195px" CssClass="vt-NumericUpDown2"  ID="TestedNumericUpDown2" GotFocusAction="NumericUpDownGotFocus\TestedNumericUpDown2_GotFocus"></vt:NumericUpDown>
            <vt:NumericUpDown runat="server" Text="TestedNumericUpDown3" Top="15px" TabIndex="3" Left="375px" CssClass="vt-NumericUpDown3"  ID="TestedNumericUpDown3" GotFocusAction="NumericUpDownGotFocus\TestedNumericUpDown3_GotFocus"></vt:NumericUpDown>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="440px" Width="400px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Focus NumericUpDown2 >>" Top="570px" ID="btnGotFocus" Width="180px" ClickAction="NumericUpDownGotFocus\btnGotFocus_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
