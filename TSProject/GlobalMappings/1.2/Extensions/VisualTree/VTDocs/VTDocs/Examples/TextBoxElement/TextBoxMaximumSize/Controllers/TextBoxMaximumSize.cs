using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "TextBox MaximumSize value: " + TestedTextBox.MaximumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
        }

        public void btnChangeTxtMaximumSize_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedTextBox.MaximumSize == new Size(140, 70))
            {
                TestedTextBox.MaximumSize = new Size(160, 20);
                lblLog1.Text = "TextBox MaximumSize value: " + TestedTextBox.MaximumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
            }
            else if (TestedTextBox.MaximumSize == new Size(160, 20))
            {
                TestedTextBox.MaximumSize = new Size(170, 50);
                lblLog1.Text = "TextBox MaximumSize value: " + TestedTextBox.MaximumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
            }
            else
            {
                TestedTextBox.MaximumSize = new Size(140, 70);
                lblLog1.Text = "TextBox MaximumSize value: " + TestedTextBox.MaximumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;
            }

        }

        public void btnSetToZeroTxtMaximumSize_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedTextBox.MaximumSize = new Size(0, 0);
            lblLog1.Text = "TextBox MaximumSize value: " + TestedTextBox.MaximumSize + "\\r\\nTextBox Size value: " + TestedTextBox.Size;

        }


    }
}