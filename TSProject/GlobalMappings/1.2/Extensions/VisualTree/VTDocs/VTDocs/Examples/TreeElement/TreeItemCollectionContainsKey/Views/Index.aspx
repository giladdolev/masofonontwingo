<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree TreeItemCollection ContainsKey Method" LoadAction="TreeItemCollectionContainsKey/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ContainsKey" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Determines whether the collection contains a tree item with the specified key.

            Syntax: public bool ContainsKey(string key)
            
            The key comparison is not case-sensitive. If the key parameter is null or an empty string, 
            the Item property returns false."
            Top="75px" ID="Label1">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can choose a key value using the combobox,
            and then press the button to perform the ContainsKey on that key value."
            Top="280px" ID="lblExp2">
        </vt:Label>

        <vt:Tree runat="server" ID="TestedTree" Width="180px" Height="210px" Top="350px" >
            <Items>
                <vt:TreeItem runat="server" ID="TreeItem0" Text="TreeItem0" IsExpanded="true">
                    <Items>
                        <vt:TreeItem runat="server" ID="TreeItem0_0" Text="TreeItem0_0"></vt:TreeItem>
                        <vt:TreeItem runat="server" ID="TreeItem0_1" Text="TreeItem0_1"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" ID="TreeItem1" Text="TreeItem1" IsExpanded="true">
                    <Items>
                        <vt:TreeItem runat="server" ID="TreeItem1_0" Text="TreeItem1_0" IsExpanded="true">
                            <Items>
                                <vt:TreeItem runat="server" ID="TreeItem1_0_0" Text="TreeItem1_0_0"></vt:TreeItem>
                            </Items>
                        </vt:TreeItem>
                        <vt:TreeItem runat="server" ID="TreeItem1_1" Text="TreeItem1_1"></vt:TreeItem>                        
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" ID="TreeItem2" Text="TreeItem2"></vt:TreeItem>
            </Items>
        </vt:Tree>
        
        <vt:Label runat="server" SkinID="Log" Top="575px" ID="lblLog1"></vt:Label>

        <vt:ComboBox runat="server" ID="cmbTreeItems" Text="Select key..." CssClass="vt-cmb-tree-items" Top="350px" Left="280px" Width="150px"></vt:ComboBox>        
        <vt:Button runat="server" Text="Perform ContainsKey >>" ID="btnPerformContainsKey" ClickAction="TreeItemCollectionContainsKey/btnPerformContainsKey_Click" Left="445px" Top="350px"></vt:Button>
    </vt:WindowView>

</asp:Content>

