<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer ApplicationId Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

             


        <vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Start" Top="65px" Left="68px" ClickAction="TimerApplicationId\btnStart_Click" ID="btnStartInitialize" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="30px" Width="102px">
		</vt:Button>
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Stop" Top="105px" Left="68px" ClickAction="TimerApplicationId\btnStop_Click" ID="btnStopInitialize" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="30px" TabIndex="1" Width="102px">
		</vt:Button>
		<vt:TextBox runat="server" PasswordChar="" Text="0" Top="87px" Left="268px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="20px" TabIndex="2" Width="42px">
		</vt:TextBox>
		<vt:Label runat="server" Text="Timer:" Top="90px" Left="226px" ID="lblTimer" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="3" Width="36px">
		</vt:Label>
		<vt:Label runat="server" Text="seconds" Top="90px" Left="316px" ID="lblseconds" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="4" Width="47px">
		</vt:Label>

        <vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Get Timer 'ApplicationId'" Top="90px" Left="400px" ClickAction="TimerApplicationId\btnApplicationId_Click" ID="btnApplicationId" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="30px" Width="150px">
		</vt:Button>

        <vt:TextBox runat="server" Text="" Top="150px" Left="220px" ID="textBox2" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>
    </vt:WindowView>

    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="timer1" Enabled="false" Interval="1000" TickAction="TimerApplicationId\timer1_Tick">
		</vt:Timer>
	</vt:ComponentManager>

</asp:Content>
        