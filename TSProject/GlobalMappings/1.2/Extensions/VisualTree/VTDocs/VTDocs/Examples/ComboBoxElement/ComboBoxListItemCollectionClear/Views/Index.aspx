<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox ListItemCollection Clear Method" ID="windowView2" LoadAction="ComboBoxListItemCollectionClear\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Clear"  ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Removes all items from the ComboBox.

            Syntax: public void Clear()" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can add Items to the ComboBox using the 'Add Item' button, 
            and than you can use the 'Clear' button to Removes all items from the ComboBox." Top="245px" ID="lblExp2"></vt:Label>

        <vt:ComboBox runat="server" CssClass="vt-test-cmb" Top="320px" ID="TestedComboBox"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Width="450px" ID="lblLog" Top="360"></vt:Label>

        <vt:Button runat="server" Text="Clear >>" Top="450px" Left="300px" ID="btnClear" ClickAction="ComboBoxListItemCollectionClear\btnClear_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Item >>" Top="450px" ID="btnAddItem" ClickAction="ComboBoxListItemCollectionClear\AddItems_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

