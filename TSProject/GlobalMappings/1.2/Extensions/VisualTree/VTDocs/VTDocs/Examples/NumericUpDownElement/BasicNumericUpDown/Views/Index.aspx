<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="Basic NumericUpDown" ID="windowView1" LoadAction="BasicNumericUpDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="NumericUpDown" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Represents a Windows spin box (also known as an up-down control) that displays numeric values. 
            
            A NumericUpDown control contains a single numeric value that can be incremented or decremented by 
            clicking the up or down buttons of the control. The user can also enter in a value, unless the ReadOnly 
            property is set to true." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the NumericUpDown control has minimum value:1, maximum value:10.
            You can enter a numeric value using the keyboard or using the up and down buttons or using the 
            'Set Value to 2' button." Top="250px" ID="Label1"></vt:Label>
        
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown1" Top="350px" ID="TestedNumericUpDown1" Height="20px" TabIndex="1" Width="200px" Minimum="1" Maximum="10" CssClass="vt-TestedNumericUpDown1" ValueChangedAction="BasicNumericUpDown\NumericUpDown1_ValueChanged"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="380px" ID="lblLog"></vt:Label>

       
        <vt:Button runat="server" Text="Set value to 2 >>" Top="450px" ID="btnSetValue" ClickAction="BasicNumericUpDown\btnSetValue_Click"></vt:Button>
    
    </vt:WindowView>
</asp:Content>
