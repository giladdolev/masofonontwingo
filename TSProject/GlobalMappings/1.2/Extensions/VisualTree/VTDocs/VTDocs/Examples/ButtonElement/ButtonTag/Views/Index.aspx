<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Tag Property" ID="windowView1" LoadAction="ButtonTag\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Tag" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the object that contains data about the control.
            
            Syntax: public object Tag { get; set; }
            Value: An Object that contains data about the control. The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="175px" ID="TestedButton1"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="225px" Width="400" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Tag property of this 'TestedButton' is initially set to the string 'New Tag'." Top="330px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Top="385px" Tag="New Tag." ID="TestedButton2"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="435px" Width="400" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Tag value >>" Top="490px" ID="btnChangeBackColor" ClickAction="ButtonTag\btnChangeTag_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

