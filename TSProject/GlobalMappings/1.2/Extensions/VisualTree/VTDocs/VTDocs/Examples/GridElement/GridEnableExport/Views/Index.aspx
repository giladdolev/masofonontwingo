﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid EnableExport Property - Only available to Sencha Premium Users" Height="800px" Width="1200px" LoadAction="GridEnableExport\OnLoad">

         <vt:Label runat="server" SkinID="Title" Text="EnableExport - Only available to Sencha Premium Users" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Grid initialized with EnableExport = true" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Grid runat="server"  Top="80px" Left="50px" ID="gridElement" Height="299px" Width="782px" LostFocusAction="True" RowCount="3" EnableExport="true">
            <Columns>
                <vt:GridTextBoxColumn ID="txtclm" runat="server" Height="70px" HeaderText="TextBox Column" DataMember="TextColumn"></vt:GridTextBoxColumn>

                <vt:GridTextButtonColumn ID="txtbtnclm" runat="server" HeaderText="TextButton Column" DataMember="TextButtonColumn"></vt:GridTextButtonColumn>

                <vt:GridCheckBoxColumn ID="cehckclm" runat="server" HeaderText="CheckBox Column" DataMember="CheckBoxColumn"></vt:GridCheckBoxColumn>

                <vt:GridDateTimePickerColumn ID="dataclm" runat="server" HeaderText="Date Column" DataMember="DateTimeColumn" Format="Short"></vt:GridDateTimePickerColumn>

                <vt:GridComboBoxColumn ID="ComboBoxColumn" runat="server" HeaderText="ComboBoxColumn" DataMember="ComboBoxColumn" Width="90" Format="Short"></vt:GridComboBoxColumn>
                 <vt:GridTextBoxColumn ID="GridTextBoxColumn1" runat="server" HeaderText="Currency" DataMember="Currency" Format="currency"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>     

    </vt:WindowView>

</asp:Content>
