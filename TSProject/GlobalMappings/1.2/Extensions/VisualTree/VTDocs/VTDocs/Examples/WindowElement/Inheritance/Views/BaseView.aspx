<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" MaximizeBox="True" MinimizeBox="True" Text="Base View" ID="BasicWindow" BackColor="Green" AcceptButtonID="buttonOK" CancelButtonID="buttonCancel" Top="20px" Left="40px" Height="800px" Width="700px">
		<vt:Panel runat="server" Dock="Fill" ID="content" BackColor="Blue">
		</vt:Panel>
		<vt:Panel runat="server" Dock="Bottom" Height="50" BackColor="Yellow">
            <vt:Button runat="server" ID="buttonOK" Text="OK" ClickAction="BaseView\buttonOK_Click"></vt:Button>
            <vt:Button runat="server" ID="buttonCancel" Text="Cancel" Left="100"></vt:Button>
		</vt:Panel>
	</vt:WindowView>
</asp:Content>
