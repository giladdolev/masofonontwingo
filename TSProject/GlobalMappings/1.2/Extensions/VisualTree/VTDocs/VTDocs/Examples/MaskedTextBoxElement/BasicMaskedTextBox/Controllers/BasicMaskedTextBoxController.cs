﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;

namespace MvcApplication9.Controllers
{
    public class BasicMaskedTextBoxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            maskedTextBox1_TextChanged(sender, e);
            maskedTextBox2_TextChanged(sender, e);
            maskedTextBox3_TextChanged(sender, e);
            maskedTextBox4_TextChanged(sender, e);
        }


        public void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox1");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            textBox1.Text = maskedTextBox1.Text;
        }

        public void maskedTextBox2_TextChanged(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox2 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox2");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");

            textBox2.Text = maskedTextBox2.Text;
        }

        public void maskedTextBox3_TextChanged(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox3 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox3");
            TextBoxElement textBox3 = this.GetVisualElementById<TextBoxElement>("textBox3");

            textBox3.Text = maskedTextBox3.Text;
        }

        public void maskedTextBox4_TextChanged(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox4 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox4");
            TextBoxElement textBox4 = this.GetVisualElementById<TextBoxElement>("textBox4");

            textBox4.Text = maskedTextBox4.Text;
        }



    }
}