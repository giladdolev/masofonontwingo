﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Grid GridCheckBoxColumn" ID="windowView1" LoadAction="BasicGridGridCheckBoxColumn\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="230px" Text="GridCheckBoxColumn" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Hosts a collection of check box user interface (UI)" 

            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can check the CheckBoexs in the Grid cells manually or by 
            clicking the buttonS."
            Top="200px" ID="lblExp2">
        </vt:Label>

         <vt:Grid runat="server" Text="TestedGrid" Top="270px" Height="230px" Width="240px" ID="TestedGrid" ReadOnly="true" RowHeaderVisible="">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="Col1" HeaderText="ID" Height="20px" Width="40px"> </vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Col2" HeaderText="Name" Height="20px" Width="70px"> </vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="Col3" HeaderText="Attendance" Height="20px" Width="90px" CheckedChangeAction="BasicGridGridCheckBoxColumn\coulmns_CheckedChange"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="515px" Width="420px" Height="50px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Check All >>"  Top="615px" Width="185px" ID="btnSetCheckBoxesValueTrue" ClickAction="BasicGridGridCheckBoxColumn\btnSetCheckBoxesValueTrue_Click"></vt:Button>

         <vt:Button runat="server" Text="UnCheck All >>"  Top="615px" Left="315px" Width="185px" ID="btnSetCheckBoxesValueFalse" ClickAction="BasicGridGridCheckBoxColumn\btnSetCheckBoxesValueFalse_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
