using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridGridComboBoxColumnSelectionChangedController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void Page_load(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedGrid.AddToComboBoxList("GridCol1", "male,female");
            TestedGrid.AddToComboBoxList("GridCol2", "male,female");
            TestedGrid.AddToComboBoxList("GridCol3", "male,female");

            TestedGrid.Rows.Add("male", "female", "male");
            TestedGrid.Rows.Add("female", "male", "male");
            TestedGrid.Rows.Add("male", "male", "female");

            lblLog1.Text = "Please Select a cell from the GridComboBoxColumns";
        }


        private void TestedGrid_SelectionChanged()
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nGrid nSelectionChanged event is invoked";

            //lblLog1.Text+=  "\\r\\nSelectionChanged Event raised" ;
        }

    }
}
