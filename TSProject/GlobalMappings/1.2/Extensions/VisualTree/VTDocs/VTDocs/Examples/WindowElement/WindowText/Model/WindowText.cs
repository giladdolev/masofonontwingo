﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowText : WindowElement
    {
        Window2TextChanged _TestedWin2;

        public WindowText()
        {

        }
       
          public Window2TextChanged TestedWin2
        {
            get { return this._TestedWin2; }
            set { this._TestedWin2 = value; }
        }
    }
}