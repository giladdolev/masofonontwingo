<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Form2 - Fill Dock style" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
       
        <vt:Label runat="server" Text="Initialized component : The Panel is initialliy docked to Fill" Top="30px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:Label runat="server" Text="Rezise Form2 and click the button to change Dock style" Top="50px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 

        <vt:Label runat="server" Text="Run Time : Click 'Change Dock style' button" Top="170px" Left="140px" ID="Label3" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:Panel runat="server" Text="Panel" Top="210px" Left="200px" ID="pnlDock" Dock="Fill" Height="150px" TabIndex="1" Width="200px" BackColor="Lime"></vt:Panel>

        <vt:Button runat="server" Text="Change panel Dock of Form2" Top="210px" Left="200px" ID="btnDockFillForm2" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="Form2DockFillAndResizing\btnDockFillForm2_Click"></vt:Button>
        
       <vt:TextBox runat="server" Text="" Top="325px" Left="200px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"></vt:TextBox>


    </vt:WindowView>
</asp:Content>
        