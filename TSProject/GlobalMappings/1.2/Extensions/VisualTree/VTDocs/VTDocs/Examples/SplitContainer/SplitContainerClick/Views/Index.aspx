<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Click event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="50px" Left="140px" ID="TestedSplitContainer" Height="60px"  Width="200px"  ClickAction="SplitContainerClick\TestedSplitContainer_Click" ></vt:SplitContainer>          

        <vt:Label runat="server" Text="Event Log" Top="50px" Left="400px" Font-Bold="true" Multiline="true" ID="Label1" Height="20px" Width="250px"></vt:Label>

        <vt:TextBox runat="server" Text="" Top="70px" Left="400px" Multiline="true" ID="textBox1" Height="100px" Width="250px"></vt:TextBox>
       
    </vt:WindowView>
</asp:Content>
