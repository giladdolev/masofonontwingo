using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarGripStyleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnChangeGripStyle_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");

            if (toolStrip1.GripStyle == ToolBarGripStyle.Visible)
            {
                toolStrip1.GripStyle = ToolBarGripStyle.Hidden;
                textBox1.Text = toolStrip1.GripStyle.ToString();
            }
            else
            {
                toolStrip1.GripStyle = ToolBarGripStyle.Visible;
                textBox1.Text = toolStrip1.GripStyle.ToString();
            }
            
                
		}

    }
}