<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton FlatStyle Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="150px"></vt:Label>     

        <vt:RadioButton runat="server" Text="FlatStyle is set to 'Flat'" FlatStyle="Flat" Top="55px"   Left="140px" ID="rdoFlatStyle" Height="36px" Width="150px"></vt:RadioButton>           

        <vt:Label runat="server" Text="RunTime" Top="210px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:RadioButton runat="server" Text="RadioButton FlatStyle Change" Top="230px" Left="140px" ID="rdoCheckFlatStyle" Height="36px" Width="150px"></vt:RadioButton>

        <vt:Button runat="server" Text="RunTime FlatStyle" Top="240px" Left="420px" ID="btnChangeFlatStyle" Height="36px" Width="200px" ClickAction="RadioButtonFlatStyle\btnChangeFlatStyle_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="300px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
