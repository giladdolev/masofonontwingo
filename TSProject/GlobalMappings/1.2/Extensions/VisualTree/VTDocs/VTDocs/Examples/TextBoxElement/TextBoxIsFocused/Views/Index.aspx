<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox IsFocused Property" ID="windowView2"  LoadAction="TextBoxIsFocused\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="IsFocused" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets a value indicating whether the control has input focus.

            public bool IsFocused { get; }
            'true' if the control has focus; otherwise, 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Focus The following TextBoxes and watch the IsFocus value of each of the textBoxes
              in label below " Top="260px" ID="lblExp1"></vt:Label>

         <vt:TextBox runat="server" Text="TextBox1" GotFocusAction="TextBoxIsFocused\textBoxes_GotFocus" LostFocusAction="TextBoxLostFocus\textBoxes_LostFocus" Top="325px" ID="TestedTextBox1"></vt:TextBox>

         <vt:TextBox runat="server" Text="TextBox2" GotFocusAction="TextBoxIsFocused\textBoxes_GotFocus" LostFocusAction="TextBoxLostFocus\textBoxes_LostFocus" Top="325px" Left="250px" ID="TestedTextBox2"></vt:TextBox>

        <vt:TextBox runat="server" Text="TextBox3" GotFocusAction="TextBoxIsFocused\textBoxes_GotFocus" LostFocusAction="TextBoxLostFocus\textBoxes_LostFocus" Top="325px" Left="420px" ID="TestedTextBox3"></vt:TextBox>
        
        <vt:Label runat="server" SkinID="Log" Height="60px" Top="380px" ID="lblLog"></vt:Label>
                   
        

    </vt:WindowView>
</asp:Content>
        