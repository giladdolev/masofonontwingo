<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Bounds Property" ID="windowView" LoadAction="SplitContainerBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Bounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size and location of the control including its nonclient elements, 
            in pixels, relative to the parent control.

            Syntax: public Rectangle Bounds { get; set; }
            
            The bounds of the control include the nonclient elements such as scroll bars, 
            borders, title bars, and menus." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The bounds of this 'TestedSplitContainer' is initially set with 
            Left: 80px, Top: 355px, Width: 200px, Height: 80px" Top="290px"  ID="lblExp1"></vt:Label>     

        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="355px" ID="TestedSplitContainer"></vt:SplitContainer>
        <vt:Label runat="server" SkinID="Log" Top="450px" Height="40" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Bounds value >>" Top="555px" Width="180px" ID="btnChangeSplitContainerBounds" ClickAction="SplitContainerBounds\btnChangeSplitContainerBounds_Click"></vt:Button>
              
    </vt:WindowView>
</asp:Content>
