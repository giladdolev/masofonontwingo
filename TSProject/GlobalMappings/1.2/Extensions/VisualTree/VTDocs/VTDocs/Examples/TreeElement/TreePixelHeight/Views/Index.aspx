<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree PixelHeight Property" ID="windowView2" LoadAction="TreePixelHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelHeight" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int PixelHeight { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this 'TestedTree' is initially set to 110px" Top="250px"  ID="lblExp1"></vt:Label>     

        <!-- TestedTree -->
        <vt:Tree runat="server" Text="TestedTree" Top="300px" ID="TestedTree">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="425px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelHeight value >>" Top="490px" ID="btnChangePixelHeight" Width="180px" ClickAction="TreePixelHeight\btnChangePixelHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
