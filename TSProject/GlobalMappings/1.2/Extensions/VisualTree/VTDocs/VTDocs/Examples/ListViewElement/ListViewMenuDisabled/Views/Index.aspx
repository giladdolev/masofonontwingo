
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="ListView MenuDisabled Property" ID="windowView1" LoadAction="ListViewMenuDisabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MenuDisabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether a menu button appears next to each header.

            Syntax: public bool MenuDisabled { get; set; }
            Values: 'false' if a menu button appears next to each header of the ListView control; otherwise, 'true'.
            The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested ListView1 -->
        <vt:ListView runat="server" Text="ListView" Top="190px" ID="TestedListView1" CssClass="vt-listview-1" View="Details">
            <Columns>
                <vt:ColumnHeader runat="server" Text="Column1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="345px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="MenuDisabled property of the left ListView is initially set to 'false'.
             A menu button should appear next to each header of the ListView.
             MenuDisabled property of the right ListView is initially set to 'false'.

             Notice - this property is not intended to be changed at run time." Top="395px" ID="lblExp1"></vt:Label>

        <!-- Tested ListView2 -->
        <vt:ListView runat="server" Text="TestedListView1" MenuDisabled="false" Top="515px" ID="TestedListView2" CssClass="vt-listview-2" View="Details">
            <Columns>
                <vt:ColumnHeader runat="server" Text="Column1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="635px" ID="lblLog2"></vt:Label>

          <!-- Tested ListView2 -->
        <vt:ListView runat="server" Text="TestedListView2" MenuDisabled="true" Top="515px" Left="400px" ID="TestedListView3" CssClass="vt-listview-3" View="Details">
            <Columns>
                <vt:ColumnHeader runat="server" Text="Column1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="635px" Left="400px" ID="lblLog3"></vt:Label>

    </vt:WindowView>

</asp:Content>
        