﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridRefreshDataController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        private void Load_View(object sender, EventArgs e)
		{
            GridElement grid = this.GetVisualElementById<GridElement>("gridElement");
		}

        public void Timer1_Tick(object sender,EventArgs e)
        {
            GridElement grid = this.GetVisualElementById<GridElement>("gridElement");
            grid.SetLoading(false);
            
            grid.Rows.Add(new []{ 
                "Cuota " + Environment.TickCount,
                "Tipo Cuota " + Environment.TickCount,
                "Fecha Inicio "+ Environment.TickCount, 
                "Fecha Vcto " + Environment.TickCount,
                "Plazo " + Environment.TickCount,
                "Saldo Capital " + Environment.TickCount,
                "Amortización " + Environment.TickCount,
            });

            grid.RefreshData();
        }
	}
}
