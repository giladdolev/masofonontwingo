﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumn ColData" ID="windowView1" LoadAction="GridColData\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ColData" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Returns or sets a user-defined variant associated with the given column
            
            Syntax: public Dictionary<int, object> ColData {get , private set}"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example shows a use of ColData property with two possible data collections.
            The collections contains few data types." Top="190px" ID="Label1"></vt:Label>

       <vt:Grid runat="server" Top="255px" Height="220px" Width="380px" ID="TestedGrid" ColumnMoveAction="GridGridColumnDisplayIndex\testedGrid_ColumnMove">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="idColumn" CssClass="vt-GridColumn1" HeaderText="#" Height="20px" Width="50px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="nameColumn" CssClass="vt-GridColumn2" HeaderText="First Name" Height="20px" Width="90px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="telephoneColumn" CssClass="vt-GridColumn3" HeaderText="Telephone No." Height="20px" Width="110px"></vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="presentColumn" CssClass="vt-GridColumn4" HeaderText="Present" Height="20px" Width="90px"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

       <vt:Label runat="server" SkinID="Log" Height="100px" Width="450px" ID="lblLog" Top="490px"></vt:Label>

        <vt:Button runat="server" Text="Set ColData with columns HeaderText >>" Top="620px" Width ="250px" ID="btnChangeColData" ClickAction="GridColData\btnChangeColData_Click"></vt:Button>

       
    </vt:WindowView>
</asp:Content>
