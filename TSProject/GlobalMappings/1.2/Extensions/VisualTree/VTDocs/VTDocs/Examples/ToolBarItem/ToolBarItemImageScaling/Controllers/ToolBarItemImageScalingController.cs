using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarItemImageScalingController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
        }
        private void btnChangeImage_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");

            TestedToolBar2.Items[0].Image = new UrlReference(@"Content/Images/Open.png");
        }
        
    }
}