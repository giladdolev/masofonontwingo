using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form_Load(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            ListViewItem newReportNode = new ListViewItem("xxxxx");
            newReportNode.Subitems.Add("1");
            listView.Items.Add(newReportNode);
            newReportNode = new ListViewItem("yyyy");
            newReportNode.Subitems.Add("2");
            listView.Items.Add(newReportNode);
            ColumnHeader header1 = new ColumnHeader();
            header1.Text = "Col1";
            listView.Columns.Add(header1);
        }

        private void btnFocus_Click(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            TextBoxElement txtFocus = this.GetVisualElementById<TextBoxElement>("txtFocus");
          
            txtFocus.Text = listView.Focus().ToString();
         
        }
    }
}