﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowShowDialog : WindowElement
    {

        ShowDialogTestedWindow _TestedWin;

        public WindowShowDialog()
        {

        }

        public ShowDialogTestedWindow TestedWin
        {
            get { return this._TestedWin; }
            set { this._TestedWin = value; }
        }
       

    }
}