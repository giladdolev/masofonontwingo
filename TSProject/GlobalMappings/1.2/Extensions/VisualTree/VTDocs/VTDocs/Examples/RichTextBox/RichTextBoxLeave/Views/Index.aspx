<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Leave event" ID="windowView2" LoadAction="RichTextBoxLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the rich text boxes,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:RichTextBox runat="server" Top="335px" Height="70px" Text="TestedRichTextBox1" ID="TestedRichTextBox1" CssClass="vt-TestedRichTextBox-1" LeaveAction="RichTextBoxLeave\TestedRichTextBox1_Leave"></vt:RichTextBox>

        <vt:RichTextBox runat="server" Top="420px" Height="70px" Text="TestedRichTextBox2" ID="TestedRichTextBox2" CssClass="vt-TestedRichTextBox-2" LeaveAction="RichTextBoxLeave\TestedRichTextBox2_Leave"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="505px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="545px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

