﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CellBorderStyle Property" ID="windowView1" LoadAction="GridCellBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CellBorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets the cell border style for the DataGridView.
            Note : only at initialize." Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Grid runat="server" Text="Grid1" Top="140px" ID="TestedGrid1" CellBorderStyle="None" Height="170">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="330px" ID="lblLog1"></vt:Label>
        
        <vt:Grid runat="server" Text="Grid2" Top="140px" ID="Grid2" Left="400" CellBorderStyle="Single" Height="170">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridColumn4" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn5" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn6" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="330px" ID="Label4" Left="400"></vt:Label>  
        
        <vt:Grid runat="server" Text="Grid3" Width="285px" Top="435px" ID="TestedGrid2" CellBorderStyle="SingleVertical" Height="170">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="620px" ID="lblLog2"></vt:Label> 
        
        <vt:Grid runat="server" Text="Grid4" Width="285px" Top="435px" ID="Grid1" CellBorderStyle="SingleHorizontal" Left="400" Height="170">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridColumn1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridColumn3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="620px" ID="Label3" Left="400"></vt:Label>

    </vt:WindowView>
</asp:Content>
