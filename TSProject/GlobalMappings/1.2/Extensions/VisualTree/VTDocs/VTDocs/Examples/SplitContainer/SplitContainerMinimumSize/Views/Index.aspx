<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer MinimumSize Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent - MinimumSize is set to 225, 85" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45"></vt:Label>     

        <vt:SplitContainer runat="server" Text="" Top="55px" MinimumSize="225, 85" Left="140px" ID="splMinimumSize" Height="70px" Width="200px"></vt:SplitContainer>           

        <vt:Label runat="server" Text="RunTime" Top="160px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:SplitContainer runat="server" Text="RuntimeMinimumSize" Top="190px" Left="140px" ID="splRuntimeMinimumSize" Height="70px" TabIndex="1" Width="200px" ></vt:SplitContainer>

        <vt:Button runat="server" Text="Change SplitContainer MinimumSize" Top="200px" Left="420px" ID="btnChangeMinimumSize" Height="36px" TabIndex="1" Width="220px" ClickAction="SplitContainerMinimumSize\btnChangeMinimumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Get SplitContainer Size" Top="240px" Left="420px" ID="btnGetSize" Height="36px" TabIndex="1" Width="220px" ClickAction="SplitContainerMinimumSize\btnGetSize_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="320px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        