<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Visible Property" ID="windowView2"  LoadAction="ButtonVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Button runat="server"  SkinID="Wide" Text="Button" Top="170px" ID="TestedButton1"></vt:Button>
        
        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="285px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this Button is initially set to 'false'
             The Button should be invisible" Top="335px" ID="lblExp1"></vt:Label>

        <vt:Button runat="server"  SkinID="Wide" Text="TestedButton" Visible="false" Top="400px" ID="TestedButton2"></vt:Button>
        
        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="515px" ID="Button1" Width="200px" ClickAction ="ButtonVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        