<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown PerformKeyUp() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformKeyUp of 'Tested NumericUpDown'" Top="45px" Left="140px" ID="btnPerformKeyUp" Height="36px" Width="300px" ClickAction="NumericUpDownPerformKeyUp\btnPerformKeyUp_Click"></vt:Button>


        <vt:NumericUpDown runat="server" Text="Tested NumericUpDown" Top="150px" Left="200px" ID="testedNumericUpDown" Height="20px"  Width="100px" KeyUpAction="NumericUpDownPerformKeyUp\testedNumericUpDown_KeyUp"></vt:NumericUpDown>           

        

    </vt:WindowView>
</asp:Content>
