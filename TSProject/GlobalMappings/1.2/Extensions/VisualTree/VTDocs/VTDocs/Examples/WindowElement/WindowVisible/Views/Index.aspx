<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window Visible Property" Top="57px" Left="0px" ID="windowView1" Height="800px" Width="700px">
		
		<vt:Button runat="server" TextAlign="MiddleCenter" Text="Open The Tested Window" Top="100px" Left="200px" ClickAction="WindowVisible\btnOpenTestedWindow_Click" ID="btnOpenTestedWindow" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="250px">
		</vt:Button>
        
        
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change Tested Window Visible Property" Top="200px" Left="200px" ClickAction="WindowVisible\btnChangeVisible_Click" ID="btnChangeVisible" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="250px">
		</vt:Button>
		

        <vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="350px" Left="200px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="250px">
		</vt:TextBox>

        </vt:WindowView>
</asp:Content>