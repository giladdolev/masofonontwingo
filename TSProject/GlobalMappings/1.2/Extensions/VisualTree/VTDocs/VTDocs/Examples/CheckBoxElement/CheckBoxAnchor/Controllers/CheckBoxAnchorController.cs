using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkAnchorChange = this.GetVisualElementById<CheckBoxElement>("chkAnchorChange");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");


            if (chkAnchorChange.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                chkAnchorChange.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                panel2.Size = new Size(400, 171);
            }

            else if (chkAnchorChange.Anchor == AnchorStyles.Top)
            {
                chkAnchorChange.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                panel2.Size = new Size(315, 190);
            }
            else if (chkAnchorChange.Anchor == AnchorStyles.Left)
            {
                chkAnchorChange.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                panel2.Size = new Size(200, 190);
            }
            else if (chkAnchorChange.Anchor == AnchorStyles.Right)
            {
                chkAnchorChange.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                panel2.Size = new Size(200, 160);
            }
            else if (chkAnchorChange.Anchor == AnchorStyles.Bottom)
            {
                chkAnchorChange.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the panel size
            }
            else if (chkAnchorChange.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and btnChangeAnchor settings
                chkAnchorChange.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                panel2.Size = new Size(300, 72);//Set  back the panel size
                chkAnchorChange.Location = new Point(75, 18);
            }       
        }

    }
}