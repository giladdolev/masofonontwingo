using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxSelectionFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "SelectionFont value:\\r\\n" + TestedRichTextBox.SelectionFont + "\\r\\nText value:\\r\\n" + TestedRichTextBox.Text;
        }


        public void btnChangeSelectionFont_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRichTextBox.SelectionFont.Underline == true)
            {
                TestedRichTextBox.SelectionFont = new Font("Niagara Engraved", 12, FontStyle.Italic);
                lblLog.Text = "SelectionFont value:\\r\\n" + TestedRichTextBox.SelectionFont + "\\r\\nText value:\\r\\n" + TestedRichTextBox.Text;
            }
            else if (TestedRichTextBox.SelectionFont.Bold == true)
            {
                TestedRichTextBox.SelectionFont = new Font("Miriam Fixed", 8, FontStyle.Underline);
                lblLog.Text = "SelectionFont value:\\r\\n" + TestedRichTextBox.SelectionFont + "\\r\\nText value:\\r\\n" + TestedRichTextBox.Text;
            }
            else if (TestedRichTextBox.SelectionFont.Italic == true)
            {
                TestedRichTextBox.SelectionFont = new Font("SketchFlow Print", 10, FontStyle.Strikeout);
                lblLog.Text = "SelectionFont value:\\r\\n" + TestedRichTextBox.SelectionFont + "\\r\\nText value:\\r\\n" + TestedRichTextBox.Text;
            }
            else
            {
                TestedRichTextBox.SelectionFont = new Font("SketchFlow Print", 20, FontStyle.Bold);
                lblLog.Text = "SelectionFont value:\\r\\n" + TestedRichTextBox.SelectionFont + "\\r\\nText value:\\r\\n" + TestedRichTextBox.Text;
            }
        }

        public void TestedRichTextBox_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            lblLog.Text = "SelectionFont value:\\r\\n" + TestedRichTextBox.SelectionFont + "\\r\\nText value:\\r\\n" +TestedRichTextBox.Text;
        }
    }
}