using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CompositeContextMenuStripController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new CompositeContextMenuStrip());
        }
        private CompositeContextMenuStrip ViewModel
        {
            get { return this.GetRootVisualElement() as CompositeContextMenuStrip; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CompositeElement TestedCompositeIndex1 = this.GetVisualElementById<CompositeElement>("TestedCompositeIndex1");
            CompositeElement TestedCompositeIndex2 = this.GetVisualElementById<CompositeElement>("TestedCompositeIndex2");

            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            //Write to log lblLog1
            if (TestedCompositeIndex1.ContextMenuStrip == null)
                lblLog1.Text = "ContextMenuStrip value is null";
            else
                lblLog1.Text = "ContextMenuStrip value is: " + TestedCompositeIndex1.ContextMenuStrip.ID;

            //Write to log lblLog2
            if (TestedCompositeIndex2.ContextMenuStrip == null)
                lblLog2.Text = "ContextMenuStrip value is null.";
            else
                lblLog2.Text = "ContextMenuStrip value is: " + TestedCompositeIndex2.ContextMenuStrip.ID + ".";
        }


        private void btnChangToContextMenuStrip1_Click()
        {
            CompositeElement TestedCompositeIndex2 = this.GetVisualElementById<CompositeElement>("TestedCompositeIndex2");
            ContextMenuStripElement contextMenuStrip1 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedCompositeIndex2.ContextMenuStrip = contextMenuStrip1;
            lblLog2.Text = "ContextMenuStrip value is: " + TestedCompositeIndex2.ContextMenuStrip.ID + ".";

        }

        private void btnChangToContextMenuStrip2_Click()
        {
            CompositeElement TestedCompositeIndex2 = this.GetVisualElementById<CompositeElement>("TestedCompositeIndex2");
            ContextMenuStripElement contextMenuStrip2 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedCompositeIndex2.ContextMenuStrip = contextMenuStrip2;
            lblLog2.Text = "ContextMenuStrip value is: " + TestedCompositeIndex2.ContextMenuStrip.ID + ".";

        }

        private void btnReset_Click()
        {
            CompositeElement TestedCompositeIndex2 = this.GetVisualElementById<CompositeElement>("TestedCompositeIndex2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedCompositeIndex2.ContextMenuStrip = null;
            lblLog2.Text = "ContextMenuStrip value is null.";

        }
  
    }
}