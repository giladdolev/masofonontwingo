<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
       <vt:WindowView runat="server" Text="Window Activate Method" Activate="true" ID="windowView2" LoadAction="WindowActivate\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Activate" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Activates the Window and gives it focus.

            Syntax: public void Activate()
            Activating a Window brings it to the front.The Window must be visible for this method to have any effect.
             " Top="75px"  ID="lblDefinition"></vt:Label>
               
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Click the following button to invoke Activate method on TestedWindow." Top="280px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" Text="Activate TestedWindow >>" Top="370px" Width="180px" ID="btnActivateTestedWindow" ClickAction="WindowActivate\btnActivateTestedWindow_Click"></vt:Button>

         <vt:Label runat="server" SkinID="Log" Top="420px"  ID="lblLog"></vt:Label>

    </vt:WindowView>
</asp:Content>
