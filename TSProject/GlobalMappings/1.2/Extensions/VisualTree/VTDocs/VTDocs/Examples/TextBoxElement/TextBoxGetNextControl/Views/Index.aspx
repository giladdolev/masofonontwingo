<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GetNextControl" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="TextBoxGetNextControl\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="GetNextControl Method" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Retrieves the next control forward or back in the tab order of child controls.

            Syntax: public Task &lt;ControlElement&gt; GetNextControl(ControlElement ctl, bool forward)
            
            Return Value: The next/previous ControlElement object if exsists; otherwise, null.
            
            Remarks: If the given ControlElement is the last ControlElement in the order of tabs
            and we want to find the next ControlElement, the method will return null.
            Same result for the first ControlElement and previous search.
            
            In the example below, each button represents the TextBox on its left.
            Hover over the button'Hover Over Me'.You should see the ID of
            the Next/Previous ControlElement ID in the logger." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="400px" ID="lblExample"></vt:Label>

        <vt:TextBox runat="server" Text="My ID is 'textBox1'" Top="450px"  Left="20px" ID="textBox1"  Height="20px" Width="180px"></vt:TextBox>
        <vt:TextBox runat="server" Text="My ID is 'textBox2'" Top="500px"  Left="20px" ID="textBox2"  Height="20px" Width="180px"></vt:TextBox>
        <vt:TextBox runat="server" Text="My ID is 'textBox3'" Top="550px"  Left="20px" ID="textBox3"  Height="20px" Width="180px"></vt:TextBox>
        <vt:TextBox runat="server" Text="My ID is 'textBox4'" Top="600px"  Left="20px" ID="textBox4"  Height="20px" Width="180px"></vt:TextBox>
        <vt:Button runat="server" Text="Hover Over Me" Top="450px" Left="300px" ID="button1" Height="36px" Width="200px" MouseHoverAction="TextBoxGetNextControl\Mouse_Hover1"></vt:Button>
        <vt:Button runat="server" Text="Hover Over Me" Top="500px" Left="300px" ID="button2" Height="36px" Width="200px" MouseHoverAction="TextBoxGetNextControl\Mouse_Hover2"></vt:Button>
        <vt:Button runat="server" Text="Hover Over Me" Top="550px" Left="300px" ID="button3" Height="36px" Width="200px" MouseHoverAction="TextBoxGetNextControl\Mouse_Hover3"></vt:Button>
        <vt:Button runat="server" Text="Hover Over Me" Top="600px" Left="300px" ID="button4" Height="36px" Width="200px" MouseHoverAction="TextBoxGetNextControl\Mouse_Hover4"></vt:Button>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="650px" Height="40px"></vt:Label>
        
    </vt:WindowView>
</asp:Content>
