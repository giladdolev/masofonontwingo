using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnTestedLabel_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");

            if (label.Size == new Size(350, 70))
            {
                label.Size = new Size(75, 100);
            }
            else
            {
                label.Size = new Size(350, 70);
            }
        }

        public void btnTestedLabel_SizeChanged(object sender, EventArgs e)
        {
            LabelElement lblEvent = this.GetVisualElementById<LabelElement>("lblEvent");
            if (lblEvent != null)
                lblEvent.Text = "label SizeChanged event is invoked";
        }
 
    }
}