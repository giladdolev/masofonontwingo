using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarBackgroundImageLayoutController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackgroundImageLayout value: " + TestedToolBar1.BackgroundImageLayout.ToString();

            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + TestedToolBar2.BackgroundImageLayout.ToString() + ".";

        }
        private void btnNone_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            TestedToolBar2.BackgroundImageLayout = ImageLayout.None;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + TestedToolBar2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnTile_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            TestedToolBar2.BackgroundImageLayout = ImageLayout.Tile;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + TestedToolBar2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnCenter_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            TestedToolBar2.BackgroundImageLayout = ImageLayout.Center;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + TestedToolBar2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnStretch_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            TestedToolBar2.BackgroundImageLayout = ImageLayout.Stretch;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + TestedToolBar2.BackgroundImageLayout.ToString() + ".";
        }
        private void btnZoom_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            TestedToolBar2.BackgroundImageLayout = ImageLayout.Zoom;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + TestedToolBar2.BackgroundImageLayout.ToString() + ".";
        }

    }
}