using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;

namespace HelloWorldApp
{
    public class BasicGridButtonColumnController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void page_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            
            //option 1
            gridElement.CellButtonClick += gridElement_CellButtonClick;
            ((GridTextButtonColumn)gridElement.Columns[1]).ButtonText = "...";


            for (int j = 0; j < 7; j++)
            {
                gridElement.Rows[j].Cells[1].Value = string.Format("{0}", j);
            }

            //option 2

            GridTextButtonColumn col = new GridTextButtonColumn();
            col.ButtonText = "XXX";
            col.ReadOnly = true;

            gridElement.Columns.Add(col);

        }

        private async void gridElement_CellButtonClick(object sender, GridElementCellEventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            int row = gridElement.SelectedRowIndex;
            await MessageBox.Show(Convert.ToString(gridElement.Rows[row].Cells[1].Value));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridElement dgvCalendario = this.GetVisualElementById<GridElement>("gridElement");
            GridImageColumn a = new GridImageColumn();
            //a.Image = 
            //Create column
            GridColumn col;
            col = new GridColumn("name");
            col.Width = 50;
            dgvCalendario.MaxRows = 1;
            //Create row          
            GridRow gridRow = new GridRow();
            GridContentCell gridCellElement1 = new GridContentCell();
            gridCellElement1.Value = "new Content";
            gridRow.Cells.Add(gridCellElement1);


            //Add to grid
            dgvCalendario.Columns.Add(col);
            dgvCalendario.Columns.Add(a);
            dgvCalendario.Rows.Add(gridRow);



            GridRow gridRow1 = new GridRow();
            GridContentCell gridCellElement11 = new GridContentCell();
            gridCellElement11.Value = "new Content1";
            gridRow.Cells.Add(gridCellElement11);


            //Add to grid
            dgvCalendario.Columns.Add(col);
            dgvCalendario.Columns.Add(a);
            dgvCalendario.Rows.Add(gridRow1);
            dgvCalendario.RefreshData();
            dgvCalendario.Refresh();


           
        }

        void gridElement_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
        }
        void act(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            MessageBox.Show(gridElement.SelectedColumnIndex.ToString());

        }

        void gridElement_CellEndEdit(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
        }

        void gridElement_CellBeginEdit(object sender, GridElementCellEventArgs e)
        {

        }
        private void button1_Click1(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");

            for (int i = 0; i < gridElement.Rows.Count; i++)
            {
                gridElement.SetRowHeaderBackgroundColor(i, Color.Aquamarine);
            }

        }



    }
}
