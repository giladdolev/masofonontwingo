using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewAddRangeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      
        private void Form_Load(object sender, EventArgs e)
        {

        }

        public void btnAddRange_Click(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView2");
            ListViewItem[] lstitems = new ListViewItem[3];
            lstitems[0] = new ListViewItem("ListViewItem1");
            lstitems[1] = new ListViewItem("ListViewItem2");
            lstitems[2] = new ListViewItem("ListViewItem3");
            listView.Items.AddRange(lstitems);

            ListViewItemCollection lst = new ListViewItemCollection(listView);
            lst.Add("01","ListViewItem4");
            lst.Add("02", "ListViewItem5");
            lst.Add("03", "ListViewItem6");
            listView.Items.AddRange(lst);

            MessageBox.Show(listView.SelectedIndexes.Count.ToString());

        }


    }
}