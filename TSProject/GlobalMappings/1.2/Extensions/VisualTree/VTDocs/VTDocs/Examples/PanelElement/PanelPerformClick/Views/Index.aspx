<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Panel runat="server" Text="Tested Panel" Top="150px" Left="200px" ID="TestedPanel" Height="70px" Width="200px" ClickAction="PanelPerformClick\TestedPanel_Click"></vt:Panel>

        <vt:Button runat="server" Text="PerformClick of 'Tested Panel'" Top="45px" Left="140px" ID="btnPerformClick" Height="36px" Width="300px" ClickAction="PanelPerformClick\btnPerformClick_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
