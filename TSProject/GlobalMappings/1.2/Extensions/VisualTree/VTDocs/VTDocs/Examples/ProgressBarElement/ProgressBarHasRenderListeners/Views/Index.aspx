<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar HasRenderListeners Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="ReadOnly property" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="45px" Left="140px" ID="testedProgressBar" Height="36px" Width="220px"></vt:ProgressBar>

        <vt:Button runat="server" Text="Get HasRenderListeners value" Top="45px" Left="400px" ID="btnHasRenderListeners" Height="36px" Width="250px" ClickAction ="ProgressBarHasRenderListeners\btnHasRenderListeners_Click"></vt:Button>           

        <vt:TextBox runat="server" Text="" Top="120px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
