<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TablePanel BackgroundImage property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
     
        <vt:TablePanel runat="server" Text="" Top="140px" Left="130px" ID="tablePanel" Width="500px" Height="350" BackgroundImage="Content/Images/gizmox.png"></vt:TablePanel>

        <vt:Button runat="server" Text="Change TablePanel BackgroundImage" Top="70px" Left="140px" ID="btnBackgroundImage" Height="36px" TabIndex="1" Width="300px" ClickAction="TablePanelBackgroundImage\btnBackgroundImage_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
