<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="Employee" Height="550px" Width="1000px" ID="WindowView1" BackColor="LightSeaGreen" >

        <vt:Label runat="server" SkinID="Title" Left="370px"  Text="Employee Desktop" ID="lblTitle"></vt:Label>

        <vt:Tab runat="server" Width="865px" Left="75px" Top="100px" Height="380px" ID="TabControl">
            <TabItems>
                <vt:TabItem runat="server" SkinID="" Text="Tasks" Font-Bold="true" ID="TasksTab" >

                    <vt:Button runat="server" ID="btnShowTasks" SkinID="Wide" Font-Size="10pt" Width="130px" Top="50px" Left="25px" Text="Show Tasks" ClickAction="Employee\btnShowTasks_Click"></vt:Button>

                    <vt:Button runat="server" ID="btnClearTasks" SkinID="Wide" Font-Size="10pt" Width="130px" Text="Clear" Top="100px" Left="25px" ClickAction="Employee\btnClearTasks_Click"></vt:Button>

                    <vt:Label runat="server" Font-Bold="true" Left="240px" Top="20px" Font-Size="19pt" Text="List" ID="lblList"></vt:Label>

                    <vt:TextBox runat="server" ID="txtList" Text="" Top="50px" Width="170px" Height="240px" Left="185px" Multiline="true"></vt:TextBox>

                    <vt:Panel runat="server"  Top="50px" Left="400px" Height="240px" Width="200px" BackColor="LightGray">
                        <vt:Label runat="server" ID="lblAttention1" Top="40px" Left="55px"  Text="Attention" Font-Underline="true" Font-Bold="true" Font-Names="Calibri" Font-Size="18pt" ></vt:Label>
                        <vt:Label runat="server" ID="lblAttention2" Text="3 Personal Tasks" BackColor="LightSeaGreen" Height="50px" Top="90px" Left="30px" Width="150px"  Font-Names="Calibri" Font-Size="16pt" ></vt:Label>
                    </vt:Panel>
                </vt:TabItem>

            </TabItems>

        </vt:Tab>

       
    </vt:WindowView>


    <%--<vt:WindowView runat="server" MaximizeBox="True" MinimizeBox="True" Text="Base View" ID="BasicWindow" BackColor="Green" AcceptButtonID="buttonOK" CancelButtonID="buttonCancel" Top="57px" Left="11px" Height="800px" Width="700px">
		<vt:Panel runat="server" Dock="Fill" ID="content" BackColor="Blue">
		</vt:Panel>
		<vt:Panel runat="server" Dock="Bottom" Height="50" BackColor="Yellow">
            <vt:Button runat="server" ID="buttonOK" Text="OK" ClickAction="BaseView\buttonOK_Click"></vt:Button>
            <vt:Button runat="server" ID="buttonCancel" Text="Cancel" Left="100"></vt:Button>
		</vt:Panel>
	</vt:WindowView>--%>
</asp:Content>
