<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab ContextMenuStrip Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        
        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:Tab runat="server" Text="Initialized with ContextMenuStrip1" ContextMenuStripID = "ContextMenuStrip1" Top="45px" Left="140px" ID="tab1" Height="150px" TabIndex="1" Width="220px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>  
          
        <vt:Label runat="server" Text="RunTime" Top="230px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:Tab runat="server" Text="Tab ContextMenuStrip - runtime" Top="255px" Left="140px" ID="ContextMenuStrip2" Height="150px" Width="220px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem4" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Button runat="server" Text="Set/Unset ContextMenuStrip" Top="280px" Left="400px" ID="btnSetContextMenuStrip" Height="36px"  TabIndex="1" Width="200px" ClickAction="TabContextMenuStrip\btnSetContextMenuStrip_Click"></vt:Button>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip1" >
            <vt:ToolBarMenuItem runat ="server" id ="exitToolStripMenuItem" Text="Exit" ClickAction = "TabContextMenuStrip\exitToolStripMenuItem_Click"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

    </vt:WindowView>
</asp:Content>
