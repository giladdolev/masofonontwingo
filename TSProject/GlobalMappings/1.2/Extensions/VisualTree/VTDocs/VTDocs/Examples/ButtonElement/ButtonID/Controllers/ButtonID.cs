using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonIDController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnID_Click(object sender, EventArgs e)
        {
            ButtonElement btnID = this.GetVisualElementById<ButtonElement>("btnID");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            textBox1.Text = "ID value:\\r\\n" + btnID.ID;
        }

    }
}