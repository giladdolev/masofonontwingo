using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformClick_Click(object sender, EventArgs e)
        {

            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");


            TestedCheckBox.PerformClick(e);
        }

        public void TestedCheckBox_Click(object sender, EventArgs e)
        {

            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");

            MessageBox.Show("TestedCheckBox Click event method is invoked");
        }

    }
}