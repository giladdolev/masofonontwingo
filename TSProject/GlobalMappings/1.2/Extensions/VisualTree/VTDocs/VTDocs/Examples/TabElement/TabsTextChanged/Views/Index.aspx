<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TextChanged event" ID="windowView1" LoadAction="TabsTextChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="TextChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the Text property value changes.

            Syntax: public event EventHandler TextChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the Text property by clicking the 'Change Text' button. 
            Each invocation of the 'TextChanged' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:Tab runat="server" Text="TestedTab" Top="325px" ID="TestedTab" TextChangedAction="TabsTextChanged\TestedTab_TextChanged">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="440px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="480px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change Text >>" Top="625px" ID="btnChangeText" ClickAction ="TabsTextChanged\btnChangeText_Click" ></vt:Button>          
       
    </vt:WindowView>
</asp:Content>
