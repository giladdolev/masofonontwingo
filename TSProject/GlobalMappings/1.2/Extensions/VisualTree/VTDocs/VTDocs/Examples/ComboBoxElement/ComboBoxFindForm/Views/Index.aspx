<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox FindForm method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxFindForm/Form_Load">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="160" Left="50px"/>
        <vt:Button runat="server" Text="Get comboBox Form" Top="112px" Left="130px" ID="btnFindForm" TabIndex="1" Width="250px" ClickAction="ComboBoxFindForm\btnFindForm_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
