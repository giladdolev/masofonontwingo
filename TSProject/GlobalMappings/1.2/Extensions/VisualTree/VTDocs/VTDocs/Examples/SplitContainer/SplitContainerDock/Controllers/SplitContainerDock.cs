using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new SplitContainerDock());
        }

        private SplitContainerDock ViewModel
        {
            get { return this.GetRootVisualElement() as SplitContainerDock; }
        }

        public void btnDock_Click(object sender, EventArgs e)
        {
            SplitContainer splDock = this.GetVisualElementById<SplitContainer>("splDock");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (splDock.Dock == Dock.Bottom)
            {
                splDock.Dock = Dock.Fill;
                textBox1.Text = splDock.Dock.ToString();
            }
            else if (splDock.Dock == Dock.Fill)
            {
                splDock.Dock = Dock.Left;
                textBox1.Text = splDock.Dock.ToString();
            }
            else if (splDock.Dock == Dock.Left)
            {
                splDock.Dock = Dock.Right;
                textBox1.Text = splDock.Dock.ToString();
            }
            else if (splDock.Dock == Dock.Right)
            {
                splDock.Dock = Dock.Top;
                textBox1.Text = splDock.Dock.ToString();
            }
            else if (splDock.Dock == Dock.Top)
            {
                splDock.Dock = Dock.None;
                textBox1.Text = splDock.Dock.ToString();
            }
            else if (splDock.Dock == Dock.None)
            {
                splDock.Dock = Dock.Bottom;
                textBox1.Text = splDock.Dock.ToString();
            }  
        }

        
       

        

    
       
    }
}