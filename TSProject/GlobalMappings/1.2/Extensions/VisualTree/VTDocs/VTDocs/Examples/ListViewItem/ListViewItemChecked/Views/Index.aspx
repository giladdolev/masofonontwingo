
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListViewItem Checked Property" Height="800px" ID="windowView1" LoadAction="ListViewItemChecked\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Checked" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the item is checked.
            
            Syntax: public bool Checked { get; set; }
            Property value: true if the item is checked; otherwise, false. The default is false.
            This property is useful only if the CheckBoxes property of the ListView control the item is contained in is 
            set to true. To take action when an item has been checked, you can create an event handler for the
             ItemCheck property of the ListView control. " Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:ListView runat="server" Text="ListView" CheckBoxes="true" Top="240px" ID="TestedListView1">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColHeader1" width="80"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColHeader2" width="80"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColHeader3" width="80"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>
        <vt:Label runat="server" SkinID="Log" Top="350px" Height="50px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="420px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Checked property of third item of this ListView is initially set to 'true'" Top="465px"  ID="lblExp1"></vt:Label>     
        
        <vt:ListView runat="server" Text="TestedListView" Top="515px" ID="TestedListView2" CssClass="vt-listviewItem-checked" CheckBoxes="true" ItemCheckAction="ListViewItemChecked\TestedListView2_ItemCheck">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColHeader1" width="80"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColHeader2" width="80"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColHeader3" width="80"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g" Checked="true">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>
        <vt:Label runat="server" SkinID="Log" Top="625px" ID="lblLog2" Height="50px"></vt:Label>
        <vt:Button runat="server" Text="Change Second Item Checked Value >>" Width="220px" Top="710px" ID="btnChangeItem2Checked" ClickAction="ListViewItemChecked\btnChangeItem2Checked_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

