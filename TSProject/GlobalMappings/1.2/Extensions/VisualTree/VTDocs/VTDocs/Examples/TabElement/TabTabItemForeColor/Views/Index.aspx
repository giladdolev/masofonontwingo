<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem ForeColor property" ID="windowView1" LoadAction="TabTabItemForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Tab1 -->
        <vt:Tab runat="server" Text="Tab" Top="170px" ID="TestedTab1" Height="110px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tab1Page1" Height="74px" Width="192px"> 
                    <vt:Button runat="server" Text="Click Me" Top="28px" Left="60px" ID="button1" Width="80px"></vt:Button>
                    <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label1" Width="80px" Left="155px"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tab1Page2" Height="74px" Width="192px">
                    <vt:Button runat="server" Text="Click Me" Top="28px" Left="60px" ID="button2" Width="80px"></vt:Button>
                    <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label2" Width="80px" Left="155px"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tab1Page3" Height="74px" Width="192px">
                     <vt:Button runat="server" Text="Click Me" Top="28px" Left="60px" ID="button3" Width="80px"></vt:Button>
                     <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label3" Width="80px" Left="155px"></vt:Label>
                </vt:TabItem>
              </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="290px" BorderStyle="None" ID="lblLog1" Height="50px" Width="320px"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of the tabPages is initially set to Green" Top="400px" ID="lblExp1"></vt:Label>

        <!-- Tested Tab2 -->
        <vt:Tab runat="server" Text="TestedTab" Top="440px" ForeColor="Green" ID="TestedTab2" Height="110px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage4" Top="22px" Left="4px" ID="tabPage4" Height="74px" Width="192px" ForeColor="Green">
                    <vt:Button runat="server" Text="Click Me" Top="28px" Left="60px" ID="button4" Width="80px"></vt:Button>
                    <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label4" Width="80px" Left="155px"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage5" Top="22px" Left="4px" ID="tabPage5" Height="74px" Width="192px" ForeColor="Green">
                    <vt:Button runat="server" Text="Click Me" Top="28px" Left="60px" ID="button5" Width="80px"></vt:Button>
                    <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label5" Width="80px" Left="155px"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage6" Top="22px" Left="4px" ID="tabPage6" Height="74px" Width="192px" ForeColor="Green">
                    <vt:Button runat="server" Text="Click Me" Top="28px" Left="60px" ID="button6" Width="80px"></vt:Button>
                    <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label6" Width="80px" Left="155px"></vt:Label>
                </vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="560px" ID="lblLog2" Width="320px" Height="50px"></vt:Label>

        <vt:Button runat="server" Text="Change tabPage4 ForeColor >>" Top="640px" Width="180px" ID="btnChangeTabPage4ForeColor_Click" ClickAction="TabTabItemForeColor\btnChangeTabPage4ForeColor_Click"></vt:Button>
        <vt:Button runat="server" Text="Change tabPage5 ForeColor >>" Top="640px" Width="180px" Left="290px" ID="btnChangeTabPage5ForeColor_Click" ClickAction="TabTabItemForeColor\btnChangeTabPage5ForeColor_Click"></vt:Button>
        <vt:Button runat="server" Text="Change tabPage6 ForeColor >>" Top="640px" Width="180px" Left="500px" ID="btnChangeTabPage6ForeColor_Click" ClickAction="TabTabItemForeColor\btnChangeTabPage6ForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
