<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid BorderStyle Property" ID="windowView1" LoadAction="GridBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Grid runat="server" Top="170px" ID="TestedGrid1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this Grid is initially set to 'Dotted'" Top="405px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server"  BorderStyle="Dotted" Top="445px" ID="TestedGrid2">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="575px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="640px" ID="btnChangeGridBorderStyle" ClickAction="GridBorderStyle\btnChangeGridBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
