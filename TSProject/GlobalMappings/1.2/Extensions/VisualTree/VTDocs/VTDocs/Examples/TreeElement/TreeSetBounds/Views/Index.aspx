<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree SetBounds() Method" ID="windowView" LoadAction="TreeSetBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SetBounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the bounds of the control to the specified location and size.

            Syntax: public void SetBounds(int left, int top, int width, int height)" Top="75px" ID="Label1" ></vt:Label>
                 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="SetBounds Method of this 'TestedTree' is initially set with 
            Left: 80px, Top: 300px, Width: 140px, Height: 110px" Top="235px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tree runat="server" Text="TestedTree" Top="300px" ID="TestedTree">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>
        <vt:Label runat="server" SkinID="Log" Top="425px" Height="40" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="SetBounds >>"  Top="520px" Width="180px" ID="btnSetBounds" ClickAction="TreeSetBounds\btnSetBounds_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

