using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button Height is: " + btnTestedButton.Height;

        }
        public void btnChangeHeight_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            if (btnTestedButton.Height == 60)
            {
                btnTestedButton.Height = 40;
                lblLog.Text = "Button Height is: " + btnTestedButton.Height;

            }
            else
            {
                btnTestedButton.Height = 60;
                lblLog.Text = "Button Height is: " + btnTestedButton.Height;
            }

        }

              
    }
}