using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowActiveFormController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowActiveForm());
        }
        private WindowActiveForm ViewModel
        {
            get { return this.GetRootVisualElement() as WindowActiveForm; }
        }

        public void btnGetActiveForm_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
           // textBox1.Text = WindowElement.ActiveForm.ToString();
		}

    }
}