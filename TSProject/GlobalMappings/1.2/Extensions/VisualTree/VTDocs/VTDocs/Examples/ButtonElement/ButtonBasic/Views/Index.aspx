<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" EnableTheming="true" StylesheetTheme="ThemeRed" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Button" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" BackColor="Beige">
        <vt:Panel runat="server" ID="pan1" Width="200" Height="200" ZIndex="-1">
            <vt:Button runat="server" Visible="false" Text="Change Button Font" Top="44px" Left="140px" ID="button1" ClickAction="BasicButton\button1_Click" ToolTipText="button 1 ToolTip"></vt:Button>
             <vt:Button runat="server" Text="Change Button Font" Top="44px" Left="140px" ID="button2" ClickAction="BasicButton\button1_Click" ToolTipText="button 1 ToolTip"></vt:Button>
        </vt:Panel>
        <%--  <vt:Button runat="server" Text="Change Button Colors Change Button Colors Change Button Colors" Top="94px" Left="140px" ID="button2" Height="36px" ForeColor="Red" Width="120px" ClickAction="BasicButton\button2_Click" ToolTipText="button 2 ToolTip"></vt:Button>
        <vt:Button runat="server"  Top="144px" Left="140px" ID="button3" Height="36px" Width="120px" ClickAction="BasicButton\button3_Click" BackgroundImage="Content/Images/Cute.png" BackColor="Yellow" BackgroundImageLayout="Zoom" ToolTipText="button 3 ToolTip"></vt:Button>
    
    <vt:Button runat="server" Text="ChangeButtonColorsChangeButtonColors ChangeButtonColors" Top="194px" Left="140px" ID="button4" Height="36px" ForeColor="Red" Width="120px" ClickAction="BasicButton\button2_Click" ToolTipText="button 2 ToolTip"></vt:Button>

        <vt:Label runat="server"  Top="250px" Left="140px" Text="Skinnable buttons:"></vt:Label>
        <vt:Button runat="server"  Top="270px" Left="140px" ID="b6" Text="Defaul Button"></vt:Button>
        <vt:Button runat="server"  Top="320px" Left="140px" ID="b7" SkinID="bigred" Text="Big Red Button"></vt:Button>--%>
    </vt:WindowView>
</asp:Content>
