<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic LinkLabel" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:LinkLabel runat="server" ID="lnklabel" Width="40" Heigh="25" Text="Go To"  Top="20px" Left="40px" LinkData="http://www.gizmoxts.com/"></vt:LinkLabel>

        <vt:Button runat="server" Text="Change link Text >>" Width="160px" Left="20px" Top="100px" ID="btnChangeLinkText" ClickAction="BasicLinkLabel\btnChangeLinkText_Click"></vt:Button>

         <vt:Button runat="server" Text="Change link >>" Width="160px" Left="230px" Top="100px" ID="btnChangeLink" ClickAction="BasicLinkLabel\btnChangeLink_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
