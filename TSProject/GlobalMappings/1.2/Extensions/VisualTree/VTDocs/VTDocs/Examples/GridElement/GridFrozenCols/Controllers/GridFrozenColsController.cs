using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridFrozenColsController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement rbFrozenCols1 = this.GetVisualElementById<RadioButtonElement>("rbFrozenCols1");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGrid1.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
            TestedGrid1.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
            TestedGrid1.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
            TestedGrid1.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");

            TestedGrid2.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
            TestedGrid2.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
            TestedGrid2.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
            TestedGrid2.Rows.Add("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");

            rbFrozenCols1.IsChecked = true;

            lblLog1.Text = "FrozenCols value is: " + TestedGrid1.FrozenCols;
            lblLog2.Text = "FrozenCols value is: " + TestedGrid2.FrozenCols;
        }

        private void rbFrozenCols1_CheckedChanged(object sender, EventArgs e)
		{
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");

            TestedGrid2.FrozenCols = 1;
            lblLog2.Text = "FrozenCols value is: " + TestedGrid2.FrozenCols;       
		}

        private void rbFrozenCols2_CheckedChanged(object sender, EventArgs e)
        {
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");

            TestedGrid2.FrozenCols = 2;
            lblLog2.Text = "FrozenCols value is: " + TestedGrid2.FrozenCols;
        }

        private void rbFrozenCols0_CheckedChanged(object sender, EventArgs e)
        {
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");

            TestedGrid2.FrozenCols = 0;
            lblLog2.Text = "FrozenCols value is: " + TestedGrid2.FrozenCols;
        }

	}
}
