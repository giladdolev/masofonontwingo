using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton1 = this.GetVisualElementById<ButtonElement>("btnTestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "ForeColor value: " + btnTestedButton1.ForeColor.ToString();

            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ForeColor value: " + btnTestedButton2.ForeColor.ToString();

        }


        public void btnChangeForeColor_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (btnTestedButton2.ForeColor == Color.YellowGreen)
            {
                btnTestedButton2.ForeColor = Color.Black;
                lblLog2.Text = "ForeColor value: " + btnTestedButton2.ForeColor.ToString();
            }
            else
            {
                btnTestedButton2.ForeColor = Color.YellowGreen;
                lblLog2.Text = "ForeColor value: " + btnTestedButton2.ForeColor.ToString();

            }
        }

        
    }
}