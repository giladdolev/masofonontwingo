using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxWaitMaskDisabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void btnToggler_Click(object sender, EventArgs e)
        {
            var txtTestedTextBox = this.GetVisualElementById<TextBoxElement>("textBox3");
            var btnToggler = this.GetVisualElementById<ButtonElement>("btnToggler");
            var statusLabel = this.GetVisualElementById<LabelElement>("statusLabel");

            txtTestedTextBox.WaitMaskDisabled = !txtTestedTextBox.WaitMaskDisabled;
            btnToggler.Text = "Set WaitMaskDisabled property to " + Convert.ToString(!txtTestedTextBox.WaitMaskDisabled);
            statusLabel.Text = "WaitMaskDisabled = " + Convert.ToString(txtTestedTextBox.WaitMaskDisabled);
            
        }


        public void txtTestedTextBox_TextChanged(object sender, EventArgs e)
        {
            //Dummy server delay and network latency
            System.Threading.Thread.Sleep(3000);
        }



    }
}