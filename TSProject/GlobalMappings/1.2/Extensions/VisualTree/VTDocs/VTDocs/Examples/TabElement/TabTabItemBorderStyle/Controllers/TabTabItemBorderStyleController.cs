using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabItemBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void OnLoad(object sender, EventArgs e)
        {
            TabItem Tab1Item2 = this.GetVisualElementById<TabItem>("Tab1Item2");
            TabItem Tab1Item3 = this.GetVisualElementById<TabItem>("Tab1Item3");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BorderStyle value for 'tabItem3': " + Tab1Item3.BorderStyle.ToString() + "\\r\\n" + "BorderStyle value for 'tabItem2': " + Tab1Item2.BorderStyle.ToString();
           

            TabItem Tab2Item2 = this.GetVisualElementById<TabItem>("Tab2Item2");
            TabItem Tab2Item1 = this.GetVisualElementById<TabItem>("Tab2Item1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BorderStyle value for 'tabItem3': " + Tab2Item1.BorderStyle.ToString() + "\\r\\n" + "BorderStyle value for 'tabItem2': " + Tab2Item2.BorderStyle.ToString();
        }


        public void btnChangeBorderStyleTab2Item1_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab2Item1 = this.GetVisualElementById<TabItem>("Tab2Item1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (Tab2Item1.BorderStyle == BorderStyle.None)
            {
                Tab2Item1.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Dotted)
            {
                Tab2Item1.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Double)
            {
                Tab2Item1.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Fixed3D)
            {
                Tab2Item1.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.FixedSingle)
            {
                Tab2Item1.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Groove)
            {
                Tab2Item1.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Inset)
            {
                Tab2Item1.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Dashed)
            {
                Tab2Item1.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.NotSet)
            {
                Tab2Item1.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Outset)
            {
                Tab2Item1.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Ridge)
            {
                Tab2Item1.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.ShadowBox)
            {
                Tab2Item1.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else if (Tab2Item1.BorderStyle == BorderStyle.Solid)
            {
                Tab2Item1.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }
            else
            {
                Tab2Item1.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + Tab2Item1.BorderStyle + '.';
            }


        }

        public void btnChangeBorderStyleTab2Item2_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            TabItem Tab2Item2 = this.GetVisualElementById<TabItem>("Tab2Item2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (Tab2Item2.BorderStyle == BorderStyle.None)
            {
                Tab2Item2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Dotted)
            {
                Tab2Item2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Double)
            {
                Tab2Item2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Fixed3D)
            {
                Tab2Item2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.FixedSingle)
            {
                Tab2Item2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Groove)
            {
                Tab2Item2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Inset)
            {
                Tab2Item2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Dashed)
            {
                Tab2Item2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.NotSet)
            {
                Tab2Item2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Outset)
            {
                Tab2Item2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Ridge)
            {
                Tab2Item2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.ShadowBox)
            {
                Tab2Item2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else if (Tab2Item2.BorderStyle == BorderStyle.Solid)
            {
                Tab2Item2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
            else
            {
                Tab2Item2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + Tab2Item2.BorderStyle + '.';
            }
        }
    }
}