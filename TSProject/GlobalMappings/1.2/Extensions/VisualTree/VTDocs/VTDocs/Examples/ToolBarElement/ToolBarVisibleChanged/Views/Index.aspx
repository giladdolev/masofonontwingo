<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar VisibleChanged Event" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        

        <vt:ToolBar runat="server" Text="Tested ToolBar" Dock="None" Top="102px" Left="276px" ID="toolBar1" Height="25px" Width="466px" VisibleChangedAction="ToolBarVisibleChanged\toolBar1_VisibleChanged" >          
        </vt:ToolBar>
        
        <vt:Button runat="server" Text="Change ToolBar Visible" Top="100px" Left="44px" ID="btnChangeVisible" Height="29px" Width="200px" ClickAction="ToolBarVisibleChanged\btnChangeVisible_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="250px" Left="320px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

        <vt:Label runat="server" Text="Event Log" Top="330px" Left="320px"  ID="label3"   Height="15px" Width="200px"> 
            </vt:Label>

        <vt:TextBox runat="server" Text="" Top="345px" Left="320px"  ID="txtEventLog" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        