<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxDataSource/Form_Load">
       <vt:ComboBox runat="server" Text="" DisplayMember="text" ValueMember="value" FormattingEnabled="True" Top="55px" Left="217px" ID="comboBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="21px" Width="121px">
		</vt:ComboBox>
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Add items" Top="55px" Left="37px" ClickAction="ComboBoxDataSource\BtnAdd_Click" ID="BtnAdd" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="23px" TabIndex="1" Width="75px">
		</vt:Button>
    </vt:WindowView>
</asp:Content>
