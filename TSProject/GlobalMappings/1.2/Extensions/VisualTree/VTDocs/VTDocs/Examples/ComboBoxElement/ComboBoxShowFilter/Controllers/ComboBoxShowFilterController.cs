using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxShowFilterController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            
            DataTable source = new DataTable();
            source.Columns.Add("value", typeof(int));
            source.Columns.Add("text", typeof(string));
            source.Columns.Add("Field3", typeof(string));
            source.Rows.Add(1, "James", "Dean");
            source.Rows.Add(2, "Bob", "Marley");
            source.Rows.Add(3, "Dana", "International");
            source.Rows.Add(4, "Sara", "");

            TestedComboBox1.ValueMember = "value";
            TestedComboBox1.DropDownHeight = 250;
            TestedComboBox1.DropDownWidth = 500;
            TestedComboBox1.DataSource = source;
            lblLog1.Text = "ShowFilter Value: " + TestedComboBox1.ShowFilter + ".";

            TestedComboBox2.ValueMember = "value";
            TestedComboBox2.DropDownHeight = 250;
            TestedComboBox2.DropDownWidth = 500;
            TestedComboBox2.DataSource = source;
            lblLog2.Text = "ShowFilter Value: " + TestedComboBox2.ShowFilter + ".";

        }

        private void btnChangeShowFilter_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if(TestedComboBox2.ShowFilter == true)
            {
                TestedComboBox2.ShowFilter = false;
                lblLog2.Text = "ShowFilter Value: " + TestedComboBox2.ShowFilter + ".";
            }
            else
            {
                TestedComboBox2.ShowFilter = true;
                lblLog2.Text = "ShowFilter Value: " + TestedComboBox2.ShowFilter + ".";
            }
        }

      

    }
}