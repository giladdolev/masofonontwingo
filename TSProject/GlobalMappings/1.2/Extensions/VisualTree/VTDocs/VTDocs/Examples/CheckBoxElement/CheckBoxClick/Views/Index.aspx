<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Click event" ID="windowView2" LoadAction="CheckBoxClick\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Click" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control is clicked.

            Syntax: public event EventHandler Click"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time one of the CheckBoxs is clicked it's background color is changed.
            'Click' action can be invoked by one of the following ways:
            1. Clicking one of the CheckBoxs using the mouse.
            2. Focus one of the CheckBoxs and pressing the 'Space' key.
             Each invocation of the 'Click' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="365px" Height="50px" Width="505px">
            <vt:CheckBox runat="server" Top="15px" Left="15px" Text="CheckBox1" ID="btnCheckBox1" TabIndex="1" ClickAction="CheckBoxClick\btnCheckBox1_Click"></vt:CheckBox>
            <vt:CheckBox runat="server" Top="15px" Left="175px" Text="CheckBox2" ID="btnCheckBox2" TabIndex="2" ClickAction="CheckBoxClick\btnCheckBox2_Click"></vt:CheckBox>
            <vt:CheckBox runat="server" Top="15px" Left="335px" Text="CheckBox3" ID="btnCheckBox3" TabIndex="3" ClickAction="CheckBoxClick\btnCheckBox3_Click"></vt:CheckBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Height="40px" Top="430px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="520px" Width="360px" ID="txtEventLog"></vt:TextBox>


    </vt:WindowView>
</asp:Content>
