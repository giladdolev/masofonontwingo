<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView View Property" ID="windowView1" LoadAction="ListViewView/Form_Load">

        <vt:Label runat="server" SkinID="Title" Text="View" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets how items are displayed in the control.
            
            Syntax: public View View { get; set; } 

            Value: One of the View values. The default is LargeIcon.
            
            The View property allows you to specify the type of display the ListView control uses to display items. 
            You can set the View property to display each item with large or small icons or display items 
            in a vertical list. " Top="75px" ID="lblDefinition"></vt:Label>

        <vt:ListView runat="server" Left="80px" Height="110px" Width="300px" Text="TestedListView1" ID="TestedListView1" Top="260px">
            <Columns>
                <vt:ColumnHeader runat="server" Text="Column1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column3" width="100"></vt:ColumnHeader>
            </Columns>
               <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="380px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="410px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="View property value of this 'TestedListView2' is initially set to Details" Top="440px" ID="lblExp1"></vt:Label>   
          
         <vt:ListView runat="server" Left="80px" Height="110px" Width="300px" Text="TestedListView2" ID="TestedListView2" Top="480px" View="Details">
            <Columns>
                <vt:ColumnHeader runat="server" Text="Column1" Width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column2" Width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="Column3" Width="100"></vt:ColumnHeader>
            </Columns>
              <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
         </vt:ListView>

          <vt:Label runat="server" SkinID="Log" Top="600px" ID="lblLog2" Width="520px"></vt:Label>
        
        <vt:Button runat="server" Text="Change View value for headers >>" ClickAction="ListViewView\ChangeHeaderVisibility_Click" Top="660px" Width="200px"> </vt:Button>
    </vt:WindowView>
</asp:Content>
