using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicRadioButtonController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnIsChecked_Click(object sender, EventArgs e)
        {
            RadioButtonElement radioButton7 = this.GetVisualElementById<RadioButtonElement>("radioButton7");
            radioButton7.IsChecked = !radioButton7.IsChecked;
        }
    }
}