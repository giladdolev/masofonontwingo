using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPanel1MinSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitterPanelElement TestedSplitContainer1 = this.GetVisualElementById<SplitterPanelElement>("s1");
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog1");

            //TestedSplitContainer1.Panel1MinSize = 200;
            //lbl.Text = "Panel1MinSize = " + TestedSplitContainer1.Panel1MinSize.ToString();
        }

        public void btnChangePanel1MinSize_Click(object sender, EventArgs e)
        {
            SplitterPanelElement splAccessibleRole = this.GetVisualElementById<SplitterPanelElement>("s1");
            SplitterPanelElement splAccessibleRole2 = this.GetVisualElementById<SplitterPanelElement>("s2");

            SplitContainer splitContainer = this.GetVisualElementById<SplitContainer>("splitContainer1");

            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog1");

            if (splitContainer.Panel1MinSize == 600)
            {
                splitContainer.Panel1MinSize = 20;
            }
            else
            {
                splitContainer.Panel1MinSize = 600;
            }



            //if (splAccessibleRole.MinWidth == 100)
            //{
            //    splAccessibleRole.MinWidth = 50;
            //    splAccessibleRole.MinHeight = 50;
            //    splAccessibleRole2.MinHeight = 50;
            //    lbl.Text = "Panel1MinSize = " + splAccessibleRole.MinWidth.ToString();
            //}
            //else
            //{
            //    splAccessibleRole.MinWidth = 100;
            //    splAccessibleRole.MinHeight = 100;
            //    splAccessibleRole2.MinHeight = 100;
            //   splitContainer.Orientation = Orientation.Vertical;
            //    lbl.Text = "Panel1MinSize = " + splAccessibleRole.MinWidth.ToString();
            //}
        }

        public void btnChangePanel2MinSize_Click(object sender, EventArgs e)
        {
            SplitterPanelElement splAccessibleRole = this.GetVisualElementById<SplitterPanelElement>("s1");
            SplitterPanelElement splAccessibleRole2 = this.GetVisualElementById<SplitterPanelElement>("s2");

            SplitContainer splitContainer = this.GetVisualElementById<SplitContainer>("splitContainer1");

            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog1");

            if (splitContainer.Panel2MinSize == 600)
            {
                splitContainer.Panel2MinSize = 20;
            }
            else
            {
                splitContainer.Panel2MinSize = 600;
            }
        }
    }
}