using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ToolBar MaximumSize value: " + TestedToolBar.MaximumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
        }

        public void btnChangeTlbMaximumSize_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedToolBar.MaximumSize == new Size(350, 100))
            {
                TestedToolBar.MaximumSize = new Size(400, 50);
                lblLog1.Text = "ToolBar MaximumSize value: " + TestedToolBar.MaximumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
            }
            else if (TestedToolBar.MaximumSize == new Size(400, 50))
            {
                TestedToolBar.MaximumSize = new Size(370, 150);
                lblLog1.Text = "ToolBar MaximumSize value: " + TestedToolBar.MaximumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
            }
            else
            {
                TestedToolBar.MaximumSize = new Size(350, 100);
                lblLog1.Text = "ToolBar MaximumSize value: " + TestedToolBar.MaximumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
            }

        }

        public void btnSetToZeroTlbMaximumSize_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedToolBar.MaximumSize = new Size(0, 0);
            lblLog1.Text = "ToolBar MaximumSize value: " + TestedToolBar.MaximumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;

        }


    }
}