using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridGridColumnCollectionAddGridColumnController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
           
            GridRow newRow1 = new GridRow();
            GridRow newRow2 = new GridRow();
            TestedGrid.Rows.Add(newRow1);
            TestedGrid.Rows.Add(newRow2);


            lblLog.Text = "Press the bellow buttons...";
        }

      
        //Add CheckBox Column
        public void btnAddCheckBoxColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            GridCheckBoxColumn newCheckBoxColumn = new GridCheckBoxColumn();
            newCheckBoxColumn.HeaderText = "CheckBoxCol";

            TestedGrid.Columns.Add(newCheckBoxColumn);
            lblLog.Text = "CheckBox Column was added.";

            TestedGrid.Refresh();
            TestedGrid.RefreshData();
        }

        //Add ComboBox Column
        public void btnAddComboBoxColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            GridComboBoxColumn newComboBoxColumn = new GridComboBoxColumn();
            newComboBoxColumn.HeaderText = "ComboBoxCol";

            TestedGrid.Columns.Add(newComboBoxColumn);
            lblLog.Text = "ComboBox Column was added.";

            TestedGrid.Refresh();
            TestedGrid.RefreshData();
        }

        //Add Image Column
        public void btnAddImageColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            GridImageColumn newImageColumn = new GridImageColumn();
            newImageColumn.HeaderText = "ImageCol";

            TestedGrid.Columns.Add(newImageColumn);
            lblLog.Text = "Image Column was added.";

            TestedGrid.Refresh();
            TestedGrid.RefreshData();
        }

        //Add Link Column
        public void btnAddLinkColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            GridLinkColumn newLinkColumn = new GridLinkColumn();
            newLinkColumn.HeaderText = "LinkCol";

            TestedGrid.Columns.Add(newLinkColumn);
            lblLog.Text = "Link Column was added.";

            TestedGrid.Refresh();
            TestedGrid.RefreshData();
        }

        //Add TextBox Column
        public void btnAddTextBoxColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            GridTextBoxColumn newTextBoxColumn = new GridTextBoxColumn();
            newTextBoxColumn.HeaderText = "TextBoxCol";

            TestedGrid.Columns.Add(newTextBoxColumn);
            lblLog.Text = "TextBox Column was added.";

            TestedGrid.Refresh();
            TestedGrid.RefreshData();
        }

        //Add Button Column
        public void btnAddButtonColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            GridButtonColumn newButtonColumn = new GridButtonColumn();
            newButtonColumn.HeaderText = "ButtonCol";
           
            TestedGrid.Columns.Add(newButtonColumn);
            lblLog.Text = "Button Column was added.";

            TestedGrid.Refresh();
            TestedGrid.RefreshData();
        }

        //Delete last column
        public void btnRemoveColumn_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGrid.Columns.Count > 0)
            {
                String removedColName =  TestedGrid.Columns[TestedGrid.Columns.Count - 1].HeaderText;
                TestedGrid.Columns.RemoveAt(TestedGrid.Columns.Count - 1);
                lblLog.Text = "Last added column was removed - " + removedColName + ".";

                TestedGrid.Refresh();
                TestedGrid.RefreshData();
            }

        }

    }
}