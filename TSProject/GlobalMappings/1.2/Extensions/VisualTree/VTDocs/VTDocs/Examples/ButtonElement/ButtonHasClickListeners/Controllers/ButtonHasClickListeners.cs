using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonHasClickListenersController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnHasClickListeners_Click(object sender, EventArgs e)
        {
            ButtonElement btnHasClickListeners = this.GetVisualElementById<ButtonElement>("btnHasClickListeners");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            textBox1.Text = "HasClickListeners value:\\r\\n" + btnHasClickListeners.HasClickListeners;
        }

    }
}