using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Height value: " + TestedTextBox.Height + '.';
        }

        private void btnChangeHeight_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTextBox.Height == 80)
            {
                TestedTextBox.Height = 26;
                lblLog.Text = "Height value: " + TestedTextBox.Height + '.';
            }
            else
            {
                TestedTextBox.Height = 80;
                lblLog.Text = "Height value: " + TestedTextBox.Height + '.';
            }
        }

    }
}