<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button ToolTipText Property" ID="windowViewToolTipText" LoadAction="ButtonToolTipText\OnLoad">          

        <vt:Label runat="server" SkinID="Title" Text="ToolTipText" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Represents a small rectangular pop-up window that displays a brief description 
            of a control's purpose when the user rests the pointer on the control.
            
            Syntax: public string ToolTipText { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ToolTipText of 'TestedButton1' and 'TestedButton2' is initially set to: 'MyToolTip1' and 'MyToolTip2',
            You can use the button to change the text values." Top="250px" ID="lblExp1"></vt:Label>

        <vt:Button runat="server" Text="TestedButton1" CssClass="vt-test-btn-1" ToolTipText="MyToolTip1" Top="325px" ID="TestedButton1"></vt:Button>

        <vt:Button runat="server" Text="TestedButton2" CssClass="vt-test-btn-2" ForeColor="Yellow" ToolTipText="MyToolTip2" Top="325px" Left="250px" ID="TestedButton2"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="370px" Height="40px" Width="320px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ToolTipText value >>"  Top="455px" ID="btnChangeToolTipText" Width="180px" ClickAction="ButtonToolTipText\btnChangeToolTipText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>