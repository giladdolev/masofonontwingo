using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabDeselectingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void ChangeSelectedIndex_Click(object sender, EventArgs e)
        {
            TextBoxElement txtExplanation = this.GetVisualElementById<TextBoxElement>("txtExplanation");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabSelectedIndex = this.GetVisualElementById<TabElement>("tabControl1");


            int index;

            if (int.TryParse(textBox1.Text, out index) && index < tabSelectedIndex.TabItems.Count)
            {
                tabSelectedIndex.SelectedIndex = index;
                txtExplanation.Text = "The selected TabIndex is: " + tabSelectedIndex.SelectedIndex;
            }
            else
                txtExplanation.Text = "Enter index number to be selected";

        }

        public void tabControl1_Deselecting(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "Deselecting Event is invoked\\r\\n";

        }
    }
}

