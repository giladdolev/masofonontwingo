﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton GetHashCode Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
      
        <vt:RadioButton runat="server" Text="Tested RadioButton" Top="170px" Left="140px" ID="testedRadioButton" Height="36px" Width="200px"></vt:RadioButton>

        <vt:Button runat="server" Text="Invoke GetHashCode()" Top="45px" Left="140px" ID="btnInvokeGetHashCode" Height="36px" Width="220px" ClickAction="RadioButtonGetHashCode\btnGetHashCode_Click"></vt:Button>

         <vt:TextBox runat="server" Text="" Top="250px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
