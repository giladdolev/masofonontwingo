using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        private void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedPanel1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedPanel1);
            lblLog2.Text = "Font value: " + TestedPanel2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedPanel2);
        }

        private void btnChangePanelFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            if (TestedPanel2.Font.Underline == true)
            {
                TestedPanel2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedPanel2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedPanel2);
            }
            else if (TestedPanel2.Font.Bold == true)
            {
                TestedPanel2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedPanel2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedPanel2);
            }
            else if (TestedPanel2.Font.Italic == true)
            {
                TestedPanel2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedPanel2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedPanel2);
            }
            else
            {
                TestedPanel2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedPanel2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedPanel2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}