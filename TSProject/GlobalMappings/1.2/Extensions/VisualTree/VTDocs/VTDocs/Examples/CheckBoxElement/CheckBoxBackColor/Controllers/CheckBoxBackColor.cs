using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedCheckBox1.BackColor.ToString();

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedCheckBox2.BackColor.ToString();

        }


        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedCheckBox2.BackColor == Color.Orange)
            {
                TestedCheckBox2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedCheckBox2.BackColor.ToString();
            }
            else
            {
                TestedCheckBox2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedCheckBox2.BackColor.ToString();

            }
        }
    }
}