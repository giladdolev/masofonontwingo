<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree ClientRectangle Property" ID="windowView" LoadAction="TreeClientRectangle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientRectangle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets the rectangle that represents the client area of the control.

            Syntax: public Rectangle ClientRectangle { get; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="lblDefinition" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ClientRectangle values of this 'TestedTree' is initially set with 
            Left: 80px, Top: 320px, Width: 140px, Height: 110px" Top="260px"  ID="lblExp1"></vt:Label>     

        <!-- TestedTree -->
        <vt:Tree runat="server" Text="TestedTree" Top="320px" ID="TestedTree">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="TreeItem1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="TreeItem2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="TreeItem3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="445px" Height="40px" Width="350px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientRectangle value >>"  Top="550px" Width="185px" ID="btnChangeTreeClientRectangle" ClickAction="TreeClientRectangle\btnChangeTreeClientRectangle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>