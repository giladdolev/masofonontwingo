<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox SelectionStart Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="SelectionStart Property" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="240px"></vt:Label>

        <vt:TextBox runat="server" Text="Hello World" Top="125px" Left="140px" ID="txtSelectionStart" Height="26px"  Width="150px"></vt:TextBox>

        <vt:Button runat="server" Text="Set" Top="115px" Left="320px" ID="btnSetSelectionStart" Height="36px"  Width="60px" ClickAction="TextBoxSelectionStart\btnSetSelectionStart_Click"></vt:Button>

        <vt:Button runat="server" Text="Get" Top="115px" Left="400px" ID="btnGetSelectionStart" Height="36px"  Width="60px" ClickAction="TextBoxSelectionStart\btnGetSelectionStart_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
