﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class TextBoxEquals : WindowElement
    {

        TextBoxElement _TestedTextBox;

        public TextBoxEquals()
        {
            _TestedTextBox = new TextBoxElement();
        }

        public TextBoxElement TestedTextBox
        {
            get { return this._TestedTextBox; }
            set { this._TestedTextBox = value; }
        }
    }
}