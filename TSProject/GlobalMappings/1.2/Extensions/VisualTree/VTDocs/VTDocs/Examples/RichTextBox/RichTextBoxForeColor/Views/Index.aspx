<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox ForeColor property" ID="windowView1" LoadAction="RichTextBoxForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested RichTextBox1 -->
        <vt:RichTextBox runat="server" Text="RichTextBox" Top="165px" ID="TestedRichTextBox1"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="255px" BorderStyle="None" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of this RichTextBox is initially set to Green" Top="385px" ID="lblExp1"></vt:Label>

        <!-- Tested RichTextBox2 -->
        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="420px" ForeColor="Green" ID="TestedRichTextBox2"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="590px" Width="180px" ID="btnChangeRichTextBoxForeColor" ClickAction="RichTextBoxForeColor\btnChangeRichTextBoxForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
