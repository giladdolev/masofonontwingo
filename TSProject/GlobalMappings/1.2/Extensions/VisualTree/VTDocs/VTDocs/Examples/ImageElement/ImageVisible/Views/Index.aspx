<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image Visible Property" ID="windowView1"  LoadAction="ImageVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Image runat="server" Text="Image" Top="155px" ID="TestedImage1"></vt:Image>
        
        <vt:Label runat="server" SkinID="Log" Top="260px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this Image is initially set to 'false'
             The Image should be invisible" Top="375px" ID="lblExp1"></vt:Label>

        <vt:Image runat="server" Text="TestedImage" Visible="false" Top="440px" ID="TestedImage2"></vt:Image>
        
        <vt:Label runat="server" SkinID="Log" Top="545px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="610px" ID="btnChangeVisible" Width="200px" ClickAction ="ImageVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
