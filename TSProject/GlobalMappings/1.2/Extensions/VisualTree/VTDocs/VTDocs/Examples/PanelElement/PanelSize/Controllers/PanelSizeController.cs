using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedPanel.Size;
        }
        private void btnChangePanelSize_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedPanel.Size == new Size(200, 80)) //Get
            {
                TestedPanel.Size = new Size(300, 40); //Set
                lblLog.Text = "Size value: " + TestedPanel.Size;
            }
            else  
            {
                TestedPanel.Size = new Size(200, 80);
                lblLog.Text = "Size value: " + TestedPanel.Size;
            }
             
        }

    }
}