<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer MaximumSize Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent - MaximumSize is set to 180, 60" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45"></vt:Label>     

        <vt:SplitContainer runat="server" Text="MaximumSize is set to 180, 60" Top="55px" MaximumSize="180, 60" Left="140px" ID="splMaximumSize" Height="70px" Width="200px"></vt:SplitContainer>           

        <vt:Label runat="server" Text="RunTime" Top="160px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:SplitContainer runat="server" Text="" Top="190px" Left="140px" ID="splRuntimeMaximumSize" Height="70px" TabIndex="1" Width="200px" ></vt:SplitContainer>

        <vt:Button runat="server" Text="Change SplitContainer MaximumSize" Top="200px" Left="420px" ID="btnChangeMaximumSize" Height="36px" TabIndex="1" Width="220px" ClickAction="SplitContainerMaximumSize\btnChangeMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Get SplitContainer Size" Top="240px" Left="420px" ID="btnGetSize" Height="36px" TabIndex="1" Width="220px" ClickAction="SplitContainerMaximumSize\btnGetSize_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="370px" Left="110px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        