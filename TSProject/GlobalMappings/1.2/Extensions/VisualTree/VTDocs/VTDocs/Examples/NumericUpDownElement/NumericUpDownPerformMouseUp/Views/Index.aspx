<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown PerformMouseUp() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseUp of 'Tested NumericUpDown'" Top="45px" Left="140px" ID="btnPerformMouseUp" Height="36px" Width="300px" ClickAction="NumericUpDownPerformMouseUp\btnPerformMouseUp_Click"></vt:Button>


        <vt:NumericUpDown runat="server" Text="Tested NumericUpDown" Top="150px" Left="200px" ID="testedNumericUpDown" Height="20px"  Width="100px" MouseUpAction="NumericUpDownPerformMouseUp\testedNumericUpDown_MouseUp"></vt:NumericUpDown>           


    </vt:WindowView>
</asp:Content>
