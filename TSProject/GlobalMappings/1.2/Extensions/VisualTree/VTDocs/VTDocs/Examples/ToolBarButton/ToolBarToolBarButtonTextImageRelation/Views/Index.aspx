<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar ToolBarButton TextImageRelation Property" ID="windowView" LoadAction="ToolBarToolBarButtonTextImageRelation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="TextImageRelation" Left="260px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the position of text and image relative to each other.
            The default value is 'ImageBeforeText'.

            Syntax: public TextImageRelation TextImageRelation { get; set; }

            Values: ImageAboveText/ ImageBeforeText/ Overlay/ TextAboveImage/ TextBeforeImage"
            Top="75px" ID="lblDefinition1">
        </vt:Label>


        <vt:ToolBar runat="server" ID="ToolBar1" Text="file" Height="70" Dock="None" Top="200">
            <vt:ToolBarButton runat="server" ID="ToolBarButton1" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
        </vt:ToolBar>
         <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px" ID="Label2"></vt:Label>
        <vt:Label runat="server" Text="In the following example, the initial TextImageRelation of ToolBarButton is set to 'ImageAboveText'.
            Use the buttons below to change the ToolBarButton Text-Image Relationt."
            Top="380px" ID="Label3">
        </vt:Label>
        <vt:ToolBar runat="server" ID="ToolBar2" Text="file" Height="70" Dock="None" Top="450">
            <vt:ToolBarButton runat="server" ID="ToolBarButton2" Image="Content/Images/Open.png" Text="open" TextImageRelation="ImageAboveText"></vt:ToolBarButton>
        </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="540px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="ImageAboveText >" Top="600px" Left="80px" ID="btnImageAboveText" Height="23px" Width="120px" ClickAction="ToolBarToolBarButtonTextImageRelation\btnImageAboveText_Click"></vt:Button>
        <vt:Button runat="server" Text="ImageBeforeText >" Top="600px" Left="205px" ID="btnImageBeforeText" Height="23px" Width="120px" ClickAction="ToolBarToolBarButtonTextImageRelation\btnImageBeforeText_Click"></vt:Button>
        <vt:Button runat="server" Text="Overlay >" Top="600px" Left="330px" ID="btnOverlay" Height="23px" Width="120px" ClickAction="ToolBarToolBarButtonTextImageRelation\btnOverlay_Click"></vt:Button>
        <vt:Button runat="server" Text="TextAboveImage >" Top="600px" Left="455px" ID="btnTextAboveImage" Height="23px" Width="120px" ClickAction="ToolBarToolBarButtonTextImageRelation\btnTextAboveImage_Click"></vt:Button>
        <vt:Button runat="server" Text="TextBeforeImage >" Top="600px" Left="580px" ID="btnNone" Height="23px" Width="120px" ClickAction="ToolBarToolBarButtonTextImageRelation\btnTextBeforeImage_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
