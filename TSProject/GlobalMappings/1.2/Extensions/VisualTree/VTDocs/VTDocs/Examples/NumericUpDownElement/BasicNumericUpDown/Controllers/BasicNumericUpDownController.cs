using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicNumericUpDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Enter a value...";
        }

        public void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "The numeric value is: " + TestedNumericUpDown1.Value + ".";

        }


        private void btnSetValue_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedNumericUpDown1.Value = 2;
            lblLog.Text = "The value is: " + TestedNumericUpDown1.Value + ".";

        }

    }
}