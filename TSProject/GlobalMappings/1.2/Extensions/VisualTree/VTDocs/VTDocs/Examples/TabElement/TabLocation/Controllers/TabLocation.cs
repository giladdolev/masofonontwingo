using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class tabLocationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Tab Location is: " + TestedTab.Location;
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTab.Location == new Point(80, 285))
            {
                TestedTab.Location = new Point(200, 270);
                lblLog.Text = "Tab Location is: " + TestedTab.Location;
            }
            else
            {
                TestedTab.Location = new Point(80, 285);
                lblLog.Text = "Tab Location is: " + TestedTab.Location;
            }
        }

    }
}