using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class DropDownButtonBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            DropDownButtonElement TestedDropDownButton = this.GetVisualElementById<DropDownButtonElement>("TestedDropDownButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "The value of DropDownButton DropDownArrows property is: " + TestedDropDownButton.DropDownArrows;
        }

        public void menuItem_Click(object sender, EventArgs e)
        {
            ToolBarItem menuItem = (ToolBarItem)sender;
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + menuItem.Text + " button was clicked";
        }

        public void TestedDropDownButton_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nDropDownButton was clicked";
        }

        public void TestedDropDownButtonArrow_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nDropDownButton arrow was clicked";
        }


        public void menuItemArrow_Click(object sender, EventArgs e)
        {
            ToolBarItem menuItem = (ToolBarItem)sender;
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + menuItem.Text + " arrow was clicked";
        }

        public void btnChangeDropDownArrowsproperty_Click(object sender, EventArgs e)
        {
            DropDownButtonElement TestedDropDownButton = this.GetVisualElementById<DropDownButtonElement>("TestedDropDownButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedDropDownButton.DropDownArrows = !TestedDropDownButton.DropDownArrows;
            lblLog.Text = "The value of DropDownButton DropDownArrows property is: " + TestedDropDownButton.DropDownArrows;
        }

        public void btnClearEventLog_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();
            txtEventLog.Text = "Event Log:";
        }
    }
}