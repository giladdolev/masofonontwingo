using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarPixelWidthController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelWidth value: " + TestedToolBar.PixelWidth + '.';
        }

        private void btnChangePixelWidth_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedToolBar.PixelWidth == 400)
            {
                TestedToolBar.PixelWidth = 250;
                lblLog.Text = "PixelWidth value: " + TestedToolBar.PixelWidth + '.';
            }
            else
            {
                TestedToolBar.PixelWidth = 400;
                lblLog.Text = "PixelWidth value: " + TestedToolBar.PixelWidth + '.';
            }
        }
    }
}