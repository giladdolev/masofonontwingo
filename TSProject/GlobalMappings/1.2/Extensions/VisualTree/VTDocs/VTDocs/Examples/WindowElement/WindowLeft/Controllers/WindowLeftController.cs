using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowLeftController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowLeft());
        }
        private WindowLeft ViewModel
        {
            get { return this.GetRootVisualElement() as WindowLeft; }
        }

        public void btnChangeLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            
           if(this.ViewModel.Left == 11)
           {
               this.ViewModel.Left = 20;
               textBox1.Text = this.ViewModel.Left.ToString();
           }
           else
           {
               this.ViewModel.Left = 11;
               textBox1.Text = this.ViewModel.Left.ToString();

           }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowLeft");

            ViewModel.TestedWin = VisualElementHelper.CreateFromView<TestedWindowLeft>("TestedWindowLeft", "Index2", null, argsDictionary, null);

            this.ViewModel.TestedWin.Show();
            //this.ViewModel.TestedWin.Left = 50;
            //this.ViewModel.TestedWin.Top = 400;
            //this.ViewModel.TestedWin.Height = 400;
            //this.ViewModel.TestedWin.Width = 900;


        }
    }
}