using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxBackGroundImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox1 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox1");
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGroupBox1.BackgroundImage == null)
            {
                lblLog1.Text = "No background image";
            }
            else
            {
                lblLog1.Text = "Set with background image";
            }


            if (TestedGroupBox2.BackgroundImage == null)
            {
                lblLog2.Text = "No background image.";
            }
            else
            {
                lblLog2.Text = "Set with background image.";
            }
        }



        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGroupBox2.BackgroundImage == null)
            {
                TestedGroupBox2.BackgroundImage = new UrlReference("/Content/Images/icon.jpg");
                lblLog2.Text = "Set with background image.";
            }
            else
            {
                TestedGroupBox2.BackgroundImage = null;
                lblLog2.Text = "No background image.";

            }
        }
    }
}