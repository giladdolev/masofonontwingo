using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "RichTextBox MaximumSize value: " + TestedRichTextBox.MaximumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;

        }
        public void btnChangeRtfMaximumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.MaximumSize == new Size(150, 90))
            {
                TestedRichTextBox.MaximumSize = new Size(200, 70);
                lblLog.Text = "RichTextBox MaximumSize value: " + TestedRichTextBox.MaximumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;

            }
            else if (TestedRichTextBox.MaximumSize == new Size(200, 70))
            {
                TestedRichTextBox.MaximumSize = new Size(160, 100);
                lblLog.Text = "RichTextBox MaximumSize value: " + TestedRichTextBox.MaximumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;
            }

            else
            {
                TestedRichTextBox.MaximumSize = new Size(150, 90);
                lblLog.Text = "RichTextBox MaximumSize value: " + TestedRichTextBox.MaximumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;
            }

        }

        public void btnSetToZeroRtfMaximumSize_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedRichTextBox.MaximumSize = new Size(0, 0);
            lblLog.Text = "RichTextBox MaximumSize value: " + TestedRichTextBox.MaximumSize + "\\r\\nRichTextBox Size value: " + TestedRichTextBox.Size;

        }

    }
}