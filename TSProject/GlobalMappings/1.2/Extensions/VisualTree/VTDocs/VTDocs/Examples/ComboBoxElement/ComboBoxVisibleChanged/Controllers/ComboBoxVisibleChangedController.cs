using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("Item2");
            TestedComboBox.Items.Add("Item3");

            lblLog.Text = "ComboBox Visible value: " + TestedComboBox.Visible;
        }

        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");

            TestedComboBox.Visible = !TestedComboBox.Visible;
        }

        public void TestedComboBox_VisibleChanged(object sender, EventArgs e)
        {

            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtEventLog != null)
                txtEventLog.Text += "\\r\\nComboBox VisibleChanged event is invoked";
            if (lblLog != null)
                lblLog.Text = "ComboBox Visible value: " + TestedComboBox.Visible;
        }      

    }
}