using MvcApplication9.Models;
using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxComboBoxColumnCollectionAddComboBoxColumnController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new ComboBoxComboBoxColumnCollectionAddComboBoxColumn());
        }

        private ComboBoxComboBoxColumnCollectionAddComboBoxColumn ViewModel
        {
            get { return this.GetRootVisualElement() as ComboBoxComboBoxColumnCollectionAddComboBoxColumn; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            DataTable source = new DataTable();
            source.Columns.Add("value", typeof(int));
            source.Columns.Add("text", typeof(string));
            source.Columns.Add("Field3", typeof(string));
            source.Rows.Add(1, "James", "Dean");
            source.Rows.Add(2, "Bob", "Marley");
            source.Rows.Add(3, "Dana", "International");

            TestedComboBox.ValueMember = "value";
            TestedComboBox.DropDownHeight = 250;
            TestedComboBox.DropDownWidth = 500;
            TestedComboBox.DataSource = source;

            lblLog.Text = "Press the bellow buttons...";
        }


        //Add Integer Column
        public void btnAddIntegerColumn_Click(object sender, EventArgs e)
        {

            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            string name = "value" + ViewModel.intCounterProperty++;

            ComboboxColumn newComboboxColumn = new ComboboxColumn(name, name, typeof(int));

            TestedComboBox.Columns.Add(newComboboxColumn);
            lblLog.Text = "Integer Column was added.";

            TestedComboBox.Refresh();
        }

        //Add String Column
        public void btnAddStringColumn_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            string name = "text" + ViewModel.stringCounterProperty++;

            ComboboxColumn newComboboxColumn = new ComboboxColumn(name, name, typeof(string));

            TestedComboBox.Columns.Add(newComboboxColumn);
            lblLog.Text = "String Column was added.";

            TestedComboBox.Refresh();
        }

        //Delete last column
        public void btnRemoveColumn_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedComboBox.Columns.Count > 0)
            {
                String removedColName = TestedComboBox.Columns[TestedComboBox.Columns.Count - 1].HeaderText;
                TestedComboBox.Columns.RemoveAt(TestedComboBox.Columns.Count - 1);
                lblLog.Text = "Last added column was removed - " + removedColName + ".";

                TestedComboBox.Refresh();
            }

        }

    }
}