using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxCheckAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCheckAlign_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkCheckAlignChange = this.GetVisualElementById<CheckBoxElement>("chkCheckAlignChange");

            if (chkCheckAlignChange.CheckAlign == ContentAlignment.BottomCenter)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.BottomLeft;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.BottomLeft)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.BottomRight;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.BottomRight)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.MiddleCenter;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.MiddleCenter)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.MiddleLeft;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.MiddleLeft)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.MiddleRight;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.MiddleRight)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.TopCenter;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.TopCenter)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.TopCenter;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.TopCenter)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.TopRight;
            }
            else if (chkCheckAlignChange.CheckAlign == ContentAlignment.TopRight)
            {
                chkCheckAlignChange.CheckAlign = ContentAlignment.BottomCenter;
            }
        }

    }
}