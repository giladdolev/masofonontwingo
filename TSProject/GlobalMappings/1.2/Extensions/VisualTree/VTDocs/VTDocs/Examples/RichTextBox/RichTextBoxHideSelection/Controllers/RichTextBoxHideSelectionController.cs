using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxHideSelectionController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
      
        public void btnHideSelection_Click(object sender, EventArgs e)
        {
            RichTextBox rtfHideSelection = this.GetVisualElementById<RichTextBox>("rtfHideSelection");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (rtfHideSelection.HideSelection == true)
            {
                rtfHideSelection.HideSelection = false;
                textBox1.Text = "HideSelection : " + rtfHideSelection.HideSelection;
            }
            else
            {
                rtfHideSelection.HideSelection = true;
                textBox1.Text = "HideSelection : " + rtfHideSelection.HideSelection;
            }
        }


    }
}