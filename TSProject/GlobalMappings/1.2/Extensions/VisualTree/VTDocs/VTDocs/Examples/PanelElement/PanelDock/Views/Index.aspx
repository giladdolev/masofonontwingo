<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Dock Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
       
         <vt:Label runat="server" Text="Initialized component : The Panel is initialliy docked to Fill" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 

        <vt:Panel runat="server" Text="Panel" Top="165px" Left="140px" ID="pnlDock" Dock="Fill" Height="150px" TabIndex="1" Width="200px"></vt:Panel>

        <vt:Label runat="server" Text="Run Time : Click 'Change Dock style' button" Top="170px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 

        <vt:Button runat="server" Text="Change Dock style" Top="210px" Left="200px" ID="btnDock" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="PanelDock\btnDock_Click"></vt:Button>
        
        <vt:TextBox runat="server" Text="" Top="325px" Left="200px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"></vt:TextBox>

        <vt:Label runat="server" Text="Click each button to change dock style for each form" Top="700px" Left="140px" ID="Label3" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 

        <vt:Button runat="server" Text="Open Form2" Top="750px" Left="230px" ID="btnOpenForm2" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="PanelDock\btnOpenForm2_Click"></vt:Button>
        <vt:Button runat="server" Text="Open Form3" Top="800px" Left="230px" ID="btnOpenForm3" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="PanelDock\btnOpenForm3_Click"></vt:Button>
        <vt:Button runat="server" Text="Open Form4" Top="850px" Left="230px" ID="btnOpenForm4" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="PanelDock\btnOpenForm4_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
        