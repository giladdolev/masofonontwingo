using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "RadioButton MinimumSize value: " + TestedRadioButton.MinimumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
        }

        public void btnChangeRdbMinimumSize_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedRadioButton.MinimumSize == new Size(140, 50))
            {
                TestedRadioButton.MinimumSize = new Size(90, 80);
                lblLog1.Text = "RadioButton MinimumSize value: " + TestedRadioButton.MinimumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
            }
            else if (TestedRadioButton.MinimumSize == new Size(90, 80))
            {
                TestedRadioButton.MinimumSize = new Size(60, 40);
                lblLog1.Text = "RadioButton MinimumSize value: " + TestedRadioButton.MinimumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
            }
            else
            {
                TestedRadioButton.MinimumSize = new Size(140, 50);
                lblLog1.Text = "RadioButton MinimumSize value: " + TestedRadioButton.MinimumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
            }

        }

        public void btnSetToZeroRdbMinimumSize_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedRadioButton.MinimumSize = new Size(0, 0);
            lblLog1.Text = "RadioButton MinimumSize value: " + TestedRadioButton.MinimumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;

        }


    }
}