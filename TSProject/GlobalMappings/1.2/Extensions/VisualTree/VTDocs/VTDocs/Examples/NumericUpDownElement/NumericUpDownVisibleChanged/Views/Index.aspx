<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown VisibleChanged event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

      <vt:button runat="server" Text="Change NumericUpDown Visibility" Top="45px" Left="350px" ID="btnChangeVisibility" Height="36px"  Width="300px"  ClickAction ="NumericUpDownVisibleChanged\btnChangeVisibility_Click" ></vt:button>

        <vt:NumericUpDown runat="server" Text="Tested NumericUpDown" Top="45px" Left="140px" ID="testedNumericUpDown" Height="20px"  Width="100px" VisibleChangedAction ="NumericUpDownVisibleChanged\testedNumericUpDown_VisibleChanged"></vt:NumericUpDown>          

        <vt:TextBox runat="server" Text="" Top="120px" Left="140px" Multiline="true" ID="txtEventTrack" Height="60px" Width="250px"></vt:TextBox>
          
    </vt:WindowView>
</asp:Content>
