using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "RadioButton MaximumSize value: " + TestedRadioButton.MaximumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
        }

        public void btnChangeRdbMaximumSize_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedRadioButton.MaximumSize == new Size(90, 110))
            {
                TestedRadioButton.MaximumSize = new Size(130, 60);
                lblLog1.Text = "RadioButton MaximumSize value: " + TestedRadioButton.MaximumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
            }
            else if (TestedRadioButton.MaximumSize == new Size(130, 60))
            {
                TestedRadioButton.MaximumSize = new Size(100, 70);
                lblLog1.Text = "RadioButton MaximumSize value: " + TestedRadioButton.MaximumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
            }
            else
            {
                TestedRadioButton.MaximumSize = new Size(90, 110);
                lblLog1.Text = "RadioButton MaximumSize value: " + TestedRadioButton.MaximumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;
            }

        }

        public void btnSetToZeroRdbMaximumSize_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedRadioButton.MaximumSize = new Size(0, 0);
            lblLog1.Text = "RadioButton MaximumSize value: " + TestedRadioButton.MaximumSize + "\\r\\nRadioButton Size value: " + TestedRadioButton.Size;

        }


    }
}