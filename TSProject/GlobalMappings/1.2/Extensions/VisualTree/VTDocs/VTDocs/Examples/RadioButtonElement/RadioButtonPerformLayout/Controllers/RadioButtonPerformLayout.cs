using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            RadioButtonElement testedRadioButton = this.GetVisualElementById<RadioButtonElement>("testedRadioButton");
            LayoutEventArgs args = new LayoutEventArgs(testedRadioButton, "Some String");

            testedRadioButton.PerformLayout(args);
        }

        public void testedRadioButton_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRadioButton Layout event method is invoked");
        }

    }
}