using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //ImageAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested Image bounds are set to: \\r\\nLeft  " + TestedImage.Left + ", Top  " + TestedImage.Top + ", Width  " + TestedImage.Width + ", Height  " + TestedImage.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            if (TestedImage.Left == 100)
            {
                TestedImage.SetBounds(80, 300, 150, 90);
                lblLog.Text = "The Tested Image bounds are set to: \\r\\nLeft  " + TestedImage.Left + ", Top  " + TestedImage.Top + ", Width  " + TestedImage.Width + ", Height  " + TestedImage.Height;

            }
            else
            {
                TestedImage.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested Image bounds are set to: \\r\\nLeft  " + TestedImage.Left + ", Top  " + TestedImage.Top + ", Width  " + TestedImage.Width + ", Height  " + TestedImage.Height;
            }
        }

    }
}