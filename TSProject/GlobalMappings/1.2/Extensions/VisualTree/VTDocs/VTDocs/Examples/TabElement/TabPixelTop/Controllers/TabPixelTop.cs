using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabPixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             lblLog.Text = "PixelTop Value: " + TestedTab.PixelTop + '.';

         }
         public void btnChangePixelTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
             if (TestedTab.PixelTop == 510)
             {
                 TestedTab.PixelTop = 300;
                 lblLog.Text = "PixelTop Value: " + TestedTab.PixelTop + '.';

             }
             else
             {
                 TestedTab.PixelTop = 510;
                 lblLog.Text = "PixelTop Value: " + TestedTab.PixelTop + '.';
             }

         }

    }
}