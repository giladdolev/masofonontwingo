using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPerformClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformClick_Click(object sender, EventArgs e)
        {

            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");

            ButtonElement btnPerformClick = this.GetVisualElementById<ButtonElement>("btnPerformClick");

            btnTestedButton.PerformClick(e);
        }

        public void btnTestedButton_Click(object sender, EventArgs e)
        {

            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");

            MessageBox.Show("TestedButton Click event method is invoked");
        }

    }
}