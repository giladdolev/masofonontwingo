<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab FindForm() Method" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Tab runat="server" Text="Tested Tab" Top="100px" Left="140px" ID="tabFindForm" Height="150px" Width="200px" >           <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px">
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px">
                </vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Button runat="server" Text="Invoke Tab FindForm() method" Top="150px" Left="420px" ID="btnInvokeFindForm" Height="36px" Width="200px" ClickAction="TabFindForm\btnInvokeFindForm_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="300px" Left="140px" ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        