using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Collections.Generic;
namespace HelloWorldApp
{
    public class GridDataSourceController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void Form_load(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid1.TextButtonList.Add("Location");
            TestedGrid1.AddToComboBoxList("Combo", "male|1,female|2");

            SetDataSource();
           
            lblLog.Text = "Grid1 DataSource was loaded.";
        }

        private void btnEmptyDataSource_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            TestedGrid1.DataSource = null;

            lblLog.Text = "Grid1 DataSource has been emptied.";

        }

        private void SetDataSource()
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");

            DataTable source = new DataTable();
            source.Columns.Add("Gizmox", typeof(string));
            source.Columns.Add("Location", typeof(string));
            source.Columns.Add("Check", typeof(bool));
            source.Columns.Add("DateTime", typeof(DateTime));
            source.Columns.Add("Combo", typeof(string));

            source.Rows.Add("Bart", @"c:\dir\file", true, "3/12/2013 12:00:00 AM", "male");
            source.Rows.Add("Homer", @"c:\dir\file", false, "6/04/2014 08:30:00 AM", "female");
            source.Rows.Add("Lisa", @"c:\dir\file", true, "6/10/2010 12:00:00 AM", "male");

            TestedGrid1.DataSource = source;
        }

        public void btnSetDataSource_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            SetDataSource();

            lblLog.Text = "Grid1 DataSource was set.";

        }

    
        public void btnSetTestedGrid1ToTestedGrid2_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid2.DataSource = TestedGrid1.DataSource;

            lblLog.Text = "TestedGrid1.DataSource was set to TestedGrid2.DataSource.";
        }

        public void btnSetGrid1TextBoxCell_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (String.Compare(TestedGrid1.Rows[0].Cells[1].Value.ToString(), "c:\\dir\\file") == 0)
                TestedGrid1.Rows[0].Cells[1].Value = "NewText";
            else
                TestedGrid1.Rows[0].Cells[1].Value = "c:\\dir\\file";

            lblLog.Text = "Grid1 TextBox Cell (0,1) was set";
        }

        public void btnSetGrid1CheckBoxCell_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            if (String.Compare(TestedGrid1.Rows[0].Cells[2].Value.ToString(),"True") == 0)
                TestedGrid1.Rows[0].Cells[2].Value = "False";
            else
                TestedGrid1.Rows[0].Cells[2].Value = "True";

            lblLog.Text = "Grid1 CheckBox Cell (0,2) was set";
        }

        public void btnSetGrid1ComboBoxCell_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
           
            if (String.Compare(TestedGrid1.Rows[2].Cells[4].Value.ToString(), "male") == 0)
                TestedGrid1.Rows[2].Cells[4].Value = "female";
            else
                TestedGrid1.Rows[2].Cells[4].Value = "male";

            lblLog.Text = "Grid1 ComboBox Cell (2,4) was set";
        }

        private void TestedGrid1_CellEndEdit(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Grid1 was edited.";
        }

        private void TestedGrid2_CellEndEdit(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Grid2 was edited.";
        }
    }
}
