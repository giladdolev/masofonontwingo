using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeSearchPanelController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            DataTable source = new DataTable();
            source.Columns.Add("ID", typeof(string));
            source.Columns.Add("ParentID", typeof(string));
            source.Columns.Add("Department", typeof(string));
            source.Columns.Add("Location", typeof(string));
            source.Columns.Add("Phone", typeof(string));

            source.Rows.Add("1", "", "Corporate Headquarters", "Monterey", "(408) 555-1234");
            source.Rows.Add("2", "1", "Sales and Marketing", "San Francisco", "(415) 555-1234");
            source.Rows.Add("3", "2", "Field Office: Canada", "Toronto", "(416) 677-1000");
            source.Rows.Add("4", "2", "Field Office: East Coast", "Boston", "(617) 555-4234");
            source.Rows.Add("5", "", "Finance", "Monterey", "(408) 555-1234");
            source.Rows.Add("6", "", "Engineering", "Monterey", "(408) 555-1234");
            source.Rows.Add("7", "6", "Software Products Div.", "Monterey", "(408) 555-1234");
            source.Rows.Add("8", "6", "Software Development", "Monterey", "(408) 555-1234");

            TestedTree.ParentFieldName = "ParentID";
            TestedTree.KeyFieldName = "ID";
            TestedTree.DataSource = source;


            lblLog.Text = "Tree SearchPanel value: " + TestedTree.SearchPanel;

        }

        public void btnChangeTreeSearchPanel_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");

            TestedTree.SearchPanel =! TestedTree.SearchPanel;
            lblLog.Text = "Tree SearchPanel value: " + TestedTree.SearchPanel;


        }

    }
}