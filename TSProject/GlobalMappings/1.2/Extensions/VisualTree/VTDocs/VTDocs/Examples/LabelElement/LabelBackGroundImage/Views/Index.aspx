<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label BackgroundImage property" ID="windowView2" LoadAction="LabelBackgroundImage\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="BackgroundImage" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the image rendered on the background of the control.
            
            public override Image BackgroundImage { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
         
        <vt:Label runat="server" SkinID="TestedLabel" Text="Label" Top="185px" ID="TestedLabel1"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="215px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="BackgroundImage property of this 'TestedLabel' is initially set to icon.jpg" Top="330px"  ID="lblExp1"></vt:Label>     

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Top="380px" ID="TestedLabel2" BackgroundImage="/Content/Elements/Image.png" Height="45px" Width="75px"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="440px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change BackgroundImage value >>" Top="505px" ID="btnBackgroundImage" Width="200px" ClickAction="LabelBackgroundImage\btnChangeBackgroundImage_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
