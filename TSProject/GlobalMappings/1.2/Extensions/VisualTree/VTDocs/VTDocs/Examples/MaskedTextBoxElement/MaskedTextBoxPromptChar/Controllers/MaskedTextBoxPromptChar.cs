using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MaskedTextBoxPromptCharController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            MaskedTextBoxElement MaskDefaultPrompt = this.GetVisualElementById<MaskedTextBoxElement>("MaskDefaultPrompt");
            MaskedTextBoxElement Mask1 = this.GetVisualElementById<MaskedTextBoxElement>("Mask1");
            MaskedTextBoxElement Mask2 = this.GetVisualElementById<MaskedTextBoxElement>("Mask2");
            MaskedTextBoxElement Mask3 = this.GetVisualElementById<MaskedTextBoxElement>("Mask3");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "PromptChar value: " + MaskDefaultPrompt.PromptChar + "\\r\\nMask value: " + MaskDefaultPrompt.Mask;


            lblLog2.Text = "Mask1 - PromptChar value: " + Mask1.PromptChar + ", Mask value: " + Mask1.Mask + ", Text value: " + Mask1.Text + "\\r\\nMask2 - PromptChar value: " + Mask2.PromptChar +  ", Mask value: " + Mask2.Mask + ", Text value: " + Mask2.Text + "\\r\\nMask3 - PromptChar value: " + Mask3.PromptChar + ", Mask value: " + Mask3.Mask + ", Text value: " + Mask3.Text;
            
        }

        public void Mask1_TextChanged(object sender, EventArgs e)
        {
            PrintToLog();
        }

        public void Mask2_TextChanged(object sender, EventArgs e)
        {
            PrintToLog();
        }

        public void Mask3_TextChanged(object sender, EventArgs e)
        {
            PrintToLog();
        }

        public void btnSetMask1Text_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement Mask1 = this.GetVisualElementById<MaskedTextBoxElement>("Mask1");

            Mask1.Text = "1";
            PrintToLog();
        }

        public void btnSetMask2Text_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement Mask2 = this.GetVisualElementById<MaskedTextBoxElement>("Mask2");

            Mask2.Text = "12";
            PrintToLog();
        }

        public void btnSetMask3Text_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement Mask3 = this.GetVisualElementById<MaskedTextBoxElement>("Mask3");

            Mask3.Text = "12ab";
            PrintToLog();
        }

        private void PrintToLog()
        {
            MaskedTextBoxElement Mask1 = this.GetVisualElementById<MaskedTextBoxElement>("Mask1");
            MaskedTextBoxElement Mask2 = this.GetVisualElementById<MaskedTextBoxElement>("Mask2");
            MaskedTextBoxElement Mask3 = this.GetVisualElementById<MaskedTextBoxElement>("Mask3");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "Mask1 - PromptChar value: " + Mask1.PromptChar + ", Mask value: " + Mask1.Mask + ", Text value: " + Mask1.Text + "\\r\\nMask2 - PromptChar value: " + Mask2.PromptChar + ", Mask value: " + Mask2.Mask + ", Text value: " + Mask2.Text + "\\r\\nMask3 - PromptChar value: " + Mask3.PromptChar + ", Mask value: " + Mask3.Mask + ", Text value: " + Mask3.Text;

        }
        public void btnChangePromptChar_Click(object sender, EventArgs e) 
        {
            MaskedTextBoxElement Mask1 = this.GetVisualElementById<MaskedTextBoxElement>("Mask1");
            MaskedTextBoxElement Mask2 = this.GetVisualElementById<MaskedTextBoxElement>("Mask2");
            MaskedTextBoxElement Mask3 = this.GetVisualElementById<MaskedTextBoxElement>("Mask3");

            if (Mask1.PromptChar == '%')
            {
                Mask1.PromptChar = '*';
                Mask2.PromptChar = '*';
                Mask3.PromptChar = '*';
            }
            else
            {
                Mask1.PromptChar = '%';
                Mask2.PromptChar = '%';
                Mask3.PromptChar = '%';
            }
            PrintToLog();

        }

    
    }
}