<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

     <vt:WindowView runat="server" Text="Tab SelectedTab Property" ID="windowView1" LoadAction="TabSelectedTab\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="250px" Text="SelectedTab" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the currently selected tab page.

            Syntax: public TabPage SelectedTab { get; set; }
            If no tab page is selected, the value is null. By default first tab page of TabElement is selected."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Tab1 -->
        <vt:Tab runat="server" Text="Tab" Top="165px" ID="TestedTab1" SelectedIndexChangedAction="TabSelectedTab\TestedTab1_SelectedIndexChanged">
             <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tab1Page1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tab1Page2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tab1Page3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>


        <vt:Label runat="server" SkinID="Log" Top="275px" ID="lblLog1"></vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="320px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="SelectedTab property of this Tab is initially set to of 'tabPage2'
             You can change the SelectedTab property by switching tab pages manually 
             or by selecting tab page from ComboBox" Top="360px" ID="lblExp1"></vt:Label>

        <!-- Tested Tab2 -->
        <vt:Tab runat="server" Text="TestedTab" SelectedTabID="tab2Page2" Top="440px" ID="TestedTab2" SelectedIndexChangedAction="TabSelectedTab\TestedTab2_SelectedIndexChanged">
             <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tab2Page1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tab2Page2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tab2Page3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="550px" ID="lblLog2"></vt:Label>

        <vt:Label runat="server" Left="80px" Top="600px" AutoSize="true" ID="Label1" Text="Change SelectedTab :" ></vt:Label>

        <vt:ComboBox runat="server" Text="Select..." Top="630px" ID="cmbInput" SelectedIndexChangedAction="TabSelectedTab\cmbInput_SelectedIndexChanged">
              <Items>
                <vt:ListItem runat="server" Text="tabPage1"></vt:ListItem>
                <vt:ListItem runat="server" Text="tabPage2"></vt:ListItem>
                <vt:ListItem runat="server" Text="tabPage3"></vt:ListItem>           
            </Items>
         </vt:ComboBox>

 
    </vt:WindowView>
</asp:Content>
