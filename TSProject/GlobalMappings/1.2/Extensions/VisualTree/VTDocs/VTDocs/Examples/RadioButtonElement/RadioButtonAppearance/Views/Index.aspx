<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton Appearance Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:RadioButton runat="server" Text="Appearance is set to 'cc3D'" BackColor="WhiteSmoke" Top="55px" Appearance="cc3D" Left="140px" ID="rdoAppearance" Height="36px" Width="200px"></vt:RadioButton>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:RadioButton runat="server" Text="RuntimeAppearance" Top="165px" BackColor="WhiteSmoke" Left="140px" ID="rdoRuntimeAppearance" Height="36px" TabIndex="1" Width="200px" ></vt:RadioButton>

        <vt:Button runat="server" Text="Change RadioButton Appearance" Top="195px" Left="420px" ID="btnChangeAppearance" Height="36px" TabIndex="1" Width="200px" ClickAction="RadioButtonAppearance\btnChangeAppearance_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="275px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        