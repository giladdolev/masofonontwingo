<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Visible Property" ID="windowView1"  LoadAction="CheckBoxVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:CheckBox runat="server" Text="CheckBox" Top="175px" ID="TestedCheckBox1"></vt:CheckBox>
        
        <vt:Label runat="server" SkinID="Log" Top="215px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this CheckBox is initially set to 'false'
             The CheckBox should be invisible" Top="330px" ID="lblExp1"></vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox" Visible="false" Top="405px" ID="TestedCheckBox2"></vt:CheckBox>
        
        <vt:Label runat="server" SkinID="Log" Top="445px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change Visible value >>" Top="510px" ID="btnChangeVisible" Width="200px" ClickAction ="CheckBoxVisible\btnChangeVisible_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
