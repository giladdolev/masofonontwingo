<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Width Property" ID="windowView1" LoadAction="LabelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="Width" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels.
            
            Syntax: public int Width { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this Label is initially set to: 80px" Top="235px" ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Top="295px" ID="TestedLabel"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="325px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Width value >>" Top="390px" ID="btnChangeWidth" Width="180px" ClickAction="LabelWidth\btnChangeWidth_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
