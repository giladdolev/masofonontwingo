 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumnCollection Add Method (GridColumn)" ID="windowView2" LoadAction="GridGridColumnCollectionAddGridColumn\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Add(GridColumn)" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Adds the given column to the collection.

            Syntax: public virtual int Add(GridColumn column)
            Parameters: column - The column to add.

            Return Value: Int - The index of the column ." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can press the bellow buttons to add and remove any type
             of GridColumn to this Grid" 
            Top="255px" ID="lblExp2"></vt:Label>

        <vt:Grid runat="server" Height="145px" CellContentClickAction="GridGridColumnCollectionAddGridColumn\TestedGrid_CellContentClick" Top="320px" Width="590px" ID="TestedGrid">
            <Columns>
                <vt:GridButtonColumn  runat="server" ID="GridCol1" HeaderText="ButtonCol" Height="20px" Width="85px"></vt:GridButtonColumn>
                
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol2" HeaderText="CheckBoxCol" Height="20px" Width="85px"></vt:GridCheckBoxColumn>
              
                  <vt:GridComboBoxColumn  runat="server" ID="GridCol3" HeaderText="ComboBoxCol" Height="20px" Width="85px"></vt:GridComboBoxColumn>
               
                 <vt:GridImageColumn  runat="server" ID="GridCol4" HeaderText="ImageCol" Height="20px" Width="85px"></vt:GridImageColumn>
               
                 <vt:GridLinkColumn  runat="server" ID="GridCol5" HeaderText="LinkCol" Height="20px" Width="85px"></vt:GridLinkColumn>
                
                <vt:GridTextBoxColumn  runat="server" ID="GridCol6" HeaderText="TextBoxCol" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Width="350px" ID="lblLog" Top="480px"></vt:Label>

        <vt:Button runat="server" Text="Add Button Column >>" Width="160px" Top="545px" ID="btnAddButtonColumn" ClickAction="GridGridColumnCollectionAddGridColumn\btnAddButtonColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Add CheckBox Column >>" Width="160px" Left="270px" Top="545px" ID="btnAddCheckBoxColumn" ClickAction="GridGridColumnCollectionAddGridColumn\btnAddCheckBoxColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Add ComboBox Column >>" Width="160px" Left="460px" Top="545px" ID="btnAddComboBoxColumn" ClickAction="GridGridColumnCollectionAddGridColumn\btnAddComboBoxColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Image Column >>" Width="160px" Top="590px" ID="btnAddImageColumn" ClickAction="GridGridColumnCollectionAddGridColumn\btnAddImageColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Link Column >>" Width="160px" Left="270px" Top="590px" ID="btnAddLinkColumn" ClickAction="GridGridColumnCollectionAddGridColumn\btnAddLinkColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Add TextBox Column >>" Width="160px" Left="460px" Top="590px" ID="btnAddTextBoxColumn" ClickAction="GridGridColumnCollectionAddGridColumn\btnAddTextBoxColumn_Click"></vt:Button>

        <vt:Button runat="server" Text="Remove Last Column >>" BackColor="Green" Width="160px" Left="270px" Top="650px" ID="btnRemoveColumn" ClickAction="GridGridColumnCollectionAddGridColumn\btnRemoveColumn_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>