using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformDragEnterController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformDragEnter_Click(object sender, EventArgs e)
        {
            DragEventArgs args = new DragEventArgs();
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");

            testedCheckBox.PerformDragEnter(args);
        }

        public void testedCheckBox_DragEnter(object sender, EventArgs e)
        {
            MessageBox.Show("TestedCheckBox DragEnter event method is invoked");
        }

    }
}