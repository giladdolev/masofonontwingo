 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel ToString Method" ID="windowView2" LoadAction="PanelToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a string representation for this panel control.

            Syntax: public override string ToString()" 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this Panel." 
            Top="250px" ID="lblExp2"></vt:Label>

        <vt:Panel runat="server" Text="TestedPanel" Top="315px" ID="TestedPanel"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="410px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="515px" ID="btnToString" ClickAction="PanelToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>