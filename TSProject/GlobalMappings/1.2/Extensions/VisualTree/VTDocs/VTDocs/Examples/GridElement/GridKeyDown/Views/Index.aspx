﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid KeyDown event" LoadAction="GridKeyDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Top="15px" Text="KeyDown" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occures when a key is pressed while the control has focus.
            
            syntax: public event EventHandler(KeyEventArgs)KeyDown."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="175px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can edit the cell content by selecting it with the mouse, 
            and start typing keys. Each invocation of the 'KeyDown' event should add a line in the 'Event Log'."
            Top="225px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="305px" ID="TestedGrid" CellkeyDownAction="GridKeyDown\TestedGrid_KeyDown">
            <Columns>
                <vt:GridTextBoxColumn runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="440px" Width ="500px" Height="40px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Width ="500px" Height="140px" Top="500px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
