<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TreeItem Checked Property" ID="windowView2" LoadAction="TreeItemChecked\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Checked" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the tree node is in a checked state.
            
            Syntax: public bool Checked { get; set; }
            Property Value: 'true' if the tree node is in a checked state; otherwise, 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In this Tree, Node1 Checked property is initially set to 'true'. Use the buttons below to change Node1
         and child Node00 Checked property or manually check this Tree CheckBoxes" Top="250px" ID="lblExp1"></vt:Label>

        <!-- TestedTree -->
        <vt:Tree runat="server" Text="TestedTree" Top="310px" ID="TestedTree" CheckBoxes="true" AfterCheckAction="TreeItemChecked\TestedTree_AfterCheck">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1">
                    <Items>
                        <vt:TreeItem runat="server" Text="Node00" ID="Tree1Item11" Checked="true"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2" Checked="true"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="435px" Height="65px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Node1 Checked Value >>" Top="540px" Width="230px" ID="btnChangeNode1Checked" ClickAction="TreeItemChecked\btnChangeNode1Checked_Click"></vt:Button>

         <vt:Button runat="server" Text="Change Child Node00 Checked Value >>" Top="540px" Width="230px" Left="350px" ID="btnChangeChildNode00Checked" ClickAction="TreeItemChecked\btnChangeChildNode00Checked_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>


