﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Text="Grid RowHeaderMouseRightClick event" ID="windowView1" LoadAction="GridRowHeaderMouseRightClick\Window_Load">

        <vt:Label runat="server" SkinID="Title" Text="Grid RowHeaderMouseRightClick" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the user mouse right click on the row header.

            Syntax: public event EventHandler RowHeaderMouseRightClick"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can mouse right click on the row header and see how the event works."
            Top="200px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="240px" ID="TestedGrid" RowHeaderMouseRightClickAction="GridRowHeaderMouseRightClick\Grid_RowHeaderClick">
            <Columns>
                <vt:GridColumn ID="name" runat="server" HeaderText="Name" DataMember="name" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="email" runat="server" HeaderText="Email" DataMember="email" Width="80"></vt:GridColumn>
            </Columns>
        </vt:Grid>      


        <vt:Label runat="server" SkinID="Log" Top="400px" Width="380px" ID="lblLog1"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="450px" ID="txtEventLog"></vt:TextBox>
      

    </vt:WindowView>

</asp:Content>
