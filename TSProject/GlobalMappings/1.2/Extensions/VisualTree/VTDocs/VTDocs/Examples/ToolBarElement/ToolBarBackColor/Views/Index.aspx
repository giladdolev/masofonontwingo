<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar BackColor Property" ID="windowView1" LoadAction="ToolBarBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color for the ToolBar.
            
            public virtual Color BackColor { get; set; }
            
            The default is the value of the DefaultBackColor property." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:ToolBar runat="server" Top="200px" ID="TestedToolBar1">
        <vt:ToolBarButton runat="server" id="ToolBarItem1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="250px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="305px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this ToolBar is initially set to Gray." Top="355px"  ID="lblExp1"></vt:Label>     
        
        <vt:ToolBar runat="server" BackColor ="Gray" Top="405px" ID="TestedToolBar2">
        <vt:ToolBarButton runat="server" id="ToolBarButton1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarButton2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="455px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Top="520px" ID="btnChangeBackColor" Width="185px" ClickAction="ToolBarBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
