using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelResizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");


            if (testedPanel.Size == new Size(200, 70))
            {
                testedPanel.Size = new Size(300, 100);
                testedPanel.Text = "Set to Height: " + testedPanel.Height + ", Width: " + testedPanel.Width;
            }
            else
            {
                testedPanel.Size = new Size(200, 70);
                testedPanel.Text = "Back to Height: " + testedPanel.Height + ", Width: " + testedPanel.Width;
            }
        }

        private void testedPanel_Resize(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "Panel Resize event is invoked\\r\\n";
        }
      
    }
}