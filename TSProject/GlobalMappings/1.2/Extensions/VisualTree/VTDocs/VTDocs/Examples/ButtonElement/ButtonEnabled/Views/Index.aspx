
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Enabled Property" ID="windowView2" LoadAction="ButtonEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.
            Values: 'true' if the control can respond to user interaction; otherwise, 'false'. The default is 'true'.

            Syntax: public bool Enabled { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="170px" ID="TestedButton1" ClickAction="ButtonEnabled\TestedButton1_Click"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Height="40" Top="220px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="310px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedButton' is initially set to 'false'.
            
            * When the button is Enabled, clicking it, will change it's back color." Top="360px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Enabled ="false" Top="440px" ID="TestedButton2" ClickAction="ButtonEnabled\TestedButton2_Click"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="490px" Height="40" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Enabled Value >>" Top="570px" ID="btnChangeEnabledValue" ClickAction="ButtonEnabled\btnChangeEnabledValue_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>