<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Equals Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction ="RichTextBoxEquals\Load">


        <vt:Label runat="server" Text="RichTextBox1:" Top="45px" Left="30px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="150px"></vt:Label>     

        <vt:RichTextBox runat="server" Text="RichTextBox1" Top="30px" Left="140px" ID="RichTextBox1" Height="70px" Width="200px" ></vt:RichTextBox>           


        <vt:Label runat="server" Text="RichTextBox2:" Top="120px" Left="30px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="150px"></vt:Label>     

         <vt:RichTextBox runat="server" Text="RichTextBox2" Top="135px" Left="140px" ID="RichTextBox2" Height="70px" Width="200px" ></vt:RichTextBox>


        <vt:Button runat="server" Text="Invoke Equals" Top="150px" Left="400px" ID="btnEquals" Height="36px" Width="220px" ClickAction ="RichTextBoxEquals\btnEquals_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="240px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="220px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
