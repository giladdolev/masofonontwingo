using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");

            lblLog.Text = "Button Visible value: " + TestedButton.Visible;
        }

        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");

            TestedButton.Visible = !TestedButton.Visible;
        }

        public void TestedButton_VisibleChanged(object sender, EventArgs e)
        {

            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtEventLog != null)
                txtEventLog.Text += "\\r\\nButton VisibleChanged event is invoked";
            if (lblLog != null)
                lblLog.Text = "Button Visible value: " + TestedButton.Visible;
        }
    }
}