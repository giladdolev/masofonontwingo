﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid BeforeSelectionChanged event" LoadAction="GridBeforeSelectionChanged\Load_View">

         <vt:Label runat="server" SkinID="Title" Text="BeforeSelectionChanged" Left="220px" ID="lblTitle" Top="15px">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs before the current selection changes.

            Syntax: public event GridElementSelectionChangeEventHandler BeforeSelectionChanged"
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells from the grid by clicking with the mouse or by pressing 
            the button. Each invocation of the 'BeforeSelectionChanged' event should add a line in the 'Event Log'.
            You can use the 'Cancel selection' checkbox to cancel the next selection. When 'Cancel selection'  
            is checked, the selection action will be canceled in the BeforeSelectionChanged event 
            and the SelectionChanged event won't invoke."
            Top="215px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="340px" ID="TestedGrid" BeforeSelectionChangedAction="GridBeforeSelectionChanged\TestedGrid_BeforeSelectionChanged" SelectionChangedAction="GridBeforeSelectionChanged\TestedGrid_SelectionChanged">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid> 

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="470px" Width="360px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="510px" Width="360px" Height="100px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select i >>" Top="635px" ID="btnSelecti" ClickAction="GridBeforeSelectionChanged\btnSelecti_Click"></vt:Button>

         <vt:CheckBox runat="server" Text="Cancel selection" Top="635px" Left="300px" ID="chkCancelSelection"></vt:CheckBox>
         
    </vt:WindowView>
</asp:Content>
