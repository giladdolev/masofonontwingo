﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class TopMostWindow : WindowElement
    {
        TopMostWindow2 _frm2;
        TopMostWindow3 _frm3;
        public TopMostWindow()
		{
		}
        public TopMostWindow2 frm2
		{
			get { return this._frm2; }
			set { this._frm2 = value; }
		}
        public TopMostWindow3 frm3
		{
			get { return this._frm3; }
			set { this._frm3 = value; }
		}
    }
}