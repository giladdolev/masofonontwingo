<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox TextChanged event" ID="windowView1" LoadAction="RichTextBoxTextChanged\OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="TextChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the Text property value changes.

            Syntax: public event EventHandler TextChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the Text property by clicking the 'Change Text' button,
            or by manually typing text into the RichTextBox. Each invocation of the 'TextChanged' event should add 
            a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:RichTextBox runat="server" Text="TestedRichTextBox" name="TestedRichTextBox" Top="325px" ID="TestedRichTextBox" TextChangedAction="RichTextBoxTextChanged\TestedRichTextBox_TextChanged" WaitMaskDisabled="true"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="420px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="460px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change Text >>" Top="605px" ID="btnChangeText" ClickAction ="RichTextBoxTextChanged\btnChangeText_Click" ></vt:Button>          
       
    </vt:WindowView>
</asp:Content>
