<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Focus() Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
      
        <vt:Label runat="server" Text="Focus() Method" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="240px"></vt:Label>

        <vt:CheckBox runat="server" Text="Hello World" Top="125px" Left="140px" ID="ChkFocus"  Height="26px"  Width="150px"></vt:CheckBox>

        <vt:Button runat="server" Text="Focus CheckBox" Top="115px" Left="350px" ID="btnFocus" Height="36px"  Width="150px" ClickAction="CheckBoxFocus\btnFocus_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
