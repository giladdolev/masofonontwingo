<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="CheckBox MinimumSize" ID="windowView1" LoadAction="CheckBoxMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedCheckBox' is width: 250px 
            and height: 50px. Its MinimumSize is initially set to width: 300px and height: 70px.
            To cancel 'TestedCheckBox' MinimumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>
        
        <vt:CheckBox runat="server" SkinID="BackColorCheckBox" Text="TestedCheckBox" Top="360px" ID="TestedCheckBox" TabIndex="1" Width="250px" Height="50" MinimumSize="300, 70"></vt:CheckBox> 

        <vt:Label runat="server" SkinID="Log" Top="475px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="560px" width="200px" Left="80px" ID="btnChangeChkMinimumSize" ClickAction="CheckBoxMinimumSize\btnChangeChkMinimumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="560px" width="200px" Left="310px" ID="btnSetToZeroChkMinimumSize" ClickAction="CheckBoxMinimumSize\btnSetToZeroChkMinimumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>







