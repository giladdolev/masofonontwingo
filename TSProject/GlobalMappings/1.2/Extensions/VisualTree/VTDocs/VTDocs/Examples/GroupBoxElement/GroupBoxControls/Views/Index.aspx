<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox BackColor Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
       

        <vt:GroupBox runat="server" Text="GroupBox" Top="165px" Left="140px" ID="grpControls" Height="150px" TabIndex="1" Width="200px"></vt:GroupBox>

        <vt:Button runat="server" Text="Add Controls" Top="195px" Left="420px" ID="btnAddControls" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="GroupBoxControls\btnAddControls_Click"></vt:Button>



    </vt:WindowView>
</asp:Content>
        