<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Draggable property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxDraggable/Form_Load">
    <vt:Label runat="server" Text="Initialized Component : ComboBox Draggable is set to true" Top="350px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="comboBox1" DisplayMember="text" ValueMember="value" Left="50" Top="380" Height="50" Draggable="true">
            <Items>
                <vt:ListItem runat="server" Text="aa"></vt:ListItem>
                <vt:ListItem runat="server" Text="bb"></vt:ListItem>
                <vt:ListItem runat="server" Text="cc"></vt:ListItem>
                <vt:ListItem runat="server" Text="55"></vt:ListItem>
            </Items>
        </vt:ComboBox>
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="160" Left="50px"/>
        <vt:Button runat="server" Text="Get comboBox Draggable value" Top="112px" Left="130px" ID="btnDraggable" TabIndex="1" Width="250px" ClickAction="ComboBoxDraggable\btnDraggable_Click"></vt:Button>
        <vt:TextBox runat="server" Text="" Top="200px" Left="50px" ID="txtDraggable" Multiline="true"  Height="100px" Width="250px"> 
        </vt:TextBox>
    </vt:WindowView>
</asp:Content>
