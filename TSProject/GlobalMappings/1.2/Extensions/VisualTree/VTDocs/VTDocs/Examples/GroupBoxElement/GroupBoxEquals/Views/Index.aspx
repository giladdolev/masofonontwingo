<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Equals Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction ="GroupBoxEquals\Load">


        <vt:Label runat="server" Text="GroupBox1:" Top="45px" Left="30px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="150px"></vt:Label>     

        <vt:GroupBox runat="server" Text="GroupBox1" Top="30px" Left="140px" ID="GroupBox1" Height="70px" Width="200px" ></vt:GroupBox>           


        <vt:Label runat="server" Text="GroupBox2:" Top="120px" Left="30px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="150px"></vt:Label>     

         <vt:GroupBox runat="server" Text="GroupBox2" Top="135px" Left="140px" ID="GroupBox2" Height="70px" Width="200px" ></vt:GroupBox>


        <vt:Button runat="server" Text="Invoke Equals" Top="150px" Left="400px" ID="btnEquals" Height="36px" Width="220px" ClickAction ="GroupBoxEquals\btnEquals_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="240px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="220px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
