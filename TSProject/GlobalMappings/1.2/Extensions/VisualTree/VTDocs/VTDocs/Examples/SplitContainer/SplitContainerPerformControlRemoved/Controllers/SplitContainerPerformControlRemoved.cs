using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPerformControlRemovedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlRemoved_Click(object sender, EventArgs e)
        {
            
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");

            ControlEventArgs args = new ControlEventArgs(testedSplitContainer);

            testedSplitContainer.PerformControlRemoved(args);
        }

        public void testedSplitContainer_ControlRemoved(object sender, EventArgs e)
        {
            MessageBox.Show("TestedSplitContainer ControlRemoved event method is invoked");
        }

    }
}