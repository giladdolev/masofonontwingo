<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton KeyPress event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

         <vt:Label runat="server" Text="Invoke KeyPress event method by pressing a character space or backspace key while the control has focus." Top="30px" Left="110px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="500px"></vt:Label>

        <vt:RadioButton runat="server" Text="TestedRadioButton" Top="50px" Left="200px" ID="rdoKeyPress" Height="36px"  Width="200px" KeyPressAction ="RadioButtonKeyPress\rdoKeyPress_KeyPress"></vt:RadioButton>          

        <vt:Label runat="server" Text="Event Log" Top="130px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:TextBox runat="server" Text="" Top="150px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
