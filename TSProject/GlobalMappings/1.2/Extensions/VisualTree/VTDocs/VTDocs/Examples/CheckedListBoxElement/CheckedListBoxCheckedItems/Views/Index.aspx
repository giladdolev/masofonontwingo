<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckedListBox CheckedItems property" ID="windowView1" LoadAction="CheckedListBoxCheckedItems\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CheckedItems " ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Collection of checked items in this CheckedListBox.

            Syntax: public ListItemCollection CheckedItems { get; }
            
            The collection is a subset of the objects in the Items collection, representing only those items whose CheckState is Checked or Indeterminate. The indexes in this collection are in ascending order."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Invoke the ItemCheck event by checking or unchecking CheckedListBox CheckBoxes."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:CheckedListBox runat="server" ID="testedCheckedListBox" Top="285px" ItemCheckAction="CheckedListBoxCheckedItems\testedCheckedListBox_ItemCheck">
            <Items>
                <vt:ListItem runat="server" ID="listItem1" Text="CheckBox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem2" Text="CheckBox 2"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem3" Text="CheckBox 3"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem4" Text="CheckBox 4"></vt:ListItem>
            </Items>
        </vt:CheckedListBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Height="80px" Top="420px" Width="450px"></vt:Label>

        <vt:Button runat="server" Text="Check\Uncheck CheckBox1 >>" Top="550px" ID="CheckUncheckCheckBox1" Width="200px" ClickAction="CheckedListBoxCheckedItems\CheckUncheckCheckBox5_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
