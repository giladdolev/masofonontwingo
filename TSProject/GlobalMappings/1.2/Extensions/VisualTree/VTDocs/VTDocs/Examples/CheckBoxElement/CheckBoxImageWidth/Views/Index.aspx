<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox ImageWidth Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Initialize - CheckBox ImageWidth is set to '150'" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:CheckBox runat="server" Text="Initialize Test" ImageWidth = "150px" Image ="~/Content/Elements/CheckBox.png" Top="45px" Left="140px" ID="btnImageWidth" Height="100px" TabIndex="1" Width="300px"></vt:CheckBox>  
          
        <vt:Label runat="server" Text="Runtime" Top="220px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:CheckBox runat="server" Text="Runtime ImageWidth" Image ="~/Content/Elements/CheckBox.png" Top="235px" Left="140px" ID="chkRuntimeImageWidth" Height="100px"  TabIndex="1" Width="300px"></vt:CheckBox>


        <vt:Button runat="server" Text="Change ImageWidth" Top="235px" Left="400px" ID="btnChangeImageWidth" Height="36px"  TabIndex="1" Width="220px" ClickAction="CheckBoxImageWidth\btnChangeImageWidth_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="380px" Left="140px" ID="textBox1" Multiline="true"  Height="40px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
