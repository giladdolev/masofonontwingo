<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Ribbon ImageList property" ID="windowView2" LoadAction="RibbonImageList\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="ImageList" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the collection of images available to the Ribbon controls.
            
            Syntax: public ImageList ImageList { get; set; }
            Value: An ImageList that contains images available to the Ribbon controls. The default is null.
            
            If you create an ImageList and assign it to the ImageList property, you can assign an image from 
            the collection to the Ribbon controls by assigning the image's index value to the ImageIndex 
            property of the Ribbon control items." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example shows a 2 Tabs Ribbon panel hosting RibbonPageGroups with Button, 
            ToolBarDropDownButton and ToolBarButton.
            This Ribbon is using an ImageList to assign images to the items it contains." Top="290px" ID="lblExp2"></vt:Label>

        <vt:RibbonControl runat="server" ID="rbCtrl" Width="600" Height="200px" Left="50px" Top="380px" >
            
                   <vt:RibbonPage runat="server" ID="rbp1" Text="Page1">                   
                        <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup1" Text="Group1" ImageListID="imglst">
                            <vt:Button runat="server" SkinID="RibbonButton" ID="btn1" Text="btn1" ImageIndex="5" TextImageRelation="ImageAboveText"></vt:Button>
                            <vt:ToolBarDropDownButton runat="server" ID="btn2" Text="dropDown" DropDownArrows="true" CssClass="vt-dropdown-page1">
                                <vt:ToolBarButton  runat="server" ID="btn3" Text="dropDown1" ImageIndex="3"></vt:ToolBarButton>
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton4" Text="dropDown2" ImageIndex="4"></vt:ToolBarButton>
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton5" Text="dropDown3" ImageIndex="5"></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                        </vt:RibbonPageGroup>                      
                 </vt:RibbonPage>

                 <vt:RibbonPage runat="server" ID="rbp2" Text="Page2" >
                         <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup3" Text="Group1" ImageListID="imglst" >
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button1" Text="btn1" ImageIndex="4"></vt:Button>
                            <vt:ToolBarDropDownButton runat="server" ID="ToolBarDropDownButton1" Text="dropDown" DropDownArrows="true">
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton1" Text="dropDown1" ImageIndex="6"></vt:ToolBarButton>
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton6" Text="dropDown2" ImageIndex="3"></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                            <vt:ToolBarDropDownButton runat="server" ID="ToolBarDropDownButton2" Text="dropDown" DropDownArrows="true">
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton2" Text="dropDown1" ImageIndex="2"></vt:ToolBarButton>
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton7" Text="dropDown2" ImageIndex="4"></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                        </vt:RibbonPageGroup>         
                      
                        <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup2" Text="Group2" ImageListID="imglst" >
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button2" ImageIndex="4" Text="btn1" TextImageRelation="ImageAboveText"></vt:Button>
                            <vt:ToolBarDropDownButton runat="server" ID="ToolBarDropDownButton3" Text="dropDown" DropDownArrows="true" ImageIndex="6" TextImageRelation="ImageAboveText">
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton3" Text="dropDown1" ImageIndex="3"></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                        </vt:RibbonPageGroup>  
                     
                     <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup4" Text="Group3" ImageListID="imglst">
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button3" ImageIndex="4" Text="btn1" TextImageRelation="ImageAboveText"></vt:Button>
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button4" ImageIndex="5" Text="btn2" TextImageRelation="ImageAboveText"></vt:Button>
                        </vt:RibbonPageGroup>                                                                      
                 </vt:RibbonPage>
               
        </vt:RibbonControl>
    </vt:WindowView>
    <vt:ComponentManager runat="server" Visible="False">
        <vt:ImageList runat="server" TransparentColor="Transparent" ID="imglst">
            <Images>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream0.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream1.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream2.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream3.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream4.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream5.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream6.png"></vt:UrlReference>
            </Images>
            <Keys>
                <vt:KeyItem runat="server" Key="Content/Images/Cute.png"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/galilcs.png" ImageIndex="1"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/gizmox.png" ImageIndex="2"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/icon.jpg" ImageIndex="3"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/New.png" ImageIndex="4"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/Open.png" ImageIndex="5"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/sleepy.jpg" ImageIndex="6"></vt:KeyItem>
            </Keys>
        </vt:ImageList>
    </vt:ComponentManager>
</asp:Content>
