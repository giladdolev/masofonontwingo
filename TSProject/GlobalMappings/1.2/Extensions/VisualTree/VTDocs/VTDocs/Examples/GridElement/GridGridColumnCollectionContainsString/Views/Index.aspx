﻿ <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumnCollection Contains  Method (String)" ID="windowView1" LoadAction="GridGridColumnCollectionContainsString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Contains(String)" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Determines whether the collection contains the column referred to by the given ID.

            Syntax: public bool Contains(string columnID)
            columnID - The ID of the column to look for.

            Return Value: 'true' if the column is contained in the collection; otherwise, 'false'.
            Exceptions - 
            ArgumentNullException - columnID is null." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="260px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Search for a column ID using the below textBox and button to invoke
             the Contains(String) method.
            Column1 ID is Col1
            Column2 ID is Col2
            Column3 ID is Col3"
            Top="310px" ID="lblExp2"></vt:Label>

        <vt:Grid runat="server" Top="420px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Width="380px" Height="40px" ID="lblLog" Top="550px"></vt:Label>

        <vt:label runat="server" Text="Search Column ID: " Font-Size="11pt" Left="80px" Width="120px" Top="617px" ID="lblInput" ></vt:label>

        <vt:TextBox runat="server" Text="ColumnID" ForeColor="Gray" Width="80px" Left="200px"  Top="615px" ID="txtInput" TextChangedAction="GridGridColumnCollectionContainsString\txtInput_TextChanged"></vt:TextBox>

        <vt:Button runat="server" Text="Contains >>" Left="330px" Top="615px" ID="btnContains" ClickAction="GridGridColumnCollectionContainsString\btnContains_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

