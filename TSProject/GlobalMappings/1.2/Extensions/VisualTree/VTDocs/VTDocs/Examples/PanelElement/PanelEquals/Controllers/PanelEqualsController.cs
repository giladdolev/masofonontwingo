using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new PanelEquals());
        }

        private PanelEquals ViewModel
        {
            get { return this.GetRootVisualElement() as PanelEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement Panel1 = this.GetVisualElementById<PanelElement>("Panel1");
            PanelElement Panel2 = this.GetVisualElementById<PanelElement>("Panel2");


            if (this.ViewModel.TestedPanel.Equals(Panel1))
            {
                this.ViewModel.TestedPanel = Panel2;
                textBox1.Text = "TestedPanel Equals Panel2";
            }
            else
            {
                this.ViewModel.TestedPanel = Panel1;
                textBox1.Text = "TestedPanel Equals Panel1";
            }

            
        }
        

        public void Load(object sender, EventArgs e)
        {
            this.ViewModel.TestedPanel.BackColor = Color.Blue;
            this.ViewModel.Controls.Add(this.ViewModel.TestedPanel);
        }



    }
}