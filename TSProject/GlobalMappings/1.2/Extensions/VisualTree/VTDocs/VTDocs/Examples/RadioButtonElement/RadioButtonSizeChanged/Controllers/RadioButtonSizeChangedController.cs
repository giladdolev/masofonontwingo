using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        public void btnChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement rdoSize = this.GetVisualElementById<RadioButtonElement>("rdoSize");

            if (rdoSize.Size == new Size(200,36)) //Get
            {
                rdoSize.Size = new Size(170, 100); //Set
                textBox1.Text = "The Size property value is: " + rdoSize.Size;
            }
            else  
            {
                rdoSize.Size = new Size(200, 36); 
                textBox1.Text = "The Size property value is: " + rdoSize.Size;
            }
             
        }
        
        public void rdoSize_SizeChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "SizeChanged Event is invoked\\r\\n";

        }
    }
}