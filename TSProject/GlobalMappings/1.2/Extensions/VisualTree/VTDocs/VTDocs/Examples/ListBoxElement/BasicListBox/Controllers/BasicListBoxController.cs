using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicListBoxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void Form_Load(object sender, EventArgs e)
        {

            ListBoxElement lbe = this.GetVisualElementById<ListBoxElement>("listBox1");
            if (lbe != null)
            {
                lbe.Items.Add("Content Text 1 Item");
                lbe.Items.Add("Content Text 2 Item");
            }
   
        }
    }
}