using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkRuntimeCursor = this.GetVisualElementById<CheckBoxElement>("chkRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (chkRuntimeCursor.Cursor == CursorsElement.Default)
            {
                chkRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                chkRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                chkRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                chkRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.Help)
            {
                chkRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                chkRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                chkRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.No)
            {
                chkRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                chkRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                chkRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;

            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                chkRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                chkRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                chkRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                chkRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                chkRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                chkRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                chkRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                chkRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                chkRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
            else if (chkRuntimeCursor.Cursor == CursorsElement.WaitCursor)
            {
                chkRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + chkRuntimeCursor.Cursor;
            }
        }

    }
}