<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Items Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="1000px">
		<vt:Label runat="server" Text="Initialized Component : Items are added from the View" Top="300px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>

        <vt:ToolBar runat="server" Text="toolStrip2" Dock="None" Top="350px" Left="50px" ID="toolStrip2" Font-Names="Microsoft Sans Serif" Font-Items="8.25pt" Height="25px" Width="466px">

            <vt:ToolBarButton runat="server" DisplayStyle="Image" Image="/Content/Images/Form1_toolStripButton1_Image.png" ImageTransparentColor="Magenta" TextDirection="Inherit" ImageAlign="MiddleRight" Text="toolStripButton1" ID="toolStripButton1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="22px" Width="150px"></vt:ToolBarButton>

            <vt:ToolBarLabel runat="server" TextDirection="Inherit" ImageAlign="MiddleRight" Text="toolStripLabel1" ID="toolStripLabel1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="22px" Width="86px"></vt:ToolBarLabel>

            <vt:ToolBarDropDownButton runat="server" DisplayStyle="Image" TextDirection="Inherit" ImageAlign="MiddleRight" Text="toolStripDropDownButton1" ID="toolStripDropDownButton1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="22px" Width="29px"></vt:ToolBarDropDownButton>

            <vt:ToolBarItem runat="server" DisplayStyle="Image" TextDirection="Inherit" ImageAlign="MiddleRight" Text="toolStripItem1" ID="toolStripItem1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="22px" Width="29px"></vt:ToolBarItem>

		</vt:ToolBar>


        <vt:Label runat="server" Text="Run Time : Clik on the buttons to add items to the ToolBar" Top="70px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ToolBar runat="server" Text="toolStrip1" Dock="None" Top="112px" Left="50px" ID="toolStrip1" Font-Names="Microsoft Sans Serif" Font-Items="8.25pt" Height="25px" Width="466px"></vt:ToolBar>

		<vt:Button runat="server" UseVisualStyleItems="True" TextAlign="MiddleCenter" Text="Add ToolBarLabel" Top="200px" Left="50px" ClickAction="ToolBarItems\btnAddToolBarLabel_Click" ID="btnAddToolBarLabel" Font-Names="Microsoft Sans Serif" Font-Items="8.25pt" Height="29px" TabIndex="1" Width="150px">
		</vt:Button>

        <vt:Button runat="server" UseVisualStyleItems="True" TextAlign="MiddleCenter" Text="Add ToolBarButton" Top="200px" Left="210px" ClickAction="ToolBarItems\btnAddToolBarButton_Click" ID="btnAddToolBarButton" Font-Names="Microsoft Sans Serif" Font-Items="8.25pt" Height="29px" TabIndex="1" Width="150px">
		</vt:Button>

          <vt:Button runat="server" UseVisualStyleItems="True" TextAlign="MiddleCenter" Text="Add ToolBarDropDownButton" Top="200px" Left="380px" ClickAction="ToolBarItems\btnAddToolBarDropDownButton_Click" ID="btnAddToolBarDropDownButton" Font-Names="Microsoft Sans Serif" Font-Items="8.25pt" Height="29px" TabIndex="1" Width="250px">
		</vt:Button>

        <vt:Button runat="server" UseVisualStyleItems="True" TextAlign="MiddleCenter" Text="Add ToolBarItem" Top="200px" Left="660px" ClickAction="ToolBarItems\btnAddToolBarItem_Click" ID="btnAddToolBarItem" Font-Names="Microsoft Sans Serif" Font-Items="8.25pt" Height="29px" TabIndex="1" Width="150px">
		</vt:Button>
           

        </vt:WindowView>
</asp:Content>