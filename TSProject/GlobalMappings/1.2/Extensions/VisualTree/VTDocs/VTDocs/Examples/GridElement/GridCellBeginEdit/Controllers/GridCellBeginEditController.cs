using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridCellBeginEditController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        private void Page_load(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog1.Text = "The Selected cell is: Row: " + TestedGrid.SelectedRowIndex + ", " + "Column: " + TestedGrid.SelectedColumnIndex;
        }

        private void TestedGrid_CellBeginEdit(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            int selectedRow = TestedGrid.SelectedRowIndex;
            int selectedColumn = TestedGrid.SelectedColumnIndex;

            lblLog1.Text = "The Edited cell is: Row: " + selectedRow + ", " + TestedGrid.Columns[selectedColumn].HeaderText + ", Edited value: " + TestedGrid.Rows[selectedRow].Cells[selectedColumn].Value + '.';

            txtEventLog.Text += "\\r\\nStarted editing at Cell (" + selectedRow + "," + selectedColumn + "), cellBeginEdit event is invoked.";
            
        }

       
   
	}
}
