<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Font Property" ID="windowView2" LoadAction="TextBoxFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:TextBox runat="server" Text="TextBox" Top="190px" Width="160px" ID="TestedTextBox1" ></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="230px" Width="430px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="320px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedTextBox is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="370px"  ID="lblExp1"></vt:Label>     
        
      
        <vt:TextBox runat="server" Text="TestedTextBox" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="435px" Width="160px" ID="TestedTextBox2" ></vt:TextBox>
        <vt:Label runat="server" SkinID="Log" Top="495px" Width="430px" Height="45px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="595px" ID="btnChangeTextBoxFont" ClickAction="TextBoxFont/btnChangeTextBoxFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>