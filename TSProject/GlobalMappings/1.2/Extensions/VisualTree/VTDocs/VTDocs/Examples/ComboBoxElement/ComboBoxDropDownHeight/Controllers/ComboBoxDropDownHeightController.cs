using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxDropDownHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement ComboBox = this.GetVisualElementById<ComboBoxElement>("ComboBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "DropDownHeight value: " + ComboBox.DropDownHeight;
            lblLog2.Text = "DropDownHeight value: " + TestedComboBox.DropDownHeight + '.';
        }

        private void btnChangeDropDownHeight_Click(object sender, EventArgs e)
        {

            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedComboBox.DropDownHeight == 40)
            {
                TestedComboBox.DropDownHeight = 80;
                lblLog2.Text = "DropDownHeight value: " + TestedComboBox.DropDownHeight + '.';
            }
            else
            {
                TestedComboBox.DropDownHeight = 40;
                lblLog2.Text = "DropDownHeight value: " + TestedComboBox.DropDownHeight + '.';
            }
        }
    }
}