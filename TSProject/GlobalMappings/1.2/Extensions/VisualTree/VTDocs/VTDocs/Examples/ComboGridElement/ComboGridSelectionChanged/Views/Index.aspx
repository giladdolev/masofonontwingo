<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid SelectionChanged event" ID="windowView2" LoadAction="ComboGridSelectionChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" Left="250px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current selection changes.

            Syntax: public event GridElementCellEventHandler SelectionChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select Row/Cell from the combogrid by mouse clicking.
            Each invocation of the 'SelectionChanged' event should add a line to the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-test-cmbgrid" Top="325px" ID="TestedComboGrid" SelectionChangedAction="ComboGridSelectionChanged\TestedComboGrid_SelectionChanged"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Left="80px" Height="40px" Top="365px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="435px" ID="txtEventLog"></vt:TextBox>       
       
    </vt:WindowView>
</asp:Content>
