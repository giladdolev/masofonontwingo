using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageBackgroundImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage1 = this.GetVisualElementById<ImageElement>("TestedImage1");
            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedImage1.BackgroundImage == null)
            {
                lblLog1.Text = "No background image";
            }
            else
            {
                lblLog1.Text = "Set with background image";
            }

          
            if (TestedImage2.BackgroundImage == null)
            {
                lblLog2.Text = "No background image.";
            }
            else
            {
                lblLog2.Text = "Set with background image.";
            }
        }

   

        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
        {
            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedImage2.BackgroundImage == null)
            {
                TestedImage2.BackgroundImage = new UrlReference("/Content/Elements/Image.png");
                lblLog2.Text = "Set with background image.";
            }
            else
            {
                TestedImage2.BackgroundImage = null;
                lblLog2.Text = "No background image.";

            }
        }

    
    }
}