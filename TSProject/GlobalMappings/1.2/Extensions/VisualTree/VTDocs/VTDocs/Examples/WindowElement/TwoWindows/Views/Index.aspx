<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>

<asp:Content ContentPlaceHolderID="Content" runat="server">
<vt:WindowView runat="server" Opacity="1" StartPosition="Manual" AutoScaleMode="Font" AutoScaleDimensions="6, 13" Text="MainForm" ID="MainForm" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="373px" TabIndex="-1" Width="659px" >
    <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Show Win1 Hide Win2 " Top="59px" Left="140px" ID="button1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="46px" Width="102px" ClickAction="TwoWindowsMainForm\button1_Click"></vt:Button>
   
</vt:WindowView>


</asp:Content>