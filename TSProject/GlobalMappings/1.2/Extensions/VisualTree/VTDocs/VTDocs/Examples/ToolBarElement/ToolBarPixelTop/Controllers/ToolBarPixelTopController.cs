using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarPixelTopController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelTop value: " + TestedToolBar.PixelTop + '.';
        }

        public void btnChangePixelTop_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedToolBar.PixelTop == 300)
            {
                TestedToolBar.PixelTop = 285;
                lblLog.Text = "PixelTop value: " + TestedToolBar.PixelTop + '.';
            }
            else
            {
                TestedToolBar.PixelTop = 300;
                lblLog.Text = "PixelTop value: " + TestedToolBar.PixelTop + '.';
            }

        }
    }
}