using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new ProgressBarEquals());
        }

        private ProgressBarEquals ViewModel
        {
            get { return this.GetRootVisualElement() as ProgressBarEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ProgressBarElement ProgressBar1 = this.GetVisualElementById<ProgressBarElement>("ProgressBar1");
            ProgressBarElement ProgressBar2 = this.GetVisualElementById<ProgressBarElement>("ProgressBar2");


            if (this.ViewModel.TestedProgressBar.Equals(ProgressBar1))
            {
                this.ViewModel.TestedProgressBar = ProgressBar2;
                textBox1.Text = "TestedProgressBar Equals ProgressBar2";
            }
            else
            {
                this.ViewModel.TestedProgressBar = ProgressBar1;
                textBox1.Text = "TestedProgressBar Equals ProgressBar1";
            }

            
        }
        

        public void Load(object sender, EventArgs e)
        {
            this.ViewModel.Controls.Add(this.ViewModel.TestedProgressBar);
        }



    }
}