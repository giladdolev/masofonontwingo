using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("Item2");
            TestedComboBox.Items.Add("Item3");
            lblLog.Text = "ComboBox bounds are: \\r\\n" + TestedComboBox.Bounds;

        }
        public void btnChangeComboBoxBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            if (TestedComboBox.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedComboBox.Bounds = new Rectangle(80, 355, 165, 25);
                lblLog.Text = "ComboBox bounds are: \\r\\n" + TestedComboBox.Bounds;

            }
            else
            {
                TestedComboBox.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "ComboBox bounds are: \\r\\n" + TestedComboBox.Bounds;
            }

        }
    }
}