using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPerformAfterrenderController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformAfterrender_Click(object sender, EventArgs e)
        {
            
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            

            btnTestedButton.PerformAfterrender(e);
        }

        public void btnTestedButton_Afterrender(object sender, EventArgs e)
        {
            MessageBox.Show("TestedButton Afterrender event method is invoked");
        }

    }
}