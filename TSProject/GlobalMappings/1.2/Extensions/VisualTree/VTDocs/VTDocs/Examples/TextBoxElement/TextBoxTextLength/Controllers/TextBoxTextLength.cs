using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxTextLengthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnGetTextLength_Click(object sender, EventArgs e)
        {
            TextBoxElement txtTextLength = this.GetVisualElementById<TextBoxElement>("txtTextLength");

            MessageBox.Show("TextLength value: " + txtTextLength.TextLength);
            
            
        }

    }
}