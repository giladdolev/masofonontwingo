using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowAccessibleRoleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowAccessibleRole());
        }
        private WindowAccessibleRole ViewModel
        {
            get { return this.GetRootVisualElement() as WindowAccessibleRole; }
        }


        //btnGetAccessibleRole_Click

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            MessageBox.Show(this.ViewModel.AccessibleRole.ToString());
        }


        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.AccessibleRole == AccessibleRole.Alert)
			{
                this.ViewModel.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
			}
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Animation)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Application)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Border)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Caret)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Cell)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Character)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Chart)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.CheckButton)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Client)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Clock)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Column)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ComboBox)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Cursor)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Default)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Diagram)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Dial)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Dialog)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Document)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.DropList)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Equation)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Graphic)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Grip)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Grouping)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.HotkeyField)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Indicator)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.IpAddress)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Link)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.List;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.List)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ListItem)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.MenuBar)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.MenuItem)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.MenuPopup)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.None;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.None)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Outline)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.OutlineButton)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.OutlineItem)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.PageTab)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.PageTabList)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Pane)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ProgressBar)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.PropertyPage)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.PushButton)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Row)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.RowHeader)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ScrollBar)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Separator)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Slider)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Sound)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.SpinButton)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.SplitButton)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.StaticText)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.StatusBar)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Table)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.Text)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.TitleBar)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ToolBar)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.ToolTip)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
            else if (this.ViewModel.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
			else
            {
                this.ViewModel.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = this.ViewModel.AccessibleRole.ToString();
            }
                
		}

    }
}