using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowControlsController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowControls());
        }
        private WindowControls ViewModel
        {
            get { return this.GetRootVisualElement() as WindowControls; }
        }

        public void btnAddControls_Click(object sender, EventArgs e)
		{

            this.ViewModel.Controls.Add(new LabelElement { Text = "newLabel1", ID = "label1", Left = 410, Top = 200, Height = 15, Width = 100 });
            this.ViewModel.Controls.Add(new LabelElement { Text = "newLabel2", ID = "label2", Left = 280, Top = 200, Height = 15, Width = 100 });
            this.ViewModel.Controls.Add(new TextBoxElement { Text = "newTextBox1", ID = "textbox1", Left = 150, Top = 200, Height = 15, Width = 100 });
            this.ViewModel.Controls.Add(new PanelElement { Text = "newPanel1", ID = "panel1", Left = 20, Top = 200, Height = 50, Width = 100 });
                
		}

    }
}