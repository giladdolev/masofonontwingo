<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="DateTimePicker CustomFormat Property" ID="windowView1" Height="800px" LoadAction="DateTimePickerCustomFormat\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CustomFormat" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the custom date/time format string.
            
        Syntax: public string CustomFormat { get; set; }
        The Format property must be set to DateTimePickerFormat.Custom for this property to affect the 
        formatting of the displayed date and time. The default is null.

        Click the following button to See a table that describes the custom date and time format specifiers.
        To display string literals that contain date and time separators or to format strings, you must use 
        escape characters in the substring. For example, to display the date as 'June 15 at 12:00 PM', 
        set the CustomFormat property to 'MMMM dd 'at' t:mm tt'. If the 'at' substring is not enclosed  
        by escape characters, the result is 'June 15 aP 12:00PM' because the 't' character is read as the one-letter 
        A.M./P.M. format string" Top="75px" ID="lblDefinition"></vt:Label>
      
       <!-- Tested DateTimePicker1 -->
        <vt:DateTimePicker runat="server" Text="DateTimePicker" Left="80px" Height="20px" Width="200px" Top="335px" Format="Custom" ID="TestedDateTimePicker1"></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Top="370px" Height="60px" ID="lblLog1"></vt:Label>

        <vt:Button runat="server" Text="Open FormatString Table >>" Top="335px" width="200px" Left="330px" ID="btnFormatStringTable" ClickAction="DateTimePickerCustomFormat\btnFormatStringTable_Click"></vt:Button>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="455px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="CustomFormat property of this 'TestedDateTimePicker' is initially set to 'yyyy-MM-dd'.
            You can Set it to a different string value using the bellow textbox and button." Top="505px" ID="lblExp1"></vt:Label>     
        
        <!-- Tested DateTimePicker2 -->
        <vt:DateTimePicker runat="server"  Text="TestedDateTimePicker" CssClass="vt-TestedDateTimePicker" Format="Custom" CustomFormat="yyyy-MM-dd" Left ="80px" Height="20px" Width="200px" Top="555px" ID="TestedDateTimePicker2" ValueChangedAction="DateTimePickerCustomFormat\TestedDateTimePicker2_ValueChanged"></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Top="590px" Height="60px" ID="lblLog2"></vt:Label>

        <vt:TextBox runat="server" Top="675px" Width="300px" ID="TextBox"></vt:TextBox>

        <vt:Button runat="server" Text="Change CustomFormat value >>" Top="675px" ID="btnChangeCustomFormat" Width="185px" Left="400px" ClickAction="DateTimePickerCustomFormat\btnChangeCustomFormat_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
