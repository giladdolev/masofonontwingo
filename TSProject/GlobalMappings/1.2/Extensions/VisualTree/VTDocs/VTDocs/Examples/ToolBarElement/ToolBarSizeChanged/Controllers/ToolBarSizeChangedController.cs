using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ToolBarElement toolBar1 = this.GetVisualElementById<ToolBarElement>("toolBar1");

            if (toolBar1.Size == new Size(466, 25))
            {
                toolBar1.Size = new Size(700, 100);
                textBox1.Text = toolBar1.Size.ToString();
            }
            else
            {
                toolBar1.Size = new Size(466, 25);
                textBox1.Text = toolBar1.Size.ToString();
            }
             
        }
        
        public void toolBar1_SizeChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "SizeChanged Event is invoked\\r\\n";

        }
    }
}