<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox CheckChanged event" ID="windowView1" LoadAction="CheckBoxCheckedChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CheckedChanged" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the value of the IsChecked property changes.

            Syntax: public event EventHandler CheckedChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the IsChecked property by
             clicking the 'Change IsChecked' button or by manually checking or unchecking the TestedCheckBox.
            Each invocation of the 'CheckChanged' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox" Top="325px" ID="TestedCheckBox" CheckedChangedAction="CheckBoxCheckedChanged\TestedCheckBox_CheckedChanged"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="365px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="405px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change IsChecked >>" Width="180px" Top="550px" ID="btnChangeIsChecked" ClickAction="CheckBoxCheckedChanged\btnChangeIsChecked_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
