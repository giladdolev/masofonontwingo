using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox1 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox1");
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedGroupBox1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
        }

        public void btnChangeBorderStyle_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGroupBox2.BorderStyle == BorderStyle.None)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Dotted)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Double)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Groove)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Inset)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Dashed)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.NotSet)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Outset)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Ridge)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else if (TestedGroupBox2.BorderStyle == BorderStyle.Solid)
            {
                TestedGroupBox2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
            else
            {
                TestedGroupBox2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedGroupBox2.BorderStyle + ".";
            }
        }


    }
}