using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImagePixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Image PixelHeight is: " + TestedImage.PixelHeight + '.';

        }
        public void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            if (TestedImage.PixelHeight == 90)
            {
                TestedImage.PixelHeight = 40;
                lblLog.Text = "Image PixelHeight is: " + TestedImage.PixelHeight + '.';

            }
            else
            {
                TestedImage.PixelHeight = 90;
                lblLog.Text = "Image PixelHeight is: " + TestedImage.PixelHeight + '.';
            }

        }
      
    }
}