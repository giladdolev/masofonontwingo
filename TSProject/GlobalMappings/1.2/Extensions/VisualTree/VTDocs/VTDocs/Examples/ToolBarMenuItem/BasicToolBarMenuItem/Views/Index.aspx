﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
 
     <vt:WindowView runat="server" Text="Basic ToolBarMenuItem" Width ="815px" ID="windowView1" LoadAction ="BasicToolBarMenuItem\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ToolBarMenuItem Class" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Represents a selectable option displayed on a MenuElement or ContextMenuStripElement.
            
           Remarks: ToolBarMenuItem is a ToolBarDropDownItem that works with ToolBarDropDownMenu and
             ContextMenuStrip to handle the special highlighting, layout, and column arrangement for menus.
             Use the ShortcutKeys property to define a keyboard combination that can be 
            pressed to select the menu item.

            Syntax: public class ToolBarMenuItem : ToolBarDropDownItem

            Constructors definitions:
           ToolBarMenuItem() - Initializes a new instance of the ToolBarMenuItem class.
            ToolBarMenuItem(String) - Initializes a new instance of the ToolBarMenuItem class that displays the specified text.
            ToolStripMenuItem(String, ResourceReference, EventHandler, Keys) - Initializes a new instance of the 
            ToolStripMenuItem class that displays the specified text and image, does the specified action when the 
            ToolStripMenuItem is clicked, and displays the specified shortcut keys." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="390px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can add a ToolBarMenuItem to this MenuElement by selecting one of the three 
            constructors from the ComboBox below" Top="430px" ID="lblExp1"></vt:Label>

       <vt:Menu runat="server" ID="TestedMenu" Dock="None" Width="500px" Top="495px">
            <vt:ToolBarMenuItem runat="server" Text="File">
                <vt:ToolBarMenuItem runat="server" Text="New..." Image="Content/Images/New.png" />
                <vt:ToolBarMenuItem runat="server" Text="Open..." Image="Content/Images/Open.png" />
                <vt:ToolBarMenuSeparator runat="server" />
                <vt:ToolBarMenuItem runat="server" Text="Exit" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Edit">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Tools">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Help">
                <vt:ToolBarMenuItem runat="server" Text="About" />
            </vt:ToolBarMenuItem>
        </vt:Menu>

        <vt:Label runat="server" SkinID="Log" Top="550px" Width="500px" ID="lblLog"></vt:Label>


        <vt:Label runat="server" Text="Select a constructor to add new ToolBarMenuItem: " Top="600px" ID="Label1"></vt:Label>

         <vt:ComboBox runat="server" Text="Constructors" Left="400px" Top="600px" Width="250px" ID="cboMenuItemConstructors" SelectedIndexChangedAction="BasicToolBarMenuItem\AddNewToolBarItem_SelectedIndexChanged">
             <Items>
                <vt:ListItem runat="server" Text="ToolBarMenuItem()"></vt:ListItem>
                <vt:ListItem runat="server" Text="ToolBarMenuItem(String)"></vt:ListItem>
                <vt:ListItem runat="server" Text="ToolBarMenuItem(string, ResourceReference, EventHandler,Keys)"></vt:ListItem>
            </Items>
         </vt:ComboBox>

    </vt:WindowView>
</asp:Content>
