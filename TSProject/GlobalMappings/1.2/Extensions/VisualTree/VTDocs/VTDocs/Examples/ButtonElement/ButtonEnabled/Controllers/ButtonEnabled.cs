using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton1 = this.GetVisualElementById<ButtonElement>("TestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedButton1.Enabled.ToString()+ "\\r\\nBackColor value: " + TestedButton1.BackColor.ToString();

            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedButton2.Enabled.ToString() + "." + "\\r\\nBackColor value: " + TestedButton2.BackColor.ToString() + ".";

        }

        public void TestedButton1_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton1 = this.GetVisualElementById<ButtonElement>("TestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedButton1.BackColor == Color.Green)
            {
                TestedButton1.BackColor = Color.Orange;
                lblLog1.Text = "Enabled value: " + TestedButton1.Enabled.ToString() + "\\r\\nBackColor value: " + TestedButton1.BackColor.ToString();
            }
            else
            {
                TestedButton1.BackColor = Color.Green;
                lblLog1.Text = "Enabled value: " + TestedButton1.Enabled.ToString() + "\\r\\nBackColor value: " + TestedButton1.BackColor.ToString();

            }
        }

        public void TestedButton2_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedButton2.BackColor == Color.Orange)
            {
                TestedButton2.BackColor = Color.Green;
                lblLog2.Text = "Enabled value: " + TestedButton2.Enabled.ToString() + "." + "\\r\\nBackColor value: " + TestedButton2.BackColor.ToString() + ".";
            }
            else
            {
                TestedButton2.BackColor = Color.Orange;
                lblLog2.Text = "Enabled value: " + TestedButton2.Enabled.ToString() + "." + "\\r\\nBackColor value: " + TestedButton2.BackColor.ToString() + ".";

            }
        }

        private void btnChangeEnabledValue_Click(object sender, EventArgs e)
        {
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedButton2.Enabled = !TestedButton2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedButton2.Enabled.ToString() + "." + "\\r\\nBackColor value: " + TestedButton2.BackColor.ToString() + ".";
        }
      
    }
}