using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class DateTimePickerBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public async void button1_Click(object sender, EventArgs e)
        {

            DialogResult result = await MessageBox.Show("Text", "Title", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            await MessageBox.Show(String.Format("User Clicked {0} button", result), "Messege Box Button Clicked", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


        //btnSetValue_Click
        public void btnSetValue_Click(object sender, EventArgs e)
        {
            DateTimePickerElement DateTimePicker1 = this.GetVisualElementById<DateTimePickerElement>("DateTimePicker1");
            DateTimePicker1.Value = new DateTime(2016, 11, 8, 0, 0, 0, 0);

            
        }

        public void btnGetValue_Click(object sender, EventArgs e)
        {
           
            DateTimePickerElement DateTimePicker1 = this.GetVisualElementById<DateTimePickerElement>("DateTimePicker1");
            MessageBox.Show(DateTimePicker1.Value.ToString());

        }

        public void btnChangeEditable_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeEditable = this.GetVisualElementById<ButtonElement>("btnChangeEditable");
            DateTimePickerElement DateTimePicker1 = this.GetVisualElementById<DateTimePickerElement>("DateTimePicker1");

            DateTimePicker1.Editable = !DateTimePicker1.Editable;
            btnChangeEditable.Text = "DateTime Editable: " + DateTimePicker1.Editable; 


            

        }
    }
}