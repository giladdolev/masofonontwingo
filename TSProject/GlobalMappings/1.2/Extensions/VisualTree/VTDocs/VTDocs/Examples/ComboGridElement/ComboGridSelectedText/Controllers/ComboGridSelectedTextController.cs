using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridSelectedTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ///Add another for Remarks  - msdn contains a lot of explanations for the SelectedText property

            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            DataTable source1 = new DataTable();
            source1.Columns.Add("Column1", typeof(string));
            source1.Columns.Add("Column2", typeof(string));
            source1.Columns.Add("Column3", typeof(int));
            source1.Rows.Add("James", "Dean", 1);
            source1.Rows.Add("Bob", "Marley", 2);
            source1.Rows.Add("Dana", "International", 3);

            TestedComboGrid.ValueMember = "Column1";
            TestedComboGrid.DisplayMember = "Column3";

            TestedComboGrid.DataSource = source1;

            TestedComboGrid.DataSource = source1;

            lblLog.Text = "SelectedIndex Value: " + TestedComboGrid.SelectedIndex.ToString();


        }


        public void btnSetSelectedText_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboGrid.SelectedText = "New Text";

            lblLog.Text = "SelectedText value: " + TestedComboGrid.SelectedText.ToString();

        }

        public void btnGetSelectedText_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "SelectedText value: " + TestedComboGrid.SelectedText.ToString();

        }


    }
}