<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Size Property" ID="windowView1"  LoadAction="CheckBoxSize/OnLoad">
         
        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the controlin pixels.
            
            Syntax: public Size Size { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="Size property of this CheckBox is initially set to:  Width='300px' Height='80px'" Top="240px"  ID="lblExp"></vt:Label>     

        <vt:CheckBox runat="server" SkinID="BackColorCheckBox" Text="TestedCheckBox" Top="300px" ID="TestedCheckBox" Height="80px" Width="300px" TabIndex="3"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Top="395px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Top="460px" ID="btnChangeSize" TabIndex="1" Width="180px" ClickAction="CheckBoxSize\btnChangeSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
