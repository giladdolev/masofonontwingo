using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedToolBar.ClientSize;

        }
        public void btnChangeToolBarClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            if (TestedToolBar.Width == 200)
            {
                TestedToolBar.ClientSize = new Size(400, 40);
                lblLog.Text = "ClientSize value: " + TestedToolBar.ClientSize;

            }
            else
            {
                TestedToolBar.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedToolBar.ClientSize;
            }

        }

    }
}