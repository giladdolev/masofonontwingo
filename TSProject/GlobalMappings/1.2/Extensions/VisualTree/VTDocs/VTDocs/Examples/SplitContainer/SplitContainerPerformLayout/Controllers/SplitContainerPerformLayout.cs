using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");
            LayoutEventArgs args = new LayoutEventArgs(testedSplitContainer, "Some String");

            testedSplitContainer.PerformLayout(args);
        }

        public void testedSplitContainer_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedSplitContainer Layout event method is invoked");
        }

    }
}