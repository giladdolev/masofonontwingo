using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MainPageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btn1_click(object sender, EventArgs e)
        {
            PanelElement Panel1 = this.GetVisualElementById<PanelElement>("Panel1");
            if (Panel1 != null)
            {
                Dictionary<string, object> argsDictionary = GetArgsDictionary();
                Panel1.Controls.Clear();
                ControlElement elementView = VisualElementHelper.CreateFromView<ControlElement>("CompositeWindow2", "CompositeWindow2", null, 
                    argsDictionary, null);
                if (elementView != null)
                {
                    Panel1.Controls.Add(elementView);
                }
            }
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "MDIWindowContextMenuStrip");
            return argsDictionary;
        }

        public void btn2_click(object sender, EventArgs e)
        {
            PanelElement Panel1 = this.GetVisualElementById<PanelElement>("Panel1");
            CompositeElement cmp = Panel1.Controls[0] as CompositeElement;
            CancelEventArgs args = new CancelEventArgs();
            cmp.PerformUnload(args);
            Panel1.Refresh();
            
           
        }
    }
}