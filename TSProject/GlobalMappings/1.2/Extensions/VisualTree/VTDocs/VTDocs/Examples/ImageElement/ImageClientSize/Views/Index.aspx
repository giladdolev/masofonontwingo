
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image ClientSize Property" ID="windowView" LoadAction="ImageClientSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the client area of the control.

            Syntax: public Size ClientSize { get; set; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="lblDefinition" ></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="235px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The ClientSize property of this 'TestedImage' is initially set with 
            Width: 150px, Height: 90px" Top="275px"  ID="lblExp1"></vt:Label>     

        <!-- TestedImage -->
        <vt:Image runat="server" Text="TestedImage" Top="345px" ID="TestedImage"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientSize value >>"  Top="515px" Width="180px" ID="btnChangeImageClientSize" ClickAction="ImageClientSize\btnChangeImageClientSize_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>




