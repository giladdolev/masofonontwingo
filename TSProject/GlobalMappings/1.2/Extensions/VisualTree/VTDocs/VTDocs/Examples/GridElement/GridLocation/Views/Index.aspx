<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Location Property" ID="windowView" LoadAction="GridLocation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Location" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the coordinates of the upper-left corner of the control 
            relative to the upper-left corner of its container.

            Syntax: public Point Location { get; set; }" Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The Location of this Grid is initially set with Left: 80px and Top: 300px" Top="250px"  ID="lblExp1"></vt:Label>     

         <!-- TestedGrid -->
        <vt:Grid runat="server" Top="300px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Grid Location >>"  Top="495px" Width="180px" ID="btnChangeGridLocation" ClickAction="GridLocation\btnChangeGridLocation_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>
