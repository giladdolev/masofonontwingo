<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
        <vt:WindowView runat="server" Text="Tree TreeParentFieldName Property" Height="800px" ID="windowView" LoadAction="TreeParentFieldName\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="250px" Text="TreeParentFieldName" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value specifying the key field of the data source bound to the Tree List control.

            Syntax: public string TreeParentFieldName { get; set; }
            Property value: A string value representing the name of the field used as the unique record identifier. 
            
            The KeyFieldName and ParentFieldName properties specify fields from the bound data source that are 
            used to represent data in a tree-like structure. The values of the field specified by the KeyFieldName
             property uniquely identify each record. The values from the field specified by the ParentFieldName
             property determine the parent of the record by the key field. Therefore, if the parent field value of 
            a record is 3, this means that the record is the child of the record whose key field value is 3." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example the TreeList is populated by Data source the KeyFieldName and
             ParentFieldName are set to ID Column and ParentID column respectively. You can change these
             properties at runtime by clicking the button bellow to switch DataSource " Top="350px"  ID="lblExp1"></vt:Label>     

        <vt:Tree runat="server" ID="tree1" SkinID="TreeList" Top="430px">
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="645px" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Switch DataSource >>" Top="710px" ID="btnSwitchDataSource" ClickAction="TreeParentFieldName\btnSwitchDataSource_Click"></vt:Button>
             
    </vt:WindowView>
</asp:Content>
