<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic NumericUpDown" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Initialize - NumericUpDown RightToLeft is set to'yes'" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

     
        <vt:NumericUpDown runat="server"  Text="" RightToLeft="Yes" Top="45px" Left="140px" ID="chkRightToLeft" Height="20px" TabIndex="2" Width="100px"></vt:NumericUpDown>
      

         <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>
     
        <vt:NumericUpDown runat="server"  Text="" Top="115px" Left="140px" ID="testedNumericUpDown" Height="20px"  TabIndex="3" Width="100px"></vt:NumericUpDown>

         <vt:button runat="server"  Text="Change NumericUpDown RightToLeft" Top="115px" Left="140px" ID="btnChangeRightToLeft" Height="36px"  TabIndex="3" Width="200px" ClickAction="NumericUpDownRightToLeft\btnChangeRightToLeft_Click" ></vt:button>

        <vt:TextBox runat="server" Text="" Top="200px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>


    </vt:WindowView>
</asp:Content>
