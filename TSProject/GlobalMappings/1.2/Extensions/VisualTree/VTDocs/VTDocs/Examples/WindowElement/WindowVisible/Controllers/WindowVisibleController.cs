using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowVisibleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowVisible());
        }
        private WindowVisible ViewModel
        {
            get { return this.GetRootVisualElement() as WindowVisible; }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
		{
            
            if (this.ViewModel.TestedWin == null)
            {
                Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
                argsDictionary.Add("ElementName", "WindowElement");
                argsDictionary.Add("ExampleName", "WindowVisible");

                ViewModel.TestedWin = VisualElementHelper.CreateFromView<VisibleTestedWindow>("VisibleTestedWindow", "Index2", null, argsDictionary, null);
                this.ViewModel.TestedWin.Show();
            }
            else if(!this.ViewModel.TestedWin.Visible)
                 this.ViewModel.TestedWin.Show();
		}

        public void btnChangeVisible_Click(object sender, EventArgs e)
        {

            if (this.ViewModel.TestedWin != null)
            {
                TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
                if (this.ViewModel.TestedWin.Visible)
                {
                    this.ViewModel.TestedWin.Visible = false;
                    textBox1.Text = "Visible = " + this.ViewModel.TestedWin.Visible.ToString();
                }
                    
                else
                {
                    this.ViewModel.TestedWin.Visible = true;
                    textBox1.Text = "Visible = " + this.ViewModel.TestedWin.Visible.ToString();

                }
            }

        }

    }
}