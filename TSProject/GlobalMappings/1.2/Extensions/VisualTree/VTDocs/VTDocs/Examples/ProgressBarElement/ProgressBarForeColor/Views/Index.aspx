<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar ForeColor property" ID="windowView1" LoadAction="ProgressBarForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested ProgressBar1 -->
        <vt:ProgressBar runat="server" Text="ProgressBar" Top="165px" ID="TestedProgressBar1"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Top="215px" BorderStyle="None" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="295px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of this ProgressBar is initially set to Green" Top="345px" ID="lblExp1"></vt:Label>

        <!-- Tested ProgressBar2 -->
        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="380px" ForeColor="Green" ID="TestedProgressBar2"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="510px" Width="180px" ID="btnChangeProgressBarForeColor" ClickAction="ProgressBarForeColor\btnChangeProgressBarForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
