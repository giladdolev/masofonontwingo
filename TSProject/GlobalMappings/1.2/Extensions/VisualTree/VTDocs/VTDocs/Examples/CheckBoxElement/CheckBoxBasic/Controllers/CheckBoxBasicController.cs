using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        // Change CheckState property
        /// <summary>
        /// Handles the Click event of the btnCheckState control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnCheckState_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkCheckStateRunTime = this.GetVisualElementById<CheckBoxElement>("chkCheckStateRunTime");
            if (chkCheckStateRunTime.CheckState == CheckState.Unchecked)
            {
                chkCheckStateRunTime.CheckState = CheckState.Checked;
            }
            else if (chkCheckStateRunTime.CheckState == CheckState.Checked)
            {
                chkCheckStateRunTime.CheckState = CheckState.Indeterminate;
            }
            else if (chkCheckStateRunTime.CheckState == CheckState.Indeterminate)
            {
                chkCheckStateRunTime.CheckState = CheckState.Unchecked;
            }
        }
        /// <summary>
        /// Handles the Click event of the btnChangeFontStyle control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnChangeFontStyle_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkFontStyle = this.GetVisualElementById<CheckBoxElement>("chkFontStyle");
            if (chkFontStyle.Font.Underline == true)
            {
                chkFontStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Italic);

            }
            else if (chkFontStyle.Font.Bold == true)
            {
                chkFontStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Underline);

            }
            else if (chkFontStyle.Font.Italic == true)
            {
                chkFontStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Strikeout);

            }
            else
            {
                chkFontStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Bold);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnChangeLocation control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnChangeLocation_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkLocation = this.GetVisualElementById<CheckBoxElement>("chkLocation");
            //317, 372 - 318, 318
            if (chkLocation.Location == new Point(318, 318))
            {
                chkLocation.Location = new Point(317, 372);
            }
            else
            {
                chkLocation.Location = new Point(318, 318);
            }
        }
        // Change CheckAlign property
        /// <summary>
        /// Handles the Click event of the btnChangeCheckAlign control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnChangeCheckAlign_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkCheckAlignRunTime = this.GetVisualElementById<CheckBoxElement>("chkCheckAlignRunTime");
            if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.MiddleLeft)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.MiddleRight;

            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.MiddleRight)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.MiddleCenter;

            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.MiddleCenter)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.BottomCenter;

            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.BottomCenter)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.BottomLeft;


            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.BottomLeft)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.BottomRight;

            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.BottomRight)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.TopCenter;

            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.TopCenter)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.TopLeft;

            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.TopLeft)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.TopRight;

            }
            else if (chkCheckAlignRunTime.CheckAlign == ContentAlignment.TopRight)
            {
                chkCheckAlignRunTime.CheckAlign = ContentAlignment.MiddleLeft;

            }
        }
    }
}