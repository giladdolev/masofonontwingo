<%@ Page Title="BasicFileSystemDialog" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="BasicFyleSystemDialog Property" ID="windowView1">

        <vt:Label runat="server" SkinID="Title" Text="BasicFyleSystemDialog" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Show behavior of compatibility FileListBox" Top="75px"  ID="lblDefinition"></vt:Label>
      
        <!-- Actual sample element(s) should be placed instead of button below -->

        <vt:DirectoryListBox runat="server" ID="dirlb1" Path="c:\" Top="100px" Left="50px" Width="650px" Height="350px"  ChangedAction="DirectoryListBoxBasic\SelectedDirectoryChanged"></vt:DirectoryListBox> 

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog1"></vt:Label>
 
    </vt:WindowView>
</asp:Content>
