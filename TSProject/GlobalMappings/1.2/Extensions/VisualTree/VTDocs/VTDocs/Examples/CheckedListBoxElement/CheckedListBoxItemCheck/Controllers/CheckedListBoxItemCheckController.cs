using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckedListBoxItemCheckController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        private void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Check\\Uncheck CheckedListBoxItemCheck CheckBoxes";
        }


        public void testedCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            int currentCheckBox = e.Index + 1;
            lblLog.Text = "CheckBox" + currentCheckBox.ToString() + " is " + e.State;
            txtEventLog.Text += "\\r\\nItemChecked event has been raised.";
        }
    }
}