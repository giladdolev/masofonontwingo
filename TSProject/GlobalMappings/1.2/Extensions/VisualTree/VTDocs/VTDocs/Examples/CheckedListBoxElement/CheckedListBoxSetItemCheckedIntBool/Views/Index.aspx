﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckedListBox SetItemChecked Method (Int32, Boolean)" Height="750px" ID="windowView1" LoadAction="CheckedListBoxSetItemCheckedIntBool\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SetItemChecked" Left="270px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets CheckState for the item at the specified index to Checked.

            Syntax: public void SetItemChecked(int index, bool isChecked)
            index : The index of the item to set the check state for.
            isChecked: 'true' to set the item as checked; otherwise, 'false'.

            Remarks: When a value of 'true' is passed, this method sets the CheckState value to Checked.
             A value of 'false' sets CheckState to Unchecked."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="255px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Use the button below to invoke SetItemChecked method with the index 
            and CheckState inputs.
             "
            Top="295px" ID="lblExp2">
        </vt:Label>

        <vt:CheckedListBox runat="server" ID="testedCheckedListBox" Height="120px" Top="355px" >
            <Items>
                <vt:ListItem runat="server" ID="listItem1" Text="CheckBox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem2" Text="CheckBox 2"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem3" Text="CheckBox 3"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem4" Text="CheckBox 4"></vt:ListItem>
                <vt:ListItem runat="server" ID="listItem5" Text="CheckBox 5"></vt:ListItem>
            </Items>
        </vt:CheckedListBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Height="100px" Top="490px" ></vt:Label>

        <vt:Label runat="server" Text="Select Index:" Left="80px"  ID="lblSelectIndex" Height="80px" Top="630px"></vt:Label>

         <vt:ComboBox runat="server" Width="110px" CssClass="vt-cmbSelectIndex" Text="Select Index" Top="655px" ID="cmbSelectIndex" SelectedIndexChangedAction="CheckedListBoxSetItemCheckedIntBool\cmbSelectIndex_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="0"></vt:ListItem>
                <vt:ListItem runat="server" Text="1"></vt:ListItem>
                <vt:ListItem runat="server" Text="2"></vt:ListItem>
                <vt:ListItem runat="server" Text="3"></vt:ListItem>
                <vt:ListItem runat="server" Text="4"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:CheckBox runat="server" Top="655px" Left="230px" ID="chkCheckState" Text="CheckState"></vt:CheckBox>

        <vt:Button runat="server" Text="Invoke SetItemChecked(Int32, Boolean) >>" Top="655px" Left="370px"  ID="btnSetItemChecked" Width="250px" ClickAction="CheckedListBoxSetItemCheckedIntBool\btnSetItemChecked_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
