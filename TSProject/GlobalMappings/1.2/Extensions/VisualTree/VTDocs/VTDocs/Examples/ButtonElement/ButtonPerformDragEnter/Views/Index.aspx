<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button PerformDragEnter() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformDragEnter of 'Tested Button'" Top="45px" Left="140px" ID="btnPerformDragEnter" Height="36px" Width="220px" ClickAction="ButtonPerformDragEnter\btnPerformDragEnter_Click"></vt:Button>


        <vt:Button runat="server" Text="Tested Button" Top="150px" Left="200px" ID="btnTestedButton" Height="36px"  Width="100px" DragEnterAction="ButtonPerformDragEnter\btnTestedButton_DragEnter"></vt:Button>           

        

    </vt:WindowView>
</asp:Content>
