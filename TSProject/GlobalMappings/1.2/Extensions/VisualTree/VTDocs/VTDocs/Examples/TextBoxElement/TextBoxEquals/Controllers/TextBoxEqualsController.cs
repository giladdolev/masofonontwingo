using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TextBoxEquals());
        }

        private TextBoxEquals ViewModel
        {
            get { return this.GetRootVisualElement() as TextBoxEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TextBoxElement testedTextBox1 = this.GetVisualElementById<TextBoxElement>("testedTextBox1");
            TextBoxElement testedTextBox2 = this.GetVisualElementById<TextBoxElement>("testedTextBox2");
            this.ViewModel.TestedTextBox.Top = 10;
            this.ViewModel.TestedTextBox.Height = 15;

            if (this.ViewModel.TestedTextBox.Equals(testedTextBox1))
            {
                this.ViewModel.TestedTextBox = testedTextBox2;
                textBox1.Text = "TestedTextBox Equals TextBox2";
            }
            else
            {
                this.ViewModel.TestedTextBox = testedTextBox1;
                textBox1.Text = "TestedTextBox Equals TextBox1";
            }

            
        }
        

        public void Load(object sender, EventArgs e)
        {
            this.ViewModel.Controls.Add(this.ViewModel.TestedTextBox);
        }



    }
}