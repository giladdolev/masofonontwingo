using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Change BackColor
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Top value: " + TestedLabel.Top + '.';
        }

        public void btnChangeTop_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedLabel.Top == 310)
            {
                TestedLabel.Top = 290;
                lblLog.Text = "Top value: " + TestedLabel.Top + '.';
            }
            else
            {
                TestedLabel.Top = 310;
                lblLog.Text = "Top value: " + TestedLabel.Top + '.';
            }

        }
    }
}