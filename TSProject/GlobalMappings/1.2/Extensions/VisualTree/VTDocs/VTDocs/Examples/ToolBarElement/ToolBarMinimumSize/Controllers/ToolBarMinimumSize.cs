using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ToolBar MinimumSize value: " + TestedToolBar.MinimumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
        }

        public void btnChangeTlbMinimumSize_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedToolBar.MinimumSize == new Size(400, 50))
            {
                TestedToolBar.MinimumSize = new Size(350, 70);
                lblLog1.Text = "ToolBar MinimumSize value: " + TestedToolBar.MinimumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
            }
            else if (TestedToolBar.MinimumSize == new Size(350, 70))
            {
                TestedToolBar.MinimumSize = new Size(250, 40);
                lblLog1.Text = "ToolBar MinimumSize value: " + TestedToolBar.MinimumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
            }
            else
            {
                TestedToolBar.MinimumSize = new Size(400, 50);
                lblLog1.Text = "ToolBar MinimumSize value: " + TestedToolBar.MinimumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;
            }

        }

        public void btnSetToZeroTlbMinimumSize_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedToolBar.MinimumSize = new Size(0, 0);
            lblLog1.Text = "ToolBar MinimumSize value: " + TestedToolBar.MinimumSize + "\\r\\nToolBar Size value: " + TestedToolBar.Size;

        }


    }
}