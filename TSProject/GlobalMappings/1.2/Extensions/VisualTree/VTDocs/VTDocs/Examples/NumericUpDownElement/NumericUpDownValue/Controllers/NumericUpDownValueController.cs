using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownValueController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "The default value is: " + TestedNumericUpDown1.Value.ToString(); //default value
            lblLog2.Text = "You can enter a value or change the value.";

        }


        //Change programmatically the value of TestedNumericUpDown2 (value between minimum and maximum range, 1-10) 
        private void btnChangeValueTo50_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedNumericUpDown2.Value = 50;

            lblLog2.Text = "The value is: " + TestedNumericUpDown2.Value.ToString() + ".";

        }

        public void TestedNumericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "The Value is: " + TestedNumericUpDown2.Value.ToString() + ".";
        }


        //Change the maximum value to 1500 - To check bug 
        private void btnChangeMaximumValueTo1500_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedNumericUpDown2.Maximum = 1500;

            lblLog2.Text = "The maximum value is: " + TestedNumericUpDown2.Maximum.ToString() + ".";

        }

    }
}