using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPerformDoubleClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformDoubleClick_Click(object sender, EventArgs e)
        {
            GroupBoxElement testedGroupBox = this.GetVisualElementById<GroupBoxElement>("testedGroupBox");           

            testedGroupBox.PerformDoubleClick(e);
        }

        public void testedGroupBox_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("TestedGroupBox DoubeleClick event method is invoked");
        }

    }
}