using System;
using System.Data;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ComboGridElement cmb1 = this.GetVisualElementById<ComboGridElement>("cmgrd");
            DataTable source = new DataTable();
            source.Columns.Add("text", typeof(int));
            source.Columns.Add("value", typeof(string));
            source.Columns.Add("Field3", typeof(string));
            source.Rows.Add(1, "J", "Dean");
            source.Rows.Add(2, "B", "Marley");
            source.Rows.Add(3, "D", "International");
            source.Rows.Add(4, "S", "");
            cmb1.DisplayMember = "text";
            cmb1.ValueMember = "value";
            cmb1.DataSource = source;
         
        }


        public void select(object sender, GridCellMouseEventArgs e){
        
            ComboGridElement comboBox1 = this.GetVisualElementById<ComboGridElement>("cmgrd");

        }

        public void textChanged(object sender, EventArgs e)
        {
            ComboGridElement comboBox1 = this.GetVisualElementById<ComboGridElement>("cmgrd");

        }

        public void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            ComboGridElement comboBox1 = this.GetVisualElementById<ComboGridElement>("cmgrd");
        }
    }
}