<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar SizeChanged Property" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        
        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="100px" Left="140px" ID="prgSize" Height="50px" Width="200px" SizeChangedAction="ProgressBarSizeChanged\grpSize_SizeChanged" >           
        </vt:ProgressBar>

        <vt:Button runat="server" Text="Change ProgressBar Size" Top="110px" Left="420px" ID="btnChangeSize" Height="36px" Width="200px" ClickAction="ProgressBarSizeChanged\btnChangeSize_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="200px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

        <vt:Label runat="server" Text="Event Log" Top="280px" Left="140px"  ID="label3"   Height="15px" Width="200px"> 
            </vt:Label>

        <vt:TextBox runat="server" Text="" Top="295px" Left="140px"  ID="txtEventLog" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>
    </vt:WindowView>
</asp:Content>
        