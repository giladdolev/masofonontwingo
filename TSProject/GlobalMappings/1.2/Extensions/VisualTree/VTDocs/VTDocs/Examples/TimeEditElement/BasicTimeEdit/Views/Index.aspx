<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic TimeEdit" ID="windowView1" LoadAction="BasicTimeEdit\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="TimeEdit" ID="Label1" ></vt:Label>

        <vt:Label runat="server" Text="TimeEdit is a control that makes it easier for end-users to input time values.
            The value of TimeEdit can be selected from a dropdown list"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example shows basic usage of TimeEdit class.
            The time format is defined as 'h:mm tt', The range of the time values: MinTime='6:00 AM', 
            maxTime='11:00 AM', and the Increment step is set to 30 seconds. 
            You can open the dropdown list to view the input time values." Top="205px" ID="lblExp2">
        </vt:Label>

        <vt:TimeEdit runat="server" Top="325px" Left="50px" CssClass="vt-test-te" MinTime="6:00 AM" maxTime="11:00 AM" Increment="30"></vt:TimeEdit>

    </vt:WindowView>

</asp:Content>
