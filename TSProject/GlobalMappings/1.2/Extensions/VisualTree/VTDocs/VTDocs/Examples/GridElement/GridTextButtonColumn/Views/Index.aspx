﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Grid GridTextButtonColumn" ID="windowView2" LoadAction="GridTextButtonColumn\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="230px" Text="GridTextButtonColumn" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Hosts a collection of DataGridViewButtonCell objects." 

            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example there is a basic example of TextButtonColumn in the Grid cells."
            Top="200px" ID="Label4">
        </vt:Label>
       

          <vt:Grid runat="server" CellMouseDownAction="BasicGrid/TestFunc" AllowUserToDeleteRows="False" Top="270px" Height="230px" Width="340px" Left="50px" ID="gridElement"  LostFocusAction="True" ContextMenuStripID="contextMenuStripCells" RowCount="3">
            <Columns>
                <vt:GridTextBoxColumn ID="txtclm" runat="server" HeaderText="TextBox Column" DataMember="TextColumn" ></vt:GridTextBoxColumn>

                <vt:GridTextButtonColumn ID="GridTextButtonColumn1" ButtonText="btn" runat="server" HeaderText="TextButton Column" DataMember="TextButtonColumn" ClickAction="BasicGridGridCheckBoxColumn\coulmns_ButtonColumnClicked"></vt:GridTextButtonColumn>
         
            </Columns>
        </vt:Grid>

    </vt:WindowView>
</asp:Content>