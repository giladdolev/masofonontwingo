﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridSelectionChanged2TextBoxColumnQAController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            GridElement testedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");

            testedGrid2.Rows.Add("j", "k", "l");
            testedGrid2.Rows.Add("m", "n", "o");
            testedGrid2.Rows.Add("p", "q", "r");

            lblLog.Text = "Left and Right Grids: SelectedCells values: Empty";
        }

        public void TestedGrid_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nLeft Grid: BeforeSelectionChanged event is invoked";
        }

        public void TestedGrid2_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nRight Grid: BeforeSelectionChanged event is invoked";
        }

        public void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nLeft Grid: SelectionChanged event is invoked";

            lblLog.Text = "Left Grid: SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }

        public void TestedGrid2_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nRight Grid: SelectionChanged event is invoked";

            lblLog.Text = "Right Grid: SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }

        public void btnSelectd_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[1].Cells[0].Selected = true;

            lblLog.Text = "Left Grid: SelectedCells values: (" + testedGrid.Rows[1].Cells[0].Value + ") ";
        }

        public void btnSelectr_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[2].Cells[2].Selected = true;

            lblLog.Text = "Right Grid: SelectedCells values: (" + testedGrid.Rows[2].Cells[2].Value + ") ";
        }

        public void btnClear_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();

            txtEventLog.Text = "Event Log:";
        }

        public void btnSelectRow0_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            GridElement testedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[0].Selected = true;
            testedGrid2.Rows[0].Selected = true;
        }

        public void btnSelectColumn0_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            GridElement testedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Columns[0].Selected = true;
            testedGrid2.Columns[0].Selected = true;
        }
    }
}
