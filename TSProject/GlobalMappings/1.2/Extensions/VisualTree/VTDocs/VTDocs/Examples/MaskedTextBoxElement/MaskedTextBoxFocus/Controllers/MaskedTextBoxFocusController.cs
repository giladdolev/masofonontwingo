using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MaskedTextBoxFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Click on the button...";
        }

        public void Focus_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedMaskedTextBox.Focus();
            lblLog.Text = "Focus method was invoked.";
           //Bug 23976 - after fix, uncomment the below line
           //lblLog.Text = "The return value is: " + TestedMaskedTextBox.Focus() + ".";
        }

        public void MaskedTextBox_LostFocus(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

           // txtEventLog.Text += "\\r\\nButton1 LostFocus event is invoked";
            lblLog.Text = "Click on the button...";
        }

        public void btnLoseFocus_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Control lost focus.";
        }     


    }
}