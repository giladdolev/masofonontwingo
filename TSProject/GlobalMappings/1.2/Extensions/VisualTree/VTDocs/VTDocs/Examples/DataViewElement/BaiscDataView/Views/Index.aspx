<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="DataViewElement" ID="windowView1" Width="800" Height="800">
        <vt:DataView runat="server" DataSource="<%#Model%>" Width="1000px" Height="1000px" Top="0px" Left="0px" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="WindowText" TabIndex="2">
            <Bands>
                <vt:DataViewBand runat="server" Height="40" Type="Header">
                      
                    <Fields>
                        <vt:DataViewLabelField runat="server" Text="ID" TemplateText="ID" Visible="True" Left="0px" Top="2px" Alignment="Center" ID="state_id_t" ForeColor="WindowText" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" Height="25px" Width="50px" ></vt:DataViewLabelField>
                        <vt:DataViewLabelField runat="server" Text="First Name" TemplateText="Name" Visible="True" Left="50px" Top="2px" Alignment="Center" ID="state_name_t" ForeColor="WindowText" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" Height="25px" Width="90px"></vt:DataViewLabelField>
                        <vt:DataViewLabelField runat="server" Text="Last Name" TemplateText="Country" Visible="True" Left="120px" Top="2px" Alignment="Center" ID="country_t" ForeColor="WindowText" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" Height="25px" Width="80px" ></vt:DataViewLabelField>
                    </Fields>
                </vt:DataViewBand>

                <vt:DataViewBand runat="server" Height="50px" Width="1000px" Type="Details">
                    <Fields>
                        <vt:DataViewTextField runat="server" DataMember="ID" Visible="True" Left="0px" Top="8px" Alignment="Center" ID="DataViewLabelField1" ForeColor="WindowText" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="20pt" Height="100px" Width="50px"></vt:DataViewTextField>

                        <vt:DataViewTextField runat="server" Visible="True" DataMember="FirstName" ReadOnly="false" Left="30px" Top="8px" Alignment="Center" ID="Text1" ForeColor="WindowText" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="20pt" Height="60px" Width="80px"></vt:DataViewTextField>

                        <vt:DataViewTextField runat="server" DataMember="LastName" Visible="True" Left="100px" Top="8px" Alignment="Center" ID="DataViewLabelField2" ForeColor="WindowText" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="20pt" Height="100px" Width="50px"></vt:DataViewTextField>

                        <vt:DataViewComboField runat="server" DataMember="Options" ReadOnly="false" Visible="True" Left="100px" Top="8px" Alignment="Center" ID="DataViewCombo1" ForeColor="WindowText" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="20pt" Height="100px" Width="50px"></vt:DataViewComboField>

                    </Fields>
                </vt:DataViewBand>

                <vt:DataViewBand runat="server" Height="100px" Type="Summary">
                    <Fields>
                    </Fields>
                </vt:DataViewBand>
                <vt:DataViewBand runat="server" Height="300px" Type="Footer">
                </vt:DataViewBand>
            </Bands>
        </vt:DataView>
        <vt:DataViewButtonField runat="server" Text="OK" Top="88px" Left="500px" BorderStyle="None" Font-Bold="True" Font-Names="Tahoma" Font-Size="10pt" Height="24px" TabIndex="30" Width="88px" ClickAction="w_welcome\cb_ok_clicked"></vt:DataViewButtonField>
    </vt:WindowView>
</asp:Content>
