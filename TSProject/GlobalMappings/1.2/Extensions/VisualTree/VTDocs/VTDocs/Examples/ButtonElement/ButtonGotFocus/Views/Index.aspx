<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button GotFocus event" ID="windowView2" LoadAction="ButtonGotFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="GotFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control receives focus.

            Syntax: public event EventHandler GotFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted into one of the Buttons,
             a 'GotFocus' event will be invoked.
             Each invocation of the 'GotFocus' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Height="50px" Width="505px">
            <vt:Button runat="server" Top="15px" Left="15px" Text="Button1" ID="btnButton1" TabIndex="1" GotFocusAction="ButtonGotFocus\btnButton1_GotFocus"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="175px" Text="Button2" ID="btnButton2" TabIndex="2" GotFocusAction="ButtonGotFocus\btnButton2_GotFocus"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="335px" Text="Button3" ID="btnButton3" TabIndex="3" GotFocusAction="ButtonGotFocus\btnButton3_GotFocus"></vt:Button>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="400px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="440px" Width="360px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Focus Button1 >>" Width="180px" Top="570px" TabIndex="4" ID="btnChangeFocus" GotFocusAction="ButtonGotFocus\btnChangeFocus_GotFocus" ClickAction="ButtonGotFocus\btnChangeFocus_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
