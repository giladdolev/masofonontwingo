<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button MouseTapHold event" ID="windowView2" LoadAction="ButtonMouseTapHold\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MouseTapHold" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when holding the mouse's left click for more than 1 second.

            Syntax: public event EventHandler<MouseEventArgs> MouseTapHold"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time one of the Buttons is long clicked it's MouseTapHold event
             is invoked and it's background color changes.
             Clicking the button less than a second will invoke the Click event . 
            A line is add to the Event log below at each event invocation 
            "
            Top="235px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Height="50px" Width="505px">
            <vt:Button runat="server" Top="15px" Left="15px" Text="Button1" ID="btnButton1" TabIndex="1" MouseTapHoldAction="ButtonMouseTapHold\btnButton1_MouseTapHold" ClickAction="ButtonMouseTapHold\btnButton1_Click"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="175px" Text="Button2" ID="btnButton2" TabIndex="2" MouseTapHoldAction="ButtonMouseTapHold\btnButton2_MouseTapHold" ClickAction="ButtonMouseTapHold\btnButton2_Click"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="335px" Text="Button3" ID="btnButton3" TabIndex="3" MouseTapHoldAction="ButtonMouseTapHold\btnButton3_MouseTapHold" ClickAction="ButtonMouseTapHold\btnButton3_Click"></vt:Button>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Height="60px" Top="400px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Height="150px" Top="500px" Width="360px" ID="txtEventLog"></vt:TextBox>

        

    </vt:WindowView>
</asp:Content>
