using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridCellBorderStyleController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {   
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "CellBorderStyle value: " + TestedGrid1.CellBorderStyle.ToString();

            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "CellBorderStyle value: " + TestedGrid2.CellBorderStyle.ToString();

            GridElement TestedGrid3 = this.GetVisualElementById<GridElement>("Grid2");
            LabelElement lblLog3 = this.GetVisualElementById<LabelElement>("Label4");
            TestedGrid3.Rows.Add("a", "b", "c");
            TestedGrid3.Rows.Add("d", "e", "f");
            TestedGrid3.Rows.Add("g", "h", "i");
            lblLog3.Text = "CellBorderStyle value: " + TestedGrid3.CellBorderStyle.ToString();

            GridElement TestedGrid4 = this.GetVisualElementById<GridElement>("Grid1");
            LabelElement lblLog4 = this.GetVisualElementById<LabelElement>("Label3");
            TestedGrid4.Rows.Add("a", "b", "c");
            TestedGrid4.Rows.Add("d", "e", "f");
            TestedGrid4.Rows.Add("g", "h", "i");
            lblLog4.Text = "CellBorderStyle value: " + TestedGrid4.CellBorderStyle.ToString();

            WindowElement win = this.GetRootVisualElement() as WindowElement;
        }
	}
}
