using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TextBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested TextBox bounds are set to: \\r\\nLeft  " + TestedTextBox.Left + ", Top  " + TestedTextBox.Top + ", Width  " + TestedTextBox.Width + ", Height  " + TestedTextBox.Height;

        }
        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            if (TestedTextBox.Left == 100)
            {
                TestedTextBox.SetBounds(80, 300, 150, 26);
                lblLog.Text = "The Tested TextBox bounds are set to: \\r\\nLeft  " + TestedTextBox.Left + ", Top  " + TestedTextBox.Top + ", Width  " + TestedTextBox.Width + ", Height  " + TestedTextBox.Height;

            }
            else
            {
                TestedTextBox.SetBounds(100, 280, 200, 50);
                lblLog.Text = "The Tested TextBox bounds are set to: \\r\\nLeft  " + TestedTextBox.Left + ", Top  " + TestedTextBox.Top + ", Width  " + TestedTextBox.Width + ", Height  " + TestedTextBox.Height;
            }
        }

    }
}