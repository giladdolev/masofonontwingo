<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox KeyUp event" ID="windowView2" LoadAction="TextBoxKeyUp\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="KeyUp" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when a key is released while the control has focus.

            Syntax: public event KeyEventHandler KeyUp"
            Top="75px" ID="lblDefinition">
        </vt:Label>

         <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the focus by pressing TAB key, or by pressing the Enter key.
             Each invocation of the 'KeyDown','KeyPress','KeyUp' event should add a line in the 'Event Log'."
            Top="195px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="255px" Height="110px" Width="340px">
            <vt:Label runat="server" Text="First Name :" Left="15px" Top="20px" ID="Label1"></vt:Label>

            <vt:TextBox runat="server" Top="20px" Left="160px" Text="Enter First Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtFirstName" TabIndex="1" 
                KeyDownAction="TextBoxKeyUp\txtFirstName_KeyDown" KeyPressAction="TextBoxKeyUp\txtFirstName_KeyPress" KeyUpAction="TextBoxKeyUp\txtFirstName_KeyUp" TextChangedAction="TextBoxKeyUp\txtFirstName_TextChanged"></vt:TextBox>

            <vt:Label runat="server" Text="Last Name :" Left="15px" Top="65px" ID="Label2"></vt:Label>

            <vt:TextBox runat="server" Top="65px" Left="160px" Text="Enter Last Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtLastName" TabIndex="2" 
                KeyDownAction="TextBoxKeyUp\txtLastName_KeyDown" KeyPressAction="TextBoxKeyUp\txtLastName_KeyPress" KeyUpAction="TextBoxKeyUp\txtLastName_KeyUp" TextChangedAction="TextBoxKeyUp\txtLastName_TextChanged"></vt:TextBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" Height="40px" ID="lblLog" Top="380px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="435px" Width="360px" Height="200px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Clear Event Log >>" Width="180px" Top="650px" TabIndex="3" ID="btnClearEventLog" ClickAction="TextBoxKeyUp\btnClearEventLog_Click"  GotFocusAction="TextBoxKeyUp\btnClearEventLog_GotFocus"></vt:Button>

    </vt:WindowView>
</asp:Content>


