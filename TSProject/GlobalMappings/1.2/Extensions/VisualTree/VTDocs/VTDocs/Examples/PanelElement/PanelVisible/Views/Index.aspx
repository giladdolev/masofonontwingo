<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="Panel Visible Property" ID="windowView1" LoadAction="PanelVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Panel1 -->
        <vt:Panel runat="server" Text="Panel" Top="170px" ID="TestedPanel1"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this Panel is initially set to 'false'
             The Panel should be invisible" Top="390px" ID="lblExp1"></vt:Label>

        <!-- Tested Panel2 -->
        <vt:Panel runat="server" Text="TestedPanel" Visible="false" Top="450px" ID="TestedPanel2"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="545px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Visible value >>" Top="620px" Width="200px" ID="btnChangePanelVisible" ClickAction="PanelVisible\btnChangePanelVisible_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
        