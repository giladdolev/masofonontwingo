<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="150px" Left="200px" ID="TestedProgressBar" Height="36px" Width="200px" ClickAction="ProgressBarPerformClick\TestedProgressBar_Click"></vt:ProgressBar>

        <vt:Button runat="server" Text="PerformClick of 'Tested ProgressBar'" Top="45px" Left="140px" ID="btnPerformClick" Height="36px" Width="300px" ClickAction="ProgressBarPerformClick\btnPerformClick_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
