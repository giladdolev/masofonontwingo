using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox1 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (TestedGroupBox1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedGroupBox1.Tag;
            }

            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            if (TestedGroupBox2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedGroupBox2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedGroupBox2.Tag == "New Tag.")
            {
                TestedGroupBox2.Tag = TestedGroupBox2;
                lblLog2.Text = "Tag value: " + TestedGroupBox2.Tag;
            }
            else
            {
                TestedGroupBox2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedGroupBox2.Tag;

            }
        }

    }
}