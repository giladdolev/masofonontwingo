using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeItemCheckedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TreeItemCollection nodes = TestedTree.Items;
            foreach (TreeItem n in nodes)
            {
                PrintRecursiveAtLoad(n);
            }
        }

        public void btnChangeNode1Checked_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTree.Items[1].Checked = !TestedTree.Items[1].Checked;

            lblLog.Text = "";
            CallRecursive();

        }

        public void btnChangeChildNode00Checked_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTree.Items[0].Items[0].Checked = !TestedTree.Items[0].Items[0].Checked;

            lblLog.Text = "";
            CallRecursive();

        }

        public void TestedTree_AfterCheck(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "";
            CallRecursive();

        }



          private void CallRecursive()
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            // Print each node recursively.  
            TreeItemCollection nodes = TestedTree.Items;
            foreach (TreeItem n in nodes)
            {
                PrintRecursive(n);
            }
        }

          private void PrintRecursive(TreeItem treeNode)
          {
              LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

              lblLog.Text += treeNode.Text + " Checked value: " + treeNode.Checked + "\\r\\n";
              foreach (TreeItem tn in treeNode.Items)
              {
                  PrintRecursive(tn);
              }

          }


        private void PrintRecursiveAtLoad(TreeItem treeNode)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text += treeNode.Text + " Checked value: " + treeNode.Checked + "\\r\\n";
            foreach (TreeItem tn in treeNode.Items)
            {
                PrintRecursiveAtLoad(tn);
            }

        }

        

      

    }
}
