<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="GridCellElement SelectedIndex Property" ID="windowView1" Height="780px" LoadAction="GridCellElementSelectedIndex\OnLoad">


        <vt:Label runat="server" SkinID="Title" Text="SelectedIndex" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the index specifying the currently ComboBox selected item in GridComboBoxColumn.

            Syntax: public int SelectedIndex { get; set; }
            Property Value: A zero-based index of the currently selected item. A value of negative one (-1) is returned
             if no item is selected."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>
       
             <vt:Label runat="server" Text="In the following example we are following the SelectedIndex of 3 comboBox cells :
                 Cell (0,0) is initialized with SelectedIndex = 1
                 Cell (0,2) is with content but its SelectedIndex wasn't initialized
                 Cell (3,0) is with no content and its SelectedIndex wasn't initialized" Top="250px" ID="lblExp1"></vt:Label>

        <!-- Tested Grid -->
        <vt:Grid runat="server" Top="355px" Height="140px" Width="480px" ID="TestedGrid" CellEndEditAction ="GridCellElementSelectedIndex\TestedGrid_CellEndEdit">
              <Columns>
                 <vt:GridComboBoxColumn  runat="server" ID="OptionsColumn1" HeaderText="Options" Height="20px" Width="90px" ></vt:GridComboBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="IDColumn1" HeaderText="ID" Height="20px" Width="60px"> </vt:GridTextBoxColumn>
                 <vt:GridComboBoxColumn  runat="server" ID="GenderColumn1" HeaderText="Gender" SelectedIndex="true" Height="20px" Width="90px" ></vt:GridComboBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="NameColumn1" HeaderText="Name" Height="20px" Width="80px"> </vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="AttendanceColumn1" HeaderText="Attendance" Height="20px" Width="110px" ></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log"  Width="480px" Top="510px" Height="50px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change SelectedIndex of cell (0,0) >>" Left="50px" Top="620px" Width="210px" ID="btnChangeSelectedIndexOfCell0_0" ClickAction="GridCellElementSelectedIndex\btnChangeSelectedIndexOfCell0_0_Click"></vt:Button>

           <vt:Button runat="server" Text="Change SelectedIndex of cell (0,2)" Top="620px" Left="280px" Width="210px" ID="btnChangeSelectedIndexOfCell0_2" ClickAction="GridCellElementSelectedIndex\btnChangeSelectedIndexOfCell0_2_Click"></vt:Button>

        <vt:Button runat="server" Text="Change SelectedIndex of cell (3,0)" Top="620px" Left="510px" Width="210px" ID="btnChangeSelectedIndexOfCell3_0" ClickAction="GridCellElementSelectedIndex\btnChangeSelectedIndexOfCell3_0_Click"></vt:Button>

        

    </vt:WindowView>

</asp:Content>
        