﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Column reisze" Height="800px" Width="1200px" LoadAction="GridColumnResize\load_view">
        
         <vt:Label runat="server" SkinID="SubTitle" Text="column resize" Top="15px" ID="lblTitle" Left="300" Font-Size="19pt" Font-Bold="true">
        </vt:Label>
        <vt:Label runat="server" Text="How to use column resize action  ."
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Grid runat="server" Top="100px" Left="10px" ID="gridElement" Height="200px" Width="782px" > 
            <Columns>
                <vt:GridColumn ID="name" runat="server" HeaderText="Name" DataMember="name" Width="60" ></vt:GridColumn>
                <vt:GridColumn ID="email" runat="server" HeaderText="Email" DataMember="email" Width="80"></vt:GridColumn>
                <vt:GridCheckBoxColumn ID="phone" runat="server" HeaderText="Phone" DataMember="phone" Width="90" ></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>
       
<vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="500px" Height="600" ID="txtEventLog"></vt:TextBox>    </vt:WindowView>

</asp:Content>
