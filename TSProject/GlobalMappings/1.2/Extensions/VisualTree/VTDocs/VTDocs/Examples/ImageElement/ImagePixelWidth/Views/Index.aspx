<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image PixelWidth Property" ID="windowView1" LoadAction="ImagePixelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this Image is initially set to: 150px" Top="205px" ID="lblExp1"></vt:Label>

         <!-- TestedImage -->
        <vt:Image runat="server" Text="TestedImage" Top="255px" ID="TestedImage" TabIndex="3"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Top="360px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelWidth value >>" Top="425px" ID="btnChangePixelWidth" TabIndex="3" Width="180px" ClickAction="ImagePixelWidth\btnChangePixelWidth_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>