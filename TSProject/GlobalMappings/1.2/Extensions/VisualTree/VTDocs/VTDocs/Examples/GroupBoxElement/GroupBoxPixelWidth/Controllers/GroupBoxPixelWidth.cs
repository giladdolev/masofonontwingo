using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPixelWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelWidth value: " + TestedGroupBox.PixelWidth + '.';
        }

        private void btnChangePixelWidth_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGroupBox.PixelWidth == 200)
            {
                TestedGroupBox.PixelWidth = 400;
                lblLog.Text = "PixelWidth value: " + TestedGroupBox.PixelWidth + '.';
            }
            else
            {
                TestedGroupBox.PixelWidth = 200;
                lblLog.Text = "PixelWidth value: " + TestedGroupBox.PixelWidth + '.';
            }
        }
      
    }
}