using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxDropDownWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement ComboBox = this.GetVisualElementById<ComboBoxElement>("ComboBox");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "DropDownWidth value: " + ComboBox.DropDownWidth;
            lblLog2.Text = "DropDownWidth value: " + TestedComboBox.DropDownWidth + '.';
        }

        private void btnChangeDropDownWidth_Click(object sender, EventArgs e)
        {

            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedComboBox.DropDownWidth == 200)
            {
                TestedComboBox.DropDownWidth = 250;
                lblLog2.Text = "DropDownWidth value: " + TestedComboBox.DropDownWidth + '.';
            }
            else
            {
                TestedComboBox.DropDownWidth = 200;
                lblLog2.Text = "DropDownWidth value: " + TestedComboBox.DropDownWidth + '.';
            }
        }
    }
}