using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowHideShowController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowHideShow());
        }
        private WindowHideShow ViewModel
        {
            get { return this.GetRootVisualElement() as WindowHideShow; }
        }

        public void btnOpenShowTestedWin_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.TestedWin == null)
            {
                Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
                argsDictionary.Add("ElementName", "WindowElement");
                argsDictionary.Add("ExampleName", "WindowHide()Show()");

                ViewModel.TestedWin = VisualElementHelper.CreateFromView<HideShowTestedWindow>("HideShowTestedWindow", "Index2", null, argsDictionary, null);
                this.ViewModel.TestedWin.Show();
                textBox1.Text = "TestedWin is Shown";
            }

            else if (!this.ViewModel.TestedWin.Visible)
            {
                this.ViewModel.TestedWin.Show();
                textBox1.Text = "TestedWin is Shown";
            }
        }

        public void btnHideTestedWin_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.TestedWin.Visible)
            {
                this.ViewModel.TestedWin.Hide();
                textBox1.Text = "TestedWin is hidden";
            }
        }

    }
}