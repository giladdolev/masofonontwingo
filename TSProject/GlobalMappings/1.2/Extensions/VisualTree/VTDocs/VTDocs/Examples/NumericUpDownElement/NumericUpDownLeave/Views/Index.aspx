

<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Leave event" ID="windowView2" LoadAction="NumericUpDownLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the NumericUpDown controls,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="265px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="345px" Height="120px" Width="200px">
                
            <vt:NumericUpDown runat="server" Top="30px" Left="15px" value="1" ID="TestedNumericUpDown1" CssClass="vt-test-nud1" TabIndex="1" LeaveAction="NumericUpDownLeave\TestedNumericUpDown1_Leave"></vt:NumericUpDown>

            <vt:NumericUpDown runat="server" Top="75px" Left="15px" value="2" ID="TestedNumericUpDown2" CssClass="vt-test-nud2" TabIndex="2" LeaveAction="NumericUpDownLeave\TestedNumericUpDown2_Leave"></vt:NumericUpDown>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="480px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="520px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
