﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridSelectedColumnIndexController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
           
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog.Text = "SelectedColumnIndex value: " + TestedGrid.SelectedColumnIndex.ToString();

        }


        //Invokes for SelectionChangedAction and for ColumnHeaderMouseClickAction 
        public void TestedGrid_SelectionChanged(object sender, EventArgs e)
        { 
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "SelectedColumnIndex value: " + TestedGrid.SelectedColumnIndex.ToString();

        }

    }
}
