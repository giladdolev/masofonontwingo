﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CellBeginEdit event" LoadAction="GridCellBeginEdit\Page_load" >

        <vt:Label runat="server" SkinID="Title" Text="CellBeginEdit" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when edit mode starts for the selected cell.

         Syntax: public event GridElementCellEventHandler CellBeginEdit"
            Top="75px" ID="Label1">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can edit the cell content by selecting it with the mouse,
            Each invocation of the 'CellBeginEdit' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>


       <vt:Grid runat="server" Top="310px" ID="TestedGrid" CellBeginEditAction="GridCellBeginEdit\TestedGrid_CellBeginEdit">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

         <vt:Label runat="server" SkinID="Log" Top="445px" Width ="500px" ID="lblLog1"></vt:Label>

         <vt:TextBox runat="server" SkinID="EventLog" Width ="500px" Text="Event Log:" Top="485px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
