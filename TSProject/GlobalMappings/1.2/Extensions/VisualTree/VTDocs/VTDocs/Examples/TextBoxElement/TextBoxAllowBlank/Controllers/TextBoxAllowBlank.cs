using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxAllowBlankController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeAllowBlank_Click(object sender, EventArgs e)
        {
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (testedTextBox.AllowBlank == true)
            {
                testedTextBox.AllowBlank = false;
                textBox1.Text = "AllowBlank value:\\r\\n" + testedTextBox.AllowBlank;

            }
            else
            {
                testedTextBox.AllowBlank = true;
                textBox1.Text = "AllowBlank value:\\r\\n" + testedTextBox.AllowBlank;
            }
        }
      
    }
}