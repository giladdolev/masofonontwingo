using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicRadioGroupController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RadioGroupElement RadioGroup1 = this.GetVisualElementById<RadioGroupElement>("RadioGroup1");
            RadioGroupElement RadioGroup2 = this.GetVisualElementById<RadioGroupElement>("RadioGroup2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            
            foreach (RadioGroupItemElement item in RadioGroup1.Items)
            {
                if (item.IsChecked)
                {
                    lblLog1.Text = " " + item.Text + " is selected.";
                }
                else
                {
                    lblLog1.Text = " No option is selected.";
                }
            }

            foreach (RadioGroupItemElement item in RadioGroup2.Items)
            {
                if (item.IsChecked)
                {
                    lblLog2.Text = " " + item.Text + " is selected.";
                }
                else
                {
                    lblLog2.Text = " No item is selected.";
                }
            } 

        }


        public void Group1_Change(object sender, EventArgs e)
        {
            RadioGroupElement RadioGroup1 = this.GetVisualElementById<RadioGroupElement>("RadioGroup1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            foreach (RadioGroupItemElement item in RadioGroup1.Items)
            {
                if (item.IsChecked)
                {
                    lblLog1.Text = " " + item.Text + " is selected.";
                }
            } 
        }

        public void Group2_Change(object sender, EventArgs e)
        {
            RadioGroupElement RadioGroup2 = this.GetVisualElementById<RadioGroupElement>("RadioGroup2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            foreach (RadioGroupItemElement item in RadioGroup2.Items)
            {
                if (item.IsChecked)
                {
                    lblLog2.Text = " " + item.Text + " is selected.";
                }
            } 
        }

        public void btnSelectOption2_Click(object sender, EventArgs e)
        {
            RadioGroupElement RadioGroup1 = this.GetVisualElementById<RadioGroupElement>("RadioGroup1");
            RadioGroupItemElement RadioGroupItem2 = this.GetVisualElementById<RadioGroupItemElement>("RadioGroupItem2");

            RadioGroupItem2.IsChecked = true;
        }

        public void btnSelectItem4_Click(object sender, EventArgs e)
        {
            RadioGroupElement RadioGroup2 = this.GetVisualElementById<RadioGroupElement>("RadioGroup2");
            RadioGroupItemElement RadioGroupItem8 = this.GetVisualElementById<RadioGroupItemElement>("RadioGroupItem8");

            RadioGroupItem8.IsChecked = true;
        }
 
    }
}