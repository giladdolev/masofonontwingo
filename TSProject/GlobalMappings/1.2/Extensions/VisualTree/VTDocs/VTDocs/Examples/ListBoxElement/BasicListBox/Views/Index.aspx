<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic ListBox"  ID="windowView1" LoadAction="BasicListBox\Form_Load">
        <vt:ListBox runat="server" DisplayMember="text" ValueMember="value"  Top="20px" Left="20px" ID="listBox1" BorderStyle="None"  Height="300px" Width="90px">
            <Items>
                <vt:ListItem runat="server" Text="aaa" Value="1"></vt:ListItem>
                <vt:ListItem runat="server" Text="bbb" Value="2"></vt:ListItem>
                <vt:ListItem runat="server" Text="ccc" Value="3"></vt:ListItem>
                <vt:ListItem runat="server" Text="ddd" Value="4"></vt:ListItem>
                <vt:ListItem runat="server" Text="eee" Value="5"></vt:ListItem>
            </Items>
        </vt:ListBox>
    </vt:WindowView>
</asp:Content>
