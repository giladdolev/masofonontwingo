using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridCellEndEditController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}


        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedGrid.AddToComboBoxList("GridCol2", "male,female");

            TestedGrid.Rows.Add(true, "male", "a", "d", "6/8/1974 12:00:00 AM" );
            TestedGrid.Rows.Add(false, "female", "b", "e", "6/9/1974 13:00:00 AM");
            TestedGrid.Rows.Add(true, "male", "c", "f", "6/10/1974 14:00:00 AM");

            lblLog1.Text = "The Selected cell is: Row: " + TestedGrid.SelectedRowIndex + ", " + "Column: " + TestedGrid.SelectedColumnIndex + '.';

        }


        protected void TestedGrid_CellEndEdit(object sender, EventArgs e)
        {

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            int selectedRow = TestedGrid.SelectedRowIndex;
            int selectedColumn = TestedGrid.SelectedColumnIndex;

            lblLog1.Text = "The Edited cell was: Row: " + selectedRow + ", " + TestedGrid.Columns[selectedColumn].HeaderText + ",\\r\\nEdited value: " + TestedGrid.Rows[selectedRow].Cells[selectedColumn].Value + '.';  

            txtEventLog.Text += "\\r\\nFinished editing at Cell ("+selectedRow +","+selectedColumn+"), cellEndEdit event is invoked.";
        }

        public void btnClear_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();

            txtEventLog.Text = "Event Log:";
        }

        public void btnAddEmptyRow_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            var gridRow = new GridRow();
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridRow.Cells.Add(new GridContentCell() { Value = (object)DBNull.Value });
            gridElement.Rows.Insert(0, gridRow);

            gridElement.RefreshData();
            gridElement.Refresh();

            lblLog1.Text = "Empty row was added to the grid " + gridElement.SelectedRowIndex;


        }
	}
}
