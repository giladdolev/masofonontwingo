<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer BackgroundImage Property" ID="windowView1" LoadAction="SplitContainerBackgroundImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImage" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image displayed in the control.
            
           public virtual ResourceReference BackgroundImage { get; set; }" Top="75px" ID="lblDefinition" ></vt:Label>
         
        <!-- TestedSplitContainer1 -->
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="145px" ID="TestedSplitContainer1" ></vt:SplitContainer>
       
         <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="BackgroundImage property of this 'TestedSplitContainer' is initially set with image" Top="350px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedSplitContainer2 -->
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" BackgroundImage ="~/Content/Elements/Image.png" Top="400px" ID="TestedSplitContainer2" ImageReference=""></vt:SplitContainer>
        
        <vt:Label runat="server" SkinID="Log" Top="495px" ID="lblLog2"></vt:Label>
     
           <vt:Button runat="server" Text="Change Split BackgroundImage Value >>"  Top="560px" Width="230px" ID="btnChangeBackgroundSplitContainer" ClickAction="SplitContainerBackgroundImage\btnChangeBackgroundImage_Click"></vt:Button>
       
             
    </vt:WindowView>
</asp:Content>
