<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="ProgressBar MinimumSize" ID="windowView1" LoadAction="ProgressBarMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedProgressBar' is width: 90px and height: 50px. 
            Its MinimumSize is initially set to width: 210px and height: 70px.
            To cancel 'TestedProgressBar' MinimumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>
        
        <vt:ProgressBar runat="server" Text="TestedProgressBar" ID="TestedProgressBar" Value="50" Top="360px" Left="80px" Height="50px" Width="90px" MinimumSize="210, 70"></vt:ProgressBar> 

        <vt:Label runat="server" SkinID="Log" Top="470px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="530px" width="200px" Left="80px" ID="btnChangePrgMinimumSize" ClickAction="ProgressBarMinimumSize\btnChangePrgMinimumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="530px" width="200px" Left="310px" ID="btnSetToZeroPrgMinimumSize" ClickAction="ProgressBarMinimumSize\btnSetToZeroPrgMinimumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>
























