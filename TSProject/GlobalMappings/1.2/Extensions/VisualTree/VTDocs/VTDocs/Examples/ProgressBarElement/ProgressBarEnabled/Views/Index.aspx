<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Enabled Property" ID="windowView1" LoadAction="ProgressBarEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:ProgressBar runat="server" Text="ProgressBar" Top="185px" ID="TestedProgressBar1" CssClass="vt-test-pb-1"></vt:ProgressBar>           

        <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedProgressBar' is initially set to 'false'." Top="375px" ID="lblExp1"></vt:Label> 

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Value="1" Top="425px" Enabled ="false" ID="TestedProgressBar2" CssClass="vt-test-pb-2"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Top="480px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="545px" ID="btnChangeEnabled" Width="180px" ClickAction="ProgressBarEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
