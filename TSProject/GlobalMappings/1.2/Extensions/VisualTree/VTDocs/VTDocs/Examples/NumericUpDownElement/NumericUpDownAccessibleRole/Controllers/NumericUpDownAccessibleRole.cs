using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            NumericUpDownElement nudAccessibleRole = this.GetVisualElementById<NumericUpDownElement>("nudAccessibleRole");

            MessageBox.Show(nudAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            NumericUpDownElement nudAccessibleRole = this.GetVisualElementById<NumericUpDownElement>("nudAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (nudAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else if (nudAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                nudAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = nudAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}