﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;

namespace MvcApplication9.Controllers
{
    public class MaskedTextBoxTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

       
        public void OnLoad(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");

            lblLog.Text = "maskedTextBox1 Text value: " + maskedTextBox1.Text;
            lblLog.Text += "\\r\\nUser Input from textBox: " + textBox2.Text;
            
        }


        public void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");

            lblLog.Text = "maskedTextBox1 Text value: " + maskedTextBox1.Text;
            lblLog.Text += "\\r\\nUser Input from textBox: " + textBox2.Text;
        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");

            maskedTextBox1.Text = textBox2.Text;
        }

        public void textBox2_TextChanged(object sender, EventArgs e)
        {
            MaskedTextBoxElement maskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("maskedTextBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");

            lblLog.Text = "maskedTextBox1 Text value: " + maskedTextBox1.Text;
            lblLog.Text += "\\r\\nUser Input from textBox: " + textBox2.Text;
        }

    }
}