using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPixelWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelWidth value: " + TestedCheckBox.PixelWidth + '.';
        }

        private void btnChangePixelWidth_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedCheckBox.PixelWidth == 150)
            {
                TestedCheckBox.PixelWidth = 300;
                lblLog.Text = "PixelWidth value: " + TestedCheckBox.PixelWidth + '.';
            }
            else
            {
                TestedCheckBox.PixelWidth = 150;
                lblLog.Text = "PixelWidth value: " + TestedCheckBox.PixelWidth + '.';
            }
        }
    }
}