<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label AutoEllipsis property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Label1AutoSize=true;AutoEllipsis=true" AutoEllipsis="true" AutoSize="true" Top="300px" Left="50px" ID="Label1" Width="50px" Height="13px"></vt:Label>
        <vt:Label runat="server" Text="Label2AutoSize=true;AutoEllipsis=false" AutoEllipsis="false" AutoSize="true" Top="360px" Left="50px" ID="Label2" Width="50px" Height="13px"></vt:Label>
        <vt:Label runat="server" Text="Label3AutoSize=false;AutoEllipsis=true" AutoEllipsis="true" AutoSize="false" Top="410px" Left="50px" ID="Label3" Width="50px" Height="13px"></vt:Label>
        <vt:Label runat="server" Text="Label4AutoSize=false;AutoEllipsis=false" AutoEllipsis="false" AutoSize="false" Top="430px" Left="50px" ID="Label4" Width="50px" Height="13px"></vt:Label>
        <vt:Label runat="server" Text="Label5AutoSize=false;AutoEllipsis=false" AutoEllipsis="false" AutoSize="false" Top="450px" Left="50px" ID="Label5" Width="50px" Height="13px"></vt:Label>
        <vt:Button runat="server" ID="Button1" Text="Change AutoSize for Label1" Top="150" Left="50" Height="50" Width="300" ClickAction="LabelAutoEllipsis\button1_Click"></vt:Button>
        <vt:Button runat="server" ID="Button2" Text="Change AutoEllipsis for Label1" Top="200" Left="50" Height="50" Width="300" ClickAction="LabelAutoEllipsis\button2_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
