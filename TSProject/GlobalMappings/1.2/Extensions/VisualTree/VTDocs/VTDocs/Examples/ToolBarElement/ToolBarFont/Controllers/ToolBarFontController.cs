using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarFontController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedToolBar1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedToolBar1);
            lblLog2.Text = "Font value: " + TestedToolBar2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedToolBar2);
        }
        private void btnChangeToolBarFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            if (TestedToolBar2.Font.Underline == true)
            {
                TestedToolBar2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedToolBar2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedToolBar2);
            }
            else if (TestedToolBar2.Font.Bold == true)
            {
                TestedToolBar2.Font = new Font("Miriam Fixed", 20, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedToolBar2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedToolBar2);
            }
            else if (TestedToolBar2.Font.Italic == true)
            {
                TestedToolBar2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedToolBar2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedToolBar2);
            }
            else
            {
                TestedToolBar2.Font = new Font("SketchFlow Print", 15, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedToolBar2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedToolBar2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}