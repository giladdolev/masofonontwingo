<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Click event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ComboBoxClick/Form_Load">
        <vt:Label runat="server" Text="Run Time : Click once on the comboBox" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ComboBox runat="server" ID="cmb1" Top="160" Left="50px" ClickAction="ComboBoxClick\ComboBoxClick_Click"/>
    <vt:Label runat="server" Text="" Top="112px" Left="150px" ID="lblEvent" Width="350px" Font-Bold="true"/>

         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="545px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>
    </vt:WindowView>
</asp:Content>
