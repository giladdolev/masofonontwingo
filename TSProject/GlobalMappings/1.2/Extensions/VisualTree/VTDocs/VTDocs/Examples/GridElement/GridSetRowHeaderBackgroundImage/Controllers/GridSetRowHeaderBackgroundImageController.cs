using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridSetRowHeaderBackgroundImageController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");

            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            cmbInd.Items.Add("0");
            cmbInd.Items.Add("1");
            cmbInd.Items.Add("2");

            cmbInd.SelectedIndex = 0;

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundImage: Null";  
        }


        private void btnLabelRowHeaderBackgroundImage_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.SetRowHeaderBackgroundImage(cmbInd.SelectedIndex, new UrlReference("/Content/Elements/Label.png"));

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundImage: Label.png"; 
        }

        private void btnButtonRowHeaderBackgroundImage_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.SetRowHeaderBackgroundImage(cmbInd.SelectedIndex, new UrlReference("/Content/Elements/Button.png"));

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundImage: Button.png"; 
        }

        private void btnClearRowHeaderBackgroundImage_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbInd = this.GetVisualElementById<ComboBoxElement>("cmbInd");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.SetRowHeaderBackgroundImage(cmbInd.SelectedIndex, null);

            lblLog.Text = "Row index: " + cmbInd.SelectedIndex + ", RowHeaderBackgroundImage: Null";
        }
    }
}
