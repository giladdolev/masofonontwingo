using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //TreeAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedTree1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedTree2.ForeColor + '.';
        }

        public void btnChangeTreeForeColor_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTree2.ForeColor == Color.Green)
            {
                TestedTree2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedTree2.ForeColor + '.';
            }
            else
            {
                TestedTree2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedTree2.ForeColor + '.';
            }
        }
    }
}