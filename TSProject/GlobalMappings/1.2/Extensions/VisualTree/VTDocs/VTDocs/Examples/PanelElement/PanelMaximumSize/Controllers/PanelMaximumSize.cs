using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Panel MaximumSize value: " + TestedPanel.MaximumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;

        }
        public void btnChangePnlMaximumSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.MaximumSize == new Size(190, 70))
            {
                TestedPanel.MaximumSize = new Size(210, 40);
                lblLog.Text = "Panel MaximumSize value: " + TestedPanel.MaximumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;

            }
            else if (TestedPanel.MaximumSize == new Size(210, 40))
            {
                TestedPanel.MaximumSize = new Size(230, 60);
                lblLog.Text = "Panel MaximumSize value: " + TestedPanel.MaximumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;
            }

            else
            {
                TestedPanel.MaximumSize = new Size(190, 70);
                lblLog.Text = "Panel MaximumSize value: " + TestedPanel.MaximumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;
            }

        }

        public void btnSetToZeroPnlMaximumSize_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedPanel.MaximumSize = new Size(0, 0);
            lblLog.Text = "Panel MaximumSize value: " + TestedPanel.MaximumSize + "\\r\\nPanel Size value: " + TestedPanel.Size;

        }

    }
}