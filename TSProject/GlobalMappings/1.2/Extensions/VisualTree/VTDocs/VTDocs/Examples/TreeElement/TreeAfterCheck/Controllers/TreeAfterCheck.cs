using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeAfterCheckController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Check the tree nodes check boxes...";
        }

        public void btnChangeNode1Checked_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");

            TestedTree.Items[1].Checked = !TestedTree.Items[1].Checked;
        }

        public void btnChangeChildNode00Checked_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");

            TestedTree.Items[0].Items[0].Checked = !TestedTree.Items[0].Items[0].Checked;
        }

        public void TestedTree_AfterCheck(object sender, TreeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = e.Item.Text + " Checked value was changed to: " + e.Item.Checked;
            txtEventLog.Text += "\\r\\nAfterCheck event was invoked";
        }


    }
}