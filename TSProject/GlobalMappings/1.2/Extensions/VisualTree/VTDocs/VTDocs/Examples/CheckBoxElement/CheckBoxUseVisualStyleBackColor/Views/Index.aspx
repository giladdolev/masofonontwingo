<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox UseVisualStyleBackColor Property" Top="57px" Left="11px" ID="windowView1" Height="750px" Width="768px">

        <vt:Label runat="server" Text="UseVisualStyleBackColor is Initially set to false" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:CheckBox runat="server" Text="Tested CheckBox" BackColor="LightGray" Top="45px" Left="140px" UseVisualStyleBackColor ="false" ID="chkUseVisualStyleBackColor" Height="36px" Width="160px"></vt:CheckBox>
                   
        <vt:Button runat="server" Text="Change UseVisualStyleBackColor value" Top="45px" Left="350px" ID="btnChangeUseVisualStyleBackColor" Height="36px" Width="220px" ClickAction ="CheckBoxUseVisualStyleBackColor\btnChangeUseVisualStyleBackColor_Click"></vt:Button>  


        <vt:TextBox runat="server" Text="" Top="120px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
