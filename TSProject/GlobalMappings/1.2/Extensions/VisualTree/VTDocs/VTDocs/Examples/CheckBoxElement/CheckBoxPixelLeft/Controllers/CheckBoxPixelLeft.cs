using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelLeft value: " + TestedCheckBox.PixelLeft + '.';
        }

        public void btnChangePixelLeft_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedCheckBox.PixelLeft == 80)
            {
                TestedCheckBox.PixelLeft = 180;
                lblLog.Text = "PixelLeft value: " + TestedCheckBox.PixelLeft + '.';
            }
            else
            {
                TestedCheckBox.PixelLeft = 80;
                lblLog.Text = "PixelLeft value: " + TestedCheckBox.PixelLeft + '.';
            }
        }
    }
}