using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownGotFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Press on the button.";
        }

        public void TestedNumericUpDown1_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "NumericUpDown1 has the Focus.";
            txtEventLog.Text += "\\r\\nNumericUpDown1 is focused, GotFocus event is invoked";
        }

        public void TestedNumericUpDown2_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "NumericUpDown2 has the Focus.";
            txtEventLog.Text += "\\r\\nNumericUpDown2 is focused, GotFocus event is invoked";
        }

        public void TestedNumericUpDown3_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            lblLog.Text = "NumericUpDown3 has the Focus.";
            txtEventLog.Text += "\\r\\nNumericUpDown3 is focused, GotFocus event is invoked";
        }

     
        public void btnGotFocus_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");

            TestedNumericUpDown2.Focus();

            lblLog.Text = "NumericUpDown2 has the Focus.";

        }



    }
}