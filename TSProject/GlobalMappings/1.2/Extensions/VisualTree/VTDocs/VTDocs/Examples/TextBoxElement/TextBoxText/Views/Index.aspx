
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox Text Property" ID="windowView2" LoadAction="TextBoxText/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with this control.
            
            Syntax: public override string Text { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Text property of the TextBox is initially set to a series of special characters '\a&quot;a'a&amp;a-@#*!^&%'
             and is limited to 50 characters.

            You can manually edit and clear text or use the buttons.
            By pressing 'Add Text' button new string will be concatenated to the text in the TextBox.
            By Pressing 'Change Text Value' all the text will be replaced. " Top="235px"  ID="lblExp1"></vt:Label>     
      
        <vt:TextBox runat="server" Name='Amit' Text="\a&quot;a'a&amp;a-@#*!^&%" Top="400px" Width="350px" MaxLength="50" ID="TestedTextBox" TextChangedAction ="TextBoxText\TestedTextBox_TextChanged"></vt:TextBox>
        <vt:Label runat="server" SkinID="Log" Top="440px" Width="550px" Height="25px" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Add Text >>"  Top="505px" Width="150px" ID="btnAddText" ClickAction="TextBoxText\btnAddText_Click"></vt:Button>
         <vt:Button runat="server" Text="Change Text Value >>"  Top="505px" Width="150px" Left= "280px" ID="btnChangeTextValue" ClickAction="TextBoxText\btnChangeTextValue_Click"></vt:Button>
         <vt:Button runat="server" Text="Clear Text >>"  Top="505px" Width="150px" Left= "480px" ID="btnClearText" ClickAction="TextBoxText\btnClearText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>