<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Dialog Window" Top="57px" Left="11px" ID="windowView2" Height="600px" Width="600px">
		
        <vt:Button runat="server" TextAlign="MiddleCenter"  Text="Switch Ok And Cancel" Top="150px" Left="160px" ClickAction="ButtonWindowDialog\btnSwitchButtons_Click" ID="btnSwitchButtons" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="300px">
		</vt:Button>

		<vt:Button runat="server" TextAlign="MiddleCenter" DialogResult="OK" Text="Ok" Top="300px" Left="50px" ClickAction="ButtonWindowDialog\btnOk_Click" ID="btnOk" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="200px">
		</vt:Button>
        
        <vt:Button runat="server" TextAlign="MiddleCenter" DialogResult="Cancel" Text="Cancel" Top="300px" Left="350px" ClickAction="ButtonWindowDialog\btnCancel_Click" ID="btnCancel" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="200px">
		</vt:Button>
		

        </vt:WindowView>
</asp:Content>