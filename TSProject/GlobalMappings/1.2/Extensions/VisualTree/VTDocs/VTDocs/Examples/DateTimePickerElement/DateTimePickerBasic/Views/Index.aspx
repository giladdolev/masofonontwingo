<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic DateTimePicker" Top="57px" Left="11px" ID="windowView1" Height="700px" Width="768px">
        <vt:DateTimePicker runat="server" ID="DateTimePicker1" Editable="false" Width="200px" Height="20"> </vt:DateTimePicker>

        <vt:Button runat="server" Text="Set Value" Top="112px" Left="130px" ID="btnSetValue" TabIndex="1" Width="250px" ClickAction="DateTimePickerBasic\btnSetValue_Click"></vt:Button>
        <vt:Button runat="server" Text="Get Value" Top="160px" Left="130px" ID="btnGetValue" TabIndex="1" Width="250px" ClickAction="DateTimePickerBasic\btnGetValue_Click"></vt:Button>

         <vt:Button runat="server" Text="Change Editable value >>" Top="250px" Left="100px" Width="230px" ID="btnChangeEditable" ClickAction="DateTimePickerBasic\btnChangeEditable_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
