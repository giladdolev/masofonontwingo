using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxVisibleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox1 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox1");
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Visible value: " + TestedGroupBox1.Visible;
            lblLog2.Text = "Visible value: " + TestedGroupBox2.Visible + '.';

        }

        public void btnChangeGroupBoxVisible_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedGroupBox2.Visible = !TestedGroupBox2.Visible;
            lblLog2.Text = "Visible value: " + TestedGroupBox2.Visible + '.';
        }
    }
}