<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabsPerRow Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent -TabsPerRow is set to 1" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Tab runat="server" TabsPerRow="1" Top="50px"  Left="140px" ID="tabTabsPerRow" Height="200px" Width="250px">
             <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px"  >
                </vt:TabItem>
                <vt:TabItem runat="server"  Text="tabPage2" Top="22px" Left="4px" ID="TabItem4" Height="74px" Width="192px"  ></vt:TabItem>
            </TabItems>
        </vt:Tab>          

        <vt:Label runat="server" Text="RunTime" Top="300px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Tab runat="server" Text="Runtime TabsPerRow" Top="320px" Left="140px" ID="tabRuntimeTabsPerRow" Height="200px" TabIndex="1" Width="250px" >
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px"  >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem2" Height="74px" Width="192px" ></vt:TabItem>
            </TabItems>
            
        </vt:Tab>

        <vt:Button runat="server"  Text="Change Tab TabsPerRow" Top="410px" Left="470px" ID="btnChangeTabsPerRow" Height="36px" TabIndex="1" Width="150px" ClickAction="TabTabsPerRow\btnChangeTabsPerRow_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="570px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>

