<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab SelectTab method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
        
        <vt:Tab runat="server" Top="124px" Left="264px" ID="tabControl1" Height="250px" Width="400px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage4" Top="22px" Left="4px" ID="tabPage4" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        
        <vt:Button runat="server" Text="SelectTab (Int index)" Top="165px" Left="50px" ClickAction="TabSelectTab\ChangeTab1_Click" ID="btn1" Height="23px" Width="200px">
        </vt:Button>
        
        <vt:Button runat="server" Text="SelectTab (String tabPageName)" Top="200px" Left="50px" ClickAction="TabSelectTab\ChangeTab2_Click" ID="btn2" Height="23px" Width="200px">
        </vt:Button>
        
        <vt:Button runat="server" Text="SelectTab (TabPage tabPage)" Top="240px" Left="50px" ClickAction="TabSelectTab\ChangeTab3_Click" ID="btn3" Height="23px" Width="200px">
        </vt:Button>
    

        <vt:TextBox runat="server" Text="" Top="400px" Left="300px" ID="textBox1" Multiline="true"  Height="50px" Width="150px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
