﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CellMouseDown event" ID="windowView1" LoadAction="GridCellMouseDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CellMouseDown" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the user presses a mouse button while the mouse pointer is within the boundaries of a cell.

            Syntax:  public event EventHandler<GridCellMouseEventArgs> CellMouseDown"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells from the grid by clicking with the mouse. 
            Each invocation of the  'CellmouseDown' event should add a line to the 'Event Log'."
            Top="215px" ID="lblExp2">
        </vt:Label>

         <vt:Grid runat="server"  Top="300px" ID="TestedGrid" CellMouseDownAction="GridCellMouseDown\TestedGrid_CellMouseDown">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="430px" Width="400px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="470px" Width="400px" Height="140px" ID="txtEventLog"></vt:TextBox>

        
    </vt:WindowView>
</asp:Content>
