using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Left value: " + TestedSplitContainer.Left + '.';
        }

        public void btnChangeLeft_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedSplitContainer.Left == 80)
            {
                TestedSplitContainer.Left = 180;
                lblLog.Text = "Left value: " + TestedSplitContainer.Left + '.';
            }
            else
            {
                TestedSplitContainer.Left = 80;
                lblLog.Text = "Left value: " + TestedSplitContainer.Left + '.';
            }
        }

    }
}