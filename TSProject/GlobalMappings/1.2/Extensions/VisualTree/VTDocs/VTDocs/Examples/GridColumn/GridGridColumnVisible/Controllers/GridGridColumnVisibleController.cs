using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridGridColumnVisibleController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}



        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog.Text = "Column1 Visible value is: " + TestedGrid.Columns[0].Visible;
        }

       
        public void btnChangeCol1Visibility_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Columns[0].Visible = !TestedGrid.Columns[0].Visible;
            lblLog.Text = "Column1 Visible value is: " + TestedGrid.Columns[0].Visible;
        }
        public void btnChangeCol2Visibility_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Columns[1].Visible = !TestedGrid.Columns[1].Visible;
            lblLog.Text = "Column2 Visible value is: " + TestedGrid.Columns[1].Visible;
        }
        public void btnChangeCol3Visibility_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Columns[2].Visible = !TestedGrid.Columns[2].Visible;
            lblLog.Text = "Column3 Visible value is: " + TestedGrid.Columns[2].Visible;
        }

	

	}
}
