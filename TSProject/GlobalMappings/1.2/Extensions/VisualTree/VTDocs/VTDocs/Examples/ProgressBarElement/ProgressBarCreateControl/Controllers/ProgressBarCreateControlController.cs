using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new ProgressBarCreateControl());

        }

        private ProgressBarCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as ProgressBarCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ProgressBarElement newProgressBar = new ProgressBarElement();

            newProgressBar.CreateControl();
            newProgressBar.Width = 100;
            newProgressBar.Height = 20;


            this.ViewModel.Controls.Add(newProgressBar);
            

            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}