using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPerformMouseDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseDown_Click(object sender, EventArgs e)
        {
            
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");

            MouseEventArgs args = new MouseEventArgs();

            testedSplitContainer.PerformMouseDown(args);
        }

        public void testedSplitContainer_MouseDown(object sender, EventArgs e)
        {
            MessageBox.Show("TestedSplitContainer MouseDown event method is invoked");
        }

    }
}