﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    public class MaskedTextBoxMaskController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new MaskedTextBoxMask());
        }
        private MaskedTextBoxMask ViewModel
        {
            get { return this.GetRootVisualElement() as MaskedTextBoxMask; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
            MaskedTextBoxElement MaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("MaskedTextBox");

            if (String.IsNullOrEmpty(MaskedTextBox.Mask))
            {
                lblLog1.Text = "Mask value: Empty";
            }
            else
            {
                lblLog1.Text = "Mask value: " + MaskedTextBox.Mask;
            }
            
            lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask is initially set to 00/00/0000 - the mask represents a date (day, numeric month, year) in international date format. The '/' character is a logical date separator, and will appear to the user as the date separator appropriate to the application's current culture.";
        }

        public void btnMaskingElementTable_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "MaskedTextBoxElement");
            argsDictionary.Add("ExampleName", "MaskedTextBoxMask");

            ViewModel.frmTable = VisualElementHelper.CreateFromView<MaskingElementsTabel>("MaskingElementsTabel", "Index2", null, argsDictionary, null);
            ViewModel.frmTable.Show();
        } 

        public void btnSetMask_Click(object sender, EventArgs e)
        {
            ComboBoxElement combo1 = this.GetVisualElementById<ComboBoxElement>("combo1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>
               ("TestedMaskedTextBox");

            switch (combo1.Text)
            {
                case "00->L<LL-0000":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask 00->L<LL-0000 was selected  - the mask represents a date (day, month abbreviation, and year) in United States format in which the three-letter month abbreviation is displayed with an initial uppercase letter followed by two lowercase letters.";
                    break;
                case "00/00/0000":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask 00/00/0000 was selected  - the mask represents a date (day, numeric month, year) in international date format. The '/' character is a logical date separator, and will appear to the user as the date separator appropriate to the application's current culture.";
                    break;
                case "(999)-000-0000":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask (999)-000-0000 was selected  - the mask represents United States phone number, area code optional. If users do not want to enter the optional characters, they can either enter spaces or place the mouse pointer directly at the position in the mask represented by the first 0.";
                    break;
                case "$999,999.00":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask $999,999.00 was selected  - the mask represents a currency value in the range of 0 to 999999. The currency, thousandth, and decimal characters will be replaced at run time with their culture-specific equivalents.";
                    break;
                case "\a. LLL, b. LLL":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask \a. LLL, b. LLL was selected  - the mask represents a two members list.";
                    break;
                case "00:00":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask 00:00 was selected  - the mask represents a European time";
                    break;
                case "90:00":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask 90:00 was selected  - the mask represents a US time.";
                    break;
                case "00/00/0000 90:00":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask 00/00/0000 90:00 was selected  - the mask represents a short date and time.";
                    break;
                case "00000":
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask 00000 was selected  - the mask represents an Israeli postcode.";
                    break;
                default:
                    lblLog.Text = "Mask value: " + TestedMaskedTextBox.Mask + "\\r\\nThe Mask " + TestedMaskedTextBox.Mask + " was selected  - custom mask.";
                    break;
            }
        }

        public void btnSetText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>
               ("TestedMaskedTextBox");

            TestedMaskedTextBox.Text = textBox1.Text;
        }

    }
}


    

    //public void combo1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
    //    ComboBoxElement combo1 = this.GetVisualElementById<ComboBoxElement>("combo1");
    //    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


    //    ReplacePartOfMaskText(1, combo1, TestedMaskedTextBox);
    //    WriteToLog(TestedMaskedTextBox);
    //}

    //public void combo2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
    //    ComboBoxElement combo2 = this.GetVisualElementById<ComboBoxElement>("combo2");
    //    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


    //    ReplacePartOfMaskText(9, combo2, TestedMaskedTextBox);
    //    WriteToLog(TestedMaskedTextBox);
    //}

    ////combo3_SelectedIndexChanged
    //public void combo3_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    MaskedTextBoxElement TestedMaskedTextBox = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox");
    //    ComboBoxElement combo3 = this.GetVisualElementById<ComboBoxElement>("combo3");
    //    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


    //     ReplacePartOfMaskText(17, combo3, TestedMaskedTextBox);
    //    WriteToLog(TestedMaskedTextBox);
    //}


    //private void WriteToLog (MaskedTextBoxElement TestedMaskedTextBox)
    //{
    //    LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
    //    lblLog.Text = "The selected Mask is: (" + TestedMaskedTextBox.Mask.Substring(1, 3) + ") - (" + TestedMaskedTextBox.Mask.Substring(9, 3) + ") - (" + TestedMaskedTextBox.Mask.Substring(17, 3) + ")\\r\\n\\r\\nPart 1: " + GetMaskElement(1, TestedMaskedTextBox) + " - " + GetMaskElementExplanation(TestedMaskedTextBox.Mask.Substring(1, 3)) + "\\r\\n\\r\\nPart 2: " + GetMaskElement(9, TestedMaskedTextBox) + " - " + GetMaskElementExplanation(TestedMaskedTextBox.Mask.Substring(9, 3)) + "\\r\\n\\r\\nPart 3: " + GetMaskElement(17, TestedMaskedTextBox) + " - " + GetMaskElementExplanation(TestedMaskedTextBox.Mask.Substring(17, 3));
    //}

    //private String GetMaskElement(int position, MaskedTextBoxElement TestedMaskedTextBox)
    //{
    //    if (TestedMaskedTextBox.Mask.Substring(position, 1) == "\\")
    //    {
    //        return "\\";
    //    }
    //    else
    //        return TestedMaskedTextBox.Mask.Substring(position, 1);

    //}
    //private void ReplacePartOfMaskText(int position, ComboBoxElement Combo3, MaskedTextBoxElement TestedMaskedTextBox)
    //{

    //    switch (Combo3.Text)
    //    {
    //        case "0":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "000");
    //            break;
    //        case "9":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "999");
    //            break;
    //        case "#":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "###");
    //            break;
    //        case "L":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "LLL");
    //            break;
    //        case "?":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "???");
    //            break;
    //        case "&":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "&&&");
    //            break;
    //        case "C":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "CCC");
    //            break;
    //        case "A":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "AAA");
    //            break;
    //        case "a":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "aaa");
    //            break;
    //        case ".":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "...");
    //            break;
    //        case ",":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, ",,,");
    //            break;
    //        case ":":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, ":::");
    //            break;
    //        case "/":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "///");
    //            break;
    //        case "$":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "$$$");
    //            break;
    //        case "<":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "<<<");
    //            break;
    //        case ">":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, ">>>");
    //            break;
    //        case "|":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, "|||");
    //            break;
    //        case "\\":
    //            TestedMaskedTextBox.Mask = TestedMaskedTextBox.Mask.Remove(position, 3).Insert(position, @"\"+@"\"+@"\"+@"\"+@"\");

    //            break;
    //        default:
    //            Combo3.Text = "Select Mask element";
    //            break;
    //    }

    //}

    //private string GetMaskElementExplanation(string theMask)
    //{
    //    switch (theMask)
    //    {
    //        case "000":
    //            return "Digit, required. This element will accept any single digit between 0 and 9.";
    //        case "999":
    //            return "Digit or space, optional.";
    //            break;
    //        case "###":
    //            return "Digit or space, optional. If this position is blank in the mask, it will be\\r\\n rendered as a space in the Text property. Plus (+) and minus (-) signs are allowed.";
    //            break;
    //        case "LLL":
    //            return "Letter, required. Restricts input to the ASCII letters a-z and A-Z. This mask\\r\\n element is equivalent to [a-zA-Z] in regular expressions.";
    //            break;
    //        case "???":
    //            return "Letter, optional. Restricts input to the ASCII letters a-z and A-Z. This mask\\r\\n element is equivalent to [a-zA-Z]? in regular expressions.";
    //            break;
    //        case "&&&":
    //            return "Character, required. If the AsciiOnly property is set to true, this element\\r\\n behaves like the 'L' element.";
    //            break;
    //        case "CCC":
    //            return "Character, optional. Any non-control character. If the AsciiOnly property\\r\\n is set to true, this element behaves like the '?' element.";
    //            break;
    //        case "AAA":
    //            return "Alphanumeric, required. If the AsciiOnly property is set to true, the only\\r\\n characters it will accept are the ASCII letters a-z and A-Z. This mask element behaves\\r\\n like the 'a' element.";
    //            break;
    //        case "aaa":
    //            return "Alphanumeric, optional. If the AsciiOnly property is set to true, the only\\r\\n characters it will accept are the ASCII letters a-z and A-Z. This mask element behaves\\r\\n like the 'A' element.";
    //            break;
    //        case "...":
    //            return "Decimal placeholder. The actual display character used will be the decimal\\r\\n symbol appropriate to the format provider, as determined by the control's FormatProvider\\r\\n property.";
    //            break;
    //        case ",,,":
    //            return "Thousands placeholder. The actual display character used will be the thousands \\r\\n placeholder appropriate to the format provider, as determined by the \\r\\n  control's FormatProvider property.";
    //            break;
    //        case ":::":
    //            return "Time separator. The actual display character used will be the time symbol\\r\\n appropriate to the format provider, as determined by the control's FormatProvider property.";
    //            break;
    //        case "///":
    //            return "Date separator. The actual display character used will be the date symbol\\r\\n appropriate to the format provider, as determined by the control's FormatProvider property.";
    //            break;
    //        case "$$$":
    //            return "Currency symbol. The actual character displayed will be the currency symbol\\r\\n appropriate to the format provider, as determined by the control's FormatProvider property.";
    //            break;
    //        case "<<<":
    //            return "Shift down. Converts all characters that follow to lowercase.";
    //            break;
    //        case ">>>":
    //            return "Shift up. Converts all characters that follow to uppercase.";
    //            break;
    //        case "|||":
    //            return "Disable a previous shift up or shift down.";
    //            break;
    //        case "\\\\\\":
    //            return "Escape. Escapes a mask character, turning it into a literal. '\\' is the escape\\r\\n sequence for a backslash.";
    //            break;
    //        default:
    //            return "Description not available";
    //            break;
    //    }
    //}


