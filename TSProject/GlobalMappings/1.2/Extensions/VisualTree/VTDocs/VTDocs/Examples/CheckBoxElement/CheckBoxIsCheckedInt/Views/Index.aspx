<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox IsCheckedInt Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
  
        <vt:Label runat="server" Text="CheckBox initially set to IsChecked = 'true'" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:CheckBox runat="server" Text="CheckBox IsCheckedInt Change" Top="115px" IsChecked = "true" Left="140px" ID="chkCheckIsCheckedInt" Height="36px" Width="150px"></vt:CheckBox>

        <vt:Button runat="server" Text="Change CheckBox IsCheckedInt" Top="115px" Left="400px" ID="btnChangeIsCheckedInt" Height="36px" Width="200px" ClickAction="CheckBoxIsCheckedInt\btnChangeIsCheckedInt_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
