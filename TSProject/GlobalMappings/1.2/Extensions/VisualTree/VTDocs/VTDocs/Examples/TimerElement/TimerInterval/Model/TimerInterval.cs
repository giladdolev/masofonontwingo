﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class TimerInterval : WindowElement
    {

        public TimerInterval()
        {
            durationTimer1 = 1;
            durationTimer2 = 1;
        }
        /// <summary>
        /// 
        /// </summary>
        int durationTimer1;
        int durationTimer2;


        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>

        public int durationTimer1Property
        {
            get { return this.durationTimer1; }
            set { this.durationTimer1 = value; }
        }

        public int durationTimer2Property
        {
            get { return this.durationTimer2; }
            set { this.durationTimer2 = value; }
        }

    }
}