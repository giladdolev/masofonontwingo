﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SellectionChanged event" ID="windowView1" LoadAction="GridSelectionChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current selection changes.

            Syntax: public event GridElementCellEventHandler SelectionChanged"
            Top="65px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="145px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select Row/ Column/ Cell from the grid by mouse clicking or by 
            pressing the buttons. Each invocation of the 'BeforeSelectionChanged' and the 'SelectionChanged' 
            events should add a line to the 'Event Log'."
            Top="180px" ID="lblExp2">
        </vt:Label>

         <vt:Grid runat="server" Text="TestedGrid" Top="300px" ID="TestedGrid" BeforeSelectionChangedAction="GridSelectionChanged\TestedGrid_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged\TestedGrid_SelectionChanged" RowHeaderVisible="false">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="75px" CssClass="vt-grid-Column1"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="75px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="75px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Grid runat="server" Text="TestedGrid" Top="260px" Left="380px" RowHeaderVisible="false" Width="243px" ID="TestedGrid1" BeforeSelectionChangedAction="GridSelectionChanged\TestedGrid1_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged\TestedGrid1_SelectionChanged">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="75px" CssClass="vt-grid1-Column1"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="75px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="75px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="385px" Width="550px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="420px" Width="550px" Height="140px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select Row[0] >>" Top="590px" ID="btnSelectRow0" ClickAction="GridSelectionChanged\btnSelectRow0_Click"></vt:Button>
        <vt:Button runat="server" Text="Select Column[0] >>" Top="590px" Left="280px" ID="btnSelectColumn0" ClickAction="GridSelectionChanged\btnSelectColumn0_Click"></vt:Button>

        <vt:Button runat="server" Text="Select i >>" Top="625px" ID="btnSelecti" ClickAction="GridSelectionChanged\btnSelecti_Click"></vt:Button>

        <vt:Button runat="server" Text="Select r >>" Top="625px" Left="280px" ID="btnSelectr" ClickAction="GridSelectionChanged\btnSelectr_Click"></vt:Button>

         <vt:Button runat="server" Text="Clear Event Log >>" Top="625px" Left="480px" ID="btnClear" ClickAction="GridSelectionChanged\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

