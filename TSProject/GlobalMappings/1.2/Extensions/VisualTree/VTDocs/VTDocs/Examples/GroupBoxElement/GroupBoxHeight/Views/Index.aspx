<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Height Property" ID="windowView2" LoadAction="GroupBoxHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Height" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels..
            
            Syntax: public int Height { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this 'TestedGroupBox' is initially set to 80." Top="235px"  ID="lblExp1"></vt:Label>     

        <!-- TestedGroupBox -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="285px" ID="TestedGroupBox"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="380px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Height >>" Top="445px" ID="btnChangeHeight" ClickAction="GroupBoxHeight\btnChangeHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
