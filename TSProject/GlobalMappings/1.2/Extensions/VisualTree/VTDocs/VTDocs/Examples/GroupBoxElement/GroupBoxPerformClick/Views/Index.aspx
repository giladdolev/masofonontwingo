<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:GroupBox runat="server" Text="Tested GroupBox" Top="150px" Left="200px" ID="TestedGroupBox" Height="70px" Width="200px" ClickAction="GroupBoxPerformClick\TestedGroupBox_Click"></vt:GroupBox>

        <vt:Button runat="server" Text="PerformClick of 'Tested GroupBox'" Top="45px" Left="140px" ID="btnPerformClick" Height="36px" Width="300px" ClickAction="GroupBoxPerformClick\btnPerformClick_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
