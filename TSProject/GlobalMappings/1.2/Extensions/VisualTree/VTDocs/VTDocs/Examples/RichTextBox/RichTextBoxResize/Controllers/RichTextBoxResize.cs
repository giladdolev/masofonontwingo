using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxResizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");


            if (testedRichTextBox.Size == new Size(200, 70))
            {
                testedRichTextBox.Size = new Size(300, 100);
                testedRichTextBox.Text = "Set to Height: " + testedRichTextBox.Height + ", Width: " + testedRichTextBox.Width;
            }
            else
            {
                testedRichTextBox.Size = new Size(200, 70);
                testedRichTextBox.Text = "Back to Height: " + testedRichTextBox.Height + ", Width: " + testedRichTextBox.Width;
            }
        }

        private void testedRichTextBox_Resize(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "RichTextBox Resize event is invoked\\r\\n";
        }
      
    }
}