using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //SplitContainerAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested SplitContainer bounds are set to: \\r\\nLeft  " + TestedSplitContainer.Left + ", Top  " + TestedSplitContainer.Top + ", Width  " + TestedSplitContainer.Width + ", Height  " + TestedSplitContainer.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            if (TestedSplitContainer.Left == 100)
            {
                TestedSplitContainer.SetBounds(80, 300, 200, 80);
                lblLog.Text = "The Tested SplitContainer bounds are set to: \\r\\nLeft  " + TestedSplitContainer.Left + ", Top  " + TestedSplitContainer.Top + ", Width  " + TestedSplitContainer.Width + ", Height  " + TestedSplitContainer.Height;

            }
            else
            {
                TestedSplitContainer.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested SplitContainer bounds are set to: \\r\\nLeft  " + TestedSplitContainer.Left + ", Top  " + TestedSplitContainer.Top + ", Width  " + TestedSplitContainer.Width + ", Height  " + TestedSplitContainer.Height;
            }
        }

    }
}