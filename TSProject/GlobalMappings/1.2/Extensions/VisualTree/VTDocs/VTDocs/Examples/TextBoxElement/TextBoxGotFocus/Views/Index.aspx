<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox GotFocus event" ID="windowView2" LoadAction="TextBoxGotFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="GotFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control receives focus.

            Syntax: public event EventHandler GotFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted into one of the text boxes,
             a 'GotFocus' event will be invoked.
             Each invocation of the 'GotFocus' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Height="120px" Width="340px">
            <vt:Label runat="server" Text="First Name :" Left="15px" Top="25px" ID="Label1"></vt:Label>

            <vt:TextBox runat="server" Top="25px" Left="160px" Text="Enter First Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtFirstName" TabIndex="1" GotFocusAction="TextBoxGotFocus\txtFirstName_GotFocus" TextChangedAction="TextBoxGotFocus\txtFirstName_TextChanged"></vt:TextBox>

            <vt:Label runat="server" Text="Last Name :" Left="15px" Top="70px" ID="Label2"></vt:Label>

            <vt:TextBox runat="server" Top="70px" Left="160px" Text="Enter Last Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtLastName" TabIndex="2" GotFocusAction="TextBoxGotFocus\txtLastName_GotFocus" TextChangedAction="TextBoxGotFocus\txtLastName_TextChanged"></vt:TextBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="470px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="510px" Width="360px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Focus First Name >>" Width="180px" Top="640px" TabIndex="3" ID="btnChangeFocus" GotFocusAction="TextBoxGotFocus\btnChangeFocus_GotFocus" ClickAction="TextBoxGotFocus\btnChangeFocus_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

