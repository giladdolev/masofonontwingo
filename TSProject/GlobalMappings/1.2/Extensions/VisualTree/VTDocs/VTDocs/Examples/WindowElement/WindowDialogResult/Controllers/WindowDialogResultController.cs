using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowDialogResultController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowDialogResult());
        }
        private WindowDialogResult ViewModel
        {
            get { return this.GetRootVisualElement() as WindowDialogResult; }
        }

        /// <summary>
        /// Handles the Click event of the btnShowDialog control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public async void btnShowDialog_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            
            ViewModel.frmDialog = WindowDialog.Instance;

            
            DialogResult dr = await ViewModel.frmDialog.ShowDialog();
            if (dr == DialogResult.OK)
                textBox1.Text = "User clicked OK button";
            else
                textBox1.Text = "User clicked Cancel button";

		}


        /// <summary>
        /// Handles the Click event of the btnOk control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            WindowElement window = this.GetRootVisualElement() as WindowElement;
            if (window != null)
            {
                window.DialogResult = DialogResult.OK;
                window.Close();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            WindowElement window = this.GetRootVisualElement() as WindowElement;
            if (window != null)
            {
                window.DialogResult = DialogResult.Cancel;
                window.Close();
            }
        }
    }
}