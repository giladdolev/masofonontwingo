<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="MaskedTextBox Regex" ID="windowView1" Height="770px" LoadAction="MaskedTextBoxRegex\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Regex" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the input Regex to use at run time. 

            Syntax: public string Regex { get; set; }
            Value: A String representing the current Regex. The default value is the empty string
             which allows any input.
            Regex must be a string composed of one or more of the special characters.
             Click the following button See all special characters in regular expressions."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Button runat="server" Text="Open Regex Characters Table >>" Top="230px" Width="200px" ID="btnSpecialCharacterTable" ClickAction="MaskedTextBoxRegex\btnSpecialCharacterTable_Click"></vt:Button>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="285px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can examine the Regex property by selecting a predefine regular
             expressions from the comboBox list below or by defining a regular expression by writing it in 
            the comboBox. 
            This MaskedTextBox Regex is initially set to '{A.*Z}'. Enter Text in the MaskedTextBox to verify
             the possible strings for the given regex. See yellow label for more information."
            Top="335px" ID="Label3">
        </vt:Label>

        <vt:MaskedTextBox runat="server" ID="TestedMaskedTextBox" Regex="{A.*Z}" Top="455px" Left="80px"></vt:MaskedTextBox>

        <vt:Label runat="server" SkinID="Log" Top="495px" ID="lblLog" Width="550px" Height="90px"></vt:Label>

        <vt:Label runat="server" Top="620px" Text="Select a Regex: " ID="Label1"></vt:Label>

        <vt:ComboBox runat="server" ID="combo1" Text=""  Width="195px" Left="150px" SelectedIndexChangedAction="MaskedTextBoxRegex\btnSetMask_Click" Top="620px">
            <Items>
                <vt:ListItem runat="server" Text="\w{3,25}"></vt:ListItem>
                <vt:ListItem runat="server" Text="[A-Za-z0-9]+"></vt:ListItem>
                <vt:ListItem runat="server" Text="[0-9A-Za-z]{8,25}"></vt:ListItem>
                <vt:ListItem runat="server" Text="\d{3,25}"></vt:ListItem>
                <vt:ListItem runat="server" Text="[cmf]an"></vt:ListItem>
                <vt:ListItem runat="server" Text="[^ghd]an"></vt:ListItem>
                <vt:ListItem runat="server" Text="waz{3,10}up"></vt:ListItem>
                <vt:ListItem runat="server" Text="(\d+)x(\d+)"></vt:ListItem>
                <vt:ListItem runat="server" Text="(\d+)x(\1)"></vt:ListItem>
                <vt:ListItem runat="server" Text="0[xX][0-9a-fA-F]+"></vt:ListItem>
                <vt:ListItem runat="server" Text="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}"></vt:ListItem>
                <vt:ListItem runat="server" Text="^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:Button runat="server" ID="btnSetRegex" Left="410px" Text="Set Rgex" Top="620px" ClickAction="MaskedTextBoxRegex\btnSetMask_Click"></vt:Button>

        <vt:Label runat="server" Top="650px" Text="Enter Text: " ID="Label2"></vt:Label>

        <vt:TextBox runat="server" ID="textBox1" Width="195px" Text="" SelectedIndexChangedAction="MaskedTextBoxRegex\combo1_SelectedIndexChanged" Left="150px" Top="650px"></vt:TextBox>

        <vt:Button runat="server" ID="btnSetText" Left="410px" Text="Set Text" Top="650px" ClickAction="MaskedTextBoxRegex\btnSetText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
