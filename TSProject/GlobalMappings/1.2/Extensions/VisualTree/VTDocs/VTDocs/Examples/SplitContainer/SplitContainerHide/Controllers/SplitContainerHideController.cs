using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerHideController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //SplitContainerAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Visible value is: " + TestedSplitContainer.Visible;
        }

        public void btnShow_Click(object sender, EventArgs e)
        {

            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedSplitContainer.Show();
            lblLog.Text = "Visible value is: " + TestedSplitContainer.Visible;
        }

        public void btnHide_Click(object sender, EventArgs e)
        {

            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedSplitContainer.Hide();
            lblLog.Text = "Visible value is: " + TestedSplitContainer.Visible;
        }
    }
}