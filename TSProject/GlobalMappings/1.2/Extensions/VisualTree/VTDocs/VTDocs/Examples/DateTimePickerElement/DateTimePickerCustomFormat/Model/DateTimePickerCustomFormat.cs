﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class DateTimePickerCustomFormat : WindowElement
    {

        
         FormatStringTabel _frmTable;

        public DateTimePickerCustomFormat()
        {

        }

        public FormatStringTabel frmTable
        {
            get { return this._frmTable; }
            set { this._frmTable = value; }
        }

    }
}