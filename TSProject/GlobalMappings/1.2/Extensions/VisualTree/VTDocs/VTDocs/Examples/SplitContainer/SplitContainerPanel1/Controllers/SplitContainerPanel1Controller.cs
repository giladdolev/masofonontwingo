using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>

    public class SplitContainerPanel1Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnGetPanel1EnabledValue_Click(object sender, EventArgs e)
        {
            SplitContainer splitContainer1 = this.GetVisualElementById<SplitContainer>("splitContainer1");
            MessageBox.Show("Panel1 Enalbed value is: " + splitContainer1.Panel1.Enabled.ToString());

        }

        private void btnChangePanel1Enabled_Click(object sender, EventArgs e)
        {
            SplitContainer splitContainer1 = this.GetVisualElementById<SplitContainer>("splitContainer1");
            if (splitContainer1.Panel1.Enabled == false)
                splitContainer1.Panel1.Enabled = true;
            else
                splitContainer1.Panel1.Enabled = false;


        }
    }
}