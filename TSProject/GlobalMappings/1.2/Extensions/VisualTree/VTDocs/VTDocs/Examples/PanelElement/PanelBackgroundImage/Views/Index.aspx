<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel BackgroundImage Property" ID="windowView2" LoadAction="PanelBackgroundImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImage" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image displayed in the control" Top="75px" ID="lblDefinition"></vt:Label>

        <!-- TestedPanel1 -->
        <vt:Panel runat="server" Text="Panel" Top="120px" ID="TestedPanel1"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="210px" ID="lblLog1"></vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="BackgroundImage property of this 'TestedPanel' is initially set with image" Top="330px" ID="lblExp1"></vt:Label>

        <!-- TestedPanel2 -->
        <vt:Panel runat="server" Text="TestedPanel" BackgroundImage="~/Content/Images/icon.jpg" Top="385px" ID="TestedPanel2"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="475px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change BackgroundImage >>" Top="535px" Width="180px" ID="btnChangeBackgroundImage" ClickAction="PanelBackgroundImage\btnChangeBackgroundImage_Click"></vt:Button>



    </vt:WindowView>

</asp:Content>
