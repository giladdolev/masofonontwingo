<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox FindStringExact Method" ID="windowView1" LoadAction="ComboBoxFindStringExact\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="FindStringExact" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Finds the item that exactly matches the specified string.

            Syntax: public int FindStringExact(string s)
            
            Return Value: The zero-based index of the first item found; returns -1 if no match is found, or 0 
            if the s parameter specifies Empty.
            The search performed by this method is not case-sensitive."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="245px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Insert a string into the TextBox bellow and press the FindStringExact button.
            Note: The first Item in the list is an empty string." 
            Top="295px" ID="lblExp2">
        </vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" Top="385px" ID="TestedComboBox"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Text="Insert a String to find..." ID="lblLog" Top="425px"></vt:Label>

        <vt:Label runat="server" Text="Search String in ComboBox:" ID="Label2" Top="495px"></vt:Label>

        <vt:TextBox runat="server" Top="495px" Left="250px" ID="txtInput" Text="Text Input" TextChangedAction="ComboBoxFindStringExact\TextBox_TextChanged" WaitMaskDisabled="true"></vt:TextBox>

        <vt:Button runat="server" Text="FindStringExact >>" Left="430px" Top="495px" ID="btnFindString" ClickAction="ComboBoxFindStringExact\btnFindString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
