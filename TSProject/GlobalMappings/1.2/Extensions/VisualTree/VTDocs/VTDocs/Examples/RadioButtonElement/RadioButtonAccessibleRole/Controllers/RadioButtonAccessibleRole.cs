using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoAccessibleRole = this.GetVisualElementById<RadioButtonElement>("rdoAccessibleRole");

            MessageBox.Show(rdoAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoAccessibleRole = this.GetVisualElementById<RadioButtonElement>("rdoAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.RadioButton;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.RadioButton)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else if (rdoAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                rdoAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = rdoAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}