<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   
     <vt:WindowView runat="server" Text="Tested Window" Top="57px" Left="100px" ID="windowView2" Height="600px" Width="700px" LoadAction="SizeGripStyleTestedWindow\OnLoad">

         <vt:Label runat="server" Text="SizeGripStyle is initially set to 'Hide'" Top="9px" Left="41px" ID="label1" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="9.75pt" Height="16px" TabIndex="4" Width="682px">
		</vt:Label>

         <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change SizeGripStyle Property" Top="200px" Left="200px" ClickAction="SizeGripStyleTestedWindow\btnChangeSizeGripStyle_Click" ID="btnChangeSizeGripStyle" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="40px" TabIndex="1" Width="250px">
		</vt:Button>

          <vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="350px" Left="200px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="250px">
		</vt:TextBox>

        </vt:WindowView>
</asp:Content>