using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicListViewController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form_Load(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView2");
            ListViewItem[] lstitems = new ListViewItem[3];
            lstitems[0] = new ListViewItem("ListViewItem1");
            lstitems[1] = new ListViewItem("ListViewItem2");
            lstitems[2] = new ListViewItem("ListViewItem3");
            listView.Items.AddRange(lstitems);

          
        }
        /// <summary>
        /// Handles the Click event of the Command1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Change_Header_Visibility(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (listView.View == System.Web.VisualTree.ItemsView.Details)
                listView.View = System.Web.VisualTree.ItemsView.LargeIcon;
            else
                listView.View = System.Web.VisualTree.ItemsView.Details;

            lblLog1.Text = "ListView.View : " + listView.View;
        }

        private void SelectionChangedAction(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            TextBoxElement txtBox = this.GetVisualElementById<TextBoxElement>("txtBox");

            txtBox.Text = "Selected Items: " + String.Join(", ", listView.SelectedItems.Select(item => item.Text));
            
        }
        private void click(object sender, EventArgs e)
        {
           

        }
        private void check(object sender, EventArgs e)
        {
            

        }
        private void chamge(object sender, EventArgs e)
        {
          

        }

    }
}