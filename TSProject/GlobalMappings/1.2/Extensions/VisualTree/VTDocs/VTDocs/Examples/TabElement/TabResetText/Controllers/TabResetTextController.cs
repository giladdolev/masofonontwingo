using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabsResetTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TabsAlignment
        public void btnInvokeResetText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabsResetText = this.GetVisualElementById<TabElement>("tabsResetText");
            if (tabsResetText.Text != "")
            {
                tabsResetText.ResetText();
                textBox1.Text = "ResetText() is invoked, the Tabs Text is now empty";
            }
            else
            {
                tabsResetText.TabItems["tabPage1"].Text = "tabPage1";
                tabsResetText.TabItems["tabPage2"].Text = "tabPage2";
                textBox1.Text = "";
            }
        }

    }
}