<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Dock" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewDock/Form_Load">
        <vt:Label runat="server" Text="Initialized Component :ListView Dock is set to the Bottom" Top="400px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:ListView runat="server" ID="listView2" Top="450" Left="50" Height="200px" Width="300px" Dock="Bottom">
            <Columns>
                <vt:ColumnHeader runat="server" Text="h1" Width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="h2" Width="200"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="h3" Width="200"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="aaaa">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="11111"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="22222"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="bbbbb">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="33333"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="44444"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ListView runat="server" ID="listView1" Top="160px" Left="50px" Height="150px" Width="500px"/>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Change ListView1 Docking" Top="112px" Left="130px" ID="btnDock" Height="30px" TabIndex="1" Width="250px" ClickAction="ListViewDock\btnDock_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
