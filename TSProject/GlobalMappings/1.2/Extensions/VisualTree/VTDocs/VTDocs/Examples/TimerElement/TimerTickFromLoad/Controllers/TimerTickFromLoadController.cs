using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerTickFromLoadController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        

        public ActionResult Index()
        {
            return View();
        }


        //Tick event from Form Load
        public void Form_Load(object sender, EventArgs e)
        {
            TimerElement timer1 = this.GetVisualElementById<TimerElement>("timer1");
            timer1.Interval = 2000;
            timer1.Tick += tick_Action;
            timer1.Enabled = true;
            timer1.Start();
        }

        public void tick_Action(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");
            textBox1.Text += "1";
            textBox2.Text += "Tick Event is Invoked\\r\\n";
        }

    }
}