<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label ToolTipText Property" ID="windowView1" LoadAction="LabelToolTipText\OnLoad">          

        <vt:Label runat="server" SkinID="Title" Text="ToolTipText" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Represents a small rectangular pop-up window that displays a brief description 
            of a control's purpose when the user rests the pointer on the control.
            
            Syntax: public string ToolTipText { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ToolTipText property of this Label is initially set to: 'MyToolTip'" Top="250px" ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" CssClass="vt-test-label" ToolTipText="MyToolTip" Top="310px" ID="TestedLabel"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="340px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ToolTipText value >>"  Top="420px" ID="btnChangeToolTipText" Width="180px" ClickAction="LabelToolTipText\btnChangeToolTipText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>