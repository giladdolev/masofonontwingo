using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //SplitContainerAlignment
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splVisible = this.GetVisualElementById<SplitContainer>("splVisible");

            if (splVisible.Visible == true) //Get
            {
                splVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + splVisible.Visible;
            }
            else
            {
                splVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + splVisible.Visible;
            }
        }

      //  splText_TextChanged
        public void splVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}