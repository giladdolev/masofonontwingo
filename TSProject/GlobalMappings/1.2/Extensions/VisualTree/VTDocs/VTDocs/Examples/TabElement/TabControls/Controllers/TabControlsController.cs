using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabControlsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnAddControls_Click(object sender, EventArgs e)
        {
            TabElement tabControls = this.GetVisualElementById<TabElement>("tabControls");

            tabControls.TabItems["tabPage1"].Controls.Add(new LabelElement { Text = "newLabel1", ID = "label1", Left = 20, Top = 30, Height = 15, Width = 100 });
            tabControls.TabItems["tabPage2"].Controls.Add(new LabelElement { Text = "newLabel1", ID = "label1", Left = 20, Top = 30, Height = 15, Width = 100 });
            tabControls.TabItems["tabPage1"].Controls.Add(new LabelElement { Text = "newLabel2", ID = "label2", Left = 20, Top = 100, Height = 15, Width = 100 });
            tabControls.TabItems["tabPage2"].Controls.Add(new LabelElement { Text = "newLabel2", ID = "label2", Left = 20, Top = 100, Height = 15, Width = 100 });
        }

    }
}