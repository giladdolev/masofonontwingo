using System;
using System.Data;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxMatchComboboxWidthController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement testedComboBox1 = this.GetVisualElementById<ComboBoxElement>("testedComboBox1");
            ComboBoxElement testedComboBox2 = this.GetVisualElementById<ComboBoxElement>("testedComboBox2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog.Text = "TestedComboBox1 MatchComboboxWidth value is: " + testedComboBox1.MatchComboboxWidth + '.' + "\\r\\nTestedComboBox2 MatchComboboxWidth value is: " + testedComboBox2.MatchComboboxWidth + '.';

        }


    }
}