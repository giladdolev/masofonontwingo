using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxAppearanceController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAppearance_Click(object sender, EventArgs e)
        {
            CheckBoxElement ckhAppearanceChange = this.GetVisualElementById<CheckBoxElement>("ckhAppearanceChange");

            if (ckhAppearanceChange.Appearance == Appearance.Button)
            {
                ckhAppearanceChange.Appearance = Appearance.Normal;
            }
            else 
            {
                ckhAppearanceChange.Appearance = Appearance.Button;
            }
            
        }

    }
}