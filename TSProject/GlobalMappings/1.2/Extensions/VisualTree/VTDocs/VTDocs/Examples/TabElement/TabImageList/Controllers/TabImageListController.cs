using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabImageListController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab1 = this.GetVisualElementById<TabElement>("TestedTab1");
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTab1.ImageList == null)
            {
                lblLog1.Text = "The ImageList value is null ";
            }
            else
                lblLog1.Text = "The ImageList value is not null";

            lblLog2.Text = "ImageList  value: " + TestedTab2.ImageList + "\\r\\nImageList ID value: " + TestedTab2.ImageList.ID;
        }

        public void btnChangeTabImageList_Click(object sender, EventArgs e)
        {
            TabElement TestedTab2 = this.GetVisualElementById<TabElement>("TestedTab2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ImageList imageList1 = this.GetVisualElementById<ImageList>("imageList1");
            ImageList imageList2 = this.GetVisualElementById<ImageList>("imageList2");
           

            if (TestedTab2.ImageList == imageList1)
            {
                TestedTab2.ImageList = imageList2;
                lblLog2.Text = "ImageList  value: " + TestedTab2.ImageList + "\\r\\nImageList ID value: " + TestedTab2.ImageList.ID;
                
            }
            else
            {
                TestedTab2.ImageList = imageList1;
                lblLog2.Text = "ImageList value: " + TestedTab2.ImageList + "\\r\\nImageList ID value: " + TestedTab2.ImageList.ID;
            }
        }

    }
}