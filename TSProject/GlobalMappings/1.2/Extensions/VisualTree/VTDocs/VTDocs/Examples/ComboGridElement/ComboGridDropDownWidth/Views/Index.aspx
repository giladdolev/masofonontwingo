<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid DropDownWidth Property" Height="800px" ID="windowView1" LoadAction="ComboGridDropDownWidth\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="DropDownWidth" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the of the drop-down portion of a ComboGrid.
            
            Syntax: public int DropDownWidth { get; set; }
            
            If a value has not been set for the DropDownWidth, this property returns the Width of the ComboGrid.
            The width of the drop-down cannot be smaller than the ComboGrid width."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboGrid runat="server" Top="210px" Text="ComboGrid" ID="ComboGrid" CssClass="vt-ComboGrid"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="400px" ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="The following example, shows the usage of the ComboGrid DropDownWidth property.
            DropDownWidth property of this TestedComboGrid is initially set to 250.
            Click the 'Change DropDownWidth Value' button to change the DropDownWidth." Top="450px" ID="lblExp2">
        </vt:Label>  

        <vt:ComboGrid runat="server" Top="530px" Text="TestedComboGrid" ID="TestedComboGrid" CssClass="vt-TestedComboGrid" DropDownWidth="250"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="680px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change DropDownWidth Value >>" Top="730px" Width="200px" ID="btnChange" ClickAction="ComboGridDropDownWidth\btnChange_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
