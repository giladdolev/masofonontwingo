using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxTabIndexTabStopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnChangeTabIndex_Click(object sender, EventArgs e)
        {
            RichTextBox btnChangeTabIndex = this.GetVisualElementById<RichTextBox>("btnChangeTabIndex");
            RichTextBox RichTextBox1 = this.GetVisualElementById<RichTextBox>("RichTextBox1");
            RichTextBox RichTextBox2 = this.GetVisualElementById<RichTextBox>("RichTextBox2");
            RichTextBox RichTextBox4 = this.GetVisualElementById<RichTextBox>("RichTextBox4");
            RichTextBox RichTextBox5 = this.GetVisualElementById<RichTextBox>("RichTextBox5");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (RichTextBox1.TabIndex == 1)
            {
                RichTextBox5.TabIndex = 1;
                RichTextBox4.TabIndex = 2;
                RichTextBox2.TabIndex = 4;
                RichTextBox1.TabIndex = 5;

                textBox1.Text = "\\r\\nRichTextBox1 TabIndex: " + RichTextBox1.TabIndex + "\\r\\nRichTextBox2 TabIndex: " + RichTextBox2.TabIndex + "\\r\\nRichTextBox4 TabIndex: " + RichTextBox4.TabIndex + "\\r\\nRichTextBox5 TabIndex: " + RichTextBox5.TabIndex;       

            }
            else
            { 
                RichTextBox1.TabIndex = 1;
                RichTextBox2.TabIndex = 2;
                RichTextBox4.TabIndex = 4;
                RichTextBox5.TabIndex = 5;

                textBox1.Text = "\\r\\nRichTextBox1 TabIndex: " + RichTextBox1.TabIndex + "\\r\\nRichTextBox2 TabIndex: " + RichTextBox2.TabIndex + "\\r\\nRichTextBox4 TabIndex: " + RichTextBox4.TabIndex + "\\r\\nRichTextBox5 TabIndex: " + RichTextBox5.TabIndex;
            }
        }
        private void btnChangeTabStop_Click(object sender, EventArgs e)
        {
            RichTextBox btnChangeTabStop = this.GetVisualElementById<RichTextBox>("btnChangeTabStop");
            RichTextBox RichTextBox3 = this.GetVisualElementById<RichTextBox>("RichTextBox3");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");


            if (RichTextBox3.TabStop == false)
            {
                RichTextBox3.TabStop = true;
                textBox2.Text = "RichTextBox3 TabStop: \\r\\n" + RichTextBox3.TabStop;

            }
            else
            {
                RichTextBox3.TabStop = false;
                textBox2.Text = "RichTextBox3 TabStop: \\r\\n" + RichTextBox3.TabStop;

            }
        }
    }
}