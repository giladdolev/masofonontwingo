<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button BorderStyle Property" ID="windowView1" LoadAction="ButtonBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <!-- TestedButton1 -->
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="200px" ID="TestedButton1"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="250px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this TestedButton is initially set to 'Dotted'" Top="380px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedButton2 -->
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" BorderStyle="Dotted" Top="450px" ID="TestedButton2"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="575px" ID="btnChangeButtonBorderStyle" ClickAction="ButtonBorderStyle\btnChangeButtonBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>


