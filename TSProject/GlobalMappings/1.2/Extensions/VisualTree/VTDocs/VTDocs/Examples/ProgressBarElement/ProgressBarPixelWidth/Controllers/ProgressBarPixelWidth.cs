using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPixelWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelWidth value: " + TestedProgressBar.PixelWidth + '.';
        }

        private void btnChangePixelWidth_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedProgressBar.PixelWidth == 250)
            {
                TestedProgressBar.PixelWidth = 500;
                lblLog.Text = "PixelWidth value: " + TestedProgressBar.PixelWidth + '.';
            }
            else
            {
                TestedProgressBar.PixelWidth = 250;
                lblLog.Text = "PixelWidth value: " + TestedProgressBar.PixelWidth + '.';
            }
        }
      
    }
}