﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumn HeaderText Property" ID="windowView1" LoadAction="GridGridColumnHeaderText\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="HeaderText" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the caption text on the column's header cell.
            
            Syntax: public string HeaderText { get; set; }
            
            The default is an empty string." Top="75px"  ID="lblDefinition"></vt:Label>
              
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px"  ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="The  HeaderText property of these 3 columns is initially set to Column1, Column2 and Column3" Top="265px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server"  Top="310px" Width="410px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server"
                     ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="125px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="125px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="125px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Width="410px" Height ="50px" Top="440px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change column1 HeaderText >>" Width="180px" Top="540px" ID="btnChangeColumn1HeaderText" ClickAction="GridGridColumnHeaderText\btnChangeColumn1HeaderText_Click"></vt:Button>

        <vt:Button runat="server" Text="Change column2 HeaderText >>" Left="290px" Width="180px" Top="540px" ID="btnChangeColumn2HeaderText" ClickAction="GridGridColumnHeaderText\btnChangeColumn2HeaderText_Click"></vt:Button>

        <vt:Button runat="server" Text="Change column3 HeaderText >>" Left="500px" Width="180px" Top="540px" ID="btnChangeColumn3HeaderText" ClickAction="GridGridColumnHeaderText\btnChangeColumn3HeaderText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
