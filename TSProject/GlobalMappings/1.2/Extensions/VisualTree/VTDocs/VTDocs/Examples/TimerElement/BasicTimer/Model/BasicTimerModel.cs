﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class BasicTimerModel
    {
        /// <summary>
		/// 
		/// </summary>
		private int tickCount;

		/// <summary>
		/// Gets or sets the .
		/// </summary>
		/// <value>The .</value>
		public int TicksCount
		{
            get { return this.tickCount; }
            set { this.tickCount = value; }
		}
	
    }
}