<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox MaximumSize Property" ID="windowView" LoadAction="RichTextBoxMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedRichTextBox' is width: 240px and height: 100px. 
            Its MaximumSize is initially set to width: 180px and height: 90px.
            To cancel 'TestedRichTextBox' MaximumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label> 

        <vt:RichTextBox runat="server" Left="80px" Height="100px" Width="240px" Top="360px" Text="TestedRichTextBox" ID="TestedRichTextBox" MaximumSize="180, 90"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="465px" ID="lblLog" Width="400px" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="550px" Width="200px" ID="btnChangeRtfMaximumSize" ClickAction="RichTextBoxMaximumSize\btnChangeRtfMaximumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="550px" width="200px" Left="310px" ID="btnSetToZeroRtfMaximumSize" ClickAction="RichTextBoxMaximumSize\btnSetToZeroRtfMaximumSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>









