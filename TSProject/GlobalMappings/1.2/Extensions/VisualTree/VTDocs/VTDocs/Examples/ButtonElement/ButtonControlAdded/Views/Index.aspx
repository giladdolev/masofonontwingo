﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button ControlAdded Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
      
        <vt:Button runat="server" Text="Tested Button" Top="100px" Left="140px" ID="btnTestedButton" Height="36px" Width="220px" ControlAddedAction ="ButtonControlAdded\btnTestedButton_ControlAdded "></vt:Button>

        <vt:Button runat="server" Text="Invoke Add()" Top="45px" Left="140px" ID="btnInvokeAdd" Height="36px" Width="220px" ClickAction="ButtonControlAdded\btnAdd_Click"></vt:Button>

         <vt:TextBox runat="server" Text="" Top="180px" Left="120px"  ID="textBox1" Multiline="true"  Height="50px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
