<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Anchor property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Initialized Component : Label Anchor is set to Bottom" Top="320px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:Panel runat="server" Top="350px" Left="50px" ID="Panel2" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="100px" TabIndex="56" Width="400px"></vt:Panel>
        <vt:Label runat="server" Text="Anchor testing initialized" Top="380px" Left="130px" ID="labelTestingInitialized" Width="350px"  Anchor="Bottom"></vt:Label>
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Panel runat="server" Top="160px" Left="50px" ID="Panel1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="100px" TabIndex="56" Width="400px"></vt:Panel>
        <vt:Label runat="server" Text="Anchor testing Run Time" Top="190px" Left="130px" ID="label" Width="350px"></vt:Label>
        <vt:Button runat="server" Text="Change Label Anchor options" Top="112px" Left="130px" ID="btnAnchor" TabIndex="1" Width="200px" ClickAction="LabelAnchor\btnAnchor_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>