<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer PixelTop Property" ID="windowView1" LoadAction="SplitContainerPixelTop\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelTop" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the top edge of the control
             and the top edge of its container's client area.
            
            Syntax: public int PixelTop { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Top property of this SplitContainer is initially set to: 310px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Top="310px" ID="TestedSplitContainer"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelTop value >>" Top="470px" ID="btnChangePixelTop" Width="180px" ClickAction="SplitContainerPixelTop\btnChangePixelTop_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
