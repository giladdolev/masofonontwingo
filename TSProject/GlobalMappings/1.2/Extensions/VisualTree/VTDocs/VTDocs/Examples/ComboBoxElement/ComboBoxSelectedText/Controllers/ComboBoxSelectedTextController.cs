using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxSelectedTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ///Add another for Remarks  - msdn contains a lot of explanations for the SelectedText property

            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedComboBox1.Items.Add("First Item");
            TestedComboBox1.Items.Add("Second Item");
            TestedComboBox1.Items.Add("Third Item");
            if (TestedComboBox1.SelectedText == null)
               lblLog1.Text = "SelectedText value: null";
            else
                lblLog1.Text = "SelectedText value: " + TestedComboBox1.SelectedText.ToString();


            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox2.Items.Add("First Item");
            TestedComboBox2.Items.Add("Second Item");
            TestedComboBox2.Items.Add("Third Item");
            TestedComboBox2.SelectedText = "OnLoad Text";
            if (TestedComboBox1.SelectedText == null)
                lblLog2.Text = "SelectedText value: null";
            else
                lblLog2.Text = "SelectedText value: " + TestedComboBox2.SelectedText.ToString();
            

        }


        public void btnSetSelectedText_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox2.SelectedText = "New Text ";

            lblLog2.Text = "SelectedText value: " + TestedComboBox2.SelectedText.ToString();

        }

        public void btnGetSelectedText_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "SelectedText value: " + TestedComboBox2.SelectedText.ToString();

        }
    }
}