using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPaddingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangePadding_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpRuntimePadding = this.GetVisualElementById<GroupBoxElement>("grpRuntimePadding");

            if (grpRuntimePadding.Padding == new Padding(3, 3, 3, 3)) //Get
            {
                grpRuntimePadding.Padding = new Padding(3, 3, 3, 50); //Set
                textBox1.Text = "The padding property value is: " + grpRuntimePadding.Padding;
            }
            else if (grpRuntimePadding.Padding == new Padding(3, 3, 3, 50))
            {
                grpRuntimePadding.Padding = new Padding(3, 3, 50, 3); 
                textBox1.Text = "The padding property value is: " + grpRuntimePadding.Padding;
            }
            else if (grpRuntimePadding.Padding == new Padding(3, 3, 50, 3)) 
            {
                grpRuntimePadding.Padding = new Padding(3, 50, 3, 3); 
                textBox1.Text = "The padding property value is: " + grpRuntimePadding.Padding;
            }
            else if (grpRuntimePadding.Padding == new Padding(3, 50, 3, 3)) 
            {
                grpRuntimePadding.Padding = new Padding(50, 3, 3, 3);
                textBox1.Text = "The padding property value is: " + grpRuntimePadding.Padding;
            }
            else 
            {
                grpRuntimePadding.Padding = new Padding(3, 3, 3, 3); 
                textBox1.Text = "The padding property value is: " + grpRuntimePadding.Padding;
            }       
        }

    }
}