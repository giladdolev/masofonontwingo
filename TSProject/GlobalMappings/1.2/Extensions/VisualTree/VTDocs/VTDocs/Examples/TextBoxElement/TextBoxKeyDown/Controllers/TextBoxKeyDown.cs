using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxKeyDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Fill up the form...";
        }

        public void txtFirstName_KeyDown(object sender, KeyEventArgs e)
        {
            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTextBox: FirstName, KeyDown event is invoked";         

            if (txtFirstName.Text != "")
            {
                lblLog.Text = "A key was typed into FirstName TextBox\\r\\nText Value: " + txtFirstName.Text + '.';
            }
            else
            {
                lblLog.Text = "A key was typed into FirstName TextBox\\r\\nText Value: Empty String";
            }

            if (e.KeyCode == Keys.Tab)
            {
                txtLastName.Focus();
            }
        }

        public void txtFirstName_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            string key = "";

            txtFirstName.ForeColor = Color.Black;

            // in order to simulate placeholder text:
            // the dimed text is changed to black and the initial text is cleared
            if (txtFirstName.Text.IndexOf("Enter First Name") == 0)
            {
                key = txtFirstName.Text.Substring(17);
                txtFirstName.Clear();
                txtFirstName.Text = key;
            }
            else if (txtFirstName.Text.IndexOf("Enter First Nam") == 0)
            {
                txtFirstName.Clear();
            }

            if (txtFirstName.Text != "")
            {
                lblLog.Text = "A key was typed into FirstName TextBox\\r\\nText Value: " + txtFirstName.Text + '.';
            }
            else
            {
                lblLog.Text = "A key was typed into FirstName TextBox\\r\\nText Value: Empty String";
            }
        }

        public void txtLastName_KeyDown(object sender, KeyEventArgs e)
        {
            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTextBox: LastName, KeyDown event is invoked";

            if (txtLastName.Text != "")
            {
                lblLog.Text = "A key was typed into LastName TextBox\\r\\nText Value: " + txtLastName.Text + '.';
            }
            else
            {
                lblLog.Text = "A key was typed into LastName TextBox\\r\\nText Value: Empty String";
            }

            if (e.KeyCode == Keys.Tab)
            {
                txtFirstName.Focus();
            }
        }

        public void txtLastName_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            string key = "";

            txtLastName.ForeColor = Color.Black;

            // in order to simulate placeholder text:
            // the dimed text is changed to black and the initial text is cleared
            if (txtLastName.Text.IndexOf("Enter Last Name") == 0)
            {
                key = txtLastName.Text.Substring(16);
                txtLastName.Clear();
                txtLastName.Text = key;
            }
            else if (txtLastName.Text.IndexOf("Enter Last Nam") == 0)
            {
                txtLastName.Clear();
            }

            if (txtLastName.Text != "")
            {
                lblLog.Text = "A key was typed into LastName TextBox\\r\\nText Value: " + txtLastName.Text + '.';
            }
            else
            {
                lblLog.Text = "A key was typed into LastName TextBox\\r\\nText Value: Empty String";
            }
        }
    }
}