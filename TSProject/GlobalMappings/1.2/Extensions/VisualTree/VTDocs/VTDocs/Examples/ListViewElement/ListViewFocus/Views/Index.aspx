<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Focus" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewFocus/Form_Load">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ListView runat="server" ID="listView1" Top="160px" Left="50px" Height="150px" Width="500px"/>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Get ListView1 Focus" Top="112px" Left="130px" ID="btnFocus" Height="30px" TabIndex="1" Width="250px" ClickAction="ListViewFocus\btnFocus_Click"></vt:Button>
        <vt:TextBox runat="server" Text="" Top="350px" Left="200px" ID="txtFocus" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>
    </vt:WindowView>
</asp:Content>
