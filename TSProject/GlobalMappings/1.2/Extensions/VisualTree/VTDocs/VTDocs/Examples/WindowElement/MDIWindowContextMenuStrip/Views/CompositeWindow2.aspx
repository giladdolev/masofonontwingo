<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server" >

    <vt:CompositeView runat="server" Text="Composite" Height="250" ID="Composite" ContextMenuStripID="contextMenuStrip1" LoadAction="CompositeWindow2\cmp_load" BackColor="Yellow" Dock="Fill" UnloadAction="CompositeWindow2\cmp_unload">
     
        <vt:Button runat="server" Text="close" Top="0" ID="Button1" Height="40" Width="80" ClickAction="CompositeWindow2\rmove"></vt:Button>

        <vt:Button runat="server" Text="change color" Top="80" ID="Button2" Height="40" Width="80"  ClickAction="CompositeWindow2\change_btn_color"></vt:Button>


        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip1" >
            <vt:ToolBarMenuItem runat ="server" id ="exitToolStripMenuItem" Text="Exit" ClickAction = "PanelContextMenuStrip\exitToolStripMenuItem_Click"></vt:ToolBarMenuItem>
	</vt:ContextMenuStrip>

    </vt:CompositeView>

    

</asp:Content>
