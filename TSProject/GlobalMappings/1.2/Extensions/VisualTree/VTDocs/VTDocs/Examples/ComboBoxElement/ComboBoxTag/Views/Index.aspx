
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Tag Property" ID="windowView1" LoadAction="ComboBoxTag\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Tag" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the object that contains data about the control.
            
            Syntax: public object Tag { get; set; }
            Value: An Object that contains data about the control. The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:ComboBox runat="server" Text="ComboBox" Top="175px" ID="TestedComboBox1">
            <Items>
                <vt:ListItem runat="server" Text="aa"></vt:ListItem>
                <vt:ListItem runat="server" Text="bb"></vt:ListItem>
                <vt:ListItem runat="server" Text="cc"></vt:ListItem>
                <vt:ListItem runat="server" Text="55"></vt:ListItem>
            </Items>
        </vt:ComboBox>
        <vt:Label runat="server" SkinID="Log" Top="225px" Width="410" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Tag property of this 'TestedComboBox' is initially set to the string 'New Tag'." Top="330px"  ID="lblExp1"></vt:Label>     
        
        <vt:ComboBox runat="server" Text="TestedComboBox" Top="385px" Tag="New Tag." ID="TestedComboBox2">
            <Items>
                <vt:ListItem runat="server" Text="ee"></vt:ListItem>
                <vt:ListItem runat="server" Text="ff"></vt:ListItem>
                <vt:ListItem runat="server" Text="gg"></vt:ListItem>
                <vt:ListItem runat="server" Text="77"></vt:ListItem>
            </Items>
        </vt:ComboBox>
        <vt:Label runat="server" SkinID="Log" Top="435px" Width="410" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Tag value >>" Top="490px" ID="btnChangeBackColor" ClickAction="ComboBoxTag\btnChangeTag_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

