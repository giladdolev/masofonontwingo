using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridSelectionChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(int));
            source.Rows.Add("James", "Dean", 1);
            source.Rows.Add("Bob", "Marley", 2);
            source.Rows.Add("Dana", "International", 3);

            TestedComboGrid.ValueMember = "Column1";
            TestedComboGrid.DisplayMember = "Column3";

            TestedComboGrid.DataSource = source;

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ComboGrid SelectedIndex value: " + TestedComboGrid.SelectedIndex + 
                "\\r\\nComboGrid SelectedValue value: " + TestedComboGrid.SelectedValue +
                "\\r\\nComboGrid SelectedItem value: " + TestedComboGrid.SelectedItem;
        }

        public void TestedComboGrid_SelectionChanged(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nComboGrid SelectionChanged event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ComboGrid SelectedIndex value: " + TestedComboGrid.SelectedIndex +
                "\\r\\nComboGrid SelectedValue value: " + TestedComboGrid.SelectedValue.ToString() +
                "\\r\\nComboGrid SelectedItem value: " + TestedComboGrid.SelectedItem.ToString();
        }
    }
}