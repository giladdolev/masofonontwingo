﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid FrozenCols Property" ID="windowView1" LoadAction="GridFrozenCols\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="FrozenCols" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the number of frozen (editable but non-scrollable) columns.        

            Syntax: public int FrozenCols { get; set; }

            Cells in frozen columns can be selected and edited, but they remain visible when the user scrolls the 
            contents of the control horizontally." 
            Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Grid runat="server" Top="205px" Width="330px" ID="TestedGrid1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col4" HeaderText="Column4" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col5" HeaderText="Column5" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col6" HeaderText="Column6" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col7" HeaderText="Column7" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col8" HeaderText="Column8" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col9" HeaderText="Column9" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col10" HeaderText="Column10" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>           

        <vt:Label runat="server" SkinID="Log" Top="335px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="375px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="FrozenCols property of this 'TestedGrid' is initially set to '1', and you can change the value using the 
            radio buttons." Top="425px" ID="lblExp1"></vt:Label> 

        <vt:Grid runat="server" Top="485px" Width="330px" FrozenCols="1" ID="TestedGrid2" >
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col4" HeaderText="Column4" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col5" HeaderText="Column5" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col6" HeaderText="Column6" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col7" HeaderText="Column7" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col8" HeaderText="Column8" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col9" HeaderText="Column9" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col10" HeaderText="Column10" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="620px" ID="lblLog2"></vt:Label>

        <vt:RadioButton runat="server" Top="485px" left="460px" Text="Frozen Col No. : 0" ID="rbFrozenCols0" CheckedChangedAction="GridFrozenCols\rbFrozenCols0_CheckedChanged"></vt:RadioButton>
        <vt:RadioButton runat="server" Top="520px" left="460px" Text="Frozen Col No. : 1" ID="rbFrozenCols1" CheckedChangedAction="GridFrozenCols\rbFrozenCols1_CheckedChanged"></vt:RadioButton>
        <vt:RadioButton runat="server" Top="555px" left="460px" Text="Frozen Col No. : 2" ID="rbFrozenCols2" CheckedChangedAction="GridFrozenCols\rbFrozenCols2_CheckedChanged"></vt:RadioButton>

    </vt:WindowView>
</asp:Content>



