<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer Enable property" ID="windowView1" LoadAction="TimerEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets whether the timer is running.

            Syntax: public virtual bool Enabled { get; set; }
            true if the timer is currently enabled; otherwise, false. The default is false.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Label and TextBox to represent TestedTimer1 -->
        <vt:Label runat="server" SkinID="TimerLabel" Top="185px" ID="timer1Label"></vt:Label>

        <vt:TextBox runat="server" SkinID="TimerTextBox" Top="180px" ID="timer1TextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Enable property of this Timer is initially set true" Top="340px" ID="lblExp2"></vt:Label>

        <!-- Label and TextBox to represent TestedTimer2  -->
         <vt:Label runat="server" SkinID="TimerLabel" Top="385px" ID="timer2Label"></vt:Label>

        <vt:TextBox runat="server" SkinID="TimerTextBox" Top="380px" ID="timer2TextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enable value >>" Top="480px" Width="180px" ID="btnChangeTimerEnable" ClickAction="TimerEnabled\btnChangeTimerEnable_Click"></vt:Button>

    </vt:WindowView>

    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="TestedTimer1" TickAction="TimerEnabled\TestedTimer1_Tick">
		</vt:Timer>

        <vt:Timer runat="server" ID="TestedTimer2" Enabled="true" Interval="1000" TickAction="TimerEnabled\TestedTimer2_Tick">
		</vt:Timer>
	</vt:ComponentManager>

</asp:Content>
