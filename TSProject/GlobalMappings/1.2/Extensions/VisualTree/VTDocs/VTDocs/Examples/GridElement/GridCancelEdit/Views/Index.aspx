﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CancelEdit event" LoadAction="GridCancelEdit\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CancelEdit" ID="Label1">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the user started editing a cell but then cancelled the edit.

         Syntax: public event EventHandler<EventArgs> CancelEdit"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can edit the cell content by selecting it with the mouse, 
            and start typing keys. You can cancel the editing of the cell and return to the previous state, 
            by typing Esc Key."
            Top="235px" ID="Label4">
        </vt:Label>
        <vt:Grid runat="server" Top="325px" ID="TestedGrid" CancelEditAction="GridCancelEdit/TestedGrid_CancelEdit">
            <Columns>
                <vt:GridTextBoxColumn runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="460px" Width ="500px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Width ="500px" Top="500px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
