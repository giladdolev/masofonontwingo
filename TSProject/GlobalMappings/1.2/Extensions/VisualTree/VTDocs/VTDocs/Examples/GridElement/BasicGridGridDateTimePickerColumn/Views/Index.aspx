<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic GridGridDateTimePicker Column" ID="windowView" LoadAction="BasicGridGridDateTimePickerColumn\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="GridDateTimePickerColumn" ID="lblTitle" Left="200"></vt:Label>

        <vt:Label runat="server" Text="GridDateTimePickerColumn is a special ColumnElement of GridElement.

            syntax: public class GridDateTimePickerColumn : GridColumn
            
            DateTimePicker represents a Windows control that allows the user to select a date and a time
             and to display the date and time with a specified format."
             Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, 'Column1' in the grid is defined as GridDateTimePickerColumn." Top="290px"  ID="lblExp1"></vt:Label>     

        <vt:Grid runat="server" Width="350px" Top="355px" ID="TestedGrid">
            <Columns>
                <vt:GridDateTimePickerColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="150px"></vt:GridDateTimePickerColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
    </vt:WindowView>
</asp:Content>

 <%--<vt:Label runat="server" SkinID="Log" Top="480px" Height="40" Width="350" ID="lblLog"  Text="Column 1 is a DateTimePicker column"></vt:Label>    
 --%> 