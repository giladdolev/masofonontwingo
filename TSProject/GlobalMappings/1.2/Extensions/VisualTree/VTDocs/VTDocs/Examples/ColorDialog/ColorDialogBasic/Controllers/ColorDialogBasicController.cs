using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ColorDialogBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnSelectColor_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ColorDialog colorDialog1 = this.GetVisualElementById<ColorDialog>("colorDialog1");
           
            CommonDialog.ShowDialog();
            colorDialog1.Color = Color.Pink;
            textBox1.BackColor = colorDialog1.Color;
        }
    }
}