<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Clear Method" ID="windowView2" LoadAction="ListViewClear\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Clear" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Removes all items from the ListView.

            Syntax: public void Clear()"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can add Items to the ListView using the 'Add Item' button, 
            and than you can use the 'Clear' button to Removes all items from the ListView."
            Top="245px" ID="lblExp2">
        </vt:Label>


        <vt:ListView runat="server" Text="ListView" CssClass="vt-test-cmb" Top="320px" ID="TestedListView">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" Width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" Width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" Width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>


        <vt:Label runat="server" SkinID="Log" Width="450px" ID="lblLog" Top="435"></vt:Label>

        <vt:Button runat="server" Text="Clear >>" Top="525px" Left="300px" ID="btnClear" ClickAction="ListViewClear\btnClear_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Item >>" Top="525px" ID="btnAddItem" ClickAction="ListViewClear\AddItems_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

