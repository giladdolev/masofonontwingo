<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer Stop Method" ID="windowView2" LoadAction="TimerStop\OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="Stop" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Stops the timer.

            Syntax: public void Stop()

            You can also stop the timer by setting the Enabled property to 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Press the 'Stop' button to invoke the 'Stop' method." Top="280px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" SkinID="TimerLabel" Top="335px" ID="timerLabel"></vt:Label>

        <vt:TextBox runat="server" SkinID="TimerTextBox" Top="330px" ID="timerTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="370"></vt:Label>

        <vt:Button runat="server" Text="Start >>" Left="300" Top="435px" ID="btnStart" ClickAction="TimerStop\btnStart_Click"></vt:Button>

        <vt:Button runat="server" Text="Stop >>" Top="435px" ID="btnStop" ClickAction="TimerStop\btnStop_Click"></vt:Button>

    </vt:WindowView>

    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="TestedTimer" Enabled="true" Interval="1000" TickAction="TimerStop\TestedTimer_Tick"></vt:Timer>
	</vt:ComponentManager>

</asp:Content>

        