<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox PerformMouseClick() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseClick of 'Tested RichTextBox'" Top="45px" Left="140px" ID="btnPerformMouseClick" Height="36px" Width="300px" ClickAction="RichTextBoxPerformMouseClick\btnPerformMouseClick_Click"></vt:Button>


        <vt:RichTextBox runat="server" Text="Tested RichTextBox" Top="150px" Left="200px" ID="testedRichTextBox" Height="70px"  Width="200px" MouseClickAction="RichTextBoxPerformMouseClick\testedRichTextBox_MouseClick"></vt:RichTextBox>           

        

    </vt:WindowView>
</asp:Content>
