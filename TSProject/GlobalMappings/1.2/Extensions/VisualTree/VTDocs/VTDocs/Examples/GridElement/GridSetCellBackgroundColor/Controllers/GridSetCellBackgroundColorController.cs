using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
using System.Data;
namespace HelloWorldApp
{
    public class GridSetCellBackgroundColorController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void Page_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            DataTable source = new DataTable();
            source.Columns.Add("Field1", typeof(int));
            source.Columns.Add("Field2", typeof(string));


            source.Rows.Add(1, "James");
            source.Rows.Add(2, "Bob");
            source.Rows.Add(3, "Dana");
            source.Rows.Add(4, "Sara");

            gridElement.DataSource = source;
            lblLog1.Text = "Cell (-1,-1) BackgroundColor : - ";


        }

        private void Grid_SetCellBackgroundColorToRed(object sender, EventArgs e)
        {          
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "Cell (1,1) BackgroundColor : Red";
            gridElement.SetCellBackgroundColor(0, 0, Color.Red);

        }
        private void Grid_SetCellBackgroundColorToWhite(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "Cell (1,1) BackgroundColor : White";
            gridElement.SetCellBackgroundColor(0, 0, Color.White);
        }
        private void Grid_SetCellBackgroundColorToPurple(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "Cell (1,1) BackgroundColor : Purple";
            gridElement.SetCellBackgroundColor(0, 0, Color.Purple);
        }
        private void Grid_SetCellBackgroundColorToOrange(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "Cell (1,1) BackgroundColor : Orange";
            gridElement.SetCellBackgroundColor(0, 0, Color.Orange);
        }
        private void Grid_SetCellBackgroundColorToGray(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Cell (1,1) BackgroundColor : Gray";
            gridElement.SetCellBackgroundColor(0, 0, Color.Gray);
        } 
    }
}
