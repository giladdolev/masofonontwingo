﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    public class TwoWindowsForm1Controller : Controller
    {
        public ActionResult TwoWindowsForm1()
        {
            return View(new TwoWindowsForm1());
        
        
        
        }



        private async Task button1_Click(object sender, EventArgs e)
        {

            (this.GetRootVisualElement() as WindowElement).PerformWindowClosing(new WindowClosingEventArgs(System.Web.VisualTree.CloseReason.UserClosing,false));



        }
    }
}
