using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RichTextBoxAlignment
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RichTextBox rtfVisible = this.GetVisualElementById<RichTextBox>("rtfVisible");

            if (rtfVisible.Visible == true) //Get
            {
                rtfVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + rtfVisible.Visible;
            }
            else
            {
                rtfVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + rtfVisible.Visible;
            }
        }

      //  rtfText_TextChanged
        public void rtfVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}