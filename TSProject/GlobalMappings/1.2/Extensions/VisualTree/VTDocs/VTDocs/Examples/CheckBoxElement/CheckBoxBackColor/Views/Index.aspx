<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox BackColor Property" ID="windowView1" LoadAction="CheckBoxBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:CheckBox runat="server" Text="CheckBox" Top="150px" ID="TestedCheckBox1"></vt:CheckBox>
        <vt:Label runat="server" SkinID="Log" Top="190px" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="255px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this 'TestedCheckBox' is initially set to Gray" Top="305px"  ID="lblExp1"></vt:Label>     
        
        <vt:CheckBox runat="server" Text="TestedCheckBox" BackColor ="Gray" Top="355px" ID="TestedCheckBox2"></vt:CheckBox>
        <vt:Label runat="server" SkinID="Log" Top="395px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Top="460px" ID="btnChangeBackColor" Width="185px" ClickAction="CheckBoxBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
