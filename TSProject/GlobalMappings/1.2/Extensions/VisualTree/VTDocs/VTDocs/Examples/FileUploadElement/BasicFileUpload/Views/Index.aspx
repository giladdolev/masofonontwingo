<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="FileUpload" >
        
         <vt:Label runat="server" SkinID="SubTitle" Text="FileUpload" Top="15px" ID="lblTitle" Left="300" Font-Size="19pt" Font-Bold="true">
        </vt:Label>
        <vt:Label runat="server" Text="How to use FileUpload ."
            Top="65px" ID="lblDefinition">
        </vt:Label>
         <vt:FileUpload runat="server" ID="fileipload1" Top="100" Left="100" Width="300" Height="100"></vt:FileUpload>
    </vt:WindowView>

</asp:Content>
