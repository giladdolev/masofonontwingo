using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            NumericUpDownElement testedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("testedNumericUpDown");
            LayoutEventArgs args = new LayoutEventArgs(testedNumericUpDown, "Some String");

            testedNumericUpDown.PerformLayout(args);
        }

        public void testedNumericUpDown_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedNumericUpDown Layout event method is invoked");
        }

    }
}