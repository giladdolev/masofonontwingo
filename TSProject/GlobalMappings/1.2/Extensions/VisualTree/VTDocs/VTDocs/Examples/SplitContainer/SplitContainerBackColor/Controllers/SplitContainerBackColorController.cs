using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedSplitContainer1.BackColor.ToString();

            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedSplitContainer2.BackColor.ToString();

        }

        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedSplitContainer2.BackColor == Color.Orange)
            {
                TestedSplitContainer2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedSplitContainer2.BackColor.ToString();
            }
            else
            {
                TestedSplitContainer2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedSplitContainer2.BackColor.ToString();

            }
        }

    }
}