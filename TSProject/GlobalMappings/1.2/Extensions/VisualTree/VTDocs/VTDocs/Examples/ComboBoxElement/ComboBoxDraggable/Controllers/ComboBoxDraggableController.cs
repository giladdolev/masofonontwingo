using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ComboBoxElement cmb = this.GetVisualElementById<ComboBoxElement>("cmb1");
            cmb.Items.Add("aaa");
            cmb.Items.Add("bbb");
        }

        private void btnDraggable_Click(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");
            TextBoxElement txtDraggable = this.GetVisualElementById<TextBoxElement>("txtDraggable");

            if (cmb1.Draggable == false)
            {
                cmb1.Draggable = true;
                txtDraggable.Text = cmb1.Draggable.ToString();
            }
            else
            {
                cmb1.Draggable = false;
                txtDraggable.Text = cmb1.Draggable.ToString();
            }
        }
    }
}