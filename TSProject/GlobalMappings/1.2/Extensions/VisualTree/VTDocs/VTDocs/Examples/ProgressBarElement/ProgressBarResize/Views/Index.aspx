<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Resize Property" Top="57px" Left="11px" ID="windowView1" Height="750px" Width="768px">
         
        <vt:Label runat="server" Text="ProgressBar 'Size' is initially set to (200,36)" Top="80px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:ProgressBar runat="server" Text="Tested ProgressBar" BackColor="LightGray" Top="95px" Left="140px" ID="testedProgressBar" Height="36px"  TabIndex="1" Width="200px"  ResizeAction="ProgressBarResize\testedProgressBar_Resize"></vt:ProgressBar>

        <vt:Button runat="server" Text="Resize ProgressBar" Top="95px" Left="480px" ID="btnChangeSize" Height="36px"  TabIndex="1" Width="250px" ClickAction="ProgressBarResize\btnChangeSize_Click" ></vt:Button>

         <vt:Label runat="server" Text="Event Log" Top="150px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>


        <vt:TextBox runat="server" Text="" Top="165px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
