using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridGridDateTimePickerColumnReadOnlyController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void Page_load(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedGrid.Rows.Add("6/8/1974 12:00:00 AM", "6/8/1974 12:00:00 AM", "6/8/1974 12:00:00 AM");
            TestedGrid.Rows.Add("6/8/1974 12:00:00 AM", "6/8/1974 12:00:00 AM", "6/8/1974 12:00:00 AM");
            TestedGrid.Rows.Add("6/8/1974 12:00:00 AM", "6/8/1974 12:00:00 AM", "6/8/1974 12:00:00 AM");

            lblLog1.Text = "Column1 ReadOnly: " + TestedGrid.Columns[0].ReadOnly +
                "\\r\\nColumn2 ReadOnly: " + TestedGrid.Columns[1].ReadOnly +
                "\\r\\nColumn3 ReadOnly: " + TestedGrid.Columns[2].ReadOnly;
        }

        private void btnSetColumnReadOnly_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            ComboBoxElement cmbSelectColumn = this.GetVisualElementById<ComboBoxElement>("cmbSelectColumn");
            CheckBoxElement chkReadOnly = this.GetVisualElementById<CheckBoxElement>("chkReadOnly");

            switch (cmbSelectColumn.Text)
            {
                case "Column1":
                    TestedGrid.Columns[0].ReadOnly = chkReadOnly.IsChecked;
                    PrintAllColumnsReadOnly();
                    break;
                case "Column2":
                    TestedGrid.Columns[1].ReadOnly = chkReadOnly.IsChecked;
                    PrintAllColumnsReadOnly();
                    break;
                case "Column3":
                    TestedGrid.Columns[2].ReadOnly = chkReadOnly.IsChecked;
                    PrintAllColumnsReadOnly();
                    break;
                default:
                    lblLog1.Text = "Please Select a column from the first ComboBox";
                    break;
            }

        }

        private void PrintAllColumnsReadOnly()
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog1.Text = "Column1 ReadOnly: " + TestedGrid.Columns[0].ReadOnly +
                "\\r\\nColumn2 ReadOnly: " + TestedGrid.Columns[1].ReadOnly +
                "\\r\\nColumn3 ReadOnly: " + TestedGrid.Columns[2].ReadOnly;

        }

    }
}
