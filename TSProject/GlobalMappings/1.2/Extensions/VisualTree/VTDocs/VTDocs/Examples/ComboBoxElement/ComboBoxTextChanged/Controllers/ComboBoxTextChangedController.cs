using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxTextChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("*Item2");
            TestedComboBox.Items.Add("Item 3");
            lblLog.Text = "ComboBox Text value: " + TestedComboBox.Text;

        }

        public void TestedComboBox_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nComboBox TextChanged event is invoked";
            lblLog.Text = "ComboBox Text value: " + TestedComboBox.Text;
        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");

            if (TestedComboBox.Text == "Some Text")
                TestedComboBox.Text = "New Text";
            else
                TestedComboBox.Text = "Some Text";
        }
    }
}
