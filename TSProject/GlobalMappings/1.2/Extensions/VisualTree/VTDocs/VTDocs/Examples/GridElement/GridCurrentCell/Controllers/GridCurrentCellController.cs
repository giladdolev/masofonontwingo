using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridCurrentCellController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void Form_load(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");

            lblLog1.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void CellSelectionChanged_Click(object sender, GridElementSelectionChangeEventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            lblLog1.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void btnChangeCurrentCell_Click(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            ComboBoxElement cmbIndexes = this.GetVisualElementById<ComboBoxElement>("cmbIndexes");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            gridElement.CurrentCell = gridElement.Rows[1].Cells[2];

            lblLog1.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;

        }
    }
}

