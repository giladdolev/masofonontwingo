using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "NumericUpDown MaximumSize value: " + TestedNumericUpDown.MaximumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;
        }

        public void btnChangeNudMaximumSize_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedNumericUpDown.MaximumSize == new Size(100, 120))
            {
                TestedNumericUpDown.MaximumSize = new Size(200, 10);
                lblLog1.Text = "NumericUpDown MaximumSize value: " + TestedNumericUpDown.MaximumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;
            }
            else if (TestedNumericUpDown.MaximumSize == new Size(200, 10))
            {
                TestedNumericUpDown.MaximumSize = new Size(50, 80);
                lblLog1.Text = "NumericUpDown MaximumSize value: " + TestedNumericUpDown.MaximumSize + "\\r\\NumericUpDown Size value: " + TestedNumericUpDown.Size;
            }
            else
            {
                TestedNumericUpDown.MaximumSize = new Size(100, 120);
                lblLog1.Text = "NumericUpDown MaximumSize value: " + TestedNumericUpDown.MaximumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;
            }

        }

        public void btnSetToZeroNudMaximumSize_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedNumericUpDown.MaximumSize = new Size(0, 0);
            lblLog1.Text = "NumericUpDown MaximumSize value: " + TestedNumericUpDown.MaximumSize + "\\r\\nNumericUpDown Size value: " + TestedNumericUpDown.Size;

        }

    }
}