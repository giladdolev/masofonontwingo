﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CurrentCellChanged event" LoadAction="GridCurrentCellChanged\Form_load">

        <vt:Label runat="server" SkinID="Title" Text="CurrentCellChanged" Top="15px" ID="lblTitle" Left="270"> </vt:Label>
        <vt:Label runat="server" Text="Occurs when the CurrentCell property changes. 
            
            syntax : public event GridElementCellEventHandler CurrentCellChanged"

           Top="65px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="180px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can change the CurrentCell value by clicking on one of the grid  
           cells or by clicking the 'Change CurrentCell to (1,2)' button.
            Each invocation of the CurrentCellChanged event should add a new line to the event log. " Top="220px" ID="Label2">
        </vt:Label>
        <vt:Grid runat="server" Top="300px" ID="TestedGrid" CurrentCellChangedAction="GridCurrentCellChanged\TestedGrid_CurrentCellChanged">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="425px" ID="lblLog1"></vt:Label>
      
        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="470px" ID="txtEventLog"></vt:TextBox>

          
        <vt:Button runat="server" Text="Change CurrentCell to (1,2) >>" Top="600px" Width="180px" ID="btnChangeCurrentCell" ClickAction="GridCurrentCellChanged\btnChangeCurrentCell_Click"></vt:Button>

                
    </vt:WindowView>

</asp:Content>
