<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox SelectTextOnFocus Property" ID="windowView1" LoadAction="ComboBoxSelectTextOnFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectTextOnFocus" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether combobox text selected on focus ."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboBox runat="server" ID="comboBox1" SelectTextOnFocus="false" Text="" DisplayMember="text" ValueMember="value"  Top="50">
            <Items>
                <vt:ListItem runat="server" Text="ff"></vt:ListItem>
                <vt:ListItem runat="server" Text="4"></vt:ListItem>
                <vt:ListItem runat="server" Text="55"></vt:ListItem>
                <vt:ListItem runat="server" Text="gg"></vt:ListItem>
            </Items>
        </vt:ComboBox>
       
        <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" Text="When the SelectTextOnFocus is set to false the Text not selected on focus ."
            Top="270px" ID="lblExp1">
        </vt:Label>
   


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="SelectTextOnFocus value is set to 'true'
            the text should be Selected on focus ." Top="370px" ID="lblExp2">
        </vt:Label>

        <vt:ComboBox runat="server" ID="comboBox2" Text="" DisplayMember="text" ValueMember="value" Top="430" SelectTextOnFocus="true">
            <Items>
                <vt:ListItem runat="server" Text="ff"></vt:ListItem>
                <vt:ListItem runat="server" Text="4"></vt:ListItem>
                <vt:ListItem runat="server" Text="55"></vt:ListItem>
                <vt:ListItem runat="server" Text="gg"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        


    </vt:WindowView>
</asp:Content>
