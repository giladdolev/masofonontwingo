<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" StartPosition="WindowsDefaultLocation" Text="Home" Top="57px" Left="11px" ID="windowView1" BackColor="Orange" BorderStyle="None" Font-Bold="True" Font-Names="Tahoma" Font-Size="9pt" Height="400px" TabIndex="-1" Width="600px" LoadAction="BasicTree\load">

        <vt:Tree runat="server" ID="tree1" Width="500px" Height="250px" Top="10px" Left="10px" >
        </vt:Tree>

    </vt:WindowView>
</asp:Content>
