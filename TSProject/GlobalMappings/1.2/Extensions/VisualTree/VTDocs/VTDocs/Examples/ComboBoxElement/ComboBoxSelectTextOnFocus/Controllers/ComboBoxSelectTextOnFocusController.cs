using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxSelectTextOnFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedCheckBox1 = this.GetVisualElementById<ComboBoxElement>("comboBox1");
            ComboBoxElement TestedCheckBox2 = this.GetVisualElementById<ComboBoxElement>("comboBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            lblLog1.Text = "SetSelectedTextOnFocus Value: " + TestedCheckBox1.SelectTextOnFocus;


        }

    }
}