﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="25206" Height="800px" ID="windowView1" LoadAction="GridChangingCellInSelectionChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="170px" Text="Testing editing cell inside SelectionChanged" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Test:" Top="70px" ID="lblTest"></vt:Label>

        <vt:Label runat="server" Text="Bug description: When we change a cell value inside SelectionChanged event  we cant select a cell
             to select because the focus is lost - for example we cant write into a textBox or open a comboBox because
             the focus is lost. 
            
            The following test we have two grids. The right grid is initialized to Grid.RowHeaderVisible = false (for extra 
            testing.) We SelectionChanged event method for each grid, inside the selectionChanged
             event methods we change the text of Grid1 cell(0,0).

            Edit at least one cell of each column and verify the editing succeeded and that focus remains on the cell
             you select. "
            Top="120px" ID="lblExp2">
        </vt:Label>

         <vt:Grid runat="server" Top="350px" Height="135px" ID="TestedGrid1" Width="310px" BeforeSelectionChangedAction="GridChangingCellInSelectionChanged\TestedGrid1_BeforeSelectionChanged" SelectionChangedAction="GridChangingCellInSelectionChanged\TestedGrid1_SelectionChanged">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="95px" CssClass="vt-grid1-Column1"></vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="95px"></vt:GridCheckBoxColumn>
                <vt:GridComboBoxColumn  runat="server" ID="Col1" HeaderText="Column3" Height="20px" Width="95px"></vt:GridComboBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Grid runat="server" Top="350px" Height="135px" Width="310px" Left="400px" RowHeaderVisible="false" ID="TestedGrid2" BeforeSelectionChangedAction="GridChangingCellInSelectionChanged\TestedGrid2_BeforeSelectionChanged" SelectionChangedAction="GridChangingCellInSelectionChanged\TestedGrid2_SelectionChanged">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="95px" CssClass="vt-grid2-Column1"></vt:GridTextBoxColumn>
                <vt:GridTextButtonColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="95px"></vt:GridTextButtonColumn>
                <vt:GridDateTimePickerColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="95px"></vt:GridDateTimePickerColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Height="50px" Top="495px" Width="550px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="570px" Width="550px" Height="120px" ID="txtEventLog"></vt:TextBox>
        
        
        
        <vt:Button runat="server" Text="Select b >>" Top="715px" ID="btnSelectb" ClickAction="GridChangingCellInSelectionChanged\btnSelectb_Click"></vt:Button>

        <vt:Button runat="server" Text="Select e >>" Top="715px" Left="280px" ID="btnSelecte" ClickAction="GridChangingCellInSelectionChanged\btnSelecte_Click"></vt:Button>

         <vt:Button runat="server" Text="Clear Event Log >>" Top="715px" Left="480px" ID="btnClear" ClickAction="GridChangingCellInSelectionChanged\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
