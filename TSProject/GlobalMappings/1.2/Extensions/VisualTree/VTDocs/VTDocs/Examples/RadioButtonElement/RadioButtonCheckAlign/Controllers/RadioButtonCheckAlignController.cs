using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonCheckAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment

        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton1 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton1");
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "CheckAlign value: " + TestedRadioButton1.CheckAlign;
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + '.';

        }

        private void btnTopLeft_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.TopLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        private void btnTopCenter_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.TopCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        private void btnTopRight_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.TopRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }


        private void btnMiddleLeft_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.MiddleLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        private void btnMiddleCenter_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.MiddleCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        private void btnMiddleRight_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.MiddleRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        private void btnBottomLeft_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.BottomLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        private void btnBottomCenter_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.BottomCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        private void btnBottomRight_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            TestedRadioButton2.CheckAlign = ContentAlignment.BottomRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "CheckAlign value: " + TestedRadioButton2.CheckAlign + ".";
        }

        //public void btnChangeCheckAlign_Click(object sender, EventArgs e)
        //{
        //    TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
        //    RadioButtonElement rdoRuntimeCheckAlign = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeCheckAlign");
            
        //    if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.BottomCenter)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.BottomLeft;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.BottomLeft)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.BottomRight;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.BottomRight)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.MiddleCenter;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.MiddleCenter)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.MiddleLeft;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.MiddleLeft)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.MiddleRight;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.MiddleRight)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.TopCenter;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.TopCenter)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.TopLeft;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else if (rdoRuntimeCheckAlign.CheckAlign == ContentAlignment.TopLeft)
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.TopRight;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //    else 
        //    {
        //        rdoRuntimeCheckAlign.CheckAlign = ContentAlignment.BottomCenter;
        //        textBox1.Text = "CheckAlign is set to: " + rdoRuntimeCheckAlign.CheckAlign;
        //    }
        //}

    }
}