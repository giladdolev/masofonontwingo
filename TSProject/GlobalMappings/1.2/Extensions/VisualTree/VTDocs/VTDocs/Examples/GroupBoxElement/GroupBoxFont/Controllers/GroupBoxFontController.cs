using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.WebControls;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        private void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox1 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox1");
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedGroupBox1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGroupBox1);
            lblLog2.Text = "Font value: " + TestedGroupBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGroupBox2);
        }
        private void btnChangeGroupBoxFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            GroupBoxElement TestedGroupBox2 = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox2");
            if (TestedGroupBox2.Font.Underline == true)
            {
                TestedGroupBox2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedGroupBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGroupBox2);
            }
            else if (TestedGroupBox2.Font.Bold == true)
            {
                TestedGroupBox2.Font = new Font("Miriam Fixed", 20, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedGroupBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGroupBox2);
            }
            else if (TestedGroupBox2.Font.Italic == true)
            {
                TestedGroupBox2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedGroupBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGroupBox2);
            }
            else
            {
                TestedGroupBox2.Font = new Font("SketchFlow Print", 15, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedGroupBox2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedGroupBox2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}