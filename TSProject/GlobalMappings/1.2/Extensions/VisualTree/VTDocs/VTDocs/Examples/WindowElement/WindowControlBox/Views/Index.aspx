<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window ControlBox Property" Left="5px" ID="windowView1" LoadAction="WindowControlBox\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ControlBox" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether a control box is displayed
             in the caption bar of the form.
            
            Syntax: public bool ControlBox { get; set; }
        'true' if the form displays a control box in the upper left corner of the form; otherwise, 'false'.
             The default is 'true'.
            " Top="75px"  ID="lblDefinition"></vt:Label>

              
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ControlBox property of this window is initially set to false" Top="300px"  ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="380px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ControlBox value >>" Top="440px" Width="160px" ID="btnChangeControlBox" ClickAction="WindowControlBox\btnChangeControlBox_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

