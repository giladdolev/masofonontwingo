using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarRightToLeftController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnChangeRightToLeft_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
			ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            
            if (toolStrip1.RightToLeft == System.Web.VisualTree.RightToLeft.No)
            {
                toolStrip1.RightToLeft = System.Web.VisualTree.RightToLeft.Yes;
                textBox1.Text = toolStrip1.RightToLeft.ToString();
            }
            else if (toolStrip1.RightToLeft == System.Web.VisualTree.RightToLeft.Yes)
            {
                toolStrip1.RightToLeft = System.Web.VisualTree.RightToLeft.Inherit;
                textBox1.Text = toolStrip1.RightToLeft.ToString();
            }
            else 
            {
                toolStrip1.RightToLeft = System.Web.VisualTree.RightToLeft.No;
                textBox1.Text = toolStrip1.RightToLeft.ToString();
            }
                
		}

    }
}