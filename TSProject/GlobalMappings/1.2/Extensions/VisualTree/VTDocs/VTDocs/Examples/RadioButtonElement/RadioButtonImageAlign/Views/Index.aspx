<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton ImageAlign Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Initialize - RadioButton ImageAlign is initialized with BottomLeft" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:RadioButton runat="server" Text="Initialize Test" ImageAlign ="BottomLeft" Image ="~/Content/Elements/RadioButton.png" Top="45px" Left="140px" ID="rdoImage" Height="100px" TabIndex="1" Width="300px"></vt:RadioButton>  
          
        <vt:Label runat="server" Text="Runtime" Top="220px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

         <vt:RadioButton runat="server" Text="Runtime ImageAlign" Image ="~/Content/Elements/RadioButton.png" Top="235px" Left="140px" ID="rdoRuntimeImageAlign" Height="100px"  TabIndex="1" Width="300px" ></vt:RadioButton>

        <vt:Button runat="server" Text="Change ImageAlign" Top="235px" Left="400px" ID="btnChangeImageAlign" Height="36px"  TabIndex="1" Width="220px" ClickAction="RadioButtonImageAlign\btnChangeImageAlign_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
