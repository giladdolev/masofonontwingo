using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxThreeStateController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "ThreeState value: " + TestedCheckBox1.ThreeState.ToString();

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "ThreeState value: " + TestedCheckBox2.ThreeState.ToString() + ".";

        }

        private void CheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ThreeState value: " + TestedCheckBox1.ThreeState.ToString();
            lblLog1.Text += "\\r\\nCheckState value: " + TestedCheckBox1.CheckState;
        }

        private void TestedCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "ThreeState value: " + TestedCheckBox2.ThreeState.ToString() + ".";
            lblLog2.Text += "\\r\\nCheckState value: " + TestedCheckBox2.CheckState + "."; 
        }

        private void btnChangeThreeState_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedCheckBox2.ThreeState = !TestedCheckBox2.ThreeState;
            lblLog2.Text = "ThreeState value: " + TestedCheckBox2.ThreeState.ToString() + ".";
            lblLog2.Text += "\\r\\nCheckState value: " + TestedCheckBox2.CheckState + "."; 
        }
     
    }
}