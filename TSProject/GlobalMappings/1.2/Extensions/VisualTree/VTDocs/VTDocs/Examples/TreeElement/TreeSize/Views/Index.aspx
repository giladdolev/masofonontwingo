<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="Tree Size Property" ID="windowView1" LoadAction="TreeSize/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control in pixels.
            
            Syntax: public Size Size { get; set; }" Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Size property of this Tree is initially set to: Width='140px' Height='110px'" Top="235px" ID="lblExp"></vt:Label>

        <!-- TestedTree -->
        <vt:Tree runat="server" Text="TestedTree" Top="285px" ID="TestedTree">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="410px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Width="180px" Top="475px" ID="btnChangeTreeSize" ClickAction="TreeSize/btnChangeTreeSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
