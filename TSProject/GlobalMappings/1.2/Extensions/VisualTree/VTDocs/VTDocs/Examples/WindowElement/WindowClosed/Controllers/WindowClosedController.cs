using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowClosedController : Controller
    {

        
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowClosed());
        }
        private WindowClosed ViewModel
        {
            get { return this.GetRootVisualElement() as WindowClosed; }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowClosed()");

            ViewModel.TestedWin = VisualElementHelper.CreateFromView<ClosedTestedWindow>("ClosedTestedWindow", "Index2", null, argsDictionary, null);
            this.ViewModel.TestedWin.Show();
            textBox1.Text = "TestedWindow is Open";



        }

        public void btnClosed_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (this.ViewModel.TestedWin != null)
            {
                this.ViewModel.TestedWin.Close();
                textBox1.Text = "TestedWindow is Closedd";
            }

        }

    }
}   