<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Height Property" ID="windowView1" LoadAction="ProgressBarHeight\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="Height" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int Height { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this ProgressBar is initially set to: 80px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="310px" ID="TestedProgressBar" Height="80px" TabIndex="2"></vt:ProgressBar>    

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Height value >>" Top="470px" ID="btnChangeHeight" TabIndex="3" Width="180px" ClickAction="ProgressBarHeight\btnChangeHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
