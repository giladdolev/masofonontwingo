using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowIconController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
         
        }


        public void OnLoad(object sender, EventArgs e)
        {
            WindowElement window = this.GetVisualElementById<WindowElement>("windowView1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Icon Is initially set to " + window.Icon.Source.Substring(16);
        }

        public void btnChangeIcon_Click(object sender, EventArgs e)
		{
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
           
            WindowElement window = this.GetVisualElementById<WindowElement>("windowView1");
            if (window.Icon.Source == "/Content/Images/Tulips.jpg")
                window.Icon = new ResourceReference("/Content/Images/Desert.jpg");
            else
                window.Icon = new ResourceReference("/Content/Images/Tulips.jpg");

            lblLog.Text = "Icon was changed to " + window.Icon.Source.Substring(16);
		}

        public void btnOpenNewWinWithIcon_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowIcon");

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            WindowElement WindowIcon2 = VisualElementHelper.CreateFromView<WindowElement>("WindowIcon2", "Index2", null, argsDictionary, null);
            WindowIcon2.Show();

            lblLog.Text = "New Window with Icon was opened";
        }

    }
}