using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeSelectedItemController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            DataTable source = new DataTable();
            source.Columns.Add("ID", typeof(string));
            source.Columns.Add("ParentID", typeof(string));

            source.Rows.Add("1", "");
            source.Rows.Add("2", "1");
            source.Rows.Add("3", "2");
            source.Rows.Add("4", "2");
            source.Rows.Add("5", "");
            source.Rows.Add("6", "5");
            source.Rows.Add("7", "5");
            source.Rows.Add("8", "5");


            TestedTree1.ParentFieldName = "ParentID";
            TestedTree1.KeyFieldName = "ID";
            TestedTree1.DataSource = source;

            lblLog.Text = "Select TreeItems or Click the buttons...";
        }

        public void TestedTree_AfterSelect(object sender, TreeEventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTree1.SelectedItem != null)
                lblLog.Text = "Tree Grid View SelectedItem ID: " + TestedTree1.SelectedItem.Values[0];
            else
                lblLog.Text = "Tree Grid View SelectedItem: is null";
            if (TestedTree2.SelectedItem != null)
                lblLog.Text += "\\r\\nTree List View SelectedItem Name: " + TestedTree2.SelectedItem.Text;
            else
                lblLog.Text += "\\r\\nTree List View SelectedItem: is null";
        }

        public void btnSelect5_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTree1.SelectedItem = TestedTree1.Items[1];
        }

        public void btnSelectNode1_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTree2.SelectedItem = TestedTree2.Items[1];
        }

        public void btnSelect2_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTree1.SelectedItem = TestedTree1.Items[0].Items[0];
        }

        public void btnSelectNode01_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTree2.SelectedItem = TestedTree2.Items[0].Items[0];
        }
    }
}