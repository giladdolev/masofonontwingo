using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedRichTextBox.ClientSize;

        }
        public void btnChangeRichTextBoxClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.Width == 300)
            {
                TestedRichTextBox.ClientSize = new Size(200, 80);
                lblLog.Text = "ClientSize value: " + TestedRichTextBox.ClientSize;

            }
            else
            {
                TestedRichTextBox.ClientSize = new Size(300, 45);
                lblLog.Text = "ClientSize value: " + TestedRichTextBox.ClientSize;
            }

        }

    }
}