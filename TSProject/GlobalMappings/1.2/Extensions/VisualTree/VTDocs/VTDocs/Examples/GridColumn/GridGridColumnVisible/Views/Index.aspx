﻿ <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid GridColumn Visible Property" ID="windowView2" LoadAction="GridGridColumnVisible\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the column is visible.

            Syntax: public bool Visible { get; set; }
            
            Property Value: 'true' if the column is visible; otherwise, 'false'." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example Column1.Visible is initially set to false. Use the buttons below
             to change the grid columns visibility" 
            Top="265px" ID="lblExp2"></vt:Label>

        <vt:Grid runat="server" Top="330px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" Visible="false" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Width="350px"  ID="lblLog" Top="460px"></vt:Label>

        <vt:Button runat="server" Text="Change Column1 Visibility >>" Width="180px" Top="530px" ID="btnChangeCol1Visibility" ClickAction="GridGridColumnVisible\btnChangeCol1Visibility_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Column2 Visibility >>" Left="290px" Width="180px" Top="530px" ID="btnChangeCol2Visibility" ClickAction="GridGridColumnVisible\btnChangeCol2Visibility_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Column3 Visibility >>" Left="500px" Width="180px" Top="530px" ID="btnChangeCol3Visibility" ClickAction="GridGridColumnVisible\btnChangeCol3Visibility_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>




