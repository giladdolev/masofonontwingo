using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowBackgroundImageLayoutController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowBackgroundImageLayout());
        }
        private WindowBackgroundImageLayout ViewModel
        {
            get { return this.GetRootVisualElement() as WindowBackgroundImageLayout; }
        }

        public void btnChangeBackgroundImageLayout_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.BackgroundImageLayout == ImageLayout.Center)
            {
                this.ViewModel.BackgroundImageLayout = ImageLayout.None;
                textBox1.Text = this.ViewModel.BackgroundImageLayout.ToString();
            }
            else if (this.ViewModel.BackgroundImageLayout == ImageLayout.None)
            {
                this.ViewModel.BackgroundImageLayout = ImageLayout.Stretch;
                textBox1.Text = this.ViewModel.BackgroundImageLayout.ToString();
            }
            else if (this.ViewModel.BackgroundImageLayout == ImageLayout.Stretch)
            {
                this.ViewModel.BackgroundImageLayout = ImageLayout.Tile;
                textBox1.Text = this.ViewModel.BackgroundImageLayout.ToString();

            }
            else if (this.ViewModel.BackgroundImageLayout == ImageLayout.Tile)
            {
                this.ViewModel.BackgroundImageLayout = ImageLayout.Zoom;
                textBox1.Text = this.ViewModel.BackgroundImageLayout.ToString();
            }
            else
            {
                this.ViewModel.BackgroundImageLayout = ImageLayout.Center;
                textBox1.Text = this.ViewModel.BackgroundImageLayout.ToString();
            }
            
                
		}

    }
}