<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Leave event" ID="windowView2" LoadAction="TabLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the tabs,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:Tab runat="server" Top="335px" Height="90px" Width="180px" LeaveAction="TabLeave\tabButton1_Leave">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage 1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px">
                    <vt:Button runat="server" Top="15px" Left="15px" Text="Button 1" ID="btnButton1" TabIndex="1" ClickAction="TabLeave\btnButton1_Click"></vt:Button>
                </vt:TabItem>
            </TabItems>            
        </vt:Tab>

        <vt:Tab runat="server" Top="335px" Height="90px" Width="180px" Left="280px" LeaveAction="TabLeave\tabButton2_Leave">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage 2" Top="22px" Left="4px" ID="TabItem1" Height="74px" Width="192px">
                    <vt:Button runat="server" Top="15px" Left="15px" Text="Button 2" ID="btnButton2" TabIndex="1" ClickAction="TabLeave\btnButton2_Click"></vt:Button>
                </vt:TabItem>
            </TabItems>            
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="445px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="485px" Width="360px" Height="140px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

