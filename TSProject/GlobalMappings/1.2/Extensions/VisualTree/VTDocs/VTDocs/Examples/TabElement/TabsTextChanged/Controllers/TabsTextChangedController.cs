using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabsTextChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //TabAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");

            lblLog.Text = "Tab Text value: " + TestedTab.Text;

        }

        public void TestedTab_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nTab TextChanged event is invoked";
            lblLog.Text = "Tab Text value: " + TestedTab.Text;
        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");

            if (TestedTab.Text == "Some Text")
                TestedTab.Text = "New Text";
            else
                TestedTab.Text = "Some Text";
        }
    }
}