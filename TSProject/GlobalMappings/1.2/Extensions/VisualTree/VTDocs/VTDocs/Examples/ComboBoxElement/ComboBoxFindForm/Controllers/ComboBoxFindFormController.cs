using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxFindFormController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            ComboBoxElement cmb = this.GetVisualElementById<ComboBoxElement>("cmb1");
            cmb.Items.Add("aaa");
            cmb.Items.Add("bbb");
        }

        private void btnFindForm_Click(object sender, EventArgs e)
        {
            ComboBoxElement cmb1 = this.GetVisualElementById<ComboBoxElement>("cmb1");
            WindowElement myForm = this.GetRootVisualElement() as WindowElement;//<WindowElement>("windowView1");
            myForm.Text = "The Form before FindForm";
            // Get the form that the ComboBox control is contained within.
            myForm = cmb1.FindForm();
            // Set the text and color of the form containing the ComboBox.
            myForm.Text = "The Form after FindForm";
            myForm.BackColor = Color.Red;
        }
    }
}