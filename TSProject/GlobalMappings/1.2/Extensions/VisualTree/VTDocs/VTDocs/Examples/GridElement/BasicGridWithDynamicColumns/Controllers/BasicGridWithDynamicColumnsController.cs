using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class BasicGridWithDynamicColumnsController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }
        public void AddColumns()
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid10");
            GridColumn col;
            col = new GridColumn("New ");
            col.Width = 110;
            TestedGrid1.Columns.Add(col);
            txtEventLog.Text += "Added new column\n";
            TestedGrid1.RefreshData();
        }
        }
}
