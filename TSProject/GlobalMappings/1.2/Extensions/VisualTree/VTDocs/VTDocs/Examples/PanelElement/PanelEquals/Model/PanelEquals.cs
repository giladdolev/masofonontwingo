﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class PanelEquals : WindowElement
    {

        PanelElement _TestedPanel;

        public PanelEquals()
        {
            _TestedPanel = new PanelElement();
        }

        public PanelElement TestedPanel
        {
            get { return this._TestedPanel; }
            set { this._TestedPanel = value; }
        }
    }
}