<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer BackColor Property" ID="windowView1" LoadAction="SplitContainerBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            Syntax: public virtual Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="140px" ID="TestedSplitContainer1"></vt:SplitContainer>
        <vt:Label runat="server" SkinID="Log" Top="235px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="BackColor property of this 'TestedSplitContainer' is initially set to Gray" Top="350px"  ID="lblExp1"></vt:Label>     
        
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" BackColor ="Gray" Top="400px" ID="TestedSplitContainer2"></vt:SplitContainer>
        <vt:Label runat="server" SkinID="Log" Top="495px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Width="180px" Top="560px" ID="btnChangeBackColor" ClickAction="SplitContainerBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
