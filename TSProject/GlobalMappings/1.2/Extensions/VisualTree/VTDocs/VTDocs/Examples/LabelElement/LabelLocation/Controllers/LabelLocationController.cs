﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelLocationController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Location value: " + TestedLabel.Location;
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedLabel.Location == new Point(80, 310))
            {
                TestedLabel.Location = new Point(200, 290);
                lblLog.Text = "Location value: " + TestedLabel.Location;
            }
            else
            {
                TestedLabel.Location = new Point(80, 310);
                lblLog.Text = "Location value: " + TestedLabel.Location;
            }
        }
    }
}