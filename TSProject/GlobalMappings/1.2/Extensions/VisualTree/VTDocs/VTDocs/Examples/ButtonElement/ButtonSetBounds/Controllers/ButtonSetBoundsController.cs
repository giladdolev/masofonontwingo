using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;

        }
       


        public void btnSetBounds1Param_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.Left == 100)
            {
                TestedButton.SetBounds(80);
                lblLog.Text = "The method SetBounds(80) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;

            }
            else
            {
                TestedButton.SetBounds(100);
                lblLog.Text = "The method SetBounds(100) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;
            }

        }

        public void btnSetBounds2Params_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.Left == 100)
            {
                TestedButton.SetBounds(80, 390);
                lblLog.Text = "The method SetBounds(80,390) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;

            }
            else
            {
                TestedButton.SetBounds(100, 370);
                lblLog.Text = "The method SetBounds(100, 370) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;
            }

        }

        public void btnSetBounds3Params_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.Left == 100)
            {
                TestedButton.SetBounds(80, 390, 150);
                lblLog.Text = "The method SetBounds(80, 390, 150) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;

            }
            else
            {
                TestedButton.SetBounds(100, 370, 200);
                lblLog.Text = "The method SetBounds(100, 370, 200) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;
            }

        }

        public void btnSetBounds4Params_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton");
            if (TestedButton.Left == 100)
            {
                TestedButton.SetBounds(80, 390, 150, 36);
                lblLog.Text = "The method SetBounds(80, 390, 150, 36) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;

            }
            else
            {
                TestedButton.SetBounds(100, 370, 200, 50);
                lblLog.Text = "The method SetBounds(100, 370, 200, 50) was invoked\\r\\nThe Tested Button bounds are set to: \\r\\nLeft  " + TestedButton.Left + ", Top  " + TestedButton.Top + ", Width  " + TestedButton.Width + ", Height  " + TestedButton.Height;
            }

        }

    }
}