﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar ToolBarMenuItem Image Property" ID="windowView1" LoadAction ="ToolBarToolBarMenuItemImage\OnLoad">

       <vt:Label runat="server" SkinID="Title" Text="Image" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the image that is displayed on a ToolBarMenuItem.           

            Syntax: public ResourceReference Image { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:ToolBar runat="server" Text="ToolBar" Top="165px" Dock="None" ID="TestedToolBar1" CssClass="vt-test-toolb-1">
            <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item0" Text="File"  CssClass="vt-test-tbbutton-1-file">
                <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item0_0" Text="New..." Image="Content/Images/New.png" CssClass="vt-test-tbbutton-1-file-new"/>
                <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item0_1" Text="Open..." Image="Content/Images/Open.png" CssClass="vt-test-tbbutton-1-file-open"/>
                <vt:ToolBarMenuSeparator runat="server" />
                <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item0_2" Text="Exit" CssClass="vt-test-tbbutton-1-file-exit"/>
            </vt:ToolBarMenuItem>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item1" Text="New" Image="Content/Images/New.png" CssClass="vt-test-tbbutton-1-new"></vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" ID="ToolBar1Item2" Text="Open" Image="Content/Images/Open.png" CssClass="vt-test-tbbutton-1-open"></vt:ToolBarMenuItem>
        </vt:ToolBar>
        
        <vt:Label runat="server" SkinID="Log" Top="215px" Width="500px" Height="60px" ID="lblLog"></vt:Label>           

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="ToolBarMenuItems with the New text, their Image property is initially set to 'New.png'.
            And ToolBarMenuItems with the Open text, their Image property is initially set to 'Open.png'.
            The Image of the New ToolBarMenuItems will change to the Image of the Open ToolBarMenuItems,
            and vice versa, when clicking the 'Change Image' button." Top="375px" ID="lblExp1"></vt:Label> 

        <vt:ToolBar runat="server" Text="TestedToolBar" Top="475px" Dock="None" ID="TestedToolBar2" CssClass="vt-test-toolb-2">
            <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item0" Text="File" CssClass="vt-test-tbbutton-2-file">
                <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item0_0" Text="New..." Image="Content/Images/New.png" CssClass="vt-test-tbbutton-2-file-new"/>
                <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item0_1" Text="Open..." Image="Content/Images/Open.png" CssClass="vt-test-tbbutton-2-file-open"/>
                <vt:ToolBarMenuSeparator runat="server" />
                <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item0_2" Text="Exit" CssClass="vt-test-tbbutton-2-file-exit"/>
            </vt:ToolBarMenuItem>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item1" Text="New" Image="Content/Images/New.png" CssClass="vt-test-tbbutton-2-new"></vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" ID="ToolBar2Item2" Text="Open" Image="Content/Images/Open.png" CssClass="vt-test-tbbutton-2-open"></vt:ToolBarMenuItem>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="525px" Width="500px" Height="60px" ID="lblLog1"></vt:Label> 

        <vt:Button runat="server" Text="Change Image >>" Top="625px" ID="btnChangeImage" Width="180px" ClickAction="ToolBarToolBarMenuItemImage\btnChangeImage_Click"></vt:Button>
       
    </vt:WindowView>
</asp:Content>