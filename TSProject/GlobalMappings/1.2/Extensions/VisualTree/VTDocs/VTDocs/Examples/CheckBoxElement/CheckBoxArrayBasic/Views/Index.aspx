<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
<vt:WindowView runat="server" Text="Basic CheckBox Array" Width="770px" ID="windowView1" >

	 <vt:Label runat="server" SkinID="Title" Text="CheckBox Array" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="The following example demonstrate usage of an CheckBox Array declared in the aspx file and used
             at runtime - this is done by the GetVisualElementsById function

             Syntax: public static List<TVisualElement> GetVisualElementsById<TVisualElement>(this Controller controller, 
            string visualElementID) where TVisualElement : VisualElement." Top="75px"  ID="lblDefinition"></vt:Label>

       
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Use the buttons below to Check\UnCheck the array CheckBoxes" Top="260px" ID="lblExp1"></vt:Label> 

        <vt:GroupBox runat="server" Text="CheckBoxArr" Top="300px" Width="186px" Height="100px"  ID="Group" >
            <vt:CheckBox runat="server" Text="Check1" Top="10px" Left="10px" ID="_CheckBoxArr_0"  BackColor="LightSalmon" Height="30px" Width="73px">
		</vt:CheckBox>
		<vt:CheckBox runat="server" Text="Check2" Top="10px" Left="103px" ID="_CheckBoxArr_1" BackColor="LightSalmon" Height="30px" TabIndex="1" Width="73px">
		</vt:CheckBox>
		<vt:CheckBox runat="server" Text="Check3" Top="60px" Left="10px" ID="_CheckBoxArr_2" BackColor="LightSalmon" Height="30px" TabIndex="2" Width="73px">
		</vt:CheckBox>
		<vt:CheckBox runat="server" Text="Check4" Top="60px" Left="103px" ID="_CheckBoxArr_3" BackColor="LightSalmon" Height="30px" TabIndex="3" Width="73px">
		</vt:CheckBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="415px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Check All >>" Top="480px" ID="btnCheckAll" Width="180px" ClickAction="CheckBoxArrayBasic\btnCheckAll_Click"></vt:Button>

        <vt:Button runat="server" Text="UnCheck All >>" Top="480px" Left="290px" ID="btnUnCheckAll" Width="180px" ClickAction="CheckBoxArrayBasic\btnUnCheckAll_Click"></vt:Button>
    
    	
	</vt:WindowView>
</asp:Content>
