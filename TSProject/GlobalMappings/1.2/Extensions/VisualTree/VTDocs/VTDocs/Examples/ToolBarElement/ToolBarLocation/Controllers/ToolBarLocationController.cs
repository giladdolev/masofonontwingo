using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarLocationController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Location value: " + TestedToolBar.Location;
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar = this.GetVisualElementById<ToolBarElement>("TestedToolBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedToolBar.Location == new Point(80, 300))
            {
                TestedToolBar.Location = new Point(200, 285);
                lblLog.Text = "Location value: " + TestedToolBar.Location;
            }
            else
            {
                TestedToolBar.Location = new Point(80, 300);
                lblLog.Text = "Location value: " + TestedToolBar.Location;
            }
        }

    }
}