using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Left value: " + TestedTextBox.Left + '.';
        }

        public void btnChangeLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedTextBox.Left == 80)
            {
                TestedTextBox.Left = 180;
                lblLog.Text = "Left value: " + TestedTextBox.Left + '.';
            }
            else
            {
                TestedTextBox.Left = 80;
                lblLog.Text = "Left value: " + TestedTextBox.Left + '.';
            }
        }

    }
}