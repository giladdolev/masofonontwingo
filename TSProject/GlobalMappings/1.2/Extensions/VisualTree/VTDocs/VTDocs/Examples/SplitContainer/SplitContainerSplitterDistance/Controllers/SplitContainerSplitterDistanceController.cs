using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerSplitterDistanceController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnGetSplitterDistance_Click(object sender, EventArgs e)
        {
            SplitContainer splitContainer1 = this.GetVisualElementById<SplitContainer>("splitContainer1");
            MessageBox.Show("SplitterDistance value is: " + splitContainer1.SplitterDistance.ToString());

        }

        private void btnChangeSplitterDistance_Click(object sender, EventArgs e)
        {
            SplitContainer splitContainer1 = this.GetVisualElementById<SplitContainer>("splitContainer1");
            if (splitContainer1.SplitterDistance == 200)
                splitContainer1.SplitterDistance = 100;
            else
                splitContainer1.SplitterDistance = 200;


        }

    }
}