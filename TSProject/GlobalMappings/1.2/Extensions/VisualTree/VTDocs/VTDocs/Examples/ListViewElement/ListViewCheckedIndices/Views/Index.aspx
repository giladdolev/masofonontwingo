<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView CheckedIndices Property" ID="windowView"  LoadAction="ListViewCheckedIndices\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CheckedIndices" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets the indexes of the currently checked items in the control.

            Syntax: public List<int> CheckedIndices { get; }
            Property Value: A List of int that contains the items that are selected in the
             control. If no items are currently selected, an empty List is returned.

            Remarks: When the MultiSelect property is set to true, this property returns a list containing the 
            items that are selected in the ListView. For a single-selection ListView, this property returns a list
             containing the only selected item in the ListView." Top="75px" ID="lblDefinition" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="290px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="CheckedIndices values of this 'TestedListView' is initially set with" Top="330px"  ID="lblExp1"></vt:Label>     

        <!-- TestedListView -->
        <vt:ListView runat="server" Text="TestedListView" ItemCheckedAction="ListViewCheckedIndices\TestedListView_ItemChecked" ItemUncheckedAction="ListViewCheckedIndices\TestedListView_ItemUnChecked" CheckBoxes="true" Top="380px" Height="99px" Width="273px" ID="TestedListView">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="490px" Height="60px" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Check\UnCheck First >>" Top="590px" Left="50px" Width="185px" ID="btnChangeCheckStateofFirst" ClickAction="ListViewCheckedIndices\btnChangeCheckStateOfFirstCheckBox_Click"></vt:Button>

         <vt:Button runat="server" Text="Check\UnCheck Second >>" Top="590px" Left="270px" Width="185px" ID="btnChangeCheckStateofSecond" ClickAction="ListViewCheckedIndices\btnChangeCheckStateOfSecondCheckBox_Click"></vt:Button>

         <vt:Button runat="server" Text="Check\UnCheck Third >>" Top="590px" Width="185px" Left="490px" ID="btnChangeCheckStateofThird" ClickAction="ListViewCheckedIndices\btnChangeCheckStateOfThirdCheckBox_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
 