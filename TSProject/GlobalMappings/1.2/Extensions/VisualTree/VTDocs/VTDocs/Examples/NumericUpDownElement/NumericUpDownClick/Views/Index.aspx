<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Click event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="50px" Left="140px" ID="TestedNumericUpDown" Height="20px"  Width="100px"  ClickAction="NumericUpDownClick\TestedNumericUpDown_Click" ></vt:NumericUpDown>          

        <vt:TextBox runat="server" Text="" Top="50px" Left="400px" Multiline="true" ID="textBox1" Height="100px" Width="250px"></vt:TextBox>
       
    </vt:WindowView>
</asp:Content>
