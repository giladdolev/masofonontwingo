using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonContextMenuStripController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new ButtonContextMenuStrip());
        }
        private ButtonContextMenuStrip ViewModel
        {
            get { return this.GetRootVisualElement() as ButtonContextMenuStrip; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton1 = this.GetVisualElementById<ButtonElement>("TestedButton1");
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");

            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            //Write to log lblLog1
            if(TestedButton1.ContextMenuStrip == null)
                lblLog1.Text = "ContextMenuStrip value is null";
            else
                lblLog1.Text = "ContextMenuStrip value is: " + TestedButton1.ContextMenuStrip.ID;

            //Write to log lblLog2
            if (TestedButton2.ContextMenuStrip == null)
                lblLog2.Text = "ContextMenuStrip value is null.";
            else
                lblLog2.Text = "ContextMenuStrip value is: " + TestedButton2.ContextMenuStrip.ID + ".";
        }


        private void btnChangToeContextMenuStrip1_Click()
        {
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            ContextMenuStripElement contextMenuStrip1 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedButton2.ContextMenuStrip = contextMenuStrip1;
            lblLog2.Text = "ContextMenuStrip value is: " + TestedButton2.ContextMenuStrip.ID + ".";

        }

        private void btnChangToeContextMenuStrip2_Click()
        {
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            ContextMenuStripElement contextMenuStrip2 = this.GetVisualElementById<ContextMenuStripElement>("contextMenuStrip2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedButton2.ContextMenuStrip = contextMenuStrip2;
            lblLog2.Text = "ContextMenuStrip value is: " + TestedButton2.ContextMenuStrip.ID + ".";

        }

        private void btnReset_Click()
        {
            ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedButton2.ContextMenuStrip = null;
            lblLog2.Text = "ContextMenuStrip value is null.";

        }

        //public void exitToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    ToolBarMenuItem menuItem = (ToolBarMenuItem)sender;
        //    if (object.Equals(menuItem.Text, "Exit"))
        //    {
        //        this.ViewModel.Close();
        //    }
        //}

        //private void menuItem_Click(object sender, EventArgs e)
        //{
        //    ToolBarMenuItem menuItem = (ToolBarMenuItem)sender;
        //    if (object.Equals(menuItem.Text, "Exit"))
        //    {
        //        this.ViewModel.Close();
        //    }
        //}
    }
}



//if (btnSetContextMenuStrip.ContextMenuStrip == null)
//{
//    ContextMenuStripElement menuStrip = new ContextMenuStripElement();
//    // menuStrip.ID = "cnt";
//    menuStrip.ID = "RunTimeCnt";

//    // TODO: Method '.ctor' of type 'System.Windows.Forms.ToolStripMenuItem' is unmapped. (CODE=1005)
//    ToolBarMenuItem menuItem = new ToolBarMenuItem("Exit");
//    menuItem.Click += new EventHandler(menuItem_Click);
//    menuItem.ID = "Exit";
//    menuStrip.Items.Add(menuItem);

//    //  this.Controls.Add(menuStrip);


//    this.ViewModel.Controls.Add(menuStrip);
//    btnSetContextMenuStrip.ContextMenuStrip = menuStrip;
//}
//else 
//{
//    btnSetContextMenuStrip.ContextMenuStrip = null;
//}