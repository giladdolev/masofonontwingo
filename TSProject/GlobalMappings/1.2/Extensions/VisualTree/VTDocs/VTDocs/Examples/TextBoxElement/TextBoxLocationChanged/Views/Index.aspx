<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox LocationChanged Event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="TextBox Location is initially set to (140, 90)" Top="70px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" ></vt:Label>

        <vt:TextBox runat="server" Text="Tested TextBox" Top="90px" Left="140px" ID="testedTextBox" Height="36px" TabIndex="1" Width="200px"  LocationChangedAction ="TextBoxLocationChanged\testedTextBox_LocationChanged"></vt:TextBox>

        <vt:Button runat="server" Text="Change TextBox Location" Top="90px" Left="400px" ID="btnChangeLocation" Height="36px" TabIndex="1" Width="300px" ClickAction="TextBoxLocationChanged\btnChangeLocation_Click"></vt:Button>

         <vt:Label runat="server" Text="Event Log" Top="180px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:TextBox runat="server" Text="" Top="195px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
