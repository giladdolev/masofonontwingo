using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabPreviousIndexController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTabControl1 = this.GetVisualElementById<TabElement>("TestedTabControl1");
            TabElement TestedTabControl2 = this.GetVisualElementById<TabElement>("TestedTabControl2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Current Selected Index = " + TestedTabControl1.SelectedIndex + "\\r\\nPrevious Selected Index = " + TestedTabControl1.PreviousIndex;
            lblLog2.Text = "Current Selected Index = " + TestedTabControl2.SelectedIndex + "\\r\\nPrevious Selected Index = " + TestedTabControl2.PreviousIndex;
                        
        }

        public void tabSelectedIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabElement TestedTabControl1 = this.GetVisualElementById<TabElement>("TestedTabControl1");
            TabElement TestedTabControl2 = this.GetVisualElementById<TabElement>("TestedTabControl2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Current Selected Index = " + TestedTabControl1.SelectedIndex + "\\r\\nPrevious Selected Index = " + TestedTabControl1.PreviousIndex;
            lblLog2.Text = "Current Selected Index = " + TestedTabControl2.SelectedIndex + "\\r\\nPrevious Selected Index = " + TestedTabControl2.PreviousIndex;
        }

        public void btnSelectTabIndex0_Click(object sender, EventArgs e)
        {
            TabElement TestedTabControl2 = this.GetVisualElementById<TabElement>("TestedTabControl2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedTabControl2.SelectedIndex = 0;
        }
    }
}

