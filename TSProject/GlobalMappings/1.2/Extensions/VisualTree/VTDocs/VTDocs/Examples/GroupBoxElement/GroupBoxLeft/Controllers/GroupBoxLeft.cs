using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Left value: " + TestedGroupBox.Left + '.';
        }

        public void btnChangeLeft_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedGroupBox.Left == 80)
            {
                TestedGroupBox.Left = 180;
                lblLog.Text = "Left value: " + TestedGroupBox.Left + '.';
            }
            else
            {
                TestedGroupBox.Left = 80;
                lblLog.Text = "Left value: " + TestedGroupBox.Left + '.';
            }
        }

    }
}