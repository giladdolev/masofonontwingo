using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPerformKeyPressController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformKeyPress_Click(object sender, EventArgs e)
        {
            Keys keys = new Keys();
            KeyPressEventArgs args = new KeyPressEventArgs(keys);
            RadioButtonElement testedRadioButton = this.GetVisualElementById<RadioButtonElement>("testedRadioButton");

            testedRadioButton.PerformKeyPress(args);
        }

        public void testedRadioButton_KeyPress(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRadioButton KeyPress event method is invoked");
        }

    }
}