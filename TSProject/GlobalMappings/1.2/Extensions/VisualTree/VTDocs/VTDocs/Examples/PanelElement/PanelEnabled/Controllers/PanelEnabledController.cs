using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedPanel1.Enabled.ToString();

            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedPanel2.Enabled.ToString() + ".";
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedPanel2.Enabled = !TestedPanel2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedPanel2.Enabled.ToString() + ".";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ButtonElement button1 = this.GetVisualElementById<ButtonElement>("button1");

            if (button1.BackColor == Color.Yellow)
            {
                button1.BackColor = Color.Green;
            }
            else
            {
                button1.BackColor = Color.Yellow;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonElement button2 = this.GetVisualElementById<ButtonElement>("button2");

            if (button2.BackColor == Color.Yellow)
            {
                button2.BackColor = Color.Green;
            }
            else
            {
                button2.BackColor = Color.Yellow;
            }
        }
    }
}