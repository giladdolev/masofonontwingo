using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller   
    /// </summary>
    public class NumericUpDownGetTypeController : Controller
    {
        /// <summary>
        /// Returns The View For Index   
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press the GetType Button...";
        }

        public void GetType_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedNumericUpDown.GetType();
            lblLog.Text = "The element Type is: \\r\\n" + TestedNumericUpDown.GetType();
        }
    }
}

        

