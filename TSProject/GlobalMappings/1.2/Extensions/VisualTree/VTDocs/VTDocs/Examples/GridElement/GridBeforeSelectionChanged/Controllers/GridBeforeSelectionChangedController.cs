﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridBeforeSelectionChangedController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void Load_View(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Grid SelectedCells values: Empty";
        }

        public void TestedGrid_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            CheckBoxElement chkCancelSelection = this.GetVisualElementById<CheckBoxElement>("chkCancelSelection");
            e.Cancel = chkCancelSelection.IsChecked;
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nGrid BeforeSelectionChanged event is invoked";
        }

        public void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nGrid SelectionChanged event is invoked";

            lblLog.Text = "Grid SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }

        public void btnSelecti_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[2].Cells[2].Selected = true;

            lblLog.Text = "Grid SelectedCells values: (" + testedGrid.Rows[2].Cells[2].Value + ") ";
        }

    }
}
