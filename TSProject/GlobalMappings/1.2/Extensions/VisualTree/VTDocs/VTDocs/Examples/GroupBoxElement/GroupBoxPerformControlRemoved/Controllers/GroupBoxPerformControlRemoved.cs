using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPerformControlRemovedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlRemoved_Click(object sender, EventArgs e)
        {
            
            GroupBoxElement testedGroupBox = this.GetVisualElementById<GroupBoxElement>("testedGroupBox");

            ControlEventArgs args = new ControlEventArgs(testedGroupBox);

            testedGroupBox.PerformControlRemoved(args);
        }

        public void testedGroupBox_ControlRemoved(object sender, EventArgs e)
        {
            MessageBox.Show("TestedGroupBox ControlRemoved event method is invoked");
        }

    }
}