using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxGetTypeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //GroupBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press the GetType Button...";
        }

        public void GetType_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGroupBox.GetType();
            lblLog.Text = "The element Type is: \\r\\n" + TestedGroupBox.GetType();
        }
    }
}