using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "CheckBox bounds are: \\r\\n" + TestedCheckBox.Bounds;

        }
        public void btnChangeCheckBoxBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            if (TestedCheckBox.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedCheckBox.Bounds = new Rectangle(80, 355, 150, 26);
                lblLog.Text = "CheckBox bounds are: \\r\\n" + TestedCheckBox.Bounds;

            }
            else
            {
                TestedCheckBox.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "CheckBox bounds are: \\r\\n" + TestedCheckBox.Bounds;
            }

        }
    }
}