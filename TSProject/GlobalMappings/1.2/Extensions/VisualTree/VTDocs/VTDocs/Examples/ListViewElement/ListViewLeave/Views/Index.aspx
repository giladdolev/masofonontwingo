<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Leave event" ID="windowView2" LoadAction="ListViewLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the listviews,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:ListView runat="server" Top="335px" Height="70px" ID="TestedListView1" CssClass="vt-TestedListView-1" LeaveAction="ListViewLeave\TestedListView1_Leave" SelectionChangedAction="ListViewLeave\TestedListView1_SelectionChanged">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="ListView1-L1">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="a"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="ListView1-L2">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="d"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:ListView runat="server" Top="420px" Height="70px" ID="TestedListView2" CssClass="vt-TestedListView-2" LeaveAction="ListViewLeave\TestedListView2_Leave" SelectionChangedAction="ListViewLeave\TestedListView2_SelectionChanged">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="ListView2-L1">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="ListView2-L2">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="g"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="505px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="545px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

