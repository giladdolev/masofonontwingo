<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic Image" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
     
     <vt:Label runat="server" Text="InitializeComponent" Top="60px" Left="240px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px"  Width="150px"></vt:Label>   

     <vt:Image runat="server" ImageReference="/Content/Images/Cute.png" Top="80px" Left="240px" ID="Picture1" BorderStyle="Inset" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="201px" Width="273px">
		</vt:Image>

    </vt:WindowView>
</asp:Content>
