using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ProgressBarElement prgRunTimeAnchor = this.GetVisualElementById<ProgressBarElement>("prgRunTimeAnchor");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");


            if (prgRunTimeAnchor.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                prgRunTimeAnchor.Anchor = AnchorStyles.Top; 
                panel2.Size = new Size(400, 171);
                textBox1.Text = "AnchorStyles.Top, ";
                textBox1.Text += "Panel Size: (400, 171)";

            }

            else if (prgRunTimeAnchor.Anchor == AnchorStyles.Top)
            {
                prgRunTimeAnchor.Anchor = AnchorStyles.Left;
                panel2.Size = new Size(315, 190);
                textBox1.Text = "AnchorStyles.Left, ";
                textBox1.Text += "Panel Size: (315, 190)";
            }
            else if (prgRunTimeAnchor.Anchor == AnchorStyles.Left)
            {
                prgRunTimeAnchor.Anchor = AnchorStyles.Right; 
                panel2.Size = new Size(200, 190);
                textBox1.Text = "AnchorStyles.Right, ";
                textBox1.Text += "Panel Size: (200, 190)";
            }
            else if (prgRunTimeAnchor.Anchor == AnchorStyles.Right)
            {
                prgRunTimeAnchor.Anchor = AnchorStyles.Bottom; 
                panel2.Size = new Size(200, 160);
                textBox1.Text = "AnchorStyles.Bottom, ";
                textBox1.Text += "Panel Size: (200, 160)";
            }
            else if (prgRunTimeAnchor.Anchor == AnchorStyles.Bottom)
            {
                prgRunTimeAnchor.Anchor = AnchorStyles.None; 
                panel2.Size = new Size(315, 171);
                textBox1.Text = "AnchorStyles.None, ";
                textBox1.Text += "Panel Size: (315, 171)";
            }
            else if (prgRunTimeAnchor.Anchor == AnchorStyles.None)
            {
                
                prgRunTimeAnchor.Anchor = (AnchorStyles.Left | AnchorStyles.Top); 
                panel2.Size = new Size(300, 72);
                prgRunTimeAnchor.Location = new Point(75, 18);
                textBox1.Text = "AnchorStyles.Left | AnchorStyles.Top, ";
                textBox1.Text += "Panel Size is back to : (300, 72)";
            }       
        }

    }
}