using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Tab bounds are: \\r\\n" + TestedTab.Bounds;

        }
        public void btnChangeTabBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            if (TestedTab.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedTab.Bounds = new Rectangle(80, 355, 300, 100);
                lblLog.Text = "Tab bounds are: \\r\\n" + TestedTab.Bounds;

            }
            else
            {
                TestedTab.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "Tab bounds are: \\r\\n" + TestedTab.Bounds;
            }

        }

    }
}