using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonBackgroundImageLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement btn1RadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackgroundImageLayout value: " + btn1RadioButton.BackgroundImageLayout.ToString();

            RadioButtonElement btn2RadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2RadioButton.BackgroundImageLayout.ToString() + ".";

        }
        private void btnNone_Click(object sender, EventArgs e)
        {
            RadioButtonElement btn2RadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            btn2RadioButton.BackgroundImageLayout = ImageLayout.None;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2RadioButton.BackgroundImageLayout.ToString() + ".";
        }
        private void btnTile_Click(object sender, EventArgs e)
        {
            RadioButtonElement btn2RadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            btn2RadioButton.BackgroundImageLayout = ImageLayout.Tile;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2RadioButton.BackgroundImageLayout.ToString() + ".";
        }
        private void btnCenter_Click(object sender, EventArgs e)
        {
            RadioButtonElement btn2RadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            btn2RadioButton.BackgroundImageLayout = ImageLayout.Center;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2RadioButton.BackgroundImageLayout.ToString() + ".";
        }
        private void btnStretch_Click(object sender, EventArgs e)
        {
            RadioButtonElement btn2RadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            btn2RadioButton.BackgroundImageLayout = ImageLayout.Stretch;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2RadioButton.BackgroundImageLayout.ToString() + ".";
        }
        private void btnZoom_Click(object sender, EventArgs e)
        {
            RadioButtonElement btn2RadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            btn2RadioButton.BackgroundImageLayout = ImageLayout.Zoom;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackgroundImageLayout value: " + btn2RadioButton.BackgroundImageLayout.ToString() + ".";
        }

    }
}