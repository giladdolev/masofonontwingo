using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Height value: " + TestedSplitContainer.Height + '.';
        }

        private void btnChangeHeight_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedSplitContainer.Height == 120)
            {
                TestedSplitContainer.Height = 80;
                lblLog.Text = "Height value: " + TestedSplitContainer.Height + '.';
            }
            else
            {
                TestedSplitContainer.Height = 120;
                lblLog.Text = "Height value: " + TestedSplitContainer.Height + '.';
            }
        }
      
    }
}