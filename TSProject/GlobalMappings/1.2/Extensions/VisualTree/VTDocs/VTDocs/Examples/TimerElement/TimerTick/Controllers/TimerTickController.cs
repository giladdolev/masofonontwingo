using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TimerTickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            
            return View(new TimerTick());
        }

        TimerTick TimerTickModel;
        private TimerTick ViewModel
        {
            get { return this.GetRootVisualElement() as TimerTick; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");

            lblLog.Text = "Timer has started, Enabled value: " + TestedTimer.Enabled;
        }

        public void TestedTimer_Tick(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            TextBoxElement timerTextBox = this.GetVisualElementById<TextBoxElement>("timerTextBox");
            timerTextBox.Text = ViewModel.durationProperty++.ToString();

            txtEventLog.Text += "\\r\\nTick Event is Invoked";
        }

        public void btnStart_Click(object sender, EventArgs e)
        {
            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            //timer1.Enabled = true;
            TestedTimer.Start();
            lblLog.Text = "Timer has started, Enabled value: " + TestedTimer.Enabled;
        }

        public void btnStop_Click(object sender, EventArgs e)
        {
            TimerElement TestedTimer = this.GetVisualElementById<TimerElement>("TestedTimer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedTimer.Stop();
            lblLog.Text = "Timer has stoped, Enabled value: " + TestedTimer.Enabled;
        }


    }
}