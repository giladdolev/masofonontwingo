using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class DateTimePickerValueController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker1 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Value property: " + TestedDateTimePicker1.Value + "\\r\\nFormat property: " + TestedDateTimePicker1.Format;


            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Value property: " + TestedDateTimePicker2.Value + "\\r\\nFormat property: " + TestedDateTimePicker2.Format;


        }


        public void btnChangeValue_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedDateTimePicker2.Value == new DateTime(2017, 4, 12, 12, 30, 12, 2))
                TestedDateTimePicker2.Value = new DateTime(2027, 7, 27, 6, 50, 24, 1);
            else
                TestedDateTimePicker2.Value = new DateTime(2017, 4, 12, 12, 30, 12, 2);

            lblLog2.Text = "Value property: " + TestedDateTimePicker2.Value + "\\r\\nFormat property: " + TestedDateTimePicker2.Format;
      
        }

     

        public void TestedDateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");

            lblLog2.Text = "Value property: " + TestedDateTimePicker2.Value;
        }

        public void btnChangeFormat_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedDateTimePicker2.Format == DateTimePickerFormat.Custom)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Default;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Default)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Line;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Line)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Long;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Long)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Short;
            else if (TestedDateTimePicker2.Format == DateTimePickerFormat.Short)
                TestedDateTimePicker2.Format = DateTimePickerFormat.Time;
            else
                TestedDateTimePicker2.Format = DateTimePickerFormat.Custom;
            
            lblLog2.Text = "Value property: " + TestedDateTimePicker2.Value + "\\r\\nFormat property: " + TestedDateTimePicker2.Format;
        }
    }
}