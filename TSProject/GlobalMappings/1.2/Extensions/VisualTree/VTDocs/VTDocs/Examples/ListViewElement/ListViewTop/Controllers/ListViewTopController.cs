using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

             lblLog.Text = "Top Value: " + TestedListView.Top + '.';

         }
         public void btnChangeTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
             if (TestedListView.Top == 285)
             {
                 TestedListView.Top = 300;
                 lblLog.Text = "Top Value: " + TestedListView.Top + '.';

             }
             else
             {
                 TestedListView.Top = 285;
                 lblLog.Text = "Top Value: " + TestedListView.Top + '.';
             }

         }

    }
}