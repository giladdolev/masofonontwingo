using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxAutoSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeAutoSize_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeAutoSize = this.GetVisualElementById<ButtonElement>("btnChangeAutoSize");
            TextBoxElement txtAutoSizeChange = this.GetVisualElementById<TextBoxElement>("txtAutoSizeChange");

            if (txtAutoSizeChange.AutoSize == false)
            {
                txtAutoSizeChange.AutoSize = true;
                btnChangeAutoSize.Text = "AutoSize = True";
            }
            else
            {
                txtAutoSizeChange.AutoSize = false;
                btnChangeAutoSize.Text = "AutoSize = False";
            }
        }
        //btnChangeText_Click
        private void btnChangeText_Click(object sender, EventArgs e)
        {
            TextBoxElement txtAutoSizeChange = this.GetVisualElementById<TextBoxElement>("txtAutoSizeChange");
            if (txtAutoSizeChange.Text == "Tested Button")
            {
                txtAutoSizeChange.Text = "Some great new Text";
           
            }
            else
                txtAutoSizeChange.Text = "Tested Button";
        }
      
    }
}