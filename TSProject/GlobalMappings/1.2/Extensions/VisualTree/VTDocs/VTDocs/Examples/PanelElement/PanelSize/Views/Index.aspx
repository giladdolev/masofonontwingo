<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="Panel Size Property" ID="windowView1" LoadAction="PanelSize/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control in pixels.
            
            Syntax: public Size Size { get; set; }" Top="75px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Size property of this Panel is initially set to:  Width='200px' Height='80px'" Top="235px" ID="lblExp"></vt:Label>

        <!-- TestedPanel -->
        <vt:Panel runat="server" Text="TestedPanel" Top="285px" ID="TestedPanel"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="380px" Width="360px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Width="180px" Top="445px" ID="btnChangePanelSize" ClickAction="PanelSize/btnChangePanelSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
