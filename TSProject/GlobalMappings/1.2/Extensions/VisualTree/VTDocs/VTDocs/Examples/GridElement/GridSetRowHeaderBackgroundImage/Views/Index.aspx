﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SetRowHeaderBackgroundImage method" ID="Form1" LoadAction="GridSetRowHeaderBackgroundImage\OnLoad">
         <vt:Label runat="server" SkinID="Title" Text="SetRowHeaderBackgroundImage" Left="200px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Sets the image icon at the RowHeader cell of the row with the specified index.
                        
            Syntax: public void SetRowHeaderBackgroundImage(int rowIndex, ResourceReference image)" Top="75px"  ID="Label2"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="Label3"></vt:Label>
        <vt:Label runat="server" Text="In the following example, You can add, change, or clear the background image of any RowHeader
            by choosing a row index from the combobox and pressing the buttons." 
            Top="240px"  ID="Label4"></vt:Label> 
              
        <vt:Grid runat="server" Top="330px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="460px" Width="400px" Height="40px" ID="lblLog"></vt:Label>

        <vt:Label runat="server" Text="Choose a row index:" Top="545px" Left="80px" ID="lblInd"></vt:Label>
        <vt:ComboBox runat="server" Text="0" Top="545px" Left="220px" Width="50px" CssClass="vt-cmb-index" ID="cmbInd"></vt:ComboBox>
        <vt:Button runat="server" Text="Clear Background Image >>" Top="590px" Left="80px" Width="185px" ID="btnClearRowHeaderBackgroundImage" ClickAction="GridSetRowHeaderBackgroundImage\btnClearRowHeaderBackgroundImage_Click"></vt:Button>
        <vt:Button runat="server" Text="Label Background Image >>" Top="590px" Left="280px" Width="185px" ID="btnLabelRowHeaderBackgroundImage" ClickAction="GridSetRowHeaderBackgroundImage\btnLabelRowHeaderBackgroundImage_Click"></vt:Button>
        <vt:Button runat="server" Text="Button Background Image >>" Top="590px" Left="480px" Width="185px" ID="btnButtonRowHeaderBackgroundImage" ClickAction="GridSetRowHeaderBackgroundImage\btnButtonRowHeaderBackgroundImage_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
