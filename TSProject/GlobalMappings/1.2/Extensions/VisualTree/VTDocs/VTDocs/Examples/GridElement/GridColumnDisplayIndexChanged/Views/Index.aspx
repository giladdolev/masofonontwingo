﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="Grid ColumnDisplayIndexChanged Event" Height="830px" LoadAction="GridColumnDisplayIndexChanged\load_view">


        <vt:Label runat="server" SkinID="Title" Left="230px" Text="ColumnDisplayIndexChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the value the DisplayIndex property for a column changes.

           Syntax: public event EventHandler<GridColumnMoveEventArgs> ColumnDisplayIndexChanged" Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the DisplayIndex property by dragging a column to a different
             location in the grid or by clicking the button below. 
             Each invocation of the 'ColumnDisplayIndexChanged' event should add a line in the 'Event Log'."
            Top="200px" ID="lblExp1"></vt:Label>

         <vt:Grid runat="server" Top="280px" Height="170px" AllowUserToOrderColumns="true" Width="370px" ColumnDisplayIndexChangedAction="GridColumnDisplayIndexChanged\TestedGrid_ColumnDisplayIndexChanged" ID="TestedGrid">
            <Columns>
                <vt:GridTextBoxColumn  runat="server" ID="Column1" HeaderText="ID" Height="20px" Width="60px"> </vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn  runat="server" ID="Column2" HeaderText="Name" Height="20px" Width="80px"> </vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="Column3" HeaderText="Attendance" Height="20px" Width="110px" ></vt:GridCheckBoxColumn>
                 <vt:GridComboBoxColumn  runat="server" ID="Column4" HeaderText="Gender" Height="20px" Width="90px" ></vt:GridComboBoxColumn>
            </Columns>
        </vt:Grid>
           <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="465px" Height="90px" Width="350"></vt:Label>

        

         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="590px" Width="430px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change Colum1 DisplayIndex value >>" Width="220px" Top="740px" ID="btnChangeDisplayIndex" ClickAction="GridColumnDisplayIndexChanged\btnChangeDisplayIndex_Click"></vt:Button>
        
    </vt:WindowView>

</asp:Content>
