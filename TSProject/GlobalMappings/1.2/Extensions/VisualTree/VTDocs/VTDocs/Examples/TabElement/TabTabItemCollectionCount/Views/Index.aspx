
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItemCollection Count Property" ID="windowView2" LoadAction="TabTabItemCollectionCount\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Count" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the number of tab pages in the collection.

            Syntax: public int Count { get; }" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="160px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the Count of TabItems is updating according to 'Remove TabItem' and 
            'AddRange TabItems' buttons. Pressing the 'Remove TabItem' button will remove TabItem, which is 
            specified by its index. Pressing the 'AddRange TabItems' button will add a set of 3
            TabItems to the collection. " Top="200px" ID="lblExp2"></vt:Label>

        <vt:Tab runat="server" Text="Tab1" Top="290px" Width="530px" ID="TestedTab1">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="Tab1Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="400" Width="380px" Height="40px"></vt:Label>

        <vt:Label runat="server" Text="Enter TabItem index to remove:" Left="80px" ID="lblInput" Top="520px"></vt:Label>
        <vt:TextBox runat="server" Text="..." Top="520px" Left="290px" Width="50px" ID="txtInput" TextChangedAction="TabTabItemCollectionCount\txtInput_TextChanged"></vt:TextBox>

        <vt:Button runat="server" Text="Remove TabItem >>" Top="520px" Left="360px" ID="btnRemoveTabItem" ClickAction="TabTabItemCollectionCount\RemoveTabItem_Click"></vt:Button>

        <vt:Button runat="server" Text="AddRange TabItems >>" Top="470px" Left="80px" ID="btnAddRangeTabItems" ClickAction="TabTabItemCollectionCount\AddRangeTabItems_Click"></vt:Button>

        

    </vt:WindowView>
</asp:Content>
