﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="Grid AllowColumnSelection Property" LoadAction="GridAllowUserToOrderColumns\Page_load">


        <vt:Label runat="server" SkinID="Title" Text="AllowUserToOrderColumns" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the AllowUserToOrderColumns for the grid.

           Syntax: public bool AllowUserToOrderColumns { get; set; }
            'true' - if the user can change the column order; 
            otherwise - 'false'  
            Note: The default is 'false' "
            
            Top="75px" ID="lblDefinition">
            <br />  
            <br /> 
        </vt:Label>
        
        <vt:Grid runat="server" Top="204px" Left="80px" ID="TestedGrid1" SortableColumns=false AllowColumnSelection="False">
             <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>      
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="316px" ID="lblLog1"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="AllowUserToOrderColumns property of this Grid is initialy set to 'true'
            You should be able to drag a column header to a new position" Top="390px" ID="lblExp1"></vt:Label>

        <vt:Grid runat="server" Top="450px" Left="80px" ID="TestedGrid2" SortableColumns=true AllowUserToOrderColumns="true">
            <Columns>
                
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns> 
        </vt:Grid>
         <vt:Label runat="server" SkinID="Log" Top="570px" ID="lblLog2"></vt:Label>

       <vt:Label runat="server" Text="Note (Sencha) : Cann`t change property value in run time, must be initialize." Top="600" ID="Label1"></vt:Label>

      
        
    </vt:WindowView>

</asp:Content>

