<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Bounds Property" ID="windowView" LoadAction="ToolBarBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Bounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size and location of the control including its nonclient elements, 
            in pixels, relative to the parent control.

            Syntax: public Rectangle Bounds { get; set; }
            
            The bounds of the control include the nonclient elements such as scroll bars, 
            borders, title bars, and menus." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The bounds of this 'TestedToolBar' is initially set with 
            Left: 80px, Top: 355px, Width: 400px, Height: 40px" Top="290px"  ID="lblExp1"></vt:Label>     

        <vt:ToolBar runat="server" Text="TestedToolBar" Dock="None" Top="355px" ID="TestedToolBar">
            <vt:ToolBarButton runat="server" id="ToolBarItem1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="410px" Height="40" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Bounds value >>" Top="505px" Width="180px" ID="btnChangeToolBarBounds" ClickAction="ToolBarBounds\btnChangeToolBarBounds_Click"></vt:Button>
              
    </vt:WindowView>
</asp:Content>