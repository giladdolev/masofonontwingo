<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox Enabled Property" ID="windowView1" LoadAction="CheckBoxEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:CheckBox runat="server" Text="CheckBox" Top="195px" ID="TestedCheckBox1" ></vt:CheckBox>           

        <vt:Label runat="server" SkinID="Log" Top="235px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedCheckBox' is initially set to 'false'." Top="350px" ID="lblExp1"></vt:Label> 

        <vt:CheckBox runat="server" Text="TestedCheckBox" Top="410px" Enabled ="false" ID="TestedCheckBox2" ></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="515px" ID="btnChangeEnabled" Width="180px" ClickAction="CheckBoxEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
