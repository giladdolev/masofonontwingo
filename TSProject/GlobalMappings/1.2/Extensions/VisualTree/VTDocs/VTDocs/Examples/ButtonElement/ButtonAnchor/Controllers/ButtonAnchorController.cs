using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeAnchor = this.GetVisualElementById<ButtonElement>("btnChangeAnchor");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");
           

            if (btnChangeAnchor.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                btnChangeAnchor.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                panel2.Size = new Size(400, 171);
            }

            else if (btnChangeAnchor.Anchor == AnchorStyles.Top)
            {
                btnChangeAnchor.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                panel2.Size = new Size(315, 190);
            }
            else if (btnChangeAnchor.Anchor == AnchorStyles.Left)
            {
                btnChangeAnchor.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                panel2.Size = new Size(200, 190);
            }
            else if (btnChangeAnchor.Anchor == AnchorStyles.Right)
            {
                btnChangeAnchor.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                panel2.Size = new Size(200, 160);
            }
            else if (btnChangeAnchor.Anchor == AnchorStyles.Bottom)
            {
                btnChangeAnchor.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the panel size
            }
            else if (btnChangeAnchor.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and btnChangeAnchor settings
                btnChangeAnchor.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                panel2.Size = new Size(300, 72);//Set  back the panel size
                btnChangeAnchor.Location = new Point(75, 18);
            }       
        }

    }
}