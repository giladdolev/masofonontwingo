<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox BackgroundImageLayout Property" ID="windowView" LoadAction="CheckBoxBackgroundImageLayout\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImageLayout" Left="250px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image position on the control (Center , None, Stretch, Tile or Zoom).
            Tile is the default value.            
            public virtual ImageLayout BackgroundImageLayout { get; set; }" Top="75px"  ID="lblDefinition1"></vt:Label>
      
        
        <vt:CheckBox runat="server" BackgroundImage ="~/Content/Images/icon.jpg" Text="CheckBox" Top="160px" ID="TestedCheckBox1"></vt:CheckBox>
        <vt:Label runat="server" SkinID="Log" Top="200px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" Text="BackgroundImageLayout takes effect only if the BackgroundImage property is set." Top="235px"  ID="lblDefinition2"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="285px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, the initial BackgroundImageLayout of TestedCheckBox is set to 'Center'.
            Use the buttons below (None/ Stretch/ Center/ Zoom/ Tile) to change the TestedCheckBox 
            Background Image Layout." Top="335px"  ID="lblExp1"></vt:Label>     
        
        <vt:CheckBox runat="server" Text="TestedCheckBox" BackgroundImage ="~/Content/Images/icon.jpg" BackgroundImageLayout ="Center" Top="435px" ID="TestedCheckBox2"></vt:CheckBox>
        <vt:Label runat="server" SkinID="Log" Top="475px" ID="lblLog2"></vt:Label>

         <vt:Button runat="server" Text="Tile"  Top="530px" Left="560px" ID="btnTile" Height="30px" TabIndex="1" Width="60px" ClickAction="CheckBoxBackgroundImageLayout\btnTile_Click"></vt:Button>  
         <vt:Button runat="server" Text="Center"   Top="530px" Left="340px" ID="btnCenter" Height="30px" TabIndex="1" Width="60px" ClickAction="CheckBoxBackgroundImageLayout\btnCenter_Click"></vt:Button>  
         <vt:Button runat="server" Text="Stretch"   Top="530px" Left="230px" ID="btnStretch" Height="30px" TabIndex="1" Width="60px" ClickAction="CheckBoxBackgroundImageLayout\btnStretch_Click"></vt:Button>  
         <vt:Button runat="server" Text="Zoom"  Top="530px" Left="450px" ID="btnZoom" Height="30px" TabIndex="1" Width="60px" ClickAction="CheckBoxBackgroundImageLayout\btnZoom_Click"></vt:Button>  
         <vt:Button runat="server" Text="None" Top="530px" Left="120px" ID="btnNone" Height="30px" TabIndex="1" Width="60px" ClickAction="CheckBoxBackgroundImageLayout\btnNone_Click"></vt:Button> 
 
    </vt:WindowView>
</asp:Content>
        