﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Basic Example" Height="800px" Width="1200px" LoadAction="BasicGrid\page_load">


        <vt:Grid runat="server" CellMouseDownAction="BasicGrid/TestFunc" AllowUserToDeleteRows="False" Top="34px" Left="10px" ID="gridElement" Height="299px" Width="782px" LostFocusAction="True" ContextMenuStripID="contextMenuStripCells" RowCount="10">
            <Columns>
                <vt:GridTextBoxColumn ID="txtclm" runat="server" Height="70px" HeaderText="TextBox Column" DataMember="TextColumn"></vt:GridTextBoxColumn>

                <vt:GridTextButtonColumn ID="txtbtnclm" runat="server" HeaderText="TextButton Column" DataMember="TextButtonColumn"></vt:GridTextButtonColumn>

                <vt:GridCheckBoxColumn ID="cehckclm" runat="server" HeaderText="CheckBox Column" DataMember="CheckBoxColumn"></vt:GridCheckBoxColumn>

                <vt:GridDateTimePickerColumn ID="dataclm" runat="server" HeaderText="Date Column" DataMember="DateTimeColumn" Format="Short"></vt:GridDateTimePickerColumn>

                <vt:GridComboBoxColumn ID="comboclm" runat="server" HeaderText="ComboBox Column" DataMember="ComboBoxColumn" Width="90" Format="Short"></vt:GridComboBoxColumn>
                 <vt:GridTextBoxColumn ID="GridTextBoxColumn1" runat="server" HeaderText="Currency" DataMember="Currency" Format="currency"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Fill the Grid" Top="100px" Left="820px" ID="button1" ClickAction="BasicGrid\button1_Click" Height="45px" Width="236px">
        </vt:Button>

        <vt:Button runat="server" UseVisualStyleBackColor="True" Text="Set BackColor" Top="180px" Left="820px" ID="button2" ClickAction="BasicGrid\button1_Click1" Height="45px" Width="236px">
        </vt:Button>

     


        <vt:ContextMenuStrip runat="server" ID="contextMenuStripCells">
        </vt:ContextMenuStrip>

    </vt:WindowView>

</asp:Content>
