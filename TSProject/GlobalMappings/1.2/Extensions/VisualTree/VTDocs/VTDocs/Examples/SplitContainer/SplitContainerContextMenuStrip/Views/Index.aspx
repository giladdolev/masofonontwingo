<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer ContextMenuStrip Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        
        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:SplitContainer runat="server" Text="Initialized with ContextMenuStrip1" ContextMenuStripID = "ContextMenuStrip1" Top="45px" Left="140px" ID="btnImage" Height="70px" TabIndex="1" Width="220px"></vt:SplitContainer>  
          
        <vt:Label runat="server" Text="RunTime" Top="135px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:SplitContainer runat="server" Text="SplitContainer ContextMenuStrip - runtime" Top="150px" Left="140px" ID="ContextMenuStrip2" Height="70px" Width="220px"></vt:SplitContainer>

        <vt:Button runat="server" Text="Set/Unset ContextMenuStrip" Top="175px" Left="400px" ID="btnSetContextMenuStrip" Height="36px"  TabIndex="1" Width="200px" ClickAction="SplitContainerContextMenuStrip\btnSetContextMenuStrip_Click"></vt:Button>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip1" >
            <vt:ToolBarMenuItem runat ="server" id ="exitToolStripMenuItem" Text="Exit" ClickAction = "SplitContainerContextMenuStrip\exitToolStripMenuItem_Click"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

    </vt:WindowView>
</asp:Content>
