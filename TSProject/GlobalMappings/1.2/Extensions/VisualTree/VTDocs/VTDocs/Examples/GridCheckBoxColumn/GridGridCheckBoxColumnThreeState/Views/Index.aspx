﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="GridCheckBoxColumn ThreeState Property" LoadAction="GridGridCheckBoxColumnThreeState\Page_load">

        <vt:Label runat="server" SkinID="Title" Text="ThreeState" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the checkBoxes of the column have 3 check states.

           Syntax: public bool ThreeState { get; set; }
            'true' if the checkBoxes of the column have a 3 check states; otherwise, 'false'.
            The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the ThreeState property of Column1 is set to true,
            the ThreeState property of column2 and column3 are initially set to the default ('true'). 
            You can change the ThreeState property of column1 by using the 
            button 'Set Column ThreeState'" Top="255px"  ID="Label1"></vt:Label>

        <vt:Grid runat="server" Top="365px" Width="330px" Height="120px" ID="TestedGrid">
            <Columns>              
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="90px" ThreeState="true" ReadOnly="true"></vt:GridCheckBoxColumn>
              
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="90px"></vt:GridCheckBoxColumn>

                <vt:GridCheckBoxColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="90px"></vt:GridCheckBoxColumn>
            </Columns> 
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="505px" Height="50" Width="400" ID="lblLog1"></vt:Label>

        <vt:Button runat="server" Text="Set Column ThreeState >>" Left="430px" Width="170px" Top="605px"  ID="btnSetColumnThreeState" ClickAction="GridGridCheckBoxColumnThreeState\btnSetColumnThreeState_Click" ></vt:Button>

    </vt:WindowView>


</asp:Content>
