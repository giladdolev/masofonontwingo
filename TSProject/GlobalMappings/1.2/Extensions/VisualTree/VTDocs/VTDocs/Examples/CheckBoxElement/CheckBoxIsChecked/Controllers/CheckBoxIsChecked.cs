using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxIsCheckedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "IsChecked value: " + TestedCheckBox1.IsChecked.ToString();

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TestedCheckBox IsChecked value: " + TestedCheckBox2.IsChecked.ToString();

            CheckBoxElement TestedCheckBox3 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox3");
            lblLog2.Text += "\\r\\nTestedCheckBox1 IsChecked value: " + TestedCheckBox3.IsChecked.ToString();

            CheckBoxElement TestedCheckBox4 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox4");
            lblLog2.Text += "\\r\\nTestedCheckBox2 IsChecked value: " + TestedCheckBox4.IsChecked.ToString();

        }

        public void btnToggleTestedCheckBox_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");

            TestedCheckBox2.IsChecked = !TestedCheckBox2.IsChecked;

            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TestedCheckBox IsChecked value: " + TestedCheckBox2.IsChecked.ToString();

            CheckBoxElement TestedCheckBox3 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox3");
            lblLog2.Text += "\\r\\nTestedCheckBox1 IsChecked value: " + TestedCheckBox3.IsChecked.ToString();

            CheckBoxElement TestedCheckBox4 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox4");
            lblLog2.Text += "\\r\\nTestedCheckBox2 IsChecked value: " + TestedCheckBox4.IsChecked.ToString();
        }

        public void btnToggleTestedCheckBox1_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox3 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox3");

            TestedCheckBox3.IsChecked = !TestedCheckBox3.IsChecked;

            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            lblLog2.Text = "TestedCheckBox IsChecked value: " + TestedCheckBox2.IsChecked.ToString();
            
            lblLog2.Text += "\\r\\nTestedCheckBox1 IsChecked value: " + TestedCheckBox3.IsChecked.ToString();

            CheckBoxElement TestedCheckBox4 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox4");
            lblLog2.Text += "\\r\\nTestedCheckBox2 IsChecked value: " + TestedCheckBox4.IsChecked.ToString();
        }

        public void btnToggleTestedCheckBox2_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox4 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox4");

            TestedCheckBox4.IsChecked = !TestedCheckBox4.IsChecked;

            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            lblLog2.Text = "TestedCheckBox IsChecked value: " + TestedCheckBox2.IsChecked.ToString();

            CheckBoxElement TestedCheckBox3 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox3");
            lblLog2.Text += "\\r\\nTestedCheckBox1 IsChecked value: " + TestedCheckBox3.IsChecked.ToString();
            
            lblLog2.Text += "\\r\\nTestedCheckBox2 IsChecked value: " + TestedCheckBox4.IsChecked.ToString();
        }

        public void TestedCheckBoxes_CheckStateChanged(object sender, EventArgs e)
        {          
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            lblLog2.Text = "TestedCheckBox IsChecked value: " + TestedCheckBox2.IsChecked.ToString();

            CheckBoxElement TestedCheckBox3 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox3");
            lblLog2.Text += "\\r\\nTestedCheckBox1 IsChecked value: " + TestedCheckBox3.IsChecked.ToString();

            CheckBoxElement TestedCheckBox4 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox4");
            lblLog2.Text += "\\r\\nTestedCheckBox2 IsChecked value: " + TestedCheckBox4.IsChecked.ToString();
        }
    }
}