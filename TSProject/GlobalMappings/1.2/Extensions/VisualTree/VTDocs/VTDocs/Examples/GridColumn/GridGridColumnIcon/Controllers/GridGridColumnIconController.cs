using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridGridColumnIconController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void OnLoad(object sender, EventArgs e)
        {

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedGrid.Rows.Add("a", "b", "c", "d");
            TestedGrid.Rows.Add("e", "f", "g", "h");
            TestedGrid.Rows.Add("i", "j", "k", "l");

            string[] column1ImageSource = TestedGrid.Columns[0].Icon.Source.Split('/');
            string[] column2ImageSource = TestedGrid.Columns[1].Icon.Source.Split('/');


            lblLog.Text = "Column1 Icon name: " + column1ImageSource[column1ImageSource.Length - 1] + "\\r\\nColumn2 Icon name: " + column2ImageSource[column1ImageSource.Length - 1];

        }


        private void btnChangeColumn1Icon_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            if (TestedGrid.Columns[0].Icon.Source.Contains("gizmox"))
            {
                TestedGrid.Columns[0].Icon.Source = "/Content/Images/icon.jpg";
            }
            else
            {
                TestedGrid.Columns[0].Icon.Source = "/Content/Images/gizmox.png";
            }
            string[] column1ImageSource = TestedGrid.Columns[0].Icon.Source.Split('/');
            string[] column2ImageSource = TestedGrid.Columns[1].Icon.Source.Split('/');


            lblLog.Text = "Column1 Icon name: " + column1ImageSource[column1ImageSource.Length - 1] + "\\r\\nColumn2 Icon name: " + column2ImageSource[column1ImageSource.Length - 1];
        }

        private void btnChangeColumn2Icon_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
          
            if (TestedGrid.Columns[1].Icon.Source.Contains("gizmox"))
            {
                TestedGrid.Columns[1].Icon.Source = "/Content/Images/icon.jpg";
            }
            else
            {
                TestedGrid.Columns[1].Icon.Source = "/Content/Images/gizmox.png";
            }
            string[] column1ImageSource = TestedGrid.Columns[0].Icon.Source.Split('/');
            string[] column2ImageSource = TestedGrid.Columns[1].Icon.Source.Split('/');

            int i = column1ImageSource.Length;
            
            lblLog.Text = "Column1 Icon name: " + column1ImageSource[column1ImageSource.Length - 1] + "\\r\\nColumn2 Icon name: " + column2ImageSource[column2ImageSource.Length - 1];
        }

       

}
}
