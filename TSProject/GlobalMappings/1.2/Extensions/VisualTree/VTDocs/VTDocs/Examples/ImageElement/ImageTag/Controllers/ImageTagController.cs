using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageTagController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
     

        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage1 = this.GetVisualElementById<ImageElement>("TestedImage1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            if (TestedImage1.Tag == null)
            {
                lblLog1.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog1.Text = "Tag value: " + TestedImage1.Tag;
            }

            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            if (TestedImage2.Tag == null)
            {
                lblLog2.Text = "Tag value: null (no Tag)";
            }
            else
            {
                lblLog2.Text = "Tag value: " + TestedImage2.Tag;
            }

        }


        public void btnChangeTag_Click(object sender, EventArgs e)
        {
            ImageElement TestedImage2 = this.GetVisualElementById<ImageElement>("TestedImage2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            if (TestedImage2.Tag == "New Tag.")
            {
                TestedImage2.Tag = TestedImage2;
                lblLog2.Text = "Tag value: " + TestedImage2.Tag;
            }
            else
            {
                TestedImage2.Tag = "New Tag.";
                lblLog2.Text = "Tag value: " + TestedImage2.Tag;

            }
        }
    }
}