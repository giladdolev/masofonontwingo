<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic TextBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="BasicTextBox\Onload">
       
        <vt:TextBox runat="server" Text="bbbbb" PasswordChar="" Top="100px" Left="20px" ID="textBox2" BackColor="Red"  Font-Underline="true" Height="20px" TabIndex="2" Width="180px" DirtychangeAction="BasicTextBox\dirtycahnge" >
        </vt:TextBox>
        <vt:Button runat="server" Text="Change Font style" Top="44px" Left="300px" ID="button1" Height="36px" TabIndex="1" Width="200px" ClickAction="BasicTextBox\ChangeFont_Click"></vt:Button>
        <vt:Button runat="server" Text="Change Size" Top="100px" Left="300px" ID="button2" Height="36px" TabIndex="1" Width="200px" ClickAction="BasicTextBox\ChangeSize_Click"></vt:Button>
      
    </vt:WindowView>
</asp:Content>
