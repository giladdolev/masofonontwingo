<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme ="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
  <vt:WindowView runat="server" Text="ToolBar BackgroundImageLayout Property" ID="windowView" LoadAction="ToolBarBackgroundImageLayout\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImageLayout" Left="240px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image position on the control (Center , None, Stretch, Tile or Zoom).
            Tile is the default value." Top="75px"  ID="lblDefinition1"></vt:Label>
      
        <!-- TestedToolBar1 -->
        <vt:ToolBar runat="server" BackgroundImage ="~/Content/Images/icon.jpg" Top="140px" ID="TestedToolBar1">
        <vt:ToolBarButton runat="server" id="ToolBarItem1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="190px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" Text="BackgroundImageLayout takes effect only if the BackgroundImage property is set." Top="240px"  ID="lblDefinition2"></vt:Label>
      


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the initial BackgroundImageLayout of the ToolBar is set to 'Center'.
            Use the Buttons below (None/ Stretch/ Center/ Zoom/ Tile) to change the ToolBar 
            Background Image Layout." Top="350px"  ID="lblExp1"></vt:Label>     
        
       <!-- TestedToolBar2 -->
        
      <vt:ToolBar runat="server" BackgroundImage ="~/Content/Images/icon.jpg" BackgroundImageLayout ="Center" Top="430px" ID="TestedToolBar2">
        <vt:ToolBarButton runat="server" id="ToolBarButton1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarButton2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            </vt:ToolBar>
        <vt:Label runat="server" SkinID="Log" Top="480px" ID="lblLog2"></vt:Label>  
      

         <vt:Button runat="server" Text="Tile"  Top="570px" Left="560px" ID="btnTile" Height="30px" TabIndex="1" Width="60px" ClickAction="ToolBarBackgroundImageLayout\btnTile_Click"></vt:Button>  
         <vt:Button runat="server" Text="Center"   Top="570px" Left="340px" ID="btnCenter" Height="30px" TabIndex="1" Width="60px" ClickAction="ToolBarBackgroundImageLayout\btnCenter_Click"></vt:Button>  
         <vt:Button runat="server" Text="Stretch"   Top="570px" Left="230px" ID="btnStretch" Height="30px" TabIndex="1" Width="60px" ClickAction="ToolBarBackgroundImageLayout\btnStretch_Click"></vt:Button>  
         <vt:Button runat="server" Text="Zoom"  Top="570px" Left="450px" ID="btnZoom" Height="30px" TabIndex="1" Width="60px" ClickAction="ToolBarBackgroundImageLayout\btnZoom_Click"></vt:Button>  
         <vt:Button runat="server" Text="None" Top="570px" Left="120px" ID="btnNone" Height="30px" TabIndex="1" Width="60px" ClickAction="ToolBarBackgroundImageLayout\btnNone_Click"></vt:Button> 
 
    </vt:WindowView>
</asp:Content>
        