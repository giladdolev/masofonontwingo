
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Value Property" ID="windowView1" LoadAction="ProgressBarValue\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Value" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the current position of the progress bar.
            The default is 0.
            
            Syntax: public int Value { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:ProgressBar runat="server" Text="ProgressBar" Top="170px" ID="TestedProgressBar1"></vt:ProgressBar>
        <vt:Label runat="server" SkinID="Log" Top="220px" Width="520px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" Text="Exception condition: 
            The value specified is greater than the value of the Maximum property.
            or
            The value specified is less than the value of the Minimum property." Top="265px"  ID="lblDefinition2"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="390px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="'Value' property of this 'TestedProgressBar' is initially set to 37.
            Min property is set to '10', Max property is set to '90'." Top="440px"  ID="lblExp1"></vt:Label>     
        
        <vt:ProgressBar runat="server" Text="TestedProgressBar" Value ="37" Min="10" Max="90" Top="500px" ID="TestedProgressBar2"></vt:ProgressBar>
        <vt:Label runat="server" SkinID="Log" Top="550px" Width="520px" ID="lblLog2"></vt:Label>

        <vt:Label runat="server" Text="Insert a new Value:" ID="lblSetValue" Left="80px" Top="610px"></vt:Label>

        <vt:TextBox runat="server" Top="610px" Left="240px" ID="txtInput" Text="..." width="50px" TextChangedAction="ProgressBarValue\TextBox_TextChanged" WaitMaskDisabled="true"></vt:TextBox>

        <vt:Button runat="server" Text="Set Value >>" left="350px" Top="610px" ID="btnChangeValue" ClickAction="ProgressBarValue\btnChangeValue_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

