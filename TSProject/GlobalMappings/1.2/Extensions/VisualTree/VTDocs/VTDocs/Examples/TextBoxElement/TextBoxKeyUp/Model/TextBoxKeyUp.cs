﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class TextBoxKeyUp : WindowElement
    {
        public TextBoxKeyUp()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        bool isCtrlPressed;


        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>

        public bool isCtrlPressedProperty
        {
            get { return this.isCtrlPressed; }
            set { this.isCtrlPressed = value; }
        }
    }
}