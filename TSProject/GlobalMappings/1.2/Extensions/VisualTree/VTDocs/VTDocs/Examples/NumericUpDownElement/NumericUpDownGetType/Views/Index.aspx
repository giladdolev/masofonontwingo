﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown GetType Method" ID="windowView2" LoadAction="NumericUpDownGetType\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="GetType" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the Type of the current instance.

            Syntax: public Type GetType()" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the GetType button will invoke the 
            GetType method of this NumericUpDown." Top="250px" ID="lblExp2"></vt:Label>

        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="340px" ID="TestedNumericUpDown"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Width="380px" Height="40px" ID="lblLog" Top="380"></vt:Label>

        <vt:Button runat="server" Text="GetType >>" Top="460px" ID="btnGetType" ClickAction="NumericUpDownGetType\GetType_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
