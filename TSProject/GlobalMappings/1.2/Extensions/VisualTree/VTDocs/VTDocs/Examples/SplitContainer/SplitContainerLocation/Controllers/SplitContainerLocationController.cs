using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerLocationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Location value: " + TestedSplitContainer.Location;
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedSplitContainer.Location == new Point(80, 310))
            {
                TestedSplitContainer.Location = new Point(200, 290);
                lblLog.Text = "Location value: " + TestedSplitContainer.Location;
            }
            else
            {
                TestedSplitContainer.Location = new Point(80, 310);
                lblLog.Text = "Location value: " + TestedSplitContainer.Location;
            }
        }

    }
}