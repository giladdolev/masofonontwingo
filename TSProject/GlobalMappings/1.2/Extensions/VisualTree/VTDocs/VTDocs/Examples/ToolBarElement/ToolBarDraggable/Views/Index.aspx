<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Draggable Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="1000px" LoadAction="ToolBarDraggable\Load_Click">
		

        <vt:Label runat="server" Text="Draggable is initially set to True" Top="48px" Left="41px" ID="label1" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="9.75pt" Height="16px" TabIndex="4" Width="682px">
		</vt:Label>

        <vt:ToolBar runat="server" Text="toolStrip1" Dock="None" Top="50px" Draggable ="true" Left="276px" ID="ToolBar1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="25px" Width="466px">
            <vt:ToolBarMenuItem runat="server" ID="ToolBarMenuItem1" Text="menuItem1" ClickAction="ToolBarDraggable\btnChangeDraggable_Click"> </vt:ToolBarMenuItem>
		</vt:ToolBar>


         <vt:Label runat="server" Text="RunTime Test" Top="120px" Left="41px" ID="label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="9.75pt" Height="16px" TabIndex="4" Width="682px">
		</vt:Label>

        <vt:ToolBar runat="server" Text="toolStrip1" Dock="None" Top="150px" Left="276px" ID="toolStrip1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="25px" Width="466px">
            <vt:ToolBarMenuItem runat="server" ID="menuItem" Text="menuItem1" ClickAction="ToolBarDraggable\btnChangeDraggable_Click"> </vt:ToolBarMenuItem>
		</vt:ToolBar>
       
        
		<vt:Button runat="server" UseVisualStyleDraggable="True" TextAlign="MiddleCenter" Text="Change Draggable" Top="145px" Left="44px" ClickAction="ToolBarDraggable\btnChangeDraggable_Click" ID="btnChangeDraggable" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button>

		
		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="270px" Left="400px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="198px">
		</vt:TextBox>

		

        </vt:WindowView>
</asp:Content>