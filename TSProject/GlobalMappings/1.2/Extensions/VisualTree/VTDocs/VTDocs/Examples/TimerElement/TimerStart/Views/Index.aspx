<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer Start Method" ID="windowView2" LoadAction="TimerStart\OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="Start" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Starts the timer.

            Syntax: public void Start()

            You can also start the timer by setting the Enabled property to 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Press the 'Start' button to invoke the 'Start' method." Top="280px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" SkinID="TimerLabel" Top="335px" ID="timerLabel"></vt:Label>

        <vt:TextBox runat="server" SkinID="TimerTextBox" Top="330px" ID="timerTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="370"></vt:Label>

        <vt:Button runat="server" Text="Start >>"  Top="435px" ID="btnStart" ClickAction="TimerStart\btnStart_Click"></vt:Button>

        <vt:Button runat="server" Text="Stop >>"  Left="300" Top="435px" ID="btnStop" ClickAction="TimerStart\btnStop_Click"></vt:Button>

    </vt:WindowView>

    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="TestedTimer" Enabled="false" Interval="1000" TickAction="TimerStart\TestedTimer_Tick"></vt:Timer>
	</vt:ComponentManager>

</asp:Content>

        