<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Value Property" Top="57px" Left="11px" ID="windowView1" Height="750px" Width="768px">

   

        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Value="MyValue" Top="115px" Left="140px" ID="testedRichTextBox" Height="70px"  TabIndex="3" Width="200px"></vt:RichTextBox>

        <vt:Button runat="server" Text="Change RichTextBox Value" Top="200px" Left="400px" ID="btnChangeValue" Height="36px" TabIndex="1" Width="220px" ClickAction="RichTextBoxValue\btnChangeValue_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="220px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
