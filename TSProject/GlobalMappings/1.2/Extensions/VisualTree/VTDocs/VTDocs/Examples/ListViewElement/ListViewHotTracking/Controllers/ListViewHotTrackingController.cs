using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewHotTrackingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Handles the Load event of the Form control.
        ///// </summary>
  
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "HotTracking value: " + TestedListView1.HotTracking;
            lblLog2.Text = "HotTracking value: " + TestedListView2.HotTracking + '.';

        }

        
    }
}