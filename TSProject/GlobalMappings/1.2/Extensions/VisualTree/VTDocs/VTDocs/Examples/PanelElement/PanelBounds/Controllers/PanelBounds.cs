using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Panel bounds are: \\r\\n" + TestedPanel.Bounds;

        }
        public void btnChangeBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.Bounds == new Rectangle(100, 340, 250, 50))
            {
                TestedPanel.Bounds = new Rectangle(80, 360, 200, 80);
                lblLog.Text = "Panel bounds are: \\r\\n" + TestedPanel.Bounds;
            }
            else
            {
                TestedPanel.Bounds = new Rectangle(100, 340, 250, 50);
                lblLog.Text = "Panel bounds are: \\r\\n" + TestedPanel.Bounds;
            }

        }

    }
}