<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox SelectionColor property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
         
        <vt:RichTextBox runat="server" Text="sample text " ID="richTextBox1" Left="20px" Top="100px" Height="200px" Width="400px"></vt:RichTextBox>
        
        <vt:Button runat="server" ID="btn1" Text="Change selectedText Color" Width="250" Height="25" ClickAction="RichTextBoxSelectionColor\clickBtn1" Top="350px" Left="100px"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="420px" Left="100px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
