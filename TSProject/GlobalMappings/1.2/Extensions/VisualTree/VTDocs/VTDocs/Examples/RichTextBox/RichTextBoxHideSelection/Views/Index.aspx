<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox HideSelection Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="HideSelection Property" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="240px"></vt:Label>

        <vt:RichTextBox runat="server" Text="Hello World" Top="125px" Left="140px" ID="rtfHideSelection" Height="100px"  Width="150px"></vt:RichTextBox>

        <vt:Button runat="server" Text="Change HideSelection" Top="320px" Left="140px" ID="btnHideSelection" Height="36px"  Width="200px" ClickAction="RichTextBoxHideSelection\btnHideSelection_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="380px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
