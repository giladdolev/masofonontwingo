﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Compatibility;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    public class BasicFileSystemDialogController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void SelectedDriveChanged(object sender, EventArgs e)
        {
            DriveListBoxElement dlb1 = this.GetVisualElementById<DriveListBoxElement>("dlb1");
            DirectoryListBoxElement dirlb1 = this.GetVisualElementById<DirectoryListBoxElement>("dirlb1");
            dirlb1.Path = dlb1.Drive.Drive;
        }
        public void SelectedDirectoryChanged(object sender, EventArgs e)
        {
            DirectoryListBoxElement dirlb1 = this.GetVisualElementById<DirectoryListBoxElement>("dirlb1");
            FileListBoxElement flb1 = this.GetVisualElementById<FileListBoxElement>("flb1");
            flb1.Path = dirlb1.Path;
        }
        public void SelectedFileChanged(object sender, EventArgs e)
        {
            FileListBoxElement flb1 = this.GetVisualElementById<FileListBoxElement>("flb1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = String.Format("{0} file selected", flb1.FileName);
        }
    }
}