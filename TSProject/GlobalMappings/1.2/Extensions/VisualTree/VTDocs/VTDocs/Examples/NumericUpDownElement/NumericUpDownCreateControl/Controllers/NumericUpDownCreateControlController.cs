using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new NumericUpDownCreateControl());

        }

        private NumericUpDownCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as NumericUpDownCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            NumericUpDownElement newNumericUpDown = new NumericUpDownElement();

            newNumericUpDown.CreateControl();
            newNumericUpDown.Text = "newNumericUpDown";

            this.ViewModel.Controls.Add(newNumericUpDown);
            

            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}