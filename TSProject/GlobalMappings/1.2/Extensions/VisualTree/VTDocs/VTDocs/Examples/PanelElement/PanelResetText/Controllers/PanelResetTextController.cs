using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelResetTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //PanelAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedPanel.Text != "")
            {
                lblLog.Text = "Panel Text is set to " + TestedPanel.Text;
            }
            else
            {
                lblLog.Text = "Panel Text is empty";
            }            
        }

        //RichPanelAlignment
        public void btnResetText_Click(object sender, EventArgs e)
        {

            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedPanel.ResetText();

            if (TestedPanel.Text != "")
            {
                lblLog.Text = "Panel Text is set to " + TestedPanel.Text;
            }
            else
            {
                lblLog.Text = "Panel Text is empty";
            }          
        }

        public void btnSetText_Click(object sender, EventArgs e)
        {

            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedPanel.Text = "TestedPanel";

            if (TestedPanel.Text != "")
            {
                lblLog.Text = "Panel Text is set to " + TestedPanel.Text;
            }
            else
            {
                lblLog.Text = "Panel Text is empty";
            }  
        }

    }
}