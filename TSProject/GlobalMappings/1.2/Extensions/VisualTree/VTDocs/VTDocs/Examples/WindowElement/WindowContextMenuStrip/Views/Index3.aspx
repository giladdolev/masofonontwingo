<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ContextMenuStrip Initialized Window" ID="ContextMenuStripInitializedWindow" ContextMenuStripID="contextMenuStrip1" EnableEsc="true" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleDimensions="6, 13" Height="262px" Width="284px" LoadAction="WindowContextMenuStrip3\OnLoad">

        <vt:Button runat="server" Text="contextMenuStrip1" Top="10px" Left="30px" ID="btnChangeToContextMenuStrip1" ClickAction="WindowContextMenuStrip3\btnChangeToContextMenuStrip1_Click"></vt:Button>

        <vt:Button runat="server" Text="contextMenuStrip2" Top="50px" Left="30px" ID="btnChangeToContextMenuStrip2" ClickAction="WindowContextMenuStrip3\btnChangeToContextMenuStrip2_Click"></vt:Button>

        <vt:Button runat="server" Text="Reset" Top="90px" Left="30px" ID="btnReset" ClickAction="WindowContextMenuStrip3\btnReset_Click"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="130px" Left="10px" Width="200px" Height="40px" ID="lblLog2"></vt:Label>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip1" >
            <vt:ToolBarMenuItem runat ="server" ID="HelloStripMenuItem" Text="Hello"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip2" >
            <vt:ToolBarMenuItem runat ="server" ID="HelloWorldMenuItem1" Text="Hello World"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>
	</vt:WindowView>
</asp:Content>
