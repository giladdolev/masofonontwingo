<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button UseVisualStyleBackColor Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="UseVisualStyleBackColor is Initially set to false" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Button runat="server" Text="Tested button" Top="45px" Left="140px" UseVisualStyleBackColor ="false" ID="btnUseVisualStyleBackColor" Height="36px" Width="160px"></vt:Button>
                   
        <vt:Button runat="server" Text="Change UseVisualStyleBackColor value" Top="45px" Left="350px" ID="btnChangeUseVisualStyleBackColor" Height="36px" Width="220px" ClickAction ="ButtonUseVisualStyleBackColor\btnChangeUseVisualStyleBackColor_Click"></vt:Button>  


        <vt:TextBox runat="server" Text="" Top="120px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
