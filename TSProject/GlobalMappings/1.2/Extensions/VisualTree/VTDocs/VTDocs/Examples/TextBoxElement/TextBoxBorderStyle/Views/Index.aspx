<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox BorderStyle Property" ID="windowView1" LoadAction="TextBoxBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:TextBox runat="server" Text="TextBox" Top="170px" ID="TestedTextBox1"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="210px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="275px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this TextBox is initially set to 'Dotted'" Top="315px"  ID="lblExp1"></vt:Label>     
        
        <vt:TextBox runat="server"  Text="TestedTextBox" BorderStyle="Dotted" Top="355px" ID="TestedTextBox2"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="470px" ID="btnChangeTextBoxBorderStyle" ClickAction="TextBoxBorderStyle\btnChangeTextBoxBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
