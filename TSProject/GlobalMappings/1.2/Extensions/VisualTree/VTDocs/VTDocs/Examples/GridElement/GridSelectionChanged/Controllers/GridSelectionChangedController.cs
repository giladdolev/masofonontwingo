﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridSelectionChangedController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.AddToComboBoxList("col1", "male,female");

            testedGrid.Rows.Add("a", false, "female");
            testedGrid.Rows.Add("b", false, "male");
            testedGrid.Rows.Add("c", true, "female");

            //GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");

           // TestedGrid2.Rows.Add("d", "TextBtn1", "6/8/1974 12:00:00 AM");
          //  TestedGrid2.Rows.Add("e", "TextBtn2", "7/8/1974 12:00:00 AM");
          //  TestedGrid2.Rows.Add("f", "TextBtn3", "8/8/1974 12:00:00 AM");
          //  lblLog.Text = "Left and Right Grids: SelectedCells values: Empty";
        }

        public void TestedGrid_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nLeft Grid: BeforeSelectionChanged event is invoked";
        }

        public void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            if (TestedGrid.Rows[0].Cells[0].Value == "a")
            {
                TestedGrid.Rows[0].Cells[0].Value = "newValue";
            }
            else
            {
                TestedGrid.Rows[0].Cells[0].Value = "a";
            }

            txtEventLog.Text += "\\r\\nLeft Grid: SelectionChanged event is invoked";

            lblLog.Text = "Left Grid: SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + TestedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }

        }

        public void TestedGrid2_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nRight Grid: BeforeSelectionChanged event is invoked";
        }

        public void TestedGrid2_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            if (TestedGrid.Rows[0].Cells[0].Value == "d")
                TestedGrid.Rows[0].Cells[0].Value = "newValue";

            else
                TestedGrid.Rows[0].Cells[0].Value = "d";

            txtEventLog.Text += "\\r\\nRight Grid: SelectionChanged event is invoked";

            lblLog.Text = "Right Grid: SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + TestedGrid2.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }

             }

        public void btnSelectRow0_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[0].Selected = true;
            TestedGrid2.Rows[0].Selected = true;
            //lblLog.Text = "Selected row index: Left Grid - (" + testedGrid.SelectedRowIndex.ToString() + "), " + "Right Grid - (" + TestedGrid2.SelectedRowIndex.ToString() + ") ";        
        }

        public void btnSelectColumn0_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Columns[0].Selected = true;
            TestedGrid2.Columns[0].Selected = true;
            //lblLog.Text = "Selected Column index: Left Grid - (" + testedGrid.SelectedColumnIndex.ToString() + "), " + "Right Grid - (" + TestedGrid2.SelectedColumnIndex.ToString() + ") ";
        }

        
        public void btnSelectb_Click(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows[1].Cells[0].Selected = true;

            lblLog.Text = "Left Grid: SelectedCells values: (" + testedGrid.Rows[1].Cells[0].Value + ") ";
        }

        public void btnSelecte_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid2.Rows[1].Cells[0].Selected = true;

            lblLog.Text = "Right Grid: SelectedCells values: (" + TestedGrid2.Rows[1].Cells[0].Value + ") ";
        }

        public void btnClear_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();

            txtEventLog.Text = "Event Log:";
        }
    }
}
