using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxTextAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Set TextAlign property
        private void btnChangeAlignment_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox19 = this.GetVisualElementById<TextBoxElement>("textBox19");

            if (textBox19.TextAlign == HorizontalAlignment.Center) 
                textBox19.TextAlign = HorizontalAlignment.Left;

            else if (textBox19.TextAlign == HorizontalAlignment.Left)
                textBox19.TextAlign = HorizontalAlignment.Right;

            else if (textBox19.TextAlign == HorizontalAlignment.Right)
                textBox19.TextAlign = HorizontalAlignment.Center;
            
            
           

        }

        //Get TextAlign property
        private void btnGetTextAlign_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox19 = this.GetVisualElementById<TextBoxElement>("textBox19");
            MessageBox.Show("TextAlign mode: " + textBox19.TextAlign.ToString());
        }
    }
}