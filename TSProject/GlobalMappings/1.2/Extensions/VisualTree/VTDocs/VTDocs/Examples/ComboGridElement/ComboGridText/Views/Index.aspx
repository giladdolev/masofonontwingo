<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid Text Property" ID="windowView2" LoadAction="ComboGridText/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with this control.
            
            Syntax: public override string Text { get; set; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Text property of the ComboGrid is initially set to 'TestedComboGrid'.
          You can change the Text property by clicking the 'Change Text' button or by typing a text
           manually.  "
            Top="235px" ID="lblExp1">
        </vt:Label>

        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-TestedComboGrid" Top="325px" ID="TestedComboGrid" TextChangedAction="ComboGridText\TestedComboGrid_TextChanged"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="395px" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Text Value >>" Top="460px" Width="180px" ID="btnChangeComboGridText" ClickAction="ComboGridText\btnChangeComboGridText_Click"></vt:Button>

        <vt:Button runat="server" Text="Empty Text >>" Width="180px" Left="290px" Top="460px" ID="btnEmptyString" ClickAction="ComboGridText\btnEmptyString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
