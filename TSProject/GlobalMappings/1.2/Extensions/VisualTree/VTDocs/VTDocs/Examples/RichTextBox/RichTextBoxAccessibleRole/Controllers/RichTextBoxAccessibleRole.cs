using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            RichTextBox rtfAccessibleRole = this.GetVisualElementById<RichTextBox>("rtfAccessibleRole");

            MessageBox.Show(rtfAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            RichTextBox rtfAccessibleRole = this.GetVisualElementById<RichTextBox>("rtfAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else if (rtfAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                rtfAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = rtfAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}