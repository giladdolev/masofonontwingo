using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformControlRemovedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlRemoved_Click(object sender, EventArgs e)
        {
            
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");

            ControlEventArgs args = new ControlEventArgs(testedTextBox);

            testedTextBox.PerformControlRemoved(args);
        }

        public void testedTextBox_ControlRemoved(object sender, EventArgs e)
        {
            MessageBox.Show("TestedTextBox ControlRemoved event method is invoked");
        }

    }
}