using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridShowAutoFilterColumnItemController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "ShowAutoFilterColumnItem value: " + TestedGrid1.ShowAutoFilterColumnItem.ToString();

            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TestedGrid2.Rows.Add("a", "b", "c");
            TestedGrid2.Rows.Add("d", "e", "f");
            TestedGrid2.Rows.Add("g", "h", "i");
            lblLog2.Text = "ShowAutoFilterColumnItem value: " + TestedGrid2.ShowAutoFilterColumnItem.ToString();

        }

        public void btnChangeShowAutoFilterColumnItem_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.ShowAutoFilterColumnItem == true)
            {
                TestedGrid2.ShowAutoFilterColumnItem = false;
                lblLog2.Text = "ShowAutoFilterColumnItem value: " + TestedGrid2.ShowAutoFilterColumnItem.ToString();
            }
            else
            {
                TestedGrid2.ShowAutoFilterColumnItem = true;
                lblLog2.Text = "ShowAutoFilterColumnItem value: " + TestedGrid2.ShowAutoFilterColumnItem.ToString();

            }
        }


	}
}
