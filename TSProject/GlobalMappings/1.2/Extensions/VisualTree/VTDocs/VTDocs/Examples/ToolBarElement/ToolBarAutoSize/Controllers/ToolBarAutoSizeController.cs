using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarAutoSizeController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnChangeSize_Click(object sender, EventArgs e)
		{
			ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
			if (toolStrip1.Size == new Size(466, 25))
			{
				toolStrip1.Size = new Size(600, 50);
			}
			else
				toolStrip1.Size = new Size(466, 25);
		}

		public void btnChangeAutoSize_Click(object sender, EventArgs e)
		{
			ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
			TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
			toolStrip1.AutoSize = !toolStrip1.AutoSize;
			textBox1.Text = "AutoSize value: " + toolStrip1.AutoSize;
		}
      
       
    }
}