<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid ExpandOnFocus Property" ID="windowView2"  LoadAction="ComboGridExpandOnFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ExpandOnFocus" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the combogrid expands its drop-down portion when it 
            has focus.
            Syntax: public bool ExpandOnFocus { get; set; }            
            Value: true if the drop-down portion expands on focus; otherwise, false. The default is false."
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:ComboGrid runat="server" Text="ComboGrid" CssClass="vt-ComboGrid" Top="180px" ID="TestedComboGrid1"></vt:ComboGrid>
        
        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="275px" ID="Label4"></vt:Label>
       
         <vt:Label runat="server" Text="ExpandOnFocus property of this 'ComboGrid' is initially set to 'True'.
             You can click on the button to toggle the value at runtime." Top="325px" ID="Label5"></vt:Label>

        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-TestedComboGrid" ExpandOnFocus="true" Top="390px" ID="TestedComboGrid2" ></vt:ComboGrid>
        
        <vt:Label runat="server" SkinID="Log" Top="555px" ID="lblLog2"></vt:Label>
                   
        <vt:Button runat="server" Text="Change ExpandOnFocus value >>" Top="605px" ID="btnChangeExpandOnFocus" Width="200px" ClickAction ="ComboGridExpandOnFocus\btnChangeExpandOnFocus_Click"></vt:Button>  

    </vt:WindowView>
</asp:Content>
        
