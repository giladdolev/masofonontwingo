using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxAutoScrollController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeAutoScroll_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpRuntimeAutoScroll = this.GetVisualElementById<GroupBoxElement>("grpRuntimeAutoScroll");

            if (grpRuntimeAutoScroll.AutoScroll == false)
            {
                grpRuntimeAutoScroll.AutoScroll = true;
                textBox1.Text = "AutoScroll == true";
            }
            else
            {
                grpRuntimeAutoScroll.AutoScroll = false;
                textBox1.Text = "AutoScroll == false";
            }
            
        }

    }
}