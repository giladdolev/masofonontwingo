using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownPerformMouseClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseClick_Click(object sender, EventArgs e)
        {
            
            NumericUpDownElement testedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("testedNumericUpDown");
            
            TreeItem t = new TreeItem();
            MouseButtons m = new MouseButtons();
            TreeItemMouseClickEventArgs args = new TreeItemMouseClickEventArgs(t,m,0,0,0);

            testedNumericUpDown.PerformMouseClick(args);
        }

        public void testedNumericUpDown_MouseClick(object sender, EventArgs e)
        {
            MessageBox.Show("TestedNumericUpDown MouseClick event method is invoked");
        }

    }
}