<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox ScrollBars property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="'ScrollBars' is initially set to 'Both'" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="240px"></vt:Label>

        <vt:RichTextBox runat="server" Text="Click the 'Change ScrollBars' button to display all ScrollBars modes" ScrollBars="Both" Multiline ="true" Top="125px" Left="140px" ID="rtfScrollBars" Height="36px"  Width="150px"></vt:RichTextBox>

        <vt:Button runat="server" Text="Change ScrollBars" Top="115px" Left="350px" ID="btnChangeScrollBars" Height="36px"  Width="150px" ClickAction="RichTextBoxScrollBars\btnChangeScrollBars_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
