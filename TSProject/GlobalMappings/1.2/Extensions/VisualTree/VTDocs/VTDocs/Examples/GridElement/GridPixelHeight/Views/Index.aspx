<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid PixelHeight Property" ID="windowView2" LoadAction="GridPixelHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelHeight" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int PixelHeight { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this 'TestedGrid' is initially set to 115px" Top="250px"  ID="lblExp1"></vt:Label>     

        <!-- TestedGrid -->
        <vt:Grid runat="server" Top="300px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelHeight value >>" Top="495px" ID="btnChangePixelHeight" Width="180px" ClickAction="GridPixelHeight\btnChangePixelHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
