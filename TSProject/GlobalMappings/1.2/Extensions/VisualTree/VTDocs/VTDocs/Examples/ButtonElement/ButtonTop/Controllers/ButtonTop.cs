using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //public void btnChangeTop_Click(object sender, EventArgs e)
        //{
        //    ButtonElement btnChangeTop = this.GetVisualElementById<ButtonElement>("btnChangeTop");

        //    if (btnChangeTop.Top == 115)
        //    {
        //        btnChangeTop.Top = 200;
        //    }
        //    else
        //    {
        //        btnChangeTop.Top = 115;
        //    }
            
        //}

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Button Top is: " + btnTestedButton.Top;

        }
        public void btnChangeTop_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            if (btnTestedButton.Top == 370)
            {
                btnTestedButton.Top = 350;
                lblLog.Text = "Button Top is: " + btnTestedButton.Top;

            }
            else
            {
                btnTestedButton.Top = 370;
                lblLog.Text = "Button Top is: " + btnTestedButton.Top;
            }

        }

    }
}