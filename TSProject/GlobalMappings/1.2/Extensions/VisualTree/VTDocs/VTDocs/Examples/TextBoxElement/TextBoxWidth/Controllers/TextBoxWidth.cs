using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Width value: " + TestedTextBox.Width + '.';
        }

        private void btnChangeWidth_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTextBox.Width == 150)
            {
                TestedTextBox.Width = 300;
                lblLog.Text = "Width value: " + TestedTextBox.Width + '.';
            }
            else
            {
                TestedTextBox.Width = 150;
                lblLog.Text = "Width value: " + TestedTextBox.Width + '.';
            }
        }
      
    }
}