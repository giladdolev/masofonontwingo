<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox TextChanged event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px" >
      
        <vt:Label runat="server" Text="Textbox that cause load mask on parent container" Top="16px" Left="16px"></vt:Label>
        <vt:TextBox runat="server" Top="40px" Left="16px" Text="Hello World"  ID="txtTestedTextBox" Height="128px" Multiline="true"  Width="164px" TextChangedAction ="TextBoxWaitMaskDisabled\txtTestedTextBox_TextChanged" WaitMaskDisabled="true"></vt:TextBox>

        <vt:Label runat="server" Top="200px" Left="16px" Text="Textbox that cause load mask on parent container" ></vt:Label>
        <vt:TextBox runat="server" Top="224px" Left="16px" Text=""  Multiline="true" ID="txtEventTrack" TextChangedAction ="TextBoxWaitMaskDisabled\txtTestedTextBox_TextChanged" Height="64px" Width="164px"></vt:TextBox>

        <vt:Label runat="server" Top="300px" Left="16px" Text="Textbox with button for toggle WaitMaskDisabled property" ></vt:Label>
        <vt:TextBox runat="server" Top="324px" Left="16px" Text=""  Multiline="true" ID="textBox3" TextChangedAction ="TextBoxWaitMaskDisabled\txtTestedTextBox_TextChanged" Height="64px" Width="164px"></vt:TextBox>
        <vt:Label runat="server" Top="324px" Left="200" ID="statusLabel" Text="WaitMaskDisabled = False" ></vt:Label>
        <vt:Button runat="server" Top="400px" Left="16px" ID="btnToggler" Text="Set WaitMaskDisabled to true" ClickAction="TextBoxWaitMaskDisabled\btnToggler_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
