﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid ShowAutoFilterColumnItem Property" ID="windowView1" LoadAction="GridShowAutoFilterColumnItem\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ShowAutoFilterColumnItem" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of the control.
            
            public virtual Color ShowAutoFilterColumnItem { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Grid runat="server" Text="Grid" Top="140px" ID="TestedGrid1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="270px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle"  Top="335px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="ShowAutoFilterColumnItem property of this 'TestedGrid' is initially set to Gray" Top="365px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" Text="TestedGrid" ShowAutoFilterColumnItem ="true" Top="435px" ID="TestedGrid2" >
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" ></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="565px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change ShowAutoFilterColumnItem value >>" Width="180px" Top="630px" ID="btnChangeShowAutoFilterColumnItem" ClickAction="GridShowAutoFilterColumnItem\btnChangeShowAutoFilterColumnItem_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
