using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPerformMouseClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseClick_Click(object sender, EventArgs e)
        {
            
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");
            
            TreeItem t = new TreeItem();
            MouseButtons m = new MouseButtons();
            TreeItemMouseClickEventArgs args = new TreeItemMouseClickEventArgs(t,m,0,0,0);

            testedSplitContainer.PerformMouseClick(args);
        }

        public void testedSplitContainer_MouseClick(object sender, EventArgs e)
        {
            MessageBox.Show("TestedSplitContainer MouseClick event method is invoked");
        }

    }
}