﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Selection Color Property" ID="windowView1" LoadAction="GridSelectionColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Selection Color" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color of a cell
            
              public Color SelectionColor" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="SelectionColor property of this 'TestedGrid' is initially set to Gray
            and will switch the color of the first cell" Top="175px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" BackColor ="Gray" Top="225px" ID="TestedGrid2">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="390px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Selection Color" Width="180px" Top="420px" ID="btnChangeBackColor" ClickAction="GridSelectionColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
