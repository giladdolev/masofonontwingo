using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridLeaveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Click on the Grid cells...";

            GridElement testedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");

            testedGrid1.Rows.Add("a", "b", "c");

            GridElement testedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");

            testedGrid2.Rows.Add("d", "e", "f");
        }

        public void TestedGrid1_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nGrid 1: Leave event is invoked";

            lblLog.Text = "Grid 1 was leaved";
        }

        public void TestedGrid1_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Grid 1 SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }

        public void TestedGrid2_Leave(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nGrid 2: Leave event is invoked";

            lblLog.Text = "Grid 2 was leaved";
        }

        public void TestedGrid2_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Grid 2 SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }
        public void clientevent(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid2");

        }


    }
}