<%@ Page Title="BasicFileSystemDialog" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic File ListBox" ID="windowView1">

        <vt:Label runat="server" SkinID="Title" Text="Basic File ListBox" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Show behavior of compatibility File ListBox" Top="75px"  ID="lblDefinition"></vt:Label>
      
        <!-- Actual sample element(s) should be placed instead of button below -->

        <vt:FileListBox runat="server" ID="flb1" Path="D:\My Work\xx\" FileUploadedAction="FileListBoxBasic\FileUploaded" SelectedItemChangedAction="FileListBoxBasic\SelectedItemChanged" Top="100px" Left="50px" Width="650px" Height="350px"></vt:FileListBox> 

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog1"></vt:Label>
 
    </vt:WindowView>
</asp:Content>
