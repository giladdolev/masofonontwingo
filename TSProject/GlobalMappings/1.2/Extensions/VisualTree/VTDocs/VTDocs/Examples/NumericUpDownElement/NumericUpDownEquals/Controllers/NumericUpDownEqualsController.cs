using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new NumericUpDownEquals());
        }

        private NumericUpDownEquals ViewModel
        {
            get { return this.GetRootVisualElement() as NumericUpDownEquals; }
        }

        //Create a new dynamic NumericUpDown to displays on the form
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Click the buttons...";

            this.ViewModel.Controls.Add(this.ViewModel.TestedNumericUpDown);
            this.ViewModel.TestedNumericUpDown.Top = 490;
            this.ViewModel.TestedNumericUpDown.Left = 80;
        }

        //Clicking the button return True if TestedNumericUpDown is equal to NumericUpDown1
        public void btnInvokeEquals_Click(object sender, EventArgs e)
        {
            NumericUpDownElement NumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "TestedNumericUpDown.Equals(NumericUpDown1) was invoked.\\r\\nReturn value: " + this.ViewModel.TestedNumericUpDown.Equals(NumericUpDown1) + ".";
            
        }

        //NumericUpDown1 is assign to TestedNumericUpDown 
        public void btnSetToNumericUpDown1_Click(object sender, EventArgs e)
        {
            NumericUpDownElement NumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            this.ViewModel.TestedNumericUpDown = NumericUpDown1;

            lblLog1.Text = "TestedNumericUpDown was set to NumericUpDown1.";
        }

        //NumericUpDown2 is assign to TestedNumericUpDown 
        public void btnSetToNumericUpDown2_Click(object sender, EventArgs e)
        {
            NumericUpDownElement NumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("NumericUpDown2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            this.ViewModel.TestedNumericUpDown = NumericUpDown2;

            lblLog1.Text = "TestedNumericUpDown was set to NumericUpDown2.";
        }

    }

}


