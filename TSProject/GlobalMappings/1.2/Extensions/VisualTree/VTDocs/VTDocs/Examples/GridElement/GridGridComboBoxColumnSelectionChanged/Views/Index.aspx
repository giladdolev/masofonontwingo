﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="GridComboBoxColumn SelectionChanged Event" LoadAction="GridGridComboBoxColumnSelectionChanged\Page_load">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current selection changes.

           Syntax:public event EventHandler SelectionChanged"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can choose any cell and the selectionChanged event wiil be invoked." Top="255px"  ID="Label1"></vt:Label>

        <vt:Grid runat="server" Top="365px" Width="360px" Height="120px" ID="TestedGrid" SelectionChangedAction="GridGridComboBoxColumnSelectionChanged\TestedGrid_SelectionChanged">
            <Columns>              
                <vt:GridComboBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="100px"></vt:GridComboBoxColumn>
              
                <vt:GridComboBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="100px"></vt:GridComboBoxColumn>

                <vt:GridComboBoxColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="100px"></vt:GridComboBoxColumn>
            </Columns> 
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="505px" Height="20" Width="400" ID="lblLog1"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="520px" Width="400px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>


</asp:Content>
