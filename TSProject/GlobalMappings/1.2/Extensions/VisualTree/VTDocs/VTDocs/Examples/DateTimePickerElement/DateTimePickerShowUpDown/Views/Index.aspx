<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="DateTimePicker ShowUpDown Property" Width="770px" Height="750px" ID="windowView1" LoadAction="DateTimePickerShowUpDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ShowUpDown" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether a spin button control (also known as an up-down control) is used
             to adjust the date/time value.
            
           Syntex: public bool ShowUpDown { get; set; }
           Property value: 'true' if a spin button control is used to adjust the date/time value;
             otherwise, 'false'. The default is 'false'.

            Remarks: When the ShowUpDown property is set to true, a spin button control (also known as an up-down
             control), rather than a drop-down calendar, is used to adjust time values. The date and time can be adjusted
             by selecting each element individually and using the up and down buttons to change the value." Top="75px"  ID="lblDefinition"></vt:Label>
      
       <!-- Tested DateTimePicker1 -->
        <vt:DateTimePicker runat="server" Text="DateTimePicker" Left="80px" Height="20px" Width="200px" Top="280px" ID="TestedDateTimePicker1"></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Top="315px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="380px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ShowUpDown property of this DateTimePicker is initially set to 'true'" Top="430px"  ID="lblExp1"></vt:Label>     
        
        <!-- Tested DateTimePicker2 -->
        <vt:DateTimePicker runat="server"  Text="TestedDateTimePicker" CssClass="vt-TestedDateTimePicker" Left ="80px" Height="20px" Width="200px" ShowUpDown="true" Top="480px" ID="TestedDateTimePicker2" ></vt:DateTimePicker>

        <vt:Label runat="server" SkinID="Log" Height="50px" Top="515px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ShowUpDown value >>" Top="610px" ID="btnChangeShowUpDown" Width="200px" ClickAction="DateTimePickerShowUpDown\btnChangeShowUpDown_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Format value >>" Top="610px" Left="380px" ID="btnChangeFormat" Width="200px" ClickAction="DateTimePickerShowUpDown\btnChangeFormat_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
