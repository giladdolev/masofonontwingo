<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar ClientRectangle Property" ID="windowView" LoadAction="ProgressBarClientRectangle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientRectangle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets the rectangle that represents the client area of the control.

            Syntax: public Rectangle ClientRectangle { get; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="lblDefinition" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ClientRectangle values of this 'TestedProgressBar' is initially set with 
            Left: 80px, Top: 320px, Width: 250px, Height: 40px" Top="260px"  ID="lblExp1"></vt:Label>     

        <!-- TestedProgressBar -->
        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="320px" ID="TestedProgressBar"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" Top="375px" Height="40" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientRectangle value >>"  Top="480px" Width="185px" ID="btnChangeProgressBarClientRectangle" ClickAction="ProgressBarClientRectangle\btnChangeProgressBarClientRectangle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
