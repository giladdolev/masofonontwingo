using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonPerformControlRemovedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlRemoved_Click(object sender, EventArgs e)
        {
            
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");

            ControlEventArgs args = new ControlEventArgs(btnTestedButton);

            btnTestedButton.PerformControlRemoved(args);
        }

        public void btnTestedButton_ControlRemoved(object sender, EventArgs e)
        {
            MessageBox.Show("TestedButton ControlRemoved event method is invoked");
        }

    }
}