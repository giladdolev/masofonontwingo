<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="MaskedTextBox PromptChar Property" ID="windowView2" Height="755" LoadAction="MaskedTextBoxPromptChar\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PromptChar" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the character used to represent the absence of user input in MaskedTextBoxElement.

            Syntax: public char PromptChar { get; set; }
            
            Property Value: The character used to prompt the user for input. The default is an underscore (_).
            PromptChar will be displayed in MaskedTextBox for any mask position that the user has not yet filled in."
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" Text="Mask=##" Top="195px" Left="80px" ID="Label1"></vt:Label>
         <vt:MaskedTextBox runat="server" Top="215px"  Width="150px" Mask="##"  ID="MaskDefaultPrompt"></vt:MaskedTextBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog1" Height="40px" Top="250px"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="310px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example contains 3 MaskedTextBoxElements each with different length of mask.
             All three initially set to PromptChar = %. Use the buttons to change PromptChar at runtime 
            and also to change Text for all MaskedTextBoxElements"
            Top="350px" ID="lblExp2"></vt:Label>

        <vt:GroupBox runat="server" Top="430px" Height="75px" Width="535px">
            <vt:Label runat="server" Text="Mask=#" Top="10px" Left="15px" ID="lblMask1"></vt:Label>
            <vt:MaskedTextBox runat="server" Top="35px" Left="15px" Width="150px" Mask="#" PromptChar="%" CssClass="vt-Msk1" ID="Mask1" TabIndex="1" TextChangedAction="MaskedTextBoxPromptChar\Mask1_TextChanged" ></vt:MaskedTextBox>

             <vt:Label runat="server" Text="Mask=##" Top="10px" Left="175px" ID="lblMask2"></vt:Label>
            <vt:MaskedTextBox runat="server" Top="35px" Left="175px" Width="150px" Mask="##" PromptChar="%" CssClass="vt-Msk2" ID="Mask2" TabIndex="2" TextChangedAction="MaskedTextBoxPromptChar\Mask2_TextChanged"></vt:MaskedTextBox>

            <vt:Label runat="server" Text="Mask=##-AA" Top="10px" Left="335px" ID="lblMask3"></vt:Label>
            <vt:MaskedTextBox runat="server" Top="35px" Left="335px" Width="150px" Mask="##-AA" PromptChar="%" ID="Mask3" CssClass="vt-Msk3" TabIndex="3" TextChangedAction="MaskedTextBoxPromptChar\Mask3_TextChanged" ></vt:MaskedTextBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="535px" ID="lblLog2" Height="50px" Top="520px"></vt:Label>

        <vt:Button runat="server"  Text="Change PromptChar for all >>" Top="605px"  Width="200px"  ID="btnChangePromptChar" ClickAction="MaskedTextBoxPromptChar\btnChangePromptChar_Click"></vt:Button>

        <vt:Button runat="server"  Text="Set Mask1 Text >>" Top="670px" Left="50px"  Width="200px"  ID="btnSetMask1Text" ClickAction="MaskedTextBoxPromptChar\btnSetMask1Text_Click"></vt:Button>

        <vt:Button runat="server"  Text="Set Mask2 Text >>" Top="670px" Left="270px" Width="200px" ID="btnSetMask2Text" ClickAction="MaskedTextBoxPromptChar\btnSetMask2Text_Click"></vt:Button>

        <vt:Button runat="server"  Text="Set Mask3 Text >>" Top="670px"  Left="490px" Width="200px" ID="btnSetMask3Text" ClickAction="MaskedTextBoxPromptChar\btnSetMask3Text_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
