<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox MouseHover event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Hover over the checkBox below" Top="110px" Left="160px" ID="label1" Height="13px" Width="350px" Font-Bold="true"></vt:Label>
        <vt:CheckBox runat="server" CheckAlign="MiddleRight" Text="Hover me" Top="150px" Left="160px" ID="chkMouseHover" Height="17px" Width="80px" MouseHoverAction="CheckBoxMouseHover\btn_MouseHover"></vt:CheckBox>
    </vt:WindowView>
</asp:Content>
