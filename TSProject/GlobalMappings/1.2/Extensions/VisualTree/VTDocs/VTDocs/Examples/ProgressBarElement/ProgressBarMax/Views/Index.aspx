
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Max Method" ID="windowView1" LoadAction="ProgressBarMax\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Max" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets the maximum value of the range of the control.

            Syntax: public int Max { get; set; }
            Value: The maximum value of the range. The default is 100.
            
            This property specifies the upper limit of the Value property. When the value of the Maximum property 
            is changed, the ProgressBar control is redrawn to reflect the new range of the control. When the value 
            of the Value property is equal to the value of the Maximum property, the progress bar is completely filled."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:ProgressBar runat="server" Text="ProgressBar" Top="250px" ID="TestedProgressBar1"></vt:ProgressBar>
        
        <vt:Label runat="server" SkinID="Log" Top="300px" Width="380px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the ProgressBar 'Max' property is set to 1000; 'Value' property is set to 100." Top="415px" ID="lblExp2">
        </vt:Label>

        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="460px" Max="1000" Value="100" ID="TestedProgressBar"></vt:ProgressBar>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="510px" Width="380px" ></vt:Label>

        <vt:Button runat="server" Text="Change Max value >>" Top="580px" ID="btnMax" ClickAction="ProgressBarMax\btnChangeMax_Click"></vt:Button>

        <vt:Button runat="server" Text="Change Value >>" Left="300" Top="580px" ID="btnValue" ClickAction="ProgressBarMax\btnChangeValue_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
