using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPasswordCharController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangePasswordChar_Click(object sender, EventArgs e)
        {
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

           
            if (testedTextBox.PasswordChar == "a")
            {
                testedTextBox.PasswordChar = "b";
                textBox1.Text = "PasswordChar value:\\r\\n" + testedTextBox.PasswordChar;
            }
            else
            {
                testedTextBox.PasswordChar = "a";
                textBox1.Text = "PasswordChar value:\\r\\n" + testedTextBox.PasswordChar;
            }
        }
      
    }
}