<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox RightToLeft Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Initialize - RichTextBox RightToLeft is set to'yes'" Top="30px" Left="140px" ID="Label1" BorderStyle="None"  Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

     
        <vt:RichTextBox runat="server"  Text="Initialize RightToLeft" RightToLeft="Yes" Value ="10" Top="45px" Left="140px" ID="rdoRightToLeft" Height="70px" TabIndex="2" Width="220px"></vt:RichTextBox>
      

         <vt:Label runat="server" Text="RunTime" Top="150px" Left="140px" ID="Label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>
     
        <vt:RichTextBox runat="server"  Text="Runtime RightToLeft" Top="165px" Value ="10" Left="140px" ID="testedRichTextBox" Height="70px"  TabIndex="3" Width="220px"></vt:RichTextBox>

         <vt:button runat="server"  Text="Change RichTextBox RightToLeft" Top="185px" Left="480px" ID="btnChangeRightToLeft" Height="36px"  TabIndex="3" Width="200px" ClickAction="RichTextBoxRightToLeft\btnChangeRightToLeft_Click" ></vt:button>

        <vt:TextBox runat="server" Text="" Top="280px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
