﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Transposition.Extensions;

namespace MvcApplication9.Controllers
{
    public class Employee : ModelBase
    {
        private int _ID;
        public int ID
        {
            get { return _ID; }
            set
            {
                if (_ID != value)
                {
                    _ID = value;
                    PropertyChanged("ID");
                }
            }
        }
        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                if (_FirstName != value)
                {
                    _FirstName = value;
                    PropertyChanged("FirstName");
                }
            }
        }

        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set
            {
                if (_LastName != value)
                {
                    _LastName = value;
                    PropertyChanged("LastName");
                }
            }
        }

        private string _Options;
        public string Options
        {
            get { return _Options; }
            set
            {
                if (_Options != value)
                {
                    _Options = value;
                    PropertyChanged("Options");
                }
            }
        }


    }
}