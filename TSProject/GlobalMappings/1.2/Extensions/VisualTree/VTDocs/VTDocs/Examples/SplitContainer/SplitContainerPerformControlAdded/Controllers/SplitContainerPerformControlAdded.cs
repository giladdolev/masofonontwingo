using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPerformControlAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlAdded_Click(object sender, EventArgs e)
        {
            
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");

            ControlEventArgs args = new ControlEventArgs(testedSplitContainer);

            testedSplitContainer.PerformControlAdded(args);
        }

        public void testedSplitContainer_ControlAdded(object sender, EventArgs e)
        {
            MessageBox.Show("TestedSplitContainer ControlAdded event method is invoked");
        }

    }
}