using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("Item2");
            TestedComboBox.Items.Add("Item3");
            lblLog.Text = "Height value: " + TestedComboBox.Height + '.';
        }

        private void btnChangeHeight_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedComboBox.Height == 80)
            {
                TestedComboBox.Height = 25;
                lblLog.Text = "Height value: " + TestedComboBox.Height + '.';
            }
            else
            {
                TestedComboBox.Height = 80;
                lblLog.Text = "Height value: " + TestedComboBox.Height + '.';
            }
        }

    }
}