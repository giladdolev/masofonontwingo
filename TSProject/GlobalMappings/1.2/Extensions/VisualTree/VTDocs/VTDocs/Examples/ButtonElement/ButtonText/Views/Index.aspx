
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Text Property" ID="windowView2" LoadAction="ButtonText/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with this control.
            
            Syntax: public override string Text { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

 
 


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Text property of the button is initially set to 'TestedButton'" Top="260px"  ID="lblExp1"></vt:Label>     

        
        <vt:Button runat="server" SkinID="Wide"  Text="TestedButton" Top="330px" ID="btnTestedButton"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="390px" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Button Text >>"  Top="470px" Width="180px" ID="btnChangeButtonSize" ClickAction="ButtonText\btnChangeButtonText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>