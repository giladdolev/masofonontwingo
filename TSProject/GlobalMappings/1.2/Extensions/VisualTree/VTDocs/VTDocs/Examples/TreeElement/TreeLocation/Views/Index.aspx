<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree Location Property" ID="windowView" LoadAction="TreeLocation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Location" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the coordinates of the upper-left corner of the control 
            relative to the upper-left corner of its container.

            Syntax: public Point Location { get; set; }" Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The Location of this Tree is initially set with Left: 80px and Top: 300px" 
            Top="250px"  ID="lblExp1"></vt:Label>     

         <!-- TestedTree -->
        <vt:Tree runat="server" Text="TestedTree" Top="300px" ID="TestedTree">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="425px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Tree Location >>"  Top="490px" Width="180px" ID="btnChangeTreeLocation" ClickAction="TreeLocation\btnChangeTreeLocation_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>
