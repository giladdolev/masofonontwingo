using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabSelectTabController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //index example
        public void ChangeTab1_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tab = this.GetVisualElementById<TabElement>("tabControl1");
            tab.SelectTab(3);
            textBox1.Text = "Tab " + tab.SelectedTab.Text + " is selected";

        }

        //tabPageName example
        public void ChangeTab2_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tab = this.GetVisualElementById<TabElement>("tabControl1");
            tab.SelectTab("tabPage2");
            textBox1.Text = "Tab " + tab.SelectedTab.Text + " is selected";

        }

        //TabItem example
        public void ChangeTab3_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tab = this.GetVisualElementById<TabElement>("tabControl1");
            tab.SelectTab(tab.TabItems[3]);
            textBox1.Text = "Tab " +  tab.SelectedTab.Text + " is selected";
        }
    }
}