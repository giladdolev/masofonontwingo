<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button PerformClick() Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
      
        <vt:Button runat="server" Text="Button1" Top="90px" Left="140px" ID="button1" Height="36px"  Width="100px"></vt:Button>     
        
        <vt:Button runat="server" Text="Button2" Top="90px" Left="100px" ID="button2" Height="36px"  Width="100px"></vt:Button>  
        
        <vt:Button runat="server" Text="Bring button1 To Fromt" Top="70px" Left="300px" ID="btnBringButton1ToFromt" Height="36px"  Width="200px" ClickAction="ButtonBringToFront\btnBringButton1ToFront_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Bring button2 To Fromt" Top="112px" Left="300px" ID="btnBringButton2ToFromt" Height="36px"  Width="200px" ClickAction="ButtonBringToFront\btnBringButton2ToFront_Click"></vt:Button>        

        <vt:TextBox runat="server" Text="" Top="200px" Left="100px" ID="textBox1" Height="36px" Width="140px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
