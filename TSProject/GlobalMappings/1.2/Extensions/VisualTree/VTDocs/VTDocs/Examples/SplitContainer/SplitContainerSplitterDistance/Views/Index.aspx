<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer SplitterDistance Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="SplitterDistance is initially set to 200" Top="30px" Left="128px" ID="label1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Font-Bold="true" Height="13px" TabIndex="4" Width="56px">
		</vt:Label>
        
        <vt:SplitContainer runat="server" SplitterDistance="200" Top="80px" Left="260px" ID="splitContainer1" BorderStyle="Solid" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="173px" TabIndex="5" Width="299px">

			<Panel1 RoundedCorners="False" FloodShowPct="False" Collapsed="False" Collapsible="False"  AutoSize="True" ClientSize="0, 0" Font="Microsoft Sans Serif, 8.25pt" AllowDrop="False" Bounds="0, 0, 0, 0" AllowColumnReorder="False" CausesValidation="False" ScaleHeight="0" HelpContextID="0" Dock="Left" ForeColor="" BackColor="" Width="" Height="" Size="0, 0" Top="" Left="" Visible="True" Enabled="True">
			</Panel1>

			<Panel2 RoundedCorners="False" FloodShowPct="False" Collapsed="False" Collapsible="False"  AutoSize="True" ClientSize="0, 0" Font="Microsoft Sans Serif, 8.25pt" AllowDrop="False" Bounds="0, 0, 0, 0" AllowColumnReorder="False" CausesValidation="False" ScaleHeight="0" HelpContextID="0" Dock="Fill" ForeColor="" BackColor="" Width="" Height="" Size="0, 0" Top="" Left="" Visible="True" Enabled="True">
			</Panel2>
		</vt:SplitContainer>

        <vt:Button runat="server" Text="Change SplitterDistance" Top="120px" Left="20px" ID="btnChangeSplitterDistance" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="SplitContainerSplitterDistance\btnChangeSplitterDistance_Click"></vt:Button>

        <vt:Button runat="server" Text="Get SplitterDistance" Top="165px" Left="20px" ID="btnGetSplitterDistance" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="SplitContainerSplitterDistance\btnGetSplitterDistance_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
