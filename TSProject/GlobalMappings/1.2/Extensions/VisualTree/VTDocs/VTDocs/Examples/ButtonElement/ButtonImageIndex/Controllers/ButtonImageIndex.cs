using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonImageIndexController : Controller
    {
        public static ImageList imglst = new ImageList();
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void fillimglst_btnclick(object sender, EventArgs e)
        {
            imglst.Images.Add("0", new UrlReference("/Content/Elements/Button.png"));
            imglst.Images.Add("0", new UrlReference("/Content/Elements/galilcs.png"));
            imglst.Images.Add("0", new UrlReference("/Content/Elements/gizmox.png"));
        }
        private void btnChangeImage_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeImage = this.GetVisualElementById<ButtonElement>("btnChangeImage");

            if (btnChangeImage.BackgroundImage == null)
            {
                btnChangeImage.BackgroundImage = imglst.Images[0];
            }
        }
      
    }
}