<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button ImageHeight Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Initialize - button ImageHeight is set to '50'" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:Button runat="server" Text="Initialize Test" ImageHeight = "50px" Image ="~/Content/Elements/Button.png" Top="45px" Left="140px" ID="btnImage" Height="100px" TabIndex="1" Width="300px"></vt:Button>  
          
        <vt:Label runat="server" Text="Runtime - Click the button to change ImageHeight" Top="220px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:Button runat="server" Text="Change ImageHeight" Image ="~/Content/Elements/Button.png" Top="235px" Left="140px" ID="btnChangeImageHeight" Height="100px"  TabIndex="1" Width="300px" ClickAction="buttonImageHeight\btnChangeImageHeight_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="380px" Left="140px" ID="textBox1" Multiline="true"  Height="40px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
