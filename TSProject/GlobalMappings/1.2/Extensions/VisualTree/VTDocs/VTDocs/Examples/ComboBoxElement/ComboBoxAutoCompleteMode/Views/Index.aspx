<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox AutoCompleteMode property" ID="windowView1" LoadAction="ComboBoxAutoCompleteMode/OnLoad">
        <vt:Label runat="server" SkinID="Title" Left="250px" Text="AutoCompleteMode" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets an option that controls how automatic completion works for the ComboBox.
            
            Syntax: public AutoCompleteMode AutoCompleteMode { get; set; }
            Value options: 
            






            You must use the AutoCompleteMode and AutoCompleteSource properties together.
            
            " Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" Text="
            None (default) -
            Append -

            Suggest -
            
            SuggestAppend -
       
            " Top="135px" Left="60px" Font-Italic="true" TextAlign="MiddleRight" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="
             Disables the automatic completion feature for the ComboBox and TextBox controls.
             Appends the remainder of the most likely candidate string to the existing characters, 
            highlighting the appended characters.
             Displays the auxiliary drop-down list associated with the edit control. This drop-down
            is populated with one or more suggested completion strings.
             Applies both Suggest and Append options.
       
            " Top="135px" Left="180px" ID="Label2"></vt:Label>
              
        <vt:ComboBox runat="server" ID="TestedComboBox1" Text="ComboBox" AutoCompleteSource="ListItems" CssClass="vt-test-cmb-1" Top="325px" SelectedIndexChangedAction="ComboBoxAutoCompleteMode\TestedComboBox1_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="January-1"></vt:ListItem>
                <vt:ListItem runat="server" Text="February-2"></vt:ListItem>
                <vt:ListItem runat="server" Text="March-3"></vt:ListItem>
                <vt:ListItem runat="server" Text="April-4"></vt:ListItem>
                 <vt:ListItem runat="server" Text="May-5"></vt:ListItem>
                <vt:ListItem runat="server" Text="June-6"></vt:ListItem>
                <vt:ListItem runat="server" Text="July-7"></vt:ListItem>
                <vt:ListItem runat="server" Text="August-8"></vt:ListItem>
                 <vt:ListItem runat="server" Text="September-9"></vt:ListItem>
                <vt:ListItem runat="server" Text="October-10"></vt:ListItem>
                 <vt:ListItem runat="server" Text="November-11"></vt:ListItem>
                <vt:ListItem runat="server" Text="December-12"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Height="40px" Top="360px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="410px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="AutoCompleteMode property of this ComboBox is initially set to 'Append'.
            You can change the AutoCompleteMode by pressing the 'Change AutoCompleteMode' button." Top="450px"  ID="lblExp1"></vt:Label>     
        
        <vt:ComboBox runat="server" ID="TestedComboBox2" Text="TestedComboBox" AutoCompleteSource="ListItems" AutoCompleteMode ="Append" CssClass="vt-test-cmb-2" Top="520px" SelectedIndexChangedAction="ComboBoxAutoCompleteMode\TestedComboBox2_SelectedIndexChanged">
            <Items>
                <vt:ListItem runat="server" Text="January"></vt:ListItem>
                <vt:ListItem runat="server" Text="February"></vt:ListItem>
                <vt:ListItem runat="server" Text="March"></vt:ListItem>
                <vt:ListItem runat="server" Text="April"></vt:ListItem>
                 <vt:ListItem runat="server" Text="May"></vt:ListItem>
                <vt:ListItem runat="server" Text="June"></vt:ListItem>
                <vt:ListItem runat="server" Text="July"></vt:ListItem>
                <vt:ListItem runat="server" Text="August"></vt:ListItem>
                 <vt:ListItem runat="server" Text="September"></vt:ListItem>
                <vt:ListItem runat="server" Text="October"></vt:ListItem>
                 <vt:ListItem runat="server" Text="November"></vt:ListItem>
                <vt:ListItem runat="server" Text="December"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="555px" Height="40px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change AutoCompleteMode >>" Width="180px" Top="620px" ID="btnChangeAutoCompleteMode" ClickAction="ComboBoxAutoCompleteMode\btnChangeAutoCompleteMode_Click" ></vt:Button>

    </vt:WindowView>
</asp:Content>


<%--<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Auto Complet Mode" ID="windowView1" LoadAction ="ComboBoxAutoCompleteMode/Form_Load">
           <vt:Label runat="server" SkinID="Title" Text="ComboBox AutoCompletMode" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets an option that controls how automatic completion works for the ComboBox
            Syntax : p  public AutoCompleteMode AutoCompleteMode = AutoCompleteMode enum 
            This property works only in initialize .
            " Top="75px"  ID="lblDefinition"></vt:Label>
              
            <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px"  ID="lblExample"></vt:Label>
             <vt:Label runat="server" Text="Try to write one of the option in the combobox its not suggset to you a automatic completion . " Top="190px"  ID="lblExp1"></vt:Label>     

        <vt:ComboBox runat="server" ID="comboBox1" Text="" MatchComboboxWidth="false" DisplayMember="text" ValueMember="value"  Top="250" LostFocusAction="ComboBoxAutoCompleteMode/a"  >
            <Items>
                <vt:ListItem runat="server" Text="��"></vt:ListItem>
                <vt:ListItem runat="server" Text="����"></vt:ListItem>
                <vt:ListItem runat="server" Text="����"></vt:ListItem>
                <vt:ListItem runat="server" Text="��� �"></vt:ListItem>
                 <vt:ListItem runat="server" Text="�'�"></vt:ListItem>
                <vt:ListItem runat="server" Text="(���"></vt:ListItem>
                <vt:ListItem runat="server" Text="����"></vt:ListItem>
                <vt:ListItem runat="server" Text="���"></vt:ListItem>
                 <vt:ListItem runat="server" Text="��"></vt:ListItem>
                <vt:ListItem runat="server" Text="�� - ��"></vt:ListItem>
                 <vt:ListItem runat="server" Text="�� �������������������������������"></vt:ListItem>
                <vt:ListItem runat="server" Text="�� - ��"></vt:ListItem>
                <vt:ListItem runat="server" Text="��-��"></vt:ListItem>
            </Items>
        </vt:ComboBox>

         <vt:Label runat="server" Text="Try to write one of the option in the combobox its suggset to you a automatic completion . " Top="320px"  ID="Label1"></vt:Label>     
         <vt:ComboBox runat="server" ID="comboBox2" Text="" DisplayMember="text" ValueMember="value"  Top="350" AutoCompleteMode="Append" >
            <Items>
                <vt:ListItem runat="server" Text="One"></vt:ListItem>
                <vt:ListItem runat="server" Text="Two"></vt:ListItem>
                <vt:ListItem runat="server" Text="Three"></vt:ListItem>
                <vt:ListItem runat="server" Text="Four"></vt:ListItem>
            </Items>
        </vt:ComboBox>
       
    </vt:WindowView>
</asp:Content>--%>
