using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxSelectedTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnSetSelectedText_Click(object sender, EventArgs e)
        {
            RichTextBox rtfSelectedText = this.GetVisualElementById<RichTextBox>("rtfSelectedText");

            rtfSelectedText.SelectedText = "Hi";
        }
        public void btnGetSelectedText_Click(object sender, EventArgs e)
        {
            RichTextBox rtfSelectedText = this.GetVisualElementById<RichTextBox>("rtfSelectedText");
            if (rtfSelectedText.SelectedText == "")
                MessageBox.Show("No text is selected");
            else
                MessageBox.Show(rtfSelectedText.SelectedText);
        }


    }
}