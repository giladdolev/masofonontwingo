<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel LocationChanged Event" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="Panel Location is initially set to (140, 90)" Top="70px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px"></vt:Label>

        <vt:Panel runat="server" Text="Tested Panel" Top="90px" Left="140px" ID="testedPanel" Height="70px" TabIndex="1" Width="200px"  LocationChangedAction ="PanelLocationChanged\testedPanel_LocationChanged"></vt:Panel>

        <vt:Button runat="server" Text="Change Panel Location" Top="110px" Left="400px" ID="btnChangeLocation" Height="36px" TabIndex="1" Width="300px" ClickAction="PanelLocationChanged\btnChangeLocation_Click"></vt:Button>

         <vt:Label runat="server" Text="Event Log" Top="180px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:TextBox runat="server" Text="" Top="195px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
