using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class Win3Controller : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index3()
        {
            return View(new Win3());
        }


        private Win3 ViewModel
        {
            get { return this.GetRootVisualElement() as Win3; }
        }

       
       

    }
}