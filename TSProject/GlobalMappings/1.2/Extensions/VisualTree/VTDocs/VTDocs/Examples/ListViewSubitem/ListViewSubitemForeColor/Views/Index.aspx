<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListViewSubitem ForeColor property" ID="windowView1" LoadAction="ListViewSubitemForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the ListViewSubitem cell.

            Syntax: public Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested ListView1 -->
        <vt:ListView runat="server" ShowHeader="true" Text="ListView" Top="165px" ID="TestedListView1">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="280px" Width="380px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of ListViewSubitem with text value 'e' is initially set to Green" Top="385px" ID="lblExp1"></vt:Label>

        <!-- Tested ListView2 -->
        <vt:ListView runat="server" ShowHeader="true" Text="TestedListView" Top="420px" ID="TestedListView2">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" ForeColor="Blue" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" ForeColor="Red" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e" ForeColor="Green"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="g">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Top="535px" Width="380px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Tested Subitem e ForeColor value >>" Top="590px" Width="280px" ID="btnChangeListViewSubitemForeColor" ClickAction="ListViewSubitemForeColor\btnChangeListViewSubitemForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
