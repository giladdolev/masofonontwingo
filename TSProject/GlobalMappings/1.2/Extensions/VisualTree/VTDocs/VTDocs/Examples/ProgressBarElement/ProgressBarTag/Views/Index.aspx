
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar Tag Property" ID="windowView1" LoadAction="ProgressBarTag\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Tag" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the object that contains data about the control.
            
            Syntax: public object Tag { get; set; }
            Value: An Object that contains data about the control. The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
         
        <vt:ProgressBar runat="server" Text="ProgressBar" Top="170px" ID="TestedProgressBar1"></vt:ProgressBar>
        <vt:Label runat="server" SkinID="Log" Top="225px" Width="430" ID="lblLog1"></vt:Label>
 
 

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Tag property of this ProgressBar is initially set to the string 'New Tag'." Top="330px"  ID="lblExp1"></vt:Label>     
        
        <vt:ProgressBar runat="server" Text="TestedProgressBar" Top="380px" Tag="New Tag." ID="TestedProgressBar2"></vt:ProgressBar>
        <vt:Label runat="server" SkinID="Log" Top="435px" Width="430" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Tag value >>" Top="490px" ID="btnChangeBackColor" ClickAction="ProgressBarTag\btnChangeTag_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

