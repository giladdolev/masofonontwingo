using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");

            lblLog.Text = "CheckBox Visible value: " + TestedCheckBox.Visible;
        }

        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");

            TestedCheckBox.Visible = !TestedCheckBox.Visible;
        }

        public void TestedCheckBox_VisibleChanged(object sender, EventArgs e)
        {

            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (txtEventLog != null)
                txtEventLog.Text += "\\r\\nCheckBox VisibleChanged event is invoked";
            if (lblLog != null)
                lblLog.Text = "CheckBox Visible value: " + TestedCheckBox.Visible;
        }
    }
}