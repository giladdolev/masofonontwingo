using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxTextLengthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnGetTextLength_Click(object sender, EventArgs e)
        {
            RichTextBox rtfTextLength = this.GetVisualElementById<RichTextBox>("rtfTextLength");

            MessageBox.Show("TextLength value: " + rtfTextLength.TextLength);
            
            
            
        }

    }
}