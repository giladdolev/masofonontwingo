using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            SplitContainer splRuntimeCursor = this.GetVisualElementById<SplitContainer>("splRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (splRuntimeCursor.Cursor == CursorsElement.Default)
            {
                splRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                splRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                splRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                splRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.Help)
            {
                splRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                splRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                splRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.No)
            {
                splRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                splRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                splRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;

            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                splRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                splRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                splRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                splRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                splRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                splRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                splRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                splRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else if (splRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                splRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
            else 
            {
                splRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + splRuntimeCursor.Cursor;
            }
        }

    }
}