﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Text="Grid BeforeContextMenuShow event" ID="windowView" LoadAction="GridBeforeContextMenuShow\OnLoad">
       
        <vt:Label runat="server" SkinID="Title" Text="BeforeContextMenuShow" Left="220px" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs before the context menu is shown.
            
            Syntax: public event CancelEventHandler BeforeContextMenuShow"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, Performing right click on the Grid will display the Custom Context Menu.
            Each invocation of the 'BeforeContextMenuShow' events should change the BackColor of the menu Items,  
            and add a line to the 'Event Log'."
            Top="215px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="300px" ID="TestedGrid" BeforeContextMenuShowAction="GridBeforeContextMenuShow\TestedGrid_BeforeContextMenuShow" ContextMenuStripID="contextMenu">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>      

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="430px" Width="340px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="480px" ID="txtEventLog"></vt:TextBox>

          <vt:ContextMenuStrip runat="server" ID="contextMenu" >
              <vt:ToolBarItem ID="tbIteml" runat="server" Height="25" Text="Item1"></vt:ToolBarItem>
              <vt:ToolBarItem ID="tbItem2" runat="server" Height="25" Text="Item2"></vt:ToolBarItem>
        </vt:ContextMenuStrip>

    </vt:WindowView>

</asp:Content>
