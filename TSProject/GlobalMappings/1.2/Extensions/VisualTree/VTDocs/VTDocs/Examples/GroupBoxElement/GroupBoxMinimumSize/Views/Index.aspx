<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox MinimumSize Property" ID="windowView" LoadAction="GroupBoxMinimumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MinimumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the lower limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MinimumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedGroupBox' is width: 170px and height: 90px. 
            Its MinimumSize is initially set to width: 190px and height: 100px.
            To cancel 'TestedGroupBox' MinimumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label>     

        <vt:GroupBox runat="server" Top="360px" Left="80px" Height="90px" Width="170px" Text="TestedGroupBox" ID="TestedGroupBox" MinimumSize="190, 100"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="495px" ID="lblLog" Width="400px" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Change MinimumSize value >>" Top="580px" Width="200px" ID="btnChangeGrpMinimumSize" ClickAction="GroupBoxMinimumSize\btnChangeGrpMinimumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MinimumSize to (0, 0) >>" Top="580px" width="200px" Left="310px" ID="btnSetToZeroGrpMinimumSize" ClickAction="GroupBoxMinimumSize\btnSetToZeroGrpMinimumSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>









