using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
       
        public void btnChangeText_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement pnlRuntimeText = this.GetVisualElementById<PanelElement>("pnlRuntimeText");

            if (pnlRuntimeText.Text == "RuntimeText") //Get
            {
                pnlRuntimeText.Text = "NewText"; //Set
                textBox1.Text = "The Text property value is: " + pnlRuntimeText.Text;
            }
            else 
            {
                pnlRuntimeText.Text = "RuntimeText"; 
                textBox1.Text = "The Text property value is: " + pnlRuntimeText.Text;
            }
            
        }

    }
}