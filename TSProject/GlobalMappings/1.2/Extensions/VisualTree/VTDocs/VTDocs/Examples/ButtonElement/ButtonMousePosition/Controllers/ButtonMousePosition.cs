using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonMousePositionController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnMousePosition_Click(object sender, EventArgs e)
        {
            ButtonElement btnMousePosition = this.GetVisualElementById<ButtonElement>("btnMousePosition");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            textBox1.Text = "MousePosition value:\\r\\n" +  btnMousePosition.MousePosition + "NOT IMPLEMENTED";
        }

    }
}