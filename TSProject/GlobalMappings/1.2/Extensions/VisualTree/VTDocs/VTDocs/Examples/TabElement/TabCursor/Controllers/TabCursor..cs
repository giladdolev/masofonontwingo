using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            TabElement tabRuntimeCursor = this.GetVisualElementById<TabElement>("tabRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (tabRuntimeCursor.Cursor == CursorsElement.Default)
            {
                tabRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                tabRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                tabRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                tabRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.Help)
            {
                tabRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                tabRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                tabRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.No)
            {
                tabRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                tabRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                tabRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;

            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                tabRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                tabRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                tabRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                tabRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                tabRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                tabRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                tabRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                tabRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else if (tabRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                tabRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
            else 
            {
                tabRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + tabRuntimeCursor.Cursor;
            }
        }

    }
}