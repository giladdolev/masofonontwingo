<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="NumericUpDown Value" ID="windowView1" LoadAction="NumericUpDownValue\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Value" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the value assigned to the spin box (also known as an up-down control). 

            Syntax: public decimal Value { get; set; }

            Property Value: The numeric value of the NumericUpDown control.
                                    
            When the Value property is set, the new value is validated to be between the Minimum
            and Maximum values." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown1" Top="270px" ID="TestedNumericUpDown1" Height="20px" TabIndex="1" Width="200px"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="305px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the value of the TestedNumericUpDown2 is initially set to 20.
            The default maximum value is: 100, default minimum value is: 0. 
            You can enter a value using keyboard or using the up and down buttons or using 'Change value' button. " Top="390px" ID="Label1"></vt:Label>
        
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown2" Top="470px" ID="TestedNumericUpDown2" Height="20px" TabIndex="1" Width="200px" Value="20"
             ValueChangedAction="NumericUpDownValue\TestedNumericUpDown2_ValueChanged" CssClass="vt-TestedNumericUpDown2"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="505px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Value to 50 >>" Top="570px" width="180px" Left="80px" ID="btnChangeValueTo50" ClickAction="NumericUpDownValue\btnChangeValueTo50_Click"></vt:Button>

         <vt:Button runat="server" Text="Change maximum value to 1500 >>" Top="570px" width="220px" Left="280px" ID="btnChangeMaximumValueTo1500" ClickAction="NumericUpDownValue\btnChangeMaximumValueTo1500_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
