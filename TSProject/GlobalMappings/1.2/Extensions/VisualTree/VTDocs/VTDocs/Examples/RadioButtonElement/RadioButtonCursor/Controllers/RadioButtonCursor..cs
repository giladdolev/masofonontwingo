using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoRuntimeCursor = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (rdoRuntimeCursor.Cursor == CursorsElement.Default)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.Help)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.No)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;

            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else if (rdoRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                rdoRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
            else 
            {
                rdoRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + rdoRuntimeCursor.Cursor;
            }
        }

    }
}