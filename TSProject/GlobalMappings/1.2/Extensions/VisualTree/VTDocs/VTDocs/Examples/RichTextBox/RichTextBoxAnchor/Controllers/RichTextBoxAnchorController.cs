using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RichTextBox rtfRunTimeAnchor = this.GetVisualElementById<RichTextBox>("rtfRunTimeAnchor");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");


            if (rtfRunTimeAnchor.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                rtfRunTimeAnchor.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                panel2.Size = new Size(400, 171);
                textBox1.Text = "AnchorStyles.Top, ";
                textBox1.Text += "\\r\\nPanel Size: " +  panel2.Size;

            }

            else if (rtfRunTimeAnchor.Anchor == AnchorStyles.Top)
            {
                rtfRunTimeAnchor.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                panel2.Size = new Size(315, 190);
                textBox1.Text = "AnchorStyles.Left, ";
                textBox1.Text += "\\r\\nPanel Size: " + panel2.Size;

            }
            else if (rtfRunTimeAnchor.Anchor == AnchorStyles.Left)
            {
                rtfRunTimeAnchor.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                panel2.Size = new Size(200, 190);
                textBox1.Text = "AnchorStyles.Right, ";
                textBox1.Text += "\\r\\nPanel Size: " + panel2.Size;

            }
            else if (rtfRunTimeAnchor.Anchor == AnchorStyles.Right)
            {
                rtfRunTimeAnchor.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                panel2.Size = new Size(200, 160);
                textBox1.Text = "AnchorStyles.Bottom, ";
                textBox1.Text += "\\r\\nPanel Size: " + panel2.Size;

            }
            else if (rtfRunTimeAnchor.Anchor == AnchorStyles.Bottom)
            {
                rtfRunTimeAnchor.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the panel size
                rtfRunTimeAnchor.Location = new Point(75, 50); // set the  back the RichTextBox location
                textBox1.Text = "AnchorStyles.None, ";
                textBox1.Text += "\\r\\nPanel Size: " + panel2.Size;

            }
            else if (rtfRunTimeAnchor.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and btnChangeAnchor settings
                rtfRunTimeAnchor.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel
                panel2.Size = new Size(300, 200);//Set  back the panel size
                textBox1.Text = "AnchorStyles.Left | AnchorStyles.Top, ";
                textBox1.Text += "\\r\\nPanel Size: " + panel2.Size;

            }       
        }

    }
}