using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabDeselectTabIntController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Current tab selected index is: " + TestedTab.SelectedTab.Index;

        }
 
        public void btnDeselectTab_Click(object sender, EventArgs e)
        {
            TabElement TestedTab = this.GetVisualElementById<TabElement>("TestedTab");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

         //   TestedTab.DeselectTab(TestedTab.SelectedTab.TabIndex);
            int val;

            if(int.TryParse(txtInput.Text, out val))
            {
                try
                {
                    TestedTab.DeselectTab(val);
                    lblLog.Text = "After Deselect(" + txtInput.Text + ")" + ", Current index Selected is " + TestedTab.SelectedIndex;
                }
                catch (ArgumentException ex)
                {
                    lblLog.Text = ex.GetType().ToString() + " was thrown\\r\\nEnter one of the tab indexes";
                }
            }
            else
            {
                lblLog.Text = "Number is required - Enter one of the tab indexes";
            }

        }

        public void txtInput_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");

            lblLog.Text = "Input index: " + txtInput.Text;
        }

        public void txtInput_GotFocus(object sender, EventArgs e)
        {
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            txtInput.Text = "";
        }


        ////tabPageName example
        //public void ChangeTab2_Click(object sender, EventArgs e)
        //{
        //    TabElement tab = this.GetVisualElementById<TabElement>("tabControl1");
        //    tab.DeselectTab(tab.SelectedTab.ID);
        //}

        ////tabPage example
        //public void ChangeTab3_Click(object sender, EventArgs e)
        //{
        //    TabElement tab = this.GetVisualElementById<TabElement>("tabControl1");
        //    tab.DeselectTab(tab.SelectedTab);
        //}
    }
}