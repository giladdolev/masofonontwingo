using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        private void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedSplitContainer1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedSplitContainer1);
            lblLog2.Text = "Font value: " + TestedSplitContainer2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedSplitContainer2);
        }

        private void btnChangeSplitContainerFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            if (TestedSplitContainer2.Font.Underline == true)
            {
                TestedSplitContainer2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedSplitContainer2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedSplitContainer2);
            }
            else if (TestedSplitContainer2.Font.Bold == true)
            {
                TestedSplitContainer2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedSplitContainer2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedSplitContainer2);
            }
            else if (TestedSplitContainer2.Font.Italic == true)
            {
                TestedSplitContainer2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedSplitContainer2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedSplitContainer2);
            }
            else
            {
                TestedSplitContainer2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedSplitContainer2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedSplitContainer2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}