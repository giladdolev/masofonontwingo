using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SizeGripStyleTestedWindowController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index2()
        {
            return View(new SizeGripStyleTestedWindow());
        }


        private SizeGripStyleTestedWindow ViewModel
        {
            get { return this.GetRootVisualElement() as SizeGripStyleTestedWindow; }
        }
        public void OnLoad(object sender, EventArgs e)
        {
            this.ViewModel.Top = 500;
            this.ViewModel.Left = 350;
        }

        public void btnChangeSizeGripStyle_Click(object sender, EventArgs e)
        {

            if (this.ViewModel != null)
            {
                TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

                this.ViewModel.Top = 500;
                this.ViewModel.Left = 350;

                if (this.ViewModel.SizeGripStyle == SizeGripStyle.Auto)
                {
                    this.ViewModel.SizeGripStyle = SizeGripStyle.Hide;
                    textBox1.Text = "SizeGripStyle = " + this.ViewModel.SizeGripStyle.ToString();
                }

                if (this.ViewModel.SizeGripStyle == SizeGripStyle.Hide)
                {
                    this.ViewModel.SizeGripStyle = SizeGripStyle.Show;
                    textBox1.Text = "SizeGripStyle = " + this.ViewModel.SizeGripStyle.ToString();
                }
                else
                {
                    this.ViewModel.SizeGripStyle = SizeGripStyle.Auto;
                    textBox1.Text = "SizeGripStyle = " + this.ViewModel.SizeGripStyle.ToString();
                }
            }

        }
       

    }
}