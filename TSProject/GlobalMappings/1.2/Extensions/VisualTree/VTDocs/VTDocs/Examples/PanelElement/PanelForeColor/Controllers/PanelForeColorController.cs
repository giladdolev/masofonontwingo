using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedPanel1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedPanel2.ForeColor + '.';
        }

        public void btnChangePanelForeColor_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedPanel2.ForeColor == Color.Green)
            {
                TestedPanel2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedPanel2.ForeColor + '.';
            }
            else
            {
                TestedPanel2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedPanel2.ForeColor + '.';
            }
        }

    }
}