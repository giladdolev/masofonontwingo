using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxCreateControlController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        
        public ActionResult Index()
        {
            return View(new TextBoxCreateControl());

        }

        private TextBoxCreateControl ViewModel
        {
            get { return this.GetRootVisualElement() as TextBoxCreateControl; }
        }
        public void btnCreateControl_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TextBoxElement newTextBox = new TextBoxElement();

            newTextBox.CreateControl();
            //newTextBox.Width = 100;
            //newTextBox.Height = 20;
            //newTextBox.Top = ;


            this.ViewModel.Controls.Add(newTextBox);
            

            textBox1.Text = "CreateControl() is invoked";
            
                
		}

    }
}