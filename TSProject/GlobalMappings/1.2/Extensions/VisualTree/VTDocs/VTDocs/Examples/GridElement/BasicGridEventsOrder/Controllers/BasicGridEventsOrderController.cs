﻿using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class BasicGridEventsOrderController : Controller
    {
        public ActionResult Index()
        {
            return this.View(new BasicGridEventsOrder());
        }

        private BasicGridEventsOrder ViewModel
        {
            get { return this.GetRootVisualElement() as BasicGridEventsOrder; }
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            gridElement.AddToComboBoxList("GridCol3", "male,female");
            gridElement.Rows.Add("a", "false", "male");
            gridElement.Rows.Add("b", "true", "female");
            gridElement.Rows.Add("c", "false", "male");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_BeforeSelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid BeforeSelectionChanged event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid SelectionChanged event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }
        public void TestedGrid_CellClick(object sender, GridElementCellEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid CellClick event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_RowClick(object sender, GridRowClickEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid RowClick event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_CurrentCellChanged(object sender, GridElementCellEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid CurrentCellChanged event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_CellBeginEdit(object sender, GridElementCellEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid CellBeginEdit event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_CellEndEdit(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid CellEndEdit event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_CellMouseDown(object sender, GridCellMouseEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid CellMouseDown event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_CellValueChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid CellValueChanged event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void TestedGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\n" + ViewModel.counterProperty++ + ": Grid CurrentCellDirtyStateChanged event is invoked";

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid Current Cell Index : " + gridElement.CurrentCell.RowIndex + " , " + gridElement.CurrentCell.ColumnIndex;
        }

        public void btnClear_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();
            txtEventLog.Text = "Event Log:";
            ViewModel.counterProperty = 0;
        }
    }
}
