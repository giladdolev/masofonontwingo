using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelTabIndexTabStopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnChangeTabIndex_Click(object sender, EventArgs e)
        {
            PanelElement btnChangeTabIndex = this.GetVisualElementById<PanelElement>("btnChangeTabIndex");
            PanelElement Panel1 = this.GetVisualElementById<PanelElement>("Panel1");
            PanelElement Panel2 = this.GetVisualElementById<PanelElement>("Panel2");
            PanelElement Panel4 = this.GetVisualElementById<PanelElement>("Panel4");
            PanelElement Panel5 = this.GetVisualElementById<PanelElement>("Panel5");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (Panel1.TabIndex == 1)
            {
                Panel5.TabIndex = 1;
                Panel4.TabIndex = 2;
                Panel2.TabIndex = 4;
                Panel1.TabIndex = 5;

                textBox1.Text = "\\r\\nPanel1 TabIndex: " + Panel1.TabIndex + "\\r\\nPanel2 TabIndex: " + Panel2.TabIndex + "\\r\\nPanel4 TabIndex: " + Panel4.TabIndex + "\\r\\nPanel5 TabIndex: " + Panel5.TabIndex;       

            }
            else
            { 
                Panel1.TabIndex = 1;
                Panel2.TabIndex = 2;
                Panel4.TabIndex = 4;
                Panel5.TabIndex = 5;

                textBox1.Text = "\\r\\nPanel1 TabIndex: " + Panel1.TabIndex + "\\r\\nPanel2 TabIndex: " + Panel2.TabIndex + "\\r\\nPanel4 TabIndex: " + Panel4.TabIndex + "\\r\\nPanel5 TabIndex: " + Panel5.TabIndex;
            }
        }
        private void btnChangeTabStop_Click(object sender, EventArgs e)
        {
            PanelElement btnChangeTabStop = this.GetVisualElementById<PanelElement>("btnChangeTabStop");
            PanelElement Panel3 = this.GetVisualElementById<PanelElement>("Panel3");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");


            if (Panel3.TabStop == false)
            {
                Panel3.TabStop = true;
                textBox2.Text = "Panel3 TabStop: \\r\\n" + Panel3.TabStop;

            }
            else
            {
                Panel3.TabStop = false;
                textBox2.Text = "Panel3 TabStop: \\r\\n" + Panel3.TabStop;

            }
        }
    }
}