<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox BorderStyle Property" ID="windowView1" LoadAction="GroupBoxBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
       
        <!-- TestedGroupBox1 -->
        <vt:GroupBox runat="server"  Text="GroupBox" Top="160px" ID="TestedGroupBox1"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="255px" ID="lblLog1"></vt:Label>
 
 


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="320px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this TestedGroupBox is initially set to 'Dotted'" Top="365px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedGroupBox2 -->
        <vt:GroupBox runat="server"  Text="TestedGroupBox" BorderStyle="Dotted" Top="415px" ID="TestedGroupBox2"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="575px" ID="btnChangeBorderStyle" ClickAction="GroupBoxBorderStyle\btnChangeBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
        