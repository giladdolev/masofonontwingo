<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Hide Method" ID="windowView1" LoadAction="TabHide\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Hide" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Conceals the control from the user.

            Syntax: public void Hide()
            
            Hiding the control is equivalent to setting the Visible property to 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the Visible property is initially set to 'true'. 
            When invoking Hide() method, the Visible property value is changed to 'false'." Top="290px" ID="lblExp2">
        </vt:Label>

        <vt:Tab runat="server" Text="TestedTab" Top="360px" Visible="true" ID="TestedTab">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="485"></vt:Label>

        <vt:Button runat="server" Text="Hide >>" Top="560px" ID="btnHide" ClickAction="TabHide\btnHide_Click"></vt:Button>

        <vt:Button runat="server" Text="Show >>" Left="300" Top="560px" ID="btnShow" ClickAction="TabHide\btnShow_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
