using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonEqualsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new RadioButtonEquals());
        }

        private RadioButtonEquals ViewModel
        {
            get { return this.GetRootVisualElement() as RadioButtonEquals; }
        }

        public void btnEquals_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement RadioButton1 = this.GetVisualElementById<RadioButtonElement>("RadioButton1");
            RadioButtonElement RadioButton2 = this.GetVisualElementById<RadioButtonElement>("RadioButton2");


            if (this.ViewModel.TestedRadioButton.Equals(RadioButton1))
            {
                this.ViewModel.TestedRadioButton = RadioButton2;
                textBox1.Text = "TestedRadioButton Equals RadioButton2";
            }
            else
            {
                this.ViewModel.TestedRadioButton = RadioButton1;
                textBox1.Text = "TestedRadioButton Equals RadioButton1";
            }

            
        }
        

        public void Load(object sender, EventArgs e)
        {
            this.ViewModel.Controls.Add(this.ViewModel.TestedRadioButton);
        }



    }
}