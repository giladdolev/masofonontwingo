﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown GetKeyPressMasks Method" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
      
        <vt:NumericUpDown runat="server" Text="Tested NumericUpDown" Top="170px" Left="140px" ID="testedNumericUpDown" Height="20px" Width="100px"></vt:NumericUpDown>

        <vt:Button runat="server" Text="Invoke GetKeyPressMasks()" Top="45px" Left="140px" ID="btnInvokeGetKeyPressMasks" Height="36px" Width="220px" ClickAction="NumericUpDownGetKeyPressMasks\btnGetKeyPressMasks_Click"></vt:Button>

         <vt:TextBox runat="server" Text="" Top="250px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="300px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
