<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown LostFocus event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

         <vt:Label runat="server" Text="Change Focus of the NumericUpDown by using  keyboard or mouse to invoke LostFocus event method" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="50px" Left="200px" ID="nudLostFocus" Height="20px"  Width="100px" LostFocusAction ="NumericUpDownLostFocus\nudLostFocus_LostFocus"></vt:NumericUpDown>          


        <vt:Label runat="server" Text="Event Log" Top="130px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:TextBox runat="server" Text="" Top="150px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
