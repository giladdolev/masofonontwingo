<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Enabled property" ID="windowView1" LoadAction="ComboBoxEnabled/OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.

            Syntax: public bool Enabled { get; set; }

            'true' if the control can respond to user interaction; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox"  ID="TestedComboBox1" CssClass="vt-test-cmb-1" Top="190px"> </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="230px" ID="lblLog1"></vt:Label>




        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="310px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Enabled value is initially set to 'false', the ComboBox should not respond to user interaction."
            Top="350px" ID="lblExp2">
        </vt:Label>
         
        <vt:ComboBox runat="server" Text="TestedComboBox"  Enabled="false" CssClass="vt-test-cmb-2" Top="405px" ID="TestedComboBox2"> </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="445px" Width="350px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled value >>" Width="180px" Top="510px" ID="btnChangeEnabled" ClickAction="ComboBoxEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
