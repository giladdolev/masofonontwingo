<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree Leave event" ID="windowView2" LoadAction="TreeLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the listviews,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:Tree runat="server" Top="335px" Height="70px" ID="TestedTree1" CssClass="vt-test-tree-1" LeaveAction="TreeLeave\TestedTree1_Leave">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Tree runat="server" Top="420px" Height="70px" ID="TestedTree2" CssClass="vt-test-tree-2" LeaveAction="TreeLeave\TestedLTree2_Leave">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item2"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="505px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="545px" Width="360px" Height="120px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

