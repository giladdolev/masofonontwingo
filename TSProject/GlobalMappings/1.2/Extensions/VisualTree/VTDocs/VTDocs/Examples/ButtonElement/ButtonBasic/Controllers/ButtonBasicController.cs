using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void button1_Click(object sender, EventArgs e)
        {
            ButtonElement button = this.GetVisualElementById<ButtonElement>("button1");
            button.Font = new System.Drawing.Font(button.Font, button.Font.Style | System.Drawing.FontStyle.Underline | System.Drawing.FontStyle.Italic);
            WindowElement root = this.GetRootVisualElement() as WindowElement;
            root.BackColor = Color.Green;
            
        }
        public void button2_Click(object sender, EventArgs e)
        {
            ButtonElement button = this.GetVisualElementById<ButtonElement>("button2");
            button.ForeColor = Color.Green;

        }
        public void button3_Click(object sender, EventArgs e)
        {
            //WindowElement root = this.GetRootVisualElement() as WindowElement;
            //root.BackColor = Color.Green;
            WindowElement win = new WindowElement();
            win.Show();
        }

        
    }
}