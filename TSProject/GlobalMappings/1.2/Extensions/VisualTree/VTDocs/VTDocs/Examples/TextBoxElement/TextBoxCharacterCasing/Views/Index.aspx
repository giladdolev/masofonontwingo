<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox CharacterCasing Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1"  Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:TextBox runat="server" Text="TextBox CharacterCasing is set to 'Upper'" CharacterCasing="Upper" Top="45px"  Left="140px" ID="txtCharacterCasing" Height="26px" Width="220px"></vt:TextBox>           

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2"  Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:TextBox runat="server" Text="Tested TextBox" Top="125px" Left="140px" ID="txtCharacterCasingChange" Height="26px" Width="120px"></vt:TextBox>

        <vt:Button runat="server" Text="Change TextBox CharacterCasing" Top="115px" Left="320px" ID="btnChangeCharacterCasing" Height="36px" Width="220px" ClickAction="TextBoxCharacterCasing\btnChangeCharacterCasing_Click"></vt:Button>


         <vt:TextBox runat="server" Text="Tested TextBox" Top="180px" Left="140px" ID="TextBox1" Height="50px" Width="200px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
