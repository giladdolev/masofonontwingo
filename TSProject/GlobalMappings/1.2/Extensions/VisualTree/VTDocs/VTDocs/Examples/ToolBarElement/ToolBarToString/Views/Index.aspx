 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar ToString Method" ID="windowView2" LoadAction="ToolBarToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a string that represents the ToolBar control.

            Syntax: public override string ToString()
            
            Return Value: A string that includes the type, the value of the Count property,  
            and the string returned by the ToString method of the toolbar�s first button." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this ToolBar." 
            Top="280px" ID="lblExp2"></vt:Label>

        <vt:ToolBar runat="server" Text="TestedToolBar" Top="345px" Dock="None" ID="TestedToolBar">
            <vt:ToolBarButton runat="server" id="ToolBarItem1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="60px" ID="lblLog" Top="400px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="525px" ID="btnToString" ClickAction="ToolBarToString\ToString_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>