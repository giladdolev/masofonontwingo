using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxPerformLocationChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLocationChanged_Click(object sender, EventArgs e)
        {
            
            CheckBoxElement testedCheckBox = this.GetVisualElementById<CheckBoxElement>("testedCheckBox");
            testedCheckBox.PerformLocationChanged(e);
        }

        public void testedCheckBox_LocationChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TestedCheckBox LocationChanged event method is invoked");
        }

    }
}