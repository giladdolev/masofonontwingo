﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="MaskedTextBox MaximumSize" ID="windowView1" LoadAction="MaskedTextBoxMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedMaskedTextBox' is width: 200px and height: 50px. 
            Its MaximumSize is initially set to width: 150px and height: 30px.
            To cancel 'TestedMaskedTextBox' MaximumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>

        <vt:MaskedTextBox runat="server" ID="TestedMaskedTextBox" Mask="(AAA) - (000) - (999) - (###)" Top="350px" Left="80px" Height="50px" Width="200px" MaximumSize="150, 30"></vt:MaskedTextBox>

        <vt:Label runat="server" SkinID="Log" Top="415px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="500px" width="200px" Left="80px" ID="btnChangeMskMaximumSize" ClickAction="MaskedTextBoxMaximumSize\btnChangeMskMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="500px" width="200px" Left="310px" ID="btnSetToZeroMskMaximumSize" ClickAction="MaskedTextBoxMaximumSize\btnSetToZeroMskMaximumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>






