using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformControlAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlAdded_Click(object sender, EventArgs e)
        {
            
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");

            ControlEventArgs args = new ControlEventArgs(testedTextBox);

            testedTextBox.PerformControlAdded(args);
        }

        public void testedTextBox_ControlAdded(object sender, EventArgs e)
        {
            MessageBox.Show("TestedTextBox ControlAdded event method is invoked");
        }

    }
}