<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Left Property" ID="windowView1" LoadAction="ComboBoxLeft\OnLoad">          

        <vt:Label runat="server" SkinID="Title" Text="Left" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the left edge of the control and the left edge 
            of its container's client area.
            
            Syntax: public int Left { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Left property of this ComboBox is initially set to: 80px" Top="250px" ID="lblExp1"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" Top="310px" ID="TestedComboBox"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Left value >>" Top="430px" ID="btnChangeLeft" Width="180px" ClickAction="ComboBoxLeft\btnChangeLeft_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
