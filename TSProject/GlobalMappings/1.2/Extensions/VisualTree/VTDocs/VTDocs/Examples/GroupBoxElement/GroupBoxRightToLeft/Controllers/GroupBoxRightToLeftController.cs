using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpRuntimeRightToLeft = this.GetVisualElementById<GroupBoxElement>("grpRuntimeRightToLeft");

            if (grpRuntimeRightToLeft.RightToLeft == RightToLeft.Inherit) //Get
            {
                grpRuntimeRightToLeft.RightToLeft = RightToLeft.No; //Set
                textBox1.Text = "The RightToLeft property value is: " + grpRuntimeRightToLeft.RightToLeft;
            }
            else if (grpRuntimeRightToLeft.RightToLeft == RightToLeft.No)
            {
                grpRuntimeRightToLeft.RightToLeft = RightToLeft.Yes; 
                textBox1.Text = "The RightToLeft property value is: " + grpRuntimeRightToLeft.RightToLeft;
            }
            else  
            {
                grpRuntimeRightToLeft.RightToLeft = RightToLeft.Inherit; 
                textBox1.Text = "The RightToLeft property value is: " + grpRuntimeRightToLeft.RightToLeft;
            }
             
        }

    }
}