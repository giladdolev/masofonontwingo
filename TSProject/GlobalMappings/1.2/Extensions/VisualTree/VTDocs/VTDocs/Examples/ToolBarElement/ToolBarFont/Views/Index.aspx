<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Font Property" ID="windowView1" LoadAction="ToolBarFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:ToolBar runat="server" Text="ToolBar" Top="185px" Dock="None" ID="TestedToolBar1">
            <vt:ToolBarButton runat="server" id="ToolBar1Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar1Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar1Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="240px" Width="360px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="320px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this ToolBar is initially set to:
             Font-Names='Niagara Engraved', Font-Italic='true' and Font-Size='50'" Top="360px"  ID="lblExp1"></vt:Label>             
      
        <vt:ToolBar runat="server" Text="TestedToolBar" Dock="None" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="50" Top="415px" ID="TestedToolBar2">
            <vt:ToolBarButton runat="server" id="ToolBar2Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar2Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar2Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="470px" Width="360px" Height="45px" ID="lblLog2"></vt:Label>
       
        <vt:Button runat="server" Text="Change Font value >>" Top="590px" ID="btnChangeToolBarFont" ClickAction="ToolBarFont/btnChangeToolBarFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>