<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Validating event" Top="57px" Left="11px" ID="windowView1" Height="700px" Width="768px">
      

         <vt:Label runat="server" Text="Validating event method is invoked when you change the focus by using mouse or keyboard." Top="30px" Left="110px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45"></vt:Label>

        <vt:Panel runat="server" Text="TestedPanel" Top="50px" Left="200px" ID="pnlValidating" Height="70px"  Width="200px" ValidatingAction ="PanelValidating\pnlValidating_Validating">
            <vt:Button runat="server" Text="button1" Top="30px" Left="40px" ID="button1" Height="26px" Width="100px"></vt:Button>
        </vt:Panel>          


        <vt:Label runat="server" Text="Event Log" Top="160px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>


        <vt:TextBox runat="server" Text="" Top="180px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
