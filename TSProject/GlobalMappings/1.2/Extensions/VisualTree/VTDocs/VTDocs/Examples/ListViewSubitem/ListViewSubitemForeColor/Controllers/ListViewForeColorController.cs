using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewSubitemForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Tested Subitem 'e' ForeColor value: " + TestedListView1.Items[1].Subitems[1].ForeColor;
            lblLog2.Text = "Tested Subitem 'e' ForeColor value: " + TestedListView2.Items[1].Subitems[1].ForeColor + '.';
        }

        public void btnChangeListViewSubitemForeColor_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView2 = this.GetVisualElementById<ListViewElement>("TestedListView2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedListView2.Items[1].Subitems[1].ForeColor == Color.Blue)
            {
                TestedListView2.Items[1].Subitems[1].ForeColor = Color.Red;
                lblLog2.Text = "Tested Subitem 'e' ForeColor value: " + TestedListView2.Items[1].Subitems[1].ForeColor + '.';
            }
            else
            {
                TestedListView2.Items[1].Subitems[1].ForeColor = Color.Blue;
                lblLog2.Text = "Tested Subitem 'e' ForeColor value: " + TestedListView2.Items[1].Subitems[1].ForeColor + '.';
            }
        }
    }
}