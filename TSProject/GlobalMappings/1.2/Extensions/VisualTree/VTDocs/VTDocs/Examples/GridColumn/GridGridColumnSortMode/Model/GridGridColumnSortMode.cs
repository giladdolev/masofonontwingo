﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class GridGridColumnSortMode : WindowElement
    {


        GridColumnSortModeRemarks _frmText;

        public GridGridColumnSortMode()
        {

        }

         public GridColumnSortModeRemarks frmText
        {
            get { return this._frmText; }
            set { this._frmText = value; }
        }

    }
}