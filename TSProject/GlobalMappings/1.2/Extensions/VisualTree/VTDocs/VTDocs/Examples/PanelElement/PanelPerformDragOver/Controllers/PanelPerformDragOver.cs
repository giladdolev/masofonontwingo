using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformDragOverController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformDragOver_Click(object sender, EventArgs e)
        {
            DragEventArgs args = new DragEventArgs();
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");

            testedPanel.PerformDragOver(args);
        }

        public void testedPanel_DragOver(object sender, EventArgs e)
        {
            MessageBox.Show("TestedPanel DragOver event method is invoked");
        }

    }
}