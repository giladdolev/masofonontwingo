<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="6, 13" Text="MDIWindow1" ID="MDIForm1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="262px" Width="777px" BackColor="Yellow">
       <vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Close" Top="10px" Left="145px" ClickAction="MDIWindow1\btnControlBox_Click" ID="btnControlBox" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="51px" TabIndex="30" Width="157px">
       </vt:Button>
	</vt:WindowView>
</asp:Content>
