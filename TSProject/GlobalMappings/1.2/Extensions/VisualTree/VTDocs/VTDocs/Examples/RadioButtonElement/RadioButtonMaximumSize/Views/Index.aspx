<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="RadioButton MaximumSize" ID="windowView1" LoadAction="RadioButtonMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; } 
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="230px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The defined Size of this 'TestedRadioButton' is width: 120px and height: 80px. 
            Its MaximumSize is initially set to width: 100px and height: 70px.
            To cancel 'TestedRadioButton' MaximumSize, set width and height values to 0px." Top="270px" ID="Label3"></vt:Label>
        
       <vt:RadioButton runat="server" SkinID="BackColorRadioButton" Left="80px" Top="350px" Height="120px" Width="80px" Text="TestedRadioButton" ID="TestedRadioButton" MaximumSize="100, 70" BackColor="#f2f2f2"></vt:RadioButton>

        <vt:Label runat="server" SkinID="Log" Top="440px" ID="lblLog1" Height="40" Width="400"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>" Top="525px" width="200px" Left="80px" ID="btnChangeRdbMaximumSize" ClickAction="RadioButtonMaximumSize\btnChangeRdbMaximumSize_Click"></vt:Button>

        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="525px" width="200px" Left="310px" ID="btnSetToZeroRdbMaximumSize" ClickAction="RadioButtonMaximumSize\btnSetToZeroRdbMaximumSize_Click"></vt:Button>


    
    </vt:WindowView>
</asp:Content>
























