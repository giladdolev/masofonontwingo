<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView SelectionChanged event" ID="windowView1" LoadAction="listViewSelectionChanged\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" Left="260px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the Items selection changed.
            The property change can occur programmatically or when the user selects an item or clears the 
            selection of an item. When the user selects an item without pressing CTRL to perform a multiple 
            selection, the control first clears the previous selection. In this case, this event occurs one 
            time for each item that was previously selected and one time for the newly selected item.

            Syntax: public event EventHandler SelectionChanged"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="225px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following ListView you can change the item selection by clicking the 'Change selection' 
            button or by manually clicking on an item. 
            Each invocation of the 'SelectionChanged' event should add a line in the 'Event Log'."
            Top="265px" ID="Label4">
        </vt:Label>
        <vt:ListView runat="server" Text="ListView" Top="340px" CssClass="vt-TestedListView" SelectionChangedAction="ListViewSelectionChanged\TestedListView1_SelectionChanged" ID="TestedListView1">
            <Columns>
                <vt:ColumnHeader runat="server" Text="ColumnHeader1" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader2" width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="ColumnHeader3" width="100"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="Item1">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="b"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="c"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Item2">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="e"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="f"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="Item3">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="h"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="i"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Height="50px" Top="455px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="520px" Width="360px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select Item3 >>" Top="640px" ID="btnSelectItem3" ClickAction="ListViewSelectionChanged\btnSelectItem3_Click"></vt:Button>
        <vt:Button runat="server" Text="Clear Event Log >>" Top="640px" Left="250px" ID="btnClearEventLog" ClickAction="ListViewSelectionChanged\btnClearEventLog_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
