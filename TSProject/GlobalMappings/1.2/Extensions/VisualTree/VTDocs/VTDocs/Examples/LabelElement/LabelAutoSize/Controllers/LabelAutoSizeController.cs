﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelAutoSizeController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Label AutoSize Value: " + TestedLabel1.AutoSize + "\\r\\nLabel Size Value: " + TestedLabel2.Size;
            lblLog2.Text = "Label AutoSize Value: " + TestedLabel2.AutoSize + '.' + "\\r\\nLabel Size Value: " + TestedLabel2.Size;
        }

        private void btnChangeAutoSize_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel2.AutoSize == true)
            {
                TestedLabel2.AutoSize = false;
                lblLog2.Text = "Label AutoSize value: " + TestedLabel2.AutoSize + '.' + "\\r\\nLabel Size Value: " + TestedLabel2.Size;
            }
            else
            {
                TestedLabel2.AutoSize = true;
                lblLog2.Text = "Label AutoSize value: " + TestedLabel2.AutoSize + '.' + "\\r\\nLabel Size Value: " + TestedLabel2.Size;
            }
        }

        private void btnChangeSizeTo80_15_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedLabel2.Size = new Size(80, 15);
            lblLog2.Text = "Label AutoSize value: " + TestedLabel2.AutoSize + '.' + "\\r\\nLabel Size Value: " + TestedLabel2.Size;

        }

        private void btnChangeSizeTo200_30_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedLabel2.Size = new Size(200, 30);
            lblLog2.Text = "Label AutoSize value: " + TestedLabel2.AutoSize + '.' + "\\r\\nLabel Size Value: " + TestedLabel2.Size;
            
        }

    }
}