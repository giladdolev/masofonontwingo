<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel AutoSize Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="PanelAutoSize\Form_Load">
        
        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Text="AutoSize is set to true" AutoSize ="true" Top="55px"  Left="140px" ID="pnlAutoSize" Height="70px" Width="150px">
                        <vt:Button runat="server" Text="button1" Top="10px" Left="10px" ID="button2" Height="26px" Width="250px"></vt:Button>
        </vt:Panel>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Text="Panel AutoSize" Top="165px" Left="140px" ID="pnlRuntimeAutoSize" Height="70px" TabIndex="1" Width="150px" >
                        <vt:Button runat="server" Text="button1" Top="10px" Left="10px" ID="button1" Height="26px" Width="250px"></vt:Button>
        </vt:Panel>

        <vt:Button runat="server" Text="Change Panel AutoSize" Top="195px" Left="420px" ID="btnChangeAutoSize" Height="36px" TabIndex="1" Width="200px" ClickAction="PanelAutoSize\btnChangeAutoSize_Click"></vt:Button>


        <vt:TextBox runat="server" ScrollBars="Both" ReadOnly="True" Multiline="True" Top="255px" Left="100px" ID="txtAutoSizeValue" BackColor="White" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="50px"  Width="300px">
		</vt:TextBox>

    </vt:WindowView>
</asp:Content>
