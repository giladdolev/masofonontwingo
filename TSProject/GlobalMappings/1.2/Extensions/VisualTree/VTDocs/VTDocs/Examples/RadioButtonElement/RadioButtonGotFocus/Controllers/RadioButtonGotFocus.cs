using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonGotFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Choose your gender...";
        }

        public void radioMale_GotFocus(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nRadioButton: Male option, GotFocus event is invoked";
            lblLog.Text = "Male RadioButton option has the Focus";
        }

        public void radioFemale_GotFocus(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nRadioButton: Female option, GotFocus event is invoked";
            lblLog.Text = "Female RadioButton option has the Focus";
        }

        public void btnChangeFocus_GotFocus(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Button has the Focus"; 
        }

        public void btnChangeFocus_Click(object sender, EventArgs e)
        {
            RadioButtonElement radioMale = this.GetVisualElementById<RadioButtonElement>("radioMale");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            radioMale.Focus();
            lblLog.Text = "Male RadioButton option has the Focus";
        }
    }
}