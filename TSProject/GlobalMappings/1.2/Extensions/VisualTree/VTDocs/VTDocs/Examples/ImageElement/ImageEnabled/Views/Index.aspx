<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Image Enabled Property" ID="windowView1" LoadAction="ImageEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Image runat="server" Text="Image" Top="195px" ID="TestedImage1" CssClass="vt-test-img-1" ClickAction="ImageEnabled\TestedImage1_Click"></vt:Image>           

        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedImage' is initially set to 'false'." Top="365px" ID="lblExp1"></vt:Label> 

        <vt:Image runat="server" Text="TestedImage" Top="415px" Enabled ="false" ID="TestedImage2" CssClass="vt-test-img-2" ClickAction="ImageEnabled\TestedImage2_Click"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog2"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="540px" Width="300px" Height="80px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="635px" ID="btnChangeEnabled" Width="180px" ClickAction="ImageEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
