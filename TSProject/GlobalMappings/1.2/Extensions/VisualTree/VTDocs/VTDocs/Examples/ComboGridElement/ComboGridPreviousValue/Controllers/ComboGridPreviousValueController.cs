using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridPreviousValueController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(int));
            source.Rows.Add("James", "Dean", 1);
            source.Rows.Add("Bob", "Marley", 2);
            source.Rows.Add("Dana", "International", 3);

            TestedComboGrid.ValueMember = "Column1";
            TestedComboGrid.DisplayMember = "Column3";

            TestedComboGrid.DataSource = source;
            TestedComboGrid.DataSource = source;

            lblLog.Text = "SelectedIndex Value: " + TestedComboGrid.SelectedIndex.ToString();
            lblLog.Text += "\\r\\nPreviousValue Value: " + TestedComboGrid.PreviousValue;

        }

        public void TestedComboGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "SelectedIndex Value: " + TestedComboGrid.SelectedIndex.ToString();
            lblLog.Text += "\\r\\nPreviousValue Value: " + TestedComboGrid.PreviousValue;
        }
    }
}
