using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "GroupBox PixelHeight is: " + TestedGroupBox.PixelHeight;

        }

        public void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            if (TestedGroupBox.PixelHeight == 80)
            {
                TestedGroupBox.PixelHeight = 40;
                lblLog.Text = "GroupBox PixelHeight is: " + TestedGroupBox.PixelHeight;

            }
            else
            {
                TestedGroupBox.PixelHeight = 80;
                lblLog.Text = "GroupBox PixelHeight is: " + TestedGroupBox.PixelHeight;
            }

        }
      
    }
}