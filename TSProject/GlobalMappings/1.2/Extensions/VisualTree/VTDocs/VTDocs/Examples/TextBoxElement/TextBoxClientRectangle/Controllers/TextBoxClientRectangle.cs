using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTextBox.ClientRectangle;

        }
        public void btnChangeTextBoxClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            if (TestedTextBox.Left == 100)
            {
                TestedTextBox.SetBounds(80, 320, 150, 26);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTextBox.ClientRectangle;

            }
            else
            {
                TestedTextBox.SetBounds(100, 310, 200, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedTextBox.ClientRectangle;
            }

        }

    }
}