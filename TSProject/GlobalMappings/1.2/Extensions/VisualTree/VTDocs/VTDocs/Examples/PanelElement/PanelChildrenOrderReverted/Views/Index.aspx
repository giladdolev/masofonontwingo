<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel ChildrenOrderReverted Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="ReadOnly property" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     


         <vt:Panel runat="server" Text="ChildrenOrderReverted property" Top="45px" Left="140px" ID="pnlChildrenOrderReverted" Height="70px" Width="250px"></vt:Panel>

        <vt:Button runat="server" Text="Get ChildrenOrderReverted value" Top="50px" Left="400px" ID="btnGetChildrenOrderReverted" Height="36px" Width="250px" ClickAction ="PanelChildrenOrderReverted\btnGetChildrenOrderReverted_Click"></vt:Button>           

        <vt:TextBox runat="server" Text="" Top="135px" Left="120px"  ID="textBox1" Multiline="true"  Height="50px" Width="310px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
