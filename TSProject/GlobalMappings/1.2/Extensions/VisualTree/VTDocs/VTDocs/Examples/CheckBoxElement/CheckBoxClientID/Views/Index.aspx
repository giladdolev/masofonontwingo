<%--<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox ClientID Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="ReadOnly property" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:CheckBox runat="server" Text="Tested CheckBox" Top="45px" Left="140px" ID="testedCheckBox" Height="36px" Width="220px"></vt:CheckBox> 

        <vt:Button runat="server" Text="Get ClientID value" Top="45px" Left="400px" ID="btnClientID" Height="36px" Width="220px" ClickAction ="CheckBoxClientID\btnClientID_Click"></vt:Button>           

        <vt:TextBox runat="server" Text="" Top="100px" Left="120px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>--%>

<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox ClientID Property" ID="windowView2" LoadAction="CheckBoxClientID\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="ClientID" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the Client Identifier of the current instance.

            Syntax: public virtual string ClientID { get; }" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the 'Get ClientID' button will get the ClientID value 
            of this CheckBox." Top="250px" ID="lblExp2"></vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox" Top="340px" ID="TestedCheckBox"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="380"></vt:Label>

        <vt:Button runat="server" Text="Get ClientID >>" Top="460px" ID="btnClientID" ClickAction="CheckBoxClientID\GetClientID_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
