using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicMessageBoxController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public async void btnShow_Click(object sender, EventArgs e)
        {

            DialogResult result = await MessageBox.Show("Hello User,\nAre you human?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
            result = await MessageBox.Show(String.Format("User Clicked {0} button", result), "Answer", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }  
    }
}