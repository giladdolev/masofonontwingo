using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeCursor = this.GetVisualElementById<ButtonElement>("btnChangeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (btnChangeCursor.Cursor == CursorsElement.Default)
            {
                btnChangeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.AppStarting)
            {
                btnChangeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.Cross)
            {
                btnChangeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.Hand)
            {
                btnChangeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.Help)
            {
                btnChangeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.HSplit)
            {
                btnChangeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.IBeam)
            {
                btnChangeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.No)
            {
                btnChangeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.NoMove2D)
            {
                btnChangeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                btnChangeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                btnChangeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanEast)
            {
                btnChangeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanNE)
            {
                btnChangeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;

            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanNorth)
            {
                btnChangeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanNW)
            {
                btnChangeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanSE)
            {
                btnChangeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanSouth)
            {
                btnChangeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanSW)
            {
                btnChangeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.PanWest)
            {
                btnChangeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.SizeAll)
            {
                btnChangeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.SizeNESW)
            {
                btnChangeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.SizeNS)
            {
                btnChangeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                btnChangeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.SizeWE)
            {
                btnChangeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.UpArrow)
            {
                btnChangeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.VSplit)
            {
                btnChangeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
            else if (btnChangeCursor.Cursor == CursorsElement.WaitCursor)
            {
                btnChangeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + btnChangeCursor.Cursor;
            }
        }

    }
}