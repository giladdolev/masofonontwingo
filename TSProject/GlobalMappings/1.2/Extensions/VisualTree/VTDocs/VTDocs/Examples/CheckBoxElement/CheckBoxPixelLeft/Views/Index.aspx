<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PixelLeft Property" ID="windowView1" LoadAction="CheckBoxPixelLeft\OnLoad">          

        <vt:Label runat="server" SkinID="Title" Text="PixelLeft" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the left edge of the control 
            and the left edge of its container's client area.
            
            Syntax: public int PixelLeft { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Left property of this CheckBox is initially set to: 80px" Top="240px" ID="lblExp1"></vt:Label>

        <vt:CheckBox runat="server" Text="TestedCheckBox" Top="300px" ID="TestedCheckBox" TabIndex="1"></vt:CheckBox>

        <vt:Label runat="server" SkinID="Log" Top="340px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelLeft value >>" Top="405px" ID="btnChangePixelLeft" TabIndex="1" Width="180px" ClickAction="CheckBoxPixelLeft\btnChangePixelLeft_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
