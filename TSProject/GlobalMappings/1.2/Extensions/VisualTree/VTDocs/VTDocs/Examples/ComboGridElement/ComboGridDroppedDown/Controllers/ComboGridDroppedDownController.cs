using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridDroppedDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid1 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(int));
            source.Rows.Add("James", "Dean", 1);
            source.Rows.Add("Bob", "Marley", 2);
            source.Rows.Add("Dana", "International", 3);

            TestedComboGrid1.ValueMember = "Column1";
            TestedComboGrid1.DisplayMember = "Column3";

            TestedComboGrid1.DataSource = source;
            lblLog1.Text = "DroppedDown Value: " + TestedComboGrid1.DroppedDown.ToString();

            DataTable source1 = new DataTable();
            source1.Columns.Add("Column1", typeof(string));
            source1.Columns.Add("Column2", typeof(string));
            source1.Columns.Add("Column3", typeof(int));
            source1.Rows.Add("James", "Dean", 1);
            source1.Rows.Add("Bob", "Marley", 2);
            source1.Rows.Add("Dana", "International", 3);

            TestedComboGrid2.ValueMember = "Column1";
            TestedComboGrid2.DisplayMember = "Column3";

            TestedComboGrid2.DataSource = source1;
            TestedComboGrid2.DataSource = source1;

            lblLog2.Text = "DroppedDown Value: " + TestedComboGrid2.DroppedDown.ToString() + ".";

        }

        public void btnChangeDroppedDown_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboGrid2.DroppedDown = !TestedComboGrid2.DroppedDown;
            lblLog2.Text = "DroppedDown value: " + TestedComboGrid2.DroppedDown + '.';
        }

             

    }
}