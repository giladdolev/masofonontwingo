using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckedListBoxBasicController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void Load_CheckedListBox(object sender, EventArgs e)
        {
            CheckedListBoxElement list = this.GetVisualElementById<CheckedListBoxElement>("CheckedListBox1");
            Random rand = new Random();
            int idx = rand.Next(4);
            list.SetItemChecked(idx, !list.GetItemChecked(idx));
        }
        public void check(object sender, EventArgs e)
        {
            ItemCheckEventArgs args = e as ItemCheckEventArgs;

            CheckedListBoxElement list = this.GetVisualElementById<CheckedListBoxElement>("CheckedListBox1");
           

        }
        
    }
}