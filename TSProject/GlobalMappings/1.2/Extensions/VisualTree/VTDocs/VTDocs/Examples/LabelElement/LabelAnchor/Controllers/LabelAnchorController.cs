﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelAnchorController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnAnchor_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");
            PanelElement Panel1 = this.GetVisualElementById<PanelElement>("Panel1");

            if (label.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                label.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                Panel1.Size = new Size(350, 90);
            }

            else if (label.Anchor == AnchorStyles.Top)
            {
                label.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                Panel1.Size = new Size(315, 140);
            }
            else if (label.Anchor == AnchorStyles.Left)
            {
                label.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                Panel1.Size = new Size(200, 150);
            }
            else if (label.Anchor == AnchorStyles.Right)
            {
                label.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                Panel1.Size = new Size(200, 160);
            }
            else if (label.Anchor == AnchorStyles.Bottom)
            {
                label.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                Panel1.Size = new Size(315, 151);//Set  back the panel size
            }
            else if (label.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and listView1 settings
                label.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                Panel1.Size = new Size(300, 100);//Set back the panel size
                //label.Location = new Point(150, 200);
            }
        }
      
    }
}