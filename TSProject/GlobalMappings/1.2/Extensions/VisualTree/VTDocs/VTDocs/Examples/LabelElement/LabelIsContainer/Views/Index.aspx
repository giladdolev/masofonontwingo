<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label IsContainer property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Label runat="server" Text="IsContainer testing Run Time" Top="190px" Left="130px" ID="label" Width="350px"></vt:Label>
        <vt:Button runat="server" Text="Get Label IsContainer value" Top="112px" Left="130px" ID="btnIsContainer" TabIndex="1" Width="200px" ClickAction="LabelIsContainer\btnIsContainer_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>