<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid SearchPanel property" ID="windowView2" LoadAction="ComboGridSearchPanel/OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="SearchPanel" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a flag for a built-in Search Panel, allowing end-users to quickly filter 
            and locate dropdown records - rows, by the text they contain.

            Syntax: public bool SearchPanel { get; set; }
            'true' if the Search Panel is shown; otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- TestedComboGrid1 -->
        <vt:ComboGrid runat="server" Text="ComboGrid" ID="TestedComboGrid1"  Top="190px"> </vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="230px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="310px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="SearchPanel value is initially set to 'true'."
            Top="350px" ID="lblExp2">
        </vt:Label>

         <!-- TestedComboGrid2 -->
        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-TestedComboGrid" Top="405px" SearchPanel="true" ID="TestedComboGrid2"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="445px" Width="350px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change SearchPanel value >>" Width="180px" Top="510px" ID="btnChangeSearchPanel" ClickAction="ComboGridSearchPanel\btnChangeSearchPanel_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
