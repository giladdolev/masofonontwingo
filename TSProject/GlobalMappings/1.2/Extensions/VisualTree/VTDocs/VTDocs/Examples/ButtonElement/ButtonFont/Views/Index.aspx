<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Font Property" ID="windowView2" LoadAction="ButtonFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>
        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
   
        <!--TestedButton1-->
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="195px" ID="TestedButton1" ></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="255px" Width="360px" Height="45px" ID="lblLog1"></vt:Label>
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px"  ID="lblExample"></vt:Label>
         <vt:Label runat="server" Text="Font property of this 'TestedButton is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="400px"  ID="lblExp1"></vt:Label>     
        
      <!--TestedButton2-->
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="465px" ID="TestedButton2" ></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="525px" Width="350px" Height="45px" ID="lblLog2"></vt:Label>
         <vt:Button runat="server" Text="Change Font Value >>" Top="610px" ID="btnChangeButtonFont" ClickAction="ButtonFont/btnChangeButtonFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>