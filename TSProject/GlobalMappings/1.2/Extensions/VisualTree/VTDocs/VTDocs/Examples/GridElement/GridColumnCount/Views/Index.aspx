﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid ColumnCount Property" Height="850px" ID="windowView1" LoadAction="GridColumnCount\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ColumnCount" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the number of columns displayed in the Grid.
            
            public int ColumnCount { get; set; }

            Remarks: Setting the ColumnCount property to 0 will remove all columns from the DataGridView.
If ColumnCount is reset to a value less than the current value, columns will be removed from the end of
             the Columns collection. If ColumnCount is set to a value greater than the current value, columns will be 
            added to the end of the Columns collection" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Grid runat="server" Top="250px" Width="500px" ID="TestedGrid1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Width="400px" Top="380px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="430px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="ColumnCount property of this Grid is initially set to '5'" Top="470px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" Width="500px" Height="140px" Top="520px" ID="TestedGrid2">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="675px" Height="60px" Width="400px" ID="lblLog2"></vt:Label>

         <vt:TextBox runat="server" Text="Input" Width="80px" Top="770px" ID="txtColumnCount"></vt:TextBox>

        <vt:Button runat="server" Text="Change ColumnCount >>" Left="200px" Width="180px" Top="770px" ID="btnChangeColumnCount" ClickAction="GridColumnCount\btnChangeColumnCount_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
