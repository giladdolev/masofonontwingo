<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox ReadOnly Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="ReadOnlt is initially set to 'true'" Top="40px" Left="160px" ID="Label3"  BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="10" Height="23px" TabIndex="45" Width="150px"></vt:Label>  

        <vt:Button runat="server" ID="btn1" ClickAction="RichTextBoxReadOnly\clickBtn1" Left="160px" Top="310px" Width="100" Height="30" Text="ReadOnly True"></vt:Button>
         
        <vt:Button runat="server" ID="Button1" ClickAction="RichTextBoxReadOnly\clickBtn2" Left="370px" Top="310px" Width="100" Height="30" Text="ReadOnly False"></vt:Button>
        
        <vt:RichTextBox runat="server" Text="sampel Text -- " ID="richTextBox1" ReadOnly="true" Left="140px" Top="100px" Height="200px" Width="400px"></vt:RichTextBox>

    </vt:WindowView>
</asp:Content>
