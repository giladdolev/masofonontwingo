﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SearchPanel" Height="800px" Width="1200px" LoadAction="GridSearchPanel\page_load">

         <vt:Label runat="server" SkinID="Title" Text="SearchPanel" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the value indicating if the filter field of the control is shown" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Grid runat="server" AllowUserToDeleteRows="False" Top="120px" Left="30px" ID="gridElement1" Height="230px" Width="782px" LostFocusAction="True" RowCount="3">
            <Columns>
                <vt:GridTextBoxColumn ID="txtclm" runat="server" Height="70px" HeaderText="TextBox Column" DataMember="TextColumn"></vt:GridTextBoxColumn>

                <vt:GridTextButtonColumn ID="txtbtnclm" runat="server" HeaderText="TextButton Column" DataMember="TextButtonColumn"></vt:GridTextButtonColumn>

                <vt:GridCheckBoxColumn ID="cehckclm" runat="server" HeaderText="CheckBox Column" DataMember="CheckBoxColumn"></vt:GridCheckBoxColumn>

                <vt:GridDateTimePickerColumn ID="dataclm" runat="server" HeaderText="Date Column" DataMember="DateTimeColumn" Format="Short"></vt:GridDateTimePickerColumn>

                <vt:GridComboBoxColumn ID="comboclm" runat="server" HeaderText="ComboBox Column" DataMember="ComboBoxColumn" Width="90" Format="Short"></vt:GridComboBoxColumn>
                 <vt:GridTextBoxColumn ID="GridTextBoxColumn1" runat="server" HeaderText="Currency" DataMember="Currency" Format="currency"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="360px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="390px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="SearchPanel property of this 'Grid' is initially set to true" Top="410px"  ID="lblExp1"></vt:Label>  

        <vt:Grid runat="server" SearchPanel="true" AllowUserToDeleteRows="False" Top="470px" Left="30px" ID="grid2" Height="250px" Width="782px" LostFocusAction="True"  RowCount="3">
            <Columns>
                <vt:GridTextBoxColumn ID="GridTextBoxColumn2" runat="server" Height="70px" HeaderText="TextBox Column" DataMember="TextColumn"></vt:GridTextBoxColumn>

                <vt:GridTextButtonColumn ID="GridTextButtonColumn1" runat="server" HeaderText="TextButton Column" DataMember="TextButtonColumn"></vt:GridTextButtonColumn>

                <vt:GridCheckBoxColumn ID="GridCheckBoxColumn1" runat="server" HeaderText="CheckBox Column" DataMember="CheckBoxColumn"></vt:GridCheckBoxColumn>

                <vt:GridDateTimePickerColumn ID="GridDateTimePickerColumn1" runat="server" HeaderText="Date Column" DataMember="DateTimeColumn" Format="Short"></vt:GridDateTimePickerColumn>

                <vt:GridComboBoxColumn ID="GridComboBoxColumn1" runat="server" HeaderText="ComboBox Column" DataMember="ComboBoxColumn" Width="90" Format="Short"></vt:GridComboBoxColumn>
                 <vt:GridTextBoxColumn ID="GridTextBoxColumn3" runat="server" HeaderText="Currency" DataMember="Currency" Format="currency"></vt:GridTextBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log"  Top="750px" ID="lblLog2"></vt:Label>
        
        <vt:Button runat="server" SkinID="Wide" UseVisualStyleBackColor="True" Text="Change SearchPanel" Top="740px" Left="470px" ID="button1" ClickAction="GridSearchPanel\button1_Click" >
        </vt:Button>
    
    </vt:WindowView>

</asp:Content>
