using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewItemsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press one of the buttons...";
        }

        public void RemoveItems_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedListView.Items.Count > 0)
                TestedListView.Items.RemoveAt(TestedListView.Items.Count - 1);
            
            lblLog.Text = "TestedListView.Items.Remove was invoked";
        }

        private void AddItems_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewItem newitem = new ListViewItem();
            newitem.Text = "myItem";
            newitem.Subitems.Add("11");
            newitem.Subitems.Add("22");
            TestedListView.Items.Add(newitem);

            lblLog.Text = "TestedListView.Items.Add was invoked.";

        }
    }
} 