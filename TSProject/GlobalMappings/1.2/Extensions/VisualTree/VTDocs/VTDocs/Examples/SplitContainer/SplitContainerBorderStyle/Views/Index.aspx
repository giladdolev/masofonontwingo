<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer BorderStyle Property" ID="windowView1" LoadAction="SplitContainerBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="170px" ID="TestedSplitContainer1"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="330px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this SplitContainer is initially set to 'Dotted'" Top="370px"  ID="lblExp1"></vt:Label>     
        
        <vt:SplitContainer runat="server"  Text="TestedSplitContainer" BorderStyle="Dotted" Top="410px" ID="TestedSplitContainer2"></vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="505px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="570px" ID="btnChangeSplitContainerBorderStyle" ClickAction="SplitContainerBorderStyle\btnChangeSplitContainerBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
