using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpSize = this.GetVisualElementById<GroupBoxElement>("grpSize");

            if (grpSize.Size == new Size(200,150)) //Get
            {
                grpSize.Size = new Size(190, 100); //Set
                textBox1.Text = "The Size property value is: " + grpSize.Size;
            }
            else  
            {
                grpSize.Size = new Size(200, 150); 
                textBox1.Text = "The Size property value is: " + grpSize.Size;
            }
             
        }
        
        public void grpSize_SizeChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "SizeChanged Event is invoked\\r\\n";

        }
    }
}