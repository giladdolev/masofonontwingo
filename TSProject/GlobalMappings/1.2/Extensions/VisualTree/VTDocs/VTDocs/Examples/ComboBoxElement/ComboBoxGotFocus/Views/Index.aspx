<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox GotFocus event" ID="windowView2" LoadAction="ComboBoxGotFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="GotFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control receives focus.

            Syntax: public event EventHandler GotFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted into one of the combo boxes,
             a 'GotFocus' event will be invoked.
             Each invocation of the 'GotFocus' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:GroupBox runat="server" Top="335px" Height="120px" Width="230px">
                
            <vt:ComboBox runat="server" Top="30px" Left="15px" Text="ComboBox 1" ID="TestedComboBox1" CssClass="vt-test-cmb-1" TabIndex="1" GotFocusAction="ComboBoxGotFocus\TestedComboBox1_GotFocus"></vt:ComboBox>

            <vt:ComboBox runat="server" Top="75px" Left="15px" Text="ComboBox 2" ID="TestedComboBox2" CssClass="vt-test-cmb-2" TabIndex="2" GotFocusAction="ComboBoxGotFocus\TestedComboBox2_GotFocus"></vt:ComboBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="470px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="510px" Width="360px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Focus Top ComboBox >>" Width="180px" Top="640px" TabIndex="3" ID="btnChangeFocus" GotFocusAction="ComboBoxGotFocus\btnChangeFocus_GotFocus" ClickAction="ComboBoxGotFocus\btnChangeFocus_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

