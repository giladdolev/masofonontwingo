<%--<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server"  EnableEsc="true"  Text="Window Enable Esc" ID="BasicWindow">
               <vt:Label runat="server" SkinID="Title" Text="Window Enable Esc" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Enable or disable the ESC button on the keyboard" Top="75px"  ID="lblDefinition"></vt:Label>
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="115px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Window Enable Esc is enable or disable you to close
             the window with esc button
            This is a config , in this window is true  (false by default)" Top="140px"  ID="lblExp1"></vt:Label>     
	</vt:WindowView>
</asp:Content>--%>


<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window EnableEsc Property" EnableEsc="true" ID="windowView2" LoadAction="WindowEnableEsc\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="EnableEsc" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Enable or disable the action of window closing when the ESC key is pressed.

            Syntax: public bool EnableEsc { get; set; }
            The default value is 'false'." Top="75px"  ID="Label2"></vt:Label>
               
        <vt:Button runat="server" SkinID="Wide" Text="Open Default Window" Top="170px" ID="TestedButton1" ClickAction="WindowEnableEsc\TestedButton1_Click"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="Label3"></vt:Label>
        <vt:Label runat="server" Text="Use the 'Open Initialized Window' button to open a window. 
            EnableEsc property of this 'Initialized Window' is initially set to 'true'.
            You can change the 'EnableEsc' value by pressing the 'Change EnableEsc Value' button.  
            When the Esc is Enabled, typing it will close the window.
            " Top="330px"  ID="Label4"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" Text="Open Initialized Window" Top="450px" ID="TestedButton2" ClickAction="WindowEnableEsc\TestedButton2_Click"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change EnableEsc Value >>" Top="580px" Width="180px" ID="btnChangeEnableEscValue" ClickAction="WindowEnableEsc\btnChangeEnableEscValue_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
