<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab GetType Method" ID="windowView2" LoadAction="TabGetType\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="GetType" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets the Type of the current instance.

            Syntax: public Type GetType()" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="210px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the GetType button will invoke the 
            GetType method of this Tab." Top="250px" ID="lblExp2"></vt:Label>

        <vt:Tab runat="server" Text="TestedTab" Top="340px" ID="TestedTab">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Width="350px" Height="40px" ID="lblLog" Top="455px"></vt:Label>

        <vt:Button runat="server" Text="GetType >>" Top="535px" ID="btnGetType" ClickAction="TabGetType\GetType_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
