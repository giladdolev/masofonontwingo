using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerGetTypeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Press the GetType Button...";
        }

        public void GetType_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer = this.GetVisualElementById<SplitContainer>("TestedSplitContainer");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedSplitContainer.GetType();
            lblLog.Text = "The element Type is: \\r\\n" + TestedSplitContainer.GetType();
        }
    }
}