<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab PixelLeft Property" ID="windowView2" LoadAction="TabPixelLeft\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PixelLeft" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the left edge of the control
            and the left edge of its container's client area.
            
            Syntax: public int PixelLeft { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Left property of this Tab is initially set to 80px" Top="250px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedTab -->
        <vt:Tab runat="server" Text="TestedTab" Top="300px" ID="TestedTab">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="415px" ID="lblLog"></vt:Label>
          
        <vt:Button runat="server" Text="Change PixelLeft Value >>" Top="480px" ID="ChangePixelLeft" ClickAction="TabPixelLeft\ChangePixelLeft_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>