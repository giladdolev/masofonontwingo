using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelTextAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "TextAlign value: " + TestedLabel1.TextAlign;
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + '.';

        }

        private void btnTopLeft_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.TopLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        private void btnTopCenter_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.TopCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        private void btnTopRight_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.TopRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }


        private void btnMiddleLeft_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.MiddleLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        private void btnMiddleCenter_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.MiddleCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        private void btnMiddleRight_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.MiddleRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        private void btnBottomLeft_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.BottomLeft;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        private void btnBottomCenter_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.BottomCenter;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        private void btnBottomRight_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            TestedLabel2.TextAlign = ContentAlignment.BottomRight;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextAlign value: " + TestedLabel2.TextAlign + ".";
        }

        
    }
}