using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPerformRenderController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformRender_Click(object sender, EventArgs e)
        {
            
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");
            
            MouseEventArgs args = new MouseEventArgs();

            //testedProgressBar.PerformRender(args);
        }

        public void testedProgressBar_Render(object sender, EventArgs e)
        {
            MessageBox.Show("TestedProgressBar Render event method is invoked");
        }

    }
}