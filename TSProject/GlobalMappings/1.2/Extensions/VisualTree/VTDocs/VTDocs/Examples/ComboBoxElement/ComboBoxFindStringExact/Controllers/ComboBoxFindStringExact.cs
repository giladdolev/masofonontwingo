using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxFindStringExactController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.Items.Add("");
            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("*Item2");
            TestedComboBox.Items.Add("Item 3");
        }

        private void btnFindString_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            int index = TestedComboBox.FindStringExact(txtInput.Text);
            lblLog.Text = "The result index is: " + index.ToString();
        }

        public void TextBox_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "User Typed: " + txtInput.Text;
        }
    }
}