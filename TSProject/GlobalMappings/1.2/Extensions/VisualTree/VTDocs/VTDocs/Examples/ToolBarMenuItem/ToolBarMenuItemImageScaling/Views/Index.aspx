﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
 
     <vt:WindowView runat="server" Text="ToolBarMenuItem ImageScaling" Width ="815px" ID="windowView1" LoadAction ="ToolBarMenuItemImageScaling\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ImageScaling property" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether an image on a 
            ToolStripItem is automatically resized to fit in a container." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="390px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can see how the ImageScaling property works and 
            it's relation to the Image property (you can also see the relation 
            between the properties: Image, ImageScaling, Text and TextImageRelation)" Top="430px" ID="lblExp1"></vt:Label>

       <vt:Menu runat="server" ID="TestedMenu" Dock="None" Width="500px" Top="495px">
            <vt:ToolBarMenuItem runat="server"  Image="Content/Images/galilcs.png" ID="tbmi1" ImageScaling="None" TextImageRelation="ImageAboveText">
                <vt:ToolBarMenuItem runat="server"  />
                <vt:ToolBarMenuItem runat="server" Text="Open..." />
                <vt:ToolBarMenuSeparator runat="server" />
                <vt:ToolBarMenuItem runat="server" Text="Exit"/>
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Edit" ImageScaling="None" TextImageRelation="ImageAboveText" ID="tbmi2">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Tools" ImageScaling="None" ID="tbmi3" Image="Content/Images/galilcs.png" TextImageRelation="ImageAboveText">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Help" TextImageRelation="ImageAboveText" ID="tbmi4" ImageScaling="None">
                <vt:ToolBarMenuItem runat="server" Text="About" />
            </vt:ToolBarMenuItem>

            <vt:ToolBarTextBox runat="server" Text="tbtb" Image="Content/Images/galilcs.png" ID="tbtb" ImageScaling="SizeToFit">           
            </vt:ToolBarTextBox>
           <vt:ToolBarButton runat="server" Text="tbtb" Image="Content/Images/galilcs.png" ID="ToolBarButton2">           
            </vt:ToolBarButton>
           <vt:ToolBarDropDownButton runat="server" Text="tbddb" Image="Content/Images/galilcs.png" ID="tbddb">           
            </vt:ToolBarDropDownButton>
       </vt:Menu>

        <vt:Label runat="server" SkinID="Log" Top="550px" Width="500px" ID="lblLog"></vt:Label>


        <vt:Label runat="server" Text="Select a constructor to add new ToolBarMenuItem: " Top="600px" ID="Label1"></vt:Label>

         <vt:ComboBox runat="server" Text="Constructors" Left="400px" Top="600px" Width="250px" ID="cboMenuItemConstructors" SelectedIndexChangedAction="ToolBarMenuItemImageScaling\AddNewToolBarItem_SelectedIndexChanged">
             <Items>
                <vt:ListItem runat="server" Text="ToolBarMenuItem()"></vt:ListItem>
                <vt:ListItem runat="server" Text="ToolBarMenuItem(String)"></vt:ListItem>
                <vt:ListItem runat="server" Text="ToolBarMenuItem(string, ResourceReference, EventHandler,Keys)"></vt:ListItem>
            </Items>
         </vt:ComboBox>

         <vt:Button runat="server" Text="Change ImageScaling >>" Top="650px" ID="btnNone" Height="23px" Width="120px" ClickAction="ToolBarMenuItemImageScaling\btnImageScaling_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
