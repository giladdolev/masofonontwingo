using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxDataSourceController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void Form_Load(object sender, EventArgs e)
        {
           
        }
        public void BtnAdd_Click(object sender, EventArgs e)
        {
            ComboBoxElement comboBox1 = this.GetVisualElementById<ComboBoxElement>("comboBox1");
            string[] a = new string[5] {
				"11",
				"22",
				"33",
				"44",
				"55",
			};
            comboBox1.DataSource = a;
        }
    }
}