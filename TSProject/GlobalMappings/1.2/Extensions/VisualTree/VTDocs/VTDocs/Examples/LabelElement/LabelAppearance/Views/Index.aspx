<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Appearance property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Initialized Component : Label Dock is set to cc3D" Top="300px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:Label runat="server" Text="Appearance testing initialized" Top="350px" Left="130px" ID="labelTestingInitialized" Width="350px" Appearance="cc3D"></vt:Label>
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Label runat="server" Text="Appearance testing Run Time" Top="190px" Left="130px" ID="label" Width="350px"></vt:Label>
        <vt:Button runat="server" Text="Change Label Appearance" Top="112px" Left="130px" ID="btnAppearance" TabIndex="1" Width="200px" ClickAction="LabelAppearance\btnAppearance_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>