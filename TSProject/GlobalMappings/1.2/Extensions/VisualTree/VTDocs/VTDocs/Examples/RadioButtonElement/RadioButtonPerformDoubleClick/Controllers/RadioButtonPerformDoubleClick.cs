using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPerformDoubleClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformDoubleClick_Click(object sender, EventArgs e)
        {
            RadioButtonElement testedRadioButton = this.GetVisualElementById<RadioButtonElement>("testedRadioButton");           

            testedRadioButton.PerformDoubleClick(e);
        }

        public void testedRadioButton_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRadioButton DoubeleClick event method is invoked");
        }

    }
}