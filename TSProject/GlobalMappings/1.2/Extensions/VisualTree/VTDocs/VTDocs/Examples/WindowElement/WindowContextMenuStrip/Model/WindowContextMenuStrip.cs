﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowContextMenuStrip : WindowElement
    {
        WindowContextMenuStrip2 _frm2;
        WindowContextMenuStrip3 _frm3;

        public WindowContextMenuStrip()
		{
		}

        public WindowContextMenuStrip2 frm2
        {
            get { return this._frm2; }
            set { this._frm2 = value; }
        }
        public WindowContextMenuStrip3 frm3
        {
            get { return this._frm3; }
            set { this._frm3 = value; }
        }
    }
}