using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Left value: " + TestedRadioButton.Left + '.';
        }

        public void btnChangeLeft_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedRadioButton.Left == 80)
            {
                TestedRadioButton.Left = 180;
                lblLog.Text = "Left value: " + TestedRadioButton.Left + '.';
            }
            else
            {
                TestedRadioButton.Left = 80;
                lblLog.Text = "Left value: " + TestedRadioButton.Left + '.';
            }
        }

    }
}