
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItemCollection Add Method (String)" ID="windowView2" LoadAction="TabTabItemCollectionAddString\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Add (String)" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Creates a tab page with the specified text, and adds it to the collection.

            Syntax: public void Add(string text)
            
            The newly created TabPage is added to the end of the collection." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the 'Add Item' button will invoke the Items.Add method 
            on this Tab control. Pressing the 'Remove Item' button will invoke the Items.RemoveAt 
            method on this Tab control." Top="230px" ID="lblExp2"></vt:Label>

        <vt:Tab runat="server" Text="Tab" Top="320px" Width="400px" ID="TestedTab1">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Width="400px" ID="lblLog" Top="435"></vt:Label>

        <vt:Button runat="server" Text="Remove Item >>" Top="510px" Left="300px" ID="btnRemoveItem" ClickAction="TabTabItemCollectionAddString\RemoveItems_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Item >>" Top="510px"  ID="btnAddItem" ClickAction="TabTabItemCollectionAddString\AddItems_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
