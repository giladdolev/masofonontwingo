<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab ImageList property" ID="windowView1" LoadAction="TabImageList\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ImageList" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the images to display on the control's tabs.

            Syntax: public ImageList ImageList { get; set; }
            Value: An ImageList that specifies the images to display on the tabs
            If no ImageList is set the value is null."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Tab1 -->
        <vt:Tab runat="server" Text="Tab" Top="165px" ID="TestedTab1">
            <TabItems>
                <vt:TabItem runat="server" Text="Item1" Top="22px" Left="4px" ID="Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="Item2" Top="22px" Left="4px" ID="Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="Item3" Top="22px" Left="4px" ID="Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="280px" BorderStyle="None" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="335px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ImageList property of this Tab is initially set to Green" Top="385px" ID="lblExp1"></vt:Label>

        <!-- Tested Tab2 -->
        <vt:Tab runat="server" Text="TestedTab" Top="420px" ImageListID="imageList1" ID="TestedTab2">
            <TabItems>
                <vt:TabItem runat="server" Text="Item4" Top="22px" Left="4px" ID="Item4" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="Item5" Top="22px" Left="4px" ID="Item5" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="Item6" Top="22px" Left="4px" ID="Item6" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="535px" Width="450px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change TabElement ImageList value >>" Top="590px" Width="180px" ID="btnChangeTabImageList" ClickAction="TabImageList\btnChangeTabImageList_Click"></vt:Button>

    </vt:WindowView>

    <vt:ComponentManager runat="server" Visible="False">
        <vt:ImageList runat="server" TransparentColor="Transparent" ID="imageList1">
            <Images>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream0.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream1.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream2.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList1_ImageStream3.png"></vt:UrlReference>
            </Images>
            <Keys>
<vt:KeyItem runat="server" Key="Content/Images/Chrysanthemum.jpg" ></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/Desert.jpg" ImageIndex="1"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/Hydrangeas.jpg" ImageIndex="2"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/Jellyfish.jpg" ImageIndex="3"></vt:KeyItem>
            </Keys>
        </vt:ImageList>
        <vt:ImageList runat="server" TransparentColor="Transparent" ID="imageList2">
            <Images>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList2_ImageStream0.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList2_ImageStream1.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList2_ImageStream2.png"></vt:UrlReference>
                <vt:UrlReference runat="server" Source="Content/WindowsFormsApplication1/Form1_imageList2_ImageStream3.png"></vt:UrlReference>
            </Images>
            <Keys>
<vt:KeyItem runat="server" Key="Content/Images/Koala.jpg" ></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/Lighthouse.jpg" ImageIndex="1"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/Penguins.jpg" ImageIndex="2"></vt:KeyItem>
                <vt:KeyItem runat="server" Key="Content/Images/Tulips.jpg" ImageIndex="3"></vt:KeyItem>
            </Keys>
        </vt:ImageList>
    </vt:ComponentManager>

</asp:Content>
