using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicMonthCalendarController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void monthCalendar1_DateChanged(object sender, EventArgs e)
        {
            TextBoxElement TextBox1 = this.GetVisualElementById<TextBoxElement>("TextBox1");
            TextBox1.Text = "The DateChanged event is invoked";

        }

    }
}