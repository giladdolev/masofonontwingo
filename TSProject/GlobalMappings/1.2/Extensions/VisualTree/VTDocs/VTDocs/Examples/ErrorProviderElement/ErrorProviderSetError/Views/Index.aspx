<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ErrorProvider SetError Method" ID="windowView1" LoadAction="ErrorProviderSetError\OnLoad">
      
       
        <vt:Label runat="server" SkinID="Title" Text="SetError(control, value)" Left="230px" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Sets the error description string for the specified control.

            Syntax:  public void SetError(ControlElement control, string value)
            
            'control' is the control to set the error description string for.
            'value' is the error description string, or null or Empty to remove the error.
            
            If the length of value is greater than zero, then the error icon is displayed, and the ToolTip for the 
            error icon is the error description text. If the length is zero or value is null, the error icon is hidden."
            Top="75px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example there are 2 textboxes in a simple login dialog.
            When one of the text boxes is left empty, the SetError method is invoked, an error icon is displayed, 
            and the ToolTip for the error icon is the error description text." Top="320px" ID="lblExp2">
        </vt:Label>

        <vt:panel runat="server" Text="" Top="410px" ID="panel1" Width="320px" Height="150">
        <vt:label runat="server" Text="Username:" Top="30px" Left="35px" ID="lblUsername"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-username" Top="30px" Left="130px" ID="txtUsername" LeaveAction="ErrorProviderSetError\txtUsername_leave"></vt:textbox>

        <vt:label runat="server" Text="Password:" Top="60px" Left="35px" ID="lblPassword"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-password" Top="60px" Left="130px" ID="txtPassword" LeaveAction="ErrorProviderSetError\txtPassword_leave"></vt:textbox>

        <vt:Button runat="server" Text="Login" Width="100px" Height="30px" Top="100px" Left="110px" ID="btnLogin" ClickAction="ErrorProviderSetError\btnLogin_Click"></vt:Button>
        </vt:panel>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="570" Height="40px" Width="320px"></vt:Label>

       
    </vt:WindowView>
    <vt:ComponentManager runat="server" >
		<vt:ErrorProvider runat="server" ID="epUsername" ></vt:ErrorProvider>
	    <vt:ErrorProvider runat="server" ID="epPassword" ></vt:ErrorProvider>
	</vt:ComponentManager>s
</asp:Content>
