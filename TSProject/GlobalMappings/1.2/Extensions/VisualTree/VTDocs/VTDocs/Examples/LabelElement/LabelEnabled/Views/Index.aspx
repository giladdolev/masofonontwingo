<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label Enabled property" ID="windowView1" LoadAction="LabelEnabled/OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.

            Syntax: public bool Enabled { get; set; }

            true if the control can respond to user interaction; otherwise, false. The default is true."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="Label"  ID="TestedLabel1"  Top="190px"> </vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Enabled value is set to 'false'
            the Label should not respond to user interaction"
            Top="350px" ID="lblExp2">
        </vt:Label>
         
        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel"  Enabled="false" Top="410px" ID="TestedLabel2"> </vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="440px" Width="350px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled value >>" Width="180px" Top="505px" ID="btnChangeEnabled" ClickAction="LabelEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
