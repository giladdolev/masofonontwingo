 <%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Timer ToString Method" ID="windowView2" LoadAction="TimerToString\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="ToString" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Returns a string that represents the Timer control.

            Syntax: public override string ToString()
            
            Return Value: A string that includes the value of the Interval property." 
            Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="215px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the ToString button will invoke the ToString method 
            of this Timer." 
            Top="265px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" SkinID="TimerLabel" Top="335px" ID="timerLabel"></vt:Label>

        <vt:TextBox runat="server" SkinID="TimerTextBox" Top="330px" ID="timerTextBox"></vt:TextBox>

        <vt:Label runat="server" SkinID="Log" Width="420px" Height="40px" ID="lblLog" Top="370px"></vt:Label>

        <vt:Button runat="server" Text="ToString >>" Top="475px" ID="btnToString" ClickAction="TimerToString\ToString_Click"></vt:Button>

    </vt:WindowView>

    <vt:ComponentManager runat="server" >
		<vt:Timer runat="server" ID="TestedTimer" Enabled="true" Interval="1000" TickAction="TimerToString\TestedTimer_Tick"></vt:Timer>
	</vt:ComponentManager>
</asp:Content>