using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridElementWithCheckBoxColumnController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

		private void button1_Click(object sender, EventArgs e)
		{
            //GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
           
            ////Create column
            //GridCheckBoxColumn col;
            //col = new GridCheckBoxColumn("chkbox", "Check Box Column");
            //col.Width = 50;

            //////Add to grid
            //gridElement.Columns.Add(col);

            //gridElement.RefreshData();
            //gridElement.Refresh();



            //this.ViewModel.frm2Property = VisualElementHelper.CreateFromView<WindowsFormsApplication1.Form2>("Form2", "Form2", "WindowsFormsApplication1_WindowsFormsApplication1");
            //this.ViewModel.frm2Property.Show();

            //avihay:
            GridElement dataGridView1 = this.GetVisualElementById<GridElement>("gridElement");

            ////galilcs for now waiting to reslove bug number 7653

            GridImageColumn imgCol = new GridImageColumn();
            imgCol.HeaderText = "Imagecol1";
            imgCol.ID = "imgCol";
            dataGridView1.Columns.Add(imgCol);

            // dataGridView1.Rows.Add("ff",);
            //

            //// Lo mismo con datagrid view
            // int ss = 0;

            //foreach (var deviceItemKeyValuePair in MainModel.Devices)
            //{
            //DeviceData device = null; ;
            //bool isOk = this.MainModel.Devices.TryGetValue(deviceItemKeyValuePair.Key, out device);
            //if (isOk)
            //{

            GridRow gridRow1;
            gridRow1 = new GridRow();

            GridRow gridRow2;
            gridRow2 = new GridRow();

            GridRow gridRow3;
            gridRow3 = new GridRow();

            GridRow gridRow4;
            gridRow4 = new GridRow();

            GridRow gridRow5;
            gridRow5 = new GridRow();

            GridRow gridRow6;
            gridRow6 = new GridRow();

            GridRow gridRow7;
            gridRow7 = new GridRow();

            GridContentCell gridCellElement1 = new GridContentCell();
            GridContentCell gridCellElement2 = new GridContentCell();
            GridContentCell gridCellElement3 = new GridContentCell();
            GridContentCell gridCellElement4 = new GridContentCell();
            GridContentCell gridCellElement5 = new GridContentCell();
            GridContentCell gridCellElement6 = new GridContentCell();
            GridContentCell gridCellElement7 = new GridContentCell();


            // ss = device.Area;
            //cell 1
            gridCellElement1.Value = new UrlReference(@"Content/sleepy.jpg");
            gridRow1.Cells.Add(gridCellElement1);

            //cell 2
            // gridCellElement2.Left = 0;
            gridCellElement2.Value = new UrlReference(@"Content/sleepy.jpg");

            gridRow2.Cells.Add(gridCellElement2);

            //cell 3

            gridCellElement3.Value = new UrlReference(@"Content/sleepy.jpg");

            gridRow3.Cells.Add(gridCellElement3);

            //cell 4
            gridCellElement4.Value = "Content/sleepy.jpg";
            gridRow4.Cells.Add(gridCellElement4);

            //cell 5
            gridCellElement5.Value = "0";
            gridRow5.Cells.Add(gridCellElement5);

            //cell 6

            gridCellElement6.Value = new UrlReference(@"Content/sleepy.jpg");
            gridRow6.Cells.Add(gridCellElement6);

            //cell 7
            gridCellElement7.Value = new UrlReference(@"Content/Images/sleepy.jpg");
            gridRow7.Cells.Add(gridCellElement7);

            dataGridView1.Rows.Add(gridRow1);
            dataGridView1.Rows.Add(gridRow2);
            dataGridView1.Rows.Add(gridRow3);
            dataGridView1.Rows.Add(gridRow4);
            dataGridView1.Rows.Add(gridRow5);
            dataGridView1.Rows.Add(gridRow6);
            dataGridView1.Rows.Add(gridRow7);

            dataGridView1.RefreshData();
		}


	}
}
