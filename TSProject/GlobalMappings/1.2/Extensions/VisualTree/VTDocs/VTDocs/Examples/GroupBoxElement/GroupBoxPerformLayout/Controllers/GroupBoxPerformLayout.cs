using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxPerformLayoutController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLayout_Click(object sender, EventArgs e)
        {
            
            GroupBoxElement testedGroupBox = this.GetVisualElementById<GroupBoxElement>("testedGroupBox");
            LayoutEventArgs args = new LayoutEventArgs(testedGroupBox, "Some String");

            testedGroupBox.PerformLayout(args);
        }

        public void testedGroupBox_Layout(object sender, EventArgs e)
        {
            MessageBox.Show("TestedGroupBox Layout event method is invoked");
        }

    }
}