<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox PerformControlRemoved() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:button runat="server" Text="PerformControlRemoved of 'Tested GroupBox'" Top="45px" Left="140px" ID="btnPerformControlRemoved" Height="36px" Width="300px" ClickAction="GroupBoxPerformControlRemoved\btnPerformControlRemoved_Click"></vt:button>


        <vt:GroupBox runat="server" Text="Tested GroupBox" Top="150px" Left="200px" ID="testedGroupBox" Height="70px"  Width="200px" ControlRemovedAction="GroupBoxPerformControlRemoved\testedGroupBox_ControlRemoved"></vt:GroupBox>           


    </vt:WindowView>
</asp:Content>
