<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel DragOver Event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Panel runat="server" ShowHeader="False" AutoScroll="True" Top="100px" BorderWidth="1" Left="0px" ID="panel1" Draggable="true" BackColor="Lime" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="262px" Width="284px" DragOverAction="PanelDragOver\panel1_dragOver" ></vt:Panel>

        <vt:Label runat="server" ID="lblx" Width="40" Height="25" Top="0" Text="0" BackColor="LightBlue"></vt:Label>
        <vt:Label runat="server" ID="lbly" Width="40" Height="25" Top="40" Text="0" BackColor="LightBlue"></vt:Label>

    </vt:WindowView>
</asp:Content>
