﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Window Closing Event" Left="5px" ID="WindowView1" LoadAction="WindowClosing\OnLoad" >
    <%--<vt:WindowView runat="server" Text="Window Closing Event" Left="5px" ID="ClosingWindowView" LoadAction="WindowClosing\OnLoad" WindowClosingAction="WindowClosing\closing" WindowClosedAction="WindowClosing\Closed">--%>

        <vt:Label runat="server" SkinID="Title" Text="Closing" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs directly after Close is called, and can be handled to cancel window closure.
            
            Syntax: public event EventHandler<WindowClosingEventArgs> WindowClosing" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="195px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="In the following example, you can test the possibility to cancel the closing action of a test window. 
             Open the test window with the button, inside the test window you can cancel it's closing action  
             by checking the 'Cancel Closing Action' checkbox and then pressing 'Close This Window' button 
             or the window's close icon" Top="245px"  ID="lblExp1"></vt:Label>

        <vt:Button runat="server" Text="Open Test Window >>" Top="390px" Width="160px" ID="btnOpenTestWindow" ClickAction="WindowClosing\btnOpenTestWindow_Click"></vt:Button>

<%--        <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="390px" Width="360px" Height="100px" ID="txtEventLog"></vt:TextBox>

        <vt:CheckBox runat="server" Text="Cancel Closing Action" Top="550px" Left="300px" ID="chkCancelClosing" CheckedChangedAction="WindowClosing\chkCancelClosing_CheckedChanged"></vt:CheckBox>

        <vt:Button runat="server" Text="Close This Window >>" Top="550px" Width="160px" ID="btnChangeHeight" ClickAction="WindowClosing\btnCloseWindow_Click"></vt:Button>
--%>
    </vt:WindowView>
</asp:Content>