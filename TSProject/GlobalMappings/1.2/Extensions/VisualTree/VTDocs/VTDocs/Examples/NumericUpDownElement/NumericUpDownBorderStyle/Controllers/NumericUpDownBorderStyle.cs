using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown1 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown1");
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedNumericUpDown1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
        }

        public void btnChangeNumericUpDownBorderStyle_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown2 = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedNumericUpDown2.BorderStyle == BorderStyle.None)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Dotted)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Double)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Groove)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Inset)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Dashed)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.NotSet)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Outset)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Ridge)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else if (TestedNumericUpDown2.BorderStyle == BorderStyle.Solid)
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
            else
            {
                TestedNumericUpDown2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedNumericUpDown2.BorderStyle + '.';
            }
        }

    }
}