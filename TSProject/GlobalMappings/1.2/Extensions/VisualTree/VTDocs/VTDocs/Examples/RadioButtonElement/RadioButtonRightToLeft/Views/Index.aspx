<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton RightToLeft Property" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:RadioButton runat="server" BackColor="WhiteSmoke" Text="RightToLeft is set to 'Yes'" Top="55px" RightToLeft="Yes" Left="140px" ID="rdoRightToLeft" Height="36px" Width="200px"></vt:RadioButton>           

        <vt:Label runat="server" Text="RunTime" Top="210px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:RadioButton runat="server" BackColor="WhiteSmoke" Text="RuntimeRightToLeft" Top="235px" Left="140px" ID="rdoRuntimeRightToLeft" Height="36px" Width="200px" >           
        </vt:RadioButton>

        <vt:Button runat="server" Text="Change RadioButton RightToLeft" Top="240px" Left="400px" ID="btnChangeRightToLeft" Height="30px" Width="200px" ClickAction="RadioButtonRightToLeft\btnChangeRightToLeft_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="300px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        