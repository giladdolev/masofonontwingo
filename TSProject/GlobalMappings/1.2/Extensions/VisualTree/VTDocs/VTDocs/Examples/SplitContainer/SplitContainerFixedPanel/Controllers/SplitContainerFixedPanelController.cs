using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>

    public class SplitContainerFixedPanelController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "FixedPanel value: " + TestedSplitContainer1.FixedPanel;
            lblLog2.Text = "FixedPanel value: " + TestedSplitContainer2.FixedPanel + '.';

        }

        public void btnChangeFixedPanel_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedSplitContainer2.FixedPanel == FixedPanel.None)
            {
                TestedSplitContainer2.FixedPanel = FixedPanel.Panel1;
            }
            else if (TestedSplitContainer2.FixedPanel == FixedPanel.Panel1)
            {
                TestedSplitContainer2.FixedPanel = FixedPanel.Panel2;
            }
            else if (TestedSplitContainer2.FixedPanel == FixedPanel.Panel2)
            {
                TestedSplitContainer2.FixedPanel = FixedPanel.None;
            }

            lblLog2.Text = "FixedPanel value: " + TestedSplitContainer2.FixedPanel + '.';
        }

        public void btnDoubleWidthButton1_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");

            TestedSplitContainer1.Size = new System.Drawing.Size(400, 80);
        }

        public void btnResetWidthButton1_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");

            TestedSplitContainer1.Size = new System.Drawing.Size(200, 80);
        }

        public void btnDoubleWidthButton2_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");

            TestedSplitContainer2.Size = new System.Drawing.Size(400, 80);
        }

        public void btnResetWidthButton2_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");

            TestedSplitContainer2.Size = new System.Drawing.Size(200, 80);
        }

         
    }
}