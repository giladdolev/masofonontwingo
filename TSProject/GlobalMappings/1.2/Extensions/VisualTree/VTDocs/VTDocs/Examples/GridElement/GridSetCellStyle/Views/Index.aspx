﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SetCellStyle Method (Int, Int, String, Boolean)" ID="windowView1" LoadAction="GridSetCellStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SetCellStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the style of specified cell by the specified CSS class.
            
            Syntax: public void SetCellStyle(int rowIndex, int columnIndex, string cssClass, bool append = false)." Top="75px"  ID="lblDefinition"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Use the buttons below to set different styles to the grid cells using the SetCellStyle method " Top="245px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server"  Width="285px" Top="305px" Height="125px" ID="TestedGrid">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="445px" Width="450px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Invoke SetCellStyle(0,0,style1) >>" Left="50px" Width="200px" Top="510px" ID="btnInvokeSetCellStyle1" ClickAction="GridSetCellStyle\btnInvokeSetCellStyle1_Click"></vt:Button>

         <vt:Button runat="server" Text="Invoke SetCellStyle(1,1,style2) >>" Left="270px" Width="200px" Top="510px" ID="btnInvokeSetCellStyle2" ClickAction="GridSetCellStyle\btnInvokeSetCellStyle2_Click"></vt:Button>

         <vt:Button runat="server" Text="Invoke SetCellStyle(2,2,style3) >>" Left="490px" Width="200px" Top="510px" ID="btnInvokeSetCellStyle3" ClickAction="GridSetCellStyle\btnInvokeSetCellStyle3_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
