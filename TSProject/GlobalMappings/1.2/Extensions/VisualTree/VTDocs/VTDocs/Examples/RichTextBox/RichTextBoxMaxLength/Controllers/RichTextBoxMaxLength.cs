using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxMaxLengthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void Load(object sender, EventArgs e)
        {
            RichTextBox rtfMaxLength = this.GetVisualElementById<RichTextBox>("rtfMaxLength");
            LabelElement Label2 = this.GetVisualElementById<LabelElement>("Label2");
            Label2.Text += rtfMaxLength.MaxLength.ToString();
        }

        public void btnSetMaxLength_Click(object sender, EventArgs e)
        {
            RichTextBox rtfMaxLength = this.GetVisualElementById<RichTextBox>("rtfMaxLength");
            if (rtfMaxLength.MaxLength == 5)
                rtfMaxLength.MaxLength = 2;
            else
                rtfMaxLength.MaxLength = 5;
        }
        public void btnGetMaxLength_Click(object sender, EventArgs e)
        {
            RichTextBox rtfMaxLength = this.GetVisualElementById<RichTextBox>("rtfMaxLength");
            MessageBox.Show("MaxLength value: " + rtfMaxLength.MaxLength.ToString());
        }


    }
}