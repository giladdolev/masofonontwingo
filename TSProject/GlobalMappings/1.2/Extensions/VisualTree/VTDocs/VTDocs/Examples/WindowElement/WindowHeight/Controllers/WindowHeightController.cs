using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowHeightController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowHeight());
        }
        private WindowHeight ViewModel
        {
            get { return this.GetRootVisualElement() as WindowHeight; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "this.Height value: " + this.ViewModel.Height + '.';
        }
      

        public void btnChangeHeight_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
           if(this.ViewModel.Height == 705)
           {
               this.ViewModel.Height = 500;
               lblLog.Text = "this.Height value: " + this.ViewModel.Height + '.';
           }
           else
           {
               this.ViewModel.Height = 705;
               lblLog.Text = "this.Height value: " + this.ViewModel.Height + '.';

           }
        }
    }
}