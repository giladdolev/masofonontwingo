<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown ForeColor property" ID="windowView1" LoadAction="NumericUpDownForeColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ForeColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the foreground color of the control.

            Syntax: public virtual Color ForeColor { get; set; }
            The default is the value of the DefaultForeColor property - 'SystemColors.ControlText'.
            "
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested NumericUpDown1 -->
        <vt:NumericUpDown runat="server" Text="NumericUpDown" Top="165px" ID="TestedNumericUpDown1"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="200px" BorderStyle="None" ID="lblLog1"></vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="ForeColor property of this NumericUpDown is initially set to Green" Top="330px" ID="lblExp1"></vt:Label>

        <!-- Tested NumericUpDown2 -->
        <vt:NumericUpDown runat="server" Text="TestedNumericUpDown" Top="365px" ForeColor="Green" ID="TestedNumericUpDown2"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="400px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change ForeColor value >>" Top="480px" Width="180px" ID="btnChangeNumericUpDownForeColor" ClickAction="NumericUpDownForeColor\btnChangeNumericUpDownForeColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
