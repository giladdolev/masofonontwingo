using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Change BorderStyle
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BorderStyle value: " + TestedLabel1.BorderStyle;

            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BorderStyle value: " + TestedLabel2.BorderStyle + ".";

        }


        public void btnChangeBorderStyle_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel2.BorderStyle == BorderStyle.None)
            {
                TestedLabel2.BorderStyle = BorderStyle.Dotted;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Dotted)
            {
                TestedLabel2.BorderStyle = BorderStyle.Double;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Double)
            {
                TestedLabel2.BorderStyle = BorderStyle.Fixed3D;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedLabel2.BorderStyle = BorderStyle.FixedSingle;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedLabel2.BorderStyle = BorderStyle.Groove;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Groove)
            {
                TestedLabel2.BorderStyle = BorderStyle.Inset;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Inset)
            {
                TestedLabel2.BorderStyle = BorderStyle.Dashed;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Dashed)
            {
                TestedLabel2.BorderStyle = BorderStyle.NotSet;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.NotSet)
            {
                TestedLabel2.BorderStyle = BorderStyle.Outset;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Outset)
            {
                TestedLabel2.BorderStyle = BorderStyle.Ridge;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Ridge)
            {
                TestedLabel2.BorderStyle = BorderStyle.ShadowBox;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedLabel2.BorderStyle = BorderStyle.Solid;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Solid)
            {
                TestedLabel2.BorderStyle = BorderStyle.Underline;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Underline)
            {
                TestedLabel2.BorderStyle = BorderStyle.None;
            }
            lblLog2.Text = "BorderStyle value: " + TestedLabel2.BorderStyle + ".";
        }

        //Change BorderStyle 
 /*       private void btnBorderStyle_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            if (TestedLabel2.BorderStyle == BorderStyle.None)
            {
                lblLog2.Text = "BorderStyle.Dotted";
                TestedLabel2.BorderStyle = BorderStyle.Dotted;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Dotted)
            {
                lblLog2.Text = "BorderStyle.Double";
                TestedLabel2.BorderStyle = BorderStyle.Double;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Double)
            {
                lblLog2.Text = "BorderStyle.Fixed3D";
                TestedLabel2.BorderStyle = BorderStyle.Fixed3D;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Fixed3D)
            {
                lblLog2.Text = "BorderStyle.FixedSingle";
                TestedLabel2.BorderStyle = BorderStyle.FixedSingle;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.FixedSingle)
            {
                lblLog2.Text = "BorderStyle.Groove";
                TestedLabel2.BorderStyle = BorderStyle.Groove;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Groove)
            {
                lblLog2.Text = "BorderStyle.Inset";
                TestedLabel2.BorderStyle = BorderStyle.Inset;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Inset)
            {
                lblLog2.Text = "BorderStyle.Dashed";
                TestedLabel2.BorderStyle = BorderStyle.Dashed;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Dashed)
            {
                lblLog2.Text = "BorderStyle.NotSet";
                TestedLabel2.BorderStyle = BorderStyle.NotSet;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.NotSet)
            {
                lblLog2.Text = "BorderStyle.Outset";
                TestedLabel2.BorderStyle = BorderStyle.Outset;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Outset)
            {
                lblLog2.Text = "BorderStyle.Ridge";
                TestedLabel2.BorderStyle = BorderStyle.Ridge;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Ridge)
            {
                lblLog2.Text = "BorderStyle.ShadowBox";
                TestedLabel2.BorderStyle = BorderStyle.ShadowBox;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.ShadowBox)
            {
                lblLog2.Text = "BorderStyle.Solid";
                TestedLabel2.BorderStyle = BorderStyle.Solid;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Solid)
            {
                lblLog2.Text = "BorderStyle.Underline";
                TestedLabel2.BorderStyle = BorderStyle.Underline;
            }
            else if (TestedLabel2.BorderStyle == BorderStyle.Underline)
            {
                lblLog2.Text = "BorderStyle.None";
                TestedLabel2.BorderStyle = BorderStyle.None;
            }
        }
  */
    }
}