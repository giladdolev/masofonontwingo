<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Height Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="button3 TabStop is set to false" Top="30px" Left="200px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="200px"></vt:Label>

        <vt:Button runat="server" Text="button1" Top="60px" Left="10px" ID="button1" Height="36px" TabIndex="1" Width="100px"></vt:Button>    

        <vt:Button runat="server" Text="button2" Top="60px" Left="120px" ID="button2" Height="36px" TabIndex="2"  Width="100px"></vt:Button>

        <vt:Button runat="server" Text="button3" Top="60px" Left="230px" ID="button3" Height="36px" TabIndex="3" TabStop="false" Width="100px"></vt:Button>    

        <vt:Button runat="server" Text="button4" Top="60px" Left="340px" ID="button4" Height="36px"  TabIndex="4" Width="100px"></vt:Button>

        <vt:Button runat="server" Text="button5" Top="60px" Left="450px" ID="button5" Height="36px" TabIndex="5" Width="100px"></vt:Button> 
        
         <vt:Button runat="server" Text="Change buttons TabIndex" Top="110px" Left="100px" TabStop ="false" ID="btnChangeTabIndex" Height="36px" Width="160px" ClickAction="ButtonTabIndexTabStop\btnChangeTabIndex_Click"></vt:Button> 
        
         <vt:Button runat="server" Text="Change button3 TabStop" Top="110px" Left="300px" ID="btnChangeTabStop" Height="36px"  TabStop ="false" Width="160px" ClickAction="ButtonTabIndexTabStop\btnChangeTabStop_Click"></vt:Button>    

            </vt:WindowView>
</asp:Content>
