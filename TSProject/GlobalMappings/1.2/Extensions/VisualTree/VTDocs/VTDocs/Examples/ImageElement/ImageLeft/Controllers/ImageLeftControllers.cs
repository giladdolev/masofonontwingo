using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ImageLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Image Left Value is: " + TestedImage.Left + '.';
        }
        public void ChangeLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ImageElement TestedImage = this.GetVisualElementById<ImageElement>("TestedImage");
            if (TestedImage.Left == 200)
            {
                TestedImage.Left = 80;
                lblLog.Text = "Image Left Value is: " + TestedImage.Left + '.';

            }
            else
            {
                TestedImage.Left = 200;
                lblLog.Text = "Image Left Value is: " + TestedImage.Left + '.';
            }
        }

    }
}