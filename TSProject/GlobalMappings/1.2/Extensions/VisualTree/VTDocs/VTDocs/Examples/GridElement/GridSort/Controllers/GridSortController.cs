using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridSortController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void Page_load(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedGrid1.Rows.Add("a", "b", "c");
            TestedGrid1.Rows.Add("d", "e", "f");
            TestedGrid1.Rows.Add("g", "h", "i");
            lblLog1.Text = "Grid Column 1 Sort Order: " + TestedGrid1.Columns[0].SortOrder.ToString();
        }


        public void btn_click(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedGrid1.Columns[0].SortOrder == SortOrder.Descending)
            {
                TestedGrid1.Sort(TestedGrid1.Columns[0], SortOrder.Ascending);
            }
            else 
            { 
                TestedGrid1.Sort(TestedGrid1.Columns[0], SortOrder.Descending);
            }
            lblLog1.Text = "Grid Column 1 Sort Order: " + TestedGrid1.Columns[0].SortOrder.ToString();
        }

        public void TestedGrid_HeaderClick(object sender, GridElementCellEventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedGrid1.Columns[e.ColumnIndex - 1].SortOrder == SortOrder.Ascending)
            {
                TestedGrid1.Columns[e.ColumnIndex - 1].SortOrder = SortOrder.Descending;
            }
            else
            {
                TestedGrid1.Columns[e.ColumnIndex - 1].SortOrder = SortOrder.Ascending;
            }

            lblLog1.Text = "Grid Column " + e.ColumnIndex.ToString() + " Sort Order: " + TestedGrid1.Columns[e.ColumnIndex - 1].SortOrder.ToString();
        }
    }
}
