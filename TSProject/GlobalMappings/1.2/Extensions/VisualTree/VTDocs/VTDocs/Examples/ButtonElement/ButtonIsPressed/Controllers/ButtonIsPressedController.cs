using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonIsPressedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton1");
            ButtonElement TestedCheckButton = this.GetVisualElementById<ButtonElement>("TestedButton2");

            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Click the Button... \\r\\nEnableToggle = '" + TestedButton.EnableToggle + "', IsPressed = '" + TestedButton.IsPressed + "'";
            lblLog2.Text = "Click the TestedButton... \\r\\nEnableToggle = " + TestedCheckButton.EnableToggle + ", IsPressed = " + TestedCheckButton.IsPressed;
        }

        public void TestedButton1_PressedChange(object sender, ButtonPressedChangeEventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ButtonElement TestedButton = this.GetVisualElementById<ButtonElement>("TestedButton1");

            lblLog1.Text = "PressedChange event is invoked. \\r\\nEnableToggle = '" + TestedButton.EnableToggle + "', IsPressed = '" + TestedButton.IsPressed + "'";
        }

        public void TestedButton2_PressedChange(object sender, ButtonPressedChangeEventArgs e)
        {
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ButtonElement TestedCheckButton = this.GetVisualElementById<ButtonElement>("TestedButton2");

            lblLog2.Text = "PressedChange event is invoked. \\r\\nEnableToggle = " + TestedCheckButton.EnableToggle + ", IsPressed = " + TestedCheckButton.IsPressed;
        }

        public void btnToggleIsPressed_Click(object sender, EventArgs e)
        {
            ButtonElement TestedCheckButton = this.GetVisualElementById<ButtonElement>("TestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedCheckButton.IsPressed = !TestedCheckButton.IsPressed;

            lblLog2.Text = "Click the TestedButton... \\r\\nEnableToggle = " + TestedCheckButton.EnableToggle.ToString() + ", IsPressed = " + TestedCheckButton.IsPressed.ToString();
        }
    }
}