﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelEnabledController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Enabled Value: " + TestedLabel1.Enabled;
            lblLog2.Text = "Enabled Value: " + TestedLabel2.Enabled + '.';
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel2.Enabled == true)
            {
                TestedLabel2.Enabled = false;
                lblLog2.Text = "Enabled Value: " + TestedLabel2.Enabled + '.';
            }
            else
            {
                TestedLabel2.Enabled = true;
                lblLog2.Text = "Enabled Value: " + TestedLabel2.Enabled + '.';
            }
        }
    }
}