using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarToolBarMenuItemVisibleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "New MenuItem Visible value: " + TestedToolBar1.Items[0].Visible.ToString();
            lblLog1.Text += "\\r\\nOpen MenuItem Visible value: " + TestedToolBar1.Items[2].Visible.ToString();

            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "New MenuItem Visible value: " + TestedToolBar2.Items[0].Visible.ToString() + '.';
            lblLog2.Text += "\\r\\nOpen MenuItem Visible value: " + TestedToolBar2.Items[2].Visible.ToString() + '.';
        }

        private void btnChangeVisible_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ComboBoxElement cmbSelectItem = this.GetVisualElementById<ComboBoxElement>("cmbSelectItem");
            CheckBoxElement chkVisible = this.GetVisualElementById<CheckBoxElement>("chkVisible");

            switch (cmbSelectItem.Text)
            {
                case "New":
                    TestedToolBar2.Items[0].Visible = chkVisible.IsChecked;
                    PrintAllColumnsReadOnly();
                    break;
                case "Open":
                    TestedToolBar2.Items[2].Visible = chkVisible.IsChecked;
                    PrintAllColumnsReadOnly();
                    break;
                default:
                    lblLog2.Text = "Please Select a MenuItem from the first ComboBox";
                    break;
            }

        }

        private void PrintAllColumnsReadOnly()
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "New MenuItem Visible value: " + TestedToolBar2.Items[0].Visible.ToString() + '.';
            lblLog2.Text += "\\r\\nOpen MenuItem Visible value: " + TestedToolBar2.Items[2].Visible.ToString() + '.';

        }

    }
}