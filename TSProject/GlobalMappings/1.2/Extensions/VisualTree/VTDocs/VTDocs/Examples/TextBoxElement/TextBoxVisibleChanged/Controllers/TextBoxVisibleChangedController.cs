using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TextBoxAlignment
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TextBoxElement txtVisible = this.GetVisualElementById<TextBoxElement>("txtVisible");

            if (txtVisible.Visible == true) //Get
            {
                txtVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + txtVisible.Visible;
            }
            else
            {
                txtVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + txtVisible.Visible;
            }
        }

      //  txtText_TextChanged
        public void txtVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}