using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxTextAlignController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Set TextAlign property
        private void btnSetTextAlign_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkTextAlign = this.GetVisualElementById<CheckBoxElement>("chkTextAlign");

            if (chkTextAlign.TextAlign == ContentAlignment.MiddleLeft) //Get
            {
                chkTextAlign.TextAlign = ContentAlignment.MiddleRight;//Set
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else if (chkTextAlign.TextAlign == ContentAlignment.MiddleRight)
            {
                chkTextAlign.TextAlign = ContentAlignment.MiddleCenter;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else if (chkTextAlign.TextAlign == ContentAlignment.MiddleCenter)
            {
                chkTextAlign.TextAlign = ContentAlignment.BottomCenter;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else if (chkTextAlign.TextAlign == ContentAlignment.BottomCenter)
            {
                chkTextAlign.TextAlign = ContentAlignment.BottomLeft;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else if (chkTextAlign.TextAlign == ContentAlignment.BottomLeft)
            {
                chkTextAlign.TextAlign = ContentAlignment.BottomRight;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else if (chkTextAlign.TextAlign == ContentAlignment.BottomRight)
            {
                chkTextAlign.TextAlign = ContentAlignment.TopCenter;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else if (chkTextAlign.TextAlign == ContentAlignment.TopCenter)
            {
                chkTextAlign.TextAlign = ContentAlignment.TopLeft;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else if (chkTextAlign.TextAlign == ContentAlignment.TopLeft)
            {
                chkTextAlign.TextAlign = ContentAlignment.TopRight;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }
            else
            {
                chkTextAlign.TextAlign = ContentAlignment.MiddleLeft;
                chkTextAlign.Text = "TextAlign - " + chkTextAlign.TextAlign.ToString();
            }

            
            
           

        }

        //Get TextAlign property
        private void btnGetTextAlign_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkTextAlign = this.GetVisualElementById<CheckBoxElement>("chkTextAlign");
            MessageBox.Show("TextAlign mode: " + chkTextAlign.TextAlign.ToString());
        }
    }
}