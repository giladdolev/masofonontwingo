

<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Image Property" ID="windowView2" LoadAction="ButtonImage\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Image" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the image that is displayed on a button control.
            The default value is null.
            
            Syntax: public Image Image { get; set; }" Top="75px" ID="lblDefinition" ></vt:Label>
         
        

        <vt:Button runat="server" Text="Button" SkinID="Wide" Top="180px" ID="btnTestedButton1" ></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="230px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Image property of this 'TestedButton' is initially set to Button.png" Top="350px"  ID="lblExp1"></vt:Label>     

        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Image ="~/Content/Elements/Button.png" Top="400px" ID="btnTestedButton2"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Image >>"  Top="505px" Width="180px" ID="btnChangeImage" ClickAction="ButtonImage\btnChangeImage_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>

