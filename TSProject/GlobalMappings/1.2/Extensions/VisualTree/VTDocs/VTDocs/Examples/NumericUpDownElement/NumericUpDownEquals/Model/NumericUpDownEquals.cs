﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class NumericUpDownEquals : WindowElement
    {

        NumericUpDownElement _TestedNumericUpDown;

        //Add the dynamic NumericUpDown that created on the form
        public NumericUpDownEquals()
        {
            _TestedNumericUpDown = new NumericUpDownElement();
        }

        public NumericUpDownElement TestedNumericUpDown
        {
            get { return this._TestedNumericUpDown; }
            set { this._TestedNumericUpDown = value; }
        }
    }
}