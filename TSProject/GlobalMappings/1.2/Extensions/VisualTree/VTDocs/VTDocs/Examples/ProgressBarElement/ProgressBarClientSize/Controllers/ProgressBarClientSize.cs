using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedProgressBar.ClientSize;

        }
        public void btnChangeProgressBarClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            if (TestedProgressBar.Width == 200)
            {
                TestedProgressBar.ClientSize = new Size(250, 40);
                lblLog.Text = "ClientSize value: " + TestedProgressBar.ClientSize;

            }
            else
            {
                TestedProgressBar.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedProgressBar.ClientSize;
            }

        }

    }
}