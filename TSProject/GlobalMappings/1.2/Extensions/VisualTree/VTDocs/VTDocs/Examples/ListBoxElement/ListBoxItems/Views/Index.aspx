<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListBox Items Property" ID="windowView2" LoadAction="ListBoxItems\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Items" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets a collection containing all items in the control.

            Syntax: public ListItemCollection Items { get; }" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, pressing the 'Add Item' button will add a new item to the ListBox. 
            Pressing the 'Remove Item' button will remove an item from the end of the ListBox." Top="230px" ID="lblExp2"></vt:Label>

        <vt:ListBox runat="server" Text="TestedListBox" Top="310px" Height="170px" ID="TestedListBox">
           <Items>
                <vt:ListItem runat="server" Text="Item1" Value="1"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item2" Value="2"></vt:ListItem>
                <vt:ListItem runat="server" Text="Item3" Value="3"></vt:ListItem>
            </Items>
        </vt:ListBox>

        <vt:Label runat="server" SkinID="Log" Width="355px" ID="lblLog" Top="490"></vt:Label>

        <vt:Button runat="server" Text="Remove Item >>" Top="560px" Left="300px" ID="btnRemoveItem" ClickAction="ListBoxItems\RemoveItems_Click"></vt:Button>

        <vt:Button runat="server" Text="Add Item >>" Top="560px"  ID="btnAddItem" ClickAction="ListBoxItems\AddItems_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
