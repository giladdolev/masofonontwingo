<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Controls Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
       

        <vt:Tab runat="server"  Top="50px"  Left="140px" ID="tabControls" Height="200px" Width="250px">
             <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>    

        <vt:Button runat="server" Text="Add Controls" Top="195px" Left="420px" ID="btnAddControls" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="TabControls\btnAddControls_Click"></vt:Button>



    </vt:WindowView>
</asp:Content>
        