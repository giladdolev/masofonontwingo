<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Hexadecimal Property" Top="57px" Left="11px" ID="windowView1" Height="700px" Width="768px">

        <vt:Label runat="server" Text="Hexadecimal is initially set to true" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:NumericUpDown runat="server" Hexadecimal="true" Top="97px" Left="250px" ID="numericUpDown1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="20px" Width="100px">
        </vt:NumericUpDown>

        <vt:Button runat="server" Text="Change Hexadecimal" Top="70px" Left="20px" ID="btnChangeHexadecimal" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="NumericUpDownHexadecimal\btnChangeHexadecimal_Click"></vt:Button>

        <vt:Button runat="server" Text="Get Hexadecimal" Top="110px" Left="20px" ID="btnGetHexadecimal" Height="26px" TabIndex="1" Width="200px" 
            ClickAction="NumericUpDownHexadecimal\btnGetHexadecimalValue_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
