<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid SelectedValue Property" ID="windowView1" LoadAction="ComboGridSelectedValue\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="SelectedValue" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the value of the member property specified by the ValueMember property.

            Syntax: public object SelectedValue { get; set; }            
            If a property is not specified in ValueMember, SelectedValue returns the results of the ToString method 
            of the object." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:ComboGrid runat="server" Text="ComboGrid" ID="TestedComboGrid1"  Top="195px"> </vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="235px" ID="lblLog1"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="320px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the SelectedValue property by selecting an item 
              from the ComboGrid or by pressing the 'Change SelectedValue' button.
              pressing the button will set the SelectedValue property, each click selects a different item, 
              in a cyclic order. The initial SelectedValue value is 'James'." 
            Top="370px" ID="lblExp2"></vt:Label>

        <vt:ComboGrid runat="server" Text="TestedComboGrid" CssClass="vt-TestedComboGrid" SelectedValue="James" Top="480px" ID="TestedComboGrid2" SelectedValueChangedAction="ComboGridSelectedValue\SelectedValueChanged"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog2" Top="520px"></vt:Label>

        <vt:Button runat="server" Text="Change SelectedValue >>" Top="610px" ID="btnChooseNext" Width="180px" ClickAction="ComboGridSelectedValue\ChooseNext_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

