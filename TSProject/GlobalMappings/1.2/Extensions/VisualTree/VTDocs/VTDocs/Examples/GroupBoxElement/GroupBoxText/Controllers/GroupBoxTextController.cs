using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement btnTestedGroupBox = this.GetVisualElementById<GroupBoxElement>("btnTestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Text  property value: " + btnTestedGroupBox.Text;

        }

        public void btnChangeGroupBoxText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GroupBoxElement btnTestedGroupBox = this.GetVisualElementById<GroupBoxElement>("btnTestedGroupBox");
            if (btnTestedGroupBox.Text == "TestedGroupBox")
            {
                btnTestedGroupBox.Text = "New Text";
                lblLog.Text = "Text  property value: " + btnTestedGroupBox.Text;

            }
            else
            {
                btnTestedGroupBox.Text = "TestedGroupBox";
                lblLog.Text = "Text  property value: " + btnTestedGroupBox.Text;
            }
        }

    }
}