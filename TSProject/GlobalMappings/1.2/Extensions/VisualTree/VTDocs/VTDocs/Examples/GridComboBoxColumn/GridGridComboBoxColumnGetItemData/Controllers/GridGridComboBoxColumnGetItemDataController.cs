﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;

namespace MvcApplication9.Controllers
{
    public class GridGridComboBoxColumnGetItemDataController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

       
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.AddToComboBoxList("col2", "male,female");
            TestedGrid.AddToComboBoxList("col3", "Math,English,History,Chemistry,Biology");

            TestedGrid.Rows.Add("Yaakov", "male", "History", false);
            TestedGrid.Rows.Add("Sara", "female", "Math", true);
            TestedGrid.Rows.Add("Eli", "male", "English", true);

            (TestedGrid.Columns[1] as GridComboBoxColumn).SetItemData(0, 1);
            (TestedGrid.Columns[1] as GridComboBoxColumn).SetItemData(1, 2);

            (TestedGrid.Columns[2] as GridComboBoxColumn).SetItemData(0, 54);
            (TestedGrid.Columns[2] as GridComboBoxColumn).SetItemData(1, 42);
            (TestedGrid.Columns[2] as GridComboBoxColumn).SetItemData(2, 36);
            (TestedGrid.Columns[2] as GridComboBoxColumn).SetItemData(3, 13);
            (TestedGrid.Columns[2] as GridComboBoxColumn).SetItemData(4, 75);

        }

        private void btnGetItemData_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbSelectPupil = this.GetVisualElementById<ComboBoxElement>("cmbSelectPupil");

            int ind = -1;
            bool skip = false;

            switch (cmbSelectPupil.Text)
            {
                case "Yaakov":
                    ind = 0;
                    break;
                case "Sara":
                    ind = 1;
                    break;
                case "Eli":
                    ind = 2;
                    break;
                default:
                    lblLog.Text = "Please Select a Pupil from the top ComboBox";
                    skip = true;
                    break;
            }

            if (!skip)
            {
                lblLog.Text = "Pupil name: " + TestedGrid.Rows[ind].Cells[0].Value + ", Gender: " + TestedGrid.Rows[ind].Cells[1].Value + ", g-code: ";

                if (String.Compare(TestedGrid.Rows[ind].Cells[1].Value.ToString(), "male") == 0)
                {
                    lblLog.Text += (TestedGrid.Columns[1] as GridComboBoxColumn).GetItemData(0);
                }
                else if (String.Compare(TestedGrid.Rows[ind].Cells[1].Value.ToString(), "female") == 0)
                {
                    lblLog.Text += (TestedGrid.Columns[1] as GridComboBoxColumn).GetItemData(1);
                }

                lblLog.Text += "\\r\\nClass: " + TestedGrid.Rows[ind].Cells[2].Value + ", c-code: ";

                if (String.Compare(TestedGrid.Rows[ind].Cells[2].Value.ToString(), "Math") == 0)
                {
                    lblLog.Text += (TestedGrid.Columns[2] as GridComboBoxColumn).GetItemData(0);
                }
                else if (String.Compare(TestedGrid.Rows[ind].Cells[2].Value.ToString(), "English") == 0)
                {
                    lblLog.Text += (TestedGrid.Columns[2] as GridComboBoxColumn).GetItemData(1);
                }
                else if (String.Compare(TestedGrid.Rows[ind].Cells[2].Value.ToString(), "History") == 0)
                {
                    lblLog.Text += (TestedGrid.Columns[2] as GridComboBoxColumn).GetItemData(2);
                }
                else if (String.Compare(TestedGrid.Rows[ind].Cells[2].Value.ToString(), "Chemistry") == 0)
                {
                    lblLog.Text += (TestedGrid.Columns[2] as GridComboBoxColumn).GetItemData(3);
                }
                else if (String.Compare(TestedGrid.Rows[ind].Cells[2].Value.ToString(), "Biology") == 0)
                {
                    lblLog.Text += (TestedGrid.Columns[2] as GridComboBoxColumn).GetItemData(4);
                }

                lblLog.Text += ", Passed Status: " + TestedGrid.Rows[ind].Cells[3].Value;
            }
        }

        public void btnSetItemData_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cmbSelectClass = this.GetVisualElementById<ComboBoxElement>("cmbSelectClass");
            TextBoxElement ClassCode = this.GetVisualElementById<TextBoxElement>("ClassCode");

            int ind = -1;
            bool skip = false;
            int inputClassCode = -1;

            if (int.TryParse(ClassCode.Text, out inputClassCode))
            {
                switch (cmbSelectClass.Text)
                {
                    case "Math":
                        ind = 0;
                        break;
                    case "English":
                        ind = 1;
                        break;
                    case "History":
                        ind = 2;
                        break;
                    case "Chemistry":
                        ind = 3;
                        break;
                    case "Biology":
                        ind = 4;
                        break;
                    default:
                        lblLog.Text = "Please Select a Class from the bottom ComboBox";
                        skip = true;
                        break;
                }
            }
            else
            {
                lblLog.Text = "Please type a numeric c-code value.";
                skip = true;
            }

            if (!skip)
            {
                (TestedGrid.Columns[2] as GridComboBoxColumn).SetItemData(ind, inputClassCode);

                lblLog.Text = "Class code was successfully changed.";
            }
        }      
           

    }
}