using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowSizeGripStyleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowSizeGripStyle());
        }
        private WindowSizeGripStyle ViewModel
        {
            get { return this.GetRootVisualElement() as WindowSizeGripStyle; }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
		{
            this.ViewModel.Left = 10;
            
            if (this.ViewModel.TestedWin == null)
            {
                Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
                argsDictionary.Add("ElementName", "WindowElement");
                argsDictionary.Add("ExampleName", "WindowSizeGripStyle");

                ViewModel.TestedWin = VisualElementHelper.CreateFromView<SizeGripStyleTestedWindow>("SizeGripStyleTestedWindow", "Index2", null, argsDictionary, null);
                this.ViewModel.TestedWin.Show();
                this.ViewModel.TestedWin.Top = 400;
                this.ViewModel.TestedWin.Width = 1000;
                this.ViewModel.TestedWin.Height = 400;
            }
            else if(!this.ViewModel.TestedWin.Visible)
                 this.ViewModel.TestedWin.Show();
            else
            {
                this.ViewModel.TestedWin.Close();
                this.ViewModel.TestedWin.Show();
            }
		}

        

    }
}