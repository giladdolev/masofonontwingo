<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Font Property" ID="windowView2" LoadAction="TabFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Tab runat="server" Text="Tab" Top="180px" ID="TestedTab1" >
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tab1Page1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tab1Page2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tab1Page3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Label runat="server" SkinID="Log" Top="295px" Width="430px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="375px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedTab is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="410px"  ID="lblExp1"></vt:Label>     
              
        <vt:Tab runat="server" Text="TestedTab" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="465px" ID="TestedTab2" >
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tab2Page1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tab2Page2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tab2Page3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="580px" Width="430px" Height="45px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="650px" ID="btnChangeTabFont" ClickAction="TabFont/btnChangeTabFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
