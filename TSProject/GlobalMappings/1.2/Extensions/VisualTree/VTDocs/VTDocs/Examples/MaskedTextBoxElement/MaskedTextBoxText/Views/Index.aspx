﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="MaskedTextBox Text"  ID="windowView1" LoadAction="MaskedTextBoxText\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="250px" Text="MaskedTextBox Text" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text as it is currently displayed to the user.
            
            Syntax: public override string Text { get; set; }
             Property Value: A String containing the text currently displayed by the control. 
            The default is an empty string.
            
            Strings retrieved using this property are formatted according to the control's
             formatting properties, such as Mask and TextMaskFormat."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="270px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can examine the MaskedTextBox property using the textBox and button
             below or simply by entering text in this MaskedTextBox.
            The Text value is according to the default value of TextMaskFormat - 'IncludeLiterals'
            and the Mask that is set to - 'LLL-LLL'"
            Top="320px" ID="lblExp1">
        </vt:Label>

        <vt:MaskedTextBox runat="server" ID="maskedTextBox1" CssClass="vt-Msk1" Text="abc" Mask="LLL-LLL" Top="435px"
            TextChangedAction="MaskedTextBoxText\maskedTextBox1_TextChanged">
        </vt:MaskedTextBox>      

        <vt:Label runat="server" SkinID="Log" Top="470px" Width="400px" Height="40px" ID="lblLog"></vt:Label>

        <vt:Label runat="server" Top="570px" Text="Enter Text: " ID="Label1"></vt:Label>

        <vt:TextBox runat="server" ID="textBox2" CssClass="vt-txt1" Left="130px" Top="570px" Height="20px" Width="180px" TextChangedAction="MaskedTextBoxText\textBox2_TextChanged"></vt:TextBox>

        <vt:Button runat="server" ID="btnChangeText" Left="360px" Text="Change Text" Top="570px" ClickAction="MaskedTextBoxText\btnChangeText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
