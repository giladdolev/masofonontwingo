using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPerformGotFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformGotFocus_Click(object sender, EventArgs e)
        {
            
            PanelElement testedPanel = this.GetVisualElementById<PanelElement>("testedPanel");

            testedPanel.PerformGotFocus(e);
        }

        public void testedPanel_GotFocus(object sender, EventArgs e)
        {
            MessageBox.Show("TestedPanel GotFocus event method is invoked");
        }

    }
}