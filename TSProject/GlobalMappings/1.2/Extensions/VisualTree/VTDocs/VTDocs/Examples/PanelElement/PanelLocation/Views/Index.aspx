<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Location Property" ID="windowView" LoadAction="PanelLocation\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Location" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the coordinates of the upper-left corner of the control 
            relative to the upper-left corner of its container.

            Syntax: public Point Location { get; set; }" Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The Location of this Panel is initially set with 
            Left: 80px and Top: 325px" Top="250px"  ID="lblExp1"></vt:Label>     

         <!-- TestedPanel -->
        <vt:Panel runat="server" Text="TestedPanel" Top="325px" ID="TestedPanel"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="420px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Panel Location >>"  Top="485px" Width="180px" ID="btnChangePanelLocation" ClickAction="PanelLocation\btnChangePanelLocation_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>
