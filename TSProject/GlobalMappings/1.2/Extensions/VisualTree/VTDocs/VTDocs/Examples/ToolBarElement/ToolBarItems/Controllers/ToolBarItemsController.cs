using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarItemsController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnAddToolBarLabel_Click(object sender, EventArgs e)
		{
            ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            //ToolBarLabel Toolbarlabel1 = this.GetVisualElementById<ToolBarLabel>("toolStripLabel1");
            ToolBarLabel Toolbarlabel2 = new ToolBarLabel();
            Toolbarlabel2.Text = "toolbarlabel1";
            toolStrip1.Items.Add(Toolbarlabel2);  
		}

        public void btnAddToolBarButton_Click(object sender, EventArgs e)
		{
            ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            ToolBarButton ToolBarButton1 = this.GetVisualElementById<ToolBarButton>("toolStripButton1");
            ToolBarButton1.Text = "toolBarButton1";
            toolStrip1.Items.Add(ToolBarButton1);
             
		}

        public void btnAddToolBarDropDownButton_Click(object sender, EventArgs e)
        {
            ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            ToolBarDropDownButton ToolBarDropDownButton1 = this.GetVisualElementById<ToolBarDropDownButton>("toolStripDropDownButton1");
            ToolBarDropDownButton1.Text = "toolBarDropDownButton1";
            toolStrip1.Items.Add(ToolBarDropDownButton1);
        }

        //Added ToolBarItem in run time related to bug 9286 - Please verify that this bug isn't 
        //Verify that when clicking the button "Add ToolBarItems" items are added without quotation marks - ' '
        public void btnAddToolBarItem_Click(object sender, EventArgs e)
        {
            ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            ToolBarItem ToolStripItem1 = this.GetVisualElementById<ToolBarItem>("toolStripItem1");
            ToolStripItem1.Text = "ToolStripItem1";
            toolStrip1.Items.Add(ToolStripItem1);
        }
    }
}