using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicTabElementController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void SelectedTabIndex_Click(object sender, EventArgs e)
        {
            TabElement tabPage1 = this.GetVisualElementById<TabElement>("tabControl1");
            ButtonElement Button1 = this.GetVisualElementById<ButtonElement>("Button1");
            Button1.Text = "SelectedIndex = " + tabPage1.SelectedTab.Index;
        }

        public void ChangeTabsText_Click(object sender, EventArgs e)
        {
            TabItem tabPage1 = this.GetVisualElementById<TabItem>("tabPage1");
            TabItem tabPage2 = this.GetVisualElementById<TabItem>("tabPage2");
            if (tabPage1.Text == "tabPage1")
            {
                tabPage1.Text = "NewText";
                tabPage2.Text = "NewText";
            }
            else
            {
                tabPage1.Text = "tabPage1";
                tabPage2.Text = "tabPage2";
            }
        }
    }
}