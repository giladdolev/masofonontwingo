using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelAutoSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void Form_Load(object sender, EventArgs e)
        {
            TextBoxElement txtAutoSizeValue = this.GetVisualElementById<TextBoxElement>("txtAutoSizeValue");
            PanelElement pnlRuntimeAutoSize = this.GetVisualElementById<PanelElement>("pnlRuntimeAutoSize");
            txtAutoSizeValue.Text = "Panel is set to AutoSize " + pnlRuntimeAutoSize.AutoSize.ToString(); 

        }
        public void btnChangeAutoSize_Click(object sender, EventArgs e)
        {
            TextBoxElement txtAutoSizeValue = this.GetVisualElementById<TextBoxElement>("txtAutoSizeValue");
            PanelElement pnlRuntimeAutoSize = this.GetVisualElementById<PanelElement>("pnlRuntimeAutoSize");

            if (pnlRuntimeAutoSize.AutoSize == false)
            {
                pnlRuntimeAutoSize.AutoSize = true;
                txtAutoSizeValue.Text = "Panel is set to AutoSize " + pnlRuntimeAutoSize.AutoSize;
            }
            else
            {
                pnlRuntimeAutoSize.AutoSize = false;
                txtAutoSizeValue.Text = "Panel is set to AutoSize " + pnlRuntimeAutoSize.AutoSize;
            }
        }
    }
}