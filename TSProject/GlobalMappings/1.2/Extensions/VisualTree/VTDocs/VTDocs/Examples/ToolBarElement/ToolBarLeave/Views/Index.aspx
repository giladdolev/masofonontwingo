<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Leave event" ID="windowView2" LoadAction="ToolBarLeave\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Leave" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the input focus leaves the control.

            Syntax: public event EventHandler Leave
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out from one of the tool bars,   
            a 'Leave' event will be invoked.
             Each invocation of the 'Leave' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>

        <vt:ToolBar runat="server" Top="335px" Height="50px" ID="TestedToolBar1" Dock="None" CssClass="vt-test-tb-1" LeaveAction="ToolBarLeave\TestedToolBar1_Leave">
            <vt:ToolBarButton runat="server" id="ToolBarItem1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem2" Image="Content/Images/Open.png" Text="Open 1" ClickAction="ToolBarLeave\OpenButton1_Click"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:ToolBar runat="server" Top="400px" Height="50px" ID="TestedToolBar2" Dock="None" CssClass="vt-test-tb-2" LeaveAction="ToolBarLeave\TestedToolBar2_Leave">
            <vt:ToolBarButton runat="server" id="ToolBarItem3" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBarItem4" Image="Content/Images/Open.png" Text="Open 2" ClickAction="ToolBarLeave\OpenButton2_Click"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel2" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="465px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="505px" Width="360px" Height="140px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>


