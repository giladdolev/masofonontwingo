using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeVisibleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Visible value: " + TestedTree1.Visible;
            lblLog2.Text = "Visible value: " + TestedTree2.Visible + '.';

        }

        public void btnChangeTreeVisible_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedTree2.Visible = !TestedTree2.Visible;
            lblLog2.Text = "Visible value: " + TestedTree2.Visible + '.';
        }
             

    }
}