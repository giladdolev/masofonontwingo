using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridColumnFormatController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}


        private void load(object sender, EventArgs e)
        {

            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");

            //Create column
            GridColumn col;
            col = new GridColumn("col2");
            col.Width = 110;

            //Create row          
            GridRow gridRow = new GridRow();

            //Create cell of row and add to row
            GridContentCell gridCellElement1 = new GridContentCell();
            gridCellElement1.Value = "2.00";
            gridRow.Cells.Add(gridCellElement1);

            //Create another cell of row and add to row
            GridContentCell gridCellElement2 = new GridContentCell();
            gridCellElement2.Value = "content2";
            gridRow.Cells.Add(gridCellElement2);

            //Add to grid
            gridElement.Columns.Add(col);
            gridElement.Rows.Add(gridRow);

            gridElement.RefreshData();
            gridElement.Refresh();
        }

		private void button1_Click(object sender, EventArgs e)
		{
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            
            switch(gridElement.Columns[0].DefaultCellStyle.CountryCurrencyFormat)
            {

                case CurrencyCountry.US:
                    gridElement.Columns[0].DefaultCellStyle.CountryCurrencyFormat = CurrencyCountry.ES;
                    break;
                case CurrencyCountry.ES:
                    gridElement.Columns[0].DefaultCellStyle.CountryCurrencyFormat = CurrencyCountry.GB;
                    break;
                case CurrencyCountry.GB:
                    gridElement.Columns[0].DefaultCellStyle.CountryCurrencyFormat = CurrencyCountry.IL;
                    break;
                case CurrencyCountry.IL:
                    gridElement.Columns[0].DefaultCellStyle.CountryCurrencyFormat = CurrencyCountry.US;
                    break;
                default: 

                     gridElement.Columns[0].DefaultCellStyle.CountryCurrencyFormat = CurrencyCountry.US;
                    break;
            }
            gridElement.RefreshData();
            gridElement.Refresh();
		}


	}
}
