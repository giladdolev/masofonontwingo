using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarTextDirectionController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void btnChangeTextDirection_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
			ToolBarElement toolStrip1 = this.GetVisualElementById<ToolBarElement>("toolStrip1");
            
            
            if (toolStrip1.TextDirection == ToolBarTextDirection.Horizontal)
            {
                toolStrip1.TextDirection = ToolBarTextDirection.Inherit;
                textBox1.Text = toolStrip1.TextDirection.ToString();
            }
            else if (toolStrip1.TextDirection == ToolBarTextDirection.Inherit)
            {
                toolStrip1.TextDirection = ToolBarTextDirection.Vertical270;
                textBox1.Text = toolStrip1.TextDirection.ToString();
            }
            else if (toolStrip1.TextDirection == ToolBarTextDirection.Vertical270)
            {
                toolStrip1.TextDirection = ToolBarTextDirection.Vertical90;
                textBox1.Text = toolStrip1.TextDirection.ToString();
            }
            else 
            {
                toolStrip1.TextDirection = ToolBarTextDirection.Horizontal;
                textBox1.Text = toolStrip1.TextDirection.ToString();
            }
                
		}

    }
}