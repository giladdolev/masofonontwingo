<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Composite PerformUnload method (CancelEventArgs)" ID="windowView" Height="800px" LoadAction="CompositePerformUnload\OnLoad" >

        <vt:Label runat="server" SkinID="Title" Text="PerformUnload" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Raises the UnLoad event

            Syntax: public void PerformUnload(CancelEventArgs args)" Top="75px"  ID="lblDefinition"></vt:Label>
     
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Click the PerformUnLoad(CancelEventArgs) button to invoke the PerformUnload
            (CancelEventArgs) method on this Composite control (Meal Reservation)" Top="240px"  ID="lblExp1"></vt:Label>    

        <vt:PartialControl runat="server" Controller="Composite1" Action="CompositeIndex" Top="310px" Left="80px" ID="CompositeIndex" Height="185px" Width="540">
		</vt:PartialControl>

        <vt:Label runat="server" SkinID="Log" Top="510px" ID="lblLog" Width="590px"></vt:Label>

        <vt:Button runat="server" Text="PerformUnLoad >>" Top="565px" ID="btnPerformUnLoad" ClickAction="CompositePerformUnload\btnPerformUnLoad_Click"></vt:Button>

        <vt:Button runat="server" Text="Reload >>" Top="565px" Left="280px" ID="btnReload" ClickAction="CompositePerformUnload\btnReload_click"></vt:Button>

    </vt:WindowView>
</asp:Content>
