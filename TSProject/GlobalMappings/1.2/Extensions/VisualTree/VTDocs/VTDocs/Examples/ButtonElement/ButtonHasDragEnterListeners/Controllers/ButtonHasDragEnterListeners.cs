using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonHasDragEnterListenersController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnHasDragEnterListeners_Click(object sender, EventArgs e)
        {
            ButtonElement btnHasDragEnterListeners = this.GetVisualElementById<ButtonElement>("btnHasDragEnterListeners");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            textBox1.Text = "HasDragEnterListeners value:\\r\\n" + btnHasDragEnterListeners.HasDragEnterListeners;
        }

    }
}