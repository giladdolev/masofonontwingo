<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer PerformMouseMove() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseMove of 'Tested SplitContainer'" Top="45px" Left="140px" ID="btnPerformMouseMove" Height="36px" Width="300px" ClickAction="SplitContainerPerformMouseMove\btnPerformMouseMove_Click"></vt:Button>


        <vt:SplitContainer runat="server" Text="Tested SplitContainer" Top="150px" Left="200px" ID="testedSplitContainer" Height="70px"  Width="200px" MouseMoveAction="SplitContainerPerformMouseMove\testedSplitContainer_MouseMove"></vt:SplitContainer>           

        

    </vt:WindowView>
</asp:Content>
