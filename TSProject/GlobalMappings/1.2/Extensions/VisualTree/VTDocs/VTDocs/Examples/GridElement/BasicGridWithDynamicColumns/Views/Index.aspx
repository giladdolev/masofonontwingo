﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Add Columns " ID="windowView1" >
        <vt:Label runat="server" SkinID="Title" Text="Add Culomns " ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Add colums to the grid to the grid in runtime and not by code" Top="75px"  ID="lblDefinition"></vt:Label>


      
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="click the button to add columns " Top="200px"  ID="lblExp1"></vt:Label>     
                      

        <vt:Grid runat="server" Top="280px" ID="TestedGrid10">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
    <vt:Button runat="server" ID="BT1" Top="280px" Left="400" Text="add Coulums" ClickAction="BasicGridWithDynamicColumns\AddColumns"></vt:Button>
                  <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="450px" ID="txtEventLog"></vt:TextBox>
    </vt:WindowView>

</asp:Content>
