<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SplitContainer Font Property" ID="windowView2" LoadAction="SplitContainerFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:SplitContainer runat="server" Text="SplitContainer" Top="190px" Width="450px"  ID="TestedSplitContainer1" >
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel1" ID="SplitterPanel1">
                <vt:Button runat="server" Text="button1" Top="5px" Left="10px" Width="60px" ID="button1" BorderStyle="None"></vt:Button>
                <vt:CheckBox runat="server" Text="checkBox1" Top="5px" Left="90px" Width="90px" ID="checkBox1" BackColor="LightGray"></vt:CheckBox>
                <vt:Label runat="server" Text="label1" Top="5px" Left="200px" ID="label1" BackColor="LightGray"></vt:Label>
                <vt:TextBox runat="server" Text="TextBox1" Top="5px" Left="270px" ID="textBox1" BorderStyle="None"></vt:TextBox>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel2" ID="SplitterPanel2"></vt:SplitterPanel>
        </vt:SplitContainer>

        <vt:Label runat="server" SkinID="Log" Top="280px" Width="435px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedSplitContainer is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="390px"  ID="lblExp1"></vt:Label>     
              
        <vt:SplitContainer runat="server" Text="TestedSplitContainer" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="450px" Width="450px" ID="TestedSplitContainer2" >
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel3" ID="SplitterPanel3">
                <vt:Button runat="server" Text="button3" Top="5px" Left="10px" Width="60px" ID="button3" BorderStyle="None"></vt:Button>
                <vt:CheckBox runat="server" Text="checkBox3" Top="5px" Left="90px" Width="90px" ID="checkBox3" BackColor="LightGray"></vt:CheckBox>
                <vt:Label runat="server" Text="label3" Top="5px" Left="200px" ID="label3" BackColor="LightGray"></vt:Label>
                <vt:TextBox runat="server" Text="TextBox3" Top="5px" Left="270px" ID="textBox3" BorderStyle="None"></vt:TextBox>
            </vt:SplitterPanel>
            <vt:SplitterPanel runat="server" ShowHeader="true" AutoSize="True" Dock="Fill" text="SplitterPanel4" ID="SplitterPanel4"></vt:SplitterPanel>
        </vt:SplitContainer>
        <vt:Label runat="server" SkinID="Log" Top="540px" Width="430px" Height="45px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="620px" ID="btnChangeSplitContainerFont" ClickAction="SplitContainerFont/btnChangeSplitContainerFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>