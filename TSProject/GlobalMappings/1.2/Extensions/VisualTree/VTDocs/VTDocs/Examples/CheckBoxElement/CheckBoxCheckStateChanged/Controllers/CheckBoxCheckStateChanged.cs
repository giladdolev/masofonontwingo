using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxCheckStateChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");

            lblLog.Text = "CheckBox CheckState value: " + TestedCheckBox.CheckState;
        }

        public void TestedCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            txtEventLog.Text += "\\r\\nCheckBox CheckStateChanged event is invoked";
            lblLog.Text = "CheckBox CheckState value: " + TestedCheckBox.CheckState;
        }

        public void btnChangeCheckState_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedCheckBox.CheckState == CheckState.Checked)
            {
                TestedCheckBox.CheckState = CheckState.Indeterminate;
                lblLog.Text = "CheckBox CheckState value: " + TestedCheckBox.CheckState;
            }

            else if (TestedCheckBox.CheckState == CheckState.Indeterminate)
            {
                TestedCheckBox.CheckState = CheckState.Unchecked;
                lblLog.Text = "CheckBox CheckState value: " + TestedCheckBox.CheckState;
            }

            else if (TestedCheckBox.CheckState == CheckState.Unchecked)
            {
                TestedCheckBox.CheckState = CheckState.Checked;
                lblLog.Text = "CheckBox CheckState value: " + TestedCheckBox.CheckState;
            }
        }


    }
}