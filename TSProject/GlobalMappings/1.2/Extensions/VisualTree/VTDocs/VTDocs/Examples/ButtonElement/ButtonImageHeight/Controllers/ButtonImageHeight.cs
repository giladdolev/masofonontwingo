using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonImageHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageHeight_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeImageHeight = this.GetVisualElementById<ButtonElement>("btnChangeImageHeight");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (btnChangeImageHeight.ImageHeight == 100) //Get
            {
                btnChangeImageHeight.ImageHeight = 50;//Set
                textBox1.Text = "ImageHeight value: " + btnChangeImageHeight.ImageHeight;
            }
            else 
            {
                btnChangeImageHeight.ImageHeight = 100;
                textBox1.Text = "ImageHeight value: " + btnChangeImageHeight.ImageHeight;

            }                    
        }
      
    }
}