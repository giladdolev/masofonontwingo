<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Label PixelWidth Property" ID="windowView1" LoadAction="LabelPixelWidth\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelWidth" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the width of the control in pixels.
            
            Syntax: public int PixelWidth { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Width property of this Label is initially set to: 80px" Top="235px" ID="lblExp1"></vt:Label>

        <vt:Label runat="server" SkinID="TestedLabel" Text="TestedLabel" Top="295px" ID="TestedLabel"></vt:Label>

        <vt:Label runat="server" SkinID="Log" Top="325px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelWidth value >>" Top="390px" ID="btnChangePixelWidth" Width="180px" ClickAction="LabelPixelWidth\btnChangePixelWidth_Click"></vt:Button>


    </vt:WindowView>
</asp:Content>
