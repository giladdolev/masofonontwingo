﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridRowCollectionIndexerController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void Load_View(object sender, EventArgs e)
        {

            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");

            DataTable source = new DataTable();
            source.Columns.Add("Field1", typeof(int));
            source.Columns.Add("Field2", typeof(string));


            source.Rows.Add(1, "James");
            source.Rows.Add(2, "Bob");
            source.Rows.Add(3, "Dana");
            source.Rows.Add(4, "Sara");

            gridElement.DataSource = source;



        }

        public void btn_Show(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
            MessageBox.Show(gridElement.Rows[0].Cells[1].Value.ToString());
                
        }
    }
}
