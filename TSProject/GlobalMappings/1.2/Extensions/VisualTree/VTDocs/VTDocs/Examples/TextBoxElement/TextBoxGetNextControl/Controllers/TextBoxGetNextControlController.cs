using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxGetNextControlController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog");
            lbl.Text = "Requested Control ID = '' , Next Control ID =  ";
        }

        public async void Mouse_Hover1(object sender, EventArgs e)
        {
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            WindowElement w = this.GetRootVisualElement() as WindowElement;
            ControlElement ctr = await w.GetNextControl(textBox1,true);
            lbl.Text = "Requested Control ID = textBox1 , Next Control ID =  " +ctr.ID;
        }

        public async void Mouse_Hover2(object sender, EventArgs e)
        {
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox2 = this.GetVisualElementById<TextBoxElement>("textBox2");
            WindowElement w = this.GetRootVisualElement() as WindowElement;
            ControlElement ctr = await w.GetNextControl(textBox2, true);
            lbl.Text = "Requested Control ID = textBox2 , Next Control ID =  " + ctr.ID;
        }

        public async void Mouse_Hover3(object sender, EventArgs e)
        {
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox3 = this.GetVisualElementById<TextBoxElement>("textBox3");
            WindowElement w = this.GetRootVisualElement() as WindowElement;
            ControlElement ctr = await w.GetNextControl(textBox3, true);
            lbl.Text = "Requested Control ID = textBox3 , Next Control ID =  " + ctr.ID;
        }

        public async void Mouse_Hover4(object sender, EventArgs e)
        {
            LabelElement lbl = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement textBox4 = this.GetVisualElementById<TextBoxElement>("textBox4");
            WindowElement w = this.GetRootVisualElement() as WindowElement;
            ControlElement ctr = await w.GetNextControl(textBox4, false);
            lbl.Text = "Requested Control ID = textBox4 , Previous Control ID =  " + ctr.ID;
        }
    }
}