using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxImageWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeImageWidth_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkRuntimeImageWidth = this.GetVisualElementById<CheckBoxElement>("chkRuntimeImageWidth");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (chkRuntimeImageWidth.ImageWidth == 150) //Get
            {
                chkRuntimeImageWidth.ImageWidth = 300;//Set
                textBox1.Text = "ImageWidth value: " + chkRuntimeImageWidth.ImageWidth;
            }
            else 
            {
                chkRuntimeImageWidth.ImageWidth = 150;
                textBox1.Text = "ImageWidth value: " + chkRuntimeImageWidth.ImageWidth;

            }                    
        }
      
    }
}