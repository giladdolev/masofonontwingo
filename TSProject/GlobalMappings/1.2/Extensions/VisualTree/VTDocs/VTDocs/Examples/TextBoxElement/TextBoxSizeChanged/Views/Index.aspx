<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox SizeChanged Property" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        
        <vt:TextBox runat="server" Text="Tested TextBox" BackColor="WhiteSmoke" Top="120px" Left="140px" ID="txtSize" Height="36px" Width="200px" SizeChangedAction="TextBoxSizeChanged\txtSize_SizeChanged" >           
        </vt:TextBox>

        <vt:Button runat="server" Text="Change TextBox Size" Top="130px" Left="420px" ID="btnChangeSize" Height="30px" Width="200px" ClickAction="TextBoxSizeChanged\btnChangeSize_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="300px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

        <vt:Label runat="server" Text="Event Log" Top="370px" Left="140px"  ID="label3"   Height="15px" Width="200px"> 
            </vt:Label>

        <vt:TextBox runat="server" Text="" Top="400px" Left="140px"  ID="txtEventLog" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>
    </vt:WindowView>
</asp:Content>
        