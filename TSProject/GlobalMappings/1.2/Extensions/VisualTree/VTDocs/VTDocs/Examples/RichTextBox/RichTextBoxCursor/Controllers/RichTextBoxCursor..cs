using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            RichTextBox rtfRuntimeCursor = this.GetVisualElementById<RichTextBox>("rtfRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (rtfRuntimeCursor.Cursor == CursorsElement.Default)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.Help)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.No)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;

            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else if (rtfRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                rtfRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
            else 
            {
                rtfRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + rtfRuntimeCursor.Cursor;
            }
        }

    }
}