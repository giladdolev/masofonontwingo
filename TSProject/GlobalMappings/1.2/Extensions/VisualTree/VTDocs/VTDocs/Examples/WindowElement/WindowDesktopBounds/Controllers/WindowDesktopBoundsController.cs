using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowDesktopBoundsController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowDesktopBounds());
        }
        private WindowDesktopBounds ViewModel
        {
            get { return this.GetRootVisualElement() as WindowDesktopBounds; }
        }

        public void btnChangeDesktopBounds_Click(object sender, EventArgs e)
		{
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.DesktopBounds == new Rectangle(250, 100, 700, 800))
			{
                this.ViewModel.DesktopBounds = new Rectangle(150, 200, 600, 600);
                textBox1.Text = this.ViewModel.DesktopBounds.ToString();
			}
			else
            {
                this.ViewModel.DesktopBounds = new Rectangle(250, 100, 700, 800);
                textBox1.Text = this.ViewModel.DesktopBounds.ToString();
            }
                
		}

    }
}