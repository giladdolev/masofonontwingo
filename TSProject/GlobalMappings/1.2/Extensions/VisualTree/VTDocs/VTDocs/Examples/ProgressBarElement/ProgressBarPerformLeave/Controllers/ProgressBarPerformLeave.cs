using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPerformLeaveController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLeave_Click(object sender, EventArgs e)
        {
            
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");

            testedProgressBar.PerformLeave(e);
        }

        public void testedProgressBar_Leave(object sender, EventArgs e)
        {
            MessageBox.Show("TestedProgressBar Leave event method is invoked");
        }

    }
}