using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonAppearanceController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAppearance_Click(object sender, EventArgs e)
        {
            ButtonElement btnChangeAppearance = this.GetVisualElementById<ButtonElement>("btnChangeAppearance");

            if (btnChangeAppearance.Appearance == AppearanceConstants.None)
            {
                btnChangeAppearance.Appearance = AppearanceConstants.ccFlat;
            }
            else if(btnChangeAppearance.Appearance == AppearanceConstants.ccFlat)
            {
                btnChangeAppearance.Appearance = AppearanceConstants.cc3D;
            }
             else if(btnChangeAppearance.Appearance == AppearanceConstants.cc3D)
            {
                btnChangeAppearance.Appearance = AppearanceConstants.None;
            }
        }

    }
}