<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="DropDownButton DropDownOpening event" ID="windowView2" LoadAction="DropDownButtonDropDownOpening\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="DropDownOpening" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the DropDownButton DropDown portion is opening.

            Syntax: public event EventHandler DropDownOpening"
            Top="75px" ID="lblDefinition">
        </vt:Label>
      
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="175px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can click on the DropDownButton arrow to open the DropDown portion. 
            Each invocation of the 'DropDownOpening' and 'DropDownOpened' events should add a line in the 
            'Event Log'."
            Top="225px" ID="lblExp2">
        </vt:Label>

        <vt:DropDownButton runat="server" Image="Content/Images/New.png" Top="315px" Left="80px" ID="TestedDropDownButton" CssClass="vt-ddbtn-TestedDropDownButton" DropDownArrows="true" Height="50px" Width="180px" Text="Drop down button" DropDownOpeningAction="DropDownButtonDropDownOpening\TestedDropDownButton_DropDownOpening" DropDownOpenedAction="DropDownButtonDropDownOpening\TestedDropDownButton_DropDownOpened">
            <vt:ToolbarButton runat="server" Top="10px"  ID="item1"  Height="30px" Width="120px"  Text="menuItem1"></vt:ToolbarButton>
            <vt:ToolbarButton runat="server" Top="10px"  ID="item2"  Height="30px" Width="120px"  Text="menuItem2"></vt:ToolbarButton>
            <vt:DropDownButton runat="server" Top="10px" ID="item3" DropDownArrows="true"  Height="30px" Width="120px"  Text="menuItem3" CssClass="vt-ddbtn-TestedmenuItem3" DropDownOpeningAction="DropDownButtonDropDownOpening\item3_DropDownOpening" DropDownOpenedAction="DropDownButtonDropDownOpening\item3_DropDownOpened"> 
                <vt:ToolbarButton runat="server" Top="10px"  ID="Button1"  Height="30px" Width="120px"  Text="Item3_1"></vt:ToolbarButton>
                <vt:ToolbarButton runat="server" Top="10px"  ID="Button2"  Height="30px" Width="120px"  Text="Item3_2"></vt:ToolbarButton>  
            </vt:DropDownButton>
        </vt:DropDownButton>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="465px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="505px" Height="150px" ID="txtEventLog"></vt:TextBox>
       
    </vt:WindowView>
</asp:Content>


