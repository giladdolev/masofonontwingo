using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedTextBox1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
        }

        public void btnChangeTextBoxBorderStyle_Click(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedTextBox2.BorderStyle == BorderStyle.None)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Dotted)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Double)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedTextBox2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Groove)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Inset)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Dashed)
            {
                TestedTextBox2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.NotSet)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Outset)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Ridge)
            {
                TestedTextBox2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else if (TestedTextBox2.BorderStyle == BorderStyle.Solid)
            {
                TestedTextBox2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
            else
            {
                TestedTextBox2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedTextBox2.BorderStyle + '.';
            }
        }

    }
}