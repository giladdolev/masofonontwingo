using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridColumnAddRangeController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

		private void button1_Click(object sender, EventArgs e)
		{
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");

            //Create column
            GridColumn column;
            column = new GridColumn("name");
            column.Width = 110;

            //Create row          
            GridRow gridRow = new GridRow();

            //Create cell of row and add to row
            GridContentCell gridCellElement1 = new GridContentCell();
            gridCellElement1.Value = "content1";
            gridRow.Cells.Add(gridCellElement1);

            //Create another cell of row and add to row
            GridContentCell gridCellElement2 = new GridContentCell();
            gridCellElement2.Value = "content2";
            gridRow.Cells.Add(gridCellElement2);


            //Create another column
            GridColumn column2;
            column2 = new GridColumn("surname");
            column2.Width = 110;


            //Create cell of row and add to row
            GridContentCell gridCellElement3 = new GridContentCell();
            gridCellElement3.Value = "content3";
            gridRow.Cells.Add(gridCellElement3);

          

            //Add to grid
            gridElement.Columns.AddRange(new GridColumn[]{
                                                    column,
                                                    column2});
            gridElement.Rows.Add(gridRow);

            gridElement.RefreshData();
            gridElement.Refresh();
		}


	}
}
