using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MaskedTextBoxSelectOnFocusController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox1");
            MaskedTextBoxElement TestedMaskedTextBox2 = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
           
            lblLog1.Text = "SelectOnFocus value is: " + TestedMaskedTextBox1.SelectOnFocus;
            lblLog2.Text = "SelectOnFocus value is: " + TestedMaskedTextBox2.SelectOnFocus;

        }


        public void btnFocus1_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedMaskedTextBox1.Focus();
            lblLog1.Text = "SelectOnFocus value is: " + TestedMaskedTextBox1.SelectOnFocus + "\\r\\nTestedMaskedTextBox1 Focus method was invoked";
        }

        public void btnFocus2_Click(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox2 = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedMaskedTextBox2.Focus();
            lblLog2.Text = "SelectOnFocus value is: " + TestedMaskedTextBox2.SelectOnFocus + "\\r\\nTestedMaskedTextBox2 Focus method was invoked";
        }


        public void TestedMaskedTextBox1_LostFocus(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox1 = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "SelectOnFocus value is: " + TestedMaskedTextBox1.SelectOnFocus;
        }

        public void TestedMaskedTextBox2_LostFocus(object sender, EventArgs e)
        {
            MaskedTextBoxElement TestedMaskedTextBox2 = this.GetVisualElementById<MaskedTextBoxElement>("TestedMaskedTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "SelectOnFocus value is: " + TestedMaskedTextBox2.SelectOnFocus;
        }

    }
}