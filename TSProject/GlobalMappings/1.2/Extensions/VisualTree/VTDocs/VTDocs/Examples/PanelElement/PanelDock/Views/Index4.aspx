<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Form4 - Top Dock style" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
       
        <vt:Label runat="server" Text="Initialized component : The Panel is initialliy docked to Top" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 

        <vt:Panel runat="server" Text="Panel" Top="165px" Left="140px" ID="pnlDock" Dock="Top" Height="150px" TabIndex="1" Width="200px" BackColor="Pink"></vt:Panel>

        <vt:Button runat="server" Text="Change panel Dock of Form4" Top="210px" Left="200px" ID="btnDockTopAndBottomForm4" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="Form4DockTopAndBottom\btnDockTopAndBottomForm4_Click"></vt:Button>
        
       <vt:TextBox runat="server" Text="" Top="325px" Left="200px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"></vt:TextBox>


    </vt:WindowView>
</asp:Content>
        