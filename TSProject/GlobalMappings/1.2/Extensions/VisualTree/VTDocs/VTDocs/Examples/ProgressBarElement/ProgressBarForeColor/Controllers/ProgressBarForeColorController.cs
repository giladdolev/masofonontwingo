using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar1 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar1");
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedProgressBar1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedProgressBar2.ForeColor + '.';
        }

        public void btnChangeProgressBarForeColor_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar2 = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedProgressBar2.ForeColor == Color.Green)
            {
                TestedProgressBar2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedProgressBar2.ForeColor + '.';
            }
            else
            {
                TestedProgressBar2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedProgressBar2.ForeColor + '.';
            }
        }

    }
}