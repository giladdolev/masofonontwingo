<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox KeyDown event" ID="windowView2" LoadAction="TextBoxKeyDown\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="KeyDown" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when a key is pressed while the control has focus.

            Syntax: public event KeyEventHandler KeyDown"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="155px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can fill up the form by inserting Keys into the text boxes, 
             and also change the focus by pressing TAB/Shift+TAB keys.
             Each invocation of the 'KeyDown' event should add a line in the 'Event Log'."
            Top="195px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="275px" Height="110px" Width="340px">

            <vt:Label runat="server" Text="First Name :" Left="15px" Top="20px" ID="Label1"></vt:Label>
            <vt:TextBox runat="server" Top="20px" Left="160px" Text="Enter First Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtFirstName" TabIndex="1" 
                    KeyDownAction="TextBoxKeyDown\txtFirstName_KeyDown" TextChangedAction="TextBoxKeyDown\txtFirstName_TextChanged"></vt:TextBox>

            <vt:Label runat="server" Text="Last Name :" Left="15px" Top="65px" ID="Label2"></vt:Label>
            <vt:TextBox runat="server" Top="65px" Left="160px" Text="Enter Last Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtLastName" TabIndex="2" 
                    KeyDownAction="TextBoxKeyDown\txtLastName_KeyDown" TextChangedAction="TextBoxKeyDown\txtLastName_TextChanged"></vt:TextBox>

        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" Height="40px" ID="lblLog" Top="400px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="455px" Width="360px" Height="230px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>


