using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Grid PixelLeft Value is: " + TestedGrid.PixelLeft + '.';
        }
        public void ChangePixelLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            if (TestedGrid.PixelLeft == 200)
            {
                TestedGrid.PixelLeft = 80;
                lblLog.Text = "Grid PixelLeft Value is: " + TestedGrid.PixelLeft + '.';

            }
            else
            {
                TestedGrid.PixelLeft = 200;
                lblLog.Text = "Grid PixelLeft Value is: " + TestedGrid.PixelLeft + '.';
            }
        }

    }
}