using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarPerformLocationChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformLocationChanged_Click(object sender, EventArgs e)
        {
            
            ProgressBarElement testedProgressBar = this.GetVisualElementById<ProgressBarElement>("testedProgressBar");
            testedProgressBar.PerformLocationChanged(e);
        }

        public void testedProgressBar_LocationChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TestedProgressBar LocationChanged event method is invoked");
        }

    }
}