using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridColumnCtorStringController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

		private void button1_Click(object sender, EventArgs e)
		{
            GridElement gridElement = this.GetVisualElementById<GridElement>("gridElement");
        
            //Create column
            GridColumn col;
            col = new GridColumn("name");
            col.Width = 110;

            //Create row          
            GridRow gridRow = new GridRow();

            //Create cell of row and add to row
            GridContentCell gridCellElement1 = new GridContentCell();
            gridCellElement1.Value = "content1";
            gridRow.Cells.Add(gridCellElement1);

            //Create another cell of row and add to row
            GridContentCell gridCellElement2 = new GridContentCell();
            gridCellElement2.Value = "content2";
            gridRow.Cells.Add(gridCellElement2);

            //Add to grid
            gridElement.Columns.Add(col);
            gridElement.Rows.Add(gridRow);

            gridElement.RefreshData();
            gridElement.Refresh();
		}


	}
}
