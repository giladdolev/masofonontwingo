using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerVisibleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //SplitContainerAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Visible value: " + TestedSplitContainer1.Visible;
            lblLog2.Text = "Visible value: " + TestedSplitContainer2.Visible + '.';

        }

        public void btnChangeSplitContainerVisible_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedSplitContainer2.Visible = !TestedSplitContainer2.Visible;
            lblLog2.Text = "Visible value: " + TestedSplitContainer2.Visible + '.';
        }

    }
}