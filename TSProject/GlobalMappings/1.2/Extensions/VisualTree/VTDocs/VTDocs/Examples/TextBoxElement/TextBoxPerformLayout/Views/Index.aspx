<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox PerformLayout() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformLayout of 'Tested TextBox'" Top="45px" Left="140px" ID="btnPerformLayout" Height="36px" Width="300px" ClickAction="TextBoxPerformLayout\btnPerformLayout_Click"></vt:Button>


        <vt:TextBox runat="server" Text="Tested TextBox" Top="150px" Left="200px" ID="testedTextBox" Height="36px"  Width="200px" LayoutAction="TextBoxPerformLayout\testedTextBox_Layout"></vt:TextBox>           

        

    </vt:WindowView>
</asp:Content>
