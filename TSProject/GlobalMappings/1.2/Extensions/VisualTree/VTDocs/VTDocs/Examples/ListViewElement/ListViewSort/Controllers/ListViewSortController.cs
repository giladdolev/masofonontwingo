using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewSortController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void btnSortAscending_Click(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            listView.Sorting = SortOrder.Ascending;
            listView.Sort();
        }

        private void btnSortDescending_Click(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            listView.Sorting = SortOrder.Descending;
            listView.Sort();
        }
    }
}
