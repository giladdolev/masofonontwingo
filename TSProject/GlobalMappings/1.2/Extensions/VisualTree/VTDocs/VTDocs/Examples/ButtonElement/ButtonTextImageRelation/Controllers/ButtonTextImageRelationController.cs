using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonTextImageRelationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton1 = this.GetVisualElementById<ButtonElement>("btnTestedButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "TextImageRelation value: " + btnTestedButton1.TextImageRelation.ToString();
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextImageRelation value: " + btnTestedButton2.TextImageRelation.ToString() + ".";

        }
        private void btnImageAboveText_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            btnTestedButton2.TextImageRelation = System.Web.VisualTree.TextImageRelation.ImageAboveText;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextImageRelation value: " + btnTestedButton2.TextImageRelation.ToString() + ".";
        }
        private void btnImageBeforeText_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            btnTestedButton2.TextImageRelation = System.Web.VisualTree.TextImageRelation.ImageBeforeText;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextImageRelation value: " + btnTestedButton2.TextImageRelation.ToString() + ".";
        }
        private void btnOverlay_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            btnTestedButton2.TextImageRelation = System.Web.VisualTree.TextImageRelation.Overlay;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextImageRelation value: " + btnTestedButton2.TextImageRelation.ToString() + ".";
        }
        private void btnTextAboveImage_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
           btnTestedButton2.TextImageRelation = System.Web.VisualTree.TextImageRelation.TextAboveImage;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextImageRelation value: " + btnTestedButton2.TextImageRelation.ToString() + ".";
        }
        private void btnTextBeforeImage_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton2 = this.GetVisualElementById<ButtonElement>("btnTestedButton2");
            btnTestedButton2.TextImageRelation = System.Web.VisualTree.TextImageRelation.TextBeforeImage;
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "TextImageRelation value: " + btnTestedButton2.TextImageRelation.ToString() + ".";
        }
    }
}