

<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CssClass Property" ID="windowView2" LoadAction="ButtonCssClass\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CssClass" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the image css class at the initialize.
            The default: without css class." Top="75px" ID="lblDefinition" ></vt:Label>
         
        

        <vt:Button runat="server" Text="Button" SkinID="Wide" Top="180px" ID="btnTestedButton1" Image ="~/Content/Images/Cute.png" ClickAction="ButtonCssClass/click"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="230px" ID="lblLog1" ></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="300px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="CssClass property of this 'TestedButton' is initially set to 'x-button-icon' class." Top="350px"  ID="lblExp1"></vt:Label>     

        
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" Image ="~/Content/Images/Cute.png" Top="400px" ID="btnTestedButton2" CssClass="x-button-icon"></vt:Button>
        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog2"></vt:Label>

              

    </vt:WindowView>
</asp:Content>

