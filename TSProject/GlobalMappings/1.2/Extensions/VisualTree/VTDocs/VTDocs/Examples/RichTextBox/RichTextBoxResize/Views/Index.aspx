<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox Resize Property" Top="57px" Left="11px" ID="windowView1" Height="750px" Width="768px">
         
        <vt:Label runat="server" Text="RichTextBox 'Size' is initially set to (200,36)" AutoSize="true" Top="80px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:RichTextBox runat="server" Text="Tested RichTextBox" BackColor="LightGray" Top="105px" Left="140px" ID="testedRichTextBox" Height="70px"  TabIndex="1" Width="200px"  ResizeAction="RichTextBoxResize\testedRichTextBox_Resize"></vt:RichTextBox>

        <vt:Button runat="server" Text="Resize RichTextBox" Top="105px" Left="480px" ID="btnChangeSize" Height="36px"  TabIndex="1" Width="250px" ClickAction="RichTextBoxResize\btnChangeSize_Click" ></vt:Button>

         <vt:Label runat="server" Text="Event Log" Top="210px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>


        <vt:TextBox runat="server" Text="" Top="225px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
