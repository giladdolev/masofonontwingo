using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowWindowBorderStyleController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowWindowBorderStyle());
        }
        private WindowWindowBorderStyle ViewModel
        {
            get { return this.GetRootVisualElement() as WindowWindowBorderStyle; }
        }

        public void btnChangeWindowBorderStyle_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.WindowBorderStyle == WindowBorderStyle.FixedDialog)
            {
                this.ViewModel.WindowBorderStyle = WindowBorderStyle.FixedDouble;
                textBox1.Text = this.ViewModel.WindowBorderStyle.ToString();
            }
            else if (this.ViewModel.WindowBorderStyle == WindowBorderStyle.FixedDouble)
            {
                this.ViewModel.WindowBorderStyle = WindowBorderStyle.FixedSingle;
                textBox1.Text = this.ViewModel.WindowBorderStyle.ToString();
            }
            else if (this.ViewModel.WindowBorderStyle == WindowBorderStyle.FixedSingle)
            {
                this.ViewModel.WindowBorderStyle = WindowBorderStyle.FixedToolWindow;
                textBox1.Text = this.ViewModel.WindowBorderStyle.ToString();
            }
            else if (this.ViewModel.WindowBorderStyle == WindowBorderStyle.FixedToolWindow)
            {
                this.ViewModel.WindowBorderStyle = WindowBorderStyle.None;
                textBox1.Text = this.ViewModel.WindowBorderStyle.ToString();
            }
            else if (this.ViewModel.WindowBorderStyle == WindowBorderStyle.None)
            {
                this.ViewModel.WindowBorderStyle = WindowBorderStyle.Sizable;
                textBox1.Text = this.ViewModel.WindowBorderStyle.ToString();
            }
            else if (this.ViewModel.WindowBorderStyle == WindowBorderStyle.Sizable)
            {
                this.ViewModel.WindowBorderStyle = WindowBorderStyle.SizableToolWindow;
                textBox1.Text = this.ViewModel.WindowBorderStyle.ToString();
            }
            else 
            {
                this.ViewModel.WindowBorderStyle = WindowBorderStyle.FixedDialog;
                textBox1.Text = this.ViewModel.BorderStyle.ToString();
            }
            
        }
    }
}