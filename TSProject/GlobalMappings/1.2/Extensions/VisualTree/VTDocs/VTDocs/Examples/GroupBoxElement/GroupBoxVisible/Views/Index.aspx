<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="GroupBox Visible Property" ID="windowView1" LoadAction="GroupBoxVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Visible" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control and all its child controls are displayed.

            Syntax: public bool Visible { get; set; }
            'true' if the control and all its child controls are displayed; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested GroupBox1 -->
        <vt:GroupBox runat="server" Text="GroupBox" Top="170px" ID="TestedGroupBox1"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="340px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Visible property of this GroupBox is initially set to 'false'
             The GroupBox should be invisible" Top="390px" ID="lblExp1"></vt:Label>

        <!-- Tested GroupBox2 -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" Visible="false" Top="450px" ID="TestedGroupBox2"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="545px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Visible value >>" Top="620px" Width="200px" ID="btnChangeGroupBoxVisible" ClickAction="GroupBoxVisible\btnChangeGroupBoxVisible_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
        