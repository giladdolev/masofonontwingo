using System.Collections.Generic;
using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowActivateController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new WindowActivate());
        }

        private WindowActivate ViewModel
        {
            get { return this.GetRootVisualElement() as WindowActivate; }
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "WindowActivate");
            return argsDictionary;
        }

        public void OnLoad(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = GetArgsDictionary();
            ViewModel.frm2 = VisualElementHelper.CreateFromView<WindowActivate2>("WindowActivate2", "Index2", null, argsDictionary, null);
           
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Click the button...";
                
            ViewModel.frm2.Show();

        }

      
        private void btnActivateTestedWindow_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            ViewModel.frm2.Activate();
            lblLog.Text = "Activate() was invoked";
          
        }
       
    }
}