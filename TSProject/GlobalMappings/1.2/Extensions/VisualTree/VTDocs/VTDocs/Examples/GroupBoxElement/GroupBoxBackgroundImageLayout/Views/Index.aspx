<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme ="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
  <vt:WindowView runat="server" Text="GroupBox BackgroundImageLayout Property" ID="windowView" LoadAction="GroupBoxBackgroundImageLayout\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackgroundImageLayout" Left="240px" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background image position on the control (Center , None, Stretch, Tile or Zoom).
            Tile is the default value." Top="75px"  ID="lblDefinition1"></vt:Label>
      
        <!-- TestedGroupBox1 -->
        <vt:GroupBox runat="server" BackgroundImage ="~/Content/Images/icon.jpg" Text="GroupBox" Top="125px" ID="TestedGroupBox1"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" Text="BackgroundImageLayout takes effect only if the BackgroundImage property is set." Top="250px"  ID="lblDefinition2"></vt:Label>
      


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="315px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the initial BackgroundImageLayout of TestedGroupBox is set to 'Center'.
            Use the buttons below (None/ Stretch/ Center/ Zoom/ Tile) to change the TestedGroupBox 
            Background Image Layout." Top="365px"  ID="lblExp1"></vt:Label>     
        
       <!-- TestedGroupBox2 -->
        <vt:GroupBox runat="server" Text="TestedGroupBox" BackgroundImage ="~/Content/Images/icon.jpg" BackgroundImageLayout ="Center" Top="460px" ID="TestedGroupBox2"></vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="555px" ID="lblLog2"></vt:Label>

         <vt:Button runat="server" Text="Tile"  Top="620px" Left="560px" ID="btnTile" Height="30px" TabIndex="1" Width="60px" ClickAction="GroupBoxBackgroundImageLayout\btnTile_Click"></vt:Button>  
         <vt:Button runat="server" Text="Center"   Top="620px" Left="340px" ID="btnCenter" Height="30px" TabIndex="1" Width="60px" ClickAction="GroupBoxBackgroundImageLayout\btnCenter_Click"></vt:Button>  
         <vt:Button runat="server" Text="Stretch"   Top="620px" Left="230px" ID="btnStretch" Height="30px" TabIndex="1" Width="60px" ClickAction="GroupBoxBackgroundImageLayout\btnStretch_Click"></vt:Button>  
         <vt:Button runat="server" Text="Zoom"  Top="620px" Left="450px" ID="btnZoom" Height="30px" TabIndex="1" Width="60px" ClickAction="GroupBoxBackgroundImageLayout\btnZoom_Click"></vt:Button>  
         <vt:Button runat="server" Text="None" Top="620px" Left="120px" ID="btnNone" Height="30px" TabIndex="1" Width="60px" ClickAction="GroupBoxBackgroundImageLayout\btnNone_Click"></vt:Button> 
 
    </vt:WindowView>
</asp:Content>
        