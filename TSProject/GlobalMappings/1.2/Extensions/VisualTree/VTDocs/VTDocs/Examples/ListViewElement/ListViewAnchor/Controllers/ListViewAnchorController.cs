using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Handles the Load event of the Form control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form_Load(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            ListViewItem newReportNode = new ListViewItem("xxxxx");
            newReportNode.Subitems.Add("1");
            listView.Items.Add(newReportNode);
            newReportNode = new ListViewItem("yyyy");
            newReportNode.Subitems.Add("2");
            listView.Items.Add(newReportNode);
            ColumnHeader header1 = new ColumnHeader();
            header1.Text = "Col1";
            listView.Columns.Add(header1);
        }

        private void btnAnchor_Click(object sender, EventArgs e)
        {
            ListViewElement listView = this.GetVisualElementById<ListViewElement>("listView1");
            PanelElement Panel1 = this.GetVisualElementById<PanelElement>("Panel1");

            if (listView.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                listView.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                Panel1.Size = new Size(450, 150);
            }

            else if (listView.Anchor == AnchorStyles.Top)
            {
                listView.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                Panel1.Size = new Size(315, 190);
            }
            else if (listView.Anchor == AnchorStyles.Left)
            {
                listView.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                Panel1.Size = new Size(200, 190);
            }
            else if (listView.Anchor == AnchorStyles.Right)
            {
                listView.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                Panel1.Size = new Size(200, 160);
            }
            else if (listView.Anchor == AnchorStyles.Bottom)
            {
                listView.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                Panel1.Size = new Size(315, 171);//Set  back the panel size
            }
            else if (listView.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and listView1 settings
                listView.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                Panel1.Size = new Size(500, 200);//Set back the panel size
                //listView.Location = new Point(150, 200);
            }
        }
    }
}