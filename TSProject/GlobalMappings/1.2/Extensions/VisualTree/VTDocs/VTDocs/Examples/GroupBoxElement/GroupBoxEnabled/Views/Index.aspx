<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox Enabled Property" ID="windowView1" LoadAction="GroupBoxEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:GroupBox runat="server" Text="GroupBox" Top="195px" Width="330px" ID="TestedGroupBox1">
            <vt:Button runat="server" Text="Click Me" Top="28px" Left="15px" ID="button1" Width="80px"  ClickAction="GroupBoxEnabled\button1_Click"></vt:Button>
            <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label1" Width="80px" Left="110px"></vt:Label>
            <vt:TextBox runat="server" Text="Add Text " Top="28px" Height="23px" Left="205px" ID="textBox1" Width="80px"></vt:TextBox>
        </vt:GroupBox>           

        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedGroupBox' is initially set to 'false'." Top="405px" ID="lblExp1"></vt:Label> 

        <vt:GroupBox runat="server" Text="TestedGroupBox" Top="465px" Width="330px" Enabled ="false" ID="TestedGroupBox2">
            <vt:Button runat="server" Text="Click Me" Top="28px" Left="15px" ID="button2" Width="80px"  ClickAction="GroupBoxEnabled\button2_Click"></vt:Button>
            <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label2" Width="80px" Left="110px"></vt:Label>
            <vt:TextBox runat="server" Text="Add Text " Top="28px" Height="23px" Left="205px" ID="textBox2" Width="80px"></vt:TextBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Top="560px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="625px" ID="btnChangeEnabled" Width="180px" ClickAction="GroupBoxEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
