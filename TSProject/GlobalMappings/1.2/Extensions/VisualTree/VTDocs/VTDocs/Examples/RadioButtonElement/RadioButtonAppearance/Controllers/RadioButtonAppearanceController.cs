using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonAppearanceController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        public void btnChangeAppearance_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement rdoRuntimeAppearance = this.GetVisualElementById<RadioButtonElement>("rdoRuntimeAppearance");

            if (rdoRuntimeAppearance.Appearance == AppearanceConstants.None)
            {
                rdoRuntimeAppearance.Appearance = AppearanceConstants.ccFlat;
                textBox1.Text = "AppearanceConstants.ccFlat";
            }
            else if (rdoRuntimeAppearance.Appearance == AppearanceConstants.ccFlat)
            {
                rdoRuntimeAppearance.Appearance = AppearanceConstants.cc3D;
                textBox1.Text = "AppearanceConstants.cc3D";
            }
            else if (rdoRuntimeAppearance.Appearance == AppearanceConstants.cc3D)
            {
                rdoRuntimeAppearance.Appearance = AppearanceConstants.None;
                textBox1.Text = "AppearanceConstants.None";
            }
        }

    }
}