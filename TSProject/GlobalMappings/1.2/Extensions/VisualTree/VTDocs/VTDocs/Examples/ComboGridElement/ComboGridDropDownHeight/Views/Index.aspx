<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboGrid DropDownHeight" Height="800px" ID="windowView1" LoadAction="ComboGridDropDownHeight\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="DropDownHeight" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height in pixels of the drop-down portion of the ComboGrid.

            Syntax: public int DropDownHeight { get; set; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboGrid runat="server" Top="160px" Text="ComboGrid" ID="ComboGrid" CssClass="vt-ComboGrid"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="The following example, shows usage of the ComboGrid DropDownHeight property.
            DropDownHeight property of this TestedComboGrid is initially set to 30.
            Click the 'Change DropDownHeight Value' button to change the DropDownHeight to 90." Top="400px" ID="lblExp2">
        </vt:Label>  

        <vt:ComboGrid runat="server" Top="480px" Text="TestedComboGrid" ID="TestedComboGrid" CssClass="vt-TestedComboGrid" DropDownHeight="30"></vt:ComboGrid>

        <vt:Label runat="server" SkinID="Log" Top="620px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change DropDownHeight Value >>" Top="700px" Width="200px" ID="btnChange" ClickAction="ComboGridDropDownHeight\btnChange_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
