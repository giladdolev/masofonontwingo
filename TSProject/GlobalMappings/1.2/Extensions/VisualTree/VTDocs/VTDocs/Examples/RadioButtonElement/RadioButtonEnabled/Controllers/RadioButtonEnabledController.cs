using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonEnabledController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton1 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Enabled value: " + TestedRadioButton1.Enabled.ToString();

            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Enabled value: " + TestedRadioButton2.Enabled.ToString() + ".";

        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedRadioButton2.Enabled = !TestedRadioButton2.Enabled;
            lblLog2.Text = "Enabled value: " + TestedRadioButton2.Enabled.ToString() + ".";
        }
    }
}