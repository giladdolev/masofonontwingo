using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelAccessibleRoleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

      

        public void btnGetAccessibleRole_Click(object sender, EventArgs e)
        {
            PanelElement pnlAccessibleRole = this.GetVisualElementById<PanelElement>("pnlAccessibleRole");

            MessageBox.Show(pnlAccessibleRole.AccessibleRole.ToString());

        }
        public void btnChangeAccessibleRole_Click(object sender, EventArgs e)
        {
            PanelElement pnlAccessibleRole = this.GetVisualElementById<PanelElement>("pnlAccessibleRole");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Alert)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Animation;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Animation)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Application;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Application)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Border;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Border)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDown;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDown)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ButtonDropDownGrid;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ButtonDropDownGrid)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ButtonMenu;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ButtonMenu)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Caret;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Caret)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Cell;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Cell)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Character;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Character)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Chart;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Chart)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.CheckButton;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.CheckButton)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Client;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Client)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Clock;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Clock)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Column;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Column)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ColumnHeader;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ColumnHeader)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ComboBox;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ComboBox)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Cursor;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Cursor)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Default;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Default)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Diagram;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Diagram)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Dial;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Dial)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Dialog;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Dialog)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Document;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Document)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.DropList;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.DropList)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Equation;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Equation)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Graphic;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Graphic)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Grip;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Grip)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Grouping;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Grouping)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.HelpBalloon;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.HelpBalloon)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.HotkeyField;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.HotkeyField)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Indicator;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Indicator)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.IpAddress;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.IpAddress)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Link;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Link)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.List;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.List)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ListItem;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ListItem)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.MenuBar;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.MenuBar)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.MenuItem;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.MenuItem)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.MenuPopup;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.MenuPopup)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.None;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.None)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Outline;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Outline)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.OutlineButton;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.OutlineButton)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.OutlineItem;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.OutlineItem)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.PageTab;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.PageTab)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.PageTabList;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.PageTabList)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Pane;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Pane)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ProgressBar;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ProgressBar)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.PropertyPage;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.PropertyPage)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.PushButton;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.PushButton)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Row;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Row)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.RowHeader;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.RowHeader)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ScrollBar;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ScrollBar)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Separator;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Separator)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Slider;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Slider)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Sound;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Sound)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.SpinButton;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.SpinButton)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.SplitButton;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.SplitButton)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.StaticText;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.StaticText)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.StatusBar;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.StatusBar)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Table;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Table)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Text;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.Text)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.TitleBar;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.TitleBar)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ToolBar;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ToolBar)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.ToolTip;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.ToolTip)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.WhiteSpace;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else if (pnlAccessibleRole.AccessibleRole == AccessibleRole.WhiteSpace)
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Window;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
            else
            {
                pnlAccessibleRole.AccessibleRole = AccessibleRole.Alert;
                textBox1.Text = pnlAccessibleRole.AccessibleRole.ToString();
            }
                
        }

    }
}