using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement btnTestedComboBox = this.GetVisualElementById<ComboBoxElement>("btnTestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            btnTestedComboBox.Items.Add("Item1");
            btnTestedComboBox.Items.Add("Item2");
            btnTestedComboBox.Items.Add("Item3");
            lblLog.Text = "Text  property value: " + btnTestedComboBox.Text;

        }

        public void btnChangeComboBoxText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement btnTestedComboBox = this.GetVisualElementById<ComboBoxElement>("btnTestedComboBox");
            if (btnTestedComboBox.Text == "TestedComboBox")
            {
                btnTestedComboBox.Text = "New Text";
                lblLog.Text = "Text  property value: " + btnTestedComboBox.Text;

            }
            else
            {
                btnTestedComboBox.Text = "TestedComboBox";
                lblLog.Text = "Text  property value: " + btnTestedComboBox.Text;
            }

        }
    }
}
