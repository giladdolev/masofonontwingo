<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown Resize Property" Top="57px" Left="11px" ID="windowView1" Height="750px" Width="768px">
         
        <vt:Label runat="server" Text="NumericUpDown 'Size' is initially set to (100,20)" Top="80px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:NumericUpDown runat="server" Text="Tested NumericUpDown" BackColor="LightGray" Top="95px" Left="140px" ID="testedNumericUpDown" Height="20px"  TabIndex="1" Width="100px"  ResizeAction="NumericUpDownResize\testedNumericUpDown_Resize"></vt:NumericUpDown>

        <vt:Button runat="server" Text="Resize NumericUpDown" Top="95px" Left="480px" ID="btnChangeSize" Height="36px"  TabIndex="1" Width="250px" ClickAction="NumericUpDownResize\btnChangeSize_Click" ></vt:Button>

         <vt:Label runat="server" Text="Event Log" Top="150px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>


        <vt:TextBox runat="server" Text="" Top="165px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
