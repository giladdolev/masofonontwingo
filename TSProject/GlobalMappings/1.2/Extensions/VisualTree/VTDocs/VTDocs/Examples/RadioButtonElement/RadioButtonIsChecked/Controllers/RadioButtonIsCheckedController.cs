using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonIsCheckedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton1 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton1");
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            lblLog1.Text = "IsChecked Value is: " + TestedRadioButton1.IsChecked + '.';
            lblLog2.Text = "IsChecked Value: " + TestedRadioButton2.IsChecked + '.';


        }

        public void btnChangeIsChecked_Click(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedRadioButton2.IsChecked)
            {
                TestedRadioButton2.IsChecked = false;
            }
            else
            {
                TestedRadioButton2.IsChecked = true;
            }

        }

        public void rarioButton0_CheckChanged(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton1 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "IsChecked Value is: " + TestedRadioButton1.IsChecked + '.';

        }

        public void rarioButton1_CheckChanged(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton2 = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "IsChecked Value: " + TestedRadioButton2.IsChecked + '.';

        }
    }
}