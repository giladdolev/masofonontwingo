<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic MessageBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:ToolBar runat="server" ID="MainMenu">
            <vt:ToolBarMenuItem runat="server" Text="File">
                <vt:ToolBarMenuItem runat="server" Text="New..." Image="Content/Images/New.png" ClickAction="BasicMenu\MainMenu_Click" />
                <vt:ToolBarMenuItem runat="server" Text="Open..." Image="Content/Images/Open.png" ClickAction="BasicMenu\MainMenu_Click" />
                <vt:ToolBarMenuSeparator runat="server" />
                <vt:ToolBarMenuItem runat="server" Text="Exit" ClickAction="BasicMenu\MainMenu_Click" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Edit">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="View">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Tools">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Window">
                <vt:ToolBarMenuItem runat="server" Text="New..." />
                <vt:ToolBarMenuSeparator runat="server" />
            </vt:ToolBarMenuItem>
            <vt:ToolBarMenuItem runat="server" Text="Help">
                <vt:ToolBarMenuItem runat="server" Text="About" />
            </vt:ToolBarMenuItem>
        </vt:ToolBar>
    </vt:WindowView>
</asp:Content>
