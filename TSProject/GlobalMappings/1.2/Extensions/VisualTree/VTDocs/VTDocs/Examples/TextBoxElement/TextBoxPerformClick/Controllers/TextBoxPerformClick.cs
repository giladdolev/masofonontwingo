using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformClickController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformClick_Click(object sender, EventArgs e)
        {

            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");


            TestedTextBox.PerformClick(e);
        }

        public void TestedTextBox_Click(object sender, EventArgs e)
        {

            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");

            MessageBox.Show("TestedTextBox Click event method is invoked");
        }

    }
}