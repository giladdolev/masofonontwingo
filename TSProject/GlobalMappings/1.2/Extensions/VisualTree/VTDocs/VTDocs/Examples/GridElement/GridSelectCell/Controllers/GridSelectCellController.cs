using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridSelectCellController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Please select a cell.";

            
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid1");
        //    GridImageColumn a = new GridImageColumn();
        //    //Create column
        //    GridColumn col;
        //    col = new GridColumn("name");
        //    col.Width = 50;

        //    //Create row          
        //    GridRow gridRow = new GridRow();
        //    GridContentCell gridCellElement1 = new GridContentCell();
        //    gridCellElement1.Value = "Content";
        //    gridRow.Cells.Add(gridCellElement1);
        //    GridContentCell gridCellElement2 = new GridContentCell();
        //    gridCellElement2.Value = "new ";
        //    gridRow.Cells.Add(gridCellElement2);

        //    //Add to grid
        //    gridElement.Columns.Add(col);
        //    gridElement.Columns.Add(a);
        //    gridElement.Rows.Add(gridRow);
           
        //    gridElement.RefreshData();
        //    gridElement.Refresh();
          
        //}

        private void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "Grid SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + TestedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }

        }

        private void SelectCell_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            
            TestedGrid.SelectCell(1, 0);

            //lblLog.Text = "Grid SelectedCells values: ";

            //for (int i = 0; i < e.SelectedCells.Count; i++)
            //{
            //    if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
            //    {
            //        lblLog.Text += " (" + TestedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
            //    }
            //}  

        }


	}
}
