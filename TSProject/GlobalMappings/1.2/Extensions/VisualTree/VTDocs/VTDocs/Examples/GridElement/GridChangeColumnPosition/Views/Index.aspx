<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid ChangeColumnPosition() Method" ID="windowView" LoadAction="GridChangeColumnPosition\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ChangeColumnPosition" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Sets the bounds of the control to the specified location and size.

            Syntax: public void ChangeColumnPosition(int left, int top, int width, int height)" Top="75px" ID="Label1" ></vt:Label>
                 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example you can invoke the ChangeColumnPosition method using the buttons below." Top="235px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" Top="290px" ID="TestedGrid" AllowColumnReorder="true" AllowUserToOrderColumns="true">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="420px" Height="80px" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Move Column1 in circle >>" Left="50px" Top="560px" Width="210px" ID="btnMoveColumn1" ClickAction="GridChangeColumnPosition\btnMoveColumn1_Click"></vt:Button>

           <vt:Button runat="server" Text="Move Column2 in circle >>" Top="560px" Left="280px" Width="210px" ID="btnMoveColumn2" ClickAction="GridChangeColumnPosition\btnMoveColumn2_Click"></vt:Button>

        <vt:Button runat="server" Text="Move Column3 in circle >>" Top="560px" Left="510px" Width="210px" ID="btnMoveColumn3" ClickAction="GridChangeColumnPosition\btnMoveColumn3_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

