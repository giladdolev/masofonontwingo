<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree Font Property" ID="windowView2" LoadAction="TreeFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Tree runat="server" Text="Tree" Top="185px" Height="80px" ID="TestedTree1" >
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="275px" Width="430px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedTree is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="400px"  ID="lblExp1"></vt:Label>     
              
        <vt:Tree runat="server" Text="TestedTree" Font-Names="Niagara Engraved" Height="80px" Font-Italic="true" Font-Size="30" Top="455px" ID="TestedTree2" >
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree2Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>
        <vt:Label runat="server" SkinID="Log" Top="545px" Width="430px" Height="45px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="630px" ID="btnChangeTreeFont" ClickAction="TreeFont/btnChangeTreeFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
