<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox PerformMouseUp() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseUp of 'Tested RichTextBox'" Top="45px" Left="140px" ID="btnPerformMouseUp" Height="36px" Width="300px" ClickAction="RichTextBoxPerformMouseUp\btnPerformMouseUp_Click"></vt:Button>


        <vt:RichTextBox runat="server" Text="Tested RichTextBox" Top="150px" Left="200px" ID="testedRichTextBox" Height="70px"  Width="200px" MouseUpAction="RichTextBoxPerformMouseUp\testedRichTextBox_MouseUp"></vt:RichTextBox>           


    </vt:WindowView>
</asp:Content>
