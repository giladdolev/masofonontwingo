using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonAutoSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeAutoSize_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            ButtonElement btnChangeAutoSize = this.GetVisualElementById<ButtonElement>("btnChangeAutoSize");

            if (btnTestedButton.AutoSize == false)
            {
                btnTestedButton.AutoSize = true;
                btnChangeAutoSize.Text = "AutoSize = True";
            }
            else
            { 
                btnTestedButton.AutoSize = false;
                btnChangeAutoSize.Text = "AutoSize = False";
            }
        }
        //btnChangeText_Click
        private void btnChangeText_Click(object sender, EventArgs e)
        {
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");
            if (btnTestedButton.Text == "Tested Button")
            {
                btnTestedButton.Text = "Some new Text";
           
            }
            else
                btnTestedButton.Text = "Tested Button";
        }
      
    }
}