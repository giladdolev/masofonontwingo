<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel BorderStyle Property" ID="windowView1" LoadAction="PanelBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <!-- TestedPanel1 -->
        <vt:Panel runat="server"  Text="Panel" Top="170px" ID="TestedPanel1"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>
 
 


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="315px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this TestedPanel is initially set to 'Dotted'" Top="360px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedPanel2 -->
        <vt:Panel runat="server"  Text="TestedPanel" BorderStyle="Dotted" Top="405px" ID="TestedPanel2"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="500px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="560px" ID="btnChangeBorderStyle" ClickAction="PanelBorderStyle\btnChangeBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
        