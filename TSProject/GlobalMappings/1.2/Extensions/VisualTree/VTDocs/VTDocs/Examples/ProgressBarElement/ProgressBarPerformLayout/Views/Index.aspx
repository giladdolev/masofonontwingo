<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar PerformLayout() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformLayout of 'Tested ProgressBar'" Top="45px" Left="140px" ID="btnPerformLayout" Height="36px" Width="300px" ClickAction="ProgressBarPerformLayout\btnPerformLayout_Click"></vt:Button>


        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="150px" Left="200px" ID="testedProgressBar" Height="36px"  Width="200px" LayoutAction="ProgressBarPerformLayout\testedProgressBar_Layout"></vt:ProgressBar>           

        

    </vt:WindowView>
</asp:Content>
