using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ListView bounds are: \\r\\n" + TestedListView.Bounds;

        }
        public void btnChangeListViewBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
            if (TestedListView.Bounds == new Rectangle(100, 340, 300, 50))
            {
                TestedListView.Bounds = new Rectangle(80, 355, 273, 99);
                lblLog.Text = "ListView bounds are: \\r\\n" + TestedListView.Bounds;

            }
            else
            {
                TestedListView.Bounds = new Rectangle(100, 340, 300, 50);
                lblLog.Text = "ListView bounds are: \\r\\n" + TestedListView.Bounds;
            }

        }

    }
}