<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox DoubleClick event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      
        <vt:CheckBox runat="server" Text="Double Click Me" Top="45px" Left="140px" ID="chkDoubleClick" Height="36px"  Width="100px" DoubleClickAction ="CheckBoxDoubleClick\chkDoubleClick_DoubleClick"></vt:CheckBox>          

        <vt:TextBox runat="server" Text="" Top="50px" Left="260px" ID="txtEventTrack" Multiline="true" Height="60px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
