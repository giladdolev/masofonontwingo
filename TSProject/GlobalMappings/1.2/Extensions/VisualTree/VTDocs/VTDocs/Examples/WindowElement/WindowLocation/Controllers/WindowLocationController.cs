using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowLocationController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowLocation());
        }
        private WindowLocation ViewModel
        {
            get { return this.GetRootVisualElement() as WindowLocation; }
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            
           if(this.ViewModel.Location == new Point(11, 57))
           {
               this.ViewModel.Location = new Point(50, 100);
               textBox1.Text = this.ViewModel.Location.ToString();
           }
           else
           {
               this.ViewModel.Location = new Point(11, 57);
               textBox1.Text = this.ViewModel.Location.ToString();

           }
        }
    }
}