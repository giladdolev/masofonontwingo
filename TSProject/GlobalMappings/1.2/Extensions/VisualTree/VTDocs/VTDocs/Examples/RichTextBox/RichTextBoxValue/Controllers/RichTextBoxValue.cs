using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxValueController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeValue_Click(object sender, EventArgs e)
        {
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (testedRichTextBox.Value.Contains("MyValue"))
            {
                testedRichTextBox.Value = "NewValue";
                textBox1.Text = "Value:\\r\\n" + testedRichTextBox.Value;
            }
            else
            {
                testedRichTextBox.Value = "MyValue";
                textBox1.Text = "Value:\\r\\n" + testedRichTextBox.Value;
            }
            
        }

    }
}