﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class LabelAccessibleRoleController : Controller 
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnAccessibleRole_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("label");
            TextBoxElement txtAccessibleRole = this.GetVisualElementById<TextBoxElement>("txtAccessibleRole");

            label.AccessibleRole = AccessibleRole.Clock;
            txtAccessibleRole.Text = Convert.ToInt32(label.AccessibleRole).ToString();
          
        }
      
    }
}