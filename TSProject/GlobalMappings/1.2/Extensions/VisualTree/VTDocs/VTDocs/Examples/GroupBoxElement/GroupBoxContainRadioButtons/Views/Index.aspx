<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox that contains RadioButtons" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="Click on each of the RadioButtons and verify that each time only one radioButton is checked" Top="50px" Left="50px" ID="labelInitialized" Width="700px" Font-Bold="true"></vt:Label>
        <vt:GroupBox runat="server" Text="GroupBox" Top="100px" Left="128px" ID="groupBox" BorderStyle="Inset" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Blue" Height="150px" Width="400px">
      	    <vt:RadioButton runat="server" CheckAlign="MiddleLeft" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" AutoSize="True" Text="radioButton1" Top="24px" Left="24px" ID="radioButton1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" Width="85px">
			</vt:RadioButton>
			<vt:RadioButton runat="server" CheckAlign="MiddleLeft" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" AutoSize="True" Text="radioButton2" Top="59px" Left="24px" ID="radioButton2" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" TabIndex="2" Width="85px">
			</vt:RadioButton>
			<vt:RadioButton runat="server" CheckAlign="MiddleLeft" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" AutoSize="True" Text="radioButton3" Top="99px" Left="24px" ID="radioButton3" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="17px" TabIndex="4" Width="85px">
			</vt:RadioButton>
        </vt:GroupBox>
    </vt:WindowView>
</asp:Content>
