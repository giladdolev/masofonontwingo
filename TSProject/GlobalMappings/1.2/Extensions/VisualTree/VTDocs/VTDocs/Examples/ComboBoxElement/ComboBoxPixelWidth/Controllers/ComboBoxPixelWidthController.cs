using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxPixelWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedComboBox.Items.Add("Item1");
            TestedComboBox.Items.Add("Item2");
            TestedComboBox.Items.Add("Item3");
            lblLog.Text = "PixelWidth value: " + TestedComboBox.PixelWidth + '.';
        }

        private void btnChangePixelWidth_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedComboBox.PixelWidth == 165)
            {
                TestedComboBox.PixelWidth = 300;
                lblLog.Text = "PixelWidth value: " + TestedComboBox.PixelWidth + '.';
            }
            else
            {
                TestedComboBox.PixelWidth = 165;
                lblLog.Text = "PixelWidth value: " + TestedComboBox.PixelWidth + '.';
            }
        }

    }
}