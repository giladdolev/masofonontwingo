<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Opacity="1" MaximizeBox="True" MinimizeBox="True" AutoScaleMode="Font" AutoScaleDimensions="6, 13" Text="Formush" LoadAction="TopMostWindow\Form1_Load" ID="Form1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="262px" Width="777px">
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="OpenForms" Top="81px" Left="92px" ClickAction="TopMostWindow\button1_Click" ID="button1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="88px" Width="177px">
		</vt:Button>
		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="TopMost" Top="12px" Left="478px" ClickAction="TopMostWindow\btnTopMost_Click" ID="btnTopMost" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="116px">
		</vt:Button>
	</vt:WindowView>
</asp:Content>
