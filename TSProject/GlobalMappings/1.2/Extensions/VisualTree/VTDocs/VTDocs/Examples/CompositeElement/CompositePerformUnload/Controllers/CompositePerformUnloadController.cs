using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CompositePerformUnloadController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult Index()
        {
            return View();
        }
        private CompositePerformUnload ViewModel
        {
            get { return this.GetRootVisualElement() as CompositePerformUnload; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Click the button below...";
        }


        //public void btnReload_click(object sender, EventArgs e)
        //{
        //    CompositeElement CompositeIndex = this.GetVisualElementById<CompositeElement>("CompositeIndex");
        //    VisualElementHelper.ReloadView<CompositeElement>(CompositeIndex, "Composite1", "CompositeIndex");
        //}
   
        public void btnPerformUnload_Click(object sender, EventArgs e)
        {
            CompositeElement CompositeIndex = this.GetVisualElementById<CompositeElement>("CompositeIndex");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            CancelEventArgs args = new CancelEventArgs();
            CompositeIndex.PerformUnloading(args);

            lblLog.Text = "PerformUnload was invoked";
        }


        //private Dictionary<string, object> GetArgsDictionary()
        //{
        //    Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
        //    argsDictionary.Add("ElementName", "WindowElement");
        //    argsDictionary.Add("ExampleName", "MDIWindowPartialControl");
        //    return argsDictionary;
        //}


    }
}