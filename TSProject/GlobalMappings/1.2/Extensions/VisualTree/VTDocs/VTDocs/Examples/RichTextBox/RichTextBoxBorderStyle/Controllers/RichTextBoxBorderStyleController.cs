using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox1 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox1");
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedRichTextBox1.BorderStyle;
            lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
        }

        public void btnChangeRichTextBoxBorderStyle_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox2 = this.GetVisualElementById<RichTextBox>("TestedRichTextBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedRichTextBox2.BorderStyle == BorderStyle.None)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Dotted)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Double)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Groove)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Inset)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Dashed)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.NotSet)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Outset)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Ridge)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else if (TestedRichTextBox2.BorderStyle == BorderStyle.Solid)
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
            else
            {
                TestedRichTextBox2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: "+ TestedRichTextBox2.BorderStyle + '.';
            }
        }


    }
}