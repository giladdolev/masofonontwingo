<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Hide Method" ID="windowView1" LoadAction="ButtonHide\OnLoad">
             
        <vt:Label runat="server" SkinID="Title" Text="Hide" ID="lblTitle" >
        </vt:Label>

        <vt:Label runat="server" Text="Conceals the control from the user.

            Syntax: public void Hide()
            
            Hiding the control is equivalent to setting the Visible property to 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>   

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example the Visible property is initially set to 'true'. 
            When invoking Hide() method, the Visible property value is changed to 'false'." Top="290px" ID="lblExp2">
        </vt:Label>

        <vt:Button runat="server" Text="TestedButton" Top="360px" Visible="true" ID="TestedButton"></vt:Button>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="400"></vt:Label>

        <vt:Button runat="server" Text="Hide >>" Top="475px" ID="btnHide" ClickAction="ButtonHide\btnHide_Click"></vt:Button>

        <vt:Button runat="server" Text="Show >>" Left="300" Top="475px" ID="btnShow" ClickAction="ButtonHide\btnShow_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
