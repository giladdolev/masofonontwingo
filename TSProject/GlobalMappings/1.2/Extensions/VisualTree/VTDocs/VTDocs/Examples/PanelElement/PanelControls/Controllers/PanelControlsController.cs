using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelControlsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnAddControls_Click(object sender, EventArgs e)
        {
            PanelElement grpControls = this.GetVisualElementById<PanelElement>("grpControls");

            grpControls.Controls.Add(new LabelElement { Text = "newLabel1", ID = "label1", Left = 20, Top = 30, Height = 15, Width = 100 });
            grpControls.Controls.Add(new LabelElement { Text = "newLabel2", ID = "label2", Left = 20, Top = 100, Height = 15, Width = 100 });
            grpControls.Controls.Add(new LabelElement { Text = "newLabel2", ID = "label3", Left = 20, Top = 70, Height = 15, Width = 100 });
        }

    }
}