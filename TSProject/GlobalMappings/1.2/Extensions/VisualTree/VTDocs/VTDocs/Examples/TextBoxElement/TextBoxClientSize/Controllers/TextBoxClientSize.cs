using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedTextBox.ClientSize;

        }
        public void btnChangeTextBoxClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement TestedTextBox = this.GetVisualElementById<TextBoxElement>("TestedTextBox");
            if (TestedTextBox.Width == 200)
            {
                TestedTextBox.ClientSize = new Size(150, 26);
                lblLog.Text = "ClientSize value: " + TestedTextBox.ClientSize;

            }
            else
            {
                TestedTextBox.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedTextBox.ClientSize;
            }

        }

    }
}