<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox DropDownStyle property" EnableEsc="true" ID="windowView2" LoadAction="ComboBoxDropDownStyle/OnLoad">
      
        <vt:Label runat="server" SkinID="Title" Text="DropDownStyle" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value specifying the style of the combo box.

            Syntax: public ComboBoxStyle DropDownStyle { get; set; }
            Values: 
            DropDown - The list is displayed by clicking the down arrow and the text portion is editable. (Default)
            DropDownList - The list is displayed by clicking the down arrow and the text portion is not editable.
            Simple - The list is always visible and the text portion is editable."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox"  ID="TestedComboBox1"  CssClass="vt-test-cmb-1" Top="230px"> </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="265px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="315px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="DropDownStyle property of this ComboBox is initially set to DropDownList.
            You can iterate over the DropDownStyle values by clicking on the 'Change DropDownStyle value' button."
            Top="355px" ID="lblExp2">
        </vt:Label>
         
        <vt:ComboBox runat="server" Text="TestedComboBox" DropDownStyle="DropDownList" Height="95px" CssClass="vt-test-cmb-2" Top="420px" ID="TestedComboBox2"> </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="525px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change DropDownStyle value >>" Width="200px" Top="590px" ID="btnChangeDropDownStyle" ClickAction="ComboBoxDropDownStyle\btnChangeDropDownStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

