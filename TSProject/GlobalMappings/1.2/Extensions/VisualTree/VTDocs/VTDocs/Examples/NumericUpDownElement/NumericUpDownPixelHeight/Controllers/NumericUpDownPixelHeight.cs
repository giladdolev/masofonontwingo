using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownPixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelHeight value: " + TestedNumericUpDown.PixelHeight + '.';
        }

        private void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            NumericUpDownElement TestedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("TestedNumericUpDown");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedNumericUpDown.PixelHeight == 80)
            {
                TestedNumericUpDown.PixelHeight = 24;
                lblLog.Text = "PixelHeight value: " + TestedNumericUpDown.PixelHeight + '.';
            }
            else
            {
                TestedNumericUpDown.PixelHeight = 80;
                lblLog.Text = "PixelHeight value: " + TestedNumericUpDown.PixelHeight + '.';
            }
        }
      
    }
}