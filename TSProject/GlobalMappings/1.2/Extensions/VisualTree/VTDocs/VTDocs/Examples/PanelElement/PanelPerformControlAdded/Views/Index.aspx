<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel PerformControlAdded() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:button runat="server" Text="PerformControlAdded of 'Tested Panel'" Top="45px" Left="140px" ID="btnPerformControlAdded" Height="36px" Width="300px" ClickAction="PanelPerformControlAdded\btnPerformControlAdded_Click"></vt:button>


        <vt:Panel  runat="server" Text="Tested Panel" Top="150px" Left="200px" ID="testedPanel" Height="70px"  Width="200px" ControlAddedAction="PanelPerformControlAdded\testedPanel_ControlAdded"></vt:Panel>           


    </vt:WindowView>
</asp:Content>
