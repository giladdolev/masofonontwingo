using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement pnlRuntimeDraggable = this.GetVisualElementById<PanelElement>("pnlRuntimeDraggable");

            if (pnlRuntimeDraggable.Draggable == false)
            {
                pnlRuntimeDraggable.Draggable = true;
                textBox1.Text = pnlRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                pnlRuntimeDraggable.Draggable = false;
                textBox1.Text = pnlRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}