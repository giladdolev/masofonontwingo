using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            PanelElement pnlRuntimeCursor = this.GetVisualElementById<PanelElement>("pnlRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (pnlRuntimeCursor.Cursor == CursorsElement.Default)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.Help)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.No)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;

            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else if (pnlRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                pnlRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
            else 
            {
                pnlRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + pnlRuntimeCursor.Cursor;
            }
        }

    }
}