using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxSelectionLengthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnSetSelectionLength_Click(object sender, EventArgs e)
        {
            RichTextBox rtfSelectionLength = this.GetVisualElementById<RichTextBox>("rtfSelectionLength");

            rtfSelectionLength.SelectionLength = 10;
        }
        public void btnGetSelectionLength_Click(object sender, EventArgs e)
        {
            RichTextBox rtfSelectionLength = this.GetVisualElementById<RichTextBox>("rtfSelectionLength");
            if (rtfSelectionLength.SelectionLength == 0)
                MessageBox.Show("No text is selected");
            else
                MessageBox.Show(rtfSelectionLength.SelectionLength.ToString());
        }


    }
}