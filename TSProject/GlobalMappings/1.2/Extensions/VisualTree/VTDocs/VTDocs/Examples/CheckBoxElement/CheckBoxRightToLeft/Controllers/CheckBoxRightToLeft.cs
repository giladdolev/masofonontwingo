using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        // Change RightToLeft property
        /// <summary>
        /// Handles the Click event of the chkChangeRightToLeft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void chkChangeRightToLeft_CheckedChanged(object sender, EventArgs e)
        {
            CheckBoxElement chkChangeRightToLeft = this.GetVisualElementById<CheckBoxElement>("chkChangeRightToLeft");
            if (chkChangeRightToLeft.RightToLeft == RightToLeft.No)
            {
                chkChangeRightToLeft.RightToLeft = RightToLeft.Yes;
            }
            else if (chkChangeRightToLeft.RightToLeft == RightToLeft.Yes)
            {
                chkChangeRightToLeft.RightToLeft = RightToLeft.Inherit;
            }
            else if (chkChangeRightToLeft.RightToLeft == RightToLeft.Inherit)
            {
                chkChangeRightToLeft.RightToLeft = RightToLeft.No;
            }
        }

    }
}