using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxLocationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Location value: " + TestedGroupBox.Location;
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            GroupBoxElement TestedGroupBox = this.GetVisualElementById<GroupBoxElement>("TestedGroupBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedGroupBox.Location == new Point(80, 310))
            {
                TestedGroupBox.Location = new Point(130, 290);
                lblLog.Text = "Location value: " + TestedGroupBox.Location;
            }
            else
            {
                TestedGroupBox.Location = new Point(80, 310);
                lblLog.Text = "Location value: " + TestedGroupBox.Location;
            }
        }

    }
}