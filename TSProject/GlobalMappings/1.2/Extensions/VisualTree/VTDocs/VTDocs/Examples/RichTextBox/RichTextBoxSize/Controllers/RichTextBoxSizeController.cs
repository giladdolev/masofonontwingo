using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedRichTextBox.Size;
        }

        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRichTextBox.Size == new Size(300, 120))   //Get
            {
                TestedRichTextBox.Size = new Size(200, 80);    //Set
                lblLog.Text = "Size value: " + TestedRichTextBox.Size;
            }
            else
            {
                TestedRichTextBox.Size = new Size(300, 120);
                lblLog.Text = "Size value: " + TestedRichTextBox.Size;
            }
        }

    }
}