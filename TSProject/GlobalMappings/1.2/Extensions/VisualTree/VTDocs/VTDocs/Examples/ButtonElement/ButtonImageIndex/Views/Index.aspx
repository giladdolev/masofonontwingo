<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button ButtonImageIndex Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ButtonImageIndex\load_action">
      
        <vt:Button runat="server" Text="fill image list" Image ="~/Content/Elements/Button.png" Top="45px" Left="140px" ID="btnButtonImageIndex" Height="36px" TabIndex="1" Width="200px" ClickAction="ButtonImageIndex/fillimglst_btnclick"></vt:Button>  
          
       

        <vt:Button runat="server" Text="Change Button Image" Top="115px" Left="140px" ID="btnChangeImage" Height="36px"  TabIndex="1" Width="200px" ClickAction="ButtonImageIndex/btnChangeImage_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
