<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Text="Image Size Property" ID="windowView1" LoadAction="ImageSize/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Size" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height and width of the control.
            
            Syntax: public Size Size { get; set; }
            Property Value: The Size that represents the height and width of the control in pixels." Top="75px"  ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Size property of this Image is initially set to:  Width='150px' Height='90px'" Top="250px" ID="lblExp"></vt:Label>

        <!-- TestedImage -->
        <vt:Image runat="server" Text="TestedImage" Top="300px" ID="TestedImage"></vt:Image>

        <vt:Label runat="server" SkinID="Log" Top="405px" Width="360px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Size value >>" Width="180px" Top="470px" ID="btnChangeImageSize" ClickAction="ImageSize/btnChangeImageSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
