<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView FindForm" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewFindForm/Form_Load">
        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ListView runat="server" ID="listView1" Top="160px" Left="50px" Height="150px" Width="500px"/>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Get ListView1 form" Top="112px" Left="130px" ID="btnFindForm" Height="30px" TabIndex="1" Width="250px" ClickAction="ListViewFindForm\btnFindForm_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
