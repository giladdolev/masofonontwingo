<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="NumericUpDown BorderStyle Property" ID="windowView1" LoadAction="NumericUpDownBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:NumericUpDown runat="server" Text="NumericUpDown" Top="170px" ID="TestedNumericUpDown1"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="205px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="270px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this NumericUpDown is initially set to 'Dotted'" Top="310px"  ID="lblExp1"></vt:Label>     
        
        <vt:NumericUpDown runat="server"  Text="TestedNumericUpDown" BorderStyle="Dotted" Top="350px" ID="TestedNumericUpDown2"></vt:NumericUpDown>

        <vt:Label runat="server" SkinID="Log" Top="385px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px" Top="455px" ID="btnChangeNumericUpDownBorderStyle" ClickAction="NumericUpDownBorderStyle\btnChangeNumericUpDownBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
