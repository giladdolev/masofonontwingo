<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox PerformMouseDown() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformMouseDown of 'Tested RichTextBox'" Top="45px" Left="140px" ID="btnPerformMouseDown" Height="36px" Width="300px" ClickAction="RichTextBoxPerformMouseDown\btnPerformMouseDown_Click"></vt:Button>


        <vt:RichTextBox runat="server" Text="Tested RichTextBox" Top="150px" Left="200px" ID="testedRichTextBox" Height="70px"  Width="200px" MouseDownAction="RichTextBoxPerformMouseDown\testedRichTextBox_MouseDown"></vt:RichTextBox>           

        

    </vt:WindowView>
</asp:Content>
