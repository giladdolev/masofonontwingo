using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerLocationChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");

            if (testedSplitContainer.Location == new Point(140, 90))
            {
                testedSplitContainer.Location = new Point(200, 500);
            }
            else
            {
                testedSplitContainer.Location = new Point(140, 90);
            }
        }

        public void testedSplitContainer_LocationChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "SplitContainer LocationChanged event is invoked\\r\\n";
        }

    }
}