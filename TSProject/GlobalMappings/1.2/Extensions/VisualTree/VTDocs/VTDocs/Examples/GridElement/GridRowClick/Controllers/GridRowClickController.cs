﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridRowClickController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Click on the grid row headers or cells...";
        }

        public void TestedGrid_RowClick(object sender, GridRowClickEventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            txtEventLog.Text += "\\r\\nRowClick event is invoked, RowHandle: " + e.RowHandle;
        }

        public void TestedGrid_SelectionChanged(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            //TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            //txtEventLog.Text += "\\r\\nGrid SelectionChanged event is invoked";

            lblLog.Text = "Grid SelectedCells values: ";

            for (int i = 0; i < e.SelectedCells.Count; i++)
            {
                if ((e.SelectedCells[i].RowIndex >= 0) && (e.SelectedCells[i].ColumnIndex >= 0))
                {
                    lblLog.Text += " (" + testedGrid.Rows[e.SelectedCells[i].RowIndex].Cells[e.SelectedCells[i].ColumnIndex].Value + ") ";
                }
            }
        }
    }
}
