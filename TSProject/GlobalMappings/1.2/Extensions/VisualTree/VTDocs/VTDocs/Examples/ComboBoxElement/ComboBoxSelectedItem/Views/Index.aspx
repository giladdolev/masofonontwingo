<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox SelectedItem Method" ID="windowView2" LoadAction="ComboBoxSelectedItem\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="SelectedItem" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Gets or sets currently selected item in the ComboBox.

            Syntax: public object SelectedItem { get; set; }
            
            The object that is the currently selected item or null if there is no currently selected item." Top="75px" ID="lblDefinition"></vt:Label>

        <vt:ComboBox runat="server" Text="ComboBox" ID="TestedComboBox1"  Top="180px" SelectedIndexChangedAction="ComboBoxSelectedItem\TestedComboBox1_SelectedIndexChanged"> </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="220px" ID="lblLog1"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="305px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can change the SelectedItem property by selecting an item 
              from the ComboBox or by pressing the 'Change SelectedItem Value' button.
              Pressing the button will set the SelectedItem property, each click selects a different item, in a cyclic order.
              The initial SelectedItem value is 'Item2'." 
            Top="355px" ID="lblExp2"></vt:Label>

        <vt:ComboBox runat="server" Text="TestedComboBox" CssClass="vt-TestedComboBox" Top="465px" ID="TestedComboBox2" SelectedIndexChangedAction="ComboBoxSelectedItem\TestedComboBox2_SelectedIndexChanged">
        </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" ID="lblLog2" Top="505px"></vt:Label>

        <vt:Button runat="server" Text="Change SelectedItem Value >>" Top="595px" ID="btnChooseNext" Width="180px" ClickAction="ComboBoxSelectedItem\ChooseNext_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

