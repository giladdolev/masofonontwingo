using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridSelectedRowsController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void Form_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement labelLog = this.GetVisualElementById<LabelElement>("lblLog2");
            
            gridElement.Rows.Add("a", "b", "c");
            gridElement.Rows.Add("d", "e", "f");
            gridElement.Rows.Add("g", "h", "i");

            labelLog.Text = "Please select some rows and press the button." ;
     
        }

        public void RowSelectionChanged_Click(object sender, GridElementSelectionChangeEventArgs e)
        {
            LabelElement labelLog = this.GetVisualElementById<LabelElement>("lblLog2");

            labelLog.Text = "Please select some rows and press the button." ;

        }

        public void SelectedRowsCount_click(object sender, EventArgs e)
        {
            GridElement grid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement labelLog = this.GetVisualElementById<LabelElement>("lblLog2");
            labelLog.Text = "Number of SelectedRows: " + grid.SelectedRows.Count;
        }
        
    }
}
