using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonPerformControlAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformControlAdded_Click(object sender, EventArgs e)
        {
            
            RadioButtonElement testedRadioButton = this.GetVisualElementById<RadioButtonElement>("testedRadioButton");

            ControlEventArgs args = new ControlEventArgs(testedRadioButton);

            testedRadioButton.PerformControlAdded(args);
        }

        public void testedRadioButton_ControlAdded(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRadioButton ControlAdded event method is invoked");
        }

    }
}