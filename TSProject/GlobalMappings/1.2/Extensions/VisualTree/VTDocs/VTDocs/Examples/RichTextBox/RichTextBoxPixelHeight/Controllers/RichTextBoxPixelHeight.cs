using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPixelHeightController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelHeight value: " + TestedRichTextBox.PixelHeight + '.';
        }

        private void btnChangePixelHeight_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRichTextBox.PixelHeight == 80)
            {
                TestedRichTextBox.PixelHeight = 45;
                lblLog.Text = "PixelHeight value: " + TestedRichTextBox.PixelHeight + '.';
            }
            else
            {
                TestedRichTextBox.PixelHeight = 80;
                lblLog.Text = "PixelHeight value: " + TestedRichTextBox.PixelHeight + '.';
            }
        }
      
    }
}