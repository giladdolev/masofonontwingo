﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectionChanged event" ID="windowView2" Height="750px" LoadAction="GridSelectionChanged3CheckBoxColumnQA\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current selection changes.

            Syntax: public event GridElementCellEventHandler SelectionChanged"
            Top="75px" ID="Label2">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="165px" ID="Label3"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells from the grid by clicking with the mouse or by pressing 
            the buttons. Each invocation of the 'BeforeSelectionChanged' and the 'SelectionChanged' events should  
            add a line to the 'Event Log'."
            Top="215px" ID="Label4">
        </vt:Label>

         <vt:Grid runat="server" Top="300px" ID="TestedGrid" BeforeSelectionChangedAction="GridSelectionChanged3CheckBoxColumnQA\TestedGrid_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged3CheckBoxColumnQA\TestedGrid_SelectionChanged">
            <Columns>
                <vt:GridCheckBoxColumn runat="server" ID="GridCheckBoxColumn1" HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-chbCol-1"></vt:GridCheckBoxColumn>
                <vt:GridCheckBoxColumn runat="server" ID="GridCheckBoxColumn2" HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-chbCol-2"></vt:GridCheckBoxColumn>
                <vt:GridCheckBoxColumn runat="server" ID="GridCheckBoxColumn3" HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-chbCol-3"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Grid runat="server" Top="300px" Left="380px" RowHeaderVisible="false" ID="TestedGrid2" BeforeSelectionChangedAction="GridSelectionChanged3CheckBoxColumnQA\TestedGrid2_BeforeSelectionChanged" SelectionChangedAction="GridSelectionChanged3CheckBoxColumnQA\TestedGrid2_SelectionChanged">
            <Columns>
                <vt:GridCheckBoxColumn runat="server" ID="GridCheckBoxColumn4" HeaderText="Column1" Height="20px" Width="85px" CssClass="vt-chbCol-4"></vt:GridCheckBoxColumn>
                <vt:GridCheckBoxColumn runat="server" ID="GridCheckBoxColumn5" HeaderText="Column2" Height="20px" Width="85px" CssClass="vt-chbCol-5"></vt:GridCheckBoxColumn>
                <vt:GridCheckBoxColumn runat="server" ID="GridCheckBoxColumn6" HeaderText="Column3" Height="20px" Width="85px" CssClass="vt-chbCol-6"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="430px" height="40px" Width="550px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="490px" Width="550px" Height="140px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Select middle cell (Left) >>" Top="655px" Width="180px" ID="btnSelectMiddleCellLeft" ClickAction="GridSelectionChanged3CheckBoxColumnQA\btnSelectMiddleCellLeft_Click"></vt:Button>

        <vt:Button runat="server" Text="Select middle cell (Right) >>" Top="655px" Width="180px" Left="280px" ID="btnSelectMiddleCellRight" ClickAction="GridSelectionChanged3CheckBoxColumnQA\btnSelectMiddleCellRight_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Row[0] >>" Top="695px" ID="btnSelectRow0" Width="180px" ClickAction="GridSelectionChanged3CheckBoxColumnQA\btnSelectRow0_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Column[0] >>" Top="695px" Left="280px" Width="180px" ID="btnSelectColumn0" ClickAction="GridSelectionChanged3CheckBoxColumnQA\btnSelectColumn0_Click"></vt:Button>

        <vt:Button runat="server" Text="Clear Event Log >>" Top="655px" Left="480px" ID="btnClear" ClickAction="GridSelectionChanged3CheckBoxColumnQA\btnClear_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

