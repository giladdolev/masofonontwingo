﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"  %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CellContentClick event" ID="Form1"  LoadAction="GridCellContentClick\OnLoad">
         <vt:Label runat="server" SkinID="Title" Text="CellContentClick" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the content within a cell is clicked.
            This event is invoked before the MouseDownButtonClick event.
            It Only accurs when we click on cell item and not header or an empty grid

            Syntax: public event EventHandler<GridElementCellEventArgs> CellContentClick
            
            It also occurs when the user presses and releases the SPACEBAR while a button cell or check box cell 
            has focus, and will occur twice for these cell types if the cell content is clicked while pressing the SPACEBAR."
                        Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select cells from the grid by clicking with the mouse. 
            Each invocation of the 'CellContentClick' event should add a line to the 'Event Log'."
            Top="290px" ID="lblExp2">
        </vt:Label>
              
         <vt:Grid runat="server" Top="350px" ID="TestedGrid" CellContentClickAction="GridCellContentClick\TestedGrid_CellContentClick">
            <Columns>         
                <vt:GridTextBoxColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridCheckBoxColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridCheckBoxColumn>
                <vt:GridButtonColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridButtonColumn>
            </Columns>
        </vt:Grid>
        
         <vt:Label runat="server" SkinID="Log" Top="480px" ID="lblLog"></vt:Label>

         <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="530px" ID="txtEventLog"></vt:TextBox>
         
    </vt:WindowView>
</asp:Content>
