using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested CheckBox bounds are set to: \\r\\nLeft  " + TestedCheckBox.Left + ", Top  " + TestedCheckBox.Top + ", Width  " + TestedCheckBox.Width + ", Height  " + TestedCheckBox.Height;

        }
        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement TestedCheckBox = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox");
            if (TestedCheckBox.Left == 100)
            {
                TestedCheckBox.SetBounds(80, 300, 150, 26);
                lblLog.Text = "The Tested CheckBox bounds are set to: \\r\\nLeft  " + TestedCheckBox.Left + ", Top  " + TestedCheckBox.Top + ", Width  " + TestedCheckBox.Width + ", Height  " + TestedCheckBox.Height;

            }
            else
            {
                TestedCheckBox.SetBounds(100, 280, 200, 50);
                lblLog.Text = "The Tested CheckBox bounds are set to: \\r\\nLeft  " + TestedCheckBox.Left + ", Top  " + TestedCheckBox.Top + ", Width  " + TestedCheckBox.Width + ", Height  " + TestedCheckBox.Height;
            }
        }
    }
}