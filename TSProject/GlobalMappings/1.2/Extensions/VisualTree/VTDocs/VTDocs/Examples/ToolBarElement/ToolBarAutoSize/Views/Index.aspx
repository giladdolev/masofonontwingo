<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar AutoSize Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="1000px">
		
        <vt:ToolBar runat="server" Text="toolStrip1" Dock="None" Top="102px" Left="276px" ID="toolStrip1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="25px" Width="466px">
		</vt:ToolBar>

		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Change AutoSize" Top="80px" Left="44px" ClickAction="ToolBarAutoSize\btnChangeAutoSize_Click" ID="btnChangeAutoSize" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="118px">
		</vt:Button>

		<vt:Button runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" Text="Change Size" Top="125px" Left="44px" ClickAction="ToolBarAutoSize\btnChangeSize_Click" ID="btnChangeSize" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="28px" TabIndex="2" Width="118px">
		</vt:Button>

		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="230px" Left="400px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="198px">
		</vt:TextBox>

		<vt:Label runat="server" Text="AutoSize is initially set to true, when the AutoSize is set to true the toolBar size can not be changed" Top="9px" Left="41px" ID="label1" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="9.75pt" Height="16px" TabIndex="4" Width="682px">
		</vt:Label>

        </vt:WindowView>
</asp:Content>