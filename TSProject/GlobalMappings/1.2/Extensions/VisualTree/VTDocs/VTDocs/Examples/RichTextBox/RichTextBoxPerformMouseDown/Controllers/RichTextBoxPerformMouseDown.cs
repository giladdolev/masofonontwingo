using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPerformMouseDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseDown_Click(object sender, EventArgs e)
        {
            
            RichTextBox testedRichTextBox = this.GetVisualElementById<RichTextBox>("testedRichTextBox");

            MouseEventArgs args = new MouseEventArgs();

            testedRichTextBox.PerformMouseDown(args);
        }

        public void testedRichTextBox_MouseDown(object sender, EventArgs e)
        {
            MessageBox.Show("TestedRichTextBox MouseDown event method is invoked");
        }

    }
}