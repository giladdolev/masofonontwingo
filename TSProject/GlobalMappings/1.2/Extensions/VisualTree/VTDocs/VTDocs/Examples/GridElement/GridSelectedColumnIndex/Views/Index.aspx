﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
        <vt:WindowView runat="server" Text="Grid SelectedColumnIndex Property"  ID="windowView1" LoadAction="GridSelectedColumnIndex\OnLoad">

        <vt:Label runat="server" SkinID="Title" Left="250px" Text="SelectedColumnIndex" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Returns the zero-based index of the current column.
            
            public int SelectedColumnIndex { get; }
            If no column has been selected the SelectedRowIndex value is -1" Top="75px"  ID="lblDefinition"></vt:Label>
              
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example you can change the value of SelectedColumnIndex property
             by clicking on the Grid cells and columns headers" Top="250px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" Text="TestedGrid" SelectedColumnIndex="2" CssClass="vt-testedGrid2"  Top="435px" ID="TestedGrid2" SelectionChangedAction="GridSelectedColumnIndex\TestedGrid2_SelectionChnaged"
            ColumnHeaderMouseClickAction="GridSelectedColumnIndex\TestedGrid2_SelectionChnaged" RowHeaderVisible="false">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="445px" ID="lblLog"></vt:Label>

    </vt:WindowView>
</asp:Content>
