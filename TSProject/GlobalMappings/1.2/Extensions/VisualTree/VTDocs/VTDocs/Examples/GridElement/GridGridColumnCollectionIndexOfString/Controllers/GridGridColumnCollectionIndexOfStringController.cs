﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
using System.Drawing;
namespace HelloWorldApp
{
    public class GridGridColumnCollectionIndexOfStringController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog.Text = "Search Column Index by using the IndexOf >> button...";
        }

        private void btnIndexOf_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            lblLog.Text = "IndexOf(" + txtInput.Text + ") output: " + TestedGrid.Columns.IndexOf(txtInput.Text) + ".\\r\\nInputText: " + txtInput.Text;
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtInput = this.GetVisualElementById<TextBoxElement>("txtInput");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            if (txtInput.Text.Contains("ColumnI"))
            {
                txtInput.Text = "";
                txtInput.ForeColor = Color.Black;
            }
            string temp = lblLog.Text;
            string[] lines = temp.Split(new string[] { "\\r\\n" }, StringSplitOptions.None);
            lblLog.Text = lines[0];
            lblLog.Text += "\\r\\nInputText: " + txtInput.Text;
        }
        
    }
}
