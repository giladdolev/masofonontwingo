using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxScrollBarsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeScrollBars_Click(object sender, EventArgs e)
        {
            TextBoxElement txtScrollBars = this.GetVisualElementById<TextBoxElement>("txtScrollBars");

            if (txtScrollBars.ScrollBars == ScrollBars.Both)
                txtScrollBars.ScrollBars = ScrollBars.Horizontal;

            else if (txtScrollBars.ScrollBars == ScrollBars.Horizontal)
                txtScrollBars.ScrollBars = ScrollBars.LargeChange;

            else if (txtScrollBars.ScrollBars == ScrollBars.LargeChange)
                txtScrollBars.ScrollBars = ScrollBars.Maximum;

            else if (txtScrollBars.ScrollBars == ScrollBars.Maximum)
                txtScrollBars.ScrollBars = ScrollBars.Minimum;

            else if (txtScrollBars.ScrollBars == ScrollBars.Minimum)
                txtScrollBars.ScrollBars = ScrollBars.None;

            else if (txtScrollBars.ScrollBars == ScrollBars.None)
                txtScrollBars.ScrollBars = ScrollBars.SmallChange;

            else if (txtScrollBars.ScrollBars == ScrollBars.SmallChange)
                txtScrollBars.ScrollBars = ScrollBars.Value;

            else if (txtScrollBars.ScrollBars == ScrollBars.Value)
                txtScrollBars.ScrollBars = ScrollBars.Vertical;

            else if (txtScrollBars.ScrollBars == ScrollBars.Vertical)
                txtScrollBars.ScrollBars = ScrollBars.Both;
            
        }

    }
}