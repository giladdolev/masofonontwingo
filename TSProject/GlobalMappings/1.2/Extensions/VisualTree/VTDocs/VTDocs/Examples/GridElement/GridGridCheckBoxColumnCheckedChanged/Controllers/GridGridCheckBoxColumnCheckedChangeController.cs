﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace MvcApplication9.Controllers
{
    public class GridGridCheckBoxColumnCheckedchangedController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add(false, false, false);

            string cellContentText;

            for(int i = 0; i < TestedGrid.Columns.Count; i++)
            {
                if (TestedGrid.Rows[0].Cells[i].Value.ToString() == "True")
                {
                    cellContentText = "Checked";
                }
                else
                {
                    cellContentText = "UnChecked";
                }

                lblLog.Text += "Column" + (i + 1).ToString() + " CheckBox is " + cellContentText + "\\r\\n";
            }
        }

        public void coulmns_CheckedChange(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            
            
            lblLog.Text = "";
            string cellContentText;

            txtEventLog.Text += "\\r\\nCheckedChange event is invoked";
            for(int i = 0; i < TestedGrid.Columns.Count; i++)
            {
                if (TestedGrid.Rows[0].Cells[i].Value.ToString() == "True")
                {
                    cellContentText = "Checked";
                }
                else
                {
                    cellContentText = "UnChecked";
                }

                lblLog.Text += "Column" + (i + 1).ToString() + " CheckBox is " + cellContentText + "\\r\\n";
            }
        }


        public void btnChangeCheckBoxValue_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            for (int i = 0; i < TestedGrid.Columns.Count; i++)
            {
                if (TestedGrid.Rows[0].Cells[i].Value.ToString() == "True")
                {
                    TestedGrid.Rows[0].Cells[i].SetValue(false);
                }
                else
                {
                    TestedGrid.Rows[0].Cells[i].SetValue(true);
                }

            }
            
        }
    }
}
