using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //SplitContainerAlignment
        public void btnChangeSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splSize = this.GetVisualElementById<SplitContainer>("splSize");

            if (splSize.Size == new Size(200,36)) //Get
            {
                splSize.Size = new Size(170, 100); //Set
                textBox1.Text = "The Size property value is: " + splSize.Size;
            }
            else  
            {
                splSize.Size = new Size(200, 36); 
                textBox1.Text = "The Size property value is: " + splSize.Size;
            }
             
        }
        
        public void splSize_SizeChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "SizeChanged Event is invoked\\r\\n";

        }
    }
}