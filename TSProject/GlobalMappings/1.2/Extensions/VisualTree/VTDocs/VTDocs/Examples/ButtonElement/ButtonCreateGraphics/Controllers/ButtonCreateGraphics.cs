using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonCreateGraphicsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnCreateGraphics_Click(object sender, EventArgs e)
        {
            
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ButtonElement btnTestedButton = this.GetVisualElementById<ButtonElement>("btnTestedButton");

         
            Graphics g = btnTestedButton.CreateGraphics();
            textBox1.Text = "CreateGraphics() is invoked";
            Size preferredSize = g.MeasureString(btnTestedButton.Text, btnTestedButton.Font).ToSize();
            btnTestedButton.Size = new Size(preferredSize.Width, preferredSize.Height);

            g.Dispose();




            
        }

    }
}