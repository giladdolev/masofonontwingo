﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class BasicToolBarMenuItem : WindowElement
    {

        public BasicToolBarMenuItem()
        {
            MenuItemsIndex = 1;
        }


        int _MenuItemsIndex;

        public int MenuItemsIndex
        {
            get { return this._MenuItemsIndex; }
            set { this._MenuItemsIndex = value; }
        }
       

    }
}