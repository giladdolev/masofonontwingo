using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxSelectAllController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void SelectAll_Click(object sender, EventArgs e)
        {
            TextBoxElement text = this.GetVisualElementById<TextBoxElement>("textBox1");
            text.SelectAll();
        }
      
    }
}