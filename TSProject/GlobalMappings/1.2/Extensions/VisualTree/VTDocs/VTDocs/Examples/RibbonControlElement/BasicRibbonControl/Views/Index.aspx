<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Ribbon Basic Panel" ID="windowView2" LoadAction="BasicRibbonControl\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Ribbon Basic" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Represents the root element of a ribbon user interface that hosts a Quick Access Toolbar, 
            Application Menu, and tabs." Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The following example is a 2 Tabs Ribbon panel hosting RibbonPageGroups with Button, 
            ToolBarDropDownButton and ToolBarButton." Top="230px" ID="lblExp2"></vt:Label>

        <vt:RibbonControl runat="server" ID="rbCtrl" Width="600" Height="200px" Left="50px" Top="290px" >
            
                   <vt:RibbonPage runat="server" ID="rbp1" Text="Page1" >                   
                        <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup1" Text="Group1" >
                            <vt:Button runat="server" SkinID="RibbonButton" ID="btn1" Text="btn1" ></vt:Button>
                            <vt:ToolBarDropDownButton runat="server" ID="btn2" Text="dropDown" DropDownArrows="true">
                                <vt:ToolBarButton  runat="server" ID="btn3" Text="dropDown1" ></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                        </vt:RibbonPageGroup>                      
                 </vt:RibbonPage>

                 <vt:RibbonPage runat="server" ID="rbp2" Text="Page2">
                         <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup3" Text="Group1" >
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button1" Text="btn1"></vt:Button>
                            <vt:ToolBarDropDownButton runat="server" ID="ToolBarDropDownButton1" Text="dropDown" DropDownArrows="true">
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton1" Text="dropDown1"></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                            <vt:ToolBarDropDownButton runat="server" ID="ToolBarDropDownButton2" Text="dropDown" DropDownArrows="true">
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton2" Text="dropDown1"></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                        </vt:RibbonPageGroup>         
                      
                        <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup2" Text="Group2" >
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button2" Image ="~/Content/Elements/Button.png" Text="btn1" TextImageRelation="ImageAboveText"></vt:Button>
                            <vt:ToolBarDropDownButton runat="server" ID="ToolBarDropDownButton3" Text="dropDown" DropDownArrows="true" Image ="~/Content/Elements/Button.png" TextImageRelation="ImageAboveText">
                                <vt:ToolBarButton  runat="server" ID="ToolBarButton3" Text="dropDown1"></vt:ToolBarButton>
                            </vt:ToolBarDropDownButton>
                        </vt:RibbonPageGroup>  
                     
                     <vt:RibbonPageGroup runat="server" ID="RibbonPageGroup4" Text="Group3" >
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button3" Image ="~/Content/Images/Cute.png" Text="btn1" TextImageRelation="ImageAboveText"></vt:Button>
                            <vt:Button runat="server" SkinID="RibbonButton" ID="Button4" Image ="~/Content/Images/icon.jpg" Text="btn2" TextImageRelation="ImageAboveText"></vt:Button>
                         <vt:Button runat="server" SkinID="RibbonButton" ID="Button5" Image ="~/Content/Images/Sleepy.jpg" Text="btn3" TextImageRelation="ImageAboveText"></vt:Button>
                        </vt:RibbonPageGroup>                                                                      
                 </vt:RibbonPage>
               
        </vt:RibbonControl>
    </vt:WindowView>
</asp:Content>
