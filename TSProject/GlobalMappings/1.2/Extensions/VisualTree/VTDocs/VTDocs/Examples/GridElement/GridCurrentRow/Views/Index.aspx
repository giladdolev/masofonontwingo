﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid CurrentRow property" LoadAction="GridCurrentRow\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CurrentRow" ID="lblTitle">
        </vt:Label>
        <vt:Label runat="server" Text="  Gets or sets the row containing the current cell. 
                      
            syntax : public GridRow CurrentRow { get; set; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExp2"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can select and edit cells with the mouse or by pressing the buttons." 
            Top="250px" ID="Label2">
        </vt:Label>
        <vt:Grid runat="server" Top="300px" ID="TestedGrid" SelectionChangedAction="GridCurrentRow\TestedGrid_RowSelectionChanged">
            <Columns>
                <vt:GridTextBoxColumn runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridTextBoxColumn>
                <vt:GridTextBoxColumn runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridTextBoxColumn>
            </Columns> 
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="430px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Edit Middle Cell >>" Top="495px" ID="btnEditMiddleCell" ClickAction="GridCurrentRow\btnEditMiddleCell_Click"></vt:Button>

        <vt:Button runat="server" Text="Select i >>" Top="495px" ID="btnSelecti" left="300px" ClickAction="GridCurrentRow\btnSelecti_Click"></vt:Button>

    </vt:WindowView>

</asp:Content>
