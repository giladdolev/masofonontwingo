<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton ToolTipText Property" Top="57px" Left="11px" ID="windowView1" Height="750px" Width="768px">
     

        <vt:Label runat="server" Text="RadioButton is set with ToolTipText" Top="30px" Left="50px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="200px"></vt:Label>

         <vt:RadioButton runat="server" UseVisualStyleBackColor="True" TextAlign="MiddleCenter" ToolTipText="MyToolTip" Text="TestedRadioButton" Top="98px" Left="56px" ID="RadioButton1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Blue" Height="23px" TabIndex="1" Width="75px">
        </vt:RadioButton>

    </vt:WindowView>
</asp:Content>
