<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar Enabled Property" ID="windowView1" LoadAction="ToolBarEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:ToolBar runat="server" Text="ToolBar" Top="185px" Dock="None" ID="TestedToolBar1" CssClass="vt-test-toolb-1">
            <vt:ToolBarButton runat="server" id="ToolBar1Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar1Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar1Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>           

        <vt:Label runat="server" SkinID="Log" Top="240px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedToolBar' is initially set to 'false'." Top="375px" ID="lblExp1"></vt:Label> 

        <vt:ToolBar runat="server" Text="TestedToolBar" Top="425px" Dock="None" Enabled ="false" ID="TestedToolBar2" CssClass="vt-test-toolb-2">
            <vt:ToolBarButton runat="server" id="ToolBar2Item1" Image="Content/Images/New.png" DisplayStyle="Image" Text="new"></vt:ToolBarButton>
            <vt:ToolBarSeparator runat="server" />
            <vt:ToolBarButton runat="server" id="ToolBar2Item2" Image="Content/Images/Open.png" Text="open"></vt:ToolBarButton>
            <vt:ToolBarLabel runat="server" ID="ToolBar2Label1" Text="ToolBarLabel1" ></vt:ToolBarLabel>
        </vt:ToolBar>

        <vt:Label runat="server" SkinID="Log" Top="480px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="545px" ID="btnChangeEnabled" Width="180px" ClickAction="ToolBarEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
