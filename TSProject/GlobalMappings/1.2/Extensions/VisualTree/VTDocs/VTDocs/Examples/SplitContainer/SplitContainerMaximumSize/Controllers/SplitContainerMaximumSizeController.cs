using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerMaximumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //SplitContainerAlignment
        public void btnChangeMaximumSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splRuntimeMaximumSize = this.GetVisualElementById<SplitContainer>("splRuntimeMaximumSize");

            if (splRuntimeMaximumSize.MaximumSize == new Size(0, 0)) //Get
            {
                splRuntimeMaximumSize.MaximumSize = new Size(180, 60); //Set
                textBox1.Text = "MaximumSize: " + splRuntimeMaximumSize.MaximumSize + " The size of the SplitContainer is: " + splRuntimeMaximumSize.Size;

            }
            else
            {
                splRuntimeMaximumSize.MaximumSize = new Size(0, 0); // return the  MaximumSize to its original value
                splRuntimeMaximumSize.Size = new Size(200, 70); // return the control tested to its original size (212, 97)
                textBox1.Text = "MaximumSize is: " + splRuntimeMaximumSize.MaximumSize + " The size of the SplitContainer is: " + splRuntimeMaximumSize.Size;
            }
        }
        //btnGetSize
        public void btnGetSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splRuntimeMaximumSize = this.GetVisualElementById<SplitContainer>("splRuntimeMaximumSize");


            textBox1.Text = "The size is: " + splRuntimeMaximumSize.Size ;
            
        }

    }
}