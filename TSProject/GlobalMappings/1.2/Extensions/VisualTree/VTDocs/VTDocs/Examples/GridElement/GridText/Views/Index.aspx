﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid Text Property" ID="windowView1" Height="800px" LoadAction="GridText\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with the control.
            
            Syntax : public virtual string Text { get; set; }
            If no Text is set the GridElement doesn't contain a title" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Grid runat="server"  Top="170px" ID="TestedGrid1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Text property of this grid  is initially set to 'TestedGrid'" Top="405px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" Height="155px" Width="305px" Top="455px" ID="TestedGrid2">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="625px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change Text value >>" Width="180px" Top="690px" ID="btnChangeText" ClickAction="GridText\btnChangeText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
