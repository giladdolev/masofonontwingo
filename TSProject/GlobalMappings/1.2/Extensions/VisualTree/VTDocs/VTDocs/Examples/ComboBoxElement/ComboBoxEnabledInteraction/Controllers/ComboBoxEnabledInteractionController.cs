using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxEnabledInteractionController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

           
            lblLog.Text = "Enabled Value: " + TestedComboBox.Enabled;
        }

        private void btnChangeEnabled_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedComboBox.Enabled == true)
            {
                TestedComboBox.Enabled = false;
                lblLog.Text = "Enabled Value: " + TestedComboBox.Enabled + "\\r\\n\\r\\nBackColor value: " + TestedComboBox.BackColor;
            }
            else
            {
                TestedComboBox.Enabled = true;
                lblLog.Text = "Enabled Value: " + TestedComboBox.Enabled + "\\r\\n\\r\\nBackColor value: " + TestedComboBox.BackColor;
            }
        }

        private void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox = this.GetVisualElementById<ComboBoxElement>("TestedComboBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedComboBox.BackColor != Color.Blue)
            {
                TestedComboBox.BackColor = Color.Blue;
                lblLog.Text = "Enabled Value: " + TestedComboBox.Enabled + "\\r\\n\\r\\nBackColor value: " + TestedComboBox.BackColor;
            }
            else
            {
                TestedComboBox.BackColor = Color.White;
                lblLog.Text = "Enabled Value: " + TestedComboBox.Enabled + "\\r\\n\\r\\nBackColor value: " + TestedComboBox.BackColor;
            }
        }

   

    }
}