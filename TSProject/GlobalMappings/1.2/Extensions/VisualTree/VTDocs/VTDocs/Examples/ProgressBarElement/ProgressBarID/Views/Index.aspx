<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar ID Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:ProgressBar runat="server" Text="Tested ProgressBar" Top="45px" Left="140px" ID="prgID" Height="36px" Width="220px"></vt:ProgressBar> 

        <vt:Button runat="server" Text="Get ProgressBar's ID value" Top="45px" Left="400px" ID="btnID" Height="36px" Width="220px" ClickAction ="ProgressBarID\btnID_Click"></vt:Button>           

        <vt:TextBox runat="server" Text="" Top="130px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="220px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
