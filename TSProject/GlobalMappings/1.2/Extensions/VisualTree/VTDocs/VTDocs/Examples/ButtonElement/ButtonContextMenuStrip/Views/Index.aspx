<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button ContextMenuStrip Property" ID="windowView"  LoadAction="ButtonContextMenuStrip\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ContextMenuStrip" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the ContextMenuStrip associated with this control.
            
            Syntax:  public ContextMenuStripElement ContextMenuStrip { get; set; }
            Property Value: The ContextMenuStrip for this control, or null if there is no ContextMenuStrip.
            The default is null." Top="75px"  ID="lblDefinition"></vt:Label>
      
         <!--TestedButton1 -->
        <vt:Button runat="server" SkinID="Wide" Text="Button" Top="190px" ID="TestedButton1"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Width="400px" Top="240px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="305px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ContextMenuStrip property of this 'TestedButton' is initially set to 
           'contextMenuStrip1' with menu item 'Hello'. Use the buttons below to set TestesButton 
            ContextMenuStrip property to a different value" Top="355px"  ID="lblExp1"></vt:Label>
             
        <!--TestedButton2 -->
        <vt:Button runat="server" SkinID="Wide" Text="TestedButton" ContextMenuStripID ="contextMenuStrip1" Top="440px" ID="TestedButton2"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="490px" Width="400px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="contextMenuStrip1" Top="555px"  ID="btnChangToeContextMenuStrip1" ClickAction="ButtonContextMenuStrip\btnChangToeContextMenuStrip1_Click"></vt:Button>

        <vt:Button runat="server" Text="contextMenuStrip2" Top="555px" Left="270px" ID="btnChangToeContextMenuStrip2" ClickAction="ButtonContextMenuStrip\btnChangToeContextMenuStrip2_Click"></vt:Button>

        <vt:Button runat="server" Text="Reset" Top="555px" Left="460px" ID="btnReset" ClickAction="ButtonContextMenuStrip\btnReset_Click"></vt:Button>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip1" >
            <vt:ToolBarMenuItem runat ="server" ID="exitToolStripMenuItem" Text="Hello"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

        <vt:ContextMenuStrip runat="server" ID="contextMenuStrip2" >
            <vt:ToolBarMenuItem runat ="server" ID="toolBarMenuItem1" Text="Hello World"></vt:ToolBarMenuItem>
		</vt:ContextMenuStrip>

    </vt:WindowView>
</asp:Content>
