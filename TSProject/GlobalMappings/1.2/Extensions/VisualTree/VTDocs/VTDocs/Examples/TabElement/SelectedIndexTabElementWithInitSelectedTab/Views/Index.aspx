<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="SelectedTab add remove tab items  Example" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="SelectedIndexTabElementWithInitSelectedTab\loadAction">
        
        <vt:Tab runat="server" Top="124px" Left="264px" ID="tabControl1" Height="100px" Width="500">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="lbl1" Text="Tab Item 1"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label2" Text="Tab Item 2"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="tabPage3" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label3" Text="Tab Item 3"></vt:Label>
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage4" Top="22px" Left="4px" ID="tabPage4" Height="74px" Width="192px">
                    <vt:Label runat="server" ID="Label4" Text="Tab Item 4"></vt:Label>
                </vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Button runat="server" Text="add tab" Top="165px" Left="109px" ClickAction="SelectedIndexTabElementWithInitSelectedTab\AddTab_Click" ID="ChangeTabsText" Height="23px" Width="117px"></vt:Button>
        <vt:Button runat="server" Text="remove all tabs" Top="200px" Left="109px" ClickAction="SelectedIndexTabElementWithInitSelectedTab\Removetab_Click" ID="Button1" Height="23px" Width="117px"></vt:Button>

          <vt:Button runat="server" Text="show selected tab" Top="240px" Left="109px" ClickAction="SelectedIndexTabElementWithInitSelectedTab\selectedtab_Click" ID="Button2" Height="23px" Width="117px"></vt:Button>

    </vt:WindowView>
</asp:Content>
