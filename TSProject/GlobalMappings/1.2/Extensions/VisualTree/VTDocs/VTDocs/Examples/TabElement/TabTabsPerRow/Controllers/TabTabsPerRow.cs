using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TabTabsPerRowController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TabAlignment
        public void btnChangeTabsPerRow_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            TabElement tabRuntimeTabsPerRow = this.GetVisualElementById<TabElement>("tabRuntimeTabsPerRow");

            if (tabRuntimeTabsPerRow.TabsPerRow == 2)
            {
                tabRuntimeTabsPerRow.TabsPerRow = 1;
                textBox1.Text = tabRuntimeTabsPerRow.TabsPerRow.ToString();
            }
            else
            {
                tabRuntimeTabsPerRow.TabsPerRow = 2;
                textBox1.Text = tabRuntimeTabsPerRow.TabsPerRow.ToString();
                
            }
            
        }

    }
}