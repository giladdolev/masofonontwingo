using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowAcceptButtonController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowAcceptButton());
        }
        private WindowAcceptButton ViewModel
        {
            get { return this.GetRootVisualElement() as WindowAcceptButton; }
        }

        public void btnChangeAcceptButton_Click(object sender, EventArgs e)
		{
            ButtonElement btnAcceptButton = this.GetVisualElementById<ButtonElement>("btnAcceptButton");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");

            if (this.ViewModel.AcceptButton == null)
			{
                this.ViewModel.AcceptButton = btnAcceptButton;
                textBox1.Text = this.ViewModel.AcceptButton.ToString();
			}
			else
            {
                this.ViewModel.AcceptButton = null;
                textBox1.Text = "AcceptButton Property is not set";
            }
		}
        public void btnAcceptButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("AcceptButton Click method is invoked");
        }

    }
    
}