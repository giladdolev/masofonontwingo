using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxLocationController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RichTextBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Location value: " + TestedRichTextBox.Location;
        }

        public void btnChangeLocation_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedRichTextBox.Location == new Point(80, 310))
            {
                TestedRichTextBox.Location = new Point(200, 290);
                lblLog.Text = "Location value: " + TestedRichTextBox.Location;
            }
            else
            {
                TestedRichTextBox.Location = new Point(80, 310);
                lblLog.Text = "Location value: " + TestedRichTextBox.Location;
            }
        }

    }
}