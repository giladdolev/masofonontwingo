<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
     <vt:WindowView runat="server" Text="Grid Is Grouping Grid" ID="windowView1" LoadAction="GridIsGroupingGrid\OnLoad">


        <vt:Label runat="server" SkinID="Title" Text="Is Grouping Grid" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control IsGrouping

            Syntax: public bool GridIsGroupingGrid { get; set; }
            'true' if the control and all its child controls are Grouping Grid; otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <!-- Tested Grid1 -->
        <vt:Grid runat="server" Top="365" ID="TestedGrid1" >
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>


        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="170px" ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="IsGroping property of this Grid is initially set to 'false'
           " Top="210px" ID="lblExp1"></vt:Label>
         <vt:Button runat="server" Text="Change IsGroupingGrid" Top ="500" ID="BT" ClickAction="GridIsGroupingGrid\Button"></vt:Button>

    </vt:WindowView>

</asp:Content>
        