<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic ProgressBar" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
		<vt:ProgressBar runat="server" Text="" Appearance="ccFlat" Top="144px" Left="8px" ID="ProgressBar1" BorderStyle="None" ForeColor="Black" Height="57px" Width="752px">
		</vt:ProgressBar>
		<vt:Button runat="server" Text="Calculate" Top="64px" Left="360px" ClickAction="BasicProgressBar\Command1_Click" ID="Command1" Height="25px" Width="97px">
		</vt:Button>
    </vt:WindowView>
</asp:Content>
