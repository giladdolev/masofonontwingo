<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel PerformLocationChanged() Method" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      

        <vt:Button runat="server" Text="PerformLocationChanged of 'Tested Panel'" Top="45px" Left="140px" ID="btnPerformLocationChanged" Height="36px" Width="300px" ClickAction="PanelPerformLocationChanged\btnPerformLocationChanged_Click"></vt:Button>


        <vt:Panel runat="server" Text="Tested Panel" Top="150px" Left="200px" ID="testedPanel" Height="70px"  Width="200px" LocationChangedAction="PanelPerformLocationChanged\testedPanel_LocationChanged"></vt:Panel>           

        

    </vt:WindowView>
</asp:Content>
