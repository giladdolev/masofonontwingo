<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Afterrender event" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
      
        <vt:Button runat="server" Text="TestedButton" Top="50px" Left="200px" ID="btnAfterrender" Height="36px"  Width="100px" AfterrenderAction ="ButtonAfterrender\btnAfterrender_Afterrender"></vt:Button>          

        <vt:TextBox runat="server" Text="" Top="150px" Left="140px" ID="txtEventTrack" Multiline="true" Height="50px" Width="250px"></vt:TextBox>

    </vt:WindowView>
</asp:Content>
