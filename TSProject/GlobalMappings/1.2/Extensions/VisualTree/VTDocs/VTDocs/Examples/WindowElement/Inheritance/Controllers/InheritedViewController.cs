using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class InheritedViewController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult InheritedView()
        {
            return View();
        }

        public void buttonOK_Click(object sender, EventArgs e)
        {
            LabelElement label = this.GetVisualElementById<LabelElement>("description");
            label.Text = "Changed by inherited view";
        }

        public void ShowBase_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "WindowElement");
            argsDictionary.Add("ExampleName", "Inheritance");
            WindowElement baseView = VisualElementHelper.CreateFromView<WindowElement>("BaseView", "BaseView", null, argsDictionary, null);
            baseView.ShowDialog();
          
        }     

    }
}