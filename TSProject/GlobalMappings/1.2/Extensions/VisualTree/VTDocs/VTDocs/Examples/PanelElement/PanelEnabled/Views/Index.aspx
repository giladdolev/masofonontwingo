<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Enabled Property" ID="windowView1" LoadAction="PanelEnabled\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Enabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control can respond to user interaction.           

            Syntax: public bool Enabled { get; set; }
            Property Values: 'true' if the control can respond to user interaction; otherwise, 'false'. 
            The default is 'true'." Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Panel runat="server" Text="Panel" Top="195px" Width="300px" ID="TestedPanel1" CssClass="vt-test-pnl-1">
            <vt:Button runat="server" Text="Click Me" Top="28px" Left="15px" ID="button1" Width="80px"  ClickAction="PanelEnabled\button1_Click"></vt:Button>
            <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label1" Width="80px" Left="110px"></vt:Label>
            <vt:TextBox runat="server" Text="Add Text " Top="28px" Height="23px" Left="205px" ID="textBox1" Width="80px"></vt:TextBox>
        </vt:Panel>           

        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Enabled property of this 'TestedPanel' is initially set to 'false'." Top="405px" ID="lblExp1"></vt:Label> 

        <vt:Panel runat="server" Text="TestedPanel" Width="300px" Top="465px" Enabled ="false" ID="TestedPanel2" CssClass="vt-test-pnl-2">
            <vt:Button runat="server" Text="Click Me" Top="28px" Left="15px" ID="button2" Width="80px"  ClickAction="PanelEnabled\button2_Click"></vt:Button>
            <vt:Label runat="server" Text="Label" Top="28px" Height="23px" BorderStyle="Outset" ID="label2" Width="80px" Left="110px"></vt:Label>
            <vt:TextBox runat="server" Text="Add Text " Top="28px" Height="23px" Left="205px" ID="textBox2" Width="80px"></vt:TextBox>
        </vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="560px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change Enabled Value >>" Top="625px" ID="btnChangeEnabled" Width="180px" ClickAction="PanelEnabled\btnChangeEnabled_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
