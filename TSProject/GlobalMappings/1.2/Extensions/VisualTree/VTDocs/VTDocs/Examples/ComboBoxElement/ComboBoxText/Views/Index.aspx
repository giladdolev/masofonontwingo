
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox Text Property" ID="windowView2" LoadAction="ComboBoxText/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Text" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the text associated with this control.
            
            Syntax: public override string Text { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="Text property of the ComboBox is initially set to 'TestedComboBox'.
          You can change the Text property by clicking the 'Change Text' button or by typing a text
           manually.  " Top="235px"  ID="lblExp1"></vt:Label>     
      
        <vt:ComboBox runat="server" Text="TestedComboBox" CssClass="vt-TestedComboBox" Top="325px" ID="btnTestedComboBox"></vt:ComboBox>
        <vt:Label runat="server" SkinID="Log" Top="365px" Width="350" ID="lblLog"></vt:Label>
        <vt:Button runat="server" Text="Change Text Value >>"  Top="430px" Width="180px" ID="btnChangeComboBoxSize" ClickAction="ComboBoxText\btnChangeComboBoxText_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>