using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedLabel.ClientRectangle;

        }
        public void btnChangeLabelClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            if (TestedLabel.Left == 100)
            {
                TestedLabel.SetBounds(80, 350, 150, 36);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedLabel.ClientRectangle;

            }
            else
            {
                TestedLabel.SetBounds(100, 310, 200, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedLabel.ClientRectangle;
            }

        }

      
    }
}