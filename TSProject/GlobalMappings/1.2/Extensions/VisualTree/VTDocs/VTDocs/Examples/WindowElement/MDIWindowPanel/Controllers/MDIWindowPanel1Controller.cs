using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class MDIWindowPanel1Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult MDIWindowPanel1()
        {
            return View(new MDIWindowPanel1());
        }

        public void ClickAction(object sender, EventArgs e)
        {
            PanelElement panelTop = this.GetVisualElementById<PanelElement>("panelRight");
            if (panelTop != null)
            {
                panelTop.Controls.Clear();
                ControlElement elementView = VisualElementHelper.CreateFromView<ControlElement>("MDIWindowPanel2", "MDIWindowPanel2");
                if (elementView != null)
                {
                    panelTop.Controls.Add(elementView);
                }
            }
        }
       
    }
}