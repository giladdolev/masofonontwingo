﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox LostFocus event" ID="windowView2" LoadAction="TextBoxLostFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="LostFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control loses focus.

            Syntax: public event EventHandler LostFocus
            
            Change the focus by using the keyboard (TAB, SHIFT+TAB, and so on) or by using the mouse."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="205px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time the focus is shifted out of one of the text boxes,
             a 'LostFocus' event will be invoked.
             Each invocation of the 'LostFocus' event should add a line in the 'Event Log'."
            Top="255px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="335px" Height="120px" Width="340px">
            <vt:Label runat="server" Text="First Name :" Left="15px" Top="25px" ID="Label1"></vt:Label>

            <vt:TextBox runat="server" Top="25px" Left="160px" Text="Enter First Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtFirstName" TabIndex="1" LostFocusAction="TextBoxLostFocus\txtFirstName_LostFocus" TextChangedAction="TextBoxLostFocus\txtFirstName_TextChanged"></vt:TextBox>

            <vt:Label runat="server" Text="Last Name :" Left="15px" Top="70px" ID="Label2"></vt:Label>

            <vt:TextBox runat="server" Top="70px" Left="160px" Text="Enter Last Name " Font-Names="Calibri Light" Font-Size="10pt" ForeColor="DimGray" ID="txtLastName" TabIndex="2" LostFocusAction="TextBoxLostFocus\txtLastName_LostFocus" TextChangedAction="TextBoxLostFocus\txtLastName_TextChanged"></vt:TextBox>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Top="470px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="510px" Width="360px" ID="txtEventLog"></vt:TextBox>

    </vt:WindowView>
</asp:Content>

