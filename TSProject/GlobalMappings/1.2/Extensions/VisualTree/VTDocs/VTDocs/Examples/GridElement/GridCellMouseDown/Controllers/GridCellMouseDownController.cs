using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;

namespace MvcApplication9.Controllers
{
    public class GridCellMouseDownController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            testedGrid.Rows.Add("a", "b", "c");
            testedGrid.Rows.Add("d", "e", "f");
            testedGrid.Rows.Add("g", "h", "i");

            lblLog.Text = "Grid Selected values: Empty";
        }

        public void TestedGrid_CellMouseDown(object sender, GridCellMouseEventArgs e)
        {
            GridElement testedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nGrid CellMouseDown event is invoked";

            lblLog.Text = "Grid Selected values: ";

            if ((e.RowIndex >= 0) && (e.ColumnIndex >= 0))
            {
                lblLog.Text += " (" + testedGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value + ") ";
            }
        }

    }
}
