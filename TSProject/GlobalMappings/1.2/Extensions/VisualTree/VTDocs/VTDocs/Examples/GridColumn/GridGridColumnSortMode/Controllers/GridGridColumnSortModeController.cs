using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;
using MvcApplication9.Models;
using System.ComponentModel;
using System.Web.VisualTree;

namespace MvcApplication9.Controllers
{
    public class GridGridColumnSortModeController : Controller
    {
        //Returns the view for Index
        public ActionResult Index()
        {
            return View(new GridGridColumnSortMode());
        }

        private GridGridColumnSortMode ViewModel
        {
            get { return this.GetRootVisualElement() as GridGridColumnSortMode; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid1 = this.GetVisualElementById<GridElement>("TestedGrid1");
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            //Returns the default SortMode of the first column
            lblLog1.Text = "The SortMode value of Column1 is: " + TestedGrid1.Columns[0].SortMode + ".";
            lblLog2.Text = "Click the buttons...";

            TestedGrid1.Rows.Add("1", "AA", "+", "�", "2c$��");
            TestedGrid1.Rows.Add("11", "A", "&", "���", "1aa+�");
            TestedGrid1.Rows.Add("10", "a", "&", "��", "11a&�");
            TestedGrid1.Rows.Add("3", "B", "$", "�", "3Bb!�");


            TestedGrid2.Rows.Add("1", "AA", "+", "�", "2c$��");
            TestedGrid2.Rows.Add("11", "A", "&", "���", "1aa+�");
            TestedGrid2.Rows.Add("10", "a", "&", "��", "11a&�");
            TestedGrid2.Rows.Add("3", "B", "$", "�", "3Bb!�");
        }

        public void btnSortModesInformation_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "GridColumn");
            argsDictionary.Add("ExampleName", "GridGridColumnSortMode");

            ViewModel.frmText = VisualElementHelper.CreateFromView<GridColumnSortModeRemarks>("GridColumnSortModeRemarks", "Index2", null, argsDictionary, null);
            ViewModel.frmText.Show();
        }

        //Sort Column10 that set to Programmatic SortMode
        private void btnSortColumn10_click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.Columns[4].SortOrder == SortOrder.Ascending)
            {
                TestedGrid2.Sort(TestedGrid2.Columns[4], SortOrder.Descending);
                lblLog2.Text = "Sort(Column10, SortOrder.Descending) method was invoked.\\r\\nColumn10 SortOrder value: " + TestedGrid2.Columns[4].SortOrder + ".\\r\\nColumn10 SortMode value: " + TestedGrid2.Columns[4].SortMode + '.';
            }
            else
            {
                TestedGrid2.Sort(TestedGrid2.Columns[4], SortOrder.Ascending);
                lblLog2.Text = "Sort(Column10, SortOrder.Ascending) method was invoked.\\r\\nColumn10 SortOrder value: " + TestedGrid2.Columns[4].SortOrder + ".\\r\\nColumn10 SortMode value: " + TestedGrid2.Columns[4].SortMode + '.';
            }
        }


        //Sort Column6 that set to NotSortable SortMode
        private void btnSortColumn6_click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.Columns[0].SortOrder == SortOrder.Ascending)
            {
                TestedGrid2.Sort(TestedGrid2.Columns[0], SortOrder.Descending);
                lblLog2.Text = "Sort(Column6, SortOrder.Descending) method was invoked.\\r\\nColumn6 SortOrder value: " + TestedGrid2.Columns[0].SortOrder + ".\\r\\nColumn6 SortMode value: " + TestedGrid2.Columns[0].SortMode + '.';  
            }
            else
            {
                TestedGrid2.Sort(TestedGrid2.Columns[0], SortOrder.Ascending);
                lblLog2.Text = "Sort(Column6, SortOrder.Ascending) method was invoked.\\r\\nColumn6 SortOrder value: " + TestedGrid2.Columns[0].SortOrder + ".\\r\\nColumn6 SortMode value: " + TestedGrid2.Columns[0].SortMode + '.';  
            }
        }

        private void btnChangeColumn6SortMode_click(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedGrid2.Columns[0].SortMode == GridColumnSortMode.Automatic)
            {
                TestedGrid2.Columns[0].SortMode = GridColumnSortMode.NotSortable;
                lblLog2.Text = "Column6 SortMode value is: " + TestedGrid2.Columns[0].SortMode + '.';
            }
            else if (TestedGrid2.Columns[0].SortMode == GridColumnSortMode.NotSortable)
            {
                TestedGrid2.Columns[0].SortMode = GridColumnSortMode.Programmatic;
                lblLog2.Text = "Column6 SortMode value is: " + TestedGrid2.Columns[0].SortMode + '.';
            }
            else
            {
                TestedGrid2.Columns[0].SortMode = GridColumnSortMode.Automatic;
                lblLog2.Text = "Column6 SortMode value is: " + TestedGrid2.Columns[0].SortMode + '.';
            }
        }

        //Complete when we will have access to HeaderCell like in WinForms:
        //For Example - Column10.HeaderCell.SortGlyphDirection = SortOrder.None;
        //Add a button to change Column10 SortMode at runtime to test the SortGlyphDirection when 
        //SortMode is changed 
        private void rdoAscending_CheckedChanged(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
        }
        private void rdoDescending_CheckedChanged(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
        }
        private void rdoNone_CheckedChanged(object sender, EventArgs e)
        {
            GridElement TestedGrid2 = this.GetVisualElementById<GridElement>("TestedGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
        }
    }
}

//}
