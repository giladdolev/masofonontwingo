using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPerformMouseUpController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformMouseUp_Click(object sender, EventArgs e)
        {
            
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");
            
            MouseEventArgs args = new MouseEventArgs();

            testedSplitContainer.PerformMouseUp(args);
        }

        public void testedSplitContainer_MouseUp(object sender, EventArgs e)
        {
            MessageBox.Show("TestedSplitContainer MouseUp event method is invoked");
        }

    }
}