﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowStartPosition : WindowElement
    {

        Win1 _TestedWin1;
        Win2 _TestedWin2;
        Win3 _TestedWin3;
        Win4 _TestedWin4;
        Win5 _TestedWin5;


        int _i;

        public WindowStartPosition()
        {

        }

        public Win1 TestedWin1
        {
            get { return this._TestedWin1; }
            set { this._TestedWin1 = value; }
        }
        public Win2 TestedWin2
        {
            get { return this._TestedWin2; }
            set { this._TestedWin2 = value; }
        }
        public Win3 TestedWin3
        {
            get { return this._TestedWin3; }
            set { this._TestedWin3 = value; }
        }
        public Win4 TestedWin4
        {
            get { return this._TestedWin4; }
            set { this._TestedWin4 = value; }
        }
        public Win5 TestedWin5
        {
            get { return this._TestedWin5; }
            set { this._TestedWin5 = value; }
        }

        public int i
        {
            get { return this._i; }
            set { this._i = value; }
        }
       

    }
}