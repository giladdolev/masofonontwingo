using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxClientRectangleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientRectangle value: \\r\\n" + TestedRichTextBox.ClientRectangle;

        }
        public void btnChangeRichTextBoxClientRectangle_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.Left == 100)
            {
                TestedRichTextBox.SetBounds(80, 320, 200, 80);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedRichTextBox.ClientRectangle;

            }
            else
            {
                TestedRichTextBox.SetBounds(100, 310, 300, 50);
                lblLog.Text = "ClientRectangle value: \\r\\n" + TestedRichTextBox.ClientRectangle;
            }

        }

    }
}