using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            RadioButtonElement rdoRunTimeAnchor = this.GetVisualElementById<RadioButtonElement>("rdoRunTimeAnchor");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");


            if (rdoRunTimeAnchor.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                rdoRunTimeAnchor.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                panel2.Size = new Size(400, 171);
                textBox1.Text = "AnchorStyles.Top, ";
                textBox1.Text += "Panel Size: " + panel2.Size;

            }

            else if (rdoRunTimeAnchor.Anchor == AnchorStyles.Top)
            {
                rdoRunTimeAnchor.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                panel2.Size = new Size(315, 190);
                textBox1.Text = "AnchorStyles.Left, ";
                textBox1.Text += "Panel Size: " + panel2.Size;
            }
            else if (rdoRunTimeAnchor.Anchor == AnchorStyles.Left)
            {
                rdoRunTimeAnchor.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                panel2.Size = new Size(200, 190);
                textBox1.Text = "AnchorStyles.Right, ";
                textBox1.Text += "Panel Size: " + panel2.Size;
            }
            else if (rdoRunTimeAnchor.Anchor == AnchorStyles.Right)
            {
                rdoRunTimeAnchor.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                panel2.Size = new Size(200, 160);
                textBox1.Text = "AnchorStyles.Bottom, ";
                textBox1.Text += "Panel Size: " + panel2.Size;
            }
            else if (rdoRunTimeAnchor.Anchor == AnchorStyles.Bottom)
            {
                rdoRunTimeAnchor.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the panel size
                textBox1.Text = "AnchorStyles.None, ";
                textBox1.Text += "Panel Size: " + panel2.Size;
            }
            else if (rdoRunTimeAnchor.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and btnChangeAnchor settings
                rdoRunTimeAnchor.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                panel2.Size = new Size(300, 200);//Set  back the panel size
                rdoRunTimeAnchor.Location = new Point(75, 18);
                textBox1.Text = "AnchorStyles.Left | AnchorStyles.Top, ";
                textBox1.Text += "Panel Size is back to : " + panel2.Size;
            }       
        }

    }
}