using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeFontController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //TreeAlignment
        private void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree1 = this.GetVisualElementById<TreeElement>("TestedTree1");
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "Font value: " + TestedTree1.Font + "\\r\\nFontStyle: " + getFontstyle(TestedTree1);
            lblLog2.Text = "Font value: " + TestedTree2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedTree2);
        }

        private void btnChangeTreeFont_Click(object sender, EventArgs e)
        {
            //lblLog2
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            TreeElement TestedTree2 = this.GetVisualElementById<TreeElement>("TestedTree2");
            if (TestedTree2.Font.Underline == true)
            {
                TestedTree2.Font = new Font("Niagara Engraved", 30, FontStyle.Italic);
                lblLog2.Text = "Font value: " + TestedTree2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedTree2);
            }
            else if (TestedTree2.Font.Bold == true)
            {
                TestedTree2.Font = new Font("Miriam Fixed", 12, FontStyle.Underline);
                lblLog2.Text = "Font value: " + TestedTree2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedTree2);
            }
            else if (TestedTree2.Font.Italic == true)
            {
                TestedTree2.Font = new Font("SketchFlow Print", 15, FontStyle.Strikeout);
                lblLog2.Text = "Font value: " + TestedTree2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedTree2);
            }
            else
            {
                TestedTree2.Font = new Font("Calibri", 13, FontStyle.Bold);
                lblLog2.Text = "Font value: " + TestedTree2.Font + "\\r\\nFontStyle: " + getFontstyle(TestedTree2);
            }
        }

        private string getFontstyle(ControlElement TheControlTested)
        {
            if (TheControlTested.Font.Underline)
                return "Underline";
            else if (TheControlTested.Font.Bold)
                return "Bold";
            else if (TheControlTested.Font.Italic)
                return "Italic";
            else if (TheControlTested.Font.Strikeout)
                return "Strikeout";
            else
                return "None";
        }
    }
}