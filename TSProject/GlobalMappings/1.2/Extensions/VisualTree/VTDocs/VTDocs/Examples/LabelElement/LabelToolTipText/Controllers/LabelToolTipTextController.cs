﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelToolTipTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Change BackColor
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "ToolTipText value: " + TestedLabel.ToolTipText + '.';
        }

        public void btnChangeToolTipText_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedLabel.ToolTipText == "MyToolTip")
            {
                TestedLabel.ToolTipText = "NewToolTip";
                lblLog.Text = "ToolTipText value: " + TestedLabel.ToolTipText + '.';
            }
            else
            {
                TestedLabel.ToolTipText = "MyToolTip";
                lblLog.Text = "ToolTipText value: " + TestedLabel.ToolTipText + '.';
            }
        }
    }
}