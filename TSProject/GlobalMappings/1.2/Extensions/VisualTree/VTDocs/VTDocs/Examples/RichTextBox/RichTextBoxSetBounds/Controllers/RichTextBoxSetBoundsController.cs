using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //RichRichTextBoxAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "The Tested RichTextBox bounds are set to: \\r\\nLeft  " + TestedRichTextBox.Left + ", Top  " + TestedRichTextBox.Top + ", Width  " + TestedRichTextBox.Width + ", Height  " + TestedRichTextBox.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.Left == 100)
            {
                TestedRichTextBox.SetBounds(80, 300, 200, 80);
                lblLog.Text = "The Tested RichTextBox bounds are set to: \\r\\nLeft  " + TestedRichTextBox.Left + ", Top  " + TestedRichTextBox.Top + ", Width  " + TestedRichTextBox.Width + ", Height  " + TestedRichTextBox.Height;

            }
            else
            {
                TestedRichTextBox.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested RichTextBox bounds are set to: \\r\\nLeft  " + TestedRichTextBox.Left + ", Top  " + TestedRichTextBox.Top + ", Width  " + TestedRichTextBox.Width + ", Height  " + TestedRichTextBox.Height;
            }
        }
    }
}