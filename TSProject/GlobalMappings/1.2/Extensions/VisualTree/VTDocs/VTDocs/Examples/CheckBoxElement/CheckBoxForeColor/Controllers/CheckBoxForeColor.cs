using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedCheckBox1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedCheckBox2.ForeColor + '.';
        }

        public void btnChangeForeColor_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedCheckBox2.ForeColor == Color.Green)
            {
                TestedCheckBox2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedCheckBox2.ForeColor + '.';
            }
            else
            {
                TestedCheckBox2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedCheckBox2.ForeColor + '.';

            }
        }

    }
}