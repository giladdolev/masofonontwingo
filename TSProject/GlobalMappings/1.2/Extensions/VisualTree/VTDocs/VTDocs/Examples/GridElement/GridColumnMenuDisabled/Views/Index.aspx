﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="Grid MenuDisabled Property" LoadAction="GridColumnMenuDisabled\Page_load">


        <vt:Label runat="server" SkinID="Title" Text="MenuDisabled" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the menuDisabled for the column.

           Syntax: public bool MenuDisabled { get; set; }
            'true' to disable the menu of the column; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Grid runat="server" Top="175px" Left="80px" ID="TestedGrid1" >
             <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px" MenuDisabled="true"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>      
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="MenuDisabled property of this Grid is initialy set to 'false'
            You should be able to sort the column" Top="390px" ID="lblExp1"></vt:Label>

        <vt:Grid runat="server" Top="450px" ReadOnly="true" Left="80px" ID="TestedGrid2">
            <Columns>
                
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px" MenuDisabled="false"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns> 
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="565px" ID="lblLog2"></vt:Label>

    </vt:WindowView>


</asp:Content>
