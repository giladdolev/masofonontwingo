﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace MvcApplication9.Models
{
    public class WindowVisible : WindowElement
    {

        VisibleTestedWindow _TestedWin;

        public WindowVisible()
        {

        }

        public VisibleTestedWindow TestedWin
        {
            get { return this._TestedWin; }
            set { this._TestedWin = value; }
        }
       

    }
}