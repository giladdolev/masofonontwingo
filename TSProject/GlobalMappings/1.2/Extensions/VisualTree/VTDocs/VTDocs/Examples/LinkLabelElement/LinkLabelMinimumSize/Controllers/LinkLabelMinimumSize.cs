using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LinkLabelMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            LinkLabelElement TestedLinkLabel = this.GetVisualElementById<LinkLabelElement>("TestedLinkLabel");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "LinkLabel MinimumSize value: " + TestedLinkLabel.MinimumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
        }

        public void btnChangeMinimumSize_Click(object sender, EventArgs e)
        {
            LinkLabelElement TestedLinkLabel = this.GetVisualElementById<LinkLabelElement>("TestedLinkLabel");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedLinkLabel.MinimumSize == new Size(400, 15))
            {
                TestedLinkLabel.MinimumSize = new Size(300, 30);
                lblLog1.Text = "LinkLabel MinimumSize value: " + TestedLinkLabel.MinimumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
            }
            else if (TestedLinkLabel.MinimumSize == new Size(300, 30))
            {
                TestedLinkLabel.MinimumSize = new Size(250, 10);
                lblLog1.Text = "LinkLabel MinimumSize value: " + TestedLinkLabel.MinimumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
            }
            else
            {
                TestedLinkLabel.MinimumSize = new Size(400, 15);
                lblLog1.Text = "LinkLabel MinimumSize value: " + TestedLinkLabel.MinimumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;
            }

        }

        public void btnSetToZeroMinimumSize_Click(object sender, EventArgs e)
        {
            LinkLabelElement TestedLinkLabel = this.GetVisualElementById<LinkLabelElement>("TestedLinkLabel");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedLinkLabel.MinimumSize = new Size(0, 0);
            lblLog1.Text = "LinkLabel MinimumSize value: " + TestedLinkLabel.MinimumSize + "\\r\\nLinkLabel Size value: " + TestedLinkLabel.Size;

        }


    }
}