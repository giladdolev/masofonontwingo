using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GridSetBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //GridAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Rows.Add("a", "b", "c");
            TestedGrid.Rows.Add("d", "e", "f");
            TestedGrid.Rows.Add("g", "h", "i");
            lblLog.Text = "The Tested Grid bounds are set to: \\r\\nLeft  " + TestedGrid.Left + ", Top  " + TestedGrid.Top + ", Width  " + TestedGrid.Width + ", Height  " + TestedGrid.Height;

        }

        public void btnSetBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            if (TestedGrid.Left == 100)
            {
                TestedGrid.SetBounds(80, 300, 285, 115);
                lblLog.Text = "The Tested Grid bounds are set to: \\r\\nLeft  " + TestedGrid.Left + ", Top  " + TestedGrid.Top + ", Width  " + TestedGrid.Width + ", Height  " + TestedGrid.Height;

            }
            else
            {
                TestedGrid.SetBounds(100, 280, 250, 50);
                lblLog.Text = "The Tested Grid bounds are set to: \\r\\nLeft  " + TestedGrid.Left + ", Top  " + TestedGrid.Top + ", Width  " + TestedGrid.Width + ", Height  " + TestedGrid.Height;
            }
        }

    }
}