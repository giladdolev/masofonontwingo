﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="windowView" Text="Grid SortableColumns Property" LoadAction="GridSortableColumns\Page_load">


        <vt:Label runat="server" SkinID="Title" Text="SortableColumns" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the SortableColumns for the grid.

           Syntax: public bool SortableColumns { get; set; }
            'true' to enable column sort; otherwise, 'false'. The default is 'true'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Grid runat="server" Top="175px" Left="80px" ID="TestedGrid1" SortableColumns="true">
             <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>      
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="290px" ID="lblLog1"></vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="350px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="SortableColumns property of this Grid is initialy set to 'false'
            You should be unable to sort the column" Top="390px" ID="lblExp1"></vt:Label>

        <vt:Grid runat="server" Top="450px" ReadOnly="true" Left="80px" ID="TestedGrid2" SortableColumns="false">
            <Columns>
                
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px" SortMode="Automatic"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns> 
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="565px" ID="lblLog2"></vt:Label>
        
    </vt:WindowView>

</asp:Content>
