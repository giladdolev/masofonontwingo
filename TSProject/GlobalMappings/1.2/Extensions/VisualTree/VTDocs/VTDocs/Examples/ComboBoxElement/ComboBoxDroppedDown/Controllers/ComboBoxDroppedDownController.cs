using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboBoxDroppedDownController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox1 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox1");
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox1.Items.Add("Item1");
            TestedComboBox1.Items.Add("Item2");
            TestedComboBox1.Items.Add("Item3");
            lblLog1.Text = "DroppedDown value: " + TestedComboBox1.DroppedDown;
            TestedComboBox2.Items.Add("Item1");
            TestedComboBox2.Items.Add("Item2");
            TestedComboBox2.Items.Add("Item3");
            lblLog2.Text = "DroppedDown value: " + TestedComboBox2.DroppedDown + '.';

        }

        public void btnChangeDroppedDown_Click(object sender, EventArgs e)
        {
            ComboBoxElement TestedComboBox2 = this.GetVisualElementById<ComboBoxElement>("TestedComboBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TestedComboBox2.DroppedDown = !TestedComboBox2.DroppedDown;
            lblLog2.Text = "DroppedDown value: " + TestedComboBox2.DroppedDown + '.';
        }

             

    }
}