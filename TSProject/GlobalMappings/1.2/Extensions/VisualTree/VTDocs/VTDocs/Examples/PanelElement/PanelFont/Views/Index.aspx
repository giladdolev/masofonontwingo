<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Font Property" ID="windowView2" LoadAction="PanelFont/OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Font" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the font of the text displayed by the control
            
            Syntax: public virtual Font { get; set; }
            The default is the value of the DefaultFont property - depending on the user's operating system
             the local culture setting of their system." Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Panel runat="server" ShowHeader="true" Text="Panel" Top="185px" Width="450px" Height="80px" ID="TestedPanel1" >
            <vt:Button runat="server" Text="button1" Top="5px" Left="10px" Width="60px" ID="button1" BorderStyle="None"></vt:Button>
                <vt:CheckBox runat="server" Text="checkBox1" Top="5px" Left="90px" Width="90px" ID="checkBox1" BackColor="LightGray"></vt:CheckBox>
                <vt:Label runat="server" Text="label1" Top="5px" Left="200px" ID="label1" BackColor="LightGray"></vt:Label>
                <vt:TextBox runat="server" Text="TextBox1" Top="5px" Left="270px" ID="textBox1" BorderStyle="None"></vt:TextBox>
        </vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="275px" Width="430px" Height="45px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="Font property of this 'TestedPanel is initially set to:
             Font-Name='Niagara Engraved', Font-Italic='true' and Font-Size='30'" Top="400px"  ID="lblExp1"></vt:Label>     
              
        <vt:Panel runat="server" ShowHeader="true" Text="TestedPanel" Font-Names="Niagara Engraved" Font-Italic="true" Font-Size="30" Top="450px" Width="450px" Height="80px" ID="TestedPanel2" >
            <vt:Button runat="server" Text="button2" Top="5px" Left="10px" Width="60px" ID="button2" BorderStyle="None"></vt:Button>
                <vt:CheckBox runat="server" Text="checkBox2" Top="5px" Left="90px" Width="90px" ID="checkBox2" BackColor="LightGray"></vt:CheckBox>
                <vt:Label runat="server" Text="label2" Top="5px" Left="200px" ID="label2" BackColor="LightGray"></vt:Label>
                <vt:TextBox runat="server" Text="TextBox2" Top="5px" Left="270px" ID="textBox2" BorderStyle="None"></vt:TextBox>
        </vt:Panel>
        <vt:Label runat="server" SkinID="Log" Top="540px" Width="420px" Height="45px" ID="lblLog2"></vt:Label>
       
         <vt:Button runat="server" Text="Change Font Value >>" Top="620px" ID="btnChangePanelFont" ClickAction="PanelFont/btnChangePanelFont_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
