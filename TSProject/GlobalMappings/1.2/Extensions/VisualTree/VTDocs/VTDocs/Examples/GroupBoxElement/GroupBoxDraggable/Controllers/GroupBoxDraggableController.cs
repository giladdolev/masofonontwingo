using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class GroupBoxDraggableController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //GroupBoxAlignment
        public void btnChangeDraggable_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            GroupBoxElement grpRuntimeDraggable = this.GetVisualElementById<GroupBoxElement>("grpRuntimeDraggable");

            if (grpRuntimeDraggable.Draggable == false)
            {
                grpRuntimeDraggable.Draggable = true;
                textBox1.Text = grpRuntimeDraggable.Draggable.ToString();
            }
            else 
            {
                grpRuntimeDraggable.Draggable = false;
                textBox1.Text = grpRuntimeDraggable.Draggable.ToString();
            }
        }

    }
}