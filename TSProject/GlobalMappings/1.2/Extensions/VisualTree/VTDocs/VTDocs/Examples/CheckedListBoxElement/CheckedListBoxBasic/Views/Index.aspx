<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Item Check" Top="57px" Left="11px" ID="windowView1" Height="700px" Width="768px">

        <vt:CheckedListBox runat="server" ID="CheckedListBox1"  BorderStyle="Dotted" Width="200px" Height="100" ItemCheckAction="BasicCheckedListBox/check">
            <Items>
                <vt:ListItem runat="server" ID="check1" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem1" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem2" Text="checkbox 1"></vt:ListItem>
                <vt:ListItem runat="server" ID="ListItem3" Text="checkbox 1"></vt:ListItem>
            </Items>
        </vt:CheckedListBox>
        <vt:Button runat="server" Text="load" Top="194px" Left="140px" ID="button1" Height="36px" ForeColor="Red" Width="120px" ClickAction="CheckedListBoxBasic\Load_CheckedListBox"></vt:Button>
    </vt:WindowView>
</asp:Content>
