using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelBackColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Change BackColor
        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel1 = this.GetVisualElementById<LabelElement>("TestedLabel1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "BackColor value: " + TestedLabel1.BackColor.ToString();

            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "BackColor value: " + TestedLabel2.BackColor.ToString();

        }


        public void btnChangeBackColor_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel2 = this.GetVisualElementById<LabelElement>("TestedLabel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedLabel2.BackColor == Color.Orange)
            {
                TestedLabel2.BackColor = Color.Green;
                lblLog2.Text = "BackColor value: " + TestedLabel2.BackColor.ToString();
            }
            else
            {
                TestedLabel2.BackColor = Color.Orange;
                lblLog2.Text = "BackColor value: " + TestedLabel2.BackColor.ToString();
            }
        }
    }
}