<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree CheckBoxes Property" Height="800px" ID="windowView1" LoadAction="TreeCheckBoxes\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="CheckBoxes" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether check boxes are displayed next to the tree nodes in the 
            tree view control.
            
            Syntax : public bool CheckBoxes { get; private set; }

            Property Value: 'true' if a check box is displayed next to each tree node in the tree view control; 
            otherwise, 'false'. The default is 'false'."
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Tree runat="server" Text="Tree" Top="230px" ID="TestedTree1" CheckBoxes="false">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1">
                    <Items>
                        <vt:TreeItem runat="server" Text="Node01" ID="Tree1Item11"></vt:TreeItem>
                        <vt:TreeItem runat="server" Text="Node11" ID="TreeItem12"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Width="185px" Top="355px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" Text="Notice - this property is not intended to be changed at run time." Top="385px" ID="Label1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="440px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The left tree is initialized to CheckBoxes 'false'
             The right tree is initialized to CheckBoxes 'true'"
            Top="490px" ID="lblExp1">
        </vt:Label>

        <vt:Tree runat="server" Text="TestedTree" Top="550px" ID="TestedTree2" >
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1">
                      <Items>
                        <vt:TreeItem runat="server" Text="Node01" ID="Tree2Item11"></vt:TreeItem>
                        <vt:TreeItem runat="server" Text="Node02" ID="Tree2Item12"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree2Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>
        <vt:Label runat="server" SkinID="Log" Width="185px" Top="675px" ID="lblLog2"></vt:Label>

        <vt:Tree runat="server" Text="TestedTree" Top="550px" ID="TestedTree3" CheckBoxes="true" Left="380px" CssClass="vt-tree-CheckBoxes">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree3Item1">
                      <Items>
                        <vt:TreeItem runat="server" Text="Node01" ID="Tree3Item11"></vt:TreeItem>
                        <vt:TreeItem runat="server" Text="Node11" ID="Tree3Item12"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree3Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree3Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>
        <vt:Label runat="server" SkinID="Log" Top="675px" Width="185px" ID="lblLog3" Left="380px"></vt:Label>


    </vt:WindowView>
</asp:Content>
