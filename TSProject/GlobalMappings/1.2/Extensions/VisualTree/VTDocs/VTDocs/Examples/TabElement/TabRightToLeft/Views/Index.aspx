<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab RightToLeft Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="Tab RightToLeft is initially set to false" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Tab runat="server" Text="Tab RightToLeft is set tp 'Yes'" RightToLeft ="Yes" Top="50px"  Left="140px" ID="tabRightToLeft" Height="200px" Width="250px">
             <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="TabItem3" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="TabItem4" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>          

        <vt:Button runat="server"  Text="Change Tab RightToLeft" Top="150px" Left="450px" ID="btnChangeRightToLeft" Height="36px" TabIndex="1" Width="150px" ClickAction="TabRightToLeft\btnChangeRightToLeft_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="275px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="250px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>

