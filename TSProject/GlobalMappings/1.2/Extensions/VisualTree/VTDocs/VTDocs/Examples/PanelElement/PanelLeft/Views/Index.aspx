<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Left Property" ID="windowView2" LoadAction="PanelLeft\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Left" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the distance, in pixels, between the left edge 
            of the control and the left edge of its container's client area.
            
            Syntax: public int Left { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
      

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Left property of this Panel is initially set to 80." Top="250px"  ID="lblExp1"></vt:Label>     
        
        <!-- TestedPanel -->
        <vt:Panel runat="server" Text="TestedPanel" Top="310px" ID="TestedPanel"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="405px" ID="lblLog"></vt:Label>
          
        <vt:Button runat="server" Text="Change Left Value >>" Top="470px" ID="ChangeLeft" ClickAction="PanelLeft\ChangeLeft_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>