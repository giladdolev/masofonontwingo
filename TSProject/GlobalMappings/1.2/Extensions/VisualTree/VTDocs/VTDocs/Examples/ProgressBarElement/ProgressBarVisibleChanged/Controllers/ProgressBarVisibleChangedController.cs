using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
       
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ProgressBarElement prgVisible = this.GetVisualElementById<ProgressBarElement>("prgVisible");

            if (prgVisible.Visible == true) //Get
            {
                prgVisible.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + prgVisible.Visible;
            }
            else
            {
                prgVisible.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + prgVisible.Visible;
            }
        }

      //  grpText_TextChanged
        public void prgVisible_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}