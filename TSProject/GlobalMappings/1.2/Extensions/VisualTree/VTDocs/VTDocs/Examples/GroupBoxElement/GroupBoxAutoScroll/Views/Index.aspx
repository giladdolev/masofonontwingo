<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage"  StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="GroupBox AutoScroll Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:GroupBox runat="server" Text="AutoScroll is set to 'true'" Top="55px" AutoScroll="true" Left="140px" ID="grpAutoScroll" Height="70px" 
            Width="200px">
            <vt:Button runat="server" Text="button1" Top="0px" Left="-30px" ID="Button1" Height="90px"  Width="100px" ></vt:Button>
        </vt:GroupBox>           

        <vt:Label runat="server" Text="RunTime" Top="140px" Left="140px" ID="Label2"  Height="13px" Width="120px"></vt:Label>

        <vt:GroupBox runat="server" Text="RuntimeAutoScroll" Top="165px" Left="140px" ID="grpRuntimeAutoScroll" Height="70px" TabIndex="1" Width="200px" >
            <vt:Button runat="server" Text="Button2" Top="0px" Left="-30px" ID="Button2" Height="90px"  Width="100px" ></vt:Button>
        </vt:GroupBox>

        <vt:Button runat="server" Text="Change GroupBox AutoScroll" Top="195px" Left="420px" ID="btnChangeAutoScroll" Height="36px" TabIndex="1" Width="200px" ClickAction="GroupBoxAutoScroll\btnChangeAutoScroll_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="275px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        