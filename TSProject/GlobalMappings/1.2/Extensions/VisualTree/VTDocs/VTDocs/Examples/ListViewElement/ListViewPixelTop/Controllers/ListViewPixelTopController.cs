using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewPixelTopController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
     
         public void OnLoad(object sender, EventArgs e)
         {
             ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

             lblLog.Text = "PixelTop Value: " + TestedListView.PixelTop + '.';

         }
         public void btnChangePixelTop_Click(object sender, EventArgs e)
         {
             LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
             ListViewElement TestedListView = this.GetVisualElementById<ListViewElement>("TestedListView");
             if (TestedListView.PixelTop == 285)
             {
                 TestedListView.PixelTop = 300;
                 lblLog.Text = "PixelTop Value: " + TestedListView.PixelTop + '.';

             }
             else
             {
                 TestedListView.PixelTop = 285;
                 lblLog.Text = "PixelTop Value: " + TestedListView.PixelTop + '.';
             }

         }

    }
}