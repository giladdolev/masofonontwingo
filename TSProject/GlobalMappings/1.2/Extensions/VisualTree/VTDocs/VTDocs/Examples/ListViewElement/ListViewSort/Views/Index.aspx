<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Items" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewItems/Form_Load">

        <vt:Label runat="server" Text="Run Time :" Top="112px" Left="50px" ID="labelRunTime" Width="350px" Font-Bold="true"></vt:Label>
        <vt:ListView runat="server" ID="listView1" Top="160px" Left="50px" Height="150px" Width="300px" Sorting="Descending">
            <Items>
                <vt:ListViewItem runat="server" Text="a">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="11111"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="22222"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="z">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="33333"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="44444"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                 <vt:ListViewItem runat="server" Text="b">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="11111"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="22222"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="d">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="33333"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="44444"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>

        </vt:ListView>
            
        <vt:Label runat="server" Text="Sort Method:" Top="100px" Left="50px" ID="label1" Width="350px" Font-Bold="true"></vt:Label>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Ascending" Top="120px" Left="130px" ID="btnItems" Height="30px" TabIndex="1" Width="150px" ClickAction="ListViewSort\btnSortAscending_Click"></vt:Button>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Descending" Top="120px" Left="300px" ID="Button1" Height="30px" TabIndex="1" Width="150px" ClickAction="ListViewSort\btnSortDescending_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
