using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxContextMenuStripController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new TextBoxContextMenuStrip());
        }
        private TextBoxContextMenuStrip ViewModel
        {
            get { return this.GetRootVisualElement() as TextBoxContextMenuStrip; }
        }

        private void CreateContextMenu()
        {
            TextBoxElement ContextMenuStrip2 = this.GetVisualElementById<TextBoxElement>("ContextMenuStrip2");

            if (ContextMenuStrip2.ContextMenuStrip == null)
            {
                ContextMenuStripElement menuStrip = new ContextMenuStripElement();
                // menuStrip.ID = "cnt";
                menuStrip.ID = "RunTimeCnt";

                // TODO: Method '.ctor' of type 'System.Windows.Forms.ToolStripMenuItem' is unmapped. (CODE=1005)
                ToolBarMenuItem menuItem = new ToolBarMenuItem("Exit");
                menuItem.Click += new EventHandler(menuItem_Click);
                menuItem.ID = "Exit";
                menuStrip.Items.Add(menuItem);

                //  this.Controls.Add(menuStrip);

                  
                this.ViewModel.Controls.Add(menuStrip);
                ContextMenuStrip2.ContextMenuStrip = menuStrip;
            }
            else 
            {
                ContextMenuStrip2.ContextMenuStrip = null;
            }
           
        }
        public void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolBarMenuItem menuItem = (ToolBarMenuItem)sender;
            if (object.Equals(menuItem.Text, "Exit"))
            {
                this.ViewModel.Close();
            }
        }

        private void menuItem_Click(object sender, EventArgs e)
        {
            ToolBarMenuItem menuItem = (ToolBarMenuItem)sender;
            if (object.Equals(menuItem.Text, "Exit"))
            {
                this.ViewModel.Close();
            }
        }

        public void btnSetContextMenuStrip_Click(object sender, EventArgs e)
        {
            CreateContextMenu();
        }
        
      
    }
}