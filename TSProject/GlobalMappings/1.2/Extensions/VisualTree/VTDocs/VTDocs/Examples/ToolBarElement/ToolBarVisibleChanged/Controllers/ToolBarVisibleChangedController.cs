using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarVisibleChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
       
        public void btnChangeVisible_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ToolBarElement toolBar1 = this.GetVisualElementById<ToolBarElement>("toolBar1");

            if (toolBar1.Visible == true) //Get
            {
                toolBar1.Visible = false; //Set
                textBox1.Text = "The Visible property value is set to: " + toolBar1.Visible;
            }
            else
            {
                toolBar1.Visible = true;
                textBox1.Text = "The Visible property value is set to: " + toolBar1.Visible;
            }
        }

      //  grpText_TextChanged
        public void toolBar1_VisibleChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "VisibleChanged Event is invoked\\r\\n";

        }
    }
}