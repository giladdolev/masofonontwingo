using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //RadioButtonAlignment
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement btnTestedRadioButton = this.GetVisualElementById<RadioButtonElement>("btnTestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Text  property value: " + btnTestedRadioButton.Text;

        }

        public void btnChangeRadioButtonText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RadioButtonElement btnTestedRadioButton = this.GetVisualElementById<RadioButtonElement>("btnTestedRadioButton");
            if (btnTestedRadioButton.Text == "TestedRadioButton")
            {
                btnTestedRadioButton.Text = "New Text";
                lblLog.Text = "Text  property value: " + btnTestedRadioButton.Text;

            }
            else
            {
                btnTestedRadioButton.Text = "TestedRadioButton";
                lblLog.Text = "Text  property value: " + btnTestedRadioButton.Text;
            }

        }

    }
}