﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Text="Grid ContextMenuStrip property" ID="windowView" LoadAction="GridContextMenuStrip\OnLoad">
       
        <vt:Label runat="server" SkinID="Title" Text="ContextMenuStrip" Left="220px" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Gets or sets the ContextMenuStrip associated with this control.
            
            Syntax: public ContextMenuStripElement ContextMenuStrip { get; set; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, Performing right click on the Grid will display the Custom Context Menu."
            Top="235px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="300px" ID="TestedGrid" CssClass="vt-test-grid" ContextMenuStripID="contextMenu">
            <Columns>
                <vt:GridColumn  runat="server" ID="GridCol1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="GridCol3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>      

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="430px" Width="340px" ID="lblLog"></vt:Label>

          <vt:ContextMenuStrip runat="server" ID="contextMenu" >
              <vt:ToolBarMenuItem ID="tbItem1" runat="server" Height="25" Text="Item1" ClickAction="GridContextMenuStrip\tbItem1_Click">
                  <vt:ToolBarItem ID="tbItem1_1" runat="server" Height="25" Text="Item1_1" ClickAction="GridContextMenuStrip\tbItem1_1_Click"></vt:ToolBarItem>
              </vt:ToolBarMenuItem>
              <vt:ToolBarItem ID="tbItem2" runat="server" Height="25" Text="Item2" ClickAction="GridContextMenuStrip\tbItem2_Click"></vt:ToolBarItem>
        </vt:ContextMenuStrip>

    </vt:WindowView>

</asp:Content>
