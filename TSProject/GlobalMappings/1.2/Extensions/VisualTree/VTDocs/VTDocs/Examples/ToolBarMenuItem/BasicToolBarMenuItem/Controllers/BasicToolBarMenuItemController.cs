using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicToolBarMenuItemController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        public ActionResult Index()
        {
            return View(new BasicToolBarMenuItem());
        }
        private BasicToolBarMenuItem ViewModel
        {
            get { return this.GetRootVisualElement() as BasicToolBarMenuItem; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Add ToolBarMenuItem ...";
        }

        public void AddNewToolBarItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            MenuElement TestedMenu = this.GetVisualElementById<MenuElement>("TestedMenu");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cboMenuItemConstructors = this.GetVisualElementById<ComboBoxElement>("cboMenuItemConstructors");

            switch (cboMenuItemConstructors.SelectedIndex)
            {
                case 0:
                    ToolBarMenuItem defaultsCtorToolBarMenuItem = new ToolBarMenuItem();
                    TestedMenu.Items.Add(defaultsCtorToolBarMenuItem); lblLog.Text = "ToolBarMenuItem() was invoked";
                    break;
                case 1:
                    ToolBarMenuItem StringParamCtorToolBarMenuItem = new ToolBarMenuItem("Menu" + this.ViewModel.MenuItemsIndex++);
                    TestedMenu.Items.Add(StringParamCtorToolBarMenuItem);
                    lblLog.Text = "ToolBarMenuItem(String) was invoked"; break;
                case 2:
                    ToolBarMenuItem FourParamsCtorToolBarMenuItem = new ToolBarMenuItem("Menu" + ViewModel.MenuItemsIndex++, new ResourceReference(@"Content\Images\icon.jpg"), null, Keys.F10);
                    lblLog.Text = "ToolBarMenuItem(String, ResourceReference, EventHandler, Keys) was invoked"; TestedMenu.Items.Add(FourParamsCtorToolBarMenuItem);
                    break;
                default:
                    lblLog.Text = "Add ToolBarMenuItem ...";
                    break;
            }
        }

        //
        public void FourParamsCtorToolBarMenuItem_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Menu" + (ViewModel.MenuItemsIndex - 1) + " Click event was invoked";
        }



    }
}