<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic MessageBox" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:ToolBar runat="server" ID="MainMenu">
            <vt:ToolBarMenuItem runat="server" Text="File">
                <vt:ToolBarCheckItem runat="server" Text="CheckItem1"  />
                <vt:ToolBarCheckItem runat="server" Text="CheckItem1" />
                <vt:ToolBarMenuSeparator runat="server" />
                <vt:ToolBarMenuItem runat="server" Text="Exit"  />
            </vt:ToolBarMenuItem>
        </vt:ToolBar>
    </vt:WindowView>
</asp:Content>
