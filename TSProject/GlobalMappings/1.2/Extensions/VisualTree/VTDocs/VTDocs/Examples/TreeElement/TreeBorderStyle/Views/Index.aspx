<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree BorderStyle Property" ID="windowView1" LoadAction="TreeBorderStyle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BorderStyle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the BorderStyle of the control

            Syntax: public BorderStyle BorderStyle { get; set; }
            The property value is one of the BorderStyle enumeration values. The default is 'None'.
            " Top="75px"  ID="lblDefinition"></vt:Label>
      
        <vt:Tree runat="server" Text="Tree" Top="170px" ID="TestedTree1">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree1Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree1Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree1Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="295px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="360px"  ID="lblExample"></vt:Label>
       
         <vt:Label runat="server" Text="BorderStyle property of this Tree is initially set to 'Dotted'" Top="400px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tree runat="server"  Text="TestedTree" BorderStyle="Dotted" Top="440px" ID="TestedTree2">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item2"></vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree2Item3"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="560px" ID="lblLog2"></vt:Label>
      
          <vt:Button runat="server" Text="Change BorderStyle value >>" Width="180px"  Top="615px" ID="btnChangeTreeBorderStyle" ClickAction="TreeBorderStyle\btnChangeTreeBorderStyle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
