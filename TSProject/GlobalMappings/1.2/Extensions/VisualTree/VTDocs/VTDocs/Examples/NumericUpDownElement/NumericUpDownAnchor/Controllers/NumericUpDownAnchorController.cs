using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownAnchorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeAnchor_Click(object sender, EventArgs e)
        {
            NumericUpDownElement nudAnchorChange = this.GetVisualElementById<NumericUpDownElement>("nudAnchorChange");
            PanelElement panel2 = this.GetVisualElementById<PanelElement>("panel2");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");



            if (nudAnchorChange.Anchor == (AnchorStyles.Left | AnchorStyles.Top)) //Get
            {
                nudAnchorChange.Anchor = AnchorStyles.Top; //button keeps the top gap from the panel
                panel2.Size = new Size(400, 171);
                textBox1.Text = "NumericUpDown Anchor value: " + nudAnchorChange.Anchor.ToString();
            }

            else if (nudAnchorChange.Anchor == AnchorStyles.Top)
            {
                nudAnchorChange.Anchor = AnchorStyles.Left; //MainControlTested keeps the left gap from the panel
                panel2.Size = new Size(315, 190);
                textBox1.Text = "NumericUpDown Anchor value: " + nudAnchorChange.Anchor.ToString();
            }
            else if (nudAnchorChange.Anchor == AnchorStyles.Left)
            {
                nudAnchorChange.Anchor = AnchorStyles.Right; //button keeps the Right gap from the panel
                panel2.Size = new Size(200, 190);
                textBox1.Text = "NumericUpDown Anchor value: " + nudAnchorChange.Anchor.ToString();
            }
            else if (nudAnchorChange.Anchor == AnchorStyles.Right)
            {
                nudAnchorChange.Anchor = AnchorStyles.Bottom; //button keeps the Bottom gap from the panel
                panel2.Size = new Size(200, 160);
                textBox1.Text = "NumericUpDown Anchor value: " + nudAnchorChange.Anchor.ToString();
            }
            else if (nudAnchorChange.Anchor == AnchorStyles.Bottom)
            {
                nudAnchorChange.Anchor = AnchorStyles.None; //Resizing panel doesn't affect the button 
                panel2.Size = new Size(315, 171);//Set  back the panel size
                textBox1.Text = "NumericUpDown Anchor value: " + nudAnchorChange.Anchor.ToString();
            }
            else if (nudAnchorChange.Anchor == AnchorStyles.None)
            {
                //Restore panel1 and btnChangeAnchor settings
                nudAnchorChange.Anchor = (AnchorStyles.Left | AnchorStyles.Top); //button keeps the Bottom gap and Left gap from the panel

                panel2.Size = new Size(300, 72);//Set  back the panel size
                nudAnchorChange.Location = new Point(75, 18);
                textBox1.Text = "NumericUpDown Anchor value: " + nudAnchorChange.Anchor.ToString();
            }       
        }

    }
}