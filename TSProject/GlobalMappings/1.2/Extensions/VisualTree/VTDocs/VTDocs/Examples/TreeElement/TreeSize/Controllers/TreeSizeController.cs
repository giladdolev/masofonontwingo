using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedTree.Size;
        }
        private void btnChangeTreeSize_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTree.Size == new Size(140, 110)) //Get
            {
                TestedTree.Size = new Size(300, 40); //Set
                lblLog.Text = "Size value: " + TestedTree.Size;
            }
            else  
            {
                TestedTree.Size = new Size(140, 110);
                lblLog.Text = "Size value: " + TestedTree.Size;
            }
             
        }

    }
}