using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerBackGroundImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            

            if (TestedSplitContainer1.BackgroundImage == null)
            {
                lblLog1.Text = "Splitter W/O background image";
            }
            else
            {
                lblLog1.Text = "Splitter Set with background image";
            }

          

            if (TestedSplitContainer2.BackgroundImage == null)
            {
                lblLog2.Text = "Splitter W/O background image.";
            }
            else
            {
                lblLog2.Text = "Splitter Set with background image.";
            }
         
        }

        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedSplitContainer2.BackgroundImage == null)
            {
                TestedSplitContainer2.BackgroundImage = new UrlReference("/Content/Images/Cute.png");
                lblLog2.Text = "Splitter set with background image.";
            }
            else
            {
                TestedSplitContainer2.BackgroundImage = null;
                lblLog2.Text = "Splitter W/O background image.";

            }
        }

    }
}