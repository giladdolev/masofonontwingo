<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tree SelectedItem Property" ID="windowView" Height="750px" LoadAction="TreeSelectedItem\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectedItem" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the tree item that is currently selected in the tree control.

            Syntax: public TreeItem SelectedItem { get; set; }
            If no TreeItem is currently selected, the SelectedItem property is null.
            " Top="75px" ID="Label3" ></vt:Label>         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, the right tree is filled through Items property and has a list view, 
            and the left tree is filled through DataSource property and has a grid view. you can set the SelectedItem  
            property by clicking on TreeItems with the mouse, or by using the buttons." Top="240px"  ID="lblExp1"></vt:Label>     

        <vt:Label runat="server" Text="Tree List View" Top="320px" Width="210px" Left="80px" ID="lblTreeListView"></vt:Label>

        <vt:Tree runat="server" ID="TestedTree1" CssClass="vt-test-tree-1" SkinID="TreeList" Top="345px" Width="210px" Height="200px" AfterSelectAction="TreeSelectedItem\TestedTree_AfterSelect"></vt:Tree>

        <vt:Label runat="server" Text="Tree Grid View" Top="320px" Width="210px" Left="380px" ID="lblTreeGridView"></vt:Label>

        <vt:Tree runat="server" Text="TestedTree2" CssClass="vt-test-tree-2" Left="380px" Width="210px" Height="200px" Top="345px" ID="TestedTree2" AfterSelectAction="TreeSelectedItem\TestedTree_AfterSelect">
            <Items>
                <vt:TreeItem runat="server" Text="Node0" ID="Tree2Item1">
                    <Items>
                        <vt:TreeItem runat="server" Text="Node01" ID="Tree2Item2">
                            <Items>
                                <vt:TreeItem runat="server" Text="Node011" ID="TreeItem3">
                                    <Items>
                                        <vt:TreeItem runat="server" Text="Node0111" ID="TreeItem4"></vt:TreeItem>
                                    </Items>
                                </vt:TreeItem>
                            </Items>
                        </vt:TreeItem>
                    </Items>
                </vt:TreeItem>

                <vt:TreeItem runat="server" Text="Node1" ID="Tree2Item5">
                    <Items>
                        <vt:TreeItem runat="server" Text="Node11" ID="Tree2Item6"></vt:TreeItem>
                        <vt:TreeItem runat="server" Text="Node12" ID="Tree2Item7"></vt:TreeItem>
                    </Items>
                </vt:TreeItem>
                <vt:TreeItem runat="server" Text="Node2" ID="Tree2Item9"></vt:TreeItem>
            </Items>
        </vt:Tree>

        <vt:Label runat="server" SkinID="Log" Top="560px" Height="40px" Width="510px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Select Item ID 5 >>" Top="625px" Width="180px" ID="btnSelect5" ClickAction="TreeSelectedItem\btnSelect5_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Node1 >>" Top="625px" Left="380px" Width="180px" ID="btnSelectNode1" ClickAction="TreeSelectedItem\btnSelectNode1_Click"></vt:Button>
             
        <vt:Button runat="server" Text="Select Item ID 2 >>" Top="665px" Width="180px" ID="btnSelect2" ClickAction="TreeSelectedItem\btnSelect2_Click"></vt:Button>

        <vt:Button runat="server" Text="Select Node01 >>" Top="665px" Left="380px" Width="180px" ID="btnSelectNode01" ClickAction="TreeSelectedItem\btnSelectNode01_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>