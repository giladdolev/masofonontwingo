using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeDock_Click(object sender, EventArgs e)
        {
            CheckBoxElement chkDockChange = this.GetVisualElementById<CheckBoxElement>("chkDockChange");
           
            if (chkDockChange.Dock == Dock.Bottom)
            {
                chkDockChange.Dock = Dock.Fill; 
            }
            else if (chkDockChange.Dock == Dock.Fill)
            {
                chkDockChange.Dock = Dock.Left; 
            }
            else if (chkDockChange.Dock == Dock.Left)
            {
                chkDockChange.Dock = Dock.Right; 
            }
            else if (chkDockChange.Dock == Dock.Right)
            {
                chkDockChange.Dock = Dock.Top; 
            }
            else if (chkDockChange.Dock == Dock.Top)
            {
                chkDockChange.Dock = Dock.None;
            }
            else if (chkDockChange.Dock == Dock.None)
            {
                chkDockChange.Dock = Dock.Bottom;
            }       
        }

    }
}