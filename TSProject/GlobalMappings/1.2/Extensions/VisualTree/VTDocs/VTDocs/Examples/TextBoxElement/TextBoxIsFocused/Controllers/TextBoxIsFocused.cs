using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxIsFocusedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            TextBoxElement TestedTextBox3 = this.GetVisualElementById<TextBoxElement>("TestedTextBox3");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            lblLog.Text = "TextBox1 IsFocuse value: " + TestedTextBox1.IsFocused + "\\r\\nTextBox2 IsFocuse value: " + TestedTextBox2.IsFocused + "\\r\\nTextBox3 IsFocuse value: " + TestedTextBox3.IsFocused;

        }

        public void textBoxes_GotFocus(object sender, EventArgs e)
        {
            PrintToLog();
        }

        public void textBoxes_LostFocus(object sender, EventArgs e)
        {
            PrintToLog();
        }
        private void PrintToLog()
        {
            TextBoxElement TestedTextBox1 = this.GetVisualElementById<TextBoxElement>("TestedTextBox1");
            TextBoxElement TestedTextBox2 = this.GetVisualElementById<TextBoxElement>("TestedTextBox2");
            TextBoxElement TestedTextBox3 = this.GetVisualElementById<TextBoxElement>("TestedTextBox3");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            lblLog.Text = "TextBox1 IsFocuse value: " + TestedTextBox1.IsFocused + "\\r\\nTextBox2 IsFocuse value: " + TestedTextBox2.IsFocused + "\\r\\nTextBox3 IsFocuse value: " + TestedTextBox3.IsFocused;
        }

        

       

    }
}