<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RadioButton Location Property" ID="windowView1" LoadAction="RadioButtonLocation\OnLoad">
   
        <vt:Label runat="server" SkinID="Title" Text="Location" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the coordinates of the upper-left corner of the control relative to the 
            upper-left corner of its container.

            Syntax: public Point Location { get; set; }"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="200px" ID="lblExample"></vt:Label>
       
        <vt:Label runat="server" Text="Location property of this RadioButton is initially set to: Left='80px' Top='310px'" Top="250px" ID="lblExp1"></vt:Label>

        <vt:RadioButton runat="server" Text="TestedRadioButton" Top="310px" ID="TestedRadioButton" TabIndex="1"></vt:RadioButton>

        <vt:Label runat="server" SkinID="Log" Top="350px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Location value >>" Top="415px" ID="btnChangeLocation" Width="180px" ClickAction="RadioButtonLocation\btnChangeLocation_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
        