using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerForeColorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer1 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer1");
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "ForeColor value: " + TestedSplitContainer1.ForeColor;
            lblLog2.Text = "ForeColor value: " + TestedSplitContainer2.ForeColor + '.';
        }

        public void btnChangeSplitContainerForeColor_Click(object sender, EventArgs e)
        {
            SplitContainer TestedSplitContainer2 = this.GetVisualElementById<SplitContainer>("TestedSplitContainer2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedSplitContainer2.ForeColor == Color.Green)
            {
                TestedSplitContainer2.ForeColor = Color.Blue;
                lblLog2.Text = "ForeColor value: " + TestedSplitContainer2.ForeColor + '.';
            }
            else
            {
                TestedSplitContainer2.ForeColor = Color.Green;
                lblLog2.Text = "ForeColor value: " + TestedSplitContainer2.ForeColor + '.';
            }
        }

    }
}