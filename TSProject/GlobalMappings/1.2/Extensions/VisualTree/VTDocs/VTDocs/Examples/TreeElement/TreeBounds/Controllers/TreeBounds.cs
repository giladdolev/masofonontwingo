using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreeBoundsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Tree bounds are: \\r\\n" + TestedTree.Bounds;

        }
        public void btnChangeTreeBounds_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            if (TestedTree.Bounds == new Rectangle(100, 340, 200, 50))
            {
                TestedTree.Bounds = new Rectangle(80, 355, 140, 110);
                lblLog.Text = "Tree bounds are: \\r\\n" + TestedTree.Bounds;

            }
            else
            {
                TestedTree.Bounds = new Rectangle(100, 340, 200, 50);
                lblLog.Text = "Tree bounds are: \\r\\n" + TestedTree.Bounds;
            }

        }

    }
}