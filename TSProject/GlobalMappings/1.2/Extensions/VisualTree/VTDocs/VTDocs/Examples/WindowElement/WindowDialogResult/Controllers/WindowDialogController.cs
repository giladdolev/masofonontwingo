using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowDialogController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index2()
        {
            return View(new WindowDialog());
        }

        private WindowDialogResult ViewModel
        {
            get { return this.GetRootVisualElement() as WindowDialogResult; }
        }

        public void btnOk_Click(object sender, EventArgs e)
        {

            this.ViewModel.DialogResult = DialogResult.OK;
        }

        public void btnCancel_Click(object sender, EventArgs e)
        {

            this.ViewModel.DialogResult = DialogResult.Cancel;
        }


        

    }
}