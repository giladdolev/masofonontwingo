﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
   <vt:WindowView runat="server" Text="Grid ColumnHeaderMouseRightClick event" ID="windowView1" LoadAction="GridColumnHeaderMouseRightClick\Window_Load">

        <vt:Label runat="server" SkinID="Title" Text="Grid ColumnHeaderMouseRightClick" ID="lblTitle">
        </vt:Label>

        <vt:Label runat="server" Text="Occurs when the user clcik on the column header.
            Syntax: public event EventHandler ColumnHeaderMouseRightClick"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example you can click on the header column grid and sho how the event works."
            Top="200px" ID="lblExp2">
        </vt:Label>

        <vt:Grid runat="server" Top="240px" Left="10px" ID="gridElement" Height="200px" Width="700px"  ColumnHeaderMouseRightClickAction="GridColumnHeaderMouseRightClick\Grid_HeaderRightClick" >
            <Columns>
                <vt:GridColumn ID="name" runat="server" HeaderText="Name" DataMember="name" Width="60"></vt:GridColumn>
                <vt:GridColumn ID="email" runat="server" HeaderText="Email" DataMember="email" Width="80"></vt:GridColumn>
                <vt:GridDateTimePickerColumn ID="phone" runat="server" HeaderText="Phone" DataMember="phone" Width="90"></vt:GridDateTimePickerColumn>
                <vt:GridCheckBoxColumn ID="clmCheck" runat="server" HeaderText="Boolt" DataMember="boolt" Width="90"></vt:GridCheckBoxColumn>
            </Columns>
        </vt:Grid>      

        <vt:Button runat="server" SkinID="Wide" Text="Raise ColumnHeaderMouseRightClick by click" Top="460px" ID="btn1" ClickAction="GridColumnHeaderMouseRightClick\btn1_raiseEvent"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Left="80px" Top="510px" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="550px" ID="txtEventLog"></vt:TextBox>


    </vt:WindowView>

</asp:Content>
