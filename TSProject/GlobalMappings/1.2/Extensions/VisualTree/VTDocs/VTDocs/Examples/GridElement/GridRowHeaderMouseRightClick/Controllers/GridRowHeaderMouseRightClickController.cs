using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridRowHeaderMouseRightClickController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void Window_Load(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            DataTable source = new DataTable();
            source.Columns.Add("name", typeof(string));
            source.Columns.Add("email", typeof(string));      

            source.Rows.Add("Lisa", "lisa@simpsons.com");
            source.Rows.Add("Bart", "Bart@simpsons.com");
            source.Rows.Add("Homer", "Homer@simpsons.com");
            source.Rows.Add("Lisa", "Lisa@simpsons.com");
            source.Rows.Add("Marge", "Marge@simpsons.com");
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com");
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com");
            TestedGrid.DataSource = source;

            lblLog1.Text = "No Row Header Mouse Right Click has been performed.";
        }

        public void Grid_RowHeaderClick(object sender, EventArgs e)
        {
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "\\r\\nGridRowHeaderMouseRightClick Event invoked";
            lblLog1.Text = "Row Header Mouse Right Click has been performed.";

        }
	}
}
