using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridSelectedValueController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid1 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");            

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(int));
            source.Rows.Add("James", "Dean", 1);
            source.Rows.Add("Bob", "Marley", 2);
            source.Rows.Add("Dana", "International", 3);

            TestedComboGrid1.ValueMember = "Column1";
            TestedComboGrid1.DisplayMember = "Column3";

            TestedComboGrid1.DataSource = source;
            lblLog1.Text = "SelectedValue: " + TestedComboGrid1.SelectedValue;

            DataTable source1 = new DataTable();
            source1.Columns.Add("Column1", typeof(string));
            source1.Columns.Add("Column2", typeof(string));
            source1.Columns.Add("Column3", typeof(int));
            source1.Rows.Add("James", "Dean", 1);
            source1.Rows.Add("Bob", "Marley", 2);
            source1.Rows.Add("Dana", "International", 3);

            TestedComboGrid2.ValueMember = "Column1";
            TestedComboGrid2.DisplayMember = "Column3";

            TestedComboGrid2.DataSource = source1;

            lblLog2.Text = "SelectedValue: " + TestedComboGrid2.SelectedValue + ".";
        }

        public void SelectedValueChanged(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "The SelectedValue is: " + TestedComboGrid2.SelectedValue.ToString() + ".";
        }

        private void ChooseNext_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");

            string sv = "";
            if (TestedComboGrid2.SelectedValue != null)
                sv = TestedComboGrid2.SelectedValue.ToString();

            if (String.Compare(sv, "James") == 0)
            {
                TestedComboGrid2.SelectedValue = "Bob";
            }
            else if (String.Compare(sv, "Bob") == 0)
            {
                TestedComboGrid2.SelectedValue = "Dana";
            }
            else 
            {
                TestedComboGrid2.SelectedValue = "James";
            }

        }
        
    }
}