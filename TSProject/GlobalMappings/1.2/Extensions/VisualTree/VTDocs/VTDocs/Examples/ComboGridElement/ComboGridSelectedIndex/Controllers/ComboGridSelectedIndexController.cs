using System;
using System.Data;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridSelectedIndexController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid1 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");            

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(int));
            source.Rows.Add("James", "Dean", 1);
            source.Rows.Add("Bob", "Marley", 2);
            source.Rows.Add("Dana", "International", 3);

            TestedComboGrid1.ValueMember = "Column1";
            TestedComboGrid1.DisplayMember = "Column3";

            TestedComboGrid1.DataSource = source;
            lblLog1.Text = "SelectedIndex Value: " + TestedComboGrid1.SelectedIndex.ToString();

            DataTable source1 = new DataTable();
            source1.Columns.Add("Column1", typeof(string));
            source1.Columns.Add("Column2", typeof(string));
            source1.Columns.Add("Column3", typeof(int));
            source1.Rows.Add("James", "Dean", 1);
            source1.Rows.Add("Bob", "Marley", 2);
            source1.Rows.Add("Dana", "International", 3);

            TestedComboGrid2.ValueMember = "Column1";
            TestedComboGrid2.DisplayMember = "Column3";

            TestedComboGrid2.DataSource = source1;

            lblLog2.Text = "SelectedIndex Value: " + TestedComboGrid2.SelectedIndex.ToString() + ".";
        }


        public void TestedComboGrid1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid1 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "The SelectedIndex is: " + TestedComboGrid1.SelectedIndex.ToString() + ".";
        }

        public void TestedComboGrid2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog2.Text = "The SelectedIndex is: " + TestedComboGrid2.SelectedIndex.ToString() + ".";
        }

        private void ChooseNext_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid2 = this.GetVisualElementById<ComboGridElement>("TestedComboGrid2");
            if (TestedComboGrid2.SelectedIndex == 2)
            {
                TestedComboGrid2.SelectedIndex = 0;
            }
            else
            {
                TestedComboGrid2.SelectedIndex++;
            }
        }
        
    }
}