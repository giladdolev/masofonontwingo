using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownDockController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeDock_Click(object sender, EventArgs e)
        {
            NumericUpDownElement nudDockChange = this.GetVisualElementById<NumericUpDownElement>("nudDockChange");
           
            if (nudDockChange.Dock == Dock.Bottom)
            {
                nudDockChange.Dock = Dock.Fill; 
            }
            else if (nudDockChange.Dock == Dock.Fill)
            {
                nudDockChange.Dock = Dock.Left; 
            }
            else if (nudDockChange.Dock == Dock.Left)
            {
                nudDockChange.Dock = Dock.Right; 
            }
            else if (nudDockChange.Dock == Dock.Right)
            {
                nudDockChange.Dock = Dock.Top; 
            }
            else if (nudDockChange.Dock == Dock.Top)
            {
                nudDockChange.Dock = Dock.None;
            }
            else if (nudDockChange.Dock == Dock.None)
            {
                nudDockChange.Dock = Dock.Bottom;
            }       
        }

    }
}