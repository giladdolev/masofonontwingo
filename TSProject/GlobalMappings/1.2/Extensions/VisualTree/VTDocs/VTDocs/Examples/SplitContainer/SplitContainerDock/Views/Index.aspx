<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="splitContainer Dock Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
       
         <vt:Label runat="server" Text="Initialized component : The splitContainer is initialliy docked to Fill" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45"></vt:Label> 

        <vt:splitContainer runat="server" Text="splitContainer" Top="165px" Left="140px" ID="splDock" Dock="Fill" Height="150px" TabIndex="1" Width="200px"></vt:splitContainer>

        <vt:Label runat="server" Text="Run Time : Click 'Change Dock style' button" Top="170px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45"></vt:Label> 

        <vt:Button runat="server" Text="Change Dock style" Top="210px" Left="200px" ID="btnDock" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="splitContainerDock\btnDock_Click"></vt:Button>
        
        <vt:TextBox runat="server" Text="" Top="325px" Left="200px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"></vt:TextBox>

       
        


    </vt:WindowView>
</asp:Content>
        