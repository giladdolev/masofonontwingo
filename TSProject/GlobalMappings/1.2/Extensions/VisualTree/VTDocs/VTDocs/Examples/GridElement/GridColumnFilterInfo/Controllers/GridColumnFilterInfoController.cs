using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System.Data;
namespace HelloWorldApp
{
    public class GridColumnFilterInfoController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        public void View_load(object sender, EventArgs e)
        {
            GridElement gridElement = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            
            DataTable source = new DataTable();
            source.Columns.Add("Name", typeof(string));
            source.Columns.Add("Email", typeof(string));
            source.Columns.Add("Phone", typeof(string));

            source.Rows.Add("1-Lisa", "lisa@simpsons.com", "555-111-1224");
            source.Rows.Add("Bart", "Bart@simpsons.com", "555-111-1224");
            source.Rows.Add("#Homer", "Homer@simpsons.com", "555-111-1224");
            source.Rows.Add("Lisa", "Lisa@simpsons.com", "555-111-1224");
            source.Rows.Add("Galit", "Galit@simpsons.com", "555-111-1224");
            source.Rows.Add("Galilcs", "Galilcs@simpsons.com", "555-111-1224");
            source.Rows.Add("Gizmox", "Gizmox@simpsons.com", "555-111-1224");
            gridElement.DataSource = source;

            lblLog.Text = "Insert a value to filter and press the button.";
            
        }

        public void buttonFilter_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement TextBoxFilter = this.GetVisualElementById<TextBoxElement>("TextBoxFilter");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (!string.IsNullOrEmpty(TextBoxFilter.Text))
            {

                TestedGrid.ColumnFilterInfo = new System.Web.VisualTree.Elements.FilterInfo("Name", TextBoxFilter.Text);
                
            }

            lblLog.Text = "Value to filter: " + TextBoxFilter.Text;
            
        }
        public void buttonClearFilter_Click(object sender, EventArgs e)
        {

            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            TextBoxElement TextBoxFilter = this.GetVisualElementById<TextBoxElement>("TextBoxFilter");
            TestedGrid.ClearFilter();

            TextBoxFilter.Text = "...";
        }

        public void TextBox_TextChanged(object sender, EventArgs e)
        {

            TextBoxElement TextBoxFilter = this.GetVisualElementById<TextBoxElement>("TextBoxFilter");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Value to filter is " + TextBoxFilter.Text + "...";

        }

    }
}
