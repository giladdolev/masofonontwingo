using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarItemAddedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void btnAddItem_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            ToolBarElement toolBar1 = this.GetVisualElementById<ToolBarElement>("toolBar1");
            ToolBarButton ToolBarButton1 = new ToolBarButton();
            ToolBarButton1.Text = "ToolBarButton1";

            toolBar1.Items.Add(ToolBarButton1); 
             
        }
        
        public void toolBar1_ItemAdded(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Text += "ItemAdded Event is invoked\\r\\n";

        }
    }
}