using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ListViewSelectionChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {

            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Please make a selection change of an Item...";
        }

        public void TestedListView1_SelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nListView SelectionChanged event is invoked";
            lblLog.Text = "";

            for (int i = 0; i < e.SelectedIndexes.Length; i++ )
            {
                
                lblLog.Text += TestedListView1.Items[e.SelectedIndexes[i]].Text + " is selected \\r\\n";

            }

        }



        public void btnSelectItem3_Click(object sender, EventArgs e)
        {
            ListViewElement TestedListView1 = this.GetVisualElementById<ListViewElement>("TestedListView1");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            TestedListView1.HideSelection = false;
            TestedListView1.SelectedIndexes.Clear();
            //TestedListView1.Items[0].Selected = false;
            TestedListView1.Items[2].Selected = true;
           

        }
        
        public void btnClearEventLog_Click(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");

            txtEventLog.Clear();
            txtEventLog.Text = "Event Log:";
        }
        
    }
}