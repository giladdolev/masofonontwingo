using System;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicTablePanelController: Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
         
        //public void btnRowCol_Click(object sender, EventArgs e)
        private void btnRowCol_Click(object sender, EventArgs e)
        {
            TablePanel tablePanel = this.GetVisualElementById<TablePanel>("tablePanel");
         
            // Add rows and columns
            tablePanel.ColumnCount = 3;
            tablePanel.RowCount = 5;

            //Row and columns style
            tablePanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            tablePanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            tablePanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40F));
            tablePanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 52F));
            tablePanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 44F));
            tablePanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 44F));
            tablePanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 38F));
            tablePanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 8F));

            
            TextBoxElement textBox1 = new TextBoxElement();
            textBox1.Location = new Point(10, 10);
            textBox1.Text = "I am a TextBox";
            textBox1.Size = new Size(200, 30);

            CheckBoxElement checkBox1 = new CheckBoxElement();
            checkBox1.Location = new Point(10, 50);
            checkBox1.Text = "Check Me";
            checkBox1.Size = new Size(200, 30);

           // Add child controls to TablePanel and specify rows and column
            tablePanel.Controls.Add(textBox1, 0, 0);
            tablePanel.Controls.Add(checkBox1, 0, 1);

        }
        

        public void btnChangeBackgroundColor_Click(object sender, EventArgs e)
        {
            TablePanel tablePanel = this.GetVisualElementById<TablePanel>("tablePanel");
            tablePanel.BackColor = Color.Blue;
        }

    }
}