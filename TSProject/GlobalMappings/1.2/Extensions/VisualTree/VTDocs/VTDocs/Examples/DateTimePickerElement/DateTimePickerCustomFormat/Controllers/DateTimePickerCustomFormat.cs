using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Collections.Generic;
using System.Linq;
using MvcApplication9.Models;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class DateTimePickerCustomFormatController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new DateTimePickerCustomFormat());
        }

        private DateTimePickerCustomFormat ViewModel
        {
            get { return this.GetRootVisualElement() as DateTimePickerCustomFormat; }
        }

        private Dictionary<string, object> GetArgsDictionary()
        {
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", "DateTimePickerElement");
            argsDictionary.Add("ExampleName", "DateTimePickerCustomFormat");
            return argsDictionary;
        }

        public void OnLoad(object sender, EventArgs e)
        {
            Dictionary<string, object> argsDictionary = GetArgsDictionary();
            ViewModel.frmTable = VisualElementHelper.CreateFromView<FormatStringTabel>("FormatStringTabel", "Index2", null, argsDictionary, null);

            DateTimePickerElement TestedDateTimePicker1 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker1");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            lblLog1.Text = "Format property: " + TestedDateTimePicker1.Format;
            lblLog1.Text += "\\r\\nCustomFormat property: " + TestedDateTimePicker1.CustomFormat;
            lblLog1.Text += "\\r\\nValue property: " + TestedDateTimePicker1.Value;

            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            lblLog2.Text = "Format property: " + TestedDateTimePicker2.Format;
            lblLog2.Text += "\\r\\nCustomFormat property: " + TestedDateTimePicker2.CustomFormat;
            lblLog2.Text += "\\r\\nValue property: " + TestedDateTimePicker2.Value;

        }

        public void btnFormatStringTable_Click(object sender, EventArgs e)
        {
            ViewModel.frmTable.Show();
        } 


        public void btnChangeCustomFormat_Click(object sender, EventArgs e)
        {
            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            TextBoxElement TextBox = this.GetVisualElementById<TextBoxElement>("TextBox");

            TestedDateTimePicker2.CustomFormat = TextBox.Text;

            lblLog2.Text = "Format property: " + TestedDateTimePicker2.Format;
            lblLog2.Text += "\\r\\nCustomFormat property: " + TestedDateTimePicker2.CustomFormat;
            lblLog2.Text += "\\r\\nValue property: " + TestedDateTimePicker2.Value;
        }

     

        public void TestedDateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            DateTimePickerElement TestedDateTimePicker2 = this.GetVisualElementById<DateTimePickerElement>("TestedDateTimePicker2");

            lblLog2.Text = "Format property: " + TestedDateTimePicker2.Format;
            lblLog2.Text += "\\r\\nCustomFormat property: " + TestedDateTimePicker2.CustomFormat;
            lblLog2.Text += "\\r\\nValue property: " + TestedDateTimePicker2.Value;
            
        }

    }
}