<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox ListItemCollection Add Method (Object)" ID="windowView2" LoadAction="ComboBoxListItemCollectionAddObject\OnLoad">
            
        <vt:Label runat="server" SkinID="Title" Text="Add" Left="200px" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Adds an item to the list of items for a ComboBox.

            Syntax: public int Add(object item)" Top="75px" ID="lblDefinition"></vt:Label>
   
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example pressing the 'Add Item' button will invoke the Items.Add method 
            on this ComboBox control." Top="245px" ID="lblExp2"></vt:Label>

        <vt:ComboBox runat="server" CssClass="vt-test-cmb" Top="320px" ID="TestedComboBox"></vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Width="450px" ID="lblLog" Top="360"></vt:Label>

<%--        <vt:Button runat="server" Text="Remove Item >>" Top="450px" Left="300px" ID="btnRemoveItem" ClickAction="ComboBoxListItemCollectionAddObject\RemoveItems_Click"></vt:Button>--%>

        <vt:Button runat="server" Text="Add Item >>" Top="450px"  ID="btnAddItem" ClickAction="ComboBoxListItemCollectionAddObject\AddItems_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

