using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CheckBoxAutoCheckController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox1 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox1");
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");


            lblLog1.Text = "AutoCheck Value: " + TestedCheckBox1.AutoCheck;
            lblLog2.Text = "AutoCheck Value: " + TestedCheckBox2.AutoCheck + '.';


        }

        public void btnChangeAutoCheck_Click(object sender, EventArgs e)
        {
            CheckBoxElement TestedCheckBox2 = this.GetVisualElementById<CheckBoxElement>("TestedCheckBox2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedCheckBox2.AutoCheck)
            {
                TestedCheckBox2.AutoCheck = false;
                lblLog2.Text = "AutoCheck Value: " + TestedCheckBox2.AutoCheck + '.';
            }
            else
            {
                TestedCheckBox2.AutoCheck = true;
                lblLog2.Text = "AutoCheck Value: " + TestedCheckBox2.AutoCheck + '.';
            }

        }
    }
}