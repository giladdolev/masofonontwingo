using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPaddingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //PanelAlignment
        public void btnChangePadding_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            PanelElement pnlRuntimePadding = this.GetVisualElementById<PanelElement>("pnlRuntimePadding");

            if (pnlRuntimePadding.Padding == new Padding(0, 0, 0, 0)) //Get
            {
                pnlRuntimePadding.Padding = new Padding(0, 0, 0, 50); //Set
                textBox1.Text = "The padding property value is: " + pnlRuntimePadding.Padding;
            }
            else if (pnlRuntimePadding.Padding == new Padding(0, 0, 0, 50))
            {
                pnlRuntimePadding.Padding = new Padding(0, 0, 50, 0); 
                textBox1.Text = "The padding property value is: " + pnlRuntimePadding.Padding;
            }
            else if (pnlRuntimePadding.Padding == new Padding(0, 0, 50, 0)) 
            {
                pnlRuntimePadding.Padding = new Padding(0, 50, 0, 0); 
                textBox1.Text = "The padding property value is: " + pnlRuntimePadding.Padding;
            }
            else if (pnlRuntimePadding.Padding == new Padding(0, 50, 0, 0)) 
            {
                pnlRuntimePadding.Padding = new Padding(50, 0, 0, 0);
                textBox1.Text = "The padding property value is: " + pnlRuntimePadding.Padding;
            }
            else 
            {
                pnlRuntimePadding.Padding = new Padding(0, 0, 0, 0); 
                textBox1.Text = "The padding property value is: " + pnlRuntimePadding.Padding;
            }       
        }

    }
}