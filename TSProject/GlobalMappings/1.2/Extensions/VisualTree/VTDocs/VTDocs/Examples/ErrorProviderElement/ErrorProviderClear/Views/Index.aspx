
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ErrorProvider Clear Method" ID="windowView1" LoadAction="ErrorProviderClear\OnLoad">
      
       
        <vt:Label runat="server" SkinID="Title" Text="Clear" ID="lblTitle" left="320px">
        </vt:Label>

        <vt:Label runat="server" Text="Clears all settings associated with this component.

            Syntax:  public void Clear()
            
            Calling this method clears all property settings for this ErrorProvider, restoring the properties to
            their default values."
            Top="75px" ID="lblDefinition">
        </vt:Label>


        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="270px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example there are 2 textboxes in a simple login dialog.
            When one of the text boxes is left empty, the SetError method is invoked.
            Clicking on one of the 'Clear' buttons will invoke the Clear method on the associated textbox. In this 
            case, the error icon and the error ToolTip will be removed." Top="310px" ID="lblExp2">
        </vt:Label>

        <vt:panel runat="server" Text="" Top="410px" ID="panel1" Width="320px" Height="150">
        <vt:label runat="server" Text="Username:" Top="30px" Left="35px" ID="lblUsername"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-username" Top="30px" Left="130px" ID="txtUsername" LeaveAction="ErrorProviderClear\txtUsername_leave"></vt:textbox>

        <vt:label runat="server" Text="Password:" Top="60px" Left="35px" ID="lblPassword"></vt:label>
        <vt:textbox runat="server" CssClass="vt-txt-password" Top="60px" Left="130px" ID="txtPassword" LeaveAction="ErrorProviderClear\txtPassword_leave"></vt:textbox>

        <vt:Button runat="server" Text="Login" Width="100px" Height="30px" Top="100px" Left="110px" ID="btnLogin" ClickAction="ErrorProviderClear\btnLogin_Click"></vt:Button>
            </vt:panel>

        <vt:Label runat="server" SkinID="Log" ID="lblLog" Top="570" Height="40px" Width="320px"></vt:Label>

        <vt:Button runat="server" Text="Clear Username ErrorProvider >>" Width="190px" SkinID="Wide" Top="410px" Left="430px" ID="btnClearU" ClickAction="ErrorProviderClear\btnClearU_Click"></vt:Button>
        <vt:Button runat="server" Text="Clear Password ErrorProvider >>" Width="190px" SkinID="Wide" Top="460px" Left="430px" ID="btnClearP" ClickAction="ErrorProviderClear\btnClearP_Click"></vt:Button>


       
    </vt:WindowView>
    <vt:ComponentManager runat="server" >
		<vt:ErrorProvider runat="server" ID="epUsername" ></vt:ErrorProvider>
	    <vt:ErrorProvider runat="server" ID="epPassword" ></vt:ErrorProvider>
	</vt:ComponentManager>s
</asp:Content>

