using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxPerformSizeChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformSizeChanged_Click(object sender, EventArgs e)
        {
            
            TextBoxElement testedTextBox = this.GetVisualElementById<TextBoxElement>("testedTextBox");
            
            MouseEventArgs args = new MouseEventArgs();

            testedTextBox.PerformSizeChanged(args);
        }

        public void testedTextBox_SizeChanged(object sender, EventArgs e)
        {
            MessageBox.Show("TestedTextBox SizeChanged event method is invoked");
        }

    }
}