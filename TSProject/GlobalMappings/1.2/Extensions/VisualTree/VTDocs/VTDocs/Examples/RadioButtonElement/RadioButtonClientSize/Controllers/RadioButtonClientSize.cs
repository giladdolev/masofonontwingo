using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RadioButtonClientSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "ClientSize value: " + TestedRadioButton.ClientSize;

        }
        public void btnChangeRadioButtonClientSize_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RadioButtonElement TestedRadioButton = this.GetVisualElementById<RadioButtonElement>("TestedRadioButton");
            if (TestedRadioButton.Width == 200)
            {
                TestedRadioButton.ClientSize = new Size(150, 26);
                lblLog.Text = "ClientSize value: " + TestedRadioButton.ClientSize;

            }
            else
            {
                TestedRadioButton.ClientSize = new Size(200, 45);
                lblLog.Text = "ClientSize value: " + TestedRadioButton.ClientSize;
            }

        }

    }
}