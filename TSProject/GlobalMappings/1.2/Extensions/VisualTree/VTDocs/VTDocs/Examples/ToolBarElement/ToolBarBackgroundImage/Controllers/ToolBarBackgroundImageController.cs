using MvcApplication9.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarBackgroundImageController : Controller
    {

        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedToolBar1.BackgroundImage != null)
                lblLog1.Text = "Set with background image";
            else
                lblLog1.Text = "No background image";

            if (TestedToolBar2.BackgroundImage != null)
                lblLog2.Text = "Set with background image.";
            else
                lblLog2.Text = "No background image.";
            
        }


        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
		{
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");

            if (TestedToolBar2.BackgroundImage == null)
			{
                TestedToolBar2.BackgroundImage = new UrlReference(@"Content/Images/Icon.jpg");
                lblLog2.Text = "Set with background image.";
			}
			else
            {
                TestedToolBar2.BackgroundImage = null;
                lblLog2.Text = "No background image.";
            }
                
		}

    }
}