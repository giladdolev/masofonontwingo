<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox PixelHeight Property" ID="windowView1" LoadAction="CheckBoxPixelHeight\OnLoad">
        
        <vt:Label runat="server" SkinID="Title" Text="PixelHeight" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the height of the control in pixels.
            
            Syntax: public int PixelHeight { get; set; }" Top="75px" ID="lblDefinition"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="175px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="Height property of this CheckBox is initially set to: 80px" Top="225px" ID="lblExp1"></vt:Label>

        <vt:CheckBox runat="server" SkinID="BackColorCheckBox" Text="TestedCheckBox" Top="285px" Height="80px" ID="TestedCheckBox" TabIndex="2"></vt:CheckBox>    

        <vt:Label runat="server" SkinID="Log" Top="380px" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change PixelHeight value >>" Top="445px" ID="btnChangePixelHeight" TabIndex="3" Width="180px" ClickAction="CheckBoxPixelHeight\btnChangePixelHeight_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
