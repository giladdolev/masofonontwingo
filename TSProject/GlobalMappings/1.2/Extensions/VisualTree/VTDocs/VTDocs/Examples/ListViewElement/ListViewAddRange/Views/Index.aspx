<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ListView Add Range" Top="57px" Left="11px" ID="windowView1" Height="400px" Width="400px" LoadAction="ListViewAddRange/Form_Load">

        <vt:ListView runat="server" ID="listView2" Top="0" Left="0px" Height="200px" Width="300px" >
        </vt:ListView>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Add Range" Top="60px" Left="300px" ID="btnDock" Height="25px" TabIndex="1" Width="100px" ClickAction="ListViewAddRange\btnAddRange_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
