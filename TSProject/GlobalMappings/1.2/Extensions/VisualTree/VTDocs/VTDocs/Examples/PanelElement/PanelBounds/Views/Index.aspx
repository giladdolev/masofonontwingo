<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Bounds Property" ID="windowView" LoadAction="PanelBounds\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Bounds" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size and location of the control including its nonclient elements, 
            in pixels, relative to the parent control.

            Syntax: public Rectangle Bounds { get; set; }
            
            The bounds of the control include the nonclient elements such as scroll bars, 
            borders, title bars, and menus." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="250px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="The bounds of this 'TestedPanel' is initially set with 
            Left: 80px, Top: 360px, Width: 200px, Height: 80px" Top="290px"  ID="lblExp1"></vt:Label>     

        <!-- TestedPanel -->
        <vt:Panel runat="server" Text="TestedPanel" Top="360px" ID="TestedPanel"></vt:Panel>

        <vt:Label runat="server" SkinID="Log" Top="450px" Height="40" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change Panel Bounds >>"  Top="540px" Width="180px" ID="btnChangeBounds" ClickAction="PanelBounds\btnChangeBounds_Click"></vt:Button>

              

    </vt:WindowView>
</asp:Content>
