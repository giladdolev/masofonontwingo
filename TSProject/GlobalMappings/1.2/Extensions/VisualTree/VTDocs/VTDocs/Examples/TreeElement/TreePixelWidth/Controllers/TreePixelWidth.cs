using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TreePixelWidthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void OnLoad(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelWidth value: " + TestedTree.PixelWidth + '.';
        }

        private void btnChangePixelWidth_Click(object sender, EventArgs e)
        {
            TreeElement TestedTree = this.GetVisualElementById<TreeElement>("TestedTree");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedTree.PixelWidth == 140)
            {
                TestedTree.PixelWidth = 300;
                lblLog.Text = "PixelWidth value: " + TestedTree.PixelWidth + '.';
            }
            else
            {
                TestedTree.PixelWidth = 140;
                lblLog.Text = "PixelWidth value: " + TestedTree.PixelWidth + '.';
            }
        }
      
    }
}