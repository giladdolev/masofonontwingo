using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxTextController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Text  property value: " + TestedRichTextBox.Text;

        }

        public void TestedRichTextBox_TextChanged(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.Text != "")
            {
            lblLog.Text = "Text property value: " + TestedRichTextBox.Text;
            }
            else
            {
                lblLog.Text = "RichTextBox is Empty";
            }
        }

        public void btnAddText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");

            TestedRichTextBox.Text += "New String";
        }


        public void btnChangeRichTextBoxText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            if (TestedRichTextBox.Text == "TestedRichTextBox")
            {
                TestedRichTextBox.Text = "New Text";

            }
            else
            {
                TestedRichTextBox.Text = "TestedRichTextBox";
             
            }
        }

        public void btnChangeRichTextBoxText2_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            //if (TestedRichTextBox.Text == "TestedRichTextBox")
            //{
                TestedRichTextBox.Text = "New Text \\r\\nSecondLine";
              
            //}
            //else
            //{
            //    TestedRichTextBox.Text = "TestedRichTextBox \\r\\nSecondLine";
             
            //}
        }


        public void btnClearText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");

            TestedRichTextBox.Text = "";
            lblLog.Text = "RichTextBox is Empty";

        }
        
    }
}