using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownResizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            NumericUpDownElement testedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("testedNumericUpDown");


            if (testedNumericUpDown.Size == new Size(100, 20))
            {
                testedNumericUpDown.Size = new Size(300, 20);
                testedNumericUpDown.Text = "Set to Height: " + testedNumericUpDown.Height + ", Width: " + testedNumericUpDown.Width;
            }
            else
            {
                testedNumericUpDown.Size = new Size(100, 20);
                testedNumericUpDown.Text = "Back to Height: " + testedNumericUpDown.Height + ", Width: " + testedNumericUpDown.Width;
            }
        }

        private void testedNumericUpDown_Resize(object sender, EventArgs e)
        {
            TextBoxElement txtEventTrack = this.GetVisualElementById<TextBoxElement>("txtEventTrack");
            txtEventTrack.Text += "NumericUpDown Resize event is invoked\\r\\n";
        }
      
    }
}