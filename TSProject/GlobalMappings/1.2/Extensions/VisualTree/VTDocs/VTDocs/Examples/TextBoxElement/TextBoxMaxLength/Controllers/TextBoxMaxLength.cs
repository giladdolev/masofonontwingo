using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class TextBoxMaxLengthController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void Load(object sender, EventArgs e)
        {
            TextBoxElement txtMaxLength = this.GetVisualElementById<TextBoxElement>("txtMaxLength");
            LabelElement Label2 = this.GetVisualElementById<LabelElement>("Label2");
            Label2.Text += txtMaxLength.MaxLength.ToString();
        }

        public void btnSetMaxLength_Click(object sender, EventArgs e)
        {
            TextBoxElement txtMaxLength = this.GetVisualElementById<TextBoxElement>("txtMaxLength");
            if (txtMaxLength.MaxLength == 32767)
                txtMaxLength.MaxLength = 3;
            else
                txtMaxLength.MaxLength = 32767;
        }
        public void btnGetMaxLength_Click(object sender, EventArgs e)
        {
            TextBoxElement txtMaxLength = this.GetVisualElementById<TextBoxElement>("txtMaxLength");
            MessageBox.Show("MaxLength value: " + txtMaxLength.MaxLength.ToString());
        }


    }
}