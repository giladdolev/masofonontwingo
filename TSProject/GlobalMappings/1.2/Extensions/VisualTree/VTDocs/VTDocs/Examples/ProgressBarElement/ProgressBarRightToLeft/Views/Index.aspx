<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ProgressBar RightToLeft Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="Initialize - ProgressBar RightToLeft is set to'yes'" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

     
        <vt:ProgressBar runat="server"  Text="" RightToLeft="Yes" Value ="10" Top="45px" Left="140px" ID="prgRightToLeft" Height="36px" TabIndex="2" Width="220px"></vt:ProgressBar>
      

         <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>
     
        <vt:ProgressBar runat="server"  Text="" Top="115px" Value ="10" Left="140px" ID="testedProgressBar" Height="36px"  TabIndex="3" Width="220px"></vt:ProgressBar>

         <vt:button runat="server"  Text="Change ProgressBar RightToLeft" Top="115px" Left="480px" ID="btnChangeRightToLeft" Height="36px"  TabIndex="3" Width="200px" ClickAction="ProgressBarRightToLeft\btnChangeRightToLeft_Click" ></vt:button>

        <vt:TextBox runat="server" Text="" Top="200px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
