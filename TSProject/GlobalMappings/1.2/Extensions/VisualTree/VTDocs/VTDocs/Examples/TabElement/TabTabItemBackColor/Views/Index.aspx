<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab TabItem BackColor Property" ID="windowView1" LoadAction="TabTabItemBackColor\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="BackColor" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the background color for the TabItem.
            
            Syntax: public override Color BackColor { get; set; }" Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Tab runat="server" Text="Tab" Top="150px" ID="TestedTab1">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab1Item1" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab1Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px" ID="Tab1Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="260px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="325px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="In the following example, BackColor property of 'tabPage1' is initially set to Gray." Top="375px"  ID="lblExp1"></vt:Label>     
        
        <vt:Tab runat="server" Text="TestedTab" Top="415px" ID="TestedTab2" TabReference="">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="Tab2Item1" BackColor ="Gray" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="Tab2Item2" Height="74px" Width="192px"></vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage3" Top="22px" Left="4px"  ID="Tab2Item3" Height="74px" Width="192px"></vt:TabItem>
            </TabItems>
        </vt:Tab>
        <vt:Label runat="server" SkinID="Log" Top="530px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change BackColor value >>" Width="180" Top="595px" ID="btnChangeBackColor" ClickAction="TabTabItemBackColor\btnChangeBackColor_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>

