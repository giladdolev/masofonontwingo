<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button Click event" ID="windowView2" LoadAction="ButtonClick\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="Click" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the control is clicked.

            Syntax: public event EventHandler Click"
            Top="75px" ID="lblDefinition">
        </vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, each time one of the Buttons is clicked it's background color is changed.
            'Click' action can be invoked by one of the following ways:
            1. Clicking one of the Buttons using the mouse.
            2. Focus one of the Buttons and pressing the 'Enter' key.
            3. Clicking 'Perform Click Button1' Button to click 'Button1'.
             Each invocation of the 'Click' event should add a line in the 'Event Log'."
            Top="235px" ID="lblExp2">
        </vt:Label>
        <vt:GroupBox runat="server" Top="365px" Height="50px" Width="505px">
            <vt:Button runat="server" Top="15px" Left="15px" Text="Button1" ID="btnButton1" TabIndex="1" ClickAction="ButtonClick\btnButton1_Click"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="175px" Text="Button2" ID="btnButton2" TabIndex="2" ClickAction="ButtonClick\btnButton2_Click"></vt:Button>
            <vt:Button runat="server" Top="15px" Left="335px" Text="Button3" ID="btnButton3" TabIndex="3" ClickAction="ButtonClick\btnButton3_Click"></vt:Button>
        </vt:GroupBox>

        <vt:Label runat="server" SkinID="Log" Width="360px" ID="lblLog" Height="40px" Top="430px"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="485px" Width="360px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Perform Click Button1 >>" Width="180px" Top="615px" TabIndex="4" ID="btnClickButton1" ClickAction="ButtonClick\btnClickButton1_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
