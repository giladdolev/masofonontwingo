using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerPerformEnterController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnPerformEnter_Click(object sender, EventArgs e)
        {
            
            SplitContainer testedSplitContainer = this.GetVisualElementById<SplitContainer>("testedSplitContainer");

            testedSplitContainer.PerformEnter(e);
        }

        public void testedSplitContainer_Enter(object sender, EventArgs e)
        {
            MessageBox.Show("TestedSplitContainer Enter event method is invoked");
        }

    }
}