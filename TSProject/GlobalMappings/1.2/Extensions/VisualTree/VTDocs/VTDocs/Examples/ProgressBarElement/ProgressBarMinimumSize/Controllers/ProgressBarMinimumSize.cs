using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ProgressBarMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void OnLoad(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            lblLog1.Text = "ProgressBar MinimumSize value: " + TestedProgressBar.MinimumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
        }

        public void btnChangePrgMinimumSize_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            if (TestedProgressBar.MinimumSize == new Size(250, 50))
            {
                TestedProgressBar.MinimumSize = new Size(200, 100);
                lblLog1.Text = "ProgressBar MinimumSize value: " + TestedProgressBar.MinimumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
            }
            else if (TestedProgressBar.MinimumSize == new Size(200, 100))
            {
                TestedProgressBar.MinimumSize = new Size(160, 90);
                lblLog1.Text = "ProgressBar MinimumSize value: " + TestedProgressBar.MinimumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
            }
            else
            {
                TestedProgressBar.MinimumSize = new Size(250, 50);
                lblLog1.Text = "ProgressBar MinimumSize value: " + TestedProgressBar.MinimumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;
            }

        }

        public void btnSetToZeroPrgMinimumSize_Click(object sender, EventArgs e)
        {
            ProgressBarElement TestedProgressBar = this.GetVisualElementById<ProgressBarElement>("TestedProgressBar");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");

            TestedProgressBar.MinimumSize = new Size(0, 0);
            lblLog1.Text = "ProgressBar MinimumSize value: " + TestedProgressBar.MinimumSize + "\\r\\nProgressBar Size value: " + TestedProgressBar.Size;

        }


    }
}