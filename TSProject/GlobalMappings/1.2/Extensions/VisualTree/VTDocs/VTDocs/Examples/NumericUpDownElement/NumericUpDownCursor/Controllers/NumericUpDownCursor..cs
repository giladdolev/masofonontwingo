using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownCursorController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnChangeCursor_Click(object sender, EventArgs e)
        {
            NumericUpDownElement nudRuntimeCursor = this.GetVisualElementById<NumericUpDownElement>("nudRuntimeCursor");

            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (nudRuntimeCursor.Cursor == CursorsElement.Default)
            {
                nudRuntimeCursor.Cursor = CursorsElement.AppStarting;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.AppStarting)
            {
                nudRuntimeCursor.Cursor = CursorsElement.Cross;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.Cross)
            {
                nudRuntimeCursor.Cursor = CursorsElement.Hand;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.Hand)
            {
                nudRuntimeCursor.Cursor = CursorsElement.Help;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.Help)
            {
                nudRuntimeCursor.Cursor = CursorsElement.HSplit;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.HSplit)
            {
                nudRuntimeCursor.Cursor = CursorsElement.IBeam;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.IBeam)
            {
                nudRuntimeCursor.Cursor = CursorsElement.No;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.No)
            {
                nudRuntimeCursor.Cursor = CursorsElement.NoMove2D;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.NoMove2D)
            {
                nudRuntimeCursor.Cursor = CursorsElement.NoMoveHoriz;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.NoMoveHoriz)
            {
                nudRuntimeCursor.Cursor = CursorsElement.NoMoveVert;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.NoMoveVert)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanEast;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanEast)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanNE;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanNE)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanNorth;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;

            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanNorth)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanNW;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanNW)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanSE;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanSE)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanSouth;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanSouth)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanSW;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanSW)
            {
                nudRuntimeCursor.Cursor = CursorsElement.PanWest;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.PanWest)
            {
                nudRuntimeCursor.Cursor = CursorsElement.SizeAll;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.SizeAll)
            {
                nudRuntimeCursor.Cursor = CursorsElement.SizeNESW;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.SizeNESW)
            {
                nudRuntimeCursor.Cursor = CursorsElement.SizeNS;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.SizeNS)
            {
                nudRuntimeCursor.Cursor = CursorsElement.SizeNWSE;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.SizeNWSE)
            {
                nudRuntimeCursor.Cursor = CursorsElement.SizeWE;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.SizeWE)
            {
                nudRuntimeCursor.Cursor = CursorsElement.UpArrow;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.UpArrow)
            {
                nudRuntimeCursor.Cursor = CursorsElement.VSplit;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else if (nudRuntimeCursor.Cursor == CursorsElement.VSplit)
            {
                nudRuntimeCursor.Cursor = CursorsElement.WaitCursor;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
            else
            {
                nudRuntimeCursor.Cursor = CursorsElement.Default;
                textBox1.Text = "Cursoe value: " + nudRuntimeCursor.Cursor;
            }
        }

    }
}