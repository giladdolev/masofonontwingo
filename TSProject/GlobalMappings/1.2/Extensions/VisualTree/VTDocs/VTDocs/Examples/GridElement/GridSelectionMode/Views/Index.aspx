﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectionMode Property" ID="windowView1" LoadAction="GridSelectionMode\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectionMode" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating how the cells of the DataGridView can be selected.
            
            public CellSelectionMode SelectionMode { get; set; }
            Value: One of the CellSelectionMode values. The default is 'Cell'." Top="75px"  ID="lblDefinition"></vt:Label>
              
        <vt:Grid runat="server" Top="170px" ID="TestedGrid1" >
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="300px" ID="lblLog1"></vt:Label>
 
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="355px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="SelectionMode property of this Grid is initially set to 'Row'." Top="385px"  ID="lblExp1"></vt:Label>     
        
        <vt:Grid runat="server" Width="270px" Top="440px" SelectionMode="Row" ID="TestedGrid2" >
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>
        <vt:Label runat="server" SkinID="Log" Top="570px" ID="lblLog2"></vt:Label>
        <vt:Button runat="server" Text="Change SelectionMode value >>" Width="180px" Top="635px" ID="btnChangeSelectionMode" ClickAction="GridSelectionMode\btnChangeSelectionMode_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
