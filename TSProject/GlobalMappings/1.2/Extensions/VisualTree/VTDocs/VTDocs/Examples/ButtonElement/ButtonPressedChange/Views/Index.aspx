
<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Button PressedChange event" ID="windowView" LoadAction="ButtonPressedChange\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="PressedChange" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the 'IsPressed' property changes it's state.
            
             Syntax: public event EventHandler<ButtonPressedChangeEventArgs> PressedChange 
            If 'EnableToggle' property is set to true the 'IsPressed' property becomes relevant.
            
            When IsPressed property is set to true the button's color is dark blue" Top="75px" ID="Label2" ></vt:Label>
               
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="190px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="In the following example, you can change the 'IsPressed' property by clicking the 'TestedButton'.
            (the 'EnableToggle' property value is set to true) 
            Each invocation of the 'PressedChange' event will add a line to the 'Event Log'." Top="250px"  ID="lblExp1"></vt:Label>     
        
        <vt:Button runat="server" SkinID="Wide" EnableToggle="true" Text="TestedButton" Top="340px" ID="btnTestedButton" PressedChangeAction="ButtonPressedChange\btnTestedButton_PressedChange"></vt:Button>

        <vt:Label runat="server" SkinID="Log" Top="390px" Width="350" ID="lblLog"></vt:Label>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="430px" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" Text="Change IsPressed Value >>"  Top="590px" Width="180px" ID="btnChangeButtonIsPressed" ClickAction="ButtonPressedChange\btnChangeButtonIsPressed_Click"></vt:Button>
             
    </vt:WindowView>
</asp:Content>




