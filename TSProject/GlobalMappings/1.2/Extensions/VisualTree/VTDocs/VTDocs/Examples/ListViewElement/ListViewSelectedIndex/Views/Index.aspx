<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic ListView" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px" LoadAction="ListViewSelectedIndex/Form_Load">
        <vt:ListView runat="server" ID="listView1" Height="200px" Width="300px">
            <Columns>
                <vt:ColumnHeader runat="server" Text="h1" Width="100"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="h2" Width="200"></vt:ColumnHeader>
                <vt:ColumnHeader runat="server" Text="h3" width="200"></vt:ColumnHeader>
            </Columns>
            <Items>
                <vt:ListViewItem runat="server" Text="aaaa">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="11111"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="22222"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
                <vt:ListViewItem runat="server" Text="bbbbb">
                    <Subitems>
                        <vt:ListViewSubitem runat="server" Text="33333"></vt:ListViewSubitem>
                        <vt:ListViewSubitem runat="server" Text="44444"></vt:ListViewSubitem>
                    </Subitems>
                </vt:ListViewItem>
            </Items>
        </vt:ListView>
        <vt:Button runat="server" TextAlign="MiddleCenter" Text="Show SelectedIndex Value" Top="96px" Left="400px" ID="Command1" Height="33px" TabIndex="1" Width="300px" ClickAction="ListViewSelectedIndex\Command1_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
