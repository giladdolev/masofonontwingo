using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class NumericUpDownRightToLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        // Change RightToLeft property
        /// <summary>
        /// Handles the Click event of the NumericUpDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnChangeRightToLeft_Click(object sender, EventArgs e)
        {
            NumericUpDownElement testedNumericUpDown = this.GetVisualElementById<NumericUpDownElement>("testedNumericUpDown");
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");


            if (testedNumericUpDown.RightToLeft == RightToLeft.No)
            {
                testedNumericUpDown.RightToLeft = RightToLeft.Yes;
                textBox1.Text = "testedNumericUpDown RightToLeft value:\\r\\n" + testedNumericUpDown.RightToLeft;

            }
            else if (testedNumericUpDown.RightToLeft == RightToLeft.Yes)
            {
                testedNumericUpDown.RightToLeft = RightToLeft.Inherit;
                textBox1.Text = "RightToLeft value:\\r\\n" + testedNumericUpDown.RightToLeft;

            }
            else 
            {
                testedNumericUpDown.RightToLeft = RightToLeft.No;
                textBox1.Text = "testedNumericUpDown RightToLeft value:\\r\\n" + testedNumericUpDown.RightToLeft;

            }
        }

    }
}