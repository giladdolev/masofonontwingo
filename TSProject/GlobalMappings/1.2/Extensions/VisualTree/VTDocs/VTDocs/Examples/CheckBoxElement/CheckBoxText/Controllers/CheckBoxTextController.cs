﻿using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;


namespace MvcApplication9.Controllers
{
    public class CheckBoxTextController : Controller
    {
        // GET: CheckBoxText
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            CheckBoxElement btnTestedCheckBox = this.GetVisualElementById<CheckBoxElement>("btnTestedCheckBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Text  property value: " + btnTestedCheckBox.Text;

        }

        public void btnChangeCheckBoxText_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            CheckBoxElement btnTestedCheckBox = this.GetVisualElementById<CheckBoxElement>("btnTestedCheckBox");
            if (btnTestedCheckBox.Text == "TestedCheckBox")
            {
                btnTestedCheckBox.Text = "New Text";
                lblLog.Text = "Text  property value: " + btnTestedCheckBox.Text;

            }
            else
            {
                btnTestedCheckBox.Text = "TestedCheckBox";
                lblLog.Text = "Text  property value: " + btnTestedCheckBox.Text;
            }

        }
    }
}