<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ComboBox MaximumSize Property" ID="windowView" LoadAction="ComboBoxMaximumSize\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MaximumSize" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets the size that is the upper limit - the size of a rectangular 
            area into which a control can be fitted.

            Syntax: public virtual Size MaximumSize { get; set; }
            
            Value: An ordered pair of type System.Drawing.Size representing the width and height of a rectangle." Top="75px" ID="Label3" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="240px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="The defined Size of this 'TestedComboBox' is width: 180px and height: 70px. 
            Its MaximumSize is initially set to width: 150px and height: 40px.
            To cancel 'TestedComboBox' MaximumSize, set width and height values to 0px." Top="280px"  ID="lblExp1"></vt:Label>     

         <vt:ComboBox runat="server" Text="TestedComboBox" ID="TestedComboBox" Top="370" Width="180px" Height="70px" MaximumSize="150, 40">
            <Items>
                <vt:ListItem runat="server" Text="aa"></vt:ListItem>
                <vt:ListItem runat="server" Text="bb"></vt:ListItem>
                <vt:ListItem runat="server" Text="cc"></vt:ListItem>
                <vt:ListItem runat="server" Text="dd"></vt:ListItem>
            </Items>
        </vt:ComboBox>

        <vt:Label runat="server" SkinID="Log" Top="450px" ID="lblLog" Width="400px" Height="40px"></vt:Label>

        <vt:Button runat="server" Text="Change MaximumSize value >>"  Top="560px" Width="200px" ID="btnChangeCmbMaximumSize" ClickAction="ComboBoxMaximumSize\btnChangeCmbMaximumSize_Click"></vt:Button>
        
        <vt:Button runat="server" Text="Set MaximumSize to (0, 0) >>" Top="560px" width="200px" Left="310px" ID="btnSetToZeroCmbMaximumSize" ClickAction="ComboBoxMaximumSize\btnSetToZeroCmbMaximumSize_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>








