<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="CheckBox TabStop and TabIndex Properties" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        
        <vt:Label runat="server" Text="CheckBox3 TabStop is set to false, CheckBoxes TabIndex are set from left to right" Top="30px" Left="100px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="200px"></vt:Label>

        <vt:CheckBox runat="server" Text="CheckBox1" Top="60px" Left="40px" ID="CheckBox1" Height="36px" TabIndex="1" Width="100px"></vt:CheckBox>    

        <vt:CheckBox runat="server" Text="CheckBox2" Top="60px" Left="150px" ID="CheckBox2" Height="36px" TabIndex="2"  Width="100px"></vt:CheckBox>

        <vt:CheckBox runat="server" Text="CheckBox3" Top="60px" Left="260px" ID="CheckBox3" Height="36px" TabIndex="3" TabStop="false" Width="100px"></vt:CheckBox>    

        <vt:CheckBox runat="server" Text="CheckBox4" Top="60px" Left="370px" ID="CheckBox4" Height="36px"  TabIndex="4" Width="100px"></vt:CheckBox>

        <vt:CheckBox runat="server" Text="CheckBox5" Top="60px" Left="480px" ID="CheckBox5" Height="36px" TabIndex="5" Width="100px"></vt:CheckBox> 
        
         <vt:button runat="server" Text="Change CheckBoxs TabIndex" Top="110px" Left="130px" TabStop ="false" ID="btnChangeTabIndex" Height="36px" Width="170px" ClickAction="CheckBoxTabIndexTabStop\btnChangeTabIndex_Click"></vt:button> 
        
         <vt:button runat="server" Text="Change CheckBox3 TabStop" Top="110px" Left="340px" ID="btnChangeTabStop" Height="36px"  TabStop ="false" Width="170px" ClickAction="CheckBoxTabIndexTabStop\btnChangeTabStop_Click"></vt:button>    


        <vt:TextBox runat="server" Text="" Top="200px" Left="130px"  ID="textBox1" Multiline="true"  Height="80px" Width="170px"> 
            </vt:TextBox>

        <vt:TextBox runat="server" Text="" Top="200px" Left="340px"  ID="textBox2" Multiline="true"  Height="80px" Width="170px"> 
            </vt:TextBox>

     </vt:WindowView>
</asp:Content>
