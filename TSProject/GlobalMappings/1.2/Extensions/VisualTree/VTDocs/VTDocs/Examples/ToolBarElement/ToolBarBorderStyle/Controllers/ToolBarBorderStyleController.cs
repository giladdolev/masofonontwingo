using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarBorderStyleController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar1 = this.GetVisualElementById<ToolBarElement>("TestedToolBar1");
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            lblLog1.Text = "BorderStyle value: " + TestedToolBar1.BorderStyle;
            lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
        }
        
        public void btnChangeToolBarBorderStyle_Click(object sender, EventArgs e)
        {
            ToolBarElement TestedToolBar2 = this.GetVisualElementById<ToolBarElement>("TestedToolBar2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedToolBar2.BorderStyle == BorderStyle.None)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Dotted;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Dotted)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Double;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Double)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Fixed3D;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Fixed3D)
            {
                TestedToolBar2.BorderStyle = BorderStyle.FixedSingle;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.FixedSingle)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Groove;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Groove)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Inset;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Inset)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Dashed;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Dashed)
            {
                TestedToolBar2.BorderStyle = BorderStyle.NotSet;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.NotSet)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Outset;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Outset)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Ridge;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Ridge)
            {
                TestedToolBar2.BorderStyle = BorderStyle.ShadowBox;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.ShadowBox)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Solid;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else if (TestedToolBar2.BorderStyle == BorderStyle.Solid)
            {
                TestedToolBar2.BorderStyle = BorderStyle.Underline;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
            else
            {
                TestedToolBar2.BorderStyle = BorderStyle.None;
                lblLog2.Text = "BorderStyle value: " + TestedToolBar2.BorderStyle + '.';
            }
        }
    }
}