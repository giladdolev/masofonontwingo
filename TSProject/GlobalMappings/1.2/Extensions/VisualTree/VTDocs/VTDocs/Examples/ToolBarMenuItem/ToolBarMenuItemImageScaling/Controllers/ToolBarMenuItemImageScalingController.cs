using MvcApplication9.Models;
using System;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ToolBarMenuItemImageScalingController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 

        public ActionResult Index()
        {
            return View(new ToolBarMenuItemImageScaling());
        }
        private ToolBarMenuItemImageScaling ViewModel
        {
            get { return this.GetRootVisualElement() as ToolBarMenuItemImageScaling; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Add ToolBarMenuItem ...";
        }

        public void AddNewToolBarItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            MenuElement TestedMenu = this.GetVisualElementById<MenuElement>("TestedMenu");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboBoxElement cboMenuItemConstructors = this.GetVisualElementById<ComboBoxElement>("cboMenuItemConstructors");

            switch (cboMenuItemConstructors.SelectedIndex)
            {
                case 0:
                    ToolBarMenuItem defaultsCtorToolBarMenuItem = new ToolBarMenuItem();
                    TestedMenu.Items.Add(defaultsCtorToolBarMenuItem); lblLog.Text = "ToolBarMenuItem() was invoked";
                    break;
                case 1:
                    ToolBarMenuItem StringParamCtorToolBarMenuItem = new ToolBarMenuItem("Menu" + this.ViewModel.MenuItemsIndex++);
                    TestedMenu.Items.Add(StringParamCtorToolBarMenuItem);
                    lblLog.Text = "ToolBarMenuItem(String) was invoked"; break;
                case 2:
                    ToolBarMenuItem FourParamsCtorToolBarMenuItem = new ToolBarMenuItem("Menu" + ViewModel.MenuItemsIndex++, new ResourceReference(@"Content\Images\icon.jpg"), null, Keys.F10);
                    lblLog.Text = "ToolBarMenuItem(String, ResourceReference, EventHandler, Keys) was invoked"; TestedMenu.Items.Add(FourParamsCtorToolBarMenuItem);
                    break;
                default:
                    lblLog.Text = "Add ToolBarMenuItem ...";
                    break;
            }
        }

        //
        public void FourParamsCtorToolBarMenuItem_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Menu" + (ViewModel.MenuItemsIndex - 1) + " Click event was invoked";
        }


        public void btnImageScaling_Click(object sender, EventArgs e)
        {
            ToolBarMenuItem tbmi = this.GetVisualElementById<ToolBarMenuItem>("tbmi1");
            ToolBarMenuItem tbmi2 = this.GetVisualElementById<ToolBarMenuItem>("tbmi2");
            ToolBarMenuItem tbmi3 = this.GetVisualElementById<ToolBarMenuItem>("tbmi3");
            ToolBarMenuItem tbmi4 = this.GetVisualElementById<ToolBarMenuItem>("tbmi4");
            ToolBarTextBox tbtb = this.GetVisualElementById<ToolBarTextBox>("tbtb");
            ToolBarDropDownButton tbddb = this.GetVisualElementById<ToolBarDropDownButton>("tbddb");
            ToolBarButton toolBarButton2 = this.GetVisualElementById<ToolBarButton>("ToolBarButton2");

            if (tbmi.ImageScaling == ToolStripItemImageScaling.None)
	        {
                tbmi.ImageScaling = ToolStripItemImageScaling.SizeToFit;
                tbmi2.ImageScaling = ToolStripItemImageScaling.SizeToFit;
                tbmi3.ImageScaling = ToolStripItemImageScaling.SizeToFit;
                tbmi4.ImageScaling = ToolStripItemImageScaling.SizeToFit;
                tbtb.ImageScaling = ToolStripItemImageScaling.SizeToFit;
                tbddb.ImageScaling = ToolStripItemImageScaling.SizeToFit;
	        }
            else
            {
                tbmi.ImageScaling = ToolStripItemImageScaling.None;
                tbmi2.ImageScaling = ToolStripItemImageScaling.None;
                tbmi3.ImageScaling = ToolStripItemImageScaling.None;
                tbmi4.ImageScaling = ToolStripItemImageScaling.None;
                tbtb.ImageScaling = ToolStripItemImageScaling.None;
                tbddb.ImageScaling = ToolStripItemImageScaling.None;
            }


          
          
            if (toolBarButton2.ImageScaling == ToolStripItemImageScaling.SizeToFit)
            {
                toolBarButton2.ImageScaling = ToolStripItemImageScaling.None;

            }
            else
            {
                toolBarButton2.ImageScaling = ToolStripItemImageScaling.SizeToFit;
            }
        }

    }
}