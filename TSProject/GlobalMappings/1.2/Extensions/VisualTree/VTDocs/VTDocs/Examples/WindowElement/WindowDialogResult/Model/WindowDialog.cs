﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Models
{
    public class WindowDialog : WindowElement
    {

        private const string DIALOG_ID = "MvcApplication9.Models.WindowDialog.Instance";

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);


            WindowDialog.Instance.Test = "dd";
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static WindowDialog Instance
        {
            get
            {
                WindowDialog objWindowDialog = ApplicationElement.Current[DIALOG_ID] as WindowDialog;
                if (objWindowDialog == null)
                {
                    using (WindowElement.CreateOpenerContext((objWindow) => { ApplicationElement.Current[DIALOG_ID] = objWindow; }))
                    {
                        Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
                        argsDictionary.Add("ElementName", "WindowElement");
                        argsDictionary.Add("ExampleName", "WindowDialogResult");
                        objWindowDialog = VisualElementHelper.CreateFromView<WindowDialog>("WindowDialog", "Index2", null, argsDictionary, null);
                    }
                }
                return objWindowDialog;
            }
        }


        /// <summary>
        /// Gets or sets the test.
        /// </summary>
        /// <value>
        /// The test.
        /// </value>
        public string Test
        {
            get;
            set;
        }

    }
}