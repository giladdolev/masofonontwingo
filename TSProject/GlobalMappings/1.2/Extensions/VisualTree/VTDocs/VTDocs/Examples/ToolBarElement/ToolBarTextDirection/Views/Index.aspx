<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="ToolBar TextDirection Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="1000px">
		
        <vt:ToolBar runat="server" Text="toolStrip1" Dock="None" Top="102px" TextDirection ="Vertical270" Left="276px" ID="toolStrip1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="50px" Width="466px">
            <vt:ToolBarLabel runat="server" ID="ToolBarLabel1" Text="ToolBarLabel1" > </vt:ToolBarLabel>
		</vt:ToolBar>

		<vt:Button runat="server" UseVisualStyleTextDirection="True" TextAlign="MiddleCenter" Text="Change TextDirection" Top="110px" Left="44px" ClickAction="ToolBarTextDirection\btnChangeTextDirection_Click" ID="btnChangeTextDirection" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="29px" TabIndex="1" Width="200px">
		</vt:Button>

		
		<vt:TextBox runat="server" PasswordChar="" Multiline="True" Top="230px" Left="400px" ID="textBox1" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="37px" TabIndex="3" Width="198px">
		</vt:TextBox>

		<vt:Label runat="server" Text="TextDirection is initially set to 'Vertical270'" Top="9px" Left="41px" ID="label1" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="9.75pt" Height="16px" TabIndex="4" Width="682px">
		</vt:Label>

        </vt:WindowView>
</asp:Content>