using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            lblLog.Text = "Panel PixelLeft Value is: " + TestedPanel.PixelLeft;
        }

        public void ChangePixelLeft_Click(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            PanelElement TestedPanel = this.GetVisualElementById<PanelElement>("TestedPanel");
            if (TestedPanel.PixelLeft == 200)
            {
                TestedPanel.PixelLeft = 80;
                lblLog.Text = "Panel PixelLeft Value is: " + TestedPanel.PixelLeft;
            }
            else
            {
                TestedPanel.PixelLeft = 200;
                lblLog.Text = "Panel PixelLeft Value is: " + TestedPanel.PixelLeft;
            }
        }      
    }
}