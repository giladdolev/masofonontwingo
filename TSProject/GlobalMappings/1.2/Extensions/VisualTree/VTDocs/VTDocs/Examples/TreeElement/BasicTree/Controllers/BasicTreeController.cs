using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Data;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class BasicTreeController : Controller
    {
        static private int i = 0;
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        public void load(object sender, EventArgs e)
        {
            TreeElement tree1 = this.GetVisualElementById<TreeElement>("tree1");
            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(string));

            source.Rows.Add("galilcs0", "", "gizmox0");
            source.Rows.Add("galilcs1", "galilcs0", "gizmox1");
            source.Rows.Add("galilcs2", "");
            source.Rows.Add("galilcs3", "galilcs0", "gizmox2");
            source.Rows.Add("galilcs4", "");
            source.Rows.Add("galilcs5", "galilcs1");
            source.Rows.Add("galilcs6", "galilcs4", "gizmox3");

            tree1.ParentFieldName = "Column2";
            tree1.KeyFieldName = "Column1";
            tree1.DataSource = source;

            tree1.Columns[0].Visible = false;
        }
    }
}