<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Panel Padding Property" Top="70px" Left="11px" ID="windowView1" Height="800px" Width="768px">

        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>     

        <vt:Panel runat="server" Text="Padding is set to '0, 50, 0, 0'" Top="55px" Padding-Bottom ="0" Padding-Left="0" Padding-Right="0" Padding-Top="50" Left="140px" ID="pnlPadding" Height="150px" Width="200px">
            <vt:Button runat="server" Text="Button" Top="10px" Left="10px" ID="Button2" Height="26px" Width="100px" ></vt:Button>
            <vt:CheckBox runat="server" Text="CheckBox" Top="40px" Left="10px" ID="CheckBox2" Height="26px"  Width="100px" ></vt:CheckBox>
            <vt:Label runat="server" Text="Label" Top="70px" Left="10px" ID="Label3" Height="26px" Width="100px" ></vt:Label>
        </vt:Panel>           

        <vt:Label runat="server" Text="RunTime" Top="220px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" Width="120px"></vt:Label>

        <vt:Panel runat="server" Text="Runtime Padding" Top="235px" Left="140px" ID="pnlRuntimePadding" Height="150px" Width="200px" >
            <vt:Button runat="server" Text="Button" Top="10px" Left="10px" ID="Button1" Height="26px" Width="100px" ></vt:Button>
            <vt:CheckBox runat="server" Text="CheckBox" Top="40px" Left="10px" ID="CheckBox1" Height="26px"  Width="100px" ></vt:CheckBox>
            <vt:Label runat="server" Text="Label" Top="70px" Left="10px" ID="Label5" Height="26px" Width="100px" ></vt:Label>
        </vt:Panel>

        <vt:Button runat="server" Text="Change Panel Padding" Top="265px" Left="420px" ID="btnChangePadding" Height="36px" TabIndex="1" Width="200px" ClickAction="PanelPadding\btnChangePadding_Click"></vt:Button>

        <vt:TextBox runat="server" Text="" Top="400px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>

    </vt:WindowView>
</asp:Content>
        