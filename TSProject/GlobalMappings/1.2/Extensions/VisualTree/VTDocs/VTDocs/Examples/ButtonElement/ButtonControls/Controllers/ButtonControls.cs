using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ButtonControlsController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        public void btnAddControls_Click(object sender, EventArgs e)
        {
            ButtonElement btnAddControls = this.GetVisualElementById<ButtonElement>("btnAddControls");

            TextBoxElement textBox2 = new TextBoxElement();

            btnAddControls.Controls.Add(textBox2);


            
        }

    }
}