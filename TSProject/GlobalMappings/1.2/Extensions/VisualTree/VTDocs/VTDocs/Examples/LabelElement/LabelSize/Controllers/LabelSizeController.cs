using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class LabelSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
        private void OnLoad(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "Size value: " + TestedLabel.Size;
        }

        private void btnChangeSize_Click(object sender, EventArgs e)
        {
            LabelElement TestedLabel = this.GetVisualElementById<LabelElement>("TestedLabel");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            if (TestedLabel.Size == new Size(300, 80))   //Get
            {
                TestedLabel.Size = new Size(80, 18);    //Set
                lblLog.Text = "Size value: " + TestedLabel.Size;
            }
            else
            {
                TestedLabel.Size = new Size(300, 80);
                lblLog.Text = "Size value: " + TestedLabel.Size;
            }
        }
    }
}