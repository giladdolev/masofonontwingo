﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid SelectedRows / Columns / Cells" ID="windowView1" LoadAction="GridSelectedRowsColumnsCells\Load_View">

        <vt:Label runat="server" SkinID="Title" Text="SelectionChanged" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Occurs when the current select several rows or cells."
            Top="75px" ID="lblDefinition">
        </vt:Label>



        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="150px" ID="lblExample"></vt:Label>

        <vt:Grid runat="server" Top="290" ID="gridElement" Height="180px" Width="300px" RowHeaderVisible="true" SelectionChangedAction="GridSelectedRowsColumnsCells\Grid_SelectionChangedAction">
        </vt:Grid>

        <vt:TextBox runat="server" SkinID="EventLog" Text="Event Log:" Top="100" Height="10000" Left
            ="400" ID="txtEventLog"></vt:TextBox>

        <vt:Button runat="server" ID="btn1" Width="40" Height="25" Top="350" Left="400" ClickAction="GridSelectedRowsColumnsCells\clcik"></vt:Button>
    </vt:WindowView>
</asp:Content>
