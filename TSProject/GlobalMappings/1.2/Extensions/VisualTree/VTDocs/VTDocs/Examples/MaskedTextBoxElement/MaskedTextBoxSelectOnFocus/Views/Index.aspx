<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="MaskedTextBox SelectOnFocus Property" ID="windowView1" LoadAction="MaskedTextBoxSelectOnFocus\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="SelectOnFocus" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the control text is selected when focused

           Property value: 'true' if the entire text will be selected when focused or false if
             only the cursor is shown when focused. The default false. 
            
            Syntax: public bool SelectOnFocus { get; set; }
            
            Notice - this property is not intended to be changed at run time." Top="75px"  ID="lblDefinition"></vt:Label>

        
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="280px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="SelectOnFocus property of 'TestedMaskedTextBox2' is initially set to 'true'.
            Use the buttons bellow to examine the behavior" Top="330px"  ID="lblExp1"></vt:Label>     
      

         <!--TestedMaskedTextBox1-->
        <vt:MaskedTextBox runat="server" Text="TestedMaskedTextBox1" Top="395px" ID="TestedMaskedTextBox1" LostFocusAction="MaskedTextBoxSelectOnFocus\TestedMaskedTextBox1_LostFocus"></vt:MaskedTextBox>  
        
        <vt:Label runat="server" SkinID="Log" Width="240px" Height="45px" Top="436px" ID="lblLog1"></vt:Label>   

        <vt:Button runat="server" Text="Focus TestedMaskedTextBox1 >>"  Width="200px" Top="521px" ID="btnFocus1" ClickAction="MaskedTextBoxSelectOnFocus\btnFocus1_Click"></vt:Button>
 
      
         <!--TestedMaskedTextBox2-->
        <vt:MaskedTextBox runat="server"  Text="TestedMaskedTextBox2" Left="430px" Top="395px" ID="TestedMaskedTextBox2" LostFocusAction="MaskedTextBoxSelectOnFocus\TestedMaskedTextBox2_LostFocus" SelectOnFocus="true"></vt:MaskedTextBox>

        <vt:Label runat="server" SkinID="Log" Top="436px" Width="240px" Height="45px" Left="430px" ID="lblLog2" ></vt:Label>

        <vt:Button runat="server" Text="Focus TestedMaskedTextBox2 >>" Width="200px" Top="521px" Left="430px" ID="btnFocus2" ClickAction="MaskedTextBoxSelectOnFocus\btnFocus2_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
