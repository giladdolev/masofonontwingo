<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="RichTextBox ClientRectangle Property" ID="windowView" LoadAction="RichTextBoxClientRectangle\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="ClientRectangle" ID="lblTitle"></vt:Label>

        <vt:Label runat="server" Text="Gets the rectangle that represents the client area of the control.

            Syntax: public Rectangle ClientRectangle { get; }
            
            The client area of a control is the bounds of the control, minus the nonclient elements such as 
            scroll bars, borders, title bars, and menus." Top="75px" ID="lblDefinition" ></vt:Label>
         
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="220px"  ID="lblExample"></vt:Label>

        <vt:Label runat="server" Text="ClientRectangle values of this 'TestedRichTextBox' is initially set with 
            Left: 80px, Top: 320px, Width: 200px, Height: 80px" Top="260px"  ID="lblExp1"></vt:Label>     

        <!-- TestedRichTextBox -->
        <vt:RichTextBox runat="server" Text="TestedRichTextBox" Top="320px" ID="TestedRichTextBox"></vt:RichTextBox>

        <vt:Label runat="server" SkinID="Log" Top="415px" Height="40" Width="350" ID="lblLog"></vt:Label>

        <vt:Button runat="server" Text="Change ClientRectangle value >>"  Top="520px" Width="200px" ID="btnChangeRichTextBoxClientRectangle" ClickAction="RichTextBoxClientRectangle\btnChangeRichTextBoxClientRectangle_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
