using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class RichTextBoxPixelLeftController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            lblLog.Text = "PixelLeft value: " + TestedRichTextBox.PixelLeft + '.';
        }

        public void btnChangePixelLeft_Click(object sender, EventArgs e)
        {
            RichTextBox TestedRichTextBox = this.GetVisualElementById<RichTextBox>("TestedRichTextBox");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");


            if (TestedRichTextBox.PixelLeft == 80)
            {
                TestedRichTextBox.PixelLeft = 180;
                lblLog.Text = "PixelLeft value: " + TestedRichTextBox.PixelLeft + '.';
            }
            else
            {
                TestedRichTextBox.PixelLeft = 80;
                lblLog.Text = "PixelLeft value: " + TestedRichTextBox.PixelLeft + '.';
            }
        }
      
    }
}