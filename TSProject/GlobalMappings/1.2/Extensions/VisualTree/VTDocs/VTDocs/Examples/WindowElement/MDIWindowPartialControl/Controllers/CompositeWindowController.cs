using MvcApplication9.Models;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class CompositeWindowController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult CompositeWindow()
        {
            return View(new CompositeWindow());
        }     

        public void cmp_load(object sender, EventArgs e)
        {
            CompositeElement cmp = this.GetRootVisualElement() as CompositeElement;
          
        }

        public void cmp_unload(object sender, EventArgs e)
        {

        }

        public void rmove(object sender, EventArgs e)
        {
            CompositeElement cmp = this.GetRootVisualElement() as CompositeElement;
            CancelEventArgs newArgs = new CancelEventArgs();
            cmp.PerformUnloading(newArgs);
        }

        public void change_btn_color(object sender, EventArgs e)
        {
            ButtonElement btn = this.GetVisualElementById<ButtonElement>("Button2");
            btn.BackColor = Color.Red;
        }

      
    }
}