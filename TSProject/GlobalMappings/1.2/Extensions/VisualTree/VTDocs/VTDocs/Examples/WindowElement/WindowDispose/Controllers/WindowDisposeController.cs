using MvcApplication9.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class WindowDisposeController : Controller
    {

        
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Index()
        {
            return View(new WindowDispose());
        }
        private WindowDispose ViewModel
        {
            get { return this.GetRootVisualElement() as WindowDispose; }
        }

        public void btnOpenTestedWindow_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (this.ViewModel.TestedWin == null ||  this.ViewModel.TestedWin.IsDisposed)
            {
                Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
                argsDictionary.Add("ElementName", "WindowElement");
                argsDictionary.Add("ExampleName", "WindowDispose()");

                ViewModel.TestedWin = VisualElementHelper.CreateFromView<DisposeTestedWindow>("DisposeTestedWindow", "Index2", null, argsDictionary, null);
                this.ViewModel.TestedWin.Show();
                textBox1.Text = "TestedWindow is Open";
            }


        }

        public void btnDispose_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            if (this.ViewModel.TestedWin != null)
            {
                this.ViewModel.TestedWin.Dispose();
                textBox1.Text = "TestedWindow is Disposed";
            }

        }

    }
}