<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="TextBox AutoSize Property" Top="57px" Left="11px" ID="windowView1" Height="905px" Width="768px">
        <vt:Label runat="server" Text="InitializeComponent" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label>

        <vt:Button runat="server" Text="TextBox AutoSize is set to trueeeeeeeeeeeeeeeeeeeee" AutoSize ="true" Top="45px" Left="140px" ID="txtAutoSize" Height="36px" TabIndex="2" ></vt:Button>    

        <vt:Label runat="server" Text="RunTime" Top="100px" Left="140px" ID="Label2" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="120px"></vt:Label>

        <vt:TextBox runat="server" Text="Tested TextBox" Top="125px"  AutoSize ="false" Left="140px" ID="txtAutoSizeChange" Height="26px" Width="90px"></vt:TextBox>

        <vt:Button runat="server" Text="Change AutoSize" Top="115px" Left="350px" ID="btnChangeAutoSize" Height="36px"  TabIndex="3" Width="100px" ClickAction="TextBoxAutoSize\btnChangeAutoSize_Click"></vt:Button>

         <vt:Button runat="server" Text="Change Text" Top="115px" Left="450px" ID="btnChangeText" Height="36px"  TabIndex="3" Width="100px" ClickAction="TextBoxAutoSize\btnChangeText_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
