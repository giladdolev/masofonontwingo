using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
namespace HelloWorldApp
{
    public class GridGridColumnVisible1WrongIndexes27367QAController : Controller
	{
		public ActionResult Index()
		{
			return this.View();
		}

        public void OnLoad(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            SetDataSource();
            TestedGrid.Columns[0].Visible = false;

            lblLog.Text = "Column0 Visible value is: " + TestedGrid.Columns[0].Visible;
        }

        private void SetDataSource()
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");

            DataTable source = new DataTable();

            source.Columns.Add("Column0", typeof(int));
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(bool));

            source.Rows.Add(12345, "James", true);
            source.Rows.Add(23451, "Bob", false);
            source.Rows.Add(34512, "Dana", true);
            source.Rows.Add(45123, "Brad", false);

            TestedGrid.DataSource = source;
        }

        public void btnChangeCol0Visibility_Click(object sender, EventArgs e)
        {
            GridElement TestedGrid = this.GetVisualElementById<GridElement>("TestedGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            TestedGrid.Columns[0].Visible = !TestedGrid.Columns[0].Visible;
            lblLog.Text = "Column0 Visible value is: " + TestedGrid.Columns[0].Visible;
        }     	
	}
}
