using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class PanelBackgroundImageController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            PanelElement TestedPanel1 = this.GetVisualElementById<PanelElement>("TestedPanel1");
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedPanel1.BackgroundImage == null)
            {
                lblLog1.Text = "No background image";
            }
            else
            {
                lblLog1.Text = "Set with background image";
            }


            if (TestedPanel2.BackgroundImage == null)
            {
                lblLog2.Text = "No background image.";
            }
            else
            {
                lblLog2.Text = "Set with background image.";
            }
        }



        public void btnChangeBackgroundImage_Click(object sender, EventArgs e)
        {
            PanelElement TestedPanel2 = this.GetVisualElementById<PanelElement>("TestedPanel2");
            LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");

            if (TestedPanel2.BackgroundImage == null)
            {
                TestedPanel2.BackgroundImage = new UrlReference("/Content/Images/icon.jpg");
                lblLog2.Text = "Set with background image.";
            }
            else
            {
                TestedPanel2.BackgroundImage = null;
                lblLog2.Text = "No background image.";

            }
        }

    }
}