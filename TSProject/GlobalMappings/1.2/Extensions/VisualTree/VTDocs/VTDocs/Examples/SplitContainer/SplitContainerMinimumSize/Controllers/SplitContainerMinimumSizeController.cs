using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class SplitContainerMinimumSizeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        //SplitContainerAlignment
        public void btnChangeMinimumSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splRuntimeMinimumSize = this.GetVisualElementById<SplitContainer>("splRuntimeMinimumSize");

            if (splRuntimeMinimumSize.MinimumSize == new Size(0, 0)) //Get
            {
                splRuntimeMinimumSize.MinimumSize = new Size(225, 85); //Set
                textBox1.Text = "MinimumSize: " + splRuntimeMinimumSize.MinimumSize + " The size of the SplitContainer is: " + splRuntimeMinimumSize.Size;

            }
            else
            {
                splRuntimeMinimumSize.MinimumSize = new Size(0, 0); 
                splRuntimeMinimumSize.Size = new Size(200, 70); 
                textBox1.Text = "MinimumSize is: " + splRuntimeMinimumSize.MinimumSize + " The size of the SplitContainer is: " + splRuntimeMinimumSize.Size;
            }
        }
        //btnGetSize
        public void btnGetSize_Click(object sender, EventArgs e)
        {
            TextBoxElement textBox1 = this.GetVisualElementById<TextBoxElement>("textBox1");
            SplitContainer splRuntimeMinimumSize = this.GetVisualElementById<SplitContainer>("splRuntimeMinimumSize");


            textBox1.Text = "The size is: " + splRuntimeMinimumSize.Size ;
            
        }

    }
}