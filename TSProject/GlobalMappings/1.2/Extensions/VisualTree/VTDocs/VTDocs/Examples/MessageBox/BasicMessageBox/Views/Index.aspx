<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Basic MessageBox" Top="57px" Left="11px" ID="windowView1" LoadAction="BasicMessageBox\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="MessageBox" ID="lblTitle" ></vt:Label>

        <vt:Label runat="server" Text="Displays a message window, also known as a dialog box, which presents a message to the user. 
            It is a modal window, blocking other actions in the application until the user closes it. 
            A MessageBox can contain text, buttons, and symbols that inform and instruct the user."
            Top="75px" ID="lblDefinition">
        </vt:Label>
        
        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="185px" ID="lblExample"></vt:Label>
        
        <vt:Label runat="server" Text="The following example shows basic usage of MessageBox class" Top="235px" ID="lblExp2">
        </vt:Label>  

        <vt:Button runat="server" Text="Show MessageBox >>" SkinID="Wide" Top="285px" ID="btnShow" ClickAction="BasicMessageBox\btnShow_Click"></vt:Button>
    </vt:WindowView>
</asp:Content>
