using System;
using System.Data;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace MvcApplication9.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComboGridTextChangedController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public void OnLoad(object sender, EventArgs e)
        {
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");

            DataTable source = new DataTable();
            source.Columns.Add("Column1", typeof(string));
            source.Columns.Add("Column2", typeof(string));
            source.Columns.Add("Column3", typeof(int));
            source.Rows.Add("James", "Dean", 1);
            source.Rows.Add("Bob", "Marley", 2);
            source.Rows.Add("Dana", "International", 3);

            TestedComboGrid.ValueMember = "Column1";
            TestedComboGrid.DisplayMember = "Column3";

            TestedComboGrid.DataSource = source;
            lblLog.Text = "ComboGrid Text value: " + TestedComboGrid.Text;

        }

        public void TestedComboGrid_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement txtEventLog = this.GetVisualElementById<TextBoxElement>("txtEventLog");
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");

            txtEventLog.Text += "\\r\\nComboGrid TextChanged event is invoked";
            lblLog.Text = "ComboGrid Text value: " + TestedComboGrid.Text;
        }

        public void btnChangeText_Click(object sender, EventArgs e)
        {
            ComboGridElement TestedComboGrid = this.GetVisualElementById<ComboGridElement>("TestedComboGrid");

            if (TestedComboGrid.Text == "Some Text")
                TestedComboGrid.Text = "New Text";
            else
                TestedComboGrid.Text = "Some Text";
        }
    }
}
