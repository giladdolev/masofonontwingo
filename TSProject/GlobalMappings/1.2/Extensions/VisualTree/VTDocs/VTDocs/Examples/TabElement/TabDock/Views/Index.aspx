<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Tab Dock Property" Top="57px" Left="11px" ID="windowView1" Height="800px" Width="768px">
       
         <vt:Label runat="server" Text="The Tab is initialliy docked to the bottom" Top="30px" Left="140px" ID="Label1" BorderStyle="None" Font-Bold="True" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="13px" TabIndex="45" Width="150px"></vt:Label> 

        <vt:Tab runat="server" Text="Tab" Top="165px" Left="140px" ID="tabDock" Dock="Bottom" Height="200px" TabIndex="1" Width="200px">
            <TabItems>
                <vt:TabItem runat="server" Text="tabPage1" Top="22px" Left="4px" ID="tabPage1" Height="74px" Width="192px" ImageIndex="0" >
                </vt:TabItem>
                <vt:TabItem runat="server" Text="tabPage2" Top="22px" Left="4px" ID="tabPage2" Height="74px" Width="192px" ImageIndex="1" ></vt:TabItem>
            </TabItems>
        </vt:Tab>

        <vt:Button runat="server" Text="Change Dock style" Top="210px" Left="420px" ID="btnDock" Height="36px" TabIndex="1" Width="200px" 
            ClickAction="TabDock\btnDock_Click"></vt:Button>
        
        <vt:TextBox runat="server" Text="" Top="350px" Left="140px"  ID="textBox1" Multiline="true"  Height="50px" Width="200px"> 
            </vt:TextBox>


    </vt:WindowView>
</asp:Content>
        