﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" StylesheetTheme="Example"%>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Text="Grid RowHeaderVisible Property" ID="windowView1" LoadAction="GridRowHeaderVisible\OnLoad">

        <vt:Label runat="server" SkinID="Title" Text="RowHeaderVisible" ID="Label1"></vt:Label>

        <vt:Label runat="server" Text="Gets or sets a value indicating whether the column that contains row headers is displayed.           

            Syntax: public bool RowHeaderVisible { get; set; }
            Property Values: 'true' if the column that contains row headers is displayed; otherwise, 'false'.
            The default is 'true'." Top="75px"  ID="Label2"></vt:Label>

        <vt:Grid runat="server" Top="195px" ID="TestedGrid1" CssClass="vt-test-grid-1">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid1Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid1Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>           

        <vt:Label runat="server" SkinID="Log" Top="325px" ID="lblLog1"></vt:Label>

        <vt:Label runat="server" SkinID="SubTitle" Text="Example:" Top="365px"  ID="lblExample"></vt:Label>
        <vt:Label runat="server" Text="RowHeaderVisible property of this 'TestedGrid' is initially set to 'false'." Top="405px" ID="lblExp1"></vt:Label> 

        <vt:Grid runat="server" Top="450px" RowHeaderVisible="false" ID="TestedGrid2" CssClass="vt-test-grid-2">
            <Columns>
                <vt:GridColumn  runat="server" ID="Grid2Col1" HeaderText="Column1" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col2" HeaderText="Column2" Height="20px" Width="85px"></vt:GridColumn>
                <vt:GridColumn  runat="server" ID="Grid2Col3" HeaderText="Column3" Height="20px" Width="85px"></vt:GridColumn>
            </Columns>
        </vt:Grid>

        <vt:Label runat="server" SkinID="Log" Top="580px" height="40px" ID="lblLog2"></vt:Label>

        <vt:Button runat="server" Text="Change RowHeaderVisible Value >>" Top="640px" ID="btnChangeRowHeaderVisible" Width="200px" ClickAction="GridRowHeaderVisible\btnChangeRowHeaderVisible_Click"></vt:Button>

        <vt:Button runat="server" Text="Get Column Index 0 Name >>" Top="640px" Left="300px" ID="btnColumnIndex0" Width="200px" ClickAction="GridRowHeaderVisible\btnColumnIndex0_Click"></vt:Button>

    </vt:WindowView>
</asp:Content>
