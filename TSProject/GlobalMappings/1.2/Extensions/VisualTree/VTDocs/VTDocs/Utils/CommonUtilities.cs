﻿using System;
using System.Web.Hosting;

namespace VTDocs.Utils
{
    public class CommonUtilities
    {
        /// <summary>
        /// Gets cached data.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="init">The initialize.</param>
        /// <returns></returns>
        internal static T GetData<T>(string key, Func<T> init) where T : class
        {
            T data = HostingEnvironment.Cache.Get(key) as T;
            if (data == null)
            {
                data = init();
                HostingEnvironment.Cache.Add(key, data, null, System.Web.Caching.Cache.NoAbsoluteExpiration, 
                    new TimeSpan(24, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
            }

            return data;
        }
    }
}