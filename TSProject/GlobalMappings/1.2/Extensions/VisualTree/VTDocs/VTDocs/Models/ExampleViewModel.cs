﻿using APIData.Models;

namespace VTDocs.Models
{
    public class ExampleViewModel : Example
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExampleViewModel" /> class.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="member">The member.</param>
        /// <param name="exampleName">Name of the example.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="action">The action.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="description">The description.</param>
        public ExampleViewModel(string element, string member, string exampleName, string controller, string action, string displayName, string description)
        {
            this.Element = element;
            this.Member = member;
            this.ID = exampleName;
            this.ExampleController = controller;
            this.ExampleAction = action;
            this.DisplayName = displayName;
            this.Description = description;
        }

        /// <summary>
        /// Gets or sets the content of the formatted model.
        /// </summary>
        /// <value>
        /// The content of the formatted model.
        /// </value>
        public string FormattedModelContent { get; set; }

        /// <summary>
        /// Gets or sets the content of the formatted view.
        /// </summary>
        /// <value>
        /// The content of the formatted view.
        /// </value>
        public string FormattedViewContent { get; set; }

        /// <summary>
        /// Gets or sets the content of the formatted controller.
        /// </summary>
        /// <value>
        /// The content of the formatted controller.
        /// </value>
        public string FormattedControllerContent { get; set; }

    }
}