﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using VTDocs.Controllers;

namespace VTDocs
{
    /// <summary>
    /// Summary description for ExampleDownload
    /// </summary>
    public class ExampleDownload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string exampleId = context.Request.Params["id"];
            string elementId = context.Request.Params["elementId"];
            string member = context.Request.Params["member"];
            string folderPath = ExampleController.GetExamplePath(elementId, member, exampleId);
            if (!String.IsNullOrEmpty(folderPath) && Directory.Exists(folderPath))
            {
                string archiveFile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                ZipFile.CreateFromDirectory(folderPath, archiveFile);

                context.Response.AppendHeader("Cache-Control", "public");
                context.Response.AppendHeader("Content-Description", "File Transfer");
                context.Response.AppendHeader("Content-Transfer-Encoding", "binary");
                context.Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=\"{0}.zip\"", exampleId));
                context.Response.AppendHeader("Content-Type", "binary/octet-stream");
                context.Response.WriteFile(archiveFile);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}