﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using VTDocs.App_Start;
using System.Web.WebPages;

namespace VTDocs
{
    public class VTDocs : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ViewEngines.Engines.Insert(0, new ExampleViewEngine());

            DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("WP")
            {
                ContextCondition = (ctx => ctx.GetOverriddenUserAgent()
                .IndexOf("Windows Phone OS", StringComparison.OrdinalIgnoreCase) > 0)
            });

            //DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("iPhone")
            DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("Android")
            {
                ContextCondition = (ctx => 
                    ctx.GetOverriddenUserAgent()
                    .IndexOf("iPhone", StringComparison.OrdinalIgnoreCase) > 0 ||
                
                    ctx.GetOverriddenUserAgent()
                    .IndexOf("iPad", StringComparison.OrdinalIgnoreCase) > 0)
            });

            DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("Android")
            {
                ContextCondition = (ctx => ctx.GetOverriddenUserAgent()
                .IndexOf("Android", StringComparison.OrdinalIgnoreCase) > 0)
            }); 
        }
    }
}
