<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
  <vt:WindowView runat="server" StartPosition="WindowsDefaultLocation" Text="Home" ID="VTwindowView" BorderStyle="None" Font-Bold="True" Font-Names="Tahoma" Font-Size="9pt" TabIndex="-1" Dock="Fill" LoadAction="Home/LoadItems" Resizable="false">
        <vt:Panel runat="server" ID="panelLeft" Width="300px" Dock="Left" CssClass="side-menu">
            <vt:TextBox runat="server" ID="txtFilter" Height="30px" Width="50px" Dock="Top" TextChangedAction="Home/txtFilter_TextChangedAction" Font-Size="12pt"/>
            <vt:Tree runat="server" Dock="Fill" ID="tvControls" AfterSelectAction="Home/tvControls_AfterSelectAction" HistoryEnabled="true"/>   
        </vt:Panel>
        <vt:Panel runat="server" ID="panelRight" Dock="Fill" CssClass="playground-sandbox">
            <vt:Panel runat="server" ID="dummy" Dock="Fill">
            </vt:Panel>
        </vt:Panel>
    </vt:WindowView>
</asp:Content>

