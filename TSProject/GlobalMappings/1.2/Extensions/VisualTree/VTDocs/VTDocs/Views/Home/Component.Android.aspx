<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<APIData.Models.TypeNode>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>

<vt:TCompositeView runat="server" Text="" ID="componentCompositeView" Dock="Fill" LoadAction="Component\ComponentCompositeView_Init">
    <vt:PartialControl runat="server" ID="componentHeader" Controller="Component" Action="ComponentHeader" Dock="Top" Height="350px">
        <vt:PartialArgument runat="server" Name="elementID" Value="<%# Model.Name %>" />
        <vt:PartialArgument runat="server" Name="propertyID" />
    </vt:PartialControl>
    

    <vt:TTab runat="server" ID="panelMembers" Dock="Fill" Tag='<%# Model.Name %>' HistoryEnabled="true" Alignment="Bottom" >
        <TabItems>
             <vt:TabItem runat="server" ID="panelEvents" Text="Events" Tag='<%# Model.Name %>' AutoScroll="true" ShowHeader="true" Dock="Fill" >
                
                <vt:TListView ID="lwEvents" runat="server" DataSource="<%# Model.Events %>" Dock="Fill" 
                    ItemTemplate="<h1>{Name}</h1> <div>{Description}</div>" AutoGenerateColumns="False"
                    ClickAction="Component\LoadEvents">
                    <Columns>
                        <vt:ColumnHeader runat="server" DataMember="Name" ></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" DataMember="Description" ></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" DataMember="DisplayName" ></vt:ColumnHeader>
                    </Columns>
                </vt:TListView>
            </vt:TabItem>

            <vt:TabItem runat="server" ID="panelMethods" Text="Methods" Tag='<%# Model.Name %>' AutoScroll="true" ShowHeader="true" Dock="Fill" >

                <vt:TListView ID="lwMethods" runat="server" DataSource="<%# Model.Methods %>" Dock="Fill" 
                    ItemTemplate="<h1>{Name}</h1> <div>{Description}</div>" AutoGenerateColumns="False"
                    ClickAction="Component\LoadMethodData">
                    <Columns>
                        <vt:ColumnHeader runat="server" DataMember="Name" ></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" DataMember="Description" ></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" DataMember="DisplayName" ></vt:ColumnHeader>
                    </Columns>
                </vt:TListView>

            </vt:TabItem>
           
            <vt:TabItem  runat="server" ID="panelProperties" Text="Properties" Tag='<%# Model.Name %>' Dock="Fill" ShowHeader="true" AutoScroll="true" Height="300" Index="2">
                <vt:TListView ID="lwProperties" runat="server" DataSource="<%# Model.Properties %>" Dock="Fill" 
                    ItemTemplate="<h1>{Name}</h1> <div>{Description}</div>" AutoGenerateColumns="False"
                    ClickAction="Component\LoadPropertyData">
                    <Columns>
                        <vt:ColumnHeader runat="server" DataMember="Name" ></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" DataMember="Description" ></vt:ColumnHeader>
                        <vt:ColumnHeader runat="server" DataMember="DisplayName" ></vt:ColumnHeader>
                    </Columns>
                </vt:TListView>
            </vt:TabItem>

             <vt:TabItem  runat="server" ID="basicExamplePanel" Text="Basic" Tag='<%# Model.Name %>' Dock="Fill" ShowHeader="true" AutoScroll="true" Height="300" Index="2">
                

            </vt:TabItem>


        </TabItems>
    </vt:TTab>
</vt:TCompositeView>
