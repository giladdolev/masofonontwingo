<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<VTDocs.Models.ExampleViewModel>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:TCompositeView runat="server" ID="exampleCompositeView" Dock="Top" ElementID="<%# Model.Element %>" Member="<%# Model.Member %>" ExampleName="<%# Model.ID %>" ExampleController="<%# Model.ExampleController %>" ExampleAction="<%# Model.ExampleAction %>">
    <vt:Button runat="server" ID="btnPreview" Text="Preview" Width="90" Height="27" Left="680px" Top="60px" ZIndex="2" ClickAction="Example/btnPreview_Click" />
    <vt:Button runat="server" ID="btnDownload" Text="Download" Width="90" Height="27" Left="770px" Top="60px" ZIndex="1" ClickAction="Example/btnDownload_Click" />
    <vt:Label runat="server" ID="labelExampleName" Text="<%# Model.DisplayName %>" Dock="Top" Height="15px" />
    <vt:Label runat="server" ID="labelDescription" Text="<%# Model.Description %>" Dock="Top" />
    <vt:TTab runat="server" Top="60px" Left="60px" Width="800px" Height="350px" Text="<%# Model.DisplayName %>" ZIndex="0">
        <TabItems>
            <vt:TabItem runat="server" ID="tabModel" Text="Model" AutoScroll="true" Visible="<%# !String.IsNullOrEmpty(Model.FormattedModelContent) %>" Index="0">
                <vt:Label runat="server" Content="<%# Model.FormattedModelContent %>" Width="800px" Height="350px" />
            </vt:TabItem>
            <vt:TabItem runat="server" ID="tabController" Text="Controller" AutoScroll="true" Index="2">
                <vt:Label runat="server" Content="<%# Model.FormattedControllerContent %>" Width="808px" Height="350px" />
            </vt:TabItem>
            <vt:TabItem runat="server" ID="tabView" Text="View" AutoScroll="true" Index="1">
                <vt:Label runat="server" Content="<%# Model.FormattedViewContent %>" Width="800px" Height="350px" />
            </vt:TabItem>
        </TabItems>
    </vt:TTab>
</vt:TCompositeView>
