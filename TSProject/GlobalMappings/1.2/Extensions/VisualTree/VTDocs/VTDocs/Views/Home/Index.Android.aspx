<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Mobile.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:TWindowView runat="server" StartPosition="WindowsDefaultLocation" Text="Home" ID="windowView1" BorderStyle="None" Font-Bold="True" Font-Names="Tahoma" Font-Size="9pt" TabIndex="-1" Dock="Fill" LoadAction="Home/LoadItems">
        
        <vt:TCarousel runat="server" id="carousel" Dock="Fill">
            <vt:TPanel runat="server" ID="panelLeft" Dock="Fill" >
                <vt:TTree runat="server" Dock="Fill" ID="tvControls" AfterSelectAction="HomeMobile/tvControls_AfterSelectAction" NodeMouseClickAction="HomeMobile/tvControls_AfterSelectAction" HistoryEnabled="true" />
            </vt:TPanel>
        </vt:TCarousel>
    </vt:TWindowView>
</asp:Content>
