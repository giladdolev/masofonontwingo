<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

   

         <vt:CompositeView runat="server" BackColor ="WhiteSmoke" Top="50px" Height="185px" Width="540" ID="CompositeIndex" BorderStyle="Dotted" >
      
           <vt:Label runat="server" Text="Meal Reservations" Top="0px" left="10px" Font-Names="Calibri Light" Font-Size="17pt" ID="lblDefinition"></vt:Label>
          
                <vt:RadioButton runat="server" Text="Breakfast" Top="5px" Left="255px" IsChecked="true" ID="rdoBreakfast" Font-Names="Calibri Light"></vt:RadioButton>
                <vt:RadioButton runat="server" Text="Lunch" Top="5px"  Left="365px" ID="rdoLunch" Font-Names="Calibri Light"></vt:RadioButton>
                <vt:RadioButton runat="server" Text="Dinner" Top="5px"  Left="450px" ID="rdoDinner" Font-Names="Calibri Light"></vt:RadioButton>
           

           <vt:Label runat="server" Text="Guest Name" Top="40px" left="30px"  ID="Label1" Font-Bold="true" Font-Names="Calibri Light" Font-Size="8pt"></vt:Label>
           <vt:TextBox runat="server" Text="" Top="55px" left="30px" Width="110px"  ID="txtFirstName"></vt:TextBox>
           <vt:TextBox runat="server" Text="" Top="55px" left="160px" Width="110px"  ID="txtLastName"></vt:TextBox>
           <vt:Label runat="server" Text="First" Top="80px" left="30px"  ID="lblFirstName" Font-Names="Calibri Light"></vt:Label>
           <vt:Label runat="server" Text="Last" Top="80px" left="160px"  ID="lblLastName" Font-Names="Calibri Light"></vt:Label>
          
           

           <vt:CheckBox runat="server" Text="Vegetarian" Left="300px" Top="55px" ID="chkVegetarian" Font-Names="Calibri Light"></vt:CheckBox>

           <vt:Button runat="server" Text="Submit" Top="55px" Left="430px"  ID="btnSubmit" Height="36px" Width="80px" Font-Names="Calibri Light" Font-Size="12pt" ClickAction="Composite1\btnSubmit_Click" ></vt:Button>

           <vt:Label runat="server" Text="Orders List: " Top="110px" left="30px"  ID="Label6" Font-Bold="true" Font-Names="Calibri Light"></vt:Label>

           <vt:ComboBox runat="server" ID="cmbOrdersList" Top="125px" Left="30px"   Width="480px"></vt:ComboBox>

           <vt:Label runat="server" Top="160px" left="10px"  ID="lblLog" Font-Size="9pt"  Font-Names="Calibri Light"></vt:Label>

         </vt:CompositeView>

    <%--    <vt:Composite runat="server" BackColor ="LightBlue" Top="400px" Height="190px" Width="590" ID="Composite1" BorderStyle="Dotted" >
      
           <vt:Label runat="server" Text="Meal Reservations" Top="0px" left="10px" Font-Names="Calibri Light" Font-Size="17pt" ID="Label8"></vt:Label>

           <vt:Label runat="server" Text="Guest Name" Top="40px" left="30px"  ID="Label9" Font-Bold="true" Font-Names="Calibri Light" Font-Size="8pt"></vt:Label>
           <vt:TextBox runat="server" Text="" Top="55px" left="30px" Width="110px"  ID="TextBox1"></vt:TextBox>
           <vt:TextBox runat="server" Text="" Top="55px" left="160px" Width="110px"  ID="TextBox2"></vt:TextBox>
           <vt:Label runat="server" Text="First" Top="80px" left="30px"  ID="Label10" Font-Names="Calibri Light"></vt:Label>
           <vt:Label runat="server" Text="Last" Top="80px" left="160px"  ID="Label11" Font-Names="Calibri Light"></vt:Label>
          <vt:Label runat="server" Text="Chosen Meal" Top="40px" left="300px"  ID="Label12" Font-Bold="true" Font-Names="Calibri Light"></vt:Label>
            <vt:GroupBox runat="server" Height="75px" Left="300px" Width="110px" Top="55px" ID="GroupBox1">
                <vt:RadioButton runat="server" Text="Breakfast" Top="5px" Left="5px" ID="RadioButton1" Font-Names="Calibri Light"></vt:RadioButton>
                <vt:RadioButton runat="server" Text="Lunch" Top="25px"  Left="5px" ID="RadioButton2" Font-Names="Calibri Light"></vt:RadioButton>
                <vt:RadioButton runat="server" Text="Dinner" Top="45px"  Left="5px" ID="RadioButton3" Font-Names="Calibri Light"></vt:RadioButton>
           </vt:GroupBox>

           <vt:CheckBox runat="server" Text="Vegetarian" Left="445px" Top="49px" ID="CheckBox1" Font-Names="Calibri Light"></vt:CheckBox>

           <vt:Button runat="server" Text="Submit" Top="93px" Left="450px" BackColor="DarkCyan" ID="Button2" Height="36px" Width="80px" Font-Names="Calibri Light" Font-Size="12pt" ></vt:Button>

           <vt:Label runat="server" Text="Orders List: " Top="130px" left="30px"  ID="Label13" Font-Bold="true" Font-Names="Calibri Light"></vt:Label>

           <vt:ComboBox runat="server" ID="ComboBox1" Top="145px" Left="30px"   Width="500px"></vt:ComboBox>

         </vt:Composite>--%>

    <vt:ComponentManager runat="server" ID="cm">
        <vt:Timer runat="server" ID="Timer1" Interval="3000" TickAction="Composite1\Timer1_Tick" Enabled="true"></vt:Timer>
    </vt:ComponentManager>
   
</asp:Content>
