<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<APIData.Models.BaseNode>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:TCompositeView runat="server" ID="componentHeader" Dock="Fill" >
    <vt:Panel runat="server" ID="panelHeader" Height="100px" Dock="Top" >
        <vt:Label runat="server" Text="<%# Model.DisplayName %>" ID="ComponentTitle" Height="35" Dock="Top" />
        <vt:Label runat="server" Text="<%# Model.Description %>" ID="Description" Height="35" Dock="Top" />
    </vt:Panel>
    <vt:Panel runat="server" ID="examples" Dock="Fill" AutoScroll="true" >
            <vt:Repeater runat="server" DataSource="<%# Model.Examples %>" >
                <ItemTemplate>
                    <vt:PartialControl runat="server" ID="componentHeader" Controller="Example" Action="Example" Dock="Fill" >
                        <vt:PartialArgument runat="server" Name="element" Value='<%# Model.Name %>' />
                        <vt:PartialArgument runat="server" Name="member" Value='<%# Eval("Member") %>' />
                        <vt:PartialArgument runat="server" Name="exampleName" Value='<%# Eval("ID") %>' />
                        <vt:PartialArgument runat="server" Name="exampleController" Value='<%# Eval("ExampleController") %>' />
                        <vt:PartialArgument runat="server" Name="exampleAction" Value='<%# Eval("ExampleAction") %>' />
                        <vt:PartialArgument runat="server" Name="displayName" Value='<%# Eval("DisplayName") %>' />
                        <vt:PartialArgument runat="server" Name="description" Value='<%# Eval("Description") %>' />
                    </vt:PartialControl>
                </ItemTemplate>
            </vt:Repeater>
    </vt:Panel>
</vt:TCompositeView>
