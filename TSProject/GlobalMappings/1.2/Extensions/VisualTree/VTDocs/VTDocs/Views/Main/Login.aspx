﻿<%@ Page Title="Index" Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" ID="test" AcceptButtonID="cmdOK" CancelButtonID="cmdCancel" ControlBox="False" WindowBorderStyle="FixedDialog" Opacity="1" StartPosition="CenterScreen" KeyPreview="True" AutoSize="True" BackgroundImageLayout="None" Text="Login" Top="232px" Left="189px" BackColor="Window" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="159px" Width="458px">
        <vt:Label runat="server" TextAlign="TopLeft" AutoSize="True" Text="User ID:" Top="20px" Left="208px" ID="lblUserName" BackColor="Window" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="17px" TabIndex="6" Width="73px">
        </vt:Label>
        <vt:Label runat="server" TextAlign="TopLeft" AutoSize="True" Text="Password:" Top="48px" Left="208px" ID="lblPassword" BackColor="Window" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="17px" TabIndex="7" Width="73px">
        </vt:Label>
        <vt:Label runat="server" TextAlign="TopLeft" AutoSize="True" Text="Environment:" Top="75px" Left="208px" ID="lblEnvironment" BackColor="Window" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="17px" TabIndex="8" Width="73px">
        </vt:Label>
        <vt:TextBox runat="server" PasswordChar="*" ImeMode="Disable" MaxLength="32767" AutoSize="True" Top="42px" Left="288px" ID="txtPassword" BorderStyle="Inset" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="22px" TabIndex="1" Width="161px">
        </vt:TextBox>
        <vt:Button runat="server" TextAlign="MiddleCenter" AutoSize="True" Text="Cancel" Top="124px" Left="369px" ClickAction="Main\OnLoginCancelClick" ID="cmdCancel" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="26px" TabIndex="4" Width="72px">
        </vt:Button>
        <vt:Button runat="server" TextAlign="MiddleCenter" AutoSize="True" Text="OK" Top="124px" Left="292px" ClickAction="Main\OnLoginOkClick" ID="cmdOK" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="26px" TabIndex="3" Width="72px">
        </vt:Button>
        <vt:TextBox runat="server" PasswordChar="" MaxLength="32767" AutoSize="True" Top="14px" Left="288px" ID="txtUserID" BorderStyle="Inset" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="22px" Width="161px">
        </vt:TextBox>
        <vt:ComboBox runat="server" Text="" DropDownStyle="DropDownList" DisplayMember="text" ValueMember="value" AutoSize="True" Top="70px" Left="288px" ID="cboEnvironment" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Black" Height="21px" TabIndex="2" Width="161px">
        </vt:ComboBox>
    </vt:WindowView>
</asp:Content>
