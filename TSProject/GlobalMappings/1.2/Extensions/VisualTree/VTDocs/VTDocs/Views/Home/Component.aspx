<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<APIData.Models.TypeNode>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>

<vt:CompositeView runat="server" Text="" ID="componentCompositeView" Dock="Fill" LoadAction="Component\ComponentCompositeView_Init">
    <vt:PartialControl runat="server" ID="componentHeader" Controller="Component" Action="ComponentHeader" Dock="Top" Height="350px" CssClass="ComponentHeader">
        <vt:PartialArgument runat="server" Name="elementID" Value="<%# Model.Name %>" />
        <vt:PartialArgument runat="server" Name="propertyID" />
    </vt:PartialControl>


    <vt:Tab runat="server" ID="panelMembers" Dock="Fill" Tag='<%# Model.Name %>' HistoryEnabled="true" CssClass="examples-holder">
        <TabItems>
            <vt:TabItem runat="server" ID="panelProperties" Text="Properties" Tag='<%# Model.Name %>' Collapsible="true" Dock="Top" ShowHeader="true" AutoScroll="true" Height="300" Index="2">
                <vt:Repeater runat="server" DataSource="<%# Model.Properties %>">
                    <ItemTemplate>
                        <vt:Panel runat="server" Text='<%# Eval("Name") %>' Collapsible="true" AutoScroll="true" ShowHeader="true" Dock="Top" Height="500" Collapsed="true" Tag='<%# Eval("Name") %>' BeforeExpandAction="Component\LoadPropertyData" CssClass='<%# "Property-" + Eval("Name") %>'>

                            <vt:Label runat="server" Dock="Top" Text=''></vt:Label>
                        </vt:Panel>
                    </ItemTemplate>
                </vt:Repeater>

            </vt:TabItem>

            <vt:TabItem runat="server" ID="panelMethods" Text="Methods" Tag='<%# Model.Name %>' Collapsible="true" AutoScroll="true" ShowHeader="true" Dock="Top" Height="200" Index="1">
                <vt:Repeater runat="server" DataSource="<%# Model.Methods %>">
                    <ItemTemplate>
                        <vt:Panel runat="server" Text='<%# Eval("Name") %>' Collapsible="true" AutoScroll="true" ShowHeader="true" Dock="Top" Height="500" Collapsed="true" Tag='<%# Eval("Name") %>' BeforeExpandAction="Component\LoadMethodData" CssClass='<%# "Method-" + Eval("Name") %>'>
                            <vt:Label runat="server" Dock="Top" Text=''></vt:Label>
                        </vt:Panel>
                    </ItemTemplate>
                </vt:Repeater>
            </vt:TabItem>

            <vt:TabItem runat="server" ID="panelEvents" Text="Events" Tag='<%# Model.Name %>' Collapsible="true" AutoScroll="true" ShowHeader="true" Dock="Top" Height="200" Index="0">
                <vt:Repeater runat="server" DataSource="<%# Model.Events %>">
                    <ItemTemplate>
                        <vt:Panel runat="server" Text='<%# Eval("Name") %>' Collapsible="true" AutoScroll="true" ShowHeader="true" Dock="Top" Height="500" Collapsed="true" Tag='<%# Eval("Name") %>' BeforeExpandAction="Component\LoadEventData" CssClass='<%# "Event-" +  Eval("Name") %>'>
                            <vt:Label runat="server" Dock="Top" Text=''></vt:Label>
                        </vt:Panel>
                    </ItemTemplate>
                </vt:Repeater>
            </vt:TabItem>
        </TabItems>
    </vt:Tab>
</vt:CompositeView>
