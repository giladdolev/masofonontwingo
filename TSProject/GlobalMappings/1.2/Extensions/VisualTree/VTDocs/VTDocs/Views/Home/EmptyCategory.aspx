<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<APIData.Models.BaseNode>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Text="" ID="emptyCategoryCompositeView" Dock="Fill">
    <vt:Label runat="server" Text='<%# Model.Name %>'  Height="26" BorderStyle="Solid" Dock="Top" />
</vt:CompositeView>
