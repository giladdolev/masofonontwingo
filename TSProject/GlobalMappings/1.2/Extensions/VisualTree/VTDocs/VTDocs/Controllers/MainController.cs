﻿using APIData.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace VTDocs.Controllers
{
    /// <summary>
    /// Main Controller
    /// </summary>
    public class MainController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Main()
        {
            DoMain();

            return this.View();
        }

        /// <summary>
        /// Does the main behavior.
        /// </summary>
        private async void DoMain()
        {
            WindowElement loginWindow = VisualElementHelper.CreateFromView<WindowElement>("Main", "Login");

            await loginWindow.ShowDialog();

            WindowElement index = VisualElementHelper.CreateFromView<WindowElement>("Home", "Index");

            // We don't need to wait here
            index.ShowDialog();
        }

        /// <summary>
        /// Gets the login view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return this.View();
        }

        /// <summary>
        /// Called when login cancel click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoginCancelClick(object sender, EventArgs e)
        {
            ((WindowElement)this.GetRootVisualElement()).Close();
        }

        /// <summary>
        /// Called when login ok click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnLoginOkClick(object sender, EventArgs e)
        {
            ((WindowElement)this.GetRootVisualElement()).Close();
        }
    }
}