using APIData.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.MVC;

namespace VTDocs.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeMobileController : HomeController
    {
        /// <summary>
        /// Adds the API.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        protected override void AddAPI(TreeItem node)
        {
            if (node == null || !node.IsLeaf)
            {
                return;
            }

            
            string elementName = Convert.ToString(node.Tag);
            string customView = node.GetAttribute("CUSTOM_VIEW") as String;

            if (!string.IsNullOrWhiteSpace(elementName))
            {
                var holder = new PanelElement();
                holder.Controls.Clear();

                var elementView = GetElementView(elementName, customView);
                if (elementView != null)
                {
                    holder.Controls.Add(elementView);
                }

                var carousel = this.GetVisualElementById<TCarouselElement>("carousel");
                var collection = (carousel.Children as ControlElementCollection);


                if (collection.Count > 1)
                {
                    collection.RemoveAt(1);
                }
                
                (carousel.Children as ControlElementCollection).Insert(1, holder);
                carousel.SelectedIndex = 1;

                //ApplicationElement.Current.Add(holder, true);
            }

           
        }

        /// <summary>
        /// Handles the AfterSelectAction event of the tvControls control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TreeEventArgs"/> instance containing the event data.</param>
        public override void tvControls_AfterSelectAction(object sender, TreeEventArgs e)
        {
            AddAPI(e.Item as TreeItem);
        }
    }
}