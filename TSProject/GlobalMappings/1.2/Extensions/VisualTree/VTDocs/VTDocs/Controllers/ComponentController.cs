using APIData.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Linq;

namespace VTDocs.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class ComponentController : Controller
    {

        /// <summary>
        /// Components the specified element identifier.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <returns></returns>
        public ActionResult Component(string elementID)
        {
            TypeNode typeNode = APIDataServices.GetTypeNodeModel(elementID);
            if (typeNode == null)
            {
                return this.View("EmptyCategory", new BaseNode() { Name = elementID });
            }            
            return this.View(typeNode);
        }

        /// <summary>
        /// Components the property.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <returns></returns>
        public ActionResult ComponentHeader(string elementID)
        {
            TypeNode typeNode = APIDataServices.GetTypeNodeWithExamplesModel(elementID);

            return this.View(typeNode);
        }

        /// <summary>
        /// Components the property.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="propertyID">The property identifier.</param>
        /// <returns></returns>
        public ActionResult ComponentProperty(string elementID, string propertyID)
        {
            PropertyNode propertyNode = APIDataServices.GetPropertyNodeWithExamplesModel(elementID, propertyID);

            return this.View("ComponentHeader", propertyNode);
        }

        /// <summary>
        /// Components the method.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="propertyID">The property identifier.</param>
        /// <returns></returns>
        public ActionResult ComponentMethod(string elementID, string propertyID)
        {
            MethodNode methodNode = APIDataServices.GetMethodNodeWithExamplesModel(elementID, propertyID);

            return this.View("ComponentHeader", methodNode);
        }

        /// <summary>
        /// Components the event.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="propertyID">The property identifier.</param>
        /// <returns></returns>
        public ActionResult ComponentEvent(string elementID, string propertyID)
        {
            EventNode eventNode = APIDataServices.GetEventNodeWithExamplesModel(elementID, propertyID);

            return this.View("ComponentHeader", eventNode);
        }

        /// <summary>
        /// Loads the member data.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void LoadPropertyData(object sender, EventArgs e)
        {
            LoadMemberData(sender, "ComponentProperty");
        }

        private void LoadMethodData(object sender, EventArgs e)
        {
            LoadMemberData(sender, "ComponentMethod");
        }

        private void LoadEventData(object sender, EventArgs e)
        {
            LoadMemberData(sender, "ComponentEvent");
        }

        /// <summary>
        /// Loads the member data.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="action">The action.</param>
        private void LoadMemberData(object sender, string action)
        {
            PanelElement memberPanel = sender as PanelElement;
            if (memberPanel != null && memberPanel.Controls.Count == 1 && memberPanel.Controls[0] is LabelElement)
            {
                VisualElement root = this.GetRootVisualElement();
                IDictionary<string, object> arguments = new Dictionary<string, object>();
                arguments.Add("elementID", (memberPanel.ParentElement as PanelElement).Tag);
                arguments.Add("propertyID", memberPanel.Tag);
                ControlElement elementView = VisualElementHelper.CreateFromView<ControlElement>("Component", action, null,arguments,null);
                memberPanel.Controls.Clear();
                memberPanel.Controls.Add(elementView);
            }
        }

        protected void ComponentCompositeView_Init(object sender, EventArgs e)
        {
            ControlElement panelMembers = this.GetVisualElementById<ControlElement>("panelMembers");

            string elementID = panelMembers.Tag as string;
            TypeNode typeNode = APIDataServices.GetTypeNodeWithAllExamplesModel(elementID);

            ControlElement panelProperties = this.GetVisualElementById<ControlElement>("panelProperties");
            SetGreyOutTitle(panelProperties, typeNode);

            ControlElement panelMethods = this.GetVisualElementById<ControlElement>("panelMethods");
            SetGreyOutTitle(panelMethods, typeNode);

            ControlElement panelEvents = this.GetVisualElementById<ControlElement>("panelEvents");
            SetGreyOutTitle(panelEvents, typeNode);
            
        }

        private void SetGreyOutTitle(ControlElement container, TypeNode typeNode)
        {
            if (container == null || typeNode == null)
            {
                return;
            }

            foreach (VisualElement item in container.Children)
            {
                ControlElement panel = item as ControlElement;
                if (panel != null && (typeNode.Examples == null || typeNode.Examples.All(x => x==null || x.Member != panel.Text)))
                {
                    panel.ForeColor = System.Drawing.Color.Gray;
                    panel.Text += " (No examples)";
                }
            }
        }

    }
}