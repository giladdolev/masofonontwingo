using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace VTDocs.Controllers
{
    public class VisualTreeController : Controller
    {
        /// <summary>
        /// Processes the events.
        /// </summary>
        /// <param name="appId">The application identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{appId}")]
        public ActionResult ProcessEvents(string appId)
        {
            // Return the visual result after processing the events
            return this.GetEventsVisualResult(appId);
        }

        /// <summary>
        /// Elements the resource.
        /// </summary>
        /// <param name="appId">The application identifier.</param>
        /// <param name="elementId">The element identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public ActionResult ElementResource(string appId, string elementId, string resourceId)
        {
            return this.GetElementResourceResult(appId, elementId, resourceId);
        }

        /// <summary>
        /// Images the specified string file identifier.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public ActionResult Resource(string resourceId)
        {
            // Try to resolve resource id
            ResourceReference resourceReference = ResourceReference.Resolve(resourceId);

            // If there is a valid resource reference
            if (resourceReference != null)
            {
                // Return result
                return resourceReference.GetResult();
            }

            // Return the empty result
            return new EmptyResult();
        }

        
    }
}