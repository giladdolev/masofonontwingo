using VTDocs.Models;
using System;
using System.Drawing;
using System.Web.Mvc;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace VTDocs.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class Composite1Controller : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <returns></returns>
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult CompositeIndex()
        {
            return this.View(new Composite1());
        }
        private Composite1 ViewModel
        {
            get { return this.GetRootVisualElement() as Composite1; }
        }

        public void OnLoad(object sender, EventArgs e)
        {
            RadioButtonElement RdoBreakFast = this.GetVisualElementById<RadioButtonElement>("RdoBreakFast");
            //LabelElement lblLog1 = this.GetVisualElementById<LabelElement>("lblLog1");
            //lblLog1.Text = "BackColor value: " + TestedButton1.BackColor.ToString();

            //ButtonElement TestedButton2 = this.GetVisualElementById<ButtonElement>("TestedButton2");
            //LabelElement lblLog2 = this.GetVisualElementById<LabelElement>("lblLog2");
            //lblLog2.Text = "BackColor value: " + TestedButton2.BackColor.ToString();
            MessageBox.Show("" + RdoBreakFast.Text);
        }

   
        public void btnSubmit_Click(object sender, EventArgs e)
        {
            RadioButtonElement rdoBreakfast = this.GetVisualElementById<RadioButtonElement>("rdoBreakfast");
            RadioButtonElement rdoLunch = this.GetVisualElementById<RadioButtonElement>("rdoLunch");
            RadioButtonElement rdoDinner = this.GetVisualElementById<RadioButtonElement>("rdoDinner");

            TextBoxElement txtFirstName = this.GetVisualElementById<TextBoxElement>("txtFirstName");
            TextBoxElement txtLastName = this.GetVisualElementById<TextBoxElement>("txtLastName");

            CheckBoxElement chkVegetarian = this.GetVisualElementById<CheckBoxElement>("chkVegetarian");
            ComboBoxElement cmbOrdersList = this.GetVisualElementById<ComboBoxElement>("cmbOrdersList");
            
            LabelElement lblLog = this.GetVisualElementById<LabelElement>("lblLog");
            String mealType;
            String vegetarian = "";
            if(rdoBreakfast.IsChecked || rdoLunch.IsChecked || rdoDinner.IsChecked)
            {

                if(rdoBreakfast.IsChecked)
                    mealType = "Breakfast";
                else if(rdoLunch.IsChecked)
                    mealType = "Lunch";
                else
                    mealType = "Dinner";

                if (txtFirstName.Text != "" && txtLastName.Text != "")
                {
                    if (cmbOrdersList.FindString(txtFirstName.Text + " " + txtLastName.Text) == -1)
                    {
                        if (chkVegetarian.IsChecked)
                            vegetarian = "yes";
                        else
                            vegetarian = "no";

                        cmbOrdersList.Items.Add(txtFirstName.Text + " " + txtLastName.Text + ", Vegetarian: " + vegetarian + ", " + mealType);
                        lblLog.ForeColor = Color.Green;
                        lblLog.Text = txtFirstName.Text + " " + txtLastName.Text + "meal reservation was added successfully!";
                    }
                    else
                    {
                        lblLog.ForeColor = Color.Red;
                        lblLog.Text = "We already have a reservation for " + txtFirstName.Text + " " + txtLastName.Text;
                    }

                }
                else
                {
                    lblLog.ForeColor = Color.Red;
                    lblLog.Text = "Please enter full name";
                }
            }
            else
            {
                lblLog.ForeColor = Color.Red;
                lblLog.Text = "Please select meal type";
            }

           
        }
        public void Timer1_Tick(object sender, EventArgs e)
        {
            TimerElement timer = this.GetVisualElementById<TimerElement>("Timer1");
            
            if (timer != null)
            {
                timer.Enabled = false;
            }
            else
            {
                MessageBox.Show(String.Format("Timer Tick event fired\nTimer element is {0}", timer == null ? "null" : timer.ID));
            }
        }
    }
}