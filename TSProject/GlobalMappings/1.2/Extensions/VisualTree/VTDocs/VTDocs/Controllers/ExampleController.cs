﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Utilities;
using VTDocs.Models;
using VTDocs.Utils;

namespace VTDocs.Controllers
{
    public class ExampleController : Controller
    {
        internal static readonly string EXAMPLES_PATH = HostingEnvironment.MapPath("~/Examples");
        internal static readonly string CONTROLLERS_DIR = "Controllers";
        internal static readonly string VIEWS_DIR = "Views";
        internal static readonly string MODELS_DIR = "Models";

        /// <summary>
        /// Examples the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="member">The member.</param>
        /// <param name="exampleName">Name of the example.</param>
        /// <param name="exampleController">The examle controller.</param>
        /// <param name="exampleAction">The example action.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="description">The description.</param>
        /// <returns></returns>
        public ActionResult Example(string element, string member, string exampleName, string exampleController, string exampleAction, string displayName, string description = "")
        {
            if (string.IsNullOrEmpty(element) || string.IsNullOrEmpty(exampleName))
                return View();
            ExampleViewModel model = new ExampleViewModel(element, member, exampleName, exampleController, exampleAction, displayName, description);
            LoadExample(model);
            return View(model);
        }


        /// <summary>
        /// Loads the example.
        /// </summary>
        /// <param name="model">The model.</param>
        private void LoadExample(ExampleViewModel model)
        {
            string modelPath = "";
            string viewPath = "";
            string controllerPath = "";

            string exPath = GetExamplePath(model.Element, model.Member, model.ID);
            if (!Directory.Exists(exPath))
            {
                return; //no such example  //TODO: LOG
            }

            modelPath = GetFirstFilePath(exPath, MODELS_DIR);
            if (!string.IsNullOrEmpty(modelPath) && System.IO.File.Exists(modelPath))
            {
                model.FormattedModelContent = HighLighterUtils.CSharpToHtml(System.IO.File.ReadAllText(modelPath));
            }
            viewPath = GetFirstFilePath(exPath, VIEWS_DIR);
            if (!string.IsNullOrEmpty(viewPath) && System.IO.File.Exists(viewPath))
            {
                model.FormattedViewContent = HighLighterUtils.AspxToHtml(System.IO.File.ReadAllText(viewPath));
            }
            controllerPath = GetFirstFilePath(exPath, CONTROLLERS_DIR);
            if (!string.IsNullOrEmpty(controllerPath) && System.IO.File.Exists(controllerPath))
            {
                model.FormattedControllerContent = HighLighterUtils.CSharpToHtml(System.IO.File.ReadAllText(controllerPath));
            }


        }

        /// <summary>
        /// Gets the example path.
        /// </summary>
        /// <param name="elementId">The element identifier.</param>
        /// <param name="member">The member.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        internal static string GetExamplePath(string elementId, string member, string id)
        {
            string exPath = Path.Combine(EXAMPLES_PATH, elementId);
            //if (!string.IsNullOrEmpty(member))
            //{
            //    exPath = Path.Combine(exPath, member);
            //}
            if (!string.IsNullOrEmpty(id))
            {
                exPath = Path.Combine(exPath, id);
            }
            return exPath;
        }

        /// <summary>
        /// Gets the first file path.
        /// </summary>
        /// <param name="exPath">The ex path.</param>
        /// <param name="folderName">Name of the folder.</param>
        /// <returns></returns>
        private static string GetFirstFilePath(string exPath, string folderName)
        {
            string returnedPath = "";
            returnedPath = Path.Combine(exPath, folderName);
            if (!Directory.Exists(returnedPath))
            {
                return returnedPath;
            }

            returnedPath = Directory.GetFiles(returnedPath).FirstOrDefault();
            return returnedPath;
        }

        /// <summary>
        /// Handles the Click event of the btnPreview control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnPreview_Click(object sender, EventArgs e)
        {
            VisualElement root = this.GetRootVisualElement();
            Dictionary<string, object> argsDictionary = new Dictionary<string, object>();
            argsDictionary.Add("ElementName", root.GetAttribute("ElementID"));
            argsDictionary.Add("ExampleName", root.GetAttribute("ExampleName"));
            string action = root.GetAttribute("ExampleAction") as string;
            string controller = root.GetAttribute("ExampleController") as string;
            string exampleName = root.GetAttribute("ExampleName") as string;

            WindowElement element = VisualElementHelper.CreateFromView<WindowElement>(controller, action, null, argsDictionary, null);

            if (element != null)
            {
                element.CssClass = string.IsNullOrWhiteSpace(element.CssClass) ? exampleName : element.CssClass + " " + exampleName;
                element.Show();
            }
            else
            {
               MessageBox.Show("Missing Example Controller");
            }
        }
	            
    
        /// <summary>
        /// Handles the Click event of the btnDownload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnDownload_Click(object sender, EventArgs e)
        {
            VisualElement root = this.GetRootVisualElement();
            BrowserUtils.NavigateUrl(String.Format("/exampledownload.ashx?elementId={0}&member={1}&id={2}",
                root.GetAttribute("ElementID"), root.GetAttribute("Member"), root.GetAttribute("ExampleName")));
        }

    }
}