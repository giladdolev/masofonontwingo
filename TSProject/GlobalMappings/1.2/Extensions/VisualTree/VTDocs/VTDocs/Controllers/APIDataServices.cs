﻿using APIData.Models;
using System;
using System.Linq;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Serialization;
using VTDocs.Utils;

namespace VTDocs.Controllers
{
    public class APIDataServices
    {
        /// <summary>
        /// Gets the documentation.
        /// </summary>
        /// <returns></returns>
        internal static Documentation GetDocumentation()
        {
            Documentation documentation = CommonUtilities.GetData<Documentation>("Documentation", () => LoadItemsInternal<Documentation>("~/Structure.xml"));

            return documentation;
        }

        /// <summary>
        /// Gets the type node model.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <returns></returns>
        internal static TypeNode GetTypeNodeModel(string elementID)
        {
            TypesRoot types = CommonUtilities.GetData<TypesRoot>("TypesRoot", () => LoadItemsInternal<TypesRoot>("~/FormattedAPI.xml")); ;
            TypeNode typeNode = types.Types.FirstOrDefault(t => string.Compare(t.Name, elementID, true) == 0 );
            return typeNode;
        }

        private static Examples LoadExamples()
        {
            Examples examples = CommonUtilities.GetData<Examples>("Examples", () => LoadItemsInternal<Examples>("~/Examples.xml"));
            return examples;
        }

        /// <summary>
        /// Loads the items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static T LoadItemsInternal<T>(string path) where T : new()
        {
            try
            {

                string structurePath = HostingEnvironment.MapPath(path);

                return XmlUtils.LoadItems<T>(structurePath);
            }
            catch (Exception ex)
            {
                //TODO: log
            }

            return default(T);

        }


        /// <summary>
        /// Gets the type node with examples model.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <returns></returns>
        internal static TypeNode GetTypeNodeWithExamplesModel(string elementID)
        {
            // Get component descriptor by id
            TypeNode typeNode = APIDataServices.GetTypeNodeModel(elementID);

            LoadExamples(elementID, null, typeNode);

            return typeNode;
        }

        /// <summary>
        /// Gets the type node with examples model.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <returns></returns>
        internal static TypeNode GetTypeNodeWithAllExamplesModel(string elementID)
        {
            // Get component descriptor by id
            TypeNode typeNode = APIDataServices.GetTypeNodeModel(elementID);

            LoadExamples(elementID, null, typeNode, true);

            return typeNode;
        }

        /// <summary>
        /// Gets the property node with examples model.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="memberID">The property identifier.</param>
        /// <returns></returns>
        internal static PropertyNode GetPropertyNodeWithExamplesModel(string elementID, string memberID)
        {
            return GetMemberNodeWithExamplesModel(elementID, memberID, t => t.Properties.FirstOrDefault(p => p.Name == memberID));
        }

        /// <summary>
        /// Gets the method node with examples model.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="memberID">The property identifier.</param>
        /// <returns></returns>
        internal static MethodNode GetMethodNodeWithExamplesModel(string elementID, string memberID)
        {
            return GetMemberNodeWithExamplesModel(elementID, memberID, t => t.Methods.FirstOrDefault(p => p.Name == memberID));
        }

        /// <summary>
        /// Gets the event node with examples model.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="memberID">The property identifier.</param>
        /// <returns></returns>
        internal static EventNode GetEventNodeWithExamplesModel(string elementID, string memberID)
        {
            return GetMemberNodeWithExamplesModel(elementID, memberID, t => t.Events.FirstOrDefault(p => p.Name == memberID));
        }

        /// <summary>
        /// Gets the member node with examples model.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="propertyID">The property identifier.</param>
        /// <param name="getMember">The get member.</param>
        /// <returns></returns>
        internal static T GetMemberNodeWithExamplesModel<T>(string elementID, string propertyID, Func<TypeNode, T> getMember)
        {
            // Get component descriptor by id
            TypeNode typeNode = APIDataServices.GetTypeNodeModel(elementID);

            T memberNode = getMember(typeNode);
            LoadExamples(elementID, propertyID, memberNode as MemberNode);

            (memberNode as MemberNode).Type = elementID;

            return memberNode;
        }

        /// <summary>
        /// Loads the examples.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="propertyID">The property identifier.</param>
        /// <param name="typeNode">The type node.</param>
        private static void LoadExamples(string elementID, string propertyID, BaseNode typeNode, bool retrieveAllExamples = false)
        {
            // Load category
            Documentation documentation = GetDocumentation();
            Category category = XmlUtils.GetCategory(documentation.Categories, elementID);

            // Load examples
            Examples examples = LoadExamples();

            if (category != null && category.Examples != null && examples != null)
            {
                // Scan listed examples for specified component
                typeNode.Examples = retrieveAllExamples ? category.Examples.ToArray() : category.Examples.Where(t => t.Member == propertyID).ToArray();
                foreach (Example item in typeNode.Examples)
                {
                    // Find example data
                    Example example = examples.Items.FirstOrDefault(t => t.ID == item.ID);
                    if (example != null)
                    {
                        item.ExampleController = example.ExampleController;
                        item.ExampleAction = example.ExampleAction;
                        // Update extendeed example data in component descriptor
                        if (String.IsNullOrEmpty(item.DisplayName))
                        {
                            item.DisplayName = example.DisplayName;
                        }
                        if (String.IsNullOrEmpty(item.Description))
                        {
                            item.Description = example.Description;
                        }
                    }

                }
            }
        }
    }
}