using APIData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;

namespace VTDocs.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Returns The View For Index
        /// </summary>
        /// <param name="appId">The application identifier.</param>
        /// <returns></returns>
        public ActionResult Index(string appId = null)
        {
            return this.GetRestorableViewResult(appId, "appId");
        }

        /// <summary>
        /// Gets the structure list.
        /// </summary>
        /// <param name="categories">The categories.</param>
        /// <returns></returns>
        internal List<TreeItem> GetStructureList(Documentation categories, TreeItemCollection treeItems)
        {
            List<TreeItem> lsTreeItems = new List<TreeItem>();

            if (categories != null)
            {
                foreach (Category category in categories.Categories)
                {
                    CreateCategoryNode(category, treeItems);

                }
            }


            return lsTreeItems;

        }

        /// <summary>
        /// Creates the category node.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        protected static void CreateCategoryNode(Category category, TreeItemCollection treeItems)
        {
            TreeItem treeItemCategory = new TreeItem()
            {
                Text = category.DisplayName,
                Tag = category.ID,
                Icon = GetElementIcon(category),
                IsExpanded = true
            };
            treeItemCategory.SetAttribute("CUSTOM_VIEW", category.CustomView);
            treeItems.Add(treeItemCategory);
            treeItemCategory.ID = category.ID;
            if (category.Items != null)
            {
                foreach (Category item in category.Items)
                {
                    CreateCategoryNode(item, treeItemCategory.Items);
                }
            }
        }

        /// <summary>
        /// Gets the element icon.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        protected static UrlReference GetElementIcon(Category category)
        {
            if (category != null && !String.IsNullOrEmpty(category.Icon))
            {
                return new UrlReference("/Content/Elements/" + category.Icon);
            }
            return null;
        }

        /// <summary>
        /// Handles The Button1 Click Action
        /// </summary>
        /// <param name="sender">The Sender</param>
        /// <param name="e">The Event Arguments</param>
        public void LoadItems(object sender, EventArgs e)
        {
            TreeElement tree = this.GetVisualElementById<TreeElement>("tvControls");
            if (tree != null)
            {
                Documentation documentation = APIDataServices.GetDocumentation();
                tree.Items.Clear();
                GetStructureList(documentation, tree.Items);
            }
        }

        /// <summary>
        /// Handles the AfterSelectAction event of the tvControls control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TreeEventArgs"/> instance containing the event data.</param>
        public virtual void tvControls_AfterSelectAction(object sender, TreeEventArgs e)
        {
            AddAPI(e.Item as TreeItem);
        }

        /// <summary>
        /// Adds the API.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        protected virtual void AddAPI(TreeItem node)
        {
            if (node != null)
            {
                PanelElement panelRight = this.GetVisualElementById<PanelElement>("panelRight");
                if (panelRight != null)
                {
                    string elementName = Convert.ToString(node.Tag);
                    string customView = node.GetAttribute("CUSTOM_VIEW") as String;
                    panelRight.Controls.Clear();
                    ControlElement elementView = GetElementView(elementName, customView);
                    if (elementView != null)
                    {
                        panelRight.Controls.Add(elementView);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the element view.
        /// </summary>
        /// <param name="elementID">Name of the element.</param>
        /// <param name="customView">The custom view.</param>
        /// <returns></returns>
        protected ControlElement GetElementView(string elementID, string customView)
        {
            ControlElement elementView = GetCustomView(elementID, customView ?? "Component");
            return elementView;
        }

        /// <summary>
        /// Gets the custom view.
        /// </summary>
        /// <param name="elementID">The element identifier.</param>
        /// <param name="customView">The custom view.</param>
        /// <returns></returns>
        protected ControlElement GetCustomView(string elementID, string customView)
        {
            if (!String.IsNullOrEmpty(customView))
            {
                IDictionary<string, object> arguments = new Dictionary<string, object>();
                arguments.Add("elementID", elementID);
                ControlElement elementView = VisualElementHelper.CreateFromView<ControlElement>(customView, customView, null, arguments, null);

                return elementView;
            }
            return null;
        }

        /// <summary>
        /// Filters the controls tree by the textbox value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void txtFilter_TextChangedAction(object sender, EventArgs e)
        {
            ValueChangedArgs<string> args = e as ValueChangedArgs<string>;
            if (args == null)
            {
                return;
            }
            TreeElement tv = this.GetVisualElementById<TreeElement>("tvControls");
            if (string.IsNullOrEmpty(args.Value))
            {
                LoadItems(this, e);
                tv.Refresh();
                return;
            }
            TreeItem itemMatces = new TreeItem("Matches");
            TreeItem itemFiltered = new TreeItem("Filtered");

            foreach (var child in tv.GetNodes().Where(x => x.IsLeaf))
            {
                if (child.Text.ToLower().Contains(args.Value))
                {
                    itemMatces.Items.Add(child);
                }
                else
                {
                    itemFiltered.Items.Add(child);
                }
            }
            tv.Items.Clear();
            if (itemMatces.Items != null && itemMatces.Items.Count > 0)
            {
                itemMatces.Expand();
                tv.Items.Add(itemMatces);
            }
            if (itemFiltered.Items != null && itemFiltered.Items.Count > 0)
            {
                itemFiltered.Collapse();
                tv.Items.Add(itemFiltered);
            }
            tv.Refresh();
        }
    }
}