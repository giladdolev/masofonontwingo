Ext.onReady(function () {
    

    Ext.define('Ext.data.proxy.VT', {
    extend: 'Ext.data.proxy.Server',
    alias: 'proxy.vt',


 
    /**
    *   Override the request to integrate it to the VT pipeline
    */
    doRequest: function (operation, callback, scope) {

   
        var writer = this.getWriter(),
            request = this.buildRequest(operation, callback, scope);

        // Write the request
        if (operation.allowWrite()) {
            request = writer.write(request);
        }
        
        // Create event parameters
        var eventParams = {};

        // Set parameters and and action
        eventParams.params = request.getParams();
        eventParams.action = request.getAction();
        eventParams.data = request.getJsonData();
        eventParams.callback = VT_getCallbackId(this.createRequestCallback(request, operation, callback, scope), "bindingRequest");

        // Raise server event
        VT_queueEvent(this.config.serverId, "BindingRequest", eventParams);

        // Raise delayed events
        VT_raiseDelayedEvents(100);

        return request;
    },

    setSelection: function (selection) {
        if(selection != null) {
            // Raise server event
            VT_queueEvent(this.serverId, "BindingRequest", { action: 'current', params: { itemId: selection.id } });

            // Raise delayed events
            VT_raiseDelayedEvents(100);
        }
    },


    /**
    *   Override to enable proper execution of buildRequest.
    */
    buildUrl: function (request) {
        return '';
    },

    /**
    *   Builds the callback request
    */
    createRequestCallback: function (request, operation, callback, scope) {
        var me = this;

        return function (options, success, response) {
            me.processResponse(success, operation, request, response, callback, scope);
        };
    }
    });

});