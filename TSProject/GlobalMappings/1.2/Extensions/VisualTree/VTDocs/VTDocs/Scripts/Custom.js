﻿function SP_Button(value, metaData, record, rowIndex, colIndex, store, view, name) {
    try {
        // generate unique id for an element
        var id = Ext.id();

        Ext.defer(function () {
            var pnl = Ext.create('Ext.panel.Panel', {
                renderTo: id,
                layout: {
                    type: 'absolute',

                },
                height: 25,
                width: 100,
                x: 0,
                y: 0,
                items: [{
                    //Button
                    x: 0,
                    xtype: 'button',
                    text: view.ownerGrid.getButton1Text(),
                    hidden: view.ownerGrid.get1State(record),
                    listeners: {
                        click: function (e) {
                            var grid = window.Ext.getCmp(this.container.el.up("div.x-grid").id);
                            grid.fireEvent('clientEvent', grid.ownGrid, "click,btn1");
                        }
                    }
                },
                  {
                      //Button
                      xtype: 'button',
                      x: 40,
                      text: view.ownerGrid.getButton2Text(),
                      hidden: view.ownerGrid.get2State(record),
                      listeners: {
                          click: function (e) {
                              var grid = window.Ext.getCmp(this.container.el.up("div.x-grid").id);
                              grid.fireEvent('clientEvent', grid.ownGrid, "click,btn2");
                          }
                      }
                  }

                ],
            });

        }, 50);
        return Ext.String.format('<div id="{0}"></div>', id);
    } catch (ex) { }
}


