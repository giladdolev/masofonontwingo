﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestReport
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string reportFile = textBoxSaveTo.Text;
            if (!String.IsNullOrEmpty(baselineRootFolder.Text) && !String.IsNullOrEmpty(baselineRootFolder.Text) && !String.IsNullOrEmpty(reportFile))
            {
                TestResultsReportTemplate results = ReportProcessor.CollectData(baselineRootFolder.Text, testRootFolder.Text);

                string report = results.TransformText();

                File.WriteAllText(reportFile, report);

                Process.Start(reportFile);
            }
        }

        private void buttonBrowseBaseline_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                baselineRootFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void buttonBrowseTest_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                testRootFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxSaveTo.Text = saveFileDialog1.FileName;
            }
        }
    }
}
