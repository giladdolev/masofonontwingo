﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestReport
{
    public partial class TestResultsReportTemplate
    {
        public double PassRate { get; set; }
        public string BaselineRootFolder { get; set; }
        public string TestRootFolder { get; set; }
        public readonly List<BrowserTestResults> BrowserResults = new List<BrowserTestResults>();
    }
}
