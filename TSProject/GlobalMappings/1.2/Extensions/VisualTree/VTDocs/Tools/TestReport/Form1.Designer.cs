﻿namespace TestReport
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonBrowseBaseline = new System.Windows.Forms.Button();
            this.buttonBrowseTest = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.testRootFolder = new System.Windows.Forms.TextBox();
            this.baselineRootFolder = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.textBoxSaveTo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Baseline Root Folder";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Test Root Folder";
            // 
            // buttonBrowseBaseline
            // 
            this.buttonBrowseBaseline.Location = new System.Drawing.Point(577, 13);
            this.buttonBrowseBaseline.Name = "buttonBrowseBaseline";
            this.buttonBrowseBaseline.Size = new System.Drawing.Size(26, 23);
            this.buttonBrowseBaseline.TabIndex = 3;
            this.buttonBrowseBaseline.Text = "...";
            this.buttonBrowseBaseline.UseVisualStyleBackColor = true;
            this.buttonBrowseBaseline.Click += new System.EventHandler(this.buttonBrowseBaseline_Click);
            // 
            // buttonBrowseTest
            // 
            this.buttonBrowseTest.Location = new System.Drawing.Point(577, 44);
            this.buttonBrowseTest.Name = "buttonBrowseTest";
            this.buttonBrowseTest.Size = new System.Drawing.Size(26, 23);
            this.buttonBrowseTest.TabIndex = 5;
            this.buttonBrowseTest.Text = "...";
            this.buttonBrowseTest.UseVisualStyleBackColor = true;
            this.buttonBrowseTest.Click += new System.EventHandler(this.buttonBrowseTest_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 114);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Generate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(97, 114);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // testRootFolder
            // 
            this.testRootFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testRootFolder.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::TestReport.Properties.Settings.Default, "TestRoot", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.testRootFolder.Location = new System.Drawing.Point(161, 44);
            this.testRootFolder.Name = "testRootFolder";
            this.testRootFolder.Size = new System.Drawing.Size(419, 20);
            this.testRootFolder.TabIndex = 4;
            this.testRootFolder.Text = global::TestReport.Properties.Settings.Default.TestRoot;
            // 
            // baselineRootFolder
            // 
            this.baselineRootFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baselineRootFolder.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::TestReport.Properties.Settings.Default, "BaselineRoot", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.baselineRootFolder.Location = new System.Drawing.Point(161, 13);
            this.baselineRootFolder.Name = "baselineRootFolder";
            this.baselineRootFolder.Size = new System.Drawing.Size(419, 20);
            this.baselineRootFolder.TabIndex = 2;
            this.baselineRootFolder.Text = global::TestReport.Properties.Settings.Default.BaselineRoot;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(577, 75);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBoxSaveTo
            // 
            this.textBoxSaveTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSaveTo.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::TestReport.Properties.Settings.Default, "SaveAs", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxSaveTo.Location = new System.Drawing.Point(161, 75);
            this.textBoxSaveTo.Name = "textBoxSaveTo";
            this.textBoxSaveTo.Size = new System.Drawing.Size(419, 20);
            this.textBoxSaveTo.TabIndex = 9;
            this.textBoxSaveTo.Text = global::TestReport.Properties.Settings.Default.SaveAs;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Save Report To .htm file";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "htm";
            // 
            // Form1
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(633, 160);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBoxSaveTo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonBrowseTest);
            this.Controls.Add(this.testRootFolder);
            this.Controls.Add(this.buttonBrowseBaseline);
            this.Controls.Add(this.baselineRootFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Generate Report";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox baselineRootFolder;
        private System.Windows.Forms.Button buttonBrowseBaseline;
        private System.Windows.Forms.Button buttonBrowseTest;
        private System.Windows.Forms.TextBox testRootFolder;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBoxSaveTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

