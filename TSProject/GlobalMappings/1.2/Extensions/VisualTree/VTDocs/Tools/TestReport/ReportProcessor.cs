﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestReport
{
    static class ReportProcessor
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public static TestResultsReportTemplate CollectData(string baselineUrl, string testUrl)
        {
            TestResultsReportTemplate reportData = new TestResultsReportTemplate() { BaselineRootFolder = baselineUrl, TestRootFolder = testUrl };

            string[] snapshotFiles = Directory.GetFiles(testUrl, "*.png", SearchOption.AllDirectories);
            foreach (string snapshotFile in snapshotFiles)
            {
                CollectData(reportData, snapshotFile);
            }

            string[] baselineFiles = Directory.GetFiles(baselineUrl, "*.png", SearchOption.AllDirectories);
            reportData.PassRate = ((double)(baselineFiles.Length - snapshotFiles.Length) / baselineFiles.Length)* 100 ;
            return reportData;
        }

        private static void CollectData(TestResultsReportTemplate reportData, string snapshotFile)
        {
            string relativeSnapshotPath = snapshotFile.Substring(reportData.TestRootFolder.Length);
            string[] nameParts = relativeSnapshotPath.Split(new char[] {'\\'}, StringSplitOptions.RemoveEmptyEntries);
            if (nameParts.Length > 0)
            {
                string browser = nameParts[0];
                BrowserTestResults browserResults = GetOrCreate<BrowserTestResults>(reportData.BrowserResults, browser);

                if (browserResults != null && nameParts.Length > 1)
                {
                    string elementName = nameParts[1];
                    ElementTestResult elementResults = GetOrCreate<ElementTestResult>(browserResults.Elements, elementName);

                    if (elementResults != null && nameParts.Length > 4)
                    {
                        string categoryName = nameParts[2];
                        CategoryResults categoryResults = GetOrCreate<CategoryResults>(elementResults.Categories, categoryName);

                        if (categoryResults != null && nameParts.Length > 3)
                        {
                            string memberName = nameParts[3];
                            MemberTestResults memberResults = GetOrCreate<MemberTestResults>(categoryResults.Members, memberName);

                            if (memberResults != null)
                            {
                                string testName = nameParts[4];
                                CollectSnapshot(relativeSnapshotPath, browserResults, elementResults.Snapshots, testName);
                            }
                       }
                    }
                    else if (elementResults != null && nameParts.Length > 2)
                    {
                        string testName = nameParts[2];
                        CollectSnapshot(relativeSnapshotPath, browserResults, elementResults.Snapshots, testName);
                    }
                }
            }
        }

        private static void CollectSnapshot(string relativeSnapshotPath, BrowserTestResults browserResults, List<SnapshotDiffDefinitions> snapshots, string testName)
        {
            SnapshotDiffDefinitions snapshotDiff = GetOrCreate<SnapshotDiffDefinitions>(snapshots, testName);

            if (snapshotDiff != null)
            {
                snapshotDiff.TestImageUrl = relativeSnapshotPath;
                browserResults.FailedCount++;
            }
        }

        private static T GetOrCreate<T>(List<T> list, string elementName) where T : ElementBase, new()
        {
            T resultItem = list.FirstOrDefault(item => item.ElementName == elementName);
            if (resultItem == null)
            {
                resultItem = new T() { ElementName = elementName };
                list.Add(resultItem);
            }

            return resultItem;
        }
    }
}
