﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestReport
{
    public class BrowserTestResults : ElementBase
    {
        public readonly List<ElementTestResult> Elements = new List<ElementTestResult>();

        public int FailedCount
        {
            get;
            set;
        }
        
    }

    public class ElementTestResult : MemberTestResults
    {
        public ElementTestResult()
        {

        }
        public readonly List<CategoryResults> Categories = new List<CategoryResults>();
    }
    public class MemberTestResults : ElementBase
    {
        public readonly List<SnapshotDiffDefinitions> Snapshots = new List<SnapshotDiffDefinitions>();
    }

    public class TestResults : ElementBase
    {
        public readonly List<SnapshotDiffDefinitions> Snapshots = new List<SnapshotDiffDefinitions>();
    }

    public class CategoryResults : ElementBase
    {
        public readonly List<MemberTestResults> Members = new List<MemberTestResults>();
    }
    public class ElementBase
    {
        public string ElementName { get; set; }
    }
}
