﻿namespace WindowsFormsApplication8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBrowseAssembly = new System.Windows.Forms.Button();
            this.buttonBrowseXML = new System.Windows.Forms.Button();
            this.buttonBrowseOutPut = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonGo = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.textBoxXML = new System.Windows.Forms.TextBox();
            this.textBoxAssembly = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonBrowseAssembly
            // 
            this.buttonBrowseAssembly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowseAssembly.Location = new System.Drawing.Point(615, 37);
            this.buttonBrowseAssembly.Name = "buttonBrowseAssembly";
            this.buttonBrowseAssembly.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseAssembly.TabIndex = 1;
            this.buttonBrowseAssembly.Text = "Browse...";
            this.buttonBrowseAssembly.UseVisualStyleBackColor = true;
            this.buttonBrowseAssembly.Click += new System.EventHandler(this.buttonBrowseAssembly_Click);
            // 
            // buttonBrowseXML
            // 
            this.buttonBrowseXML.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowseXML.Location = new System.Drawing.Point(615, 74);
            this.buttonBrowseXML.Name = "buttonBrowseXML";
            this.buttonBrowseXML.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseXML.TabIndex = 1;
            this.buttonBrowseXML.Text = "Browse...";
            this.buttonBrowseXML.UseVisualStyleBackColor = true;
            this.buttonBrowseXML.Click += new System.EventHandler(this.buttonBrowseXML_Click);
            // 
            // buttonBrowseOutPut
            // 
            this.buttonBrowseOutPut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowseOutPut.Location = new System.Drawing.Point(615, 126);
            this.buttonBrowseOutPut.Name = "buttonBrowseOutPut";
            this.buttonBrowseOutPut.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseOutPut.TabIndex = 1;
            this.buttonBrowseOutPut.Text = "Browse...";
            this.buttonBrowseOutPut.UseVisualStyleBackColor = true;
            this.buttonBrowseOutPut.Click += new System.EventHandler(this.buttonBrowseOutPut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Assembly Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "XML Doc Path:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Output Path:";
            // 
            // buttonGo
            // 
            this.buttonGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo.Location = new System.Drawing.Point(615, 193);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(75, 23);
            this.buttonGo.TabIndex = 3;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutput.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApplication8.Properties.Settings.Default, "OutputPath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxOutput.Location = new System.Drawing.Point(111, 127);
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.Size = new System.Drawing.Size(484, 20);
            this.textBoxOutput.TabIndex = 0;
            this.textBoxOutput.Text = global::WindowsFormsApplication8.Properties.Settings.Default.OutputPath;
            // 
            // textBoxXML
            // 
            this.textBoxXML.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxXML.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApplication8.Properties.Settings.Default, "XMLDocFilePath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxXML.Location = new System.Drawing.Point(111, 77);
            this.textBoxXML.Name = "textBoxXML";
            this.textBoxXML.Size = new System.Drawing.Size(484, 20);
            this.textBoxXML.TabIndex = 0;
            this.textBoxXML.Text = global::WindowsFormsApplication8.Properties.Settings.Default.XMLDocFilePath;
            // 
            // textBoxAssembly
            // 
            this.textBoxAssembly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAssembly.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApplication8.Properties.Settings.Default, "AssemblyPath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxAssembly.Location = new System.Drawing.Point(111, 37);
            this.textBoxAssembly.Name = "textBoxAssembly";
            this.textBoxAssembly.Size = new System.Drawing.Size(484, 20);
            this.textBoxAssembly.TabIndex = 0;
            this.textBoxAssembly.Text = global::WindowsFormsApplication8.Properties.Settings.Default.AssemblyPath;
            // 
            // Form1
            // 
            this.AcceptButton = this.buttonGo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 242);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonBrowseOutPut);
            this.Controls.Add(this.buttonBrowseXML);
            this.Controls.Add(this.buttonBrowseAssembly);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.textBoxXML);
            this.Controls.Add(this.textBoxAssembly);
            this.MaximumSize = new System.Drawing.Size(10000, 280);
            this.MinimumSize = new System.Drawing.Size(731, 280);
            this.Name = "Form1";
            this.Text = "Generate API XML";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxAssembly;
        private System.Windows.Forms.TextBox textBoxXML;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Button buttonBrowseAssembly;
        private System.Windows.Forms.Button buttonBrowseXML;
        private System.Windows.Forms.Button buttonBrowseOutPut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

