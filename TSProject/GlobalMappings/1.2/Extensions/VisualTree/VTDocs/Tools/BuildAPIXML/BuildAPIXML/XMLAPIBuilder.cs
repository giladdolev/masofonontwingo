﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using APIData.Models;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication8
{
    public class XMLAPIBuilder
    {
        public string AssemblyPath { get; set; }
        public string XMLPath { get; set; }
        public string OutputPath { get; set; }

        public void Run()
        {
            if (string.IsNullOrEmpty(AssemblyPath) || string.IsNullOrEmpty(XMLPath) || string.IsNullOrEmpty(OutputPath))
                return;
            try
            {

                XmlDocument xmlCommentsDoc = new XmlDocument();
                xmlCommentsDoc.Load(XMLPath);
                TypesRoot types = new TypesRoot();
                Assembly assemblyFile = Assembly.LoadFrom(AssemblyPath);
                foreach (Type type in assemblyFile.GetTypes())
                {
                    TypeNode typeNode = new TypeNode()
                    {
                        Name = type.Name,
                        DisplayName = type.Name,
                        NS = type.Namespace,
                        Description = GetTypeDescription(type, xmlCommentsDoc),
                        BaseTypes = new List<TypeNode>(),
                        Properties = new List<PropertyNode>(),
                        Methods = new List<MethodNode>(),
                        Events = new List<EventNode>()
                    };

                    foreach (MethodInfo methodInfo in type.GetMethods().Where(IsValidMethodForDocumentation))
                    {
                        MethodNode memberNode = new MethodNode()
                        {
                            Name = methodInfo.Name,
                            Type = methodInfo.ReturnType.FullName,
                            Description = GetMethodDescription(methodInfo, xmlCommentsDoc),
                            Parameters = new List<ParameterNode>()
                        };

                        foreach (var parameterInfo in methodInfo.GetParameters())
                        {
                            memberNode.Parameters.Add(new ParameterNode()
                            {
                                Name = parameterInfo.Name,
                                Type = parameterInfo.ParameterType.FullName
                            });
                            //TODO Description
                        }
                        

                        typeNode.Methods.Add(memberNode);
                    }
                    foreach (EventInfo eventInfo in type.GetEvents())
                    {
                        EventNode memberNode = new EventNode()
                        {
                            Name = eventInfo.Name,
                            Type = eventInfo.EventHandlerType.FullName,
                            Description = GetEventDescription(eventInfo, xmlCommentsDoc)
                        };

                        //TODO EventArgs?

                        typeNode.Events.Add(memberNode);
                    }
                    foreach (PropertyInfo propertyInfo in type.GetProperties().Where(IsValidPropertyForDocumentation))
                    {
                        PropertyNode memberNode = new PropertyNode()
                        {
                            Name = propertyInfo.Name,
                            Type = propertyInfo.PropertyType.FullName,
                            Description = GetPropertyDescription(propertyInfo, xmlCommentsDoc)

                        };


                        typeNode.Properties.Add(memberNode);
                    }
                    typeNode.Properties.Sort();
                    typeNode.Methods.Sort();
                    typeNode.Events.Sort();
                    types.Types.Add(typeNode);
                }
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof (TypesRoot));
                    using (StreamWriter writer = new StreamWriter(OutputPath))
                    {
                        serializer.Serialize(writer, types);
                    }

                }
                catch (Exception)
                {

                }
            }
            catch (ReflectionTypeLoadException ex)
            {

                
            }

        }
        private static readonly string[] _propertiesBlackList = new string[] { @"\bHas([\w\d_]+)Listeners\b", @"\bPerform([\w\d_]+)"};
        private static bool IsValidPropertyForDocumentation(PropertyInfo property)
        {
            if (!property.IsSpecialName)
            {
                foreach (string pattern in _propertiesBlackList)
                {
                    Regex blackListRegex = new Regex(pattern, RegexOptions.None);
                    Match blackListedName = blackListRegex.Match(property.Name);
                    if (blackListedName != null && blackListedName.Success)
                    {
                        return false;
                    }

                }
                return true;
            }
            return false;
        }
        private static readonly string[] _methodsBlackList = new string[] {  };
        private static bool IsValidMethodForDocumentation(MethodInfo method)
        {
            if (!method.IsSpecialName)
            {
                foreach (string pattern in _methodsBlackList)
                {
		            Regex blackListRegex = new Regex(pattern, RegexOptions.None);
                    Match blackListedName = blackListRegex.Match(method.Name);
                    if (blackListedName != null && blackListedName.Success)
                    {
                        return false;
                    }

                }
                return true;
            }
            return false;
        }
        private static string GetTypeDescription(Type type, XmlDocument xmlCommentsDoc)
        {
            string description = "";
            string path = "T:" + type.FullName;

            var xmlDocMemberNode = GetXmlDocMemberNode(xmlCommentsDoc, path);
            return GetDescription(xmlDocMemberNode, description);
        }
        private static string GetMethodDescription(MethodInfo methodInfo, XmlDocument xmlCommentsDoc)
        {
            string description = "";
            string path = "M:" + methodInfo.DeclaringType.FullName + "." + methodInfo.Name;

            var xmlDocMemberNode = GetXmlDocMemberNode(xmlCommentsDoc, path);
            return GetDescription(xmlDocMemberNode, description);
        }
        private static string GetPropertyDescription(PropertyInfo propertyInfo, XmlDocument xmlCommentsDoc)
        {
            string description = "";
            string path = "P:" + propertyInfo.DeclaringType.FullName + "." + propertyInfo.Name;

            var xmlDocMemberNode = GetXmlDocMemberNode(xmlCommentsDoc, path);
            return GetDescription(xmlDocMemberNode, description);
        }
        private static string GetEventDescription(EventInfo eventInfo, XmlDocument xmlCommentsDoc)
        {
            string description = "";
            string path = "E:" + eventInfo.DeclaringType.FullName + "." + eventInfo.Name;

            var xmlDocMemberNode = GetXmlDocMemberNode(xmlCommentsDoc, path);
            return GetDescription(xmlDocMemberNode, description);
        }
        private static string GetDescription(XmlNode xmlDocMemberNode, string description)
        {
            if (xmlDocMemberNode != null)
            {
                if (xmlDocMemberNode.HasChildNodes)
                {
                    XmlNode summaryNode =
                        xmlDocMemberNode.ChildNodes.Cast<XmlNode>()
                            .FirstOrDefault(childNode => childNode.Name == "summary");
                    if (summaryNode != null)
                        description = summaryNode.InnerText;
                }
                else
                {
                    description = xmlDocMemberNode.InnerText;
                }
            }
            return description;
        }

        private static XmlNode GetXmlDocMemberNode(XmlDocument xmlCommentsDoc, string path)
        {
            XmlNode xmlDocMemberNode = xmlCommentsDoc.SelectSingleNode(
                "//member[starts-with(@name, '" + path + "')]");
            return xmlDocMemberNode;
        }
    }
}
