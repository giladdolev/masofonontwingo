﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication8
{
    static class Program
    {

        private static Dictionary<string, string> argsList;


        private static void InitArgsList()
        {
            argsList = new Dictionary<string, string>() {
                  {"/assemblyPath", "[Optional] Full path to .NET assembly (.dll) file"}
                 , {"/xmlPath", "[Optional] Full path to the XML Comments File"}
                 , {"/outputPath", "[Optional] Full path for output file"}
                 , {"/noUI", "[Optional] Run without UI (only if all paths were supplied)"}
            };
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            InitArgsList();
            Dictionary<string, string> providedArgs = ParseArgs(args);
             XMLAPIBuilder builder = new XMLAPIBuilder()
                {
                    AssemblyPath = providedArgs["/assemblyPath"],
                    XMLPath = providedArgs["/xmlPath"],
                    OutputPath = providedArgs["/outputPath"]
                };
            if (!string.IsNullOrEmpty(providedArgs["/noUI"]))
            {
               
                builder.Run();
                return;
            }
            Application.Run(new Form1(builder));
        }

        private static Dictionary<string, string> ParseArgs(string[] args)
        {
            Dictionary<string, string> returnDictionary = argsList.Keys.ToDictionary(key => key, key => "");

            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i].ToUpper())
                {
                    case "/ASSEMBLYPATH":
                        if (i + 1 < args.Length)
                            returnDictionary["/assemblyPath"] = args[i + 1];
                        break;
                    case "/XMLPATH":
                        if (i + 1 < args.Length)
                            returnDictionary["/xmlPath"] = args[i + 1];
                        break;
                    case "/OUTPUTPATH":
                        if (i + 1 < args.Length)
                            returnDictionary["/outputPath"] = args[i + 1];
                        break;
                    case "/NOUI":
                        returnDictionary["/noUI"] = "True";
                        break;
                }
            }

            return returnDictionary;
        }
    }
}
