﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication8
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        public Form1(XMLAPIBuilder builder) :this()
        {
            textBoxAssembly.Text = builder.AssemblyPath;
            textBoxXML.Text = builder.XMLPath;
            textBoxOutput.Text = builder.OutputPath;


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonBrowseAssembly_Click(object sender, EventArgs e)
        {
            openFileDialog1.Reset();
            openFileDialog1.Title = "Please Selected The Assembly File";
            openFileDialog1.Filter = "Assembly File | *.dll";
            openFileDialog1.Multiselect = false;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxAssembly.Text = openFileDialog1.FileName;
                
            }
        }

        private void buttonBrowseXML_Click(object sender, EventArgs e)
        {
            openFileDialog1.Reset();
            openFileDialog1.Title = "Please Selected The XML File";
            openFileDialog1.Filter = "XML File | *.xml";
            openFileDialog1.Multiselect = false;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxXML.Text = openFileDialog1.FileName;

            }
        }

        private void buttonBrowseOutPut_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Reset();
            saveFileDialog1.FileName = "FormattedAPI.xml";
            saveFileDialog1.Title = "Please Selected Output Path";
            saveFileDialog1.Filter = "XML File | *.xml";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxOutput.Text = saveFileDialog1.FileName;

            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            XMLAPIBuilder builder = new XMLAPIBuilder()
            {
                AssemblyPath = textBoxAssembly.Text,
                XMLPath = textBoxXML.Text,
                OutputPath =  textBoxOutput.Text
            };

            try
            {
                builder.Run();

                MessageBox.Show("Done!", this.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        
    }
}
