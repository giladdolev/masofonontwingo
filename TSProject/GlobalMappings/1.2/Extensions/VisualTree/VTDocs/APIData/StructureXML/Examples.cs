﻿
namespace APIData.Models
{
    public class Examples
    {
        [System.Xml.Serialization.XmlElementAttribute("Example")]
        public Example[] Items { get; set; }
    }
}
