﻿using System.Xml.Serialization;

namespace APIData.Models
{
    public class Item
    {
        [XmlAttribute("ID")]
        public string ID { get; set; }
        [XmlAttribute("DisplayName")]
        public string DisplayName { get; set; }
        [XmlAttribute("Description")]
        public string Description { get; set; }
        [XmlAttribute("CustomView")]
        public string CustomView { get; set; }
    }
}
