﻿using System.ComponentModel;
using System.Xml.Serialization;

namespace APIData.Models
{
    public class Example : Item
    {
        /// <summary>
        /// Gets or sets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public string Element { get; set; }

        /// <summary>
        /// Gets or sets the member.
        /// </summary>
        /// <value>
        /// The member.
        /// </value>
        [DefaultValue(null)]
        [XmlAttribute("Member")]
        public string Member { get; set; }

        private string _action = "Index";

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        [DefaultValue("Index")]
        [XmlAttribute("ExampleAction")]
        public string ExampleAction
        {
            get { return _action; }
            set { _action = value; }
        }

        private string _controlle = "Home";

        /// <summary>
        /// Gets or sets the controller.
        /// </summary>
        /// <value>
        /// The controller.
        /// </value>
        [DefaultValue("Home")]
        [XmlAttribute("ExampleController")]
        public string ExampleController
        {
            get { return _controlle; }
            set { _controlle = value; }
        }
        
    }
}
