﻿using System.Xml.Serialization;

namespace APIData.Models
{
    public class Category : Item
    {
        [XmlAttribute("Icon")]
        public string Icon { get; set; }
        [System.Xml.Serialization.XmlElementAttribute("Category")]
        public Category[] Items { get; set; }
        [XmlArrayItemAttribute("Example", IsNullable = false)]
        public Example[] Examples { get; set; }
    }
}
