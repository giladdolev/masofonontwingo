﻿using System.Xml.Serialization;

namespace APIData.Models
{
    [XmlRoot("Documentation")]
    [XmlInclude(typeof(Category))]
    public class Documentation
    {
        [XmlArrayItemAttribute("Category", IsNullable = false)]
        public Category[] Categories { get; set; }

    }
}
