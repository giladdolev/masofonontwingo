﻿using System.Xml.Serialization;

namespace APIData.Models
{
    public class ParameterNode : BaseNode
    {
        [XmlAttribute("Type")]
        public string Type { get; set; }
    }
}
