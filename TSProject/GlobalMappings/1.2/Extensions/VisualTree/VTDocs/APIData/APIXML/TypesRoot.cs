﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace APIData.Models
{
    [XmlRoot("Types")]
    [XmlInclude(typeof(TypeNode))]
    public class TypesRoot
    {
        private List<TypeNode> _types = new List<TypeNode>();


        [XmlElement(ElementName = "Type")]
        public List<TypeNode> Types
        {
            get { return _types; }
            set { _types = value; }
        }


    }
}
