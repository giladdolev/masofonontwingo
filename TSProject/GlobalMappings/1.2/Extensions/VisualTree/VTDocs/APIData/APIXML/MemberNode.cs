﻿using System.Xml.Serialization;

namespace APIData.Models
{
    public abstract class MemberNode : BaseNode
    {

        [XmlAttribute("Type")]
        virtual public string Type { get; set; }

    }
}
