﻿using APIData.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace APIData.Models
{
    public class XmlUtils
    {
        /// <summary>
        /// Loads the items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static T LoadItems<T>(string path) where T : new()
        {
            T items = default(T);

            try
            {
                if (System.IO.File.Exists(path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    using (XmlReader reader = XmlReader.Create(path))
                    {
                        items = (T)serializer.Deserialize(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: log
            }

            return items;

        }

        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <param name="categories">The categories.</param>
        /// <param name="elementID">The element identifier.</param>
        /// <returns></returns>
        public static Category GetCategory(Category[] categories, string elementID)
        {
            if (categories != null)
            {
                Category category = categories.FirstOrDefault(t => t.ID == elementID);
                if (category != null)
                {
                    return category;
                }
                foreach (Category item in categories)
                {
                    category = GetCategory(item.Items, elementID);
                    if (category != null)
                    {
                        return category;
                    }
                }
            }
            return null;
        }

    }
}
