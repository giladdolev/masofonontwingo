﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace APIData.Models
{
    public class MethodNode : MemberNode
    {
        [XmlElement("Parameter")]
        public List<ParameterNode> Parameters { get; set; }

        [XmlElement("ReturnType")]
        public TypeNode ReturnType { get; set; }
    }
}
