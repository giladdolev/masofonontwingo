﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace APIData.Models
{
    public class TypeNode : BaseNode
    {
        /// <summary>
        /// </summary>
        /// <value>
        /// The ns.
        /// </value>
        [XmlAttribute("NS")]
        public string NS { get; set; }

        /// <summary>
        /// Gets or sets the base types.
        /// </summary>
        /// <value>
        /// The base types.
        /// </value>
        [XmlArrayItemAttribute("Type", IsNullable = false)]
        public List<TypeNode> BaseTypes { get; set; }

        /// <summary>
        /// Gets or sets the events.
        /// </summary>
        /// <value>
        /// The events.
        /// </value>
        [XmlArrayItemAttribute("Event", IsNullable = false)]
        public List<EventNode> Events { get; set; }

        /// <summary>
        /// Gets or sets the methods.
        /// </summary>
        /// <value>
        /// The methods.
        /// </value>
        [XmlArrayItemAttribute("Method", IsNullable = false)]
        public List<MethodNode> Methods { get; set; }

        /// <summary>
        /// Gets or sets the properties.
        /// </summary>
        /// <value>
        /// The properties.
        /// </value>
        [XmlArrayItemAttribute("Property", IsNullable = false)]
        public List<PropertyNode> Properties { get; set; }

        /// <summary>
        /// Gets the members.
        /// </summary>
        /// <value>
        /// The members.
        /// </value>
        public IEnumerable<MemberNode> Members
        {
            get
            {
                if (Properties != null)
                {
                    foreach (var item in Properties)
                    {
                        yield return item;
                    }
                }
                if (Methods != null)
                {
                    foreach (var item in Methods)
                    {
                        yield return item;
                    }
                }
                if (Events != null)
                {
                    foreach (var item in Events)
                    {
                        yield return item;
                    }
                }
            }
        }
    }
}
