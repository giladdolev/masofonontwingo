﻿using System;
using System.Xml.Serialization;

namespace APIData.Models
{
    public class BaseNode : IComparable
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlAttribute("Name")]
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        [XmlAttribute("DisplayName")]
        public string DisplayName { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlAttribute("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the examples.
        /// </summary>
        /// <value>
        /// The examples.
        /// </value>
        [XmlArrayItemAttribute("Example", IsNullable = false)]
        public Example[] Examples { get; set; }

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj" /> in the sort order. Zero This instance occurs in the same position in the sort order as <paramref name="obj" />. Greater than zero This instance follows <paramref name="obj" /> in the sort order.
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public int CompareTo(object obj)
        {
            BaseNode node = obj as BaseNode;
            if (node == null)
                return -1;

            return String.Compare(this.Name, node.Name);
        }
    }
}
