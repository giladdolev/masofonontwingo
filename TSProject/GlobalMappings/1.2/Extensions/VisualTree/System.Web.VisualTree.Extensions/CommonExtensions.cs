﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.MVC;

namespace System.Web.VisualTree.Extensions
{
    public static class CommonExtensions
    {
        /// <summary>
        /// Sets the font bold.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="blnBold">If set to <c>true</c> convert font to bold.</param>
        public static void SetFontBold(this ControlElement control, bool blnBold)
        {
            SetFontProperty(control, blnBold, FontStyle.Bold);
        }
        /// <summary>
        /// Sets the font italic.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="blnItalic">If set to <c>true</c> convert font to italic.</param>
        public static void SetFontItalic(this ControlElement control, bool blnItalic)
        {
            SetFontProperty(control, blnItalic, FontStyle.Italic);
        }
        /// <summary>
        /// Sets the font strikeout.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="blnStrikeout">If set to <c>true</c> convert font to strikeout.</param>
        public static void SetFontStrikeout(this ControlElement control, bool blnStrikeout)
        {
            SetFontProperty(control, blnStrikeout, FontStyle.Strikeout);
        }
        /// <summary>
        /// Sets the font underline.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="blnUnderline">If set to <c>true</c> convert font to underline.</param>
        public static void SetFontUnderline(this ControlElement control, bool blnUnderline)
        {
            SetFontProperty(control, blnUnderline, FontStyle.Underline);
        }
        /// <summary>
        /// Sets the font property.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="blnSet">if set to <c>true</c> set specified font property.</param>
        /// <param name="objStyle">The font style to set.</param>
        private static void SetFontProperty(ControlElement control, bool blnSet, FontStyle objStyle)
        {
            control.Font = SetFontProperty(control.Font, blnSet, objStyle);
        }

        /// <summary>
        /// Sets the font property.
        /// </summary>
        /// <param name="objFont">The font.</param>
        /// <param name="blnSet">if set to <c>true</c> [BLN set].</param>
        /// <param name="objStyle">The style.</param>
        /// <returns>The new font</returns>
        public static Font SetFontProperty(Font objFont, bool blnSet, FontStyle objStyle)
        {
            // validate parameter
            if (objFont == null)
            {
                throw new ArgumentNullException("objFont");
            }


            FontStyle objFontStyle = (blnSet) ? objFont.Style | objStyle : objFont.Style ^ objStyle;
            try
            {
                // Generate new font
                return new Font(objFont, objFontStyle);
            }

            catch (ArgumentException)
            {

            }
            catch (NullReferenceException)
            {
            }
            // Incompatible font style;
            return objFont;
        }
        /// <summary>
        /// Sets the name of the font.
        /// </summary>
        /// <param name="objFont">The font.</param>
        /// <param name="strName">Name of the font family.</param>
        /// <returns>The name of the font</returns>
        public static Font SetFontName(Font objFont, string strName)
        {

            try
            {
                if (objFont == null)
                {
                    objFont = SystemFonts.DefaultFont;
                }

                // Generate new font
                return new Font(strName, objFont.Size, objFont.Style);
            }
            catch (ArgumentException)
            {

            }
            catch (NullReferenceException)
            {
            }

            try
            {
                // Try to generate new font with default style
                return new Font(strName, objFont.Size);
            }
            catch (ArgumentException)
            {

            }
            catch (NullReferenceException)
            {
            }

            // Incompatible font style;
            return objFont;
        }

        /// <summary>
        /// Gets the font property.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="objStyle">The style to check.</param>
        /// <returns>The font</returns>
        public static bool GetFontProperty(this ControlElement control, FontStyle objStyle)
        {
            // validate parameter
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            return GetFontProperty(control.Font, objStyle);
        }
        /// <summary>
        /// Gets the font property.
        /// </summary>
        /// <param name="objFont">The font.</param>
        /// <param name="objStyle">The style.</param>
        /// <returns>The font</returns>
        public static bool GetFontProperty(Font objFont, FontStyle objStyle)
        {
            // validate parameter
            if (objFont == null)
            {
                throw new ArgumentNullException("objFont");
            }
            return (objFont.Style & objStyle) > 0;
        }

        public static WindowStartPosition ConvertBooleanToWindowStartPosition(bool isCenter)
        {
            if (isCenter)
            {
                return WindowStartPosition.CenterScreen;
            }
            return WindowStartPosition.WindowsDefaultLocation;
        }

        public static async Task<T> Open<T>(string areaName, params object[] args) where T : WindowElement
        {
            IDictionary<string, object> arguments = GetArguments(args);
            string controllerName = GetFriendlyName(typeof(T));
            T w = VisualElementHelper.CreateFromView<T>(controllerName, controllerName, areaName, arguments, null);
            await w.ShowDialog();
            return w;

        }

        public static void Open(Type type, params object[] args)
        {
            string areaName = ""; //TODO
            typeof(CommonExtensions).GetMethod("Open").MakeGenericMethod(type).Invoke(null, new object[] { areaName, args });
         }

        private static IDictionary<string, object> GetArguments(object[] args)
        {
            IDictionary<string, object> arguments = null;

            if (args != null)
            {
                arguments = new Dictionary<string, object>();
                for (int i = 0; i < args.Length; i++)
                {
                    arguments.Add(args[i].ToString(), args[i + 1]);
                    i++;
                }
            }
            return arguments;
        }


        private static string GetFriendlyName(Type type)
        {
            string friendlyName = type.Name;
            if (type.IsGenericType)
            {
                int iBacktick = friendlyName.IndexOf('`');
                if (iBacktick > 0)
                {
                    friendlyName = friendlyName.Remove(iBacktick);
                }
                friendlyName += "<";
                Type[] typeParameters = type.GetGenericArguments();
                for (int i = 0; i < typeParameters.Length; ++i)
                {
                    string typeParamName = typeParameters[i].Name;
                    friendlyName += (i == 0 ? typeParamName : "," + typeParamName);
                }
                friendlyName += ">";
            }

            return friendlyName;
        }

        public static int Close<T>(WindowElement closedWindow, T returnValue)
        {
            try
            {
                SetParam<T>(closedWindow, returnValue);
                closedWindow.Close();
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public static T GetParam<T>(ControlElement element)
        {

            DataExtender<T> extender = VisualElementExtender.GetExtender<DataExtender<T>>(element);
            if (extender != null)
            {
                return extender.Value;
            }
            return default(T);
        }

        public static void SetParam<T>(ControlElement element, T returnValue)
        {
            DataExtender<T> extender = VisualElementExtender.GetOrCreateExtender(element,
                arg => new DataExtender<T>());
            extender.Value = returnValue;
        }
    

    }
}
