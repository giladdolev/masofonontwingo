﻿using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Extensions
{
    public static class MessageBoxExtensions
    {
        public static Task<DialogResult> MsgBox(object prompt, MessageBoxStyle style, string title, string helpFile, object context)
        {

            return MessageBox.Show(Convert.ToString(prompt, CultureInfo.InvariantCulture), title, GetButtonsFromStyle(style), GetIconFromStyle(style));
        }

        public static Task<DialogResult> MsgBox(object prompt, MessageBoxStyle buttons, string title)
        {

            return MessageBox.Show(Convert.ToString(prompt, CultureInfo.InvariantCulture), title, GetButtonsFromStyle(buttons), GetIconFromStyle(buttons));
        }

        public static Task<DialogResult> MsgBox(object prompt, MessageBoxStyle buttons)
        {

            return MessageBox.Show(Convert.ToString(prompt, CultureInfo.InvariantCulture), "", GetButtonsFromStyle(buttons), GetIconFromStyle(buttons));
        }

        public static Task<DialogResult> MsgBox(string prompt)
        {
            return MessageBox.Show(Convert.ToString(prompt, CultureInfo.InvariantCulture), "");
        }

        public static Task<DialogResult> MsgBox(string prompt, string title)
        {
            return MessageBox.Show(prompt, title);
        }
 

        private static MessageBoxIcon GetIconFromStyle(MessageBoxStyle style)
        {
            if ((style & MessageBoxStyle.Exclamation) == MessageBoxStyle.Exclamation)
            {
                return MessageBoxIcon.Exclamation;
            }
            else if ((style & MessageBoxStyle.Information) == MessageBoxStyle.Information)
            {
                return MessageBoxIcon.Information;
            }
            else if ((style & MessageBoxStyle.Critical) == MessageBoxStyle.Critical)
            {
                return MessageBoxIcon.Error;
            }
            else if ((style & MessageBoxStyle.Question) == MessageBoxStyle.Question)
            {
                return MessageBoxIcon.Question;
            }
            else
            {
                return MessageBoxIcon.None; 
            }
           
        }

        private static MessageBoxButtons GetButtonsFromStyle(MessageBoxStyle style)
        {
            if((style & MessageBoxStyle.YesNo) == MessageBoxStyle.YesNo)
            {
                return MessageBoxButtons.YesNo;
            }
            else if((style & MessageBoxStyle.OkCancel) == MessageBoxStyle.OkCancel)
            {
                return MessageBoxButtons.OKCancel;
            }
            else if ((style & MessageBoxStyle.YesNoCancel) == MessageBoxStyle.YesNoCancel)
            {
                return MessageBoxButtons.YesNoCancel;
            }
            else if((style & MessageBoxStyle.AbortRetryIgnore) == MessageBoxStyle.AbortRetryIgnore)
            {
                return MessageBoxButtons.AbortRetryIgnore;
            }
            else if ((style & MessageBoxStyle.RetryCancel) == MessageBoxStyle.RetryCancel)
            {
                return MessageBoxButtons.RetryCancel;
            }
            else
            {
                return MessageBoxButtons.OK;
            }
        }



       
    }

    /// <summary>
    /// Specifies styles for message box
    /// </summary>
    [Flags]
    public enum MessageBoxStyle
    {
        /// <summary>
        /// Not defined
        /// </summary>
        None = 1,
        /// <summary>
        /// The message box contains Abort, Retry, and Ignore buttons.
        /// </summary>
        AbortRetryIgnore = 2,
        /// <summary>
        /// Application is modal. The user must respond to the message box before continuing work in the current application.
        /// </summary>
        ApplicationModal = 4,
        /// <summary>
        /// The message box contains a symbol consisting of white X in a circle with
        /// a red background.
        /// </summary>
        Critical = 8,
        /// <summary>
        /// The first button on the message box is the default button.
        /// </summary>
        DefaultButton1 = 16,
        /// <summary>
        /// The second button on the message box is the default button.
        /// </summary>
        DefaultButton2 = 32,
        /// <summary>
        /// The third button on the message box is the default button.
        /// </summary>
        DefaultButton3 = 64,
        /// <summary>
        /// The fourth button on the message box is the default button.
        /// </summary>
        DefaultButton4 = 128,
        /// <summary>
        /// The message box contains a symbol consisting of an exclamation point in a
        ///  triangle with a yellow background.
        /// </summary>
        Exclamation = 256,
        /// <summary>
        /// The message box contains a symbol consisting of a lowercase letter  in a circle
        /// </summary>
        Information = 512,
        /// <summary>
        /// Adds Help button to the message box
        /// </summary>
        MsgBoxHelp = 1024,
        /// <summary>
        /// The message box text is right-aligned.
        /// </summary>
        MsgBoxRight = 2048,
        /// <summary>
        /// Specifies that the message box text is displayed with right to left reading order.
        /// </summary>
        MsgBoxRtlReading = 4096,
        /// <summary>
        /// Specifies the message box window as the foreground window.
        /// </summary>
        MsgBoxSetForeground = 8192,
        /// <summary>
        /// The message box contains OK and Cancel buttons.
        /// </summary>
        OkCancel = 16384,
        /// <summary>
        /// The message box contains an OK button.
        /// </summary>
        OkOnly = 32768,
        /// <summary>
        /// The message box contains a symbol consisting of a question mark in a circle.
        /// </summary>
        Question = 65536,
        /// <summary>
        /// The message box contains Retry and Cancel buttons.
        /// </summary>
        RetryCancel = 131072,
        /// <summary>
        /// System is modal. All applications are suspended until the user responds to the message box.
        /// </summary>
        SystemModal = 262144,
        /// <summary>
        /// The message box contains Yes and No buttons.
        /// </summary>
        YesNo = 524288,
        /// <summary>
        /// The message box contains Yes, No, and Cancel buttons.
        /// </summary>
        YesNoCancel = 1048576
    }

}
