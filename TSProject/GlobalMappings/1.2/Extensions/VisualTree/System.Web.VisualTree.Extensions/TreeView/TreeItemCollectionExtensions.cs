﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Extensions
{
    /// <summary>
    /// Implement extension methods for TreeItemCollection
    /// </summary>
    public static class TreeItemCollectionExtensions
    {
		#region Methods (16) 

		// Public Methods (12) 

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItemCollection object</param>
        /// <param name="strRelative">Relative string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <returns>Created TreeItem object</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, string strRelative , TreeRelationshipConstants enumRelationship, string strKey, string strText)
        {
            // Find the node in the collection based on relative string
            TreeItem objNode = objTreeItem.FindNode(strRelative);

            // Call AddNode overload
            return objTreeItem.AddNode(enumRelationship, objNode, strKey, strText);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="objRelative">Relative TreeItem</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <returns>The new node</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, TreeRelationshipConstants enumRelationship, TreeItem objRelative, string strKey)
        {
            return AddNode(objTreeItem, enumRelationship, objRelative,  strKey, "");
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="treeItem">TreeItem object</param>
        /// <param name="relative">Relative TreeItem</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="key">Key value</param>
        /// <param name="text">Text value</param>
        /// <returns>The new node</returns>
        public static TreeItem AddNode(this TreeItemCollection treeItem, TreeRelationshipConstants enumRelationship, TreeItem relative, string key, string text)
        {
            // validate parameter
            if (treeItem == null)
            {
                throw new ArgumentNullException("treeItem");
            }

            // The variable that will hold the returning result
            TreeItem objNode = null;

            // Check if there is a relative object specified
            if (relative == null)
            {
                // If no relative found, add the TreeItem at the end of the TreeElement
                objNode = treeItem.Add(key, text);
            }

            else
            {
                // Check for the relationship
                switch (enumRelationship)
                {
                    // Case child
                    case TreeRelationshipConstants.tvwChild:

                        // Cache nodes collection on relative node
                        TreeItemCollection objRelativeNodes = relative.Items;

                        // Add the Node to the Nodes collection of the specified relative Node
                        objNode = objRelativeNodes.Add(key, text);

                        break;

                    // Case first
                    case TreeRelationshipConstants.tvwFirst:

                        // Check if the relative Node has parent
                        if (relative.ParentElement == null)
                        {
                            // If it doesn't have parent then add it at the beginning of the tree
                            treeItem.Insert(0, new TreeItem(text));

                            // Assign the newly created TreeItem to the returning variable
                            objNode = treeItem[0];
                        }
                        else
                        {
                            // Cache relative node parent
                            TreeItem relativeParent = relative.ParentElement as TreeItem;

                            // Cache relative node parent nodes collection
                            TreeItemCollection relativeParentNodes = relativeParent.Items;

                            // If the relative node has parent, add the TreeItem to the beginning of its parents Nodes collection
                            relativeParentNodes.Insert(0, new TreeItem(text));

                            // Assign the newly created TreeItem to the returning variable
                            objNode = relativeParent.Items[0];
                        }

                        //Assign the Name property with the value passed from the Key parameter
                        objNode.ID = key;

                        break;
                    // Case Last
                    case TreeRelationshipConstants.tvwLast:

                        // Check if the relative Node has parent
                        if (relative.ParentElement == null)
                        {
                            // If it doesn't have a parent add it at the end of the TreeElement
                            objNode = treeItem.Add(key, text);
                        }

                        else
                        {
                            // Cache relative node parent
                            TreeItem objRelativeParent = relative.ParentElement as TreeItem;

                            // Cache relative node parent nodes collection
                            TreeItemCollection objRelativeParentNodes = objRelativeParent.Items;

                            // If it has parent add it at the end of the child nodes collection
                            objNode = objRelativeParentNodes.Add(key, text);
                        }

                        break;
                    // Case Next
                    case TreeRelationshipConstants.tvwNext:

                        // Check if the relative node has parent
                        if (relative.ParentElement == null)
                        {
                            // If it doesn't have, check if the selected node is the last
                            if (relative.Index + 1 == treeItem.Count)
                            {
                                // If it's the last add it at the end
                                objNode = treeItem.Add(key, text);
                            }

                            else
                            {
                                // If the selected node is not the last add it on the next index
                                treeItem.Insert(relative.Index + 1, new TreeItem(text));

                                // Assign the newly created TreeItem to the return variable
                                objNode = treeItem[relative.Index + 1];

                                // Assign the Name property with the value passed from the Key parameter
                                objNode.ID = key;
                            }
                        }

                        else
                        {
                            // If it has parent check if it is the last node in the collection
                            if (relative.Index + 1 == treeItem.Count)
                            {
                                // Cache relative node parent
                                TreeItem objRelativeParent = relative.ParentElement as TreeItem;

                                // Cache relative node parent nodes collection
                                TreeItemCollection objRelativeParentNodes = objRelativeParent.Items;

                                // If it's the last add the new node at the end
                                objNode = objRelativeParentNodes.Add(key, text);
                            }
                            else
                            {
                                // Cache relative node parent
                                TreeItem objRelativeParent = relative.ParentElement as TreeItem;

                                // Cache relative node parent nodes collection
                                TreeItemCollection objRelativeParentNodes = objRelativeParent.Items;

                                // If it's not the last, insert it after the selected index
                                objRelativeParentNodes.Insert(relative.Index + 1, new TreeItem(text));

                                // Assign the newly created TreeItem to the return variable
                                objNode = objRelativeParent.Items[relative.Index + 1];

                                // Assign the Name property with the value passed from the Key parameter
                                objNode.ID = key;
                            }
                        }
                        break;
                    // Case Previous
                    case TreeRelationshipConstants.tvwPrevious:

                        // Check if the relative object has parent
                        if (relative.ParentElement == null)
                        {
                            // If it doesn't have a parent add new node at the root of the tree with same index as the selected node
                            treeItem.Insert(relative.Index, new TreeItem(text));

                            // Assign the newly created TreeItem to the return variable
                            objNode = treeItem[relative.Index];
                        }
                        else
                        {
                            // Cache relative node parent
                            TreeItem objRelativeParent = relative.ParentElement as TreeItem;

                            // Cache relative node parent nodes collection
                            TreeItemCollection objRelativeParentNodes = objRelativeParent.Items;

                            // If it has a parent add the node to its parents node collection
                            objRelativeParentNodes.Insert(relative.Index, new TreeItem(text));

                            // Assign the newly created TreeItem to the return variable
                            objNode = objRelativeParent.Items[relative.Index];
                        }

                        // Assign the Name property with the value passed from the Key parameter
                        objNode.ID = key;
                        break;

                    default:
                        break;
                }
            }

            // Return the new node
            return objNode;
        }



        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="strRelative">Relative Node</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <returns>TreeItem object</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey)
        {
            // Find the node in the collection based on relative string
            TreeItem objNode = objTreeItem.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeItem.AddNode(objNode, enumRelationship, strKey, strText, strImageKey, strImageKey);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="strRelative">Relative TreeItem string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns>TreeItem object</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex)
        {
            // Find the node in the collection based on relative string
            TreeItem objNode = objTreeItem.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeItem.AddNode(objNode, enumRelationship, strKey, strText, intImageIndex, intSelectedImageIndex);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="strRelative">Relative node string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns>The new Node</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            // Find the node in the collection based on relative string
            TreeItem objNode = objTreeItem.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeItem.AddNode(objNode, enumRelationship, strKey, strText, strImageKey, intSelectedImageIndex);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="strRelative">Relative node string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="strSelectedImageKey">Selected ImageKey value</param>
        /// <returns>TreeItem object</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
            // Find the node in the collection based on relative string
            TreeItem objNode = objTreeItem.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeItem.AddNode(objNode, enumRelationship, strKey, strText, intImageIndex, strSelectedImageKey);
        }

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="strRelative">Relative node string</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="strSelectedImageKey">Selected ImageKey value</param>
        /// <returns>TreeItem object</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, string strRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            // Find the node in the collection based on relative string
            TreeItem objNode = objTreeItem.FindNode(strRelative);

            // Call overloaded add node method
            return objTreeItem.AddNode(objNode, enumRelationship, strKey, strText, strImageKey, strSelectedImageKey);
        }

        /// <summary>
        /// Add tree node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="objRelative">Relative object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="strSelectedImageKey">Selected ImageKey value</param>
        /// <returns>TreeItem object</returns>
        public static TreeItem AddNode(this TreeItemCollection objTreeItem, TreeItem objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            // Add the node without the image details
            TreeItem objNode = objTreeItem.AddNode(enumRelationship, objRelative, strKey, strText);

            // Check if we assign ImageKey
            if (strImageKey != null)
            {
                objNode.ImageKey = strImageKey;
            }

            // Check if we assign SelectedImageKey
            if (strSelectedImageKey != null)
            {
                objNode.SelectedImageKey = strSelectedImageKey;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Finds a node in the collection
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="strNodeName">Node name</param>
        /// <returns>TreeItem object</returns>
        public static TreeItem FindNode(this TreeItemCollection objTreeItem, string strNodeName)
        {
            // validate parameter
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }

            //Search the node collection and take the first node or default
            TreeItem objFindNode = objTreeItem.Find(strNodeName, true).FirstOrDefault();

            //Return tree node object
            return objFindNode;
        }

        /// <summary>
        /// Gets the number of all nodes.
        /// </summary>
        /// <param name="objTreeItems">The object tree nodes.</param>
        /// <returns>The number of all nodes</returns>
        public static int GetNumberOfAllNodes(this TreeItemCollection objTreeItems)
        {
            // validate parameter
            if (objTreeItems == null)
            {
                throw new ArgumentNullException("objTreeItems");
            }

            // Declare variable sum where we will store our calculations
            int intSum = 0;

            // Loop through all nodes in tree view
            foreach (TreeItem node in objTreeItems)
            {
                // Sum is equal to number of all sub nodes in the node plus one for the root node
                intSum += GetNodes(node) + 1;
            }

            // Return the number of all nodes
            return intSum;
        }

        /// <summary>
        /// Remove node from collection
        /// </summary>
        /// <param name="objNodeCollection">TreeItem object</param>
        /// <param name="strNodeName">NodeName value</param>
        /// <exception cref="System.NullReferenceException">The NodeCollection not exist.</exception>
        public static void Remove(this TreeItemCollection objNodeCollection, string strNodeName)
        {
            // Check for null reference
            if (objNodeCollection != null && objNodeCollection.Count > 0)
            {
                // Find the node to remove by name
                TreeItem objNodeToRemove = objNodeCollection.FindNode(strNodeName);

                // Null reference checking
                if (objNodeToRemove != null)
                {
                    // Remove it
                    objNodeToRemove.Remove();
                }
            }
            else
            {
                // If object is null throw exception
                throw new ArgumentException("Node Collection is either null or empty", "objNodeCollection");
            }
        }

        /// <summary>
        /// Removes the node.
        /// </summary>
        /// <param name="objNodeCollection">The object node collection.</param>
        /// <param name="intIndex">Index of the int.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">The node index is out of range.</exception>
        /// <exception cref="System.NullReferenceException">The NodeCollection not exist.</exception>
        public static void Remove(this TreeItemCollection objNodeCollection, int intIndex)
        {
            // Check for null reference
            if (objNodeCollection != null)
            {
                // Get the number of all nodes
                int intNumberOfAllNodes = objNodeCollection.GetNumberOfAllNodes();

                // Check if index is valid
                if (intIndex < intNumberOfAllNodes  && intIndex >= 0)
                {
                    //Find the node to remove with our extension method
                    TreeItem objNodeToRemove = objNodeCollection.Item(intIndex);

                    // Null reference checking
                    if (objNodeToRemove != null)
                    {
                        // Remove it
                        objNodeToRemove.Remove();
                    }
                }
                else
                {
                    // If index is out of range throw exception
                    throw new ArgumentOutOfRangeException("intIndex");
                }
            }
            else
            {
                // If object is null throw exception
                throw new ArgumentException("Node Collection is null", "objNodeCollection");
            }
        }
		// Private Methods (4) 

        /// <summary>
        /// Add node to the tree view
        /// </summary>
        /// <param name="objTreeItem">TreeItem collection object</param>
        /// <param name="objRelative">Relative object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns>TreeItem object</returns>
        private static TreeItem AddNode(this TreeItemCollection objTreeItem, TreeItem objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex)
        {
             // Add the node without the image details
            TreeItem objNode = objTreeItem.AddNode(enumRelationship, objRelative, strKey, strText);

            // Assign ImageIndex
            if (intImageIndex != -1)
            {
                objNode.ImageIndex = intImageIndex;
            }

            // Assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Add TreeItem to the TreeElement
        /// </summary>
        /// <param name="objTreeItem">ThreeNode object</param>
        /// <param name="objRelative">Relative TreeItem object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="strImageKey">ImageKey value</param>
        /// <param name="intSelectedImageIndex">Selected ImageIndex value</param>
        /// <returns>TreeItem object</returns>
        private static TreeItem AddNode(this TreeItemCollection objTreeItem, TreeItem objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            // Add the node without the image details
            TreeItem objNode = objTreeItem.AddNode(enumRelationship, objRelative, strKey, strText);

            // Assign ImageKey
            if (strImageKey != null)
            {
                objNode.ImageKey = strImageKey;
            }

            // Assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Add TreeItem object to the TreeElement
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="objRelative">Relative TreeItem object</param>
        /// <param name="enumRelationship">Relationship object</param>
        /// <param name="strKey">Key value</param>
        /// <param name="strText">Text value</param>
        /// <param name="intImageIndex">ImageIndex value</param>
        /// <param name="strSelectedImageKey">ImageKey value</param>
        /// <returns>TreeItem object</returns>
        private static TreeItem AddNode(this TreeItemCollection objTreeItem, TreeItem objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
            // Add the node without the image details
            TreeItem objNode = objTreeItem.AddNode(enumRelationship, objRelative, strKey, strText);

            // Assign ImageIndex
            if (intImageIndex != -1)
            {
                objNode.ImageIndex = intImageIndex;
            }

            // Assign Selected ImageKey
            if (strSelectedImageKey != null)
            {
                objNode.SelectedImageKey = strSelectedImageKey;
            }

            // Return the node
            return objNode;
        }

        /// <summary>
        /// Gets the number of sub nodes.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>The number of sub nodes</returns>
        private static int GetNodes(TreeItem node)
        {
            // Declare a variable where we will store number of sub nodes
            int intSubNodesSum = 0;

            // Calculate number of sub nodes
            intSubNodesSum += node.GetNodeCount(true);

            // Return number of sub nodes
            return intSubNodesSum;
        }

        /// <summary>
        /// Gets TreeItem from collection by index
        /// </summary>
        /// <param name="objTreeItemCollection">TreeItem collection</param>
        /// <param name="intIndex">Index value</param>
        /// <returns>Tree node object</returns>
        public static TreeItem Item(this TreeItemCollection objTreeItemCollection, int intIndex)
        {
            // Null reference checking and check if there are nodes in the collection
            if (objTreeItemCollection != null && objTreeItemCollection.Count > 0)
            {
                // List to keeps all tree nodes
                Collection<TreeItem> objNodesCollection = new Collection<TreeItem>();

                // Make collection empty
                objNodesCollection.Clear();

                // Loops through the root nodes
                foreach (TreeItem objChild in objTreeItemCollection)
                {
                    // Call the method for recursion
                    TreeElementExtensions.BuildChildNodesList(objChild, objNodesCollection);
                }

                // Check if index is into range
                if (intIndex >= 0 || intIndex < objNodesCollection.Count)
                {
                    // Return the TreeItem found
                    return objNodesCollection[intIndex];
                }
                else
                {
                    // Throw argument out of range
                    throw new ArgumentOutOfRangeException("intIndex");
                }
            }
            else
            {
                //Throw null reference exception
                throw new ArgumentException("Tree Item Collection is either null or empty", "objTreeItemCollection");
            }
        }

		#endregion Methods 
    }
}
