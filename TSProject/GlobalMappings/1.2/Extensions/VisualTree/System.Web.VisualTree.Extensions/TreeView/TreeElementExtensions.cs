﻿using System.Collections.ObjectModel;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Extensions
{
   
    /// <summary>
    /// TreeElementConstants enumeration
    /// </summary>
    public enum TreeElementConstants
    {
        /// <summary>
        /// Text only
        /// </summary>
        tvwTextOnly = 0,

        /// <summary>
        ///  Picture and Text
        /// </summary>
        tvwPictureText = 1,

        /// <summary>
        /// Plus/Minus and Text
        /// <summary>
        tvwPlusMinusText = 2,

        /// <summary>
        /// Plus/Minus, Picture and Text
        /// <summary>
        tvwPlusPictureText = 3,

        /// <summary>
        ///  TreeLines and Text
        /// </summary>
        tvwTreelinesText = 4,

        /// <summary>
        ///  TreeLines, Picture and Text
        /// </summary>
        tvwTreelinesPictureText = 5,

        /// <summary>
        ///  TreeLines, Plus/Minus and Text
        /// </summary>
        tvwTreelinesPlusMinusText = 6,

        /// <summary>
        ///  TreeLines, Plus/Minus, Picture and Text
        /// </summary>
        tvwTreelinesPlusMinusPictureText = 7

    }

    /// <summary>
    /// TreeLineStyleConstants enumeration
    /// </summary>
    public enum TreeLineStyleConstants
    {

        /// <summary>
        ///  TreeLines
        /// </summary>
        tvwTreeLines = 0,

        /// <summary>
        ///  RootLines
        /// </summary>
        tvwRootLines = 1

    }

    /// <summary>
    /// TreeRelationshipConstants enumeration
    /// </summary>
    public enum TreeRelationshipConstants
    {

        /// <summary>
        /// First sibling
        /// </summary> 
        tvwFirst = 0,

        /// <summary>
        /// Child
        /// </summary>
        tvwChild = 4,        

        /// <summary>
        /// Last sibling
        /// </summary> 
        tvwLast = 1,

        /// <summary>
        /// Next sibling
        /// </summary>
        tvwNext = 2,

        /// <summary>
        /// Previous sibling
        /// </summary>
 
        tvwPrevious = 3

    }

    /// <summary>
    /// Implement extension methods for TreeElement control
    /// </summary>
    public static class TreeElementExtensions
    {
        /// <summary>
        /// Gets the ImageList for the TreeElement object
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <returns>ImageList object</returns>
        public static ImageList GetImageList(this TreeElement objTreeElement)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the ImageList object on TreeElement
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <param name="objImageList">ImageList object</param>
        public static void SetImageList(this TreeElement objTreeElement, ImageList objImageList)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            objTreeElement.SetImageList(objImageList);
        }

        /// <summary>
        /// Get ShowImage value
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <returns>ShowImage value</returns>
        public static bool GetShowImage(this TreeElement objTreeElement)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            throw new NotImplementedException();
            
        }

        /// <summary>
        /// Sets ShowImage value
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <param name="blnShowImage">ShowImage value</param>
        public static void SetShowImage(this TreeElement objTreeElement, bool blnShowImage)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            objTreeElement.SetShowImage(blnShowImage);
        }

        /// <summary>
        /// Sets the style for the TreeElement object
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <param name="blnPlusMinus">ShowPlusMinus value</param>
        /// <param name="blnLines">ShowLines value</param>
        /// <param name="blnImage">ShowImage value</param>
        public static void SetStyle(this TreeElement objTreeElement, bool blnPlusMinus, bool blnLines, bool blnImage)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            objTreeElement.SetStyle(blnPlusMinus, blnLines,blnImage);
        }

        /// <summary>
        /// Gets the style value for the TreeElement object
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <returns>Style value</returns>
        public static TreeElementConstants GetStyle(this TreeElement objTreeElement)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            throw new NotImplementedException();

        }

        /// <summary>
        /// Sets the style for TreeElement
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <param name="objStyle">Style value</param>
        public static void SetStyle(this TreeElement objTreeElement, TreeElementConstants objStyle)
        {

            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            objTreeElement.SetStyle(objStyle);
        }

        /// <summary>
        /// Private method used recursively to pass through the nodes in the TreeElement
        /// </summary>
        /// <param name="objNode">Parent TreeItem object</param>
        /// <param name="objNodesCollection">TreeItem collection to store all TreeItems</param>
        internal static void BuildChildNodesList(TreeItem objNode, Collection<TreeItem> objNodesCollection)
        {
            // Add TreeItem to the collection
            objNodesCollection.Add(objNode);

            // Loop through the nodes
            foreach (TreeItem objChild in objNode.Items)
            {
                // If this node object has child call the method again
                BuildChildNodesList(objChild, objNodesCollection);
            }
        }

        /// <summary>
        /// Gets TreeItem objects on all levels
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <returns>List of TreeItems</returns>
        public static Collection<TreeItem> GetNodes(this TreeElement objTreeElement)
        {
            // Null reference checking
            if (objTreeElement != null)
            {
                // List to keeps all TreeItems
                Collection<TreeItem> objNodesCollection = new Collection<TreeItem>();

                // Make it empty
                objNodesCollection.Clear();

                // Loops through the root nodes
                foreach (TreeItem objChild in objTreeElement.Items)
                {
                    // Call the method for recursion
                    BuildChildNodesList(objChild, objNodesCollection);
                }

                // Return the collection
                return objNodesCollection;
            }
            else
            {
                // Throw exception if TreeElement is null
                throw new ArgumentException("Tree Element is null", "objTreeElement");
            }
        }

        /// <summary>
        /// Sets line style on TreeElement
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <param name="objLineStyle">LineStyle value</param>
        public static void SetLineStyle(this TreeElement objTreeElement, TreeLineStyleConstants objLineStyle)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }

            objTreeElement.SetLineStyle(objLineStyle);
        }

        /// <summary>
        /// Gets the line style of a TreeElement
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        /// <returns>TreeLineStyleConstants value</returns>
        public static TreeLineStyleConstants GetLineStyle(this TreeElement objTreeElement)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Starts label edit on selected node
        /// </summary>
        /// <param name="objTreeElement">TreeElement object</param>
        public static void StartLabelEdit(this TreeElement objTreeElement)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }

            throw new NotImplementedException();
        }


        /// <summary>
        /// Determines whether a node expands to show its child nodes when selected
        /// </summary>
        /// <param name="objTreeElement">The obj tree view.</param>
        /// <param name="blnValue">if set to <c>true</c> [BLN value].</param>
        public static void SetSingleSel(this TreeElement objTreeElement, bool blnValue)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            objTreeElement.SetSingleSel(blnValue);
        }


        /// <summary>
        /// Gets whether a node expands to show its child nodes when selected.
        /// </summary>
        /// <param name="objTreeElement">The obj tree view.</param>
        /// <returns>Boolean</returns>
        public static bool GetSingleSel (this TreeElement objTreeElement)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            throw new NotImplementedException();
        }


        /// <summary>
        /// TreeElement HitTest(int, int) Extension
        /// </summary>
        /// <param name="objTreeElement">TreeElement object.</param>
        /// <param name="intX">Integer.</param>
        /// <param name="intY">Integer.</param>
        /// <returns>TreeItem object</returns>
        public static TreeItem HitTestEx(this TreeElement objTreeElement, int intX, int intY)
        {
            if (objTreeElement == null)
            {
                throw new ArgumentNullException("objTreeElement");
            }
            return objTreeElement.HitTestEx(intX, intY);
        }
    }
}
