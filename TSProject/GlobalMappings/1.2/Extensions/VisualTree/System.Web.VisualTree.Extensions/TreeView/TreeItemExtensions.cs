﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.VisualTree.Elements;


namespace System.Web.VisualTree.Extensions
{
    /// <summary>
    /// Implement extension methods and properties for TreeItem
    /// </summary>
    public static class TreeItemExtensions
    {
        /// <summary>
        /// Sorts the nodes of a node
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="blnValue">boolean value</param>
        public static void SetSorted(this TreeItem objTreeItem)
        {
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the sorted.
        /// </summary>
        /// <param name="objTreeItem">The tree node object.</param>
        /// <returns>Boolean value indicating that is sorted or not</returns>
        public static bool GetSorted(this TreeItem objTreeItem)
        {
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the node font bold.
        /// </summary>
        /// <param name="objNode">The node object.</param>
        /// <param name="blnValue">if set to <c>true</c> [BLN value].</param>
        /// <exception cref="System.NullReferenceException">The TreeItem not exist.</exception>
        public static void SetNodeFontBold(this TreeItem objNode, bool blnValue)
        {
            if (objNode == null)
            {
                throw new ArgumentNullException("objNode");
            }
            objNode.SetNodeFontBold(blnValue);
        }

        /// <summary>
        /// Gets the node font bold.
        /// </summary>
        /// <param name="objNode">The node object.</param>
        /// <returns>If node is bold then return <c>true</c> otherwise <c>false</c></returns>
        /// <exception cref="System.NullReferenceException">The TreeItem not exist.</exception>
        public static bool GetNodeFontBold(this TreeItem objNode)
        {
            // validate parameter
            if (objNode == null)
            {
                throw new ArgumentNullException("objNode");
            }

            throw new NotImplementedException();
        }    
       


        /// <summary>
        /// Returns the number of child nodes a Node object has.
        /// </summary>
        /// <param name="objNode">The node object.</param>
        /// <returns>Count of sub nodes </returns>
        /// <exception cref="System.NullReferenceException">Node not exist.</exception>
        public static int Children(this TreeItem objNode)
        {
            // Check for null reference 
            if (objNode != null)
            {
                // Return count of sub nodes 
                return objNode.GetNodeCount(false);
            }
            else
            {
                // If object is null throw exception
                throw new ArgumentException("Node is null", "objNode");
            }
        }


        /// <summary>
        /// Sets the selected node.
        /// </summary>
        /// <param name="objTreeItem">The  tree node object.</param>
        /// <param name="intIndex">Index of the int.</param>
        /// <exception cref="System.NullReferenceException">The TreeItem not exist.</exception>
        public static void SetSelected(this TreeItem objTreeItem)
        {
            // Check for null reference 
            if (objTreeItem != null)
            {
                 // Get tree view control
                TreeElement objTreeElement = objTreeItem.ParentTree;

                // Set selected node 
                objTreeElement.SelectedItem = objTreeItem;

            }
            else
            {
                // If object is null throw exception
                throw new ArgumentException("Tree Item is null", "objTreeItem");
            }
        }


        /// <summary>
        ///  Returns a reference to the root Node object of a TreeElement control
        /// </summary>
        /// <param name="treeItem">The tree node object.</param>
        /// <returns>Reference to the root Node object of a TreeElement control</returns>
        /// <exception cref="System.NullReferenceException">The TreeItem not exist.</exception>
        public static TreeItem Root(this TreeItem treeItem)
        {
            // Check for null reference 
            if (treeItem != null)
            {
                // Loop until node parent is null
                while (treeItem.ParentElement != null)
                {
                    // Set parent node to be current node
                    treeItem = treeItem.ParentElement as TreeItem;
                }
                // Return the root node 
                return treeItem.FirstSibling();
            }
            else
            {
                // If object is null throw exception
                throw new ArgumentException("tree is null", "treeItem");
            }
        }


        /// <summary>
        /// Gets the last sibling.
        /// </summary>
        /// <param name="objTreeItem">The obj tree node.</param>
        /// <returns>LastSibling node</returns>
        /// <exception cref="System.NullReferenceException">The TreeItem not exist.</exception>
        public static TreeItem LastSibling(this TreeItem objTreeItem)
        {
            // Check for null reference
            if (objTreeItem != null)
            {
                // Loop until last node
                while (objTreeItem.NextNode != null)
                {
                    objTreeItem = objTreeItem.NextNode;
                }

                // Return LastSibling
                return objTreeItem;
            }

            else
            {
                // If object is null return 
                throw new ArgumentException("Tree Item is null", "objTreeItem");
            }
        }


        /// <summary>
        /// Gets the First Sibling.
        /// </summary>
        /// <param name="objTreeItem">The obj tree node.</param>
        /// <returns>FirstSibling node</returns>
        /// <exception cref="System.NullReferenceException">Node not exist.</exception>
        public static TreeItem FirstSibling(this TreeItem objTreeItem)
        {

            // Check for null reference
            if (objTreeItem != null)
            {
                // Loop until you find first node
                while (objTreeItem.PrevNode != null)
                {
                    objTreeItem = objTreeItem.PrevNode;
                }

                // Return First Sibling
                return objTreeItem;
            }

            else
            {
                // If object is null throw exception 
                throw new ArgumentException("Tree Item is null", "objTreeItem");
            }
        }


        /// <summary>
        /// Sets the expanded.
        /// </summary>
        /// <param name="objTreeItem">The obj tree node.</param>
        /// <param name="blnValue">if set to <c>true</c> [BLN value].</param>
        public static void SetExpanded(this TreeItem objTreeItem )
        {
            // validate parameter
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }
            throw new NotImplementedException();
        }


        /// <summary>
        ///  Returns a value which specifies if a Node object is expanded.
        /// </summary>
        /// <param name="objTreeItem">The obj tree node.</param>
        /// <returns>Returns a value which specifies if a Node object is expanded.</returns>
        public static bool GetExpanded(this TreeItem objTreeItem)
        {
            // validate parameter
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Dictionary used to store expanded image index or key
        /// </summary>
        private static Dictionary<TreeItem, Tuple<object, object>> objTreeItemDictionary = new Dictionary<TreeItem, Tuple<object, object>>();

        /// <summary>
        /// Sets expanded image to a node
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="objExpandedImage">Image key or index</param>
        public static void SetExpandedImage(this TreeItem objTreeItem, object objExpandedImage)
        {
            // validate parameter
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }


            // Check key
            bool blnParseInt = false;
            string strExpandedImage = objExpandedImage as string;
            int intExpandedImage = 0;

            // if nut key - check index
            if (strExpandedImage == null && objExpandedImage != null)
            {
                blnParseInt = int.TryParse(objExpandedImage.ToString(), out intExpandedImage);
            }

            // Check if this TreeItem exists in the list
            if (objTreeItemDictionary.ContainsKey(objTreeItem))
            {
                // Check whatever is objExpandedImage - index or key
                if (blnParseInt)
                {
                    // Store the image index for expanded image
                    objTreeItemDictionary[objTreeItem] = new Tuple<object, object>(objTreeItem.ImageIndex, objExpandedImage);
                }
                    // If expanded image is string
                else if (strExpandedImage != null)
                {
                    // Store image key for expanded image
                    objTreeItemDictionary[objTreeItem] = new Tuple<object, object>(objTreeItem.ImageKey, strExpandedImage);
                }
            }
                // In case when TreeItem object not exists add it in the list
            else
            {

                if (blnParseInt)
                {
                    objTreeItemDictionary.Add(objTreeItem, new Tuple<object, object>(objTreeItem.ImageIndex, objExpandedImage));
                }
                else if (strExpandedImage != null)
                {
                    objTreeItemDictionary.Add(objTreeItem, new Tuple<object, object>(objTreeItem.ImageKey, strExpandedImage));
                }
            }
        }

        /// <summary>
        /// Gets expanded image for TreeItem
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <returns>Expanded image</returns>
        public static Tuple<object, object> GetExpandedImg(this TreeItem objTreeItem)
        {
            if (objTreeItemDictionary.ContainsKey(objTreeItem))
            {
                return objTreeItemDictionary[objTreeItem];
            }

            return null;
        }

        /// <summary>
        /// Gets expanded image for TreeItem
        /// </summary>
        /// <param name="objTreenode">TreeItem object</param>
        /// <returns>Expanded image.</returns>
        public static object GetExpandedImage(this TreeItem objTreenode)
        {
            Tuple<object, object> objExpandedImage = objTreenode.GetExpandedImg();
            
            return objExpandedImage.Item2;
        }

        /// <summary>
        /// Sets selected image on node
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <param name="objSelectedImage">Index or key value</param>
        public static void SetSelectedImage(this TreeItem objTreeItem, object objSelectedImage)
        {
            // validate parameter
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }

            // Check if it's index
            if (objSelectedImage is int)
            {
                // Set the selected index on tree node
                objTreeItem.SelectedImageIndex = Convert.ToInt32(objSelectedImage, CultureInfo.InvariantCulture);
            }
                // If it's key
            else if (objSelectedImage is string)
            {
                // Set selected image key
                objTreeItem.SelectedImageKey = objSelectedImage.ToString();
            }
        }

        /// <summary>
        /// Gets selected image on node
        /// </summary>
        /// <param name="objTreeItem">TreeItem object</param>
        /// <returns>Index or key value</returns>
        public static object GetSelectedImage(this TreeItem objTreeItem)
        {
            // validate parameter
            if (objTreeItem == null)
            {
                throw new ArgumentNullException("objTreeItem");
            }

            // Check if selected image index exists
            if (objTreeItem.SelectedImageIndex != -1)
            {
                // Return it
                return objTreeItem.SelectedImageIndex;
            }
                // Check if selected image key exists 
            else if (!string.IsNullOrEmpty(objTreeItem.SelectedImageKey))
            {
                // Return it
                return objTreeItem.SelectedImageKey;
            }

            // Return null
            return null;
        }
        
    }
}

