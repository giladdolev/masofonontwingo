﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Extensions
{
 public sealed class FuncResults
    {

public static FuncResults<TValue1, TReturnValue> Return<TValue1, TReturnValue>(TValue1 value1, TReturnValue returnValue)
{
return new FuncResults<TValue1, TReturnValue>(value1, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TReturnValue> Return<TValue1, TValue2, TReturnValue>(TValue1 value1, TValue2 value2, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TReturnValue>(value1, value2, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TReturnValue> Return<TValue1, TValue2, TValue3, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TReturnValue>(value1, value2, value3, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TReturnValue>(value1, value2, value3, value4, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TReturnValue>(value1, value2, value3, value4, value5, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TReturnValue>(value1, value2, value3, value4, value5, value6, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TReturnValue>(value1, value2, value3, value4, value5, value6, value7, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TReturnValue>(value1, value2, value3, value4, value5, value6, value7, value8, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TReturnValue>(value1, value2, value3, value4, value5, value6, value7, value8, value9, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TReturnValue>(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10, TValue11 value11, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TReturnValue>(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, returnValue);
}		

		public static FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TValue12, TReturnValue> Return<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TValue12, TReturnValue>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10, TValue11 value11, TValue12 value12, TReturnValue returnValue)
{
return new FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TValue12, TReturnValue>(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12, returnValue);
}		

		} 

 public struct FuncResults<TValue1, TReturnValue>
{
private TValue1 _value1;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TReturnValue returnValue)
{
_value1 = value1;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1)
{
value1 = _value1;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2)
{
value1 = _value1;
value2 = _value2;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TValue6 value6 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TValue6 value6 ,TValue7 value7 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TValue6 value6 ,TValue7 value7 ,TValue8 value8 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TValue6 value6 ,TValue7 value7 ,TValue8 value8 ,TValue9 value9 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;
private TValue10 _value10;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TValue6 value6 ,TValue7 value7 ,TValue8 value8 ,TValue9 value9 ,TValue10 value10 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
_value10 = value10;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9, out TValue10 value10)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
value10 = _value10;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;
private TValue10 _value10;
private TValue11 _value11;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TValue6 value6 ,TValue7 value7 ,TValue8 value8 ,TValue9 value9 ,TValue10 value10 ,TValue11 value11 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
_value10 = value10;
_value11 = value11;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9, out TValue10 value10, out TValue11 value11)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
value10 = _value10;
value11 = _value11;
return _returnValue;
}

}

public struct FuncResults<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TValue12, TReturnValue>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;
private TValue10 _value10;
private TValue11 _value11;
private TValue12 _value12;
private TReturnValue _returnValue;

public FuncResults(TValue1 value1 ,TValue2 value2 ,TValue3 value3 ,TValue4 value4 ,TValue5 value5 ,TValue6 value6 ,TValue7 value7 ,TValue8 value8 ,TValue9 value9 ,TValue10 value10 ,TValue11 value11 ,TValue12 value12 ,TReturnValue returnValue)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
_value10 = value10;
_value11 = value11;
_value12 = value12;
_returnValue = returnValue;
}

public TReturnValue Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9, out TValue10 value10, out TValue11 value11, out TValue12 value12)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
value10 = _value10;
value11 = _value11;
value12 = _value12;
return _returnValue;
}

}

			 public sealed class Results
			{
				
public static Results<TValue1> Return<TValue1>(TValue1 value1)
{
return new Results<TValue1>(value1);
}

public static Results<TValue1 ,TValue2> Return<TValue1 ,TValue2>(TValue1 value1, TValue2 value2)
{
return new Results<TValue1, TValue2>(value1, value2);
}

public static Results<TValue1 ,TValue2 ,TValue3> Return<TValue1 ,TValue2 ,TValue3>(TValue1 value1, TValue2 value2, TValue3 value3)
{
return new Results<TValue1, TValue2, TValue3>(value1, value2, value3);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4> Return<TValue1 ,TValue2 ,TValue3 ,TValue4>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4)
{
return new Results<TValue1, TValue2, TValue3, TValue4>(value1, value2, value3, value4);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5>(value1, value2, value3, value4, value5);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6>(value1, value2, value3, value4, value5, value6);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7>(value1, value2, value3, value4, value5, value6, value7);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8>(value1, value2, value3, value4, value5, value6, value7, value8);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9>(value1, value2, value3, value4, value5, value6, value7, value8, value9);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9 ,TValue10> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9 ,TValue10>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10>(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9 ,TValue10 ,TValue11> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9 ,TValue10 ,TValue11>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10, TValue11 value11)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11>(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11);
}

public static Results<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9 ,TValue10 ,TValue11 ,TValue12> Return<TValue1 ,TValue2 ,TValue3 ,TValue4 ,TValue5 ,TValue6 ,TValue7 ,TValue8 ,TValue9 ,TValue10 ,TValue11 ,TValue12>(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10, TValue11 value11, TValue12 value12)
{
return new Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TValue12>(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12);
}
}

public struct Results<TValue1>
{
private TValue1 _value1;

public Results(TValue1 value1)
{
_value1 = value1;
}

public void Retrieve(out TValue1 value1)
{
value1 = _value1;
}
}


public struct Results<TValue1, TValue2>
{
private TValue1 _value1;
private TValue2 _value2;

public Results(TValue1 value1, TValue2 value2)
{
_value1 = value1;
_value2 = value2;
}

public void Retrieve(out TValue1 value1, out TValue2 value2)
{
value1 = _value1;
value2 = _value2;
}
}


public struct Results<TValue1, TValue2, TValue3>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;

public Results(TValue1 value1, TValue2 value2, TValue3 value3)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;
private TValue10 _value10;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
_value10 = value10;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9, out TValue10 value10)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
value10 = _value10;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;
private TValue10 _value10;
private TValue11 _value11;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10, TValue11 value11)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
_value10 = value10;
_value11 = value11;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9, out TValue10 value10, out TValue11 value11)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
value10 = _value10;
value11 = _value11;
}
}


public struct Results<TValue1, TValue2, TValue3, TValue4, TValue5, TValue6, TValue7, TValue8, TValue9, TValue10, TValue11, TValue12>
{
private TValue1 _value1;
private TValue2 _value2;
private TValue3 _value3;
private TValue4 _value4;
private TValue5 _value5;
private TValue6 _value6;
private TValue7 _value7;
private TValue8 _value8;
private TValue9 _value9;
private TValue10 _value10;
private TValue11 _value11;
private TValue12 _value12;

public Results(TValue1 value1, TValue2 value2, TValue3 value3, TValue4 value4, TValue5 value5, TValue6 value6, TValue7 value7, TValue8 value8, TValue9 value9, TValue10 value10, TValue11 value11, TValue12 value12)
{
_value1 = value1;
_value2 = value2;
_value3 = value3;
_value4 = value4;
_value5 = value5;
_value6 = value6;
_value7 = value7;
_value8 = value8;
_value9 = value9;
_value10 = value10;
_value11 = value11;
_value12 = value12;
}

public void Retrieve(out TValue1 value1, out TValue2 value2, out TValue3 value3, out TValue4 value4, out TValue5 value5, out TValue6 value6, out TValue7 value7, out TValue8 value8, out TValue9 value9, out TValue10 value10, out TValue11 value11, out TValue12 value12)
{
value1 = _value1;
value2 = _value2;
value3 = _value3;
value4 = _value4;
value5 = _value5;
value6 = _value6;
value7 = _value7;
value8 = _value8;
value9 = _value9;
value10 = _value10;
value11 = _value11;
value12 = _value12;
}
}

			
			
 }
*/