﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Extensions
{
    public static class EventHelper
    {
        #region KeyEventArgs
        /// <summary>
        /// Gets the key event arguments.
        /// </summary>
        /// <param name="intKeyCode">The key code.</param>
        /// <param name="intShift">The shift.</param>
        /// <returns></returns>
        public static KeyEventArgs GetKeyEventArgs(int intKeyCode, int intShift)
        {
            Keys enmKeys = (Keys)intKeyCode;
            if ((intShift & 1) > 0)
            {
                enmKeys |= Keys.Shift;
            }
            if ((intShift & 2) > 0)
            {
                enmKeys |= Keys.Control;
            }
            if ((intShift & 4) > 0)
            {
                enmKeys |= Keys.Alt;
            }
            KeyEventArgs objEventArgs = new KeyEventArgs(enmKeys);

            return objEventArgs;
        }

        /// <summary>
        /// Gets the shift modifier.
        /// </summary>
        /// <param name="objKeyEventArgs">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        public static int GetShift(KeyEventArgs objKeyEventArgs)
        {
            // validate parameter
            if (objKeyEventArgs == null)
            {
                throw new ArgumentNullException("objKeyEventArgs");
            }

            int intShift = 0;
            if (objKeyEventArgs.Modifiers == Keys.Alt)
            {
                intShift |= 4;
            }
            if (objKeyEventArgs.Modifiers == Keys.Control)
            {
                intShift |= 2;
            }
            if (objKeyEventArgs.Modifiers == Keys.Shift)
            {
                intShift |= 1;
            }
            return intShift;
        }
        #endregion

        /// <summary>
        /// Gets the mouse event arguments.
        /// </summary>
        /// <param name="intButton">The button.</param>
        /// <param name="intShift">The shift.</param>
        /// <param name="intX">The X.</param>
        /// <param name="intY">The Y.</param>
        /// <returns></returns>
        public static MouseEventArgs GetMouseEventArgs(int intButton, int intShift, int intX, int intY)
        {
            MouseButtons enmButtons = MouseButtons.Left;
            if ((intButton & 1) > 0)
            {
                enmButtons = MouseButtons.Left;
            }
            else if ((intButton & 2) > 0)
            {
                enmButtons = MouseButtons.Right;
            }
            else if ((intButton & 4) > 0)
            {
                enmButtons = MouseButtons.Middle;
            }
            MouseEventArgs objMouseEventArgs = new MouseEventArgs(enmButtons, 1, intX, intY, intShift);

            return objMouseEventArgs;
        }

        /// <summary>
        /// Gets the mouse button.
        /// </summary>
        /// <param name="enmMouseButtons">The mouse buttons.</param>
        /// <returns></returns>
        public static int GetMouseButton(MouseButtons enmMouseButtons)
        {
            if (enmMouseButtons == MouseButtons.Left)
            {
                return 1;
            }
            else if (enmMouseButtons == MouseButtons.Right)
            {
                return 2;
            }
            return 3;
        }

        /// <summary>
        /// Gets the window closing event arguments.
        /// </summary>
        /// <param name="blnCancel">if set to <c>true</c> [BLN cancel].</param>
        /// <param name="intUnloadMode">The unload mode.</param>
        /// <returns></returns>
        public static WindowClosingEventArgs GetWindowClosingEventArgs(bool blnCancel, int intUnloadMode)
        {
            CloseReason enmCloseReason = CloseReason.UserClosing;
            switch (intUnloadMode)
            {
                case 4:
                    enmCloseReason = CloseReason.MdiFormClosing;
                    break;
                case 5:
                    enmCloseReason = CloseReason.FormOwnerClosing;
                    break;
            }
            WindowClosingEventArgs objWindowClosingEventArgs = new WindowClosingEventArgs(enmCloseReason, blnCancel);

            return objWindowClosingEventArgs;
        }

        /// <summary>
        /// Gets the unload mode.
        /// </summary>
        /// <param name="enmCloseReason">The close reason.</param>
        /// <returns></returns>
        public static int GetUnloadMode(CloseReason enmCloseReason)
        {
            if (enmCloseReason == CloseReason.MdiFormClosing)
            {
                return 4;
            }
            else if (enmCloseReason == CloseReason.FormOwnerClosing)
            {
                return 5;
            }
            return 0;
        }
    }
}

