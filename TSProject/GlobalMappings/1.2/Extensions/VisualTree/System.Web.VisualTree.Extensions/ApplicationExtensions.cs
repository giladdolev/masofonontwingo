﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationExtensions
    {
        private static Dictionary<Type, string> _typeNameMapping = new Dictionary<Type, string>()
        {
            {typeof(ListViewElement), "ListView"},
            {typeof(ButtonElement), "CommandButton"},
            {typeof(WindowElement), "Form"},
            {typeof(LabelElement), "Label"},
            {typeof(TextBoxElement), "TextBox"},
            {typeof(ComboBoxElement), "ComboBox"},
            {typeof(CheckBoxElement), "CheckBox"},
            {typeof(GroupBoxElement), "Frame"},
            {typeof(TabElement), "SSTab"},
            {typeof(RadioButtonElement), "OptionButton"}
        };


        /// <summary>
        /// Get the type name of the provided instance
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static string TypeName(object instance)
        {
            // If instance is VisualElement
            if (instance is VisualElement)
            {
                // Get the instance type object
                Type instanceType = instance.GetType();

                // Init variable for the instance name
                string instanceTypeName = null;

                // Try get the type name of the instance from the dictionary
                if (_typeNameMapping.TryGetValue(instanceType, out instanceTypeName))
                {
                    return instanceTypeName;
                }
            }

            // Default behavior using VisualBasic assembly
            return TypeName(instance);
        }

        /// <summary>
        /// Initializes the array of objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objArray">The array.</param>
        public static void InitializeArrayOfObjects<T>(params T[] objArray)
            where T : new()
        {
            if (objArray != null)
            {
                for (int i = 0; i < objArray.Length; i++)
                {
                    if (objArray[i] == null)
                    {
                        objArray[i] = new T();
                    }
                }
            }
        }

        /// <summary>
        /// Creates the array of objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arraySize">Size of the array.</param>
        /// <param name="array">The array.</param>
        /// <returns></returns>
        public static T[] CreateArrayOfObjects<T>(int arraySize)
            where T : new()
        {
            checked
            {
                arraySize++;

            }
                
            T[] array = Array.CreateInstance(typeof(T), arraySize) as T[];
            if (array != null)
            {
                InitializeArrayOfObjects(array);
            }

            return array;
        }

        /// <summary>
        /// Unloads the specified instance.
        /// </summary>
        /// <param name="instance">The instance.</param>
        public static void Unload(object instance)
        {
            WindowElement windowElement = instance as WindowElement;

            if (windowElement != null)
            {
                windowElement.Close();
            }
        }

        /// <summary>
        /// Does the events.
        /// </summary>
        public static void DoEvents()
        {            
            ApplicationElement.DoEvents();
        }




        /// <summary>
        /// Logs the event.
        /// </summary>
        /// <param name="message">The log message.</param>
        /// <param name="eventType">Type of the event.</param>
        public static void LogEvent(string message, EventLogEntryType eventType)
        {
            string source = ApplicationElement.ProductName;
            if (!EventLog.SourceExists(source))
                EventLog.CreateEventSource(source, "Application");

            EventLog.WriteEntry(source, message, eventType);
        }

        /// <summary>
        /// Exits this instance.
        /// </summary>
        public static void Exit()
        {

            BrowserUtils.NavigateUrl("about:blank");
        }


        /// <summary>
        /// Loads a picture.
        /// </summary>
        /// <returns></returns>
        public static ResourceReference LoadPicture()
        {
            return null;
        }

        /// <summary>
        /// Loads the picture.
        /// </summary>
        /// <param name="FileName">Name of the file.</param>
        /// <returns></returns>
        public static ResourceReference LoadPicture(object FileName)
        {
            return null;
        }

        /// <summary>
        /// Loads the picture.
        /// </summary>
        /// <param name="FileName">Name of the file.</param>
        /// <param name="Size">The size.</param>
        /// <returns></returns>
        public static ResourceReference LoadPicture(object FileName, object Size)
        {
            return null;
        }

        /// <summary>
        /// Loads the picture.
        /// </summary>
        /// <param name="FileName">Name of the file.</param>
        /// <param name="Size">The size.</param>
        /// <param name="ColorDepth">The color depth.</param>
        /// <returns></returns>
        public static ResourceReference LoadPicture(object FileName, object Size, object ColorDepth)
        {
            return null;
        }

        /// <summary>
        /// Loads the picture.
        /// </summary>
        /// <param name="FileName">Name of the file.</param>
        /// <param name="Size">The size.</param>
        /// <param name="ColorDepth">The color depth.</param>
        /// <param name="X">The x.</param>
        /// <returns></returns>
        public static ResourceReference LoadPicture(object FileName, object Size, object ColorDepth, object X)
        {
            return null;
        }

        /// <summary>
        /// Loads the picture.
        /// </summary>
        /// <param name="FileName">Name of the file.</param>
        /// <param name="Size">The size.</param>
        /// <param name="ColorDepth">The color depth.</param>
        /// <param name="X">The x.</param>
        /// <param name="Y">The y.</param>
        /// <returns></returns>
        public static ResourceReference LoadPicture(object FileName, object Size, object ColorDepth, object X, object Y)
        {
            return null;
        }


        /// <summary>
        /// Strings the PTR.
        /// </summary>
        /// <param name="object">The object.</param>
        /// <returns></returns>
        public static object StrPtr(object @object)
        {
            return null;
        }

        /// <summary>
        /// Variables the PTR.
        /// </summary>
        /// <param name="object">The object.</param>
        /// <returns></returns>
        public static object VarPtr(object @object)
        {
            return null;
        }
    }
}
