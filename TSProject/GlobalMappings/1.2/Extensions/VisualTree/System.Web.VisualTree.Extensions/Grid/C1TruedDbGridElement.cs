﻿using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Extensions
{
    public class C1TruedDbGridElement : GridElement
    {

        public enum AddNewModeEnum
        {
            AddNewCurrent,
            AddNewPending,
            NoAddNew
        }

        #region Events

        /// <summary>
        /// Occurs Paint.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> Paint;

        /// <summary>
        /// Gets a value indicating whether this instance has Paint listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasPaintListeners
        {
            get { return Paint != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Paint" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Paint"/> instance containing the event data.</param>
        protected virtual void OnPaint(EventArgs args)
        {

            // Check if there are listeners.
            if (Paint != null)
            {
                Paint(this, args);
            }
        }

        #endregion



        private AddNewModeEnum _addNewMode;
        /// <summary>
        /// Gets or sets the AddNewMode.
        /// </summary>
        /// <value>
        /// The AddNewMode.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public AddNewModeEnum AddNewMode
        {
            get { return _addNewMode; }
            set
            {
                if (_addNewMode != value)
                {
                    _addNewMode = value;
                    OnPropertyChanged("AddNewMode");
                }
            }
        }


        public GridLines RowDivider
        {

            get
            {
                throw new NotImplementedException();
            }
        }


        public bool FilterActive
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public PresentationEnum Presentation
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public bool Translate
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public bool Validate
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public int MaxComboItems
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public GridCellStyle OddRowStyle
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public GridCellStyle EvenRowStyle
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int GroupByAreaVisible
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool RecordSelectors
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public SplitCollection Splits
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public event EventHandler OnAddNew
        {
            add
            {

            }
            remove
            {

            }
        }

        public int RowTop(int row)
        {
            throw new NotImplementedException();
        }

    }
}
