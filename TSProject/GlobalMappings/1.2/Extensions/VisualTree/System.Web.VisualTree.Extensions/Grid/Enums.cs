﻿namespace System.Web.VisualTree.Extensions
{
    public enum LineStyleEnum
    {
        Double,
        Inset,
        None,
        Raised,
        Single
    }

    public enum PresentationEnum
    {
        CheckBox,
        ComboBox,
        Normal,
        RadioButton,
        SortedComboBox
    }

    public enum SizeModeEnum
    {
        Exact,
        NumberOfColumns,
        Scalable
    }
}
