﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Extensions
{
    public class C1TruedDbGridColumn : GridColumn
    {
        public object CellValue(int rowIndex)
        {
            GridElement grid = this.Parent as GridElement;
            if (grid != null && (rowIndex >= 0 && grid.Rows.Count < rowIndex))
            {
                GridRow row = grid.Rows[rowIndex];
                GridCellCollection cells = row.Cells;
                if (cells != null && rowIndex < cells.Count)
                {
                    return cells[rowIndex].Value;
                }
            }
            return null;
        }

        public bool Locked
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string FilterText
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AutoComplete
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


    }


}
