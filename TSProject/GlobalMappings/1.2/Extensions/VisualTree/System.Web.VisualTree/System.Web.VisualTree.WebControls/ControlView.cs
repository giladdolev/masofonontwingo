﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.WebControls
{
    [Designer("System.Web.VisualTree.Design.RootComponentDesigner, System.Web.VisualTree.Design")]
    [ToolboxData("<{0}:ControlView runat=\"server\"></{0}:ControlView>")]
    public class ControlView : Control
    {
         /// <summary>
        /// The composite controls
        /// </summary>
        private List<System.Web.UI.Control> _controls = new List<System.Web.UI.Control>();

        /// <summary>
        /// Flag indicating if model was initialized.
        /// </summary>
        bool _modelInitialized = false;

        /// <summary>
        /// Flag indicating we are in create view mode
        /// </summary>
        bool _preventRendering = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlView" /> class.
        /// </summary>
        public ControlView()
            : base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Control" /> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager"></param>
        internal ControlView(ControlElement visualElement, ComponentManager componentManager = null)
            : base(visualElement, componentManager)
        {

        }


        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
            this.Element = new ControlElement();
        }

        /// <summary>
        /// Gets the current rendering environment.
        /// </summary>
        /// <value>
        /// The current rendering environment.
        /// </value>
        protected override RenderingEnvironment CurrentRenderingEnvironment
        {
            get
            {
                return RenderingEnvironment.ExtJS;
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if visible; otherwise, <c>false</c>.
        /// </value>
        public override bool Visible
        {
            get
            {
                return true;
            }
            set
            {

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Init" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            bool isInitialized = false;

            // If is not in design mode
            if (!this.DesignMode)
            {
                // Ensure the composite view is updated (ASPMVC model)
                this.DataBind();

                // Get the main control
                IVisualTreeComponent mainControl = _controls.OfType<IVisualTreeComponent>().FirstOrDefault();

                // If there is a valid main control
                if (mainControl != null)
                {
                    // Get composite element
                    ControlElement modelControlElement = ComponentLoader.OnInitializeWindow(mainControl as Control, mainControl.Element, ref _preventRendering) as ControlElement;

                    // If there is a valid composite element
                    if (modelControlElement != null)
                    {

                        // Get the composite element
                        ControlElement compositeElement = mainControl.Element as ControlElement;

                        // If there is a valid composite element
                        if (compositeElement != null)
                        {
                            // Set the model element
                            this.Element = modelControlElement;

                            // If there is a different element
                            if (compositeElement != modelControlElement)
                            {
                                // Copy the composite properties
                                SyncModel(modelControlElement, compositeElement);

                                // Reattach events to the new element
                                ReattachEvents();
                            }


                            // The fields cache
                            Dictionary<string, FieldInfo> fieldsCache = new Dictionary<string, FieldInfo>();

                            // Fill named controls in the model
                            FillNamedControls(fieldsCache, this.Page.Controls, modelControlElement, modelControlElement.GetType(), false);

                            base.OnInit(e);
                            isInitialized = true;

                            // Perform the view initialized event
                            modelControlElement.PerformInitialize();


                            // Run composite initialize
                            ComponentLoader.InitializeWindow(this, modelControlElement);
                        }
                    }
                }
            }

            if (!isInitialized)
            {
                base.OnInit(e);
            }
        }



        /// <summary>
        /// Reattaches the events.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        private void ReattachEvents()
        {
            this.LoadAction = this.LoadAction;
        }

        /// <summary>
        /// Fills the named controls.
        /// </summary>
        /// <param name="fieldsCache">The fields cache.</param>
        /// <param name="controlCollection">The control collection.</param>
        /// <param name="instance">The instance.</param>
        /// <param name="instanceType">The instance type.</param>
        /// <exception cref="System.Exception"></exception>
        private void FillNamedControls(Dictionary<string, FieldInfo> fieldsCache, ControlCollection controlCollection, ControlElement instance, Type instanceType, bool isComponentManager)
        {
            // If there is a valid control collection
            if (controlCollection != null)
            {
                // Loop all controls
                foreach (System.Web.UI.Control control in controlCollection)
                {
                    // If there is a valid control
                    if (control != null)
                    {
                        // If is a valid control id
                        string controlID = control.ID;

                        // If is a named control
                        if (controlID != null)
                        {
                            try
                            {
                                // Get tree component
                                IVisualTreeComponent treeComponent = control as IVisualTreeComponent;

                                // If there is a valid tree component
                                if (treeComponent != null)
                                {
                                    // If is component manager
                                    if (isComponentManager && instance != null)
                                    {
                                        //instance.AddGhostElement(treeComponent.Element);
                                    }

                                    // Try to get field info
                                    FieldInfo fieldInfo = GetFieldInfo(fieldsCache, instanceType, controlID);

                                    // If there is a valid field
                                    if (fieldInfo != null)
                                    {
                                        // Set the field value
                                        fieldInfo.SetValue(instance, treeComponent.Element);
                                    }

                                    // If is a potential array list control
                                    if (controlID.StartsWith("_"))
                                    {
                                        // Get control ID array
                                        string[] controlIDArray = controlID.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

                                        // If there is a valid control array
                                        if (controlIDArray.Length > 1)
                                        {
                                            int index = 0;

                                            // Try to get last part as number
                                            if (int.TryParse(controlIDArray[controlIDArray.Length - 1], out index))
                                            {
                                                // Get non array id
                                                string nonArrayID = string.Join("_", controlIDArray, 0, controlIDArray.Length - 1);

                                                // If there is a valid non array id
                                                if (!string.IsNullOrEmpty(nonArrayID))
                                                {
                                                    // Try to get field info
                                                    FieldInfo arrayFieldInfo = GetFieldInfo(fieldsCache, instanceType, nonArrayID);

                                                    // If there is a valid field
                                                    if (arrayFieldInfo != null)
                                                    {
                                                        // Set the field value
                                                        IList list = arrayFieldInfo.GetValue(instance) as IList;

                                                        // If there is a valid list
                                                        if (list != null)
                                                        {
                                                            // Add the list element
                                                            while (list.Count <= index)
                                                            {
                                                                list.Add(null);
                                                            }

                                                            // Set the element at the specific index
                                                            list[index] = treeComponent.Element;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception exception)
                            {
                                // Throw a informative exception
                                throw new Exception(string.Format("Failed to set '{0}' component to in the matching module field.", controlID), exception);
                            }
                        }

                        // Fill the named controls
                        FillNamedControls(fieldsCache, control.Controls, instance, instanceType, control is ComponentManager);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the field information.
        /// </summary>
        /// <param name="fieldsCache">The fields cache.</param>
        /// <param name="instanceType">Type of the instance.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        private static FieldInfo GetFieldInfo(Dictionary<string, FieldInfo> fieldsCache, Type instanceType, string fieldName)
        {
            FieldInfo fieldInfo = null;

            if (fieldName != null)
            {
                if (!fieldsCache.TryGetValue(fieldName, out fieldInfo))
                {
                    fieldsCache[fieldName] = fieldInfo = instanceType.GetField(fieldName, Reflection.BindingFlags.Public | Reflection.BindingFlags.NonPublic | Reflection.BindingFlags.Instance);
                }
            }

            return fieldInfo;
        }

        /// <summary>
        /// Addeds the control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="index">The index.</param>
        protected override void AddedControl(System.Web.UI.Control control, int index)
        {
            // If model was initialized
            if (this.DesignMode || _modelInitialized)
            {
                base.AddedControl(control, index);
            }
            else
            {
                _controls.Add(control);
            }
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            // If is not in prevent rendering mode
            if (!_preventRendering)
            {
                base.RenderControl(writer);
            }
        }


        /// <summary>
        /// Gets or sets the render to.
        /// </summary>
        /// <value>
        /// The render to.
        /// </value>
        public string RenderTo
        {
            get;
            set;
        }

        /// <summary>
        /// Renders the element to.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="output">The output.</param>
        /// <param name="renderTo">The render to.</param>
        protected override void RenderElementTo(RenderingContext renderingContext, HtmlTextWriter output, string renderTo = null)
        {
            // If is in design mode
            if (this.DesignMode)
            {
                base.RenderElementTo(renderingContext, output);
            }
            else
            {
                base.RenderElementTo(renderingContext, output, this.RenderTo);
            }
        }


    }
}
