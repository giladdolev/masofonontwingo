﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace System.Web.VisualTree.WebControls
{

    [ToolboxData("<{0}:PartialControl runat=\"server\"></{0}:PartialControl>")]
    public class PartialControl : Control
    {

        /// <summary>
        /// The loaded
        /// </summary>
        private bool _loaded = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="PartialControl"/> class.
        /// </summary>
        public PartialControl() : base()
        {

        }

        public override void DataBind()
        {
            base.DataBind();
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="PartialControl"/> class.
        /// </summary>
        /// <param name="visualElement"></param>
        /// <param name="componentManager"></param>
        internal PartialControl(ControlElement visualElement, ComponentManager componentManager = null)
            : base(visualElement, componentManager,false)
		{
            Type actualType = ControlResolver.GetType(visualElement);
            if (actualType != null)
            {
                this.Controller = this.Action = actualType.Name;

                try
                {
                    // Get Area name
                    this.Area = actualType.InvokeMember("__GetExtendedProperty", Reflection.BindingFlags.Default, null, visualElement, new object[] { "Area" }) as string;
                }
                catch (Exception exception)
                {
                    Trace.WriteLine(exception.ToString());
                }
            }
		}

        /// <summary>
        /// Gets or sets the controller.
        /// </summary>
        /// <value>
        /// The controller.
        /// </value>
        public string Controller
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        public string Action
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the area.
        /// </summary>
        /// <value>
        /// The area.
        /// </value>
        public string Area
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new ControlElement Element
        {
            get
            {
                return base.Element as ControlElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
            this.Element = new ControlElement();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        /// <summary>
        /// Loads instance.
        /// </summary>
        public void Load()
        {
            if (!_loaded)
            {
                _loaded = true;

                ControlElement actualElement = null;

                ControlElement currentElement = this.Element;

                if (!string.IsNullOrWhiteSpace(this.Controller))
                {
                    if (!string.IsNullOrWhiteSpace(this.Action))
                    {
                        actualElement = VisualElementHelper.CreateFromView<ControlElement>(this.Controller, this.Action,this.Area, this.GetArguments(), this.GetAttributesAsProperties());
                    }
                    else
                    {
                        actualElement = VisualElementHelper.CreateFromView<ControlElement>(this.Controller,this.Area, this.GetArguments(), this.GetAttributesAsProperties());
                    }
                }

                if (actualElement != null && actualElement != currentElement)
                {
                    if (currentElement != null)
                    {
                        base.SyncModel(actualElement, currentElement);
                    }

                    this.Element = actualElement;

                    this.ReattachEvents();
                }
            }
        }

        /// <summary>
        /// Gets the arguments.
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, object> GetArguments()
        {
            IDictionary<string, object> arguments = new Dictionary<string, object>();
            foreach (System.Web.UI.Control control in this.Controls)
            {
                PartialArgument argument = control as PartialArgument;
                if (argument != null)
                {
                    arguments[argument.Name] = argument.Value;
                }
                
            }
            return arguments;
        }

        /// <summary>
        /// Gets the attributes as properties.
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, object> GetAttributesAsProperties()
        {
            // The control properties
            IDictionary<string, object> properties = null;

            // Loop all keys
            foreach(object keyObject in this.Attributes.Keys)
            {
                // Get key as string
                string key = keyObject as string;

                // If there is a valid key
                if(!string.IsNullOrEmpty(key))
                {
                    // If we need to create properties
                    if (properties == null)
                    {
                        // Create properties
                        properties = new Dictionary<string, object>();
                    }

                    // Set the current attribute
                    properties[key] = this.Attributes[key];
                }
            }

            // Return properties
            return properties;
        }

        /// <summary>
        /// Loads the attributes.
        /// </summary>
        protected override void LoadAttributes()
        {
            // Prevent default processing
        }

        /// <summary>
        /// Reattaches the events.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        private void ReattachEvents()
        {
            this.DataSourceCurrentItemChangedAction = this.DataSourceCurrentItemChangedAction;
            this.DataSourceItemCreatedAction = this.DataSourceItemCreatedAction;
            this.DataSourceItemDeletedAction = this.DataSourceItemDeletedAction;
            this.DataSourceItemUpdatedAction = this.DataSourceItemUpdatedAction;
            this.LoadAction = this.LoadAction;
        } 
       
    }

    /// <summary>
    /// Provides support for passing arguments to controller actions.
    /// </summary>
    [ToolboxData("<{0}:PartialArgument runat=\"server\" Name=\"\" Value=\"\"></{0}:PartialArgument>")]
    public class PartialArgument : WebControl
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value
        {
            get;
            set;
        }
    }
}
