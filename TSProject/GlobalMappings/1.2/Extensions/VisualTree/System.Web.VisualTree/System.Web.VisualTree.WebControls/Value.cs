﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace System.Web.VisualTree
{
    /// <summary>
    /// Indicates a value item
    /// </summary>
    public interface IValue
    {
        /// <summary>
        /// Gets the object value.
        /// </summary>
        /// <value>
        /// The object value.
        /// </value>
        object ObjectValue
        {
            get;
        }
    }

    /// <summary>
    /// Provides support for typed list items
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    [ParseChildren(false)]
    public abstract class ValueItem<TValue> : IValue
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public TValue Value
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the object value.
        /// </summary>
        /// <value>
        /// The object value.
        /// </value>
        object IValue.ObjectValue
        {
            get
            {
                return ObjectValue;
            }
        }


        protected object ObjectValue
        {
            get
            {
                return this.Value;
            }
        }
    }

    public static class ValueListExtensions
    {
        /// <summary>
        /// To the value list.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static ValueList ToValueList(this IList<object> values)
        {
            return new ValueList(values);
        }
    }

    public class ValueList : Collection<IValue>
    {
        /// <summary>
        /// The values
        /// </summary>
        private IList<object> _values;

        /// <summary>
        /// The sync
        /// </summary>
        private bool _sync = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueList"/> class.
        /// </summary>
        /// <param name="values">The values.</param>
        public ValueList(IList<object> values)
        {
            // Set the values list
            _values = values;

            // If there is a valid values enumerator
            if(values != null)
            {
                // Loop all values
                foreach(object value in values)
                {
                    // Add current value
                    this.AddValue(value);
                }
            }
        }

        /// <summary>
        /// Adds the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        private  void AddValue(object value)
        {
            IValue listValue = value as IValue;

            if(listValue != null)
            {
                base.Add(listValue);

                return;
            }
            try
            {
                _sync = false;

                if (value is string)
                {
                    base.Add(new StringValue() { Value = (string)value });
                }
                else if (value is bool)
                {
                    base.Add(new BooleanValue() { Value = (bool)value });
                }
                else if (value is int)
                {
                    base.Add(new Int32Value() { Value = (int)value });
                }
                else if (value is short)
                {
                    base.Add(new Int16Value() { Value = (short)value });
                }
                else if (value is long)
                {
                    base.Add(new Int64Value() { Value = (long)value });
                }
                else if (value is DateTime)
                {
                    base.Add(new DateTimeValue() { Value = (DateTime)value });
                }
                else
                {
                    base.Add(new ObjectValue() { Value = value });
                }
            }
            finally
            {
                _sync = true;
            }
        }

        /// <summary>
        /// Inserts an element into the <see cref="T:System.Collections.ObjectModel.Collection`1" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert. The value can be null for reference types.</param>
        protected override void InsertItem(int index, IValue item)
        {
            if (item != null)
            {
                if (_sync)
                {
                    if (_values != null)
                    {
                        _values.Add(item.ObjectValue);
                    }
                }
                base.InsertItem(index, item);
            }
        }

        /// <summary>
        /// Replaces the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to replace.</param>
        /// <param name="item">The new value for the element at the specified index. The value can be null for reference types.</param>
        protected override void SetItem(int index, IValue item)
        {
            if (item != null)
            {
                if (_sync)
                {
                    if (_values != null)
                    {
                        _values.Add(item.ObjectValue);
                    }
                }
                base.SetItem(index, item);
            }

        }
    }

    
    public class ObjectValue : ValueItem<object>
    {

    }

    
    public class StringValue : ValueItem<string>
    {

    }

    
    public class BooleanValue : ValueItem<bool>
    {

    }

    
    public class Int32Value : ValueItem<int>
    {

    }

    
    public class Int16Value : ValueItem<short>
    {

    }

    
    public class Int64Value : ValueItem<long>
    {

    }

    
    public class DateTimeValue : ValueItem<DateTime>
    {

    }
}
