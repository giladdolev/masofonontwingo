﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace System.Web.VisualTree.WebControls
{

    [ToolboxData("<{0}:Repeater runat=\"server\"></{0}:Repeater>")]
    public class Repeater : System.Web.UI.WebControls.Repeater, IVisualTreeComponentGenerator
    {
        public override void DataBind()
        {
            base.DataBind();
        }
        /// <summary>
        /// Gets the generated components.
        /// </summary>
        /// <value>
        /// The generated components.
        /// </value>
        IEnumerable<IVisualTreeComponent> IVisualTreeComponentGenerator.GeneratedComponents
        {
            get
            {
                // Loop all controls
                foreach (System.Web.UI.Control control in this.Controls)
                {
                    // Get repeater item
                    RepeaterItem repeaterItem = control as RepeaterItem;

                    // If there is a valid repeater item
                    if (repeaterItem != null)
                    {
                        // Loop all controls
                        foreach (System.Web.UI.Control repeaterItemControl in repeaterItem.Controls)
                        {
                            // Get as visual tree component
                            IVisualTreeComponent visualTreeComponent = repeaterItemControl as IVisualTreeComponent;

                            // If there is a valid visual tree component
                            if (visualTreeComponent != null)
                            {
                                // Return the visual tree component
                                yield return visualTreeComponent;
                            }
                        }
                    }
                }
            }
        }
    }
}
