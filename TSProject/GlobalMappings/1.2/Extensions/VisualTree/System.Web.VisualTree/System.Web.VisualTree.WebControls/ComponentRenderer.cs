﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.WebControls
{
    
    internal class ComponentRenderer
    {
        /// <summary>
        /// The _rendering engine
        /// </summary>
        [Import(typeof(IRenderingEngine))]
        private IRenderingEngine _renderingEngine;

        /// <summary>
        /// The _loader
        /// </summary>
        private static ComponentRenderer _loader = new ComponentRenderer();

        /// <summary>
        /// Initializes the components.
        /// </summary>
        private ComponentRenderer()
        {
            LoadingHelper.CombineCatalog(null, this);
        }

        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="renderTo">The render to.</param>
        internal static void Render(RenderingContext renderingContext, IVisualElement visualElement, TextWriter writer, string renderTo)
        {
            if (_loader != null && _loader._renderingEngine != null)
            {
                _loader._renderingEngine.Render(renderingContext, visualElement, writer, renderTo);
            }
        }
    }
}
