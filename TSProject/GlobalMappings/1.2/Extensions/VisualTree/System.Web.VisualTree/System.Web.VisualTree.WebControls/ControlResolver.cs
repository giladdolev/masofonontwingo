﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.WebControls
{
    internal class ControlResolver : ControlUtils.ControlResolverBase
    {
        public override UI.WebControls.WebControl GetControl<T>(T visualElement, string typeName, ComponentManager componentManager)
        {
            switch (typeName)
            {
                case "System.Web.VisualTree.Elements.CompositeElement":
                    if (visualElement.ParentElement == null)
                    {
                        return new CompositeView(visualElement as System.Web.VisualTree.Elements.CompositeElement, componentManager);
                    }
                    return new PartialControl(visualElement as System.Web.VisualTree.Elements.CompositeElement, componentManager);
                case "System.Web.VisualTree.Elements.WindowElement":
                    if (visualElement.ParentElement == null)
                    {
                        return new WindowView(visualElement as System.Web.VisualTree.Elements.WindowElement, componentManager);
                    }
                    return new PartialControl(visualElement as System.Web.VisualTree.Elements.CompositeElement, componentManager);
            }
            return base.GetControl<T>(visualElement, typeName, componentManager);
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        internal static Type GetType(object instance)
        {
            if (instance != null)
            {
                IDictionary<object, Type> interpreterRepository = ControlTypeResolver.TypeResolver;
                if (interpreterRepository != null)
                {
                    Type interpreterType = null;
                    if (interpreterRepository.TryGetValue(instance, out interpreterType))
                    {
                        return interpreterType;
                    }
                }
            }
            return null;
        }
    }

    public class ControlTypeResolver
    {


        /// <summary>
        /// Gets or sets the type resolver.
        /// </summary>
        /// <value>
        /// The type resolver.
        /// </value>
        public static IDictionary<object, Type> TypeResolver
        {
            get;
            set;
        }
    }
}
