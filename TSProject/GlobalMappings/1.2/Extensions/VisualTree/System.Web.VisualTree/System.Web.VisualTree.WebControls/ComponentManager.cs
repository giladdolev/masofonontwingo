﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.InteropServices;

namespace System.Web.VisualTree.WebControls
{
    [ToolboxData("<{0}:ComponentManager runat=\"server\"></{0}:ComponentManager>")]
    [NonVisualControl]
    [ParseChildren]
    public sealed class ComponentManager : System.Web.UI.Control
    {
        /// <summary>
        /// The registered element
        /// </summary>
        private readonly HashSet<VisualElement> _registeredElement = new HashSet<VisualElement>();

        /// <summary>
        /// The control reviser
        /// </summary>
        private readonly IDictionary<string, object> _persistParameters = null;

        /// <summary>
        /// The control reviser
        /// </summary>
        private readonly Action<System.Web.UI.Control> _controlReviser = null;

        /// <summary>
        /// The control domain reviser
        /// </summary>
        private readonly _MethodBase _controlDomainReviser = null;


        public ComponentManager()
        {
            // Empty CTOR is mandatory in order to display non visual controls in aspx files 
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentManager"/> class.
        /// </summary>
        public ComponentManager(IDictionary<string, object> persistParameters = null)
        {
            // Set persist parameters
            _persistParameters = persistParameters;

            // Get the control reviser
            _controlReviser = GetParameter<Action<System.Web.UI.Control>>(_persistParameters, "controlReviser");

            // Get the control reviser
            _controlDomainReviser = GetParameter<_MethodBase>(_persistParameters, "controlDomainReviser");
        }

        /// <summary>
        /// Gets the master page file.
        /// </summary>
        /// <value>
        /// The master page file.
        /// </value>
        public string MasterPageFile
        {
            get
            {
                return GetParameter<string>(_persistParameters, "MasterPageFile", "~/Site.Master");
            }
        }


        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        public string Language
        {
            get
            {
                return GetParameter<string>(_persistParameters, "Language", "C#");
            }
        }


        /// <summary>
        /// Gets the inherits.
        /// </summary>
        /// <value>
        /// The inherits.
        /// </value>
        public string Inherits
        {
            get
            {
                return GetParameter<string>(_persistParameters, "Inherits", string.Format("System.Web.Mvc.ViewPage<{0}>", this.Model));
            }
        }

        /// <summary>
        /// Gets the Visual Tree assembly.
        /// </summary>
        /// <value>
        /// The Visual Tree assembly.
        /// </value>
        public string VTAssembly
        {
            get
            {
                return GetParameter<string>(_persistParameters, "VTAssembly", "System.Web.VisualTree.WebControls");
            }
        }


        /// <summary>
        /// Gets the Visual Tree namespace.
        /// </summary>
        /// <value>
        /// The Visual Tree namespace.
        /// </value>
        public string VTNamespace
        {
            get
            {
                return GetParameter<string>(_persistParameters, "VTNamespace", "System.Web.VisualTree.WebControls");
            }
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public string Model
        {
            get
            {
                return GetParameter<string>(_persistParameters, "Model", "System.Object");
            }
        }


        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <typeparam name="TParameterType">The the parameter type.</typeparam>
        /// <param name="persistParameters">The persist parameters.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        private TParameterType GetParameter<TParameterType>(IDictionary<string, object> persistParameters, string name, TParameterType defaultValue = default(TParameterType))
            where TParameterType : class
        {
            // If there are valid persist parameters
            if(persistParameters != null)
            {
                // The parameter value
                object parameterValue = null;

                // Try to get parameters value
                if(persistParameters.TryGetValue(name, out parameterValue))
                {
                    // Return parameters value
                    return parameterValue as TParameterType;
                }
            }

            return defaultValue;
        }


        /// <summary>
        /// Revises the control.
        /// </summary>
        /// <param name="control">The control.</param>
        internal void ReviseControl(System.Web.UI.Control control)
        {
            // If there is a valid control reviser
            if (_controlReviser != null)
            {
                // Revise control
                _controlReviser(control);
            }

            if(_controlDomainReviser != null)
            {
                _controlDomainReviser.Invoke(null, new object[] { control.ID, new ControlProxy(control) });
            }
        }

        private class ControlProxy : MarshalByRefObject, IDictionary<string, object>
        {
            /// <summary>
            /// The control
            /// </summary>
            private readonly System.Web.UI.Control _control;

            /// <summary>
            /// Initializes a new instance of the <see cref="ControlProxy"/> class.
            /// </summary>
            /// <param name="control">The control.</param>
            public ControlProxy(System.Web.UI.Control control)
            {
                _control = control;
            }

            /// <summary>
            /// Gets the type of the control.
            /// </summary>
            /// <value>
            /// The type of the control.
            /// </value>
            private Type ControlType
            {
                get { return _control.GetType(); }
            }

            /// <summary>
            /// Sets the property.
            /// </summary>
            /// <param name="name">The name.</param>
            /// <param name="value">The value.</param>
            /// <returns></returns>
            private bool SetProperty(string name, object value)
            {
                Reflection.PropertyInfo propertyInfo = this.ControlType.GetProperty(name);

                if(propertyInfo != null)
                {
                    if(value == null && propertyInfo.PropertyType.IsValueType)
                    {
                        // Get default value for value type.
                        value = Activator.CreateInstance(propertyInfo.PropertyType);
                    }

                    propertyInfo.SetValue(_control, value);

                    return true;
                }
                else
                {
                    WebControl webControl = _control as WebControl;

                    if(webControl != null)
                    {
                        webControl.Attributes[name] = Convert.ToString(value);
                    }
                }

                return false;
            }

            /// <summary>
            /// Tries the get property.
            /// </summary>
            /// <param name="name">The name.</param>
            /// <param name="value">The value.</param>
            /// <returns></returns>
            private bool TryGetProperty(string name, out object value)
            {
                value = null;

                Reflection.PropertyInfo propertyInfo = this.ControlType.GetProperty(name);

                if (propertyInfo != null)
                {
                    value = propertyInfo.GetValue(_control);

                    return true;
                }
                else
                {
                    WebControl webControl = _control as WebControl;

                    if (webControl != null)
                    {
                        if (!string.IsNullOrEmpty(webControl.Attributes[name]))
                        {
                            value = webControl.Attributes[name];

                            return true;
                        }
                    }
                }
                return false;
            }



            public void Add(string key, object value)
            {
                this[key] = value;
            }

            public bool ContainsKey(string key)
            {
                return this.ControlType.GetMember(key).Any();
            }

            public ICollection<string> Keys
            {
                get
                {
                    return (from propertyInfo in this.ControlType.GetProperties()
                            select propertyInfo.Name)
                            .ToList();                    
                }
            }

            public bool Remove(string key)
            {
                return this.SetProperty(key, null);
            }

            public bool TryGetValue(string key, out object value)
            {
                return TryGetProperty(key, out value);
            }

            public ICollection<object> Values
            {
                get
                {
                    return (from propertyInfo in this.ControlType.GetProperties()
                            select propertyInfo.GetValue(_control))
                            .ToList();
                }
            }

            public object this[string key]
            {
                get
                {
                    object value = null;

                    TryGetProperty(key, out value);

                    return value;
                }
                set
                {
                    SetProperty(key, value);
                }
            }

            public void Add(KeyValuePair<string, object> item)
            {
                SetProperty(item.Key, item.Value);
            }

            public void Clear()
            {
                
            }

            public bool Contains(KeyValuePair<string, object> item)
            {
                object value = null;
                if(TryGetProperty(item.Key, out value))
                {
                    return object.Equals(item.Value, value);                    
                }

                return false;
            }

            public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
            {                
            }

            public int Count
            {
                get
                {
                    return this.ControlType.GetProperties().Count();
                }
            }

            public bool IsReadOnly
            {
                get { return false; }
            }

            public bool Remove(KeyValuePair<string, object> item)
            {
                return SetProperty(item.Key, null);
            }

            public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
            {
                return (from propertyInfo in this.ControlType.GetProperties()
                        select new KeyValuePair<string, object>(propertyInfo.Name, propertyInfo.GetValue(_control)))
                        .GetEnumerator();
            }

            Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
        /// </summary>
        /// <returns>true if the control is visible on the page; otherwise false.</returns>
        public override bool Visible
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        /// <summary>
        /// Registers the component.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public void RegisterComponent(VisualElement visualElement)
        {
            // If there is a valid visual element
            if (visualElement != null)
            {
                // If visual element is not registered
                if (_registeredElement.Add(visualElement))
                {
                    // Get control 
                    WebControl control = ControlUtils.GetControl(visualElement, this);

                    // If there is a valid control
                    if (control != null)
                    {
                        // Add the current control
                        this.Controls.Add(control);
                    }
                }
            }
        }
    }
}
