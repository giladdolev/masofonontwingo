﻿using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Common;
using System.Linq;
   
namespace System.Web.VisualTree.WebControls
{ 
	/// <summary>
    /// Server designer control related utilities
    /// </summary>
	public static class ControlUtils
	{ 

		internal class ControlResolverBase
		{
			/// <summary>
			/// Gets the control for underlined visual element.
			/// </summary>
			/// <typeparam name="T"></typeparam>
			/// <param name="visualElement">The visual element.</param>
			/// <param name="componentManager">The component manager.</param>
			/// <returns></returns>
			public virtual WebControl GetControl<T>(T visualElement, string typeName, ComponentManager componentManager) where T : System.Web.VisualTree.Elements.VisualElement
			{
				switch (typeName)
				{  
					case "System.Web.VisualTree.Elements.SeriesLabelStyle":
						return new SeriesLabelStyle(visualElement as System.Web.VisualTree.Elements.SeriesLabelStyle, componentManager);
					case "System.Web.VisualTree.Elements.ComboboxColumn":
						return new ComboboxColumn(visualElement as System.Web.VisualTree.Elements.ComboboxColumn, componentManager);
					case "System.Web.VisualTree.Elements.PanelElement":
						return new Panel(visualElement as System.Web.VisualTree.Elements.PanelElement, componentManager);
					case "System.Web.VisualTree.Elements.CompositeElementBase":
						return new CompositeBase(visualElement as System.Web.VisualTree.Elements.CompositeElementBase, componentManager);
					case "System.Web.VisualTree.Elements.Compatibility.FileSystemBase":
						return new FileSystemBase(visualElement as System.Web.VisualTree.Elements.Compatibility.FileSystemBase, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarElement":
						return new ToolBar(visualElement as System.Web.VisualTree.Elements.ToolBarElement, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarDropDownMenu":
						return new ToolBarDropDownMenu(visualElement as System.Web.VisualTree.Elements.ToolBarDropDownMenu, componentManager);
					case "System.Web.VisualTree.Elements.ContextMenuStripElement":
						return new ContextMenuStrip(visualElement as System.Web.VisualTree.Elements.ContextMenuStripElement, componentManager);
					case "System.Web.VisualTree.Elements.DataViewBand":
						return new DataViewBand(visualElement as System.Web.VisualTree.Elements.DataViewBand, componentManager);
					case "System.Web.VisualTree.Elements.DataViewField":
						return new DataViewField(visualElement as System.Web.VisualTree.Elements.DataViewField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewShapeField":
						return new DataViewShapeField(visualElement as System.Web.VisualTree.Elements.DataViewShapeField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewGroupBoxField":
						return new DataViewGroupBoxField(visualElement as System.Web.VisualTree.Elements.DataViewGroupBoxField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewImageField":
						return new DataViewImageField(visualElement as System.Web.VisualTree.Elements.DataViewImageField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewLabelBaseField":
						return new DataViewLabelBaseField(visualElement as System.Web.VisualTree.Elements.DataViewLabelBaseField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewButtonField":
						return new DataViewButtonField(visualElement as System.Web.VisualTree.Elements.DataViewButtonField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewCheckField":
						return new DataViewCheckField(visualElement as System.Web.VisualTree.Elements.DataViewCheckField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewRadioField":
						return new DataViewRadioField(visualElement as System.Web.VisualTree.Elements.DataViewRadioField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewDropDownField":
						return new DataViewDropDownField(visualElement as System.Web.VisualTree.Elements.DataViewDropDownField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewComboField":
						return new DataViewComboField(visualElement as System.Web.VisualTree.Elements.DataViewComboField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewTextField":
						return new DataViewTextField(visualElement as System.Web.VisualTree.Elements.DataViewTextField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewDateField":
						return new DataViewDateField(visualElement as System.Web.VisualTree.Elements.DataViewDateField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewElement":
						return new DataView(visualElement as System.Web.VisualTree.Elements.DataViewElement, componentManager);
					case "System.Web.VisualTree.Elements.DataViewLabelField":
						return new DataViewLabelField(visualElement as System.Web.VisualTree.Elements.DataViewLabelField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewNumericField":
						return new DataViewNumericField(visualElement as System.Web.VisualTree.Elements.DataViewNumericField, componentManager);
					case "System.Web.VisualTree.Elements.DataViewTreeViewField":
						return new DataViewTreeViewField(visualElement as System.Web.VisualTree.Elements.DataViewTreeViewField, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarItem":
						return new ToolBarItem(visualElement as System.Web.VisualTree.Elements.ToolBarItem, componentManager);
					case "System.Web.VisualTree.Elements.DropDownButtonElement":
						return new DropDownButton(visualElement as System.Web.VisualTree.Elements.DropDownButtonElement, componentManager);
					case "System.Web.VisualTree.Elements.RibbonPageGroupElement":
						return new RibbonPageGroup(visualElement as System.Web.VisualTree.Elements.RibbonPageGroupElement, componentManager);
					case "System.Web.VisualTree.Elements.RadioButtonElement":
						return new RadioButton(visualElement as System.Web.VisualTree.Elements.RadioButtonElement, componentManager);
					case "System.Web.VisualTree.RadioGroupItemElement":
						return new RadioGroupItem(visualElement as System.Web.VisualTree.RadioGroupItemElement, componentManager);
					case "System.Web.VisualTree.Elements.RadioGroupElement":
						return new RadioGroup(visualElement as System.Web.VisualTree.Elements.RadioGroupElement, componentManager);
					case "System.Web.VisualTree.Elements.TextBoxElement":
						return new TextBox(visualElement as System.Web.VisualTree.Elements.TextBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.TimeEditElement":
						return new TimeEdit(visualElement as System.Web.VisualTree.Elements.TimeEditElement, componentManager);
					case "System.Web.VisualTree.Elements.DockPanelDocumentElement":
						return new DockPanelDocument(visualElement as System.Web.VisualTree.Elements.DockPanelDocumentElement, componentManager);
					case "System.Web.VisualTree.Elements.DockPanelToolElement":
						return new DockPanelTool(visualElement as System.Web.VisualTree.Elements.DockPanelToolElement, componentManager);
					case "System.Web.VisualTree.Elements.ErrorProviderElement":
						return new ErrorProvider(visualElement as System.Web.VisualTree.Elements.ErrorProviderElement, componentManager);
					case "System.Web.VisualTree.Elements.GridElement":
						return new Grid(visualElement as System.Web.VisualTree.Elements.GridElement, componentManager);
					case "System.Web.VisualTree.Elements.WidgetGridElement":
						return new WidgetGrid(visualElement as System.Web.VisualTree.Elements.WidgetGridElement, componentManager);
					case "System.Web.VisualTree.Elements.GridItemElement":
						return new GridItem(visualElement as System.Web.VisualTree.Elements.GridItemElement, componentManager);
					case "System.Web.VisualTree.Elements.GridBand":
						return new GridBand(visualElement as System.Web.VisualTree.Elements.GridBand, componentManager);
					case "System.Web.VisualTree.Elements.GridColumn":
						return new GridColumn(visualElement as System.Web.VisualTree.Elements.GridColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridHtmlContentColumn":
						return new GridHtmlContentColumn(visualElement as System.Web.VisualTree.Elements.GridHtmlContentColumn, componentManager);
					case "System.Web.VisualTree.Elements.ComboBoxElement":
						return new ComboBox(visualElement as System.Web.VisualTree.Elements.ComboBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.GridMultiComboBoxElement":
						return new GridMultiComboBox(visualElement as System.Web.VisualTree.Elements.GridMultiComboBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.WidgetColumnMultiComboBoxElement":
						return new WidgetColumnMultiComboBox(visualElement as System.Web.VisualTree.Elements.WidgetColumnMultiComboBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.WidgetColumn":
						return new WidgetColumn(visualElement as System.Web.VisualTree.Elements.WidgetColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridTextButtonColumn":
						return new GridTextButtonColumn(visualElement as System.Web.VisualTree.Elements.GridTextButtonColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridComboBoxColumn":
						return new GridComboBoxColumn(visualElement as System.Web.VisualTree.Elements.GridComboBoxColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridContentCell":
						return new GridContentCell(visualElement as System.Web.VisualTree.Elements.GridContentCell, componentManager);
					case "System.Web.VisualTree.Elements.GridCustomColumn":
						return new GridCustomColumn(visualElement as System.Web.VisualTree.Elements.GridCustomColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridImageCell":
						return new GridImageCell(visualElement as System.Web.VisualTree.Elements.GridImageCell, componentManager);
					case "System.Web.VisualTree.Elements.GridLinkColumn":
						return new GridLinkColumn(visualElement as System.Web.VisualTree.Elements.GridLinkColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridDateTimePickerColumn":
						return new GridDateTimePickerColumn(visualElement as System.Web.VisualTree.Elements.GridDateTimePickerColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridRowHeader":
						return new GridRowHeader(visualElement as System.Web.VisualTree.Elements.GridRowHeader, componentManager);
					case "System.Web.VisualTree.Elements.GridTextBoxCell":
						return new GridTextBoxCell(visualElement as System.Web.VisualTree.Elements.GridTextBoxCell, componentManager);
					case "System.Web.VisualTree.Elements.HTMLElement":
						return new HTML(visualElement as System.Web.VisualTree.Elements.HTMLElement, componentManager);
					case "System.Web.VisualTree.Elements.KeyItem":
						return new KeyItem(visualElement as System.Web.VisualTree.Elements.KeyItem, componentManager);
					case "System.Web.VisualTree.Elements.LabelElement":
						return new Label(visualElement as System.Web.VisualTree.Elements.LabelElement, componentManager);
					case "System.Web.VisualTree.Elements.LinkLabelElement":
						return new LinkLabel(visualElement as System.Web.VisualTree.Elements.LinkLabelElement, componentManager);
					case "System.Web.VisualTree.Elements.ListItem":
						return new ListItem(visualElement as System.Web.VisualTree.Elements.ListItem, componentManager);
					case "System.Web.VisualTree.Elements.ComboGridElement":
						return new ComboGrid(visualElement as System.Web.VisualTree.Elements.ComboGridElement, componentManager);
					case "System.Web.VisualTree.Elements.MaskedTextBoxElement":
						return new MaskedTextBox(visualElement as System.Web.VisualTree.Elements.MaskedTextBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.RibbonPageElement":
						return new RibbonPage(visualElement as System.Web.VisualTree.Elements.RibbonPageElement, componentManager);
					case "System.Web.VisualTree.Elements.TabElement":
						return new Tab(visualElement as System.Web.VisualTree.Elements.TabElement, componentManager);
					case "System.Web.VisualTree.Elements.AccordionElement":
						return new Accordion(visualElement as System.Web.VisualTree.Elements.AccordionElement, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarCheckItem":
						return new ToolBarCheckItem(visualElement as System.Web.VisualTree.Elements.ToolBarCheckItem, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarMenuItem":
						return new ToolBarMenuItem(visualElement as System.Web.VisualTree.Elements.ToolBarMenuItem, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarMenuSeparator":
						return new ToolBarMenuSeparator(visualElement as System.Web.VisualTree.Elements.ToolBarMenuSeparator, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarTextBox":
						return new ToolBarTextBox(visualElement as System.Web.VisualTree.Elements.ToolBarTextBox, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TCardsElement":
						return new TCards(visualElement as System.Web.VisualTree.Elements.Touch.TCardsElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TComboBoxElement":
						return new TComboBox(visualElement as System.Web.VisualTree.Elements.Touch.TComboBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TComboBoxFieldElement":
						return new TComboBoxField(visualElement as System.Web.VisualTree.Elements.Touch.TComboBoxFieldElement, componentManager);
					case "System.Web.VisualTree.Elements.TreeColumn":
						return new TreeColumn(visualElement as System.Web.VisualTree.Elements.TreeColumn, componentManager);
					case "System.Web.VisualTree.Elements.WidgetColumnTextBoxElement":
						return new WidgetColumnTextBox(visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.DataEntryElement":
						return new DataEntry(visualElement as System.Web.VisualTree.Elements.DataEntryElement, componentManager);
					case "System.Web.VisualTree.Elements.DockPanelElement":
						return new DockPanel(visualElement as System.Web.VisualTree.Elements.DockPanelElement, componentManager);
					case "System.Web.VisualTree.Elements.Compatibility.DirectoryListBoxElement":
						return new DirectoryListBox(visualElement as System.Web.VisualTree.Elements.Compatibility.DirectoryListBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.Compatibility.DriveListBoxElement":
						return new DriveListBox(visualElement as System.Web.VisualTree.Elements.Compatibility.DriveListBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.Compatibility.FileListBoxElement":
						return new FileListBox(visualElement as System.Web.VisualTree.Elements.Compatibility.FileListBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.FileUploadElement":
						return new FileUpload(visualElement as System.Web.VisualTree.Elements.FileUploadElement, componentManager);
					case "System.Web.VisualTree.Elements.ProgressBarElement":
						return new ProgressBar(visualElement as System.Web.VisualTree.Elements.ProgressBarElement, componentManager);
					case "System.Web.VisualTree.Elements.ResourceReference":
						return new ResourceReference(visualElement as System.Web.VisualTree.Elements.ResourceReference, componentManager);
					case "System.Web.VisualTree.Elements.IconReference":
						return new IconReference(visualElement as System.Web.VisualTree.Elements.IconReference, componentManager);
					case "System.Web.VisualTree.Elements.ImageReference":
						return new ImageReference(visualElement as System.Web.VisualTree.Elements.ImageReference, componentManager);
					case "System.Web.VisualTree.Elements.UrlReference":
						return new UrlReference(visualElement as System.Web.VisualTree.Elements.UrlReference, componentManager);
					case "System.Web.VisualTree.Elements.LineElement":
						return new Line(visualElement as System.Web.VisualTree.Elements.LineElement, componentManager);
					case "System.Web.VisualTree.Elements.ShapeElement":
						return new Shape(visualElement as System.Web.VisualTree.Elements.ShapeElement, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarDropDownButton":
						return new ToolBarDropDownButton(visualElement as System.Web.VisualTree.Elements.ToolBarDropDownButton, componentManager);
					case "System.Web.VisualTree.Elements.ButtonElement":
						return new Button(visualElement as System.Web.VisualTree.Elements.ButtonElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TButtonElement":
						return new TButton(visualElement as System.Web.VisualTree.Elements.Touch.TButtonElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TCarouselElement":
						return new TCarousel(visualElement as System.Web.VisualTree.Elements.Touch.TCarouselElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TDockPanelElement":
						return new TDockPanel(visualElement as System.Web.VisualTree.Elements.Touch.TDockPanelElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TFormElement":
						return new TForm(visualElement as System.Web.VisualTree.Elements.Touch.TFormElement, componentManager);
					case "System.Web.VisualTree.Elements.GroupBoxElement":
						return new GroupBox(visualElement as System.Web.VisualTree.Elements.GroupBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TFormFieldSetElement":
						return new TFormFieldSet(visualElement as System.Web.VisualTree.Elements.Touch.TFormFieldSetElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TLabelElement":
						return new TLabel(visualElement as System.Web.VisualTree.Elements.Touch.TLabelElement, componentManager);
					case "System.Web.VisualTree.Elements.ListViewElement":
						return new ListView(visualElement as System.Web.VisualTree.Elements.ListViewElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TListViewElement":
						return new TListView(visualElement as System.Web.VisualTree.Elements.Touch.TListViewElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TPanelElement":
						return new TPanel(visualElement as System.Web.VisualTree.Elements.Touch.TPanelElement, componentManager);
					case "System.Web.VisualTree.Elements.SplitContainer":
						return new SplitContainer(visualElement as System.Web.VisualTree.Elements.SplitContainer, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TSplitContainer":
						return new TSplitContainer(visualElement as System.Web.VisualTree.Elements.Touch.TSplitContainer, componentManager);
					case "System.Web.VisualTree.Elements.SplitterPanelElement":
						return new SplitterPanel(visualElement as System.Web.VisualTree.Elements.SplitterPanelElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TSplitterPanelElement":
						return new TSplitterPanel(visualElement as System.Web.VisualTree.Elements.Touch.TSplitterPanelElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TTabElement":
						return new TTab(visualElement as System.Web.VisualTree.Elements.Touch.TTabElement, componentManager);
					case "System.Web.VisualTree.Elements.TabItem":
						return new TabItem(visualElement as System.Web.VisualTree.Elements.TabItem, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TTabItem":
						return new TTabItem(visualElement as System.Web.VisualTree.Elements.Touch.TTabItem, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TTextBoxElement":
						return new TTextBox(visualElement as System.Web.VisualTree.Elements.Touch.TTextBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TTextBoxFieldElement":
						return new TTextBoxField(visualElement as System.Web.VisualTree.Elements.Touch.TTextBoxFieldElement, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarButton":
						return new ToolBarButton(visualElement as System.Web.VisualTree.Elements.ToolBarButton, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TToolBarButton":
						return new TToolBarButton(visualElement as System.Web.VisualTree.Elements.Touch.TToolBarButton, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TToolBarDropDownButton":
						return new TToolBarDropDownButton(visualElement as System.Web.VisualTree.Elements.Touch.TToolBarDropDownButton, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TToolBarElement":
						return new TToolBar(visualElement as System.Web.VisualTree.Elements.Touch.TToolBarElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TToolBarItem":
						return new TToolBarItem(visualElement as System.Web.VisualTree.Elements.Touch.TToolBarItem, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TToolBarMenuItem":
						return new TToolBarMenuItem(visualElement as System.Web.VisualTree.Elements.Touch.TToolBarMenuItem, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarSeparator":
						return new ToolBarSeparator(visualElement as System.Web.VisualTree.Elements.ToolBarSeparator, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TToolBarSeparator":
						return new TToolBarSeparator(visualElement as System.Web.VisualTree.Elements.Touch.TToolBarSeparator, componentManager);
					case "System.Web.VisualTree.Elements.TreeElement":
						return new Tree(visualElement as System.Web.VisualTree.Elements.TreeElement, componentManager);
					case "System.Web.VisualTree.Elements.Touch.TTreeElement":
						return new TTree(visualElement as System.Web.VisualTree.Elements.Touch.TTreeElement, componentManager);
					case "System.Web.VisualTree.Elements.ChartAnnotation":
						return new ChartAnnotation(visualElement as System.Web.VisualTree.Elements.ChartAnnotation, componentManager);
					case "System.Web.VisualTree.Elements.Axis":
						return new Axis(visualElement as System.Web.VisualTree.Elements.Axis, componentManager);
					case "System.Web.VisualTree.Elements.TextAnnotation":
						return new TextAnnotation(visualElement as System.Web.VisualTree.Elements.TextAnnotation, componentManager);
					case "System.Web.VisualTree.Elements.CalloutAnnotation":
						return new CalloutAnnotation(visualElement as System.Web.VisualTree.Elements.CalloutAnnotation, componentManager);
					case "System.Web.VisualTree.Elements.Chart":
						return new Chart(visualElement as System.Web.VisualTree.Elements.Chart, componentManager);
					case "System.Web.VisualTree.Elements.ChartArea":
						return new ChartArea(visualElement as System.Web.VisualTree.Elements.ChartArea, componentManager);
					case "System.Web.VisualTree.Elements.CustomLabel":
						return new CustomLabel(visualElement as System.Web.VisualTree.Elements.CustomLabel, componentManager);
					case "System.Web.VisualTree.Elements.DataPointCustomProperties":
						return new DataPointCustomProperties(visualElement as System.Web.VisualTree.Elements.DataPointCustomProperties, componentManager);
					case "System.Web.VisualTree.Elements.DataPoint":
						return new DataPoint(visualElement as System.Web.VisualTree.Elements.DataPoint, componentManager);
					case "System.Web.VisualTree.Elements.ElementPosition":
						return new Position(visualElement as System.Web.VisualTree.Elements.ElementPosition, componentManager);
					case "System.Web.VisualTree.Elements.AxisLabelStyle":
						return new AxisLabelStyle(visualElement as System.Web.VisualTree.Elements.AxisLabelStyle, componentManager);
					case "System.Web.VisualTree.Elements.Legend":
						return new Legend(visualElement as System.Web.VisualTree.Elements.Legend, componentManager);
					case "System.Web.VisualTree.Elements.Series":
						return new Series(visualElement as System.Web.VisualTree.Elements.Series, componentManager);
					case "System.Web.VisualTree.Elements.ChartTitle":
						return new ChartTitle(visualElement as System.Web.VisualTree.Elements.ChartTitle, componentManager);
					case "System.Web.VisualTree.Elements.CompositeElement":
						return new Composite(visualElement as System.Web.VisualTree.Elements.CompositeElement, componentManager);
					case "System.Web.VisualTree.Elements.MenuElement":
						return new Menu(visualElement as System.Web.VisualTree.Elements.MenuElement, componentManager);
					case "System.Web.VisualTree.Elements.TimerElement":
						return new Timer(visualElement as System.Web.VisualTree.Elements.TimerElement, componentManager);
					case "System.Web.VisualTree.Elements.CheckBoxElement":
						return new CheckBox(visualElement as System.Web.VisualTree.Elements.CheckBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.CheckedListBoxElement":
						return new CheckedListBox(visualElement as System.Web.VisualTree.Elements.CheckedListBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.ColumnHeader":
						return new ColumnHeader(visualElement as System.Web.VisualTree.Elements.ColumnHeader, componentManager);
					case "System.Web.VisualTree.Elements.GridButtonColumn":
						return new GridButtonColumn(visualElement as System.Web.VisualTree.Elements.GridButtonColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridCheckBoxColumn":
						return new GridCheckBoxColumn(visualElement as System.Web.VisualTree.Elements.GridCheckBoxColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridHeaderCell":
						return new GridHeaderCell(visualElement as System.Web.VisualTree.Elements.GridHeaderCell, componentManager);
					case "System.Web.VisualTree.Elements.GridColumnHeaderCell":
						return new GridColumnHeaderCell(visualElement as System.Web.VisualTree.Elements.GridColumnHeaderCell, componentManager);
					case "System.Web.VisualTree.Elements.GridImageColumn":
						return new GridImageColumn(visualElement as System.Web.VisualTree.Elements.GridImageColumn, componentManager);
					case "System.Web.VisualTree.Elements.GridRow":
						return new GridRow(visualElement as System.Web.VisualTree.Elements.GridRow, componentManager);
					case "System.Web.VisualTree.Elements.GridTextBoxColumn":
						return new GridTextBoxColumn(visualElement as System.Web.VisualTree.Elements.GridTextBoxColumn, componentManager);
					case "System.Web.VisualTree.Elements.HScrollBar":
						return new HScrollBar(visualElement as System.Web.VisualTree.Elements.HScrollBar, componentManager);
					case "System.Web.VisualTree.Elements.ImageElement":
						return new Image(visualElement as System.Web.VisualTree.Elements.ImageElement, componentManager);
					case "System.Web.VisualTree.Elements.ImageList":
						return new ImageList(visualElement as System.Web.VisualTree.Elements.ImageList, componentManager);
					case "System.Web.VisualTree.Elements.ImageListStreamer":
						return new ImageListStreamer(visualElement as System.Web.VisualTree.Elements.ImageListStreamer, componentManager);
					case "System.Web.VisualTree.Elements.ListViewItem":
						return new ListViewItem(visualElement as System.Web.VisualTree.Elements.ListViewItem, componentManager);
					case "System.Web.VisualTree.Elements.ListViewSubitem":
						return new ListViewSubitem(visualElement as System.Web.VisualTree.Elements.ListViewSubitem, componentManager);
					case "System.Web.VisualTree.Elements.TableStyle":
						return new TableStyle(visualElement as System.Web.VisualTree.Elements.TableStyle, componentManager);
					case "System.Web.VisualTree.Elements.ColumnStyle":
						return new ColumnStyle(visualElement as System.Web.VisualTree.Elements.ColumnStyle, componentManager);
					case "System.Web.VisualTree.Elements.TreeItem":
						return new TreeItem(visualElement as System.Web.VisualTree.Elements.TreeItem, componentManager);
					case "System.Web.VisualTree.Elements.VScrollBar":
						return new VScrollBar(visualElement as System.Web.VisualTree.Elements.VScrollBar, componentManager);
					case "System.Web.VisualTree.Elements.DateTimePickerElement":
						return new DateTimePicker(visualElement as System.Web.VisualTree.Elements.DateTimePickerElement, componentManager);
					case "System.Web.VisualTree.Elements.WindowMdiContainer":
						return new WindowMdiContainer(visualElement as System.Web.VisualTree.Elements.WindowMdiContainer, componentManager);
					case "System.Web.VisualTree.Elements.NumericUpDownElement":
						return new NumericUpDown(visualElement as System.Web.VisualTree.Elements.NumericUpDownElement, componentManager);
					case "System.Web.VisualTree.Elements.RichTextBox":
						return new RichTextBox(visualElement as System.Web.VisualTree.Elements.RichTextBox, componentManager);
					case "System.Web.VisualTree.Elements.StatusElement":
						return new Status(visualElement as System.Web.VisualTree.Elements.StatusElement, componentManager);
					case "System.Web.VisualTree.Elements.RowStyle":
						return new RowStyle(visualElement as System.Web.VisualTree.Elements.RowStyle, componentManager);
					case "System.Web.VisualTree.Elements.TablePanel":
						return new TablePanel(visualElement as System.Web.VisualTree.Elements.TablePanel, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarContainer":
						return new ToolBarContainer(visualElement as System.Web.VisualTree.Elements.ToolBarContainer, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarLabel":
						return new ToolBarLabel(visualElement as System.Web.VisualTree.Elements.ToolBarLabel, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarProgressBar":
						return new ToolBarProgressBar(visualElement as System.Web.VisualTree.Elements.ToolBarProgressBar, componentManager);
					case "System.Web.VisualTree.Elements.ToolBarSplitButton":
						return new ToolBarSplitButton(visualElement as System.Web.VisualTree.Elements.ToolBarSplitButton, componentManager);
					case "System.Web.VisualTree.Elements.StatusLabel":
						return new StatusLabel(visualElement as System.Web.VisualTree.Elements.StatusLabel, componentManager);
					case "System.Web.VisualTree.Elements.RangeElement":
						return new Range(visualElement as System.Web.VisualTree.Elements.RangeElement, componentManager);
					case "System.Web.VisualTree.MonthCalendarElement":
						return new MonthCalendar(visualElement as System.Web.VisualTree.MonthCalendarElement, componentManager);
					case "System.Web.VisualTree.Elements.ListBoxElement":
						return new ListBox(visualElement as System.Web.VisualTree.Elements.ListBoxElement, componentManager);
					case "System.Web.VisualTree.Elements.WindowElement":
						return new Window(visualElement as System.Web.VisualTree.Elements.WindowElement, componentManager);
					case "System.Web.VisualTree.RibbonControlElement":
						return new RibbonControl(visualElement as System.Web.VisualTree.RibbonControlElement, componentManager);
				}

				return null;
			}
		}
        /// <summary>
        /// Gets the control for underlined visual element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="componentManager">The component manager.</param>
        /// <returns></returns>
		public static WebControl GetControl<T>(T visualElement, ComponentManager componentManager = null) where T : System.Web.VisualTree.Elements.VisualElement
		{
            Type actualType = visualElement.GetType();
			ControlResolver controlResolver = new ControlResolver();
			WebControl control = controlResolver.GetControl(visualElement, actualType.ToString(), componentManager);
			return control;
		}


        /// <summary>
        /// Resolves the control.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <param name="parent">The parent.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
		public static T2 ResolveControl<T1,T2>(WebControl parent,T1 item) where T1 : System.Web.VisualTree.Elements.VisualElement where T2 : WebControl
		{			
		if(item == null)
		{
		   return null;
		}
			foreach (var control in parent.Controls.OfType<Component>())
			{
			if(control.Element == item)
			 return control as T2;
			}

			return ControlUtils.GetControl(item) as T2;
		}
	}

	#region Property Collections
    /// <summary>
    /// ToolBarItemCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.ToolBarItemCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class ToolBarItemCollection : System.Collections.Generic.ICollection<ToolBarItem>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.ToolBarItemCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public ToolBarItemCollection(WebControl parent, System.Web.VisualTree.Elements.ToolBarItemCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ToolBarItem item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ToolBarItem item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ToolBarItem[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ToolBarItem[] elementArray = new Elements.ToolBarItem[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ToolBarItem,ToolBarItem>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ToolBarItem item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ToolBarItem> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ToolBarItem item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ToolBarItem,ToolBarItem>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ToolBarItem,ToolBarItem>(_parent,_elementCollection[index]);
            }
            set
            {
                ToolBarItem item = value as ToolBarItem;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ToolBarItem> Items
        {
            get
            {
                System.Collections.Generic.List<ToolBarItem> items = new System.Collections.Generic.List<ToolBarItem>();
                foreach (System.Web.VisualTree.Elements.ToolBarItem item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ToolBarItem,ToolBarItem>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ToolBarItem controlItem = item as ToolBarItem;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ToolBarItem controlItem = item as ToolBarItem;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ToolBarItem item = value as ToolBarItem;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ToolBarItem item = value as ToolBarItem;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ToolBarItem controlItem = item as ToolBarItem;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// ToolBarItemCollection collection 
    /// </summary>
    public class ToolBarItemCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.ToolBarItemCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public ToolBarItemCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ToolBarItem);
        }
	}

    /// <summary>
    /// DataViewFieldCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.DataViewFieldCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class DataViewFieldCollection : System.Collections.Generic.ICollection<DataViewField>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.DataViewFieldCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public DataViewFieldCollection(WebControl parent, System.Web.VisualTree.Elements.DataViewFieldCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(DataViewField item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(DataViewField item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(DataViewField[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.DataViewField[] elementArray = new Elements.DataViewField[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewField,DataViewField>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(DataViewField item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<DataViewField> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.DataViewField item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewField,DataViewField>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewField,DataViewField>(_parent,_elementCollection[index]);
            }
            set
            {
                DataViewField item = value as DataViewField;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<DataViewField> Items
        {
            get
            {
                System.Collections.Generic.List<DataViewField> items = new System.Collections.Generic.List<DataViewField>();
                foreach (System.Web.VisualTree.Elements.DataViewField item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewField,DataViewField>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            DataViewField controlItem = item as DataViewField;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            DataViewField controlItem = item as DataViewField;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            DataViewField item = value as DataViewField;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            DataViewField item = value as DataViewField;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            DataViewField controlItem = item as DataViewField;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// DataViewFieldCollection collection 
    /// </summary>
    public class DataViewFieldCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.DataViewFieldCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public DataViewFieldCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(DataViewField);
        }
	}

    /// <summary>
    /// DataViewBandCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.DataViewBandCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class DataViewBandCollection : System.Collections.Generic.ICollection<DataViewBand>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.DataViewBandCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public DataViewBandCollection(WebControl parent, System.Web.VisualTree.Elements.DataViewBandCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(DataViewBand item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(DataViewBand item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(DataViewBand[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.DataViewBand[] elementArray = new Elements.DataViewBand[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewBand,DataViewBand>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(DataViewBand item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<DataViewBand> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.DataViewBand item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewBand,DataViewBand>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewBand,DataViewBand>(_parent,_elementCollection[index]);
            }
            set
            {
                DataViewBand item = value as DataViewBand;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<DataViewBand> Items
        {
            get
            {
                System.Collections.Generic.List<DataViewBand> items = new System.Collections.Generic.List<DataViewBand>();
                foreach (System.Web.VisualTree.Elements.DataViewBand item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.DataViewBand,DataViewBand>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            DataViewBand controlItem = item as DataViewBand;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            DataViewBand controlItem = item as DataViewBand;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            DataViewBand item = value as DataViewBand;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            DataViewBand item = value as DataViewBand;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            DataViewBand controlItem = item as DataViewBand;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// DataViewBandCollection collection 
    /// </summary>
    public class DataViewBandCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.DataViewBandCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public DataViewBandCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(DataViewBand);
        }
	}

    /// <summary>
    /// GridColumnCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.GridColumnCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class GridColumnCollection : System.Collections.Generic.ICollection<GridColumn>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.GridColumnCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public GridColumnCollection(WebControl parent, System.Web.VisualTree.Elements.GridColumnCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(GridColumn item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(GridColumn item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(GridColumn[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.GridColumn[] elementArray = new Elements.GridColumn[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.GridColumn,GridColumn>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(GridColumn item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<GridColumn> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.GridColumn item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.GridColumn,GridColumn>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.GridColumn,GridColumn>(_parent,_elementCollection[index]);
            }
            set
            {
                GridColumn item = value as GridColumn;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<GridColumn> Items
        {
            get
            {
                System.Collections.Generic.List<GridColumn> items = new System.Collections.Generic.List<GridColumn>();
                foreach (System.Web.VisualTree.Elements.GridColumn item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.GridColumn,GridColumn>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            GridColumn controlItem = item as GridColumn;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            GridColumn controlItem = item as GridColumn;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            GridColumn item = value as GridColumn;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            GridColumn item = value as GridColumn;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            GridColumn controlItem = item as GridColumn;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// GridColumnCollection collection 
    /// </summary>
    public class GridColumnCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.GridColumnCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public GridColumnCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(GridColumn);
        }
	}

    /// <summary>
    /// ListItemCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.ListItemCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class ListItemCollection : System.Collections.Generic.ICollection<ListItem>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.ListItemCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public ListItemCollection(WebControl parent, System.Web.VisualTree.Elements.ListItemCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ListItem item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ListItem item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ListItem[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ListItem[] elementArray = new Elements.ListItem[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListItem,ListItem>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ListItem item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ListItem> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ListItem item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListItem,ListItem>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListItem,ListItem>(_parent,_elementCollection[index]);
            }
            set
            {
                ListItem item = value as ListItem;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ListItem> Items
        {
            get
            {
                System.Collections.Generic.List<ListItem> items = new System.Collections.Generic.List<ListItem>();
                foreach (System.Web.VisualTree.Elements.ListItem item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListItem,ListItem>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ListItem controlItem = item as ListItem;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ListItem controlItem = item as ListItem;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ListItem item = value as ListItem;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ListItem item = value as ListItem;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ListItem controlItem = item as ListItem;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// ListItemCollection collection 
    /// </summary>
    public class ListItemCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.ListItemCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public ListItemCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ListItem);
        }
	}

    /// <summary>
    /// TabItemCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.TabItemCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class TabItemCollection : System.Collections.Generic.ICollection<TabItem>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.TabItemCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public TabItemCollection(WebControl parent, System.Web.VisualTree.Elements.TabItemCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(TabItem item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(TabItem item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(TabItem[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.TabItem[] elementArray = new Elements.TabItem[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TabItem,TabItem>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(TabItem item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<TabItem> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.TabItem item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TabItem,TabItem>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TabItem,TabItem>(_parent,_elementCollection[index]);
            }
            set
            {
                TabItem item = value as TabItem;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<TabItem> Items
        {
            get
            {
                System.Collections.Generic.List<TabItem> items = new System.Collections.Generic.List<TabItem>();
                foreach (System.Web.VisualTree.Elements.TabItem item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TabItem,TabItem>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            TabItem controlItem = item as TabItem;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            TabItem controlItem = item as TabItem;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            TabItem item = value as TabItem;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            TabItem item = value as TabItem;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            TabItem controlItem = item as TabItem;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// TabItemCollection collection 
    /// </summary>
    public class TabItemCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.TabItemCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public TabItemCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(TabItem);
        }
	}

    /// <summary>
    /// ColumnHeaderCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.ColumnHeaderCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class ColumnHeaderCollection : System.Collections.Generic.ICollection<ColumnHeader>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.ColumnHeaderCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public ColumnHeaderCollection(WebControl parent, System.Web.VisualTree.Elements.ColumnHeaderCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ColumnHeader item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ColumnHeader item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ColumnHeader[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ColumnHeader[] elementArray = new Elements.ColumnHeader[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnHeader,ColumnHeader>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ColumnHeader item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ColumnHeader> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ColumnHeader item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnHeader,ColumnHeader>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnHeader,ColumnHeader>(_parent,_elementCollection[index]);
            }
            set
            {
                ColumnHeader item = value as ColumnHeader;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ColumnHeader> Items
        {
            get
            {
                System.Collections.Generic.List<ColumnHeader> items = new System.Collections.Generic.List<ColumnHeader>();
                foreach (System.Web.VisualTree.Elements.ColumnHeader item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnHeader,ColumnHeader>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ColumnHeader controlItem = item as ColumnHeader;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ColumnHeader controlItem = item as ColumnHeader;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ColumnHeader item = value as ColumnHeader;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ColumnHeader item = value as ColumnHeader;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ColumnHeader controlItem = item as ColumnHeader;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// ColumnHeaderCollection collection 
    /// </summary>
    public class ColumnHeaderCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.ColumnHeaderCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public ColumnHeaderCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ColumnHeader);
        }
	}

    /// <summary>
    /// ListViewItemCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.ListViewItemCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class ListViewItemCollection : System.Collections.Generic.ICollection<ListViewItem>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.ListViewItemCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public ListViewItemCollection(WebControl parent, System.Web.VisualTree.Elements.ListViewItemCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ListViewItem item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ListViewItem item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ListViewItem[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ListViewItem[] elementArray = new Elements.ListViewItem[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewItem,ListViewItem>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ListViewItem item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ListViewItem> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ListViewItem item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewItem,ListViewItem>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewItem,ListViewItem>(_parent,_elementCollection[index]);
            }
            set
            {
                ListViewItem item = value as ListViewItem;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ListViewItem> Items
        {
            get
            {
                System.Collections.Generic.List<ListViewItem> items = new System.Collections.Generic.List<ListViewItem>();
                foreach (System.Web.VisualTree.Elements.ListViewItem item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewItem,ListViewItem>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ListViewItem controlItem = item as ListViewItem;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ListViewItem controlItem = item as ListViewItem;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ListViewItem item = value as ListViewItem;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ListViewItem item = value as ListViewItem;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ListViewItem controlItem = item as ListViewItem;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// ListViewItemCollection collection 
    /// </summary>
    public class ListViewItemCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.ListViewItemCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public ListViewItemCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ListViewItem);
        }
	}

    /// <summary>
    /// TreeItemCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.TreeItemCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class TreeItemCollection : System.Collections.Generic.ICollection<TreeItem>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.TreeItemCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public TreeItemCollection(WebControl parent, System.Web.VisualTree.Elements.TreeItemCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(TreeItem item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(TreeItem item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(TreeItem[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.TreeItem[] elementArray = new Elements.TreeItem[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TreeItem,TreeItem>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(TreeItem item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<TreeItem> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.TreeItem item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TreeItem,TreeItem>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TreeItem,TreeItem>(_parent,_elementCollection[index]);
            }
            set
            {
                TreeItem item = value as TreeItem;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<TreeItem> Items
        {
            get
            {
                System.Collections.Generic.List<TreeItem> items = new System.Collections.Generic.List<TreeItem>();
                foreach (System.Web.VisualTree.Elements.TreeItem item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.TreeItem,TreeItem>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            TreeItem controlItem = item as TreeItem;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            TreeItem controlItem = item as TreeItem;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            TreeItem item = value as TreeItem;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            TreeItem item = value as TreeItem;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            TreeItem controlItem = item as TreeItem;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// TreeItemCollection collection 
    /// </summary>
    public class TreeItemCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.TreeItemCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public TreeItemCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(TreeItem);
        }
	}

    /// <summary>
    /// CustomLabelsCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.CustomLabelsCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class CustomLabelsCollection : System.Collections.Generic.ICollection<CustomLabel>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.CustomLabelsCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public CustomLabelsCollection(WebControl parent, System.Web.VisualTree.Elements.CustomLabelsCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(CustomLabel item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(CustomLabel item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(CustomLabel[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.CustomLabel[] elementArray = new Elements.CustomLabel[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.CustomLabel,CustomLabel>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(CustomLabel item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<CustomLabel> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.CustomLabel item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.CustomLabel,CustomLabel>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.CustomLabel,CustomLabel>(_parent,_elementCollection[index]);
            }
            set
            {
                CustomLabel item = value as CustomLabel;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<CustomLabel> Items
        {
            get
            {
                System.Collections.Generic.List<CustomLabel> items = new System.Collections.Generic.List<CustomLabel>();
                foreach (System.Web.VisualTree.Elements.CustomLabel item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.CustomLabel,CustomLabel>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            CustomLabel controlItem = item as CustomLabel;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            CustomLabel controlItem = item as CustomLabel;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            CustomLabel item = value as CustomLabel;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            CustomLabel item = value as CustomLabel;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            CustomLabel controlItem = item as CustomLabel;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// CustomLabelsCollection collection 
    /// </summary>
    public class CustomLabelsCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.CustomLabelsCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public CustomLabelsCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(CustomLabel);
        }
	}

    /// <summary>
    /// AnnotationCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.AnnotationCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class AnnotationCollection : System.Collections.Generic.ICollection<ChartAnnotation>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.AnnotationCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public AnnotationCollection(WebControl parent, System.Web.VisualTree.Elements.AnnotationCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ChartAnnotation item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ChartAnnotation item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ChartAnnotation[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ChartAnnotation[] elementArray = new Elements.ChartAnnotation[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartAnnotation,ChartAnnotation>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ChartAnnotation item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ChartAnnotation> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ChartAnnotation item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartAnnotation,ChartAnnotation>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartAnnotation,ChartAnnotation>(_parent,_elementCollection[index]);
            }
            set
            {
                ChartAnnotation item = value as ChartAnnotation;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ChartAnnotation> Items
        {
            get
            {
                System.Collections.Generic.List<ChartAnnotation> items = new System.Collections.Generic.List<ChartAnnotation>();
                foreach (System.Web.VisualTree.Elements.ChartAnnotation item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartAnnotation,ChartAnnotation>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ChartAnnotation controlItem = item as ChartAnnotation;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ChartAnnotation controlItem = item as ChartAnnotation;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ChartAnnotation item = value as ChartAnnotation;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ChartAnnotation item = value as ChartAnnotation;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ChartAnnotation controlItem = item as ChartAnnotation;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// AnnotationCollection collection 
    /// </summary>
    public class AnnotationCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.AnnotationCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public AnnotationCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ChartAnnotation);
        }
	}

    /// <summary>
    /// ChartAreaCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.ChartAreaCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class ChartAreaCollection : System.Collections.Generic.ICollection<ChartArea>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.ChartAreaCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public ChartAreaCollection(WebControl parent, System.Web.VisualTree.Elements.ChartAreaCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ChartArea item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ChartArea item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ChartArea[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ChartArea[] elementArray = new Elements.ChartArea[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartArea,ChartArea>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ChartArea item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ChartArea> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ChartArea item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartArea,ChartArea>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartArea,ChartArea>(_parent,_elementCollection[index]);
            }
            set
            {
                ChartArea item = value as ChartArea;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ChartArea> Items
        {
            get
            {
                System.Collections.Generic.List<ChartArea> items = new System.Collections.Generic.List<ChartArea>();
                foreach (System.Web.VisualTree.Elements.ChartArea item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartArea,ChartArea>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ChartArea controlItem = item as ChartArea;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ChartArea controlItem = item as ChartArea;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ChartArea item = value as ChartArea;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ChartArea item = value as ChartArea;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ChartArea controlItem = item as ChartArea;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// ChartAreaCollection collection 
    /// </summary>
    public class ChartAreaCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.ChartAreaCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public ChartAreaCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ChartArea);
        }
	}

    /// <summary>
    /// SeriesCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.SeriesCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class SeriesCollection : System.Collections.Generic.ICollection<Series>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.SeriesCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public SeriesCollection(WebControl parent, System.Web.VisualTree.Elements.SeriesCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(Series item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(Series item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Series[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.Series[] elementArray = new Elements.Series[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.Series,Series>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(Series item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<Series> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.Series item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.Series,Series>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.Series,Series>(_parent,_elementCollection[index]);
            }
            set
            {
                Series item = value as Series;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<Series> Items
        {
            get
            {
                System.Collections.Generic.List<Series> items = new System.Collections.Generic.List<Series>();
                foreach (System.Web.VisualTree.Elements.Series item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.Series,Series>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            Series controlItem = item as Series;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            Series controlItem = item as Series;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            Series item = value as Series;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            Series item = value as Series;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            Series controlItem = item as Series;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// SeriesCollection collection 
    /// </summary>
    public class SeriesCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.SeriesCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public SeriesCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(Series);
        }
	}

    /// <summary>
    /// TitleCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.TitleCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class TitleCollection : System.Collections.Generic.ICollection<ChartTitle>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.TitleCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public TitleCollection(WebControl parent, System.Web.VisualTree.Elements.TitleCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ChartTitle item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ChartTitle item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ChartTitle[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ChartTitle[] elementArray = new Elements.ChartTitle[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartTitle,ChartTitle>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ChartTitle item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ChartTitle> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ChartTitle item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartTitle,ChartTitle>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartTitle,ChartTitle>(_parent,_elementCollection[index]);
            }
            set
            {
                ChartTitle item = value as ChartTitle;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ChartTitle> Items
        {
            get
            {
                System.Collections.Generic.List<ChartTitle> items = new System.Collections.Generic.List<ChartTitle>();
                foreach (System.Web.VisualTree.Elements.ChartTitle item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ChartTitle,ChartTitle>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ChartTitle controlItem = item as ChartTitle;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ChartTitle controlItem = item as ChartTitle;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ChartTitle item = value as ChartTitle;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ChartTitle item = value as ChartTitle;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ChartTitle controlItem = item as ChartTitle;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// TitleCollection collection 
    /// </summary>
    public class TitleCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.TitleCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public TitleCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ChartTitle);
        }
	}

    /// <summary>
    /// ImageCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.ImageCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class ImageCollection : System.Collections.Generic.ICollection<ResourceReference>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.ImageCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public ImageCollection(WebControl parent, System.Web.VisualTree.Elements.ImageCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ResourceReference item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ResourceReference item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ResourceReference[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ResourceReference[] elementArray = new Elements.ResourceReference[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ResourceReference,ResourceReference>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ResourceReference item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ResourceReference> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ResourceReference item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ResourceReference,ResourceReference>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ResourceReference,ResourceReference>(_parent,_elementCollection[index]);
            }
            set
            {
                ResourceReference item = value as ResourceReference;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ResourceReference> Items
        {
            get
            {
                System.Collections.Generic.List<ResourceReference> items = new System.Collections.Generic.List<ResourceReference>();
                foreach (System.Web.VisualTree.Elements.ResourceReference item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ResourceReference,ResourceReference>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ResourceReference controlItem = item as ResourceReference;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ResourceReference controlItem = item as ResourceReference;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ResourceReference item = value as ResourceReference;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ResourceReference item = value as ResourceReference;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ResourceReference controlItem = item as ResourceReference;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// ImageCollection collection 
    /// </summary>
    public class ImageCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.ImageCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public ImageCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ResourceReference);
        }
	}

    /// <summary>
    /// KeyCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.KeyCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class KeyCollection : System.Collections.Generic.ICollection<KeyItem>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.KeyCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public KeyCollection(WebControl parent, System.Web.VisualTree.Elements.KeyCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(KeyItem item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(KeyItem item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(KeyItem[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.KeyItem[] elementArray = new Elements.KeyItem[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.KeyItem,KeyItem>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(KeyItem item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<KeyItem> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.KeyItem item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.KeyItem,KeyItem>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.KeyItem,KeyItem>(_parent,_elementCollection[index]);
            }
            set
            {
                KeyItem item = value as KeyItem;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<KeyItem> Items
        {
            get
            {
                System.Collections.Generic.List<KeyItem> items = new System.Collections.Generic.List<KeyItem>();
                foreach (System.Web.VisualTree.Elements.KeyItem item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.KeyItem,KeyItem>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            KeyItem controlItem = item as KeyItem;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            KeyItem controlItem = item as KeyItem;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            KeyItem item = value as KeyItem;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            KeyItem item = value as KeyItem;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            KeyItem controlItem = item as KeyItem;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// KeyCollection collection 
    /// </summary>
    public class KeyCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.KeyCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public KeyCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(KeyItem);
        }
	}

    /// <summary>
    /// ListViewSubitemCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.ListViewSubitemCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class ListViewSubitemCollection : System.Collections.Generic.ICollection<ListViewSubitem>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.ListViewSubitemCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public ListViewSubitemCollection(WebControl parent, System.Web.VisualTree.Elements.ListViewSubitemCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ListViewSubitem item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ListViewSubitem item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ListViewSubitem[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ListViewSubitem[] elementArray = new Elements.ListViewSubitem[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewSubitem,ListViewSubitem>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ListViewSubitem item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ListViewSubitem> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ListViewSubitem item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewSubitem,ListViewSubitem>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewSubitem,ListViewSubitem>(_parent,_elementCollection[index]);
            }
            set
            {
                ListViewSubitem item = value as ListViewSubitem;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ListViewSubitem> Items
        {
            get
            {
                System.Collections.Generic.List<ListViewSubitem> items = new System.Collections.Generic.List<ListViewSubitem>();
                foreach (System.Web.VisualTree.Elements.ListViewSubitem item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ListViewSubitem,ListViewSubitem>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ListViewSubitem controlItem = item as ListViewSubitem;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ListViewSubitem controlItem = item as ListViewSubitem;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ListViewSubitem item = value as ListViewSubitem;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ListViewSubitem item = value as ListViewSubitem;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ListViewSubitem controlItem = item as ListViewSubitem;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// ListViewSubitemCollection collection 
    /// </summary>
    public class ListViewSubitemCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.ListViewSubitemCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public ListViewSubitemCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ListViewSubitem);
        }
	}

    /// <summary>
    /// TableLayoutColumnStyleCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.TableLayoutColumnStyleCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class TableLayoutColumnStyleCollection : System.Collections.Generic.ICollection<ColumnStyle>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.TableLayoutColumnStyleCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public TableLayoutColumnStyleCollection(WebControl parent, System.Web.VisualTree.Elements.TableLayoutColumnStyleCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(ColumnStyle item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(ColumnStyle item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(ColumnStyle[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.ColumnStyle[] elementArray = new Elements.ColumnStyle[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnStyle,ColumnStyle>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(ColumnStyle item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<ColumnStyle> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.ColumnStyle item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnStyle,ColumnStyle>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnStyle,ColumnStyle>(_parent,_elementCollection[index]);
            }
            set
            {
                ColumnStyle item = value as ColumnStyle;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<ColumnStyle> Items
        {
            get
            {
                System.Collections.Generic.List<ColumnStyle> items = new System.Collections.Generic.List<ColumnStyle>();
                foreach (System.Web.VisualTree.Elements.ColumnStyle item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.ColumnStyle,ColumnStyle>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            ColumnStyle controlItem = item as ColumnStyle;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            ColumnStyle controlItem = item as ColumnStyle;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            ColumnStyle item = value as ColumnStyle;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            ColumnStyle item = value as ColumnStyle;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            ColumnStyle controlItem = item as ColumnStyle;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// TableLayoutColumnStyleCollection collection 
    /// </summary>
    public class TableLayoutColumnStyleCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.TableLayoutColumnStyleCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public TableLayoutColumnStyleCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(ColumnStyle);
        }
	}

    /// <summary>
    /// TableLayoutRowStyleCollection collection 
    /// </summary>
    [Editor("System.Web.VisualTree.WebControls.TableLayoutRowStyleCollectionEditor, System.Web.VisualTree.WebControls", typeof(UITypeEditor))]
    public class TableLayoutRowStyleCollection : System.Collections.Generic.ICollection<RowStyle>, System.Collections.IList
    {
        /// <summary>
        /// The underlined _element collection
        /// </summary>
        private System.Web.VisualTree.Elements.TableLayoutRowStyleCollection _elementCollection;

        /// <summary>
        /// The collection parent
        /// </summary>
        private WebControl _parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollectionCollection" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="elementCollection">The element collection.</param>
        public TableLayoutRowStyleCollection(WebControl parent, System.Web.VisualTree.Elements.TableLayoutRowStyleCollection elementCollection)
        {
			_parent = parent;
            _elementCollection = elementCollection;
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(RowStyle item)
        {
			_parent.Controls.Add(item);
            item.InitializeElement();
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            _elementCollection.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(RowStyle item)
        {
            return _elementCollection.Contains(item.Element);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(RowStyle[] array, int arrayIndex)
        {
            // Allocate temporary element array
            System.Web.VisualTree.Elements.RowStyle[] elementArray = new Elements.RowStyle[array.Length - arrayIndex];

            // Copy underlined elements
            _elementCollection.CopyTo(elementArray, 0);
            for (int i = 0; i < elementArray.Length; i++)
            {
                // Wrap elements with controls and copy to target
                array[i + arrayIndex] = ControlUtils.ResolveControl<System.Web.VisualTree.Elements.RowStyle,RowStyle>(_parent,elementArray[i]);
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get
            {
                return _elementCollection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(RowStyle item)
        {
            return _elementCollection.Remove(item.Element);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Collections.Generic.IEnumerator<RowStyle> GetEnumerator()
        {
            foreach (System.Web.VisualTree.Elements.RowStyle item in _elementCollection)
            {
				yield return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.RowStyle,RowStyle>(_parent,item);
			}
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public void RemoveAt(int index)
        {
            _elementCollection.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                return ControlUtils.ResolveControl<System.Web.VisualTree.Elements.RowStyle,RowStyle>(_parent,_elementCollection[index]);
            }
            set
            {
                RowStyle item = value as RowStyle;
                if (item != null)
                {
                    _elementCollection[index] = item.Element;
                }
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CopyTo(Array array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        #region IList implementation
        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        protected System.Collections.Generic.IList<RowStyle> Items
        {
            get
            {
                System.Collections.Generic.List<RowStyle> items = new System.Collections.Generic.List<RowStyle>();
                foreach (System.Web.VisualTree.Elements.RowStyle item in _elementCollection)
                {
                    items.Add(ControlUtils.ResolveControl<System.Web.VisualTree.Elements.RowStyle,RowStyle>(_parent,item));
                }

                return items;
            }
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int System.Collections.IList.Add(object item)
        {
            RowStyle controlItem = item as RowStyle;
            if (controlItem != null)
            {
                this.Add(controlItem);
                return _elementCollection.IndexOf(controlItem.Element);
            }

            return -1;
        }
        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool System.Collections.IList.Contains(object item)
        {
            RowStyle controlItem = item as RowStyle;
            if (controlItem != null)
            {
                return this.Contains(controlItem);
            }

            return false;
        }
        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int System.Collections.IList.IndexOf(object value)
        {
            RowStyle item = value as RowStyle;
            if (item != null)
            {
                return _elementCollection.IndexOf(item.Element);
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
        void System.Collections.IList.Insert(int index, object value)
        {
            RowStyle item = value as RowStyle;
            if (item != null)
            {
                _elementCollection.Insert(index, item.Element);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
        /// </summary>
        bool System.Collections.IList.IsFixedSize
        {
            get { return ((System.Collections.IList)_elementCollection).IsFixedSize; }
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void System.Collections.IList.Remove(object item)
        {
            RowStyle controlItem = item as RowStyle;
            if (controlItem != null)
            {
                _elementCollection.Remove(controlItem.Element);
            }
        }
        #endregion

        #region ICollection implementation
        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        bool System.Collections.ICollection.IsSynchronized
        {
            get { return ((System.Collections.ICollection)_elementCollection).IsSynchronized; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get { return ((System.Collections.ICollection)_elementCollection).SyncRoot; }
        }
        #endregion
    }

	/// <summary>
    /// TableLayoutRowStyleCollection collection 
    /// </summary>
    public class TableLayoutRowStyleCollectionEditor : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Web.UI.Design.WebControls.TableLayoutRowStyleCollectionEditor" /> class.
        /// </summary>
        /// <param name="type">The <see cref="T:System.Type" /> of the collection to edit.</param>
        public TableLayoutRowStyleCollectionEditor(Type type)
            : base(type)
        {
        }

		/// <summary>
        /// Gets a value indicating whether multiple <see cref="T:System.Web.UI.WebControls.ListItem" /> elements can be selected at one time.
        /// </summary>
        /// <returns>
        /// Always false.
        /// </returns>
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        /// <summary>
        /// Creates the type of the collection item.
        /// </summary>
        /// <returns></returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(RowStyle);
        }
	}

	#endregion 

	#region Controls
	/// <summary>
    /// ChartItem
    /// </summary>
	public abstract class ChartItem  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartItem"/> class.
        /// </summary>
		public ChartItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ChartItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ChartItemElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ChartItemElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// SeriesLabelStyle
    /// </summary>
	public class SeriesLabelStyle  : ChartItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SeriesLabelStyle"/> class.
        /// </summary>
		public SeriesLabelStyle()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="SeriesLabelStyle"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal SeriesLabelStyle(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.SeriesLabelStyle Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.SeriesLabelStyle;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.SeriesLabelStyle();
        }
 
		/// <summary>
		/// Gets or sets the Enabled 
		/// </summary>
		/// <value>
		/// The Enabled
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Enabled {
			get { return this.Element.Enabled; }
			set { this.Element.Enabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the Position 
		/// </summary>
		/// <value>
		/// The Position
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.SeriesLabelPosition))]
		public System.Web.VisualTree.Elements.SeriesLabelPosition Position {
			get { return this.Element.Position; }
			set { this.Element.Position = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Control
    /// </summary>
	public abstract partial class Control  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
		public Control()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Control(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ControlElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ControlElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
        /// The state of the MenuID property.
        /// </summary>
		private string _MenuID = null;

		/// <summary>
		/// Gets or sets the Menu Reference
		/// </summary>
		/// <value>
		/// The Menu
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string MenuID {
           get
            {
                if (this.Element.Menu != null)
                {
                    return this.Element.Menu.ID;
                }
                return _MenuID;
            }
            set
            {
				// Set the menu id state
				_MenuID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeMenu(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeMenu;
				}
            }
		}

		/// <summary>
        /// Initialize the Menu property.
        /// </summary>
		private void InitializeMenu(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_MenuID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.Menu = component.Element as System.Web.VisualTree.Elements.MenuElement;
				}
			}
		}

		/// <summary>
		/// Gets or sets the Menu 
		/// </summary>
		/// <value>
		/// The Menu
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.MenuElement Menu {
			get { return this.Element.Menu; }
			set { this.Element.Menu = value; }
		}
		
		/// <summary>
		/// Gets or sets the HistoryEnabled 
		/// </summary>
		/// <value>
		/// The HistoryEnabled
		/// </value>
		[DefaultValue(false)]
		public System.Boolean HistoryEnabled {
			get { return this.Element.HistoryEnabled; }
			set { this.Element.HistoryEnabled = value; }
		}
		
		/// <summary>
		/// Gets the IsFocused 
		/// </summary>
		/// <value>
		/// The IsFocused
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsFocused {
			get { return this.Element.IsFocused; }
		}
		
		/// <summary>
		/// Gets or sets the WaitMaskDisabled 
		/// </summary>
		/// <value>
		/// The WaitMaskDisabled
		/// </value>
		[DefaultValue(false)]
		public System.Boolean WaitMaskDisabled {
			get { return this.Element.WaitMaskDisabled; }
			set { this.Element.WaitMaskDisabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the Container 
		/// </summary>
		/// <value>
		/// The Container
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.IControlElement Container {
			get { return this.Element.Container; }
			set { this.Element.Container = value; }
		}
		
		/// <summary>
		/// Gets or sets the ToolTipText 
		/// </summary>
		/// <value>
		/// The ToolTipText
		/// </value>
		[DefaultValue("")]
		public System.String ToolTipText {
			get { return this.Element.ToolTipText; }
			set { this.Element.ToolTipText = value; }
		}
		
		/// <summary>
		/// Gets or sets the CssClass 
		/// </summary>
		/// <value>
		/// The CssClass
		/// </value>
		[DefaultValue(null)]
		public System.String CssClass {
			get { return this.Element.CssClass; }
			set { this.Element.CssClass = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaximumSize 
		/// </summary>
		/// <value>
		/// The MaximumSize
		/// </value>
		public System.Drawing.Size MaximumSize {
			get { return this.Element.MaximumSize; }
			set { this.Element.MaximumSize = value; }
		}
		
		internal bool ShouldSerializeMaximumSize() { return this.MaximumSize!=System.Drawing.Size.Empty;}

		/// <summary>
		/// Gets or sets the MinimumSize 
		/// </summary>
		/// <value>
		/// The MinimumSize
		/// </value>
		public System.Drawing.Size MinimumSize {
			get { return this.Element.MinimumSize; }
			set { this.Element.MinimumSize = value; }
		}
		
		internal bool ShouldSerializeMinimumSize() { return this.MinimumSize!=System.Drawing.Size.Empty;}

		/// <summary>
		/// Gets or sets the ZIndex 
		/// </summary>
		/// <value>
		/// The ZIndex
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 ZIndex {
			get { return this.Element.ZIndex; }
			set { this.Element.ZIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the TabStop 
		/// </summary>
		/// <value>
		/// The TabStop
		/// </value>
		[DefaultValue(true)]
		public System.Boolean TabStop {
			get { return this.Element.TabStop; }
			set { this.Element.TabStop = value; }
		}
		
		/// <summary>
		/// Gets or sets the Draggable 
		/// </summary>
		/// <value>
		/// The Draggable
		/// </value>
		[DefaultValue(false)]
		public System.Boolean Draggable {
			get { return this.Element.Draggable; }
			set { this.Element.Draggable = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoSize 
		/// </summary>
		/// <value>
		/// The AutoSize
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AutoSize {
			get { return this.Element.AutoSize; }
			set { this.Element.AutoSize = value; }
		}
		
		/// <summary>
		/// Gets or sets the RightToLeft 
		/// </summary>
		/// <value>
		/// The RightToLeft
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.RightToLeft), "Inherit")]
		public System.Web.VisualTree.RightToLeft RightToLeft {
			get { return this.Element.RightToLeft; }
			set { this.Element.RightToLeft = value; }
		}
		
		/// <summary>
		/// Gets or sets the RightToLeftDecoration 
		/// </summary>
		/// <value>
		/// The RightToLeftDecoration
		/// </value>
		[DefaultValue(false)]
		public System.Boolean RightToLeftDecoration {
			get { return this.Element.RightToLeftDecoration; }
			set { this.Element.RightToLeftDecoration = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowDrop 
		/// </summary>
		/// <value>
		/// The AllowDrop
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AllowDrop {
			get { return this.Element.AllowDrop; }
			set { this.Element.AllowDrop = value; }
		}
		
		/// <summary>
		/// Gets the MousePosition 
		/// </summary>
		/// <value>
		/// The MousePosition
		/// </value>
		public System.Drawing.Point MousePosition {
			get { return this.Element.MousePosition; }
		}
		
		internal bool ShouldSerializeMousePosition() { return this.MousePosition!=System.Drawing.Point.Empty;}

		/// <summary>
		/// Gets or sets the AccessibleRole 
		/// </summary>
		/// <value>
		/// The AccessibleRole
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.AccessibleRole), "Default")]
		public System.Web.VisualTree.AccessibleRole AccessibleRole {
			get { return this.Element.AccessibleRole; }
			set { this.Element.AccessibleRole = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImeMode 
		/// </summary>
		/// <value>
		/// The ImeMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ImeMode), "Inherit")]
		public System.Web.VisualTree.ImeMode ImeMode {
			get { return this.Element.ImeMode; }
			set { this.Element.ImeMode = value; }
		}
		
			
		public string BackgroundImage {
			get { return this.GetStringFromResource( this.Element.BackgroundImage); }
			set { this.Element.BackgroundImage = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the BackgroundImageLayout 
		/// </summary>
		/// <value>
		/// The BackgroundImageLayout
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ImageLayout), "Tile")]
		public System.Web.VisualTree.ImageLayout BackgroundImageLayout {
			get { return this.Element.BackgroundImageLayout; }
			set { this.Element.BackgroundImageLayout = value; }
		}
		
		/// <summary>
		/// Gets the Handle 
		/// </summary>
		/// <value>
		/// The Handle
		/// </value>
		public System.IntPtr Handle {
			get { return this.Element.Handle; }
		}
		
		/// <summary>
		/// Gets the ClientRectangle 
		/// </summary>
		/// <value>
		/// The ClientRectangle
		/// </value>
		public System.Drawing.Rectangle ClientRectangle {
			get { return this.Element.ClientRectangle; }
		}
		
		internal bool ShouldSerializeClientRectangle() { return this.ClientRectangle!=System.Drawing.Rectangle.Empty;}

		/// <summary>
		/// Gets or sets the AllowColumnReorder 
		/// </summary>
		/// <value>
		/// The AllowColumnReorder
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AllowColumnReorder {
			get { return this.Element.AllowColumnReorder; }
			set { this.Element.AllowColumnReorder = value; }
		}
		
		/// <summary>
		/// Gets or sets the CausesValidation 
		/// </summary>
		/// <value>
		/// The CausesValidation
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean CausesValidation {
			get { return this.Element.CausesValidation; }
			set { this.Element.CausesValidation = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScaleHeight 
		/// </summary>
		/// <value>
		/// The ScaleHeight
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single ScaleHeight {
			get { return this.Element.ScaleHeight; }
			set { this.Element.ScaleHeight = value; }
		}
		
		/// <summary>
		/// Gets the ScaleWidth 
		/// </summary>
		/// <value>
		/// The ScaleWidth
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single ScaleWidth {
			get { return this.Element.ScaleWidth; }
		}
		
		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
		/// <summary>
		/// Gets or sets the Appearance 
		/// </summary>
		/// <value>
		/// The Appearance
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.AppearanceConstants), "cc3D")]
		public System.Web.VisualTree.AppearanceConstants Appearance {
			get { return this.Element.Appearance; }
			set { this.Element.Appearance = value; }
		}
		
		/// <summary>
		/// Gets or sets the HelpContextID 
		/// </summary>
		/// <value>
		/// The HelpContextID
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 HelpContextID {
			get { return this.Element.HelpContextID; }
			set { this.Element.HelpContextID = value; }
		}
		
		/// <summary>
		/// Gets or sets the ControlLayout 
		/// </summary>
		/// <value>
		/// The ControlLayout
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ControlElementLayout ControlLayout {
			get { return this.Element.ControlLayout; }
			set { this.Element.ControlLayout = value; }
		}
		
		/// <summary>
		/// Gets or sets the Dock 
		/// </summary>
		/// <value>
		/// The Dock
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Dock), "None")]
		public System.Web.VisualTree.Dock Dock {
			get { return this.Element.Dock; }
			set { this.Element.Dock = value; }
		}
		
		/// <summary>
		/// Gets or sets the Anchor 
		/// </summary>
		/// <value>
		/// The Anchor
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.AnchorStyles), "5")]
		public System.Web.VisualTree.AnchorStyles Anchor {
			get { return this.Element.Anchor; }
			set { this.Element.Anchor = value; }
		}
		
		/// <summary>
		/// Gets or sets the Tag 
		/// </summary>
		/// <value>
		/// The Tag
		/// </value>
		[DefaultValue(null)]
		public System.String Tag {
			get { return this.Element.Tag as System.String; }
			set { this.Element.Tag = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelWidth 
		/// </summary>
		/// <value>
		/// The PixelWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelWidth {
			get { return this.Element.PixelWidth; }
			set { this.Element.PixelWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelLeft 
		/// </summary>
		/// <value>
		/// The PixelLeft
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelLeft {
			get { return this.Element.PixelLeft; }
			set { this.Element.PixelLeft = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelTop 
		/// </summary>
		/// <value>
		/// The PixelTop
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelTop {
			get { return this.Element.PixelTop; }
			set { this.Element.PixelTop = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelHeight 
		/// </summary>
		/// <value>
		/// The PixelHeight
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelHeight {
			get { return this.Element.PixelHeight; }
			set { this.Element.PixelHeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue("")]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the Top 
		/// </summary>
		/// <value>
		/// The Top
		/// </value>
		public System.Web.UI.WebControls.Unit Top {
			get { return this.Element.Top; }
			set { this.Element.Top = value; }
		}
		
		internal bool ShouldSerializeTop() { return this.Top!=System.Web.UI.WebControls.Unit.Empty;}

		/// <summary>
		/// Gets or sets the Left 
		/// </summary>
		/// <value>
		/// The Left
		/// </value>
		public System.Web.UI.WebControls.Unit Left {
			get { return this.Element.Left; }
			set { this.Element.Left = value; }
		}
		
		internal bool ShouldSerializeLeft() { return this.Left!=System.Web.UI.WebControls.Unit.Empty;}

		/// <summary>
		/// Gets or sets the Location 
		/// </summary>
		/// <value>
		/// The Location
		/// </value>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Drawing.Point Location {
			get { return this.Element.Location; }
			set { this.Element.Location = value; }
		}
		
		internal bool ShouldSerializeLocation() { return this.Location!=System.Drawing.Point.Empty;}

		/// <summary>
		/// Gets or sets the Margin 
		/// </summary>
		/// <value>
		/// The Margin
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Padding Margin {
			get { return this.Element.Margin; }
			set { this.Element.Margin = value; }
		}
		
		/// <summary>
		/// Gets or sets the Padding 
		/// </summary>
		/// <value>
		/// The Padding
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Padding Padding {
			get { return this.Element.Padding; }
			set { this.Element.Padding = value; }
		}
		
		/// <summary>
		/// Gets or sets the CustomUI 
		/// </summary>
		/// <value>
		/// The CustomUI
		/// </value>
		[DefaultValue(null)]
		public System.String CustomUI {
			get { return this.Element.CustomUI; }
			set { this.Element.CustomUI = value; }
		}
		
		/// <summary>
        /// The state of the ContextMenuStripID property.
        /// </summary>
		private string _ContextMenuStripID = null;

		/// <summary>
		/// Gets or sets the ContextMenuStrip Reference
		/// </summary>
		/// <value>
		/// The ContextMenuStrip
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string ContextMenuStripID {
           get
            {
                if (this.Element.ContextMenuStrip != null)
                {
                    return this.Element.ContextMenuStrip.ID;
                }
                return _ContextMenuStripID;
            }
            set
            {
				// Set the menu id state
				_ContextMenuStripID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeContextMenuStrip(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeContextMenuStrip;
				}
            }
		}

		/// <summary>
        /// Initialize the ContextMenuStrip property.
        /// </summary>
		private void InitializeContextMenuStrip(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_ContextMenuStripID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.ContextMenuStrip = component.Element as System.Web.VisualTree.Elements.ContextMenuStripElement;
				}
			}
		}

		/// <summary>
		/// Gets or sets the ContextMenuStrip 
		/// </summary>
		/// <value>
		/// The ContextMenuStrip
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ContextMenuStripElement ContextMenuStrip {
			get { return this.Element.ContextMenuStrip; }
			set { this.Element.ContextMenuStrip = value; }
		}
		
		/// <summary>
		/// Gets or sets the Resizable 
		/// </summary>
		/// <value>
		/// The Resizable
		/// </value>
		[DefaultValue(false)]
		public System.Boolean Resizable {
			get { return this.Element.Resizable; }
			set { this.Element.Resizable = value; }
		}
		
		/// <summary>
        /// The state of the ImageListID property.
        /// </summary>
		private string _ImageListID = null;

		/// <summary>
		/// Gets or sets the ImageList Reference
		/// </summary>
		/// <value>
		/// The ImageList
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string ImageListID {
           get
            {
                if (this.Element.ImageList != null)
                {
                    return this.Element.ImageList.ID;
                }
                return _ImageListID;
            }
            set
            {
				// Set the menu id state
				_ImageListID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeImageList(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeImageList;
				}
            }
		}

		/// <summary>
        /// Initialize the ImageList property.
        /// </summary>
		private void InitializeImageList(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_ImageListID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.ImageList = component.Element as System.Web.VisualTree.Elements.ImageList;
				}
			}
		}

		/// <summary>
		/// Gets or sets the ImageList 
		/// </summary>
		/// <value>
		/// The ImageList
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ImageList ImageList {
			get { return this.Element.ImageList; }
			set { this.Element.ImageList = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataMember 
		/// </summary>
		/// <value>
		/// The DataMember
		/// </value>
		[DefaultValue(null)]
		public System.String DataMember {
			get { return this.Element.DataMember; }
			set { this.Element.DataMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the ClientLeave 
		/// </summary>
		/// <value>
		/// The ClientLeave
		/// </value>
		[DefaultValue(null)]
		public System.String ClientLeave {
			get { return this.Element.ClientLeave; }
			set { this.Element.ClientLeave = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void RaiseUpdateClient() {
			this.Element.RaiseUpdateClient();
		}

		/// <summary>
		/// 
		/// </summary>
		public System.Threading.Tasks.Task<System.Web.VisualTree.Elements.ControlElement> GetActiveControl() {
			return this.Element.GetActiveControl();
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetActiveControl(System.String controlId) {
			this.Element.SetActiveControl(controlId);
		}

		/// <summary>
		/// 
		/// </summary>
		public void Focus() {
			this.Element.Focus();
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetLoading(System.Boolean state) {
			this.Element.SetLoading(state);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetLoading(System.String message) {
			this.Element.SetLoading(message);
		}

		/// <summary>
		/// 
		/// </summary>
		public void Refresh() {
			this.Element.Refresh();
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Load = Component.GetHandler<System.EventArgs>(this.Element, this.LoadAction);
			if (_Load != null) { this.Element.Load += _Load.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.LayoutEventArgs> _Layout = Component.GetHandler<System.Web.VisualTree.Elements.LayoutEventArgs>(this.Element, this.LayoutAction);
			if (_Layout != null) { this.Element.Layout += _Layout.Invoke; }
			IVisualElementAction<System.EventArgs> _DataSourceItemUpdated = Component.GetHandler<System.EventArgs>(this.Element, this.DataSourceItemUpdatedAction);
			if (_DataSourceItemUpdated != null) { this.Element.DataSourceItemUpdated += _DataSourceItemUpdated.Invoke; }
			IVisualElementAction<System.EventArgs> _DataSourceItemDeleted = Component.GetHandler<System.EventArgs>(this.Element, this.DataSourceItemDeletedAction);
			if (_DataSourceItemDeleted != null) { this.Element.DataSourceItemDeleted += _DataSourceItemDeleted.Invoke; }
			IVisualElementAction<System.EventArgs> _DataSourceItemCreated = Component.GetHandler<System.EventArgs>(this.Element, this.DataSourceItemCreatedAction);
			if (_DataSourceItemCreated != null) { this.Element.DataSourceItemCreated += _DataSourceItemCreated.Invoke; }
			IVisualElementAction<System.EventArgs> _DataSourceCurrentItemChanged = Component.GetHandler<System.EventArgs>(this.Element, this.DataSourceCurrentItemChangedAction);
			if (_DataSourceCurrentItemChanged != null) { this.Element.DataSourceCurrentItemChanged += _DataSourceCurrentItemChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.MouseEventArgs> _MouseTapHold = Component.GetHandler<System.Web.VisualTree.Elements.MouseEventArgs>(this.Element, this.MouseTapHoldAction);
			if (_MouseTapHold != null) { this.Element.MouseTapHold += _MouseTapHold.Invoke; }
			IVisualElementAction<System.EventArgs> _Click = Component.GetHandler<System.EventArgs>(this.Element, this.ClickAction);
			if (_Click != null) { this.Element.Click += _Click.Invoke; }
			IVisualElementAction<System.EventArgs> _VisibleChanged = Component.GetHandler<System.EventArgs>(this.Element, this.VisibleChangedAction);
			if (_VisibleChanged != null) { this.Element.VisibleChanged += _VisibleChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _MouseHover = Component.GetHandler<System.EventArgs>(this.Element, this.MouseHoverAction);
			if (_MouseHover != null) { this.Element.MouseHover += _MouseHover.Invoke; }
			IVisualElementAction<System.EventArgs> _MouseEnter = Component.GetHandler<System.EventArgs>(this.Element, this.MouseEnterAction);
			if (_MouseEnter != null) { this.Element.MouseEnter += _MouseEnter.Invoke; }
			IVisualElementAction<System.EventArgs> _MouseLeave = Component.GetHandler<System.EventArgs>(this.Element, this.MouseLeaveAction);
			if (_MouseLeave != null) { this.Element.MouseLeave += _MouseLeave.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ResizeEventArgs> _Resize = Component.GetHandler<System.Web.VisualTree.Elements.ResizeEventArgs>(this.Element, this.ResizeAction);
			if (_Resize != null) { this.Element.Resize += _Resize.Invoke; }
			IVisualElementAction<System.EventArgs> _Validating = Component.GetHandler<System.EventArgs>(this.Element, this.ValidatingAction);
			if (_Validating != null) { this.Element.Validating += _Validating.Invoke; }
			IVisualElementAction<System.EventArgs> _TextChanged = Component.GetHandler<System.EventArgs>(this.Element, this.TextChangedAction);
			if (_TextChanged != null) { this.Element.TextChanged += _TextChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _DragDrop = Component.GetHandler<System.EventArgs>(this.Element, this.DragDropAction);
			if (_DragDrop != null) { this.Element.DragDrop += _DragDrop.Invoke; }
			IVisualElementAction<System.EventArgs> _DragOver = Component.GetHandler<System.EventArgs>(this.Element, this.DragOverAction);
			if (_DragOver != null) { this.Element.DragOver += _DragOver.Invoke; }
			IVisualElementAction<System.EventArgs> _SizeChanged = Component.GetHandler<System.EventArgs>(this.Element, this.SizeChangedAction);
			if (_SizeChanged != null) { this.Element.SizeChanged += _SizeChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.MouseEventArgs> _MouseMove = Component.GetHandler<System.Web.VisualTree.Elements.MouseEventArgs>(this.Element, this.MouseMoveAction);
			if (_MouseMove != null) { this.Element.MouseMove += _MouseMove.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.MouseEventArgs> _MouseDown = Component.GetHandler<System.Web.VisualTree.Elements.MouseEventArgs>(this.Element, this.MouseDownAction);
			if (_MouseDown != null) { this.Element.MouseDown += _MouseDown.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.MouseEventArgs> _MouseUp = Component.GetHandler<System.Web.VisualTree.Elements.MouseEventArgs>(this.Element, this.MouseUpAction);
			if (_MouseUp != null) { this.Element.MouseUp += _MouseUp.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeItemMouseClickEventArgs> _MouseClick = Component.GetHandler<System.Web.VisualTree.Elements.TreeItemMouseClickEventArgs>(this.Element, this.MouseClickAction);
			if (_MouseClick != null) { this.Element.MouseClick += _MouseClick.Invoke; }
			IVisualElementAction<System.EventArgs> _LocationChanged = Component.GetHandler<System.EventArgs>(this.Element, this.LocationChangedAction);
			if (_LocationChanged != null) { this.Element.LocationChanged += _LocationChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _Validated = Component.GetHandler<System.EventArgs>(this.Element, this.ValidatedAction);
			if (_Validated != null) { this.Element.Validated += _Validated.Invoke; }
			IVisualElementAction<System.EventArgs> _Leave = Component.GetHandler<System.EventArgs>(this.Element, this.LeaveAction);
			if (_Leave != null) { this.Element.Leave += _Leave.Invoke; }
			IVisualElementAction<System.EventArgs> _LostFocus = Component.GetHandler<System.EventArgs>(this.Element, this.LostFocusAction);
			if (_LostFocus != null) { this.Element.LostFocus += _LostFocus.Invoke; }
			IVisualElementAction<System.EventArgs> _DoubleClick = Component.GetHandler<System.EventArgs>(this.Element, this.DoubleClickAction);
			if (_DoubleClick != null) { this.Element.DoubleClick += _DoubleClick.Invoke; }
			IVisualElementAction<System.EventArgs> _DragEnter = Component.GetHandler<System.EventArgs>(this.Element, this.DragEnterAction);
			if (_DragEnter != null) { this.Element.DragEnter += _DragEnter.Invoke; }
			IVisualElementAction<System.EventArgs> _Enter = Component.GetHandler<System.EventArgs>(this.Element, this.EnterAction);
			if (_Enter != null) { this.Element.Enter += _Enter.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ControlEventArgs> _ControlAdded = Component.GetHandler<System.Web.VisualTree.Elements.ControlEventArgs>(this.Element, this.ControlAddedAction);
			if (_ControlAdded != null) { this.Element.ControlAdded += _ControlAdded.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ControlEventArgs> _ControlRemoved = Component.GetHandler<System.Web.VisualTree.Elements.ControlEventArgs>(this.Element, this.ControlRemovedAction);
			if (_ControlRemoved != null) { this.Element.ControlRemoved += _ControlRemoved.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.KeyPressEventArgs> _KeyPress = Component.GetHandler<System.Web.VisualTree.Elements.KeyPressEventArgs>(this.Element, this.KeyPressAction);
			if (_KeyPress != null) { this.Element.KeyPress += _KeyPress.Invoke; }
			IVisualElementAction<System.EventArgs> _GotFocus = Component.GetHandler<System.EventArgs>(this.Element, this.GotFocusAction);
			if (_GotFocus != null) { this.Element.GotFocus += _GotFocus.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.KeyEventArgs> _KeyUp = Component.GetHandler<System.Web.VisualTree.Elements.KeyEventArgs>(this.Element, this.KeyUpAction);
			if (_KeyUp != null) { this.Element.KeyUp += _KeyUp.Invoke; }
			IVisualElementAction<System.Web.VisualTree.KeyDownEventArgs> _KeyDown = Component.GetHandler<System.Web.VisualTree.KeyDownEventArgs>(this.Element, this.KeyDownAction);
			if (_KeyDown != null) { this.Element.KeyDown += _KeyDown.Invoke; }
			IVisualElementAction<System.EventArgs> _Afterrender = Component.GetHandler<System.EventArgs>(this.Element, this.AfterrenderAction);
			if (_Afterrender != null) { this.Element.Afterrender += _Afterrender.Invoke; }
			IVisualElementAction<System.EventArgs> _WidgetControlEditChanged = Component.GetHandler<System.EventArgs>(this.Element, this.WidgetControlEditChangedAction);
			if (_WidgetControlEditChanged != null) { this.Element.WidgetControlEditChanged += _WidgetControlEditChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _WidgetControlItemChanged = Component.GetHandler<System.EventArgs>(this.Element, this.WidgetControlItemChangedAction);
			if (_WidgetControlItemChanged != null) { this.Element.WidgetControlItemChanged += _WidgetControlItemChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridCellEventArgs> _WidgetControlFocusEnter = Component.GetHandler<System.Web.VisualTree.Elements.GridCellEventArgs>(this.Element, this.WidgetControlFocusEnterAction);
			if (_WidgetControlFocusEnter != null) { this.Element.WidgetControlFocusEnter += _WidgetControlFocusEnter.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridCellEventArgs> _WidgetControlDoubleClick = Component.GetHandler<System.Web.VisualTree.Elements.GridCellEventArgs>(this.Element, this.WidgetControlDoubleClickAction);
			if (_WidgetControlDoubleClick != null) { this.Element.WidgetControlDoubleClick += _WidgetControlDoubleClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridCellEventArgs> _WidgetControlClick = Component.GetHandler<System.Web.VisualTree.Elements.GridCellEventArgs>(this.Element, this.WidgetControlClickAction);
			if (_WidgetControlClick != null) { this.Element.WidgetControlClick += _WidgetControlClick.Invoke; }
          
        }

		private string _LoadAction;

		public string LoadAction
        {
            get
			{
				return _LoadAction;
			}
			set
			{
				_LoadAction = value;
			}            
        }

		private string _LayoutAction;

		public string LayoutAction
        {
            get
			{
				return _LayoutAction;
			}
			set
			{
				_LayoutAction = value;
			}            
        }

		private string _DataSourceItemUpdatedAction;

		public string DataSourceItemUpdatedAction
        {
            get
			{
				return _DataSourceItemUpdatedAction;
			}
			set
			{
				_DataSourceItemUpdatedAction = value;
			}            
        }

		private string _DataSourceItemDeletedAction;

		public string DataSourceItemDeletedAction
        {
            get
			{
				return _DataSourceItemDeletedAction;
			}
			set
			{
				_DataSourceItemDeletedAction = value;
			}            
        }

		private string _DataSourceItemCreatedAction;

		public string DataSourceItemCreatedAction
        {
            get
			{
				return _DataSourceItemCreatedAction;
			}
			set
			{
				_DataSourceItemCreatedAction = value;
			}            
        }

		private string _DataSourceCurrentItemChangedAction;

		public string DataSourceCurrentItemChangedAction
        {
            get
			{
				return _DataSourceCurrentItemChangedAction;
			}
			set
			{
				_DataSourceCurrentItemChangedAction = value;
			}            
        }

		private string _MouseTapHoldAction;

		public string MouseTapHoldAction
        {
            get
			{
				return _MouseTapHoldAction;
			}
			set
			{
				_MouseTapHoldAction = value;
			}            
        }

		private string _ClickAction;

		public string ClickAction
        {
            get
			{
				return _ClickAction;
			}
			set
			{
				_ClickAction = value;
			}            
        }

		private string _VisibleChangedAction;

		public string VisibleChangedAction
        {
            get
			{
				return _VisibleChangedAction;
			}
			set
			{
				_VisibleChangedAction = value;
			}            
        }

		private string _MouseHoverAction;

		public string MouseHoverAction
        {
            get
			{
				return _MouseHoverAction;
			}
			set
			{
				_MouseHoverAction = value;
			}            
        }

		private string _MouseEnterAction;

		public string MouseEnterAction
        {
            get
			{
				return _MouseEnterAction;
			}
			set
			{
				_MouseEnterAction = value;
			}            
        }

		private string _MouseLeaveAction;

		public string MouseLeaveAction
        {
            get
			{
				return _MouseLeaveAction;
			}
			set
			{
				_MouseLeaveAction = value;
			}            
        }

		private string _ResizeAction;

		public string ResizeAction
        {
            get
			{
				return _ResizeAction;
			}
			set
			{
				_ResizeAction = value;
			}            
        }

		private string _ValidatingAction;

		public string ValidatingAction
        {
            get
			{
				return _ValidatingAction;
			}
			set
			{
				_ValidatingAction = value;
			}            
        }

		private string _TextChangedAction;

		public string TextChangedAction
        {
            get
			{
				return _TextChangedAction;
			}
			set
			{
				_TextChangedAction = value;
			}            
        }

		private string _DragDropAction;

		public string DragDropAction
        {
            get
			{
				return _DragDropAction;
			}
			set
			{
				_DragDropAction = value;
			}            
        }

		private string _DragOverAction;

		public string DragOverAction
        {
            get
			{
				return _DragOverAction;
			}
			set
			{
				_DragOverAction = value;
			}            
        }

		private string _SizeChangedAction;

		public string SizeChangedAction
        {
            get
			{
				return _SizeChangedAction;
			}
			set
			{
				_SizeChangedAction = value;
			}            
        }

		private string _MouseMoveAction;

		public string MouseMoveAction
        {
            get
			{
				return _MouseMoveAction;
			}
			set
			{
				_MouseMoveAction = value;
			}            
        }

		private string _MouseDownAction;

		public string MouseDownAction
        {
            get
			{
				return _MouseDownAction;
			}
			set
			{
				_MouseDownAction = value;
			}            
        }

		private string _MouseUpAction;

		public string MouseUpAction
        {
            get
			{
				return _MouseUpAction;
			}
			set
			{
				_MouseUpAction = value;
			}            
        }

		private string _MouseClickAction;

		public string MouseClickAction
        {
            get
			{
				return _MouseClickAction;
			}
			set
			{
				_MouseClickAction = value;
			}            
        }

		private string _LocationChangedAction;

		public string LocationChangedAction
        {
            get
			{
				return _LocationChangedAction;
			}
			set
			{
				_LocationChangedAction = value;
			}            
        }

		private string _ValidatedAction;

		public string ValidatedAction
        {
            get
			{
				return _ValidatedAction;
			}
			set
			{
				_ValidatedAction = value;
			}            
        }

		private string _LeaveAction;

		public string LeaveAction
        {
            get
			{
				return _LeaveAction;
			}
			set
			{
				_LeaveAction = value;
			}            
        }

		private string _LostFocusAction;

		public string LostFocusAction
        {
            get
			{
				return _LostFocusAction;
			}
			set
			{
				_LostFocusAction = value;
			}            
        }

		private string _DoubleClickAction;

		public string DoubleClickAction
        {
            get
			{
				return _DoubleClickAction;
			}
			set
			{
				_DoubleClickAction = value;
			}            
        }

		private string _DragEnterAction;

		public string DragEnterAction
        {
            get
			{
				return _DragEnterAction;
			}
			set
			{
				_DragEnterAction = value;
			}            
        }

		private string _EnterAction;

		public string EnterAction
        {
            get
			{
				return _EnterAction;
			}
			set
			{
				_EnterAction = value;
			}            
        }

		private string _ControlAddedAction;

		public string ControlAddedAction
        {
            get
			{
				return _ControlAddedAction;
			}
			set
			{
				_ControlAddedAction = value;
			}            
        }

		private string _ControlRemovedAction;

		public string ControlRemovedAction
        {
            get
			{
				return _ControlRemovedAction;
			}
			set
			{
				_ControlRemovedAction = value;
			}            
        }

		private string _KeyPressAction;

		public string KeyPressAction
        {
            get
			{
				return _KeyPressAction;
			}
			set
			{
				_KeyPressAction = value;
			}            
        }

		private string _GotFocusAction;

		public string GotFocusAction
        {
            get
			{
				return _GotFocusAction;
			}
			set
			{
				_GotFocusAction = value;
			}            
        }

		private string _KeyUpAction;

		public string KeyUpAction
        {
            get
			{
				return _KeyUpAction;
			}
			set
			{
				_KeyUpAction = value;
			}            
        }

		private string _KeyDownAction;

		public string KeyDownAction
        {
            get
			{
				return _KeyDownAction;
			}
			set
			{
				_KeyDownAction = value;
			}            
        }

		private string _AfterrenderAction;

		public string AfterrenderAction
        {
            get
			{
				return _AfterrenderAction;
			}
			set
			{
				_AfterrenderAction = value;
			}            
        }

		private string _WidgetControlEditChangedAction;

		public string WidgetControlEditChangedAction
        {
            get
			{
				return _WidgetControlEditChangedAction;
			}
			set
			{
				_WidgetControlEditChangedAction = value;
			}            
        }

		private string _WidgetControlItemChangedAction;

		public string WidgetControlItemChangedAction
        {
            get
			{
				return _WidgetControlItemChangedAction;
			}
			set
			{
				_WidgetControlItemChangedAction = value;
			}            
        }

		private string _WidgetControlFocusEnterAction;

		public string WidgetControlFocusEnterAction
        {
            get
			{
				return _WidgetControlFocusEnterAction;
			}
			set
			{
				_WidgetControlFocusEnterAction = value;
			}            
        }

		private string _WidgetControlDoubleClickAction;

		public string WidgetControlDoubleClickAction
        {
            get
			{
				return _WidgetControlDoubleClickAction;
			}
			set
			{
				_WidgetControlDoubleClickAction = value;
			}            
        }

		private string _WidgetControlClickAction;

		public string WidgetControlClickAction
        {
            get
			{
				return _WidgetControlClickAction;
			}
			set
			{
				_WidgetControlClickAction = value;
			}            
        }


	}  

	/// <summary>
    /// ComboboxColumn
    /// </summary>
	public class ComboboxColumn  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComboboxColumn"/> class.
        /// </summary>
		public ComboboxColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboboxColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ComboboxColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ComboboxColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ComboboxColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ComboboxColumn();
        }
 
		/// <summary>
		/// Gets or sets the HeaderText 
		/// </summary>
		/// <value>
		/// The HeaderText
		/// </value>
		[DefaultValue(null)]
		public System.String HeaderText {
			get { return this.Element.HeaderText; }
			set { this.Element.HeaderText = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataMember 
		/// </summary>
		/// <value>
		/// The DataMember
		/// </value>
		[DefaultValue(null)]
		public System.String DataMember {
			get { return this.Element.DataMember; }
			set { this.Element.DataMember = value; }
		}
		
		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
		/// <summary>
		/// Gets the DataTypeName 
		/// </summary>
		/// <value>
		/// The DataTypeName
		/// </value>
		[DefaultValue(null)]
		public System.String DataTypeName {
			get { return this.Element.DataTypeName; }
		}
		
		/// <summary>
		/// Gets or sets the SortType 
		/// </summary>
		/// <value>
		/// The SortType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ColumnSortMode))]
		public System.Web.VisualTree.ColumnSortMode SortType {
			get { return this.Element.SortType; }
			set { this.Element.SortType = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ContentControl
    /// </summary>
	public abstract class ContentControl  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentControl"/> class.
        /// </summary>
		public ContentControl()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentControl"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ContentControl(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ContentControlElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ContentControlElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the Content 
		/// </summary>
		/// <value>
		/// The Content
		/// </value>
		[DefaultValue(null)]
		public System.Object Content {
			get { return this.Element.Content; }
			set { this.Element.Content = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ScrollableControl
    /// </summary>
	public abstract class ScrollableControl  : ContentControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollableControl"/> class.
        /// </summary>
		public ScrollableControl()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollableControl"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ScrollableControl(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ScrollableControl Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ScrollableControl;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the AutoScroll 
		/// </summary>
		/// <value>
		/// The AutoScroll
		/// </value>
		[DefaultValue(false)]
		public System.Boolean AutoScroll {
			get { return this.Element.AutoScroll; }
			set { this.Element.AutoScroll = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Panel
    /// </summary>
	public class Panel  : ScrollableControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Panel"/> class.
        /// </summary>
		public Panel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Panel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Panel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.PanelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.PanelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.PanelElement();
        }
 
		/// <summary>
		/// Gets or sets the MinWidth 
		/// </summary>
		/// <value>
		/// The MinWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 MinWidth {
			get { return this.Element.MinWidth; }
			set { this.Element.MinWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the MinHeight 
		/// </summary>
		/// <value>
		/// The MinHeight
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 MinHeight {
			get { return this.Element.MinHeight; }
			set { this.Element.MinHeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoSizeMode 
		/// </summary>
		/// <value>
		/// The AutoSizeMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.AutoSizeMode), "GrowOnly")]
		public System.Web.VisualTree.AutoSizeMode AutoSizeMode {
			get { return this.Element.AutoSizeMode; }
			set { this.Element.AutoSizeMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the RoundedCorners 
		/// </summary>
		/// <value>
		/// The RoundedCorners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean RoundedCorners {
			get { return this.Element.RoundedCorners; }
			set { this.Element.RoundedCorners = value; }
		}
		
		/// <summary>
		/// Gets or sets the FloodShowPct 
		/// </summary>
		/// <value>
		/// The FloodShowPct
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean FloodShowPct {
			get { return this.Element.FloodShowPct; }
			set { this.Element.FloodShowPct = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowHeader 
		/// </summary>
		/// <value>
		/// The ShowHeader
		/// </value>
		[DefaultValue(false)]
		public System.Boolean ShowHeader {
			get { return this.Element.ShowHeader; }
			set { this.Element.ShowHeader = value; }
		}
		
		/// <summary>
		/// Gets or sets the WindowTargetID 
		/// </summary>
		/// <value>
		/// The WindowTargetID
		/// </value>
		[DefaultValue(null)]
		public System.String WindowTargetID {
			get { return this.Element.WindowTargetID; }
			set { this.Element.WindowTargetID = value; }
		}
		
		/// <summary>
		/// Gets or sets the Content 
		/// </summary>
		/// <value>
		/// The Content
		/// </value>
		[DefaultValue(null)]
		public System.String Content {
			get { return this.Element.Content as System.String; }
			set { this.Element.Content = value; }
		}
		
		/// <summary>
		/// Gets or sets the Collapsed 
		/// </summary>
		/// <value>
		/// The Collapsed
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Collapsed {
			get { return this.Element.Collapsed; }
			set { this.Element.Collapsed = value; }
		}
		
		/// <summary>
		/// Gets or sets the Collapsible 
		/// </summary>
		/// <value>
		/// The Collapsible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Collapsible {
			get { return this.Element.Collapsible; }
			set { this.Element.Collapsible = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _BeforeCollapse = Component.GetHandler<System.EventArgs>(this.Element, this.BeforeCollapseAction);
			if (_BeforeCollapse != null) { this.Element.BeforeCollapse += _BeforeCollapse.Invoke; }
			IVisualElementAction<System.EventArgs> _BeforeExpand = Component.GetHandler<System.EventArgs>(this.Element, this.BeforeExpandAction);
			if (_BeforeExpand != null) { this.Element.BeforeExpand += _BeforeExpand.Invoke; }
			IVisualElementAction<System.EventArgs> _AfterCollapse = Component.GetHandler<System.EventArgs>(this.Element, this.AfterCollapseAction);
			if (_AfterCollapse != null) { this.Element.AfterCollapse += _AfterCollapse.Invoke; }
			IVisualElementAction<System.EventArgs> _AfterExpand = Component.GetHandler<System.EventArgs>(this.Element, this.AfterExpandAction);
			if (_AfterExpand != null) { this.Element.AfterExpand += _AfterExpand.Invoke; }
        }

		private string _BeforeCollapseAction;

		public string BeforeCollapseAction
        {
            get
			{
				return _BeforeCollapseAction;
			}
			set
			{
				_BeforeCollapseAction = value;
			}            
        }

		private string _BeforeExpandAction;

		public string BeforeExpandAction
        {
            get
			{
				return _BeforeExpandAction;
			}
			set
			{
				_BeforeExpandAction = value;
			}            
        }

		private string _AfterCollapseAction;

		public string AfterCollapseAction
        {
            get
			{
				return _AfterCollapseAction;
			}
			set
			{
				_AfterCollapseAction = value;
			}            
        }

		private string _AfterExpandAction;

		public string AfterExpandAction
        {
            get
			{
				return _AfterExpandAction;
			}
			set
			{
				_AfterExpandAction = value;
			}            
        }


	}  

	/// <summary>
    /// CompositeBase
    /// </summary>
	public class CompositeBase  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompositeBase"/> class.
        /// </summary>
		public CompositeBase()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="CompositeBase"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal CompositeBase(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.CompositeElementBase Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.CompositeElementBase;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.CompositeElementBase();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// FileSystemBase
    /// </summary>
	public class FileSystemBase  : CompositeBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemBase"/> class.
        /// </summary>
		public FileSystemBase()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemBase"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal FileSystemBase(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Compatibility.FileSystemBase Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Compatibility.FileSystemBase;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Compatibility.FileSystemBase();
        }
 
		/// <summary>
		/// Gets or sets the FileSystemID 
		/// </summary>
		/// <value>
		/// The FileSystemID
		/// </value>
		[DefaultValue(null)]
		public System.String FileSystemID {
			get { return this.Element.FileSystemID; }
			set { this.Element.FileSystemID = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBar
    /// </summary>
	[ParseChildren(true, "Items")]
	public class ToolBar  : ScrollableControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBar"/> class.
        /// </summary>
		public ToolBar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarElement();
        }
 
		/// <summary>
		/// Gets or sets the AllowMerge 
		/// </summary>
		/// <value>
		/// The AllowMerge
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AllowMerge {
			get { return this.Element.AllowMerge; }
			set { this.Element.AllowMerge = value; }
		}
		
		/// <summary>
		/// Gets or sets the GripStyle 
		/// </summary>
		/// <value>
		/// The GripStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.ToolBarGripStyle), "Visible")]
		public System.Web.VisualTree.Elements.ToolBarGripStyle GripStyle {
			get { return this.Element.GripStyle; }
			set { this.Element.GripStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the LayoutStyle 
		/// </summary>
		/// <value>
		/// The LayoutStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.ToolBarLayoutStyle), "StackWithOverflow")]
		public System.Web.VisualTree.Elements.ToolBarLayoutStyle LayoutStyle {
			get { return this.Element.LayoutStyle; }
			set { this.Element.LayoutStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the RenderMode 
		/// </summary>
		/// <value>
		/// The RenderMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.ToolBarRenderMode), "ManagerRenderMode")]
		public System.Web.VisualTree.Elements.ToolBarRenderMode RenderMode {
			get { return this.Element.RenderMode; }
			set { this.Element.RenderMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextDirection 
		/// </summary>
		/// <value>
		/// The TextDirection
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ToolBarTextDirection), "Horizontal")]
		public System.Web.VisualTree.ToolBarTextDirection TextDirection {
			get { return this.Element.TextDirection; }
			set { this.Element.TextDirection = value; }
		}
		
		/// <summary>
		/// Gets or sets the Orientation 
		/// </summary>
		/// <value>
		/// The Orientation
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Orientation))]
		public System.Web.VisualTree.Orientation Orientation {
			get { return this.Element.Orientation; }
			set { this.Element.Orientation = value; }
		}
		
		/// <summary>
		/// Gets or sets the Style 
		/// </summary>
		/// <value>
		/// The Style
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ToolBarStyleConstants))]
		public System.Web.VisualTree.Elements.ToolBarStyleConstants Style {
			get { return this.Element.Style; }
			set { this.Element.Style = value; }
		}
		
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerDefaultProperty)]
		public ToolBarItemCollection Items {
			get { return new ToolBarItemCollection(this, this.Element.Items); }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _ItemAdded = Component.GetHandler<System.EventArgs>(this.Element, this.ItemAddedAction);
			if (_ItemAdded != null) { this.Element.ItemAdded += _ItemAdded.Invoke; }
        }

		private string _ItemAddedAction;

		public string ItemAddedAction
        {
            get
			{
				return _ItemAddedAction;
			}
			set
			{
				_ItemAddedAction = value;
			}            
        }


	}  

	/// <summary>
    /// ToolBarDropDown
    /// </summary>
	public abstract class ToolBarDropDown  : ToolBar   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDown"/> class.
        /// </summary>
		public ToolBarDropDown()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDown"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarDropDown(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarDropDown Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarDropDown;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Opening = Component.GetHandler<System.EventArgs>(this.Element, this.OpeningAction);
			if (_Opening != null) { this.Element.Opening += _Opening.Invoke; }
        }

		private string _OpeningAction;

		public string OpeningAction
        {
            get
			{
				return _OpeningAction;
			}
			set
			{
				_OpeningAction = value;
			}            
        }


	}  

	/// <summary>
    /// ToolBarDropDownMenu
    /// </summary>
	public class ToolBarDropDownMenu  : ToolBarDropDown   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownMenu"/> class.
        /// </summary>
		public ToolBarDropDownMenu()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownMenu"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarDropDownMenu(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarDropDownMenu Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarDropDownMenu;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarDropDownMenu();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ContextMenuStrip
    /// </summary>
	public class ContextMenuStrip  : ToolBarDropDownMenu   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContextMenuStrip"/> class.
        /// </summary>
		public ContextMenuStrip()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextMenuStrip"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ContextMenuStrip(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ContextMenuStripElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ContextMenuStripElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ContextMenuStripElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewBand
    /// </summary>
	[ParseChildren(true)]
	public class DataViewBand  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewBand"/> class.
        /// </summary>
		public DataViewBand()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewBand"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewBand(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewBand Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewBand;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewBand();
        }
 
		/// <summary>
		/// Gets or sets the PixelHeight 
		/// </summary>
		/// <value>
		/// The PixelHeight
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelHeight {
			get { return this.Element.PixelHeight; }
			set { this.Element.PixelHeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the CssClass 
		/// </summary>
		/// <value>
		/// The CssClass
		/// </value>
		[DefaultValue(null)]
		public System.String CssClass {
			get { return this.Element.CssClass; }
			set { this.Element.CssClass = value; }
		}
		
		/// <summary>
		/// Gets or sets the Height 
		/// </summary>
		/// <value>
		/// The Height
		/// </value>
		public System.Web.UI.WebControls.Unit Height {
			get { return this.Element.Height; }
			set { this.Element.Height = value; }
		}
		
		internal bool ShouldSerializeHeight() { return this.Height!=System.Web.UI.WebControls.Unit.Empty;}

		/// <summary>
		/// Gets the IsContainer 
		/// </summary>
		/// <value>
		/// The IsContainer
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsContainer {
			get { return this.Element.IsContainer; }
		}
		
		/// <summary>
		/// Gets the Fields 
		/// </summary>
		/// <value>
		/// The Fields
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public DataViewFieldCollection Fields {
			get { return new DataViewFieldCollection(this, this.Element.Fields); }
		}
		
		/// <summary>
		/// Gets or sets the BackColor 
		/// </summary>
		/// <value>
		/// The BackColor
		/// </value>
		public System.Drawing.Color BackColor {
			get { return this.Element.BackColor; }
			set { this.Element.BackColor = value; }
		}
		
		internal bool ShouldSerializeBackColor() { return this.BackColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the ForeColor 
		/// </summary>
		/// <value>
		/// The ForeColor
		/// </value>
		[DefaultValue(typeof(System.Drawing.Color), "0")]
		public System.Drawing.Color ForeColor {
			get { return this.Element.ForeColor; }
			set { this.Element.ForeColor = value; }
		}
		
		internal bool ShouldSerializeForeColor() { return this.ForeColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the Type 
		/// </summary>
		/// <value>
		/// The Type
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.DataViewBandType), "None")]
		public System.Web.VisualTree.Elements.DataViewBandType Type {
			get { return this.Element.Type; }
			set { this.Element.Type = value; }
		}
		
		/// <summary>
		/// Gets or sets the WrapText 
		/// </summary>
		/// <value>
		/// The WrapText
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean WrapText {
			get { return this.Element.WrapText; }
			set { this.Element.WrapText = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewField
    /// </summary>
	public class DataViewField  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewField"/> class.
        /// </summary>
		public DataViewField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewField();
        }
 
		/// <summary>
		/// Gets or sets the DataMember 
		/// </summary>
		/// <value>
		/// The DataMember
		/// </value>
		[DefaultValue(null)]
		public System.String DataMember {
			get { return this.Element.DataMember; }
			set { this.Element.DataMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the ReadOnly 
		/// </summary>
		/// <value>
		/// The ReadOnly
		/// </value>
		[DefaultValue(true)]
		public System.Boolean ReadOnly {
			get { return this.Element.ReadOnly; }
			set { this.Element.ReadOnly = value; }
		}
		
		/// <summary>
		/// Gets or sets the Visible 
		/// </summary>
		/// <value>
		/// The Visible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Visible {
			get { return this.Element.Visible; }
			set { this.Element.Visible = value; }
		}
		
		/// <summary>
		/// Gets or sets the TabIndex 
		/// </summary>
		/// <value>
		/// The TabIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 TabIndex {
			get { return this.Element.TabIndex; }
			set { this.Element.TabIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelWidth 
		/// </summary>
		/// <value>
		/// The PixelWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelWidth {
			get { return this.Element.PixelWidth; }
			set { this.Element.PixelWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelLeft 
		/// </summary>
		/// <value>
		/// The PixelLeft
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelLeft {
			get { return this.Element.PixelLeft; }
			set { this.Element.PixelLeft = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelTop 
		/// </summary>
		/// <value>
		/// The PixelTop
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelTop {
			get { return this.Element.PixelTop; }
			set { this.Element.PixelTop = value; }
		}
		
		/// <summary>
		/// Gets or sets the Left 
		/// </summary>
		/// <value>
		/// The Left
		/// </value>
		public System.Web.UI.WebControls.Unit Left {
			get { return this.Element.Left; }
			set { this.Element.Left = value; }
		}
		
		internal bool ShouldSerializeLeft() { return this.Left!=System.Web.UI.WebControls.Unit.Empty;}

		/// <summary>
		/// Gets or sets the Top 
		/// </summary>
		/// <value>
		/// The Top
		/// </value>
		public System.Web.UI.WebControls.Unit Top {
			get { return this.Element.Top; }
			set { this.Element.Top = value; }
		}
		
		internal bool ShouldSerializeTop() { return this.Top!=System.Web.UI.WebControls.Unit.Empty;}

		/// <summary>
		/// Gets or sets the Alignment 
		/// </summary>
		/// <value>
		/// The Alignment
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.HorizontalAlignment))]
		public System.Web.VisualTree.HorizontalAlignment Alignment {
			get { return this.Element.Alignment; }
			set { this.Element.Alignment = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelHeight 
		/// </summary>
		/// <value>
		/// The PixelHeight
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelHeight {
			get { return this.Element.PixelHeight; }
			set { this.Element.PixelHeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the WrapText 
		/// </summary>
		/// <value>
		/// The WrapText
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean WrapText {
			get { return this.Element.WrapText; }
			set { this.Element.WrapText = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewShapeField
    /// </summary>
	public class DataViewShapeField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewShapeField"/> class.
        /// </summary>
		public DataViewShapeField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewShapeField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewShapeField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewShapeField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewShapeField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewShapeField();
        }
 
		/// <summary>
		/// Gets or sets the Type 
		/// </summary>
		/// <value>
		/// The Type
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ShapeType))]
		public System.Web.VisualTree.Elements.ShapeType Type {
			get { return this.Element.Type; }
			set { this.Element.Type = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelLeft 
		/// </summary>
		/// <value>
		/// The PixelLeft
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelLeft {
			get { return this.Element.PixelLeft; }
			set { this.Element.PixelLeft = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelRight 
		/// </summary>
		/// <value>
		/// The PixelRight
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelRight {
			get { return this.Element.PixelRight; }
			set { this.Element.PixelRight = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelTop 
		/// </summary>
		/// <value>
		/// The PixelTop
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelTop {
			get { return this.Element.PixelTop; }
			set { this.Element.PixelTop = value; }
		}
		
		/// <summary>
		/// Gets or sets the PixelBottom 
		/// </summary>
		/// <value>
		/// The PixelBottom
		/// </value>
		[DefaultValue(default(System.Int32))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Int32 PixelBottom {
			get { return this.Element.PixelBottom; }
			set { this.Element.PixelBottom = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewGroupBoxField
    /// </summary>
	public class DataViewGroupBoxField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewGroupBoxField"/> class.
        /// </summary>
		public DataViewGroupBoxField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewGroupBoxField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewGroupBoxField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewGroupBoxField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewGroupBoxField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewGroupBoxField();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewImageField
    /// </summary>
	public class DataViewImageField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewImageField"/> class.
        /// </summary>
		public DataViewImageField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewImageField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewImageField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewImageField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewImageField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewImageField();
        }
 
			
		public string ImageReference {
			get { return this.GetStringFromResource( this.Element.Image); }
			set { this.Element.Image = this.GetResourceFromString(value); }
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewLabelBaseField
    /// </summary>
	public class DataViewLabelBaseField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewLabelBaseField"/> class.
        /// </summary>
		public DataViewLabelBaseField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewLabelBaseField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewLabelBaseField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewLabelBaseField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewLabelBaseField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewLabelBaseField();
        }
 
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the TemplateText 
		/// </summary>
		/// <value>
		/// The TemplateText
		/// </value>
		[DefaultValue(null)]
		public System.String TemplateText {
			get { return this.Element.TemplateText; }
			set { this.Element.TemplateText = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewButtonField
    /// </summary>
	public class DataViewButtonField  : DataViewLabelBaseField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewButtonField"/> class.
        /// </summary>
		public DataViewButtonField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewButtonField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewButtonField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewButtonField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewButtonField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewButtonField();
        }
 
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewCheckField
    /// </summary>
	public class DataViewCheckField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewCheckField"/> class.
        /// </summary>
		public DataViewCheckField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewCheckField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewCheckField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewCheckField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewCheckField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewCheckField();
        }
 
		/// <summary>
		/// Gets or sets the CheckedValue 
		/// </summary>
		/// <value>
		/// The CheckedValue
		/// </value>
		[DefaultValue(null)]
		public System.String CheckedValue {
			get { return this.Element.CheckedValue as System.String; }
			set { this.Element.CheckedValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the UncheckedValue 
		/// </summary>
		/// <value>
		/// The UncheckedValue
		/// </value>
		[DefaultValue(null)]
		public System.String UncheckedValue {
			get { return this.Element.UncheckedValue as System.String; }
			set { this.Element.UncheckedValue = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewRadioField
    /// </summary>
	public class DataViewRadioField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewRadioField"/> class.
        /// </summary>
		public DataViewRadioField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewRadioField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewRadioField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewRadioField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewRadioField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewRadioField();
        }
 
		/// <summary>
		/// Gets or sets the Sorted 
		/// </summary>
		/// <value>
		/// The Sorted
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Sorted {
			get { return this.Element.Sorted; }
			set { this.Element.Sorted = value; }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewDropDownField
    /// </summary>
	public class DataViewDropDownField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewDropDownField"/> class.
        /// </summary>
		public DataViewDropDownField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewDropDownField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewDropDownField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewDropDownField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewDropDownField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewDropDownField();
        }
 
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets the IsContainer 
		/// </summary>
		/// <value>
		/// The IsContainer
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsContainer {
			get { return this.Element.IsContainer; }
		}
		
		/// <summary>
		/// Gets or sets the DropDownControl 
		/// </summary>
		/// <value>
		/// The DropDownControl
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ControlElement DropDownControl {
			get { return this.Element.DropDownControl; }
			set { this.Element.DropDownControl = value; }
		}
		
		/// <summary>
		/// Gets or sets the ValueMember 
		/// </summary>
		/// <value>
		/// The ValueMember
		/// </value>
		[DefaultValue(null)]
		public System.String ValueMember {
			get { return this.Element.ValueMember; }
			set { this.Element.ValueMember = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewComboField
    /// </summary>
	public class DataViewComboField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewComboField"/> class.
        /// </summary>
		public DataViewComboField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewComboField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewComboField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewComboField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewComboField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewComboField();
        }
 
		/// <summary>
		/// Gets or sets the Sorted 
		/// </summary>
		/// <value>
		/// The Sorted
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Sorted {
			get { return this.Element.Sorted; }
			set { this.Element.Sorted = value; }
		}
		
		/// <summary>
		/// Gets or sets the ValueMember 
		/// </summary>
		/// <value>
		/// The ValueMember
		/// </value>
		[DefaultValue(null)]
		public System.String ValueMember {
			get { return this.Element.ValueMember; }
			set { this.Element.ValueMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the DisplayMember 
		/// </summary>
		/// <value>
		/// The DisplayMember
		/// </value>
		[DefaultValue(null)]
		public System.String DisplayMember {
			get { return this.Element.DisplayMember; }
			set { this.Element.DisplayMember = value; }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewTextField
    /// </summary>
	public class DataViewTextField  : DataViewField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewTextField"/> class.
        /// </summary>
		public DataViewTextField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewTextField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewTextField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewTextField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewTextField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewTextField();
        }
 
		/// <summary>
		/// Gets or sets the Format 
		/// </summary>
		/// <value>
		/// The Format
		/// </value>
		[DefaultValue("[general]")]
		public System.String Format {
			get { return this.Element.Format; }
			set { this.Element.Format = value; }
		}
		
		/// <summary>
		/// Gets or sets the Mask 
		/// </summary>
		/// <value>
		/// The Mask
		/// </value>
		[DefaultValue(null)]
		public System.String Mask {
			get { return this.Element.Mask; }
			set { this.Element.Mask = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewDateField
    /// </summary>
	public class DataViewDateField  : DataViewTextField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewDateField"/> class.
        /// </summary>
		public DataViewDateField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewDateField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewDateField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewDateField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewDateField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewDateField();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataView
    /// </summary>
	[ParseChildren(true)]
	public class DataView  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataView"/> class.
        /// </summary>
		public DataView()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataView"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataView(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewElement();
        }
 
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets the IsContainer 
		/// </summary>
		/// <value>
		/// The IsContainer
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsContainer {
			get { return this.Element.IsContainer; }
		}
		
		/// <summary>
		/// Gets the Bands 
		/// </summary>
		/// <value>
		/// The Bands
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public DataViewBandCollection Bands {
			get { return new DataViewBandCollection(this, this.Element.Bands); }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewLabelField
    /// </summary>
	public class DataViewLabelField  : DataViewLabelBaseField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewLabelField"/> class.
        /// </summary>
		public DataViewLabelField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewLabelField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewLabelField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewLabelField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewLabelField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewLabelField();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewNumericField
    /// </summary>
	public class DataViewNumericField  : DataViewTextField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewNumericField"/> class.
        /// </summary>
		public DataViewNumericField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewNumericField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewNumericField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewNumericField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewNumericField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewNumericField();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataViewTreeViewField
    /// </summary>
	public class DataViewTreeViewField  : DataViewLabelBaseField   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewTreeViewField"/> class.
        /// </summary>
		public DataViewTreeViewField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewTreeViewField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataViewTreeViewField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataViewTreeViewField Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataViewTreeViewField;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataViewTreeViewField();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarItem
    /// </summary>
	[ParseChildren(true, "Items")]
	public class ToolBarItem  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarItem"/> class.
        /// </summary>
		public ToolBarItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarItem();
        }
 
		/// <summary>
		/// Gets or sets the DisplayStyle 
		/// </summary>
		/// <value>
		/// The DisplayStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.ToolBarItemDisplayStyle), "ImageAndText")]
		public System.Web.VisualTree.Elements.ToolBarItemDisplayStyle DisplayStyle {
			get { return this.Element.DisplayStyle; }
			set { this.Element.DisplayStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoToolTip 
		/// </summary>
		/// <value>
		/// The AutoToolTip
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AutoToolTip {
			get { return this.Element.AutoToolTip; }
			set { this.Element.AutoToolTip = value; }
		}
		
		/// <summary>
		/// Gets or sets the Alignment 
		/// </summary>
		/// <value>
		/// The Alignment
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.ToolBarItemAlignment), "Left")]
		public System.Web.VisualTree.Elements.ToolBarItemAlignment Alignment {
			get { return this.Element.Alignment; }
			set { this.Element.Alignment = value; }
		}
		
			
		public string Image {
			get { return this.GetStringFromResource( this.Element.Image); }
			set { this.Element.Image = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the ImageTransparentColor 
		/// </summary>
		/// <value>
		/// The ImageTransparentColor
		/// </value>
		public System.Drawing.Color ImageTransparentColor {
			get { return this.Element.ImageTransparentColor; }
			set { this.Element.ImageTransparentColor = value; }
		}
		
		internal bool ShouldSerializeImageTransparentColor() { return this.ImageTransparentColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the TextAlign 
		/// </summary>
		/// <value>
		/// The TextAlign
		/// </value>
		[DefaultValue(typeof(System.Drawing.ContentAlignment), "MiddleRight")]
		public System.Drawing.ContentAlignment TextAlign {
			get { return this.Element.TextAlign; }
			set { this.Element.TextAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the AccessibleName 
		/// </summary>
		/// <value>
		/// The AccessibleName
		/// </value>
		[DefaultValue(null)]
		public System.String AccessibleName {
			get { return this.Element.AccessibleName; }
			set { this.Element.AccessibleName = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextDirection 
		/// </summary>
		/// <value>
		/// The TextDirection
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ToolBarTextDirection), "Horizontal")]
		public System.Web.VisualTree.ToolBarTextDirection TextDirection {
			get { return this.Element.TextDirection; }
			set { this.Element.TextDirection = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextImageRelation 
		/// </summary>
		/// <value>
		/// The TextImageRelation
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.TextImageRelation), "Overlay")]
		public System.Web.VisualTree.TextImageRelation TextImageRelation {
			get { return this.Element.TextImageRelation; }
			set { this.Element.TextImageRelation = value; }
		}
		
		/// <summary>
		/// Gets or sets the Bevel 
		/// </summary>
		/// <value>
		/// The Bevel
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.PanelBevelConstants))]
		public System.Web.VisualTree.Elements.PanelBevelConstants Bevel {
			get { return this.Element.Bevel; }
			set { this.Element.Bevel = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageIndex 
		/// </summary>
		/// <value>
		/// The ImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ImageIndex {
			get { return this.Element.ImageIndex; }
			set { this.Element.ImageIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageKey 
		/// </summary>
		/// <value>
		/// The ImageKey
		/// </value>
		[DefaultValue(null)]
		public System.String ImageKey {
			get { return this.Element.ImageKey; }
			set { this.Element.ImageKey = value; }
		}
		
		/// <summary>
		/// Gets or sets the DropDownArrows 
		/// </summary>
		/// <value>
		/// The DropDownArrows
		/// </value>
		[DefaultValue(false)]
		public System.Boolean DropDownArrows {
			get { return this.Element.DropDownArrows; }
			set { this.Element.DropDownArrows = value; }
		}
		
		/// <summary>
		/// Gets or sets the RightToLeftAutoMirrorImage 
		/// </summary>
		/// <value>
		/// The RightToLeftAutoMirrorImage
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean RightToLeftAutoMirrorImage {
			get { return this.Element.RightToLeftAutoMirrorImage; }
			set { this.Element.RightToLeftAutoMirrorImage = value; }
		}
		
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerDefaultProperty)]
		public ToolBarItemCollection Items {
			get { return new ToolBarItemCollection(this, this.Element.Items); }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets or sets the BorderSides 
		/// </summary>
		/// <value>
		/// The BorderSides
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ToolBarStatusLabelBorderSides))]
		public System.Web.VisualTree.ToolBarStatusLabelBorderSides BorderSides {
			get { return this.Element.BorderSides; }
			set { this.Element.BorderSides = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageAlign 
		/// </summary>
		/// <value>
		/// The ImageAlign
		/// </value>
		[DefaultValue(typeof(System.Drawing.ContentAlignment), "MiddleCenter")]
		public System.Drawing.ContentAlignment ImageAlign {
			get { return this.Element.ImageAlign; }
			set { this.Element.ImageAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageScaling 
		/// </summary>
		/// <value>
		/// The ImageScaling
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ToolStripItemImageScaling))]
		public System.Web.VisualTree.ToolStripItemImageScaling ImageScaling {
			get { return this.Element.ImageScaling; }
			set { this.Element.ImageScaling = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarDropDownItem
    /// </summary>
	[ParseChildren(true, "DropDownItems")]
	public abstract class ToolBarDropDownItem  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownItem"/> class.
        /// </summary>
		public ToolBarDropDownItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarDropDownItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarDropDownItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarDropDownItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets the DropDownItems 
		/// </summary>
		/// <value>
		/// The DropDownItems
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerDefaultProperty)]
		public ToolBarItemCollection DropDownItems {
			get { return new ToolBarItemCollection(this, this.Element.DropDownItems); }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets the HasDropDownItems 
		/// </summary>
		/// <value>
		/// The HasDropDownItems
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasDropDownItems {
			get { return this.Element.HasDropDownItems; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DropDownButton
    /// </summary>
	public class DropDownButton  : ToolBarDropDownItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DropDownButton"/> class.
        /// </summary>
		public DropDownButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DropDownButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DropDownButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DropDownButtonElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DropDownButtonElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DropDownButtonElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _DropDownOpening = Component.GetHandler<System.EventArgs>(this.Element, this.DropDownOpeningAction);
			if (_DropDownOpening != null) { this.Element.DropDownOpening += _DropDownOpening.Invoke; }
			IVisualElementAction<System.EventArgs> _DropDownOpened = Component.GetHandler<System.EventArgs>(this.Element, this.DropDownOpenedAction);
			if (_DropDownOpened != null) { this.Element.DropDownOpened += _DropDownOpened.Invoke; }
			IVisualElementAction<System.EventArgs> _DropDownClosed = Component.GetHandler<System.EventArgs>(this.Element, this.DropDownClosedAction);
			if (_DropDownClosed != null) { this.Element.DropDownClosed += _DropDownClosed.Invoke; }
        }

		private string _DropDownOpeningAction;

		public string DropDownOpeningAction
        {
            get
			{
				return _DropDownOpeningAction;
			}
			set
			{
				_DropDownOpeningAction = value;
			}            
        }

		private string _DropDownOpenedAction;

		public string DropDownOpenedAction
        {
            get
			{
				return _DropDownOpenedAction;
			}
			set
			{
				_DropDownOpenedAction = value;
			}            
        }

		private string _DropDownClosedAction;

		public string DropDownClosedAction
        {
            get
			{
				return _DropDownClosedAction;
			}
			set
			{
				_DropDownClosedAction = value;
			}            
        }


	}  

	/// <summary>
    /// RibbonPageGroup
    /// </summary>
	public class RibbonPageGroup  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonPageGroup"/> class.
        /// </summary>
		public RibbonPageGroup()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonPageGroup"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RibbonPageGroup(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.RibbonPageGroupElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.RibbonPageGroupElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.RibbonPageGroupElement();
        }
 
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ControlElementCollection Items {
			get { return this.Element.Items; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ButtonBase
    /// </summary>
	public abstract class ButtonBase  : ContentControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonBase"/> class.
        /// </summary>
		public ButtonBase()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonBase"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ButtonBase(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ButtonBaseElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ButtonBaseElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the FlatStyle 
		/// </summary>
		/// <value>
		/// The FlatStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.FlatStyle), "System")]
		public System.Web.VisualTree.FlatStyle FlatStyle {
			get { return this.Element.FlatStyle; }
			set { this.Element.FlatStyle = value; }
		}
		
			
		public string Image {
			get { return this.GetStringFromResource( this.Element.Image); }
			set { this.Element.Image = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the ImageIndex 
		/// </summary>
		/// <value>
		/// The ImageIndex
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 ImageIndex {
			get { return this.Element.ImageIndex; }
			set { this.Element.ImageIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageAlign 
		/// </summary>
		/// <value>
		/// The ImageAlign
		/// </value>
		[DefaultValue(typeof(System.Drawing.ContentAlignment), "MiddleCenter")]
		public System.Drawing.ContentAlignment ImageAlign {
			get { return this.Element.ImageAlign; }
			set { this.Element.ImageAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the UseVisualStyleBackColor 
		/// </summary>
		/// <value>
		/// The UseVisualStyleBackColor
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean UseVisualStyleBackColor {
			get { return this.Element.UseVisualStyleBackColor; }
			set { this.Element.UseVisualStyleBackColor = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextAlign 
		/// </summary>
		/// <value>
		/// The TextAlign
		/// </value>
		[DefaultValue(typeof(System.Drawing.ContentAlignment), "MiddleCenter")]
		public System.Drawing.ContentAlignment TextAlign {
			get { return this.Element.TextAlign; }
			set { this.Element.TextAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextImageRelation 
		/// </summary>
		/// <value>
		/// The TextImageRelation
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.TextImageRelation), "Overlay")]
		public System.Web.VisualTree.TextImageRelation TextImageRelation {
			get { return this.Element.TextImageRelation; }
			set { this.Element.TextImageRelation = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// RadioButton
    /// </summary>
	public class RadioButton  : ButtonBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
		public RadioButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RadioButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.RadioButtonElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.RadioButtonElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.RadioButtonElement();
        }
 
		/// <summary>
		/// Gets or sets the IsChecked 
		/// </summary>
		/// <value>
		/// The IsChecked
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsChecked {
			get { return this.Element.IsChecked; }
			set { this.Element.IsChecked = value; }
		}
		
		/// <summary>
		/// Gets or sets the CheckAlign 
		/// </summary>
		/// <value>
		/// The CheckAlign
		/// </value>
		[DefaultValue(typeof(System.Drawing.ContentAlignment), "MiddleLeft")]
		public System.Drawing.ContentAlignment CheckAlign {
			get { return this.Element.CheckAlign; }
			set { this.Element.CheckAlign = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _CheckedChanged = Component.GetHandler<System.EventArgs>(this.Element, this.CheckedChangedAction);
			if (_CheckedChanged != null) { this.Element.CheckedChanged += _CheckedChanged.Invoke; }
        }

		private string _CheckedChangedAction;

		public string CheckedChangedAction
        {
            get
			{
				return _CheckedChangedAction;
			}
			set
			{
				_CheckedChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// RadioGroupItem
    /// </summary>
	public class RadioGroupItem  : RadioButton   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroupItem"/> class.
        /// </summary>
		public RadioGroupItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroupItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RadioGroupItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.RadioGroupItemElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.RadioGroupItemElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.RadioGroupItemElement();
        }
 
		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		[DefaultValue(null)]
		public System.Object Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// RadioGroup
    /// </summary>
	public class RadioGroup  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroup"/> class.
        /// </summary>
		public RadioGroup()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroup"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RadioGroup(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.RadioGroupElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.RadioGroupElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.RadioGroupElement();
        }
 
		/// <summary>
		/// Gets or sets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
			set { this.Element.Index = value; }
		}
		
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.RadioGroupItemCollection Items {
			get { return this.Element.Items; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Change = Component.GetHandler<System.EventArgs>(this.Element, this.ChangeAction);
			if (_Change != null) { this.Element.Change += _Change.Invoke; }
        }

		private string _ChangeAction;

		public string ChangeAction
        {
            get
			{
				return _ChangeAction;
			}
			set
			{
				_ChangeAction = value;
			}            
        }


	}  

	/// <summary>
    /// Input
    /// </summary>
	public abstract class Input  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Input"/> class.
        /// </summary>
		public Input()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Input"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Input(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.InputElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.InputElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the AutoCompleteMode 
		/// </summary>
		/// <value>
		/// The AutoCompleteMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.AutoCompleteMode), "None")]
		public System.Web.VisualTree.AutoCompleteMode AutoCompleteMode {
			get { return this.Element.AutoCompleteMode; }
			set { this.Element.AutoCompleteMode = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TextBoxBase
    /// </summary>
	public abstract class TextBoxBase  : Input   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxBase"/> class.
        /// </summary>
		public TextBoxBase()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxBase"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TextBoxBase(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TextBoxBaseElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TextBoxBaseElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the MaxLength 
		/// </summary>
		/// <value>
		/// The MaxLength
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 MaxLength {
			get { return this.Element.MaxLength; }
			set { this.Element.MaxLength = value; }
		}
		
		/// <summary>
		/// Gets or sets the ReadOnly 
		/// </summary>
		/// <value>
		/// The ReadOnly
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ReadOnly {
			get { return this.Element.ReadOnly; }
			set { this.Element.ReadOnly = value; }
		}
		
		/// <summary>
		/// Gets or sets the Multiline 
		/// </summary>
		/// <value>
		/// The Multiline
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Multiline {
			get { return this.Element.Multiline; }
			set { this.Element.Multiline = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectionStart 
		/// </summary>
		/// <value>
		/// The SelectionStart
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectionStart {
			get { return this.Element.SelectionStart; }
			set { this.Element.SelectionStart = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectionLength 
		/// </summary>
		/// <value>
		/// The SelectionLength
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectionLength {
			get { return this.Element.SelectionLength; }
			set { this.Element.SelectionLength = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextLength 
		/// </summary>
		/// <value>
		/// The TextLength
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 TextLength {
			get { return this.Element.TextLength; }
			set { this.Element.TextLength = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedText 
		/// </summary>
		/// <value>
		/// The SelectedText
		/// </value>
		[DefaultValue(null)]
		public System.String SelectedText {
			get { return this.Element.SelectedText; }
			set { this.Element.SelectedText = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectOnFocus 
		/// </summary>
		/// <value>
		/// The SelectOnFocus
		/// </value>
		[DefaultValue(false)]
		public System.Boolean SelectOnFocus {
			get { return this.Element.SelectOnFocus; }
			set { this.Element.SelectOnFocus = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TextBox
    /// </summary>
	public class TextBox  : TextBoxBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextBox"/> class.
        /// </summary>
		public TextBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TextBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TextBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TextBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TextBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the AllowBlank 
		/// </summary>
		/// <value>
		/// The AllowBlank
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AllowBlank {
			get { return this.Element.AllowBlank; }
			set { this.Element.AllowBlank = value; }
		}
		
		/// <summary>
		/// Gets or sets the AcceptsReturn 
		/// </summary>
		/// <value>
		/// The AcceptsReturn
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AcceptsReturn {
			get { return this.Element.AcceptsReturn; }
			set { this.Element.AcceptsReturn = value; }
		}
		
		/// <summary>
		/// Gets or sets the UseSystemPasswordChar 
		/// </summary>
		/// <value>
		/// The UseSystemPasswordChar
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean UseSystemPasswordChar {
			get { return this.Element.UseSystemPasswordChar; }
			set { this.Element.UseSystemPasswordChar = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScrollBars 
		/// </summary>
		/// <value>
		/// The ScrollBars
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ScrollBars))]
		public System.Web.VisualTree.ScrollBars ScrollBars {
			get { return this.Element.ScrollBars; }
			set { this.Element.ScrollBars = value; }
		}
		
		/// <summary>
		/// Gets or sets the PasswordChar 
		/// </summary>
		/// <value>
		/// The PasswordChar
		/// </value>
		[DefaultValue("")]
		public System.String PasswordChar {
			get { return this.Element.PasswordChar; }
			set { this.Element.PasswordChar = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImeMode 
		/// </summary>
		/// <value>
		/// The ImeMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ImeMode), "On")]
		public System.Web.VisualTree.ImeMode ImeMode {
			get { return this.Element.ImeMode; }
			set { this.Element.ImeMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextAlign 
		/// </summary>
		/// <value>
		/// The TextAlign
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.HorizontalAlignment), "Left")]
		public System.Web.VisualTree.HorizontalAlignment TextAlign {
			get { return this.Element.TextAlign; }
			set { this.Element.TextAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelLength 
		/// </summary>
		/// <value>
		/// The SelLength
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelLength {
			get { return this.Element.SelLength; }
			set { this.Element.SelLength = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelStart 
		/// </summary>
		/// <value>
		/// The SelStart
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelStart {
			get { return this.Element.SelStart; }
			set { this.Element.SelStart = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelText 
		/// </summary>
		/// <value>
		/// The SelText
		/// </value>
		[DefaultValue(null)]
		public System.String SelText {
			get { return this.Element.SelText; }
			set { this.Element.SelText = value; }
		}
		
		/// <summary>
		/// Gets or sets the CharacterCasing 
		/// </summary>
		/// <value>
		/// The CharacterCasing
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.CharacterCasing))]
		public System.Web.VisualTree.CharacterCasing CharacterCasing {
			get { return this.Element.CharacterCasing; }
			set { this.Element.CharacterCasing = value; }
		}
		
		/// <summary>
		/// Gets or sets the ClientDirtychange 
		/// </summary>
		/// <value>
		/// The ClientDirtychange
		/// </value>
		[DefaultValue(null)]
		public System.String ClientDirtychange {
			get { return this.Element.ClientDirtychange; }
			set { this.Element.ClientDirtychange = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Dirtychange = Component.GetHandler<System.EventArgs>(this.Element, this.DirtychangeAction);
			if (_Dirtychange != null) { this.Element.Dirtychange += _Dirtychange.Invoke; }
        }

		private string _DirtychangeAction;

		public string DirtychangeAction
        {
            get
			{
				return _DirtychangeAction;
			}
			set
			{
				_DirtychangeAction = value;
			}            
        }


	}  

	/// <summary>
    /// TimeEdit
    /// </summary>
	public class TimeEdit  : TextBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeEdit"/> class.
        /// </summary>
		public TimeEdit()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeEdit"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TimeEdit(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TimeEditElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TimeEditElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TimeEditElement();
        }
 
		/// <summary>
		/// Gets or sets the Increment 
		/// </summary>
		/// <value>
		/// The Increment
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Increment {
			get { return this.Element.Increment; }
			set { this.Element.Increment = value; }
		}
		
		/// <summary>
		/// Gets or sets the MinTime 
		/// </summary>
		/// <value>
		/// The MinTime
		/// </value>
		public System.DateTime MinTime {
			get { return this.Element.MinTime; }
			set { this.Element.MinTime = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxTime 
		/// </summary>
		/// <value>
		/// The MaxTime
		/// </value>
		public System.DateTime MaxTime {
			get { return this.Element.MaxTime; }
			set { this.Element.MaxTime = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DockPanelItem
    /// </summary>
	public abstract class DockPanelItem  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelItem"/> class.
        /// </summary>
		public DockPanelItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DockPanelItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DockPanelItemElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DockPanelItemElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

			
		public string Icon {
			get { return this.GetStringFromResource( this.Element.Icon); }
			set { this.Element.Icon = this.GetResourceFromString(value); }
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Closed = Component.GetHandler<System.EventArgs>(this.Element, this.ClosedAction);
			if (_Closed != null) { this.Element.Closed += _Closed.Invoke; }
        }

		private string _ClosedAction;

		public string ClosedAction
        {
            get
			{
				return _ClosedAction;
			}
			set
			{
				_ClosedAction = value;
			}            
        }


	}  

	/// <summary>
    /// DockPanelDocument
    /// </summary>
	public class DockPanelDocument  : DockPanelItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelDocument"/> class.
        /// </summary>
		public DockPanelDocument()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelDocument"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DockPanelDocument(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DockPanelDocumentElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DockPanelDocumentElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DockPanelDocumentElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DockPanelTool
    /// </summary>
	public class DockPanelTool  : DockPanelItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelTool"/> class.
        /// </summary>
		public DockPanelTool()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelTool"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DockPanelTool(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DockPanelToolElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DockPanelToolElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DockPanelToolElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ErrorProvider
    /// </summary>
	public class ErrorProvider  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorProvider"/> class.
        /// </summary>
		public ErrorProvider()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorProvider"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ErrorProvider(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ErrorProviderElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ErrorProviderElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ErrorProviderElement();
        }
 
		/// <summary>
		/// Gets or sets the BlinkRate 
		/// </summary>
		/// <value>
		/// The BlinkRate
		/// </value>
		[DefaultValue(250)]
		public System.Int32 BlinkRate {
			get { return this.Element.BlinkRate; }
			set { this.Element.BlinkRate = value; }
		}
		
		/// <summary>
		/// Gets or sets the ControlContainer 
		/// </summary>
		/// <value>
		/// The ControlContainer
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ControlContainerElement ControlContainer {
			get { return this.Element.ControlContainer; }
			set { this.Element.ControlContainer = value; }
		}
		
		/// <summary>
		/// Gets or sets the Icon 
		/// </summary>
		/// <value>
		/// The Icon
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.IconReference Icon {
			get { return this.Element.Icon; }
			set { this.Element.Icon = value; }
		}
		
		/// <summary>
		/// Gets or sets the RightToLeft 
		/// </summary>
		/// <value>
		/// The RightToLeft
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean RightToLeft {
			get { return this.Element.RightToLeft; }
			set { this.Element.RightToLeft = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Grid
    /// </summary>
	[ParseChildren(true)]
	public class Grid  : ScrollableControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Grid"/> class.
        /// </summary>
		public Grid()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Grid"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Grid(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridElement();
        }
 
		/// <summary>
		/// Gets or sets the ClientRowTemplate 
		/// </summary>
		/// <value>
		/// The ClientRowTemplate
		/// </value>
		[DefaultValue("")]
		public System.String ClientRowTemplate {
			get { return this.Element.ClientRowTemplate; }
			set { this.Element.ClientRowTemplate = value; }
		}
		
		/// <summary>
		/// Gets or sets the contextMenuXPosition 
		/// </summary>
		/// <value>
		/// The contextMenuXPosition
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 contextMenuXPosition {
			get { return this.Element.contextMenuXPosition; }
			set { this.Element.contextMenuXPosition = value; }
		}
		
		/// <summary>
		/// Gets or sets the contextMenuYPosition 
		/// </summary>
		/// <value>
		/// The contextMenuYPosition
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 contextMenuYPosition {
			get { return this.Element.contextMenuYPosition; }
			set { this.Element.contextMenuYPosition = value; }
		}
		
		/// <summary>
        /// The state of the ContextMenuID property.
        /// </summary>
		private string _ContextMenuID = null;

		/// <summary>
		/// Contexts the menu show.
/// </summary>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string ContextMenuID {
           get
            {
                if (this.Element.ContextMenu != null)
                {
                    return this.Element.ContextMenu.ID;
                }
                return _ContextMenuID;
            }
            set
            {
				// Set the menu id state
				_ContextMenuID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeContextMenu(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeContextMenu;
				}
            }
		}

		/// <summary>
        /// Initialize the ContextMenu property.
        /// </summary>
		private void InitializeContextMenu(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_ContextMenuID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.ContextMenu = component.Element as System.Web.VisualTree.Elements.MenuElement;
				}
			}
		}

		/// <summary>
		/// Contexts the menu show.
/// </summary>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.MenuElement ContextMenu {
			get { return this.Element.ContextMenu; }
			set { this.Element.ContextMenu = value; }
		}
		
		/// <summary>
		/// Gets the Columns 
		/// </summary>
		/// <value>
		/// The Columns
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public GridColumnCollection Columns {
			get { return new GridColumnCollection(this, this.Element.Columns); }
		}
		
		/// <summary>
		/// Gets the HeaderColumns 
		/// </summary>
		/// <value>
		/// The HeaderColumns
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public GridColumnCollection HeaderColumns {
			get { return new GridColumnCollection(this, this.Element.HeaderColumns); }
		}
		
		/// <summary>
		/// Gets or sets the ReadOnly 
		/// </summary>
		/// <value>
		/// The ReadOnly
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ReadOnly {
			get { return this.Element.ReadOnly; }
			set { this.Element.ReadOnly = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowUserToAddRows 
		/// </summary>
		/// <value>
		/// The AllowUserToAddRows
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AllowUserToAddRows {
			get { return this.Element.AllowUserToAddRows; }
			set { this.Element.AllowUserToAddRows = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsGroupingGrid 
		/// </summary>
		/// <value>
		/// The IsGroupingGrid
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsGroupingGrid {
			get { return this.Element.IsGroupingGrid; }
			set { this.Element.IsGroupingGrid = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowUserToDeleteRows 
		/// </summary>
		/// <value>
		/// The AllowUserToDeleteRows
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AllowUserToDeleteRows {
			get { return this.Element.AllowUserToDeleteRows; }
			set { this.Element.AllowUserToDeleteRows = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowUserToResizeColumns 
		/// </summary>
		/// <value>
		/// The AllowUserToResizeColumns
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AllowUserToResizeColumns {
			get { return this.Element.AllowUserToResizeColumns; }
			set { this.Element.AllowUserToResizeColumns = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowUserToOrderColumns 
		/// </summary>
		/// <value>
		/// The AllowUserToOrderColumns
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AllowUserToOrderColumns {
			get { return this.Element.AllowUserToOrderColumns; }
			set { this.Element.AllowUserToOrderColumns = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowUserToResizeRows 
		/// </summary>
		/// <value>
		/// The AllowUserToResizeRows
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AllowUserToResizeRows {
			get { return this.Element.AllowUserToResizeRows; }
			set { this.Element.AllowUserToResizeRows = value; }
		}
		
		/// <summary>
		/// Gets or sets the ColumnHeadersHeightSizeMode 
		/// </summary>
		/// <value>
		/// The ColumnHeadersHeightSizeMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.GridColumnHeadersHeightSizeMode), "EnableResizing")]
		public System.Web.VisualTree.Elements.GridColumnHeadersHeightSizeMode ColumnHeadersHeightSizeMode {
			get { return this.Element.ColumnHeadersHeightSizeMode; }
			set { this.Element.ColumnHeadersHeightSizeMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoGenerateColumns 
		/// </summary>
		/// <value>
		/// The AutoGenerateColumns
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AutoGenerateColumns {
			get { return this.Element.AutoGenerateColumns; }
			set { this.Element.AutoGenerateColumns = value; }
		}
		
		/// <summary>
		/// Changes column count.
/// </summary>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ColumnCount {
			get { return this.Element.ColumnCount; }
			set { this.Element.ColumnCount = value; }
		}
		
		/// <summary>
		/// Gets or sets the UseFixedHeaders 
		/// </summary>
		/// <value>
		/// The UseFixedHeaders
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean UseFixedHeaders {
			get { return this.Element.UseFixedHeaders; }
			set { this.Element.UseFixedHeaders = value; }
		}
		
		/// <summary>
		/// Gets or sets the FirstDisplayedScrollingRowIndex 
		/// </summary>
		/// <value>
		/// The FirstDisplayedScrollingRowIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 FirstDisplayedScrollingRowIndex {
			get { return this.Element.FirstDisplayedScrollingRowIndex; }
			set { this.Element.FirstDisplayedScrollingRowIndex = value; }
		}
		
		/// <summary>
		/// Changes rows count.
/// </summary>
		[DefaultValue(default(System.Int32))]
		public System.Int32 RowCount {
			get { return this.Element.RowCount; }
			set { this.Element.RowCount = value; }
		}
		
		/// <summary>
		/// Gets or sets the RowHeadersBorderStyle 
		/// </summary>
		/// <value>
		/// The RowHeadersBorderStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.GridHeaderBorderStyle))]
		public System.Web.VisualTree.Elements.GridHeaderBorderStyle RowHeadersBorderStyle {
			get { return this.Element.RowHeadersBorderStyle; }
			set { this.Element.RowHeadersBorderStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the RowHeaderVisible 
		/// </summary>
		/// <value>
		/// The RowHeaderVisible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean RowHeaderVisible {
			get { return this.Element.RowHeaderVisible; }
			set { this.Element.RowHeaderVisible = value; }
		}
		
		/// <summary>
		/// Gets or sets the ColumnHeadersVisible 
		/// </summary>
		/// <value>
		/// The ColumnHeadersVisible
		/// </value>
		[DefaultValue(true)]
		public System.Boolean ColumnHeadersVisible {
			get { return this.Element.ColumnHeadersVisible; }
			set { this.Element.ColumnHeadersVisible = value; }
		}
		
		/// <summary>
		/// Gets or sets the RowHeadersWidth 
		/// </summary>
		/// <value>
		/// The RowHeadersWidth
		/// </value>
		[DefaultValue(43)]
		public System.Int32 RowHeadersWidth {
			get { return this.Element.RowHeadersWidth; }
			set { this.Element.RowHeadersWidth = value; }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets or sets the RowsDefaultCellStyle 
		/// </summary>
		/// <value>
		/// The RowsDefaultCellStyle
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.GridCellStyle RowsDefaultCellStyle {
			get { return this.Element.RowsDefaultCellStyle; }
			set { this.Element.RowsDefaultCellStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScrollBars 
		/// </summary>
		/// <value>
		/// The ScrollBars
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ScrollBars), "Both")]
		public System.Web.VisualTree.ScrollBars ScrollBars {
			get { return this.Element.ScrollBars; }
			set { this.Element.ScrollBars = value; }
		}
		
		/// <summary>
		/// Gets or sets the FixedAlignment 
		/// </summary>
		/// <value>
		/// The FixedAlignment
		/// </value>
		[DefaultValue(default(System.Int16))]
		public System.Int16 FixedAlignment {
			get { return this.Element.FixedAlignment; }
			set { this.Element.FixedAlignment = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectionMode 
		/// </summary>
		/// <value>
		/// The SelectionMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.CellSelectionMode), "Cell")]
		public System.Web.VisualTree.Elements.CellSelectionMode SelectionMode {
			get { return this.Element.SelectionMode; }
			set { this.Element.SelectionMode = value; }
		}
		
		/// <summary>
		/// Gets the SelectedRowIndex 
		/// </summary>
		/// <value>
		/// The SelectedRowIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedRowIndex {
			get { return this.Element.SelectedRowIndex; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedColumnIndex 
		/// </summary>
		/// <value>
		/// The SelectedColumnIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedColumnIndex {
			get { return this.Element.SelectedColumnIndex; }
			set { this.Element.SelectedColumnIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the MultiSelect 
		/// </summary>
		/// <value>
		/// The MultiSelect
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean MultiSelect {
			get { return this.Element.MultiSelect; }
			set { this.Element.MultiSelect = value; }
		}
		
		/// <summary>
		/// Gets or sets the Content 
		/// </summary>
		/// <value>
		/// The Content
		/// </value>
		[DefaultValue(null)]
		public System.String Content {
			get { return this.Element.Content as System.String; }
			set { this.Element.Content = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxRows 
		/// </summary>
		/// <value>
		/// The MaxRows
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 MaxRows {
			get { return this.Element.MaxRows; }
			set { this.Element.MaxRows = value; }
		}
		
		/// <summary>
		/// Gets or sets the CanSelect 
		/// </summary>
		/// <value>
		/// The CanSelect
		/// </value>
		[DefaultValue(true)]
		public System.Boolean CanSelect {
			get { return this.Element.CanSelect; }
			set { this.Element.CanSelect = value; }
		}
		
		/// <summary>
		/// Gets or sets the FrozenCols 
		/// </summary>
		/// <value>
		/// The FrozenCols
		/// </value>
		[DefaultValue(0)]
		public System.Int32 FrozenCols {
			get { return this.Element.FrozenCols; }
			set { this.Element.FrozenCols = value; }
		}
		
		/// <summary>
		/// Gets the ColData 
		/// </summary>
		/// <value>
		/// The ColData
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.Dictionary<System.Int32, System.Object> ColData {
			get { return this.Element.ColData; }
		}
		
		/// <summary>
		/// Gets the RowData 
		/// </summary>
		/// <value>
		/// The RowData
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.Dictionary<System.Int32, System.Object> RowData {
			get { return this.Element.RowData; }
		}
		
		/// <summary>
		/// Gets or sets the ColumnFilterInfo 
		/// </summary>
		/// <value>
		/// The ColumnFilterInfo
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.FilterInfo ColumnFilterInfo {
			get { return this.Element.ColumnFilterInfo; }
			set { this.Element.ColumnFilterInfo = value; }
		}
		
		/// <summary>
		/// Gets or sets the GroupField 
		/// </summary>
		/// <value>
		/// The GroupField
		/// </value>
		[DefaultValue(null)]
		public System.String GroupField {
			get { return this.Element.GroupField; }
			set { this.Element.GroupField = value; }
		}
		
		/// <summary>
		/// Gets or sets the ComboBoxList 
		/// </summary>
		/// <value>
		/// The ComboBoxList
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.Dictionary<System.String, System.String> ComboBoxList {
			get { return this.Element.ComboBoxList; }
			set { this.Element.ComboBoxList = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextButtonList 
		/// </summary>
		/// <value>
		/// The TextButtonList
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.List<System.String> TextButtonList {
			get { return this.Element.TextButtonList; }
			set { this.Element.TextButtonList = value; }
		}
		
		/// <summary>
		/// Gets or sets the SortableColumns 
		/// </summary>
		/// <value>
		/// The SortableColumns
		/// </value>
		[DefaultValue(true)]
		public System.Boolean SortableColumns {
			get { return this.Element.SortableColumns; }
			set { this.Element.SortableColumns = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowColumnSelection 
		/// </summary>
		/// <value>
		/// The AllowColumnSelection
		/// </value>
		[DefaultValue(false)]
		public System.Boolean AllowColumnSelection {
			get { return this.Element.AllowColumnSelection; }
			set { this.Element.AllowColumnSelection = value; }
		}
		
		/// <summary>
		/// Gets or sets the EnableLocking 
		/// </summary>
		/// <value>
		/// The EnableLocking
		/// </value>
		[DefaultValue(false)]
		public System.Boolean EnableLocking {
			get { return this.Element.EnableLocking; }
			set { this.Element.EnableLocking = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowAutoFilterColumnItem 
		/// </summary>
		/// <value>
		/// The ShowAutoFilterColumnItem
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ShowAutoFilterColumnItem {
			get { return this.Element.ShowAutoFilterColumnItem; }
			set { this.Element.ShowAutoFilterColumnItem = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectionColor 
		/// </summary>
		/// <value>
		/// The SelectionColor
		/// </value>
		public System.Drawing.Color SelectionColor {
			get { return this.Element.SelectionColor; }
			set { this.Element.SelectionColor = value; }
		}
		
		internal bool ShouldSerializeSelectionColor() { return this.SelectionColor!=System.Drawing.Color.Empty;}

			
		public string RowHeaderCornerIcon {
			get { return this.GetStringFromResource( this.Element.RowHeaderCornerIcon); }
			set { this.Element.RowHeaderCornerIcon = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the AllowTrackMouseOver 
		/// </summary>
		/// <value>
		/// The AllowTrackMouseOver
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AllowTrackMouseOver {
			get { return this.Element.AllowTrackMouseOver; }
			set { this.Element.AllowTrackMouseOver = value; }
		}
		
		/// <summary>
		/// Gets the HasCellkeyDownListeners 
		/// </summary>
		/// <value>
		/// The HasCellkeyDownListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasCellkeyDownListeners {
			get { return this.Element.HasCellkeyDownListeners; }
		}
		
		/// <summary>
		/// Gets or sets the ColumnHtmlContent 
		/// </summary>
		/// <value>
		/// The ColumnHtmlContent
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.Dictionary<System.Int32, System.String> ColumnHtmlContent {
			get { return this.Element.ColumnHtmlContent; }
			set { this.Element.ColumnHtmlContent = value; }
		}
		
		/// <summary>
		/// Gets or sets the EnableExport 
		/// </summary>
		/// <value>
		/// The EnableExport
		/// </value>
		[DefaultValue(false)]
		public System.Boolean EnableExport {
			get { return this.Element.EnableExport; }
			set { this.Element.EnableExport = value; }
		}
		
		/// <summary>
		/// Gets or sets the SearchPanel 
		/// </summary>
		/// <value>
		/// The SearchPanel
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean SearchPanel {
			get { return this.Element.SearchPanel; }
			set { this.Element.SearchPanel = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsDirty 
		/// </summary>
		/// <value>
		/// The IsDirty
		/// </value>
		[DefaultValue(false)]
		public System.Boolean IsDirty {
			get { return this.Element.IsDirty; }
			set { this.Element.IsDirty = value; }
		}
		
		/// <summary>
		/// Gets or sets the CellBorderStyle 
		/// </summary>
		/// <value>
		/// The CellBorderStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.GridCellBorderStyle))]
		public System.Web.VisualTree.Elements.GridCellBorderStyle CellBorderStyle {
			get { return this.Element.CellBorderStyle; }
			set { this.Element.CellBorderStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the ClientSelectionChanged 
		/// </summary>
		/// <value>
		/// The ClientSelectionChanged
		/// </value>
		[DefaultValue(null)]
		public System.String ClientSelectionChanged {
			get { return this.Element.ClientSelectionChanged; }
			set { this.Element.ClientSelectionChanged = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void OnDataSourceItemUpdated(System.Object item) {
			this.Element.OnDataSourceItemUpdated(item);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetCellStyle(System.Int32 rowIndex, System.Int32 columnIndex, System.String cssClass, System.Boolean appned) {
			this.Element.SetCellStyle(rowIndex, columnIndex, cssClass, appned);
		}

		/// <summary>
		/// 
		/// </summary>
		public void Sort(System.Web.VisualTree.Elements.GridColumn grdColumn, System.Web.VisualTree.SortOrder sortOrder) {
			this.Element.Sort(grdColumn, sortOrder);
		}

		/// <summary>
		/// 
		/// </summary>
		public void ScrollToRow(System.Int32 rowIndex) {
			this.Element.ScrollToRow(rowIndex);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetWidgetStyle(System.Int32 rowIndex, System.Int32 columnIndex, System.String dataMember, System.String cssClass, System.Boolean appned) {
			this.Element.SetWidgetStyle(rowIndex, columnIndex, dataMember, cssClass, appned);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetProtectCell(System.Int32 rowIndex, System.Int32 colIndex, System.Boolean protect) {
			this.Element.SetProtectCell(rowIndex, colIndex, protect);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SelectCell(System.Int32 row, System.Int32 col) {
			this.Element.SelectCell(row, col);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetRowVisibility(System.Int32 rowIndex, System.Boolean visible) {
			this.Element.SetRowVisibility(rowIndex, visible);
		}

		/// <summary>
		/// 
		/// </summary>
		public void EditCellByPosition(System.Int32 rowIndex, System.Int32 colIndex) {
			this.Element.EditCellByPosition(rowIndex, colIndex);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetCellBackgroundColor(System.Int32 rowIndex, System.Int32 colIndex, System.Drawing.Color color) {
			this.Element.SetCellBackgroundColor(rowIndex, colIndex, color);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetRowHeaderBackgroundColor(System.Int32 rowIndex, System.Drawing.Color color) {
			this.Element.SetRowHeaderBackgroundColor(rowIndex, color);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetRowHeaderBackgroundImage(System.Int32 rowIndex, System.Web.VisualTree.Elements.ResourceReference image) {
			this.Element.SetRowHeaderBackgroundImage(rowIndex, image);
		}

		/// <summary>
		/// 
		/// </summary>
		public void ContextMenuShow() {
			this.Element.ContextMenuShow();
		}

		/// <summary>
		/// 
		/// </summary>
		public void ClearFilter() {
			this.Element.ClearFilter();
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellDoubleClick = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellDoubleClickAction);
			if (_CellDoubleClick != null) { this.Element.CellDoubleClick += _CellDoubleClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellButtonClick = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellButtonClickAction);
			if (_CellButtonClick != null) { this.Element.CellButtonClick += _CellButtonClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellLeave = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellLeaveAction);
			if (_CellLeave != null) { this.Element.CellLeave += _CellLeave.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementSelectionChangeEventArgs> _SelectionChanged = Component.GetHandler<System.Web.VisualTree.Elements.GridElementSelectionChangeEventArgs>(this.Element, this.SelectionChangedAction);
			if (_SelectionChanged != null) { this.Element.SelectionChanged += _SelectionChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementSelectionChangeEventArgs> _BeforeSelectionChanged = Component.GetHandler<System.Web.VisualTree.Elements.GridElementSelectionChangeEventArgs>(this.Element, this.BeforeSelectionChangedAction);
			if (_BeforeSelectionChanged != null) { this.Element.BeforeSelectionChanged += _BeforeSelectionChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _ColumnHeaderMouseClick = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.ColumnHeaderMouseClickAction);
			if (_ColumnHeaderMouseClick != null) { this.Element.ColumnHeaderMouseClick += _ColumnHeaderMouseClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellContentClick = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellContentClickAction);
			if (_CellContentClick != null) { this.Element.CellContentClick += _CellContentClick.Invoke; }
			IVisualElementAction<System.EventArgs> _CellFormatting = Component.GetHandler<System.EventArgs>(this.Element, this.CellFormattingAction);
			if (_CellFormatting != null) { this.Element.CellFormatting += _CellFormatting.Invoke; }
			IVisualElementAction<System.EventArgs> _ColumnStateChanged = Component.GetHandler<System.EventArgs>(this.Element, this.ColumnStateChangedAction);
			if (_ColumnStateChanged != null) { this.Element.ColumnStateChanged += _ColumnStateChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _DataBindingComplete = Component.GetHandler<System.EventArgs>(this.Element, this.DataBindingCompleteAction);
			if (_DataBindingComplete != null) { this.Element.DataBindingComplete += _DataBindingComplete.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ValueChangedArgs<System.Int32>> _AddedColumn = Component.GetHandler<System.Web.VisualTree.Elements.ValueChangedArgs<System.Int32>>(this.Element, this.AddedColumnAction);
			if (_AddedColumn != null) { this.Element.AddedColumn += _AddedColumn.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CurrentCellChanged = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CurrentCellChangedAction);
			if (_CurrentCellChanged != null) { this.Element.CurrentCellChanged += _CurrentCellChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellBeginEdit = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellBeginEditAction);
			if (_CellBeginEdit != null) { this.Element.CellBeginEdit += _CellBeginEdit.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellClick = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellClickAction);
			if (_CellClick != null) { this.Element.CellClick += _CellClick.Invoke; }
			IVisualElementAction<System.EventArgs> _CellEndEdit = Component.GetHandler<System.EventArgs>(this.Element, this.CellEndEditAction);
			if (_CellEndEdit != null) { this.Element.CellEndEdit += _CellEndEdit.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _ColumnHeaderMouseRightClick = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.ColumnHeaderMouseRightClickAction);
			if (_ColumnHeaderMouseRightClick != null) { this.Element.ColumnHeaderMouseRightClick += _ColumnHeaderMouseRightClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridRowClickEventArgs> _RowClick = Component.GetHandler<System.Web.VisualTree.Elements.GridRowClickEventArgs>(this.Element, this.RowClickAction);
			if (_RowClick != null) { this.Element.RowClick += _RowClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridCellMouseEventArgs> _CellMouseDown = Component.GetHandler<System.Web.VisualTree.Elements.GridCellMouseEventArgs>(this.Element, this.CellMouseDownAction);
			if (_CellMouseDown != null) { this.Element.CellMouseDown += _CellMouseDown.Invoke; }
			IVisualElementAction<System.EventArgs> _MouseRightClick = Component.GetHandler<System.EventArgs>(this.Element, this.MouseRightClickAction);
			if (_MouseRightClick != null) { this.Element.MouseRightClick += _MouseRightClick.Invoke; }
			IVisualElementAction<System.ComponentModel.CancelEventArgs> _BeforeContextMenuShow = Component.GetHandler<System.ComponentModel.CancelEventArgs>(this.Element, this.BeforeContextMenuShowAction);
			if (_BeforeContextMenuShow != null) { this.Element.BeforeContextMenuShow += _BeforeContextMenuShow.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridColumnMoveEventArgs> _ColumnDisplayIndexChanged = Component.GetHandler<System.Web.VisualTree.Elements.GridColumnMoveEventArgs>(this.Element, this.ColumnDisplayIndexChangedAction);
			if (_ColumnDisplayIndexChanged != null) { this.Element.ColumnDisplayIndexChanged += _ColumnDisplayIndexChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellMouseUp = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellMouseUpAction);
			if (_CellMouseUp != null) { this.Element.CellMouseUp += _CellMouseUp.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _RowHeaderMouseRightClick = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.RowHeaderMouseRightClickAction);
			if (_RowHeaderMouseRightClick != null) { this.Element.RowHeaderMouseRightClick += _RowHeaderMouseRightClick.Invoke; }
			IVisualElementAction<System.EventArgs> _CancelEdit = Component.GetHandler<System.EventArgs>(this.Element, this.CancelEditAction);
			if (_CancelEdit != null) { this.Element.CancelEdit += _CancelEdit.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellkeyDown = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellkeyDownAction);
			if (_CellkeyDown != null) { this.Element.CellkeyDown += _CellkeyDown.Invoke; }
			IVisualElementAction<System.Web.VisualTree.GridComboBoxColumnEventArgs> _ComboCellSelectedIndexChanged = Component.GetHandler<System.Web.VisualTree.GridComboBoxColumnEventArgs>(this.Element, this.ComboCellSelectedIndexChangedAction);
			if (_ComboCellSelectedIndexChanged != null) { this.Element.ComboCellSelectedIndexChanged += _ComboCellSelectedIndexChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _ClientEvent = Component.GetHandler<System.EventArgs>(this.Element, this.ClientEventAction);
			if (_ClientEvent != null) { this.Element.ClientEvent += _ClientEvent.Invoke; }
			IVisualElementAction<System.Web.VisualTree.KeyDownEventArgs> _KeyDown = Component.GetHandler<System.Web.VisualTree.KeyDownEventArgs>(this.Element, this.KeyDownAction);
			if (_KeyDown != null) { this.Element.KeyDown += _KeyDown.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ButtonsColumnClickEventsArgs> _ButtonsColumnClick = Component.GetHandler<System.Web.VisualTree.Elements.ButtonsColumnClickEventsArgs>(this.Element, this.ButtonsColumnClickAction);
			if (_ButtonsColumnClick != null) { this.Element.ButtonsColumnClick += _ButtonsColumnClick.Invoke; }
			IVisualElementAction<System.EventArgs> _dataChanged = Component.GetHandler<System.EventArgs>(this.Element, this.dataChangedAction);
			if (_dataChanged != null) { this.Element.dataChanged += _dataChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridContextMenuCellEventArgs> _ContextMenuClick = Component.GetHandler<System.Web.VisualTree.Elements.GridContextMenuCellEventArgs>(this.Element, this.ContextMenuClickAction);
			if (_ContextMenuClick != null) { this.Element.ContextMenuClick += _ContextMenuClick.Invoke; }
        }

		private string _CellDoubleClickAction;

		public string CellDoubleClickAction
        {
            get
			{
				return _CellDoubleClickAction;
			}
			set
			{
				_CellDoubleClickAction = value;
			}            
        }

		private string _CellButtonClickAction;

		public string CellButtonClickAction
        {
            get
			{
				return _CellButtonClickAction;
			}
			set
			{
				_CellButtonClickAction = value;
			}            
        }

		private string _CellLeaveAction;

		public string CellLeaveAction
        {
            get
			{
				return _CellLeaveAction;
			}
			set
			{
				_CellLeaveAction = value;
			}            
        }

		private string _SelectionChangedAction;

		public string SelectionChangedAction
        {
            get
			{
				return _SelectionChangedAction;
			}
			set
			{
				_SelectionChangedAction = value;
			}            
        }

		private string _BeforeSelectionChangedAction;

		public string BeforeSelectionChangedAction
        {
            get
			{
				return _BeforeSelectionChangedAction;
			}
			set
			{
				_BeforeSelectionChangedAction = value;
			}            
        }

		private string _ColumnHeaderMouseClickAction;

		public string ColumnHeaderMouseClickAction
        {
            get
			{
				return _ColumnHeaderMouseClickAction;
			}
			set
			{
				_ColumnHeaderMouseClickAction = value;
			}            
        }

		private string _CellContentClickAction;

		public string CellContentClickAction
        {
            get
			{
				return _CellContentClickAction;
			}
			set
			{
				_CellContentClickAction = value;
			}            
        }

		private string _CellFormattingAction;

		public string CellFormattingAction
        {
            get
			{
				return _CellFormattingAction;
			}
			set
			{
				_CellFormattingAction = value;
			}            
        }

		private string _ColumnStateChangedAction;

		public string ColumnStateChangedAction
        {
            get
			{
				return _ColumnStateChangedAction;
			}
			set
			{
				_ColumnStateChangedAction = value;
			}            
        }

		private string _DataBindingCompleteAction;

		public string DataBindingCompleteAction
        {
            get
			{
				return _DataBindingCompleteAction;
			}
			set
			{
				_DataBindingCompleteAction = value;
			}            
        }

		private string _AddedColumnAction;

		public string AddedColumnAction
        {
            get
			{
				return _AddedColumnAction;
			}
			set
			{
				_AddedColumnAction = value;
			}            
        }

		private string _CurrentCellChangedAction;

		public string CurrentCellChangedAction
        {
            get
			{
				return _CurrentCellChangedAction;
			}
			set
			{
				_CurrentCellChangedAction = value;
			}            
        }

		private string _CellBeginEditAction;

		public string CellBeginEditAction
        {
            get
			{
				return _CellBeginEditAction;
			}
			set
			{
				_CellBeginEditAction = value;
			}            
        }

		private string _CellClickAction;

		public string CellClickAction
        {
            get
			{
				return _CellClickAction;
			}
			set
			{
				_CellClickAction = value;
			}            
        }

		private string _CellEndEditAction;

		public string CellEndEditAction
        {
            get
			{
				return _CellEndEditAction;
			}
			set
			{
				_CellEndEditAction = value;
			}            
        }

		private string _ColumnHeaderMouseRightClickAction;

		public string ColumnHeaderMouseRightClickAction
        {
            get
			{
				return _ColumnHeaderMouseRightClickAction;
			}
			set
			{
				_ColumnHeaderMouseRightClickAction = value;
			}            
        }

		private string _RowClickAction;

		public string RowClickAction
        {
            get
			{
				return _RowClickAction;
			}
			set
			{
				_RowClickAction = value;
			}            
        }

		private string _CellMouseDownAction;

		public string CellMouseDownAction
        {
            get
			{
				return _CellMouseDownAction;
			}
			set
			{
				_CellMouseDownAction = value;
			}            
        }

		private string _MouseRightClickAction;

		public string MouseRightClickAction
        {
            get
			{
				return _MouseRightClickAction;
			}
			set
			{
				_MouseRightClickAction = value;
			}            
        }

		private string _BeforeContextMenuShowAction;

		public string BeforeContextMenuShowAction
        {
            get
			{
				return _BeforeContextMenuShowAction;
			}
			set
			{
				_BeforeContextMenuShowAction = value;
			}            
        }

		private string _ColumnDisplayIndexChangedAction;

		public string ColumnDisplayIndexChangedAction
        {
            get
			{
				return _ColumnDisplayIndexChangedAction;
			}
			set
			{
				_ColumnDisplayIndexChangedAction = value;
			}            
        }

		private string _CellMouseUpAction;

		public string CellMouseUpAction
        {
            get
			{
				return _CellMouseUpAction;
			}
			set
			{
				_CellMouseUpAction = value;
			}            
        }

		private string _RowHeaderMouseRightClickAction;

		public string RowHeaderMouseRightClickAction
        {
            get
			{
				return _RowHeaderMouseRightClickAction;
			}
			set
			{
				_RowHeaderMouseRightClickAction = value;
			}            
        }

		private string _CancelEditAction;

		public string CancelEditAction
        {
            get
			{
				return _CancelEditAction;
			}
			set
			{
				_CancelEditAction = value;
			}            
        }

		private string _CellkeyDownAction;

		public string CellkeyDownAction
        {
            get
			{
				return _CellkeyDownAction;
			}
			set
			{
				_CellkeyDownAction = value;
			}            
        }

		private string _ComboCellSelectedIndexChangedAction;

		public string ComboCellSelectedIndexChangedAction
        {
            get
			{
				return _ComboCellSelectedIndexChangedAction;
			}
			set
			{
				_ComboCellSelectedIndexChangedAction = value;
			}            
        }

		private string _ClientEventAction;

		public string ClientEventAction
        {
            get
			{
				return _ClientEventAction;
			}
			set
			{
				_ClientEventAction = value;
			}            
        }

		private string _KeyDownAction;

		public string KeyDownAction
        {
            get
			{
				return _KeyDownAction;
			}
			set
			{
				_KeyDownAction = value;
			}            
        }

		private string _ButtonsColumnClickAction;

		public string ButtonsColumnClickAction
        {
            get
			{
				return _ButtonsColumnClickAction;
			}
			set
			{
				_ButtonsColumnClickAction = value;
			}            
        }

		private string _dataChangedAction;

		public string dataChangedAction
        {
            get
			{
				return _dataChangedAction;
			}
			set
			{
				_dataChangedAction = value;
			}            
        }

		private string _ContextMenuClickAction;

		public string ContextMenuClickAction
        {
            get
			{
				return _ContextMenuClickAction;
			}
			set
			{
				_ContextMenuClickAction = value;
			}            
        }


	}  

	/// <summary>
    /// WidgetGrid
    /// </summary>
	public class WidgetGrid  : Grid   
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetGrid"/> class.
        /// </summary>
		public WidgetGrid()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetGrid"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal WidgetGrid(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.WidgetGridElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.WidgetGridElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.WidgetGridElement();
        }

        private string _WidgetControlSpecialKeyAction;

        public string WidgetControlSpecialKeyAction
        {
            get
            {
                return _WidgetControlSpecialKeyAction;
            }
            set
            {
                _WidgetControlSpecialKeyAction = value;
            }
        }

        /// <summary>
        /// Gets or sets the WidgetTemplate 
        /// </summary>
        /// <value>
        /// The WidgetTemplate
        /// </value>
        [DefaultValue(null)]
		public System.String WidgetTemplate {
			get { return this.Element.WidgetTemplate; }
			set { this.Element.WidgetTemplate = value; }
		}
		
		/// <summary>
		/// Gets or sets the WidgetTemplateModel 
		/// </summary>
		/// <value>
		/// The WidgetTemplateModel
		/// </value>
		[DefaultValue(null)]
		public System.String WidgetTemplateModel {
			get { return this.Element.WidgetTemplateModel; }
			set { this.Element.WidgetTemplateModel = value; }
		}
		
		/// <summary>
		/// Gets or sets the WidgetTemplateWidth 
		/// </summary>
		/// <value>
		/// The WidgetTemplateWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 WidgetTemplateWidth {
			get { return this.Element.WidgetTemplateWidth; }
			set { this.Element.WidgetTemplateWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the WidgetTemplateItemsCount 
		/// </summary>
		/// <value>
		/// The WidgetTemplateItemsCount
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 WidgetTemplateItemsCount {
			get { return this.Element.WidgetTemplateItemsCount; }
			set { this.Element.WidgetTemplateItemsCount = value; }
		}
		
		/// <summary>
		/// Gets or sets the WidgetTemplateDataMembers 
		/// </summary>
		/// <value>
		/// The WidgetTemplateDataMembers
		/// </value>
		[DefaultValue(null)]
		public System.String WidgetTemplateDataMembers {
			get { return this.Element.WidgetTemplateDataMembers; }
			set { this.Element.WidgetTemplateDataMembers = value; }
		}
		
		/// <summary>
		/// Gets or sets the WidgetTemplateDataAttachMethod 
		/// </summary>
		/// <value>
		/// The WidgetTemplateDataAttachMethod
		/// </value>
		[DefaultValue(null)]
		public System.String WidgetTemplateDataAttachMethod {
			get { return this.Element.WidgetTemplateDataAttachMethod; }
			set { this.Element.WidgetTemplateDataAttachMethod = value; }
		}
		
		/// <summary>
		/// Gets the HasCellkeyDownListeners 
		/// </summary>
		/// <value>
		/// The HasCellkeyDownListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasCellkeyDownListeners {
			get { return this.Element.HasCellkeyDownListeners; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void SetFocus(System.Int32 rowindex, System.String datamember) {
			this.Element.SetFocus(rowindex, datamember);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetEnabled(System.Int32 rowindex, System.String datamember, System.Boolean state) {
			this.Element.SetEnabled(rowindex, datamember, state);
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetBackColor(System.Int32 rowindex, System.String datamember, System.Drawing.Color color) {
			this.Element.SetBackColor(rowindex, datamember, color);
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementSelectionChangeEventArgs> _SelectionChanged = Component.GetHandler<System.Web.VisualTree.Elements.GridElementSelectionChangeEventArgs>(this.Element, this.SelectionChangedAction);
			if (_SelectionChanged != null) { this.Element.SelectionChanged += _SelectionChanged.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridCellMouseEventArgs> _CellMouseDown = Component.GetHandler<System.Web.VisualTree.Elements.GridCellMouseEventArgs>(this.Element, this.CellMouseDownAction);
			if (_CellMouseDown != null) { this.Element.CellMouseDown += _CellMouseDown.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _CellkeyDown = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.CellkeyDownAction);
			if (_CellkeyDown != null) { this.Element.CellkeyDown += _CellkeyDown.Invoke; }
            IVisualElementAction<System.Web.VisualTree.Elements.GridElementCellEventArgs> _WidgetControlSpecialKey = Component.GetHandler<System.Web.VisualTree.Elements.GridElementCellEventArgs>(this.Element, this.WidgetControlSpecialKeyAction);
            if (_WidgetControlSpecialKey != null) { this.Element.WidgetControlSpecialKey += _WidgetControlSpecialKey.Invoke; }
        }

		private string _SelectionChangedAction;

		public string SelectionChangedAction
        {
            get
			{
				return _SelectionChangedAction;
			}
			set
			{
				_SelectionChangedAction = value;
			}            
        }

		private string _CellMouseDownAction;

		public string CellMouseDownAction
        {
            get
			{
				return _CellMouseDownAction;
			}
			set
			{
				_CellMouseDownAction = value;
			}            
        }

		private string _CellkeyDownAction;

		public string CellkeyDownAction
        {
            get
			{
				return _CellkeyDownAction;
			}
			set
			{
				_CellkeyDownAction = value;
			}            
        }


	}  

	/// <summary>
    /// GridItem
    /// </summary>
	public class GridItem  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridItem"/> class.
        /// </summary>
		public GridItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridItemElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridItemElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridItemElement();
        }
 
		/// <summary>
		/// Gets the GridElement 
		/// </summary>
		/// <value>
		/// The GridElement
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.GridElement GridElement {
			get { return this.Element.GridElement; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridBand
    /// </summary>
	public class GridBand  : GridItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridBand"/> class.
        /// </summary>
		public GridBand()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridBand"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridBand(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridBand Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridBand;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridBand();
        }
 
		/// <summary>
		/// Gets or sets the ReadOnly 
		/// </summary>
		/// <value>
		/// The ReadOnly
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ReadOnly {
			get { return this.Element.ReadOnly; }
			set { this.Element.ReadOnly = value; }
		}
		
		/// <summary>
		/// Gets or sets the Selected 
		/// </summary>
		/// <value>
		/// The Selected
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Selected {
			get { return this.Element.Selected; }
			set { this.Element.Selected = value; }
		}
		
		/// <summary>
		/// Gets or sets the Resizable 
		/// </summary>
		/// <value>
		/// The Resizable
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.GridTristate))]
		public System.Web.VisualTree.Elements.GridTristate Resizable {
			get { return this.Element.Resizable; }
			set { this.Element.Resizable = value; }
		}
		
		/// <summary>
		/// Gets the InheritedStyle 
		/// </summary>
		/// <value>
		/// The InheritedStyle
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.GridCellStyle InheritedStyle {
			get { return this.Element.InheritedStyle; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridColumn
    /// </summary>
	public class GridColumn  : GridBand   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
		public GridColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridColumn();
        }
 
		/// <summary>
		/// Gets or sets the CellTemplate 
		/// </summary>
		/// <value>
		/// The CellTemplate
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.GridCellElement CellTemplate {
			get { return this.Element.CellTemplate; }
			set { this.Element.CellTemplate = value; }
		}
		
		/// <summary>
		/// Gets or sets the ColumnFilter 
		/// </summary>
		/// <value>
		/// The ColumnFilter
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ColumnFilter {
			get { return this.Element.ColumnFilter; }
			set { this.Element.ColumnFilter = value; }
		}
		
		/// <summary>
		/// Format cell text .
/// </summary>
		[DefaultValue(null)]
		public System.String Format {
			get { return this.Element.Format; }
			set { this.Element.Format = value; }
		}
		
		/// <summary>
		/// Gets or sets the Content 
		/// </summary>
		/// <value>
		/// The Content
		/// </value>
		[DefaultValue(null)]
		public System.Object Content {
			get { return this.Element.Content; }
			set { this.Element.Content = value; }
		}
		
		/// <summary>
		/// Gets or sets the HeaderText 
		/// </summary>
		/// <value>
		/// The HeaderText
		/// </value>
		[DefaultValue(null)]
		public System.String HeaderText {
			get { return this.Element.HeaderText; }
			set { this.Element.HeaderText = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataType 
		/// </summary>
		/// <value>
		/// The DataType
		/// </value>
		[DefaultValue(null)]
		public System.Type DataType {
			get { return this.Element.DataType; }
			set { this.Element.DataType = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataMember 
		/// </summary>
		/// <value>
		/// The DataMember
		/// </value>
		[DefaultValue(null)]
		public System.String DataMember {
			get { return this.Element.DataMember; }
			set { this.Element.DataMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the SortMode 
		/// </summary>
		/// <value>
		/// The SortMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.GridColumnSortMode), "Automatic")]
		public System.Web.VisualTree.Elements.GridColumnSortMode SortMode {
			get { return this.Element.SortMode; }
			set { this.Element.SortMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the MinimumWidth 
		/// </summary>
		/// <value>
		/// The MinimumWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 MinimumWidth {
			get { return this.Element.MinimumWidth; }
			set { this.Element.MinimumWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the FillWeight 
		/// </summary>
		/// <value>
		/// The FillWeight
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single FillWeight {
			get { return this.Element.FillWeight; }
			set { this.Element.FillWeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoSizeMode 
		/// </summary>
		/// <value>
		/// The AutoSizeMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.GridAutoSizeColumnMode))]
		public System.Web.VisualTree.Elements.GridAutoSizeColumnMode AutoSizeMode {
			get { return this.Element.AutoSizeMode; }
			set { this.Element.AutoSizeMode = value; }
		}
		
		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
		/// <summary>
		/// Gets or sets the ReadOnly 
		/// </summary>
		/// <value>
		/// The ReadOnly
		/// </value>
		[DefaultValue(false)]
		public System.Boolean ReadOnly {
			get { return this.Element.ReadOnly; }
			set { this.Element.ReadOnly = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxInputLength 
		/// </summary>
		/// <value>
		/// The MaxInputLength
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 MaxInputLength {
			get { return this.Element.MaxInputLength; }
			set { this.Element.MaxInputLength = value; }
		}
		
		/// <summary>
		/// Gets or sets the MenuDisabled 
		/// </summary>
		/// <value>
		/// The MenuDisabled
		/// </value>
		[DefaultValue(true)]
		public System.Boolean MenuDisabled {
			get { return this.Element.MenuDisabled; }
			set { this.Element.MenuDisabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the DisplayIndex 
		/// </summary>
		/// <value>
		/// The DisplayIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 DisplayIndex {
			get { return this.Element.DisplayIndex; }
			set { this.Element.DisplayIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the SortOrder 
		/// </summary>
		/// <value>
		/// The SortOrder
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.SortOrder))]
		public System.Web.VisualTree.SortOrder SortOrder {
			get { return this.Element.SortOrder; }
			set { this.Element.SortOrder = value; }
		}
		
			
		public string Icon {
			get { return this.GetStringFromResource( this.Element.Icon); }
			set { this.Element.Icon = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the MaxWidth 
		/// </summary>
		/// <value>
		/// The MaxWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 MaxWidth {
			get { return this.Element.MaxWidth; }
			set { this.Element.MaxWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the SortType 
		/// </summary>
		/// <value>
		/// The SortType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ColumnSortMode))]
		public System.Web.VisualTree.ColumnSortMode SortType {
			get { return this.Element.SortType; }
			set { this.Element.SortType = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridHtmlContentColumn
    /// </summary>
	public class GridHtmlContentColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridHtmlContentColumn"/> class.
        /// </summary>
		public GridHtmlContentColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridHtmlContentColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridHtmlContentColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridHtmlContentColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridHtmlContentColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridHtmlContentColumn();
        }
 
		/// <summary>
		/// Gets or sets the HtmlContent 
		/// </summary>
		/// <value>
		/// The HtmlContent
		/// </value>
		[DefaultValue(null)]
		public System.String HtmlContent {
			get { return this.Element.HtmlContent; }
			set { this.Element.HtmlContent = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataMember 
		/// </summary>
		/// <value>
		/// The DataMember
		/// </value>
		[DefaultValue(null)]
		public System.String DataMember {
			get { return this.Element.DataMember; }
			set { this.Element.DataMember = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ListControl
    /// </summary>
	[ParseChildren(true)]
	public abstract class ListControl  : Input   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListControl"/> class.
        /// </summary>
		public ListControl()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ListControl"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ListControl(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ListControlElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ListControlElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public ListItemCollection Items {
			get { return new ListItemCollection(this, this.Element.Items); }
		}
		
		/// <summary>
		/// Gets or sets the DisplayMember 
		/// </summary>
		/// <value>
		/// The DisplayMember
		/// </value>
		[DefaultValue(null)]
		public System.String DisplayMember {
			get { return this.Element.DisplayMember; }
			set { this.Element.DisplayMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the ValueMember 
		/// </summary>
		/// <value>
		/// The ValueMember
		/// </value>
		[DefaultValue(null)]
		public System.String ValueMember {
			get { return this.Element.ValueMember; }
			set { this.Element.ValueMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the FormattingEnabled 
		/// </summary>
		/// <value>
		/// The FormattingEnabled
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean FormattingEnabled {
			get { return this.Element.FormattingEnabled; }
			set { this.Element.FormattingEnabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the ItemData 
		/// </summary>
		/// <value>
		/// The ItemData
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ItemData {
			get { return this.Element.ItemData; }
			set { this.Element.ItemData = value; }
		}
		
		/// <summary>
		/// Gets the Count 
		/// </summary>
		/// <value>
		/// The Count
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Count {
			get { return this.Element.Count; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedIndex 
		/// </summary>
		/// <value>
		/// The SelectedIndex
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 SelectedIndex {
			get { return this.Element.SelectedIndex; }
			set { this.Element.SelectedIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedValue 
		/// </summary>
		/// <value>
		/// The SelectedValue
		/// </value>
		[DefaultValue(null)]
		public System.Object SelectedValue {
			get { return this.Element.SelectedValue; }
			set { this.Element.SelectedValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedText 
		/// </summary>
		/// <value>
		/// The SelectedText
		/// </value>
		[DefaultValue(null)]
		public System.Object SelectedText {
			get { return this.Element.SelectedText; }
			set { this.Element.SelectedText = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedItem 
		/// </summary>
		/// <value>
		/// The SelectedItem
		/// </value>
		[DefaultValue(null)]
		public System.Object SelectedItem {
			get { return this.Element.SelectedItem; }
			set { this.Element.SelectedItem = value; }
		}
		
		/// <summary>
		/// Gets or sets the Sorted 
		/// </summary>
		/// <value>
		/// The Sorted
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Sorted {
			get { return this.Element.Sorted; }
			set { this.Element.Sorted = value; }
		}
		
		/// <summary>
		/// Gets or sets the MultiColumn 
		/// </summary>
		/// <value>
		/// The MultiColumn
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean MultiColumn {
			get { return this.Element.MultiColumn; }
			set { this.Element.MultiColumn = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void ClearItems() {
			this.Element.ClearItems();
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _SelectedIndexChanged = Component.GetHandler<System.EventArgs>(this.Element, this.SelectedIndexChangedAction);
			if (_SelectedIndexChanged != null) { this.Element.SelectedIndexChanged += _SelectedIndexChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _SelectedValueChanged = Component.GetHandler<System.EventArgs>(this.Element, this.SelectedValueChangedAction);
			if (_SelectedValueChanged != null) { this.Element.SelectedValueChanged += _SelectedValueChanged.Invoke; }
        }

		private string _SelectedIndexChangedAction;

		public string SelectedIndexChangedAction
        {
            get
			{
				return _SelectedIndexChangedAction;
			}
			set
			{
				_SelectedIndexChangedAction = value;
			}            
        }

		private string _SelectedValueChangedAction;

		public string SelectedValueChangedAction
        {
            get
			{
				return _SelectedValueChangedAction;
			}
			set
			{
				_SelectedValueChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// ComboBox
    /// </summary>
	public class ComboBox  : ListControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComboBox"/> class.
        /// </summary>
		public ComboBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ComboBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ComboBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ComboBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ComboBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the MatchComboboxWidth 
		/// </summary>
		/// <value>
		/// The MatchComboboxWidth
		/// </value>
		[DefaultValue(false)]
		public System.Boolean MatchComboboxWidth {
			get { return this.Element.MatchComboboxWidth; }
			set { this.Element.MatchComboboxWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the Locked 
		/// </summary>
		/// <value>
		/// The Locked
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Locked {
			get { return this.Element.Locked; }
			set { this.Element.Locked = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue("")]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets the Columns 
		/// </summary>
		/// <value>
		/// The Columns
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ComboboxColumnCollection Columns {
			get { return this.Element.Columns; }
		}
		
		/// <summary>
		/// Gets or sets the ShowFilter 
		/// </summary>
		/// <value>
		/// The ShowFilter
		/// </value>
		[DefaultValue(false)]
		public System.Boolean ShowFilter {
			get { return this.Element.ShowFilter; }
			set { this.Element.ShowFilter = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedIndex 
		/// </summary>
		/// <value>
		/// The SelectedIndex
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 SelectedIndex {
			get { return this.Element.SelectedIndex; }
			set { this.Element.SelectedIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedItem 
		/// </summary>
		/// <value>
		/// The SelectedItem
		/// </value>
		[DefaultValue(null)]
		public System.Object SelectedItem {
			get { return this.Element.SelectedItem; }
			set { this.Element.SelectedItem = value; }
		}
		
		/// <summary>
		/// Gets or sets the DropDownWidth 
		/// </summary>
		/// <value>
		/// The DropDownWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 DropDownWidth {
			get { return this.Element.DropDownWidth; }
			set { this.Element.DropDownWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the DroppedDown 
		/// </summary>
		/// <value>
		/// The DroppedDown
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean DroppedDown {
			get { return this.Element.DroppedDown; }
			set { this.Element.DroppedDown = value; }
		}
		
		/// <summary>
		/// Gets or sets the DropDownHeight 
		/// </summary>
		/// <value>
		/// The DropDownHeight
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 DropDownHeight {
			get { return this.Element.DropDownHeight; }
			set { this.Element.DropDownHeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the DropDownStyle 
		/// </summary>
		/// <value>
		/// The DropDownStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.ComboBoxStyle), "DropDown")]
		public System.Web.VisualTree.Elements.ComboBoxStyle DropDownStyle {
			get { return this.Element.DropDownStyle; }
			set { this.Element.DropDownStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectionStart 
		/// </summary>
		/// <value>
		/// The SelectionStart
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectionStart {
			get { return this.Element.SelectionStart; }
			set { this.Element.SelectionStart = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectionLength 
		/// </summary>
		/// <value>
		/// The SelectionLength
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectionLength {
			get { return this.Element.SelectionLength; }
			set { this.Element.SelectionLength = value; }
		}
		
		/// <summary>
		/// Gets the AutoCompleteCustomSource 
		/// </summary>
		/// <value>
		/// The AutoCompleteCustomSource
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.AutoCompleteStringCollection AutoCompleteCustomSource {
			get { return this.Element.AutoCompleteCustomSource; }
		}
		
		/// <summary>
		/// Gets or sets the SelectTextOnFocus 
		/// </summary>
		/// <value>
		/// The SelectTextOnFocus
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean SelectTextOnFocus {
			get { return this.Element.SelectTextOnFocus; }
			set { this.Element.SelectTextOnFocus = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowExpand 
		/// </summary>
		/// <value>
		/// The AllowExpand
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AllowExpand {
			get { return this.Element.AllowExpand; }
			set { this.Element.AllowExpand = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedText 
		/// </summary>
		/// <value>
		/// The SelectedText
		/// </value>
		[DefaultValue(null)]
		public System.Object SelectedText {
			get { return this.Element.SelectedText; }
			set { this.Element.SelectedText = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedValue 
		/// </summary>
		/// <value>
		/// The SelectedValue
		/// </value>
		[DefaultValue(null)]
		public System.Object SelectedValue {
			get { return this.Element.SelectedValue; }
			set { this.Element.SelectedValue = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _DropDownClosed = Component.GetHandler<System.EventArgs>(this.Element, this.DropDownClosedAction);
			if (_DropDownClosed != null) { this.Element.DropDownClosed += _DropDownClosed.Invoke; }
			IVisualElementAction<System.EventArgs> _SelectedIndexChanged = Component.GetHandler<System.EventArgs>(this.Element, this.SelectedIndexChangedAction);
			if (_SelectedIndexChanged != null) { this.Element.SelectedIndexChanged += _SelectedIndexChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _SelectionChangeCommitted = Component.GetHandler<System.EventArgs>(this.Element, this.SelectionChangeCommittedAction);
			if (_SelectionChangeCommitted != null) { this.Element.SelectionChangeCommitted += _SelectionChangeCommitted.Invoke; }
			IVisualElementAction<System.EventArgs> _DropDown = Component.GetHandler<System.EventArgs>(this.Element, this.DropDownAction);
			if (_DropDown != null) { this.Element.DropDown += _DropDown.Invoke; }
			IVisualElementAction<System.EventArgs> _BeforeSelect = Component.GetHandler<System.EventArgs>(this.Element, this.BeforeSelectAction);
			if (_BeforeSelect != null) { this.Element.BeforeSelect += _BeforeSelect.Invoke; }
        }

		private string _DropDownClosedAction;

		public string DropDownClosedAction
        {
            get
			{
				return _DropDownClosedAction;
			}
			set
			{
				_DropDownClosedAction = value;
			}            
        }

		private string _SelectedIndexChangedAction;

		public string SelectedIndexChangedAction
        {
            get
			{
				return _SelectedIndexChangedAction;
			}
			set
			{
				_SelectedIndexChangedAction = value;
			}            
        }

		private string _SelectionChangeCommittedAction;

		public string SelectionChangeCommittedAction
        {
            get
			{
				return _SelectionChangeCommittedAction;
			}
			set
			{
				_SelectionChangeCommittedAction = value;
			}            
        }

		private string _DropDownAction;

		public string DropDownAction
        {
            get
			{
				return _DropDownAction;
			}
			set
			{
				_DropDownAction = value;
			}            
        }

		private string _BeforeSelectAction;

		public string BeforeSelectAction
        {
            get
			{
				return _BeforeSelectAction;
			}
			set
			{
				_BeforeSelectAction = value;
			}            
        }


	}  

	/// <summary>
    /// GridMultiComboBox
    /// </summary>
	public class GridMultiComboBox  : ComboBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridMultiComboBox"/> class.
        /// </summary>
		public GridMultiComboBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridMultiComboBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridMultiComboBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridMultiComboBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridMultiComboBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridMultiComboBoxElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// WidgetColumnMultiComboBox
    /// </summary>
	public class WidgetColumnMultiComboBox  : GridMultiComboBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetColumnMultiComboBox"/> class.
        /// </summary>
		public WidgetColumnMultiComboBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetColumnMultiComboBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal WidgetColumnMultiComboBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.WidgetColumnMultiComboBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.WidgetColumnMultiComboBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.WidgetColumnMultiComboBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the HasItemChanged 
		/// </summary>
		/// <value>
		/// The HasItemChanged
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasItemChanged {
			get { return this.Element.HasItemChanged; }
			set { this.Element.HasItemChanged = value; }
		}
		
		/// <summary>
		/// Gets the HasWidgetControlItemChangedListeners 
		/// </summary>
		/// <value>
		/// The HasWidgetControlItemChangedListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWidgetControlItemChangedListeners {
			get { return this.Element.HasWidgetControlItemChangedListeners; }
		}
		
		/// <summary>
		/// Gets the HasWidgetControlEditChangedListeners 
		/// </summary>
		/// <value>
		/// The HasWidgetControlEditChangedListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWidgetControlEditChangedListeners {
			get { return this.Element.HasWidgetControlEditChangedListeners; }
		}
		
		/// <summary>
		/// Gets or sets the HasEditChange 
		/// </summary>
		/// <value>
		/// The HasEditChange
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasEditChange {
			get { return this.Element.HasEditChange; }
			set { this.Element.HasEditChange = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// WidgetColumn
    /// </summary>
	public class WidgetColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetColumn"/> class.
        /// </summary>
		public WidgetColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal WidgetColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.WidgetColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.WidgetColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.WidgetColumn();
        }
 
		/// <summary>
		/// Gets or sets the DataMembers 
		/// </summary>
		/// <value>
		/// The DataMembers
		/// </value>
		[DefaultValue(null)]
		public System.String DataMembers {
			get { return this.Element.DataMembers; }
			set { this.Element.DataMembers = value; }
		}
		
		/// <summary>
		/// Gets or sets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ControlElementCollection Items {
			get { return this.Element.Items; }
			set { this.Element.Items = value; }
		}
		
		/// <summary>
		/// Gets the IsContainer 
		/// </summary>
		/// <value>
		/// The IsContainer
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsContainer {
			get { return this.Element.IsContainer; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridTextButtonColumn
    /// </summary>
	public class GridTextButtonColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn"/> class.
        /// </summary>
		public GridTextButtonColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridTextButtonColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridTextButtonColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridTextButtonColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridTextButtonColumn();
        }
 
		/// <summary>
		/// Gets or sets the ButtonText 
		/// </summary>
		/// <value>
		/// The ButtonText
		/// </value>
		[DefaultValue("")]
		public System.String ButtonText {
			get { return this.Element.ButtonText; }
			set { this.Element.ButtonText = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextReadOnly 
		/// </summary>
		/// <value>
		/// The TextReadOnly
		/// </value>
		[DefaultValue("")]
		public System.Boolean TextReadOnly {
			get { return this.Element.TextReadOnly; }
			set { this.Element.TextReadOnly = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridComboBoxColumn
    /// </summary>
	public class GridComboBoxColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridComboBoxColumn"/> class.
        /// </summary>
		public GridComboBoxColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridComboBoxColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridComboBoxColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridComboBoxColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridComboBoxColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridComboBoxColumn();
        }
 
		/// <summary>
		/// Gets or sets the DisplayMember 
		/// </summary>
		/// <value>
		/// The DisplayMember
		/// </value>
		[DefaultValue(null)]
		public System.String DisplayMember {
			get { return this.Element.DisplayMember; }
			set { this.Element.DisplayMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the ValueMember 
		/// </summary>
		/// <value>
		/// The ValueMember
		/// </value>
		[DefaultValue(null)]
		public System.String ValueMember {
			get { return this.Element.ValueMember; }
			set { this.Element.ValueMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoCompleteMode 
		/// </summary>
		/// <value>
		/// The AutoCompleteMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.AutoCompleteMode), "None")]
		public System.Web.VisualTree.AutoCompleteMode AutoCompleteMode {
			get { return this.Element.AutoCompleteMode; }
			set { this.Element.AutoCompleteMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the ForceSelection 
		/// </summary>
		/// <value>
		/// The ForceSelection
		/// </value>
		[DefaultValue(false)]
		public System.Boolean ForceSelection {
			get { return this.Element.ForceSelection; }
			set { this.Element.ForceSelection = value; }
		}
		
		/// <summary>
		/// Gets or sets the ExpandOnFocus 
		/// </summary>
		/// <value>
		/// The ExpandOnFocus
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ExpandOnFocus {
			get { return this.Element.ExpandOnFocus; }
			set { this.Element.ExpandOnFocus = value; }
		}
		
		/// <summary>
		/// Gets the DataType 
		/// </summary>
		/// <value>
		/// The DataType
		/// </value>
		[DefaultValue(null)]
		public System.Type DataType {
			get { return this.Element.DataType; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridCell
    /// </summary>
	public abstract class GridCell  : GridItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridCell"/> class.
        /// </summary>
		public GridCell()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCell"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridCell(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridCellElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridCellElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		[DefaultValue(null)]
		public System.Object Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedIndex 
		/// </summary>
		/// <value>
		/// The SelectedIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedIndex {
			get { return this.Element.SelectedIndex; }
			set { this.Element.SelectedIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue("")]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the Selected 
		/// </summary>
		/// <value>
		/// The Selected
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Selected {
			get { return this.Element.Selected; }
			set { this.Element.Selected = value; }
		}
		
		/// <summary>
		/// Gets or sets the Style 
		/// </summary>
		/// <value>
		/// The Style
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.GridCellStyle Style {
			get { return this.Element.Style; }
			set { this.Element.Style = value; }
		}
		
		/// <summary>
		/// Gets or sets the RowIndex 
		/// </summary>
		/// <value>
		/// The RowIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 RowIndex {
			get { return this.Element.RowIndex; }
			set { this.Element.RowIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the ColumnIndex 
		/// </summary>
		/// <value>
		/// The ColumnIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ColumnIndex {
			get { return this.Element.ColumnIndex; }
			set { this.Element.ColumnIndex = value; }
		}
		
		/// <summary>
		/// Gets the FormattedValue 
		/// </summary>
		/// <value>
		/// The FormattedValue
		/// </value>
		[DefaultValue(null)]
		public System.Object FormattedValue {
			get { return this.Element.FormattedValue; }
		}
		
		/// <summary>
		/// Gets the FieldType 
		/// </summary>
		/// <value>
		/// The FieldType
		/// </value>
		[DefaultValue(null)]
		public System.Type FieldType {
			get { return this.Element.FieldType; }
		}
		
		/// <summary>
		/// Gets or sets the Button1Visible 
		/// </summary>
		/// <value>
		/// The Button1Visible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Button1Visible {
			get { return this.Element.Button1Visible; }
			set { this.Element.Button1Visible = value; }
		}
		
		/// <summary>
		/// Gets or sets the Button2Visible 
		/// </summary>
		/// <value>
		/// The Button2Visible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Button2Visible {
			get { return this.Element.Button2Visible; }
			set { this.Element.Button2Visible = value; }
		}
		
		/// <summary>
		/// Gets or sets the Protect 
		/// </summary>
		/// <value>
		/// The Protect
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Protect {
			get { return this.Element.Protect; }
			set { this.Element.Protect = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridContentCell
    /// </summary>
	public class GridContentCell  : GridCell   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridContentCell"/> class.
        /// </summary>
		public GridContentCell()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridContentCell"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridContentCell(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridContentCell Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridContentCell;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridContentCell();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridCustomColumn
    /// </summary>
	public class GridCustomColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridCustomColumn"/> class.
        /// </summary>
		public GridCustomColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCustomColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridCustomColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridCustomColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridCustomColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridCustomColumn();
        }
 
		/// <summary>
		/// Gets or sets the Renderer 
		/// </summary>
		/// <value>
		/// The Renderer
		/// </value>
		[DefaultValue(null)]
		public System.String Renderer {
			get { return this.Element.Renderer; }
			set { this.Element.Renderer = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _ColumnAction = Component.GetHandler<System.EventArgs>(this.Element, this.ColumnActionAction);
			if (_ColumnAction != null) { this.Element.ColumnAction += _ColumnAction.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.CellRenderingEventArgs> _BeforeCellRendering = Component.GetHandler<System.Web.VisualTree.Elements.CellRenderingEventArgs>(this.Element, this.BeforeCellRenderingAction);
			if (_BeforeCellRendering != null) { this.Element.BeforeCellRendering += _BeforeCellRendering.Invoke; }
        }

		private string _ColumnActionAction;

		public string ColumnActionAction
        {
            get
			{
				return _ColumnActionAction;
			}
			set
			{
				_ColumnActionAction = value;
			}            
        }

		private string _BeforeCellRenderingAction;

		public string BeforeCellRenderingAction
        {
            get
			{
				return _BeforeCellRenderingAction;
			}
			set
			{
				_BeforeCellRenderingAction = value;
			}            
        }


	}  

	/// <summary>
    /// GridImageCell
    /// </summary>
	public class GridImageCell  : GridCell   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridImageCell"/> class.
        /// </summary>
		public GridImageCell()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridImageCell"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridImageCell(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridImageCell Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridImageCell;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridImageCell();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridLinkColumn
    /// </summary>
	public class GridLinkColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridLinkColumn"/> class.
        /// </summary>
		public GridLinkColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridLinkColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridLinkColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridLinkColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridLinkColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridLinkColumn();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridDateTimePickerColumn
    /// </summary>
	public class GridDateTimePickerColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridDateTimePickerColumn"/> class.
        /// </summary>
		public GridDateTimePickerColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridDateTimePickerColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridDateTimePickerColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridDateTimePickerColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridDateTimePickerColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridDateTimePickerColumn();
        }
 
		/// <summary>
		/// Format cell text .
/// </summary>
		[DefaultValue(typeof(System.Web.VisualTree.DateTimePickerFormat), "Default")]
		public System.Web.VisualTree.DateTimePickerFormat Format {
			get { return this.Element.Format; }
			set { this.Element.Format = value; }
		}
		
		/// <summary>
		/// Gets or sets the CustomFormat 
		/// </summary>
		/// <value>
		/// The CustomFormat
		/// </value>
		[DefaultValue(null)]
		public System.String CustomFormat {
			get { return this.Element.CustomFormat; }
			set { this.Element.CustomFormat = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridRowHeader
    /// </summary>
	public class GridRowHeader  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridRowHeader"/> class.
        /// </summary>
		public GridRowHeader()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridRowHeader"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridRowHeader(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridRowHeader Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridRowHeader;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridRowHeader();
        }
 
		/// <summary>
		/// Gets or sets the ShowRowNumber 
		/// </summary>
		/// <value>
		/// The ShowRowNumber
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ShowRowNumber {
			get { return this.Element.ShowRowNumber; }
			set { this.Element.ShowRowNumber = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridTextBoxCell
    /// </summary>
	public class GridTextBoxCell  : GridCell   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextBoxCell"/> class.
        /// </summary>
		public GridTextBoxCell()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextBoxCell"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridTextBoxCell(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridTextBoxCell Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridTextBoxCell;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridTextBoxCell();
        }
 
		/// <summary>
		/// Gets or sets the EditType 
		/// </summary>
		/// <value>
		/// The EditType
		/// </value>
		[DefaultValue(null)]
		public System.Type EditType {
			get { return this.Element.EditType; }
			set { this.Element.EditType = value; }
		}
		
		/// <summary>
		/// Gets or sets the ValueType 
		/// </summary>
		/// <value>
		/// The ValueType
		/// </value>
		[DefaultValue(null)]
		public System.Type ValueType {
			get { return this.Element.ValueType; }
			set { this.Element.ValueType = value; }
		}
		
		/// <summary>
		/// Gets or sets the DefaultNewRowValue 
		/// </summary>
		/// <value>
		/// The DefaultNewRowValue
		/// </value>
		[DefaultValue(null)]
		public System.Object DefaultNewRowValue {
			get { return this.Element.DefaultNewRowValue; }
			set { this.Element.DefaultNewRowValue = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// HTML
    /// </summary>
	public class HTML  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HTML"/> class.
        /// </summary>
		public HTML()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="HTML"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal HTML(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.HTMLElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.HTMLElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.HTMLElement();
        }
 
		/// <summary>
		/// Gets or sets the Url 
		/// </summary>
		/// <value>
		/// The Url
		/// </value>
		[DefaultValue(null)]
		public System.String Url {
			get { return this.Element.Url; }
			set { this.Element.Url = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// KeyItem
    /// </summary>
	public class KeyItem  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyItem"/> class.
        /// </summary>
		public KeyItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal KeyItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.KeyItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.KeyItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.KeyItem();
        }
 
		/// <summary>
		/// Gets or sets the Key 
		/// </summary>
		/// <value>
		/// The Key
		/// </value>
		[DefaultValue(null)]
		public System.String Key {
			get { return this.Element.Key; }
			set { this.Element.Key = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageIndex 
		/// </summary>
		/// <value>
		/// The ImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ImageIndex {
			get { return this.Element.ImageIndex; }
			set { this.Element.ImageIndex = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Label
    /// </summary>
	public class Label  : ContentControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class.
        /// </summary>
		public Label()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Label(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.LabelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.LabelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.LabelElement();
        }
 
		/// <summary>
		/// Gets or sets the TextAlign 
		/// </summary>
		/// <value>
		/// The TextAlign
		/// </value>
		[DefaultValue(default(System.Drawing.ContentAlignment))]
		public System.Drawing.ContentAlignment TextAlign {
			get { return this.Element.TextAlign; }
			set { this.Element.TextAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxWidth 
		/// </summary>
		/// <value>
		/// The MaxWidth
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 MaxWidth {
			get { return this.Element.MaxWidth; }
			set { this.Element.MaxWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxHeight 
		/// </summary>
		/// <value>
		/// The MaxHeight
		/// </value>
		[DefaultValue(-1)]
		public System.Int32 MaxHeight {
			get { return this.Element.MaxHeight; }
			set { this.Element.MaxHeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoEllipsis 
		/// </summary>
		/// <value>
		/// The AutoEllipsis
		/// </value>
		[DefaultValue(false)]
		public System.Boolean AutoEllipsis {
			get { return this.Element.AutoEllipsis; }
			set { this.Element.AutoEllipsis = value; }
		}
		
		/// <summary>
		/// Gets the PreferredWidth 
		/// </summary>
		/// <value>
		/// The PreferredWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 PreferredWidth {
			get { return this.Element.PreferredWidth; }
		}
		
		/// <summary>
		/// Gets or sets the FlatStyle 
		/// </summary>
		/// <value>
		/// The FlatStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.FlatStyle), "Standard")]
		public System.Web.VisualTree.FlatStyle FlatStyle {
			get { return this.Element.FlatStyle; }
			set { this.Element.FlatStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the WordWrap 
		/// </summary>
		/// <value>
		/// The WordWrap
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean WordWrap {
			get { return this.Element.WordWrap; }
			set { this.Element.WordWrap = value; }
		}
		
		/// <summary>
		/// Gets or sets the Content 
		/// </summary>
		/// <value>
		/// The Content
		/// </value>
		[DefaultValue(null)]
		public System.String Content {
			get { return this.Element.Content as System.String; }
			set { this.Element.Content = value; }
		}
		
			
		public string Image {
			get { return this.GetStringFromResource( this.Element.Image); }
			set { this.Element.Image = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the ImageAlign 
		/// </summary>
		/// <value>
		/// The ImageAlign
		/// </value>
		[DefaultValue(typeof(System.Drawing.ContentAlignment), "MiddleCenter")]
		public System.Drawing.ContentAlignment ImageAlign {
			get { return this.Element.ImageAlign; }
			set { this.Element.ImageAlign = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// LinkLabel
    /// </summary>
	public class LinkLabel  : Label   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LinkLabel"/> class.
        /// </summary>
		public LinkLabel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkLabel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal LinkLabel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.LinkLabelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.LinkLabelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.LinkLabelElement();
        }
 
		/// <summary>
		/// Gets or sets the LinkData 
		/// </summary>
		/// <value>
		/// The LinkData
		/// </value>
		[DefaultValue(null)]
		public System.String LinkData {
			get { return this.Element.LinkData; }
			set { this.Element.LinkData = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ListItem
    /// </summary>
	public class ListItem  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem"/> class.
        /// </summary>
		public ListItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ListItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ListItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ListItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ListItem();
        }
 
		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		[DefaultValue(null)]
		public System.String Value {
			get { return this.Element.Value as System.String; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageIndex 
		/// </summary>
		/// <value>
		/// The ImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ImageIndex {
			get { return this.Element.ImageIndex; }
			set { this.Element.ImageIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the ItemData 
		/// </summary>
		/// <value>
		/// The ItemData
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ItemData {
			get { return this.Element.ItemData; }
			set { this.Element.ItemData = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataRow 
		/// </summary>
		/// <value>
		/// The DataRow
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ListItemCollection DataRow {
			get { return this.Element.DataRow; }
			set { this.Element.DataRow = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ComboGrid
    /// </summary>
	public class ComboGrid  : Grid   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComboGrid"/> class.
        /// </summary>
		public ComboGrid()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboGrid"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ComboGrid(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ComboGridElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ComboGridElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ComboGridElement();
        }
 
		/// <summary>
		/// Gets or sets the DisplayMember 
		/// </summary>
		/// <value>
		/// The DisplayMember
		/// </value>
		[DefaultValue(null)]
		public System.String DisplayMember {
			get { return this.Element.DisplayMember; }
			set { this.Element.DisplayMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the ValueMember 
		/// </summary>
		/// <value>
		/// The ValueMember
		/// </value>
		[DefaultValue(null)]
		public System.String ValueMember {
			get { return this.Element.ValueMember; }
			set { this.Element.ValueMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedItem 
		/// </summary>
		/// <value>
		/// The SelectedItem
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ListItem SelectedItem {
			get { return this.Element.SelectedItem; }
			set { this.Element.SelectedItem = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedText 
		/// </summary>
		/// <value>
		/// The SelectedText
		/// </value>
		[DefaultValue(null)]
		public System.String SelectedText {
			get { return this.Element.SelectedText; }
			set { this.Element.SelectedText = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedValue 
		/// </summary>
		/// <value>
		/// The SelectedValue
		/// </value>
		[DefaultValue(null)]
		public System.Object SelectedValue {
			get { return this.Element.SelectedValue; }
			set { this.Element.SelectedValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedIndex 
		/// </summary>
		/// <value>
		/// The SelectedIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedIndex {
			get { return this.Element.SelectedIndex; }
			set { this.Element.SelectedIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the DroppedDown 
		/// </summary>
		/// <value>
		/// The DroppedDown
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean DroppedDown {
			get { return this.Element.DroppedDown; }
			set { this.Element.DroppedDown = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowHeader 
		/// </summary>
		/// <value>
		/// The ShowHeader
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ShowHeader {
			get { return this.Element.ShowHeader; }
			set { this.Element.ShowHeader = value; }
		}
		
		/// <summary>
		/// Gets or sets the PreviousValue 
		/// </summary>
		/// <value>
		/// The PreviousValue
		/// </value>
		[DefaultValue(null)]
		public System.Object PreviousValue {
			get { return this.Element.PreviousValue; }
			set { this.Element.PreviousValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the ExpandOnFocus 
		/// </summary>
		/// <value>
		/// The ExpandOnFocus
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ExpandOnFocus {
			get { return this.Element.ExpandOnFocus; }
			set { this.Element.ExpandOnFocus = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.GridCellMouseEventArgs> _SelectedIndexChange = Component.GetHandler<System.Web.VisualTree.Elements.GridCellMouseEventArgs>(this.Element, this.SelectedIndexChangeAction);
			if (_SelectedIndexChange != null) { this.Element.SelectedIndexChange += _SelectedIndexChange.Invoke; }
			IVisualElementAction<System.EventArgs> _SelectedIndexChanged = Component.GetHandler<System.EventArgs>(this.Element, this.SelectedIndexChangedAction);
			if (_SelectedIndexChanged != null) { this.Element.SelectedIndexChanged += _SelectedIndexChanged.Invoke; }

         
        }

		private string _SelectedIndexChangeAction;

		public string SelectedIndexChangeAction
        {
            get
			{
				return _SelectedIndexChangeAction;
			}
			set
			{
				_SelectedIndexChangeAction = value;
			}            
        }

		private string _SelectedIndexChangedAction;

		public string SelectedIndexChangedAction
        {
            get
			{
				return _SelectedIndexChangedAction;
			}
			set
			{
				_SelectedIndexChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// MaskedTextBox
    /// </summary>
	public class MaskedTextBox  : TextBoxBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaskedTextBox"/> class.
        /// </summary>
		public MaskedTextBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="MaskedTextBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal MaskedTextBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.MaskedTextBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.MaskedTextBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.MaskedTextBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the Mask 
		/// </summary>
		/// <value>
		/// The Mask
		/// </value>
		[DefaultValue("")]
		public System.String Mask {
			get { return this.Element.Mask; }
			set { this.Element.Mask = value; }
		}
		
		/// <summary>
		/// Gets or sets the Regex 
		/// </summary>
		/// <value>
		/// The Regex
		/// </value>
		[DefaultValue(null)]
		public System.String Regex {
			get { return this.Element.Regex; }
			set { this.Element.Regex = value; }
		}
		
		/// <summary>
		/// Gets or sets the PromptChar 
		/// </summary>
		/// <value>
		/// The PromptChar
		/// </value>
		public System.Char PromptChar {
			get { return this.Element.PromptChar; }
			set { this.Element.PromptChar = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextMaskFormat 
		/// </summary>
		/// <value>
		/// The TextMaskFormat
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.MaskFormat), "ExcludePromptAndLiterals")]
		public System.Web.VisualTree.Elements.MaskFormat TextMaskFormat {
			get { return this.Element.TextMaskFormat; }
			set { this.Element.TextMaskFormat = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowBlank 
		/// </summary>
		/// <value>
		/// The AllowBlank
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AllowBlank {
			get { return this.Element.AllowBlank; }
			set { this.Element.AllowBlank = value; }
		}
		
		/// <summary>
		/// Gets or sets the Multiline 
		/// </summary>
		/// <value>
		/// The Multiline
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Multiline {
			get { return this.Element.Multiline; }
			set { this.Element.Multiline = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaskedText 
		/// </summary>
		/// <value>
		/// The MaskedText
		/// </value>
		[DefaultValue("")]
		public System.String MaskedText {
			get { return this.Element.MaskedText; }
			set { this.Element.MaskedText = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue("")]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// RibbonPage
    /// </summary>
	public class RibbonPage  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonPage"/> class.
        /// </summary>
		public RibbonPage()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonPage"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RibbonPage(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.RibbonPageElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.RibbonPageElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.RibbonPageElement();
        }
 
		/// <summary>
		/// Gets the Groups 
		/// </summary>
		/// <value>
		/// The Groups
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.RibbonPageGroupCollection Groups {
			get { return this.Element.Groups; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ControlContainer
    /// </summary>
	public abstract class ControlContainer  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlContainer"/> class.
        /// </summary>
		public ControlContainer()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlContainer"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ControlContainer(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ControlContainerElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ControlContainerElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the AutoScaleMode 
		/// </summary>
		/// <value>
		/// The AutoScaleMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.AutoScaleMode))]
		public System.Web.VisualTree.AutoScaleMode AutoScaleMode {
			get { return this.Element.AutoScaleMode; }
			set { this.Element.AutoScaleMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoScaleDimensions 
		/// </summary>
		/// <value>
		/// The AutoScaleDimensions
		/// </value>
		public System.Drawing.SizeF AutoScaleDimensions {
			get { return this.Element.AutoScaleDimensions; }
			set { this.Element.AutoScaleDimensions = value; }
		}
		
		internal bool ShouldSerializeAutoScaleDimensions() { return this.AutoScaleDimensions!=System.Drawing.SizeF.Empty;}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Tab
    /// </summary>
	[ParseChildren(true)]
	public class Tab  : ControlContainer   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tab"/> class.
        /// </summary>
		public Tab()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Tab"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Tab(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TabElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TabElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TabElement();
        }
 
		/// <summary>
		/// Gets or sets the TabsPerRow 
		/// </summary>
		/// <value>
		/// The TabsPerRow
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 TabsPerRow {
			get { return this.Element.TabsPerRow; }
			set { this.Element.TabsPerRow = value; }
		}
		
		/// <summary>
		/// Gets or sets the WordWrap 
		/// </summary>
		/// <value>
		/// The WordWrap
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean WordWrap {
			get { return this.Element.WordWrap; }
			set { this.Element.WordWrap = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedIndex 
		/// </summary>
		/// <value>
		/// The SelectedIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedIndex {
			get { return this.Element.SelectedIndex; }
			set { this.Element.SelectedIndex = value; }
		}
		
		/// <summary>
		/// Gets the PreviousIndex 
		/// </summary>
		/// <value>
		/// The PreviousIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 PreviousIndex {
			get { return this.Element.PreviousIndex; }
		}
		
		/// <summary>
		/// Gets or sets the Alignment 
		/// </summary>
		/// <value>
		/// The Alignment
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.TabAlignment))]
		public System.Web.VisualTree.Elements.TabAlignment Alignment {
			get { return this.Element.Alignment; }
			set { this.Element.Alignment = value; }
		}
		
		/// <summary>
		/// Gets the TabItems 
		/// </summary>
		/// <value>
		/// The TabItems
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public TabItemCollection TabItems {
			get { return new TabItemCollection(this, this.Element.TabItems); }
		}
		
		/// <summary>
        /// The state of the SelectedTabID property.
        /// </summary>
		private string _SelectedTabID = null;

		/// <summary>
		/// Gets or sets the SelectedTab Reference
		/// </summary>
		/// <value>
		/// The SelectedTab
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string SelectedTabID {
           get
            {
                if (this.Element.SelectedTab != null)
                {
                    return this.Element.SelectedTab.ID;
                }
                return _SelectedTabID;
            }
            set
            {
				// Set the menu id state
				_SelectedTabID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeSelectedTab(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeSelectedTab;
				}
            }
		}

		/// <summary>
        /// Initialize the SelectedTab property.
        /// </summary>
		private void InitializeSelectedTab(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_SelectedTabID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.SelectedTab = component.Element as System.Web.VisualTree.Elements.TabItem;
				}
			}
		}

		/// <summary>
		/// Gets or sets the SelectedTab 
		/// </summary>
		/// <value>
		/// The SelectedTab
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.TabItem SelectedTab {
			get { return this.Element.SelectedTab; }
			set { this.Element.SelectedTab = value; }
		}
		
		/// <summary>
		/// Gets or sets the WindowTargetID 
		/// </summary>
		/// <value>
		/// The WindowTargetID
		/// </value>
		[DefaultValue(null)]
		public System.String WindowTargetID {
			get { return this.Element.WindowTargetID; }
			set { this.Element.WindowTargetID = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _SelectedIndexChanged = Component.GetHandler<System.EventArgs>(this.Element, this.SelectedIndexChangedAction);
			if (_SelectedIndexChanged != null) { this.Element.SelectedIndexChanged +=
                    
                    _SelectedIndexChanged.Invoke;
            }
			IVisualElementAction<System.Web.VisualTree.Elements.TabViewCancelEventArgs> _Selecting = Component.GetHandler<System.Web.VisualTree.Elements.TabViewCancelEventArgs>(this.Element, this.SelectingAction);
			if (_Selecting != null) { this.Element.Selecting += _Selecting.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TabViewCancelEventArgs> _Deselecting = Component.GetHandler<System.Web.VisualTree.Elements.TabViewCancelEventArgs>(this.Element, this.DeselectingAction);
			if (_Deselecting != null) { this.Element.Deselecting += _Deselecting.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TabControlEventArgs> _Selected = Component.GetHandler<System.Web.VisualTree.Elements.TabControlEventArgs>(this.Element, this.SelectedAction);
			if (_Selected != null) { this.Element.Selected += _Selected.Invoke; }
        }

		private string _SelectedIndexChangedAction;

		public string SelectedIndexChangedAction
        {
            get
			{
				return _SelectedIndexChangedAction;
			}
			set
			{
				_SelectedIndexChangedAction = value;
			}            
        }

		private string _SelectingAction;

		public string SelectingAction
        {
            get
			{
				return _SelectingAction;
			}
			set
			{
				_SelectingAction = value;
			}            
        }

		private string _DeselectingAction;

		public string DeselectingAction
        {
            get
			{
				return _DeselectingAction;
			}
			set
			{
				_DeselectingAction = value;
			}            
        }

		private string _SelectedAction;

		public string SelectedAction
        {
            get
			{
				return _SelectedAction;
			}
			set
			{
				_SelectedAction = value;
			}            
        }


	}  

	/// <summary>
    /// Accordion
    /// </summary>
	public class Accordion  : Tab   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Accordion"/> class.
        /// </summary>
		public Accordion()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Accordion"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Accordion(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.AccordionElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.AccordionElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.AccordionElement();
        }
 
		/// <summary>
		/// Gets or sets the ItemSizeMode 
		/// </summary>
		/// <value>
		/// The ItemSizeMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.PageViewItemSizeMode))]
		public System.Web.VisualTree.Elements.PageViewItemSizeMode ItemSizeMode {
			get { return this.Element.ItemSizeMode; }
			set { this.Element.ItemSizeMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the ViewMode 
		/// </summary>
		/// <value>
		/// The ViewMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.PageViewMode))]
		public System.Web.VisualTree.Elements.PageViewMode ViewMode {
			get { return this.Element.ViewMode; }
			set { this.Element.ViewMode = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarCheckItem
    /// </summary>
	public class ToolBarCheckItem  : ToolBarDropDownItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarCheckItem"/> class.
        /// </summary>
		public ToolBarCheckItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarCheckItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarCheckItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarCheckItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarCheckItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarCheckItem();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarMenuItem
    /// </summary>
	public class ToolBarMenuItem  : ToolBarDropDownItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarMenuItem"/> class.
        /// </summary>
		public ToolBarMenuItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarMenuItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarMenuItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarMenuItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarMenuItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarMenuItem();
        }
 
		/// <summary>
		/// Gets or sets the Checked 
		/// </summary>
		/// <value>
		/// The Checked
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Checked {
			get { return this.Element.Checked; }
			set { this.Element.Checked = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShortcutKeys 
		/// </summary>
		/// <value>
		/// The ShortcutKeys
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Keys), "None")]
		public System.Web.VisualTree.Keys ShortcutKeys {
			get { return this.Element.ShortcutKeys; }
			set { this.Element.ShortcutKeys = value; }
		}
		
		/// <summary>
		/// Gets or sets the CheckOnClick 
		/// </summary>
		/// <value>
		/// The CheckOnClick
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean CheckOnClick {
			get { return this.Element.CheckOnClick; }
			set { this.Element.CheckOnClick = value; }
		}
		
		/// <summary>
		/// Gets or sets the CheckState 
		/// </summary>
		/// <value>
		/// The CheckState
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.CheckState))]
		public System.Web.VisualTree.CheckState CheckState {
			get { return this.Element.CheckState; }
			set { this.Element.CheckState = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _CheckStateChanged = Component.GetHandler<System.EventArgs>(this.Element, this.CheckStateChangedAction);
			if (_CheckStateChanged != null) { this.Element.CheckStateChanged += _CheckStateChanged.Invoke; }
        }

		private string _CheckStateChangedAction;

		public string CheckStateChangedAction
        {
            get
			{
				return _CheckStateChangedAction;
			}
			set
			{
				_CheckStateChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// ToolBarMenuSeparator
    /// </summary>
	public class ToolBarMenuSeparator  : ToolBarMenuItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarMenuSeparator"/> class.
        /// </summary>
		public ToolBarMenuSeparator()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarMenuSeparator"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarMenuSeparator(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarMenuSeparator Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarMenuSeparator;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarMenuSeparator();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarTextBox
    /// </summary>
	public class ToolBarTextBox  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarTextBox"/> class.
        /// </summary>
		public ToolBarTextBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarTextBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarTextBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarTextBox Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarTextBox;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarTextBox();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TCards
    /// </summary>
	public class TCards  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TCards"/> class.
        /// </summary>
		public TCards()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TCards"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TCards(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TCardsElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TCardsElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TCardsElement();
        }
 
		/// <summary>
		/// Gets or sets the SelectedIndex 
		/// </summary>
		/// <value>
		/// The SelectedIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedIndex {
			get { return this.Element.SelectedIndex; }
			set { this.Element.SelectedIndex = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TComboBox
    /// </summary>
	public class TComboBox  : ComboBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TComboBox"/> class.
        /// </summary>
		public TComboBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TComboBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TComboBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TComboBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TComboBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TComboBoxElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TComboBoxField
    /// </summary>
	public class TComboBoxField  : TComboBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TComboBoxField"/> class.
        /// </summary>
		public TComboBoxField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TComboBoxField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TComboBoxField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TComboBoxFieldElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TComboBoxFieldElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TComboBoxFieldElement();
        }
 
		/// <summary>
		/// Gets or sets the Label 
		/// </summary>
		/// <value>
		/// The Label
		/// </value>
		[DefaultValue(null)]
		public System.String Label {
			get { return this.Element.Label; }
			set { this.Element.Label = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TreeColumn
    /// </summary>
	public class TreeColumn  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeColumn"/> class.
        /// </summary>
		public TreeColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TreeColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TreeColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TreeColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TreeColumn();
        }
 
		/// <summary>
		/// Gets or sets the HeaderText 
		/// </summary>
		/// <value>
		/// The HeaderText
		/// </value>
		[DefaultValue(null)]
		public System.String HeaderText {
			get { return this.Element.HeaderText; }
			set { this.Element.HeaderText = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataType 
		/// </summary>
		/// <value>
		/// The DataType
		/// </value>
		[DefaultValue(null)]
		public System.Type DataType {
			get { return this.Element.DataType; }
			set { this.Element.DataType = value; }
		}
		
		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
		/// <summary>
		/// Gets or sets the DataMember 
		/// </summary>
		/// <value>
		/// The DataMember
		/// </value>
		[DefaultValue(null)]
		public System.String DataMember {
			get { return this.Element.DataMember; }
			set { this.Element.DataMember = value; }
		}
		
		/// <summary>
		/// Gets or sets the SortType 
		/// </summary>
		/// <value>
		/// The SortType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ColumnSortMode))]
		public System.Web.VisualTree.ColumnSortMode SortType {
			get { return this.Element.SortType; }
			set { this.Element.SortType = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// WidgetColumnTextBox
    /// </summary>
	public class WidgetColumnTextBox  : TextBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetColumnTextBox"/> class.
        /// </summary>
		public WidgetColumnTextBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetColumnTextBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal WidgetColumnTextBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.WidgetColumnTextBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.WidgetColumnTextBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the HasEditChange 
		/// </summary>
		/// <value>
		/// The HasEditChange
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasEditChange {
			get { return this.Element.HasEditChange; }
			set { this.Element.HasEditChange = value; }
		}
		
		/// <summary>
		/// Gets or sets the HasFocusEnter 
		/// </summary>
		/// <value>
		/// The HasFocusEnter
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasFocusEnter {
			get { return this.Element.HasFocusEnter; }
			set { this.Element.HasFocusEnter = value; }
		}
		
		/// <summary>
		/// Gets or sets the HasItemChanged 
		/// </summary>
		/// <value>
		/// The HasItemChanged
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasItemChanged {
			get { return this.Element.HasItemChanged; }
			set { this.Element.HasItemChanged = value; }
		}
		
		/// <summary>
		/// Gets or sets the HasDoubleClick 
		/// </summary>
		/// <value>
		/// The HasDoubleClick
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasDoubleClick {
			get { return this.Element.HasDoubleClick; }
			set { this.Element.HasDoubleClick = value; }
		}
		
		/// <summary>
		/// Gets or sets the HasClick 
		/// </summary>
		/// <value>
		/// The HasClick
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasClick {
			get { return this.Element.HasClick; }
			set { this.Element.HasClick = value; }
		}
		
		/// <summary>
		/// Gets the HasWidgetControlEditChangedListeners 
		/// </summary>
		/// <value>
		/// The HasWidgetControlEditChangedListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWidgetControlEditChangedListeners {
			get { return this.Element.HasWidgetControlEditChangedListeners; }
		}
		
		/// <summary>
		/// Gets the HasWidgetControlItemChangedListeners 
		/// </summary>
		/// <value>
		/// The HasWidgetControlItemChangedListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWidgetControlItemChangedListeners {
			get { return this.Element.HasWidgetControlItemChangedListeners; }
		}
		
		/// <summary>
		/// Gets the HasWidgetControlFocusEnterListeners 
		/// </summary>
		/// <value>
		/// The HasWidgetControlFocusEnterListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWidgetControlFocusEnterListeners {
			get { return this.Element.HasWidgetControlFocusEnterListeners; }
		}
		
		/// <summary>
		/// Gets the HasWidgetControlDoubleClickListeners 
		/// </summary>
		/// <value>
		/// The HasWidgetControlDoubleClickListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWidgetControlDoubleClickListeners {
			get { return this.Element.HasWidgetControlDoubleClickListeners; }
		}
		
		/// <summary>
		/// Gets the HasWidgetControlClickListeners 
		/// </summary>
		/// <value>
		/// The HasWidgetControlClickListeners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWidgetControlClickListeners {
			get { return this.Element.HasWidgetControlClickListeners; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataEntry
    /// </summary>
	public class DataEntry  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataEntry"/> class.
        /// </summary>
		public DataEntry()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataEntry"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataEntry(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataEntryElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataEntryElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataEntryElement();
        }
 
		/// <summary>
		/// Gets or sets the FlowDirection 
		/// </summary>
		/// <value>
		/// The FlowDirection
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.FlowDirection))]
		public System.Web.VisualTree.Elements.FlowDirection FlowDirection {
			get { return this.Element.FlowDirection; }
			set { this.Element.FlowDirection = value; }
		}
		
		/// <summary>
		/// Gets or sets the ItemDefaultSize 
		/// </summary>
		/// <value>
		/// The ItemDefaultSize
		/// </value>
		public System.Drawing.Size ItemDefaultSize {
			get { return this.Element.ItemDefaultSize; }
			set { this.Element.ItemDefaultSize = value; }
		}
		
		internal bool ShouldSerializeItemDefaultSize() { return this.ItemDefaultSize!=System.Drawing.Size.Empty;}

		/// <summary>
		/// Gets or sets the ColumnCount 
		/// </summary>
		/// <value>
		/// The ColumnCount
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ColumnCount {
			get { return this.Element.ColumnCount; }
			set { this.Element.ColumnCount = value; }
		}
		
		/// <summary>
		/// Gets or sets the FitToParentWidth 
		/// </summary>
		/// <value>
		/// The FitToParentWidth
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean FitToParentWidth {
			get { return this.Element.FitToParentWidth; }
			set { this.Element.FitToParentWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the ItemSpace 
		/// </summary>
		/// <value>
		/// The ItemSpace
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ItemSpace {
			get { return this.Element.ItemSpace; }
			set { this.Element.ItemSpace = value; }
		}
		
		/// <summary>
		/// Gets or sets the DataSource 
		/// </summary>
		/// <value>
		/// The DataSource
		/// </value>
		[DefaultValue(null)]
		public System.Object DataSource {
			get { return this.Element.DataSource; }
			set { this.Element.DataSource = value; }
		}
		
		/// <summary>
		/// Gets the ItemSource 
		/// </summary>
		/// <value>
		/// The ItemSource
		/// </value>
		[DefaultValue(null)]
		public System.Object ItemSource {
			get { return this.Element.ItemSource; }
		}
		
		/// <summary>
		/// Gets the ItemProperties 
		/// </summary>
		/// <value>
		/// The ItemProperties
		/// </value>
		[DefaultValue(null)]
		public System.ComponentModel.PropertyDescriptorCollection ItemProperties {
			get { return this.Element.ItemProperties; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Updated = Component.GetHandler<System.EventArgs>(this.Element, this.UpdatedAction);
			if (_Updated != null) { this.Element.Updated += _Updated.Invoke; }
        }

		private string _UpdatedAction;

		public string UpdatedAction
        {
            get
			{
				return _UpdatedAction;
			}
			set
			{
				_UpdatedAction = value;
			}            
        }


	}  

	/// <summary>
    /// DockPanel
    /// </summary>
	public class DockPanel  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanel"/> class.
        /// </summary>
		public DockPanel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DockPanel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DockPanelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DockPanelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DockPanelElement();
        }
 
		/// <summary>
		/// Gets or sets the IsCleanUpTarget 
		/// </summary>
		/// <value>
		/// The IsCleanUpTarget
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsCleanUpTarget {
			get { return this.Element.IsCleanUpTarget; }
			set { this.Element.IsCleanUpTarget = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DirectoryListBox
    /// </summary>
	public class DirectoryListBox  : FileSystemBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryListBox"/> class.
        /// </summary>
		public DirectoryListBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryListBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DirectoryListBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Compatibility.DirectoryListBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Compatibility.DirectoryListBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Compatibility.DirectoryListBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the Path 
		/// </summary>
		/// <value>
		/// The Path
		/// </value>
		[DefaultValue(null)]
		public System.String Path {
			get { return this.Element.Path; }
			set { this.Element.Path = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Changed = Component.GetHandler<System.EventArgs>(this.Element, this.ChangedAction);
			if (_Changed != null) { this.Element.Changed += _Changed.Invoke; }
        }

		private string _ChangedAction;

		public string ChangedAction
        {
            get
			{
				return _ChangedAction;
			}
			set
			{
				_ChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// DriveListBox
    /// </summary>
	public class DriveListBox  : FileSystemBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DriveListBox"/> class.
        /// </summary>
		public DriveListBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DriveListBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DriveListBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Compatibility.DriveListBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Compatibility.DriveListBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Compatibility.DriveListBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the DriveID 
		/// </summary>
		/// <value>
		/// The DriveID
		/// </value>
		[DefaultValue(null)]
		public System.String DriveID {
			get { return this.Element.DriveID; }
			set { this.Element.DriveID = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Changed = Component.GetHandler<System.EventArgs>(this.Element, this.ChangedAction);
			if (_Changed != null) { this.Element.Changed += _Changed.Invoke; }
        }

		private string _ChangedAction;

		public string ChangedAction
        {
            get
			{
				return _ChangedAction;
			}
			set
			{
				_ChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// FileListBox
    /// </summary>
	public class FileListBox  : CompositeBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileListBox"/> class.
        /// </summary>
		public FileListBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="FileListBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal FileListBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Compatibility.FileListBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Compatibility.FileListBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Compatibility.FileListBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the Path 
		/// </summary>
		/// <value>
		/// The Path
		/// </value>
		[DefaultValue(null)]
		public System.String Path {
			get { return this.Element.Path; }
			set { this.Element.Path = value; }
		}
		
		/// <summary>
		/// Gets or sets the EnableUpload 
		/// </summary>
		/// <value>
		/// The EnableUpload
		/// </value>
		[DefaultValue(true)]
		public System.Boolean EnableUpload {
			get { return this.Element.EnableUpload; }
			set { this.Element.EnableUpload = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _SelectedItemChanged = Component.GetHandler<System.EventArgs>(this.Element, this.SelectedItemChangedAction);
			if (_SelectedItemChanged != null) { this.Element.SelectedItemChanged += _SelectedItemChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _FileUploaded = Component.GetHandler<System.EventArgs>(this.Element, this.FileUploadedAction);
			if (_FileUploaded != null) { this.Element.FileUploaded += _FileUploaded.Invoke; }
        }

		private string _SelectedItemChangedAction;

		public string SelectedItemChangedAction
        {
            get
			{
				return _SelectedItemChangedAction;
			}
			set
			{
				_SelectedItemChangedAction = value;
			}            
        }

		private string _FileUploadedAction;

		public string FileUploadedAction
        {
            get
			{
				return _FileUploadedAction;
			}
			set
			{
				_FileUploadedAction = value;
			}            
        }


	}  

	/// <summary>
    /// FileUpload
    /// </summary>
	public class FileUpload  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileUpload"/> class.
        /// </summary>
		public FileUpload()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="FileUpload"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal FileUpload(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.FileUploadElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.FileUploadElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.FileUploadElement();
        }
 
		/// <summary>
		/// Gets or sets the UploadPath 
		/// </summary>
		/// <value>
		/// The UploadPath
		/// </value>
		[DefaultValue(null)]
		public System.String UploadPath {
			get { return this.Element.UploadPath; }
			set { this.Element.UploadPath = value; }
		}
		
		/// <summary>
		/// Gets or sets the File 
		/// </summary>
		/// <value>
		/// The File
		/// </value>
		[DefaultValue(null)]
		public System.String File {
			get { return this.Element.File; }
			set { this.Element.File = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue("")]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _FileUploaded = Component.GetHandler<System.EventArgs>(this.Element, this.FileUploadedAction);
			if (_FileUploaded != null) { this.Element.FileUploaded += _FileUploaded.Invoke; }
        }

		private string _FileUploadedAction;

		public string FileUploadedAction
        {
            get
			{
				return _FileUploadedAction;
			}
			set
			{
				_FileUploadedAction = value;
			}            
        }


	}  

	/// <summary>
    /// ProgressBar
    /// </summary>
	public class ProgressBar  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressBar"/> class.
        /// </summary>
		public ProgressBar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressBar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ProgressBar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ProgressBarElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ProgressBarElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ProgressBarElement();
        }
 
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the Max 
		/// </summary>
		/// <value>
		/// The Max
		/// </value>
		[DefaultValue(100)]
		public System.Int32 Max {
			get { return this.Element.Max; }
			set { this.Element.Max = value; }
		}
		
		/// <summary>
		/// Gets or sets the Min 
		/// </summary>
		/// <value>
		/// The Min
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Min {
			get { return this.Element.Min; }
			set { this.Element.Min = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ResourceReference
    /// </summary>
	public class ResourceReference  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceReference"/> class.
        /// </summary>
		public ResourceReference()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ResourceReference(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ResourceReference Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ResourceReference;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ResourceReference();
        }
 
		/// <summary>
		/// Gets or sets the Source 
		/// </summary>
		/// <value>
		/// The Source
		/// </value>
		[DefaultValue(null)]
		public System.String Source {
			get { return this.Element.Source; }
			set { this.Element.Source = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// IconReference
    /// </summary>
	public class IconReference  : ResourceReference   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IconReference"/> class.
        /// </summary>
		public IconReference()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="IconReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal IconReference(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.IconReference Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.IconReference;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.IconReference();
        }
 
		/// <summary>
		/// Gets the Source 
		/// </summary>
		/// <value>
		/// The Source
		/// </value>
		[DefaultValue(null)]
		public System.String Source {
			get { return this.Element.Source; }
		}
		
		/// <summary>
		/// Gets the IconSize 
		/// </summary>
		/// <value>
		/// The IconSize
		/// </value>
		public System.Drawing.Size IconSize {
			get { return this.Element.IconSize; }
		}
		
		internal bool ShouldSerializeIconSize() { return this.IconSize!=System.Drawing.Size.Empty;}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ImageReference
    /// </summary>
	public class ImageReference  : ResourceReference   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageReference"/> class.
        /// </summary>
		public ImageReference()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ImageReference(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ImageReference Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ImageReference;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ImageReference();
        }
 
		/// <summary>
		/// Gets the ImageSize 
		/// </summary>
		/// <value>
		/// The ImageSize
		/// </value>
		public System.Drawing.Size ImageSize {
			get { return this.Element.ImageSize; }
		}
		
		internal bool ShouldSerializeImageSize() { return this.ImageSize!=System.Drawing.Size.Empty;}

		/// <summary>
		/// Gets the Source 
		/// </summary>
		/// <value>
		/// The Source
		/// </value>
		[DefaultValue(null)]
		public System.String Source {
			get { return this.Element.Source; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// UrlReference
    /// </summary>
	public class UrlReference  : ResourceReference   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UrlReference"/> class.
        /// </summary>
		public UrlReference()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal UrlReference(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.UrlReference Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.UrlReference;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.UrlReference();
        }
 
		/// <summary>
		/// Gets or sets the Source 
		/// </summary>
		/// <value>
		/// The Source
		/// </value>
		[DefaultValue(null)]
		public System.String Source {
			get { return this.Element.Source; }
			set { this.Element.Source = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Line
    /// </summary>
	public class Line  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Line"/> class.
        /// </summary>
		public Line()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Line"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Line(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.LineElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.LineElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.LineElement();
        }
 
		/// <summary>
		/// Gets or sets the X1 
		/// </summary>
		/// <value>
		/// The X1
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 X1 {
			get { return this.Element.X1; }
			set { this.Element.X1 = value; }
		}
		
		/// <summary>
		/// Gets or sets the Y1 
		/// </summary>
		/// <value>
		/// The Y1
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Y1 {
			get { return this.Element.Y1; }
			set { this.Element.Y1 = value; }
		}
		
		/// <summary>
		/// Gets or sets the X2 
		/// </summary>
		/// <value>
		/// The X2
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 X2 {
			get { return this.Element.X2; }
			set { this.Element.X2 = value; }
		}
		
		/// <summary>
		/// Gets or sets the Y2 
		/// </summary>
		/// <value>
		/// The Y2
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Y2 {
			get { return this.Element.Y2; }
			set { this.Element.Y2 = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Shape
    /// </summary>
	public class Shape  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shape"/> class.
        /// </summary>
		public Shape()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Shape"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Shape(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ShapeElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ShapeElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ShapeElement();
        }
 
		/// <summary>
		/// Gets or sets the Type 
		/// </summary>
		/// <value>
		/// The Type
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ShapeType))]
		public System.Web.VisualTree.Elements.ShapeType Type {
			get { return this.Element.Type; }
			set { this.Element.Type = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarDropDownButton
    /// </summary>
	public class ToolBarDropDownButton  : ToolBarDropDownItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownButton"/> class.
        /// </summary>
		public ToolBarDropDownButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarDropDownButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarDropDownButton Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarDropDownButton;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarDropDownButton();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Button
    /// </summary>
	public class Button  : ButtonBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class.
        /// </summary>
		public Button()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Button(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ButtonElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ButtonElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ButtonElement();
        }
 
		/// <summary>
		/// Gets or sets the CssClass 
		/// </summary>
		/// <value>
		/// The CssClass
		/// </value>
		[DefaultValue("")]
		public System.String CssClass {
			get { return this.Element.CssClass; }
			set { this.Element.CssClass = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsPressed 
		/// </summary>
		/// <value>
		/// The IsPressed
		/// </value>
		[DefaultValue(false)]
		public System.Boolean IsPressed {
			get { return this.Element.IsPressed; }
			set { this.Element.IsPressed = value; }
		}
		
		/// <summary>
		/// Gets or sets the EnableToggle 
		/// </summary>
		/// <value>
		/// The EnableToggle
		/// </value>
		[DefaultValue(false)]
		public System.Boolean EnableToggle {
			get { return this.Element.EnableToggle; }
			set { this.Element.EnableToggle = value; }
		}
		
		/// <summary>
		/// Gets or sets the DialogResult 
		/// </summary>
		/// <value>
		/// The DialogResult
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.DialogResult), "None")]
		public System.Web.VisualTree.DialogResult DialogResult {
			get { return this.Element.DialogResult; }
			set { this.Element.DialogResult = value; }
		}
		
		/// <summary>
		/// Gets or sets the Scale 
		/// </summary>
		/// <value>
		/// The Scale
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ScaleConstants), "Small")]
		public System.Web.VisualTree.ScaleConstants Scale {
			get { return this.Element.Scale; }
			set { this.Element.Scale = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.ButtonPressedChangeEventArgs> _PressedChange = Component.GetHandler<System.Web.VisualTree.Elements.ButtonPressedChangeEventArgs>(this.Element, this.PressedChangeAction);
			if (_PressedChange != null) { this.Element.PressedChange += _PressedChange.Invoke; }
        }

		private string _PressedChangeAction;

		public string PressedChangeAction
        {
            get
			{
				return _PressedChangeAction;
			}
			set
			{
				_PressedChangeAction = value;
			}            
        }


	}  

	/// <summary>
    /// TButton
    /// </summary>
	public class TButton  : Button   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TButton"/> class.
        /// </summary>
		public TButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TButtonElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TButtonElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TButtonElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TCarousel
    /// </summary>
	public class TCarousel  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TCarousel"/> class.
        /// </summary>
		public TCarousel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TCarousel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TCarousel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TCarouselElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TCarouselElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TCarouselElement();
        }
 
		/// <summary>
		/// Gets or sets the SelectedIndex 
		/// </summary>
		/// <value>
		/// The SelectedIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedIndex {
			get { return this.Element.SelectedIndex; }
			set { this.Element.SelectedIndex = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TDockPanel
    /// </summary>
	public class TDockPanel  : DockPanel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TDockPanel"/> class.
        /// </summary>
		public TDockPanel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TDockPanel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TDockPanel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TDockPanelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TDockPanelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TDockPanelElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TForm
    /// </summary>
	public class TForm  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TForm"/> class.
        /// </summary>
		public TForm()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TForm"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TForm(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TFormElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TFormElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TFormElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GroupBox
    /// </summary>
	public class GroupBox  : ScrollableControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupBox"/> class.
        /// </summary>
		public GroupBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GroupBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GroupBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GroupBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GroupBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the Alignment 
		/// </summary>
		/// <value>
		/// The Alignment
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.GroupBoxAlignment), "LeftTop")]
		public System.Web.VisualTree.Elements.GroupBoxAlignment Alignment {
			get { return this.Element.Alignment; }
			set { this.Element.Alignment = value; }
		}
		
		/// <summary>
		/// Gets or sets the RoundedCorners 
		/// </summary>
		/// <value>
		/// The RoundedCorners
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean RoundedCorners {
			get { return this.Element.RoundedCorners; }
			set { this.Element.RoundedCorners = value; }
		}
		
		/// <summary>
		/// Gets or sets the FloodShowPct 
		/// </summary>
		/// <value>
		/// The FloodShowPct
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean FloodShowPct {
			get { return this.Element.FloodShowPct; }
			set { this.Element.FloodShowPct = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TFormFieldSet
    /// </summary>
	public class TFormFieldSet  : GroupBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TFormFieldSet"/> class.
        /// </summary>
		public TFormFieldSet()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TFormFieldSet"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TFormFieldSet(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TFormFieldSetElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TFormFieldSetElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TFormFieldSetElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TLabel
    /// </summary>
	public class TLabel  : Label   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TLabel"/> class.
        /// </summary>
		public TLabel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TLabel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TLabel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TLabelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TLabelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TLabelElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ListView
    /// </summary>
	[ParseChildren(true)]
	public class ListView  : ControlContainer   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListView"/> class.
        /// </summary>
		public ListView()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ListView"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ListView(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ListViewElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ListViewElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ListViewElement();
        }
 
		/// <summary>
		/// Gets or sets the AutoGenerateColumns 
		/// </summary>
		/// <value>
		/// The AutoGenerateColumns
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AutoGenerateColumns {
			get { return this.Element.AutoGenerateColumns; }
			set { this.Element.AutoGenerateColumns = value; }
		}
		
		/// <summary>
		/// Gets or sets the FullRowSelect 
		/// </summary>
		/// <value>
		/// The FullRowSelect
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean FullRowSelect {
			get { return this.Element.FullRowSelect; }
			set { this.Element.FullRowSelect = value; }
		}
		
		/// <summary>
		/// Gets the HeadersVisible 
		/// </summary>
		/// <value>
		/// The HeadersVisible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HeadersVisible {
			get { return this.Element.HeadersVisible; }
		}
		
		/// <summary>
		/// Gets the ColumnLinesVisible 
		/// </summary>
		/// <value>
		/// The ColumnLinesVisible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ColumnLinesVisible {
			get { return this.Element.ColumnLinesVisible; }
		}
		
		/// <summary>
		/// Gets the AutoAdjustColumnWidth 
		/// </summary>
		/// <value>
		/// The AutoAdjustColumnWidth
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AutoAdjustColumnWidth {
			get { return this.Element.AutoAdjustColumnWidth; }
		}
		
		/// <summary>
		/// Gets the RowLinesVisible 
		/// </summary>
		/// <value>
		/// The RowLinesVisible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean RowLinesVisible {
			get { return this.Element.RowLinesVisible; }
		}
		
		/// <summary>
		/// Gets the Columns 
		/// </summary>
		/// <value>
		/// The Columns
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public ColumnHeaderCollection Columns {
			get { return new ColumnHeaderCollection(this, this.Element.Columns); }
		}
		
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public ListViewItemCollection Items {
			get { return new ListViewItemCollection(this, this.Element.Items); }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets or sets the CheckBoxes 
		/// </summary>
		/// <value>
		/// The CheckBoxes
		/// </value>
		[DefaultValue(false)]
		public System.Boolean CheckBoxes {
			get { return this.Element.CheckBoxes; }
			set { this.Element.CheckBoxes = value; }
		}
		
		/// <summary>
		/// Gets or sets the UseCompatibleStateImageBehavior 
		/// </summary>
		/// <value>
		/// The UseCompatibleStateImageBehavior
		/// </value>
		[DefaultValue(true)]
		public System.Boolean UseCompatibleStateImageBehavior {
			get { return this.Element.UseCompatibleStateImageBehavior; }
			set { this.Element.UseCompatibleStateImageBehavior = value; }
		}
		
		/// <summary>
		/// Gets or sets the HotTracking 
		/// </summary>
		/// <value>
		/// The HotTracking
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HotTracking {
			get { return this.Element.HotTracking; }
			set { this.Element.HotTracking = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowItemToolTips 
		/// </summary>
		/// <value>
		/// The ShowItemToolTips
		/// </value>
		[DefaultValue(true)]
		public System.Boolean ShowItemToolTips {
			get { return this.Element.ShowItemToolTips; }
			set { this.Element.ShowItemToolTips = value; }
		}
		
		/// <summary>
        /// The state of the SmallImageListID property.
        /// </summary>
		private string _SmallImageListID = null;

		/// <summary>
		/// Gets or sets the SmallImageList Reference
		/// </summary>
		/// <value>
		/// The SmallImageList
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string SmallImageListID {
           get
            {
                if (this.Element.SmallImageList != null)
                {
                    return this.Element.SmallImageList.ID;
                }
                return _SmallImageListID;
            }
            set
            {
				// Set the menu id state
				_SmallImageListID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeSmallImageList(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeSmallImageList;
				}
            }
		}

		/// <summary>
        /// Initialize the SmallImageList property.
        /// </summary>
		private void InitializeSmallImageList(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_SmallImageListID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.SmallImageList = component.Element as System.Web.VisualTree.Elements.ImageList;
				}
			}
		}

		/// <summary>
		/// Gets or sets the SmallImageList 
		/// </summary>
		/// <value>
		/// The SmallImageList
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ImageList SmallImageList {
			get { return this.Element.SmallImageList; }
			set { this.Element.SmallImageList = value; }
		}
		
		/// <summary>
		/// Gets or sets the View 
		/// </summary>
		/// <value>
		/// The View
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ItemsView), "LargeIcon")]
		public System.Web.VisualTree.ItemsView View {
			get { return this.Element.View; }
			set { this.Element.View = value; }
		}
		
		/// <summary>
		/// Gets or sets the LabelEdit 
		/// </summary>
		/// <value>
		/// The LabelEdit
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ListLabelEditConstants))]
		public System.Web.VisualTree.ListLabelEditConstants LabelEdit {
			get { return this.Element.LabelEdit; }
			set { this.Element.LabelEdit = value; }
		}
		
		/// <summary>
		/// Gets or sets the LabelWrap 
		/// </summary>
		/// <value>
		/// The LabelWrap
		/// </value>
		[DefaultValue(true)]
		public System.Boolean LabelWrap {
			get { return this.Element.LabelWrap; }
			set { this.Element.LabelWrap = value; }
		}
		
		/// <summary>
		/// Gets or sets the HideSelection 
		/// </summary>
		/// <value>
		/// The HideSelection
		/// </value>
		[DefaultValue(true)]
		public System.Boolean HideSelection {
			get { return this.Element.HideSelection; }
			set { this.Element.HideSelection = value; }
		}
		
		/// <summary>
		/// Gets or sets the GridLines 
		/// </summary>
		/// <value>
		/// The GridLines
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean GridLines {
			get { return this.Element.GridLines; }
			set { this.Element.GridLines = value; }
		}
		
		/// <summary>
		/// Gets or sets the ListViewItemSorter 
		/// </summary>
		/// <value>
		/// The ListViewItemSorter
		/// </value>
		[DefaultValue(null)]
		public System.Collections.IComparer ListViewItemSorter {
			get { return this.Element.ListViewItemSorter; }
			set { this.Element.ListViewItemSorter = value; }
		}
		
		/// <summary>
		/// Gets the SelectedIndexes 
		/// </summary>
		/// <value>
		/// The SelectedIndexes
		/// </value>
		[DefaultValue(-1)]
		public System.Web.VisualTree.Elements.SelectedListViewIndexCollection SelectedIndexes {
			get { return this.Element.SelectedIndexes; }
		}
		
		/// <summary>
		/// Gets or sets the MultiSelect 
		/// </summary>
		/// <value>
		/// The MultiSelect
		/// </value>
		[DefaultValue(true)]
		public System.Boolean MultiSelect {
			get { return this.Element.MultiSelect; }
			set { this.Element.MultiSelect = value; }
		}
		
		/// <summary>
		/// Gets or sets the Sorting 
		/// </summary>
		/// <value>
		/// The Sorting
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.SortOrder), "None")]
		public System.Web.VisualTree.SortOrder Sorting {
			get { return this.Element.Sorting; }
			set { this.Element.Sorting = value; }
		}
		
		/// <summary>
		/// Gets or sets the MenuDisabled 
		/// </summary>
		/// <value>
		/// The MenuDisabled
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean MenuDisabled {
			get { return this.Element.MenuDisabled; }
			set { this.Element.MenuDisabled = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void SetItemForeColor(System.Drawing.Color color, System.Int32 RowIndex, System.Int32 ColumnIndex) {
			this.Element.SetItemForeColor(color, RowIndex, ColumnIndex);
		}

		/// <summary>
		/// 
		/// </summary>
		public void Sort() {
			this.Element.Sort();
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.ValueChangedArgs<System.Int32>> _ItemClick = Component.GetHandler<System.Web.VisualTree.Elements.ValueChangedArgs<System.Int32>>(this.Element, this.ItemClickAction);
			if (_ItemClick != null) { this.Element.ItemClick += _ItemClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ColumnClickEventArgs> _ColumnClick = Component.GetHandler<System.Web.VisualTree.Elements.ColumnClickEventArgs>(this.Element, this.ColumnClickAction);
			if (_ColumnClick != null) { this.Element.ColumnClick += _ColumnClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ItemCheckEventArgs> _ItemChecked = Component.GetHandler<System.Web.VisualTree.Elements.ItemCheckEventArgs>(this.Element, this.ItemCheckedAction);
			if (_ItemChecked != null) { this.Element.ItemChecked += _ItemChecked.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ItemCheckEventArgs> _ItemUnchecked = Component.GetHandler<System.Web.VisualTree.Elements.ItemCheckEventArgs>(this.Element, this.ItemUncheckedAction);
			if (_ItemUnchecked != null) { this.Element.ItemUnchecked += _ItemUnchecked.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.ListViewItemSelectionChangedEventArgs> _SelectionChanged = Component.GetHandler<System.Web.VisualTree.Elements.ListViewItemSelectionChangedEventArgs>(this.Element, this.SelectionChangedAction);
			if (_SelectionChanged != null) { this.Element.SelectionChanged += _SelectionChanged.Invoke; }
        }

		private string _ItemClickAction;

		public string ItemClickAction
        {
            get
			{
				return _ItemClickAction;
			}
			set
			{
				_ItemClickAction = value;
			}            
        }

		private string _ColumnClickAction;

		public string ColumnClickAction
        {
            get
			{
				return _ColumnClickAction;
			}
			set
			{
				_ColumnClickAction = value;
			}            
        }

		private string _ItemCheckedAction;

		public string ItemCheckedAction
        {
            get
			{
				return _ItemCheckedAction;
			}
			set
			{
				_ItemCheckedAction = value;
			}            
        }

		private string _ItemUncheckedAction;

		public string ItemUncheckedAction
        {
            get
			{
				return _ItemUncheckedAction;
			}
			set
			{
				_ItemUncheckedAction = value;
			}            
        }

		private string _SelectionChangedAction;

		public string SelectionChangedAction
        {
            get
			{
				return _SelectionChangedAction;
			}
			set
			{
				_SelectionChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// TListView
    /// </summary>
	public class TListView  : ListView   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TListView"/> class.
        /// </summary>
		public TListView()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TListView"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TListView(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TListViewElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TListViewElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TListViewElement();
        }
 
		/// <summary>
		/// Gets or sets the ItemTemplate 
		/// </summary>
		/// <value>
		/// The ItemTemplate
		/// </value>
		[DefaultValue(null)]
		public System.String ItemTemplate {
			get { return this.Element.ItemTemplate; }
			set { this.Element.ItemTemplate = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TPanel
    /// </summary>
	public class TPanel  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TPanel"/> class.
        /// </summary>
		public TPanel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TPanel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TPanel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TPanelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TPanelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TPanelElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// SplitContainer
    /// </summary>
	public class SplitContainer  : ControlContainer   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SplitContainer"/> class.
        /// </summary>
		public SplitContainer()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="SplitContainer"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal SplitContainer(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.SplitContainer Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.SplitContainer;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.SplitContainer();
        }
 
		/// <summary>
		/// Gets or sets the FixedPanel 
		/// </summary>
		/// <value>
		/// The FixedPanel
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.FixedPanel))]
		public System.Web.VisualTree.FixedPanel FixedPanel {
			get { return this.Element.FixedPanel; }
			set { this.Element.FixedPanel = value; }
		}
		
		/// <summary>
		/// Gets or sets the SplitterDistance 
		/// </summary>
		/// <value>
		/// The SplitterDistance
		/// </value>
		[DefaultValue(50)]
		public System.Int32 SplitterDistance {
			get { return this.Element.SplitterDistance; }
			set { this.Element.SplitterDistance = value; }
		}
		
		/// <summary>
		/// Gets or sets the Panel1MinSize 
		/// </summary>
		/// <value>
		/// The Panel1MinSize
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Panel1MinSize {
			get { return this.Element.Panel1MinSize; }
			set { this.Element.Panel1MinSize = value; }
		}
		
		/// <summary>
		/// Gets or sets the Panel2MinSize 
		/// </summary>
		/// <value>
		/// The Panel2MinSize
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Panel2MinSize {
			get { return this.Element.Panel2MinSize; }
			set { this.Element.Panel2MinSize = value; }
		}
		
		/// <summary>
		/// Gets or sets the Orientation 
		/// </summary>
		/// <value>
		/// The Orientation
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Orientation), "Horizontal")]
		public System.Web.VisualTree.Orientation Orientation {
			get { return this.Element.Orientation; }
			set { this.Element.Orientation = value; }
		}
		
		/// <summary>
		/// Gets or sets the Panel2Collapsed 
		/// </summary>
		/// <value>
		/// The Panel2Collapsed
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Panel2Collapsed {
			get { return this.Element.Panel2Collapsed; }
			set { this.Element.Panel2Collapsed = value; }
		}
		
		/// <summary>
		/// Gets or sets the Panel1Collapsed 
		/// </summary>
		/// <value>
		/// The Panel1Collapsed
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Panel1Collapsed {
			get { return this.Element.Panel1Collapsed; }
			set { this.Element.Panel1Collapsed = value; }
		}
		
		/// <summary>
		/// Gets the Panel1 
		/// </summary>
		/// <value>
		/// The Panel1
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public System.Web.VisualTree.Elements.SplitterPanelElement Panel1 {
			get { return this.Element.Panel1; }
		}
		
		/// <summary>
		/// Gets the Panel2 
		/// </summary>
		/// <value>
		/// The Panel2
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public System.Web.VisualTree.Elements.SplitterPanelElement Panel2 {
			get { return this.Element.Panel2; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TSplitContainer
    /// </summary>
	public class TSplitContainer  : SplitContainer   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TSplitContainer"/> class.
        /// </summary>
		public TSplitContainer()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TSplitContainer"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TSplitContainer(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TSplitContainer Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TSplitContainer;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TSplitContainer();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// SplitterPanel
    /// </summary>
	public class SplitterPanel  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SplitterPanel"/> class.
        /// </summary>
		public SplitterPanel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="SplitterPanel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal SplitterPanel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.SplitterPanelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.SplitterPanelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.SplitterPanelElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TSplitterPanel
    /// </summary>
	public class TSplitterPanel  : SplitterPanel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TSplitterPanel"/> class.
        /// </summary>
		public TSplitterPanel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TSplitterPanel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TSplitterPanel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TSplitterPanelElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TSplitterPanelElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TSplitterPanelElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TTab
    /// </summary>
	public class TTab  : Tab   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TTab"/> class.
        /// </summary>
		public TTab()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TTab"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TTab(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TTabElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TTabElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TTabElement();
        }
 
		/// <summary>
		/// Gets or sets the Layout 
		/// </summary>
		/// <value>
		/// The Layout
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.Touch.TVisualLayout))]
		public System.Web.VisualTree.Elements.Touch.TVisualLayout Layout {
			get { return this.Element.Layout; }
			set { this.Element.Layout = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TabItem
    /// </summary>
	public class TabItem  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TabItem"/> class.
        /// </summary>
		public TabItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TabItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TabItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TabItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TabItem();
        }
 
		/// <summary>
		/// Gets or sets the UseVisualStyleBackColor 
		/// </summary>
		/// <value>
		/// The UseVisualStyleBackColor
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean UseVisualStyleBackColor {
			get { return this.Element.UseVisualStyleBackColor; }
			set { this.Element.UseVisualStyleBackColor = value; }
		}
		
		/// <summary>
		/// Gets or sets the ItemSize 
		/// </summary>
		/// <value>
		/// The ItemSize
		/// </value>
		public System.Drawing.SizeF ItemSize {
			get { return this.Element.ItemSize; }
			set { this.Element.ItemSize = value; }
		}
		
		internal bool ShouldSerializeItemSize() { return this.ItemSize!=System.Drawing.SizeF.Empty;}

			
		public string Image {
			get { return this.GetStringFromResource( this.Element.Image); }
			set { this.Element.Image = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the ImageIndex 
		/// </summary>
		/// <value>
		/// The ImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ImageIndex {
			get { return this.Element.ImageIndex; }
			set { this.Element.ImageIndex = value; }
		}
		
		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _TabActivated = Component.GetHandler<System.EventArgs>(this.Element, this.TabActivatedAction);
			if (_TabActivated != null) { this.Element.TabActivated += _TabActivated.Invoke; }
        }

		private string _TabActivatedAction;

		public string TabActivatedAction
        {
            get
			{
				return _TabActivatedAction;
			}
			set
			{
				_TabActivatedAction = value;
			}            
        }


	}  

	/// <summary>
    /// TTabItem
    /// </summary>
	public class TTabItem  : TabItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TTabItem"/> class.
        /// </summary>
		public TTabItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TTabItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TTabItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TTabItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TTabItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TTabItem();
        }
 
		/// <summary>
		/// Gets or sets the Layout 
		/// </summary>
		/// <value>
		/// The Layout
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.Touch.TVisualLayout))]
		public System.Web.VisualTree.Elements.Touch.TVisualLayout Layout {
			get { return this.Element.Layout; }
			set { this.Element.Layout = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TTextBox
    /// </summary>
	public class TTextBox  : TextBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TTextBox"/> class.
        /// </summary>
		public TTextBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TTextBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TTextBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TTextBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TTextBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TTextBoxElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TTextBoxField
    /// </summary>
	public class TTextBoxField  : TTextBox   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TTextBoxField"/> class.
        /// </summary>
		public TTextBoxField()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TTextBoxField"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TTextBoxField(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TTextBoxFieldElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TTextBoxFieldElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TTextBoxFieldElement();
        }
 
		/// <summary>
		/// Gets or sets the Label 
		/// </summary>
		/// <value>
		/// The Label
		/// </value>
		[DefaultValue(null)]
		public System.String Label {
			get { return this.Element.Label; }
			set { this.Element.Label = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarButton
    /// </summary>
	public class ToolBarButton  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarButton"/> class.
        /// </summary>
		public ToolBarButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarButton Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarButton;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarButton();
        }
 
		/// <summary>
		/// Gets or sets the Checked 
		/// </summary>
		/// <value>
		/// The Checked
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Checked {
			get { return this.Element.Checked; }
			set { this.Element.Checked = value; }
		}
		
		/// <summary>
		/// Gets or sets the DropDownButtonWidth 
		/// </summary>
		/// <value>
		/// The DropDownButtonWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 DropDownButtonWidth {
			get { return this.Element.DropDownButtonWidth; }
			set { this.Element.DropDownButtonWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the ButtonWidth 
		/// </summary>
		/// <value>
		/// The ButtonWidth
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single ButtonWidth {
			get { return this.Element.ButtonWidth; }
			set { this.Element.ButtonWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the ButtonHeight 
		/// </summary>
		/// <value>
		/// The ButtonHeight
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single ButtonHeight {
			get { return this.Element.ButtonHeight; }
			set { this.Element.ButtonHeight = value; }
		}
		
		/// <summary>
		/// Gets or sets the AllowCustomize 
		/// </summary>
		/// <value>
		/// The AllowCustomize
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean AllowCustomize {
			get { return this.Element.AllowCustomize; }
			set { this.Element.AllowCustomize = value; }
		}
		
		/// <summary>
		/// Gets or sets the Wrappable 
		/// </summary>
		/// <value>
		/// The Wrappable
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Wrappable {
			get { return this.Element.Wrappable; }
			set { this.Element.Wrappable = value; }
		}
		
		/// <summary>
		/// Gets or sets the Style 
		/// </summary>
		/// <value>
		/// The Style
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.AppearanceConstants))]
		public System.Web.VisualTree.AppearanceConstants Style {
			get { return this.Element.Style; }
			set { this.Element.Style = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TToolBarButton
    /// </summary>
	public class TToolBarButton  : ToolBarButton   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarButton"/> class.
        /// </summary>
		public TToolBarButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TToolBarButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TToolBarButton Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TToolBarButton;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TToolBarButton();
        }
 
		/// <summary>
		/// Gets or sets the ButtonAppearance 
		/// </summary>
		/// <value>
		/// The ButtonAppearance
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.Touch.TButtonAppearance))]
		public System.Web.VisualTree.Elements.Touch.TButtonAppearance ButtonAppearance {
			get { return this.Element.ButtonAppearance; }
			set { this.Element.ButtonAppearance = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TToolBarDropDownButton
    /// </summary>
	public class TToolBarDropDownButton  : ToolBarDropDownButton   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarDropDownButton"/> class.
        /// </summary>
		public TToolBarDropDownButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarDropDownButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TToolBarDropDownButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TToolBarDropDownButton Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TToolBarDropDownButton;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TToolBarDropDownButton();
        }
 
		/// <summary>
		/// Gets or sets the ButtonAppearance 
		/// </summary>
		/// <value>
		/// The ButtonAppearance
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.Touch.TButtonAppearance))]
		public System.Web.VisualTree.Elements.Touch.TButtonAppearance ButtonAppearance {
			get { return this.Element.ButtonAppearance; }
			set { this.Element.ButtonAppearance = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TToolBar
    /// </summary>
	public class TToolBar  : ToolBar   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBar"/> class.
        /// </summary>
		public TToolBar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TToolBar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TToolBarElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TToolBarElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TToolBarElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TToolBarItem
    /// </summary>
	public class TToolBarItem  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarItem"/> class.
        /// </summary>
		public TToolBarItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TToolBarItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TToolBarItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TToolBarItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TToolBarItem();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TToolBarMenuItem
    /// </summary>
	public class TToolBarMenuItem  : ToolBarMenuItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarMenuItem"/> class.
        /// </summary>
		public TToolBarMenuItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarMenuItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TToolBarMenuItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TToolBarMenuItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TToolBarMenuItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TToolBarMenuItem();
        }
 
		/// <summary>
		/// Gets or sets the Appearance 
		/// </summary>
		/// <value>
		/// The Appearance
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.Touch.TButtonAppearance))]
		public System.Web.VisualTree.Elements.Touch.TButtonAppearance Appearance {
			get { return this.Element.Appearance; }
			set { this.Element.Appearance = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarSeparator
    /// </summary>
	public class ToolBarSeparator  : ToolBarMenuItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarSeparator"/> class.
        /// </summary>
		public ToolBarSeparator()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarSeparator"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarSeparator(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarSeparator Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarSeparator;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarSeparator();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TToolBarSeparator
    /// </summary>
	public class TToolBarSeparator  : ToolBarSeparator   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarSeparator"/> class.
        /// </summary>
		public TToolBarSeparator()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TToolBarSeparator"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TToolBarSeparator(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TToolBarSeparator Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TToolBarSeparator;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TToolBarSeparator();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Tree
    /// </summary>
	[ParseChildren(true)]
	public class Tree  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tree"/> class.
        /// </summary>
		public Tree()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Tree"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Tree(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TreeElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TreeElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TreeElement();
        }
 
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public TreeItemCollection Items {
			get { return new TreeItemCollection(this, this.Element.Items); }
		}
		
		/// <summary>
		/// Gets or sets the CheckBoxes 
		/// </summary>
		/// <value>
		/// The CheckBoxes
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean CheckBoxes {
			get { return this.Element.CheckBoxes; }
			set { this.Element.CheckBoxes = value; }
		}
		
		/// <summary>
		/// Gets or sets the SearchPanel 
		/// </summary>
		/// <value>
		/// The SearchPanel
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean SearchPanel {
			get { return this.Element.SearchPanel; }
			set { this.Element.SearchPanel = value; }
		}
		
		/// <summary>
		/// Gets or sets the DrawMode 
		/// </summary>
		/// <value>
		/// The DrawMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.TreeDrawMode), "Normal")]
		public System.Web.VisualTree.Elements.TreeDrawMode DrawMode {
			get { return this.Element.DrawMode; }
			set { this.Element.DrawMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the HideSelection 
		/// </summary>
		/// <value>
		/// The HideSelection
		/// </value>
		[DefaultValue(true)]
		public System.Boolean HideSelection {
			get { return this.Element.HideSelection; }
			set { this.Element.HideSelection = value; }
		}
		
		/// <summary>
        /// The state of the ImageListID property.
        /// </summary>
		private string _ImageListID = null;

		/// <summary>
		/// Gets or sets the ImageList Reference
		/// </summary>
		/// <value>
		/// The ImageList
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string ImageListID {
           get
            {
                if (this.Element.ImageList != null)
                {
                    return this.Element.ImageList.ID;
                }
                return _ImageListID;
            }
            set
            {
				// Set the menu id state
				_ImageListID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeImageList(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeImageList;
				}
            }
		}

		/// <summary>
        /// Initialize the ImageList property.
        /// </summary>
		private void InitializeImageList(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_ImageListID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.ImageList = component.Element as System.Web.VisualTree.Elements.ImageList;
				}
			}
		}

		/// <summary>
		/// Gets or sets the ImageList 
		/// </summary>
		/// <value>
		/// The ImageList
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ImageList ImageList {
			get { return this.Element.ImageList; }
			set { this.Element.ImageList = value; }
		}
		
		/// <summary>
		/// Gets or sets the LineColor 
		/// </summary>
		/// <value>
		/// The LineColor
		/// </value>
		public System.Drawing.Color LineColor {
			get { return this.Element.LineColor; }
			set { this.Element.LineColor = value; }
		}
		
		internal bool ShouldSerializeLineColor() { return this.LineColor!=System.Drawing.Color.Empty;}

		/// <summary>
        /// The state of the StateImageListID property.
        /// </summary>
		private string _StateImageListID = null;

		/// <summary>
		/// Gets or sets the StateImageList Reference
		/// </summary>
		/// <value>
		/// The StateImageList
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string StateImageListID {
           get
            {
                if (this.Element.StateImageList != null)
                {
                    return this.Element.StateImageList.ID;
                }
                return _StateImageListID;
            }
            set
            {
				// Set the menu id state
				_StateImageListID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeStateImageList(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeStateImageList;
				}
            }
		}

		/// <summary>
        /// Initialize the StateImageList property.
        /// </summary>
		private void InitializeStateImageList(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_StateImageListID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.StateImageList = component.Element as System.Web.VisualTree.Elements.ImageList;
				}
			}
		}

		/// <summary>
		/// Gets or sets the StateImageList 
		/// </summary>
		/// <value>
		/// The StateImageList
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ImageList StateImageList {
			get { return this.Element.StateImageList; }
			set { this.Element.StateImageList = value; }
		}
		
		/// <summary>
		/// Gets or sets the Style 
		/// </summary>
		/// <value>
		/// The Style
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.TreeElementStyles), "TreelinesPlusMinusText")]
		public System.Web.VisualTree.Elements.TreeElementStyles Style {
			get { return this.Element.Style; }
			set { this.Element.Style = value; }
		}
		
		/// <summary>
		/// Gets the Columns 
		/// </summary>
		/// <value>
		/// The Columns
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.TreeColumnCollection Columns {
			get { return this.Element.Columns; }
		}
		
		/// <summary>
		/// Gets or sets the ShowLines 
		/// </summary>
		/// <value>
		/// The ShowLines
		/// </value>
		[DefaultValue(true)]
		public System.Boolean ShowLines {
			get { return this.Element.ShowLines; }
			set { this.Element.ShowLines = value; }
		}
		
		/// <summary>
		/// Gets or sets the Sorted 
		/// </summary>
		/// <value>
		/// The Sorted
		/// </value>
		[DefaultValue(false)]
		public System.Boolean Sorted {
			get { return this.Element.Sorted; }
			set { this.Element.Sorted = value; }
		}
		
		/// <summary>
		/// Gets or sets the LabelEdit 
		/// </summary>
		/// <value>
		/// The LabelEdit
		/// </value>
		[DefaultValue(false)]
		public System.Boolean LabelEdit {
			get { return this.Element.LabelEdit; }
			set { this.Element.LabelEdit = value; }
		}
		
		/// <summary>
		/// Gets or sets the Indent 
		/// </summary>
		/// <value>
		/// The Indent
		/// </value>
		[DefaultValue(19)]
		public System.Int32 Indent {
			get { return this.Element.Indent; }
			set { this.Element.Indent = value; }
		}
		
		/// <summary>
		/// Gets or sets the ParentFieldName 
		/// </summary>
		/// <value>
		/// The ParentFieldName
		/// </value>
		[DefaultValue(null)]
		public System.String ParentFieldName {
			get { return this.Element.ParentFieldName; }
			set { this.Element.ParentFieldName = value; }
		}
		
		/// <summary>
		/// Gets or sets the KeyFieldName 
		/// </summary>
		/// <value>
		/// The KeyFieldName
		/// </value>
		[DefaultValue(null)]
		public System.String KeyFieldName {
			get { return this.Element.KeyFieldName; }
			set { this.Element.KeyFieldName = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void CollapseAll() {
			this.Element.CollapseAll();
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.TreeEventArgs> _AfterSelect = Component.GetHandler<System.Web.VisualTree.Elements.TreeEventArgs>(this.Element, this.AfterSelectAction);
			if (_AfterSelect != null) { this.Element.AfterSelect += _AfterSelect.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeEventArgs> _AfterCheck = Component.GetHandler<System.Web.VisualTree.Elements.TreeEventArgs>(this.Element, this.AfterCheckAction);
			if (_AfterCheck != null) { this.Element.AfterCheck += _AfterCheck.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeEventArgs> _BeforeCheck = Component.GetHandler<System.Web.VisualTree.Elements.TreeEventArgs>(this.Element, this.BeforeCheckAction);
			if (_BeforeCheck != null) { this.Element.BeforeCheck += _BeforeCheck.Invoke; }
			IVisualElementAction<System.EventArgs> _BeforeSelect = Component.GetHandler<System.EventArgs>(this.Element, this.BeforeSelectAction);
			if (_BeforeSelect != null) { this.Element.BeforeSelect += _BeforeSelect.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeMouseClickEventArgs> _NodeMouseClick = Component.GetHandler<System.Web.VisualTree.Elements.TreeMouseClickEventArgs>(this.Element, this.NodeMouseClickAction);
			if (_NodeMouseClick != null) { this.Element.NodeMouseClick += _NodeMouseClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeMouseClickEventArgs> _NodeMouseDoubleClick = Component.GetHandler<System.Web.VisualTree.Elements.TreeMouseClickEventArgs>(this.Element, this.NodeMouseDoubleClickAction);
			if (_NodeMouseDoubleClick != null) { this.Element.NodeMouseDoubleClick += _NodeMouseDoubleClick.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeEventArgs> _BeforeCollapse = Component.GetHandler<System.Web.VisualTree.Elements.TreeEventArgs>(this.Element, this.BeforeCollapseAction);
			if (_BeforeCollapse != null) { this.Element.BeforeCollapse += _BeforeCollapse.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeEventArgs> _BeforeExpand = Component.GetHandler<System.Web.VisualTree.Elements.TreeEventArgs>(this.Element, this.BeforeExpandAction);
			if (_BeforeExpand != null) { this.Element.BeforeExpand += _BeforeExpand.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.TreeEventArgs> _AfterExpand = Component.GetHandler<System.Web.VisualTree.Elements.TreeEventArgs>(this.Element, this.AfterExpandAction);
			if (_AfterExpand != null) { this.Element.AfterExpand += _AfterExpand.Invoke; }
        }

		private string _AfterSelectAction;

		public string AfterSelectAction
        {
            get
			{
				return _AfterSelectAction;
			}
			set
			{
				_AfterSelectAction = value;
			}            
        }

		private string _AfterCheckAction;

		public string AfterCheckAction
        {
            get
			{
				return _AfterCheckAction;
			}
			set
			{
				_AfterCheckAction = value;
			}            
        }

		private string _BeforeCheckAction;

		public string BeforeCheckAction
        {
            get
			{
				return _BeforeCheckAction;
			}
			set
			{
				_BeforeCheckAction = value;
			}            
        }

		private string _BeforeSelectAction;

		public string BeforeSelectAction
        {
            get
			{
				return _BeforeSelectAction;
			}
			set
			{
				_BeforeSelectAction = value;
			}            
        }

		private string _NodeMouseClickAction;

		public string NodeMouseClickAction
        {
            get
			{
				return _NodeMouseClickAction;
			}
			set
			{
				_NodeMouseClickAction = value;
			}            
        }

		private string _NodeMouseDoubleClickAction;

		public string NodeMouseDoubleClickAction
        {
            get
			{
				return _NodeMouseDoubleClickAction;
			}
			set
			{
				_NodeMouseDoubleClickAction = value;
			}            
        }

		private string _BeforeCollapseAction;

		public string BeforeCollapseAction
        {
            get
			{
				return _BeforeCollapseAction;
			}
			set
			{
				_BeforeCollapseAction = value;
			}            
        }

		private string _BeforeExpandAction;

		public string BeforeExpandAction
        {
            get
			{
				return _BeforeExpandAction;
			}
			set
			{
				_BeforeExpandAction = value;
			}            
        }

		private string _AfterExpandAction;

		public string AfterExpandAction
        {
            get
			{
				return _AfterExpandAction;
			}
			set
			{
				_AfterExpandAction = value;
			}            
        }


	}  

	/// <summary>
    /// TTree
    /// </summary>
	public class TTree  : Tree   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TTree"/> class.
        /// </summary>
		public TTree()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TTree"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TTree(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Touch.TTreeElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Touch.TTreeElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Touch.TTreeElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// UpDown
    /// </summary>
	public abstract class UpDown  : ControlContainer   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpDown"/> class.
        /// </summary>
		public UpDown()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="UpDown"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal UpDown(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.UpDownElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.UpDownElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ChartNamed
    /// </summary>
	public abstract class ChartNamed  : ChartItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartNamed"/> class.
        /// </summary>
		public ChartNamed()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartNamed"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ChartNamed(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ChartNamedElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ChartNamedElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ChartAnnotation
    /// </summary>
	public class ChartAnnotation  : ChartNamed   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartAnnotation"/> class.
        /// </summary>
		public ChartAnnotation()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartAnnotation"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ChartAnnotation(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ChartAnnotation Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ChartAnnotation;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ChartAnnotation();
        }
 
		/// <summary>
		/// Gets or sets the AnchorDataPoint 
		/// </summary>
		/// <value>
		/// The AnchorDataPoint
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.DataPoint AnchorDataPoint {
			get { return this.Element.AnchorDataPoint; }
			set { this.Element.AnchorDataPoint = value; }
		}
		
		/// <summary>
		/// Gets or sets the BackColor 
		/// </summary>
		/// <value>
		/// The BackColor
		/// </value>
		public System.Drawing.Color BackColor {
			get { return this.Element.BackColor; }
			set { this.Element.BackColor = value; }
		}
		
		internal bool ShouldSerializeBackColor() { return this.BackColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the Font 
		/// </summary>
		/// <value>
		/// The Font
		/// </value>
		[DefaultValue(null)]
		public System.Drawing.Font Font {
			get { return this.Element.Font; }
			set { this.Element.Font = value; }
		}
		
		/// <summary>
		/// Gets or sets the ForeColor 
		/// </summary>
		/// <value>
		/// The ForeColor
		/// </value>
		public System.Drawing.Color ForeColor {
			get { return this.Element.ForeColor; }
			set { this.Element.ForeColor = value; }
		}
		
		internal bool ShouldSerializeForeColor() { return this.ForeColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the Height 
		/// </summary>
		/// <value>
		/// The Height
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double Height {
			get { return this.Element.Height; }
			set { this.Element.Height = value; }
		}
		
		/// <summary>
		/// Gets or sets the LineColor 
		/// </summary>
		/// <value>
		/// The LineColor
		/// </value>
		public System.Drawing.Color LineColor {
			get { return this.Element.LineColor; }
			set { this.Element.LineColor = value; }
		}
		
		internal bool ShouldSerializeLineColor() { return this.LineColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the Width 
		/// </summary>
		/// <value>
		/// The Width
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double Width {
			get { return this.Element.Width; }
			set { this.Element.Width = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Axis
    /// </summary>
	[ParseChildren(true)]
	public class Axis  : ChartNamed   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Axis"/> class.
        /// </summary>
		public Axis()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Axis"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Axis(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Axis Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Axis;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Axis();
        }
 
		/// <summary>
		/// Gets the AxisName 
		/// </summary>
		/// <value>
		/// The AxisName
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.AxisName))]
		public System.Web.VisualTree.Elements.AxisName AxisName {
			get { return this.Element.AxisName; }
		}
		
		/// <summary>
		/// Gets or sets the AxisType 
		/// </summary>
		/// <value>
		/// The AxisType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.AxisType))]
		public System.Web.VisualTree.Elements.AxisType AxisType {
			get { return this.Element.AxisType; }
			set { this.Element.AxisType = value; }
		}
		
		/// <summary>
		/// Gets the CustomLabels 
		/// </summary>
		/// <value>
		/// The CustomLabels
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public CustomLabelsCollection CustomLabels {
			get { return new CustomLabelsCollection(this, this.Element.CustomLabels); }
		}
		
		/// <summary>
		/// Gets or sets the Enabled 
		/// </summary>
		/// <value>
		/// The Enabled
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.AxisEnabled))]
		public System.Web.VisualTree.Elements.AxisEnabled Enabled {
			get { return this.Element.Enabled; }
			set { this.Element.Enabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the IntervalAutoMode 
		/// </summary>
		/// <value>
		/// The IntervalAutoMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.IntervalAutoMode))]
		public System.Web.VisualTree.Elements.IntervalAutoMode IntervalAutoMode {
			get { return this.Element.IntervalAutoMode; }
			set { this.Element.IntervalAutoMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsLabelAutoFit 
		/// </summary>
		/// <value>
		/// The IsLabelAutoFit
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsLabelAutoFit {
			get { return this.Element.IsLabelAutoFit; }
			set { this.Element.IsLabelAutoFit = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsLogarithmic 
		/// </summary>
		/// <value>
		/// The IsLogarithmic
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsLogarithmic {
			get { return this.Element.IsLogarithmic; }
			set { this.Element.IsLogarithmic = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsMarginVisible 
		/// </summary>
		/// <value>
		/// The IsMarginVisible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsMarginVisible {
			get { return this.Element.IsMarginVisible; }
			set { this.Element.IsMarginVisible = value; }
		}
		
		/// <summary>
		/// Gets or sets the LabelStyle 
		/// </summary>
		/// <value>
		/// The LabelStyle
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.AxisLabelStyle LabelStyle {
			get { return this.Element.LabelStyle; }
			set { this.Element.LabelStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the Maximum 
		/// </summary>
		/// <value>
		/// The Maximum
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double Maximum {
			get { return this.Element.Maximum; }
			set { this.Element.Maximum = value; }
		}
		
		/// <summary>
		/// Gets or sets the Minimum 
		/// </summary>
		/// <value>
		/// The Minimum
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double Minimum {
			get { return this.Element.Minimum; }
			set { this.Element.Minimum = value; }
		}
		
		/// <summary>
		/// Gets or sets the MinorGrid 
		/// </summary>
		/// <value>
		/// The MinorGrid
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ChartingGrid MinorGrid {
			get { return this.Element.MinorGrid; }
			set { this.Element.MinorGrid = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScaleView 
		/// </summary>
		/// <value>
		/// The ScaleView
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.AxisScaleView ScaleView {
			get { return this.Element.ScaleView; }
			set { this.Element.ScaleView = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScrollBar 
		/// </summary>
		/// <value>
		/// The ScrollBar
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.AxisScrollBar ScrollBar {
			get { return this.Element.ScrollBar; }
			set { this.Element.ScrollBar = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextOrientation 
		/// </summary>
		/// <value>
		/// The TextOrientation
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.TextOrientation))]
		public System.Web.VisualTree.Elements.TextOrientation TextOrientation {
			get { return this.Element.TextOrientation; }
			set { this.Element.TextOrientation = value; }
		}
		
		/// <summary>
		/// Gets or sets the Title 
		/// </summary>
		/// <value>
		/// The Title
		/// </value>
		[DefaultValue(null)]
		public System.String Title {
			get { return this.Element.Title; }
			set { this.Element.Title = value; }
		}
		
		/// <summary>
		/// Gets or sets the GapLength 
		/// </summary>
		/// <value>
		/// The GapLength
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double GapLength {
			get { return this.Element.GapLength; }
			set { this.Element.GapLength = value; }
		}
		
		/// <summary>
		/// Gets or sets the TitleFont 
		/// </summary>
		/// <value>
		/// The TitleFont
		/// </value>
		[DefaultValue(null)]
		public System.Drawing.Font TitleFont {
			get { return this.Element.TitleFont; }
			set { this.Element.TitleFont = value; }
		}
		
		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TextAnnotation
    /// </summary>
	public class TextAnnotation  : ChartAnnotation   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextAnnotation"/> class.
        /// </summary>
		public TextAnnotation()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TextAnnotation"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TextAnnotation(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TextAnnotation Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TextAnnotation;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TextAnnotation();
        }
 
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// CalloutAnnotation
    /// </summary>
	public class CalloutAnnotation  : TextAnnotation   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CalloutAnnotation"/> class.
        /// </summary>
		public CalloutAnnotation()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="CalloutAnnotation"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal CalloutAnnotation(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.CalloutAnnotation Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.CalloutAnnotation;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.CalloutAnnotation();
        }
 
		/// <summary>
		/// Gets or sets the CalloutStyle 
		/// </summary>
		/// <value>
		/// The CalloutStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.CalloutStyle))]
		public System.Web.VisualTree.Elements.CalloutStyle CalloutStyle {
			get { return this.Element.CalloutStyle; }
			set { this.Element.CalloutStyle = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Chart
    /// </summary>
	[ParseChildren(true)]
	public class Chart  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Chart"/> class.
        /// </summary>
		public Chart()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Chart"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Chart(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Chart Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Chart;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Chart();
        }
 
		/// <summary>
		/// Gets the Annotations 
		/// </summary>
		/// <value>
		/// The Annotations
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public AnnotationCollection Annotations {
			get { return new AnnotationCollection(this, this.Element.Annotations); }
		}
		
		/// <summary>
		/// Gets the ChartAreas 
		/// </summary>
		/// <value>
		/// The ChartAreas
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public ChartAreaCollection ChartAreas {
			get { return new ChartAreaCollection(this, this.Element.ChartAreas); }
		}
		
		/// <summary>
		/// Gets the Legend 
		/// </summary>
		/// <value>
		/// The Legend
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Legend Legend {
			get { return this.Element.Legend; }
		}
		
		/// <summary>
		/// Gets the Printing 
		/// </summary>
		/// <value>
		/// The Printing
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.PrintingManager Printing {
			get { return this.Element.Printing; }
		}
		
		/// <summary>
		/// Gets the Serializer 
		/// </summary>
		/// <value>
		/// The Serializer
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ChartSerializer Serializer {
			get { return this.Element.Serializer; }
		}
		
		/// <summary>
		/// Gets the Series 
		/// </summary>
		/// <value>
		/// The Series
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public SeriesCollection Series {
			get { return new SeriesCollection(this, this.Element.Series); }
		}
		
		/// <summary>
		/// Gets the Titles 
		/// </summary>
		/// <value>
		/// The Titles
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public TitleCollection Titles {
			get { return new TitleCollection(this, this.Element.Titles); }
		}
		
		/// <summary>
		/// Gets or sets the AxisX 
		/// </summary>
		/// <value>
		/// The AxisX
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Axis AxisX {
			get { return this.Element.AxisX; }
			set { this.Element.AxisX = value; }
		}
		
		/// <summary>
		/// Gets or sets the AxisY 
		/// </summary>
		/// <value>
		/// The AxisY
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Axis AxisY {
			get { return this.Element.AxisY; }
			set { this.Element.AxisY = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _AxisViewChanged = Component.GetHandler<System.EventArgs>(this.Element, this.AxisViewChangedAction);
			if (_AxisViewChanged != null) { this.Element.AxisViewChanged += _AxisViewChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _CursorPositionChanging = Component.GetHandler<System.EventArgs>(this.Element, this.CursorPositionChangingAction);
			if (_CursorPositionChanging != null) { this.Element.CursorPositionChanging += _CursorPositionChanging.Invoke; }
			IVisualElementAction<System.EventArgs> _LabelFormatting = Component.GetHandler<System.EventArgs>(this.Element, this.LabelFormattingAction);
			if (_LabelFormatting != null) { this.Element.LabelFormatting += _LabelFormatting.Invoke; }
        }

		private string _AxisViewChangedAction;

		public string AxisViewChangedAction
        {
            get
			{
				return _AxisViewChangedAction;
			}
			set
			{
				_AxisViewChangedAction = value;
			}            
        }

		private string _CursorPositionChangingAction;

		public string CursorPositionChangingAction
        {
            get
			{
				return _CursorPositionChangingAction;
			}
			set
			{
				_CursorPositionChangingAction = value;
			}            
        }

		private string _LabelFormattingAction;

		public string LabelFormattingAction
        {
            get
			{
				return _LabelFormattingAction;
			}
			set
			{
				_LabelFormattingAction = value;
			}            
        }


	}  

	/// <summary>
    /// ChartArea
    /// </summary>
	public class ChartArea  : ChartNamed   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartArea"/> class.
        /// </summary>
		public ChartArea()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartArea"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ChartArea(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ChartArea Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ChartArea;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ChartArea();
        }
 
		/// <summary>
		/// Gets the Grid 
		/// </summary>
		/// <value>
		/// The Grid
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ChartingGrid Grid {
			get { return this.Element.Grid; }
		}
		
		/// <summary>
		/// Gets or sets the Orientation 
		/// </summary>
		/// <value>
		/// The Orientation
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Orientation), "Vertical")]
		public System.Web.VisualTree.Orientation Orientation {
			get { return this.Element.Orientation; }
			set { this.Element.Orientation = value; }
		}
		
		/// <summary>
		/// Gets or sets the Visible 
		/// </summary>
		/// <value>
		/// The Visible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Visible {
			get { return this.Element.Visible; }
			set { this.Element.Visible = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowGrid 
		/// </summary>
		/// <value>
		/// The ShowGrid
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ShowGrid {
			get { return this.Element.ShowGrid; }
			set { this.Element.ShowGrid = value; }
		}
		
		/// <summary>
		/// Gets or sets the AlignWithChartArea 
		/// </summary>
		/// <value>
		/// The AlignWithChartArea
		/// </value>
		[DefaultValue(null)]
		public System.String AlignWithChartArea {
			get { return this.Element.AlignWithChartArea; }
			set { this.Element.AlignWithChartArea = value; }
		}
		
		/// <summary>
		/// Gets or sets the CursorY 
		/// </summary>
		/// <value>
		/// The CursorY
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ChartCursor CursorY {
			get { return this.Element.CursorY; }
			set { this.Element.CursorY = value; }
		}
		
		/// <summary>
		/// Gets or sets the CursorX 
		/// </summary>
		/// <value>
		/// The CursorX
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ChartCursor CursorX {
			get { return this.Element.CursorX; }
			set { this.Element.CursorX = value; }
		}
		
		/// <summary>
		/// Gets or sets the AlignmentOrientation 
		/// </summary>
		/// <value>
		/// The AlignmentOrientation
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.AreaAlignmentOrientations))]
		public System.Web.VisualTree.Elements.AreaAlignmentOrientations AlignmentOrientation {
			get { return this.Element.AlignmentOrientation; }
			set { this.Element.AlignmentOrientation = value; }
		}
		
		/// <summary>
		/// Gets or sets the AlignmentStyle 
		/// </summary>
		/// <value>
		/// The AlignmentStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.AreaAlignmentStyles))]
		public System.Web.VisualTree.Elements.AreaAlignmentStyles AlignmentStyle {
			get { return this.Element.AlignmentStyle; }
			set { this.Element.AlignmentStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the BorderWidth 
		/// </summary>
		/// <value>
		/// The BorderWidth
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 BorderWidth {
			get { return this.Element.BorderWidth; }
			set { this.Element.BorderWidth = value; }
		}
		
		/// <summary>
		/// Gets or sets the BorderDashStyle 
		/// </summary>
		/// <value>
		/// The BorderDashStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ChartDashStyle))]
		public System.Web.VisualTree.Elements.ChartDashStyle BorderDashStyle {
			get { return this.Element.BorderDashStyle; }
			set { this.Element.BorderDashStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the Axes 
		/// </summary>
		/// <value>
		/// The Axes
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Axis[] Axes {
			get { return this.Element.Axes; }
			set { this.Element.Axes = value; }
		}
		
		/// <summary>
		/// Gets or sets the AxisX 
		/// </summary>
		/// <value>
		/// The AxisX
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Axis AxisX {
			get { return this.Element.AxisX; }
			set { this.Element.AxisX = value; }
		}
		
		/// <summary>
		/// Gets or sets the AxisY 
		/// </summary>
		/// <value>
		/// The AxisY
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.Axis AxisY {
			get { return this.Element.AxisY; }
			set { this.Element.AxisY = value; }
		}
		
		/// <summary>
		/// Gets or sets the BackImageWrapMode 
		/// </summary>
		/// <value>
		/// The BackImageWrapMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ChartImageWrapMode))]
		public System.Web.VisualTree.Elements.ChartImageWrapMode BackImageWrapMode {
			get { return this.Element.BackImageWrapMode; }
			set { this.Element.BackImageWrapMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the InnerPlotPosition 
		/// </summary>
		/// <value>
		/// The InnerPlotPosition
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ElementPosition InnerPlotPosition {
			get { return this.Element.InnerPlotPosition; }
			set { this.Element.InnerPlotPosition = value; }
		}
		
		/// <summary>
		/// Gets or sets the Position 
		/// </summary>
		/// <value>
		/// The Position
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ElementPosition Position {
			get { return this.Element.Position; }
			set { this.Element.Position = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// CustomLabel
    /// </summary>
	public class CustomLabel  : ChartNamed   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomLabel"/> class.
        /// </summary>
		public CustomLabel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomLabel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal CustomLabel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.CustomLabel Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.CustomLabel;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.CustomLabel();
        }
 
		/// <summary>
		/// Gets or sets the GridTicks 
		/// </summary>
		/// <value>
		/// The GridTicks
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.GridTickTypes))]
		public System.Web.VisualTree.Elements.GridTickTypes GridTicks {
			get { return this.Element.GridTicks; }
			set { this.Element.GridTicks = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataPointCustomProperties
    /// </summary>
	public class DataPointCustomProperties  : ChartNamed   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataPointCustomProperties"/> class.
        /// </summary>
		public DataPointCustomProperties()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPointCustomProperties"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataPointCustomProperties(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataPointCustomProperties Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataPointCustomProperties;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataPointCustomProperties();
        }
 
		/// <summary>
		/// Gets or sets the BackHatchStyle 
		/// </summary>
		/// <value>
		/// The BackHatchStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ChartHatchStyle))]
		public System.Web.VisualTree.Elements.ChartHatchStyle BackHatchStyle {
			get { return this.Element.BackHatchStyle; }
			set { this.Element.BackHatchStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the BorderColor 
		/// </summary>
		/// <value>
		/// The BorderColor
		/// </value>
		public System.Drawing.Color BorderColor {
			get { return this.Element.BorderColor; }
			set { this.Element.BorderColor = value; }
		}
		
		internal bool ShouldSerializeBorderColor() { return this.BorderColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the BorderDashStyle 
		/// </summary>
		/// <value>
		/// The BorderDashStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ChartDashStyle))]
		public System.Web.VisualTree.Elements.ChartDashStyle BorderDashStyle {
			get { return this.Element.BorderDashStyle; }
			set { this.Element.BorderDashStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the Color 
		/// </summary>
		/// <value>
		/// The Color
		/// </value>
		public System.Drawing.Color Color {
			get { return this.Element.Color; }
			set { this.Element.Color = value; }
		}
		
		internal bool ShouldSerializeColor() { return this.Color!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the MarkerColor 
		/// </summary>
		/// <value>
		/// The MarkerColor
		/// </value>
		public System.Drawing.Color MarkerColor {
			get { return this.Element.MarkerColor; }
			set { this.Element.MarkerColor = value; }
		}
		
		internal bool ShouldSerializeMarkerColor() { return this.MarkerColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the MarkerSize 
		/// </summary>
		/// <value>
		/// The MarkerSize
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 MarkerSize {
			get { return this.Element.MarkerSize; }
			set { this.Element.MarkerSize = value; }
		}
		
		/// <summary>
		/// Gets or sets the MarkerStyle 
		/// </summary>
		/// <value>
		/// The MarkerStyle
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.MarkerStyle))]
		public System.Web.VisualTree.Elements.MarkerStyle MarkerStyle {
			get { return this.Element.MarkerStyle; }
			set { this.Element.MarkerStyle = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DataPoint
    /// </summary>
	public class DataPoint  : DataPointCustomProperties   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataPoint"/> class.
        /// </summary>
		public DataPoint()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DataPoint"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DataPoint(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DataPoint Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DataPoint;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DataPoint();
        }
 
		/// <summary>
		/// Gets or sets the YValues 
		/// </summary>
		/// <value>
		/// The YValues
		/// </value>
		[DefaultValue(null)]
		public System.Double[] YValues {
			get { return this.Element.YValues; }
			set { this.Element.YValues = value; }
		}
		
		/// <summary>
		/// Gets or sets the XValue 
		/// </summary>
		/// <value>
		/// The XValue
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double XValue {
			get { return this.Element.XValue; }
			set { this.Element.XValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the YValue 
		/// </summary>
		/// <value>
		/// The YValue
		/// </value>
		public System.Nullable<System.Double> YValue {
			get { return this.Element.YValue; }
			set { this.Element.YValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the AxisLabel 
		/// </summary>
		/// <value>
		/// The AxisLabel
		/// </value>
		[DefaultValue(null)]
		public System.String AxisLabel {
			get { return this.Element.AxisLabel; }
			set { this.Element.AxisLabel = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Position
    /// </summary>
	public class Position  : ChartItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Position"/> class.
        /// </summary>
		public Position()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Position"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Position(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ElementPosition Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ElementPosition;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ElementPosition();
        }
 
		/// <summary>
		/// Gets or sets the Auto 
		/// </summary>
		/// <value>
		/// The Auto
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Auto {
			get { return this.Element.Auto; }
			set { this.Element.Auto = value; }
		}
		
		/// <summary>
		/// Gets or sets the Height 
		/// </summary>
		/// <value>
		/// The Height
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single Height {
			get { return this.Element.Height; }
			set { this.Element.Height = value; }
		}
		
		/// <summary>
		/// Gets or sets the Width 
		/// </summary>
		/// <value>
		/// The Width
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single Width {
			get { return this.Element.Width; }
			set { this.Element.Width = value; }
		}
		
		/// <summary>
		/// Gets or sets the X 
		/// </summary>
		/// <value>
		/// The X
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single X {
			get { return this.Element.X; }
			set { this.Element.X = value; }
		}
		
		/// <summary>
		/// Gets or sets the Y 
		/// </summary>
		/// <value>
		/// The Y
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single Y {
			get { return this.Element.Y; }
			set { this.Element.Y = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// AxisLabelStyle
    /// </summary>
	public class AxisLabelStyle  : ChartItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AxisLabelStyle"/> class.
        /// </summary>
		public AxisLabelStyle()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="AxisLabelStyle"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal AxisLabelStyle(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.AxisLabelStyle Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.AxisLabelStyle;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.AxisLabelStyle();
        }
 
		/// <summary>
		/// Gets or sets the Enabled 
		/// </summary>
		/// <value>
		/// The Enabled
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Enabled {
			get { return this.Element.Enabled; }
			set { this.Element.Enabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the Format 
		/// </summary>
		/// <value>
		/// The Format
		/// </value>
		[DefaultValue(null)]
		public System.String Format {
			get { return this.Element.Format; }
			set { this.Element.Format = value; }
		}
		
		/// <summary>
		/// Gets or sets the Interval 
		/// </summary>
		/// <value>
		/// The Interval
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double Interval {
			get { return this.Element.Interval; }
			set { this.Element.Interval = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Legend
    /// </summary>
	public class Legend  : ChartNamed   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Legend"/> class.
        /// </summary>
		public Legend()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Legend"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Legend(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Legend Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Legend;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Legend();
        }
 
		/// <summary>
		/// Gets or sets the Enabled 
		/// </summary>
		/// <value>
		/// The Enabled
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Enabled {
			get { return this.Element.Enabled; }
			set { this.Element.Enabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the Position 
		/// </summary>
		/// <value>
		/// The Position
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.LegendPosition))]
		public System.Web.VisualTree.Elements.LegendPosition Position {
			get { return this.Element.Position; }
			set { this.Element.Position = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Series
    /// </summary>
	public class Series  : DataPointCustomProperties   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Series"/> class.
        /// </summary>
		public Series()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Series"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Series(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.Series Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.Series;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.Series();
        }
 
		/// <summary>
		/// Gets or sets the Enabled 
		/// </summary>
		/// <value>
		/// The Enabled
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Enabled {
			get { return this.Element.Enabled; }
			set { this.Element.Enabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the ChartType 
		/// </summary>
		/// <value>
		/// The ChartType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.SeriesChartType))]
		public System.Web.VisualTree.Elements.SeriesChartType ChartType {
			get { return this.Element.ChartType; }
			set { this.Element.ChartType = value; }
		}
		
		/// <summary>
		/// Gets or sets the ChartAreaID 
		/// </summary>
		/// <value>
		/// The ChartAreaID
		/// </value>
		[DefaultValue(null)]
		public System.String ChartAreaID {
			get { return this.Element.ChartAreaID; }
			set { this.Element.ChartAreaID = value; }
		}
		
		/// <summary>
		/// Gets or sets the CombineMode 
		/// </summary>
		/// <value>
		/// The CombineMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ChartSeriesCombineMode))]
		public System.Web.VisualTree.Elements.ChartSeriesCombineMode CombineMode {
			get { return this.Element.CombineMode; }
			set { this.Element.CombineMode = value; }
		}
		
		/// <summary>
		/// Gets the ChartArea 
		/// </summary>
		/// <value>
		/// The ChartArea
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ChartArea ChartArea {
			get { return this.Element.ChartArea; }
		}
		
		/// <summary>
		/// Gets or sets the Legend 
		/// </summary>
		/// <value>
		/// The Legend
		/// </value>
		[DefaultValue(null)]
		public System.String Legend {
			get { return this.Element.Legend; }
			set { this.Element.Legend = value; }
		}
		
		/// <summary>
		/// Gets the Points 
		/// </summary>
		/// <value>
		/// The Points
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.DataPointCollection Points {
			get { return this.Element.Points; }
		}
		
		/// <summary>
		/// Gets the LabelStyle 
		/// </summary>
		/// <value>
		/// The LabelStyle
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.SeriesLabelStyle LabelStyle {
			get { return this.Element.LabelStyle; }
		}
		
		/// <summary>
		/// Gets the DrawLinesToLabels 
		/// </summary>
		/// <value>
		/// The DrawLinesToLabels
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.SeriesLabelStyle DrawLinesToLabels {
			get { return this.Element.DrawLinesToLabels; }
		}
		
		/// <summary>
		/// Gets or sets the XValueType 
		/// </summary>
		/// <value>
		/// The XValueType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ChartValueType))]
		public System.Web.VisualTree.Elements.ChartValueType XValueType {
			get { return this.Element.XValueType; }
			set { this.Element.XValueType = value; }
		}
		
		/// <summary>
		/// Gets or sets the YValueType 
		/// </summary>
		/// <value>
		/// The YValueType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.ChartValueType))]
		public System.Web.VisualTree.Elements.ChartValueType YValueType {
			get { return this.Element.YValueType; }
			set { this.Element.YValueType = value; }
		}
		
		/// <summary>
		/// Gets or sets the YValuesPerPoint 
		/// </summary>
		/// <value>
		/// The YValuesPerPoint
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 YValuesPerPoint {
			get { return this.Element.YValuesPerPoint; }
			set { this.Element.YValuesPerPoint = value; }
		}
		
		/// <summary>
		/// Gets or sets the Title 
		/// </summary>
		/// <value>
		/// The Title
		/// </value>
		[DefaultValue(null)]
		public System.String Title {
			get { return this.Element.Title; }
			set { this.Element.Title = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ChartTitle
    /// </summary>
	public class ChartTitle  : ChartNamed   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartTitle"/> class.
        /// </summary>
		public ChartTitle()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartTitle"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ChartTitle(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ChartTitle Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ChartTitle;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ChartTitle();
        }
 
		/// <summary>
		/// Gets or sets the Visible 
		/// </summary>
		/// <value>
		/// The Visible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Visible {
			get { return this.Element.Visible; }
			set { this.Element.Visible = value; }
		}
		
		/// <summary>
		/// Gets or sets the Position 
		/// </summary>
		/// <value>
		/// The Position
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ElementPosition Position {
			get { return this.Element.Position; }
			set { this.Element.Position = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Composite
    /// </summary>
	public class Composite  : CompositeBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Composite"/> class.
        /// </summary>
		public Composite()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Composite"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Composite(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.CompositeElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.CompositeElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.CompositeElement();
        }
 
		/// <summary>
		/// 
		/// </summary>
		public void RemoveComposite() {
			this.Element.RemoveComposite();
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.ComponentModel.CancelEventArgs> _Unloading = Component.GetHandler<System.ComponentModel.CancelEventArgs>(this.Element, this.UnloadingAction);
			if (_Unloading != null) { this.Element.Unloading += _Unloading.Invoke; }
			IVisualElementAction<System.ComponentModel.CancelEventArgs> _Unload = Component.GetHandler<System.ComponentModel.CancelEventArgs>(this.Element, this.UnloadAction);
			if (_Unload != null) { this.Element.Unload += _Unload.Invoke; }
        }

		private string _UnloadingAction;

		public string UnloadingAction
        {
            get
			{
				return _UnloadingAction;
			}
			set
			{
				_UnloadingAction = value;
			}            
        }

		private string _UnloadAction;

		public string UnloadAction
        {
            get
			{
				return _UnloadAction;
			}
			set
			{
				_UnloadAction = value;
			}            
        }


	}  

	/// <summary>
    /// Menu
    /// </summary>
	public class Menu  : ToolBar   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
		public Menu()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Menu(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.MenuElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.MenuElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.MenuElement();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Timer
    /// </summary>
	public class Timer  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Timer"/> class.
        /// </summary>
		public Timer()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Timer"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Timer(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TimerElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TimerElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TimerElement();
        }
 
		/// <summary>
		/// Gets or sets the Enabled 
		/// </summary>
		/// <value>
		/// The Enabled
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Enabled {
			get { return this.Element.Enabled; }
			set { this.Element.Enabled = value; }
		}
		
		/// <summary>
		/// Gets or sets the Interval 
		/// </summary>
		/// <value>
		/// The Interval
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Interval {
			get { return this.Element.Interval; }
			set { this.Element.Interval = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Tick = Component.GetHandler<System.EventArgs>(this.Element, this.TickAction);
			if (_Tick != null) { this.Element.Tick += _Tick.Invoke; }
        }

		private string _TickAction;

		public string TickAction
        {
            get
			{
				return _TickAction;
			}
			set
			{
				_TickAction = value;
			}            
        }


	}  

	/// <summary>
    /// CheckBox
    /// </summary>
	public class CheckBox  : ButtonBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
		public CheckBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal CheckBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.CheckBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.CheckBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.CheckBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the AutoCheck 
		/// </summary>
		/// <value>
		/// The AutoCheck
		/// </value>
		[DefaultValue(true)]
		public System.Boolean AutoCheck {
			get { return this.Element.AutoCheck; }
			set { this.Element.AutoCheck = value; }
		}
		
		/// <summary>
		/// Gets or sets the Appearance 
		/// </summary>
		/// <value>
		/// The Appearance
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Appearance), "Normal")]
		public System.Web.VisualTree.Appearance Appearance {
			get { return this.Element.Appearance; }
			set { this.Element.Appearance = value; }
		}
		
		/// <summary>
		/// Gets or sets the ThreeState 
		/// </summary>
		/// <value>
		/// The ThreeState
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ThreeState {
			get { return this.Element.ThreeState; }
			set { this.Element.ThreeState = value; }
		}
		
		/// <summary>
		/// Gets or sets the CheckAlign 
		/// </summary>
		/// <value>
		/// The CheckAlign
		/// </value>
		[DefaultValue(typeof(System.Drawing.ContentAlignment), "MiddleLeft")]
		public System.Drawing.ContentAlignment CheckAlign {
			get { return this.Element.CheckAlign; }
			set { this.Element.CheckAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the CheckState 
		/// </summary>
		/// <value>
		/// The CheckState
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.CheckState))]
		public System.Web.VisualTree.CheckState CheckState {
			get { return this.Element.CheckState; }
			set { this.Element.CheckState = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsChecked 
		/// </summary>
		/// <value>
		/// The IsChecked
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsChecked {
			get { return this.Element.IsChecked; }
			set { this.Element.IsChecked = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _CheckStateChanged = Component.GetHandler<System.EventArgs>(this.Element, this.CheckStateChangedAction);
			if (_CheckStateChanged != null) { this.Element.CheckStateChanged += _CheckStateChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _CheckedChanged = Component.GetHandler<System.EventArgs>(this.Element, this.CheckedChangedAction);
			if (_CheckedChanged != null) { this.Element.CheckedChanged += _CheckedChanged.Invoke; }
        }

		private string _CheckStateChangedAction;

		public string CheckStateChangedAction
        {
            get
			{
				return _CheckStateChangedAction;
			}
			set
			{
				_CheckStateChangedAction = value;
			}            
        }

		private string _CheckedChangedAction;

		public string CheckedChangedAction
        {
            get
			{
				return _CheckedChangedAction;
			}
			set
			{
				_CheckedChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// CheckedListBox
    /// </summary>
	public class CheckedListBox  : ListControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckedListBox"/> class.
        /// </summary>
		public CheckedListBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckedListBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal CheckedListBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.CheckedListBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.CheckedListBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.CheckedListBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the MultiColumn 
		/// </summary>
		/// <value>
		/// The MultiColumn
		/// </value>
		[DefaultValue(false)]
		public System.Boolean MultiColumn {
			get { return this.Element.MultiColumn; }
			set { this.Element.MultiColumn = value; }
		}
		
		/// <summary>
		/// Gets or sets the CheckOnClick 
		/// </summary>
		/// <value>
		/// The CheckOnClick
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean CheckOnClick {
			get { return this.Element.CheckOnClick; }
			set { this.Element.CheckOnClick = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedIndexsArray 
		/// </summary>
		/// <value>
		/// The SelectedIndexsArray
		/// </value>
		[DefaultValue(-1)]
		public System.Int32[] SelectedIndexsArray {
			get { return this.Element.SelectedIndexsArray; }
			set { this.Element.SelectedIndexsArray = value; }
		}
		
		/// <summary>
		/// Gets or sets the ThreeDCheckBoxes 
		/// </summary>
		/// <value>
		/// The ThreeDCheckBoxes
		/// </value>
		[DefaultValue(true)]
		public System.Boolean ThreeDCheckBoxes {
			get { return this.Element.ThreeDCheckBoxes; }
			set { this.Element.ThreeDCheckBoxes = value; }
		}
		
		/// <summary>
		/// Gets the CheckedItems 
		/// </summary>
		/// <value>
		/// The CheckedItems
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ListItemCollection CheckedItems {
			get { return this.Element.CheckedItems; }
		}
		
		/// <summary>
		/// Gets or sets the AutoScroll 
		/// </summary>
		/// <value>
		/// The AutoScroll
		/// </value>
		[DefaultValue(false)]
		public System.Boolean AutoScroll {
			get { return this.Element.AutoScroll; }
			set { this.Element.AutoScroll = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void SetItemChecked(System.Int32 index, System.Boolean isChecked) {
			this.Element.SetItemChecked(index, isChecked);
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.ItemCheckEventArgs> _ItemCheck = Component.GetHandler<System.Web.VisualTree.Elements.ItemCheckEventArgs>(this.Element, this.ItemCheckAction);
			if (_ItemCheck != null) { this.Element.ItemCheck += _ItemCheck.Invoke; }
        }

		private string _ItemCheckAction;

		public string ItemCheckAction
        {
            get
			{
				return _ItemCheckAction;
			}
			set
			{
				_ItemCheckAction = value;
			}            
        }


	}  

	/// <summary>
    /// ColumnHeader
    /// </summary>
	public class ColumnHeader  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnHeader"/> class.
        /// </summary>
		public ColumnHeader()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnHeader"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ColumnHeader(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ColumnHeader Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ColumnHeader;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ColumnHeader();
        }
 
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextAlign 
		/// </summary>
		/// <value>
		/// The TextAlign
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.HorizontalAlignment))]
		public System.Web.VisualTree.HorizontalAlignment TextAlign {
			get { return this.Element.TextAlign; }
			set { this.Element.TextAlign = value; }
		}
		
		/// <summary>
		/// Gets or sets the Width 
		/// </summary>
		/// <value>
		/// The Width
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Width {
			get { return this.Element.Width; }
			set { this.Element.Width = value; }
		}
		
		/// <summary>
		/// Gets or sets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
			set { this.Element.Index = value; }
		}
		
		/// <summary>
		/// Gets or sets the SortType 
		/// </summary>
		/// <value>
		/// The SortType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.ColumnSortMode))]
		public System.Web.VisualTree.ColumnSortMode SortType {
			get { return this.Element.SortType; }
			set { this.Element.SortType = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridButtonColumn
    /// </summary>
	public class GridButtonColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridButtonColumn"/> class.
        /// </summary>
		public GridButtonColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridButtonColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridButtonColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridButtonColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridButtonColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridButtonColumn();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridCheckBoxColumn
    /// </summary>
	public class GridCheckBoxColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridCheckBoxColumn"/> class.
        /// </summary>
		public GridCheckBoxColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCheckBoxColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridCheckBoxColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridCheckBoxColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridCheckBoxColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridCheckBoxColumn();
        }
 
		/// <summary>
		/// Sets the FalseValue 
		/// </summary>
		/// <value>
		/// The FalseValue
		/// </value>
		[DefaultValue(null)]
		public System.Object FalseValue {
			set { this.Element.FalseValue = value; }
		}
		
		/// <summary>
		/// Sets the TrueValue 
		/// </summary>
		/// <value>
		/// The TrueValue
		/// </value>
		[DefaultValue(null)]
		public System.Object TrueValue {
			set { this.Element.TrueValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the CheckState 
		/// </summary>
		/// <value>
		/// The CheckState
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.CheckState))]
		public System.Web.VisualTree.CheckState CheckState {
			get { return this.Element.CheckState; }
			set { this.Element.CheckState = value; }
		}
		
		/// <summary>
		/// Gets or sets the ThreeState 
		/// </summary>
		/// <value>
		/// The ThreeState
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ThreeState {
			get { return this.Element.ThreeState; }
			set { this.Element.ThreeState = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _CheckedChange = Component.GetHandler<System.EventArgs>(this.Element, this.CheckedChangeAction);
			if (_CheckedChange != null) { this.Element.CheckedChange += _CheckedChange.Invoke; }
			IVisualElementAction<System.ComponentModel.CancelEventArgs> _BeforeChecked = Component.GetHandler<System.ComponentModel.CancelEventArgs>(this.Element, this.BeforeCheckedAction);
			if (_BeforeChecked != null) { this.Element.BeforeChecked += _BeforeChecked.Invoke; }
        }

		private string _CheckedChangeAction;

		public string CheckedChangeAction
        {
            get
			{
				return _CheckedChangeAction;
			}
			set
			{
				_CheckedChangeAction = value;
			}            
        }

		private string _BeforeCheckedAction;

		public string BeforeCheckedAction
        {
            get
			{
				return _BeforeCheckedAction;
			}
			set
			{
				_BeforeCheckedAction = value;
			}            
        }


	}  

	/// <summary>
    /// GridHeaderCell
    /// </summary>
	public class GridHeaderCell  : GridCell   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridHeaderCell"/> class.
        /// </summary>
		public GridHeaderCell()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridHeaderCell"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridHeaderCell(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridHeaderCell Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridHeaderCell;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridHeaderCell();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridColumnHeaderCell
    /// </summary>
	public class GridColumnHeaderCell  : GridHeaderCell   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnHeaderCell"/> class.
        /// </summary>
		public GridColumnHeaderCell()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnHeaderCell"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridColumnHeaderCell(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridColumnHeaderCell Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridColumnHeaderCell;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridColumnHeaderCell();
        }
 
		/// <summary>
		/// Gets or sets the AutoSizeMode 
		/// </summary>
		/// <value>
		/// The AutoSizeMode
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.Elements.GridAutoSizeColumnMode))]
		public System.Web.VisualTree.Elements.GridAutoSizeColumnMode AutoSizeMode {
			get { return this.Element.AutoSizeMode; }
			set { this.Element.AutoSizeMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the HeaderCell 
		/// </summary>
		/// <value>
		/// The HeaderCell
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.GridHeaderCell HeaderCell {
			get { return this.Element.HeaderCell; }
			set { this.Element.HeaderCell = value; }
		}
		
		/// <summary>
		/// Gets or sets the SortGlyphDirection 
		/// </summary>
		/// <value>
		/// The SortGlyphDirection
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.SortOrder))]
		public System.Web.VisualTree.SortOrder SortGlyphDirection {
			get { return this.Element.SortGlyphDirection; }
			set { this.Element.SortGlyphDirection = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridImageColumn
    /// </summary>
	public class GridImageColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridImageColumn"/> class.
        /// </summary>
		public GridImageColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridImageColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridImageColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridImageColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridImageColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridImageColumn();
        }
 
		/// <summary>
		/// Gets or sets the Image 
		/// </summary>
		/// <value>
		/// The Image
		/// </value>
		[DefaultValue(null)]
		public System.String Image {
			get { return this.Element.Image; }
			set { this.Element.Image = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridRow
    /// </summary>
	public class GridRow  : GridBand   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridRow"/> class.
        /// </summary>
		public GridRow()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridRow"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridRow(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridRow Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridRow;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridRow();
        }
 
		/// <summary>
		/// Gets or sets the ReadOnly 
		/// </summary>
		/// <value>
		/// The ReadOnly
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ReadOnly {
			get { return this.Element.ReadOnly; }
			set { this.Element.ReadOnly = value; }
		}
		
		/// <summary>
		/// Gets or sets the Selected 
		/// </summary>
		/// <value>
		/// The Selected
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Selected {
			get { return this.Element.Selected; }
			set { this.Element.Selected = value; }
		}
		
		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
		/// <summary>
		/// Gets the WidgetControls 
		/// </summary>
		/// <value>
		/// The WidgetControls
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.List<System.Web.VisualTree.Elements.ControlElement> WidgetControls {
			get { return this.Element.WidgetControls; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// GridTextBoxColumn
    /// </summary>
	public class GridTextBoxColumn  : GridColumn   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextBoxColumn"/> class.
        /// </summary>
		public GridTextBoxColumn()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextBoxColumn"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal GridTextBoxColumn(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.GridTextBoxColumn Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.GridTextBoxColumn;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.GridTextBoxColumn();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ScrollBar
    /// </summary>
	public abstract class ScrollBar  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollBar"/> class.
        /// </summary>
		public ScrollBar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollBar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ScrollBar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ScrollBarElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ScrollBarElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the LargeChange 
		/// </summary>
		/// <value>
		/// The LargeChange
		/// </value>
		[DefaultValue(10)]
		public System.Int32 LargeChange {
			get { return this.Element.LargeChange; }
			set { this.Element.LargeChange = value; }
		}
		
		/// <summary>
		/// Gets or sets the SmallChange 
		/// </summary>
		/// <value>
		/// The SmallChange
		/// </value>
		[DefaultValue(1)]
		public System.Int32 SmallChange {
			get { return this.Element.SmallChange; }
			set { this.Element.SmallChange = value; }
		}
		
		/// <summary>
		/// Gets or sets the Maximum 
		/// </summary>
		/// <value>
		/// The Maximum
		/// </value>
		[DefaultValue(100)]
		public System.Int32 Maximum {
			get { return this.Element.Maximum; }
			set { this.Element.Maximum = value; }
		}
		
		/// <summary>
		/// Gets or sets the Minimum 
		/// </summary>
		/// <value>
		/// The Minimum
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Minimum {
			get { return this.Element.Minimum; }
			set { this.Element.Minimum = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _ValueChanged = Component.GetHandler<System.EventArgs>(this.Element, this.ValueChangedAction);
			if (_ValueChanged != null) { this.Element.ValueChanged += _ValueChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _Scroll = Component.GetHandler<System.EventArgs>(this.Element, this.ScrollAction);
			if (_Scroll != null) { this.Element.Scroll += _Scroll.Invoke; }
        }

		private string _ValueChangedAction;

		public string ValueChangedAction
        {
            get
			{
				return _ValueChangedAction;
			}
			set
			{
				_ValueChangedAction = value;
			}            
        }

		private string _ScrollAction;

		public string ScrollAction
        {
            get
			{
				return _ScrollAction;
			}
			set
			{
				_ScrollAction = value;
			}            
        }


	}  

	/// <summary>
    /// HScrollBar
    /// </summary>
	public class HScrollBar  : ScrollBar   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HScrollBar"/> class.
        /// </summary>
		public HScrollBar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="HScrollBar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal HScrollBar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.HScrollBar Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.HScrollBar;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.HScrollBar();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Image
    /// </summary>
	public class Image  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class.
        /// </summary>
		public Image()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Image(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ImageElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ImageElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ImageElement();
        }
 
			
		public string ImageReference {
			get { return this.GetStringFromResource( this.Element.Image); }
			set { this.Element.Image = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the InitialImage 
		/// </summary>
		/// <value>
		/// The InitialImage
		/// </value>
		[DefaultValue(null)]
		public System.Drawing.Image InitialImage {
			get { return this.Element.InitialImage; }
			set { this.Element.InitialImage = value; }
		}
		
		/// <summary>
		/// Gets or sets the SizeMode 
		/// </summary>
		/// <value>
		/// The SizeMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.PictureBoxSizeMode), "Normal")]
		public System.Web.VisualTree.PictureBoxSizeMode SizeMode {
			get { return this.Element.SizeMode; }
			set { this.Element.SizeMode = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ImageList
    /// </summary>
	[ParseChildren(true)]
	public class ImageList  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageList"/> class.
        /// </summary>
		public ImageList()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageList"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ImageList(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ImageList Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ImageList;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ImageList();
        }
 
		/// <summary>
		/// Gets or sets the TransparentColor 
		/// </summary>
		/// <value>
		/// The TransparentColor
		/// </value>
		public System.Drawing.Color TransparentColor {
			get { return this.Element.TransparentColor; }
			set { this.Element.TransparentColor = value; }
		}
		
		internal bool ShouldSerializeTransparentColor() { return this.TransparentColor!=System.Drawing.Color.Empty;}

		/// <summary>
        /// The state of the ImageStreamID property.
        /// </summary>
		private string _ImageStreamID = null;

		/// <summary>
		/// Gets or sets the ImageStream Reference
		/// </summary>
		/// <value>
		/// The ImageStream
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string ImageStreamID {
           get
            {
                if (this.Element.ImageStream != null)
                {
                    return this.Element.ImageStream.ID;
                }
                return _ImageStreamID;
            }
            set
            {
				// Set the menu id state
				_ImageStreamID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeImageStream(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeImageStream;
				}
            }
		}

		/// <summary>
        /// Initialize the ImageStream property.
        /// </summary>
		private void InitializeImageStream(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_ImageStreamID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.ImageStream = component.Element as System.Web.VisualTree.Elements.ImageListStreamer;
				}
			}
		}

		/// <summary>
		/// Gets or sets the ImageStream 
		/// </summary>
		/// <value>
		/// The ImageStream
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ImageListStreamer ImageStream {
			get { return this.Element.ImageStream; }
			set { this.Element.ImageStream = value; }
		}
		
		/// <summary>
		/// Gets the Images 
		/// </summary>
		/// <value>
		/// The Images
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public ImageCollection Images {
			get { return new ImageCollection(this, this.Element.Images); }
		}
		
		/// <summary>
		/// Gets the Keys 
		/// </summary>
		/// <value>
		/// The Keys
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public KeyCollection Keys {
			get { return new KeyCollection(this, this.Element.Keys); }
		}
		
		/// <summary>
		/// Gets the Children 
		/// </summary>
		/// <value>
		/// The Children
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.VisualElement> Children {
			get { return this.Element.Children; }
		}
		
		/// <summary>
		/// Gets or sets the ImageSize 
		/// </summary>
		/// <value>
		/// The ImageSize
		/// </value>
		public System.Drawing.Size ImageSize {
			get { return this.Element.ImageSize; }
			set { this.Element.ImageSize = value; }
		}
		
		internal bool ShouldSerializeImageSize() { return this.ImageSize!=System.Drawing.Size.Empty;}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ImageListStreamer
    /// </summary>
	public class ImageListStreamer  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageListStreamer"/> class.
        /// </summary>
		public ImageListStreamer()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageListStreamer"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ImageListStreamer(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ImageListStreamer Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ImageListStreamer;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ImageListStreamer();
        }
 
		/// <summary>
		/// Gets the Images 
		/// </summary>
		/// <value>
		/// The Images
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.IEnumerable<System.Web.VisualTree.Elements.ResourceReference> Images {
			get { return this.Element.Images; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ListViewItem
    /// </summary>
	[ParseChildren(true)]
	public class ListViewItem  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItem"/> class.
        /// </summary>
		public ListViewItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ListViewItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ListViewItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ListViewItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ListViewItem();
        }
 
		/// <summary>
		/// Gets the Default 
		/// </summary>
		/// <value>
		/// The Default
		/// </value>
		[DefaultValue(null)]
		public System.String Default {
			get { return this.Element.Default; }
		}
		
		/// <summary>
		/// Gets or sets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
			set { this.Element.Index = value; }
		}
		
		/// <summary>
		/// Gets or sets the Checked 
		/// </summary>
		/// <value>
		/// The Checked
		/// </value>
		[DefaultValue(false)]
		public System.Boolean Checked {
			get { return this.Element.Checked; }
			set { this.Element.Checked = value; }
		}
		
		/// <summary>
		/// Gets or sets the Selected 
		/// </summary>
		/// <value>
		/// The Selected
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Selected {
			get { return this.Element.Selected; }
			set { this.Element.Selected = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the Tag 
		/// </summary>
		/// <value>
		/// The Tag
		/// </value>
		[DefaultValue(null)]
		public System.String Tag {
			get { return this.Element.Tag as System.String; }
			set { this.Element.Tag = value; }
		}
		
		/// <summary>
		/// Gets or sets the BackColor 
		/// </summary>
		/// <value>
		/// The BackColor
		/// </value>
		public System.Drawing.Color BackColor {
			get { return this.Element.BackColor; }
			set { this.Element.BackColor = value; }
		}
		
		internal bool ShouldSerializeBackColor() { return this.BackColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the ImageIndex 
		/// </summary>
		/// <value>
		/// The ImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ImageIndex {
			get { return this.Element.ImageIndex; }
			set { this.Element.ImageIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the Key 
		/// </summary>
		/// <value>
		/// The Key
		/// </value>
		[DefaultValue(null)]
		public System.String Key {
			get { return this.Element.Key; }
			set { this.Element.Key = value; }
		}
		
		/// <summary>
		/// Gets the Subitems 
		/// </summary>
		/// <value>
		/// The Subitems
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public ListViewSubitemCollection Subitems {
			get { return new ListViewSubitemCollection(this, this.Element.Subitems); }
		}
		
		/// <summary>
		/// Gets or sets the UseItemStyleForSubitems 
		/// </summary>
		/// <value>
		/// The UseItemStyleForSubitems
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean UseItemStyleForSubitems {
			get { return this.Element.UseItemStyleForSubitems; }
			set { this.Element.UseItemStyleForSubitems = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ListViewSubitem
    /// </summary>
	public class ListViewSubitem  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewSubitem"/> class.
        /// </summary>
		public ListViewSubitem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewSubitem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ListViewSubitem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ListViewSubitem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ListViewSubitem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ListViewSubitem();
        }
 
		/// <summary>
		/// Gets or sets the BackColor 
		/// </summary>
		/// <value>
		/// The BackColor
		/// </value>
		public System.Drawing.Color BackColor {
			get { return this.Element.BackColor; }
			set { this.Element.BackColor = value; }
		}
		
		internal bool ShouldSerializeBackColor() { return this.BackColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the ForeColor 
		/// </summary>
		/// <value>
		/// The ForeColor
		/// </value>
		public System.Drawing.Color ForeColor {
			get { return this.Element.ForeColor; }
			set { this.Element.ForeColor = value; }
		}
		
		internal bool ShouldSerializeForeColor() { return this.ForeColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets the Default 
		/// </summary>
		/// <value>
		/// The Default
		/// </value>
		[DefaultValue(null)]
		public System.String Default {
			get { return this.Element.Default; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TableStyle
    /// </summary>
	public class TableStyle  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableStyle"/> class.
        /// </summary>
		public TableStyle()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TableStyle"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TableStyle(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TableStyle Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TableStyle;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TableStyle();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ColumnStyle
    /// </summary>
	public class ColumnStyle  : TableStyle   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnStyle"/> class.
        /// </summary>
		public ColumnStyle()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnStyle"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ColumnStyle(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ColumnStyle Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ColumnStyle;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ColumnStyle();
        }
 
		/// <summary>
		/// Gets the Size 
		/// </summary>
		/// <value>
		/// The Size
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single Size {
			get { return this.Element.Size; }
		}
		
		/// <summary>
		/// Gets the SizeType 
		/// </summary>
		/// <value>
		/// The SizeType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.SizeType))]
		public System.Web.VisualTree.SizeType SizeType {
			get { return this.Element.SizeType; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TreeItem
    /// </summary>
	[ParseChildren(true)]
	public class TreeItem  : Component   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeItem"/> class.
        /// </summary>
		public TreeItem()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeItem"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TreeItem(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TreeItem Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TreeItem;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TreeItem();
        }
 
		/// <summary>
		/// Gets the IsLeaf 
		/// </summary>
		/// <value>
		/// The IsLeaf
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsLeaf {
			get { return this.Element.IsLeaf; }
		}
		
		/// <summary>
		/// Gets the Items 
		/// </summary>
		/// <value>
		/// The Items
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public TreeItemCollection Items {
			get { return new TreeItemCollection(this, this.Element.Items); }
		}
		
		/// <summary>
		/// Gets or sets the Tag 
		/// </summary>
		/// <value>
		/// The Tag
		/// </value>
		[DefaultValue(null)]
		public System.String Tag {
			get { return this.Element.Tag as System.String; }
			set { this.Element.Tag = value; }
		}
		
			
		public string Icon {
			get { return this.GetStringFromResource( this.Element.Icon); }
			set { this.Element.Icon = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the Checked 
		/// </summary>
		/// <value>
		/// The Checked
		/// </value>
		[DefaultValue(false)]
		public System.Boolean Checked {
			get { return this.Element.Checked; }
			set { this.Element.Checked = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsSelected 
		/// </summary>
		/// <value>
		/// The IsSelected
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsSelected {
			get { return this.Element.IsSelected; }
			set { this.Element.IsSelected = value; }
		}
		
		/// <summary>
		/// Gets the PrevNode 
		/// </summary>
		/// <value>
		/// The PrevNode
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.TreeItem PrevNode {
			get { return this.Element.PrevNode; }
		}
		
		/// <summary>
		/// Gets the NextNode 
		/// </summary>
		/// <value>
		/// The NextNode
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.TreeItem NextNode {
			get { return this.Element.NextNode; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedImageIndex 
		/// </summary>
		/// <value>
		/// The SelectedImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 SelectedImageIndex {
			get { return this.Element.SelectedImageIndex; }
			set { this.Element.SelectedImageIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageKey 
		/// </summary>
		/// <value>
		/// The ImageKey
		/// </value>
		[DefaultValue(null)]
		public System.String ImageKey {
			get { return this.Element.ImageKey; }
			set { this.Element.ImageKey = value; }
		}
		
		/// <summary>
		/// Gets or sets the ImageIndex 
		/// </summary>
		/// <value>
		/// The ImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ImageIndex {
			get { return this.Element.ImageIndex; }
			set { this.Element.ImageIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the Text 
		/// </summary>
		/// <value>
		/// The Text
		/// </value>
		[DefaultValue(null)]
		public System.String Text {
			get { return this.Element.Text; }
			set { this.Element.Text = value; }
		}
		
		/// <summary>
		/// Gets or sets the ToolTipText 
		/// </summary>
		/// <value>
		/// The ToolTipText
		/// </value>
		[DefaultValue(null)]
		public System.String ToolTipText {
			get { return this.Element.ToolTipText; }
			set { this.Element.ToolTipText = value; }
		}
		
		/// <summary>
		/// Gets or sets the Visible 
		/// </summary>
		/// <value>
		/// The Visible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Visible {
			get { return this.Element.Visible; }
			set { this.Element.Visible = value; }
		}
		
		/// <summary>
		/// Gets or sets the ForeColor 
		/// </summary>
		/// <value>
		/// The ForeColor
		/// </value>
		public System.Drawing.Color ForeColor {
			get { return this.Element.ForeColor; }
			set { this.Element.ForeColor = value; }
		}
		
		internal bool ShouldSerializeForeColor() { return this.ForeColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the BackColor 
		/// </summary>
		/// <value>
		/// The BackColor
		/// </value>
		public System.Drawing.Color BackColor {
			get { return this.Element.BackColor; }
			set { this.Element.BackColor = value; }
		}
		
		internal bool ShouldSerializeBackColor() { return this.BackColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets the Index 
		/// </summary>
		/// <value>
		/// The Index
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Index {
			get { return this.Element.Index; }
		}
		
		/// <summary>
		/// Gets or sets the StateImageIndex 
		/// </summary>
		/// <value>
		/// The StateImageIndex
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 StateImageIndex {
			get { return this.Element.StateImageIndex; }
			set { this.Element.StateImageIndex = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedImageKey 
		/// </summary>
		/// <value>
		/// The SelectedImageKey
		/// </value>
		[DefaultValue(null)]
		public System.String SelectedImageKey {
			get { return this.Element.SelectedImageKey; }
			set { this.Element.SelectedImageKey = value; }
		}
		
		/// <summary>
		/// Gets the Level 
		/// </summary>
		/// <value>
		/// The Level
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Level {
			get { return this.Element.Level; }
		}
		
		/// <summary>
		/// Gets or sets the Font 
		/// </summary>
		/// <value>
		/// The Font
		/// </value>
		[DefaultValue(null)]
		public System.Drawing.Font Font {
			get { return this.Element.Font; }
			set { this.Element.Font = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsExpanded 
		/// </summary>
		/// <value>
		/// The IsExpanded
		/// </value>
		[DefaultValue(false)]
		public System.Boolean IsExpanded {
			get { return this.Element.IsExpanded; }
			set { this.Element.IsExpanded = value; }
		}
		
		/// <summary>
		/// Gets or sets the Key 
		/// </summary>
		/// <value>
		/// The Key
		/// </value>
		[DefaultValue(null)]
		public System.String Key {
			get { return this.Element.Key; }
			set { this.Element.Key = value; }
		}
		
		/// <summary>
		/// Gets the FieldType 
		/// </summary>
		/// <value>
		/// The FieldType
		/// </value>
		[DefaultValue(null)]
		public System.Type FieldType {
			get { return this.Element.FieldType; }
		}
		
		/// <summary>
		/// Gets or sets the Values 
		/// </summary>
		/// <value>
		/// The Values
		/// </value>
		[DefaultValue(null)]
		public System.Collections.Generic.List<System.String> Values {
			get { return this.Element.Values; }
			set { this.Element.Values = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// VScrollBar
    /// </summary>
	public class VScrollBar  : ScrollBar   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VScrollBar"/> class.
        /// </summary>
		public VScrollBar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="VScrollBar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal VScrollBar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.VScrollBar Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.VScrollBar;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.VScrollBar();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// DateTimePicker
    /// </summary>
	public class DateTimePicker  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimePicker"/> class.
        /// </summary>
		public DateTimePicker()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimePicker"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal DateTimePicker(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.DateTimePickerElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.DateTimePickerElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.DateTimePickerElement();
        }
 
		/// <summary>
		/// Gets or sets the Format 
		/// </summary>
		/// <value>
		/// The Format
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.DateTimePickerFormat), "Default")]
		public System.Web.VisualTree.DateTimePickerFormat Format {
			get { return this.Element.Format; }
			set { this.Element.Format = value; }
		}
		
		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		public System.DateTime Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the CustomFormat 
		/// </summary>
		/// <value>
		/// The CustomFormat
		/// </value>
		[DefaultValue(null)]
		public System.String CustomFormat {
			get { return this.Element.CustomFormat; }
			set { this.Element.CustomFormat = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowUpDown 
		/// </summary>
		/// <value>
		/// The ShowUpDown
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ShowUpDown {
			get { return this.Element.ShowUpDown; }
			set { this.Element.ShowUpDown = value; }
		}
		
		/// <summary>
		/// Gets or sets the Checked 
		/// </summary>
		/// <value>
		/// The Checked
		/// </value>
		[DefaultValue(true)]
		public System.Boolean Checked {
			get { return this.Element.Checked; }
			set { this.Element.Checked = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxDate 
		/// </summary>
		/// <value>
		/// The MaxDate
		/// </value>
		public System.DateTime MaxDate {
			get { return this.Element.MaxDate; }
			set { this.Element.MaxDate = value; }
		}
		
		/// <summary>
		/// Gets or sets the SpinInterval 
		/// </summary>
		/// <value>
		/// The SpinInterval
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.DateInterval))]
		public System.Web.VisualTree.DateInterval SpinInterval {
			get { return this.Element.SpinInterval; }
			set { this.Element.SpinInterval = value; }
		}
		
		/// <summary>
		/// Gets or sets the MinDate 
		/// </summary>
		/// <value>
		/// The MinDate
		/// </value>
		public System.DateTime MinDate {
			get { return this.Element.MinDate; }
			set { this.Element.MinDate = value; }
		}
		
		/// <summary>
		/// Gets or sets the Editable 
		/// </summary>
		/// <value>
		/// The Editable
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Editable {
			get { return this.Element.Editable; }
			set { this.Element.Editable = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowCheckBox 
		/// </summary>
		/// <value>
		/// The ShowCheckBox
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ShowCheckBox {
			get { return this.Element.ShowCheckBox; }
			set { this.Element.ShowCheckBox = value; }
		}
		
		/// <summary>
		/// Gets or sets the MonthBackground 
		/// </summary>
		/// <value>
		/// The MonthBackground
		/// </value>
		public System.Drawing.Color MonthBackground {
			get { return this.Element.MonthBackground; }
			set { this.Element.MonthBackground = value; }
		}
		
		internal bool ShouldSerializeMonthBackground() { return this.MonthBackground!=System.Drawing.Color.Empty;}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _ValueChanged = Component.GetHandler<System.EventArgs>(this.Element, this.ValueChangedAction);
			if (_ValueChanged != null) { this.Element.ValueChanged += _ValueChanged.Invoke; }
			IVisualElementAction<System.EventArgs> _CloseUp = Component.GetHandler<System.EventArgs>(this.Element, this.CloseUpAction);
			if (_CloseUp != null) { this.Element.CloseUp += _CloseUp.Invoke; }
        }

		private string _ValueChangedAction;

		public string ValueChangedAction
        {
            get
			{
				return _ValueChangedAction;
			}
			set
			{
				_ValueChangedAction = value;
			}            
        }

		private string _CloseUpAction;

		public string CloseUpAction
        {
            get
			{
				return _CloseUpAction;
			}
			set
			{
				_CloseUpAction = value;
			}            
        }


	}  

	/// <summary>
    /// WindowMdiContainer
    /// </summary>
	public class WindowMdiContainer  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WindowMdiContainer"/> class.
        /// </summary>
		public WindowMdiContainer()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowMdiContainer"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal WindowMdiContainer(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.WindowMdiContainer Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.WindowMdiContainer;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.WindowMdiContainer();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// NumericUpDown
    /// </summary>
	public class NumericUpDown  : UpDown   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NumericUpDown"/> class.
        /// </summary>
		public NumericUpDown()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericUpDown"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal NumericUpDown(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.NumericUpDownElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.NumericUpDownElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.NumericUpDownElement();
        }
 
		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		public System.Decimal Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the Maximum 
		/// </summary>
		/// <value>
		/// The Maximum
		/// </value>
		public System.Decimal Maximum {
			get { return this.Element.Maximum; }
			set { this.Element.Maximum = value; }
		}
		
		/// <summary>
		/// Gets or sets the Minimum 
		/// </summary>
		/// <value>
		/// The Minimum
		/// </value>
		public System.Decimal Minimum {
			get { return this.Element.Minimum; }
			set { this.Element.Minimum = value; }
		}
		
		/// <summary>
		/// Gets or sets the Hexadecimal 
		/// </summary>
		/// <value>
		/// The Hexadecimal
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Hexadecimal {
			get { return this.Element.Hexadecimal; }
			set { this.Element.Hexadecimal = value; }
		}
		
		/// <summary>
		/// Gets or sets the Increment 
		/// </summary>
		/// <value>
		/// The Increment
		/// </value>
		public System.Decimal Increment {
			get { return this.Element.Increment; }
			set { this.Element.Increment = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxText 
		/// </summary>
		/// <value>
		/// The MaxText
		/// </value>
		[DefaultValue(null)]
		public System.String MaxText {
			get { return this.Element.MaxText; }
			set { this.Element.MaxText = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _ValueChanged = Component.GetHandler<System.EventArgs>(this.Element, this.ValueChangedAction);
			if (_ValueChanged != null) { this.Element.ValueChanged += _ValueChanged.Invoke; }
        }

		private string _ValueChangedAction;

		public string ValueChangedAction
        {
            get
			{
				return _ValueChangedAction;
			}
			set
			{
				_ValueChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// RichTextBox
    /// </summary>
	public class RichTextBox  : TextBoxBase   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RichTextBox"/> class.
        /// </summary>
		public RichTextBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RichTextBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RichTextBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.RichTextBox Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.RichTextBox;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.RichTextBox();
        }
 
		/// <summary>
		/// Gets or sets the SelectionColor 
		/// </summary>
		/// <value>
		/// The SelectionColor
		/// </value>
		public System.Drawing.Color SelectionColor {
			get { return this.Element.SelectionColor; }
			set { this.Element.SelectionColor = value; }
		}
		
		internal bool ShouldSerializeSelectionColor() { return this.SelectionColor!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the SelectionFont 
		/// </summary>
		/// <value>
		/// The SelectionFont
		/// </value>
		[DefaultValue(null)]
		public System.Drawing.Font SelectionFont {
			get { return this.Element.SelectionFont; }
			set { this.Element.SelectionFont = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectedText 
		/// </summary>
		/// <value>
		/// The SelectedText
		/// </value>
		[DefaultValue(null)]
		public System.String SelectedText {
			get { return this.Element.SelectedText; }
			set { this.Element.SelectedText = value; }
		}
		
		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		[DefaultValue(null)]
		public System.String Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScrollBars 
		/// </summary>
		/// <value>
		/// The ScrollBars
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.ScrollBars), "Both")]
		public System.Web.VisualTree.ScrollBars ScrollBars {
			get { return this.Element.ScrollBars; }
			set { this.Element.ScrollBars = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextRtf 
		/// </summary>
		/// <value>
		/// The TextRtf
		/// </value>
		[DefaultValue("")]
		public System.String TextRtf {
			get { return this.Element.TextRtf; }
			set { this.Element.TextRtf = value; }
		}
		
		/// <summary>
		/// Gets or sets the HideSelection 
		/// </summary>
		/// <value>
		/// The HideSelection
		/// </value>
		[DefaultValue(true)]
		public System.Boolean HideSelection {
			get { return this.Element.HideSelection; }
			set { this.Element.HideSelection = value; }
		}
		
		/// <summary>
		/// Gets or sets the FontName 
		/// </summary>
		/// <value>
		/// The FontName
		/// </value>
		[DefaultValue(null)]
		public System.String FontName {
			get { return this.Element.FontName; }
			set { this.Element.FontName = value; }
		}
		
		/// <summary>
		/// Gets or sets the FontSize 
		/// </summary>
		/// <value>
		/// The FontSize
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single FontSize {
			get { return this.Element.FontSize; }
			set { this.Element.FontSize = value; }
		}
		
		/// <summary>
		/// Gets or sets the TextLength 
		/// </summary>
		/// <value>
		/// The TextLength
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 TextLength {
			get { return this.Element.TextLength; }
			set { this.Element.TextLength = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Status
    /// </summary>
	public class Status  : ToolBar   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Status"/> class.
        /// </summary>
		public Status()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Status"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Status(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.StatusElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.StatusElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.StatusElement();
        }
 
		/// <summary>
		/// Gets or sets the SizingGrip 
		/// </summary>
		/// <value>
		/// The SizingGrip
		/// </value>
		[DefaultValue(true)]
		public System.Boolean SizingGrip {
			get { return this.Element.SizingGrip; }
			set { this.Element.SizingGrip = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// RowStyle
    /// </summary>
	public class RowStyle  : TableStyle   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RowStyle"/> class.
        /// </summary>
		public RowStyle()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RowStyle"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RowStyle(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.RowStyle Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.RowStyle;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.RowStyle();
        }
 
		/// <summary>
		/// Gets the Size 
		/// </summary>
		/// <value>
		/// The Size
		/// </value>
		[DefaultValue(default(System.Single))]
		public System.Single Size {
			get { return this.Element.Size; }
		}
		
		/// <summary>
		/// Gets the SizeType 
		/// </summary>
		/// <value>
		/// The SizeType
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.SizeType))]
		public System.Web.VisualTree.SizeType SizeType {
			get { return this.Element.SizeType; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// TablePanel
    /// </summary>
	[ParseChildren(true)]
	public class TablePanel  : Panel   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TablePanel"/> class.
        /// </summary>
		public TablePanel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TablePanel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal TablePanel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.TablePanel Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.TablePanel;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.TablePanel();
        }
 
		/// <summary>
		/// Gets the ColumnStyles 
		/// </summary>
		/// <value>
		/// The ColumnStyles
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public TableLayoutColumnStyleCollection ColumnStyles {
			get { return new TableLayoutColumnStyleCollection(this, this.Element.ColumnStyles); }
		}
		
		/// <summary>
		/// Gets the RowStyles 
		/// </summary>
		/// <value>
		/// The RowStyles
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
		public TableLayoutRowStyleCollection RowStyles {
			get { return new TableLayoutRowStyleCollection(this, this.Element.RowStyles); }
		}
		
		/// <summary>
		/// Gets or sets the LayoutSettings 
		/// </summary>
		/// <value>
		/// The LayoutSettings
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.TablePanelLayoutSettings LayoutSettings {
			get { return this.Element.LayoutSettings; }
			set { this.Element.LayoutSettings = value; }
		}
		
		/// <summary>
		/// Gets the Controls 
		/// </summary>
		/// <value>
		/// The Controls
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.TableControlContainerElement Controls {
			get { return this.Element.Controls; }
		}
		
		/// <summary>
		/// Gets or sets the ColumnCount 
		/// </summary>
		/// <value>
		/// The ColumnCount
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 ColumnCount {
			get { return this.Element.ColumnCount; }
			set { this.Element.ColumnCount = value; }
		}
		
		/// <summary>
		/// Gets or sets the RowCount 
		/// </summary>
		/// <value>
		/// The RowCount
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 RowCount {
			get { return this.Element.RowCount; }
			set { this.Element.RowCount = value; }
		}
		
		/// <summary>
		/// Gets or sets the GrowStyle 
		/// </summary>
		/// <value>
		/// The GrowStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.TablePanelGrowStyle), "AddRows")]
		public System.Web.VisualTree.Elements.TablePanelGrowStyle GrowStyle {
			get { return this.Element.GrowStyle; }
			set { this.Element.GrowStyle = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarControlHost
    /// </summary>
	public abstract class ToolBarControlHost  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarControlHost"/> class.
        /// </summary>
		public ToolBarControlHost()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarControlHost"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarControlHost(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarControlHost Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarControlHost;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

		/// <summary>
		/// Gets the Control 
		/// </summary>
		/// <value>
		/// The Control
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.ControlElement Control {
			get { return this.Element.Control; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarContainer
    /// </summary>
	public class ToolBarContainer  : ControlContainer   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarContainer"/> class.
        /// </summary>
		public ToolBarContainer()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarContainer"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarContainer(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarContainer Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarContainer;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarContainer();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarLabel
    /// </summary>
	public class ToolBarLabel  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarLabel"/> class.
        /// </summary>
		public ToolBarLabel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarLabel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarLabel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarLabel Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarLabel;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarLabel();
        }
 
		/// <summary>
		/// Gets or sets the Spring 
		/// </summary>
		/// <value>
		/// The Spring
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean Spring {
			get { return this.Element.Spring; }
			set { this.Element.Spring = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarProgressBar
    /// </summary>
	public class ToolBarProgressBar  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarProgressBar"/> class.
        /// </summary>
		public ToolBarProgressBar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarProgressBar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarProgressBar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarProgressBar Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarProgressBar;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarProgressBar();
        }
 
		/// <summary>
		/// Gets or sets the Value 
		/// </summary>
		/// <value>
		/// The Value
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Value {
			get { return this.Element.Value; }
			set { this.Element.Value = value; }
		}
		
		/// <summary>
		/// Gets or sets the Max 
		/// </summary>
		/// <value>
		/// The Max
		/// </value>
		[DefaultValue(100)]
		public System.Int32 Max {
			get { return this.Element.Max; }
			set { this.Element.Max = value; }
		}
		
		/// <summary>
		/// Gets or sets the Min 
		/// </summary>
		/// <value>
		/// The Min
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Min {
			get { return this.Element.Min; }
			set { this.Element.Min = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// ToolBarSplitButton
    /// </summary>
	public class ToolBarSplitButton  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarSplitButton"/> class.
        /// </summary>
		public ToolBarSplitButton()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarSplitButton"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ToolBarSplitButton(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ToolBarSplitButton Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ToolBarSplitButton;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ToolBarSplitButton();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// StatusLabel
    /// </summary>
	public class StatusLabel  : ToolBarItem   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusLabel"/> class.
        /// </summary>
		public StatusLabel()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusLabel"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal StatusLabel(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.StatusLabel Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.StatusLabel;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.StatusLabel();
        }
 
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Range
    /// </summary>
	public class Range  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Range"/> class.
        /// </summary>
		public Range()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Range"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Range(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.RangeElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.RangeElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.RangeElement();
        }
 
		/// <summary>
		/// Gets or sets the ValuesRange 
		/// </summary>
		/// <value>
		/// The ValuesRange
		/// </value>
		[DefaultValue(null)]
		public System.Int32[] ValuesRange {
			get { return this.Element.ValuesRange; }
			set { this.Element.ValuesRange = value; }
		}
		
		/// <summary>
		/// Gets or sets the Increment 
		/// </summary>
		/// <value>
		/// The Increment
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 Increment {
			get { return this.Element.Increment; }
			set { this.Element.Increment = value; }
		}
		
		/// <summary>
		/// Gets or sets the MinValue 
		/// </summary>
		/// <value>
		/// The MinValue
		/// </value>
		[DefaultValue(0)]
		public System.Int32 MinValue {
			get { return this.Element.MinValue; }
			set { this.Element.MinValue = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaxValue 
		/// </summary>
		/// <value>
		/// The MaxValue
		/// </value>
		[DefaultValue(0)]
		public System.Int32 MaxValue {
			get { return this.Element.MaxValue; }
			set { this.Element.MaxValue = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _Scroll = Component.GetHandler<System.EventArgs>(this.Element, this.ScrollAction);
			if (_Scroll != null) { this.Element.Scroll += _Scroll.Invoke; }
        }

		private string _ScrollAction;

		public string ScrollAction
        {
            get
			{
				return _ScrollAction;
			}
			set
			{
				_ScrollAction = value;
			}            
        }


	}  

	/// <summary>
    /// MonthCalendar
    /// </summary>
	public class MonthCalendar  : Control   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MonthCalendar"/> class.
        /// </summary>
		public MonthCalendar()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="MonthCalendar"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal MonthCalendar(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.MonthCalendarElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.MonthCalendarElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.MonthCalendarElement();
        }
 
		/// <summary>
		/// Gets or sets the Date 
		/// </summary>
		/// <value>
		/// The Date
		/// </value>
		public System.DateTime Date {
			get { return this.Element.Date; }
			set { this.Element.Date = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.EventArgs> _DateChanged = Component.GetHandler<System.EventArgs>(this.Element, this.DateChangedAction);
			if (_DateChanged != null) { this.Element.DateChanged += _DateChanged.Invoke; }
        }

		private string _DateChangedAction;

		public string DateChangedAction
        {
            get
			{
				return _DateChangedAction;
			}
			set
			{
				_DateChangedAction = value;
			}            
        }


	}  

	/// <summary>
    /// ListBox
    /// </summary>
	public class ListBox  : ListControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListBox"/> class.
        /// </summary>
		public ListBox()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ListBox"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal ListBox(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.ListBoxElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.ListBoxElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.ListBoxElement();
        }
 
		/// <summary>
		/// Gets or sets the DrawMode 
		/// </summary>
		/// <value>
		/// The DrawMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.DrawMode), "Normal")]
		public System.Web.VisualTree.DrawMode DrawMode {
			get { return this.Element.DrawMode; }
			set { this.Element.DrawMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScrollAlwaysVisible 
		/// </summary>
		/// <value>
		/// The ScrollAlwaysVisible
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean ScrollAlwaysVisible {
			get { return this.Element.ScrollAlwaysVisible; }
			set { this.Element.ScrollAlwaysVisible = value; }
		}
		
		/// <summary>
		/// Gets or sets the HorizontalScrollBar 
		/// </summary>
		/// <value>
		/// The HorizontalScrollBar
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HorizontalScrollBar {
			get { return this.Element.HorizontalScrollBar; }
			set { this.Element.HorizontalScrollBar = value; }
		}
		
		/// <summary>
		/// Gets or sets the SelectionMode 
		/// </summary>
		/// <value>
		/// The SelectionMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.MultiSelectMode), "ListBoxStandard")]
		public System.Web.VisualTree.Elements.MultiSelectMode SelectionMode {
			get { return this.Element.SelectionMode; }
			set { this.Element.SelectionMode = value; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	/// <summary>
    /// Window
    /// </summary>
	public class Window  : ControlContainer   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Window"/> class.
        /// </summary>
		public Window()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Window"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal Window(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.Elements.WindowElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.Elements.WindowElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.Elements.WindowElement();
        }
 
		/// <summary>
		/// Gets or sets the WindowTarget 
		/// </summary>
		/// <value>
		/// The WindowTarget
		/// </value>
		[DefaultValue(null)]
		public System.String WindowTarget {
			get { return this.Element.WindowTarget; }
			set { this.Element.WindowTarget = value; }
		}
		
		/// <summary>
		/// Gets the HasWindowTarget 
		/// </summary>
		/// <value>
		/// The HasWindowTarget
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean HasWindowTarget {
			get { return this.Element.HasWindowTarget; }
		}
		
		/// <summary>
		/// Gets or sets the PaletteMode 
		/// </summary>
		/// <value>
		/// The PaletteMode
		/// </value>
		[DefaultValue(default(System.Int32))]
		public System.Int32 PaletteMode {
			get { return this.Element.PaletteMode; }
			set { this.Element.PaletteMode = value; }
		}
		
		/// <summary>
		/// Gets or sets the IsTouchWindow 
		/// </summary>
		/// <value>
		/// The IsTouchWindow
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsTouchWindow {
			get { return this.Element.IsTouchWindow; }
			set { this.Element.IsTouchWindow = value; }
		}
		
		/// <summary>
		/// Gets or sets the ScaleMode 
		/// </summary>
		/// <value>
		/// The ScaleMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.ScaleModeConstants), "Pixels")]
		public System.Web.VisualTree.Elements.ScaleModeConstants ScaleMode {
			get { return this.Element.ScaleMode; }
			set { this.Element.ScaleMode = value; }
		}
		
		/// <summary>
		/// Gets the OwnedForms 
		/// </summary>
		/// <value>
		/// The OwnedForms
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.WindowElement[] OwnedForms {
			get { return this.Element.OwnedForms; }
		}
		
		/// <summary>
        /// The state of the MenuID property.
        /// </summary>
		private string _MenuID = null;

		/// <summary>
		/// Gets or sets the Menu Reference
		/// </summary>
		/// <value>
		/// The Menu
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string MenuID {
           get
            {
                if (this.Element.Menu != null)
                {
                    return this.Element.Menu.ID;
                }
                return _MenuID;
            }
            set
            {
				// Set the menu id state
				_MenuID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeMenu(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeMenu;
				}
            }
		}

		/// <summary>
        /// Initialize the Menu property.
        /// </summary>
		private void InitializeMenu(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_MenuID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.Menu = component.Element as System.Web.VisualTree.Elements.MenuElement;
				}
			}
		}

		/// <summary>
		/// Gets or sets the Menu 
		/// </summary>
		/// <value>
		/// The Menu
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.MenuElement Menu {
			get { return this.Element.Menu; }
			set { this.Element.Menu = value; }
		}
		
		/// <summary>
		/// Gets the ClientID 
		/// </summary>
		/// <value>
		/// The ClientID
		/// </value>
		[DefaultValue(null)]
		public System.String ClientID {
			get { return this.Element.ClientID; }
		}
		
		/// <summary>
		/// Gets or sets the DialogResult 
		/// </summary>
		/// <value>
		/// The DialogResult
		/// </value>
		[DefaultValue(default(System.Web.VisualTree.DialogResult))]
		public System.Web.VisualTree.DialogResult DialogResult {
			get { return this.Element.DialogResult; }
			set { this.Element.DialogResult = value; }
		}
		
		/// <summary>
        /// The state of the AcceptButtonID property.
        /// </summary>
		private string _AcceptButtonID = null;

		/// <summary>
		/// Gets or sets the AcceptButton Reference
		/// </summary>
		/// <value>
		/// The AcceptButton
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string AcceptButtonID {
           get
            {
                if (this.Element.AcceptButton != null)
                {
                    return this.Element.AcceptButton.ID;
                }
                return _AcceptButtonID;
            }
            set
            {
				// Set the menu id state
				_AcceptButtonID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeAcceptButton(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeAcceptButton;
				}
            }
		}

		/// <summary>
        /// Initialize the AcceptButton property.
        /// </summary>
		private void InitializeAcceptButton(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_AcceptButtonID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.AcceptButton = component.Element as System.Web.VisualTree.Elements.ButtonElement;
				}
			}
		}

		/// <summary>
		/// Gets or sets the AcceptButton 
		/// </summary>
		/// <value>
		/// The AcceptButton
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ButtonElement AcceptButton {
			get { return this.Element.AcceptButton; }
			set { this.Element.AcceptButton = value; }
		}
		
		/// <summary>
		/// Gets or sets the AutoScaleBaseSize 
		/// </summary>
		/// <value>
		/// The AutoScaleBaseSize
		/// </value>
		public System.Drawing.Size AutoScaleBaseSize {
			get { return this.Element.AutoScaleBaseSize; }
			set { this.Element.AutoScaleBaseSize = value; }
		}
		
		internal bool ShouldSerializeAutoScaleBaseSize() { return this.AutoScaleBaseSize!=System.Drawing.Size.Empty;}

		/// <summary>
		/// Gets or sets the AutoSizeMode 
		/// </summary>
		/// <value>
		/// The AutoSizeMode
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.AutoSizeMode), "GrowOnly")]
		public System.Web.VisualTree.AutoSizeMode AutoSizeMode {
			get { return this.Element.AutoSizeMode; }
			set { this.Element.AutoSizeMode = value; }
		}
		
		/// <summary>
        /// The state of the CancelButtonID property.
        /// </summary>
		private string _CancelButtonID = null;

		/// <summary>
		/// Gets or sets the CancelButton Reference
		/// </summary>
		/// <value>
		/// The CancelButton
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string CancelButtonID {
           get
            {
                if (this.Element.CancelButton != null)
                {
                    return this.Element.CancelButton.ID;
                }
                return _CancelButtonID;
            }
            set
            {
				// Set the menu id state
				_CancelButtonID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeCancelButton(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeCancelButton;
				}
            }
		}

		/// <summary>
        /// Initialize the CancelButton property.
        /// </summary>
		private void InitializeCancelButton(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_CancelButtonID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.CancelButton = component.Element as System.Web.VisualTree.Elements.ButtonBaseElement;
				}
			}
		}

		/// <summary>
		/// Gets or sets the CancelButton 
		/// </summary>
		/// <value>
		/// The CancelButton
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ButtonBaseElement CancelButton {
			get { return this.Element.CancelButton; }
			set { this.Element.CancelButton = value; }
		}
		
		/// <summary>
		/// Gets or sets the ControlBox 
		/// </summary>
		/// <value>
		/// The ControlBox
		/// </value>
		[DefaultValue(true)]
		public System.Boolean ControlBox {
			get { return this.Element.ControlBox; }
			set { this.Element.ControlBox = value; }
		}
		
		/// <summary>
		/// Gets or sets the DesktopBounds 
		/// </summary>
		/// <value>
		/// The DesktopBounds
		/// </value>
		public System.Drawing.Rectangle DesktopBounds {
			get { return this.Element.DesktopBounds; }
			set { this.Element.DesktopBounds = value; }
		}
		
		internal bool ShouldSerializeDesktopBounds() { return this.DesktopBounds!=System.Drawing.Rectangle.Empty;}

		/// <summary>
		/// Gets or sets the WindowBorderStyle 
		/// </summary>
		/// <value>
		/// The WindowBorderStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.WindowBorderStyle), "Sizable")]
		public System.Web.VisualTree.Elements.WindowBorderStyle WindowBorderStyle {
			get { return this.Element.WindowBorderStyle; }
			set { this.Element.WindowBorderStyle = value; }
		}
		
			
		public string Icon {
			get { return this.GetStringFromResource( this.Element.Icon); }
			set { this.Element.Icon = this.GetResourceFromString(value); }
		}

		/// <summary>
		/// Gets or sets the IsMdiContainer 
		/// </summary>
		/// <value>
		/// The IsMdiContainer
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsMdiContainer {
			get { return this.Element.IsMdiContainer; }
			set { this.Element.IsMdiContainer = value; }
		}
		
		/// <summary>
		/// Gets or sets the Opacity 
		/// </summary>
		/// <value>
		/// The Opacity
		/// </value>
		[DefaultValue(default(System.Double))]
		public System.Double Opacity {
			get { return this.Element.Opacity; }
			set { this.Element.Opacity = value; }
		}
		
		/// <summary>
		/// Gets or sets the ShowIcon 
		/// </summary>
		/// <value>
		/// The ShowIcon
		/// </value>
		[DefaultValue(true)]
		public System.Boolean ShowIcon {
			get { return this.Element.ShowIcon; }
			set { this.Element.ShowIcon = value; }
		}
		
		/// <summary>
		/// Gets or sets the SizeGripStyle 
		/// </summary>
		/// <value>
		/// The SizeGripStyle
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.SizeGripStyle), "Auto")]
		public System.Web.VisualTree.SizeGripStyle SizeGripStyle {
			get { return this.Element.SizeGripStyle; }
			set { this.Element.SizeGripStyle = value; }
		}
		
		/// <summary>
		/// Gets or sets the StartPosition 
		/// </summary>
		/// <value>
		/// The StartPosition
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.WindowStartPosition), "CenterParent")]
		public System.Web.VisualTree.WindowStartPosition StartPosition {
			get { return this.Element.StartPosition; }
			set { this.Element.StartPosition = value; }
		}
		
		/// <summary>
		/// Gets or sets the TopMost 
		/// </summary>
		/// <value>
		/// The TopMost
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean TopMost {
			get { return this.Element.TopMost; }
			set { this.Element.TopMost = value; }
		}
		
		/// <summary>
		/// Gets or sets the TransparencyKey 
		/// </summary>
		/// <value>
		/// The TransparencyKey
		/// </value>
		public System.Drawing.Color TransparencyKey {
			get { return this.Element.TransparencyKey; }
			set { this.Element.TransparencyKey = value; }
		}
		
		internal bool ShouldSerializeTransparencyKey() { return this.TransparencyKey!=System.Drawing.Color.Empty;}

		/// <summary>
		/// Gets or sets the WindowState 
		/// </summary>
		/// <value>
		/// The WindowState
		/// </value>
		[DefaultValue(typeof(System.Web.VisualTree.Elements.WindowState), "Normal")]
		public System.Web.VisualTree.Elements.WindowState WindowState {
			get { return this.Element.WindowState; }
			set { this.Element.WindowState = value; }
		}
		
		/// <summary>
        /// The state of the ToolBarID property.
        /// </summary>
		private string _ToolBarID = null;

		/// <summary>
		/// Gets or sets the ToolBar Reference
		/// </summary>
		/// <value>
		/// The ToolBar
		/// </value>
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
		public string ToolBarID {
           get
            {
                if (this.Element.ToolBar != null)
                {
                    return this.Element.ToolBar.ID;
                }
                return _ToolBarID;
            }
            set
            {
				// Set the menu id state
				_ToolBarID = value;

				// If there is a valid naming container
				if(this.NamingContainer != null)
				{
					this.InitializeToolBar(null, EventArgs.Empty);
				}
				else
				{
					this.Load += this.InitializeToolBar;
				}
            }
		}

		/// <summary>
        /// Initialize the ToolBar property.
        /// </summary>
		private void InitializeToolBar(object sender, EventArgs e)
		{
			// Get the current naming container
			System.Web.UI.Control container = this.NamingContainer;

			// If there is a valid naming container
            if (container != null) 
			{
				// Get control by id
				IVisualTreeComponent component = container.FindControl(_ToolBarID) as IVisualTreeComponent;

				// If there is a valid control
				if(component != null)
				{
					this.Element.ToolBar = component.Element as System.Web.VisualTree.Elements.ToolBarElement;
				}
			}
		}

		/// <summary>
		/// Gets or sets the ToolBar 
		/// </summary>
		/// <value>
		/// The ToolBar
		/// </value>
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.VisualTree.Elements.ToolBarElement ToolBar {
			get { return this.Element.ToolBar; }
			set { this.Element.ToolBar = value; }
		}
		
		/// <summary>
		/// Gets or sets the MaximizeBox 
		/// </summary>
		/// <value>
		/// The MaximizeBox
		/// </value>
		[DefaultValue(true)]
		public System.Boolean MaximizeBox {
			get { return this.Element.MaximizeBox; }
			set { this.Element.MaximizeBox = value; }
		}
		
		/// <summary>
		/// Gets or sets the MinimizeBox 
		/// </summary>
		/// <value>
		/// The MinimizeBox
		/// </value>
		[DefaultValue(true)]
		public System.Boolean MinimizeBox {
			get { return this.Element.MinimizeBox; }
			set { this.Element.MinimizeBox = value; }
		}
		
		/// <summary>
		/// Gets or sets the KeyPreview 
		/// </summary>
		/// <value>
		/// The KeyPreview
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean KeyPreview {
			get { return this.Element.KeyPreview; }
			set { this.Element.KeyPreview = value; }
		}
		
		/// <summary>
		/// Gets the IsWindowless 
		/// </summary>
		/// <value>
		/// The IsWindowless
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsWindowless {
			get { return this.Element.IsWindowless; }
		}
		
		/// <summary>
		/// Gets or sets the IsCreated 
		/// </summary>
		/// <value>
		/// The IsCreated
		/// </value>
		[DefaultValue(default(System.Boolean))]
		public System.Boolean IsCreated {
			get { return this.Element.IsCreated; }
			set { this.Element.IsCreated = value; }
		}
		
		/// <summary>
		/// Gets or sets the EnableEsc 
		/// </summary>
		/// <value>
		/// The EnableEsc
		/// </value>
		[DefaultValue(false)]
		public System.Boolean EnableEsc {
			get { return this.Element.EnableEsc; }
			set { this.Element.EnableEsc = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void Activate() {
			this.Element.Activate();
		}

 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
			IVisualElementAction<System.Web.VisualTree.Elements.WindowClosingEventArgs> _WindowClosing = Component.GetHandler<System.Web.VisualTree.Elements.WindowClosingEventArgs>(this.Element, this.WindowClosingAction);
			if (_WindowClosing != null) { this.Element.WindowClosing += _WindowClosing.Invoke; }
			IVisualElementAction<System.EventArgs> _Shown = Component.GetHandler<System.EventArgs>(this.Element, this.ShownAction);
			if (_Shown != null) { this.Element.Shown += _Shown.Invoke; }
			IVisualElementAction<System.Web.VisualTree.Elements.WindowClosedEventArgs> _WindowClosed = Component.GetHandler<System.Web.VisualTree.Elements.WindowClosedEventArgs>(this.Element, this.WindowClosedAction);
			if (_WindowClosed != null) { this.Element.WindowClosed += _WindowClosed.Invoke; }
			IVisualElementAction<System.EventArgs> _Deactivate = Component.GetHandler<System.EventArgs>(this.Element, this.DeactivateAction);
			if (_Deactivate != null) { this.Element.Deactivate += _Deactivate.Invoke; }
			IVisualElementAction<System.EventArgs> _Activated = Component.GetHandler<System.EventArgs>(this.Element, this.ActivatedAction);
			if (_Activated != null) { this.Element.Activated += _Activated.Invoke; }
        }

		private string _WindowClosingAction;

		public string WindowClosingAction
        {
            get
			{
				return _WindowClosingAction;
			}
			set
			{
				_WindowClosingAction = value;
			}            
        }

		private string _ShownAction;

		public string ShownAction
        {
            get
			{
				return _ShownAction;
			}
			set
			{
				_ShownAction = value;
			}            
        }

		private string _WindowClosedAction;

		public string WindowClosedAction
        {
            get
			{
				return _WindowClosedAction;
			}
			set
			{
				_WindowClosedAction = value;
			}            
        }

		private string _DeactivateAction;

		public string DeactivateAction
        {
            get
			{
				return _DeactivateAction;
			}
			set
			{
				_DeactivateAction = value;
			}            
        }

		private string _ActivatedAction;

		public string ActivatedAction
        {
            get
			{
				return _ActivatedAction;
			}
			set
			{
				_ActivatedAction = value;
			}            
        }


	}  

	/// <summary>
    /// RibbonControl
    /// </summary>
	public class RibbonControl  : ContentControl   
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonControl"/> class.
        /// </summary>
		public RibbonControl()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonControl"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
		internal RibbonControl(System.Web.VisualTree.Elements.VisualElement visualElement, ComponentManager componentManager = null,bool shouldGenerateChildren = true) : base(visualElement, componentManager,shouldGenerateChildren)
		{
		}

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public new System.Web.VisualTree.RibbonControlElement Element
        {
            get
            {
                return base.Element as System.Web.VisualTree.RibbonControlElement;
            }
            protected set
            {
                // Set the visual element
                base.Element = value;
            }
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected override void CreateVisualElement()
        {
			this.Element = new System.Web.VisualTree.RibbonControlElement();
        }
 
		/// <summary>
		/// Gets the Pages 
		/// </summary>
		/// <value>
		/// The Pages
		/// </value>
		[DefaultValue(null)]
		public System.Web.VisualTree.Elements.RibbonPageCollection Pages {
			get { return this.Element.Pages; }
		}
		
 
        /// <summary>
        /// Initializes the element.
        /// </summary>
        public override void InitializeEvents()
        {
            base.InitializeEvents();
        }


	}  

	#endregion
}
  
