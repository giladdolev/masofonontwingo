﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.WebControls
{
    public class Converter
    {
        /// <summary>
        /// Converts the border style.
        /// </summary>
        /// <param name="borderStyle">The border style.</param>
        /// <returns></returns>
        internal static UI.WebControls.BorderStyle ConvertBorderStyle(BorderStyle borderStyle)
        {
            switch (borderStyle)
            {
                case System.Web.VisualTree.BorderStyle.None:
                    return UI.WebControls.BorderStyle.None;
                case System.Web.VisualTree.BorderStyle.FixedSingle:
                    return UI.WebControls.BorderStyle.Solid;
                case System.Web.VisualTree.BorderStyle.Fixed3D:
                    return UI.WebControls.BorderStyle.Inset;
            }

            return UI.WebControls.BorderStyle.NotSet;
        }

        /// <summary>
        /// Converts the border style.
        /// </summary>
        /// <param name="borderStyle">The border style.</param>
        /// <returns></returns>
        internal static BorderStyle ConvertBorderStyle(UI.WebControls.BorderStyle borderStyle)
        {
            switch (borderStyle)
            {
                case System.Web.UI.WebControls.BorderStyle.Inset:
                    return BorderStyle.Inset;
                case System.Web.UI.WebControls.BorderStyle.Outset:
                    return BorderStyle.Outset;
                case System.Web.UI.WebControls.BorderStyle.Ridge:
                    return BorderStyle.Ridge;
                case System.Web.UI.WebControls.BorderStyle.Dashed:
                    return BorderStyle.Dashed;
                case System.Web.UI.WebControls.BorderStyle.Dotted:
                    return BorderStyle.Dotted;
                case System.Web.UI.WebControls.BorderStyle.Groove:
                    return BorderStyle.Groove;

                case System.Web.UI.WebControls.BorderStyle.Solid:
                    return BorderStyle.FixedSingle;
                case System.Web.UI.WebControls.BorderStyle.NotSet:
                    return BorderStyle.NotSet;
            };
            return BorderStyle.None;
        }

        /// <summary>
        /// Converts the alignment.
        /// </summary>
        /// <param name="alignment">The alignment.</param>
        /// <returns></returns>
        public static HorizontalAlignment Convert2HorizontalAlignment(ContentAlignment alignment)
        {
            switch (alignment)
            {
                case ContentAlignment.TopCenter:
                case ContentAlignment.MiddleCenter:
                case ContentAlignment.BottomCenter:
                    return HorizontalAlignment.Center;

                case ContentAlignment.BottomRight:
                case ContentAlignment.MiddleRight:
                case ContentAlignment.TopRight:
                    return HorizontalAlignment.Right;
            }
            return HorizontalAlignment.Left;
        }
    }
}
