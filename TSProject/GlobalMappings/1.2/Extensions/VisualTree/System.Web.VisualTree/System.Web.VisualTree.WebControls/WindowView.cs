﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.UI;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.MVC;

namespace System.Web.VisualTree.WebControls
{

    [Designer("System.Web.VisualTree.Design.RootComponentDesigner, System.Web.VisualTree.Design")]
    [ToolboxData("<{0}:WindowView runat=\"server\"></{0}:WindowView>")]
    [ControlBuilder(typeof(ViewHierarchyControlBuilder))]
    public class WindowView : Window
    {
        bool _visible = true;
        public override void InitializeElementInternal()
        {
            _visible = false;
            base.InitializeElementInternal();
            this.Element.Visible = this.Visible;

            _visible = true;
        }

        /// <summary>
        /// Initializes the view element.
        /// </summary>
        /// <param name="parentController">The parent controller.</param>
        /// <param name="parentAction">The parent action.</param>
        /// <param name="parentArea">The parent area.</param>
        /// <param name="parentArguments">The parent arguments.</param>
        public void InitializeRootElement(string parentController, string parentAction, string parentArea, string parentArguments)
        {
            this.ParentController = parentController;
            if (this.IsInherited)
            {
                this.Element = CompositeView.InitializeBaseElement<WindowElement>(parentController, parentAction, parentArea, parentArguments);
            }
            else
            {
                this.CreateVisualElement();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is inherited.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is inherited; otherwise, <c>false</c>.
        /// </value>
        internal override bool IsInherited
        {
            get
            {
                return !String.IsNullOrEmpty(this.ParentController);
            }
        }

        /// <summary>
        /// The window controls
        /// </summary>
        private List<System.Web.UI.Control> _controls = new List<System.Web.UI.Control>();

        /// <summary>
        /// Flag indicating if model was initialized.
        /// </summary>
        bool _modelInitialized = false;

        /// <summary>
        /// Flag indicating we are in create view mode
        /// </summary>
        bool _preventRendering = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowView" /> class.
        /// </summary>
        public WindowView()
            : base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Window" /> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager"></param>
        internal WindowView(WindowElement visualElement, ComponentManager componentManager = null)
            : base(visualElement, componentManager)
        {

        }



        /// <summary>
        /// Gets or sets the parent.
        /// Provides support for visual inheritance
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public string ParentController { get; set; }


        /// <summary>
        /// Gets or sets the parent action.
        /// </summary>
        /// <value>
        /// The parent action.
        /// </value>
        public string ParentAction { get; set; }


        /// <summary>
        /// Gets or sets the parent area.
        /// </summary>
        /// <value>
        /// The parent area.
        /// </value>
        public string ParentArea { get; set; }


        /// <summary>
        /// Gets or sets the parent arguments.
        /// Optional Name-Value collection of rout arguments separated with | character
        /// </summary>
        /// <value>
        /// The parent arguments.
        /// </value>
        public string ParentArguments { get; set; }

        /// <summary>
        /// Gets the current rendering environment.
        /// </summary>
        /// <value>
        /// The current rendering environment.
        /// </value>
        protected override RenderingEnvironment CurrentRenderingEnvironment
        {
            get
            {
                return RenderingEnvironment.ExtJS;
            }
        }


        /// <summary>
        /// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
        /// </summary>
        /// <value>
        ///   <c>true</c> if visible; otherwise, <c>false</c>.
        /// </value>
        public override bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Init" /> event.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            bool isInitialized = false;

            // If is not in design mode
            if (!this.DesignMode)
            {
                // Ensure the window view is updated (ASPMVC model)
                this.DataBind();

                // If we should prevent synchronizing
                bool preventSynchronize = false;

                // Get window element
                WindowElement modelWindowElement = ComponentLoader.OnInitializeWindow(this, this.Element, ref _preventRendering, ref preventSynchronize) as WindowElement;

                // If there is a valid window element
                if (modelWindowElement != null)
                {
                    // If we should prevent synchronizing
                    if (preventSynchronize)
                    {
                        // Indicate model initialized
                        _modelInitialized = true;

                        // Swap the model
                        this.Element = modelWindowElement;

                        // Indicate model initialized
                        isInitialized = true;
                    }
                    else
                    {
                        // Get the window element
                        WindowElement windowElement = this.Element as WindowElement;

                        // If there is a valid window element
                        if (windowElement != null)
                        {
                            // If there is a different element
                            if (modelWindowElement != windowElement)
                            {
                                // Copy the window properties
                                SyncModel(modelWindowElement, windowElement);

                                // Swap the model
                                this.Element = modelWindowElement;
                            }

                            // Indicate model initialized
                            _modelInitialized = true;

                            // The control index
                            int index = 0;

                            // Loop all controls
                            foreach (System.Web.UI.Control control in _controls)
                            {
                                Component componentControl = control as Component;
                                if (componentControl == null || !this.IsInherited || !componentControl.IsInherited)
                                {
                                    // Add controls to the 
                                    this.AddedControl(control, index);
                                }

                                index++;
                            }

                            // The fields cache
                            Dictionary<string, FieldInfo> fieldsCache = new Dictionary<string, FieldInfo>();

                            // Fill named controls in the model
                            FillNamedControls(fieldsCache, this.Page.Controls, modelWindowElement, modelWindowElement.GetType(), false);

                            base.OnInit(e);
                            isInitialized = true;

                            // Perform the view initialized event
                            modelWindowElement.PerformInitialize();

                            // Sync window buttons
                            SyncWindowButtons(modelWindowElement, windowElement);

                            // Run window initializer
                            ComponentLoader.InitializeWindow(this, modelWindowElement);
                        }
                    }
                }
            }
            if (!isInitialized)
            {
                base.OnInit(e);
            }

            
        }


        /// <summary>
        /// Synchronizes the window buttons.
        /// </summary>
        /// <param name="modelWindowElement">The model window element.</param>
        /// <param name="windowElement">The window element.</param>
        private void SyncWindowButtons(WindowElement modelWindowElement, WindowElement windowElement)
        {
            modelWindowElement.AcceptButton = windowElement.AcceptButton;
            modelWindowElement.CancelButton = windowElement.CancelButton;

            windowElement.AcceptButton = null;
            windowElement.CancelButton = null;
        }



        /// <summary>
        /// Synchronizes the model.
        /// </summary>
        /// <param name="modelWindowElement">The model window element.</param>
        /// <param name="windowElement">The window element.</param>
        protected virtual void SyncModel(WindowElement modelWindowElement, WindowElement windowElement)
        {
            base.SyncModel(modelWindowElement, windowElement);
            modelWindowElement.ControlBox = windowElement.ControlBox;
            modelWindowElement.Icon = windowElement.Icon;
            modelWindowElement.MaximizeBox = windowElement.MaximizeBox;
            modelWindowElement.MinimizeBox = windowElement.MinimizeBox;
            modelWindowElement.StartPosition = windowElement.StartPosition;
            modelWindowElement.WindowBorderStyle = windowElement.WindowBorderStyle;
            modelWindowElement.WindowState = windowElement.WindowState;
            modelWindowElement.WindowTarget = windowElement.WindowTarget;
        }

        /// <summary>
        /// Fills the named controls.
        /// </summary>
        /// <param name="fieldsCache">The fields cache.</param>
        /// <param name="controlCollection">The control collection.</param>
        /// <param name="instance">The instance.</param>
        /// <param name="instanceType">The instance type.</param>
        /// <exception cref="System.Exception"></exception>
        private void FillNamedControls(Dictionary<string, FieldInfo> fieldsCache, ControlCollection controlCollection, WindowElement instance, Type instanceType, bool isComponentManager)
        {
            // If there is a valid control collection
            if (controlCollection != null)
            {
                // Loop all controls
                foreach (System.Web.UI.Control control in controlCollection)
                {
                    // If there is a valid control
                    if (control != null)
                    {
                        // If is a valid control id
                        string controlID = control.ID;

                        // If is a named control
                        if (controlID != null)
                        {
                            try
                            {
                                // Get tree component
                                IVisualTreeComponent treeComponent = control as IVisualTreeComponent;

                                // If there is a valid tree component
                                if (treeComponent != null)
                                {
                                    // If is component manager
                                    if (isComponentManager && instance != null)
                                    {
                                        // Get component manager container
                                        IComponentManagerContainer componentManagerContainer = instance as IComponentManagerContainer;

                                        // If there is a valid component manager container
                                        if (componentManagerContainer != null)
                                        {
                                            // Register component
                                            componentManagerContainer.RegisterComponent(treeComponent.Element);
                                        }
                                    }

                                    // Try to get field info
                                    FieldInfo fieldInfo = GetFieldInfo(fieldsCache, instanceType, controlID);

                                    // If there is a valid field
                                    if (fieldInfo != null)
                                    {
                                        // Set the field value
                                        fieldInfo.SetValue(instance, treeComponent.Element);
                                    }

                                    // If is a potential array list control
                                    if (controlID.StartsWith("_"))
                                    {
                                        // Get control ID array
                                        string[] controlIDArray = controlID.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

                                        // If there is a valid control array
                                        if (controlIDArray.Length > 1)
                                        {
                                            int index = 0;

                                            // Try to get last part as number
                                            if (int.TryParse(controlIDArray[controlIDArray.Length - 1], out index))
                                            {
                                                // Get non array id
                                                string nonArrayID = string.Join("_", controlIDArray, 0, controlIDArray.Length - 1);

                                                // If there is a valid non array id
                                                if (!string.IsNullOrEmpty(nonArrayID))
                                                {
                                                    // Try to get field info
                                                    FieldInfo arrayFieldInfo = GetFieldInfo(fieldsCache, instanceType, nonArrayID);

                                                    // If there is a valid field
                                                    if (arrayFieldInfo != null)
                                                    {
                                                        // Set the field value
                                                        IList list = arrayFieldInfo.GetValue(instance) as IList;

                                                        // If there is a valid list
                                                        if (list != null)
                                                        {
                                                            // Add the list element
                                                            while (list.Count <= index)
                                                            {
                                                                list.Add(null);
                                                            }

                                                            // Set the element at the specific index
                                                            list[index] = treeComponent.Element;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception exception)
                            {
                                // Throw a informative exception
                                throw new Exception(string.Format("Failed to set '{0}' component to in the matching module field.", controlID), exception);
                            }
                        }

                        // Fill the named controls
                        FillNamedControls(fieldsCache, control.Controls, instance, instanceType, control is ComponentManager);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the field information.
        /// </summary>
        /// <param name="fieldsCache">The fields cache.</param>
        /// <param name="instanceType">Type of the instance.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        private static FieldInfo GetFieldInfo(Dictionary<string, FieldInfo> fieldsCache, Type instanceType, string fieldName)
        {
            FieldInfo fieldInfo = null;

            if (fieldName != null)
            {
                if (!fieldsCache.TryGetValue(fieldName, out fieldInfo))
                {
                    fieldsCache[fieldName] = fieldInfo = instanceType.GetField(fieldName, Reflection.BindingFlags.Public | Reflection.BindingFlags.NonPublic | Reflection.BindingFlags.Instance);
                }
            }

            return fieldInfo;
        }

        /// <summary>
        /// Addeds the control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="index">The index.</param>
        protected override void AddedControl(System.Web.UI.Control control, int index)
        {
            // If model was initialized
            if (this.DesignMode || _modelInitialized)
            {
                base.AddedControl(control, index);
            }
            else
            {
                _controls.Add(control);
            }
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            // If is not in prevent rendering mode
            if (!_preventRendering)
            {
                base.RenderControl(writer);
            }
        }


        /// <summary>
        /// Gets or sets the render to.
        /// </summary>
        /// <value>
        /// The render to.
        /// </value>
        public string RenderTo
        {
            get;
            set;
        }

        /// <summary>
        /// Renders the element to.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="output">The output.</param>
        /// <param name="renderTo">The render to.</param>
        protected override void RenderElementTo(RenderingContext renderingContext, HtmlTextWriter output, string renderTo = null)
        {
            // If is in design mode
            if (this.DesignMode)
            {
                base.RenderElementTo(renderingContext, output);
            }
            else
            {
                base.RenderElementTo(renderingContext, output, this.RenderTo);
            }
        }


    }
}
