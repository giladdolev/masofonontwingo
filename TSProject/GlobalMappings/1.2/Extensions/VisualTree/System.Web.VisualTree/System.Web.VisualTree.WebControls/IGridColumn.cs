﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.WebControls
{
    public interface IGridColumn
    {
        void InitializeElement();
        System.Web.VisualTree.Elements.GridColumn Element { get; }
    }
    public interface IToolBarItem
    {
        void InitializeElement();
        System.Web.VisualTree.Elements.ToolBarItem Element { get; }
    }
}
