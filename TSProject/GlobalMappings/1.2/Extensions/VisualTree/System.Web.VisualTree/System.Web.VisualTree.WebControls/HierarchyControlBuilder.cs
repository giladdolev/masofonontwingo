﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace System.Web.VisualTree.WebControls
{
    public class ViewHierarchyControlBuilder : ControlBuilder 
    {
        /// <summary>
        /// Determines whether white space literals are permitted in the content between a control's opening and closing tags. This method is called by the ASP.NET page framework.
        /// </summary>
        /// <returns>
        /// Always returns true.
        /// </returns>
        public override bool AllowWhitespaceLiterals()
        {
            return false;
        }

        /// <summary>
        /// Enables custom control builders to access the generated Code Document Object Model (CodeDom) and insert and modify code during the process of parsing and building controls.
        /// </summary>
        /// <param name="codeCompileUnit">The root container of a CodeDOM graph of the control that is being built.</param>
        /// <param name="baseType">The base type of the page or user control that contains the control that is being built.</param>
        /// <param name="derivedType">The derived type of the page or user control that contains the control that is being built.</param>
        /// <param name="buildMethod">The code that is used to build the control.</param>
        /// <param name="dataBindingMethod">The code that is used to build the data-binding method of the control.</param>
        public override void ProcessGeneratedCode(CodeDom.CodeCompileUnit codeCompileUnit, CodeDom.CodeTypeDeclaration baseType, 
            CodeDom.CodeTypeDeclaration derivedType, CodeDom.CodeMemberMethod buildMethod, CodeDom.CodeMemberMethod dataBindingMethod)
        {
            base.ProcessGeneratedCode(codeCompileUnit, baseType, derivedType, buildMethod, dataBindingMethod);

            // Check validity of ASPX compilation unit
            if (codeCompileUnit != null && codeCompileUnit.Namespaces != null && codeCompileUnit.Namespaces.Count > 0)
            {
                // Get namespace
                CodeNamespace rootNamespace = codeCompileUnit.Namespaces[0];

                // Validate types
                if (rootNamespace.Types != null && rootNamespace.Types.Count > 0)
                {
                    // Get view type
                    CodeTypeDeclaration viewType = rootNamespace.Types[0];

                    // Add field to reference root component
                    viewType.Members.Add(new CodeMemberField(
                        new CodeTypeReference("System.Web.VisualTree.WebControls.Component"), 
                        ControlBuilderHelper.ROOT_COMPONENT_NAME));
                }
            }

            // Check validity of control build method
            if (buildMethod != null && buildMethod.Statements != null)
            {
                int initializationIndex = ControlBuilderHelper.GetComponentInitializationStatementIndex(buildMethod);
                if (initializationIndex >= 0)
                {
                    // Collect parent access parameters
                    Dictionary<string, string> arguments = CollectValues(buildMethod, "ParentController", "ParentAction", "ParentArea", "ParentArguments");

                    // Get parent controller hierarchy value
                    string parentController = GetSimpleValue(arguments, "ParentController");

                    // Create root component reference assignment
                    CodeAssignStatement rootComponentAssignment = new CodeAssignStatement(
                        new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), ControlBuilderHelper.ROOT_COMPONENT_NAME),
                        GetRootComponentReferenceExpression(parentController));

                    // Create Element initialization method invocation
                    CodeExpressionStatement componentInitialization = new CodeExpressionStatement(
                        new CodeMethodInvokeExpression(
                            new CodeMethodReferenceExpression(
                                new CodeVariableReferenceExpression(ControlBuilderHelper.COMPONENT_NAME), "InitializeRootElement"),
                            new CodePrimitiveExpression(parentController), 
                                new CodePrimitiveExpression(GetSimpleValue(arguments, "ParentAction")), 
                                new CodePrimitiveExpression(GetSimpleValue(arguments, "ParentArea")), 
                                new CodePrimitiveExpression(GetSimpleValue(arguments, "ParentArguments"))));

                    // Check if component creation statement is a last statement in build method
                    if (buildMethod.Statements.Count > initializationIndex + 1)
                    {
                        // Insert new statements after component creation statement
                        buildMethod.Statements.Insert(initializationIndex + 1, rootComponentAssignment);
                        buildMethod.Statements.Insert(initializationIndex + 2, componentInitialization);
                    }
                    else
                    {
                        // Add new statements
                        buildMethod.Statements.Add(rootComponentAssignment);
                        buildMethod.Statements.Add(componentInitialization);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the root component reference expression.
        /// </summary>
        /// <param name="parentController">The parent controller.</param>
        /// <returns></returns>
        private static CodeExpression GetRootComponentReferenceExpression(string parentController)
        {
            if (String.IsNullOrEmpty(parentController))
            {
                return new CodePrimitiveExpression(null);
            }

            // Initialize root control in case of hierarchy only
            return new CodeVariableReferenceExpression(ControlBuilderHelper.COMPONENT_NAME);
        }

        /// <summary>
        /// Collects the values.
        /// </summary>
        /// <param name="buildMethod">The build method.</param>
        /// <param name="attributeNames">The parameter names.</param>
        /// <returns></returns>
        private Dictionary<string, string> CollectValues(CodeDom.CodeMemberMethod buildMethod, params string[] attributeNames)
        {
            // Initialize attributes dictionary
            Dictionary<string, string> attributes = new Dictionary<string, string>();

            // Initialize counter of attributes to be found
            int parametersCount = 0;
            foreach (CodeStatement item in buildMethod.Statements)
            {
                // Looking for assignment statements
                CodeAssignStatement statement = item as CodeAssignStatement;
                if (statement != null)
                {
                    // Looking for assignment to component property
                    CodePropertyReferenceExpression propertyReference = statement.Left as CodePropertyReferenceExpression;
                    if (propertyReference != null)
                    {
                        // Looking for assignment to root component property (__ctrl.PropertyName)
                        CodeVariableReferenceExpression componentReference = propertyReference.TargetObject as CodeVariableReferenceExpression;
                        if (componentReference != null && componentReference.VariableName == ControlBuilderHelper.COMPONENT_NAME &&
                            attributeNames.Contains(propertyReference.PropertyName))
                        {
                            // Get assigned value expression
                            CodePrimitiveExpression attributeValueExpression = statement.Right as CodePrimitiveExpression;
                            if (attributeValueExpression != null)
                            {
                                // Add attribute value to dictionary
                                attributes.Add(propertyReference.PropertyName, attributeValueExpression.Value as string);
                                if (++parametersCount >= attributeNames.Length)
                                {
                                    // Stop if all requested attributes found
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return attributes;
        }

        /// <summary>
        /// Gets the simple value.
        /// </summary>
        /// <param name="attributes">The attributes dictionary.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        private static string GetSimpleValue(Dictionary<string, string> attributes, string propertyName)
        {
            string value = String.Empty;
            attributes.TryGetValue(propertyName, out value);

            return value;
        }

    }
    public class HierarchyControlBuilder : ControlBuilder
    {
        /// <summary>
        /// Determines whether white space literals are permitted in the content between a control's opening and closing tags. This method is called by the ASP.NET page framework.
        /// </summary>
        /// <returns>
        /// Always returns true.
        /// </returns>
        public override bool AllowWhitespaceLiterals()
        {
            return false;
        }
        /// <summary>
        /// Enables custom control builders to access the generated Code Document Object Model (CodeDom) and insert and modify code during the process of parsing and building controls.
        /// </summary>
        /// <param name="codeCompileUnit">The root container of a CodeDOM graph of the control that is being built.</param>
        /// <param name="baseType">The base type of the page or user control that contains the control that is being built.</param>
        /// <param name="derivedType">The derived type of the page or user control that contains the control that is being built.</param>
        /// <param name="buildMethod">The code that is used to build the control.</param>
        /// <param name="dataBindingMethod">The code that is used to build the data-binding method of the control.</param>
        public override void ProcessGeneratedCode(CodeDom.CodeCompileUnit codeCompileUnit, CodeDom.CodeTypeDeclaration baseType, CodeDom.CodeTypeDeclaration derivedType, CodeDom.CodeMemberMethod buildMethod, CodeDom.CodeMemberMethod dataBindingMethod)
        {
            base.ProcessGeneratedCode(codeCompileUnit, baseType, derivedType, buildMethod, dataBindingMethod);

            // Check validity of control build method
            if (buildMethod != null && buildMethod.Statements != null)
            {
                int initializationIndex = ControlBuilderHelper.GetComponentInitializationStatementIndex(buildMethod);
                if (initializationIndex >= 0)
                {
                    // Create Element initialization method invocation
                    CodeExpressionStatement componentInitialization = new CodeExpressionStatement(
                        new CodeMethodInvokeExpression(
                            new CodeMethodReferenceExpression(
                                new CodeVariableReferenceExpression(ControlBuilderHelper.COMPONENT_NAME), 
                                "InitializeComponentElement"),
                                new CodePrimitiveExpression(GetID(this.ID)),
                                new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), ControlBuilderHelper.ROOT_COMPONENT_NAME)));

                    // Check if component creation statement is a last statement in build method
                    if (buildMethod.Statements.Count > initializationIndex + 1)
                    {
                        // Insert new statements after component creation statement
                        buildMethod.Statements.Insert(initializationIndex + 1, componentInitialization);
                    }
                    else
                    {
                        // Add new statements
                        buildMethod.Statements.Add(componentInitialization);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the identifier.
        /// Pass null instead of default id to initialization method
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        private static string GetID(string id)
        {
            if (String.IsNullOrEmpty(id) || id.StartsWith("__control"))
            {
                return null;
            }

            return id;
        }
    }

    internal static class ControlBuilderHelper
    {
        internal const string ROOT_COMPONENT_NAME = "__rootComponent";
        internal const string COMPONENT_NAME = "__ctrl";
        /// <summary>
        /// Gets the index of the component initialization statement.
        /// </summary>
        /// <param name="buildMethod">The build method.</param>
        /// <returns></returns>
        internal static int GetComponentInitializationStatementIndex(CodeDom.CodeMemberMethod buildMethod)
        {
            // Find component initialization statement
            foreach (CodeStatement statement in buildMethod.Statements)
            {
                CodeAssignStatement assignment = statement as CodeAssignStatement;
                if (assignment != null)
                {
                    CodeVariableReferenceExpression componentVariable = assignment.Left as CodeVariableReferenceExpression;
                    if (componentVariable != null && componentVariable.VariableName == COMPONENT_NAME)
                    {
                        return buildMethod.Statements.IndexOf(statement);
                    }
                }
            }

            return -1;
        }
    }
}
