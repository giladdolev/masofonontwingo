﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.WebControls
{

    [Designer("System.Web.VisualTree.Design.TRootComponentDesigner, System.Web.VisualTree.Design")]
    [ToolboxData("<{0}:TCompositeView runat=\"server\"></{0}:TCompositeView>")]
    public class TCompositeView : CompositeView
    {




        /// <summary>
        /// Renders the HTML opening tag of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter" /> that represents the output stream to render HTML content on the client.</param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {

        }

        /// <summary>
        /// Gets the current rendering environment.
        /// </summary>
        /// <value>
        /// The current rendering environment.
        /// </value>
        protected override RenderingEnvironment CurrentRenderingEnvironment
        {
            get
            {
                return RenderingEnvironment.ExtJSTouch;
            }
        }


        /// <summary>
        /// Renders the HTML closing tag of the control into the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter" /> that represents the output stream to render HTML content on the client.</param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {

        }
    }
}
