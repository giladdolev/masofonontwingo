﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.WebControls
{
	public partial class Control
    {
        /// <summary>
        /// Gets or sets the foreground color (typically the color of the text) of the Web server control.
        /// </summary>
        public override Drawing.Color ForeColor
        {
            get { return this.Element.ForeColor; }
            set { this.Element.ForeColor = value; }
        }
        /// <summary>
        /// Gets or sets the background color of the Web server control.
        /// </summary>
        public override Drawing.Color BackColor
        {
            get { return this.Element.BackColor; }
            set { this.Element.BackColor = value; }
        }
        /// <summary>
        /// Gets or sets the width of the Web server control.
        /// </summary>
        public override UI.WebControls.Unit Width
        {
            get { return this.Element.Width; }
            set { this.Element.Width = value; }
        }
        /// <summary>
        /// Gets or sets the height of the Web server control.
        /// </summary>
        public override UI.WebControls.Unit Height
        {
            get { return this.Element.Height; }
            set { this.Element.Height = value; }
        }
        /// <summary>
        /// Gets or sets the tab index of the Web server control.
        /// </summary>
        public override short TabIndex
        {
            get { return (short)this.Element.TabIndex; }
            set { this.Element.TabIndex = value; }
        }
        /// <summary>
        /// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
        /// </summary>
        public override bool Visible
        {
            get { return this.Element.Visible; }
            set { this.Element.Visible = value; }
        }
        /// <summary>
        /// Gets or sets a value indicating whether the Web server control is enabled.
        /// </summary>
        public override bool Enabled
        {
            get { return this.Element.Enabled; }
            set { this.Element.Enabled = value; }
        }
        /// <summary>
        /// Gets or sets the skin to apply to the control.
        /// </summary>
        public override string SkinID
        {
            get { return this.Element.SkinID; }
            set { this.Element.SkinID = value; }
        }
        /// <summary>
        /// Gets or sets the border style of the Web server control.
        /// </summary>
        public override UI.WebControls.BorderStyle BorderStyle
        {
            get { return Converter.ConvertBorderStyle(this.Element.BorderStyle); }
            set
            {
                if (value != UI.WebControls.BorderStyle.NotSet)
                {
                    this.Element.BorderStyle = Converter.ConvertBorderStyle(value);
                }
            }
        }
        /// <summary>
        /// Gets or sets the border color of the Web control.
        /// </summary>
        public override Drawing.Color BorderColor
        {
            get { return this.Element.BorderColor; }
            set { this.Element.BorderColor = value; }
        }

        /// <summary>
        /// Synchronizes the model.
        /// </summary>
        /// <param name="modelControlElement">The model control element.</param>
        /// <param name="compositeElement">The composite element.</param>
        protected virtual void SyncModel(Elements.ControlElement modelControlElement, Elements.ControlElement compositeElement)
        {
            // Set rendering environment
            Elements.ApplicationElement.CurrentRenderingEnvironment = this.CurrentRenderingEnvironment;

            modelControlElement.Anchor = compositeElement.Anchor;
            modelControlElement.BackColor = compositeElement.BackColor;
            modelControlElement.BackgroundImage = compositeElement.BackgroundImage;
            modelControlElement.BorderColor = compositeElement.BorderColor;
            modelControlElement.BorderStyle = compositeElement.BorderStyle;
            modelControlElement.ClientSize = compositeElement.ClientSize;
            modelControlElement.CssClass = compositeElement.CssClass;
            modelControlElement.Dock = compositeElement.Dock;
            modelControlElement.Font = compositeElement.Font;
            modelControlElement.ForeColor = compositeElement.ForeColor;
            modelControlElement.ID = compositeElement.ID;
            modelControlElement.Location = compositeElement.Location;
            modelControlElement.Menu = compositeElement.Menu;
            modelControlElement.PixelHeight = compositeElement.Size.Height;
            modelControlElement.PixelWidth = compositeElement.Size.Width;
            modelControlElement.RightToLeft = compositeElement.RightToLeft;
            modelControlElement.Text = compositeElement.Text;
            modelControlElement.ZIndex = compositeElement.ZIndex;

            // Loop all child elements
            foreach (Elements.VisualElement childElement in compositeElement.Children)
            {
                // Add child element
                modelControlElement.Add(childElement, false);
            }

        }
    }
}
