﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.VisualTree.Elements;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Common;


namespace System.Web.VisualTree.WebControls
{


    /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
    [Designer("System.Web.VisualTree.Design.ComponentDesigner, System.Web.VisualTree.Design")]
    [ParseChildren]
    [ControlBuilder(typeof(HierarchyControlBuilder))]
    public abstract class Component : WebControl, IVisualTreeComponent
    {

        /// <summary>
        /// The visual element
        /// </summary>
        private VisualElement _visualElement = null;
        private bool _initialized = false;


        /// <summary>
        /// Initializes a new instance of the <see cref="Component"/> class.
        /// </summary>
        public Component()
        {
            EnsureID();
        }

        /// <summary>
        /// Creates the visual element.
        /// </summary>
        /// <returns></returns>
        protected abstract void CreateVisualElement();

        /// <summary>
        /// Determines whether is there loop
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="childElement">The child element.</param>
        /// <returns>If there is loop</returns>
        bool IsThereLoop(VisualElement visualElement, VisualElement childElement)
        {
            // If one of the parameters is null - there is no loop
            if (childElement == null || visualElement == null)
            {
                return false;
            }
            // If the child is the same - there is loop
            if (childElement == visualElement)
            {
                return true;
            }
            // Check the next parent
            bool blnIsThereLoop = IsThereLoop(visualElement.ParentElement, childElement);
            return blnIsThereLoop;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Component"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="componentManager">The component manager.</param>
        public Component(VisualElement visualElement, ComponentManager componentManager = null, bool shouldGenerateChildren = true)
        {

            if (shouldGenerateChildren)
            {
                List<WebControl> controls = new List<WebControl>();

                // Loop all child element
                foreach (VisualElement childElement in visualElement.Children)
                {
                    // If a loop may be created - skip
                    if (IsThereLoop(visualElement, childElement))
                    {
                        continue;
                    }

                    // If child element is not an internal implementation control
                    if (!(childElement is IInternalImplementationElement))
                    {
                        // Get control 
                        WebControl control = ControlUtils.GetControl(childElement, componentManager);

                        // If there is a valid control
                        if (control != null)
                        {
                            // If there is a valid child element id
                            if (!string.IsNullOrEmpty(childElement.ID))
                            {
                                // Set the control id
                                control.ID = childElement.ID;
                            }

                            // Add child control
                            controls.Add(control);
                        }
                    }
                }

                // If is a control element (add controls in reverse)
                if (visualElement.ChildrenOrderReverted)
                {
                    // Add the controls in a reversed order
                    for (int index = controls.Count - 1; index >= 0; index--)
                    {
                        // Add control
                        this.Controls.Add(controls[index]);
                    }
                }
                else
                {
                    // Add controls
                    foreach (WebControl control in controls)
                    {
                        // Add control
                        this.Controls.Add(control);
                    }
                }
            }

            // Set current visual element
            _visualElement = visualElement;

            // Initialize components
            this.InitializeComponents(componentManager);

            // Initialize the control
            this.InitializeControl();

            // If there is a valid component manager
            if (componentManager != null)
            {
                // Revise control
                componentManager.ReviseControl(this);
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
        /// </summary>
        public override bool Visible
        {
            get
            {
                // If there is a valid visual element
                if (_visualElement != null)
                {
                    // Get the control element
                    ControlElement controlElement = _visualElement as ControlElement;

                    // If there is a valid control element
                    if (controlElement != null)
                    {
                        // Set visibility
                        return controlElement.Visible;
                    }
                }
                return true;
            }
            set
            {
                // If there is a valid visual element
                if (_visualElement != null)
                {
                    // Get the control element
                    ControlElement controlElement = _visualElement as ControlElement;

                    // If there is a valid control element
                    if (controlElement != null)
                    {
                        // Set visibility
                        controlElement.Visible = value;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the programmatic identifier assigned to the server control.
        /// </summary>
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                if (_visualElement != null)
                {
                    _visualElement.ID = value;
                }
                base.ID = value;
            }
        }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>
        /// The data source.
        /// </value>
        [DefaultValue(null)]
        public object DataSource
        {
            get { return this.Element.DataSource; }
            set { this.Element.DataSource = value; }
        }

        /// <summary>
        /// Initializes the components.
        /// </summary>
        /// <param name="componentManager">The component manager.</param>
        private void InitializeComponents(ComponentManager componentManager)
        {
            // If there is a valid visual element
            if (_visualElement != null && componentManager != null)
            {
                // Get the control element
                ControlElement controlElement = _visualElement as ControlElement;

                // If there is a valid control element
                if (controlElement != null)
                {
                    // Register the component
                    componentManager.RegisterComponent(controlElement.Menu);
                }
            }
        }

        /// <summary>
        /// Gets the string from resource.
        /// </summary>
        /// <param name="resourceReference">The resource reference.</param>
        /// <returns></returns>
        protected string GetStringFromResource(System.Web.VisualTree.Elements.ResourceReference resourceReference)
        {
            if (resourceReference != null)
            {
                string path = GetUrlPath(resourceReference.Source);

                return path;
            }

            return null;
        }

        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the 
        /// full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        public static string GetAbsoluteUrlFromRelative(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            HttpContext objHttpContext = RenderingContext.CurrentHttpContext;
            if (objHttpContext == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = objHttpContext.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        /// <summary>
        /// Gets the URL path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private string GetUrlPath(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                // Check if the path is app relative (~/...)
                if (IsRelativePath(path))
                {
                    path = GetAbsoluteUrlFromRelative(path);
                }
            }

            // path is null or remote URL
            return path;
        }

        /// <summary>
        /// Determines whether the path is a relative URL.
        /// If the path is app relative (~/...), we cannot take shortcuts, since
        /// the ~ is meaningless on the client, and must be resolved
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static bool IsRelativePath(string path)
        {

            if (path == null)
                return false;

            int len = path.Length;

            // Empty string case
            if (len == 0) return false;

            // It must start with ~
            if (path[0] != '~')
                return false;

            // Single character case: "~"
            if (len == 1)
                return true;

            // If it's longer, checks if it starts with "~/" or "~\"
            return path[1] == '\\' || path[1] == '/';
        }

        /// <summary>
        /// Determines whether the specified path is physical path
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static bool IsPhysicalPath(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the resource from string.
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <returns></returns>
        protected System.Web.VisualTree.Elements.ResourceReference GetResourceFromString(string resource)
        {
            if (!string.IsNullOrEmpty(resource))
            {
                if (IsPhysicalPath(resource))
                {
                    return new Bitmap(resource);
                }

                string path = GetUrlPath(resource);
                return new System.Web.VisualTree.Elements.UrlReference(path);
            }

            return null;
        }

        /// <summary>
        /// Called after a child control is added to the <see cref="P:System.Web.UI.Control.Controls" /> collection of the <see cref="T:System.Web.UI.Control" /> object.
        /// </summary>
        /// <param name="control">The <see cref="T:System.Web.UI.Control" /> that has been added.</param>
        /// <param name="index">The index of the control in the <see cref="P:System.Web.UI.Control.Controls" /> collection.</param>
        protected override void AddedControl(UI.Control control, int index)
        {
            base.AddedControl(control, index);

            Component objComponentControl = control as Component;
            if (objComponentControl != null && objComponentControl.IsInherited)
            {
                return;
            }
            // Add visual tree component
            this.AddVisualTreeComponent(control as IVisualTreeComponent);
        }

        /// <summary>
        /// Binds a data source to the invoked server control and all its child controls.
        /// </summary>
        public override void DataBind()
        {
            base.DataBind();

            // Loop all child controls
            foreach (System.Web.UI.Control control in this.Controls)
            {
                // Try to get component generator
                IVisualTreeComponentGenerator componentGenerator = control as IVisualTreeComponentGenerator;

                // If there is a valid component generator
                if (componentGenerator != null)
                {
                    // Loop all generated components
                    foreach (IVisualTreeComponent generatedComponent in componentGenerator.GeneratedComponents)
                    {
                        // Return generated component
                        this.AddVisualTreeComponent(generatedComponent);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the visual tree component.
        /// </summary>
        /// <param name="visualTreeComponent">The visual tree component.</param>
        private void AddVisualTreeComponent(IVisualTreeComponent visualTreeComponent)
        {
            // If there is a valid visual tree control
            if (visualTreeComponent != null && _visualElement != null)
            {
                PartialControl partialControl = visualTreeComponent as PartialControl;

                if (partialControl != null)
                {
                    partialControl.Load();
                }

                // Get the control element
                ControlElement controlElement = _visualElement as ControlElement;

                // If there is a valid control element
                if (controlElement != null)
                {
                    // Get added control element
                    ControlElement addedControlElement = visualTreeComponent.Element as ControlElement;

                    // If there is a valid added control element
                    if (addedControlElement != null)
                    {
                        // If we are not creating a parent-child-parent relation
                        if (controlElement != addedControlElement)
                        {
                            // Insert control element at first position
                            _visualElement.Add(addedControlElement, true);
                        }
                    }
                    else
                    {
                        // Add visual tree element
                        _visualElement.Add(visualTreeComponent.Element, false);
                    }
                }
                else
                {
                    // Add visual tree element
                    _visualElement.Add(visualTreeComponent.Element, false);
                }
            }
        }
        /// <summary>
        /// Initializes the events.
        /// </summary>
        public virtual void InitializeEvents()
        {
        }

        /// <summary>
        /// Initializes the element.
        /// </summary>
        public virtual void InitializeElement()
        {
            this.OnInit(EventArgs.Empty);
        }
        /// <summary>
        /// Initializes the element internal.
        /// </summary>
        public virtual void InitializeElementInternal()
        {
            // If is not in view debugging context
            if (!RenderingContext.IsViewDebuggingContext)
            {
                // Initialize element events from actions
                InitializeEvents();
            }

            // Get control element
            IControlElement controlElement = _visualElement as IControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                controlElement.ID = this.ID;
                SetFont(controlElement);

                LoadAttributes();
            }
        }

        /// <summary>
        /// Loads the attributes.
        /// </summary>
        protected virtual void LoadAttributes()
        {
            foreach (object keyObject in this.Attributes.Keys)
            {
                // Get key as string
                string key = keyObject as string;

                // If there is a valid key
                if (!string.IsNullOrEmpty(key))
                {
                    // Set the current attribute
                    _visualElement.SetAttribute(key, this.Attributes[key]);
                }
            }
        }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        public virtual void InitializeControl()
        {
            // If there is a valid visual element
            if (_visualElement != null)
            {
                // Set the control id
                this.ID = _visualElement.ID;
            }

            // Get control element
            IControlElement controlElement = _visualElement as IControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                GetFont(controlElement);
            }
        }

        /// <summary>
        /// Gets the font.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        private void GetFont(IControlElement controlElement)
        {
            // If there is a valid control element
            if (controlElement != null)
            {
                // Get elements font
                System.Drawing.Font font = controlElement.Font;
                if (font != null && !String.IsNullOrEmpty(font.Name))
                {
                    if (!String.Equals(font.Name, ControlElement.DefaultFont.Name))
                    {
                        this.Font.Name = font.Name;
                    }
                    if (font.Size != ControlElement.DefaultFont.Size)
                    {
                        this.Font.Size = new FontUnit(font.Size);
                    }
                    if (font.Bold) this.Font.Bold = font.Bold;
                    if (font.Italic) this.Font.Italic = font.Italic;
                    if (font.Underline) this.Font.Underline = font.Underline;
                    if (font.Strikeout) this.Font.Strikeout = font.Strikeout;
                }
            }
        }

        /// <summary>
        /// Sets the font.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        private void SetFont(IControlElement controlElement)
        {
            // If there is a valid control element
            if (controlElement != null)
            {
                // Get Web Controls font
                FontInfo controlFont = this.Font;

                // Check font is set
                if (controlFont != null && !string.IsNullOrEmpty(controlFont.Name))
                {
                    // Set font style
                    FontStyle style = FontStyle.Regular;
                    if (controlFont.Bold)
                    {
                        style |= FontStyle.Bold;
                    }
                    if (controlFont.Italic)
                    {
                        style |= FontStyle.Italic;
                    }
                    if (controlFont.Underline)
                    {
                        style |= FontStyle.Underline;
                    }
                    if (controlFont.Strikeout)
                    {
                        style |= FontStyle.Strikeout;
                    }

                    string fontName = controlFont.Name;
                    if (String.IsNullOrEmpty(fontName))
                    {
                        fontName = SystemFonts.DefaultFont.Name;
                    }
                    float size = (float)controlFont.Size.Unit.Value;
                    if (size <= 0)
                    {
                        size = SystemFonts.DefaultFont.Size;
                    }

                    // Create font object
                    Font font = new Font(fontName, size, style);

                    // Set visual elements font
                    controlElement.Font = font;
                }
                else if (!this.IsInherited)
                {
                    controlElement.Font = SystemFonts.DefaultFont;
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            if (!_initialized)
            {
                base.OnInit(e);
                InitializeElementInternal();
                _initialized = true;
            }
        }

        /// <summary>
        /// The inherited flag
        /// </summary>
        private bool _isInherited;

        /// <summary>
        /// Gets a value indicating whether this instance is inherited.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is inherited; otherwise, <c>false</c>.
        /// </value>
        internal virtual bool IsInherited
        {
            get { return _isInherited; }
        }
        
        /// <summary>
        /// Initializes the component element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="rootComponent">The root component.</param>
        public void InitializeComponentElement(string id, Component rootComponent)
        {
            if (id != null && rootComponent != null)
            {
                // Check valid control element
                ControlElement rootElement = rootComponent.Element as ControlElement;
                if (rootElement != null)
                {
                    ControlElement control = LocateChildControl(id, rootElement.Controls);
                    if (control != null)
                    {
                        this.Element = control;
                        _isInherited = true;
                        return;
                    }
                }
            }

            this.CreateVisualElement();
        }

        /// <summary>
        /// Locates the child control.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="controls">The controls.</param>
        /// <returns></returns>
        private static ControlElement LocateChildControl(string id, ControlElementCollection controls)
        {
            // Perform in depth search for component id in root component hierarchy
            foreach (ControlElement control in controls)
            {
                if (control.ID == id)
                {
                    return control;
                }
                ControlElement locatedControl = LocateChildControl(id, control.Controls);
                if (locatedControl != null)
                {
                    return locatedControl;
                }
            }
            return null;
        }
        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public VisualElement Element
        {
            get
            {
                return _visualElement;
            }
            protected set
            {
                // Set the visual element
                _visualElement = value;
            }
        }

        /// <summary>
        /// Renders the contents.
        /// </summary>
        /// <param name="output">The output.</param>
        protected override void RenderContents(HtmlTextWriter output)
        {
            RenderElementTo(new RenderingContext(this.CurrentRenderingEnvironment, false), output);
        }

        /// <summary>
        /// Gets the current rendering environment.
        /// </summary>
        /// <value>
        /// The current rendering environment.
        /// </value>
        protected virtual RenderingEnvironment CurrentRenderingEnvironment
        {
            get
            {
                return RenderingEnvironment.Default;
            }
        }

        /// <summary>
        /// Renders the element to.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="output">The output.</param>
        /// <param name="renderTo">The render to.</param>
        protected virtual void RenderElementTo(RenderingContext renderingContext, HtmlTextWriter output, string renderTo = null)
        {
            // If we should render the contents of the control
            if (_visualElement is WindowElement || _visualElement is CompositeElement)
            {
                // Render control
                ComponentRenderer.Render(renderingContext, _visualElement, output, renderTo);
            }
        }

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        VisualElement IVisualTreeComponent.Element
        {
            get
            {
                return _visualElement;
            }
        }

        /// <summary>
        /// Gets the height of the Web server control.
        /// </summary>
        Unit IVisualTreeComponent.Height
        {
            get
            {
                // Get control element
                ControlElement controlElement = _visualElement as ControlElement;

                // If there is a valid control element
                if (controlElement != null)
                {
                    // Return width
                    return controlElement.Height;
                }

                return base.Height;
            }
        }

        /// <summary>
        /// Gets the width of the Web server control.
        /// </summary>
        Unit IVisualTreeComponent.Width
        {
            get
            {
                // Get control element
                ControlElement controlElement = _visualElement as ControlElement;

                // If there is a valid control element
                if (controlElement != null)
                {
                    // Return height
                    return controlElement.Width;
                }

                return base.Width;
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        IEnumerable<IVisualTreeComponent> IVisualTreeComponent.Children
        {
            get
            {
                return this.GetChildren();
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerable<IVisualTreeComponent> GetChildren()
        {
            // Loop all controls
            foreach (Control control in this.Controls)
            {
                // Get component
                IVisualTreeComponent component = control as IVisualTreeComponent;

                // If there is a valid component
                if (component != null)
                {
                    // Return component
                    yield return component;
                }
                else
                {
                    // Try to get component generator
                    IVisualTreeComponentGenerator componentGenerator = control as IVisualTreeComponentGenerator;

                    // If there is a valid component generator
                    if (componentGenerator != null)
                    {
                        // Loop all generated components
                        foreach (IVisualTreeComponent generatedComponent in componentGenerator.GeneratedComponents)
                        {
                            // Return generated component
                            yield return generatedComponent;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the handler.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal static IVisualElementAction<T> GetHandler<T>(IVisualElement visualElement, string value)
        {
            if (value != null)
            {
                string[] actionParts = value.Split(',', '.', '\\', '/');

                if (actionParts.Length > 1)
                {

                    IVisualElementAction<T> elementAction = ComponentLoader.CreateAction<T>(visualElement, actionParts[0], actionParts[1]);

                    return elementAction;
                }
            }
            return null;
        }


        /// <summary>
        /// Ensures component is visible.
        /// </summary>
        void IVisualTreeComponent.EnsureVisible()
        {
            // Make sure element is visible
            this.Visible = true;
        }

    }

    public interface IVisualTreeComponentGenerator
    {

        /// <summary>
        /// Gets the generated components.
        /// </summary>
        /// <value>
        /// The generated components.
        /// </value>
        IEnumerable<IVisualTreeComponent> GeneratedComponents
        {
            get;
        }
    }

    public interface IVisualTreeComponent : IComponent
    {

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string ID
        {
            get;
        }

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        VisualElement Element
        {
            get;
        }

        /// <summary>
        /// Gets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        Unit Height
        {
            get;
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        Unit Width
        {
            get;
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        IEnumerable<IVisualTreeComponent> Children
        {
            get;
        }

        /// <summary>
        /// Ensures component is visible.
        /// </summary>
        void EnsureVisible();
    }
}
