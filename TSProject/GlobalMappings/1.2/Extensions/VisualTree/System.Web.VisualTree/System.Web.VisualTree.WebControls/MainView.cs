﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.WebControls
{
    [Designer("System.Web.VisualTree.Design.RootComponentDesigner, System.Web.VisualTree.Design")]
    [ToolboxData("<{0}:MainView runat=\"server\"></{0}:MainView>")]
    public class MainView : Component
    {
        /// <summary>
        /// Creates the visual element.
        /// </summary>
        protected override void CreateVisualElement()
        {
            this.Element = ApplicationElement.MainWindow;
        }

    }

    public class TMainView : MainView
    {
        /// <summary>
        /// Gets the current rendering environment.
        /// </summary>
        /// <value>
        /// The current rendering environment.
        /// </value>
        protected override RenderingEnvironment CurrentRenderingEnvironment
        {
            get
            {
                return RenderingEnvironment.ExtJSTouch;
            }
        }
    }
}
