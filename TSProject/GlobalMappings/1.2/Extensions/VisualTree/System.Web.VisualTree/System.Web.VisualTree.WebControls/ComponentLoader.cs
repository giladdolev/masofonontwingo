﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.WebControls
{
    internal class ComponentLoader
    {
        [Import(typeof(IVisualElementProvider))]
        private IVisualElementProvider _visualElementProvider;

        private static ComponentLoader _loader = new ComponentLoader();

        /// <summary>
        /// Initializes the components.
        /// </summary>
        private ComponentLoader()
        {
            LoadingHelper.CombineCatalog(null, this);
        }

        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="windowControl">The window control.</param>
        /// <param name="element">The element.</param>
        /// <param name="preventRendering">if set to <c>true</c> prevent rendering.</param>
        internal static IVisualElement OnInitializeWindow(Control windowControl, IVisualElement element, ref bool preventRendering)
        {
            bool preventSynchronize = false;
            return OnInitializeWindow(windowControl, element, ref preventRendering, ref preventSynchronize);
        }

        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="windowControl">The window control.</param>
        /// <param name="element">The element.</param>
        /// <param name="preventRendering">if set to <c>true</c> prevent rendering.</param>
        /// <param name="preventSynchronizing">if set to <c>true</c> prevent synchronizing.</param>
        /// <returns></returns>
        internal static IVisualElement OnInitializeWindow(Control windowControl, IVisualElement element, ref bool preventRendering, ref bool preventSynchronize)
        {
            if (_loader != null && _loader._visualElementProvider != null)
            {
                return _loader._visualElementProvider.OnInitializeWindow(windowControl, element, ref preventRendering, ref preventSynchronize);
            }
            return null;
        }
        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="windowControl">The window control.</param>
        /// <param name="windowElement">The window element.</param>
        /// <returns></returns>
        internal static void InitializeWindow(Control windowControl, IVisualElement windowElement)
        {
            if (_loader != null && _loader._visualElementProvider != null)
            {
                _loader._visualElementProvider.InitializeWindow(windowControl, windowElement);
            }
        }

        /// <summary>
        /// Creates the action.
        /// </summary>
        /// <typeparam name="TActionArgs">The type of the action arguments.</typeparam>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <returns></returns>
        internal static IVisualElementAction<TActionArgs> CreateAction<TActionArgs>(IVisualElement visualElement, string controllerName, string actionName)
        {
            if (_loader != null && _loader._visualElementProvider != null)
            {
                return _loader._visualElementProvider.CreateAction<TActionArgs>(visualElement, controllerName, actionName);
            }
            return null;
        }
    }
}
