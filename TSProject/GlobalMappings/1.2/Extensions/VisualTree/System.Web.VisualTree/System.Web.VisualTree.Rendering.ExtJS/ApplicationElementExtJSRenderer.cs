﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Common.Interfaces;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ApplicationElement), "VisualElementExtJSRenderer", "", "")]
    [ExtJSEventDescription("MousePositionChanged", ServerEventArgsCreateCode = "new MouseEventArgs(){ X = this.x.ToInt32(), Y = this.y.ToInt32()}")]
    [ExtJSEventDescription("ActiveControlChanged", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.activeControlId)")]
    [ExtJSMethodDescription("Beep", MethodCode = "VT_beep()")]
    [ExtJSMethodDescription("ExecuteOnRespone")]
    public class ApplicationElementExtJSRenderer : ApplicationElementExtJSRendererBase
    {
        /// <summary>
        /// Provides support for rendering update code.
        /// </summary>
        private class UpdateResult : VisualElementResource
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="UpdateResult" /> class.
            /// </summary>
            /// <param name="objApplicationElement">The visual element.</param>
            public UpdateResult(ApplicationElement objApplicationElement)
                : base(objApplicationElement)
            {

            }

            /// <summary>
            /// Executes the JSON result.
            /// </summary>
            /// <param name="renderingContext">The rendering context.</param>
            /// <param name="jsonWriter">The JSON writer.</param>
            /// <exception cref="System.ArgumentNullException">jsonWriter</exception>
            protected override void RenderJsonResource(RenderingContext renderingContext, JsonTextWriter jsonWriter)
            {
                if (jsonWriter == null)
                {
                    throw new ArgumentNullException("jsonWriter");
                }

                // Write the start array
                jsonWriter.WriteStartArray();

                // Get current element
                ApplicationElement applicationElement = Element as ApplicationElement;

                if (applicationElement != null)
                {
                    // If there is no valid rendering context
                    if (renderingContext == null)
                    {
                        // Create rendering context based on current rendering environment
                        renderingContext = ApplicationElement.CurrentRenderingContext;
                    }

                    renderingContext.SettingAlternativeMainWindow = applicationElement.SettingAlternativeMainWindow;

                    // If the element is valid and we should render blank page
                    if (applicationElement.ShouldRenderBlankPage)
                    {
                        // Render blank page (as in the window was closed and there is not a main window anymore)
                        RenderBlankPage(renderingContext, jsonWriter);
                    }
                    else
                    {
                        // Get the visual element renderer
                        VisualElementExtJSRenderer objVisualElementRenderer = VisualTreeManager.GetRenderer(renderingContext, applicationElement) as VisualElementExtJSRenderer;

                        // If there is a valid visual element renderer
                        if (objVisualElementRenderer != null)
                        {
                            // Render the update 
                            objVisualElementRenderer.RenderUpdate(renderingContext, applicationElement, jsonWriter);
                        }
                    }
                }

                // Write the end array
                jsonWriter.WriteEndArray();
            }



            /// <summary>
            /// Renders the blank page.
            /// </summary>
            /// <param name="renderingContext">The rendering context.</param>
            /// <param name="jsonWriter">The json writer.</param>
            private void RenderBlankPage(RenderingContext renderingContext, JsonTextWriter jsonWriter)
            {
                jsonWriter.WriteRaw("window.location.href=\"about:blank\"");
            }



            /// <summary>
            /// Writes to the specific writer.
            /// </summary>
            /// <param name="renderingContext">The rendering context.</param>
            /// <param name="jsonWriter">The json writer.</param>
            public void WriteTo(RenderingContext renderingContext, JsonTextWriter jsonWriter)
            {
                RenderJsonResource(renderingContext, jsonWriter);
            }

            /// <summary>
            /// Writes to the specific writer.
            /// </summary>
            /// <param name="renderingContext">The rendering context.</param>
            /// <param name="writer">The writer.</param>
            public void WriteTo(RenderingContext renderingContext, StringWriter writer)
            {
                RenderJsonResource(renderingContext, writer);
            }

            /// <summary>
            /// Writes to.
            /// </summary>
            /// <param name="renderingContext">The rendering context.</param>
            /// <param name="httpResponse">The HTTP response.</param>
            internal void WriteTo(RenderingContext renderingContext, HttpResponse httpResponse)
            {
                StreamWriter streamWriter = new StreamWriter(httpResponse.OutputStream);
                RenderJsonResource(renderingContext, streamWriter);
                streamWriter.Flush();
            }
        }



        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="renderTo">The render to.</param>
        /// <returns></returns>
        public override string Render(RenderingContext renderingContext, IVisualElement visualElement, string renderTo = null)
        {
            // Get the window element
            IVisualElementRenderer objVisualElementExtJSRenderer= null;

            // Get visual element ext js renderer
            if (visualElement is WindowElement)
            {
                objVisualElementExtJSRenderer = VisualTreeManager.GetRenderer(renderingContext, visualElement) as WindowElementBaseExtJSRenderer;
            }
            else if (visualElement is CompositeElement)
            {
                objVisualElementExtJSRenderer = VisualTreeManager.GetRenderer(renderingContext, visualElement) as CompositeElementExtJSRenderer;
            }

            if (objVisualElementExtJSRenderer != null) 
            {
                IVisualElementRendererApp objVisualElementRendererApp = objVisualElementExtJSRenderer as IVisualElementRendererApp;

                if (objVisualElementRendererApp != null)
                {
                    // Create writer
                    using (StringWriter objWriter = new StringWriter(CultureInfo.InvariantCulture))
                    {
                        // Render the app
                        objVisualElementRendererApp.RenderApp(renderingContext, visualElement, objWriter, renderTo);

                        // Return writer content
                        return objWriter.ToString();
                    }
                }
            }
           
            return null;
        }

        /// <summary>
        /// Extracts the MousePositionChanged event event arguments.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractMousePositionChangedCallbackEventArgs(RenderingContext renderingContext, VisualElement visualElement, string serverEventName, JObject args)
        {
            return new System.Web.VisualTree.Elements.MouseEventArgs() { X = args.GetPropertyAsInt("x", 0), Y = args.GetPropertyAsInt("y", 0) };
        }

        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="applicationElement">The application element.</param>
        /// <returns></returns>
        private static IVisualElementRenderer RenderResult(IVisualElement applicationElement)
        {
            return new UpdateResult(applicationElement as ApplicationElement);
        }

        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="applicationElement">The application element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        internal static void RenderResult(RenderingContext renderingContext, ApplicationElement applicationElement, JsonTextWriter jsonWriter)
        {
            UpdateResult updateResult = new UpdateResult(applicationElement);
            updateResult.WriteTo(renderingContext, jsonWriter);
        }

        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="request">The http request.</param>
        /// <returns></returns>
        public override IVisualElementRenderer RenderResult(RenderingContext renderingContext, IVisualElement visualElement, HttpRequestBase request = null)
        {
            // Get application element
            ApplicationElement applicationElement = visualElement as ApplicationElement;

            // Process events 
            ProcessEvents(renderingContext, applicationElement, request);

            // Return the operation results
            return new UpdateResult(applicationElement);

        }
        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="applicationElement">The application element.</param>
        /// <param name="context">The context.</param>
        internal override void RenderResult(RenderingContext renderingContext, ApplicationElement applicationElement, HttpContext context)
        {
            // Process events 
            ProcessEvents(renderingContext, applicationElement, context.Request);

            // Return the update results
            UpdateResult updateResult = new UpdateResult(applicationElement);

            // Render result
            updateResult.WriteTo(renderingContext, context.Response);
        }

        /// <summary>
        /// Processes the events.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="applicationElement">The application element.</param>
        /// <param name="request">The request.</param>
        private static void ProcessEvents(RenderingContext renderingContext, ApplicationElement applicationElement, HttpRequest request)
        {
            // If there is a valid request
            if (request != null && applicationElement != null)
            {
                // Get the parsed events
                JArray events = GetEventsFromRequest(request);

                // Process the events
                ProcessEvents(renderingContext, applicationElement, events);
            }
        }



        /// <summary>
        /// Processes the events.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="applicationElement">The application element.</param>
        /// <param name="request">The request.</param>
        private static void ProcessEvents(RenderingContext renderingContext, ApplicationElement applicationElement, HttpRequestBase request)
        {
            // If there is a valid request
            if (request != null && applicationElement != null)
            {
                // Get the parsed events
                JArray events = GetEventsFromRequest(request);

                // Process the events
                ProcessEvents(renderingContext, applicationElement, events);
            }
        }

        /// <summary>
        /// Processes the events.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="applicationElement">The application element.</param>
        /// <param name="events">The events.</param>
        private static void ProcessEvents(RenderingContext renderingContext, ApplicationElement applicationElement, JArray events)
        {
            // If there is a valid application element
            if (applicationElement != null)
            {

                // If there are valid events
                if (events != null)
                {
                    // Loop all events
                    foreach (JToken eventToken in events)
                    {
                        // Get event object
                        JObject eventObject = eventToken as JObject;

                        // If there is a valid event object
                        if (eventObject != null)
                        {
                            // The element id token
                            JToken idToken = null;

                            // The element event name token
                            JToken eventNameToken = null;

                            // If we found id token
                            if (eventObject.TryGetValue("id", out idToken) && eventObject.TryGetValue("name", out eventNameToken))
                            {
                                // Get the event name
                                string eventName = eventNameToken.ToString();

                                // If is a history request event name
                                if (string.Equals(eventName, "HistoryRequest"))
                                {
                                    // Get token
                                    string historyToken = idToken.ToString();

                                    // If there is a valid token
                                    if(historyToken != null)
                                    {
                                        // Navigate history
                                        ((IClientHistoryManager)applicationElement).Navigate(historyToken);
                                    }
                                }

                                // Get the element id
                                IClientElement eventElement = applicationElement.GetClientElementByPath(idToken.ToString());

                                // If there is a valid event element
                                if (eventElement != null)
                                {                                    
                                    // If is a binding request event name
                                    if (string.Equals(eventName, "BindingRequest"))
                                    {
                                        // Get binding manager
                                        BindingManager bindingManager = BindingManager.GetBindingManager(eventElement as VisualElement);

                                        // If there is a valid binding manager
                                        if (bindingManager != null)
                                        {
                                            // Get ext JS renderer
                                            BindingManagerExtJSRenderer bindingManagerRenderer = VisualTreeManager.GetRenderer(renderingContext, bindingManager as VisualElement) as BindingManagerExtJSRenderer;

                                            // If there is a valid renderer
                                            if (bindingManagerRenderer != null)
                                            {
                                                // Handle data source request
                                                bindingManagerRenderer.HandleDataSourceRequest(renderingContext, bindingManager, eventObject);
                                            }
                                        }
                                    }
                                    else
                                    {

                                        // Get the event name
                                        MethodInfo eventActionInvoker = VisualElement.GetPerformEventMethod(eventElement, eventName);

                                        // If there is a valid event action invoker
                                        if (eventActionInvoker != null)
                                        {
                                            // Get the action renderer
                                            VisualElementRenderer actionRenderer = null;

                                            // The method invoker
                                            IVisualElementMethodInvoker methodInvoker = null;

                                            // The visual element
                                            VisualElement visualElement = null;

                                            // Try to get visual element
                                            visualElement = eventElement as VisualElement;

                                            // If there is a valid visual element
                                            if (visualElement != null)
                                            {
                                                // Get renderer from visual element
                                                actionRenderer = VisualTreeManager.GetRenderer(renderingContext, visualElement) as VisualElementRenderer;
                                            }
                                            else
                                            {
                                                // Get method invoker
                                                methodInvoker = eventElement as IVisualElementMethodInvoker;

                                                // If there is a valid method invoker
                                                if (methodInvoker != null)
                                                {
                                                    // Get renderer from method invoker
                                                    actionRenderer = VisualTreeManager.GetRenderer(renderingContext, methodInvoker) as VisualElementRenderer;
                                                }
                                            }

                                            // If there is a valid action renderer
                                            if (actionRenderer != null)
                                            {
                                                // If there is a valid visual element
                                                if (visualElement != null)
                                                {
                                                    // Invoke visual element
                                                    eventActionInvoker.Invoke(eventElement, new object[] { actionRenderer.GetEventArgs(renderingContext, visualElement, eventName, eventObject) });
                                                }
                                                else
                                                {
                                                    // If there is a valid method invoker
                                                    if (methodInvoker != null)
                                                    {
                                                        // Invoke callback method
                                                        eventActionInvoker.Invoke(eventElement, new object[] { actionRenderer.GetMethodCallbackData(renderingContext, methodInvoker.Name, eventObject) });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Invoke timers
                int nextCallback = ((ITimerHandler)applicationElement).InvokeTimers(DateTime.Now.Ticks);

                // If timer needs to be invoked
                if(nextCallback > 0)
                {
                    // Raise next delayed event
                    BrowserUtils.RaiseDelayedEvents(nextCallback);
                }
            }
        }


        /// <summary>
        /// Gets the events from request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private static JArray GetEventsFromRequest(HttpRequest request)
        {
            // If there is a valid request
            if (request != null)
            {
                // Get the input stream
                Stream stream = request.InputStream;

                // Return to beginning
                stream.Seek(0, SeekOrigin.Begin);

                // Create stream reader based on the input stream
                StreamReader reader = new StreamReader(stream);

                // Return the parsed JArray
                return JArray.Parse(reader.ReadToEnd());
            }

            return null;
        }


        /// <summary>
        /// Gets the events from request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private static JArray GetEventsFromRequest(HttpRequestBase request)
        {
            // If there is a valid request
            if (request != null)
            {
                // Get the input stream
                Stream stream = request.InputStream;

                // Return to beginning
                stream.Seek(0, SeekOrigin.Begin);

                // Create stream reader based on the input stream
                StreamReader reader = new StreamReader(stream);

                // Return the parsed JArray
                return JArray.Parse(reader.ReadToEnd());
            }

            return null;
        }



        /// <summary>
        /// Renders the update.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        internal override void RenderUpdate(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderUpdate(renderingContext, visualElement, jsonWriter);

            // Get the method invoker container
            IVisualElementMethodInvokerContainer methodInvokerContainer = visualElement as IVisualElementMethodInvokerContainer;

            // If there is a valid method invoker container
            if (methodInvokerContainer != null)
            {
                // The current method invoker
                IVisualElementMethodInvoker methodInvoker = null;

                // De-queue all method invokers
                while ((methodInvoker = methodInvokerContainer.Dequeue()) != null)
                {
                    // Get the visual element renderer
                    VisualElementExtJSRenderer objVisualElementRenderer = VisualTreeManager.GetRenderer(renderingContext, methodInvoker) as VisualElementExtJSRenderer;

                    // If there is a valid visual element renderer
                    if (objVisualElementRenderer != null)
                    {
                        // Render the operation
                        objVisualElementRenderer.RenderMethod(renderingContext, methodInvoker, jsonWriter);
                    }
                }

                // End the current response
                methodInvokerContainer.Clear();
            }
        }

        /// <summary>
        /// Gets the ext js type.
        /// </summary>
        /// <value>
        /// The ext js type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.application.Application";
            }
        }

        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "application";
            }
        }

        protected override void RenderExecuteOnResponeMethod(RenderingContext context, IVisualElementMethodInvoker methodInvoker, object methodData,
            Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            var builder = getJavaScriptBuilder(true);
            if (builder != null)
            {
                builder.AppendFormattedRawValue(
                    @"setTimeout(function () {{VT_raiseEvent({0}, 'Callback', null);}}, {1});",
                    System.Web.VisualTree.Elements.VisualElement.GetElementId(methodInvoker), methodData);
            }
        }
    }
}
