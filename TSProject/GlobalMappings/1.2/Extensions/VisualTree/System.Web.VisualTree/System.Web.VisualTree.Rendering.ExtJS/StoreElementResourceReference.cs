﻿using Newtonsoft.Json;
using System.Linq;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    /// <summary>
    /// Provides support for referencing a store element resource
    /// </summary>
    public class StoreElementResourceReference : JsonValueWriter
    {
        /// <summary>
        /// The proxy URI
        /// </summary>
        private readonly string _proxyUri;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreElementResourceReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        public StoreElementResourceReference(VisualElement visualElement, string resourceId, string storeXType )
        {
            // Set the proxy Uri
            _proxyUri = VisualElementRenderer.GetElementResourceUri(visualElement, resourceId);
            _storeXType = storeXType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreElementResourceReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        public StoreElementResourceReference(VisualElement visualElement, string resourceId)
            :this(visualElement,resourceId,"store")
        {

        }

        /// <summary>
        /// The store type
        /// </summary>
        private string _storeXType = "store";

        /// <summary>
        /// Gets or sets the type of the store.
        /// </summary>
        /// <value>
        /// The type of the store.
        /// </value>
        public string StoreXType
        {
            get { return _storeXType; }
            set { _storeXType = value; }
        }

        /// <summary>
        /// The model type
        /// </summary>
        private string _modelType;

        /// <summary>
        /// Gets or sets the type of the model.
        /// </summary>
        /// <value>
        /// The type of the model.
        /// </value>
        public string ModelType
        {
            get { return _modelType; }
            set { _modelType = value; }
        }

        /// <summary>
        /// The fields
        /// </summary>
        private string[] _fields;
        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        public string[] Fields
        {
            get { return _fields; }
            set { _fields = value; }
        }


        /// <summary>
        /// Gets the proxy URI.
        /// </summary>
        /// <value>
        /// The proxy URI.
        /// </value>
        public string ProxyUri
        {
            get { return _proxyUri; }
        }

        /// <summary>
        /// The auto load
        /// </summary>
        private bool? _autoLoad;

        /// <summary>
        /// Gets or sets the automatic load.
        /// </summary>
        /// <value>
        /// The automatic load.
        /// </value>
        public bool? AutoLoad
        {
            get { return _autoLoad; }
            set { _autoLoad = value; }
        }

        /// <summary>
        /// The proxy type
        /// </summary>
        private string _proxyType = "ajax";

        /// <summary>
        /// Gets or sets the type of the proxy.
        /// </summary>
        /// <value>
        /// The type of the proxy.
        /// </value>
        public string ProxyType
        {
            get { return _proxyType; }
            set { _proxyType = value; }
        }

        /// <summary>
        /// The proxy reader root
        /// </summary>
        private string _proxyReaderRoot;

        /// <summary>
        /// Gets or sets the proxy reader root.
        /// </summary>
        /// <value>
        /// The proxy reader root.
        /// </value>
        public string ProxyReaderRoot
        {
            get { return _proxyReaderRoot; }
            set { _proxyReaderRoot = value; }
        }
        

        /// <summary>
        /// The proxy reader type
        /// </summary>
        private string _proxyReaderType = "json";


        /// <summary>
        /// Gets or sets the type of the proxy reader.
        /// </summary>
        /// <value>
        /// The type of the proxy reader.
        /// </value>
        public string ProxyReaderType
        {
            get { return _proxyReaderType; }
            set { _proxyReaderType = value; }
        }




        /// <summary>
        /// Writes the value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        public override void WriteValue(JsonTextWriter jsonWriter)
        {
            if (jsonWriter == null)
            {
                return;
            }
            
            jsonWriter.WriteStartObject();
            jsonWriter.WriteProperty("xtype", StoreXType);
            if (!string.IsNullOrEmpty(_modelType))
            {
                jsonWriter.WriteProperty("model", _modelType);
            }
            if (_fields != null && _fields.Any())
            {
                jsonWriter.WritePropertyName("fields");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    foreach (string field in _fields)
                    {
                        jsonWriter.WriteValue(field);
                    }
                }
            }
            jsonWriter.WriteStartObjectProperty("proxy");
            jsonWriter.WriteProperty("type", ProxyType);
            jsonWriter.WriteProperty("url", ProxyUri);
            jsonWriter.WriteStartObjectProperty("reader");
            jsonWriter.WriteProperty("type", ProxyReaderType);

            // Render proxy reader root if valid
            if(!string.IsNullOrEmpty(ProxyReaderRoot))
            {
                jsonWriter.WriteProperty("root", ProxyReaderRoot);
            }
            
            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndObject();

            // If there is a valid auto load
            if(AutoLoad != null)
            {
                // Write auto load
                jsonWriter.WriteProperty("autoLoad", AutoLoad.Value);
            }

            jsonWriter.WriteEndObject();
        }
    }
}
