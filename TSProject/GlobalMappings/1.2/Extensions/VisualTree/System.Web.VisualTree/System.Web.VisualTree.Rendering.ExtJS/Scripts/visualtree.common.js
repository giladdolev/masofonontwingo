//Ext.Loader.setPath('Ext.ux', 'Scripts/extjs/examples/ux');
//Ext.require([
//   'Ext.ux.form.MultiSelect'
//]);

VT_GridPanelID = "";

/// <summary>
/// store active control id .
/// </summary>
var VT_ActiveControlId = "";

/// <summary>
/// last raised active control changed id
/// </summary>
var VT_LastActiveControlId = "";

/// <summary>
/// Future support for multiple apps in a single session
/// </summary>
var VT_APPID = 1;

/// <summary>
/// A flag indicating that during updating of the UI we don't want to process events.
/// </summary>
var VT_SuspendEvents = false;

/// <summary>
/// The client event queue used to accumulate client events.
/// </summary>
var VT_EventQueue = [];

/// <summary>
/// The client event hash used to prevent enqueuing duplicate events.
/// </summary>
var VT_EventQueueHash = {};

/// <summary>
/// The mask used during loading.
/// </summary>
var VT_LoadingMask = null;

/// <summary>
/// The used to delay loading mask.
/// </summary>
var VT_LoadingMaskHandle = 0;

/// <summary>
/// The used to delay raising events.
/// </summary>
var VT_raiseDelayedEventsHandle = 0;

/// <summary>
/// The application is running in design mode.
/// </summary>
var VT_DesignMode = false;

/// <summary>
/// The application is running in mobile mode.
/// </summary>
var VT_MobileMode = false;

/// <summary>
/// The process element id to enable sending events to the process.
/// </summary>
var VT_ProcessElementID = "__process";

/// <summary>
/// The callback hash.
/// </summary>
var VT_callbacks = {};

/// <summary>
/// The timer pointers hash.
/// </summary>
var VT_timers = {};

/// <summary>
/// Indicates we should ignore the history changes
/// </summary>
var VT_suspendHistory = false;

var VT_lastRaiseDelay = Number.MAX_VALUE;

Ext.override(Ext.window.Window,
    {
        initComponent: function () {
            this.callParent(arguments);

            var me = this;

            this.on("beforeclose", me.beforeCloseHandler, me);

        },

        beforeCloseHandler: function (view, e) {
            try {
                return false;
            } catch (err) {
                console.log("beforeCloseHandler : " + err.message);
            }

        },

    });


function VT_beep() {
    var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
    snd.play();
}

function VT_raiseEvent(id, name, params, loadingId) {

    /// <signature>
    ///   <summary>Generates the event request.</summary>
    ///   <param name="id">The element id.</param>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    ///   <param name="loadingId">The loading target.</params>
    /// </signature>

    // If we are in suspend mode
    if (VT_SuspendEvents || VT_DesignMode) {
        VT_queueEventObject(id, name, params, loadingId);
        return;
    }

    // Raise active control 
    VT_raiseActiveControlEvent();

    // Raise mouse position
    VT_raiseMousePositionEvent();

    try {
        if (is_touch_device()) {
            if (name == 'Click') {
                var cmp = VT_GetCmp(id);
                if (cmp != null && cmp.xtype == 'button') {
                    cmp.focus();
                    asyncCallSleep();
                    // Do something after the sleep!
                    if (VT_GridPanelID != '') {
                        var grid = VT_GetCmp(VT_GridPanelID);
                        if (grid != null) {
                            if (grid.hasDirtyCell) {
                                grid.completeEdit();
                                grid.hasDirtyCell = false;
                            }
                        }
                    }
                }
            }
        }


    } catch (err) {
    }

    // Enqueue the event object
    VT_queueEventObject(id, name, params, false);
    // Raise events
    VT_raiseEvents(loadingId ? loadingId : id);
};

function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('');
        }, ms);
    });
};

async function asyncCallSleep() {
    var result = await sleep(80);//was 100 gilad
};

function is_touch_device() {
    return 'ontouchstart' in window        // works on most browsers 
        || navigator.maxTouchPoints;       // works on IE10/11 and Surface
};

function VT_raiseDelayedEvents(delay) {
    /// <signature>
    ///   <summary>Raises delayed events request.</summary>
    ///   <param name="delay">The delay time.</param>
    /// </signature>

    if (VT_lastRaiseDelay < delay) {
        return;
    }

    VT_lastRaiseDelay = delay;

    // Cancel the execution of the timed event
    clearTimeout(VT_raiseDelayedEventsHandle);



    // Set raise event timeout
    VT_raiseDelayedEventsHandle = setTimeout(function () {
        VT_raiseEvents();
    }, delay)
}
//gilad new
function VT_raiseEvents(loadingId) {
  
    /// <signature>
    ///   <summary>Raises events request.</summary>
    ///   <param name="loadingId">The loading element id.</param>
    /// </signature>
    //test gilad
    let showMask=true;
    if(loadingId!="w_mini_terminal/pnlContent/uo_shipments_list/sle_scan_line" )
    { 
        // Cancel the execution of the timed event
        clearTimeout(VT_raiseDelayedEventsHandle);
        loaderId='';
    }
    else if(loaderId!='w_mini_terminal/pnlContent/uo_shipments_list/sle_scan_line')
    {   //do not operate the hide/show loader 
       clearTimeout(VT_raiseDelayedEventsHandle);
       loaderId='w_mini_terminal/pnlContent/uo_shipments_list/sle_scan_line'
    }
    else
        showMask=false;

       
    // Clear timeout handle 
    VT_raiseDelayedEventsHandle = 0;
    VT_lastRaiseDelay = Number.MAX_VALUE;

    // Get the current queue
    var currentQueue = VT_EventQueue;

    // Clear the global event queue
    VT_EventQueue = [];
    VT_EventQueueHash = {};

    //test gilad
    if (showMask && loadingId != null && !VT_isTimerId(loadingId)) {
            VT_ShowLoadingMask(loadingId);
        }


    // Call action
    VT_callAction(currentQueue);
}
//old gilad
//function VT_raiseEvents(loadingId) {
//    /// <signature>
//    ///   <summary>Raises events request.</summary>
//    ///   <param name="loadingId">The loading element id.</param>
//    /// </signature>

//    // Cancel the execution of the timed event
//    clearTimeout(VT_raiseDelayedEventsHandle);

//    // Clear timeout handle
//    VT_raiseDelayedEventsHandle = 0;
//    VT_lastRaiseDelay = Number.MAX_VALUE;

//    // Get the current queue
//    var currentQueue = VT_EventQueue;

//    // Clear the global event queue
//    VT_EventQueue = [];
//    VT_EventQueueHash = {};



//    // If there is a valid loading id
//    if (loadingId != null && !VT_isTimerId(loadingId)) {
//        // Show the loading mask for the given component id
//        VT_ShowLoadingMask(loadingId);
//    }

//    // Call action
//    VT_callAction(currentQueue);
//}

function VT_isTimerId(cmpId) {
    return Ext.Array.some(Ext.Object.getKeys(VT_timers), function (item) {
        return cmpId.indexOf(item) >= 0;
    });
}

function VT_raiseMousePositionEvent() {
    /// <signature>
    ///   <summary>Raise the mouse position event.</summary>
    /// </signature>

    // Get the event object
    var eventObject = Ext.EventObject;

    // If there is a valid event object
    if (eventObject) {

        // If we have a get XY method
        if (eventObject.getXY) {

            // Get XY mouse position
            var eventXY = eventObject.getXY();

            // If there is a valid position
            if (eventXY) {

                // Enqueue the mouse position event
                VT_queueEventObject(VT_ProcessElementID, "MousePosition", { x: eventXY[0], y: eventXY[1] }, true);
            }
        }
    }
}

function VT_raiseDelayedEvent(id, name, interval, params) {
    /// <signature>
    ///   <summary>Generates the timedout event request.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>
    VT_queueEvent(id, name, params);
    VT_raiseDelayedEvents(interval);
}


function VT_raiseActiveControlEvent() {
    /// <signature>
    ///   <summary>Raise the active control event.</summary>
    /// </signature>

    // If we have a valid VT_ActiveControlId  .
    if (VT_ActiveControlId != "" && VT_ActiveControlId != VT_LastActiveControlId) {
        VT_LastActiveControlId = VT_ActiveControlId;

        // Enqueue the active control event
        VT_queueEventObject(VT_ProcessElementID, "ActiveControlChanged", { activeControlId: VT_ActiveControlId }, true);
    }

}

function VT_raiseDoubleFilteredEvent(id, name, params) {
    /// <signature>
    ///   <summary>Generates the timedout event request but filters out duplicate calls.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>

    // Set the owner for closure support
    var owner = this;

    // Get the current double filter handle
    var handle = owner._VT_DoubleFilteredHandle;

    // If there is allready a registered handler
    if (!isNaN(handle) && handle > 0) {

        // Cancel the execution of the timed event
        clearTimeout(handle);

        // Clear the filtered event handle
        owner._VT_DoubleFilteredHandle = 0;

    } else {

        // Set delayed event raising
        owner._VT_DoubleFilteredHandle = setTimeout(function () {

            // Raise the event
            VT_raiseEvent(id, name, params);

            // Clear the timeout handle
            owner._VT_DoubleFilteredHandle = 0;

        }, 500);
    }
}

function VT_queueEvent(id, name, params) {
    /// <signature>
    ///   <summary>Generates an event in the event queue but it is not sent to the server.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>

    // If we are in design mode
    if (VT_DesignMode) {
        return;
    }

    // Enqueue the event object
    VT_queueEventObject(id, name, params, true);
}

function VT_queueEventObject(id, name, params, isUnique) {
    /// <signature>
    ///   <summary>Generates an event in the event queue but it is not sent to the server.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>

    // Create the event object
    var eventObject = null;

    // Create the event key
    var eventKey = null;

    // If is a unique event
    if (isUnique) {

        // Create the event key
        eventKey = id + "::" + name;

        // Try to get existing event
        eventObject = VT_EventQueueHash[eventKey];
    }

    // If there is no valid event object
    if (!eventObject) {

        // Create the event object
        eventObject = { id: id, name: name };

        // Push new event to queue
        VT_EventQueue.push(eventObject);

        // If is a unique event
        if (isUnique) {

            // Try to get existing event
            VT_EventQueueHash[eventKey] = eventObject;
        }
    }

    // If there is a valid params action
    if (params) {

        // Loop all paramss
        for (var param in params) {

            // Set action object param
            eventObject[param] = params[param];
        }
    }


}

function VT_invokeCallbackById(id) {
    /// <signature>
    ///   <summary>Invokes the callback.</summary>
    ///   <param name="id">The callback id.</param>
    ///   <param name="arguments">The callback arguments.</param>
    /// </signature>

    // Get callback
    var callbackFn = VT_callbacks[id];

    // If there is a valid callback
    if (Ext.isFunction(callbackFn)) {

        // Invoke callback
        callbackFn.apply(this, [].slice.call(arguments, 1));

        // Remove callback
        delete VT_callbacks[id];
    }
}

function VT_getCallbackId(callbackFn, id) {
    /// <signature>
    ///   <summary>Stores the callback and returns an id.</summary>
    ///   <param name="callbackFn">The callback function.</param>
    /// </signature>

    // If there is a valid callback
    if (callbackFn != null) {
        // Set default id
        if (id == null) id = "_callback";

        // The index
        var index = 0;

        // The unique id
        var uniqueId = id;

        // While not unique
        while (VT_callbacks[uniqueId] != null) {
            // Next index
            index++;

            // Create unique id
            uniqueId = id + index;
        }

        // Set the callback function
        VT_callbacks[uniqueId] = callbackFn;

        // Return unique id
        return uniqueId;
    }
}

function VT_onAfterInitialize() {
    /// <signature>
    ///   <summary>Provides support for processing actions after initialization of the app environment.</summary>
    /// </signature>

    Ext.History.on('change', function (token) {
        if (!VT_suspendHistory) {
            VT_navigateToHistory(token);
        } else {
            VT_suspendHistory = false;
        }
    });
}

function VT_addToHistory(token) {
    /// <signature>
    ///   <summary>Adding a history token.</summary>
    /// </signature>
    VT_suspendHistory = true;

    Ext.History.add(token);
}

function VT_navigateToHistory(token) {
    /// <signature>
    ///   <summary>Navigate back to a history token.</summary>
    /// </signature>

    // Raise server event
    VT_raiseEvent(token, "HistoryRequest");
}

function VT_getItemIdPath(record) {
    /// <signature>
    ///   <summary>Gets item id path.</summary>
    /// </signature>
    if (record) {

        // Get parent record
        var parentRecord = record.parentNode;

        // If is not the root node
        if (parentRecord && parentRecord.parentNode) {

            // Get parent path
            var parentPath = VT_getItemIdPath(parentRecord);

            // If there is a valid parent path
            if (parentPath) {

                // Get full path
                return parentPath + "/" + record.get("itemId");
            }
        }
        else {
            return record.get("itemId");
        }
    }
}





function VT_onInitialize() {

    // Initialize the history
    Ext.History.init();

    // Handle the document key down
    Ext.getBody().addListener('keydown', function (e, t) {

        var VT_FocusedComponent = Ext.get(Ext.Element.getActiveElement());
        // If there is a valid focus component
        if (VT_FocusedComponent) {

            // Get the key code
            var keyCode = e.keyCode;

            // TODO: add special key mask

            // Raise the event
            VT_FocusedComponent.fireEvent("vt_keypress", keyCode);

            // Get the focused root
            var focusedRoot = VT_GetRootCmp(VT_FocusedComponent);

            // If there is a valid focused component
            if (focusedRoot) {


                // Raise the event
                focusedRoot.fireEvent("vt_keypress", keyCode);
            }
        }
    });

    // Set the ajax timeout
    Ext.Ajax.setTimeout(60000000);
}

function VT_ShowLoadingMask(id, msg) {
    /// <signature>
    ///   <summary>Hide loading mask.</summary>
    ///   <param name="id">The id of the component to use to show mask.</param>
    ///   <param name="id">The global id or the full path id.</param>
    /// </signature>
    //gilad

    // Hide existing loading mask if there is one
    VT_HideLoadingMask();

    // Schedule showing the mask
    VT_LoadingMaskHandle = setTimeout(function () {

        // Get root component from component id
        var rootHolderCmp = VT_GetCmp(id, true),
            cmp = VT_GetCmp(id);

        // If there is a valid component
        if (cmp && !cmp.waitMaskDisabled && rootHolderCmp) {
            // If we need to create mask
            msg = msg || "Loading...";

            // Set the loading mask
            VT_LoadingMask = rootHolderCmp.setLoading({ msg: msg, maskCls: 'vt-mask' });
        }
    }, 500);
}

function VT_HideLoadingMask() {

    /// <signature>
    ///   <summary>Hide loading mask.</summary>
    /// </signature>

    // Clear timeout
    clearTimeout(VT_LoadingMaskHandle);

    // Clear handle
    VT_LoadingMaskHandle = 0;

    // If there is a valid mask hide it
    if (VT_LoadingMask) {
        VT_LoadingMask.hide();
        VT_LoadingMask = null;
    }
}

function VT_GetRootCmp(cmp) {
    /// <signature>
    ///   <summary>Gets the root component from from current.</summary>
    ///   <param name="cmp">The component to get it's root.</param>
    /// </signature>

    if (cmp) {
        var ownerCt = cmp.ownerCt;

        if (ownerCt) {
            return VT_GetRootCmp(ownerCt);
        } else {
            return cmp;
        }
    }
}

function VT_GetCmp(id, getRoot) {
    /// <signature>
    ///   <summary>Gets a component from a full path id.</summary>
    ///   <param name="id">The global id or the full path id.</param>
    ///   <param name="id">(optional) Indicates if to get the root component.</param>
    /// </signature>

    // If there is a valid id
    if (id) {
        // Get id parts from path
        var idParts = String(id).split("/");

        // If should get root only
        if (getRoot) {

            // If there are valid id parts
            if (idParts && idParts.length) {

                // Get first id part as an array
                idParts = [idParts[0]];
            }
        }

        // The current component
        var cmp = null;

        // Get components by query
        var cmps = Ext.ComponentQuery.query("#" + idParts.join(" #"));

        // If there are valid results
        if (cmps != null && cmps.length > 0) {

            // Get first result
            cmp = cmps[0];
        }

    }

    // If should get the root component
    if (!cmp && getRoot) {

        // Try to get the viewport
        cmps = Ext.ComponentQuery.query('viewport');

        // If there are valid results
        if (cmps != null && cmps.length > 0) {

            // Get first result
            cmp = cmps[0];
        }
    }


    if (!cmp && typeof idParts !== 'undefined' && idParts.length > 1) {
        //try to get tree view item

        var itemId = idParts.pop(),
            treeViewCmp, store;
        cmps = Ext.ComponentQuery.query("#" + idParts.join(" #"));
        treeViewCmp = cmps && cmps[0];

        if (treeViewCmp && treeViewCmp instanceof Ext.tree.Panel) {
            store = treeViewCmp && treeViewCmp.getStore && treeViewCmp.getStore();
            cmp = store.findNode('itemId', itemId);
        }
    }

    if (cmp == null) {
        //gilad
        //console.warn('Component with ID: %s not found', id);
    }

    // Return result
    return cmp;
}

function VT_callAction(events) {
    /// <signature>
    ///   <summary>Generates the action request.</summary>
    ///   <param name="action">The action url.</param>
    ///   <param name="events">The action events.</param>
    /// </signature>

    // If we need to create events
    if (!events) {
        //gilad
        return;
    }
    //console.log(timetest+"<<<<<<<<<<<<--REQUEST--<<<<<<<<<<<<<<<<<<<----------");
   // console.log(JSON.stringify(events));
   // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end request------------------");

    //gilad  remove  unnecessary calls .TODO manage it from VT

    let removeList="remove,w_mini_terminal/pnlContent/uo_pallet_manual/uo_mini_terminal_pallet_manual_dw_inv_pack_details,CellMouseDown,all,all,all;";
    removeList = removeList.concat("remove,w_mini_terminal/pnlContent/uo_pallet_manual/uo_mini_terminal_pallet_manual_dw_inv_pack_details,WidgetControlEditChanged,,all,barcode;");
    removeList = removeList.concat("remove,w_mini_terminal/pnlContent/uo_pallet_manual/uo_mini_terminal_pallet_manual_dw_inv_pack_details,WidgetControlFocusEnter,all,all,barcode;");
    let list = removeList.split(';')
    for (let i = 0; i < events.length; i++) 
    {
        for (let j = 1; j < list.length; j++)//first row not in use
        {
            let listArr = list[j].split(',')
            let action=listArr[0]?listArr[0].trim():null;
            let id=listArr[1]?listArr[1].trim():null;
            let name=listArr[2]?listArr[2].trim():null;
            let rowIndex=listArr[3]?listArr[3].trim():null;
            let newValue=listArr[4]?listArr[4].trim():null;
            let dataindex=listArr[5]?listArr[5].trim():null;

            if(events[i] &&events[i].id && events[i].id==id && (name =='all'||events[i].name==name) &&(rowIndex =='all'||events[i].rowIndex==rowIndex) &&(newValue =='all'||events[i].newValue==newValue)&&(dataindex =='all'||events[i].dataindex==dataindex))
            {
                if(action=='remove')
                {
                    events.splice(i,1);
                }
            }
        }
    }

    // Call the server
    Ext.Ajax.request({
        url: "/VisualTree/ProcessEvents",
        method: 'POST',
        params: {
            appId: VT_APPID
        },
        jsonData: events,
        success: function (response, opts) {

            //gilad for debugg
           // console.log(timetest+">>>>>>>>>>>>>>>>> RESPONSE >>>>>>>>>>>>>>>----------");
           // console.log(response.responseText);
           // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end response ------------------");
     

            // Hide existing loading mask if there is one
            VT_HideLoadingMask();

            //if the page has html content , so will update inner html
            if (typeof response.getAllResponseHeaders()["loginpage"] !== 'undefined') {
                window.location.href = window.location.origin;
            }
            else {
                VT_callback(response.responseText);
            }
        },
        failure: function (response, opts) {

            // Hide existing loading mask if there is one
            VT_HideLoadingMask();

            // If we could not connect the server
            if (response.status == 0) {

                // Could not connect to the server
                VT_Alert('server-side failure', "Could not connect to server. Retry?", Ext.Msg.YESNO, function (btn) {

                    // If user required a retry
                    if (btn == 'yes') {

                        // Show the loading mask
                        VT_ShowLoadingMask();

                        // Try to send events again
                        VT_callAction(events);

                    }
                });
            } else {
                VT_Alert('server-side failure', response.status + "-" + response.responseText);
            }
        }
    });
};

function VT_callback(responseText) {
    /// <signature>
    ///   <summary>Processes the visual tree callback.</summary>
    ///   <param name="responseText">The response text from the server.</param>
    /// </signature>

    try {


        // Enter suspend mode
        VT_SuspendEvents = true;

        // Evaluate the response
        VT_callbackApplyItems(eval(responseText));

        // Resume actions
        VT_SuspendEvents = false;

        // Raise Suspend Events . 
        VT_RaiseSuspendEvents();
    }
    catch (e) {

        // Resume actions
        VT_SuspendEvents = false;
        console.log('server-side callback error.', e.message);
        //VT_Alert('server-side callback error.', e.message);
    }
}

function VT_RaiseSuspendEvents() {

    if (VT_EventQueue.length > 0) {
        VT_raiseEvents(VT_EventQueue[0].id, VT_EventQueue[0].name);
    }
}

function VT_callbackApplyItems(response) {
    /// <signature>
    ///   <summary>Processes the visual tree callbacks.</summary>
    ///   <param name="response">The response items to apply.</param>
    /// </signature>
    // If there is a valid respose
    if (response && response.length) {
        // Loop all callbacks
        for (var index = -1, len = response.length; ++index ^ len;) {

            // Apply callback
            VT_callbackApply(response[index]);
        }
    }
}

function VT_callbackApply(responseItem) {
    /// <signature>
    ///   <summary>Processes the visual tree callback.</summary>
    ///   <param name="responseItem">The response item evaluated from the response.</param>
    /// </signature>

    // If response item is a function
    if (typeof responseItem === 'function') {
        try {
            responseItem();
        } catch (e) {
            console.log('Operation error.', e.message);
            //VT_Alert('Operation error.', e.message);
        }
    }
    else {
        VT_Alert('Unknown response item.', responseItem);
    }
}

function VT_Alert(title, message, buttons, callback) {
    /// <signature>
    ///   <summary>Shows an alert box.</summary>
    ///   <param name="title">The title.</param>
    ///   <param name="message">The message.</param>
    /// </signature>

    // If there is no buttons
    if (!buttons) {

        // Set default buttons
        buttons = Ext.MessageBox.OK;
    }

    Ext.Msg.show({
        title: title,
        msg: message,
        buttons: buttons,
        fn: callback
    });

    console.error(arguments);
}

function VT_ShowWindow(win) {
    /// <signature>
    ///   <summary>Opens a window.</summary>
    ///   <param name="win">The window to open.</param>
    /// </signature>
    if (win) {
        win.center();
        win.show();

        // If the application is running in mobile mode
        if (VT_MobileMode) {
            win.maximize();
        }
        else {
            // Ensure form is not hidden beyond top
            win.setY(Math.max(0, win.getY()));
        }
    }
}



function VT_GetCssClassForBackgroundImage(imageSrc, width, height) {
    width = typeof width !== 'undefined' ? width : 20;
    height = typeof height !== 'undefined' ? height : 20;
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.dynClass' + VT_CssUniqueId + ' { background-image: url(' + imageSrc + '); background-size: contain; width: ' + width + 'px; height: ' + height + 'px;}';
    document.getElementsByTagName('head')[0].appendChild(style);

    var cls = 'dynClass' + VT_CssUniqueId;
    VT_CssUniqueId++;
    return cls;
}

function VT_ShowToolTip(cmp)
{ Ext.create('Ext.tip.ToolTip', { target: cmp.getEl(), html: cmp.tip }); }





function VT_Column_Style_renderer(value, metaData, record, row, col, store, gridView) {
    try {
        var columnStyle = gridView.grid.getColumns()[col].style
        metaData.tdStyle = metaData.tdStyle || '';
        for (var name in columnStyle) {
            if (!columnStyle.hasOwnProperty(name)) {
                continue;
            }

            name = name.replace(/[A-Z]/g, function (match) { return '-' + match.toLowerCase() });
            metaData.tdStyle += '; ' + name + ': ' + columnStyle[name];
        }
    } catch (err) {
    }
}


function VT_RemoveListener(elem, eventName) {
    var hasListeners = elem && elem.hasListeners,
        eventListeners = hasListeners && hasListeners[eventName],
        vtFilter = function (item) {
            return item.isVtListener;
        },
        platformListener = eventListeners && Ext.Array.filter(eventListeners.listeners, vtFilter)[0];

    platformListener && elem.removeListener(eventName, platformListener);
}

function VT_CreateMarkedListener(eventHandler) {
    eventHandler.isVtListener = true;
    return eventHandler;
}

function getUxPath() {
    var tags = document.getElementsByTagName('script') || [],
        path = null,
        scriptTag, i, len;

    for (i = 0, len = tags.length; i < len; ++i) {
        scriptTag = tags[i];
        path = scriptTag && scriptTag.getAttribute('data-ext-ux-path');
        if (path) {
            return path;
        }
    }
}


function VT_GetFirstrowIndex(selection) {
    if (selection != null) {

        if (selection.isCells || selection.isRows) {
            return selection.getFirstRowIndex();
        }
    }
    return -1;
}


function VT_GetFirstcolumnIndex(selection) {
    if (selection != null) {
        if (selection.isCells) {
            return selection.getFirstColumnIndex();
        } else if (selection.isRows) {
            return 1;
        }

    }
    return -1;
}

function VT_GetLastrowIndex(selection) {
    if (selection != null) {
        if (selection.isCells || selection.isRows) {
            return selection.getLastRowIndex();
        }
    }
    return -1;
}

function VT_GetLastcolumnIndex(selection) {
    if (selection != null) {
        if (selection.isCells) {
            return selection.getLastColumnIndex();
        } else if (selection.isRows) {
            var resultIndex = Number.MIN_VALUE;
            selection.eachCell(function (cntx, colIndex, rowIndex) {
                if (colIndex > resultIndex) {
                    resultIndex = colIndex;
                }
            });
            return resultIndex;
        }
    }
    return -1;
}

function VT_RenderCellEditor(val, meta, rec, rowIndex, cellIndex) {
    var itemPath = meta.column.itemPath;
    VT_queueEventObject(itemPath, "BeforeCellRendering", { rowIndex: rowIndex, columnIndex: cellIndex, newValue: val }, false);
    VT_raiseDelayedEvents(100);

    return Ext.String.format('<div id="{0}-{1}"></div>', meta.column.itemId, meta.rowIndex);
}

function VT_GetSelectedRowIndex(cellIndex, rowIndex, RowsSelect, index) {

    if (cellIndex == 0) {
        if (RowsSelect != null) {
            if (RowsSelect.lastRange != null) {
                if (RowsSelect.lastRange[index].length == 2) {
                    return RowsSelect.lastRange[index][1];
                }
            }
            else {
                if (rowIndex != null) {
                    return rowIndex;
                }
            }
        }
    }
    return -1;
}

function VT_GetSelectedCellsIndeces(selection, view) {
    var selectedCells = [];
    var type = "";
    if (view) {
        if (view.selModel) {
            type = view.selModel.type;
        }
    }

    if (selection != null) {

        if (selection.allSelected == undefined)
            return null;

        var oneBased = selection.view && selection.view.ownerGrid && selection.view.ownerGrid.enableLocking;
        var fn = function (record, cellIndex, rowIndex) {
            selectedCells.push([rowIndex, cellIndex]);
        };

        if (oneBased || type === "spreadsheet") {
            fn = function (record, cellIndex, rowIndex) {
                selectedCells.push([rowIndex, cellIndex - 1]);
            };
        }

        if (typeof selection.eachCell !== 'undefined') {
            selection.eachCell(fn);
        }


    }
    return selectedCells;
}

function VT_GetSelectedRowIndeces(table, selection) {
    var selectedRows = [];
    if (typeof table.selModel !== 'undefined' && table.selModel.type != "spreadsheet") {
        return selectedRows.push(table.getStore().indexOf(selection));
    }
    if (table.type === 'rowmodel' && selection.length === 1) {
        return selectedRows.push(table.view.dataSource.indexOfId(selection[0].id));
    }
    if (selection != null) {

        if (selection.allSelected == undefined)
            return selection[0];

        try {
            selection.eachRow(function (rec) {
                var position = this.view.dataSource.indexOf(rec)
                if (position >= 0) {
                    selectedRows.push(position);
                }
            });
        } catch (ex) {

        }
    }
    return selectedRows;
}

function VT_CopyText(text) {

    var textArea = document.createElement("textarea");
    textArea.id = "txtForCopy";
    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();
    document.getElementById('txtForCopy').addEventListener('click', function () {
        console.log('error :- ', document.execCommand('copy', false));
    });

    try {
        var event = new Event('click');
        document.getElementById('txtForCopy').dispatchEvent(event);

    } catch (err) {
        console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
}

function VT_GridActualColumnIndex(column, index) {
    if (column) {
        var grid = column.findParentByType('gridpanel');
        if (grid) {
            var lockedTable = grid.selModel.views[0];
            if (lockedTable) {
                var offset = lockedTable.getColumnManager().getColumns().length,
                    currentTableId = column.findParentByType('tablepanel').view.id;
                if (currentTableId === lockedTable.id) {
                    return index;
                } else {
                    return index + offset;
                }
            } else {
                return VT_GetCellIndex(grid, index)
            }
        }
    }
    return index;
}

function VT_GetCellIndex(table, index) {
    var selmodel = table.selModel;
    if (selmodel) {
        if (selmodel.type == "spreadsheet") {
            return index - 1;
        } else {
            return index;
        }
    }
    return -1;
}

function VT_GetActualCellIndex(table, cellIndex) {
    var view = table;
    if (view.ownerGrid.enableLocking != null) {
        lockedTable = view.selModel.views[0];
        offset = lockedTable.getColumnManager().getColumns().length;
        if (table.id === lockedTable.id) {
            return cellIndex;
        } else {
            if (table.selModel.type != "spreadsheet") {
                return cellIndex;
            } else {
                return cellIndex + offset - 1;
            }
        }

    } else {

        return VT_GetCellIndex(table, cellIndex);
    }
    return cellIndex;

}

function VT_GetWidgetColumnIndex(columns, dataIndex) {
    var index;
    for (index = 0; index < columns.length; ++index) {
        if (columns[index].dataIndex == dataIndex) {
            return index - 1;
        }
    }
    return -1;
}


function VT_GetWidgetRowIndex(view) {

    return view.up('gridview').indexOf(view.el.up('table'));
}

function VT_GetListViewSelectedIndexes(view, selected) {
    var valuesArr = '';

    for (i = 0; i < selected.length; i++) {

        valuesArr += view.store.indexOf(selected[i]);
        if (i + 1 < selected.length) {
            valuesArr += ',';
        }
    }
    return (valuesArr == "") ? -1 : valuesArr;
}

function VT_GetComboboxSelectedIndex(combo, records) {
    try {
        if (combo) {
            if (combo.getStore()) {
                combo.getStore().clearFilter();
                return combo.store.indexOf(records);
            }
        }
    } catch (ex)
    { }
    return -1;
}


function VT_GetColumnFromItemClickEvent(e) {
    try {
        if (typeof (e) !== 'undefined') {
            if (typeof (e.position) !== 'undefined') {
                if (typeof (e.position.colIdx) !== 'undefined') {
                    return e.position.colIdx;
                }
            }
        }
    } catch (ex)
    { }
    return -1;
}

function VT_Test(view, e) {
    try {
        view.getView().getCell(view.store.getAt(0), view.getColumns()[1]).addCls("asd");
    } catch (err) {
        console.log("error :- widgetControlEditChanged : " + err.message);
    }
}

// Hides the default android keyboard
function VT_HideKeyboard(element) {
    try {
        var tmpField = document.createElement('input');
        tmpField.setAttribute('type', 'text');
        document.body.appendChild(tmpField);

        setTimeout(function () {
            tmpField.focus();
            setTimeout(function () {
                tmpField.setAttribute('style', 'display:none;');
            }, 50);
        }, 50);
    }
    catch (e) {
        console.log(e);
    }


}


function VT_GetTabTitle(cmp) {
    try {
        if (cmp != null) {
            return cmp.title;
        }
        return "";
    } catch (err) {
        console.log(err);
    }
}

function VT_GetMaskedTextboxValue(view, newValue) {
    var value = newValue;
    if (view != null) {
        if (value.length && !value.match(view.config.regex)) {
            view.setStyle('border', ' 1px solid red');
        }
        else {
            view.setStyle('border', ' 0');
        }
        return view.getValue();
    }
    return null;
}

function getWidgetColumnControls(view, rowIndex) {
    try {
        var controls = null;
        var grd = view.ownerGrid;
        if (grd != null && grd.xtype == "VTWidgetGrid") {
            if (rowIndex > -1) {
                grd.FocusingRow = true;
                grd.getView().focusRow(rowIndex);
                grd.FocusingRow = false;
                var gridRow = grd.getViewModelContrlos()[rowIndex];
                if (gridRow != null) {
                    var gridRowItems = gridRow.getView().items;
                    if (gridRowItems) {
                        controls = gridRowItems.items;
                    }
                }
            }
        }
        return controls;
    } catch (err) {
        console.log("error getWidgetColumnControls :- " + err.message)
    }
}

function VT_widgetgridfocuscontrol(view, rowIndex, columnIndedx, widgetDataMember) {
    try {
        var controls = getWidgetColumnControls(view, rowIndex);
        if (controls != null) {
            for (var i = 0; i < controls.length; i++) {
                if (controls[i] && controls[i].rendered != 'undefined') {
                    if (controls[i].dataIndex == widgetDataMember) {
                        view.FocusingRow = true; 
                        controls[i].focus();
                        view.FocusingRow = false;
                        break;
                    }
                }
            }
        }
    } catch (err) {
        console.log("error VT_widgetgridfocuscontrol :- " + err.message)
    }
}

function VT_widgetgridenablecontrol(view, rowIndex, columnIndedx, widgetDataMember, state) {
    try {
        var controls = getWidgetColumnControls(view, rowIndex);
        if (controls != null) {
            for (var i = 0; i < controls.length; i++) {
                if (controls[i] && controls[i].rendered != 'undefined') {
                    if (controls[i].dataIndex == widgetDataMember) {
                        if (controls[i].xtype == "sptextfield") {
                            controls[i].setReadOnly(!state)
                        } else if (controls[i].xtype == "gridMultiComboBox") {
                            controls[i].setDisabled(!state)
                        }
                        break;
                    }
                }
            }
        }
    } catch (err) {
        console.log("error VT_widgetgridenablecontrol :- " + err.message)
    }
}

function VT_widgetgridbackcolorcontrol(view, rowIndex, columnIndedx, widgetDataMember, color) {
    try {
        var controls = getWidgetColumnControls(view, rowIndex);
        if (controls != null) {
            for (var i = 0; i < controls.length; i++) {
                if (controls[i].dataIndex == widgetDataMember) {
                    if (controls[i] && controls[i].rendered != 'undefined') {
                        if (controls[i].dataIndex == widgetDataMember) {
                            if (controls[i].xtype == "sptextfield") {
                                controls[i].setFieldStyle('background:' + color)
                            }
                            break;
                        }
                    }
                }
            }
        }
    } catch (err) {
        console.log("error VT_widgetgridbackcolorcontrol :- " + err.message)
    }
}

function VT_WidgetColumnAttach(view, col, widget, rec) {
    var comboData = "";

    //console.log("cons");
    var viewModelRef = widget.getViewModel();
    view.ownerGrid.viewModelContrlos.push(viewModelRef);

    //TODO : hardcoded index
    displayRef = Ext.getCmp(widget.items.keys[11]);
    viewModelRef.set("record", rec);
    viewModelRef.set("row_no", rec.data['row_no']);
    viewModelRef.set("barcode", rec.data['barcode']);
    viewModelRef.set("material_name", rec.data['material_name']);
    viewModelRef.set("expected_material_quantity", rec.data['expected_material_quantity']);
    viewModelRef.set("invoice_pack_quantity", rec.data['invoice_pack_quantity']);
    viewModelRef.set("doc_no_en", rec.data['doc_no_en']);
    viewModelRef.set("doc_no", rec.data['doc_no']);
    comboData = rec.data['invoice_data'];
    displayRef.setStore(comboData);
}

function VT_GridAfterRender(grid, contextId) {

    try {

        /*
        * Show the filterplugin
        */
        if (grid) {
            var plugin = grid.getPlugin('filterplugin');
            if (plugin) {
                plugin.setVisible(true);
            }

            // Update the selected row on endupdate
            /*   grid.store.on('endupdate', function () {
                   var gridId = this.proxy.serverId;
                   var gridElement = VT_GetCmp(gridId);
                   var model = gridElement.getSelectionModel();
                   if (gridElement) {
   
                       // If there are records and there hasn't been any selection, then select the first record by default
                       if (gridElement.store.totalCount > 0 && gridElement.selection == null) {
                           if (gridElement.xtype != 'Grid-Combo') {
                               if (model.type == 'spreadsheet' && model.cellSelect == true) {
                                   // Select the first cell
                                   //model.selectCells([1, 0], [1, 0]);
                               } else {
                                   //gridElement.getSelectionModel().select(0);
                               }
                           }
                       }
                   }
               });*/

            // If there is a valid contextId
            if (contextId) {
                grid.el.on('contextmenu', function (e) {
                    e.stopEvent();

                    var menuCmp = VT_GetCmp(contextId);
                    if (menuCmp != null) {
                        menuCmp.on('render', function (menu) {
                            menu.getEl().on('contextmenu', function (e) {
                                e.stopEvent();
                            });
                        });
                        menuCmp.enable();
                        menuCmp.showBy(this, "tl?", [e.getX() - this.getX(), e.getY() - this.getY()]);
                    }
                });
            }
        }

    } catch (e) {
        console.log('VT_GridAfterRender() error : ' + e.message);
    }

}

function VT_GridUpdateCellValue(cmp, rowIdx, cellIdx, val) {
    try {
        if (cmp.xtype == 'Grid-Combo')
            return;

        if (rowIdx > -1 && cellIdx > -1) {

            // Get the record
            var record = cmp.getStore().getAt(rowIdx);

            // In case of spreadsheet avoid the rownumberer column
            if (cmp.selModel.type == 'spreadsheet') {
                cellIdx = cellIdx + 1;
            }

            // Get the column
            var col = cmp.getColumns()[cellIdx];
            if (record != null && col != null) {

                // Update record[dataIndex] value
                record.set(col.dataIndex, val);
            }
        }
    } catch (e) {
        console.log('an error has occurred in GridElementRendere.UpdateCellValue : ' + e.message);
    }
}

function VT_GetGridIdFromEditor(cmp) {
    try {
        // Grid editor got focus
        if (cmp.ownerCt && cmp.ownerCt.context) {
            return cmp.ownerCt.context.grid.itemId;
        } else {
            // Clicked on TableView
            if (cmp.grid) {
                return cmp.grid.itemId;
            }

            // CheckListBox
            if (cmp.ownerCt && cmp.ownerCt.itemId) {
                return cmp.ownerCt.itemId;
            }

            // Clicked on ListView Column
            if (cmp.ownerCt && cmp.ownerCt.container) {
                var container = cmp.ownerCt.container;
                if (container && container.component && container.component.ownerGrid) {
                    return container.component.ownerGrid.itemId;
                } else {
                    // Clicked on Grid Column
                    if (container && container.component && container.component.ownerCt && container.component.ownerCt.grid) {
                        return container.component.ownerCt.grid.itemId;
                    }
                }
            }
            return "";
        }
    } catch (e) {
        console.log('VT_GetGridIdFromEditor error : ' + e.message);
    }
}