﻿Ext.define('VT.ux.VTDatePicker', {
    extend: 'Ext.form.field.Date',
    alias: 'widget.vtdate',
    spinInterval: 'Day',
    showSpinner: false,
    triggers: {
        spinner: {
            type: 'spinner',
            scope: 'this'
        }
    },
    afterRender: function () {
        try {
            this.triggers['spinner'].upEl.on('click', this.upDateClick, this);
            this.triggers['spinner'].downEl.on('click', this.downDateClick, this);
            if (this.showSpinner) {
                this.switchTriggers('spinner');
            }
            else {
                this.switchTriggers('picker');
            }

            this.callParent();
        } catch (err) {
            console.log(err.message);
        }
    },
    switchTriggers: function (triggerName) {
        try {
            if (triggerName == 'spinner') {
                this.triggers['picker'].hide();
                this.triggers[triggerName].show();
            }
            else {
                this.triggers['spinner'].hide();
                this.triggers[triggerName].show();
            }
        } catch (err) {
            console.log(err.message);
        }
    },
    upDateClick: function () {
        try {
            switch (this.spinInterval) {
                case 'Month':
                    this.rawDate.setMonth(this.rawDate.getMonth() + 1);
                    break;
                case 'Year':
                    this.rawDate.setYear(this.rawDate.getFullYear() + 1);
                    break;
                default:
                    this.rawDate.setDate(this.rawDate.getDate() + 1);
            }
            this.setValue(this.rawDate);
        } catch (err) {
            console.log(err.message);
        }
    },
    downDateClick: function () {
        try {
            switch (this.spinInterval) {
                case 'Month':
                    this.rawDate.setMonth(this.rawDate.getMonth() - 1);
                    break;
                case 'Year':
                    this.rawDate.setYear(this.rawDate.getFullYear() - 1);
                    break;
                default:
                    this.rawDate.setDate(this.rawDate.getDate() - 1);
            }
            this.setValue(this.rawDate);
        } catch (err) {
            console.log(err.message);
        }
    }

});
