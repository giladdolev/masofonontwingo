﻿
/// <summary>
/// Ensure design mode is on.
/// </summary>
VT_DesignMode = true;


function VT_onAfterInitialize() {
    /// <signature>
    ///   <summary>Provides support for processing actions after initialization of the app environment.</summary>
    /// </signature>

    // Get the root component
    var rootComponent = window.VT_RootComponent;

    // If there is a valid component
    if(rootComponent)
    {
        // Get all components within the root component
        var components = rootComponent.query();

        // Get components length
        var componentsLength = components.length;

        // Add components start tag
        var manifest = "<components>";

        // Loop all components
        for (var index = 0; index < componentsLength; index++) {

            // Get component
            var component = components[index];
            
            // If there is a valid component
            if (component && component.isVisible()) {

                // Get component id
                var id = component.getItemId();

                // If there is a valid id
                if (id) {
                    try{
                        // Add component tag
                        manifest += "<component id=\"" + id + "\"  x=\"" + component.getX() + "\"  y=\"" + component.getY() + "\"  h=\"" + component.getHeight() + "\"  w=\"" + component.getWidth() + "\"/>";
                    }
                    catch (e) {
                        manifest += "<component id=\"" + id + "\"  e=\"" + e.message + "\" />";
                    }
                }
            }
        }

        // Add components end tag
        manifest += "</components>";

        // Set the component manifest
        document.body.setAttribute("vt_components_manifest", manifest)
    }
}

