﻿/**
* This class provides support for a drop down field control 
*/
Ext.define('Ext.form.field.DropDownControl', {
    extend: 'Ext.form.field.Picker',
    alias: 'widget.dropdownfield',

    // Handles initialization and creation of the picker
    createPicker: function () {

        var me = this;

        // Create drop control config
        var dropctl = {
            floating: true,
            minHeight: me.minPickerHeight,
            maxHeight: me.maxPickerHeight,
            manageHeight: false,
            matchFieldWidth: false,
            shadow: true,

            listeners: {

                // Attach record click 
                recordclick: function (record) {
                    me.setValue(record.id);
                    me.collapse();
                }
            }
        };

        // Merge with drop control configuration
        dropctl = Ext.apply(me.dropdownctl, dropctl);

        // Create and return picker
        picker = Ext.create(dropctl);
        return picker;
    },



});

/**
 * This class provides support for VT data view
 */
Ext.define('VT.ux.DataView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.vtdataview',
    layout: 'border',

    // Handles initialization and creates items configuration from component configuration.
    initComponent: function () {

        var me = this,
            items = [];

        // Create empty store
        var emptyStore = { fields: [], data: [{}] };

        var contentWidth = 100;
        contentWidth = me.calculateWidthFromItems(contentWidth, me.headers);
        contentWidth = me.calculateWidthFromItems(contentWidth, me.footers);
        contentWidth = me.calculateWidth(contentWidth, me.details);

        // Create the footers grid
        me.createGrids(me.footers, emptyStore, items, 'south', false, contentWidth);

        // Create the headers grid
        me.createGrids(me.headers, emptyStore, items, 'north', false, contentWidth);


        // Create grid
        me.createGrid(me.details, me.store, items, 'center', true, contentWidth);



        // Set the items
        me.items = items;


        me.on({
            afterlayout: function () {
                var index = 0,
                    cmp = null,
                    cmps = [];

                // Loop all components
                while ((cmp = this.getComponent(index)) != null) {

                    // If is the center grid
                    if (cmp.getItemId() == 'centerGrid') {

                        // Attach scroll event
                        cmp.view.getEl().on('scroll', function (e, t) {

                            // Loop all components
                            for (var i = 0; i < cmps.length; i++) {

                                // Synchronize the scroll left
                                cmps[i].scrollLeft = t.scrollLeft;
                            }
                        });
                    }
                    else {
                        // Store the component
                        cmps.push(cmp.view.getEl().dom);
                    }

                    // Increment index
                    index++;
                }

            }
        });

        // Call the parent implementation
        me.callParent();
    },

    getStore: function () {

        // Get the center grid
        var centerGrid = this.getComponent('centerGrid');

        // If there is a valid grid
        if (centerGrid != null) {
            // Return the grid store
            return centerGrid.getStore();
        }

        return null;
    },

    privates: {

        calculateWidthFromItems: function (contentWidth, bands) {
            if (bands != null) {
                for (var i in bands) {
                    contentWidth = this.calculateWidth(contentWidth, bands[i]);
                }
            }
            return contentWidth;
        },

        calculateWidth: function (contentWidth, band) {

            if (band != null) {
                var items = band.items;
                if (items != null) {
                    for (var i in items) {
                        contentWidth = this.calculateControlWidth(contentWidth, items[i]);
                    }
                }
            }
            return contentWidth;
        },

        calculateControlWidth: function (contentWidth, control) {

            if (control != null) {
                contentWidth = Math.max(contentWidth, control.x + control.width);
            }

            return contentWidth;
        },

        // Creates the grids given the specific configurations
        createGrids: function (gridConfigs, store, items, region, isBody, contentWidth) {

            // If there are valid configurations
            if (gridConfigs != null) {

                // Loop all configurations
                for (var i in gridConfigs) {

                    // Get current item
                    var gridConfig = gridConfigs[i];

                    // If there is a valid grid configuration
                    if (gridConfig != null) {

                        // Create grid
                        this.createGrid(gridConfig, store, items, region, isBody, contentWidth);
                    }
                }
            }
        },

        // Creates the grid given the specific configuration
        createGrid: function (gridConfig, store, items, region, isBody, contentWidth) {

            var me = this;

            // If there is a valid grid
            if (gridConfig != null) {

                // Create grid item
                var gridItem = {
                    xtype: 'gridpanel',
                    viewType: 'vtdataviewview',
                    viewConfig: {
                        rowHeight: gridConfig.height,
                        trackOver: isBody

                    },
                    hideHeaders: true,
                    plugins: {
                        ptype: 'cellediting',
                        clicksToEdit: 1
                    },
                    constrain: true,
                    scrollable: isBody,
                    store: store,
                    region: region,
                    disableSelection: !isBody,
                    border: false,
                    listeners: gridConfig.listeners
                };

                // If is not the body we need height
                if (!isBody) {
                    gridItem.height = gridConfig.height;
                } else {

                    // Set the body center grid
                    gridItem.itemId = 'centerGrid';

                    // Apply grid listeners
                    gridItem.listeners = Ext.apply({
                        rowclick: function (view, record) {
                            me.fireEvent('recordclick', record);
                        },
                        select: function (model, selected) {
                            // If there is a valid selected item
                            if (selected != null) {

                                // Get store
                                var store = this.getStore();

                                // If there is a valid store
                                if (store != null) {

                                    // Get proxy 
                                    var proxy = store.getProxy();

                                    // IF there is a valid proxy
                                    if (proxy != null && proxy.setSelection) {

                                        // Set the selection
                                        proxy.setSelection(selected);
                                    }
                                }
                            }
                        }
                    }, gridItem.listeners);
                }

                // Copy body style grid config
                if (gridConfig.bodyStyle) {
                    gridItem.bodyStyle = gridConfig.bodyStyle;
                }

                // Copy cls grid config
                if (gridConfig.cls) {
                    gridItem.cls = gridConfig.cls;
                }

                // Get items
                var configItems = gridConfig.items;

                // If there are items
                if (configItems != null) {

                    var columns = [];

                    // Get column width
                    var columnWidth = contentWidth / Math.max(1, configItems.length);

                    // Loop all items
                    for (var i in configItems) {

                        // Get current item
                        var configItem = configItems[i];

                        // Build column
                        var column = {
                            width: columnWidth,
                            xheight: configItem.height,
                            xwidth: configItem.width,
                            xtop: configItem.y,
                            xleft: configItem.x,
                            dataIndex: configItem.member,
                            header: configItem.member,
                            renderer: configItem.renderer,
                            tpl: configItem.tpl,
                            cellWrap: configItem.textWrap,
                            style: configItem.fieldStyle
                        };

                        // Cell style
                        if (configItem.fieldStyle) {
                            if (!column.renderer) {
                                column.renderer = function (value, metaData) {
                                    metaData.tdStyle = metaData.column.style;

                                    // Return cell data
                                    if (value) {
                                        return value;
                                    }
                                    if (metaData && metaData.column && metaData.column.tpl && metaData.column.tpl.html) {
                                        return metaData.column.tpl.html;
                                    }
                                }
                            }
                        }

                        // If there is a column xtype
                        if (configItem.xtype != null) {
                            // Set column xtype
                            column.xtype = configItem.xtype;
                        }

                        // Get editor configuration
                        var editorConfig = configItem.editor;

                        // If there is a valid editor configuration
                        if (editorConfig != null) {

                            // Create editor configuration
                            var editor = {
                            };

                            // Copy configuration
                            for (var name in editorConfig) {

                                // If is a copy instruction
                                if (name == '__copy') {

                                    // Get copy variables
                                    var copyVars = editorConfig[name];

                                    // If there are valid variables
                                    if (Object.prototype.toString.call(copyVars) == '[object Array]') {

                                        // Loop all variables
                                        for (var iVar in copyVars) {

                                            // Copy values to 
                                            editor[copyVars[iVar]] = configItem[copyVars[iVar]];
                                        }
                                    }

                                } else {
                                    editor[name] = editorConfig[name];
                                }
                            }

                            // Set the editor
                            column.editor = editor;
                        }

                        // Add to column list
                        columns.push(column);
                    }

                    // Set the columns
                    gridItem.columns = columns;
                }

                // Add grid item
                items.push(gridItem);

                // Return the grid item
                return gridItem;
            }
        }
    }

});


Ext.define("VT.ux.DataViewView", {
    extend: 'Ext.view.Table',
    alias: 'widget.vtdataviewview',


    tpl: [
    '{%',
        'view = values.view;',
        'if (!(columns = values.columns)) {',
            'columns = values.columns = view.ownerCt.getVisibleColumnManager().getColumns();',
        '}',
        'values.fullWidth = 0;',

        'for (i = 0, len = columns.length; i < len; i++) {',
            'column = columns[i];',
            'values.fullWidth += (column.cellWidth = column.lastBox ? column.lastBox.width : column.width || column.minWidth);',
        '}',


        'tableCls=values.tableCls=[];',
    '%}',
    '<div class="' + Ext.baseCSSPrefix + 'grid-item-container" style="width:100%">',
        '{[view.renderTHead(values, out, parent)]}',
        '{%',
            'view.renderRows(values.rows, values.columns, values.viewStartIndex, out);',
'%}',
'{[view.renderTFoot(values, out, parent)]}',
'</div>',
{
    definitions: 'var view, tableCls, columns, i, len, column;',
    priority: 0
}
    ],

    outerRowTpl: [
'<table id="{rowId}" ',
    'data-boundView="{view.id}" ',
    'data-recordId="{record.internalId}" ',
    'data-recordIndex="{recordIndex}" ',


    'class="{[values.itemClasses.join(" ")]}" cellPadding="0" cellSpacing="0" {ariaTableAttr} style="width:100%;{itemStyle}">',



        '{%',
            'this.nextTpl.applyOut(values, out, parent)',
        '%}',
'</table>', {
    priority: 9999
}
    ],
    rowTpl: [
'{%',
'var dataRowCls = values.recordIndex === -1 ? "" : " ' + Ext.baseCSSPrefix + 'grid-row";',
'%}',
'<tr class="{[values.rowClasses.join(" ")]} {[dataRowCls]}" {rowAttr:attributes} {ariaRowAttr}>',
'<td>',
'<div style="position:relative;height:{view.rowHeight}px;width:100%">',
'<tpl for="columns">' +
    '{%',
        'parent.view.renderCell(values, parent.record, parent.recordIndex, parent.rowIndex, xindex - 1, out, parent)',
     '%}',
'</tpl>',
'</div>',
'</td>',
'</tr>',
{
    priority: 0
}
    ],
    cellTpl: [
'<div class="{tdCls}" {tdAttr} {[Ext.aria ? "id=\\"" + Ext.id() + "\\"" : ""]} style="',
'width:{column.xwidth}px;',
'height:{column.xheight}px;',
'top:{column.xtop}px;',
'left:{column.xleft}px;',
'overflow:hidden;',
'position:absolute;<tpl if="tdStyle">{tdStyle}</tpl>" tabindex="-1" {ariaCellAttr} data-columnid="{[values.column.getItemId()]}">',
'<div {unselectableAttr} class="' + Ext.baseCSSPrefix + 'grid-cell-inner {innerCls}" ',
'style="text-align:{align};<tpl if="style">{style}</tpl>" {ariaCellInnerAttr}>{value}</div>',
'</div>',
{
    priority: 0
}
    ],



    cellSelector: 'div.' + Ext.baseCSSPrefix + 'grid-cell',

    finishedLayout: function (ownerContext) {

    },

    handleUpdate: function (store, record, operation, changedFieldNames) {
        var me = this,
            rowTpl = me.rowTpl,
            oldItem, oldItemDom, oldDataRow,
            newItemDom,
            newAttrs, attLen, attName, attrIndex,
            overItemCls,
            focusedItemCls,
            selectedItemCls,
            columns,
            column,
            columnsToUpdate = [],
            len, i,
            hasVariableRowHeight = me.variableRowHeight,
            cellUpdateFlag,
                updateTypeFlags = 0,
                cell,
    fieldName,
    value,
                defaultRenderer,
                scope,
    ownerCt = me.ownerCt;

        if (me.viewReady) {

            oldItemDom = me.getNodeByRecord(record);


            if (oldItemDom) {
                overItemCls = me.overItemCls;
                focusedItemCls = me.focusedItemCls;
                selectedItemCls = me.selectedItemCls;
                columns = me.ownerCt.getVisibleColumnManager().getColumns();




                for (i = 0, len = columns.length; i < len; i++) {
                    column = columns[i];




                    cellUpdateFlag = me.shouldUpdateCell(record, column, changedFieldNames);

                    if (cellUpdateFlag) {



                        updateTypeFlags = updateTypeFlags | cellUpdateFlag;

                        columnsToUpdate[columnsToUpdate.length] = column;
                        hasVariableRowHeight = hasVariableRowHeight || column.variableRowHeight;
                    }
                }




                if (!me.getRowFromItem(oldItemDom) || (updateTypeFlags & 1) || (oldItemDom.tBodies[0].childNodes.length > 1)) {
                    oldItem = Ext.fly(oldItemDom, '_internal');
                    newItemDom = me.createRowElement(record, me.dataSource.indexOf(record), columnsToUpdate);
                    if (oldItem.hasCls(overItemCls)) {
                        Ext.fly(newItemDom).addCls(overItemCls);
                    }
                    if (oldItem.hasCls(focusedItemCls)) {
                        Ext.fly(newItemDom).addCls(focusedItemCls);
                    }
                    if (oldItem.hasCls(selectedItemCls)) {
                        Ext.fly(newItemDom).addCls(selectedItemCls);
                    }





                    if (Ext.isIE9m && oldItemDom.mergeAttributes) {
                        oldItemDom.mergeAttributes(newItemDom, true);
                    } else {
                        newAttrs = newItemDom.attributes;
                        attLen = newAttrs.length;
                        for (attrIndex = 0; attrIndex < attLen; attrIndex++) {
                            attName = newAttrs[attrIndex].name;
                            if (attName !== 'id') {
                                oldItemDom.setAttribute(attName, newAttrs[attrIndex].value);
                            }
                        }
                    }



                    if (columns.length && (oldDataRow = me.getRow(oldItemDom))) {
                        me.updateColumns(oldDataRow, Ext.fly(newItemDom).down(me.rowSelector, true), columnsToUpdate);
                    }


                    while (rowTpl) {
                        if (rowTpl.syncContent) {


                            if (rowTpl.syncContent(oldItemDom, newItemDom, changedFieldNames ? columnsToUpdate : null) === false) {
                                break;
                            }
                        }
                        rowTpl = rowTpl.nextTpl;
                    }
                }


                else {


                    if (!me.cellFly) {
                        me.cellFly = new Ext.dom.Fly();
                    }


                    for (i = 0, len = columnsToUpdate.length; i < len; i++) {
                        column = columnsToUpdate[i];


                        fieldName = column.dataIndex;

                        value = record.get(fieldName);
                        cell = oldItemDom.firstChild.firstChild.firstChild.firstChild.childNodes[column.getVisibleIndex()];


                        if (me.markDirty) {
                            me.cellFly.attach(cell);
                            if (record.isModified(column.dataIndex)) {
                                me.cellFly.addCls(me.dirtyCls);
                            } else {
                                me.cellFly.removeCls(me.dirtyCls);
                            }
                        }

                        defaultRenderer = column.usingDefaultRenderer;
                        scope = defaultRenderer ? column : column.scope;


                        if (column.updater) {
                            Ext.callback(column.updater, scope, [cell, value, record, me, me.dataSource], 0, column, ownerCt);
                        }
                        else {
                            if (column.renderer) {
                                value = Ext.callback(column.renderer, scope,
                                                [value, null, record, 0, 0, me.dataSource, me], 0, column, ownerCt);
                            }




                            if (column.producesHTML) {
                                cell.childNodes[0].innerHTML = value;
                            } else {
                                cell.childNodes[0].childNodes[0].data = value;
                            }
                        }


                        if (me.highlightClass) {
                            Ext.fly(cell).addCls(me.highlightClass);



                            if (!me.changedCells) {
                                me.self.prototype.changedCells = [];
                                me.prototype.clearChangedTask = new Ext.util.DelayedTask(me.clearChangedCells, me.prototype);
                                me.clearChangedTask.delay(me.unhighlightDelay);
                            }


                            me.changedCells.push({
                                cell: cell,
                                cls: me.highlightClass,
                                expires: Ext.Date.now() + 1000
                            });
                        }
                    }
                }


                if (hasVariableRowHeight) {
                    Ext.suspendLayouts();
                }



                me.fireEvent('itemupdate', record, me.store.indexOf(record), oldItemDom);


                if (hasVariableRowHeight) {
                    me.refreshSize();


                    Ext.resumeLayouts(true);
                }
            }
        }
    }


});
