﻿VT_MobileMode = true;

Ext.Loader.setConfig({
    enabled: true,
    paths: {
        'VT': 'Scripts/vt'
    }
});

Ext.require([
    'VT.data.proxy.VTServer'
]);

var classAliases = {
    'Ext.NestedList': ['widget.treepanel'],
    'Ext.data.TreeStore': ['widget.store:tree'],
    'Ext.data.Store': ['widget.store'],
    'grippanel': ['grid', 'widget.vtlistview'],
    'Ext.Panel': ['widget.window'],
    'Ext.layout.Float': ['layout.border', 'layout.absolute']
};

Ext.ClassManager.addNameAliasMappings(classAliases);

Ext.override(Ext.dataview.List, {
    getSelectedIndexes: function () {
        var result = [],
            me = this,
            selection = me.getSelections(),
            store = me.getStore();

        if (selection && store) {
            Ext.each(selection, function (item) {
                var index = store.indexOf(item);
                if (index >= 0) {
                    result.push(index);
                }
            });
        }

        return result;
    }
});


function VT_raiseMousePositionEvent() {
    /// <signature>
    ///   <summary>Raise the mouse position event.</summary>
    /// </signature>
    /// <remarks>Override original implementation.</remarks>

}

var _temp_view = null;

function VT_ShowWindow(view) {
    VT_ShowView(view);
}

function VT_ShowView(view) {
    /// <signature>
    ///   <summary>Opens a view.</summary>
    ///   <param name="view">The view to open.</param>
    /// </signature>
    if (view) {
        var actionSheet = Ext.create('Ext.Panel', {
            layout: 'fit',
            items: [view],
            //title: view.title,
            height: "100%",
            width: "100%"
        });

        view.on('hide', function () {
            VT_CloseView(actionSheet);
        });



        // Add action sheet
        Ext.Viewport.add(actionSheet);

        // Show the action sheet
        actionSheet.show();

        // set action sheet
        _temp_view = actionSheet;
    }
}

function VT_CloseView(id) {

    // Add action sheet
    Ext.Viewport.remove(Ext.Viewport.getComponent(id));
}


function VT_ShowLoadingMask(id, msg) {
    /// <signature>
    ///   <summary>Hide loading mask.</summary>
    ///   <param name="id">The id of the component to use to show mask.</param>
    ///   <param name="id">The global id or the full path id.</param>
    /// </signature>
    /// <remarks>Override original implementation.</remarks>
}

function VT_HideLoadingMask() {
    /// <signature>
    ///   <summary>Hide loading mask.</summary>
    /// </signature>
    /// <remarks>Override original implementation.</remarks>
}

function VT_ShowActionSheet(items) {    /// <signature>
    ///   <summary>Show the action sheet.</summary>
    ///   <param name="items">The items to show.</param>
    /// </signature>
    var actionSheet = Ext.create('Ext.ActionSheet', {
        defaults: {
            handler: function () {

                // Destroy the action sheet
                actionSheet.destroy();

                // Fire the tap event
                this.fireEvent("tap");
            }
        },
        items: items
    });

    // Add action sheet
    Ext.Viewport.add(actionSheet);

    // Show the action sheet
    actionSheet.show();

}


function VT_Alert(title, message, buttons, callback) {
    /// <signature>
    ///   <summary>Shows an alert box.</summary>
    ///   <param name="title">The title.</param>
    ///   <param name="message">The message.</param>
    /// </signature>

    // If there is no buttons
    if (!buttons) {

        // Set default buttons
        buttons = Ext.MessageBox.OK;
    }

    
    Ext.Msg.show({
        title: title,
        message: message,
        buttons: buttons,
        maxHeight: '90%',
        autoScroll: true,
        //multiLine: true,
        fn: callback
    });
}



var VT_CssUniqueId = 0;


function VT_GetCssClassForBackgroundImage(imageSrc, width, height) {
    width = typeof width !== 'undefined' ? width : 20;
    height = typeof height !== 'undefined' ? height : 20;
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.dynClass' + VT_CssUniqueId + ' { background-image: url(' + imageSrc + '); background-size: contain; width: ' + width + 'px; height: ' + height + 'px;}';
    document.getElementsByTagName('head')[0].appendChild(style);

    var cls = 'dynClass' + VT_CssUniqueId;
    VT_CssUniqueId++;
    return cls;
}