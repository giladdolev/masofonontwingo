﻿Ext.define('VT.ux.ComboGrid', {
    extend: 'Ext.form.field.ComboBox',
    requires: ['Ext.grid.Panel'],
    alias: 'widget.Grid-Combo',
    emptyText: '',
    queryCaching: false, // Must reload each time trigger is clicked
    queryMode: 'local', // Local query by default (for filtering to work)
    enableTrigger: true,
    filtered: false,
    displayField: '',
    valueField: '',
    selectedRowIndex: -1,
    enableKeyEvents: true,
    autoSelect: false, // Do not select first record automatically
    expandOnFocus: false,
    initComponent: function () {
        this.callParent(arguments);
        var me = this;
        this.on("keydown", me.keyDownHandler, me);
        this.on("specialkey", me.specialkeyHandler, me);
        this.on("focus", me.focusHandler, me);
        this.on("blur", me.blurHandler, me);
        this.on("beforerender", me.beforerenderHandler, me);
        this.on({ scope: me, change: me.changeHandler });    
    },
    test: function() {
        console.log("dfdfdfdf");
    },
    createPicker: function () {
        var me = this,
        picker,
        pickerCfg = Ext.apply({
            xtype: 'gridpanel',
            bufferedRenderer: true,
            /* plugins: {
                ptype: 'bufferedrenderer'
            },*/
            pickerField: me,
            viewConfig: {
                selectionModel: me.pickerSelectionModel,
                pickerField: me,
                preserveScrollOnRefresh: true
            },
            floating: true,
            hidden: true,
            store: me.store,
            displayField: me.displayField,
            pageSize: me.pageSize,
            tpl: me.tpl,
            refresh: Ext.emptyFn
        }, me.listConfig, me.defaultListConfig);

        picker = me.picker = Ext.widget(pickerCfg);

        picker.refresh = function () {
            picker.getView().refresh(arguments);
            //me.focusEl.focus(50);
        };

        picker.getNode = function () {
            picker.getView().getNode(arguments);
        };


        picker.highlightItem = function () {
            picker.getView().highlightItem(arguments);
        };

        // We limit the height of the picker to fit in the space above
        // or below this field unless the picker has its own ideas about that.
        if (!picker.initialConfig.maxHeight) {
            picker.on({
                beforeshow: me.onBeforePickerShow,
                scope: me
            });
        }

        picker.getSelectionModel().on({
            beforeselect: me.onBeforeSelect,
            beforedeselect: me.onBeforeDeselect,
            selectionchange: me.onSelectionChange,
            select: me.onSelect,
            scope: me
        });

        picker.store.on({
            endupdate: me.onEndupdate,
            scope: me
        });
        var store = this.getStore();
        if (store.data.items.length > 0) {
            var keys = Object.keys(this.getPicker().getStore().data.items[1].data);

            for (i = 1; i < keys.length; i++) {
                var column = Ext.create('Ext.grid.column.Column', {
                    text: keys[i],
                    dataIndex: keys[i]
                });
                if (column.dataIndex != "id") {
                    picker.ownerGrid.columns.push(column);
                }
            }
        }
        return picker;
    },
    onExpand: function () {
        //Remove all filters on expand
        this.getPicker().store.getFilters().removeAll();

        var res = this.HighlightMatch();
        if (res == -1) {
            this.inputEl.focusOnExpand = true;
            this.inputEl.focus();
        }
    },  
    onCollapse: function () {
        var rowIndex = this.getSelectedRowIndex()
        console.log("collapse:" + rowIndex);
        this.setSelectedRowIndex(rowIndex);
    },
    onEndupdate: function (picker) {
        if (this.selectedRowIndex != -1) {
            var picker = this.getPicker();
            if (picker != null) {
                picker.getView().select(this.selectedRowIndex);
            }
        }
    },

    HighlightMatch: function () {
        var matchedItem = this.getStore().findRecord(this.displayField, this.getValue(), 0, false, false, true);
        if (matchedItem == null)
            matchedItem = this.getStore().findRecord(this.valueField, this.getValue(), 0, false, false, true);
        if (matchedItem == null) {
            return -1;
        }
        var indxMatch = this.getStore().indexOf(matchedItem);
        var view = this.getPicker().getView();
        var items = view.getNodes();
        this.setHighlightedItem(view, matchedItem, items[indxMatch], this.getValue());
    },

    changeHandler: function (view, newValue, oldValue, eOpts) {
        try {
            this.HighlightMatch();
            //Fire textchange
            this.fireEvent('textChanged', view, newValue);
        } catch (err) {
            console.log("error :- textChanged : " + err.message);
        }
    },
    keyDownHandler: function (view, e, eOpts) {
        try {
            console.log("key down");
        } catch (err) {
            console.log("keyDownHandler : " + err.message);
        }
    },
    specialkeyHandler: function (view, e, eOpts) {
        try {
            var _key = e.getKey();
            if (_key == 13 || _key == 9) {
                var item = this.getHighlightedItem();
                var hValue = this.highlightedValue;
                var currentValue = this.valueOf();
                if (hValue != currentValue) {
                    this.HighlightMatch();
                    item = this.getHighlightedItem();
                }

                if (item) {
                    var grid = this.getPicker();
                    if (grid != null) {
                        var rowIndex = grid.store.indexOf(item);
                        if (rowIndex != -1) {
                            this.setSelectedRowIndex(rowIndex);
                            grid.getView().select(rowIndex);
                            this.collapse();
                        }
                    }
                }

                try {
                    if (is_touch_device()) {
                        var path = VT_getItemIdPathEx(view);
                        if (path) {
                            VT_raiseEvent(path, "KeyPress", { key: _key });
                        }
                    }
                } catch (err) { }
            }
        } catch (err) {
            console.log("specialkeyHandler : " + err.message);
        }
    },
    blurHandler: function (view, event, eOpts) {
        try {
      //      this.collapse();
        }
        catch (err) {
            console.log("error :- blurHandler : " + err.message);
        }
    },
    focusHandler: function (view, event, eOpts) {
        try {
            if (this.inputType == 'number') {
                this.inputEl.dom.setSelectionRange = null;
            }
            if (this.expandOnFocus) {
                view.expand();
            }

        }
        catch (err) {
            console.log("error :- focusHandler : " + err.message);
        }
    },
    beforerenderHandler: function () {
        if (this.getItemId() != 'supplier_name')
            this.inputType = 'number';
    },
    textChanged: function (view, newValue) {
        try {

        } catch (err) {
            console.log("error :- textChanged : " + err.message);
        }
    },
    setHighlightedItem: function (view, matchedItem, item, highlightedValue) {
        try {
            this.highlightedItem = matchedItem;
            this.highlightedValue = highlightedValue;
            view.highlightItem(item);

        } catch (err) { }
    },
    getHighlightedItem: function () {
        try {
            return this.highlightedItem;
        } catch (err) { }
    },
    setDisplayField: function (displayFieldValue) {
        this.displayField = displayFieldValue;
    },
    setValueField: function (valueFieldValue) {
        this.valueField = valueFieldValue;
    },
    getExpandOnFocus: function () {
        return this.expandOnFocus;
    },
    setExpandOnFocus: function (value) {
        if (this.expandOnFocus != value) {
            this.expandOnFocus = value;
        }
    },
    getSelectedRowIndex: function () {
        return this.selectedRowIndex;
    },
    setSelectedRowIndex: function (value) {
        if (this.selectedRowIndex != value) {
            this.selectedRowIndex = value;
        }
    },
    onSelectionChange: function (m,d) {
        console.log("on selection changed");
    },
    onSelect: function (grid, selected, eOpts) {
        console.log("selection changed");
        var rowIndex = grid.store.indexOf(selected);
        if (rowIndex != -1) {
            this.setRawValue(selected.get(this.displayField));
            this.setSelectedRowIndex(rowIndex);
            //this.collapse();
        }
    },
    getStore: function () {
        return this.getPicker().getStore();
    },
    getSelectedIndex: function (field, value) {
        var store = field.getStore();
        store.clearFilter();
        return store.indexOf(value);
    },
    createColumns: function () {

        var store = this.getStore();
        if (store.data.items.length > 0) {
            var keys = Object.keys(this.getPicker().getStore().data.items[1].data);

            for (i = 1 ; i < keys.length; i++) {
                var column = Ext.create('Ext.grid.column.Column', {
                    text: keys[i],
                    dataIndex: keys[i]
                });
                if (column.dataIndex != "id") {
                    this.getPicker().columns.push(column);
                }
            }
        }
        return this.getPicker().getColumns();
    },        
});
