﻿Ext.define('VT.ux.CheckBoxListView', {
    extend: 'VT.ux.ListView',
    xtype: 'vtcheckboxlistview',
    uses: ['Ext.selection.CheckboxModel'],
    config: {
        selModel: {
            selType: 'checkboxmodel',
            mode: 'MULTI',
            checkOnly: true,
        }
    },
    setCheckboxies: function (visible) {
        var me = this,
            columns = me.getColumns(),
            selectionColumn = columns && columns[0];
        me.showCheckboxes = visible;

        selectionColumn && selectionColumn[visible ? 'show' : 'hide']();
    },

    afterRender: function () {
        var me = this;

        me.callParent();
        me.setCheckboxies(me.showCheckboxes);
       
    },

    reconfigure: function () {
        var me = this;
        me.callParent(arguments);
        me.setCheckboxies(me.showCheckboxes);
    }
});
