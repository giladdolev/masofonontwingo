﻿
Ext.define('VT.ux.SPGridComponent', {
    extend: 'Ext.Container',
    alias: 'widget.spgridcomponent',
    defaultBindProperty: 'value',
    ownerWidgetGrid: '',

    viewModel: 'vm-spviewmodel',

    setValue: function () {
        this.getViewModel().set('record', this.getWidgetRecord());
    },

    layout: {
        type: "absolute"
    },
    setOwnerGrid: function (view) {
        var widgetGrid = view;
        widgetGrid = widgetGrid.ownerCmp
        if (widgetGrid) {
            widgetGrid = widgetGrid.ownerGrid;
            if (widgetGrid) {
                this.ownerWidgetGrid = widgetGrid.ownerGrid;
            }
        }
    },
    getOwnerGrid: function () {
        return this.ownerWidgetGrid;
    },

    height: 50,
    items: [
        {
            xtype: "label",
            x: 0,
            y: 0,
            docked: "right",
            style: "background:lightgrey",
            width: "30px",
            height: "50px",
            bind: {
                text: '{row_no}'
            },
            dataIndex: 'row_no',
        }, {
            xtype: "sptextfield",
            x: 0,
            y: 34,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "121px",
            height: " 22px",
            bind: {
                value: '{barcode}'
            },
            dataIndex: 'barcode',
        }, {
            xtype: "sptextfield",
            x: 157,
            y: 0,
            fieldStyle: "background:lightgrey",
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "171px",
            height: "24px",
            bind: {
                value: '{materials_material_name}'
            },
            dataIndex: 'materials_material_name',
        }, {
            xtype: "label",
            x: 163,
            y: 25,
            style: "background: lightblue",
            width: "20px",
            height: "24px",
            text: "מס"
        }, {
            xtype: "sptextfield",
            x: 186,
            y: 25,
            fieldStyle: "background:lightgrey",
            dataIndex: "inv_pack_quantity",
            width: "30px",
            height: "22px",
            bind: {
                value: '{inv_pack_quantity}',
            },
            dataIndex: 'inv_pack_quantity',
        }, {
            xtype: "label",
            x: 219,
            y: 25,
            style: "background: lightblue",
            width: "20px",
            height: "24px",
            text: "ח"
        }, {
            xtype: "sptextfield",
            x: 56,
            y: 25,
            width: "34px",
            height: "22px",
            dataIndex: "expected_material_quantity",
            bind: {
                value: '{expected_material_quantity}',
            },
            dataIndex: 'expected_material_quantity',
        }, {
            xtype: "sptextfield",
            x: 114,
            y: 25,
            width: "34px",
            height: "22px",
            dataIndex: "order_quantity",
            bind: {
                value: '{order_quantity}',
            },
            dataIndex: 'order_quantity',
        }, {
            xtype: "sptextfield",
            x: 172,
            y: 25,
            width: "34px",
            height: "22px",
            dataIndex: "invoice_pack_quantity",
            bind: {
                value: '{invoice_pack_quantity}',
            },
        }, {
            xtype: "sptextfield",
            x: 230,
            y: 25,
            width: "98px",
            height: "22px",
            dataIndex: "doc_no_en",
            bind: {
                value: '{doc_no_en}',
            },
            dataIndex: "doc_no_en",
        },
     {
         xtype: "label",
         x: 272,
         y: 25,
         style: "background: lightblue",
         width: "20px",
         height: "24px",
         text: "מ"
     }, {
         xtype: "gridMultiComboBox",
         x: 230,
         y: 25,
         fieldStyle: "background:lightgrey",
         dataIndex: "invoice_number",
         style: "background: lightgrey",
         width: "98px",
         height: "22px",
         bind: {
             value: '{doc_no}',
         },
         dataIndex: "doc_no",
     }
    ],
    listeners: {
        afterrender: function (view, eOpts) {
            this.setOwnerGrid(view);
        },
    }
});