﻿Ext.define('VT.ux.VTGridColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.grid-column',
    htmlContent: "",
    dataMembers: "",
    buttonVisibility: 'both',

    GetRendere: function (value, p, record) {
        try {
            var datamemebersarray = this.dataMembers.split(",");
            var expression;
            switch (datamemebersarray.length) {
                case 0:
                    return Ext.String.format(this.htmlContent);
                    break;
                case 1:
                    return Ext.String.format(this.htmlContent,
                        record.get(datamemebersarray[0]));
                    break;
                case 2:
                    return Ext.String.format(this.htmlContent,
                        record.get(datamemebersarray[0]), record.get(datamemebersarray[1]));
                    break;
                case 3:
                    return Ext.String.format(this.htmlContent,
                        record.get(datamemebersarray[0]), record.get(datamemebersarray[1]), record.get(datamemebersarray[2]));
                    break;
                case 4:
                    return Ext.String.format(this.htmlContent,
                        record.get(datamemebersarray[0]), record.get(datamemebersarray[1]), record.get(datamemebersarray[2]), record.get(datamemebersarray[3]));
                    break;
                case 5:
                    return Ext.String.format(this.htmlContent,
                        record.get(datamemebersarray[0]), record.get(datamemebersarray[1]), record.get(datamemebersarray[2]), record.get(datamemebersarray[3]), record.get(datamemebersarray[4]));
                    break;
                case 6:
                    return Ext.String.format(this.htmlContent,
                        record.get(datamemebersarray[0]), record.get(datamemebersarray[1]), record.get(datamemebersarray[2]), record.get(datamemebersarray[3]), record.get(datamemebersarray[4]), record.get(datamemebersarray[5]));
                    break;

            }
        } catch (ex) {
        }
    },
    setButtonVisibility: function (value) {
        try {
            {
                this.buttonVisibility = value;
            }
        } catch (ex) {
        }
    },
    setHtmlContent: function (value) {
        try {
            {
                this.htmlContent = value;
            }
        } catch (ex) {
        }
    },
    setDataMembers: function (value) {
        try {
            {
                this.dataMembers = value;
            }
        } catch (ex) { }
    }
});