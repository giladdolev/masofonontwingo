﻿
Ext.define('VT.ux.VTCombobox', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.vtcombo',
    allowExpand: true,
    getAllowExpand: function () {
        return this.allowExpand;
    },
    setAllowExpand: function (allowToExpand) {
        this.allowExpand = allowToExpand;
    },
    onTriggerClick: function (e) {
        if (!this.isExpanded) {
            var tmp = this.getAllowExpand();
            this.setAllowExpand(true);
            this.expand();
            this.setAllowExpand(tmp);
        }
        else
            this.collapse();
    },

    expand: function () {
        if (this.getAllowExpand() != undefined) {
            if (this.getAllowExpand())
                this.superclass.expand.call(this);
            else {
                return true;
            }
        }
    }
});
