﻿Ext.define('VT.ux.VTGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.VTgridpanel',

    buttonVisibility: 'both',
    button1Text: "btn1",
    button2Text: "btn2",
    selectedRowIndex: -1,
    selectedColumnIndex: -1,
    hasDirtyCell: false,
    dirtyCellValue: "",
    bufferedRenderer: true,
    widgetFocusDataIndex: "",
    widgetFocusValue: "",

    viewConfig: {
        markDirty: false,
        loadMask: false
    },
    completeCellEditing: function () {
        this.plugins && Ext.Array.forEach(this.plugins, function (item) {
            if (Ext.getClassName(item) === 'Ext.grid.plugin.CellEditing') {
                item.completeEdit();
            }
        });
    },
    completeEdit: function () {
        var id = VT_getItemIdPathEx(this);
        VT_queueEventObject(id, "CellEndEdit", { newValue: this.dirtyCellValue }, false);
    },
    initComponent: function () {
            this.callParent(arguments);
            var me = this;
        },
   
    setButtonVisibility: function (value) {
        try {
            {
                this.buttonVisibility = value;
            }
        } catch (ex) {
        }
    },
    setButton1Text: function (value) {
        try {
            {
                this.button1Text = value;
            }
        } catch (ex) {
        }
    },
    getButton1Text: function () {
        try {
            {
                return this.button1Text;
            }
        } catch (ex) {
        }
    },
    setButton2Text: function (value) {
        try {
            {
                this.button2Text = value;
            }
        } catch (ex) {
        }
    },
    getButton2Text: function () {
        try {
            {
                return this.button2Text;
            }
        } catch (ex) {
        }
    },
    get1State: function (record) {
        try {
            var value = record.get('state_auto_scan_status');
            switch (value) {
                case 'none':
                    return true;
                    break;
                case 'both':
                case 'right':
                    return false;
                    break;
                case 'left':
                    return true;
                default:

            }
        } catch (ex) {
        }
    },
    get2State: function (record) {
        try {
            var value = record.get('state_auto_scan_status');
            switch (value) {
                case 'none':
                    return true;
                    break;
                case 'both':
                case 'left':
                    return false;
                    break;
                case 'right':
                    return true;
                default:

            }
        } catch (ex) {
            }
    },
    setSelectedRowIndex: function (value) {
        this.selectedRowIndex = value;
        },
    getSelectedRowIndex: function () {
        return this.selectedRowIndex;
        },
    setSelectedColumnIndex: function (value) {
        this.selectedColumnIndex = value;
    },
    getSelectedColumnIndex: function () {
        return this.selectedColumnIndex;
    },
    listeners: {
        dataChanged: function (view) {
            try {
                //console.log("dataChanged");
            } catch (err) {
                console.log("me.store.on" + err.message);
                }
        },
         load: function () {
            try {
                //console.log("load");
            } catch (err) {
                console.log("me.store.on" + err.message);
            }
        },
        widgetControlEditChanged: function (view, obj, value, dataIndex) {
            //console.log("widgetControlEditChanged");
        },
        widgetControlItemChanged: function (view, obj, value, dataIndex) {
            if (value == this.widgetFocusValue) {
                return false;
            }
            //console.log("widgetControlItemChanged");
        },
        widgetControlFocusEnter: function (view, obj, value, dataIndex) {
            this.widgetFocusDataIndex = dataIndex
            this.widgetFocusValue = value;
            //console.log("widgetControlFocusEnter");
        },
        widgetControlDoubleClick: function (view, obj, value, dataIndex) {
            //console.log("widgetControlDoubleClick");
        },
        widgetControlClick: function (view, obj, value, dataIndex) {
            //console.log("widgetControlClick");
        },
        selectionchange: function (view, selected, eOpts) {
            //console.log(selected);
        },
        cellclick: function (view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
            var oldRowindex = this.getSelectedRowIndex();
            this.setSelectedRowIndex(rowIndex);
            this.setSelectedColumnIndex(cellIndex);
            if (oldRowindex != rowIndex) {

                //raise selectionchanged
                view.ownerGrid.fireEvent("selectionchange", view.ownerGrid, record, eOpts);
        }
            //console.log(rowIndex);
    },

        afterrender: function () {
            var grd = this;

            grd.store && grd.store.on({
                load: {
                    fn: function () {
                        setTimeout(function (grd) {
                            grd.ownerGrid.fireEvent("", grd.ownerGrid);
                        },
                                   20,
                                       grd);


            }
    },
                single: true
            })
        }, 
    }
    });