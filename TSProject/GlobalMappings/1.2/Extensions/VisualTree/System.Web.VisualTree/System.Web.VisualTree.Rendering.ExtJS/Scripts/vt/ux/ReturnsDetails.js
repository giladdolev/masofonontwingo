﻿//For manual pallet
Ext.define('VT.ux.SPViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.vm-spviewmodelReturnsDetails',

    data: {
        row_no: '',
        barcode: '',
        material_name: '',
        material_quantity: '',
        supplier_number: '',
        declines: '',
        // multi combobox datra 
        invoice_data: '',
        record: null
    }
});

function VT_WidgetColumnAttachReturnsDetails(view, col, widget, rec) {
    var comboData = "";
    var viewModelRef = widget.getViewModel();
    view.ownerGrid.viewModelContrlos.push(viewModelRef);
    //TODO : hardcoded index vt grid multi combo box .
    displayRef = Ext.getCmp(widget.items.keys[widget.items.length - 1]);
    viewModelRef.set("record", rec);
    viewModelRef.set("row_no", rec.data['row_no']);
    viewModelRef.set("barcode", rec.data['barcode']);
    viewModelRef.set("material_name", rec.data['material_name']);
    viewModelRef.set("material_quantity", rec.data['material_quantity']);
    viewModelRef.set("supplier_number", rec.data['supplier_number']);
    viewModelRef.set("declines", rec.data['declines']);
    comboData = rec.data['invoice_data'];
    if (displayRef.xtype == "gridMultiComboBox") {
        //Hide value column
        displayRef.setStore(comboData);
        displayRef.getPicker().getColumns()[0].setHidden(true);
    }
    
}

Ext.define('VT.ux.SPGridComponent', {
    extend: 'Ext.Container',
    alias: 'widget.spgridcomponentReturnsDetails',
    defaultBindProperty: 'value',
    ownerWidgetGrid: '',

    viewModel: 'vm-spviewmodelReturnsDetails',

    setValue: function () {
        this.getViewModel().set('record', this.getWidgetRecord());
    },

    layout: {
        type: "absolute"
    },
    setOwnerGrid: function (view) {
        var widgetGrid = view;
        widgetGrid = widgetGrid.ownerCmp
        if (widgetGrid) {
            widgetGrid = widgetGrid.ownerGrid;
            if (widgetGrid) {
                this.ownerWidgetGrid = widgetGrid.ownerGrid;
            }
        }
    },
    getOwnerGrid: function () {
        return this.ownerWidgetGrid;
    },

    height: 50,
    items: [
        {
            xtype: "label",
            x: 0,
            y: 0,
            docked: "right",
            style: "background:lightgrey",
            width: "30px",
            cls: "WidgetColumnLabelRowNoCssClass",
            height: "50px",
            bind: {
                text: '{row_no}'
            },
            dataIndex: 'row_no',
        },
        {
            xtype: "sptextfield",
            x: 34,
            y: 0,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "121px",
            height: "22px",
            bind: {
                value: '{barcode}'
            },
            dataIndex: 'barcode',
        }, {
            xtype: "sptextfield",
            x: 157,
            y: 0,
            fieldStyle: "background:lightgrey",
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "164px",
            height: "24px",
            cls: "WidgetColumnDisableCssClass",
            readOnly: true,
            bind: {
                value: '{material_name}'
            },
            dataIndex: 'material_name',
        }, {
            xtype: "label",
            x: 34,
            y: 25,
            style: "background: lightblue",
            width: "100px",
            height: "24px",
            text: "כasd",
            cls: "WidgetColumnLabelCssClass"
        }, {
            xtype: "sptextfield",
            x: 56,
            y: 25,
            width: "34px",
            height: "22px",
            cls: "WidgetColumnDisableCssClass",
            bind: {
                value: '{material_quantity}',
            },
            dataIndex: 'material_quantity',
        },
        , {
            xtype: "label",
            x: 95,
            y: 25,
            style: "background: lightblue",
            cls: "WidgetColumnLabelCssClass",
            width: "34px",
            height: "24px",
            text: "ס"
        }, {
            xtype: "sptextfield",
            x: 121,
            y: 25,
            width: "34px",
            height: "22px",
            readOnly: true,
            dataIndex: "supplier_number",
            bind: {
                value: '{supplier_number}',
            },
        }, {
            xtype: "label",
            x: 158,
            y: 25,
            cls: "WidgetColumnLabelCssClass",
            width: "26px",
            height: "24px",
            text: "ס"
        }, 
        , {
            xtype: "gridMultiComboBox",
            x: 186,
            y: 25,
            fieldStyle: "background:lightgrey",
            dataIndex: "invoice_number",
            style: "background: lightgrey",
            width: "135px",
            height: "22px",
            bind: {
                value: '{declines}',
            },
            dataIndex: "declines",
            cls: "WidgetColumnDisableCssClass"
        }
    ],
    listeners: {
        afterrender: function (view, eOpts) {
            this.setOwnerGrid(view);
        },
    }
});
