﻿Ext.define('VT.ux.VTWidgetGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.VTWidgetGrid',
    initComponent: function () {
        this.callParent(arguments);
        var me = this;

    },
    trackMouseOver: false,
    hideHeaders: true,
    viewModelContrlos: {},
    WidgetTemplate: 'spgridcomponent',
    selectedRowIndex: -1,
    widgetDataMember: '',
    scrollable: true,
    viewConfig: {
        stripeRows: false,
        enableTextSelection: true,
        markDirty: false,
        loadMask: false
    },
    removeFromViewModelContrlos: function (index) {
        if (this.store.data.items.length <= index) {
            this.getViewModelContrlos()[index] = null;
        }

        //Reset all saved widget 
        this.widgetFocusDataIndex = "";
        this.widgetDataMember = "";
        this.widgetFocusValue = "";
    },
    resetViewModelContrlos: function () {
        this.viewModelContrlos = {};
        //Reset all saved widget 
        this.widgetFocusDataIndex = "";
        this.widgetDataMember = "";
        this.widgetFocusValue = "";
    },
    setSelectedRowIndex: function (value) {
        this.selectedRowIndex = value;
    },
    getSelectedRowIndex: function () {
        return this.selectedRowIndex;
    },
    getViewModelContrlos: function () {
        return this.viewModelContrlos;
    },
    listeners: {
        dataChanged: function (view) {
            try {
                //console.log("dataChanged");
            } catch (err) {
                console.log("dataChanged" + err.message);
            }
        },

        widgetControlEditChanged: function (view, obj, value, dataIndex) {
            if (obj == null || obj == "")
                return false;
            //console.log("widgetControlEditChanged  Row: " + obj + "  , DataIndex: " + dataIndex + "   ,Value:" + value);
        },
        widgetControlItemChanged: function (view, obj, value, dataIndex) {
            var rowIndex = obj;

            //If grid is refreshing then don't fire ItemChanged event.
            if (view.getView().refreshing)
                return false;

            var store = view.store;
            if (store) {
                var record = store.getAt(rowIndex);
                if (record) {
                    var valueFromStore = record.get(dataIndex);
                    if (valueFromStore == null)
                        valueFromStore = "";
                    if (value == valueFromStore) {
                        return false;
                    }
                }
            }
           

            view.lastDataIndex = dataIndex;
            view.lastValue = value;
            view.obj = rowIndex;
            //console.log("widgetControlItemChanged  Row: " + rowIndex + "  , DataIndex: " + dataIndex + "   ,Value:" + value);
        },
        widgetControlFocusEnter: function (view, obj, value, dataIndex) {
            this.widgetDataMember = dataIndex
            this.widgetFocusValue = value;
            //console.log("widgetControlFocusEnter");


            //Update SelectedRowIndex
            var rowIndex = obj;

           

            if (this.getSelectedRowIndex() != rowIndex) {
                this.setSelectedRowIndex(rowIndex);
                //raise selectionchanged
                var record = this.store.data.items[rowIndex];
                view.ownerGrid.fireEvent("selectionchange", view.ownerGrid, record);
            }
        },
        widgetControlDoubleClick: function (view, obj, value, dataIndex) {
            //console.log("widgetControlDoubleClick");
        },
        widgetControlClick: function (view, obj, value, dataIndex) {
            //console.log("widgetControlClick");
        },
        cellclick: function (view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
            if (this.getSelectedRowIndex() != rowIndex) {
                this.setSelectedRowIndex(rowIndex);
                //raise selectionchanged
                view.ownerGrid.fireEvent("selectionchange", view.ownerGrid, record, eOpts);
            }
        },
        selectionchange: function (view, selected, eOpts) {
            //console.log("selectionchange");
            try {
                var selectedRecord = null;
                if (Array.isArray(selected)) {
                    selectedRecord = selected[0];
                }
                else {
                    selectedRecord = selected;
                }
                //Select row
                var rowIndex = this.store.indexOf(selectedRecord);
                if (!this.getView().getNode(rowIndex).PreventHighlight) {
                    this.getSelectionModel().select(rowIndex);
                }
                else {
                    this.getSelectionModel().deselectAll(true);
                }

               
            } catch (err) {
            }
        },
    },
});