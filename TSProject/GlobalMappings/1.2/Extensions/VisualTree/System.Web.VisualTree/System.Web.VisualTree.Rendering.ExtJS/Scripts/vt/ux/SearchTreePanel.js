﻿Ext.define('VT.ux.SearchTreePanel', {
    extend: 'Ext.tree.Panel',
    requires:['Ext.form.field.Text'],
    alias: 'widget.searchTree',

    searchPanel: false,

    initComponent: function() {
        var me = this,
            lastFilterValue = "";
       
        Ext.apply(me, {          
            dockedItems:
            [{
                xtype: 'textfield',
                dock: 'top',
                emptyText: 'Search',
                enableKeyEvents: true,
                hidden: !me.getSearchPanel(),
                triggers:
                {
                    clear:
                        {
                            cls: 'x-form-clear-trigger',
                            handler: 'onClearTriggerClick',
                            hidden: true,
                            scope: 'this'
                        },
                    search:
                    {
                        cls: 'x-form-search-trigger',
                        weight: 1,
                        handler: 'onSearchTriggerClick',
                        scope: 'this'
                    }
                },
 
                onClearTriggerClick: function() {
                    this.setValue();
                    me.store.clearFilter();
                    this.getTrigger('clear').hide();
                },
 
                onSearchTriggerClick: function() {
                    me.filterStore(this.getValue());
                },
 
                listeners:
                {
                    keyup: {
                        fn: function(field, event, eOpts) {
                            var value = field.getValue();
 
                            // Only filter if they actually changed the field value.
                            // Otherwise the view refreshes and scrolls to top.
                            if (value == '') {
                                field.getTrigger('clear').hide();
                                me.filterStore(value);
                                lastFilterValue = value;
                            } else if (value && value !== lastFilterValue) {
                                field.getTrigger('clear')[(value.length > 0) ? 'show' : 'hide']();
                                me.filterStore(value);
                                lastFilterValue = value;
                            }
                        },
                        buffer: 300
                    },
 
                    render: function(field) {
                        this.searchField = field;
                    },
 
                    scope: me
                }
            }]
        });
 
        me.callParent(arguments);
 
    },

    setSearchPanel: function (value) {
        var me = this,
          textField = me.getDockedItems().find(function (x) { return x.xtype == 'textfield'; });

        me.searchPanel = value;  
        textField.getTrigger('search')[(value == true) ? 'show' : 'hide']();  
        textField.setVisible(value);    
              
    },

    getSearchPanel: function () {
        var me = this;

        return me.searchPanel; 
    },
 
    filterStore: function(value) {
        var me = this,
            store = me.store,
            searchString = value.toLowerCase(),
            filterFn = function(node) {
                var children = node.childNodes,
                    len = children && children.length,
                    visible = node.get('text') == searchString || node.get('ID') == searchString,
                    i;
 
                // If the current node does NOT match the search condition
                // specified by the user...
                if (!visible)
                {
 
                    // Check to see if any of the child nodes of this node
                    // match the search condition.  If they do then we will
                    // mark the current node as visible as well.
                    for (i = 0; i < len; i++)
                    {
                        if (children[i].isLeaf())
                        {
                            var result = (children[i].get('text') == searchString) ||  (children[i].get('ID') == searchString)
                            visible = result && children[i].get('visible');
                        } else
                        {
                            var result = (children[i].get('text') == searchString) || (children[i].get('ID') == searchString)
                            visible = result || filterFn(children[i]);
                        }
                        if (visible)
                        {
                            break;
                        }
                    }
 
                }
                else
                { // Current node matches the search condition...
 
                    // Force all of its child nodes to be visible as well so
                    // that the user is able to select an example to display.
                    for (i = 0; i < len; i++)
                    {
                        children[i].set('visible', true);
                    }
 
                }
 
                return visible;
            },
            v;
 
        if (searchString.length < 1)
        {
            store.clearFilter();
        }
        else
        {
            v = new RegExp(searchString,"ig");
            store.getFilters().replaceAll({
                filterFn: filterFn
            });
        }
    }
});