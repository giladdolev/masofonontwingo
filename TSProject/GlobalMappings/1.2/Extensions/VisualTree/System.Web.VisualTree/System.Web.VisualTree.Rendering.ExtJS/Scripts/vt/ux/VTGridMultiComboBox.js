﻿Ext.define('VT.ux.VTGridMultiComboBox', {
    extend: 'Ext.form.field.Picker',
    alias: 'widget.gridMultiComboBox',
    matchFieldWidth: false,
    focusable: true,
    zIndex: 1,
    columns: false,
    store: false,
    enableKeyEvents: true,
    dataSource: "",
    displayField: "text",
    valueField: "value",
    selectedRecords: [],
    isJsonObject: false,
    //queryMode: 'local',
    setPickerFocus: true,
    initComponent: function () {
        this.callParent(arguments);
        var me = this;

    },

    createPicker: function () {
        var me = this;
        var config = Ext.apply({
            scrollable: true,
            floating: true,
            store: this.getStore(),
            columns: this.getColumns(),
            listeners: {
                selectionchange: {
                    fn: function (grid, selectedRecords) {
                        this.setRecords(selectedRecords);
                        this.fireEvent('select', this, selectedRecords);

                        if (VT_getItemIdPathEx(this.up('grid')) == "w_mini_terminal/pnlContent/uo_body/dw_inv_pack_details") {
                            this.collapse();
                            this.inputEl.focus(100);
                            return;
                        }

                        var ownergrid = this.ownerCt.ownerCmp.ownerGrid;
                        var rowIndex = ownergrid.getView().indexOf(this.el.up('table'));
                        ownergrid.fireEvent('widgetControlItemChanged', ownergrid, rowIndex, this.getRawValue(), this.dataIndex);
                        this.collapse();
                        this.focus(100);
                    },
                    scope: this
                },
                beforeshow: function (grid, eOpts) {
                    var gridStoreItems = grid.store.data.items;
                    var comboStoreItems = grid.PickerField.store.data.items;

                    // If they have different items count reconfigure
                    if (gridStoreItems.length != comboStoreItems.length) {
                        grid.reconfigure(grid.PickerField.store);
                    }

                    // If they have the same counter, check the data
                    if (gridStoreItems.length == comboStoreItems.length) {

                        // Check if they have the same data
                        for (i = 0; i < comboStoreItems.length; i++) {
                            if (comboStoreItems[i].data.text != gridStoreItems[i].data.text || comboStoreItems[i].data.value != gridStoreItems[i].data.value) {
                                grid.reconfigure(grid.PickerField.store);
                                break;
                            }
                        }
                    }
                    if (this.columns.length > 0) {
                        var w = 0;
                        this.columns.forEach(function (col) {
                            if (!col.hidden) {
                                w += col.width
                            }
                        });
                        this.width = w
                    }
                }
            },

        }, this.gridConfig);
        var grid = Ext.create('Ext.grid.Panel', config);
        grid.PickerField = this;

        return grid
    },
    defaultListConfig: {
        minHeight: 26,
        minWidth: 70
    },
    columns: [{
        xtype: "gridcolumn",
        editor: {
            xtype: "textfield"
        },
        menuDisabled: "true",
        dataIndex: "value"
    }, {
        xtype: "gridcolumn",
        editor: {
            xtype: "textfield"
        },
        menuDisabled: "true",
        dataIndex: "text"
    },
    ],
    setRecords: function (records) {
        if (records && !Ext.isArray(records)) {
            records = [records]
        }
        this.selectedRecords = records;
        var rawValue = [];
        Ext.Array.each(records, function (record) {
            rawValue.push(record.get(this.displayField))
        }, this);
        this.setRawValue(rawValue.join(this.delimiter))
    },
    getRecords: function () {
        return this.selectedRecords
    },
    getSubmitValue: function () {
        var values = [];
        Ext.Array.each(this.selectedRecords, function (record) {
            values.push(record.get(this.displayField))
        }, this);
        return values
    },
    getValue: function () {
        return this.getSubmitValue()
    },
    beforeReset: function () {
        if (this.picker) {
            this.picker.getSelectionModel().deselectAll()
        }
        this.callParent(arguments);
    },
    setStore: function (storeData) {
        try {
            var newData = this.tryJSONParse(storeData);
            var userStore = Ext.create('Ext.data.Store', {
                fields: [{
                    name: 'value',
                    type: 'string'
                }, {
                    name: 'text',
                    type: 'string'
                }, ],
                data: newData,
            });
            this.store = userStore;

        } catch (ex) { }
    },
    tryJSONParse: function (storeData) {
        try {
            if (storeData == '') {
                this.setIsJsonObject(false);
                return storeData
            }
            return JSON.parse(storeData);
            this.setIsJsonObject(true)
        } catch (ex) {
            this.setIsJsonObject(false);
            return storeData
        }
    },
    getIsJsonObject: function () {
        return this.isJsonObject
    },
    setIsJsonObject: function (value) {
        this.isJsonObject = value
    },
    getStore: function () {
        if (!this.store) {
            this.store = Ext.create('Ext.data.Store', {})
        }
        return this.store
    },
    getColumns: function () {
        if (!this.columns) {
            this.columns = []
        }
        return this.columns
    },
    setdisplayField: function (value) {
        this.displayField = value
    },
    getdisplayField: function () {
        return this.displayField
    },
    setvalueField: function (value) {
        this.valueField = value
    },
    getvalueField: function () {
        return this.valueField
    },
    setselectedText: function (value) {
        this.selectedText = value;
        this.setRawValue(value)
    },
    getselectedText: function () {
        return this.selectedText
    },
    setdataSource: function (value) {
        this.dataSource = value
    },
    getdataSource: function () {
        return this.dataSource
    },
    onCollapse: function () {
        try {
            if (new Error().stack.includes('onGlobalScroll')) {
                setTimeout(function (combo) {
                    if (combo.el.getTop() > 280)
                        combo.pickerAlign = 'bl-tl?'
                    combo.expand();
                }, 80, this);
            }
        } catch (err) {
        }
    },
    listeners: {
        focusenter: function (view, e, eOpts) {
            try {
                if (view.preventRaisewidgetControlFocusEnter != true) {
                    var grid = this.ownerCt.ownerCmp.ownerGrid;
                    var rowIndex = grid.getView().indexOf(this.el.up('table'));
                    grid.fireEvent('widgetControlFocusEnter', grid, rowIndex, this.getRawValue(), this.dataIndex);
                }

                if (!view.isExpanded) {
                    view.expand();
                    view.inputEl.focus();
                }
            } catch (ex) { }
        },
        focusleave: function (view, e, eOpts) {
            try {
                if (view.isExpanded) {
                    view.collapse()
                }
            } catch (ex) { }
        },
        afterrender: function (view, eOpts) {

            view.inputEl.on('keydown', function (e, view, eOpts) {
                if (e.keyCode == 13 || e.keyCode == 9) {
                    var id = this.id.replace('-inputEl', '');
                    if (id) {
                        var cmp = Ext.getCmp(id);
                        if (cmp) {
                            if (VT_getItemIdPathEx(cmp.up('grid')) == "w_mini_terminal/pnlContent/uo_body/dw_inv_pack_details") {
                                var ownergrid = cmp.ownerCt.ownerCmp.ownerGrid;
                                var rowIndex = ownergrid.getView().indexOf(cmp.el.up('table'));
                                ownergrid.fireEvent('widgetControlItemChanged', ownergrid, rowIndex, cmp.getRawValue(), cmp.dataIndex);
                                //Raise widgetControlSpecialKey event
                                try {
                                    setTimeout(function (ownergrid, e, cmp, rowIndex) {
                                        var cellIndex = 0;
                                        var key = e.keyCode;
                                        var dataMember = cmp.dataIndex;
                                        ownergrid.fireEvent('widgetControlSpecialKey', ownergrid, null, cellIndex, null, null, rowIndex, { getKey: function () { return key; } }, dataMember);
                                    }, 100, ownergrid, e, cmp, rowIndex);
                                } catch (err2) {
                                    console.log("error :- specialkey - Raise widgetControlSpecialKey : " + err.message)
                                }
                                return;
                            }
                        }
                    }
                }
            });


            if (view.inputEl) {
                var inputElement = document.getElementById(view.inputEl.id);
                inputElement.parentEx = this;
                inputElement.onkeyup = function (evt) {
                    if (this.parentEx && this.parentEx.NotEditableEx) {
                        this.value = this.parentEx.value;
                    }
                }
            }

        }
    }
});
