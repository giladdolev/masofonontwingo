﻿Ext.define('VT.ux.SPGridColumn', {
    extend: 'Ext.grid.column.Widget',
    alias: 'widget.SPGridColumnwidgetcolumn',
    Rows: new Array(),
    initComponent: function () {
        this.callParent(arguments);
        this.Rows = new Array()
    },
    setDataMembers: function (value) {
        this.datamembers = value
    },
    getDataMembers: function (index) {
        var str = this.datamembers.split(",");
        return str[index]
    },
    setComboFields: function (fields) {
        this.comboFields = fields
    },
    getComboFields: function () {
        return this.comboFields
    },
    setComboData: function (data) {
        this.comboData = data
    },
    getComboData: function () {
        return this.comboData
    },
    getwidgetControl: function (rowIndex, dataMember) {
        try {
            if (rowIndex >= 0 && rowIndex < this.Rows.length) {
                for (var i = 0; i < this.Rows[rowIndex].length; i++) {
                    if (this.Rows[rowIndex][i].dataIndex == dataMember) {
                        return this.Rows[rowIndex][i]
                    }
                }
            }
        } catch (err) {
            console.log("error :- getwidgetControl" + err.message)
        }
    },
    onWidgetAttach: function (col, widget, rec) {
        var comboData = "";
        var grid = window.Ext.getCmp(this.el.up("div.x-grid").id);
        var index = col.Rows.length;
        if (index == grid.store.data.length) {
            this.Rows = new Array();
            index = 0
        }
        col.Rows[index] = new Array();
        for (i = 0; i < widget.items.keys.length; i++) {
            displayRef = Ext.getCmp(widget.items.keys[i]);
            var tabIndex = index * widget.items.keys.length + index + 50;
            displayRef.setTabIndex(tabIndex);
            col.Rows[index].push(displayRef);
            displayRef.tabIndex = i;
            switch (displayRef.xtype) {
                case "textfield":
                    comboData = rec.get(displayRef.dataIndex);
                    displayRef.setRawValue(rec.get(displayRef.dataIndex));
                    if (typeof displayRef.getEl().hasListeners["dblclick"] == 'undefined') {
                        displayRef.getEl().on('dblclick', function () {
                            var ownGrid = grid.ownerGrid;
                            var _rawValue = this.component.rawValue;
                            var _dataIndex = this.component.dataIndex;
                            grid.fireEvent('widgetControlDoubleClick', ownGrid, _rawValue, _rawValue, _dataIndex)
                        }, this.component, {
                            single: true
                        })
                    }
                    if (typeof displayRef.getEl().hasListeners["click"] == 'undefined') {
                        displayRef.getEl().on('click', function () {
                            var ownGrid = grid.ownerGrid;
                            var _rawValue = this.component.rawValue;
                            var _dataIndex = this.component.dataIndex;
                            grid.fireEvent('widgetControlClick', ownGrid, _rawValue, _rawValue, _dataIndex)
                        }, this.component, {
                            single: true
                        })
                    }
                    break;
                case "gridMultiComboBox":
                    comboData = rec.get(displayRef.dataIndex);
                    displayRef.setStore(comboData);
                    break;
                case "label":
                    comboData = rec.get(displayRef.dataIndex);
                    if (typeof displayRef.dataIndex !== 'undefined') {
                        displayRef.setText(rec.get(displayRef.dataIndex))
                    }
                default:
                    break
            }
        }
    }
});