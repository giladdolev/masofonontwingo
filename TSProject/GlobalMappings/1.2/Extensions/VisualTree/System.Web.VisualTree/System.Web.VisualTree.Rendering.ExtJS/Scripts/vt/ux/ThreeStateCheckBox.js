﻿Ext.define('VT.ux.ThreeStateCheckBox', {
    extend: 'Ext.form.field.Checkbox',
    alias: 'widget.three-check-box',

    values: ['0', '1', '-'],
    checkedClasses: ['', Ext.baseCSSPrefix + 'form-cb-checked', 'x-checkbox-dash'],
    threeState: false,
    currentCheck: 0, // internal use

    getSubmitValue: function () {
        return this.value;
    },

    getRawValue: function () {
        return this.value;
    },

    getValue: function () {
        return this.value;
    },

    initValue: function () {
        var me = this;
        if (this.threeState) {
            me.originalValue = me.lastValue = me.value;
            me.suspendCheckChange++;
            me.setValue(me.value);
            me.suspendCheckChange--;
        }
    },
    setThreeState: function (value) {

        this.threeState = value;
    },
    getThreeState: function () {

        return this.threeState;
    },

    setRawValue: function (v) {
        var me = this,
            oldCheck = me.currentCheck,
            inputEl = me.inputEl;



        if (v === false || v == 0) {
            v = '0';
        }
        if (v === true || v == 1) {
            v = '1';
        }
        if (v == null || v == '' || v === undefined) {
            v = '0';
        }


        me.currentCheck = me.getCheckIndex(v);
        me.value = me.rawValue = me.values[me.currentCheck];



        if (inputEl) {
            inputEl.dom.setAttribute('aria-checked', me.value == '1' ? true : false);
        }
        me.removeCls(me.checkedClasses)
        me.addCls(me.checkedClasses[this.currentCheck]);
    },

    // Override default function
    updateCheckedCls: Ext.emptyFn,

    // Returns the index from a value to a member of me.values
    getCheckIndex: function (value) {
        var values = this.values,
            length = values.length,
            i;

        for (i = 0; i < length; i++) {
            if (value === values[i]) {
                return i;
            }
        }
        return 0;
    },

    // Handels a click on the checkbox
    listeners: {
        afterrender: function () {
            var me = this;

            this.el.dom.onclick = function () {
                me.toggle();
                return false;
            };
        }
    },

    // Switches to the next checkbox-state
    toggle: function () {
        var me = this;

        if (!me.disabled && !me.readOnly) {
            var check = me.currentCheck;
            check++;


            //if the threeState property is false then we show only tow states
            if (this.getThreeState() === false && check === 2) {
                check++;
            }


            if (check >= me.values.length) {
                check = 0;
            }

            this.setValue(me.values[check]);


        }
    }
});
