﻿VT_MobileMode = false;

Ext.Loader.setConfig({
    enabled: true,
    //disableCaching: false,
    paths: {
        'VT': 'Scripts/vt'
    }
});

Ext.require([
        'Ext.ux.form.MultiSelect',
        'VT.data.proxy.VTServer',
        'VT.ux.MultiLineTabs',
        'VT.ux.MaskedTextBox',
        'VT.ux.DataView',
        'VT.ux.SearchTreePanel',
        'VT.ux.VTGridPanel',
		'VT.ux.ComboGrid',
        'VT.ux.ThreeStateCheckBox',
        'VT.ux.VTCombobox',
        'VT.ux.VTGridColumn',
        'VT.ux.VTGridMultiComboBox',
        'VT.ux.SPGridColumn',
        'VT.ux.ThreeStateCheckboxColumn',
        'VT.ux.VTWidgetGrid',
        'VT.ux.SPGridComponent',
        'VT.ux.SPViewModel',
        'VT.ux.SPTextFieldComponent',

]);

Ext.override(Ext.layout.BorderLayout, {
    calculateChildAxis: function (childContext, axis) {
        var collapseTarget = childContext.collapseTarget,
            setSizeMethod = 'set' + axis.sizePropCap,
            sizeProp = axis.sizeProp,
            childMarginSize = childContext.getMarginInfo()[sizeProp],
		region,
		isBegin,
		flex,
		pos,
		size;
        if (collapseTarget) {

            region = collapseTarget.region;
        } else {
            region = childContext.region;
            flex = childContext.flex;
        }
        isBegin = region === axis.borderBegin;
        if (!isBegin && region !== axis.borderEnd) {

            childContext[setSizeMethod](axis.end - axis.begin - childMarginSize - (this.owner.scrollFlags[axis.posProp] ? Ext.getScrollbarSize()[axis.sizeProp] : 0));
            pos = axis.begin;
        } else {
            if (flex) {
                size = Math.ceil(axis.flexSpace * (flex / axis.totalFlex));
                size = childContext[setSizeMethod](size);
            } else if (childContext.percentage) {

                size = childContext.peek(sizeProp);
            } else {
                size = childContext.getProp(sizeProp);
            }
            size += childMarginSize;
            if (isBegin) {
                pos = axis.begin;
                axis.begin += size;
            } else {
                axis.end = pos = axis.end - size;
            }
        }
        childContext.layoutPos[axis.posProp] = pos;
    }
});



Ext.override(Ext.Component, {
    initComponent: function () {
        this.callParent();
    },
    delayLayout: function () {
        var cmp = this;
        cmp.suspendLayout = true;

        clearTimeout(cmp.$layoutQueue);

        cmp.$layoutQueue = setTimeout(function () {
            if (cmp) {
                cmp.suspendLayout = false;
                cmp.updateLayout();
            }
        }, 1);
    }
});

Ext.define('Ext.form.field.FormattedText', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.formattedtext',
    formatOnBlur: true,
    formatOnInit:true,
    initComponent: function () {
        this.callParent(arguments);
    },
    listeners: {
        keydown: function (me, e, opts) {
            if (this.formatOnKeydown) {
                this.setValue((this.formatter(this.getValue())));
            }
        },
        blur: function (me, e, opts) {
            if (this.formatOnBlur) {
                this.setValue((this.formatter(this.getValue())));
            }
        },
        afterrender:function (m, opts) {
            if (this.formatOnInit) {
                this.setValue((this.formatter(this.getValue())));
            }
        }
    }
});



var cnp = Ext.util.Format.numberRenderer('0.00');
Ext.util.Format.decimal2places = function (v) {
    return cnp(v);
};


Ext.override(Ext.container.Viewport, {
    setTitle: function (title) {
        document.title = title;
    },
    afterLayout: function (layout) {
        if (Ext.supports.Touch) {
            if (document.documentElement) { // #928
                document.documentElement.scrollTop = 0;
            }

            if (document.body) { // #928
                document.body.scrollTop = 0;
            }
        }

        this.callSuper([layout]);
    }
});






Ext.onReady(function () {
    Ext.tip.QuickTipManager.init();
});


Ext.define('Vt.ux.Keyboard', {
    extend: 'Ext.panel.Panel',
    xtype: 'vtkeyboard',
    field: '',
    localX: 0,
    localY: 0,
});

Ext.define('Vt.ux.ListView', {
    extend: 'Ext.grid.Panel',
    xtype: 'vtlistview',
    uses: ['Ext.selection.CheckboxModel'],
    config: {
        selModel: {
            selType: 'checkboxmodel',
            mode: 'MULTI',
            checkOnly :true,
        }
    },
    setMultiSelect: function (isMulty) {
        var selModel = this.getSelectionModel();
        selModel && selModel.setSelectionMode && selModel.setSelectionMode(isMulty ? 'MULTI' : 'SINGLE');
    },
    setCellForeColor: function (color, rowIdx, colIdx) {
        var cell = this.view.getCellByPosition({ row: rowIdx, column: colIdx });
        if (cell != null) {
            cell.setStyle('color', color);
        }
    },
    setCheckboxies: function (visible) {
        var me = this,
            columns = me.getColumns(),
            selectionColumn = columns && columns[0];
        me.showCheckboxes = visible;

        selectionColumn && selectionColumn[visible ? 'show' : 'hide']();
    },

    afterRender: function () {
        var me = this;

        me.callParent();
        me.setMultiSelect(me.multiSelect);
        me.setCheckboxies(me.showCheckboxes);
        //console.log('me.showCheckboxes: ', me.showCheckboxes);
    },

    getSelectedIndexes: function () {
        var result = [],
            me = this,
            selModel = me.getSelectionModel(),
            selection = selModel && selModel.getSelection(),
            store = me.getStore();
        if (selection && store) {
            Ext.each(selection, function (item) {
                var index = store.indexOf(item);
                if (index >= 0) {
                    result.push(index);
                }
            });
        }

        return result;
    },

    reconfigure: function () {
        var me = this;
        me.callParent(arguments);
        me.setCheckboxies(me.showCheckboxes);
    }
});

Ext.override(Ext.grid.selection.Rows, {
    getLastRowIndex: function () {
        return this.getCount() ? this.view.dataSource.indexOf(this.selectedRecords.last()) : -1;
    }
});

Ext.override(Ext.window.Window, {
    constrainHeader: true
});

Ext.override(Ext.view.BoundListKeyNav, {
    onKeyUp: function (e) {
        var me = this,
            boundList = me.view,
            allItems = boundList.all,
            oldItem = boundList.highlightedItem,
            OldItemIdx = oldItem ? boundList.indexOf(oldItem) : -1,
            newItemIdx = OldItemIdx - 1;
        if (newItemIdx > -1) {
            // set the selected text
            me.setPosition(newItemIdx);
            var displayText = me.view.store.data.items[newItemIdx].data.Text
            me.view.ownerCmp.setValue(displayText)
        }
        // stop this from moving the cursor in the field
        e.preserventDefault();
    },
    onKeyDown: function (e) {
        var me = this,
             boundList = me.view,
            allItems = boundList.all,
            oldItem = boundList.highlightedItem,
            OldItemIdx = oldItem ? boundList.indexOf(oldItem) : -1,
            newItemIdx = OldItemIdx + 1;
        if (newItemIdx < allItems.getCount()) {
            // set the selected text
            me.setPosition(newItemIdx);
            var displayText = me.view.store.data.items[newItemIdx].data.Text
            me.view.ownerCmp.setValue(displayText)
        }
        // stop this from moving the cursor in the field
        e.preserventDefault();
    }
});


Ext.define('ThreeStateBool', {
    extend: 'Ext.data.field.Boolean',
    alias: 'data.field.threeStateBool',
    constructor: function () {
        this.callParent(arguments);
        this.allowNull = true;
    },

    convert: function (value) {

        if ((value === undefined || value === null || value === '') && this.getAllowNull()) {
            return null;
        }
        return (value !== 'false') && !!value;

    }
});