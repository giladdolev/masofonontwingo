﻿Ext.define('VT.ux.SPViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.vm-spviewmodel',

    data: {
        row_no: '',
        barcode:'',
        material_name: '',
        expected_material_quantity: '',
        order_quantity: '',
        invoice_pack_quantity:'',
        doc_no_en: '',
        doc_no: '',
        // multi combobox datra 
        invoice_data:'',
        record: null
    }
});
