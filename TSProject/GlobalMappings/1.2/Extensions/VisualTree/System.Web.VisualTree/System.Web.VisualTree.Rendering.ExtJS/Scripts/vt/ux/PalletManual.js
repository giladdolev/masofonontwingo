﻿//For manual pallet
Ext.define('VT.ux.SPViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.vm-spviewmodelPalletManual',

    data: {
        row_no: '',
        barcode: '',
        material_name: '',
        expected_material_quantity: '',
        order_quantity: '',
        invoice_pack_quantity: '',
        doc_no_en: '',
        doc_no: '',
        declines:'',
        // multi combobox datra 
        invoice_data: '',
        record: null
    }
});

function VT_WidgetColumnAttachPalletManual(view, col, widget, rec) {
    var comboData = "";
    var viewModelRef = widget.getViewModel();
    view.ownerGrid.viewModelContrlos.push(viewModelRef);

    //TODO : hardcoded index vt grid multi combo box .
    displayRef = Ext.getCmp(widget.items.keys[widget.items.length - 1]);
    viewModelRef.set("record", rec);
    viewModelRef.set("row_no", rec.data['row_no']);
    viewModelRef.set("barcode", rec.data['barcode']);
    viewModelRef.set("material_name", rec.data['material_name']);
    viewModelRef.set("order_quantity", rec.data['order_quantity']);
    viewModelRef.set("expected_material_quantity", rec.data['expected_material_quantity']);
    viewModelRef.set("invoice_pack_quantity", rec.data['invoice_pack_quantity']);
    viewModelRef.set("doc_no_en", rec.data['doc_no_en']);
    viewModelRef.set("doc_no", rec.data['doc_no']);
    comboData = rec.data['invoice_data'];
    if (displayRef.xtype == "gridMultiComboBox") {
        displayRef.setStore(comboData);
    }
    var doc_no_en = rec.data['doc_no_en'];
    if(doc_no_en > 1)
    {
        displayRef.setDisabled(false);
    } else {
        displayRef.setDisabled(true);
    }

    var declines = rec.data['declines'];
    if(declines > 0)
    {
        widget.addCls("TrueFontWeightExpressionDeclineAttribute");
    } else {
        widget.removeCls("TrueFontWeightExpressionDeclineAttribute");
    }
}

Ext.define('VT.ux.SPGridComponent', {
    extend: 'Ext.Container',
    alias: 'widget.spgridcomponentPalletManual',
    defaultBindProperty: 'value',
    ownerWidgetGrid: '',

    viewModel: 'vm-spviewmodelPalletManual',

    setValue: function () {
        this.getViewModel().set('record', this.getWidgetRecord());
    },

    layout: {
        type: "absolute"
    },
    setOwnerGrid: function (view) {
        var widgetGrid = view;
        widgetGrid = widgetGrid.ownerCmp
        if (widgetGrid) {
            widgetGrid = widgetGrid.ownerGrid;
            if (widgetGrid) {
                this.ownerWidgetGrid = widgetGrid.ownerGrid;
            }
        }
    },
    getOwnerGrid: function () {
        return this.ownerWidgetGrid;
    },

    height: 50,
    items: [
        {
            xtype: "label",
            x: 0,
            y: 0,
            docked: "right",
            style: "background:lightgrey",
            width: "30px",
            cls: "WidgetColumnLabelRowNoCssClass",
            height: "50px",
            bind: {
                text: '{row_no}'
            },
            dataIndex: 'row_no',
        },
        {
            xtype: "sptextfield",
            x: 34,
            y: 0,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "121px",
            height: "22px",
            bind: {
                value: '{barcode}'
            },
            dataIndex: 'barcode',
        }, {
            xtype: "sptextfield",
            x: 157,
            y: 0,
            fieldStyle: "background:lightgrey",
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "164px",
            height: "24px",
            cls: "WidgetColumnDisableCssClass",
            readOnly: true,
            bind: {
                value: '{material_name}'
            },
            dataIndex: 'material_name',
        }, {
            xtype: "label",
            x: 34,
            y: 25,
            style: "background: lightblue",
            width: "23px",
            height: "24px",
            text: "מ",
            cls: "WidgetColumnLabelCssClass"
        }, {
            xtype: "sptextfield",
            x: 56,
            y: 25,
            width: "34px",
            height: "22px",
            cls: "WidgetColumnDisableCssClass",
            bind: {
                value: '{expected_material_quantity}',
            },
            dataIndex: 'expected_material_quantity',
        },
        , {
            xtype: "label",
            x: 95,
            y: 25,
            style: "background: lightblue",
            cls: "WidgetColumnLabelCssClass",
            width: "34px",
            height: "24px",
            text: "ח"
        }, {
            xtype: "sptextfield",
            x: 121,
            y: 25,
            width: "34px",
            height: "22px",
            readOnly: true,
            dataIndex: "invoice_pack_quantity",
            bind: {
                value: '{invoice_pack_quantity}',
            },
        }, {
            xtype: "label",
            x: 158,
            y: 25,
            cls: "WidgetColumnLabelCssClass",
            width: "26px",
            height: "24px",
            text: "מס"
        }, {
            xtype: "sptextfield",
            x: 230,
            y: 25,
            width: "98px",
            height: "22px",
            readOnly: true,
            hidden: true,
            dataIndex: "doc_no_en",
            bind: {
                value: '{doc_no_en}',
            },
            dataIndex: "doc_no_en",
        }, {
            xtype: "gridMultiComboBox",
            x: 186,
            y: 25,
            fieldStyle: "background:lightgrey",
            dataIndex: "invoice_number",
            style: "background: lightgrey",
            width: "135px",
            height: "22px",
            bind: {
                value: '{doc_no}',
            },
            dataIndex: "doc_no",
            cls: "WidgetColumnDisableCssClass"
        }
    ],
    listeners: {
        afterrender: function (view, eOpts) {
            this.setOwnerGrid(view);
        },
    }
});
