﻿
Ext.define('VT.ux.SPTextFieldComponent', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.sptextfield',
    value: '',
    dataIndex: '',
    enableKeyEvents: true,
    isInit: true,
    // Remove spinner buttons, and arrow key and mouse wheel listeners
    hideTrigger: true,
    keyNavEnabled: false,
    mouseWheelEnabled: false,
    inputType: 'number',
    listeners: {
        blur: function (view, e, o) {
            var grid = this.ownerCt.getOwnerGrid();
            try {
                if (grid && !grid.bufferedRenderer.refreshing && !grid.FocusingRow && view.preventRaisewidgetControlFocusEnter != true) {
                    view.preventRaisewidgetControlItemChange = true;
                    if (view.ownerCt.getWidgetRecord().get(view.dataIndex) != view.getValue()) {
                        var rowIndex = grid.getView().indexOf(this.el.up('table'));
                        var event = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                        // Listen for the event.
                        elem = grid.el.dom;
                        elem.addEventListener('widgetControlItemChanged', function (e) {
                            e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                            this.removeEventListener('widgetControlItemChanged', arguments.callee)
                        }, false);
                        // Dispatch the event.
                        elem.dispatchEvent(event);

                        view.preventRaisewidgetControlItemChange = false;
                        view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                        view.resetOriginalValue();
                    }
                }
            } catch (err) {
                console.log("error :- text box blur event : " + err.message)
            }
        },
        focus: function (view, event, eOpts) {
            if (this.inputType == 'number') {
                this.inputEl.dom.setSelectionRange = null;
            }
            var grid = this.ownerCt.getOwnerGrid();
            try {
                if (grid && view.preventRaisewidgetControlFocusEnter != true) {
                    var rowIndex = grid.getView().indexOf(this.el.up('table'));
                    grid.fireEvent('widgetControlFocusEnter', grid, rowIndex, view.getValue(), view.dataIndex);
                }
            } catch (err) {
                console.log("error :- text box focus event : " + err.message)
            }
        },
        specialkey: function (view, event, eOpts) {
            var grid = this.ownerCt.getOwnerGrid();
            try {
                if (grid) {
                    if (event.keyCode == 13 || event.keyCode == 9) {
                        if (view.ownerCt.getWidgetRecord().get(view.dataIndex) != view.getValue() && view.preventRaisewidgetControlFocusEnter != true) {
                            //Raise  widgetControlItemChanged
                            try {
                                view.preventRaisewidgetControlItemChange = true;
                                var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                var event1 = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                                // Listen for the event.
                                elem = grid.el.dom;
                                elem.addEventListener('widgetControlItemChanged', function (e) {
                                    e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                                    this.removeEventListener('widgetControlItemChanged', arguments.callee)
                                }, false);

                                // Dispatch the event.
                                elem.dispatchEvent(event1);

                            } catch (err) {
                                console.log("error :- specialkey - Raise widgetControlItemChanged : " + err.message)
                            }
                            view.preventRaisewidgetControlItemChange = false;
                            view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                            view.resetOriginalValue();
                        }

                        //Raise widgetControlSpecialKey event
                        try {
                            setTimeout(function (grid, event, view) {
                                var rowIndex = grid.getView().indexOf(view.el.up('table'));
                                var cellIndex = 0;
                                var key = event.keyCode;
                                var dataMember = view.dataIndex;
                                grid.fireEvent('widgetControlSpecialKey', grid, null, cellIndex, null, null, rowIndex, { getKey: function () { return key; } }, dataMember);
                            }, 100, grid, event, view);
                        } catch (err2) {
                            console.log("error :- specialkey - Raise widgetControlSpecialKey : " + err.message)
                        }
                    }
                }
            } catch (err) {
                console.log("error :- text box keydown event : " + err.message)
            }
        },
        afterrender: function (view, eOpts) {
            view.getEl().on('dblclick', function () {
                var ownGrid = this.component.ownerCt.ownerCmp.ownerGrid;
                var _rawValue = this.component.rawValue;
                var _dataIndex = this.component.dataIndex;
                ownGrid.fireEvent('widgetControlDoubleClick', ownGrid, _rawValue, _rawValue, _dataIndex)
            });
            view.on('change', function (view, newValue, oldValue, eOpts) {
                try {
                    var grid = this.ownerCt.getOwnerGrid();
                    if (grid && this.hasFocus) {
                        var _rawValue = this.rawValue;
                        var _dataIndex = this.dataIndex;
                        if (view.ownerCt.getWidgetRecord().get(_dataIndex) != newValue) {
                            grid.fireEvent('widgetControlEditChanged', grid, this.getValue(), this.getValue(), this.dataIndex);
                        }
                    }
                } catch (err) {
                    console.log("error :- text box change event : " + err.message)
                }
            });
        }
    },
    setDataIndex: function (value) {
        this.dataIndex = value;
    },
    getDataIndex: function () {
        return this.dataIndex;
    }
});