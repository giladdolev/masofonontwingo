﻿Ext.define('VT.ux.ListView', {
    extend: 'Ext.grid.Panel',
    xtype: 'vtlistview',
   
    setMultiSelect: function (isMulty) {
        var selModel = this.getSelectionModel();
        selModel && selModel.setSelectionMode && selModel.setSelectionMode(isMulty ? 'MULTI' : 'SINGLE');
    },
    setCellForeColor: function (color, rowIdx, colIdx) {
        var cell = this.view.getCellByPosition({ row: rowIdx, column: colIdx });
        if (cell != null) {
            cell.setStyle('color', color);
        }
    },

    onFocusLeave: function(e)
    {
        this.setSelection(null);
    },

    afterRender: function () {
        var me = this;

        me.callParent();
        me.setMultiSelect(me.multiSelect);
    },

    getSelectedIndexes: function () {
        var result = [],
            me = this,
            selModel = me.getSelectionModel(),
            selection = selModel && selModel.getSelection(),
            store = me.getStore();
        if (selection && store) {
            Ext.each(selection, function (item) {
                var index = store.indexOf(item);
                if (index >= 0) {
                    result.push(index);
                }
            });
        }

        return result;
    },

 
});