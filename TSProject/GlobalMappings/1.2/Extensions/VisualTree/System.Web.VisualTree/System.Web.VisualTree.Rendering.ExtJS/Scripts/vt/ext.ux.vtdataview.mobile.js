﻿/**
* This class provides support for VT data view
*/
Ext.define('VT.ux.DataView', {
    extend: 'Ext.carousel.Carousel',
    alias: 'widget.vtdataview',
    layout: 'fit',

    // Handles initialization and creates items configuration from component configuration.
    initComponent: function () {
        me.items = [{ xtype: 'button', text: 'ddd' }];

        // Call the parent implementation
        me.callParent();
    },

    getStore: function () {
        return null;
    }

});
