﻿/**
 * InputTextMask script used for mask/regexp operations.
 * Mask Individual Character Usage:
 * 9 - designates only numeric values
 * L - designates only uppercase letter values
 * l - designates only lowercase letter values
 * A - designates only alphanumeric values
 * X - denotes that a custom client script regular expression is specified</li>
 * All other characters are assumed to be "special" characters used to mask the input component.
 * Example 1:
 * (999)999-9999 only numeric values can be entered where the the character
 * position value is 9. Parenthesis and dash are non-editable/mask characters.
 * Example 2:
 * 99L-ll-X[^A-C]X only numeric values for the first two characters,
 * uppercase values for the third character, lowercase letters for the
 * fifth/sixth characters, and the last character X[^A-C]X together counts
 * as the eighth character regular expression that would allow all characters
 * but "A", "B", and "C". Dashes outside the regular expression are non-editable/mask characters.
 * @constructor
 * @param (String) mask The InputTextMask
 * @param (boolean) clearWhenInvalid True to clear the mask when the field blurs and the text is invalid. Optional, default is true.
 */

Ext.define('Ux.InputTextMask', {
    extend: 'Ext.plugin.Abstract',
    constructor: function (mask, clearWhenInvalid) {
        var me = this;

        me.clearWhenInvalid = Ext.isDefined(clearWhenInvalid) ? clearWhenInvalid : false;
        me.setMask(mask || '');

        return me;
    },
    alias: 'plugin.maskedinputplugin',
    init: function (field) {
        var me = this;

        me.field = field;
        me.newValue = "";

        if (field.rendered) {
            me.assignEl();
        } else {
            field.on('render', me.assignEl, me);
        }

        field.on('blur', me.removeValueWhenInvalid, me);
        field.on('focus', me.processMaskFocus, me);

    },

    setMask: function (mask) {
        var me = this,
			mai = 0,
			regexp = '',
			PROMPT_CHAR = (me.field && me.field.promptChar) || '_';

        me.rawMask = mask;
        me.viewMask = '';
        me.maskArray = [];

        if (mask != null) {

            for (var i = 0; i < mask.length; i++) {
                var iChar = mask.charAt(i);
                if (regexp) {
                    if (regexp == 'X') {
                        regexp = '';
                    }
                    if (iChar == 'X') {
                        me.maskArray[mai] = regexp;
                        mai++;
                        regexp = '';
                    } else {
                        regexp += iChar;
                    }
                } else if (iChar == 'X') {
                    regexp += 'X';
                    me.viewMask += PROMPT_CHAR;
                } else if (iChar == '#' || iChar == '9' || iChar == 'L' || iChar == 'l' || iChar == 'A' || iChar == '0') {
                    me.viewMask += PROMPT_CHAR;
                    me.maskArray[mai] = iChar;
                    mai++;
                } else {
                    me.viewMask += iChar;
                    me.maskArray[mai] = RegExp.escape(iChar);
                    mai++;
                }
            }
        }

        me.specialChars = me.viewMask.replace(new RegExp('(L|l|9|A|0|#|' + PROMPT_CHAR + '|X)', 'g'), '');

        me.inputTextElement && me.processMaskFocus();
    },

    assignEl: function () {
        var me = this,
            fieldInputEl = me.field.inputEl;

        me.inputTextElement = fieldInputEl.dom;
        fieldInputEl.on('keypress', me.processKeyPress, me);
        fieldInputEl.on('keydown', me.processKeyDown, me);

        if (Ext.isSafari || Ext.isIE) {
            fieldInputEl.on('paste', me.startTask, me);
            fieldInputEl.on('cut', me.startTask, me);
        }
        if (Ext.isGecko || Ext.isOpera) {
            fieldInputEl.on('mousedown', me.setPreviousValue, me);
        }
        if (Ext.isGecko) {
            fieldInputEl.on('input', me.onInput, me);
        }
        if (Ext.isOpera) {
            fieldInputEl.on('input', me.onInputOpera, me);
        }

        fieldInputEl.on('click', me.processClick, me);
    },

    onInput: function () {
        this.startTask(false);
    },

    onInputOpera: function () {
        var me = this;
        if (!me.prevValueOpera) {
            me.startTask(false);
        } else {
            me.manageBackspaceAndDeleteOpera();
        }
    },

    manageBackspaceAndDeleteOpera: function () {
        var me = this;
        me.inputTextElement.value = me.prevValueOpera.cursorPos.previousValue;
        me.manageTheText(me.prevValueOpera.keycode, me.prevValueOpera.cursorPos);
        me.prevValueOpera = null;
    },

    setPreviousValue: function (event) {
        this.oldCursorPos = this.getCursorPosition();
    },

    getValidatedKey: function (keycode, cursorPosition) {
        var pressedKey = keycode.pressedKey,
			maskKey = this.maskArray[cursorPosition.start];
        switch (maskKey) {
            case '0':
                return pressedKey.match(/[0-9]/);
            case '9':
                return pressedKey.match(/[0-9\s]/);
            case '#':
                return pressedKey.match(/[0-9\s+-]/);
            case 'L':
                return (pressedKey.match(/[A-Za-z]/)) ? pressedKey.toUpperCase() : null;
            case 'l':
                return (pressedKey.match(/[A-Za-z]/)) ? pressedKey.toLowerCase() : null;
            case 'A':
                return pressedKey.match(/[A-Za-z0-9]/);
            default:
                if (maskKey) {
                    return (keycode.pressedKey.match(new RegExp(maskKey)));
                }

        }

        return (null);
    },

    removeValueWhenInvalid: function () {
        var me = this,
            PROMPT_CHAR = (me.field && me.field.promptChar) || '_';

        if (me.clearWhenInvalid && me.inputTextElement.value.indexOf(PROMPT_CHAR) > -1) {
            me.inputTextElement.value = '';
        }
    },

    managePaste: function () {

        if (this.oldCursorPos == null) {
            return;
        }
        var valuePasted = this.inputTextElement.value.substring(this.oldCursorPos.start, this.inputTextElement.value.length - (this.oldCursorPos.previousValue.length - this.oldCursorPos.end));
        if (this.oldCursorPos.start < this.oldCursorPos.end) {
            this.oldCursorPos.previousValue =
			this.oldCursorPos.previousValue.substring(0, this.oldCursorPos.start) +
			this.viewMask.substring(this.oldCursorPos.start, this.oldCursorPos.end) +
			this.oldCursorPos.previousValue.substring(this.oldCursorPos.end, this.oldCursorPos.previousValue.length);
            valuePasted = valuePasted.substr(0, this.oldCursorPos.end - this.oldCursorPos.start);
        }
        this.inputTextElement.value = this.oldCursorPos.previousValue;
        var keycode = {
            unicode: '',
            isShiftPressed: false,
            isTab: false,
            isBackspace: false,
            isLeftOrRightArrow: false,
            isDelete: false,
            pressedKey: ''
        }
        var charOk = false;
        for (var i = 0; i < valuePasted.length; i++) {
            keycode.pressedKey = valuePasted.substr(i, 1);
            keycode.unicode = valuePasted.charCodeAt(i);
            this.oldCursorPos = this.skipMaskCharacters(keycode, this.oldCursorPos);
            if (this.oldCursorPos === false) {
                break;
            }
            if (this.injectValue(keycode, this.oldCursorPos)) {
                charOk = true;
                this.moveCursorToPosition(keycode, this.oldCursorPos);
                this.oldCursorPos.previousValue = this.inputTextElement.value;
                this.oldCursorPos.start = this.oldCursorPos.start + 1;
            }
        }
        if (!charOk && this.oldCursorPos !== false) {
            this.moveCursorToPosition(null, this.oldCursorPos);
        }
        this.oldCursorPos = null;
    },

    processKeyDown: function (e) {
        this.processMaskFormatting(e, 'keydown');
    },

    processKeyPress: function (e) {
        this.processMaskFormatting(e, 'keypress');
    },

    startTask: function (setOldCursor) {
        var me = this;

        if (me.task == undefined) {
            me.task = new Ext.util.DelayedTask(me.managePaste, me);
        }
        if (setOldCursor !== false) {
            me.oldCursorPos = me.getCursorPosition();
        }
        me.task.delay(0);
    },

    skipMaskCharacters: function (keycode, cursorPos) {
        if (cursorPos != null) {
            if (cursorPos.start != cursorPos.end && (keycode.isDelete || keycode.isBackspace)) {
                return (cursorPos);
            }

            while (this.specialChars.match(RegExp.escape(this.viewMask.charAt(((keycode.isBackspace) ? cursorPos.start - 1 : cursorPos.start))))) {
                if (keycode.isBackspace) {
                    cursorPos.dec();
                } else {
                    cursorPos.inc();
                }
                if (cursorPos.start >= cursorPos.previousValue.length || cursorPos.start < 0) {
                    return false;
                }
            }
            return (cursorPos);
        }
    },

    isManagedByKeyDown: function (keycode) {
        if (keycode.isDelete || keycode.isBackspace) {
            return (true);
        }
        return (false);
    },

    processMaskFormatting: function (e, type) {
        if (!this.cmp.inputMask) {
            return (this.manageTheText(keycode, cursorPos));
        }
        this.oldCursorPos = null;
        var cursorPos = this.getCursorPosition(),
			keycode = this.getKeyCode(e, type),
			keyUnicode = keycode.unicode;

        if (keyUnicode == 0) {//?? sometimes on Safari
            return;
        }
        if ((keyUnicode == 67 || keyUnicode == 99) && e.ctrlKey) {//Ctrl+c, let's the browser manage it!
            return;
        }
        if ((keycode.unicode == 88 || keyUnicode == 120) && e.ctrlKey) {//Ctrl+x, manage paste
            this.startTask();
            return;
        }
        if ((keyUnicode == 86 || keyUnicode == 118) && e.ctrlKey) {//Ctrl+v, manage paste....
            this.startTask();
            return;
        }
        if ((keycode.isBackspace || keycode.isDelete) && Ext.isOpera) {
            this.prevValueOpera = { cursorPos: cursorPos, keycode: keycode };
            return;
        }
        if (type == 'keydown' && !this.isManagedByKeyDown(keycode)) {
            return true;
        }
        if (type == 'keypress' && this.isManagedByKeyDown(keycode)) {
            return true;
        }
        if (this.handleEventBubble(e, keycode, type)) {
            return true;
        }
        return (this.manageTheText(keycode, cursorPos));
    },

    manageTheText: function (keycode, cursorPos) {
        var me = this;
        if (me.inputTextElement.value.length === 0) {
            me.inputTextElement.value = me.viewMask;
        }
        cursorPos = me.skipMaskCharacters(keycode, cursorPos);
        if (cursorPos === false) {
            return false;
        }
        if (me.injectValue(keycode, cursorPos)) {
            me.moveCursorToPosition(keycode, cursorPos);
        }
        return (false);
    },

    processMaskFocus: function () {
        var me = this;
        if (me.inputTextElement.value.length == 0) {
            var cursorPos = me.getCursorPosition();
            me.inputTextElement.value = me.viewMask;
            me.moveCursorToPosition(null, cursorPos);
        }
    },

    isManagedByBrowser: function (keyEvent, keycode, type) {
        var eventObject = Ext.EventObject,
			keyList = [
				eventObject.TAB,
				eventObject.RETURN,
				eventObject.ENTER,
				eventObject.SHIFT,
				eventObject.CONTROL,
				eventObject.ESC,
				eventObject.PAGEUP,
				eventObject.PAGEDOWN,
				eventObject.END,
				eventObject.HOME,
				eventObject.LEFT,
				eventObject.UP,
				eventObject.RIGHT,
				eventObject.DOWN
			];

        if (((type == 'keypress' && keyEvent.charCode === 0) || type == 'keydown') && Ext.Array.contains(keyList, keycode.unicode)) {
            return (true);
        }
        return (false);
    },

    handleEventBubble: function (keyEvent, keycode, type) {
        try {
            if (keycode && this.isManagedByBrowser(keyEvent, keycode, type)) {
                return true;
            }
            keyEvent.stopEvent();
            return false;
        } catch (e) {
            alert(e.message);
        }
    },

    getCursorPosition: function () {
        var s, e, r,
            me = this,
            inputTextElement = me.inputTextElement;

        if (inputTextElement.createTextRange && document.selection) {
            if (document.selection) {
                r = document.selection.createRange().duplicate();
                r = me.getRange();
                r.moveEnd('character', this.inputTextElement.value.length);
                if (r.text === '') {
                    s = inputTextElement.value.length;
                } else {
                    s = inputTextElement.value.lastIndexOf(r.text);
                }
                r = me.getRange();
                r.moveStart('character', -inputTextElement.value.length);
                e = r.text.length;
            }

        } else {
            s = inputTextElement.selectionStart;
            e = inputTextElement.selectionEnd;
        }
        return me.CursorPosition(s, e, r, inputTextElement.value);
    },

    moveCursorToPosition: function (keycode, cursorPosition) {
        var p = (!keycode || (keycode && keycode.isBackspace)) ? cursorPosition.start : cursorPosition.start + 1;
        if (this.inputTextElement.createTextRange && document.selection) {
            cursorPosition.range.move('character', p);
            cursorPosition.range.select();
        } else {
            this.inputTextElement.selectionStart = p;
            this.inputTextElement.selectionEnd = p;
        }
    },

    injectValue: function (keycode, cursorPosition) {
        if (keycode == null) {
            return;
        }
        var key;
        var cur = -1;
        if (!keycode.isDelete && !keycode.isBackspace) {
            key = this.getValidatedKey(keycode, cursorPosition);
            // if text selected 
            if (cursorPosition.start < cursorPosition.end) {
                // count the selectd chars 
                var def = cursorPosition.end - cursorPosition.start;
                for (i = 1 ; i < def ; i++) {
                    // replace them with the promptChar
                    key += this.field.promptChar;
                }
            }
        } else {
            if (cursorPosition.start == cursorPosition.end) {
                key = '_';
                cur = cursorPosition.start;
                while (this.isMaskChar(this.inputTextElement.value[cur]) && cur < this.viewMask.length) {
                    // count available fields in the masked from current position .
                    cur++;
                }

                if (keycode.isBackspace) {
                    cursorPosition.dec();
                }
            } else {
                key = this.viewMask.substring(cursorPosition.start, cursorPosition.end);
            }
        }
        if (key) {
            //check if the masked text is full 
            var canPush = this.CanPushToInputTextElement(cursorPosition.previousValue);
            if (canPush == false && key.length > 1) {
                // replace selected text with the key.
                cursorPosition.previousValue = cursorPosition.previousValue.substring(0, cursorPosition.start)
               + key +
               cursorPosition.previousValue.substring(cursorPosition.start + key.length, cursorPosition.previousValue.length);
            }
            // do again CanPushToInputTextElement method 
            // to arrange the masked text box values.
            canPush = this.CanPushToInputTextElement(cursorPosition.previousValue);
            if (canPush || cur != -1) {
                // how many chars we need to push 
                var needToPush = cursorPosition.previousValue.substring(cursorPosition.start, cursorPosition.previousValue.length);
                // get the rest of the  mask . 
                var viewMaskTemp = this.viewMask.substring(cursorPosition.start, cursorPosition.previousValue.length);
                this.inputTextElement.value = cursorPosition.previousValue.substring(0, cursorPosition.start)
                + key +
                cursorPosition.previousValue.substring(cursorPosition.start + key.length, cursorPosition.previousValue.length);
                // if the mask not equal the prevoius value 
                if (!this.isMaskEqualPrevious(viewMaskTemp, needToPush)) {
                    // get difference chars 
                    var def = cursorPosition.end - cursorPosition.start;
                    // create the str the new pushed chars.
                    var str = this.getNeedToPuchCharacteres(needToPush, viewMaskTemp, cur, def)
                    // push the chars .
                    this.pushCharacter(str, cursorPosition.start + 1, viewMaskTemp, cur, def);
                }
                if (keycode.isDelete) {
                    cursorPosition.dec();
                }
                return true;
            } else {
                if (this.viewMask.length == 1 ) {
                    var def = cursorPosition.end - cursorPosition.start;
                    if (def == 1 || def == 0) {
                        // push the chars .
                        this.inputTextElement.value = key;
                    }
                }
            }
        }
        return false;
    },

    CanPushToInputTextElement: function (maskValue) {
        for (i = maskValue.length ; i >= 0 ; i--) {
            if (maskValue[i] === this.field.promptChar) {
                return true;
            }
        }
        return false;
    },

    getKeyCode: function (onKeyDownEvent, type) {
        var keycode = {},
			DELETE_CODE = Ext.EventObject.DELETE;
        keycode.unicode = onKeyDownEvent.getKey();
        keycode.isShiftPressed = onKeyDownEvent.shiftKey;

        keycode.isDelete = ((onKeyDownEvent.getKey() == DELETE_CODE && type == 'keydown') || (type == 'keypress' && onKeyDownEvent.charCode === 0 && onKeyDownEvent.keyCode == DELETE_CODE));
        keycode.isTab = (onKeyDownEvent.getKey() == Ext.EventObject.TAB);
        keycode.isBackspace = (onKeyDownEvent.getKey() == Ext.EventObject.BACKSPACE);
        keycode.isLeftOrRightArrow = (onKeyDownEvent.getKey() == Ext.EventObject.LEFT || onKeyDownEvent.getKey() == Ext.EventObject.RIGHT);
        keycode.pressedKey = String.fromCharCode(keycode.unicode);
        return (keycode);
    },

    CursorPosition: function (start, end, range, previousValue) {
        var cursorPosition = {};
        cursorPosition.start = isNaN(start) ? 0 : start;
        cursorPosition.end = isNaN(end) ? 0 : end;
        cursorPosition.range = range;
        cursorPosition.previousValue = previousValue;
        cursorPosition.inc = function () { cursorPosition.start++; cursorPosition.end++; };
        cursorPosition.dec = function () { cursorPosition.start--; cursorPosition.end--; };
        return (cursorPosition);
    },

    processClick: function (snd) {
        if (this.isMaskValue()) {
            this.moveCursorToStart();
        }
    },

    isMaskValue: function () {
        return this.inputTextElement.value === this.viewMask;
    },

    moveCursorToStart: function () {
        var me = this;

        if (me.inputTextElement.createTextRange && document.selection) {
            cursorPosition.range.move('character', 0);
            cursorPosition.range.select();
        } else {
            me.inputTextElement.selectionStart = 0;
            me.inputTextElement.selectionEnd = 0;
        }

        me.inputTextElement.focus();
    },

    destroy: function () {
        var me = this,
            field = me.field,
            fieldInputEl = field.inputEl;

        me.inputTextElement = undefined;
        me.field = undefined;

        fieldInputEl.un('keypress', me.processKeyPress, me);
        fieldInputEl.un('keydown', me.processKeyDown, me);

        if (Ext.isSafari || Ext.isIE) {
            fieldInputEl.un('paste', me.startTask, me);
            fieldInputEl.un('cut', me.startTask, me);
        }
        if (Ext.isGecko || Ext.isOpera) {
            fieldInputEl.un('mousedown', me.setPreviousValue, me);
        }
        if (Ext.isGecko) {
            fieldInputEl.un('input', me.onInput, me);
        }
        if (Ext.isOpera) {
            fieldInputEl.un('input', me.onInputOpera, me);
        }

        fieldInputEl.un('click', me.processClick, me);

        field.un('render', me.assignEl, me);
        field.un('blur', me.removeValueWhenInvalid, me);
        field.un('focus', me.processMaskFocus, me);
    },

    isMaskEqualPrevious: function (Previous, temp) {
        if (Previous.length === temp.length) {
            return Previous === temp;
        }
        return false;
    },

    isMaskChar: function (char) {
        for (i = 0 ; i < this.viewMask.length; i++) {
            if (char === this.viewMask[i])
                return true;
        }
        return false;
    },

    getNeedToPuchCharacteres: function (str, viewMaskTemp, cur, def) {
        var newStr = "";
        var inedx = 0;
        if (cur != -1) {
            inedx = 1;
        }
        if (def > 0) {
            inedx = def;
        }
        for (i = inedx ; i < str.length ; i++) {
            if (str[i] != this.field.promptChar) {
                if (viewMaskTemp[i] != str[i]) {
                    newStr += str[i];
                }
            }
        }
        return newStr;
    },

    numberOfDeffrence: function (Previous, temp) {
        var counter = Previous.length;
        if (Previous.length === temp.length) {
            for (i = 0 ; i < Previous.length ; i++) {
                if (Previous[i] === temp[i]) {
                    counter--;
                }
            }
        }
        return counter;
    },

    pushCharacter: function (str, cursorPosition, viewMaskTemp, cur, def) {
        var j = 0;
        this.newValue = "";
        var length = str.length;
        var index = 0;

        for (j = 0; j, j < cursorPosition ; j++) {
            this.newValue += this.inputTextElement.value[j];

        }
        if (this.newValue[--j] == this.field.promptChar) {
            this.newValue = this.newValue.substring(0, j);
            cursorPosition -= 1;
        }
        var mask = this.viewMask.substring(cursorPosition, this.viewMask.length);
        for (i = 0 ; i < mask.length ; i++) {
            if (mask[i] === this.field.promptChar) {
                if (index < length) {
                    this.newValue += str[index++];
                } else {
                    this.newValue += mask[i];
                }
            } else {
                this.newValue += mask[i];
            }
        }
        this.inputTextElement.value = this.newValue
    }
});

Ext.applyIf(RegExp, {
    escape: function (str) {
        return new String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
    }
});


Ext.define('VT.ux.MaskedTextBox', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.maskedinput',
    plugins: [{
        ptype: 'maskedinputplugin'
    }],
    setMask: function (mask) {
        var me = this,
            plugin;

        plugin = me.findPlugin('maskedinputplugin');
        plugin && plugin.setMask(mask);
        me.inputMask = mask;
    },

    afterRender: function () {
        var me = this;

        me.callParent();
        if (me.inputMask != "") {
            me.setMask(me.inputMask);
        }
    }

})