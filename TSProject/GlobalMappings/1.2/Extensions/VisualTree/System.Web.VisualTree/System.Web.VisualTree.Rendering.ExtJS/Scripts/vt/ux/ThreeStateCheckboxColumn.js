﻿
Ext.define('VT.ux.ThreeStateCheckboxColumn', {
    extend: 'Ext.grid.column.Check',
    alias: 'widget.threeStateCheckboxColumn',

    constructor: function () {
        this.callParent(arguments);
    },
    threeState: false,
    checkState: 0,
    checkboxCheckedCls: ['', Ext.baseCSSPrefix + 'grid-checkcolumn-checked', 'x-checkboxColumn-dash'],

    getThreeState: function () {
        return this.threeState;
    },

    setThreeState: function (state) {
        this.threeState = state;
        this.getEditor().setAllowNull(state);
    },

    getCheckState: function () {
        return this.checkState;
    },

    setCheckState: function (state) {
        this.checkState = state;
    },


    defaultRenderer: function (value, cellValues) {
        var me = this,
            cls = me.checkboxCls,
            tip = me.tooltip;

        me.setCheckState( me.convertValueToInt(value) );

        if (me.invert) {
            if (value != null) {
                value = !value;
                me.setCheckState( me.convertValueToInt(value) );

            }
        }
        if (me.disabled) {
            cellValues.tdCls += ' ' + me.disabledCls;
        }


        cls += ' ' + me.checkboxCheckedCls[me.getCheckState()];
        tip = me.checkedTooltip || tip;


        if (me.useAriaElements) {
            cellValues.tdAttr += ' aria-describedby="' + me.id + '-cell-description' +
                                 (me.getThreeState() && me.getCheckState() == 3 ? '-undetermined"' : ((value ? '-not' : '') + '-selected"'));
        }

        // This will update the header state on the next animation frame 
        // after all rows have been rendered. 
        me.updateHeaderState();

        return '<span ' + (tip || '') + ' class="' + cls + '" role="' + me.checkboxAriaRole + '"' +
                (!me.ariaStaticRoles[me.checkboxAriaRole] ? ' tabIndex="0"' : '') +
               '></span>';
    },

    updater: function (cell, PrevValue, value) {
        var me = this,
            tip = me.tooltip;

        if (me.invert) {
            if (value != null) {
                value = !value;
            }
        }
        if (value) {
            tip = me.checkedTooltip || tip;
        }
        if (tip) {
            cell.setAttribute('data-qtip', tip);
        } else {
            cell.removeAttribute('data-qtip');
        }
        cell = Ext.fly(cell);

        if (me.useAriaElements) {
            me.updateCellAriaDescription(null, value, cell);
        }

        cell[me.disabled ? 'addCls' : 'removeCls'](me.disabledCls);
        if (me.getThreeState()) {
            Ext.fly(cell.down(me.getView().innerSelector, true).firstChild).removeCls(Ext.baseCSSPrefix + me.checkboxCheckedCls[me.convertValueToInt(PrevValue)]);
            Ext.fly(cell.down(me.getView().innerSelector, true).firstChild).addCls(Ext.baseCSSPrefix + me.checkboxCheckedCls[me.convertValueToInt(value)]);
        }
        else {
            Ext.fly(cell.down(me.getView().innerSelector, true).firstChild)[value ? 'addCls' : 'removeCls'](Ext.baseCSSPrefix + 'grid-checkcolumn-checked');
        }


        // This will update the header state on the next animation frame 
        // after all rows have been updated. 
        me.updateHeaderState();
    },


    updateCellAriaDescription: function (record, isSelected, cell) { 
        var me = this;

        if (me.useAriaElements) {
            cell = cell || me.getView().getCell(record, me);

            if (cell) {
                cell.dom.setAttribute('aria-describedby', me.id + '-cell-description' +
                                        (me.getThreeState() && isSelected == null ? '-undetermined' : ((isSelected ? '-not' : '') + '-selected')));
            }
        }
    },


    processEvent: function (type, view, cell, recordIndex, cellIndex, e, record, row) {
        var me = this,
            key = type === 'keydown' && e.getKey(),
            isClick = type === me.triggerEvent,
            disabled = me.disabled,
			value = me.isRecordChecked(record),
            ret;

        me.setCheckState( me.convertValueToInt(value) ),

        // Flag event to tell SelectionModel not to process it. 
        e.stopSelection = !key && me.stopSelection;

        if (!disabled && (isClick || (key === e.ENTER || key === e.SPACE))) {
            me.setCheckState( me.getNextCheckState(me.getCheckState()) );
            value = me.convertValueToBool(me.getCheckState());
            // Allow apps to hook beforecheckchange 
            if (me.fireEvent('beforecheckchange', me, recordIndex, value, record, e) !== false) {

                me.setRecordCheck(record, recordIndex, value, cell, e);

                // Do not allow focus to follow from this mousedown unless the grid is already in actionable mode 
                if (isClick && !view.actionableMode) {
                    e.preventDefault();
                }
                if (me.hasListeners.checkchange) {
                    me.fireEvent('checkchange', me, recordIndex, value, record, e);
                }
            }
        } else {
            ret = me.callParent(arguments);
        }
        return ret;
    },

    setRecordCheck: function (record, recordIndex, value, cell) {
        var me = this,
            prop = me.property,
			prevValue,
            result;

        // Only proceed if we NEED to change 
        if (prop ? record[prop] : record.get(me.dataIndex) != value) {
            if (prop) {
                prevValue = record[prop];
                record[prop] = value;
                me.updater(cell, prevValue, value);
            } else {
                record.set(me.dataIndex, value);
            }
        }
    },


    convertValueToInt: function (value) {

        if (!this.invert) {
            switch (value) {
                case '0':
                case 'false':
                case false:
                    return 0;
                case '1':
                case 'true':
                case true:
                    return 1;
                case null:
                    return 2;
            }
        }
        else {
            switch (value) {
                case '0':
                case 'false':
                case false:
                    return 1;
                case '1':
                case 'true':
                case true:
                    return 0;
                case null:
                    return 2;
            }
        }
    },

    convertValueToBool: function (checkStateValue) {

        if (!this.invert) {
            switch (checkStateValue) {
                case 0:
                    return false;
                case 1:
                    return true;
                case 2:
                    return null;
            }
        }
        else {
            switch (checkStateValue) {
                case 0:
                    return true;
                case 1:
                    return false;
                case 2:
                    return null;
            }
        }
    },

    getNextCheckState: function (checkStateValue) {
        var lastState = this.getThreeState() ? 3 : 2;

        if (++checkStateValue == lastState) {
            checkStateValue = 0;
        }

        return checkStateValue;
    },

});






