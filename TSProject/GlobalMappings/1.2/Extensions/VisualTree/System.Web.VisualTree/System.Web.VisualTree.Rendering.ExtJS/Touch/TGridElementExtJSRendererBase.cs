using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{

	public class TGridElementExtJSRendererBase : GridElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.dataview.ListView";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "grid";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the ContextMenuClick event attach code.
			RenderAttachContextMenuClickEvent(context, visualElement, writer); 
			// Renders the CellDoubleClick event attach code.
			RenderAttachCellDoubleClickEvent(context, visualElement, writer); 
			// Renders the CellButtonClick event attach code.
			RenderAttachCellButtonClickEvent(context, visualElement, writer); 
			// Renders the SelectionChanged event attach code.
			RenderAttachSelectionChangedEvent(context, visualElement, writer); 
			// Renders the BeforeSelectionChanged event attach code.
			RenderAttachBeforeSelectionChangedEvent(context, visualElement, writer); 
			// Renders the ColumnHeaderMouseClick event attach code.
			RenderAttachColumnHeaderMouseClickEvent(context, visualElement, writer); 
			// Renders the CellContentClick event attach code.
			RenderAttachCellContentClickEvent(context, visualElement, writer); 
			// Renders the CellFormatting event attach code.
			RenderAttachCellFormattingEvent(context, visualElement, writer); 
			// Renders the ColumnStateChanged event attach code.
			RenderAttachColumnStateChangedEvent(context, visualElement, writer); 
			// Renders the DataBindingComplete event attach code.
			RenderAttachDataBindingCompleteEvent(context, visualElement, writer); 
			// Renders the AddedColumn event attach code.
			RenderAttachAddedColumnEvent(context, visualElement, writer); 
			// Renders the CurrentCellChanged event attach code.
			RenderAttachCurrentCellChangedEvent(context, visualElement, writer); 
			// Renders the CellBeginEdit event attach code.
			RenderAttachCellBeginEditEvent(context, visualElement, writer); 
			// Renders the CellClick event attach code.
			RenderAttachCellClickEvent(context, visualElement, writer); 
			// Renders the CellEndEdit event attach code.
			RenderAttachCellEndEditEvent(context, visualElement, writer); 
			// Renders the ColumnHeaderMouseRightClick event attach code.
			RenderAttachColumnHeaderMouseRightClickEvent(context, visualElement, writer); 
			// Renders the RowClick event attach code.
			RenderAttachRowClickEvent(context, visualElement, writer); 
			// Renders the CellMouseDown event attach code.
			RenderAttachCellMouseDownEvent(context, visualElement, writer); 
			// Renders the MouseRightClick event attach code.
			RenderAttachMouseRightClickEvent(context, visualElement, writer); 
			// Renders the BeforeContextMenuShow event attach code.
			RenderAttachBeforeContextMenuShowEvent(context, visualElement, writer); 
			// Renders the ColumnDisplayIndexChanged event attach code.
			RenderAttachColumnDisplayIndexChangedEvent(context, visualElement, writer); 
			// Renders the CellMouseUp event attach code.
			RenderAttachCellMouseUpEvent(context, visualElement, writer); 
			// Renders the RowHeaderMouseRightClick event attach code.
			RenderAttachRowHeaderMouseRightClickEvent(context, visualElement, writer); 
			// Renders the CancelEdit event attach code.
			RenderAttachCancelEditEvent(context, visualElement, writer); 
			// Renders the CellkeyDown event attach code.
			RenderAttachCellkeyDownEvent(context, visualElement, writer); 
			// Renders the ComboCellSelectedIndexChanged event attach code.
			RenderAttachComboCellSelectedIndexChangedEvent(context, visualElement, writer); 
			// Renders the ClientEvent event attach code.
			RenderAttachClientEventEvent(context, visualElement, writer); 
			// Renders the KeyDown event attach code.
			RenderAttachKeyDownEvent(context, visualElement, writer); 
			// Renders the dataChanged event attach code.
			RenderAttachdataChangedEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "ContextMenuClick":
					return this.ExtractContextMenuClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellDoubleClick":
					return this.ExtractCellDoubleClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellButtonClick":
					return this.ExtractCellButtonClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "SelectionChanged":
					return this.ExtractSelectionChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeSelectionChanged":
					return this.ExtractBeforeSelectionChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnHeaderMouseClick":
					return this.ExtractColumnHeaderMouseClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellContentClick":
					return this.ExtractCellContentClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellFormatting":
					return this.ExtractCellFormattingCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnStateChanged":
					return this.ExtractColumnStateChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DataBindingComplete":
					return this.ExtractDataBindingCompleteCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "AddedColumn":
					return this.ExtractAddedColumnCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CurrentCellChanged":
					return this.ExtractCurrentCellChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellBeginEdit":
					return this.ExtractCellBeginEditCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellClick":
					return this.ExtractCellClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellEndEdit":
					return this.ExtractCellEndEditCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnHeaderMouseRightClick":
					return this.ExtractColumnHeaderMouseRightClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "RowClick":
					return this.ExtractRowClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellMouseDown":
					return this.ExtractCellMouseDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseRightClick":
					return this.ExtractMouseRightClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeContextMenuShow":
					return this.ExtractBeforeContextMenuShowCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnDisplayIndexChanged":
					return this.ExtractColumnDisplayIndexChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellMouseUp":
					return this.ExtractCellMouseUpCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "RowHeaderMouseRightClick":
					return this.ExtractRowHeaderMouseRightClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CancelEdit":
					return this.ExtractCancelEditCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellkeyDown":
					return this.ExtractCellkeyDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ComboCellSelectedIndexChanged":
					return this.ExtractComboCellSelectedIndexChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ClientEvent":
					return this.ExtractClientEventCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "KeyDown":
					return this.ExtractKeyDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "dataChanged":
					return this.ExtractdataChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "ClientRowTemplate":
					this.RenderClientRowTemplatePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "UseFixedHeaders":
					this.RenderUseFixedHeadersPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "RowHeaderVisible":
					this.RenderRowHeaderVisiblePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SelectionMode":
					this.RenderSelectionModePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MultiSelect":
					this.RenderMultiSelectPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Content":
					this.RenderContentPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CanSelect":
					this.RenderCanSelectPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "FrozenCols":
					this.RenderFrozenColsPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnFilterInfo":
					this.RenderColumnFilterInfoPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SortableColumns":
					this.RenderSortableColumnsPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AllowColumnSelection":
					this.RenderAllowColumnSelectionPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "EnableLocking":
					this.RenderEnableLockingPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ShowAutoFilterColumnItem":
					this.RenderShowAutoFilterColumnItemPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AllowTrackMouseOver":
					this.RenderAllowTrackMouseOverPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SearchPanel":
					this.RenderSearchPanelPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnLines":
					this.RenderColumnLinesPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "RowLines":
					this.RenderRowLinesPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellBorderStyle":
					this.RenderCellBorderStylePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderClientRowTemplateProperty(renderingContext, visualElement, jsonWriter);
			this.RenderUseFixedHeadersProperty(renderingContext, visualElement, jsonWriter);
			this.RenderRowHeaderVisibleProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSelectionModeProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMultiSelectProperty(renderingContext, visualElement, jsonWriter);
			this.RenderContentProperty(renderingContext, visualElement, jsonWriter);
			this.RenderCanSelectProperty(renderingContext, visualElement, jsonWriter);
			this.RenderFrozenColsProperty(renderingContext, visualElement, jsonWriter);
			this.RenderColumnFilterInfoProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSortableColumnsProperty(renderingContext, visualElement, jsonWriter);
			this.RenderAllowColumnSelectionProperty(renderingContext, visualElement, jsonWriter);
			this.RenderEnableLockingProperty(renderingContext, visualElement, jsonWriter);
			this.RenderShowAutoFilterColumnItemProperty(renderingContext, visualElement, jsonWriter);
			this.RenderAllowTrackMouseOverProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSearchPanelProperty(renderingContext, visualElement, jsonWriter);
			this.RenderColumnLinesProperty(renderingContext, visualElement, jsonWriter);
			this.RenderRowLinesProperty(renderingContext, visualElement, jsonWriter);
			this.RenderCellBorderStyleProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the ContextMenuClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachContextMenuClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ContextMenuClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseContextMenuClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ContextMenuClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderContextMenuClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ContextMenuClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractContextMenuClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ContextMenuClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderContextMenuClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ContextMenuClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderContextMenuClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderCellDoubleClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseCellDoubleClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"list", "index"}, 
									() => {  
										// Create the server callback code.CellDoubleClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{selectedIndex : index}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("itemdoubletap", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of CellDoubleClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellDoubleClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellDoubleClickListeners);
		}


		/// <summary>
        /// Extracts the CellDoubleClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellDoubleClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridElementCellEventArgs(0, args.Value<System.Int32>("selectedIndex"));
		}


		/// <summary>
        /// Renders the CellDoubleClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellDoubleClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellDoubleClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellDoubleClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"list", "index"}, 
                            () => {  
										// Create the server callback code.CellDoubleClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{selectedIndex : index}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('itemdoubletap', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the CellDoubleClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellDoubleClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellDoubleClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "itemdoubletap");
            }
        }

		/// <summary>
        /// Renders the CellButtonClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellButtonClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellButtonClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellButtonClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellButtonClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellButtonClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellButtonClickListeners);
		}


		/// <summary>
        /// Extracts the CellButtonClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellButtonClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellButtonClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellButtonClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellButtonClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellButtonClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the SelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of SelectionChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseSelectionChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the SelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientSelectionChanged) || element.HasSelectionChangedListeners);
		}


		/// <summary>
        /// Extracts the SelectionChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractSelectionChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the SelectionChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the SelectionChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the BeforeSelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeSelectionChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeSelectionChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeSelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasBeforeSelectionChangedListeners);
		}


		/// <summary>
        /// Extracts the BeforeSelectionChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeSelectionChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeSelectionChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectionChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeSelectionChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectionChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ColumnHeaderMouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnHeaderMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ColumnHeaderMouseClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnHeaderMouseClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnHeaderMouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnHeaderMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ColumnHeaderMouseClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnHeaderMouseClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ColumnHeaderMouseClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ColumnHeaderMouseClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellContentClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellContentClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellContentClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellContentClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellContentClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellContentClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellContentClickListeners);
		}


		/// <summary>
        /// Extracts the CellContentClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellContentClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellContentClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellContentClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellContentClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellContentClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellFormatting event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellFormattingEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellFormatting event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellFormattingEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellFormatting event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellFormattingEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellFormattingListeners);
		}


		/// <summary>
        /// Extracts the CellFormatting event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellFormattingCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellFormatting event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellFormattingAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellFormatting event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellFormattingRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ColumnStateChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnStateChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ColumnStateChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnStateChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnStateChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnStateChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasColumnStateChangedListeners);
		}


		/// <summary>
        /// Extracts the ColumnStateChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnStateChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ColumnStateChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnStateChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ColumnStateChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnStateChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the DataBindingComplete event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDataBindingCompleteEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of DataBindingComplete event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDataBindingCompleteEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DataBindingComplete event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDataBindingCompleteEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasDataBindingCompleteListeners);
		}


		/// <summary>
        /// Extracts the DataBindingComplete event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDataBindingCompleteCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DataBindingComplete event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataBindingCompleteAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the DataBindingComplete event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataBindingCompleteRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the AddedColumn event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachAddedColumnEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of AddedColumn event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAddedColumnEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the AddedColumn event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderAddedColumnEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasAddedColumnListeners);
		}


		/// <summary>
        /// Extracts the AddedColumn event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractAddedColumnCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the AddedColumn event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAddedColumnAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the AddedColumn event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAddedColumnRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CurrentCellChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCurrentCellChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CurrentCellChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCurrentCellChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CurrentCellChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCurrentCellChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCurrentCellChangedListeners);
		}


		/// <summary>
        /// Extracts the CurrentCellChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCurrentCellChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CurrentCellChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCurrentCellChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CurrentCellChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCurrentCellChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellBeginEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellBeginEditEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellBeginEdit event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellBeginEditEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellBeginEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellBeginEditEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the CellBeginEdit event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellBeginEditCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellBeginEdit event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellBeginEditAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellBeginEdit event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellBeginEditRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the CellClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellEndEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellEndEditEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellEndEdit event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellEndEditEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellEndEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellEndEditEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientCellEndEdit) || element.HasCellEndEditListeners);
		}


		/// <summary>
        /// Extracts the CellEndEdit event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellEndEditCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellEndEdit event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellEndEditAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellEndEdit event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellEndEditRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ColumnHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ColumnHeaderMouseRightClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnHeaderMouseRightClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ColumnHeaderMouseRightClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnHeaderMouseRightClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ColumnHeaderMouseRightClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseRightClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ColumnHeaderMouseRightClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseRightClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the RowClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachRowClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of RowClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseRowClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the RowClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderRowClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasRowClickListeners);
		}


		/// <summary>
        /// Extracts the RowClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractRowClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the RowClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the RowClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellMouseDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellMouseDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellMouseDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellMouseDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientCellMouseDown));
		}


		/// <summary>
        /// Extracts the CellMouseDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellMouseDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellMouseDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellMouseDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the MouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseRightClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseRightClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasMouseRightClickListeners);
		}


		/// <summary>
        /// Extracts the MouseRightClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseRightClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseRightClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseRightClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseRightClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseRightClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the BeforeContextMenuShow event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeContextMenuShowEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeContextMenuShow event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeContextMenuShowEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeContextMenuShow event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeContextMenuShowEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasBeforeContextMenuShowListeners);
		}


		/// <summary>
        /// Extracts the BeforeContextMenuShow event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeContextMenuShowCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeContextMenuShow event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeContextMenuShowAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeContextMenuShow event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeContextMenuShowRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ColumnDisplayIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnDisplayIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ColumnDisplayIndexChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnDisplayIndexChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnDisplayIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnDisplayIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ColumnDisplayIndexChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnDisplayIndexChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ColumnDisplayIndexChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnDisplayIndexChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ColumnDisplayIndexChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnDisplayIndexChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellMouseUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellMouseUpEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellMouseUp event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellMouseUpEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellMouseUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellMouseUpEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellMouseUpListeners);
		}


		/// <summary>
        /// Extracts the CellMouseUp event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellMouseUpCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellMouseUp event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseUpAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellMouseUp event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseUpRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the RowHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachRowHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of RowHeaderMouseRightClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseRowHeaderMouseRightClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the RowHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderRowHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasRowHeaderMouseRightClickListeners);
		}


		/// <summary>
        /// Extracts the RowHeaderMouseRightClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractRowHeaderMouseRightClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the RowHeaderMouseRightClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowHeaderMouseRightClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the RowHeaderMouseRightClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowHeaderMouseRightClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CancelEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCancelEditEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CancelEdit event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCancelEditEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CancelEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCancelEditEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCancelEditListeners);
		}


		/// <summary>
        /// Extracts the CancelEdit event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCancelEditCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CancelEdit event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCancelEditAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CancelEdit event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCancelEditRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellkeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellkeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellkeyDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellkeyDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellkeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellkeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the CellkeyDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellkeyDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellkeyDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellkeyDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellkeyDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellkeyDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ComboCellSelectedIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachComboCellSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ComboCellSelectedIndexChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseComboCellSelectedIndexChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ComboCellSelectedIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderComboCellSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasComboCellSelectedIndexChangedListeners);
		}


		/// <summary>
        /// Extracts the ComboCellSelectedIndexChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractComboCellSelectedIndexChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ComboCellSelectedIndexChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderComboCellSelectedIndexChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ComboCellSelectedIndexChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderComboCellSelectedIndexChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ClientEvent event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachClientEventEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ClientEvent event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseClientEventEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ClientEvent event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderClientEventEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ClientEvent event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractClientEventCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ClientEvent event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClientEventAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ClientEvent event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClientEventRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of KeyDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseKeyDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the KeyDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractKeyDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the KeyDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the KeyDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the dataChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachdataChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of dataChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType RevisedataChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the dataChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderdataChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasdataChangedListeners);
		}


		/// <summary>
        /// Extracts the dataChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractdataChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the dataChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderdataChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the dataChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderdataChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "CellDoubleClick":
					this.RenderCellDoubleClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "CellDoubleClick":
					this.RenderCellDoubleClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the ClientRowTemplate property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderClientRowTemplateProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.ClientRowTemplate))
					{
						jsonWriter.WriteProperty("itemTpl", element.ClientRowTemplate);
					}
				}
			}

		}

		/// <summary>
        /// Renders the ClientRowTemplate property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClientRowTemplatePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the UseFixedHeaders property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderUseFixedHeadersProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the UseFixedHeaders property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderUseFixedHeadersPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the RowHeaderVisible property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderRowHeaderVisibleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the RowHeaderVisible property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowHeaderVisiblePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the SelectionMode property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSelectionModeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the SelectionMode property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionModePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MultiSelect property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMultiSelectProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the MultiSelect property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMultiSelectPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Content property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderContentProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the Content property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderContentPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the CanSelect property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderCanSelectProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the CanSelect property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCanSelectPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the FrozenCols property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderFrozenColsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the FrozenCols property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderFrozenColsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ColumnFilterInfo property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderColumnFilterInfoProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ColumnFilterInfo property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnFilterInfoPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the SortableColumns property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSortableColumnsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the SortableColumns property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSortableColumnsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the AllowColumnSelection property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAllowColumnSelectionProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the AllowColumnSelection property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAllowColumnSelectionPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the EnableLocking property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnableLockingProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the EnableLocking property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnableLockingPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ShowAutoFilterColumnItem property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderShowAutoFilterColumnItemProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ShowAutoFilterColumnItem property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderShowAutoFilterColumnItemPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the AllowTrackMouseOver property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAllowTrackMouseOverProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the AllowTrackMouseOver property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAllowTrackMouseOverPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the SearchPanel property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSearchPanelProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the SearchPanel property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSearchPanelPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ColumnLines property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderColumnLinesProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ColumnLines property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnLinesPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the RowLines property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderRowLinesProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the RowLines property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowLinesPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the CellBorderStyle property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderCellBorderStyleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the CellBorderStyle property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellBorderStylePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "SetCellStyle":
					return this.ExtractSetCellStyleMethodCallbackData(context, eventObject);						
				case "SetComboboxSelectedIndex":
					return this.ExtractSetComboboxSelectedIndexMethodCallbackData(context, eventObject);						
				case "SwapColumns":
					return this.ExtractSwapColumnsMethodCallbackData(context, eventObject);						
				case "Sort":
					return this.ExtractSortMethodCallbackData(context, eventObject);						
				case "ScrollToRow":
					return this.ExtractScrollToRowMethodCallbackData(context, eventObject);						
				case "SetWidgetStyle":
					return this.ExtractSetWidgetStyleMethodCallbackData(context, eventObject);						
				case "SetProtectCell":
					return this.ExtractSetProtectCellMethodCallbackData(context, eventObject);						
				case "SelectCell":
					return this.ExtractSelectCellMethodCallbackData(context, eventObject);						
				case "SetRowVisibility":
					return this.ExtractSetRowVisibilityMethodCallbackData(context, eventObject);						
				case "EditCellByPosition":
					return this.ExtractEditCellByPositionMethodCallbackData(context, eventObject);						
				case "SetCellBackgroundColor":
					return this.ExtractSetCellBackgroundColorMethodCallbackData(context, eventObject);						
				case "SetRowHeaderBackgroundColor":
					return this.ExtractSetRowHeaderBackgroundColorMethodCallbackData(context, eventObject);						
				case "SetRowHeaderBackgroundImage":
					return this.ExtractSetRowHeaderBackgroundImageMethodCallbackData(context, eventObject);						
				case "SetRowHeaderCornerIcon":
					return this.ExtractSetRowHeaderCornerIconMethodCallbackData(context, eventObject);						
				case "SetButtonVisibility":
					return this.ExtractSetButtonVisibilityMethodCallbackData(context, eventObject);						
				case "ContextMenuShow":
					return this.ExtractContextMenuShowMethodCallbackData(context, eventObject);						
				case "ClearFilter":
					return this.ExtractClearFilterMethodCallbackData(context, eventObject);						
				case "SetSelection":
					return this.ExtractSetSelectionMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "SetCellStyle":
					this.RenderSetCellStyleMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetComboboxSelectedIndex":
					this.RenderSetComboboxSelectedIndexMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SwapColumns":
					this.RenderSwapColumnsMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "Sort":
					this.RenderSortMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "ScrollToRow":
					this.RenderScrollToRowMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetWidgetStyle":
					this.RenderSetWidgetStyleMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetProtectCell":
					this.RenderSetProtectCellMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SelectCell":
					this.RenderSelectCellMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowVisibility":
					this.RenderSetRowVisibilityMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "EditCellByPosition":
					this.RenderEditCellByPositionMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetCellBackgroundColor":
					this.RenderSetCellBackgroundColorMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowHeaderBackgroundColor":
					this.RenderSetRowHeaderBackgroundColorMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowHeaderBackgroundImage":
					this.RenderSetRowHeaderBackgroundImageMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowHeaderCornerIcon":
					this.RenderSetRowHeaderCornerIconMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetButtonVisibility":
					this.RenderSetButtonVisibilityMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "ContextMenuShow":
					this.RenderContextMenuShowMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "ClearFilter":
					this.RenderClearFilterMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetSelection":
					this.RenderSetSelectionMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the SetCellStyle method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetCellStyleMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetCellStyle callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetCellStyleMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetComboboxSelectedIndex method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetComboboxSelectedIndexMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetComboboxSelectedIndex callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetComboboxSelectedIndexMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SwapColumns method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSwapColumnsMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SwapColumns callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSwapColumnsMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the Sort method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSortMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method Sort callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSortMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the ScrollToRow method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderScrollToRowMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method ScrollToRow callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractScrollToRowMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetWidgetStyle method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetWidgetStyleMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetWidgetStyle callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetWidgetStyleMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetProtectCell method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetProtectCellMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetProtectCell callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetProtectCellMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SelectCell method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSelectCellMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SelectCell callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSelectCellMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowVisibility method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowVisibilityMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetRowVisibility callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowVisibilityMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the EditCellByPosition method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderEditCellByPositionMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method EditCellByPosition callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractEditCellByPositionMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetCellBackgroundColor method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetCellBackgroundColorMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetCellBackgroundColor callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetCellBackgroundColorMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowHeaderBackgroundColor method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowHeaderBackgroundColorMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetRowHeaderBackgroundColor callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowHeaderBackgroundColorMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowHeaderBackgroundImage method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowHeaderBackgroundImageMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetRowHeaderBackgroundImage callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowHeaderBackgroundImageMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowHeaderCornerIcon method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowHeaderCornerIconMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetRowHeaderCornerIcon callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowHeaderCornerIconMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetButtonVisibility method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetButtonVisibilityMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetButtonVisibility callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetButtonVisibilityMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the ContextMenuShow method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderContextMenuShowMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method ContextMenuShow callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractContextMenuShowMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the ClearFilter method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderClearFilterMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method ClearFilter callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractClearFilterMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetSelection method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetSelectionMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetSelection callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetSelectionMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}