﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(ToolBarElement), "ToolBarElementExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("Text", ClientConfigName="title")]
    public class TToolBarElementExtJSRenderer : TToolBarElementExtJSRendererBase
    {


        /// <summary>
        /// Revises the XTYPE property value.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <returns></returns>
        protected override string ReviseXTypePropertyValue(VisualElement visualElement, string xtype)
        {
            // Get tool bar
            ToolBarElement toolbar = visualElement as ToolBarElement;

            // If there is a valid tool bar
            if(toolbar != null)
            {
                // If there is text
                if(!string.IsNullOrEmpty(toolbar.Text))
                {
                    // Return title bar XTYPE
                    return "titlebar";
                }
            }

            return xtype;
        }

    }
}
