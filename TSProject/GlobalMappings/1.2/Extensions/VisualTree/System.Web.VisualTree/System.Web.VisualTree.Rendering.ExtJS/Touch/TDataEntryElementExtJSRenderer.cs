﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(DataEntryElement), "DataEntryElementExtJSRenderer", "formpanel", "Ext.form.Panel")]
    public class TDataEntryElementExtJSRenderer : TDataEntryElementExtJSRendererBase
    {

        /// <summary>
        /// Revise the event behavior type of Updated event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        /// <returns></returns>
        protected override ExtJSEventBehaviorType ReviseUpdatedEventBehavior(RenderingContext context, DataEntryElement element, ExtJSEventBehaviorType eventBehaviorType)
        {
            return ExtJSEventBehaviorType.Queued;
        }



        /// <summary>
        /// Extracts the Updated event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractUpdatedCallbackEventArgs(RenderingContext context, VisualElement visualElement, string serverEventName, JObject args)
        {
            
            // Get values send from client
            JObject valuesQueryString = args.Value<JObject>("values");

            // If there are values send from client
            if (valuesQueryString != null)
            {
                
                // Get query string values
                NameValueCollection values = ParseQueryString(valuesQueryString.Properties());

                // If there is a valid values collection
                if (values != null)
                {
                    // Get the data entry 
                    DataEntryElement dataEntry = visualElement as DataEntryElement;

                    // If there is a valid data entry 
                    if (dataEntry != null)
                    {
                        // Get item
                        object item = dataEntry.ItemSource;

                        // If there is a valid item
                        if (item != null)
                        {
                            // Loop all item properties
                            foreach (PropertyDescriptor property in dataEntry.ItemProperties)
                            {
                                // Get the property name
                                string propertyName = property.Name;

                                // If there is a valid property name
                                if (!string.IsNullOrEmpty(propertyName))
                                {
                                    // Get the property value 
                                    string propertyValue = values.Get(propertyName);

                                    // If there is a valid property value
                                    if (propertyValue != null)
                                    {
                                        // Get property type
                                        Type propertyType = property.PropertyType;

                                        // If there is a valid property type
                                        if (propertyType != null)
                                        {
                                            // Get property type converter
                                            TypeConverter propertyTypeConverter = TypeDescriptor.GetConverter(propertyType);

                                            // If there is a valid 
                                            if (propertyTypeConverter != null)
                                            {
                                                try
                                                {
                                                    // Set value based on conversion
                                                    property.SetValue(item, propertyTypeConverter.ConvertFromString(propertyValue));
                                                }
                                                catch
                                                {

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Return empty arguments
            return EventArgs.Empty;
        }

        /// <summary>
        /// Parses the query string.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <returns></returns>
        private static NameValueCollection ParseQueryString(IEnumerable<JProperty> properties)
        {
            NameValueCollection queryString = new NameValueCollection();

            if(properties != null)
            {
                foreach (JProperty property in properties)
                {
                    queryString[property.Name] = property.Value.ToString();
                }
            }

            return queryString;
        }

        /// <summary>
        /// Gets the containing form script.
        /// </summary>
        /// <returns></returns>
        protected override string GetContainingFormScript()
        {
            return "this.getParent()";
        }

        /// <summary>
        /// Gets the name of the field label configuration.
        /// </summary>
        /// <returns></returns>
        protected override string GetFieldLabelConfigName()
        {
            return "label";
        }

        /// <summary>
        /// Gets the type of the CheckBox field.
        /// </summary>
        /// <returns></returns>
        protected override string GetCheckBoxFieldXType()
        {
            return "checkboxfield";
        }

        /// <summary>
        /// Gets the type of the date field.
        /// </summary>
        /// <returns></returns>
        protected override string GetDateFieldXType()
        {
            return "datepickerfield";
        }

        /// <summary>
        /// Gets the type of the number field.
        /// </summary>
        /// <returns></returns>
        protected override string GetNumberFieldXType()
        {
            return "numberfield";
        }

        /// <summary>
        /// Gets the type of the text field.
        /// </summary>
        /// <returns></returns>
        protected override string GetTextFieldXType()
        {
            return "textfield";
        }
    }
}
