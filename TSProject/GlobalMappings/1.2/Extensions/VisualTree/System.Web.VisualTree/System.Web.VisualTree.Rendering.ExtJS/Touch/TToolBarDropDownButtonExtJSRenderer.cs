﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TToolBarDropDownButton), "ToolBarItemExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("ButtonAppearance", PropertyInitializeCode = "ui = <#=TUtilities.GetButtonUIPropertyFromAppearance(element.ButtonAppearance) #>")]
    public class TToolBarDropDownButtonExtJSRenderer : TToolBarDropDownButtonExtJSRendererBase
    {

        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The writer.</param>
        protected override void RenderEvents(RenderingContext context, Elements.VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderEvents(context, visualElement, jsonWriter);

            // Get the typed element
            TToolBarDropDownButton element = visualElement as TToolBarDropDownButton;

            // If there is a valid element
            if (element != null)
            {
                // If there is a valid JSON writer
                if (jsonWriter != null)
                {
                    // Render the client event handler
                    jsonWriter.WritePropertyWithAnonymousFunction("tap", new string[] { }, (javaScriptBuilder) =>
                    {
                        StringBuilder sb = new StringBuilder();

                        using (JsonTextWriter itemsWriter = RenderingUtils.GetJsonWriter(sb))
                        {
                            RenderPlainItemsProperty(context, element.Items, itemsWriter, null);

                            itemsWriter.Flush();
                        }

                        // Create the callback code.
                        javaScriptBuilder.AppendInvocationStatement("VT_ShowActionSheet", new JavaScriptCode(sb.ToString()));
                    });
                }
            }
        }
    }

}
