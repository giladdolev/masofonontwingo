﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(GridElement), "GridElementExtJSRenderer", "grid", "Ext.dataview.ListView")]
    [ExtJSPropertyDescription("ClientRowTemplate", ClientConfigName = "itemTpl")]
    [ExtJSEventDescription("CellDoubleClick", ClientEventName = "itemdoubletap", ClientEventHandlerParameterNames = "list, index", ClientHandlerCallbackData = "selectedIndex = index", ServerEventArgsCreateCode = "new GridElementCellEventArgs(0, this.selectedIndex.ToInt32())")]
    public class TGridElementExtJSRenderer : TGridElementExtJSRendererBase
    {



        /// <summary>
        /// Provides support for rendering the grid store
        /// </summary>
        private class GridStoreWriter : JsonValueWriter
        {
            /// <summary>
            /// The grid element
            /// </summary>
            private GridElement _gridElement;

            /// <summary>
            /// Initializes a new instance of the <see cref="GridStoreWriter"/> class.
            /// </summary>
            /// <param name="gridElement">The grid element.</param>
            public GridStoreWriter(GridElement gridElement)
            {
                this._gridElement = gridElement;
            }

            /// <summary>
            /// Writes the value.
            /// </summary>
            /// <param name="jsonWriter">The JSON writer.</param>
            public override void WriteValue(JsonTextWriter jsonWriter)
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WriteEndObject();
            }
        }


        /// <summary>
        /// Renders the store.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <returns></returns>
        internal static JsonValueWriter RenderStore(GridElement gridElement)
        {
            return new GridStoreWriter(gridElement);
        }



        /// <summary>
        /// Renders the ClientRowTemplate property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderClientRowTemplateProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            GridElement gridElement = visualElement as GridElement;
            if (gridElement != null && String.IsNullOrEmpty(gridElement.ClientRowTemplate))
            {
                gridElement.ClientRowTemplate = GetDefaultRenderTemplate(gridElement);
            }
            base.RenderClientRowTemplateProperty(context, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the refresh.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRefresh(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {

        }



        /// <summary>
        /// Renders the template. 
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <returns></returns>
        private static string GetDefaultRenderTemplate(GridElement gridElement)
        {
            StringBuilder template = new StringBuilder();

            // Open the div
            template.Append("<div>");

            int index = 0;

            // Loop all columns
            foreach (GridColumn column in gridElement.Columns)
            {
                if (index == 0)
                {
                    template.Append("<b>");

                    // Add place holder
                    AddColumnValueTemplate(template, column);
                    template.Append("</b>");
                }
                else if (index == 1)
                {
                    template.Append("&nbsp;<b>");

                    // Add place holder
                    AddColumnValueTemplate(template, column);

                    template.Append("</b><br>");
                }
                else
                {
                    AddColumnTemplate(template, column);
                }
                index++;
                if (index > 4)
                {
                    break;
                }
            }

            // Close the div
            template.Append("</div>");

            return template.ToString();
        }

        private static void AddColumnTemplate(StringBuilder template, GridColumn column)
        {
            bool isImage = (GridColumnExtJSRenderer.IsPicture(column));

            if (!isImage)
            {
                template.Append(column.HeaderText);
                template.Append(":&nbsp;");
            }

            // Add place holder
            AddColumnValueTemplate(template, column);
            template.Append("<br>");
        }

        /// <summary>
        /// Adds the column template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="column">The column.</param>
        private static void AddColumnValueTemplate(StringBuilder template, GridColumn column)
        {
            bool isImage = (GridColumnExtJSRenderer.IsPicture(column));
            if (isImage)
            {
                template.Append(@"<img  width='150'  height='150' src='data:image/jpeg;base64,");
            }
            template.Append("{");
            template.Append(column.HeaderText);

            template.Append("}");
            if (isImage)
            {
                template.Append("' />");
            }
        }
    }
}
