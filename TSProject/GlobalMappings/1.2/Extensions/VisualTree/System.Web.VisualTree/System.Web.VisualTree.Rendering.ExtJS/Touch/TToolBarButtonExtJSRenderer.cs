﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TToolBarButton), "ToolBarItemExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("ButtonAppearance", PropertyInitializeCode = "ui = <#=TUtilities.GetButtonUIPropertyFromAppearance(element.ButtonAppearance) #>")]
    [ExtJSEventDescription("Click", ClientEventName = "tap")] 
    public class TToolBarButtonExtJSRenderer : TToolBarButtonExtJSRendererBase
    {

         
    }

    internal class TUtilities
    {
        /// <summary>
        /// Gets the button UI property from appearance.
        /// </summary>
        /// <param name="appearance">The appearance.</param>
        /// <returns></returns>
        public static string GetButtonUIPropertyFromAppearance(TButtonAppearance appearance)
        {
            switch (appearance)
            {
                
                case TButtonAppearance.Back:
                    return "normal";
                case TButtonAppearance.Forward:
                    return "forward";
                case TButtonAppearance.Round:
                    return "round";
                case TButtonAppearance.Action:
                    return "action";
                case TButtonAppearance.Decline:
                    return "decline";
                case TButtonAppearance.Confirm:
                    return "confirm";
                case TButtonAppearance.Normal:                    
                default:
                    return "normal";
            }
        }
    }
}
