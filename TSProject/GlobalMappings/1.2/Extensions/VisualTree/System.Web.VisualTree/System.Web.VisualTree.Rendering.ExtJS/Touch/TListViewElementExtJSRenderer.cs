﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
	[ExtJSTouchRendererDescription(typeof(TListViewElement), "ListViewElementExtJSRenderer", "list", "Ext.dataview.List")]
    [ExtJSPropertyDescription("ItemTemplate", ClientConfigName = "itemTpl")]
    [ExtJSEventDescription("ItemClick", ClientEventName = "itemtap", ClientEventHandlerParameterNames = "view, index, target, record, e, eOpts", ClientHandlerCallbackData = "item: record.get('listItemId')", ServerEventArgsCreateCode = "new ValueChangedArgs<int>(this.item.ToInt32())")]

	public class TListViewElementExtJSRenderer : TListViewElementExtJSRendererBase
    {
        /// <summary>
        /// Gets the name of the item identifier.
        /// </summary>
        /// <returns></returns>
        protected override string GetItemIdName()
        {
            return "listItemId";
        }


        /// <summary>
        /// Renders the columns in array.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderColumnsInArray(ListViewElement listViewElement, JsonTextWriter jsonWriter)
        {
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
            {
                jsonWriter.WriteProperty("header", "ItemId");
                jsonWriter.WriteProperty("dataIndex", "listItemId");
            }

            base.RenderColumnsInArray(listViewElement, jsonWriter);
        }




        /// <summary>
        /// Gets the fields.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <returns></returns>
        protected override string[] GetFields(ListViewElement listViewElement)
        {
            List<string> fields = new List<string>(base.GetFields(listViewElement));
            fields.Add("listItemId");
            return fields.ToArray();
        }
    }
}
