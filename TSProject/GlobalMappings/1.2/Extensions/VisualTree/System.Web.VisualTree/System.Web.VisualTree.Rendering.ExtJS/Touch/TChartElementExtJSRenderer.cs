﻿using System.Drawing;
using System.Globalization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Common;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Text;
using System.IO;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(Chart), "ChartElementExtJSRenderer", "chart", "Ext.chart.CartesianChart")]
    public class TChartElementExtJSRenderer : TChartElementExtJSRendererBase
    {
       
    }
}
