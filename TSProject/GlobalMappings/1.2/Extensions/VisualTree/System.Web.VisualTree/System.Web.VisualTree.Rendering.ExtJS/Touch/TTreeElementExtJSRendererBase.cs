using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Elements;
using Newtonsoft.Json;
using System.IO;


namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{

	public class TTreeElementExtJSRendererBase : ControlElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.dataview.NestedListView";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "nestedlist";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the AfterSelect event attach code.
			RenderAttachAfterSelectEvent(context, visualElement, writer); 
			// Renders the BeforeCheck event attach code.
			RenderAttachBeforeCheckEvent(context, visualElement, writer); 
			// Renders the BeforeSelect event attach code.
			RenderAttachBeforeSelectEvent(context, visualElement, writer); 
			// Renders the NodeMouseClick event attach code.
			RenderAttachNodeMouseClickEvent(context, visualElement, writer); 
			// Renders the NodeMouseDoubleClick event attach code.
			RenderAttachNodeMouseDoubleClickEvent(context, visualElement, writer); 
			// Renders the BeforeCollapse event attach code.
			RenderAttachBeforeCollapseEvent(context, visualElement, writer); 
			// Renders the BeforeExpand event attach code.
			RenderAttachBeforeExpandEvent(context, visualElement, writer); 
			// Renders the AfterExpand event attach code.
			RenderAttachAfterExpandEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "AfterSelect":
					return this.ExtractAfterSelectCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeCheck":
					return this.ExtractBeforeCheckCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeSelect":
					return this.ExtractBeforeSelectCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "NodeMouseClick":
					return this.ExtractNodeMouseClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "NodeMouseDoubleClick":
					return this.ExtractNodeMouseDoubleClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeCollapse":
					return this.ExtractBeforeCollapseCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeExpand":
					return this.ExtractBeforeExpandCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "AfterExpand":
					return this.ExtractAfterExpandCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.TreeElement element = visualElement as System.Web.VisualTree.Elements.TreeElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "SelectedItem":
					this.RenderSelectedItemPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Sorted":
					this.RenderSortedPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "LabelEdit":
					this.RenderLabelEditPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Indent":
					this.RenderIndentPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderSelectedItemProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSortedProperty(renderingContext, visualElement, jsonWriter);
			this.RenderLabelEditProperty(renderingContext, visualElement, jsonWriter);
			this.RenderIndentProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the AfterSelect event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachAfterSelectEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of AfterSelect event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAfterSelectEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the AfterSelect event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderAfterSelectEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return (element.HasAfterSelectListeners);
		}


		/// <summary>
        /// Extracts the AfterSelect event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractAfterSelectCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the AfterSelect event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAfterSelectAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the AfterSelect event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAfterSelectRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the BeforeCheck event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeCheckEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeCheck event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeCheckEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeCheck event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeCheckEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the BeforeCheck event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeCheckCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeCheck event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeCheckAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeCheck event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeCheckRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the BeforeSelect event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeSelectEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeSelect event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeSelectEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeSelect event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeSelectEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return (element.HasBeforeSelectListeners);
		}


		/// <summary>
        /// Extracts the BeforeSelect event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeSelectCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeSelect event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeSelect event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the NodeMouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachNodeMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of NodeMouseClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseNodeMouseClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the NodeMouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderNodeMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the NodeMouseClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractNodeMouseClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the NodeMouseClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderNodeMouseClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the NodeMouseClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderNodeMouseClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the NodeMouseDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachNodeMouseDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.TreeElement element = visualElement as System.Web.VisualTree.Elements.TreeElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderNodeMouseDoubleClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseNodeMouseDoubleClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "list", "index", "target", "record", "e", "eOpts"}, 
									() => {  
										// Create the server callback code.NodeMouseDoubleClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "NodeMouseDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{node: VT_getItemIdPath(record)}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("itemdoubletap", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of NodeMouseDoubleClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseNodeMouseDoubleClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the NodeMouseDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderNodeMouseDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return (element.HasNodeMouseDoubleClickListeners);
		}


		/// <summary>
        /// Extracts the NodeMouseDoubleClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractNodeMouseDoubleClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new TreeMouseClickEventArgs(visualElement.GetVisualElementByPath(args.Value<System.String>("node")) as TreeItem);
		}


		/// <summary>
        /// Renders the NodeMouseDoubleClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderNodeMouseDoubleClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.TreeElement element = visualElement as System.Web.VisualTree.Elements.TreeElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderNodeMouseDoubleClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseNodeMouseDoubleClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "list", "index", "target", "record", "e", "eOpts"}, 
                            () => {  
										// Create the server callback code.NodeMouseDoubleClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "NodeMouseDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{node: VT_getItemIdPath(record)}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('itemdoubletap', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the NodeMouseDoubleClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderNodeMouseDoubleClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.TreeElement element = visualElement as System.Web.VisualTree.Elements.TreeElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderNodeMouseDoubleClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "itemdoubletap");
            }
        }

		/// <summary>
        /// Renders the BeforeCollapse event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeCollapseEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeCollapse event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeCollapseEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeCollapse event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeCollapseEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return (element.HasBeforeCollapseListeners);
		}


		/// <summary>
        /// Extracts the BeforeCollapse event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeCollapseCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeCollapse event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeCollapseAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeCollapse event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeCollapseRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the BeforeExpand event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeExpandEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeExpand event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeExpandEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeExpand event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeExpandEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return (element.HasBeforeExpandListeners);
		}


		/// <summary>
        /// Extracts the BeforeExpand event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeExpandCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeExpand event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeExpandAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeExpand event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeExpandRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the AfterExpand event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachAfterExpandEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of AfterExpand event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAfterExpandEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the AfterExpand event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderAfterExpandEvent(RenderingContext context, System.Web.VisualTree.Elements.TreeElement element)
		{
			return (element.HasAfterExpandListeners);
		}


		/// <summary>
        /// Extracts the AfterExpand event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractAfterExpandCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the AfterExpand event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAfterExpandAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the AfterExpand event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAfterExpandRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.TreeElement element = visualElement as System.Web.VisualTree.Elements.TreeElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "NodeMouseDoubleClick":
					this.RenderNodeMouseDoubleClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.TreeElement element = visualElement as System.Web.VisualTree.Elements.TreeElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "NodeMouseDoubleClick":
					this.RenderNodeMouseDoubleClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the SelectedItem property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSelectedItemProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the SelectedItem property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectedItemPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Sorted property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSortedProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the Sorted property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSortedPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the LabelEdit property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderLabelEditProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the LabelEdit property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLabelEditPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Indent property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderIndentProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the Indent property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderIndentPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "CollapseAll":
					return this.ExtractCollapseAllMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "CollapseAll":
					this.RenderCollapseAllMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the CollapseAll method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderCollapseAllMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method CollapseAll callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractCollapseAllMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}