﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TToolBarMenuItem), "ToolBarMenuItemExtJSRenderer", "", "")]
    [ExtJSEventDescription("Click", ClientEventName = "tap")]
    [ExtJSPropertyDescription("Style", PropertyInitializeCode = "ui = <#=TUtilities.GetButtonUIPropertyFromAppearance(element.Appearance) #>")]
    public class TToolBarMenuItemExtJSRenderer : TToolBarMenuItemExtJSRendererBase
    {
    }
}
