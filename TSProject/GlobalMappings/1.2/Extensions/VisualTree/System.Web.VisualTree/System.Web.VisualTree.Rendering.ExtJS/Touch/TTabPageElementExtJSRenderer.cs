﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TabItem), "TabPageElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSPropertyDescription("IconCls", PropertyInitializeCode = "iconCls = <#= TTabPageElementExtJSRenderer.GetImageClass(element) #>")]
    public class TTabPageElementExtJSRenderer : TTabPageElementExtJSRendererBase
    {
        internal static JavaScriptCode GetImageClass(TabItem element)
        {
            string imageClass = (element.Image != null) ? String.Format("VT_GetCssClassForBackgroundImage('{0}')", element.Image.Source) : "null";
            return new JavaScriptCode(imageClass);
        }
    }
}
