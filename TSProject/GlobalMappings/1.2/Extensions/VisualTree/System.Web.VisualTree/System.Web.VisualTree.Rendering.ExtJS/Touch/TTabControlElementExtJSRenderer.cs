﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TTabElement), "TabControlElementExtJSRenderer", "tabpanel", "Ext.tab.Panel")]
    public class TTabControlElementExtJSRenderer : TTabControlElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the requires configuration.
        /// </summary>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderRequiresConfig(Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Clear the requires configuration
        }

        /// <summary>
        /// Gets the name of the tab position configuration.
        /// </summary>
        /// <returns></returns>
        protected override string GetTabPositionConfigName()
        {
            return "tabBarPosition";
        }
    }
}
