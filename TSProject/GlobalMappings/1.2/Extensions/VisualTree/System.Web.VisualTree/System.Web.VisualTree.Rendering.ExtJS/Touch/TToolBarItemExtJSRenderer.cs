﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TToolBarItem), "ToolBarItemExtJSRenderer", "button", "Ext.Button")]
    [ExtJSEventDescription("Click", ClientEventName = "tap")]
    public class TToolBarItemExtJSRenderer : TToolBarItemExtJSRendererBase
    {

        public override void Render(RenderingContext renderingContext, Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.Render(renderingContext, visualElement, jsonWriter);
        }


    }
}
