﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.Web.VisualTree.Elements;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.ComponentModel.Composition;
using System.Text;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))] 
    [ExtJSTouchRendererDescription(typeof(WindowElement), "WindowElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    public class TWindowElementExtJSRenderer : TWindowElementExtJSRendererBase
    {


        /// <summary>
        /// Revises the renderer.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public override IVisualElementRenderer ReviseRenderer(RenderingContext renderingContext, IVisualElement visualElement)
        {
            return this;
        }

        /// <summary>
        /// Renders the Height property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderHeightProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            
        }

        /// <summary>
        /// Renders the Width property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderWidthProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
           
        }


        /// <summary>
        /// Renders the application.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="windowElement">The window element.</param>
        /// <param name="objWriter">The writer.</param>
        /// <param name="renderTo">The render to.</param>
        public override void RenderApp(RenderingContext renderingContext, IVisualElement visualElement, StringWriter objWriter, string renderTo)
        {
            WindowElement windowElement = (visualElement != null && visualElement is WindowElement) ? visualElement as WindowElement : null;

            objWriter.WriteLine("<script type=\"text/javascript\">");

            // If there is a valid visual element
            if (visualElement != null)
            {
                // Write the visual tree application ID
                objWriter.WriteLine(string.Format("VT_APPID = '{0}';", visualElement.ApplicationId));
            }

            objWriter.WriteLine(string.Format("Ext.application({{name: '{0}', launch: function() {{", visualElement.ApplicationId));
            
            // Call VT_onInitialize to hook events and such
            objWriter.WriteLine("VT_onInitialize();");


            objWriter.WriteLine("Ext.Viewport.add(");



            // Create the json writer
            // dispose by JsonTextWriter when call close, but call is missing, can use using for the JsonTextWriter, no wanning for fxcop for this.. 
            JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(objWriter);

            // Write start object
            jsonWriter.WriteStartObject();
            jsonWriter.WriteProperty("fullscreen", true);

            // Render visual element properties
            this.RenderProperties(renderingContext, windowElement, jsonWriter);

            // Write the end object
            jsonWriter.WriteEndObject();

            // Flush json writer
            jsonWriter.Flush();

            objWriter.WriteLine(");");

            // Call VT_onActerInitialize to hook events and such after initialization
            objWriter.WriteLine("VT_onAfterInitialize();");

            objWriter.WriteLine("}});");

            objWriter.WriteLine("</script>");


        }

        /// <summary>
        /// Renders the invoke window action method.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">Name of the component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder.</param>
        protected override void RenderInvokeWindowActionMethod(RenderingContext renderingContext, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, VisualElement visualElement, object methodData, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get the java script builder
            JavaScriptBuilder builder = getJavaScriptBuilder(false);

            // If there is a valid java script builder
            if (builder != null)
            {
                // Get window action type
                WindowActionType actionType = methodData != null ? (WindowActionType)methodData : WindowActionType.Open;

                // If is an open window action 
                if (actionType == WindowActionType.Open || actionType == WindowActionType.OpenModal)
                {

                    // Create the JSON string writer
                    StringBuilder stringBuilder = new StringBuilder();

                    // Create the JSON writer
                    using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(stringBuilder))
                    {

                        // Render window
                        this.Render(renderingContext, visualElement, jsonWriter);

                        // Flush content
                        jsonWriter.Flush();

                        // Add open window code
                        builder.AppendVariableWithInvocationInitializer("view", "Ext.create", "Ext.Panel", new JavaScriptCode(stringBuilder.ToString()));

                        // Show the view
                        builder.AppendInvocationStatement("VT_ShowView", new JavaScriptCode("view"));
                    }

                }
                else
                {
                    // Get component and destroy it if valid.
                    builder.AppendInvocationStatement("VT_CloseView", VisualElement.GetElementId(visualElement));
              
                }
            }

        }
    }
}
