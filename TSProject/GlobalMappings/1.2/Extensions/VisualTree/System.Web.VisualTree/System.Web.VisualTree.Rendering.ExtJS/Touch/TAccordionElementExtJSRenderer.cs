﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(AccordionElement), "TTabControlElementExtJSRenderer", "tabpanel", "Ext.tab.Panel")]
    public class TAccordionElementExtJSRenderer : TAccordionElementExtJSRendererBase
    {

        public TAccordionElementExtJSRenderer()
        {

        }

    }
}
