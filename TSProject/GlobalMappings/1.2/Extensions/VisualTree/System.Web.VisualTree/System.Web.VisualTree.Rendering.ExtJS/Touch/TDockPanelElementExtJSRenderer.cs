﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(DockPanelElement), "DockPanelElementExtJSRenderer", "tabpanel", "Ext.tab.Panel")]
    public class TDockPanelElementExtJSRenderer : DockPanelElementExtJSRendererBase
    {
        /// <summary>
        /// Renders the SelectedPanel property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderSelectedPanelPropertyChange(ExtJSRenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.DockPanelElement element = visualElement as System.Web.VisualTree.Elements.DockPanelElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRawValue("{0}.setActiveItem({1});", new JavaScriptCode(componentVariable), element.Controls != null ? element.Controls.Count - 1 : 0);
            }
        }
    }
}
