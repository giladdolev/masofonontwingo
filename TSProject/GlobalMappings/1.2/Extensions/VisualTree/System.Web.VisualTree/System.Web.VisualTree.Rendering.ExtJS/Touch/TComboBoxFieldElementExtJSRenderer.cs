﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering;
using Newtonsoft.Json;

namespace System.Web.VisualTree.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TComboBoxFieldElement), "ComboBoxElementExtJSRenderer", "selectfield", "Ext.field.Select")]
    [ExtJSEventDescription("SelectedIndexChanged", ClientEventName = "change", ClientEventHandlerParameterNames = "combo,newValue,oldValue,eOpts", ClientHandlerCallbackData = "selectedIndex: combo.getStore().findExact('value',newValue)", ServerEventArgsCreateCode = "new ValueChangedArgs<int>(this.selectedIndex.ToInt32())")]
    public class TComboBoxFieldElementExtJSRenderer : TComboBoxFieldElementExtJSRendererBase
    {



    }
    
}