using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.Web.VisualTree.Elements;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.ComponentModel.Composition;
using System.Text;


namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{

	public class TWindowElementExtJSRendererBase : WindowElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.panel.Panel";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "panel";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the WindowClosing event attach code.
			RenderAttachWindowClosingEvent(context, visualElement, writer); 
			// Renders the Shown event attach code.
			RenderAttachShownEvent(context, visualElement, writer); 
			// Renders the Deactivate event attach code.
			RenderAttachDeactivateEvent(context, visualElement, writer); 
			// Renders the Activated event attach code.
			RenderAttachActivatedEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "WindowClosing":
					return this.ExtractWindowClosingCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Shown":
					return this.ExtractShownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Deactivate":
					return this.ExtractDeactivateCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Activated":
					return this.ExtractActivatedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "StartPosition":
					this.RenderStartPositionPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "TopMost":
					this.RenderTopMostPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "WindowState":
					this.RenderWindowStatePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MaximizeBox":
					this.RenderMaximizeBoxPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MinimizeBox":
					this.RenderMinimizeBoxPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "EnableEsc":
					this.RenderEnableEscPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderStartPositionProperty(renderingContext, visualElement, jsonWriter);
			this.RenderTopMostProperty(renderingContext, visualElement, jsonWriter);
			this.RenderWindowStateProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMaximizeBoxProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMinimizeBoxProperty(renderingContext, visualElement, jsonWriter);
			this.RenderEnableEscProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the WindowClosing event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWindowClosingEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of WindowClosing event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWindowClosingEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WindowClosing event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWindowClosingEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasWindowClosingListeners);
		}


		/// <summary>
        /// Extracts the WindowClosing event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractWindowClosingCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WindowClosing event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWindowClosingAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WindowClosing event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWindowClosingRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Shown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachShownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of Shown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseShownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Shown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderShownEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasShownListeners);
		}


		/// <summary>
        /// Extracts the Shown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractShownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Shown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderShownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Shown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderShownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Deactivate event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDeactivateEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of Deactivate event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDeactivateEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Deactivate event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDeactivateEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasDeactivateListeners);
		}


		/// <summary>
        /// Extracts the Deactivate event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDeactivateCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Deactivate event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDeactivateAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Deactivate event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDeactivateRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Activated event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachActivatedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of Activated event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseActivatedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Activated event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderActivatedEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasActivatedListeners);
		}


		/// <summary>
        /// Extracts the Activated event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractActivatedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Activated event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderActivatedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Activated event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderActivatedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the StartPosition property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderStartPositionProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the StartPosition property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderStartPositionPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the TopMost property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTopMostProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the TopMost property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTopMostPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the WindowState property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWindowStateProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the WindowState property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWindowStatePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MaximizeBox property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaximizeBoxProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the MaximizeBox property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaximizeBoxPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MinimizeBox property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMinimizeBoxProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the MinimizeBox property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMinimizeBoxPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the EnableEsc property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnableEscProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the EnableEsc property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnableEscPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "RaiseClosedEvent":
					return this.ExtractRaiseClosedEventMethodCallbackData(context, eventObject);						
				case "InvokeWindowAction":
					return this.ExtractInvokeWindowActionMethodCallbackData(context, eventObject);						
				case "Activate":
					return this.ExtractActivateMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "RaiseClosedEvent":
					this.RenderRaiseClosedEventMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "InvokeWindowAction":
					this.RenderInvokeWindowActionMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "Activate":
					this.RenderActivateMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the RaiseClosedEvent method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderRaiseClosedEventMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method RaiseClosedEvent callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractRaiseClosedEventMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the InvokeWindowAction method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderInvokeWindowActionMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method InvokeWindowAction callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractInvokeWindowActionMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the Activate method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderActivateMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method Activate callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractActivateMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}