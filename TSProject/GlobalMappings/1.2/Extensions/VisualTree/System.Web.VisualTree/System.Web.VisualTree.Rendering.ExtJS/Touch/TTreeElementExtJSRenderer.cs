﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Elements;
using Newtonsoft.Json;
using System.IO;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TreeElement), "ControlElementExtJSRenderer", "nestedlist", "Ext.dataview.NestedListView")]
    [ExtJSEventDescription("NodeMouseDoubleClick", ClientEventName = "itemdoubletap", ClientEventHandlerParameterNames = "view, list, index, target, record, e, eOpts", ClientHandlerCallbackData = "node: VT_getItemIdPath(record)", ServerEventArgsCreateCode = "new TreeMouseClickEventArgs(visualElement.GetVisualElementByPath(this.node) as TreeItem)")]
    public class TTreeElementExtJSRenderer : TTreeElementExtJSRendererBase
    {


        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderContentProperties(RenderingContext context, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(context, visualElement, jsonWriter);

            jsonWriter.WriteProperty("displayField", "text");


            StringBuilder sb = new StringBuilder();
            using (JsonTextWriter writer = RenderingUtils.GetJsonWriter(sb))
            {
                writer.WriteStartObject();
                writer.WritePropertyWithRawValue("model", "Ext.define('ListItem', {extend: 'Ext.data.Model', config: {fields: [{name: 'text',type: 'string'}]}})");
                writer.WriteProperty("defaultRootProperty", "items");
                writer.WritePropertyName("root");
                writer.WriteStartObject();
                this.RenderItems(context, ((TreeElement)visualElement).Items, writer);
                writer.WriteEndObject();
                writer.WriteEndObject();
                writer.Flush();
            }
            jsonWriter.WritePropertyWithRawValue("store", string.Format("Ext.create(\"Ext.data.TreeStore\", {0})", sb.ToString()));

        }

        /// <summary>
        /// Renders the items.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="items">The items.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        /// <returns></returns>
        private bool RenderItems(RenderingContext context, TreeItemCollection items, JsonTextWriter jsonWriter)
        {
            if (items.Count > 0)
            {
                jsonWriter.WritePropertyName("items");
                jsonWriter.WriteStartArray();
                foreach (TreeItem item in items)
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WriteProperty("itemId", item.ClientID);
                    jsonWriter.WriteProperty("text", item.Text);
                    if (!this.RenderItems(context, item.Items, jsonWriter))
                    {
                        jsonWriter.WriteProperty("leaf", true);
                    }
                    jsonWriter.WriteEndObject();
                }
                jsonWriter.WriteEndArray();

                return true;
            }

            return false;
        }
    }
}
