﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(TCardsElement), "PanelElementExtJSRenderer", "carousel", "Ext.carousel.Carousel")]
    [ExtJSPropertyDescription("SelectedIndex", PropertyChangeCode = "this.setActiveItem(<#= element.SelectedIndex #>);")]

    public class TCardsElementExtJSRenderer : TCardsElementExtJSRendererBase
    {
    }
}
