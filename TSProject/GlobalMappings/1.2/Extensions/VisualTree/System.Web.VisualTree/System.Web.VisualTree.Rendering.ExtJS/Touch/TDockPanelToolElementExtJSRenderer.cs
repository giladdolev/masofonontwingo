﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Touch
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSTouchRendererDescription(typeof(DockPanelToolElement), "DockPanelElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "title = <#= GetFormatedTextProperty(element, GetDisplayText(element.Text)) #>")]
    public class TDockPanelToolElementExtJSRenderer : TDockPanelToolElementExtJSRendererBase
    {
        
    }
}
