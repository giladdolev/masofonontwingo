﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering
{

    public static class RenderingUtils
    {

        internal enum JsonTextWriterScopeType
        {
            /// <summary>
            /// The object
            /// </summary>
            Object,
            /// <summary>
            /// The array
            /// </summary>
            Array,
            /// <summary>
            /// The constructor
            /// </summary>
            Constructor
        }


        internal enum BrowserCapabilities
        {
            /// <summary>
            /// The multi line tabs
            /// </summary>
            MultiLineTabs
        }


        private class JsonTextWriterScope : IDisposable
        {
            /// <summary>
            /// The type
            /// </summary>
            JsonTextWriterScopeType _type;

            /// <summary>
            /// The json writer
            /// </summary>
            JsonTextWriter _jsonWriter;

            /// <summary>
            /// The constructor name
            /// </summary>
            string _constructorName;

            /// <summary>
            /// Initializes a new instance of the <see cref="JsonTextWriterScope" /> class.
            /// </summary>
            /// <param name="jsonWriter">The json writer.</param>
            /// <param name="type">The type.</param>
            /// <param name="constructorName">The constructor name.</param>
            public JsonTextWriterScope(JsonTextWriter jsonWriter, JsonTextWriterScopeType type, string constructorName = null)
            {
                _type = type;
                _jsonWriter = jsonWriter;
                _constructorName = constructorName;

                StartScope();
            }

            /// <summary>
            /// Starts the scope.
            /// </summary>
            private void StartScope()
            {
                if (_jsonWriter != null)
                {
                    switch (_type)
                    {
                        case JsonTextWriterScopeType.Constructor:
                            _jsonWriter.WriteStartConstructor(_constructorName ?? string.Empty);
                            break;

                        case JsonTextWriterScopeType.Array:
                            _jsonWriter.WriteStartArray();
                            break;

                        case JsonTextWriterScopeType.Object:
                            _jsonWriter.WriteStartObject();
                            break;
                    }
                }
            }

            /// <summary>
            /// Ends the scope.
            /// </summary>
            private void EndScope()
            {
                if (_jsonWriter != null)
                {
                    switch (_type)
                    {
                        case JsonTextWriterScopeType.Constructor:
                            _jsonWriter.WriteEndConstructor();
                            break;

                        case JsonTextWriterScopeType.Array:
                            _jsonWriter.WriteEndArray();
                            break;

                        case JsonTextWriterScopeType.Object:
                            _jsonWriter.WriteEndObject();
                            break;
                    }
                }
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            void IDisposable.Dispose()
            {
                EndScope();
            }
        }

        /// <summary>
        /// Opens json scope.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="type">The type.</param>
        /// <param name="constructorName">The constructor name.</param>
        /// <returns></returns>
        internal static IDisposable OpenScope(this JsonTextWriter jsonWriter, JsonTextWriterScopeType type, string constructorName = null)
        {
            return new JsonTextWriterScope(jsonWriter, type, constructorName);
        }

        /// <summary>
        /// Gets a value indicating whether is empty.
        /// </summary>
        /// <param name="elementPosition">The element position.</param>
        /// <returns></returns>
        /// <value>
        ///   <c>true</c> if is empty; otherwise, <c>false</c>.
        /// </value>
        internal static bool IsEmpty(this ElementPosition elementPosition)
        {
            return elementPosition.Auto
                    && elementPosition.Height == 0
                    && elementPosition.Width == 0
                    && elementPosition.X == 0
                    && elementPosition.Y == 0;
        }

        /// <summary>
        /// Deletes the double specified character in string
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="character">The character.</param>
        /// <returns></returns>
        internal static string NormalizeText(string value, string character)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Replace(String.Concat(character, character), character);
            }

            return string.Empty;
        }

        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The writer.</param>
        public static void Render(RenderingContext renderingContext, VisualElement visualElement, TextWriter writer)
        {
            Render(renderingContext, visualElement, writer, null);
        }

        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="renderTo">The render to.</param>
        public static void Render(RenderingContext renderingContext, VisualElement visualElement, TextWriter writer, string renderTo)
        {
            if (writer == null)
            {
                return;
            }

            // If there is a valid visual element
            if (visualElement != null)
            {
                // Get the current control container
                VisualElement objControlContainer = ApplicationElement.CurrentContainer;

                // If there is a valid control container
                if (objControlContainer != null)
                {
                    // Add the current visual element
                    objControlContainer.Add(visualElement, false);
                }

                // Get control element
                ControlElement controlElement = visualElement as ControlElement;

                // If there is a valid control element
                if (controlElement != null)
                {
                    // Ensure control is created
                    controlElement.CreateControl();
                }

                // Initialize assemblies from which to load visual tree renderers
                HashSet<Assembly> assemblies = null;

                // If rendering context is valid
                if (renderingContext != null)
                {
                    // Get the root site assembly references
                    assemblies = renderingContext.RootSiteAssemblyReferences;
                }

                // Initialize renderers
                VisualTreeManager.Initialize(assemblies);

                // Render element code
                string strCode = VisualTreeManager.RenderElement(renderingContext, visualElement, renderTo);

                // Write the code
                writer.Write(strCode);
            }


        }

        /// <summary>
        /// Processes the events request.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="context">The context.</param>
        /// <param name="appId">The application identifier.</param>
        public static void ProcessEventsRequest(RenderingContext renderingContext, HttpContext context, string appId)
        {
            // Restore the application element
            ApplicationElement.RestoreApplicationElement(appId);

            // Increment revision
            ApplicationElement.IncrementRevision();

            // Create visual element render
            VisualElementRenderer objVisualElementRenderer = VisualTreeManager.GetRenderer(RenderingEnvironment.GetRenderingEnvironment(renderingContext), typeof(ApplicationElement)) as VisualElementRenderer;

            // If there is a valid visual element renderer
            if (objVisualElementRenderer != null)
            {
                // Render the current application element
                objVisualElementRenderer.RenderResult(renderingContext, ApplicationElement.Current, context);
            }
        }

        /// <summary>
        /// Writes the body style.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="controlElement">The control element.</param>
        internal static void WriteBodyStyle(JsonTextWriter jsonWriter, ControlElement controlElement)
        {
            // If there is a valid panel element
            if (controlElement != null)
            {
                // Get the window back color
                Color backColor = controlElement.BackColor;

                // If there is a valid color
                if (backColor != Color.Empty || controlElement.BackgroundImage != null)
                {
                    jsonWriter.WritePropertyName("bodyStyle");

                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        if (controlElement.BackgroundImage != null)
                        {
                            // Write the background image property
                            jsonWriter.WriteProperty("backgroundImage", "url(" + controlElement.BackgroundImage.Source + ")");
                            jsonWriter.WriteProperty("backgroundSize", "cover");
                        }
                        else if (backColor != Color.Empty)
                        {
                            // Write the back color property
                            jsonWriter.WriteProperty("backgroundColor", ColorTranslator.ToHtml(backColor));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the site URL.
        /// </summary>
        /// <returns></returns>
        internal static string GetSiteUrl()
        {
            HttpContext objHttpContext = RenderingContext.CurrentHttpContext;
            if (objHttpContext != null)
            {
                HttpRequest objHttpRequest = objHttpContext.Request;
                if (objHttpRequest != null)
                {
                    return objHttpRequest.RawUrl;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Determines whether is browser supports the specified browser capabilities.
        /// </summary>
        /// <param name="browserCapabilities">The browser capabilities.</param>
        /// <returns></returns>
        internal static bool IsBrowserSupports(BrowserCapabilities browserCapabilities)
        {
            HttpContext currentContext = RenderingContext.CurrentHttpContext;

            if (currentContext == null)
                return false;

            HttpRequest request = currentContext.Request;

            if (request == null)
                return false;

            HttpBrowserCapabilities browser = request.Browser;

            if (browser == null)
                return false;

            switch (browserCapabilities)
            {
                case BrowserCapabilities.MultiLineTabs:
                    return !browser.IsMobileDevice;
            }

            return false;
        }

        /// <summary>
        /// Gets the json writer.
        /// </summary>
        /// <param name="sb">The sb.</param>
        /// <returns></returns>
        internal static JsonTextWriter GetJsonWriter(Text.StringBuilder sb)
        {
            JsonTextWriter writer = System.Web.VisualTree.Rendering.JsonExtensions.CreateWriter(new StringWriter(sb));

            return writer;
        }

        /// <summary>
        /// Converts the type of to script.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <returns></returns>
        internal static string ConvertToScriptType(string typeName)
        {
            switch (typeName)
            {
                case "int":
                case "Int32":
                case "Int16":
                case "Int64":
                case "UInt32":
                case "UInt16":
                case "UInt64":
                    return "int";

                case "Double":
                case "double":
                case "float":
                    return "float";

                case "DateTime":
                    return "date";

                case "Boolean":
                case "bool":
                    return "bool";
            }

            return "string";
        }
    }
}
