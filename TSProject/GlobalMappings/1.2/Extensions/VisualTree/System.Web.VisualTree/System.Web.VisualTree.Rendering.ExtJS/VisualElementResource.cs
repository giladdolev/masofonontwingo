﻿using Newtonsoft.Json;
using System.IO;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering
{
    /// <summary>
    /// Provides support for exposing element resources.
    /// </summary>
    public class VisualElementResource : IVisualElementRenderer
    {
        /// <summary>
        /// The visual element
        /// </summary>
        private readonly VisualElement _visualElement;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementResource" /> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public VisualElementResource(IVisualElement visualElement)
        {
            _visualElement = visualElement as VisualElement;
        }

        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public VisualElement Element
        {
            get { return _visualElement; }
        }



        /// <summary>
        /// Enables processing of the result of an action method by a custom type that inherits from the <see cref="T:System.Web.Mvc.ActionResult" /> class.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="httpContext">The HTTP context in which the result is executed.</param>
        /// <exception cref="System.ArgumentNullException">context</exception>
        public virtual void ExecuteResult(RenderingContext renderingContext, HttpContextBase httpContext)
        {
            // If there is a valid http context
            if (httpContext != null)
            {
                // Get response
                HttpResponseBase response = httpContext.Response;

                // If there is a valid response
                if (response != null)
                {
                    // Execute result
                    RenderResource(renderingContext, response);
                }
            }
        }

        /// <summary>
        /// Renders the json resource.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="response">The response.</param>
        protected virtual void RenderJsonResource(RenderingContext renderingContext, HttpResponseBase response)
        {
            // If there is a valid response
            if (response != null)
            {
                // Render json resource
                RenderJsonResource(renderingContext, response.Output);
                
                // Flush response
                response.Flush();
            }
        }

        /// <summary>
        /// Renders the json resource.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="textWriter">The text writer.</param>
        protected virtual void RenderJsonResource(RenderingContext renderingContext, TextWriter textWriter)
        {
            

            // If there is a valid writer
            if (textWriter != null)
            {
                // Create the json writer
                JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(textWriter);
                    
                // Write the json result
                RenderJsonResource(renderingContext, jsonWriter);

                // Flush result
                jsonWriter.Flush();
            }
        }


        /// <summary>
        /// Renders the json resource.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderJsonResource(RenderingContext renderingContext, JsonTextWriter jsonWriter)
        {
            jsonWriter.WriteEmptyObject();
        }

        /// <summary>
        /// Renders the resource.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="response">The response.</param>
        protected virtual void RenderResource(RenderingContext renderingContext, HttpResponseBase response)
        {
            // Execute json result
            RenderJsonResource(renderingContext, response);
        }

        /// <summary>
        /// Revises the renderer.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public IVisualElementRenderer ReviseRenderer(RenderingContext renderingContext, IVisualElement visualElement)
        {
            return this;
        }

        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="renderTo">The render to.</param>
        /// <returns></returns>
        public string Render(RenderingContext renderingContext, IVisualElement visualElement, string renderTo = null)
        {
            return null;
        }

        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="request">The http request.</param>
        /// <returns></returns>
        public IVisualElementRenderer RenderResult(RenderingContext renderingContext, IVisualElement visualElement, HttpRequestBase request = null)
        {
            return null;
        }

        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public IVisualElementRenderer RenderResourceResult(RenderingContext renderingContext, IVisualElement visualElement, string resourceId)
        {
            return null;
        }
    }
}

