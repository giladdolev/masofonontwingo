﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Utilities
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(System.Web.VisualTree.Utilities.BrowserUtils), "System.Web.VisualTree.Rendering.ExtJS.DefaultComponentExtJSRenderer", "", "")]
    [ExtJSMethodDescription("Reload", MethodCode = "document.location.reload();")]
    [ExtJSMethodDescription("NavigateUrl", MethodCode = "document.location.href = <#= data #>;")]
    [ExtJSMethodDescription("OpenWindow", MethodCode = "window.open(<#= data #>);")]
    [ExtJSMethodDescription("SetDocumentTitle", MethodCode = "document.title = <#= data #>;")]
    [ExtJSMethodDescription("AddHistoryToken", MethodCode = "VT_addToHistory(<#= data #>);")]
    [ExtJSMethodDescription("RaiseDelayedEvents", MethodCode = "VT_raiseDelayedEvents(<#= data #>);")]
    public class BrowserUtilsExtJSRenderer : BrowserUtilsExtJSRendererBase 
    {
    }   
}
   