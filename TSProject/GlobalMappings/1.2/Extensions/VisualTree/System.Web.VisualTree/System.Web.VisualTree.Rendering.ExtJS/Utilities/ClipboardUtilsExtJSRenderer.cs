﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Utilities
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(System.Web.VisualTree.Utilities.ClipboardUtils), "System.Web.VisualTree.Rendering.ExtJS.DefaultComponentExtJSRenderer", "", "")]
    [ExtJSMethodDescription("CopyText", MethodCode = "VT_CopyText(<#= data #>)")]
    public class ClipboardElementExtJSRenderer : ClipboardUtilsExtJSRendererBase
    {
      
    }
}
