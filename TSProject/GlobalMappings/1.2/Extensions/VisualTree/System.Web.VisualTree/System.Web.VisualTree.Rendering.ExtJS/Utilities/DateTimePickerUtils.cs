﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    public static class DateTimePickerUtils
    {
        /// <summary>
        /// Parse Format Date 
        /// </summary>
        /// <param name="element">Date element</param>
        /// <returns>correct sencha format </returns>
        internal static string ParseGridDateTimePickerColumnFormat(GridDateTimePickerColumn element)
        {
            return ConvertFormatToString(element.Format, element.CustomFormat);
        }

        private static string ConvertFormatToString(DateTimePickerFormat format, string customFormat)
        {

            switch (format)
            {
                case DateTimePickerFormat.Short:
                    return "d/m/Y";

                case DateTimePickerFormat.Long:
                    return "D d M Y";

                case DateTimePickerFormat.Line:
                    return "d-m-Y";
                case DateTimePickerFormat.Custom:
                    if (!string.IsNullOrWhiteSpace(customFormat))
                    {
                        return ConvertToClientCustomFormat(customFormat);
                    }
                    break;
            }
            return "d/m/y";
        }
       
        /// <summary>
        /// Parse Format Date 
        /// </summary>
        /// <param name="element">Date element</param>
        /// <returns>correct sencha format </returns>
        internal static string ParseDateTimePickerFormat(DateTimePickerElement element)
        {
           return ConvertFormatToString(element.Format, element.CustomFormat);
        }

        /// <summary>
        /// method to return the correct date time with format 
        /// </summary>
        /// <param name="value">date time value </param>
        /// <returns>formatted date time </returns>
        internal static string ParseDate(DateTimePickerElement element)
        {
            if (element != null)
            {
                switch (element.Format)
                {
                    case DateTimePickerFormat.Custom:
                        if (!string.IsNullOrWhiteSpace(element.CustomFormat))
                            return element.Value.ToString(element.CustomFormat, CultureInfo.CurrentUICulture);
                        else
                            return element.Value.ToString();
                    case DateTimePickerFormat.Default:
                        return element.Value.ToString("d");
                    case DateTimePickerFormat.Long:
                        return element.Value.ToString("D");
                    case DateTimePickerFormat.Short:
                        return element.Value.ToString("d");
                    case DateTimePickerFormat.Time:
                        return element.Value.ToString("t");
                    default:
                        return element.Value.ToString("d");
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// method to Convert server string format to client suitable format
        /// </summary>
        /// <param name="serverFormat">the custom format used by the server</param>
        /// <returns>the format for the client component</returns>
        internal static string ConvertToClientCustomFormat(string serverFormat)
        {
            string returnVal = "d/m/Y"; //default DD/MM/YYYY
            //Temp Chars table:
            // @ = d, # = M, $ = y, ^ = h / ! = H  (hours), & = m (minutes), s (secondes). 
            //
            string tmp = serverFormat.Replace("d", "@").Replace("M", "#").Replace("y", "$").Replace("h", "^").Replace("m", "&").Replace("ss","s").Replace("H","!"); //creating temp string to Parse by.

            serverFormat = tmp.Replace("@@@@", "l").Replace("@@@", "D").Replace("@@", "d").Replace("@", "j"); //replacing temp string - DAYS formatting
            tmp = serverFormat.Replace("####", "F").Replace("###", "M").Replace("##", "m").Replace("#", "n"); //replacing temp string - MONTH formatting
            serverFormat = tmp.Replace("$$$$", "Y").Replace("$$", "y"); //replacing temp string - YEARS formatting
            tmp = serverFormat.Replace("^^", "h").Replace("^", "g").Replace("!!","H").Replace("!","G"); //replacing temp string - HOURS formatting

            //replacing temp string - MINUTES formatting
            int firstMinuteChar = tmp.IndexOf('&');
            serverFormat = tmp.Replace("&", "");
            tmp = firstMinuteChar != -1? serverFormat.Insert(firstMinuteChar, "i") : tmp;

            //Cleaning the final string
            returnVal = tmp.Replace("@", "").Replace("#", "").Replace("$", "").Replace("^", "").Replace("&", "").Replace("!","").Replace("tt","A") ;

            return returnVal;
        }
    }
}


// Date Client Custom Format Table

//         Format      Description                                                               Example returned values
// ------      -----------------------------------------------------------------------   -----------------------
//   d         Day of the month, 2 digits with leading zeros                             01 to 31
//   D         A short textual representation of the day of the week                     Mon to Sun
//   j         Day of the month without leading zeros                                    1 to 31
//   l         A full textual representation of the day of the week                      Sunday to Saturday
//   N         ISO-8601 numeric representation of the day of the week                    1 (for Monday) through 7 (for Sunday)
//   S         English ordinal suffix for the day of the month, 2 characters             st, nd, rd or th. Works well with j
//   w         Numeric representation of the day of the week                             0 (for Sunday) to 6 (for Saturday)
//   z         The day of the year (starting from 0)                                     0 to 364 (365 in leap years)
//   W         ISO-8601 week number of year, weeks starting on Monday                    01 to 53
//   F         A full textual representation of a month, such as January or March        January to December
//   m         Numeric representation of a month, with leading zeros                     01 to 12
//   M         A short textual representation of a month                                 Jan to Dec
//   n         Numeric representation of a month, without leading zeros                  1 to 12
//   t         Number of days in the given month                                         28 to 31
//   L         Whether it&#39;s a leap year                                                  1 if it is a leap year, 0 otherwise.
//   o         ISO-8601 year number (identical to (Y), but if the ISO week number (W)    Examples: 1998 or 2004
//             belongs to the previous or next year, that year is used instead)
//   Y         A full numeric representation of a year, 4 digits                         Examples: 1999 or 2003
//   y         A two digit representation of a year                                      Examples: 99 or 03
//   a         Lowercase Ante meridiem and Post meridiem                                 am or pm
//   A         Uppercase Ante meridiem and Post meridiem                                 AM or PM
//   g         12-hour format of an hour without leading zeros                           1 to 12
//   G         24-hour format of an hour without leading zeros                           0 to 23
//   h         12-hour format of an hour with leading zeros                              01 to 12
//   H         24-hour format of an hour with leading zeros                              00 to 23
//   i         Minutes, with leading zeros                                               00 to 59
//   s         Seconds, with leading zeros                                               00 to 59
//   u         Decimal fraction of a second                                              Examples:
//             (minimum 1 digit, arbitrary number of digits allowed)                     001 (i.e. 0.001s) or
//                                                                                       100 (i.e. 0.100s) or
//                                                                                       999 (i.e. 0.999s) or
//                                                                                       999876543210 (i.e. 0.999876543210s)
//   O         Difference to Greenwich time (GMT) in hours and minutes                   Example: +1030
//   P         Difference to Greenwich time (GMT) with colon between hours and minutes   Example: -08:00
//   T         Timezone abbreviation of the machine running the code                     Examples: EST, MDT, PDT ...
//   Z         Timezone offset in seconds (negative if west of UTC, positive if east)    -43200 to 50400
//   c         ISO 8601 date represented as the local time with an offset to UTC appended.
//             Notes:                                                                    Examples:
//             1) If unspecified, the month / day defaults to the current month / day,   1991 or
//                the time defaults to midnight, while the timezone defaults to the      1992-10 or
//                browser's timezone. If a time is specified, it must include both hours 1993-09-20 or
//                and minutes. The "T" delimiter, seconds, milliseconds and timezone     1994-08-19T16:20+01:00 or
//                are optional.                                                          1995-07-18T17:21:28-02:00 or
//             2) The decimal fraction of a second, if specified, must contain at        1996-06-17T18:22:29.98765+03:00 or
//                least 1 digit (there is no limit to the maximum number                 1997-05-16T19:23:30,12345-0400 or
//                of digits allowed), and may be delimited by either a '.' or a ','      1998-04-15T20:24:31.2468Z or
//             Refer to the examples on the right for the various levels of              1999-03-14T20:24:32Z or
//             date-time granularity which are supported, or see                         2000-02-13T21:25:33
//             http://www.w3.org/TR/NOTE-datetime for more info.                         2001-01-12 22:26:34
//   C         An ISO date string as implemented by the native Date object's             1962-06-17T09:21:34.125Z
//             [Date.toISOString](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString)
//             method. This outputs the numeric part with *UTC* hour and minute
//             values, and indicates this by appending the `'Z'` timezone
//             identifier.
//   U         Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)                1193432466 or -2138434463
//   MS        Microsoft AJAX serialized dates                                           \/Date(1238606590509)\/ (i.e. UTC milliseconds since epoch) or
//                                                                                       \/Date(1238606590509+0800)\/
//   time      A javascript millisecond timestamp                                        1350024476440
//   timestamp A UNIX timestamp (same as U)                                              1350024866

