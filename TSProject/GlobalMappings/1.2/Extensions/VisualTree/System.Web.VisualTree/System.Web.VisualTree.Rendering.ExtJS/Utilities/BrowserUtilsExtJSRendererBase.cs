using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Utilities
{

	public abstract class BrowserUtilsExtJSRendererBase : System.Web.VisualTree.Rendering.ExtJS.DefaultComponentExtJSRenderer
	{
		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "Reload":
					return this.ExtractReloadMethodCallbackData(context, eventObject);						
				case "NavigateUrl":
					return this.ExtractNavigateUrlMethodCallbackData(context, eventObject);						
				case "OpenWindow":
					return this.ExtractOpenWindowMethodCallbackData(context, eventObject);						
				case "SetDocumentTitle":
					return this.ExtractSetDocumentTitleMethodCallbackData(context, eventObject);						
				case "AddHistoryToken":
					return this.ExtractAddHistoryTokenMethodCallbackData(context, eventObject);						
				case "RaiseDelayedEvents":
					return this.ExtractRaiseDelayedEventsMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderStaticMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, string methodName, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "Reload":
					this.RenderReloadMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
				case "NavigateUrl":
					this.RenderNavigateUrlMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
				case "OpenWindow":
					this.RenderOpenWindowMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
				case "SetDocumentTitle":
					this.RenderSetDocumentTitleMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
				case "AddHistoryToken":
					this.RenderAddHistoryTokenMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
				case "RaiseDelayedEvents":
					this.RenderRaiseDelayedEventsMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
                default:
                    base.RenderStaticMethod(context, methodInvoker, methodName, methodData, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the Reload method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderReloadMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
							var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
				}

		}


		/// <summary>
        /// Gets the method Reload callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractReloadMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the NavigateUrl method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderNavigateUrlMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
							var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
				builder.AppendFormattedRawValue(@"document.location.href = {0};",  data );
				}

		}


		/// <summary>
        /// Gets the method NavigateUrl callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractNavigateUrlMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the OpenWindow method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderOpenWindowMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
							var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
				builder.AppendFormattedRawValue(@"window.open({0});",  data );
				}

		}


		/// <summary>
        /// Gets the method OpenWindow callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractOpenWindowMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetDocumentTitle method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetDocumentTitleMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
							var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
				builder.AppendFormattedRawValue(@"document.title = {0};",  data );
				}

		}


		/// <summary>
        /// Gets the method SetDocumentTitle callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetDocumentTitleMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the AddHistoryToken method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderAddHistoryTokenMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
							var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
				builder.AppendFormattedRawValue(@"VT_addToHistory({0});",  data );
				}

		}


		/// <summary>
        /// Gets the method AddHistoryToken callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractAddHistoryTokenMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        internal static class LastraiseDelayedEvents
        {
            public static DateTime saveTime { get; set; }
            public static DateTime retriveTime()
            {
                return saveTime;
            }
            static LastraiseDelayedEvents()
            {
                saveTime = DateTime.Now;
            }
            public static int saveTime1 = 0;


        }
        /// Renders the RaiseDelayedEvents method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderRaiseDelayedEventsMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            var builder = getJavaScriptBuilder(true);
            if (builder != null)
            {
                System.Object data = (System.Object)methodData;

                TimeSpan diff = LastraiseDelayedEvents.retriveTime() - DateTime.Now;
                int millisceonds = Math.Abs((int)diff.TotalMilliseconds);
                if (millisceonds >= 1000)
                {
                    builder.AppendFormattedRawValue(@"VT_raiseDelayedEvents({0});", data);
                }
                LastraiseDelayedEvents.saveTime = DateTime.Now;
            }
        }


        /// <summary>
        /// Gets the method RaiseDelayedEvents callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractRaiseDelayedEventsMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}