﻿using Newtonsoft.Json;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Engine;
using System.IO;
using System.Web.UI;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Data;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [ExtJSRendererDescription(typeof(ControlElement), "VisualElementExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("AutoSize", PropertyInitializeCode = "Style.overflow = <#= LabelElementExtJSRenderer.ReviseAutoSizeStyle(element) #>", ClientNotDefaultCode = "!element.AutoSize", PropertyChangeCode = "this.setStyle('overflow','<#= LabelElementExtJSRenderer.ReviseAutoSizeStyle(element) #>');")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "Style.background = <#= ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != Color.Empty", PropertyChangeCode = "this.setStyle('background','<#= ColorTranslator.ToHtml(element.BackColor) #>');")]
    [ExtJSPropertyDescription("BackgroundImage", PropertyInitializeCode = "Style.backgroundImage = <#= ControlElementExtJSRenderer.ReviseBackground(renderingContext, element) #>", ClientDefault = null, PropertyChangeCode = "this.setStyle('backgroundImage','<#= ControlElementExtJSRenderer.ReviseBackground(context, element) #>');")]
    [ExtJSPropertyDescription("BackgroundImageLayout", PropertyInitializeCode = "Style.backgroundSize = <#= ControlElementExtJSRenderer.ReviseBackgroundSize(element.BackgroundImageLayout) #>, Style.backgroundRepeat = <#= ControlElementExtJSRenderer.ReviseBackgroundRepeat(element.BackgroundImageLayout) #> ,Style.backgroundPosition = <#= ControlElementExtJSRenderer.ReviseBackgroundPosition(element.BackgroundImageLayout) #> ", ClientNotDefaultCode = "element.BackgroundImage != null", PropertyChangeCode = " this.setStyle('backgroundSize','<#= ControlElementExtJSRenderer.ReviseBackgroundSize(element.BackgroundImageLayout) #>');this.setStyle('backgroundRepeat','<#= ControlElementExtJSRenderer.ReviseBackgroundRepeat(element.BackgroundImageLayout) #>');this.setStyle('backgroundPosition','<#= ControlElementExtJSRenderer.ReviseBackgroundPosition(element.BackgroundImageLayout) #>');")]
    [ExtJSPropertyDescription("CssClass", PropertyInitializeCode = "cls = <#= element.CssClass #>")]
    [ExtJSPropertyDescription("DataMember", PropertyInitializeCode = "dataIndex = <#= element.DataMember #>")]
    [ExtJSPropertyDescription("Draggable", PropertyInitializeCode = "draggable = <#=element.Draggable #>", ClientNotDefaultCode = "element.Draggable", PropertyChangeCode = "draggable =<#=element.Draggable #>")]
    [ExtJSPropertyDescription("EnableKeyEvents", PropertyInitializeCode = "enableKeyEvents =<#= ControlElementExtJSRenderer.ReviseEnableKeyPress(element) #>", ClientNotDefaultCode = "element.HasKeyPressListeners")]
    [ExtJSPropertyDescription("Font", PropertyInitializeCode = "Style.font = <#= ControlElementExtJSRenderer.ReviseFont(element) #>,Style.textDecoration = <#= ControlElementExtJSRenderer.ReviseFontDecoration(element) #>", ClientNotDefaultCode = "ControlElementExtJSRenderer.IsNonDefaultFont(element.Font)", PropertyChangeCode = "this.setStyle('font','<#= ControlElementExtJSRenderer.ReviseFont(element) #>');this.setStyle('textDecoration','<#= ControlElementExtJSRenderer.ReviseFontDecoration(element) #>');")]
    [ExtJSPropertyDescription("ForeColor", PropertyInitializeCode = "Style.color = <#= ColorTranslator.ToHtml(element.ForeColor) #>", PropertyChangeCode = "this.setStyle('color','<#= ColorTranslator.ToHtml(element.ForeColor) #>');")]
    [ExtJSPropertyDescription("Height", PropertyInitializeCode = "height = <#= element.PixelHeight #>", ClientDefault = 0, PropertyChangeCode = "this.setHeight(<#= element.PixelHeight#>);")]
    [ExtJSPropertyDescription("liveDrag", PropertyInitializeCode = "liveDrag = true")]
    [ExtJSPropertyDescription("IsFocused", PropertyInitializeCode = "hasFocus = <#= element.IsFocused #>", ClientNotDefaultCode = "element.IsFocused != false")]
    [ExtJSPropertyDescription("Margin", PropertyInitializeCode = "bodyPadding=<#= ControlElementExtJSRenderer.RevisePadding(element.Margin) #>")]
    [ExtJSPropertyDescription("MaximumSize", PropertyInitializeCode = "maxWidth=<#= element.MaximumSize.Width #>,maxHeight=<#= element.MaximumSize.Height #>", ClientNotDefaultCode = "element.MaximumSize.Width != 0 || element.MaximumSize.Height !=0", PropertyChangeCode = "this.setMaxWidth(<#=element.MaximumSize.Width #>);this.setMaxHeight(<#=element.MaximumSize.Height #>);")]
    [ExtJSPropertyDescription("MinimumSize", PropertyInitializeCode = "minWidth=<#= element.MinimumSize.Width #>,minHeight=<#= element.MinimumSize.Height #>", ClientNotDefaultCode = "element.MinimumSize.Width != 0 || element.MinimumSize.Height !=0", PropertyChangeCode = "this.setMinWidth(<#=element.MinimumSize.Width #>);this.setMinHeight(<#=element.MinimumSize.Height #>);")]
    [ExtJSPropertyDescription("Padding", PropertyInitializeCode = "padding=<#= ControlElementExtJSRenderer.RevisePadding(element.Padding) #>", PropertyChangeCode = "this.setStyle({padding:'<#= ControlElementExtJSRenderer.FormmatPaddingString(element)#>'}) ")]
    [ExtJSPropertyDescription("ReadOnly")]
    [ExtJSPropertyDescription("Resizable", PropertyInitializeCode = "resizable = <#= element.Resizable #>", ClientNotDefaultCode = "element.Resizable == true")]
    [ExtJSPropertyDescription("RightToLeft", PropertyInitializeCode = "rtl = <#= (element.RightToLeft == RightToLeft.Yes) ? true : false #> ", ClientNotDefaultCode = "element.RightToLeft != RightToLeft.Inherit")]
    [ExtJSPropertyDescription("RightToLeftDecoration", PropertyInitializeCode = " Style.direction = 'rtl',Style.text-align = 'right' ", ClientNotDefaultCode = "element.RightToLeft == RightToLeft.Yes")]
    [ExtJSPropertyDescription("TabIndex", PropertyInitializeCode = " tabIndex = <#= element.TabIndex #>", PropertyChangeCode = "this.setTabIndex(<#= ControlElementExtJSRenderer.ReviseTabIndex(element)#>);", ClientNotDefaultCode = "element.TabStop && element.TabIndex >= 0")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "text = <#= GetFormatedTextProperty(element, GetDisplayText(element.Text)) #>", PropertyChangeCode = "this.setText(<#= GetFormatedTextProperty(element, GetDisplayText(element.Text)) #>)", ClientNotDefaultCode = "element.Text != string.Empty ")]
    [ExtJSPropertyDescription("ToolTipText", PropertyInitializeCode = " autoEl= {tag: 'label','data-qtip': '<#= element.ToolTipText #>'}", PropertyChangeCode = "this.ariaEl.dom.setAttribute('data-qtip','<#= element.ToolTipText#>');")]
    [ExtJSPropertyDescription("Visible", PropertyInitializeCode = "hidden = <#= !element.Visible #>", ClientNotDefaultCode = "!element.Visible", PropertyChangeCode = "this.setHidden(<#=JavaScriptCode.GetBool( !element.Visible )#>);")]
    [ExtJSPropertyDescription("WaitMaskDisabled", PropertyInitializeCode = "waitMaskDisabled = <#= element.WaitMaskDisabled #>", ClientNotDefaultCode = "element.WaitMaskDisabled", PropertyChangeCode = "this.waitMaskDisabled = <#= JavaScriptCode.GetBool( element.WaitMaskDisabled )#>;")]
    [ExtJSPropertyDescription("Width", PropertyInitializeCode = "width = <#= element.PixelWidth #>", ClientDefault = 0, PropertyChangeCode = "this.setWidth(<#= element.PixelWidth#>);")]

    [ExtJSPropertyDescription("ZIndex", PropertyInitializeCode = " Style.zIndex = <#= ControlElementExtJSRenderer.ReviseZIndex(element) #>", ClientDefault = -1, PropertyChangeCode = "this.setStyle('zIndex','<#= ControlElementExtJSRenderer.ReviseZIndex(element) #>');")]

    [ExtJSMethodDescription("GetActiveControl", MethodCode = "<#resource:GetActiveControl.js#>", ServerCallbackDataCode = "ControlElementExtJSRenderer.GetControlId(this.value.ToString())")]
    [ExtJSMethodDescription("Focus", MethodCode = "var control = this; if(control != null){control.focus();}")]
    [ExtJSMethodDescription("SetActiveControl", MethodCode = "<#resource:SetActiveControl.js#>", MethodDataType = typeof(string))]
    [ExtJSMethodDescription("SetError", MethodCode = "this.setActiveError(<#= data #>);")]
    [ExtJSMethodDescription("SetLoading", MethodCode = "this.setLoading(<#= data #>); if(Ext.isBoolean(<#= data #>) &&  this.view && this.view.loadMask ){ if(<#= data #>){this.view.loadMask.bindStore && this.view.loadMask.bindStore(this.getStore()) } else {this.view.loadMask.unbindStoreListeners(this.getStore()); }} ")]

    [ExtJSEventDescription("Afterrender", ClientEventName = "afterrender", ClientEventHandlerParameterNames = "cmp", ClientCode = "<#resource:ShowContextMenu.js#>", ClientEventBehaviorType = ExtJSEventBehaviorType.Queued)]
    [ExtJSEventDescription("Click", ClientEventName = "click", ClientEventHandlerParameterNames = "")]
    [ExtJSEventDescription("DragOver", ClientEventName = "move", ClientEventHandlerParameterNames = "view, x, y, eOpts", ServerEventArgsCreateCode = "new DragEventArgs(this.value1.ToInt32(),this.value2.ToInt32())", ClientHandlerCallbackData = "value1 = x, value2 = y ")]
    [ExtJSEventDescription("GotFocus", "focusenter", ClientEventHandlerParameterNames = "view , event , eOpts ")]
    [ExtJSEventDescription("KeyDown", ClientEventName = "keydown", ClientEventHandlerParameterNames = "view, e, t, eOpts", ClientHandlerCallbackData = "key = e.getKey()", ServerEventArgsCreateCode = "new KeyDownEventArgs((Keys)this.key.ToInt32())")]
    [ExtJSEventDescription("KeyPress", ClientEventName = "keypress", ClientEventHandlerParameterNames = "view, e, eOpts", ServerEventArgsCreateCode = "new KeyPressEventArgs(Convert.ToInt32(this.key))", ClientHandlerCallbackData = "key = e.getKey()")]
    [ExtJSEventDescription("Leave", "blur", ClientCode = "<#= ControlElementExtJSRenderer.ReviseValidatedEvent(element) #>")]
    [ExtJSEventDescription("LostFocus", "focusleave", ClientEventHandlerParameterNames = "view , event , eOpts")]
    [ExtJSEventDescription("MouseHover", ClientEventName = "mouseover")]
    [ExtJSEventDescription("Resize", ClientEventName = "resize", ClientEventHandlerParameterNames = " view , width , height , oldWidth , oldHeight , eOpts", ClientHandlerCallbackData = "width = width , height= height", ServerEventArgsCreateCode = "new ResizeEventArgs(this.width.ToInt32(),this.height.ToInt32())")]
    [ExtJSEventDescription("Validated", ClientEventName = "validated")]
    [ExtJSPropertyDescription("CustomUI", PropertyInitializeCode = "ui = <#= element.CustomUI #>", PropertyChangeCode = "this.setUi(<#= element.CustomUI#>);")]

    public abstract class ControlElementExtJSRenderer : ControlElementExtJSRendererBase
    {
        /// <summary>
        /// Revises the auto size style.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static string ReviseAutoSizeStyle(ControlElement element)
        {
            return element.AutoSize ? "visible" : "hidden";
        }


        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Try casting the element in to a control element
            ControlElement controlElement = visualElement as ControlElement;

            // If casting is valid
            if (controlElement != null)
            {
                // Render layout properties
                RenderLayoutProperties(renderingContext, controlElement, jsonWriter);

                // Write viewConfig property
                RenderViewConfigProperty(renderingContext, controlElement, jsonWriter);

                // Write style property
                RenderStyleProperty(renderingContext, controlElement, jsonWriter);

                // Write field style property
                RenderFieldStyleProperty(renderingContext, controlElement, jsonWriter);

                // Write style property
                RenderBodyStyleProperty(renderingContext, visualElement, jsonWriter);

                // Write header object
                RenderHeaderObject(renderingContext, controlElement, jsonWriter);

                // Render 'enabled' property
                RenderEnabledProperty(renderingContext, controlElement, jsonWriter);


                // Render the 'border' property
                RenderBorderProperty(renderingContext, controlElement, jsonWriter);

                // Render the 'autoScroll' property
                RenderScrollProperty(renderingContext, controlElement, jsonWriter);

                // Render the 'tabIndex' property
                RenderTabIndexProperty(renderingContext, controlElement, jsonWriter);

                // Render the 'label' property
                RenderTouchFieldLabelProperty(jsonWriter, controlElement);

            }
        }

        /// <summary>
        /// Renders the Controls property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderControlsPropertyChange(ExtJSRenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // Render the collection change actions
                VisualElementExtJSCollectionRenderer.RenderCollectionChangeActions<VisualElementExtJSCollectionRenderer>(context, controlElement.Controls, builder, componentVariable);
            }
        }

        /// <summary>
        /// Renders the tool tip text property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderToolTipTextProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the toolbar element
            ToolBarItem element = visualElement as ToolBarItem;

            // If the element is valid
            if (element != null && !String.IsNullOrEmpty(element.ToolTipText))
            {
                // Render tool tip property
                base.RenderToolTipTextProperty(context, visualElement, jsonWriter);

                // Write title type
                //jsonWriter.WriteProperty("tooltipType", "title");
            }
            base.RenderToolTipTextProperty(context, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the header object.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderHeaderObject(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {
            RenderBlockProperty(renderingContext, controlElement, jsonWriter, "header", RenderHeaderProperties);
        }

        /// <summary>
        /// Renders the header object properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="headerWriter">The style writer.</param>
        public virtual void RenderHeaderProperties(RenderingContext renderingContext, VisualElement controlElement, JsonTextWriter headerWriter)
        {

        }

        /// <summary>
        /// Renders the touch field label property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="controlElement">The control element.</param>
        protected virtual void RenderTouchFieldLabelProperty(JsonTextWriter jsonWriter, ControlElement controlElement)
        {
            // Get the field element
            ITFormFieldElement touchFormField = controlElement as ITFormFieldElement;

            // If is a valid field
            if (touchFormField != null && !string.IsNullOrEmpty(touchFormField.Label))
            {
                // Render field label
                jsonWriter.WriteProperty("label", touchFormField.Label);
            }
        }

        /// <summary>
        /// Renders the scroll property for the current control.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderScrollProperty(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the style font property.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseFont(ControlElement controlElement)
        {
            string fontStyle = "";

            if (controlElement != null)
            {
                Font font = controlElement.Font;
                if (font != null)
                {
                    if (font.Bold)
                        fontStyle += "bold ";
                    if (font.Italic)
                        fontStyle += "italic ";


                    if (font.SizeInPoints > 0)
                        fontStyle += font.SizeInPoints + "pt ";

                    if (font.FontFamily != null && !string.IsNullOrEmpty(font.FontFamily.Name))
                        fontStyle += font.FontFamily.Name;
                }
            }

            return fontStyle;
        }


        /// <summary>
        /// Get Control Id .
        /// </summary>
        /// <param name="controlId"></param>
        /// <returns></returns>
        internal static string GetControlId(string controlId)
        {
            return controlId;
        }


        /// <summary>
        /// Revises the font decoration.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseFontDecoration(ControlElement controlElement)
        {
            string fontStyle = "";

            if (controlElement != null)
            {
                Font font = controlElement.Font;
                if (font != null)
                {
                    if (font.Underline)
                        fontStyle += "underline";
                    if (font.Strikeout)
                        fontStyle += " line-through";
                }
            }
            return fontStyle.TrimStart();
        }

        /// <summary>
        /// Revises the border style.
        /// </summary>
        /// <param name="borderStyle">The border style.</param>
        /// <param name="color">The color.</param>
        /// <param name="borderWidth">Width of the border.</param>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        internal static string ReviseBorderInitializeStyle(ControlElement controlElement)
        {
            string color = GetBorderColor(controlElement);
            string borderWidth = GetBorderWidth(controlElement);
            switch (controlElement.BorderStyle)
            {
                case BorderStyle.FixedSingle:
                    return String.Format("{0} solid {1}", borderWidth, color);
                case BorderStyle.Outset:
                    return String.Format("{0} outset", borderWidth);
                case BorderStyle.Fixed3D:
                case BorderStyle.Inset:
                    return String.Format("{0} inset", borderWidth);
                case BorderStyle.Dotted:
                    return String.Format("{0} dotted {1}", borderWidth, color);
                case BorderStyle.Dashed:
                    return String.Format("{0} dashed {1}", borderWidth, color);
                case BorderStyle.Solid:
                    return String.Format("{0} solid {1}", borderWidth, color);
                case BorderStyle.Double:
                    return String.Format("{0} double {1}", borderWidth, color);
                case BorderStyle.Groove:
                    return String.Format("{0} groove {1}", borderWidth, color);
                case BorderStyle.Ridge:
                    return String.Format("{0} ridge {1}", borderWidth, color);
                case BorderStyle.None:
                    return "0";
            }
            return "";
        }

        /// <summary>
        /// Revises the border bottom initialize style.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseBorderBottomInitializeStyle(ControlElement controlElement)
        {
            string color = GetBorderColor(controlElement);
            string borderWidth = GetBorderWidth(controlElement);
            switch (controlElement.BorderStyle)
            {
                case BorderStyle.ShadowBox:
                case BorderStyle.Underline:
                    return String.Format("{0} solid {1}", borderWidth, color);
            }
            return "";
        }
        /// <summary>
        /// Revises the border right initialize style.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseBorderRightInitializeStyle(ControlElement controlElement)
        {
            string color = GetBorderColor(controlElement);
            string borderWidth = GetBorderWidth(controlElement);
            if (controlElement.BorderStyle == BorderStyle.ShadowBox)
            {
                return String.Format("thick  solid {1}", borderWidth, color);
            }
            return "";
        }
        /// <summary>
        /// Gets the width of the border.
        /// </summary>
        /// <param name="fieldElement">The field element.</param>
        /// <returns></returns>
        private static string GetBorderWidth(ControlElement fieldElement)
        {
            return "1px";
        }

        /// <summary>
        /// Gets the color of the border.
        /// </summary>
        /// <param name="fieldElement">The field element.</param>
        /// <returns></returns>
        private static string GetBorderColor(ControlElement fieldElement)
        {
            return ColorTranslator.ToHtml(fieldElement.BorderColor);
        }
        /// <summary>
        /// Revises the border style.
        /// </summary>
        /// <param name="borderStyle">The border style.</param>
        /// <param name="color">The color.</param>
        /// <param name="borderWidth">Width of the border.</param>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        internal static JavaScriptCode ReviseBorderStyle(ControlElement controlElement)
        {
            string color = GetBorderColor(controlElement);
            string borderWidth = GetBorderWidth(controlElement);
            string returnValue = "border:'solid 0px'";
            switch (controlElement.BorderStyle)
            {
                case BorderStyle.FixedSingle:
                    returnValue = String.Format("border:'{0} solid {1}'", borderWidth, color);
                    break;
                case BorderStyle.Outset:
                    returnValue = String.Format("border:'{0} outset'", borderWidth);
                    break;
                case BorderStyle.Fixed3D:
                case BorderStyle.Inset:
                    returnValue = String.Format("border:'{0} inset'", borderWidth);
                    break;
                case BorderStyle.Dotted:
                    returnValue = String.Format("border:'{0} dotted {1}'", borderWidth, color);
                    break;
                case BorderStyle.Dashed:
                    returnValue = String.Format("border:'{0} dashed {1}'", borderWidth, color);
                    break;
                case BorderStyle.Solid:
                    returnValue = String.Format("border:'{0} solid {1}'", borderWidth, color);
                    break;
                case BorderStyle.Double:
                    returnValue = String.Format("border:'3px double {0}'", color);
                    break;
                case BorderStyle.Groove:
                    returnValue = String.Format("border:'{0} groove {1}'", borderWidth, color);
                    break;
                case BorderStyle.Ridge:
                    returnValue = String.Format("border:'{0} ridge {1}'", borderWidth, color);
                    break;
                case BorderStyle.ShadowBox:
                    returnValue = String.Format("border:0,borderBottom:'thick solid {1}',borderRight:'thick  solid {1}'", borderWidth, color);
                    break;
                case BorderStyle.Underline:
                    returnValue = String.Format("border:0,borderBottom:'{0} solid {1}'", borderWidth, color);
                    break;
            }
            return new JavaScriptCode(returnValue);
        }

        /// <summary>
        /// Revises the formatted text.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <param name="writer">The writer.</param>
        internal static void ReviseFormattedText(ControlElement controlElement, HtmlTextWriter writer)
        {
            // if we must render the fore color
            if (controlElement.ForeColor != Color.Empty)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.Color, ColorTranslator.ToHtml(controlElement.ForeColor));
            }

            #region Fontstyle
            Font font = controlElement.Font;
            if (font != null)
            {
                string fontStyle = ReviseFont(controlElement);
                if (!String.IsNullOrEmpty(fontStyle))
                {
                    if (IsNonDefaultFont(font))
                    {
                        writer.AddStyleAttribute("font", fontStyle);
                    }

                }

                string fontDecoration = ReviseFontDecoration(controlElement);
                if (!String.IsNullOrEmpty(fontDecoration))
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.TextDecoration, fontDecoration);
                }
            }
            #endregion


            writer.RenderBeginTag("span");
            // in vb the character & must be write twice but not in html5
            writer.Write(RenderingUtils.NormalizeText(controlElement.Text, "&"));
            writer.RenderEndTag();
        }

        /// <summary>
        /// Revises the formatted text.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseFormattedText(ControlElement controlElement)
        {
            if (controlElement != null)
            {

                // If text is valid
                if (!string.IsNullOrEmpty(controlElement.Text))
                {

                    // Render the title  property as html in order to add style
                    StringWriter stringwriter = new StringWriter(CultureInfo.InvariantCulture);
                    HtmlTextWriter writer = new HtmlTextWriter(stringwriter);

                    ReviseFormattedText(controlElement, writer);

                    return stringwriter.ToString();
                }

            }
            return String.Empty;
        }

        /// <summary>
        /// Determines whether the specified font is a non default font.
        /// </summary>
        /// <param name="font">The font.</param>
        /// <returns></returns>
        internal static bool IsNonDefaultFont(Font font)
        {
            return !font.Equals(SystemFonts.DefaultFont);
        }

        /// <summary>
        /// Revises the background.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseBackground(RenderingContext context, ControlElement controlElement)
        {
            if (controlElement.BackgroundImage != null)
            {
                return String.Format("url({0})", FormatHelper.FormatResource(context, controlElement.BackgroundImage));
            }

            return null;
        }

        /// <summary>
        /// Revises the index of the z.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseZIndex(ControlElement controlElement)
        {
            if (controlElement != null)
            {
                if (controlElement.ZIndex != -1)
                {
                    return controlElement.ZIndex.ToString();
                }
            }

            return "auto";
        }


        #region Layout Rendering

        /// <summary>
        /// Renders the layout properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderLayoutProperties(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {


            if (controlElement == null)
            {
                return;
            }

            // Flags for size/dimensions rendering conditions
            bool renderLocation = GetDefaultRenderLocation(controlElement);
            bool renderTouch = this.IsTouchEnvironment;

            if (renderTouch)
            {
                renderLocation = false;
            }

            // Get table layout
            ControlElementTableLayout tableLayout = controlElement.ControlLayout as ControlElementTableLayout;

            // If there is a valid table layout
            if (tableLayout != null)
            {
                // If there is a row span defined
                if (tableLayout.RowSpan > 1)
                {
                    RenderProperty(renderingContext, jsonWriter, "rowspan", tableLayout.RowSpan);
                }

                // If there is a column span defined
                if (tableLayout.ColumnSpan > 1)
                {
                    RenderProperty(renderingContext, jsonWriter, "colspan", tableLayout.ColumnSpan);
                }

                renderLocation = false;
            }
            else
            {
                string propertyName, value;
                bool renderSplit;

                if (TryGetDockingData(controlElement, renderTouch, out propertyName, out value, out renderSplit))
                {
                    // Docking does not required location
                    renderLocation = false;

                    if (propertyName == "flex")
                    {
                        RenderProperty(renderingContext, jsonWriter, propertyName, Convert.ToInt32(value));
                    }
                    else
                    {
                        RenderProperty(renderingContext, jsonWriter, propertyName, value);
                    }


                    if (renderSplit)
                    {
                        RenderProperty(renderingContext, jsonWriter, "split", true);
                    }
                }
            }


            // If should render location
            if (renderLocation)
            {
                // If left is valid
                if (controlElement.Left != Unit.Empty)
                {
                    // Render the left property
                    RenderProperty(renderingContext, jsonWriter, this.IsTouchEnvironment ? "left" : "x", controlElement.PixelLeft + GetAbsoluteOffsetLeft(controlElement.Parent));
                }

                // If top is valid
                if (controlElement.Top != Unit.Empty)
                {
                    // Render the Top property
                    RenderProperty(renderingContext, jsonWriter, this.IsTouchEnvironment ? "top" : "y", controlElement.PixelTop + GetAbsoluteOffsetTop(controlElement.Parent));
                }
            }
        }

        /// <summary>
        /// Tries the get docking data.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <param name="renderTouch">if set to <c>true</c> [render touch].</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private bool TryGetDockingData(ControlElement controlElement, bool renderTouch, out string propertyName, out string value, out bool renderSplit)
        {
            propertyName = value = null;
            renderSplit = false;

            Dock effectiveDock = GetEffectiveDock(controlElement);
            // If is a docked element
            if (effectiveDock == Dock.None)
            {
                return false;
            }

            // If is in touch mode
            if (renderTouch)
            {
                // If there is a valid docking mode
                if (effectiveDock != Dock.None && effectiveDock != Dock.Fill)
                {
                    propertyName = "docked";
                    value = effectiveDock.ToString().ToLowerInvariant();
                }
                else
                {
                    propertyName = "flex";
                    value = "1";
                }
            }
            else if (VisualElementExtJSRenderer.GetParentLayout(controlElement) == PanelLayout.Border)
            {
                renderSplit = controlElement is SplitterPanelElement;

                propertyName = "region";
                // Check docking style
                switch (effectiveDock)
                {
                    case Dock.Left:
                        value = "west";
                        break;
                    case Dock.Right:
                        value = "east";
                        break;
                    case Dock.Bottom:
                        value = "south";
                        break;
                    case Dock.Top:
                        value = "north";
                        break;
                    case Dock.Fill:
                        value = "center";
                        renderSplit = false;
                        break;
                    case Dock.None:
                        if (controlElement.Height == Unit.Empty && controlElement.Width == Unit.Empty)
                        {
                            value = "";
                            renderSplit = false;
                        }
                        break;
                    default:
                        break;
                }
            }

            return value != null;
        }

        /// <summary>
        /// Gets the default render location.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        private static bool GetDefaultRenderLocation(ControlElement controlElement)
        {
            if (controlElement != null)
            {
                if (controlElement.Parent is TabElement)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the absolute offset top.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        private static int GetAbsoluteOffsetTop(ControlElement controlElement)
        {
            // When group box has text - reduce group box text height
            GroupBoxElement groupBox = controlElement as GroupBoxElement;
            if (groupBox != null && !string.IsNullOrEmpty(groupBox.Text))
            {
                return -15;
            }

            // When window has top main menu - return vb menu height
            WindowElement windowElement = controlElement as WindowElement;
            if (windowElement != null)
            {
                MenuElement mainMenu = windowElement.Menu;
                if (mainMenu != null && mainMenu.Dock == Dock.Top)
                {
                    return 30;
                }
            }

            return 0;
        }

        /// <summary>
        /// Gets the absolute offset left.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        private static int GetAbsoluteOffsetLeft(ControlElement controlElement)
        {
            // When group box has text - reduce group box text left
            GroupBoxElement groupBox = controlElement as GroupBoxElement;
            if (groupBox != null && !string.IsNullOrEmpty(groupBox.Text))
            {
                return -6;
            }

            return 0;
        }

        /// <summary>
        /// Renders the height property.
        /// </summary>
        /// <param name="context">The rendering context.</param>
        /// <param name="visualElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderHeightProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ControlElement controlElement = visualElement as ControlElement;
            if (controlElement == null)
            {
                return;
            }

            if (!ShouldRenderHeightProperty())
            {
                return;
            }

            if (GetParentLayout(controlElement) == PanelLayout.Center)
            {
                jsonWriter.WriteProperty("height", "100%");
            }
            else
            {
                Dock effectiveDock = GetEffectiveDock(controlElement);
                if (effectiveDock == Dock.None || effectiveDock == Dock.Top || effectiveDock == Dock.Bottom)
                {
                    int height = GetEffectiveHeight(controlElement);
                    if (height > 0)
                    {
                        jsonWriter.WriteProperty("height", height);
                    }
                }
            }
        }

        /// <summary>
        /// Should render height property.
        /// </summary>
        /// <returns></returns>
        protected virtual bool ShouldRenderHeightProperty()
        {
            return !this.IsTouchEnvironment;
        }

        /// <summary>
        /// Gets the height of the effective.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        private int GetEffectiveHeight(ControlElement controlElement)
        {
            int height = 0;

            if (controlElement != null)
            {
                if (controlElement.Dock == Dock.None || !controlElement.Height.IsEmpty)
                {
                    return controlElement.PixelHeight;
                }

                foreach (ControlElement child in controlElement.Controls)
                {
                    int childHeight = GetEffectiveHeight(child);

                    if (height < childHeight + child.PixelTop)
                    {
                        height = childHeight + child.PixelTop;
                    }
                }
            }

            return height;
        }

        /// <summary>
        /// Renders the width property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderWidthProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ControlElement controlElement = visualElement as ControlElement;
            // validate parameter
            if (controlElement == null)
            {
                return;
            }

            if (!ShouldRenderWidthProperty())
            {
                return;
            }

            PanelLayout containerLayout = GetParentLayout(controlElement);

            if (containerLayout == PanelLayout.Center)
            {
                jsonWriter.WriteProperty("width", "100%");
            }

            Dock effectiveDock = GetEffectiveDock(controlElement);
            if (effectiveDock == Dock.None || effectiveDock == Dock.Left || effectiveDock == Dock.Right)
            {
                int width = GetEffectiveWidth(controlElement);
                if (width > 0)
                {
                    jsonWriter.WriteProperty("width", width);
                }
            }
        }

        /// <summary>
        /// Should render width property.
        /// </summary>
        /// <returns></returns>
        protected virtual bool ShouldRenderWidthProperty()
        {
            return !this.IsTouchEnvironment;
        }

        /// <summary>
        /// Gets the width of the effective.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        protected int GetEffectiveWidth(ControlElement controlElement)
        {
            int width = 0;

            if (controlElement != null)
            {
                if (controlElement.Dock == Dock.None || !controlElement.Width.IsEmpty)
                {
                    return controlElement.PixelWidth;
                }

                foreach (ControlElement child in controlElement.Controls)
                {
                    int childWidth = GetEffectiveWidth(child);

                    if (width < childWidth + child.PixelLeft)
                    {
                        width = childWidth + child.PixelLeft;
                    }
                }
            }

            return width;
        }

        /// <summary>
        /// Gets the effective dock.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">controlElement</exception>
        protected virtual Dock GetEffectiveDock(ControlElement controlElement)
        {
            // validate parameter
            if (controlElement == null)
            {
                throw new ArgumentNullException("controlElement"); ;
            }

            return controlElement.Dock;
        }

        #endregion


        /// <summary>
        /// Renders the enabled property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderEnabledProperty(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {
            if (controlElement != null && jsonWriter != null)
            {
                // If element disabled
                if (!controlElement.Enabled)
                {
                    // Render disabled attribute
                    RenderProperty(renderingContext, jsonWriter, "disabled", true);
                }
            }
        }

        /// <summary>
        /// Renders the border property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBorderProperty(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the tab index property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderTabIndexProperty(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {
            if (controlElement != null && jsonWriter != null)
            {
                // If element has tab index
                if (controlElement.TabStop && controlElement.TabIndex > 0)
                {
                    // Render tab index attribute
                    RenderProperty(renderingContext, jsonWriter, "tabIndex", controlElement.TabIndex);
                }
                else if (!controlElement.TabStop)
                {
                    // Render tab index attribute
                    RenderProperty(renderingContext, jsonWriter, "tabIndex", -1);
                }
            }
        }

        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The writer.</param>
        protected override void RenderEvents(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

            // Render tap hold event
            RenderTapHoldEvent(renderingContext, visualElement, jsonWriter);
            // Render base class events
            base.RenderEvents(renderingContext, visualElement, jsonWriter);


            //// Render text changed event
            //RenderTextChangedEvent(visualElement, getWriter);

            //// Render the key press event
            //RenderKeyPressEvent(visualElement, getWriter);

        }


        /// <summary>
        /// Renders the TapHold event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        private void RenderTapHoldEvent(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ControlElement ctrl = visualElement as ControlElement;

            if (ctrl.HasMouseTapHoldListeners)
            {
                jsonWriter.WritePropertyName("el");

                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteRaw("taphold : VT_CreateMarkedListener(function(event){");
                    // Record event.target.offsetParent.innerText
                    // currentInnerText  event.target.innerText
                    jsonWriter.WriteRaw("console.log(event);");
                    jsonWriter.WriteRaw("var targetContent = event.target.innerText;");
                    jsonWriter.WriteRaw("var parentTargetContent = event.target.offsetParent.innerText;");
                    jsonWriter.WriteRaw("var args = [event.parentEvent.pageX.toString(),event.parentEvent.pageY.toString(), targetContent, parentTargetContent];");
                    jsonWriter.WriteRaw("VT_raiseEvent('" + VisualElement.GetElementId(visualElement) + "', 'MouseTapHold', {data : args});");
                    jsonWriter.WriteRaw("})");
                }
                jsonWriter.WriteRaw(",");
            }
        }

        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            // Choose property
            switch (property)
            {
                case "Enabled":
                    RenderEnabledPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Top":
                    RenderTopPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Left":
                    RenderLeftPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Cursor":
                    RenderCursorPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Dock":
                    RenderDockPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }

        }

        /// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
            switch (serverEventName)
            {
                case "MouseTapHold":
                    return this.ExtractMouseTapHoldCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
                    return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }
        }

        /// <summary>
        /// Gets the method TapHold callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual EventArgs ExtractMouseTapHoldCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            var pageX = args["data"][0];
            var pageY = args["data"][1];
            var targetInnerText = args["data"][2];
            var targetParentInnerText = args["data"][3];

            return new MouseEventArgs(Int32.Parse(pageX.ToString()), Int32.Parse(pageY.ToString()));
        }

        /// <summary>
        /// Renders the dock property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="ArgumentNullException">builder</exception>
        protected virtual void RenderDockPropertyChange(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // validate parameter
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                string propertyName, value;
                bool renderSplit;

                bool isTouch = this.IsTouchEnvironment;
                TryGetDockingData(controlElement, isTouch, out propertyName, out value, out renderSplit);

                if (!isTouch)
                {
                    // render width
                    builder.AppendInvocationStatement(string.Concat(componentVariable, ".setRegion"), value);
                }
            }
        }

        /// <summary>
        /// Renders the top property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="System.ArgumentNullException">builder</exception>
        protected virtual void RenderTopPropertyChange(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // validate parameter
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null && !this.IsTouchEnvironment)
            {
                // render width
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setLocalY"), controlElement.PixelTop + GetAbsoluteOffsetTop(controlElement.Parent));
            }
        }

        /// <summary>
        /// Renders the left property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="System.ArgumentNullException">builder</exception>
        protected virtual void RenderLeftPropertyChange(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // validate parameter
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null && !this.IsTouchEnvironment)
            {
                // render width
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setLocalX"), controlElement.PixelLeft + GetAbsoluteOffsetLeft(controlElement.Parent));
            }
        }

        /// <summary>
        /// Renders the cursor property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="System.ArgumentNullException">builder</exception>
        protected virtual void RenderCursorPropertyChange(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // validate parameter
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // render width todo
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setStyle"), "Cursor", controlElement.Cursor.CursorName);
            }
        }

        /// <summary>
        /// Renders the height property change.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="System.ArgumentNullException">builder</exception>
        protected virtual void RenderHeightPropertyChange(VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // validate parameter
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // render width
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setHeight"), controlElement.PixelHeight);
            }
        }

        /// <summary>
        /// Renders the width property change.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="System.ArgumentNullException">builder</exception>
        protected virtual void RenderWidthPropertyChange(VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // validate parameter
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // render width
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setWidth"), controlElement.PixelWidth);
            }
        }

        /// <summary>
        /// Renders the enabled property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="System.ArgumentNullException">builder</exception>
        protected virtual void RenderEnabledPropertyChange(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // validate parameter
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // render enable
                builder.AppendInvocationStatement(string.Concat(componentVariable, controlElement.Enabled ? ".enable" : ".disable"));
            }
        }

        /// <summary>
        /// Revises the enable key press.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        internal static bool ReviseEnableKeyPress(VisualElement visualElement)
        {
            ControlElement element = visualElement as ControlElement;

            if (element != null)
            {
                // Element has listener
                if (element.HasKeyPressListeners)
                {
                    return true;
                }
            }

            // Return false
            return false;
        }

        /// <summary>
        /// Revise Background Repeat css attribute
        /// </summary>
        /// <param name="imageLayout">ImageLayout </param>
        /// <returns>css value </returns>
        internal static string ReviseBackgroundRepeat(ImageLayout imageLayout)
        {
            string backgroundRepeat = "repeat";
            if (imageLayout != null)
            {
                switch (imageLayout)
                {
                    case ImageLayout.Center:
                    case ImageLayout.None:
                    case ImageLayout.Stretch:
                    case ImageLayout.Zoom:
                        backgroundRepeat = "no-repeat";
                        break;
                }
            }
            return backgroundRepeat;
        }

        /// <summary>
        /// Revises the padding.
        /// </summary>
        /// <param name="padding">The padding.</param>
        /// <returns></returns>
        internal static string RevisePadding(Padding padding)
        {
            if (padding != Padding.Empty)
            {
                if (padding.All != -1)
                {
                    return padding.All.ToString();
                }
                else
                {
                    return String.Format("{0} {1} {2} {3}", padding.Top, padding.Right, padding.Bottom, padding.Left);
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Revises the padding.
        /// </summary>
        /// <param name="padding">The padding.</param>
        /// <returns></returns>

        internal static string FormmatPaddingString(ControlElement element)
        {

            if (element != null)
            {
                Padding padding = element.Padding;

                return (padding.Left.ToString(CultureInfo.CurrentCulture) + "px " + padding.Top.ToString(CultureInfo.CurrentCulture) + "px " + padding.Right.ToString(CultureInfo.CurrentCulture) + "px " + padding.Bottom.ToString(CultureInfo.CurrentCulture) + "px");
            }
            return "";
        }

        /// <summary>
        /// Revise Background Size css attribute
        /// </summary>
        /// <param name="imageLayout">ImageLayout </param>
        /// <returns>css value </returns>
        internal static string ReviseBackgroundSize(ImageLayout imageLayout)
        {
            string backgroundSize = "auto";
            if (imageLayout != null)
            {
                switch (imageLayout)
                {
                    case ImageLayout.Center:
                        backgroundSize = "auto";
                        break;
                    case ImageLayout.None:
                        backgroundSize = "initial";
                        break;
                    case ImageLayout.Stretch:
                    case ImageLayout.Zoom:
                        backgroundSize = "cover";
                        break;

                }
            }
            return backgroundSize;
        }


        /// <summary>
        /// Revise Background Position css attribute
        /// </summary>
        /// <param name="imageLayout">ImageLayout </param>
        /// <returns>css value </returns>
        internal static string ReviseBackgroundPosition(ImageLayout imageLayout)
        {
            string backgroundPosition = "0% 0%";
            if (imageLayout != null)
            {
                switch (imageLayout)
                {
                    case ImageLayout.Center:
                        backgroundPosition = "center center";
                        break;
                    case ImageLayout.Stretch:
                    case ImageLayout.Zoom:
                        backgroundPosition = "0% 0%";
                        break;
                    case ImageLayout.None:
                        backgroundPosition = "top left";
                        break;
                }
            }
            return backgroundPosition;
        }

        /// <summary>
        /// Revises the auto size style.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static int ReviseTabIndex(ControlElement element)
        {
            return element.TabStop ? element.TabIndex : -1;
        }

        /// <summary>
        /// Should revise validate event 
        /// </summary>
        /// <param name="element">The control element </param>
        /// <returns>true if the control have validated event , otherwise fasle </returns>
        internal static string ReviseValidatedEvent(ControlElement element)
        {
            if (element != null)
            {
                if (element.HasValidatedListeners)
                {
                    return "this.fireEvent('validated');";
                }
            }
            return "";
        }


        /// <summary>
        /// Revise contextmenu id 
        /// </summary>
        /// <param name="visualElement">The window element .</param>
        /// <returns>return contextmenu id.</returns>
        internal static string ReviseContextMenuStripID(VisualElement visualElement)
        {
            string contextMenuId = "";
            ControlElement ctrl = visualElement as ControlElement;
            if (ctrl != null)
            {
                if (ctrl.ContextMenuStrip != null)
                {
                    return ctrl.ContextMenuStrip.ID;
                }
            }
            return contextMenuId;
        }
    }
}
