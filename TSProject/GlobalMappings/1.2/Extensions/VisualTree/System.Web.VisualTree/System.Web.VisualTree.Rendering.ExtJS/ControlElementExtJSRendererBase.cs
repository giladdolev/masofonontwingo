using Newtonsoft.Json;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Engine;
using System.IO;
using System.Web.UI;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Data;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public abstract class ControlElementExtJSRendererBase : VisualElementExtJSRenderer
	{
		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the Load event attach code.
			RenderAttachLoadEvent(context, visualElement, writer); 
			// Renders the MouseTapHold event attach code.
			RenderAttachMouseTapHoldEvent(context, visualElement, writer); 
			// Renders the Click event attach code.
			RenderAttachClickEvent(context, visualElement, writer); 
			// Renders the MouseHover event attach code.
			RenderAttachMouseHoverEvent(context, visualElement, writer); 
			// Renders the MouseEnter event attach code.
			RenderAttachMouseEnterEvent(context, visualElement, writer); 
			// Renders the MouseLeave event attach code.
			RenderAttachMouseLeaveEvent(context, visualElement, writer); 
			// Renders the Resize event attach code.
			RenderAttachResizeEvent(context, visualElement, writer); 
			// Renders the TextChanged event attach code.
			RenderAttachTextChangedEvent(context, visualElement, writer); 
			// Renders the DragDrop event attach code.
			RenderAttachDragDropEvent(context, visualElement, writer); 
			// Renders the DragOver event attach code.
			RenderAttachDragOverEvent(context, visualElement, writer); 
			// Renders the MouseMove event attach code.
			RenderAttachMouseMoveEvent(context, visualElement, writer); 
			// Renders the MouseDown event attach code.
			RenderAttachMouseDownEvent(context, visualElement, writer); 
			// Renders the MouseUp event attach code.
			RenderAttachMouseUpEvent(context, visualElement, writer); 
			// Renders the MouseClick event attach code.
			RenderAttachMouseClickEvent(context, visualElement, writer); 
			// Renders the Validated event attach code.
			RenderAttachValidatedEvent(context, visualElement, writer); 
			// Renders the Leave event attach code.
			RenderAttachLeaveEvent(context, visualElement, writer); 
			// Renders the LostFocus event attach code.
			RenderAttachLostFocusEvent(context, visualElement, writer); 
			// Renders the DoubleClick event attach code.
			RenderAttachDoubleClickEvent(context, visualElement, writer); 
			// Renders the DragEnter event attach code.
			RenderAttachDragEnterEvent(context, visualElement, writer); 
			// Renders the Enter event attach code.
			RenderAttachEnterEvent(context, visualElement, writer); 
			// Renders the ControlAdded event attach code.
			RenderAttachControlAddedEvent(context, visualElement, writer); 
			// Renders the ControlRemoved event attach code.
			RenderAttachControlRemovedEvent(context, visualElement, writer); 
			// Renders the KeyPress event attach code.
			RenderAttachKeyPressEvent(context, visualElement, writer); 
			// Renders the GotFocus event attach code.
			RenderAttachGotFocusEvent(context, visualElement, writer); 
			// Renders the KeyUp event attach code.
			RenderAttachKeyUpEvent(context, visualElement, writer); 
			// Renders the KeyDown event attach code.
			RenderAttachKeyDownEvent(context, visualElement, writer); 
			// Renders the Afterrender event attach code.
			RenderAttachAfterrenderEvent(context, visualElement, writer); 
			// Renders the WidgetControlEditChanged event attach code.
			RenderAttachWidgetControlEditChangedEvent(context, visualElement, writer); 
			// Renders the WidgetControlItemChanged event attach code.
			RenderAttachWidgetControlItemChangedEvent(context, visualElement, writer); 
			// Renders the WidgetControlFocusEnter event attach code.
			RenderAttachWidgetControlFocusEnterEvent(context, visualElement, writer); 
			// Renders the WidgetControlDoubleClick event attach code.
			RenderAttachWidgetControlDoubleClickEvent(context, visualElement, writer); 
			// Renders the WidgetControlClick event attach code.
			RenderAttachWidgetControlClickEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "Load":
					return this.ExtractLoadCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseTapHold":
					return this.ExtractMouseTapHoldCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Click":
					return this.ExtractClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseHover":
					return this.ExtractMouseHoverCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseEnter":
					return this.ExtractMouseEnterCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseLeave":
					return this.ExtractMouseLeaveCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Resize":
					return this.ExtractResizeCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "TextChanged":
					return this.ExtractTextChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DragDrop":
					return this.ExtractDragDropCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DragOver":
					return this.ExtractDragOverCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseMove":
					return this.ExtractMouseMoveCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseDown":
					return this.ExtractMouseDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseUp":
					return this.ExtractMouseUpCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseClick":
					return this.ExtractMouseClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Validated":
					return this.ExtractValidatedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Leave":
					return this.ExtractLeaveCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "LostFocus":
					return this.ExtractLostFocusCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DoubleClick":
					return this.ExtractDoubleClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DragEnter":
					return this.ExtractDragEnterCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Enter":
					return this.ExtractEnterCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ControlAdded":
					return this.ExtractControlAddedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ControlRemoved":
					return this.ExtractControlRemovedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "KeyPress":
					return this.ExtractKeyPressCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "GotFocus":
					return this.ExtractGotFocusCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "KeyUp":
					return this.ExtractKeyUpCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "KeyDown":
					return this.ExtractKeyDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Afterrender":
					return this.ExtractAfterrenderCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "WidgetControlEditChanged":
					return this.ExtractWidgetControlEditChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "WidgetControlItemChanged":
					return this.ExtractWidgetControlItemChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "WidgetControlFocusEnter":
					return this.ExtractWidgetControlFocusEnterCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "WidgetControlDoubleClick":
					return this.ExtractWidgetControlDoubleClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "WidgetControlClick":
					return this.ExtractWidgetControlClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			this.RenderZIndexProperty(context, visualElement, styleWriter);
			this.RenderAutoSizeProperty(context, visualElement, styleWriter);
			this.RenderFontProperty(context, visualElement, styleWriter);
			this.RenderRightToLeftDecorationProperty(context, visualElement, styleWriter);
			this.RenderBackgroundImageProperty(context, visualElement, styleWriter);
			this.RenderForeColorProperty(context, visualElement, styleWriter);
			this.RenderBackColorProperty(context, visualElement, styleWriter);
			this.RenderBackgroundImageLayoutProperty(context, visualElement, styleWriter);
		}


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "WaitMaskDisabled":
					this.RenderWaitMaskDisabledPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ToolTipText":
					this.RenderToolTipTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CssClass":
					this.RenderCssClassPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MaximumSize":
					this.RenderMaximumSizePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MinimumSize":
					this.RenderMinimumSizePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ZIndex":
					this.RenderZIndexPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Draggable":
					this.RenderDraggablePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AutoSize":
					this.RenderAutoSizePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Font":
					this.RenderFontPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "RightToLeft":
					this.RenderRightToLeftPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "RightToLeftDecoration":
					this.RenderRightToLeftDecorationPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BackgroundImage":
					this.RenderBackgroundImagePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BorderStyle":
					this.RenderBorderStylePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BorderColor":
					this.RenderBorderColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Controls":
					this.RenderControlsPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ForeColor":
					this.RenderForeColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BackColor":
					this.RenderBackColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Width":
					this.RenderWidthPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Tag":
					this.RenderTagPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Text":
					this.RenderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Height":
					this.RenderHeightPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "TabIndex":
					this.RenderTabIndexPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Visible":
					this.RenderVisiblePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Padding":
					this.RenderPaddingPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CustomUI":
					this.RenderCustomUIPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Resizable":
					this.RenderResizablePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DataMember":
					this.RenderDataMemberPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BackgroundImageLayout":
					this.RenderBackgroundImageLayoutPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "EnableKeyEvents":
					this.RenderEnableKeyEventsPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "liveDrag":
					this.RenderliveDragPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "IsFocused":
					this.RenderIsFocusedPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Margin":
					this.RenderMarginPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ReadOnly":
					this.RenderReadOnlyPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderWaitMaskDisabledProperty(renderingContext, visualElement, jsonWriter);
			this.RenderToolTipTextProperty(renderingContext, visualElement, jsonWriter);
			this.RenderCssClassProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMaximumSizeProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMinimumSizeProperty(renderingContext, visualElement, jsonWriter);
			this.RenderDraggableProperty(renderingContext, visualElement, jsonWriter);
			this.RenderRightToLeftProperty(renderingContext, visualElement, jsonWriter);
			this.RenderBorderStyleProperty(renderingContext, visualElement, jsonWriter);
			this.RenderBorderColorProperty(renderingContext, visualElement, jsonWriter);
			this.RenderControlsProperty(renderingContext, visualElement, jsonWriter);
			this.RenderWidthProperty(renderingContext, visualElement, jsonWriter);
			this.RenderTagProperty(renderingContext, visualElement, jsonWriter);
			this.RenderTextProperty(renderingContext, visualElement, jsonWriter);
			this.RenderHeightProperty(renderingContext, visualElement, jsonWriter);
			this.RenderTabIndexProperty(renderingContext, visualElement, jsonWriter);
			this.RenderVisibleProperty(renderingContext, visualElement, jsonWriter);
			this.RenderPaddingProperty(renderingContext, visualElement, jsonWriter);
			this.RenderCustomUIProperty(renderingContext, visualElement, jsonWriter);
			this.RenderResizableProperty(renderingContext, visualElement, jsonWriter);
			this.RenderDataMemberProperty(renderingContext, visualElement, jsonWriter);
			this.RenderEnableKeyEventsProperty(renderingContext, visualElement, jsonWriter);
			this.RenderliveDragProperty(renderingContext, visualElement, jsonWriter);
			this.RenderIsFocusedProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMarginProperty(renderingContext, visualElement, jsonWriter);
			this.RenderReadOnlyProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the Load event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachLoadEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of Load event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseLoadEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Load event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderLoadEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the Load event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractLoadCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Load event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLoadAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Load event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLoadRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the MouseTapHold event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseTapHoldEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseTapHold event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseTapHoldEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseTapHold event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseTapHoldEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasMouseTapHoldListeners);
		}


		/// <summary>
        /// Extracts the MouseTapHold event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseTapHoldCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseTapHold event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseTapHoldAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseTapHold event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseTapHoldRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Click event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the client callback code.Click
										if (!String.IsNullOrEmpty(element.ClientClick))
										{
											javaScriptBuilder.AppendRawValue(element.ClientClick);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.Click
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Click", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("click", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Click event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Click event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientClick) || element.HasClickListeners);
		}


		/// <summary>
        /// Extracts the Click event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Click event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the client callback code.Click
										if (!String.IsNullOrEmpty(element.ClientClick))
										{
											javaScriptBuilder.AppendRawValue(element.ClientClick);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.Click
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Click", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('click', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the Click event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "click");
            }
        }

		/// <summary>
        /// Renders the MouseHover event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseHoverEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderMouseHoverEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseMouseHoverEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.MouseHover
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "MouseHover", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("mouseover", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of MouseHover event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseHoverEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseHover event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseHoverEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasMouseHoverListeners);
		}


		/// <summary>
        /// Extracts the MouseHover event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseHoverCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseHover event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseHoverAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderMouseHoverEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseMouseHoverEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.MouseHover
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "MouseHover", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('mouseover', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the MouseHover event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseHoverRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderMouseHoverEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "mouseover");
            }
        }

		/// <summary>
        /// Renders the MouseEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseEnter event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseEnterEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasMouseEnterListeners);
		}


		/// <summary>
        /// Extracts the MouseEnter event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseEnterCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseEnter event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseEnterAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseEnter event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseEnterRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the MouseLeave event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseLeaveEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseLeave event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseLeaveEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseLeave event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseLeaveEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasMouseLeaveListeners);
		}


		/// <summary>
        /// Extracts the MouseLeave event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseLeaveCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseLeave event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseLeaveAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseLeave event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseLeaveRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Resize event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachResizeEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderResizeEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseResizeEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "width", "height", "oldWidth", "oldHeight", "eOpts"}, 
									() => {  
										// Create the server callback code.Resize
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Resize", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{width : width , height: height}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("resize", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Resize event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseResizeEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Resize event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderResizeEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasResizeListeners);
		}


		/// <summary>
        /// Extracts the Resize event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractResizeCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new ResizeEventArgs(args.Value<System.Int32>("width"),args.Value<System.Int32>("height"));
		}


		/// <summary>
        /// Renders the Resize event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderResizeAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderResizeEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseResizeEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "width", "height", "oldWidth", "oldHeight", "eOpts"}, 
                            () => {  
										// Create the server callback code.Resize
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Resize", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{width : width , height: height}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('resize', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the Resize event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderResizeRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderResizeEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "resize");
            }
        }

		/// <summary>
        /// Renders the TextChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachTextChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of TextChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseTextChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the TextChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderTextChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasTextChangedListeners);
		}


		/// <summary>
        /// Extracts the TextChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractTextChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the TextChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTextChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the TextChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTextChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the DragDrop event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDragDropEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of DragDrop event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDragDropEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DragDrop event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDragDropEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the DragDrop event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDragDropCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DragDrop event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDragDropAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the DragDrop event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDragDropRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the DragOver event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDragOverEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderDragOverEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseDragOverEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "x", "y", "eOpts"}, 
									() => {  
										// Create the server callback code.DragOver
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "DragOver", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{value1 : x, value2 : y }}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("move", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of DragOver event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDragOverEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DragOver event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDragOverEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasDragOverListeners);
		}


		/// <summary>
        /// Extracts the DragOver event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDragOverCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new DragEventArgs(args.Value<System.Int32>("value1"),args.Value<System.Int32>("value2"));
		}


		/// <summary>
        /// Renders the DragOver event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDragOverAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDragOverEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseDragOverEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "x", "y", "eOpts"}, 
                            () => {  
										// Create the server callback code.DragOver
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "DragOver", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{value1 : x, value2 : y }}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('move', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the DragOver event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDragOverRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDragOverEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "move");
            }
        }

		/// <summary>
        /// Renders the MouseMove event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseMoveEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseMove event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseMoveEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseMove event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseMoveEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the MouseMove event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseMoveCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseMove event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseMoveAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseMove event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseMoveRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the MouseDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the MouseDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the MouseUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseUpEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseUp event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseUpEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseUpEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the MouseUp event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseUpCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseUp event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseUpAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseUp event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseUpRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the MouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of MouseClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the MouseClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the MouseClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Validated event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachValidatedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderValidatedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseValidatedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.Validated
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Validated", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("validated", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Validated event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseValidatedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Validated event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderValidatedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasValidatedListeners);
		}


		/// <summary>
        /// Extracts the Validated event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractValidatedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Validated event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderValidatedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderValidatedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseValidatedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.Validated
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Validated", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('validated', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the Validated event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderValidatedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderValidatedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "validated");
            }
        }

		/// <summary>
        /// Renders the Leave event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachLeaveEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderLeaveEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseLeaveEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw( ControlElementExtJSRenderer.ReviseValidatedEvent(element) );
										javaScriptBuilder.AppendSemicolon();

										// Create the client callback code.Leave
										if (!String.IsNullOrEmpty(element.ClientLeave))
										{
											javaScriptBuilder.AppendRawValue(element.ClientLeave);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.Leave
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Leave", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("blur", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Leave event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseLeaveEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Leave event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderLeaveEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientLeave) || element.HasLeaveListeners);
		}


		/// <summary>
        /// Extracts the Leave event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractLeaveCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Leave event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLeaveAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Leave event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLeaveRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderLeaveEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the LostFocus event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachLostFocusEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderLostFocusEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseLostFocusEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "event", "eOpts"}, 
									() => {  
										// Create the client callback code.LostFocus
										if (!String.IsNullOrEmpty(element.ClientLostFocus))
										{
											javaScriptBuilder.AppendRawValue(element.ClientLostFocus);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.LostFocus
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "LostFocus", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("focusleave", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of LostFocus event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseLostFocusEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the LostFocus event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderLostFocusEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientLostFocus) || element.HasLostFocusListeners);
		}


		/// <summary>
        /// Extracts the LostFocus event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractLostFocusCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the LostFocus event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLostFocusAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderLostFocusEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseLostFocusEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "event", "eOpts"}, 
                            () => {  
										// Create the client callback code.LostFocus
										if (!String.IsNullOrEmpty(element.ClientLostFocus))
										{
											javaScriptBuilder.AppendRawValue(element.ClientLostFocus);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.LostFocus
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "LostFocus", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('focusleave', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the LostFocus event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLostFocusRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderLostFocusEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "focusleave");
            }
        }

		/// <summary>
        /// Renders the DoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of DoubleClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDoubleClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the DoubleClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDoubleClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DoubleClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDoubleClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the DoubleClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDoubleClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the DragEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDragEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of DragEnter event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDragEnterEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DragEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDragEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasDragEnterListeners);
		}


		/// <summary>
        /// Extracts the DragEnter event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDragEnterCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DragEnter event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDragEnterAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the DragEnter event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDragEnterRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Enter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of Enter event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseEnterEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Enter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the Enter event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractEnterCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Enter event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnterAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Enter event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnterRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ControlAdded event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachControlAddedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ControlAdded event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseControlAddedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ControlAdded event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderControlAddedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ControlAdded event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractControlAddedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ControlAdded event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderControlAddedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ControlAdded event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderControlAddedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ControlRemoved event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachControlRemovedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ControlRemoved event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseControlRemovedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ControlRemoved event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderControlRemovedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ControlRemoved event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractControlRemovedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ControlRemoved event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderControlRemovedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ControlRemoved event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderControlRemovedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the KeyPress event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachKeyPressEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderKeyPressEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseKeyPressEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "e", "eOpts"}, 
									() => {  
										// Create the client callback code.KeyPress
										if (!String.IsNullOrEmpty(element.ClientKeyPress))
										{
											javaScriptBuilder.AppendRawValue(element.ClientKeyPress);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.KeyPress
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "KeyPress", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey()}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("keypress", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of KeyPress event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseKeyPressEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the KeyPress event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderKeyPressEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientKeyPress) || element.HasKeyPressListeners);
		}


		/// <summary>
        /// Extracts the KeyPress event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractKeyPressCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new KeyPressEventArgs(Convert.ToInt32(args.Value<System.String>("key")));
		}


		/// <summary>
        /// Renders the KeyPress event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyPressAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderKeyPressEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseKeyPressEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "e", "eOpts"}, 
                            () => {  
										// Create the client callback code.KeyPress
										if (!String.IsNullOrEmpty(element.ClientKeyPress))
										{
											javaScriptBuilder.AppendRawValue(element.ClientKeyPress);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.KeyPress
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "KeyPress", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey()}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('keypress', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the KeyPress event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyPressRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderKeyPressEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "keypress");
            }
        }

		/// <summary>
        /// Renders the GotFocus event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachGotFocusEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderGotFocusEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseGotFocusEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "event", "eOpts"}, 
									() => {  
										// Create the client callback code.GotFocus
										if (!String.IsNullOrEmpty(element.ClientGotFocus))
										{
											javaScriptBuilder.AppendRawValue(element.ClientGotFocus);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.GotFocus
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "GotFocus", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("focusenter", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of GotFocus event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseGotFocusEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the GotFocus event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderGotFocusEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientGotFocus) || element.HasGotFocusListeners);
		}


		/// <summary>
        /// Extracts the GotFocus event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractGotFocusCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the GotFocus event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderGotFocusAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderGotFocusEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseGotFocusEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "event", "eOpts"}, 
                            () => {  
										// Create the client callback code.GotFocus
										if (!String.IsNullOrEmpty(element.ClientGotFocus))
										{
											javaScriptBuilder.AppendRawValue(element.ClientGotFocus);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.GotFocus
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "GotFocus", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('focusenter', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the GotFocus event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderGotFocusRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderGotFocusEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "focusenter");
            }
        }

		/// <summary>
        /// Renders the KeyUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachKeyUpEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of KeyUp event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseKeyUpEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the KeyUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderKeyUpEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the KeyUp event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractKeyUpCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the KeyUp event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyUpAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the KeyUp event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyUpRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderKeyDownEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseKeyDownEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "e", "t", "eOpts"}, 
									() => {  
										// Create the client callback code.KeyDown
										if (!String.IsNullOrEmpty(element.ClientKeyDown))
										{
											javaScriptBuilder.AppendRawValue(element.ClientKeyDown);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.KeyDown
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "KeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey()}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("keydown", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of KeyDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseKeyDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientKeyDown) || element.HasKeyDownListeners);
		}


		/// <summary>
        /// Extracts the KeyDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractKeyDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new KeyDownEventArgs((Keys)args.Value<System.Int32>("key"));
		}


		/// <summary>
        /// Renders the KeyDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderKeyDownEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseKeyDownEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "e", "t", "eOpts"}, 
                            () => {  
										// Create the client callback code.KeyDown
										if (!String.IsNullOrEmpty(element.ClientKeyDown))
										{
											javaScriptBuilder.AppendRawValue(element.ClientKeyDown);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.KeyDown
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "KeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey()}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('keydown', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the KeyDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderKeyDownEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "keydown");
            }
        }

		/// <summary>
        /// Renders the Afterrender event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderAfterrenderEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Queued;

						// Revise the event behavior
						eventBehaviorType = this.ReviseAfterrenderEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "eOpts"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"var menuCmp = VT_GetCmp('{0}');
if (menuCmp != null) {{
    cmp.el.addListener('contextmenu', function (e) {{
        e.stopEvent();
        menuCmp.on('render', function (menu) {{
            menu.getEl().on('contextmenu', function (e) {{
                e.stopEvent();
            }});
        }});
        menuCmp.enable();
        menuCmp.showBy(cmp, ""tl?"", [e.getX() - cmp.getX(), e.getY() - cmp.getY()]);
    }});
}}",  WindowElementExtJSRenderer.ReviseContextMenuStripID(element) );
										javaScriptBuilder.AppendSemicolon();

										// Create the server callback code.Afterrender
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Afterrender", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("afterrender", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Afterrender event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAfterrenderEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Afterrender event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasAfterrenderListeners);
		}


		/// <summary>
        /// Extracts the Afterrender event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractAfterrenderCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Afterrender event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAfterrenderAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderAfterrenderEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

                // Revise the event behavior
                eventBehaviorType = this.ReviseAfterrenderEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "eOpts"}, 
                            () => {  
										// Create the server callback code.Afterrender
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Afterrender", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
        }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('afterrender', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the Afterrender event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAfterrenderRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderAfterrenderEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "afterrender");
            }
        }

		/// <summary>
        /// Renders the WidgetControlEditChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWidgetControlEditChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlEditChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlEditChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WidgetControlEditChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWidgetControlEditChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlEditChangedListeners);
		}


		/// <summary>
        /// Extracts the WidgetControlEditChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractWidgetControlEditChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WidgetControlEditChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlEditChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlEditChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlEditChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the WidgetControlItemChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWidgetControlItemChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlItemChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlItemChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WidgetControlItemChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWidgetControlItemChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlItemChangedListeners);
		}


		/// <summary>
        /// Extracts the WidgetControlItemChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractWidgetControlItemChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WidgetControlItemChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlItemChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlItemChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlItemChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the WidgetControlFocusEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWidgetControlFocusEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlFocusEnter event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlFocusEnterEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WidgetControlFocusEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWidgetControlFocusEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlFocusEnterListeners);
		}


		/// <summary>
        /// Extracts the WidgetControlFocusEnter event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractWidgetControlFocusEnterCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WidgetControlFocusEnter event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlFocusEnterAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlFocusEnter event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlFocusEnterRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the WidgetControlDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWidgetControlDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlDoubleClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlDoubleClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}
                

		/// <summary>
        /// Indicates if we should render the WidgetControlDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWidgetControlDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlDoubleClickListeners);
            }


		/// <summary>
        /// Extracts the WidgetControlDoubleClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractWidgetControlDoubleClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
        }


		/// <summary>
        /// Renders the WidgetControlDoubleClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlDoubleClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlDoubleClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlDoubleClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the WidgetControlClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWidgetControlClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WidgetControlClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWidgetControlClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlClickListeners);
		}


		/// <summary>
        /// Extracts the WidgetControlClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractWidgetControlClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WidgetControlClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "Click":
					this.RenderClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MouseHover":
					this.RenderMouseHoverAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Resize":
					this.RenderResizeAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DragOver":
					this.RenderDragOverAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Validated":
					this.RenderValidatedAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "LostFocus":
					this.RenderLostFocusAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "KeyPress":
					this.RenderKeyPressAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "GotFocus":
					this.RenderGotFocusAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "KeyDown":
					this.RenderKeyDownAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Afterrender":
					this.RenderAfterrenderAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "Click":
					this.RenderClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MouseHover":
					this.RenderMouseHoverRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Resize":
					this.RenderResizeRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DragOver":
					this.RenderDragOverRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Validated":
					this.RenderValidatedRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "LostFocus":
					this.RenderLostFocusRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "KeyPress":
					this.RenderKeyPressRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "GotFocus":
					this.RenderGotFocusRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "KeyDown":
					this.RenderKeyDownRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Afterrender":
					this.RenderAfterrenderRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the WaitMaskDisabled property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWaitMaskDisabledProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If WaitMaskDisabled value is not default
					if (element.WaitMaskDisabled)
					{
						jsonWriter.WriteProperty("waitMaskDisabled",  element.WaitMaskDisabled );
					}
				}
			}

		}

		/// <summary>
        /// Renders the WaitMaskDisabled property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWaitMaskDisabledPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.waitMaskDisabled = {1};", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool( element.WaitMaskDisabled ));
			}

		}


		/// <summary>
        /// Renders the ToolTipText property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderToolTipTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.ToolTipText))
					{
						jsonWriter.WriteProperty("autoEl", new JavaScriptCode(String.Format(@"{{tag: 'label','data-qtip': '{0}'}}",  element.ToolTipText )));
					}
				}
			}

		}

		/// <summary>
        /// Renders the ToolTipText property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderToolTipTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.ariaEl.dom.setAttribute('data-qtip','{1}');", new JavaScriptCode(componentVariable),  element.ToolTipText);
			}

		}


		/// <summary>
        /// Renders the CssClass property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderCssClassProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.CssClass))
					{
						jsonWriter.WriteProperty("cls",  element.CssClass );
					}
				}
			}

		}

		/// <summary>
        /// Renders the CssClass property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCssClassPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MaximumSize property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaximumSizeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MaximumSize value is not default
					if (element.MaximumSize.Width != 0 || element.MaximumSize.Height !=0)
					{
						jsonWriter.WriteProperty("maxWidth",  element.MaximumSize.Width );
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MaximumSize value is not default
					if (element.MaximumSize.Width != 0 || element.MaximumSize.Height !=0)
					{
						jsonWriter.WriteProperty("maxHeight",  element.MaximumSize.Height );
					}
				}
			}

		}

		/// <summary>
        /// Renders the MaximumSize property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaximumSizePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setMaxWidth({1});{0}.setMaxHeight({2});", new JavaScriptCode(componentVariable), element.MaximumSize.Width , element.MaximumSize.Height );
			}

		}


		/// <summary>
        /// Renders the MinimumSize property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMinimumSizeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MinimumSize value is not default
					if (element.MinimumSize.Width != 0 || element.MinimumSize.Height !=0)
					{
						jsonWriter.WriteProperty("minWidth",  element.MinimumSize.Width );
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MinimumSize value is not default
					if (element.MinimumSize.Width != 0 || element.MinimumSize.Height !=0)
					{
						jsonWriter.WriteProperty("minHeight",  element.MinimumSize.Height );
					}
				}
			}

		}

		/// <summary>
        /// Renders the MinimumSize property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMinimumSizePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setMinWidth({1});{0}.setMinHeight({2});", new JavaScriptCode(componentVariable), element.MinimumSize.Width , element.MinimumSize.Height );
			}

		}


		/// <summary>
        /// Renders the ZIndex property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderZIndexProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If ZIndex value is not default
					if (element.ZIndex != -1)
					{
						jsonWriter.WriteProperty("zIndex",  ControlElementExtJSRenderer.ReviseZIndex(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the ZIndex property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderZIndexPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('zIndex','{1}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseZIndex(element) );
			}

		}


		/// <summary>
        /// Renders the Draggable property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDraggableProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Draggable value is not default
					if (element.Draggable)
					{
						jsonWriter.WriteProperty("draggable", element.Draggable );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Draggable property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDraggablePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"draggable ={0}", element.Draggable );
			}

		}


		/// <summary>
        /// Renders the AutoSize property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAutoSizeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If AutoSize value is not default
					if (!element.AutoSize)
					{
						jsonWriter.WriteProperty("overflow",  LabelElementExtJSRenderer.ReviseAutoSizeStyle(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the AutoSize property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAutoSizePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('overflow','{1}');", new JavaScriptCode(componentVariable),  LabelElementExtJSRenderer.ReviseAutoSizeStyle(element) );
			}

		}


		/// <summary>
        /// Renders the Font property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderFontProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If Font value is not default
					if (ControlElementExtJSRenderer.IsNonDefaultFont(element.Font))
					{
						jsonWriter.WriteProperty("font",  ControlElementExtJSRenderer.ReviseFont(element) );
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If Font value is not default
					if (ControlElementExtJSRenderer.IsNonDefaultFont(element.Font))
					{
						jsonWriter.WriteProperty("textDecoration",  ControlElementExtJSRenderer.ReviseFontDecoration(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Font property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderFontPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('font','{1}');{0}.setStyle('textDecoration','{2}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseFont(element) ,  ControlElementExtJSRenderer.ReviseFontDecoration(element) );
			}

		}


		/// <summary>
        /// Renders the RightToLeft property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderRightToLeftProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If RightToLeft value is not default
					if (element.RightToLeft != RightToLeft.Inherit)
					{
						jsonWriter.WriteProperty("rtl",  (element.RightToLeft == RightToLeft.Yes) ? true : false );
					}
				}
			}

		}

		/// <summary>
        /// Renders the RightToLeft property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRightToLeftPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the RightToLeftDecoration property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderRightToLeftDecorationProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If RightToLeftDecoration value is not default
					if (element.RightToLeft == RightToLeft.Yes)
					{
						jsonWriter.WritePropertyWithRawValue("direction", @"'rtl'");
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If RightToLeftDecoration value is not default
					if (element.RightToLeft == RightToLeft.Yes)
					{
						jsonWriter.WritePropertyWithRawValue("'text-align'", @"'right'");
					}
				}
			}

		}

		/// <summary>
        /// Renders the RightToLeftDecoration property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRightToLeftDecorationPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the BackgroundImage property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBackgroundImageProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If BackgroundImage value is not default
					if (element.BackgroundImage != null)
					{
						jsonWriter.WriteProperty("backgroundImage",  ControlElementExtJSRenderer.ReviseBackground(renderingContext, element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the BackgroundImage property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBackgroundImagePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('backgroundImage','{1}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseBackground(context, element) );
			}

		}


		/// <summary>
        /// Renders the BorderStyle property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBorderStyleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the BorderStyle property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBorderStylePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the BorderColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBorderColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the BorderColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBorderColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Controls property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderControlsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the Controls property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderControlsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ForeColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderForeColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("color",  ColorTranslator.ToHtml(element.ForeColor) );
				}
			}

		}

		/// <summary>
        /// Renders the ForeColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderForeColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('color','{1}');", new JavaScriptCode(componentVariable),  ColorTranslator.ToHtml(element.ForeColor) );
			}

		}


		/// <summary>
        /// Renders the BackColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBackColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If BackColor value is not default
					if (element.BackColor != Color.Empty)
					{
						jsonWriter.WriteProperty("background",  ColorTranslator.ToHtml(element.BackColor) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the BackColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBackColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('background','{1}');", new JavaScriptCode(componentVariable),  ColorTranslator.ToHtml(element.BackColor) );
			}

		}


		/// <summary>
        /// Renders the Width property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Width value is not default
					if (element.Width != 0)
					{
						jsonWriter.WriteProperty("width",  element.PixelWidth );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Width property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setWidth({1});", new JavaScriptCode(componentVariable),  element.PixelWidth);
			}

		}


		/// <summary>
        /// Renders the Tag property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTagProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the Tag property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTagPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Text property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Text value is not default
					if (element.Text != string.Empty )
					{
						// If property is valid
						if(ValidateProperty(element.Text))
						{
							jsonWriter.WriteProperty("text",  GetFormatedTextProperty(element, GetDisplayText(element.Text)) );
						}
					}
				}
			}

		}

		/// <summary>
        /// Renders the Text property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setText({1})", new JavaScriptCode(componentVariable),  GetFormatedTextProperty(element, GetDisplayText(element.Text)) );
			}

		}


		/// <summary>
        /// Renders the Height property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderHeightProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Height value is not default
					if (element.Height != 0)
					{
						jsonWriter.WriteProperty("height",  element.PixelHeight );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Height property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderHeightPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setHeight({1});", new JavaScriptCode(componentVariable),  element.PixelHeight);
			}

		}


		/// <summary>
        /// Renders the TabIndex property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTabIndexProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If TabIndex value is not default
					if (element.TabStop && element.TabIndex >= 0)
					{
						jsonWriter.WriteProperty("tabIndex",  element.TabIndex );
					}
				}
			}

		}

		/// <summary>
        /// Renders the TabIndex property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTabIndexPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setTabIndex({1});", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseTabIndex(element));
			}

		}


		/// <summary>
        /// Renders the Visible property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderVisibleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Visible value is not default
					if (!element.Visible)
					{
						jsonWriter.WriteProperty("hidden",  !element.Visible );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Visible property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderVisiblePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setHidden({1});", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool( !element.Visible ));
			}

		}


		/// <summary>
        /// Renders the Padding property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderPaddingProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("padding",  ControlElementExtJSRenderer.RevisePadding(element.Padding) );
				}
			}

		}

		/// <summary>
        /// Renders the Padding property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderPaddingPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle({{padding:'{1}'}}) ", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.FormmatPaddingString(element));
			}

		}


		/// <summary>
        /// Renders the CustomUI property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderCustomUIProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.CustomUI))
					{
						jsonWriter.WriteProperty("ui",  element.CustomUI );
					}
				}
			}

		}

		/// <summary>
        /// Renders the CustomUI property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCustomUIPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setUi({1});", new JavaScriptCode(componentVariable),  element.CustomUI);
			}

		}


		/// <summary>
        /// Renders the Resizable property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderResizableProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Resizable value is not default
					if (element.Resizable == true)
					{
						jsonWriter.WriteProperty("resizable",  element.Resizable );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Resizable property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderResizablePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the DataMember property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDataMemberProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.DataMember))
					{
						jsonWriter.WriteProperty("dataIndex",  element.DataMember );
					}
				}
			}

		}

		/// <summary>
        /// Renders the DataMember property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataMemberPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
			{

		}


		/// <summary>
        /// Renders the BackgroundImageLayout property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBackgroundImageLayoutProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If BackgroundImageLayout value is not default
					if (element.BackgroundImage != null)
					{
						jsonWriter.WriteProperty("backgroundSize",  ControlElementExtJSRenderer.ReviseBackgroundSize(element.BackgroundImageLayout) );
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If BackgroundImageLayout value is not default
					if (element.BackgroundImage != null)
					{
						jsonWriter.WriteProperty("backgroundRepeat",  ControlElementExtJSRenderer.ReviseBackgroundRepeat(element.BackgroundImageLayout) );
			}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If BackgroundImageLayout value is not default
					if (element.BackgroundImage != null)
					{
						jsonWriter.WriteProperty("backgroundPosition",  ControlElementExtJSRenderer.ReviseBackgroundPosition(element.BackgroundImageLayout) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the BackgroundImageLayout property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBackgroundImageLayoutPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@" {0}.setStyle('backgroundSize','{1}');{0}.setStyle('backgroundRepeat','{2}');{0}.setStyle('backgroundPosition','{3}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseBackgroundSize(element.BackgroundImageLayout) ,  ControlElementExtJSRenderer.ReviseBackgroundRepeat(element.BackgroundImageLayout) ,  ControlElementExtJSRenderer.ReviseBackgroundPosition(element.BackgroundImageLayout) );
			}

		}


		/// <summary>
        /// Renders the EnableKeyEvents property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnableKeyEventsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If EnableKeyEvents value is not default
					if (element.HasKeyPressListeners)
					{
						jsonWriter.WriteProperty("enableKeyEvents",  ControlElementExtJSRenderer.ReviseEnableKeyPress(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the EnableKeyEvents property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnableKeyEventsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the liveDrag property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderliveDragProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WritePropertyWithRawValue("liveDrag", @"true");
				}
			}

		}

		/// <summary>
        /// Renders the liveDrag property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderliveDragPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the IsFocused property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderIsFocusedProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If IsFocused value is not default
					if (element.IsFocused != false)
					{
						jsonWriter.WriteProperty("hasFocus",  element.IsFocused );
					}
				}
			}

		}

		/// <summary>
        /// Renders the IsFocused property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderIsFocusedPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Margin property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMarginProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("bodyPadding",  ControlElementExtJSRenderer.RevisePadding(element.Margin) );
				}
			}

		}

		/// <summary>
        /// Renders the Margin property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMarginPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ReadOnly property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderReadOnlyProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ReadOnly property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderReadOnlyPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "RaiseUpdateClient":
					return this.ExtractRaiseUpdateClientMethodCallbackData(context, eventObject);						
				case "GetActiveControl":
					return this.ExtractGetActiveControlMethodCallbackData(context, eventObject);						
				case "SetActiveControl":
					return this.ExtractSetActiveControlMethodCallbackData(context, eventObject);						
				case "SetError":
					return this.ExtractSetErrorMethodCallbackData(context, eventObject);						
				case "Focus":
					return this.ExtractFocusMethodCallbackData(context, eventObject);						
				case "SetLoading":
					return this.ExtractSetLoadingMethodCallbackData(context, eventObject);						
				case "Refresh":
					return this.ExtractRefreshMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "RaiseUpdateClient":
					this.RenderRaiseUpdateClientMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "GetActiveControl":
					this.RenderGetActiveControlMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetActiveControl":
					this.RenderSetActiveControlMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetError":
					this.RenderSetErrorMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "Focus":
					this.RenderFocusMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetLoading":
					this.RenderSetLoadingMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "Refresh":
					this.RenderRefreshMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the RaiseUpdateClient method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderRaiseUpdateClientMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method RaiseUpdateClient callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractRaiseUpdateClientMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
        }


        /// Renders the GetActiveControl method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderGetActiveControlMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"
setTimeout(function(){{
    try {{
        var activeControl =Ext.get(Ext.Element.getActiveElement());
        console.log(activeControl);
        var activeControlId = """";
        if (activeControl != null) {{
            activeControlId = activeControl.component.itemId;
        }}
        VT_raiseEvent({0}, 'Callback', {{value:activeControlId}});;
    }} catch (e) {{
        console.log(""GetActiveControl.js error : "" + e.message);
    }}
}},100);",  System.Web.VisualTree.Elements.VisualElement.GetElementId(methodInvoker) );
				}
			}

		}


		/// <summary>
        /// Gets the method GetActiveControl callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractGetActiveControlMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return ControlElementExtJSRenderer.GetControlId(eventObject.Value<System.String>("value"));
		}


        /// Renders the SetActiveControl method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetActiveControlMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.String data = (System.String)methodData;
					builder.AppendFormattedRawValue(@"try {{
    var controlId = {0};
    var control = VT_GetCmp(controlId);

    if(control != null){{
        control.focus();
    }}
}} catch (e) {{
    console.log(""an error has occurred in SetActiveControl.js : "" + e.message);
}}",  data );
				}
			}

		}


		/// <summary>
        /// Gets the method SetActiveControl callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetActiveControlMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetError method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetErrorMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"{0}.setActiveError({1});", new JavaScriptCode(componentVariableName),  data );
				}
			}

		}


		/// <summary>
        /// Gets the method SetError callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetErrorMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the Focus method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderFocusMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"var control = {0}; if(control != null){{control.focus();}}", new JavaScriptCode(componentVariableName));
				}
			}

		}


		/// <summary>
        /// Gets the method Focus callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractFocusMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetLoading method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetLoadingMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.ControlElement element = visualElement as System.Web.VisualTree.Elements.ControlElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"{0}.setLoading({1}); if(Ext.isBoolean({2}) &&  {0}.view && {0}.view.loadMask ){{ if({3}){{{0}.view.loadMask.bindStore && {0}.view.loadMask.bindStore({0}.getStore()) }} else {{{0}.view.loadMask.unbindStoreListeners({0}.getStore()); }}}} ", new JavaScriptCode(componentVariableName),  data ,  data ,  data );
				}
			}

		}


		/// <summary>
        /// Gets the method SetLoading callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetLoadingMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the Refresh method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderRefreshMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method Refresh callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractRefreshMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}