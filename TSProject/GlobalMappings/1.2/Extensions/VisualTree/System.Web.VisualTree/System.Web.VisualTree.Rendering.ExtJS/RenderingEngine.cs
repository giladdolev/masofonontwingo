﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;

namespace System.Web.VisualTree
{
    
    [Export(typeof(IRenderingEngine))]
    public class RenderingEngine : IRenderingEngine
    {
        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="renderTo">The render to.</param>
        public void Render(RenderingContext renderingContext, IVisualElement visualElement, IO.TextWriter writer, string renderTo)
        {
            // Render control
            RenderingUtils.Render(renderingContext, visualElement as VisualElement, writer, renderTo);
        }
    }
}
