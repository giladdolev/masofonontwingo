﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.Mvc.VisualTree.Rendering.ExtJS
{
    public class RangeElementExtJSRenderer : ContentControlElementExtJSRenderer
    {

        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.button.Button";
            }

        }



        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "button";
            }
        }


        public string tempcontenttoremove
        {
            get { return "TrackBarElement"; }
        }
    }
}
