﻿using Newtonsoft.Json;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    /// <summary>
    /// Provides a temporary place holder for controls
    /// </summary>
	public class DefaultControlElementExtJSRenderer : ControlElementExtJSRenderer
    {
        /// <summary>
        /// Gets the ext js type.
        /// </summary>
        /// <value>
        /// The ext js type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return _defaultExtJSType;
            }
        }


        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return _defaultXType;
            }
        }

        /// <summary>
        /// Renders the text property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderDefaultTextProperty(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderStyleProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderDefaultStyleProperty(renderingContext, jsonWriter);
        }
    }
}
