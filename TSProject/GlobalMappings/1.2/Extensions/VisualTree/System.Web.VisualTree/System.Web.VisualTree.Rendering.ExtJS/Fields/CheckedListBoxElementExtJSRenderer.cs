﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(CheckedListBoxElement), "ListControlElementExtJSRenderer", "checkboxgroup", "Ext.form.CheckboxGroup")]
    [ExtJSPropertyDescription("columns", PropertyInitializeCode = "columns = 1")]
    [ExtJSPropertyDescription("AutoScroll", PropertyInitializeCode = "scrollable = <#= element.AutoScroll #>", ClientNotDefaultCode = "element.AutoScroll", PropertyChangeCode = "this.setScrollable(<#= JavaScriptCode.GetBool( element.AutoScroll )#>)")]

    [ExtJSMethodDescription("SetItemChecked", MethodCode = "this.items.items[<#=data.Index#>].setValue(<#=data.IsChecked#>);", MethodDataType = typeof(CheckItemData))]

    [ExtJSEventDescription("ItemCheck", ClientEventName = "change", ClientEventHandlerParameterNames = " view,newValue,oldValue,eOpts", ClientHandlerCallbackData = " value = newValue.items, oldVal = oldValue.items", ServerEventArgsCreateCode = "new ItemCheckEventArgs(FormatHelper.FormatItemCheckedArgs(this.value.ToObject()), FormatHelper.FormatStateCheckedArgs(this.value.ToObject(), this.oldVal.ToObject()) ,FormatHelper.FormatIndexCheckedArgs(this.value.ToObject(), this.oldVal.ToObject()))")]

    public class CheckedListBoxElementExtJSRenderer : CheckedListBoxElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
            CheckedListBoxElement objCheckedListBox = visualElement as CheckedListBoxElement;
            if (objCheckedListBox != null)
            {
                jsonWriter.WritePropertyName("items");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    foreach (ListItem item in objCheckedListBox.Items)
                    {
                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                        {
                            jsonWriter.WriteProperty("boxLabel", Convert.ToString(item.Text));
                            jsonWriter.WriteProperty("inputValue", objCheckedListBox.Items.IndexOf(item).ToString());
                            jsonWriter.WriteProperty("name", "items");
                        }
                    }
                }

            }
        }
    }
}
