﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree.Fields
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(LinkLabelElement), "LabelElementExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = " html = <a class='link' href = '<#= element.LinkData #>' > <#= element.Text #> </a>")]
    public class LinkLabelElementExtJSRenderer : LinkLabelElementExtJSRendererBase
    {
    }
}
