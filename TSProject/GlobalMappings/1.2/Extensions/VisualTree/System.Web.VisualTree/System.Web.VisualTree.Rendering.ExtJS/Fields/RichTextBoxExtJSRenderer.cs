﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(RichTextBox), "TextBoxBaseElementExtJSRenderer", "htmleditor", "Ext.form.field.HtmlEditor")]
    [ExtJSPropertyDescription("EnableLinks", PropertyInitializeCode = "enableLinks = false")]
    [ExtJSPropertyDescription("EnableLists", PropertyInitializeCode = "enableLists = false")]
    [ExtJSPropertyDescription("EnableSourceEdit", PropertyInitializeCode = "enableSourceEdit = false")]
    [ExtJSPropertyDescription("EnableColors", PropertyInitializeCode = "enableColors = false")]
    [ExtJSPropertyDescription("EnableFontSize", PropertyInitializeCode = "enableFontSize = false")]
    [ExtJSPropertyDescription("EnableFormat", PropertyInitializeCode = "enableFormat = false")]
    [ExtJSPropertyDescription("EnableAlignments", PropertyInitializeCode = "enableAlignments = false")]
    [ExtJSPropertyDescription("EnableFont", PropertyInitializeCode = "enableFont = false")]
    [ExtJSPropertyDescription("SelectionColor", PropertyChangeCode = "this.relayCmd('forecolor','<#= ColorTranslator.ToHtml(element.SelectionColor) #>');")]
    [ExtJSPropertyDescription("SelectionFont", PropertyChangeCode = "this.relayCmd('fontname','<#= element.FontName#>');this.relayCmd('fontsize','<#= element.FontSize#>');")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "value = <#= GetDisplayText(element.Text) #>", PropertyChangeCode = "this.setValue('<#= GetDisplayText(element.Text) #>');")]
    public class RichTextBoxExtJSRenderer : RichTextBoxExtJSRendererBase
    {

    }
}
