﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Text;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TextBoxBaseElement), "InputElementExtJSRenderer", "textfield", "Ext.form.field.Text")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "fieldStyle.background-color = <#= ColorTranslator.ToHtml(element.BackColor) #>", PropertyChangeCode = "this.setFieldStyle('background-color:<#= ColorTranslator.ToHtml(element.BackColor) #>');")]
    [ExtJSPropertyDescription("EnforceMaxLength", PropertyInitializeCode = "enforceMaxLength = true", ClientNotDefaultCode = "element.MaxLength > 0")]
    [ExtJSPropertyDescription("Font", PropertyInitializeCode = "fieldStyle.font = <#= ControlElementExtJSRenderer.ReviseFont(element) #>,fieldStyle.text-decoration = <#= ControlElementExtJSRenderer.ReviseFontDecoration(element) #>", ClientNotDefaultCode = "ControlElementExtJSRenderer.IsNonDefaultFont(element.Font)", PropertyChangeCode = "this.setFieldStyle('font:<#= ControlElementExtJSRenderer.ReviseFont(element) #>;text-decoration:<#= ControlElementExtJSRenderer.ReviseFontDecoration(element) #>');")]
    [ExtJSPropertyDescription("ForeColor", PropertyInitializeCode = "fieldStyle.color = <#= ColorTranslator.ToHtml(element.ForeColor) #>", ClientNotDefaultCode = "element.ForeColor != Color.Empty", PropertyChangeCode = "this.setFieldStyle('color:<#= ColorTranslator.ToHtml(element.ForeColor) #>');")]
    [ExtJSPropertyDescription("MaxLength", PropertyInitializeCode = "maxLength = <#= element.MaxLength #>")]
    [ExtJSPropertyDescription("ReadOnly", PropertyInitializeCode = "readOnly = <#= element.ReadOnly #>", PropertyChangeCode = "this.setReadOnly(<#= JavaScriptCode.GetBool(element.ReadOnly) #>);")]
    [ExtJSPropertyDescription("SelectionLength", PropertyChangeCode = "this.selectText(<#=element.SelectionStart#>,<#=element.SelectionLength#>)", ClientNotDefaultCode = "element.SelectionLength > 0")]
    [ExtJSPropertyDescription("SelectOnFocus", PropertyInitializeCode = "selectOnFocus = <#=element.SelectOnFocus #>")]
    public abstract class TextBoxBaseElementExtJSRenderer : TextBoxBaseElementExtJSRendererBase
    {
         
    }
}