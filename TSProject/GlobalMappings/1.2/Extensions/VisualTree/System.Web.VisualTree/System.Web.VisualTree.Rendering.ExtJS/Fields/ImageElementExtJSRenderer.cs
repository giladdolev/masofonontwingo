﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ImageElement), "ControlElementExtJSRenderer", "image", "Ext.Img")]
    [ExtJSPropertyDescription("Image", PropertyInitializeCode = "src = <#= FormatHelper.FormatResource(renderingContext, element.Image) #>", PropertyChangeCode = "this.setSrc('<#= FormatHelper.FormatResource(context, element.Image) #>');")]
    [ExtJSPropertyDescription("BorderStyle", PropertyInitializeCode = "Style.border = <#= ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) #>,Style.borderRight=<#= ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) #>,Style.borderBottom=<#= ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) #>", PropertyChangeCode = "this.setStyle({<#= ControlElementExtJSRenderer.ReviseBorderStyle(element) #>});")]
    public class ImageElementExtJSRenderer : ImageElementExtJSRendererBase
    {
        protected override void RenderEvents(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            if(jsonWriter != null)
            {
                jsonWriter.WritePropertyName("el");

                using(jsonWriter.OpenScope( RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    base.RenderEvents(renderingContext, visualElement, jsonWriter);
                }
            }
            
        }


        /// <summary>
        /// Renders the width property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderWidthProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ControlElement controlElement = visualElement as ControlElement;
            // validate parameter
            if (controlElement == null)
            {
                return;
            }

            int width = GetEffectiveWidth(controlElement);
            if (width > 0)
            {
                jsonWriter.WriteProperty("width", width);
            }
        }

        /// <summary>
        /// Should render height property.
        /// </summary>
        /// <returns></returns>
        protected override bool ShouldRenderHeightProperty()
        {
            return true;
        }

        /// <summary>
        /// Should render width property.
        /// </summary>
        /// <returns></returns>
        protected override bool ShouldRenderWidthProperty()
        {
            return true;
        }
    }
}
