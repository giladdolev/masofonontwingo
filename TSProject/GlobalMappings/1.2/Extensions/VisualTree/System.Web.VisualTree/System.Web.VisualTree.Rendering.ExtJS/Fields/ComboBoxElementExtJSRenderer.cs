﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ComboBoxElement), "ListControlElementExtJSRenderer", "combo", "Ext.form.field.ComboBox")]
    [ExtJSPropertyDescription("AutoCompleteMode", PropertyInitializeCode = "typeAhead = true ", ClientNotDefaultCode = "element.AutoCompleteMode !=AutoCompleteMode.None")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "FieldStyle.background = <#= ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != Color.Empty", PropertyChangeCode = "this.setFieldStyle('background:<#= ColorTranslator.ToHtml(element.BackColor) #>');")]
    [ExtJSPropertyDescription("DataSource")]
    [ExtJSPropertyDescription("DropDownHeight", PropertyChangeCode = "this.getPicker().setBox({ height: <#= element.DropDownHeight #> });")]
    [ExtJSPropertyDescription("DropDownStyle", PropertyInitializeCode = "editable = <#=ComboBoxElementExtJSRenderer.ReviseEditableProperty(element) #>", PropertyChangeCode = "this.setEditable(<#= JavaScriptCode.GetBool(ComboBoxElementExtJSRenderer.ReviseEditableProperty(element)) #>)")]
    [ExtJSPropertyDescription("DropDownWidth", PropertyChangeCode = "this.getPicker().setBox({ width: <#= element.DropDownWidth #> });")]
    [ExtJSPropertyDescription("MatchComboboxWidth", PropertyInitializeCode = "matchFieldWidth = <#= element.MatchComboboxWidth #>", ClientNotDefaultCode = "element.MatchComboboxWidth != true")]
    [ExtJSPropertyDescription("minChars", PropertyInitializeCode = " minChars = 1", ClientNotDefaultCode = "element.AutoCompleteMode !=AutoCompleteMode.None")]
    [ExtJSPropertyDescription("queryMode", PropertyInitializeCode = "queryMode = 'local'")]
    [ExtJSPropertyDescription("SelectTextOnFocus", PropertyInitializeCode = "selectOnFocus = <#=  element.SelectTextOnFocus #>", ClientNotDefaultCode = "element.SelectTextOnFocus != false && ComboBoxElementExtJSRenderer.ReviseEditableProperty(element) == true && element.Locked != true")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "value = <#= ComboBoxElementExtJSRenderer.GetValue(element) #>", PropertyChangeCode = "this.setValue('<#= ComboBoxElementExtJSRenderer.GetValue(element) #>');")]
    [ExtJSPropertyDescription("Locked", PropertyInitializeCode = "this.setEditable(<#=JavaScriptCode.GetBool( !(element.Locked)) #>)", PropertyChangeCode = "this.setEditable(<#= JavaScriptCode.GetBool( !(element.Locked)) #>);")]

    [ExtJSMethodDescription("ExpandDropDownList", MethodCode = "this.expand()")]

    [ExtJSEventDescription("BeforeSelect", ClientEventName = "beforeselect", ClientEventHandlerParameterNames = "combo, record, index, eOpts ")]
    [ExtJSMethodDescription("CollapseDropDownList", MethodCode = "this.collapse()")]

    [ExtJSEventDescription("SelectedIndexChanged", ClientEventName = "select", ClientEventHandlerParameterNames = "combo,records,eOpts", ClientHandlerCallbackData = "selectedIndex = VT_GetComboboxSelectedIndex(combo,records) ", ServerEventArgsCreateCode = "new ValueChangedArgs<int>(this.selectedIndex.ToInt32())")]
    [ExtJSEventDescription("Validated", ClientEventName = "validitychange")]
    public class ComboBoxElementExtJSRenderer : ComboBoxElementExtJSRendererBase
    {

        /// <summary>
        /// Revises the XTYPE property value.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <returns></returns>
        protected override string ReviseXTypePropertyValue(VisualElement visualElement, string xtype)
        {
            // Get ComboBoxElement .
            ComboBoxElement combo = visualElement as ComboBoxElement;

            // If there is a valid ComboBoxElement
            if (combo != null)
            {
                // If there is text
                if (combo.AutoCompleteMode == AutoCompleteMode.Suggest || combo.AutoCompleteMode == AutoCompleteMode.SuggestAppend)
                {
                    // Return title bar XTYPE
                    return "vtcombo";
                }
            }

            return xtype;
        }

        /// <summary>
        /// Indicates if we should render the BeforeSelect event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderBeforeSelectEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element)
        {
            return (!String.IsNullOrEmpty(element.ClientBeforeSelect) && element.HasBeforeSelectListeners);
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
            jsonWriter.WritePropertyName("defaultListConfig");
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
            {
                //dedault by sencha in case of No DataSource or empty
                jsonWriter.WriteProperty("minHeight", 26);
                jsonWriter.WriteProperty("minWidth", 70);
            }

            //Get the to comboBox Element .
            ComboBoxElement element = visualElement as ComboBoxElement;
            jsonWriter.WritePropertyName("listConfig");
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
            {
                // MatchComboboxWidth must be false to set the DropDown width .
                if (element.MatchComboboxWidth == false && element.DropDownWidth > 0)
                {
                    jsonWriter.WriteProperty("width", element.DropDownWidth);
                }
                if (element.DropDownHeight > 0)
                {
                    jsonWriter.WriteProperty("height", element.DropDownHeight);
                }
            }
        }

        /// <summary>
        /// Revises the editable property.
        /// </summary>
        /// <param name="comboBox">The ComboBox instance being rendered.</param>
        /// <returns></returns>
        internal static bool ReviseEditableProperty(ComboBoxElement comboBox)
        {
            // Default ExtJS value according to Sencha Docs
            bool IsEditable = true;

            if (comboBox != null)
            {
                if (comboBox.Locked == true)
                {
                    return false;
                }
                if (comboBox.DropDownStyle == ComboBoxStyle.DropDown || comboBox.DropDownStyle == ComboBoxStyle.Simple)
                {
                    IsEditable = true;
                }
                if (comboBox.DropDownStyle == ComboBoxStyle.DropDownList)
                {
                    IsEditable = false;
                }
            }

            return IsEditable;
        }

        /// <summary>
        /// Get selected value.
        /// </summary>
        /// <returns>
        /// The selected value.
        /// </returns>
        internal static string GetValue(ComboBoxElement element)
        {
            if (element != null)
            {
                int index = element.SelectedIndex;
                if (index > -1 && index < element.Items.Count)
                {
                    if (!string.IsNullOrEmpty(element.ValueMember))
                    {
                        return Convert.ToString(element.Items[index].Value);
                    }
                }
                else
                {
                    return element.Text;
                }
            }
            return "";
        }

        /// <summary>
        /// Renders the AutoCompleteMode property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAutoCompleteModeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                switch (element.AutoCompleteMode)
                {
                    case AutoCompleteMode.None:
                        element.AllowExpand = false;
                        jsonWriter.WritePropertyWithRawValue("typeAhead", @"false");
                        jsonWriter.WritePropertyWithRawValue("autoComplete", @"false");
                        break;
                    case AutoCompleteMode.Append:
                        element.AllowExpand = false;
                        jsonWriter.WritePropertyWithRawValue("typeAhead", @"true");
                        jsonWriter.WritePropertyWithRawValue("autoComplete", @"true");
                        break;
                    case AutoCompleteMode.Suggest:
                        element.AllowExpand = true;
                        jsonWriter.WritePropertyWithRawValue("typeAhead", @"false");
                        jsonWriter.WritePropertyWithRawValue("autoComplete", @"false");
                        break;
                    case AutoCompleteMode.SuggestAppend:
                        element.AllowExpand = true;
                        jsonWriter.WritePropertyWithRawValue("typeAhead", @"true");
                        jsonWriter.WritePropertyWithRawValue("autoComplete", @"true");
                        break;

                }

            }
        }

        /// <summary>
        /// Renders the changes of AutoCompleteMode property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            ComboBoxElement element = visualElement as ComboBoxElement;
            base.RenderPropertyChange(context, visualElement, property, builder, componentVariable);
            if (property == "AutoCompleteMode")
            {
                // If there is a valid typed element
                if (element != null)
                {
                    // Check rendering type
                    switch (element.AutoCompleteMode)
                    {
                        case AutoCompleteMode.None:
                            builder.AppendRawValue("cmp.typeAhead = false;");
                            builder.AppendRawValue("cmp.autoComplete = false;");
                            element.AllowExpand = false;
                            break;
                        case AutoCompleteMode.Append:
                            builder.AppendRawValue("cmp.typeAhead = true;");
                            builder.AppendRawValue("cmp.autoComplete = true;");
                            element.AllowExpand = false;
                            break;
                        case AutoCompleteMode.Suggest:
                            builder.AppendRawValue("cmp.typeAhead = false;");
                            builder.AppendRawValue("cmp.autoComplete = false;");
                            element.AllowExpand = true;
                            break;
                        case AutoCompleteMode.SuggestAppend:
                            builder.AppendRawValue("cmp.typeAhead = true;");
                            builder.AppendRawValue("cmp.autoComplete = true;");
                            element.AllowExpand = true;
                            break;

                    }
                    builder.AppendRawValue("cmp.setAllowExpand(" + JavaScriptCode.GetBool(element.AllowExpand) + ");");//Renders AllowExpand change

                }
            }
        }

    }
}
