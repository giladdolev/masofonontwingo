﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(LabelElement), "ContentControlElementExtJSRenderer", "label", "Ext.form.Label")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "text = <#= GetDisplayText(element.Text) #>", PropertyChangeCode = "this.setText('<#= JavaScriptUtilities.ParseString(element.Text) #>');")]
    [ExtJSPropertyDescription("AutoEllipsis", PropertyInitializeCode = "Style.textOverflow=<#= LabelElementExtJSRenderer.ReviseAutoEllipsisStyle(element) #>,Style.whiteSpace=<#= LabelElementExtJSRenderer.ReviseWhiteSpaceStyle(element) #>", PropertyChangeCode = "this.setStyle({textOverflow:'<#= LabelElementExtJSRenderer.ReviseAutoEllipsisStyle(element) #>',whiteSpace:'<#= LabelElementExtJSRenderer.ReviseWhiteSpaceStyle(element) #>'});")]
    [ExtJSPropertyDescription("AutoSize", PropertyInitializeCode = "shrinkWrap = <#= JavaScriptCode.GetBool( element.AutoSize )#>; ", ClientNotDefaultCode = "element.AutoSize != false")]
    [ExtJSPropertyDescription("BorderStyle", PropertyInitializeCode = "Style.border = <#= ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) #>,Style.borderRight=<#= ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) #>,Style.borderBottom=<#= ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) #>", PropertyChangeCode = "this.setStyle({<#= ControlElementExtJSRenderer.ReviseBorderStyle(element) #>});")]
    [ExtJSPropertyDescription("Content", PropertyInitializeCode = "html = <#= Convert.ToString(element.Content) #>")]
    [ExtJSPropertyDescription("StyleOverflow", PropertyInitializeCode = "Style.overflowX='hidden',Style.overflowY = 'hidden'", ClientNotDefaultCode = "!element.AutoSize")]
    [ExtJSPropertyDescription("Enabled", PropertyInitializeCode = "Style.backgroundColor = <#= LabelElementExtJSRenderer.ReviseDisabledBackGroundColor(element) #>,Style.color = <#= LabelElementExtJSRenderer.ReviseDisabledForeColor(element) #>", ClientNotDefaultCode = "!element.Enabled", PropertyChangeCode = "this.setStyle('backgroundColor','<#= LabelElementExtJSRenderer.ReviseDisabledBackGroundColor(element) #>'),this.setStyle('color','<#= LabelElementExtJSRenderer.ReviseDisabledForeColor(element) #>');")]
    [ExtJSPropertyDescription("TextAlign", PropertyInitializeCode = "Style.textAlign = <#= LabelElementExtJSRenderer.ReviseTextAlignment(element) #>", ClientNotDefaultCode = "element.TextAlign != Drawing.ContentAlignment.TopLeft", PropertyChangeCode = "this.setStyle('textAlign','<#= LabelElementExtJSRenderer.ReviseTextAlignment(element) #>');")]
    [ExtJSPropertyDescription("MaxWidth", PropertyInitializeCode = "maxWidth = <#=element.MaxWidth#> ", PropertyChangeCode = "this.setMaxWidth(<#=element.MaxWidth#>);", ClientNotDefaultCode = "element.AutoSize == true")]
    [ExtJSPropertyDescription("MaxHeight", PropertyInitializeCode = "maxHeight  = <#=element.MaxHeight #> ", PropertyChangeCode = "this.setMaxHeight (<#=element.MaxHeight #>);", ClientNotDefaultCode = "element.AutoSize == true ")]
    [ExtJSPropertyDescription("Image", PropertyInitializeCode = "Style.background = <#= FormatHelper.FormatResourceUrl(renderingContext, element.Image) #>", PropertyChangeCode = "this.setStyle('background-image',' <#= FormatHelper.FormatResourceUrl(context, element.Image) #>');", ClientNotDefaultCode = "Convert.ToString(element.Image.Source) != string.Empty ")]
    [ExtJSPropertyDescription("ImageAlign", PropertyInitializeCode = "Style.backgroundPosition = <#=LabelElementExtJSRenderer.ReviseImageAlignment(element)#>", PropertyChangeCode = "this.setStyle('background-position','<#=LabelElementExtJSRenderer.ReviseImageAlignment(element)#>');")]
    public class LabelElementExtJSRenderer : LabelElementExtJSRendererBase
    {

        /// <summary>
        /// Gets the display text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        protected override string GetDisplayText(string text)
        {
            
            if(text.Contains("\\r\\n")){
                text = text.Replace("\\r\\n", "\r\n");
            }
            if (text.Contains("\\n"))
            {
                text = text.Replace("\\n", "\n");
            }
            return GetDisplayTextWithoutCommands(text);
        }

        /// <summary>
        /// Revises the automatic ellipsis style.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static string ReviseAutoEllipsisStyle(LabelElement element)
        {
            return element.AutoEllipsis ? "ellipsis" : "clip";
        }
        /// <summary>
        /// Revises the white space style.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static string ReviseWhiteSpaceStyle(LabelElement element)
        {
            return "nowrap";
        }

        /// <summary>
        /// Revise Disabled Enabled Back Ground Color
        /// </summary>
        /// <param name="enabled">enabled</param>
        /// <returns>enabled color name</returns>
        internal static string ReviseDisabledBackGroundColor(LabelElement element)
        {
            if (element.Enabled)
            {
                return Color.Transparent.Name;
            }
            else
            {
                return "#F0F0F0";
            }

        }

        /// <summary>
        /// Revise Disabled Enabled forecolor
        /// </summary>
        /// <param name="enabled">enables</param>
        /// <returns>enables for color name </returns>
        internal static string ReviseDisabledForeColor(LabelElement element)
        {
            if (element.Enabled)
            {
                return element.ForeColor.Name;
            }
            else
            {
                return "Gray";
            }
        }

        /// <summary>
        /// Revises the text alignment.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        internal static string ReviseTextAlignment(LabelElement control)
        {
            switch (control.TextAlign)
            {
                case Drawing.ContentAlignment.BottomCenter:
                case Drawing.ContentAlignment.MiddleCenter:
                case Drawing.ContentAlignment.TopCenter:
                    return "center";
                case Drawing.ContentAlignment.BottomLeft:
                case Drawing.ContentAlignment.MiddleLeft:
                case Drawing.ContentAlignment.TopLeft:
                    return "left";
                case Drawing.ContentAlignment.BottomRight:
                case Drawing.ContentAlignment.MiddleRight:
                case Drawing.ContentAlignment.TopRight:
                    return "right";
                default:
                    return "left";
            }
        }

        /// Revises the text alignment.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        internal static string ReviseImageAlignment(LabelElement control)
        {
            switch (control.ImageAlign)
            {
                case Drawing.ContentAlignment.BottomCenter:
                    return "bottom";
                case Drawing.ContentAlignment.MiddleCenter:
                    return "center";
                case Drawing.ContentAlignment.TopCenter:
                    return "top";
                case Drawing.ContentAlignment.BottomLeft:
                    return "bottom left";
                case Drawing.ContentAlignment.MiddleLeft:
                    return "left";
                case Drawing.ContentAlignment.TopLeft:
                    return "unset";
                case Drawing.ContentAlignment.BottomRight:
                    return "bottom right";
                case Drawing.ContentAlignment.MiddleRight:
                    return "right";
                case Drawing.ContentAlignment.TopRight:
                    return "top right";
                default:
                    return "initialize";
            }
        }

    }
}