﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.VisualTree.Rendering;

namespace System.Web.VisualTree.Fields
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(MaskedTextBoxElement), "TextBoxBaseElementExtJSRenderer", "maskedinput", "VT.ux.MaskedTextBox")]
    [ExtJSPropertyDescription("AllowBlank", PropertyInitializeCode = "allowBlank = <#=element.AllowBlank#>", ClientNotDefaultCode = "!element.AllowBlank")]
    [ExtJSPropertyDescription("Mask", PropertyInitializeCode = "inputMask = <#= GetDisplayText(element.Mask) #>")]
    [ExtJSPropertyDescription("PromptChar", PropertyInitializeCode = "promptChar = <#= GetDisplayText(element.PromptChar.ToString()) #>")]
    [ExtJSPropertyDescription("MaskedText", PropertyInitializeCode = "value = <#= GetDisplayText(element.MaskedText) #>", PropertyChangeCode = "this.setValue('<#= element.MaskedText #>');")]
    [ExtJSPropertyDescription("Regex", PropertyInitializeCode = "regex = <#= GetDisplayText(element.Regex) #>")]
    [ExtJSEventDescription("TextChanged", ClientEventName = "change", ClientEventHandlerParameterNames = "view, newValue, oldValue, eOpts", ClientHandlerCallbackData = "value = VT_GetMaskedTextboxValue(view,newValue)", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)")]
    [ExtJSPropertyDescription("TextMaskFormat", PropertyInitializeCode = "textMaskFormat = <#= GetDisplayText(element.TextMaskFormat.ToString()) #>")]
    
    public class MaskedTextBoxElementExtJSRenderer : MaskedTextBoxElementExtJSRendererBase
    {

        /// <summary> 
        /// Revise the event behavior type of TextChanged event.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        /// <returns></returns>
        protected override ExtJSEventBehaviorType ReviseTextChangedEventBehavior(RenderingContext renderingContext, ControlElement element, ExtJSEventBehaviorType eventBehaviorType)
        {
            if (element.HasTextChangedListeners)
            {
                return ExtJSEventBehaviorType.Immediate;
            }
            return ExtJSEventBehaviorType.Queued;
        }



        /// <summary>
        /// Indicates if we should render the TextChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <returns></returns>
        protected override bool ShouldRenderTextChangedEvent(RenderingContext context, ControlElement element)
        {
            return true;
        }
    }
}
