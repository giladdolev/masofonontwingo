﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.Drawing;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ButtonElement), "ButtonBaseElementExtJSRenderer", "button", "Ext.button.Button")]
    [ExtJSPropertyDescription("BorderStyle", PropertyInitializeCode = "Style.border = <#= ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) #>, Style.borderRight=<#= ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) #>,Style.borderBottom=<#= ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) #>", PropertyChangeCode = "this.setStyle({<#= ControlElementExtJSRenderer.ReviseBorderStyle(element) #>});")]
    [ExtJSPropertyDescription("EnableToggle", ClientNotDefaultCode = "element.EnableToggle != false", PropertyInitializeCode = "enableToggle = <#= element.EnableToggle #>", PropertyChangeCode = "this.toggle(<#= JavaScriptCode.GetBool(element.EnableToggle )#>)  ")]
    [ExtJSPropertyDescription("ForeColor", PropertyChangeCode = "this.setText('<#= ControlElementExtJSRenderer.ReviseFormattedText(element) #>');")]
    [ExtJSPropertyDescription("Font", PropertyChangeCode = "this.setText('<#= ControlElementExtJSRenderer.ReviseFormattedText(element) #>');")]
    [ExtJSPropertyDescription("CssClass", PropertyInitializeCode = "iconCls = <#= element.CssClass #>", PropertyChangeCode = "this.setIconCls(<#= element.CssClass #>)", ClientNotDefaultCode = " !String.IsNullOrEmpty(element.CssClass)")]
    [ExtJSPropertyDescription("IsPressed", PropertyInitializeCode = "pressed = <#= element.IsPressed #>", ClientNotDefaultCode = "element.IsPressed != false", PropertyChangeCode = "this.setPressed('<#= element.IsPressed #>');")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "text = <#= ControlElementExtJSRenderer.ReviseFormattedText(element) #>",PropertyChangeCode = "this.setText('<#= ControlElementExtJSRenderer.ReviseFormattedText(element) #>');")]
  
    [ExtJSEventDescription("PressedChange", ClientEventName = "toggle", ClientEventHandlerParameterNames = " view, pressed, eOpts ", ClientHandlerCallbackData = "pressState = pressed",ServerEventArgsCreateCode = "new ButtonPressedChangeEventArgs(Convert.ToBoolean(this.pressState))")]
   
    [ExtJSPropertyDescription("Scale", PropertyInitializeCode = "scale = <#= ButtonElementExtJSRenderer.ReviseScale(element) #>", ClientNotDefaultCode = "element.Scale != ScaleConstants.Small", PropertyChangeCode = "this.setScale('<#= ButtonElementExtJSRenderer.ReviseScale(element) #>');")]
    
    public class ButtonElementExtJSRenderer : ButtonElementExtJSRendererBase
    {
         
        /// <summary>
        /// Gets a value indicating whether requires formatted text.
        /// </summary>
        /// <value>
        ///   <c>true</c> if requires formatted text; otherwise, <c>false</c>.
        /// </value>
        protected override bool RequiresFormatedText
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Revise the Button Scale
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string ReviseScale(ButtonElement element)
        {
            if (element != null)
            {
                switch (element.Scale)
                {
                    case ScaleConstants.Medium:
                        return "medium";
                    case ScaleConstants.Large:
                        return "large";
                }
            }
            return "small";
        }
    }
}
