﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ComboGridElement), "GridElementExtJSRenderer", "Grid-Combo", "VT.ux.ComboGrid")]
    [ExtJSPropertyDescription("DisplayMember", PropertyInitializeCode = "displayField = <#= element.DisplayMember #>", PropertyChangeCode = "this.setDisplayField('<#= element.DisplayMember #>');")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "value = <#= GetFormatedTextProperty(element, GetDisplayText(element.Text)) #>", PropertyChangeCode = "this.setValue('<#= GetFormatedTextProperty(element, GetDisplayText(element.Text)) #>'); ")]
    [ExtJSPropertyDescription("ValueMember", PropertyInitializeCode = "valueField = <#= element.ValueMember #>", PropertyChangeCode = "this.setValueField('<#= element.ValueMember #>');")]
    [ExtJSPropertyDescription("ExpandOnFocus", PropertyInitializeCode = "expandOnFocus = <#= element.ExpandOnFocus #>", PropertyChangeCode = "this.setExpandOnFocus(<#= JavaScriptCode.GetBool(element.ExpandOnFocus) #>);")]


    [ExtJSEventDescription("SelectedIndexChange", ClientEventName = "select", ClientEventHandlerParameterNames = " field, value, eOpts ", ClientHandlerCallbackData = " rowIndex = field.getStore().indexOf(this.getPicker().ownerGrid.getSelectionModel().getSelection()[0]) ", ServerEventArgsCreateCode = "new GridCellMouseEventArgs(0, this.rowIndex.ToInt32())")]

    [ExtJSEventDescription("TextChanged", ClientEventName = "textChanged", ClientEventHandlerParameterNames = " view, newValue ", ClientHandlerCallbackData = " value = newValue ", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)")]

    [ExtJSMethodDescription("SetSelection", MethodCode = "<#resource:SetSelection.js#>")]

    [ExtJSMethodDescription("ExpandDropDownList", MethodCode = "this.expand()")]
    [ExtJSMethodDescription("SowHeaderMethod", MethodCode = "this.getPicker().setHideHeaders(<#= !element.ShowHeader #>)")]

    [ExtJSEventDescription("Afterrender", ClientEventName = "afterrender", ClientEventHandlerParameterNames = " view, eOpts ", ClientCode = "view.setSelectedRowIndex(<#= element.SelectedIndex #>)", RenderAlways = true)]
    [ExtJSEventDescription("SelectedIndexChange", ClientEventName = "select", ClientEventHandlerParameterNames = " field, value, eOpts ", ClientHandlerCallbackData = " rowIndex = this.getSelectedIndex(field,value) ", ServerEventArgsCreateCode = "new GridCellMouseEventArgs(0, this.rowIndex.ToInt32())")]
    [ExtJSEventDescription("TextChanged", ClientEventName = "textChanged", ClientEventHandlerParameterNames = " view, newValue ", ClientHandlerCallbackData = " value = newValue ", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)")]



    public class ComboGridElementExtJSRenderer : ComboGridElementExtJSRendererBase
    {
        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Set Headers Visibility
            ComboGridElement comboGrid = visualElement as ComboGridElement;
            if (comboGrid != null)
            {
                comboGrid.InvokeMethodOnce("SowHeaderMethod");
            }
        }

        /// <summary>
        /// Renders the refresh.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRefresh(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get the GridElement element
            GridElement gridElement = visualElement as GridElement;

            // If there is a valid listview element
            if (gridElement == null)
            {
                return;
            }

            // Render the reconfigure script
            builder.AppendStartInvocation(string.Concat(componentVariable, ".getPicker().ownerGrid.reconfigure"));

            // Create the json string writer
            Text.StringBuilder stringBuilder = new Text.StringBuilder();

            // Create the json writer
            using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(stringBuilder))
            {

                RenderPlainItemsPropertyValue(renderingContext, gridElement.Columns, jsonWriter, (element => element != null && ((GridColumn)element).Visible && !(element is GridRowHeader)));

                // Flush columns
                jsonWriter.Flush();

                builder.AppendRawValue(stringBuilder.ToString());
            }


            builder.AppendEndInvocation(true);


        }
    }
}
