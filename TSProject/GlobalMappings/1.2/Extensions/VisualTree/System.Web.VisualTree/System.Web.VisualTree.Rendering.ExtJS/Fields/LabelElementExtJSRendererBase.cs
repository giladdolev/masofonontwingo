using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class LabelElementExtJSRendererBase : ContentControlElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.form.Label";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "label";
			}
        }


		/// <summary>
        /// Renders the style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			this.RenderAutoEllipsisProperty(context, visualElement, styleWriter);
			this.RenderImageProperty(context, visualElement, styleWriter);
			this.RenderImageAlignProperty(context, visualElement, styleWriter);
			this.RenderBorderStyleProperty(context, visualElement, styleWriter);
			this.RenderStyleOverflowProperty(context, visualElement, styleWriter);
			this.RenderEnabledProperty(context, visualElement, styleWriter);
			this.RenderTextAlignProperty(context, visualElement, styleWriter);
		}


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "MaxWidth":
					this.RenderMaxWidthPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MaxHeight":
					this.RenderMaxHeightPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AutoEllipsis":
					this.RenderAutoEllipsisPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Content":
					this.RenderContentPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Image":
					this.RenderImagePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ImageAlign":
					this.RenderImageAlignPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BorderStyle":
					this.RenderBorderStylePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Text":
					this.RenderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "StyleOverflow":
					this.RenderStyleOverflowPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Enabled":
					this.RenderEnabledPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "TextAlign":
					this.RenderTextAlignPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderMaxWidthProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMaxHeightProperty(renderingContext, visualElement, jsonWriter);
			this.RenderContentProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the MaxWidth property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaxWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MaxWidth value is not default
					if (element.AutoSize == true)
					{
						jsonWriter.WriteProperty("maxWidth", element.MaxWidth);
					}
				}
			}

		}

		/// <summary>
        /// Renders the MaxWidth property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaxWidthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setMaxWidth({1});", new JavaScriptCode(componentVariable), element.MaxWidth);
			}

		}


		/// <summary>
        /// Renders the MaxHeight property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaxHeightProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MaxHeight value is not default
					if (element.AutoSize == true )
					{
						jsonWriter.WriteProperty("maxHeight", element.MaxHeight );
					}
				}
			}

		}

		/// <summary>
        /// Renders the MaxHeight property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaxHeightPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setMaxHeight ({1});", new JavaScriptCode(componentVariable), element.MaxHeight );
			}

		}


		/// <summary>
        /// Renders the AutoEllipsis property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAutoEllipsisProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("textOverflow",  LabelElementExtJSRenderer.ReviseAutoEllipsisStyle(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("whiteSpace",  LabelElementExtJSRenderer.ReviseWhiteSpaceStyle(element) );
				}
			}

		}

		/// <summary>
        /// Renders the AutoEllipsis property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAutoEllipsisPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle({{textOverflow:'{1}',whiteSpace:'{2}'}});", new JavaScriptCode(componentVariable),  LabelElementExtJSRenderer.ReviseAutoEllipsisStyle(element) ,  LabelElementExtJSRenderer.ReviseWhiteSpaceStyle(element) );
			}

		}


		/// <summary>
        /// Renders the Content property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderContentProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.Content))
					{
						jsonWriter.WriteProperty("html",  Convert.ToString(element.Content) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Content property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderContentPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Image property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderImageProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If Image value is not default
					if (Convert.ToString(element.Image.Source) != string.Empty )
					{
						jsonWriter.WriteProperty("background",  FormatHelper.FormatResourceUrl(renderingContext, element.Image) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Image property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderImagePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('background-image',' {1}');", new JavaScriptCode(componentVariable),  FormatHelper.FormatResourceUrl(context, element.Image) );
			}
			
		}


		/// <summary>
        /// Renders the ImageAlign property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderImageAlignProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("backgroundPosition", LabelElementExtJSRenderer.ReviseImageAlignment(element));
				}
			}

		}

		/// <summary>
        /// Renders the ImageAlign property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderImageAlignPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('background-position','{1}');", new JavaScriptCode(componentVariable), LabelElementExtJSRenderer.ReviseImageAlignment(element));
			}

		}


		/// <summary>
        /// Renders the AutoSize property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAutoSizeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If AutoSize value is not default
					if (element.AutoSize != false)
					{
						jsonWriter.WriteProperty("shrinkWrap", @"{0};",  JavaScriptCode.GetBool( element.AutoSize ));
					}
				}
			}

		}

		/// <summary>
        /// Renders the BorderStyle property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBorderStyleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("border",  ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("borderRight",  ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("borderBottom",  ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) );
				}
			}

		}

		/// <summary>
        /// Renders the BorderStyle property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderBorderStylePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle({{{1}}});", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseBorderStyle(element) );
			}

		}


		/// <summary>
        /// Renders the Text property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.Text))
					{
						jsonWriter.WriteProperty("text",  GetDisplayText(element.Text) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Text property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setText('{1}');", new JavaScriptCode(componentVariable),  JavaScriptUtilities.ParseString(element.Text) );
			}

		}


		/// <summary>
        /// Renders the StyleOverflow property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderStyleOverflowProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If StyleOverflow value is not default
					if (!element.AutoSize)
					{
						jsonWriter.WritePropertyWithRawValue("overflowX", @"'hidden'");
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If StyleOverflow value is not default
					if (!element.AutoSize)
					{
						jsonWriter.WritePropertyWithRawValue("overflowY", @"'hidden'");
					}
				}
			}

		}

		/// <summary>
        /// Renders the StyleOverflow property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderStyleOverflowPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Enabled property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnabledProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If Enabled value is not default
					if (!element.Enabled)
					{
						jsonWriter.WriteProperty("backgroundColor",  LabelElementExtJSRenderer.ReviseDisabledBackGroundColor(element) );
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If Enabled value is not default
					if (!element.Enabled)
					{
						jsonWriter.WriteProperty("color",  LabelElementExtJSRenderer.ReviseDisabledForeColor(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Enabled property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnabledPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('backgroundColor','{1}'),{0}.setStyle('color','{2}');", new JavaScriptCode(componentVariable),  LabelElementExtJSRenderer.ReviseDisabledBackGroundColor(element) ,  LabelElementExtJSRenderer.ReviseDisabledForeColor(element) );
			}

		}


		/// <summary>
        /// Renders the TextAlign property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTextAlignProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If TextAlign value is not default
					if (element.TextAlign != Drawing.ContentAlignment.TopLeft)
					{
						jsonWriter.WriteProperty("textAlign",  LabelElementExtJSRenderer.ReviseTextAlignment(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the TextAlign property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTextAlignPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.LabelElement element = visualElement as System.Web.VisualTree.Elements.LabelElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('textAlign','{1}');", new JavaScriptCode(componentVariable),  LabelElementExtJSRenderer.ReviseTextAlignment(element) );
			}

		}


	}
}