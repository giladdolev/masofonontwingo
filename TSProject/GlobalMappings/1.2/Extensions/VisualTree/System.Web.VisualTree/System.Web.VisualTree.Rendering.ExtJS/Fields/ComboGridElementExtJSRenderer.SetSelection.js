var store = this && this.getPicker().getStore && this.getPicker().getStore(),
    selectedIndex = <#= element.SelectedIndex #>;

if(store && (store.isLoading() || store.getCount() <= selectedIndex || store.loadTimer)){
    
    store.on('load', function(){
        if(store.getCount() > selectedIndex){
            this.getPicker().getSelectionModel().select(selectedIndex);
            this.getPicker().getView().focusRow(selectedIndex);
        }        

    }, this,  {single: true})
} else {
    this.getPicker().getSelectionModel().select(selectedIndex);
    this.getPicker().getView().focusRow(selectedIndex);
}