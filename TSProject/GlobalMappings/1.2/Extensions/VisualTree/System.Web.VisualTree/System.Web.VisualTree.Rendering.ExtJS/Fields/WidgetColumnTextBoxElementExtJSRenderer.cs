﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(WidgetColumnTextBoxElement), "TextBoxElementExtJSRenderer", "", "")]
    [ExtJSEventDescription("WidgetControlEditChanged", ClientEventName = "change", ClientEventHandlerParameterNames = "view,  newValue,oldValue, eOpts", ClientCode = "<# resource: WidgetControlFireEditChangedEvent.js #>")]
    [ExtJSEventDescription("WidgetControlItemChanged", ClientEventName = "blur", ClientEventHandlerParameterNames = "view,e,o", ClientCode = "<# resource: WidgetControlFireBlurEvent.js #>")]
    [ExtJSEventDescription("WidgetControlFocusEnter", ClientEventName = "focus", ClientEventHandlerParameterNames = "view, event, eOpts ", ClientCode = "<# resource: WidgetControlFireFocusEnterEvent.js #>")]
    [ExtJSEventDescription("KeyDown", ClientEventName = "keydown", ClientEventHandlerParameterNames = "view, e, eOpts ", ClientCode = "<# resource: WidgetControlFireItemChangedWithEnter.js #>")]
    public class WidgetColumnTextBoxElementExtJSRenderer : WidgetColumnTextBoxElementExtJSRendererBase
    {

        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
            jsonWriter.WriteProperty("enableKeyEvents", "true");
        }
        /// <summary>
        /// Extracts the WidgetControlFocusEnter event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractWidgetControlFocusEnterCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellEventArgs();
        }

        /// <summary>
        /// Indicates if we should render the KeyDown event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return true;
        }

    }
}
