[{
    text: 'Upload',
    handler: function(){
        var form = this.up('form').getForm();
	
        if(form.isValid())
        {
            form.submit({
                url: <#=string.Format(System.Globalization.CultureInfo.InvariantCulture,"'{0}'", GetElementResourceUri(element, "file-upload"))#>,
                waitMsg: 'Uploading file...',
                success: function(fp, o) {VT_callbackApplyItems(o.result.results);},
                failure: function(fp, o) {Ext.Msg.alert('Failed', o.error);}
            });
        }
    }
}, {
    text: 'Reset',
    handler: function(){
        this.up('form').getForm().reset();
    }
}]