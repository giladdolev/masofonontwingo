﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(System.Web.VisualTree.MonthCalendarElement), "ControlElementExtJSRenderer", "datepicker", "Ext.DatePicker")] 
    [ExtJSEventDescription("DateChanged", ClientEventName = "select", 
                                            ClientEventHandlerParameterNames = "x,y", 
                                                ClientHandlerCallbackData = "value = this.getValue()", 
                                                    ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value.ToString())")]
    public class MonthCalendarElementExtJSRenderer : MonthCalendarElementExtJSRendererBase
    {       

    } 
}
