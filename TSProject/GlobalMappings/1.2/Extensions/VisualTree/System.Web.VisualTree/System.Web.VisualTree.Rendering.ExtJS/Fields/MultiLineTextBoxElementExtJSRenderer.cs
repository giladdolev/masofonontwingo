﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TextBoxElement), "TextBoxElementExtJSRenderer", "textareafield", "Ext.form.field.TextArea", IsAlternative = true)]
    [ExtJSPropertyDescription("Grow", PropertyInitializeCode = "grow = true")]
    public class MultilineTextBoxElementExtJSRenderer : MultilineTextBoxElementExtJSRendererBase
    {

    }
}
