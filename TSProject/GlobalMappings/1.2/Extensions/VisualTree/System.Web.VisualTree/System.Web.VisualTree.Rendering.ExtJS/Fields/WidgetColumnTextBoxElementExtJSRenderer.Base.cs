using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public abstract class WidgetColumnTextBoxElementExtJSRendererBase : TextBoxElementExtJSRenderer
	{
		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "BackColor":
					this.RenderBackColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderBackColorProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderKeyDownEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseKeyDownEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "e", "eOpts"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"try {{
    if (e.keyCode == 13) {{
        view.blur();
    }}
}} catch (err) {{
    console.log(""error :- widgetControlItemChanged : "" + err.message);
}}");
										javaScriptBuilder.AppendSemicolon();

										// Create the client callback code.KeyDown
										if (!String.IsNullOrEmpty(element.ClientKeyDown))
										{
											javaScriptBuilder.AppendRawValue(element.ClientKeyDown);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.KeyDown
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "KeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("keydown", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of KeyDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseKeyDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientKeyDown) || element.HasKeyDownListeners);
		}


		/// <summary>
        /// Extracts the KeyDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractKeyDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the KeyDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderKeyDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the KeyDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderKeyDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderKeyDownEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the WidgetControlEditChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlEditChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderWidgetControlEditChangedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseWidgetControlEditChangedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "newValue", "oldValue", "eOpts"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"try {{ if (cmp.ownerGrid) {{ cmp.ownerGrid.fireEvent('widgetControlEditChanged', view.ownerGrid, newValue, newValue, this.dataIndex) }} }} catch (err) {{ console.log(""error :- widgetControlEditChanged : "" + err.message) }}");
										javaScriptBuilder.AppendSemicolon();

										// Create the server callback code.WidgetControlEditChanged
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlEditChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("change", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlEditChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlEditChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WidgetControlEditChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlEditChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlEditChangedListeners);
		}


		/// <summary>
        /// Extracts the WidgetControlEditChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractWidgetControlEditChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WidgetControlEditChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlEditChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlEditChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlEditChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlEditChangedEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the WidgetControlItemChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlItemChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderWidgetControlItemChangedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseWidgetControlItemChangedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "e", "o"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"try {{
    if (cmp.ownerGrid){{
        cmp.ownerGrid.fireEvent('widgetControlItemChanged', cmp.ownerGrid, view.getValue(), view.getValue(), this.dataIndex);
    }}
}} catch (err) {{
    console.log(""error :- widgetControlItemChanged : "" + err.message);
}}");
										javaScriptBuilder.AppendSemicolon();

										// Create the server callback code.WidgetControlItemChanged
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlItemChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("blur", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlItemChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlItemChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WidgetControlItemChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlItemChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlItemChangedListeners);
		}


		/// <summary>
        /// Extracts the WidgetControlItemChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractWidgetControlItemChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WidgetControlItemChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlItemChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlItemChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlItemChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlItemChangedEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the WidgetControlFocusEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlFocusEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderWidgetControlFocusEnterEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseWidgetControlFocusEnterEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "event", "eOpts"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"try {{
    cmp.ownerGrid.fireEvent('widgetControlFocusEnter', cmp, view.getValue(), view.getValue(), this.dataIndex);
}} catch (err) {{
    console.log(""error :- widgetControlFocusEnter : "" + err.message);
}}");
										javaScriptBuilder.AppendSemicolon();

										// Create the server callback code.WidgetControlFocusEnter
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlFocusEnter", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("focus", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of WidgetControlFocusEnter event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlFocusEnterEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WidgetControlFocusEnter event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlFocusEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasWidgetControlFocusEnterListeners);
		}


		/// <summary>
        /// Extracts the WidgetControlFocusEnter event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractWidgetControlFocusEnterCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the WidgetControlFocusEnter event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlFocusEnterAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the WidgetControlFocusEnter event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlFocusEnterRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.WidgetColumnTextBoxElement element = visualElement as System.Web.VisualTree.Elements.WidgetColumnTextBoxElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlFocusEnterEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the BackColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBackColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the BackColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBackColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


	}
}