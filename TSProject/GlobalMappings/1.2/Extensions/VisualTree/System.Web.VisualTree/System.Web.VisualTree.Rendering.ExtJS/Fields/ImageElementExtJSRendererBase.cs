using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class ImageElementExtJSRendererBase : ControlElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.Img";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "image";
			}
        }


		/// <summary>
        /// Renders the style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.ImageElement element = visualElement as System.Web.VisualTree.Elements.ImageElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			this.RenderBorderStyleProperty(context, visualElement, styleWriter);
		}


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.ImageElement element = visualElement as System.Web.VisualTree.Elements.ImageElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "Image":
					this.RenderImagePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BorderStyle":
					this.RenderBorderStylePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderImageProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the Image property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderImageProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ImageElement element = visualElement as System.Web.VisualTree.Elements.ImageElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("src",  FormatHelper.FormatResource(renderingContext, element.Image) );
				}
			}

		}

		/// <summary>
        /// Renders the Image property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderImagePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ImageElement element = visualElement as System.Web.VisualTree.Elements.ImageElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setSrc('{1}');", new JavaScriptCode(componentVariable),  FormatHelper.FormatResource(context, element.Image) );
			}

		}


		/// <summary>
        /// Renders the BorderStyle property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBorderStyleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ImageElement element = visualElement as System.Web.VisualTree.Elements.ImageElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("border",  ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("borderRight",  ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("borderBottom",  ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) );
				}
			}

		}

		/// <summary>
        /// Renders the BorderStyle property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderBorderStylePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ImageElement element = visualElement as System.Web.VisualTree.Elements.ImageElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle({{{1}}});", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseBorderStyle(element) );
			}

		}


	}
}