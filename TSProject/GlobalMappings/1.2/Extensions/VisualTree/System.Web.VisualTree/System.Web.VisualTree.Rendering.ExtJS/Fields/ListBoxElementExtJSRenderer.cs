﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ListBoxElement), "ListControlElementExtJSRenderer", "multiselect", "Ext.ux.form.MultiSelect")]
    [ExtJSPropertyDescription("DataSource")]

    public class ListBoxElementExtJSRenderer : ListBoxElementExtJSRendererBase
    {
        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement,JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Render data source
            this.RenderDataSourceProperty(renderingContext, visualElement, jsonWriter);
        }

    }
}
