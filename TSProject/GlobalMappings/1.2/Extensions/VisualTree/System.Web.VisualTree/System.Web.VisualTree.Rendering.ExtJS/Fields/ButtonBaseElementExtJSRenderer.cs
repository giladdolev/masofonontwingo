﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.Drawing;
using System.Globalization;
using System.Web.VisualTree.Elements;
using System.Diagnostics;
using System.IO;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ButtonBaseElement), "ControlElementExtJSRenderer", "button", "Ext.button.Button")]
    [ExtJSPropertyDescription("Image", PropertyInitializeCode = "icon = <#= FormatHelper.FormatResource(renderingContext, element.Image) #>", PropertyChangeCode = "this.setIcon('<#= FormatHelper.FormatResource(context, element.Image) #>');")]
    [ExtJSPropertyDescription("TextImageRelation", PropertyInitializeCode = "iconAlign = <#= ButtonBaseElementExtJSRenderer.ReviseTextImageRelation(element) #>", ClientNotDefaultCode = "element.TextImageRelation != TextImageRelation.Overlay", PropertyChangeCode = "this.setIconAlign('<#= ButtonBaseElementExtJSRenderer.ReviseTextImageRelation(element) #>');")]

    

    public abstract class ButtonBaseElementExtJSRenderer : ButtonBaseElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Render button icon
            ButtonBaseElement buttonBaseElement = visualElement as ButtonBaseElement;
            if (buttonBaseElement != null && buttonBaseElement.Image != null && !ShouldRenderTextWithImage(buttonBaseElement))
            {
                // Render button image
                jsonWriter.WriteProperty("icon", buttonBaseElement.Image);
            }
        }

        /// <summary>
        /// Renders the text property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get button element
            ButtonBaseElement buttonBaseElement = visualElement as ButtonBaseElement;

            // If there is a valid button element
            if (buttonBaseElement != null)
            {
                // Get the button text
                string text = GetDisplayText(buttonBaseElement.Text);

                // If there is a valid text or image
                if (!string.IsNullOrEmpty(text) || buttonBaseElement.Image != null)
                {
                    text = HttpUtility.HtmlEncode(text);
                    string style = GetTextStyle(buttonBaseElement);

                    // Render text with image when image size is more than 16x16 or image align is not middle left
                    if (ShouldRenderTextWithImage(buttonBaseElement))
                    {
                        string imageSource = buttonBaseElement.Image.Source;

                        if (renderingContext != null && renderingContext.IsDesignTime)
                        {
                            string path = String.Format("{0}{1}", renderingContext.RootSite, imageSource.Replace("/", "\\"));
                            imageSource = String.Format("data:image/png;base64,{0}", Convert.ToBase64String(File.ReadAllBytes(path)));
                        }

                        // Check image alignment
                        switch (buttonBaseElement.ImageAlign)
                        {
                            // Render image above text
                            case ContentAlignment.TopCenter:
                                RenderProperty(renderingContext, jsonWriter, "text", string.Format(CultureInfo.InvariantCulture, @"<image src=""{0}""></image><span style=""float:left; clear:left; width:100%; {1}"">{2}</span>",
                                    imageSource,
                                    style,
                                    text
                                    ));
                                break;

                            // Render text above image
                            case ContentAlignment.BottomCenter:
                                RenderProperty(renderingContext, jsonWriter, "text", string.Format(CultureInfo.InvariantCulture, @"<span style=""float:left; clear:left; {2}"">{1}</span><image src=""{0}""  style=""float:left; clear:left;""></image>",
                                    imageSource,
                                    text,
                                    style));
                                break;

                            // Render image before text
                            case ContentAlignment.MiddleLeft:
                                RenderProperty(renderingContext, jsonWriter, "text", string.Format(CultureInfo.InvariantCulture, @"<image src=""{0}""></image><span style=""{2}"">{1}</span>",
                                    imageSource,
                                    text,
                                    style));
                                break;

                            // Render text before image
                            case ContentAlignment.MiddleRight:
                                style = string.Format("{0}{1}", style,@"display: inline-block;vertical-align: top;text-align: center;line-height: 1em;");
                                RenderProperty(renderingContext, jsonWriter, "text", string.Format(CultureInfo.InvariantCulture, @"<span style=""{2}"">{1}</span>&nbsp;&nbsp;<image src=""{0}""></image>",
                                    imageSource,
                                    text,
                                   style));
                                break;

                            // Overlay
                            case ContentAlignment.MiddleCenter:
                                // TODO: render overlay text and image
                                RenderProperty(renderingContext, jsonWriter, "text", string.Format(CultureInfo.InvariantCulture, @"<span style=""{0}"">{1}</span>", style, text));

                                break;
                        }
                    }
                    else
                    {
                        RenderProperty(renderingContext, jsonWriter, "text", string.Format(CultureInfo.InvariantCulture, @"<span style=""{0}"">{1}</span>", style, text));
                    }
                }
            }
        }

        /// <summary>
        /// Gets the text style.
        /// </summary>
        /// <param name="buttonElement">The button element.</param>
        /// <returns></returns>
        private static string GetTextStyle(ButtonBaseElement buttonElement)
        {
            string style = string.Empty;

            if (buttonElement != null)
            {
                // Render text wrap
                style = "white-space:normal; ";

                // Get the control fore color
                Color foreColor = buttonElement.ForeColor;

                // If there is a valid color
                if (foreColor != Color.Empty)
                {
                    // Write text color                    
                    style += string.Format(CultureInfo.InvariantCulture, "color:{0};", ColorTranslator.ToHtml(foreColor));
                }
            }

            return style;
        }

        /// <summary>
        /// Should render custom text.
        /// </summary>
        /// <param name="buttonBaseElement">The button base element.</param>
        /// <returns></returns>
        private static bool ShouldRenderTextWithImage(ButtonBaseElement buttonBaseElement)
        {
            // If there is a valid button element
            if (buttonBaseElement != null && buttonBaseElement.Image != null)
            {
                // Get the image size
                Size imageSize = ResourceReference.GetImageSize(buttonBaseElement.Image);

                // Check if we should render text with image
                return imageSize.Width > 16 || imageSize.Height > 16 || buttonBaseElement.ImageAlign != ContentAlignment.MiddleLeft;
            }

            return false;
        }


        /// <summary>
        /// Gets the display text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        protected override string GetDisplayText(string text)
        {
            return GetDisplayTextWithoutCommands(text);
        }

        /// <summary>
        /// Renders the text property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <exception cref="System.ArgumentNullException">builder</exception>
        protected override void RenderTextPropertyChange(ExtJSRenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            if (builder == null)
            {
                throw new ArgumentNullException("builder");
            }

            // Get control element
            ButtonBaseElement controlElement = visualElement as ButtonBaseElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // Call cmp.setValue(controlElement.Text)...
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setText"), controlElement.Text);
            }
        }



        /// <summary>
        /// Revises the textImageRelation.
        /// </summary>    
        /// <param name="buttonBaseElement"the control.</param>
        /// <returns></returns>
        internal static string ReviseTextImageRelation(ButtonBaseElement buttonBaseElement)
        {
            switch (buttonBaseElement.TextImageRelation)
            {
                case TextImageRelation.ImageAboveText:
                    return "top";
                case TextImageRelation.ImageBeforeText:
                    return "left";
                case TextImageRelation.TextAboveImage:
                    return "bottom";
                case TextImageRelation.TextBeforeImage:
                    return "right";
            }
            return "center";
        }

    }
}
