﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(NumericUpDownElement), "ControlContainerElementExtJSRenderer", "numberfield", "Ext.form.field.Number")]
    [ExtJSEventDescription("LostFocus", ClientEventName = "focusleave", ClientHandlerCallbackData = "value = this.getValue()",
                                                            ServerEventArgsCreateCode = "new ValueChangedArgs<int>(this.value.ToInt32())")]
    [ExtJSPropertyDescription("Maximum", PropertyInitializeCode = "maxValue =  <#= element.Maximum #> ", ClientNotDefaultCode = "element.Maximum != Decimal.MaxValue", PropertyChangeCode = "this.setMaxValue(<#= element.Maximum #>);")]
    [ExtJSPropertyDescription("Minimum", PropertyInitializeCode = "minValue = <#= element.Minimum #>", ClientNotDefaultCode = "element.Minimum != Decimal.MinValue", PropertyChangeCode = "this.setMinValue(<#= element.Minimum #>);")]
    [ExtJSPropertyDescription("Value", PropertyInitializeCode = "value = <#= element.Value #>", ClientDefault = 0, PropertyChangeCode = "this.setValue(<#= element.Value #>);")]
    [ExtJSEventDescription("ValueChanged", ClientEventName = "spin", ClientEventHandlerParameterNames = "view,direction,eOpts", ClientHandlerCallbackData = "directionVal = direction.toString()", ServerEventArgsCreateCode = "new NumericUpDownEventArgs(this.directionVal)")]

    public class NumericUpDownElementExtJSRenderer : NumericUpDownElementExtJSRendererBase
    {

    }
}
