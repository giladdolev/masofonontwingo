﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Linq;
using System.Collections.Generic;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ListControlElement), "InputElementExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("DisplayMember", PropertyInitializeCode = "displayField = <#= \"Text\" #>")]
    [ExtJSPropertyDescription("ValueMember", PropertyInitializeCode = "valueField = <#= \"Value\" #>")]
    [ExtJSEventDescription("SelectedValueChanged", ClientEventName = "change", ClientEventHandlerParameterNames = " view, newValue, oldValue, eOpts", ClientHandlerCallbackData = " value = (newValue) ? newValue:'' ", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)",RenderAlways=true)]

    public abstract class ListControlElementExtJSRenderer : ListControlElementExtJSRendererBase
    {
       

        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Render data source
            RenderDataSourceProperty(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            if (property == "$refresh")
            {
                builder.AppendStartInvocation(string.Concat(componentVariable, ".store.reload"));
                builder.AppendEndInvocation(true);
            }
            else
            {
                base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
            }
        }
    }
}
