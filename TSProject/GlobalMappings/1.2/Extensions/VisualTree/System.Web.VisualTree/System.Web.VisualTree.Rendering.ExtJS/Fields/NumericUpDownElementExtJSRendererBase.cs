using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class NumericUpDownElementExtJSRendererBase : ControlContainerElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.form.field.Number";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "numberfield";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the ValueChanged event attach code.
			RenderAttachValueChangedEvent(context, visualElement, writer); 
			// Renders the LostFocus event attach code.
			RenderAttachLostFocusEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "ValueChanged":
					return this.ExtractValueChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "LostFocus":
					return this.ExtractLostFocusCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "Maximum":
					this.RenderMaximumPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Minimum":
					this.RenderMinimumPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Value":
					this.RenderValuePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderMaximumProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMinimumProperty(renderingContext, visualElement, jsonWriter);
			this.RenderValueProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the ValueChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachValueChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderValueChangedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseValueChangedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "direction", "eOpts"}, 
									() => {  
										// Create the server callback code.ValueChanged
										if (this.ShouldRenderValueChangedEvent(context, element)) 
										{
											javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "ValueChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{directionVal : direction.toString()}}"));
										}
 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("spin", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of ValueChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseValueChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.NumericUpDownElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ValueChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderValueChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.NumericUpDownElement element)
		{
			return (element.HasValueChangedListeners);
		}


		/// <summary>
        /// Extracts the ValueChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractValueChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new NumericUpDownEventArgs(args.Value<System.String>("directionVal"));
		}


		/// <summary>
        /// Renders the ValueChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderValueChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderValueChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseValueChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "direction", "eOpts"}, 
                            () => {  
										// Create the server callback code.ValueChanged
										if (this.ShouldRenderValueChangedEvent(context, element)) 
										{
											jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "ValueChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{directionVal : direction.toString()}}"));
										}
 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('spin', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the ValueChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderValueChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderValueChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "spin");
            }
        }

		/// <summary>
        /// Renders the LostFocus event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachLostFocusEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderLostFocusEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseLostFocusEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the client callback code.LostFocus
										if (!String.IsNullOrEmpty(element.ClientLostFocus))
										{
											javaScriptBuilder.AppendRawValue(element.ClientLostFocus);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.LostFocus
										if (this.ShouldRenderLostFocusEvent(context, element)) 
										{
											javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "LostFocus", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{value : this.getValue()}}"));
										}
 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("focusleave", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of LostFocus event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseLostFocusEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the LostFocus event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderLostFocusEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientLostFocus) || element.HasLostFocusListeners);
		}


		/// <summary>
        /// Extracts the LostFocus event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractLostFocusCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new ValueChangedArgs<int>(args.Value<System.Int32>("value"));
		}


		/// <summary>
        /// Renders the LostFocus event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderLostFocusAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderLostFocusEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseLostFocusEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the client callback code.LostFocus
										if (!String.IsNullOrEmpty(element.ClientLostFocus))
										{
											javaScriptBuilder.AppendRawValue(element.ClientLostFocus);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.LostFocus
										if (this.ShouldRenderLostFocusEvent(context, element)) 
										{
											jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "LostFocus", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{value : this.getValue()}}"));
										}
 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('focusleave', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the LostFocus event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderLostFocusRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderLostFocusEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "focusleave");
            }
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "ValueChanged":
					this.RenderValueChangedAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "LostFocus":
					this.RenderLostFocusAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "ValueChanged":
					this.RenderValueChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "LostFocus":
					this.RenderLostFocusRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the Maximum property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaximumProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Maximum value is not default
					if (element.Maximum != Decimal.MaxValue)
					{
						jsonWriter.WriteProperty("maxValue",  element.Maximum );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Maximum property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaximumPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setMaxValue({1});", new JavaScriptCode(componentVariable),  element.Maximum );
			}

		}


		/// <summary>
        /// Renders the Minimum property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMinimumProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Minimum value is not default
					if (element.Minimum != Decimal.MinValue)
					{
						jsonWriter.WriteProperty("minValue",  element.Minimum );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Minimum property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMinimumPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setMinValue({1});", new JavaScriptCode(componentVariable),  element.Minimum );
			}

		}


		/// <summary>
        /// Renders the Value property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderValueProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Value value is not default
					if (element.Value != 0)
					{
						jsonWriter.WriteProperty("value",  element.Value );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Value property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderValuePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.NumericUpDownElement element = visualElement as System.Web.VisualTree.Elements.NumericUpDownElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setValue({1});", new JavaScriptCode(componentVariable),  element.Value );
			}

		}


	}
}