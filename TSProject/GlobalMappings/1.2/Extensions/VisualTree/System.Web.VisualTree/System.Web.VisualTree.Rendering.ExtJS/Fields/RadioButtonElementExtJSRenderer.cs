﻿using System.Drawing;
using Newtonsoft.Json;
using System.Web.VisualTree.Elements;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(RadioButtonElement), "ButtonBaseElementExtJSRenderer", "radiofield", "Ext.form.field.Radio")]
    [ExtJSPropertyDescription("ButtonName", PropertyInitializeCode = "name = <#= RadioButtonElementExtJSRenderer.ReviseName(element) #>")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "boxLabel=<#= GetDisplayText(element.Text)#>", ClientNotDefaultCode = "!string.IsNullOrEmpty(element.Text)", PropertyChangeCode = "this.setBoxLabel('<#= GetDisplayText(element.Text) #>');")]
    [ExtJSPropertyDescription("IsChecked", PropertyInitializeCode = "checked = <#=element.IsChecked#>", PropertyChangeCode = "this.setValue(<#= Convert.ToString(element.IsChecked).ToLower() #>);")]
    [ExtJSEventDescription("CheckedChanged",ClientEventName = "change", ClientHandlerCallbackData = "value = this.getValue()", ServerEventArgsCreateCode = "new ValueChangedArgs<bool>(Convert.ToBoolean(this.value))")]
    [ExtJSPropertyDescription("CheckAlign", PropertyInitializeCode = "boxLabelAlign = <#= RadioButtonElementExtJSRenderer.ReviseLabelAlignment(element.CheckAlign) #>")]
    public class RadioButtonElementExtJSRenderer : RadioButtonElementExtJSRendererBase
    {
        /// <summary>
        /// Revises the name of the RadioButton element being rendered.
        /// </summary>
        /// <param name="radioButton">The radio button.</param>
        /// <returns></returns>
        internal static string ReviseName(RadioButtonElement radioButton)
        {
            VisualElement parent = radioButton.ParentElement;

            if (parent != null && parent.IsContainer)
            {
                return parent.ClientID;
            }

            return null;
        }


        internal static string ReviseBackColor(RadioButtonElement radioButton)
        {
            if (radioButton != null && radioButton.BackColor != Color.Transparent)
            {
                ControlElement parentElement = radioButton.ParentElement as ControlElement;

                // Render radio back color when it is different than parent back color
                while (parentElement != null
                    && (parentElement.BackColor == radioButton.BackColor
                    || parentElement.BackColor == Color.Transparent))
                {
                    parentElement = parentElement.ParentElement as ControlElement;
                }

                if (parentElement != null)
                {
                    return ColorTranslator.ToHtml(radioButton.BackColor);
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Indicates if we should render the CheckedChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderCheckedChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.RadioButtonElement element)
        {
            // Check if the event should be registered
            RadioGroupElement radioGroup = element.Parent as RadioGroupElement;
            if (radioGroup != null && radioGroup.HasChangeListeners)
            {
                return true;
            }
           return base.ShouldRenderCheckedChangedEvent(context, element);
           
        }

        /// <summary>
        /// Revises the label alignment.
        /// </summary>
        /// <param name="alignment">The alignment.</param>
        /// <returns>return sencha alignment equivalent</returns>
        public static string ReviseLabelAlignment(ContentAlignment alignment)
        {
            string labelAlign = "after";
                switch (alignment)
                {
                    
                    case ContentAlignment.MiddleRight:
                    case ContentAlignment.TopRight:
                    case ContentAlignment.BottomRight:
                        labelAlign = "before";
                        break;
                    case ContentAlignment.MiddleLeft:
                    case ContentAlignment.BottomLeft:
                    case ContentAlignment.TopLeft:
                        labelAlign = "after";
                        break;
            }
            return labelAlign;
        }
    }
}