using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using System.Text;


namespace System.Web.VisualTree.Rendering.ExtJS
{

    public class ComboGridElementExtJSRendererBase : GridElementExtJSRenderer
    {
        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "VT.ux.ComboGrid";
            }
        }


        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "Grid-Combo";
            }
        }


        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
            // Call the base implementation
            base.RenderEvents(context, visualElement, writer);
            // Renders the SelectedIndexChange event attach code.
            RenderAttachSelectedIndexChangeEvent(context, visualElement, writer);
            // Renders the SelectedIndexChanged event attach code.
            RenderAttachSelectedIndexChangedEvent(context, visualElement, writer);
            // Renders the TextChanged event attach code.
            RenderAttachTextChangedEvent(context, visualElement, writer);
            // Renders the Afterrender event attach code.
            RenderAttachAfterrenderEvent(context, visualElement, writer);
        }


        /// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
            // Choose event name
            switch (serverEventName)
            {
                case "SelectedIndexChange":
                    return this.ExtractSelectedIndexChangeCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "SelectedIndexChanged":
                    return this.ExtractSelectedIndexChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "TextChanged":
                    return this.ExtractTextChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "Afterrender":
                    return this.ExtractAfterrenderCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
                    // Call the base implementation
                    return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }
        }


        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            // Choose property
            switch (property)
            {
                case "DisplayMember":
                    this.RenderDisplayMemberPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "ValueMember":
                    this.RenderValueMemberPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "ExpandOnFocus":
                    this.RenderExpandOnFocusPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Text":
                    this.RenderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            this.RenderDisplayMemberProperty(renderingContext, visualElement, jsonWriter);
            this.RenderValueMemberProperty(renderingContext, visualElement, jsonWriter);
            this.RenderExpandOnFocusProperty(renderingContext, visualElement, jsonWriter);
        }


        /// <summary>
        /// Renders the SelectedIndexChange event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachSelectedIndexChangeEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderSelectedIndexChangeEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseSelectedIndexChangeEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "field", "value", "eOpts" },
                                    () => {
                                        // Create the server callback code.SelectedIndexChange

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "SelectedIndexChange", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{ rowIndex : this.getSelectedIndex(field,value) }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("select", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of SelectedIndexChange event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseSelectedIndexChangeEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ComboGridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the SelectedIndexChange event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderSelectedIndexChangeEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboGridElement element)
        {
            return true;
        }


        /// <summary>
        /// Extracts the SelectedIndexChange event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractSelectedIndexChangeCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellMouseEventArgs(0, args.Value<System.Int32>("rowIndex"));
        }


        /// <summary>
        /// Renders the SelectedIndexChange event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectedIndexChangeAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderSelectedIndexChangeEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseSelectedIndexChangeEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "field", "value", "eOpts" },
                            () => {
                                // Create the server callback code.SelectedIndexChange

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "SelectedIndexChange", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{ rowIndex : this.getSelectedIndex(field,value) }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('select', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the SelectedIndexChange event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectedIndexChangeRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderSelectedIndexChangeEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "select");
            }
        }

        /// <summary>
        /// Renders the SelectedIndexChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
        }


        /// <summary>
        /// Revise the event behavior type of SelectedIndexChanged event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseSelectedIndexChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ComboGridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the SelectedIndexChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboGridElement element)
        {
            return (element.HasSelectedIndexChangedListeners);
        }


        /// <summary>
        /// Extracts the SelectedIndexChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractSelectedIndexChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the SelectedIndexChanged event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectedIndexChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the SelectedIndexChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectedIndexChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

        /// <summary>
        /// Renders the TextChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachTextChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderTextChangedEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseTextChangedEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "newValue" },
                                    () => {

                                        // Create the server callback code.TextChanged

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "TextChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{ value : newValue }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("textChanged", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of TextChanged event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseTextChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the TextChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderTextChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (element.HasTextChangedListeners);
        }


        /// <summary>
        /// Extracts the TextChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractTextChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new ValueChangedArgs<string>(args.Value<System.String>("value"));
        }


        /// <summary>
        /// Renders the TextChanged event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderTextChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseTextChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "newValue" },
                            () => {
                                // Create the server callback code.TextChanged

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "TextChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{ value : newValue }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('textChanged', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the TextChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderTextChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "textChanged");
            }
        }

        /// <summary>
        /// Renders the Afterrender event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderAfterrenderEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseAfterrenderEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "eOpts" },
                                    () => {
                                        javaScriptBuilder.AppendFormattedRaw(@"view.setSelectedRowIndex({0})", element.SelectedIndex);
                                        javaScriptBuilder.AppendSemicolon();

                                        // Create the server callback code.Afterrender

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Afterrender", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("afterrender", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of Afterrender event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAfterrenderEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the Afterrender event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return true;
        }


        /// <summary>
        /// Extracts the Afterrender event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractAfterrenderCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the Afterrender event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Afterrender event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderAfterrenderEvent(context, element))
            {

            }
        }

        /// <summary>
        /// Renders the  Add Listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Choose event
            switch (eventName)
            {
                case "SelectedIndexChange":
                    this.RenderSelectedIndexChangeAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "TextChanged":
                    this.RenderTextChangedAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

        /// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Choose event
            switch (eventName)
            {
                case "SelectedIndexChange":
                    this.RenderSelectedIndexChangeRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "TextChanged":
                    this.RenderTextChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
        /// <summary>
        /// Renders the DisplayMember property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDisplayMemberProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If property is valid
                    if (ValidateProperty(element.DisplayMember))
                    {
                        jsonWriter.WriteProperty("displayField", element.DisplayMember);
                    }
                }
            }

        }

        /// <summary>
        /// Renders the DisplayMember property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDisplayMemberPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setDisplayField('{1}');", new JavaScriptCode(componentVariable), element.DisplayMember);
            }

        }


        /// <summary>
        /// Renders the ValueMember property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderValueMemberProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If property is valid
                    if (ValidateProperty(element.ValueMember))
                    {
                        jsonWriter.WriteProperty("valueField", element.ValueMember);
                    }
                }
            }

        }

        /// <summary>
        /// Renders the ValueMember property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderValueMemberPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setValueField('{1}');", new JavaScriptCode(componentVariable), element.ValueMember);
            }

        }


        /// <summary>
        /// Renders the ExpandOnFocus property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderExpandOnFocusProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    jsonWriter.WriteProperty("expandOnFocus", element.ExpandOnFocus);
                }
            }

        }

        /// <summary>
        /// Renders the ExpandOnFocus property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderExpandOnFocusPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setExpandOnFocus({1});", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(element.ExpandOnFocus));
            }

        }


        /// <summary>
        /// Renders the Text property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If property is valid
                    if (ValidateProperty(element.Text))
                    {
                        jsonWriter.WriteProperty("value", GetFormatedTextProperty(element, GetDisplayText(element.Text)));
                    }
                }
            }

        }

        /// <summary>
        /// Renders the Text property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setValue('{1}'); ", new JavaScriptCode(componentVariable), GetFormatedTextProperty(element, GetDisplayText(element.Text)));
            }

        }


        /// <summary>
        /// Extract the method callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
            // Choose method
            switch (methodName)
            {
                case "SetSelection":
                    return this.ExtractSetSelectionMethodCallbackData(context, eventObject);
                case "ExpandDropDownList":
                    return this.ExtractExpandDropDownListMethodCallbackData(context, eventObject);
                case "SowHeaderMethod":
                    return this.ExtractSowHeaderMethodMethodCallbackData(context, eventObject);
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Choose method
            switch (methodName)
            {
                case "SetSelection":
                    this.RenderSetSelectionMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                case "ExpandDropDownList":
                    this.RenderExpandDropDownListMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                case "SowHeaderMethod":
                    this.RenderSowHeaderMethodMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the SetSelection method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetSelectionMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Object data = (System.Object)methodData;
                    builder.AppendFormattedRawValue(@"var store = {0} && {0}.getPicker().getStore && {0}.getPicker().getStore(),
    selectedIndex = {1};

if(store && (store.isLoading() || store.getCount() <= selectedIndex || store.loadTimer)){{
    
    store.on('load', function(){{
        if(store.getCount() > selectedIndex){{
            {0}.getPicker().getSelectionModel().select(selectedIndex);
            {0}.getPicker().getView().focusRow(selectedIndex);
        }}        

    }}, {0},  {{single: true}})
}} else {{
    {0}.getPicker().getSelectionModel().select(selectedIndex);
    {0}.getPicker().getView().focusRow(selectedIndex);
}}", new JavaScriptCode(componentVariableName), element.SelectedIndex);
                }
            }

        }


        /// <summary>
        /// Gets the method SetSelection callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetSelectionMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


        /// Renders the ExpandDropDownList method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderExpandDropDownListMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Object data = (System.Object)methodData;
                    builder.AppendFormattedRawValue(@"{0}.expand()", new JavaScriptCode(componentVariableName));
                }
            }

        }


        /// <summary>
        /// Gets the method ExpandDropDownList callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractExpandDropDownListMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


        /// Renders the SowHeaderMethod method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSowHeaderMethodMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboGridElement element = visualElement as System.Web.VisualTree.Elements.ComboGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Object data = (System.Object)methodData;
                    builder.AppendFormattedRawValue(@"{0}.getPicker().setHideHeaders({1})", new JavaScriptCode(componentVariableName), !element.ShowHeader);
                }
            }

        }


        /// <summary>
        /// Gets the method SowHeaderMethod callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSowHeaderMethodMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


    }
}
