using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Text;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class TextBoxBaseElementExtJSRendererBase : InputElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.form.field.Text";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "textfield";
			}
        }


		/// <summary>
        /// Renders the field style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderFieldStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderFieldStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}
			this.RenderFontProperty(context, visualElement, styleWriter);
			this.RenderForeColorProperty(context, visualElement, styleWriter);
			this.RenderBackColorProperty(context, visualElement, styleWriter);
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "MaxLength":
					this.RenderMaxLengthPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ReadOnly":
					this.RenderReadOnlyPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SelectionLength":
					this.RenderSelectionLengthPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SelectOnFocus":
					this.RenderSelectOnFocusPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Font":
					this.RenderFontPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ForeColor":
					this.RenderForeColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BackColor":
					this.RenderBackColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "EnforceMaxLength":
					this.RenderEnforceMaxLengthPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderMaxLengthProperty(renderingContext, visualElement, jsonWriter);
			this.RenderReadOnlyProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSelectionLengthProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSelectOnFocusProperty(renderingContext, visualElement, jsonWriter);
			this.RenderEnforceMaxLengthProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the MaxLength property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaxLengthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("maxLength",  element.MaxLength );
				}
			}

		}

		/// <summary>
        /// Renders the MaxLength property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaxLengthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ReadOnly property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderReadOnlyProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("readOnly",  element.ReadOnly );
				}
			}

		}

		/// <summary>
        /// Renders the ReadOnly property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderReadOnlyPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setReadOnly({1});", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.ReadOnly) );
			}

		}


		/// <summary>
        /// Renders the SelectionLength property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSelectionLengthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the SelectionLength property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionLengthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.selectText({1},{2})", new JavaScriptCode(componentVariable), element.SelectionStart, element.SelectionLength);
			}

		}


		/// <summary>
        /// Renders the SelectOnFocus property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSelectOnFocusProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("selectOnFocus", element.SelectOnFocus );
				}
			}

		}

		/// <summary>
        /// Renders the SelectOnFocus property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectOnFocusPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Font property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderFontProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.FieldStyle)
				{
					// If Font value is not default
					if (ControlElementExtJSRenderer.IsNonDefaultFont(element.Font))
					{
						jsonWriter.WriteRaw(String.Format("font:{0};", NormalizeStyleValue( ControlElementExtJSRenderer.ReviseFont(element) )));
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.FieldStyle)
				{
					// If Font value is not default
					if (ControlElementExtJSRenderer.IsNonDefaultFont(element.Font))
					{
						jsonWriter.WriteRaw(String.Format("text-decoration:{0};", NormalizeStyleValue( ControlElementExtJSRenderer.ReviseFontDecoration(element) )));
					}
				}
			}

		}

		/// <summary>
        /// Renders the Font property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderFontPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setFieldStyle('font:{1};text-decoration:{2}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseFont(element) ,  ControlElementExtJSRenderer.ReviseFontDecoration(element) );
			}

		}


		/// <summary>
        /// Renders the ForeColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderForeColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.FieldStyle)
				{
					// If ForeColor value is not default
					if (element.ForeColor != Color.Empty)
					{
						jsonWriter.WriteRaw(String.Format("color:{0};", NormalizeStyleValue( ColorTranslator.ToHtml(element.ForeColor) )));
					}
				}
			}

		}

		/// <summary>
        /// Renders the ForeColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderForeColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setFieldStyle('color:{1}');", new JavaScriptCode(componentVariable),  ColorTranslator.ToHtml(element.ForeColor) );
			}

		}


		/// <summary>
        /// Renders the BackColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBackColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.FieldStyle)
				{
					jsonWriter.WriteRaw(String.Format("background-color:{0};", NormalizeStyleValue( ColorTranslator.ToHtml(element.BackColor) )));
				}
			}

		}

		/// <summary>
        /// Renders the BackColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderBackColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setFieldStyle('background-color:{1}');", new JavaScriptCode(componentVariable),  ColorTranslator.ToHtml(element.BackColor) );
			}

		}


		/// <summary>
        /// Renders the EnforceMaxLength property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnforceMaxLengthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.TextBoxBaseElement element = visualElement as System.Web.VisualTree.Elements.TextBoxBaseElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If EnforceMaxLength value is not default
					if (element.MaxLength > 0)
					{
						jsonWriter.WritePropertyWithRawValue("enforceMaxLength", @"true");
					}
				}
			}

		}

		/// <summary>
        /// Renders the EnforceMaxLength property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnforceMaxLengthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


	}
}