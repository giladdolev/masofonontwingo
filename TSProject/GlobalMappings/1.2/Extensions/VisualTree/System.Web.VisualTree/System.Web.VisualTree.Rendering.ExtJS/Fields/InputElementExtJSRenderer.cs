﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(InputElement), "ControlElementExtJSRenderer", "field", "Ext.form.field.Base")]
    [ExtJSPropertyDescription("msgTarget", PropertyInitializeCode = "msgTarget = 'side'")]
    [ExtJSEventDescription("LostFocus", ClientEventName = "focusleave",ClientCode = "", ClientHandlerCallbackData = "value = this.getValue()",
                                                            ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)")]
    [ExtJSPropertyDescription("AutoCompleteMode", PropertyInitializeCode = "typeAhead = <#= InputElementExtJSRenderer.ReviseTypeAheadProperty(element) #>", ClientNotDefaultCode = "element.AutoCompleteMode != AutoCompleteMode.None")]
    public class InputElementExtJSRenderer : InputElementExtJSRendererBase
    {

        /// <summary>
        /// Revises the typeAhead property.
        /// </summary>
        /// <param name="inputElement">The inputElement instance being rendered.</param>
        /// <returns></returns>
        internal static string ReviseTypeAheadProperty(InputElement inputElement)
        {
            // Default ExtJS value according to Sencha Docs
            bool typeAhead = false;

            if (inputElement != null)
            {
                if (inputElement.AutoCompleteMode == AutoCompleteMode.Append || inputElement.AutoCompleteMode == AutoCompleteMode.SuggestAppend)
                {
                    typeAhead = true;
                }
            }

            return typeAhead.ToString();
        }
    }
}
