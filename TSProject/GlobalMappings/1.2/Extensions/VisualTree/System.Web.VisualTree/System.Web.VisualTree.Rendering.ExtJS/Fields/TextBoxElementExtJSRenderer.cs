﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TextBoxElement), "TextBoxBaseElementExtJSRenderer", "textfield", "Ext.form.field.Text")]
    [ExtJSPropertyDescription("AllowBlank", PropertyInitializeCode = "allowBlank = <#=element.AllowBlank#>", ClientNotDefaultCode = "!element.AllowBlank")]
    [ExtJSPropertyDescription("FieldLabel", PropertyInitializeCode = "fieldLabel = <#=element.FieldLabel#>")]
    [ExtJSPropertyDescription("InputType", PropertyInitializeCode = "inputType = <#= TextBoxElementExtJSRenderer.ReviseInputType(element) #>")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "value = <#= element.Text #>",
                                            PropertyChangeCode = "this.setValue('<#= element.Text #>');")]
    [ExtJSPropertyDescription("TextAlign", PropertyInitializeCode = "fieldStyle.text-align = <#= TextBoxElementExtJSRenderer.ReviseTextAlignment(element) #>", ClientNotDefaultCode = "element.TextAlign != HorizontalAlignment.Left", PropertyChangeCode = "this.setFieldStyle('text-align:<#= TextBoxElementExtJSRenderer.ReviseTextAlignment(element) #>');")]

    [ExtJSEventDescription("Dirtychange", ClientEventName = "blur", ClientEventHandlerParameterNames = "view,event, eOpts")]
    [ExtJSEventDescription("TextChanged", ClientEventName = "change", ClientHandlerCallbackData = "value = this.getValue()",
                                                            ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)", ClientCode = "this.isDirtyValue = true;")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "FieldStyle.background = <#= ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != Color.Empty", PropertyChangeCode = "this.setFieldStyle('background:<#= ColorTranslator.ToHtml(element.BackColor) #>');")]
    public class TextBoxElementExtJSRenderer : TextBoxElementExtJSRendererBase
    {
        /// <summary> 
        /// Revise the event behavior type of TextChanged event.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        /// <returns></returns>
        protected override ExtJSEventBehaviorType ReviseTextChangedEventBehavior(RenderingContext renderingContext, ControlElement element, ExtJSEventBehaviorType eventBehaviorType)
        {
            if (element.HasTextChangedListeners)
            {
                return ExtJSEventBehaviorType.Immediate;
            }
            return ExtJSEventBehaviorType.Queued;
        }



        /// <summary>
        /// Indicates if we should render the TextChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <returns></returns>
        protected override bool ShouldRenderTextChangedEvent(RenderingContext context, ControlElement element)
        {
            WidgetColumnTextBoxElement textElement = element as WidgetColumnTextBoxElement;
            if (textElement != null && textElement.HasEditChange)
            {
                return false;
            }
            return true;
        }


        protected override bool ShouldRenderDirtychangeEvent(RenderingContext context, TextBoxElement element)
        {
            WidgetColumnTextBoxElement textElement = element as WidgetColumnTextBoxElement;
            if (textElement != null && textElement.HasItemChanged)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Redirect to the right render class
        /// according the multiline porperty
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">the control</param>
        /// <returns></returns>
        public override IVisualElementRenderer ReviseRenderer(RenderingContext renderingContext, IVisualElement visualElement)
        {

            // Get textbox element 
            TextBoxElement textBoxElement = (TextBoxElement)visualElement;

            // If there is a valid text box element
            if (textBoxElement != null)
            {
                // If is a multiline textbox
                if (textBoxElement.Multiline)
                {
                    // use MultiLineTextBoxElementExtJSRenderer  for render
                    return new MultilineTextBoxElementExtJSRenderer();
                }
            }

            // use simple textbox render 
            return this;
        }



        /// <summary>
        /// Revises the type of the text box input.
        /// </summary>
        /// <param name="textBoxElement">The text box element.</param>
        /// <returns></returns>
        internal static string ReviseInputType(TextBoxElement textBoxElement)
        {
            string inputType = "text";

            if (textBoxElement != null)
            {
                // Render password field
                if (!String.IsNullOrEmpty(textBoxElement.PasswordChar) || textBoxElement.UseSystemPasswordChar)
                {
                    inputType = "password";
                }
                inputType = textBoxElement.InputType;
            }

            return inputType;
        }
        /// <summary>
        /// Revises the text alignment.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        internal static string ReviseTextAlignment(TextBoxElement control)
        {
            switch (control.TextAlign)
            {
                case HorizontalAlignment.Right:
                    return "right";
                case HorizontalAlignment.Center:
                    return "center";
            }
            return "left";
        }

        /// <summary>
        /// Gets the display text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        protected override string GetDisplayText(string text)
        {
            return HttpUtility.JavaScriptStringEncode(text);
        }


        
    }
}
