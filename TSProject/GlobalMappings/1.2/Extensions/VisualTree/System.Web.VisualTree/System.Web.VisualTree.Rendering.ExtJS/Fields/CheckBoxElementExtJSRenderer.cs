﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(CheckBoxElement), "ButtonBaseElementExtJSRenderer", "checkboxfield", "Ext.form.field.Checkbox")]
    [ExtJSPropertyDescription("IsChecked", ClientConfigName = "checked", PropertyChangeCode = "this.setValue(<#= JavaScriptCode.GetBool(element.IsChecked) #>);")]
    [ExtJSPropertyDescription("CheckAlign", PropertyInitializeCode = "boxLabelAlign=<#= CheckBoxElementExtJSRenderer.ReviseLabelAlignment(element.CheckAlign)#>")]
    [ExtJSPropertyDescription("AutoCheck", PropertyInitializeCode = "readOnly = <#=!element.AutoCheck#>", PropertyChangeCode = "this.setReadOnly(<#=JavaScriptCode.GetBool(!element.AutoCheck)#>);")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "boxLabel=<#= GetDisplayText(element.Text)#>", PropertyChangeCode = "this.setBoxLabel('<#= GetDisplayText(element.Text) #>');")]
    [ExtJSPropertyDescription("ThreeState", PropertyInitializeCode = "threeState = <#= element.ThreeState #>", PropertyChangeCode = "this.setThreeState(<#= JavaScriptCode.GetBool(element.ThreeState) #>);")]
    [ExtJSEventDescription("CheckStateChanged", ClientEventName = "change", ClientHandlerCallbackData = "value = this.getValue()", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)")]
    [ExtJSEventDescription("LostFocus", ClientEventName = "focusleave", ClientHandlerCallbackData = "value = this.getValue()", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)")]
    public class CheckBoxElementExtJSRenderer : CheckBoxElementExtJSRendererBase
    {

        /// <summary>
        /// Revises the XTYPE property value.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <returns></returns>
        protected override string ReviseXTypePropertyValue(VisualElement visualElement, string xtype)
        {
            // Get CheckBoxElement .
            CheckBoxElement checkBoxElement = visualElement as CheckBoxElement;

            // If there is a valid checkBoxElement
            if (checkBoxElement != null)
            {
             
                if (checkBoxElement.ThreeState == true)
                {
                    // Return title bar XTYPE
                    return "three-check-box";
                }
            }

            return xtype;
        }

        /// <summary>
        /// Revises the label alignment.
        /// </summary>
        /// <param name="alignment">The alignment.</param>
        /// <returns></returns>
        public static string ReviseLabelAlignment(ContentAlignment alignment)
        {
            switch (alignment)
            {
                case ContentAlignment.BottomRight:
                case ContentAlignment.MiddleRight:
                case ContentAlignment.TopRight:
                    return "before";
            }

            return null;
        }

   
    }
}
