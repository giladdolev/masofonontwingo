﻿using System.IO;
using Newtonsoft.Json;
using System.Web.VisualTree.Elements;
using System.Globalization;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(FileUploadElement), "ControlElementExtJSRenderer", "form", "Ext.form.Panel")]
    [ExtJSPropertyDescription("Frame", PropertyInitializeCode = "frame = false")]
    [ExtJSPropertyDescription("BodyPadding", PropertyInitializeCode = "bodyPadding = 5")]
    [ExtJSPropertyDescription("Defaults", PropertyInitializeCode = "defaults = {anchor:'100%', allowBlank:false, msgTarget:'side'}")]
    [ExtJSPropertyDescription("Items", PropertyInitializeCode = "items = <#resource:items.js#>", PropertyChangeCode = "items = <#resource:items.js#>")]
    [ExtJSPropertyDescription("Buttons", PropertyInitializeCode = "buttons = <#resource:buttons.js#>")]
    public class FileUploadElementExtJSRenderer : FileUploadElementExtJSRendererBase
    {



        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <exception cref="System.ArgumentNullException">jsonWriter</exception>
        protected internal override void RenderResourceResult(RenderingContext renderingContext, VisualElement visualElement, string resourceId, JsonTextWriter jsonWriter)
        {
            if (jsonWriter == null)
            {
                throw new ArgumentNullException("jsonWriter");
            }

            jsonWriter.WriteStartObject();

            try
            {
                // Get file upload element
                FileUploadElement fileUploadElement = visualElement as FileUploadElement;

                // If there is a valid file upload
                if (fileUploadElement != null)
                {
                    // Get the http context
                    HttpContext httpContext = HttpContext.Current;

                    // If there is a valid http context
                    if (httpContext != null)
                    {
                        // Get the http request
                        HttpRequest httpRequest = httpContext.Request;

                        // If there is a valid http request
                        if (httpRequest != null)
                        {
                            // Get uploaded file
                            HttpPostedFile postedFile = httpRequest.Files["uploaded-file"];

                            // If there is a valid uploaded file
                            if (postedFile != null)
                            {
                                // Handler posted file
                                fileUploadElement.PerformFileUploaded(postedFile);
                            }
                        }
                    }
                }

                // Indicate we succeeded to upload the file
                jsonWriter.WriteProperty("success", true);
            }
            catch (HttpException e)
            {
                FailToUploadFile(jsonWriter, e);
            }
            catch (IOException e)
            {
                FailToUploadFile(jsonWriter, e);
            }

            // Write the results property
            jsonWriter.WritePropertyName("results");

            // Render the application results
            ApplicationElementExtJSRenderer.RenderResult(renderingContext, ApplicationElement.Current, jsonWriter);

            // Write the end object
            jsonWriter.WriteEndObject();
        }

        /// <summary>
        /// Fails to upload file.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="exception">The exception.</param>
        private static void FailToUploadFile(JsonTextWriter jsonWriter, Exception exception)
        {
            jsonWriter.WriteProperty("success", false);
            jsonWriter.WriteProperty("errors", exception.Message);
        }
    }
}
