try {
    if (cmp.ownerGrid){
        cmp.ownerGrid.fireEvent('widgetControlItemChanged', cmp.ownerGrid, view.getValue(), view.getValue(), this.dataIndex);
    }
} catch (err) {
    console.log("error :- widgetControlItemChanged : " + err.message);
}
