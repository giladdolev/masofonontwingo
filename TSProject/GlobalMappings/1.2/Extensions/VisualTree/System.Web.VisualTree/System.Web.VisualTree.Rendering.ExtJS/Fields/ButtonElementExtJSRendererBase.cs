using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.Drawing;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class ButtonElementExtJSRendererBase : ButtonBaseElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.button.Button";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "button";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the PressedChange event attach code.
			RenderAttachPressedChangeEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "PressedChange":
					return this.ExtractPressedChangeCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			this.RenderBorderStyleProperty(context, visualElement, styleWriter);
		}


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "CssClass":
					this.RenderCssClassPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "IsPressed":
					this.RenderIsPressedPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "EnableToggle":
					this.RenderEnableTogglePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Scale":
					this.RenderScalePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Font":
					this.RenderFontPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BorderStyle":
					this.RenderBorderStylePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ForeColor":
					this.RenderForeColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Text":
					this.RenderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderCssClassProperty(renderingContext, visualElement, jsonWriter);
			this.RenderIsPressedProperty(renderingContext, visualElement, jsonWriter);
			this.RenderEnableToggleProperty(renderingContext, visualElement, jsonWriter);
			this.RenderScaleProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the PressedChange event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachPressedChangeEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderPressedChangeEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.RevisePressedChangeEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "pressed", "eOpts"}, 
									() => {  
										// Create the server callback code.PressedChange
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "PressedChange", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{pressState : pressed}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("toggle", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of PressedChange event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType RevisePressedChangeEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ButtonElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the PressedChange event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderPressedChangeEvent(RenderingContext context, System.Web.VisualTree.Elements.ButtonElement element)
		{
			return (element.HasPressedChangeListeners);
		}


		/// <summary>
        /// Extracts the PressedChange event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractPressedChangeCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new ButtonPressedChangeEventArgs(Convert.ToBoolean(args.Value<System.String>("pressState")));
		}


		/// <summary>
        /// Renders the PressedChange event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderPressedChangeAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderPressedChangeEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.RevisePressedChangeEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "pressed", "eOpts"}, 
                            () => {  
										// Create the server callback code.PressedChange
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "PressedChange", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{pressState : pressed}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('toggle', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the PressedChange event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderPressedChangeRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderPressedChangeEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "toggle");
            }
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "PressedChange":
					this.RenderPressedChangeAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "PressedChange":
					this.RenderPressedChangeRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the CssClass property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderCssClassProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If CssClass value is not default
					if ( !String.IsNullOrEmpty(element.CssClass))
					{
						// If property is valid
						if(ValidateProperty(element.CssClass))
						{
							jsonWriter.WriteProperty("iconCls",  element.CssClass );
						}
					}
				}
			}

		}

		/// <summary>
        /// Renders the CssClass property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCssClassPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setIconCls({1})", new JavaScriptCode(componentVariable),  element.CssClass );
			}

		}


		/// <summary>
        /// Renders the IsPressed property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderIsPressedProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If IsPressed value is not default
					if (element.IsPressed != false)
					{
						jsonWriter.WriteProperty("pressed",  element.IsPressed );
					}
				}
			}

		}

		/// <summary>
        /// Renders the IsPressed property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderIsPressedPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setPressed('{1}');", new JavaScriptCode(componentVariable),  element.IsPressed );
			}

		}


		/// <summary>
        /// Renders the EnableToggle property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnableToggleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If EnableToggle value is not default
					if (element.EnableToggle != false)
					{
						jsonWriter.WriteProperty("enableToggle",  element.EnableToggle );
					}
				}
			}

		}

		/// <summary>
        /// Renders the EnableToggle property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnableTogglePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.toggle({1})  ", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.EnableToggle ));
			}

		}


		/// <summary>
        /// Renders the Scale property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderScaleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Scale value is not default
					if (element.Scale != ScaleConstants.Small)
					{
						jsonWriter.WriteProperty("scale",  ButtonElementExtJSRenderer.ReviseScale(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Scale property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderScalePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setScale('{1}');", new JavaScriptCode(componentVariable),  ButtonElementExtJSRenderer.ReviseScale(element) );
			}

		}


		/// <summary>
        /// Renders the Font property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderFontProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the Font property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderFontPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setText('{1}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseFormattedText(element) );
			}

		}


		/// <summary>
        /// Renders the BorderStyle property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBorderStyleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("border",  ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("borderRight",  ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					jsonWriter.WriteProperty("borderBottom",  ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) );
				}
			}

		}

		/// <summary>
        /// Renders the BorderStyle property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderBorderStylePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle({{{1}}});", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseBorderStyle(element) );
			}

		}


		/// <summary>
        /// Renders the ForeColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderForeColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ForeColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderForeColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setText('{1}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseFormattedText(element) );
			}

		}


		/// <summary>
        /// Renders the Text property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.Text))
					{
						jsonWriter.WriteProperty("text",  ControlElementExtJSRenderer.ReviseFormattedText(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Text property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.ButtonElement element = visualElement as System.Web.VisualTree.Elements.ButtonElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setText('{1}');", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseFormattedText(element) );
			}

		}


	}
}