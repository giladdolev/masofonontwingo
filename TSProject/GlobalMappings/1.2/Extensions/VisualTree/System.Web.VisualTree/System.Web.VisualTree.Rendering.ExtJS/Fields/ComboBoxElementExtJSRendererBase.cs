using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using System.Text;


namespace System.Web.VisualTree.Rendering.ExtJS
{

    public class ComboBoxElementExtJSRendererBase : ListControlElementExtJSRenderer
    {
        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.form.field.ComboBox";
            }
        }


        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "combo";
            }
        }


        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
            // Call the base implementation
            base.RenderEvents(context, visualElement, writer);
            // Renders the DropDownClosed event attach code.
            RenderAttachDropDownClosedEvent(context, visualElement, writer);
            // Renders the SelectedIndexChanged event attach code.
            RenderAttachSelectedIndexChangedEvent(context, visualElement, writer);
            // Renders the SelectionChangeCommitted event attach code.
            RenderAttachSelectionChangeCommittedEvent(context, visualElement, writer);
            // Renders the DropDown event attach code.
            RenderAttachDropDownEvent(context, visualElement, writer);
            // Renders the BeforeSelect event attach code.
            RenderAttachBeforeSelectEvent(context, visualElement, writer);
            // Renders the Validated event attach code.
            RenderAttachValidatedEvent(context, visualElement, writer);
        }


        /// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
            // Choose event name
            switch (serverEventName)
            {
                case "DropDownClosed":
                    return this.ExtractDropDownClosedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "SelectedIndexChanged":
                    return this.ExtractSelectedIndexChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "SelectionChangeCommitted":
                    return this.ExtractSelectionChangeCommittedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "DropDown":
                    return this.ExtractDropDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "BeforeSelect":
                    return this.ExtractBeforeSelectCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "Validated":
                    return this.ExtractValidatedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
                    // Call the base implementation
                    return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }
        }


        /// <summary>
        /// Renders the field style content properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderFieldStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
            base.RenderFieldStyleContentProperties(context, visualElement, styleWriter);

            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }
            this.RenderBackColorProperty(context, visualElement, styleWriter);
        }


        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            // Choose property
            switch (property)
            {
                case "Text":
                    this.RenderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "DropDownHeight":
                    this.RenderDropDownHeightPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "DropDownStyle":
                    this.RenderDropDownStylePropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "SelectTextOnFocus":
                    this.RenderSelectTextOnFocusPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "BackColor":
                    this.RenderBackColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "DataSource":
                    this.RenderDataSourcePropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "DropDownWidth":
                    this.RenderDropDownWidthPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "MatchComboboxWidth":
                    this.RenderMatchComboboxWidthPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "minChars":
                    this.RenderminCharsPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "queryMode":
                    this.RenderqueryModePropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Locked":
                    this.RenderLockedPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            this.RenderTextProperty(renderingContext, visualElement, jsonWriter);
            this.RenderDropDownHeightProperty(renderingContext, visualElement, jsonWriter);
            this.RenderDropDownStyleProperty(renderingContext, visualElement, jsonWriter);
            this.RenderSelectTextOnFocusProperty(renderingContext, visualElement, jsonWriter);
            this.RenderDataSourceProperty(renderingContext, visualElement, jsonWriter);
            this.RenderDropDownWidthProperty(renderingContext, visualElement, jsonWriter);
            this.RenderMatchComboboxWidthProperty(renderingContext, visualElement, jsonWriter);
            this.RenderminCharsProperty(renderingContext, visualElement, jsonWriter);
            this.RenderqueryModeProperty(renderingContext, visualElement, jsonWriter);
            this.RenderLockedProperty(renderingContext, visualElement, jsonWriter);
        }


        /// <summary>
        /// Renders the DropDownClosed event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDropDownClosedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
        }


        /// <summary>
        /// Revise the event behavior type of DropDownClosed event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDropDownClosedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the DropDownClosed event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDropDownClosedEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element)
        {
            return (element.HasDropDownClosedListeners);
        }


        /// <summary>
        /// Extracts the DropDownClosed event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractDropDownClosedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the DropDownClosed event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownClosedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the DropDownClosed event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownClosedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

        /// <summary>
        /// Renders the SelectedIndexChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderSelectedIndexChangedEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseSelectedIndexChangedEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "combo", "records", "eOpts" },
                                    () =>
                                    {
                                        // Create the server callback code.SelectedIndexChanged

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "SelectedIndexChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{selectedIndex : VT_GetComboboxSelectedIndex(combo,records) }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("select", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of SelectedIndexChanged event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseSelectedIndexChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the SelectedIndexChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element)
        {
            return true;
        }


        /// <summary>
        /// Extracts the SelectedIndexChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractSelectedIndexChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new ValueChangedArgs<int>(args.Value<System.Int32>("selectedIndex"));
        }


        /// <summary>
        /// Renders the SelectedIndexChanged event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectedIndexChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderSelectedIndexChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseSelectedIndexChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "combo", "records", "eOpts" },
                            () =>
                            {
                                // Create the server callback code.SelectedIndexChanged

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "SelectedIndexChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{selectedIndex : VT_GetComboboxSelectedIndex(combo,records) }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('select', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the SelectedIndexChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectedIndexChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderSelectedIndexChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "select");
            }
        }

        /// <summary>
        /// Renders the SelectionChangeCommitted event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachSelectionChangeCommittedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
        }


        /// <summary>
        /// Revise the event behavior type of SelectionChangeCommitted event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseSelectionChangeCommittedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the SelectionChangeCommitted event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderSelectionChangeCommittedEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element)
        {
            return (element.HasSelectionChangeCommittedListeners);
        }


        /// <summary>
        /// Extracts the SelectionChangeCommitted event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractSelectionChangeCommittedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the SelectionChangeCommitted event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangeCommittedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the SelectionChangeCommitted event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangeCommittedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

        /// <summary>
        /// Renders the DropDown event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDropDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
        }


        /// <summary>
        /// Revise the event behavior type of DropDown event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDropDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the DropDown event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDropDownEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element)
        {
            return (element.HasDropDownListeners);
        }


        /// <summary>
        /// Extracts the DropDown event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractDropDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the DropDown event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the DropDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

        /// <summary>
        /// Renders the BeforeSelect event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeSelectEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderBeforeSelectEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseBeforeSelectEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "combo", "record", "index", "eOpts" },
                                    () =>
                                    {
                                        // Create the client callback code.BeforeSelect
                                        if (!String.IsNullOrEmpty(element.ClientBeforeSelect))
                                        {
                                            javaScriptBuilder.AppendRawValue(element.ClientBeforeSelect);
                                            javaScriptBuilder.AppendSemicolon();
                                        }
                                        // Create the server callback code.BeforeSelect

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "BeforeSelect", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("beforeselect", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of BeforeSelect event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeSelectEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the BeforeSelect event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeSelectEvent(RenderingContext context, System.Web.VisualTree.Elements.ComboBoxElement element)
        {
            return (!String.IsNullOrEmpty(element.ClientBeforeSelect) || element.HasBeforeSelectListeners);
        }


        /// <summary>
        /// Extracts the BeforeSelect event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractBeforeSelectCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the BeforeSelect event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderBeforeSelectEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseBeforeSelectEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "combo", "record", "index", "eOpts" },
                            () =>
                            {
                                // Create the client callback code.BeforeSelect
                                if (!String.IsNullOrEmpty(element.ClientBeforeSelect))
                                {
                                    javaScriptBuilder.AppendRawValue(element.ClientBeforeSelect);
                                    javaScriptBuilder.AppendSemicolon();
                                }
                                // Create the server callback code.BeforeSelect

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "BeforeSelect", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('beforeselect', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the BeforeSelect event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderBeforeSelectEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "beforeselect");
            }
        }

        /// <summary>
        /// Renders the Validated event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachValidatedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderValidatedEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseValidatedEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { },
                                    () =>
                                    {
                                        // Create the server callback code.Validated

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Validated", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("validitychange", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of Validated event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseValidatedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the Validated event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderValidatedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (element.HasValidatedListeners);
        }


        /// <summary>
        /// Extracts the Validated event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractValidatedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the Validated event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderValidatedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderValidatedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseValidatedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { },
                            () =>
                            {
                                // Create the server callback code.Validated

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Validated", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('validitychange', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the Validated event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderValidatedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderValidatedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "validitychange");
            }
        }

        /// <summary>
        /// Renders the  Add Listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Choose event
            switch (eventName)
            {
                case "SelectedIndexChanged":
                    this.RenderSelectedIndexChangedAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "BeforeSelect":
                    this.RenderBeforeSelectAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Validated":
                    this.RenderValidatedAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

        /// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Choose event
            switch (eventName)
            {
                case "SelectedIndexChanged":
                    this.RenderSelectedIndexChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "BeforeSelect":
                    this.RenderBeforeSelectRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Validated":
                    this.RenderValidatedRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
        /// <summary>
        /// Renders the Text property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If property is valid
                    if (ValidateProperty(element.Text))
                    {
                        jsonWriter.WriteProperty("value", ComboBoxElementExtJSRenderer.GetValue(element));
                    }
                }
            }

        }

        /// <summary>
        /// Renders the Text property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setValue('{1}');", new JavaScriptCode(componentVariable), ComboBoxElementExtJSRenderer.GetValue(element));
            }

        }


        /// <summary>
        /// Renders the DropDownHeight property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDropDownHeightProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the DropDownHeight property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownHeightPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.getPicker().setBox({{ height: {1} }});", new JavaScriptCode(componentVariable), element.DropDownHeight);
            }

        }


        /// <summary>
        /// Renders the DropDownStyle property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDropDownStyleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    jsonWriter.WriteProperty("editable", ComboBoxElementExtJSRenderer.ReviseEditableProperty(element));
                }
            }

        }

        /// <summary>
        /// Renders the DropDownStyle property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownStylePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setEditable({1})", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(ComboBoxElementExtJSRenderer.ReviseEditableProperty(element)));
            }

        }


        /// <summary>
        /// Renders the SelectTextOnFocus property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSelectTextOnFocusProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If SelectTextOnFocus value is not default
                    if (element.SelectTextOnFocus != false && ComboBoxElementExtJSRenderer.ReviseEditableProperty(element) == true && element.Locked != true)
                    {
                        jsonWriter.WriteProperty("selectOnFocus", element.SelectTextOnFocus);
                    }
                }
            }

        }

        /// <summary>
        /// Renders the SelectTextOnFocus property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectTextOnFocusPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the AutoCompleteMode property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAutoCompleteModeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If AutoCompleteMode value is not default
                    if (element.AutoCompleteMode != AutoCompleteMode.None)
                    {
                        jsonWriter.WritePropertyWithRawValue("typeAhead", @"true");
                    }
                }
            }

        }

        /// <summary>
        /// Renders the BackColor property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBackColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.FieldStyle)
                {
                    // If BackColor value is not default
                    if (element.BackColor != Color.Empty)
                    {
                        jsonWriter.WriteRaw(String.Format("background:{0};", NormalizeStyleValue(ColorTranslator.ToHtml(element.BackColor))));
                    }
                }
            }

        }

        /// <summary>
        /// Renders the BackColor property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderBackColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setFieldStyle('background:{1}');", new JavaScriptCode(componentVariable), ColorTranslator.ToHtml(element.BackColor));
            }

        }


        /// <summary>
        /// Renders the DataSource property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDataSourceProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the DataSource property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataSourcePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the DropDownWidth property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDropDownWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the DropDownWidth property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownWidthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.getPicker().setBox({{ width: {1} }});", new JavaScriptCode(componentVariable), element.DropDownWidth);
            }

        }


        /// <summary>
        /// Renders the MatchComboboxWidth property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMatchComboboxWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If MatchComboboxWidth value is not default
                    if (element.MatchComboboxWidth != true)
                    {
                        jsonWriter.WriteProperty("matchFieldWidth", element.MatchComboboxWidth);
                    }
                }
            }

        }

        /// <summary>
        /// Renders the MatchComboboxWidth property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMatchComboboxWidthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the minChars property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderminCharsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If minChars value is not default
                    if (element.AutoCompleteMode != AutoCompleteMode.None)
                    {
                        jsonWriter.WritePropertyWithRawValue("minChars", @"1");
                    }
                }
            }

        }

        /// <summary>
        /// Renders the minChars property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderminCharsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the queryMode property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderqueryModeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    jsonWriter.WritePropertyWithRawValue("queryMode", @"'local'");
                }
            }

        }

        /// <summary>
        /// Renders the queryMode property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderqueryModePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the Locked property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderLockedProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
            }

        }

        /// <summary>
        /// Renders the Locked property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLockedPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setEditable({1});", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(!(element.Locked)));
            }

        }


        /// <summary>
        /// Extract the method callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
            // Choose method
            switch (methodName)
            {
                case "CollapseDropDownList":
                    return this.ExtractCollapseDropDownListMethodCallbackData(context, eventObject);
                case "ExpandDropDownList":
                    return this.ExtractExpandDropDownListMethodCallbackData(context, eventObject);
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Choose method
            switch (methodName)
            {
                case "CollapseDropDownList":
                    this.RenderCollapseDropDownListMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                case "ExpandDropDownList":
                    this.RenderExpandDropDownListMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the CollapseDropDownList method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderCollapseDropDownListMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Object data = (System.Object)methodData;
                    builder.AppendFormattedRawValue(@"{0}.collapse()", new JavaScriptCode(componentVariableName));
                }
            }

        }


        /// <summary>
        /// Gets the method CollapseDropDownList callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractCollapseDropDownListMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


        /// Renders the ExpandDropDownList method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderExpandDropDownListMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ComboBoxElement element = visualElement as System.Web.VisualTree.Elements.ComboBoxElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Object data = (System.Object)methodData;
                    builder.AppendFormattedRawValue(@"{0}.expand()", new JavaScriptCode(componentVariableName));
                }
            }

        }


        /// <summary>
        /// Gets the method ExpandDropDownList callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractExpandDropDownListMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


    }
}