﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(RangeElement), "ControlElementExtJSRenderer", "slider", "Ext.slider.Single")]
    [ExtJSPropertyDescription("Increment", PropertyInitializeCode = "increment = <#= element.Increment #>", ClientDefault = 1)]
    [ExtJSPropertyDescription("MaxValue", PropertyInitializeCode = "maxValue = <#= element.MaxValue #>", ClientDefault = 100, PropertyChangeCode = "this.setMaxValue(<#= element.MaxValue #>);")]
    [ExtJSPropertyDescription("MinValue", PropertyInitializeCode = "minValue = <#= element.MinValue #>", ClientDefault = 0, PropertyChangeCode = "this.setMinValue(<#= element.MinValue #>);")]
    [ExtJSPropertyDescription("ValuesRange", PropertyInitializeCode = "values = <#= element.ValuesRange #>", PropertyChangeCode = "this.setValues(<#= element.ValuesRange #>);")]

    public class RangeElementExtJSRenderer : RangeElementExtJSRendererBase
    {

    }
}
