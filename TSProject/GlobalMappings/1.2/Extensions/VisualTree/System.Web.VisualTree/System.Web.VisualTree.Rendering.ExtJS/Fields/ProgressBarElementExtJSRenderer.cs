﻿using System.Globalization;
using System.Web.VisualTree.Elements;
using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    // TODO: GENERATOR-TOREVIEW (Formatters, Non regular update method, other properties effecting updating of property)
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ProgressBarElement), "ControlElementExtJSRenderer", "progressbar", "Ext.ProgressBar")]
    [ExtJSPropertyDescription("Value", PropertyInitializeCode = "value = <#= ProgressBarElementExtJSRenderer.FormatValue(element) #>", 
                                        PropertyChangeCode = "this.updateProgress(<#= ProgressBarElementExtJSRenderer.FormatValue(element) #>, '<#= ProgressBarElementExtJSRenderer.FormatText(element) #>');")]      
    public class ProgressBarElementExtJSRenderer : ProgressBarElementExtJSRendererBase
    {
        /// <summary>
        /// Formats the text.
        /// </summary>
        /// <param name="progressbarElement">The progress bar element.</param>
        /// <returns></returns>
        public static string FormatText(ProgressBarElement progressbarElement)
        {
            return string.Concat(Math.Ceiling(FormatValue(progressbarElement) * 100), "%");
        }


        /// <summary>
        /// Formats the value.
        /// </summary>
        /// <param name="progressbarElement">The progress bar element.</param>
        /// <returns></returns>
        public static double FormatValue(ProgressBarElement progressbarElement)
        {
            double value = 0;
            
            if (progressbarElement != null)
            {
                if (progressbarElement.Min < progressbarElement.Max)
                {
                    // value should be between 0-1
                    value = (progressbarElement.Value - progressbarElement.Min) / (progressbarElement.Max - progressbarElement.Min);
                }
            }

            return value;
        }

    }
}
