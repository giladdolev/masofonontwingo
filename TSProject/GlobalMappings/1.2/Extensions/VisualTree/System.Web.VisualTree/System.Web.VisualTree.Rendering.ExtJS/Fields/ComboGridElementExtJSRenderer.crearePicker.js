
var columns = <#=data.Columns#>;
var data = <#=data.Data#>;
var showFilter = <#=data.ShowFilter#>;
var displayMember = <#=data.DisplayMember#>;
var valueMember = <#=data.ValueMember#>;

this.createStore(columns,data,showFilter,displayMember,valueMember);