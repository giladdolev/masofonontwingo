﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering;

namespace System.Web.VisualTree.DevExpress
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(RibbonPageElement), "PanelElementExtJSRenderer", "buttongroup", "Ext.container.ButtonGroup")]
    public class RibbonPageElementExtJSRenderer : RibbonPageElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <exception cref="System.ArgumentNullException">jsonWriter</exception>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

            RibbonPageElement ribbonPage = visualElement as RibbonPageElement;

            if (ribbonPage != null)
            {
                // Write Items
                RenderPlainItemsProperty(renderingContext, ribbonPage.Groups, jsonWriter, "items");
            }
        }
    }
}
