﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree.DevExpress
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(RadioGroupItemElement), "RadioButtonElementExtJSRenderer", "radiofield", "Ext.form.field.Radio")]

    public class RadioGroupItemElementExtJSRenderer : RadioGroupItemElementExtJSRendererBase
    {

    }
}
