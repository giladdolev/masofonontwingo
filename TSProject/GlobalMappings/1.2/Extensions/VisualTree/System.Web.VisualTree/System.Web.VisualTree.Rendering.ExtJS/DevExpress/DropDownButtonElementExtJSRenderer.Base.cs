using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class DropDownButtonElementExtJSRendererBase : ToolBarDropDownItemExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.button.Split";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "splitbutton";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the DropDownOpening event attach code.
			RenderAttachDropDownOpeningEvent(context, visualElement, writer); 
			// Renders the DropDownOpened event attach code.
			RenderAttachDropDownOpenedEvent(context, visualElement, writer); 
			// Renders the DropDownClosed event attach code.
			RenderAttachDropDownClosedEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "DropDownOpening":
					return this.ExtractDropDownOpeningCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DropDownOpened":
					return this.ExtractDropDownOpenedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DropDownClosed":
					return this.ExtractDropDownClosedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the DropDownOpening event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDropDownOpeningEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderDropDownOpeningEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseDropDownOpeningEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.DropDownOpening
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "DropDownOpening", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("arrowclick", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of DropDownOpening event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDropDownOpeningEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.DropDownButtonElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DropDownOpening event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDropDownOpeningEvent(RenderingContext context, System.Web.VisualTree.Elements.DropDownButtonElement element)
		{
			return (element.HasDropDownOpeningListeners);
		}


		/// <summary>
        /// Extracts the DropDownOpening event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDropDownOpeningCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DropDownOpening event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownOpeningAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDropDownOpeningEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseDropDownOpeningEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.DropDownOpening
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "DropDownOpening", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('arrowclick', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the DropDownOpening event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownOpeningRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDropDownOpeningEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "arrowclick");
            }
        }

		/// <summary>
        /// Renders the DropDownOpened event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDropDownOpenedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderDropDownOpenedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseDropDownOpenedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.DropDownOpened
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "DropDownOpened", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("menushow", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of DropDownOpened event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDropDownOpenedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.DropDownButtonElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DropDownOpened event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDropDownOpenedEvent(RenderingContext context, System.Web.VisualTree.Elements.DropDownButtonElement element)
		{
			return (element.HasDropDownOpenedListeners);
		}


		/// <summary>
        /// Extracts the DropDownOpened event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDropDownOpenedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DropDownOpened event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownOpenedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDropDownOpenedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseDropDownOpenedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.DropDownOpened
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "DropDownOpened", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('menushow', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the DropDownOpened event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownOpenedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDropDownOpenedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "menushow");
            }
        }

		/// <summary>
        /// Renders the DropDownClosed event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDropDownClosedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderDropDownClosedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseDropDownClosedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.DropDownClosed
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "DropDownClosed", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("menuhide", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of DropDownClosed event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDropDownClosedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.DropDownButtonElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DropDownClosed event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDropDownClosedEvent(RenderingContext context, System.Web.VisualTree.Elements.DropDownButtonElement element)
		{
			return (element.HasDropDownClosedListeners);
		}


		/// <summary>
        /// Extracts the DropDownClosed event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDropDownClosedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DropDownClosed event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownClosedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDropDownClosedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseDropDownClosedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.DropDownClosed
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "DropDownClosed", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('menuhide', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the DropDownClosed event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownClosedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDropDownClosedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "menuhide");
            }
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "DropDownOpening":
					this.RenderDropDownOpeningAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DropDownOpened":
					this.RenderDropDownOpenedAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DropDownClosed":
					this.RenderDropDownClosedAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.DropDownButtonElement element = visualElement as System.Web.VisualTree.Elements.DropDownButtonElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "DropDownOpening":
					this.RenderDropDownOpeningRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DropDownOpened":
					this.RenderDropDownOpenedRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DropDownClosed":
					this.RenderDropDownClosedRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
	}
}