﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DropDownButtonElement), "ToolBarDropDownItemExtJSRenderer", "splitbutton", "Ext.button.Split")]
    [ExtJSEventDescription("DropDownOpening", ClientEventName = "arrowclick")]
    [ExtJSEventDescription("DropDownOpened", ClientEventName = "menushow")]
    [ExtJSEventDescription("DropDownClosed", ClientEventName = "menuhide")]

    public class DropDownButtonElementExtJSRenderer : DropDownButtonElementExtJSRendererBase
    {
        /// <summary>
        /// Gets the item x type
        /// </summary>
        /// <value>
        /// The item x type.
        /// </value>
        protected override string ItemXType
        {
            get
            {
                return XType;
            }
        }

    }
    
}
