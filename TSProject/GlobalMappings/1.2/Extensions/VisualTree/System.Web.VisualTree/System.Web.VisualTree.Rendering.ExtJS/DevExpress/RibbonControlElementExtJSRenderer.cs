﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree.DevExpress
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(RibbonControlElement), "ContentControlElementExtJSRenderer", "tabpanel", "Ext.tab.Panel")]
    public class RibbonControlElementExtJSRenderer : RibbonControlElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RibbonControlElement ribbonCtrl = visualElement as RibbonControlElement;

            if (ribbonCtrl != null)
            {
                // Write RibbonPages
                RenderPlainItemsProperty(renderingContext, ribbonCtrl.Pages, jsonWriter, "items");
            }
        }
    }
}
