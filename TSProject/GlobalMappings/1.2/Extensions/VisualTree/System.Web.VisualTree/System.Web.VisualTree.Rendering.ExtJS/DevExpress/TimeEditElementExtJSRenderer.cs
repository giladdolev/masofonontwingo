﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TimeEditElement), "TextBoxElementExtJSRenderer", "timefield", "Ext.form.field.Time")]
    [ExtJSPropertyDescription("Increment", PropertyInitializeCode = "increment = <#= element.Increment #>")]
    [ExtJSPropertyDescription("MaxTime", PropertyInitializeCode = "maxValue = <#= TimeEditElementExtJSRenderer.ReviseMaxTime(element) #>", ClientNotDefaultCode = "element.MaxTime != DateTime.MaxValue", PropertyChangeCode = "this.setMaxValue(<#= element.MaxTime #>);")]
    [ExtJSPropertyDescription("MinTime", PropertyInitializeCode = "minValue = <#= TimeEditElementExtJSRenderer.ReviseMinTime(element) #>", ClientNotDefaultCode = "element.MinTime != DateTime.MinValue", PropertyChangeCode = "this.setMinValue(<#= element.MinTime #>);")]

    public class TimeEditElementExtJSRenderer : TimeEditElementExtJSRendererBase
    {
        /// <summary>
        /// Revises the MaxTime property
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static string ReviseMaxTime(TimeEditElement element)
        {
            if (element != null)
                return element.MaxTime.ToString(TimeEditElement.MAX_TIME_FOEMAT);
            else
                return null;
        }


        /// <summary>
        /// Revises the MinTime property
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static string ReviseMinTime(TimeEditElement element)
        {
            if (element != null)
                return element.MinTime.ToString("h:mm tt");
            else
                return null;
        }
    }
    
}
