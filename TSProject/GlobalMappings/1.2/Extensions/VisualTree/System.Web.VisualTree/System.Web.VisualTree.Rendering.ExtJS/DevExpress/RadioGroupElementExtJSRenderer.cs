﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(RadioGroupElement), "PanelElementExtJSRenderer", "radiogroup", "Ext.form.RadioGroup")]
    public class RadioGroupElementExtJSRenderer : RadioGroupElementExtJSRendererBase
    {
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

            RadioGroupElement obj = visualElement as RadioGroupElement;
            
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
        }
    }
}
