var menuCmp = VT_GetCmp('<#= WindowElementExtJSRenderer.ReviseContextMenuStripID(element) #>');
if (menuCmp != null) {
    cmp.el.addListener('contextmenu', function (e) {
    e.stopEvent();
    menuCmp.on('render', function (menu) {
        menu.getEl().on('contextmenu', function (e) {
            e.stopEvent();
        });
    });
    menuCmp.enable();
    menuCmp.showBy(cmp, "tl?", [e.getX() - cmp.getX(), e.getY() - cmp.getY()]);
});
}