try {
    var controlId = <#= data #>;
    var control = VT_GetCmp(controlId);

    if(control != null){
        control.focus();
    }
} catch (e) {
    console.log("an error has occurred in SetActiveControl.js : " + e.message);
}