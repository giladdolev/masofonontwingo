using System.Drawing;
using System.Globalization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Common;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Text;
using System.IO;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS;


namespace System.Web.VisualTree
{

	public class ContextMenuStripExtJSRendererBase : ControlElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.menu.Menu";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "menu";
			}
        }


	}
}