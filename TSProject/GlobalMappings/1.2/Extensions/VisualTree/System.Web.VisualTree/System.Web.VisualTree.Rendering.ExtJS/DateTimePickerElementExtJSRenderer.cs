﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DateTimePickerElement), "ControlElementExtJSRenderer", "datefield", "Ext.form.field.Date")]
    [ExtJSPropertyDescription("Format", PropertyInitializeCode = "format =<#= DateTimePickerUtils.ParseDateTimePickerFormat(element) #>, submitFormat=<#= DateTimePickerUtils.ParseDateTimePickerFormat(element) #>", PropertyChangeCode = "this.format='<#= DateTimePickerUtils.ParseDateTimePickerFormat(element) #>'; this.setValue(this.getValue())")]
    [ExtJSPropertyDescription("SpinInterval", PropertyInitializeCode = "spinInterval = <#= element.SpinInterval.ToString() #>", PropertyChangeCode = "this.spinInterval='<#= element.SpinInterval.ToString() #>';", ClientNotDefaultCode = "element.SpinInterval != DateInterval.Day")]
    [ExtJSPropertyDescription("ShowUpDown", PropertyInitializeCode = "showSpinner=<#= JavaScriptCode.GetBool(element.ShowUpDown)#>", PropertyChangeCode = "if(<#= JavaScriptCode.GetBool(element.ShowUpDown)#>){this.switchTriggers('spinner');}else{this.switchTriggers('picker');}", ClientNotDefaultCode = "element.ShowUpDown != false")]
    [ExtJSPropertyDescription("Editable", PropertyInitializeCode = "editable =<#= element.Editable #>", ClientNotDefaultCode="!element.Editable", PropertyChangeCode = "this.setEditable(<#= JavaScriptCode.GetBool(element.Editable) #>)")]
    [ExtJSPropertyDescription("Value", PropertyInitializeCode = "value =  <#=DateTimePickerUtils.ParseDate(element) #>", PropertyChangeCode = "this.setValue('<#=DateTimePickerUtils.ParseDate(element)  #>');")]
    [ExtJSEventDescription("ValueChanged", ClientEventName = "change", ClientEventHandlerParameterNames = "  view, newValue, oldValue, eOpts", ClientHandlerCallbackData = " value = view.rawValue", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.value)")]
    public class DateTimePickerElementExtJSRenderer : DateTimePickerElementExtJSRendererBase
    {

        /// <summary>
        /// Revises the XTYPE property value.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <returns></returns>
        protected override string ReviseXTypePropertyValue(VisualElement visualElement, string xtype)
        {
            // Get dateTimePicker .
            DateTimePickerElement dateTimePicker = visualElement as DateTimePickerElement;

            // If there is a valid dateTimePicker
            if (dateTimePicker != null)
            {
                // If there is text
                if (dateTimePicker.ShowUpDown == true)
                {
                    // Return title bar XTYPE
                    return "vtdate";
                }
            }

            return xtype;
        }
    }
}
