﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;
using Newtonsoft.Json;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewBand), "VisualElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSPropertyDescription("Height", PropertyInitializeCode = "height = <#= element.PixelHeight #>")]
    [ExtJSPropertyDescription("CssClass", PropertyInitializeCode = "cls = <#= element.CssClass #>")]
    [ExtJSPropertyDescription("BackColor", "", PropertyInitializeCode = "fieldStyle.background-color = <#= ColorTranslator.ToHtml(element.BackColor) #>", PropertyChangeCode = "this.setFieldStyle('background-color:<#= ColorTranslator.ToHtml(element.BackColor) #>');")]
    [ExtJSPropertyDescription("ForeColor", PropertyInitializeCode = "fieldStyle.color = <#= ColorTranslator.ToHtml(element.ForeColor) #>", PropertyChangeCode = "this.setFieldStyle('color:<#= ColorTranslator.ToHtml(element.ForeColor) #>');")]
    public class DataViewBandExtJSRender : DataViewBandExtJSRenderBase
    {


        /// <summary>
        /// Renders the identifier property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderItemIdProperty(VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);

            // Get the band
            DataViewBand band = visualElement as DataViewBand;

            // If there is a valid band
            if (band != null)
            {
                // Render items
                RenderPlainItemsProperty(renderingContext, band.Fields, jsonWriter);
            }
        }

        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Render id property
            base.RenderProperties(renderingContext, visualElement, jsonWriter);
            // Write style property
            RenderBodyStyleProperty(renderingContext, visualElement, jsonWriter);
        }
    }
}
