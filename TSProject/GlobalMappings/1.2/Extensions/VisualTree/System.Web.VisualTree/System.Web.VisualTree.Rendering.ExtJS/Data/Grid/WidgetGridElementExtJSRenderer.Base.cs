using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

    public class WidgetGridElementExtJSRendererBase : ControlElementExtJSRenderer
    {
        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "VT.ux.VTWidgetGrid";
            }
        }


        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "VTWidgetGrid";
            }
        }


        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
            // Call the base implementation
            base.RenderEvents(context, visualElement, writer);
            // Renders the SelectionChanged event attach code.
            RenderAttachSelectionChangedEvent(context, visualElement, writer);
            // Renders the CellMouseDown event attach code.
            RenderAttachCellMouseDownEvent(context, visualElement, writer);
            // Renders the CellkeyDown event attach code.
            RenderAttachCellkeyDownEvent(context, visualElement, writer);
            // Renders the Afterrender event attach code.
            RenderAttachAfterrenderEvent(context, visualElement, writer);
            // Renders the WidgetControlEditChanged event attach code.
            RenderAttachWidgetControlEditChangedEvent(context, visualElement, writer);
            // Renders the WidgetControlItemChanged event attach code.
            RenderAttachWidgetControlItemChangedEvent(context, visualElement, writer);
            // Renders the WidgetControlFocusEnter event attach code.
            RenderAttachWidgetControlFocusEnterEvent(context, visualElement, writer);
            // Renders the WidgetControlDoubleClick event attach code.
            RenderAttachWidgetControlDoubleClickEvent(context, visualElement, writer);
            // Renders the WidgetControlClick event attach code.
            RenderAttachWidgetControlClickEvent(context, visualElement, writer);
            // Renders the WidgetControlSpecialKey event attach code.
            RenderAttachWidgetControlSpecialKeyEvent(context, visualElement, writer);
        }


        /// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
            // Choose event name
            switch (serverEventName)
            {
                case "SelectionChanged":
                    return this.ExtractSelectionChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "CellMouseDown":
                    return this.ExtractCellMouseDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "CellkeyDown":
                    return this.ExtractCellkeyDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "Afterrender":
                    return this.ExtractAfterrenderCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "WidgetControlEditChanged":
                    return this.ExtractWidgetControlEditChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "WidgetControlItemChanged":
                    return this.ExtractWidgetControlItemChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "WidgetControlFocusEnter":
                    return this.ExtractWidgetControlFocusEnterCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "WidgetControlDoubleClick":
                    return this.ExtractWidgetControlDoubleClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "WidgetControlClick":
                    return this.ExtractWidgetControlClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                case "WidgetControlSpecialKey":
                    return this.ExtractWidgetControlSpecialKeyCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
                    // Call the base implementation
                    return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }
        }


        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            // Choose property
            switch (property)
            {
                case "WidgetTemplate":
                    this.RenderWidgetTemplatePropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetTemplateModel":
                    this.RenderWidgetTemplateModelPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetTemplateWidth":
                    this.RenderWidgetTemplateWidthPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetTemplateItemsCount":
                    this.RenderWidgetTemplateItemsCountPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetTemplateDataMembers":
                    this.RenderWidgetTemplateDataMembersPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetTemplateDataAttachMethod":
                    this.RenderWidgetTemplateDataAttachMethodPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            this.RenderWidgetTemplateProperty(renderingContext, visualElement, jsonWriter);
            this.RenderWidgetTemplateModelProperty(renderingContext, visualElement, jsonWriter);
            this.RenderWidgetTemplateWidthProperty(renderingContext, visualElement, jsonWriter);
            this.RenderWidgetTemplateItemsCountProperty(renderingContext, visualElement, jsonWriter);
            this.RenderWidgetTemplateDataMembersProperty(renderingContext, visualElement, jsonWriter);
            this.RenderWidgetTemplateDataAttachMethodProperty(renderingContext, visualElement, jsonWriter);
        }


        /// <summary>
        /// Renders the SelectionChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderSelectionChangedEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseSelectionChangedEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "selection", "eOpts" },
                                    () => {
                                        // Create the server callback code.SelectionChanged

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "SelectionChanged", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rows : VT_GetSelectedRowIndeces(view,selection), cells : VT_GetSelectedCellsIndeces(selection)}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("selectionchange", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of SelectionChanged event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseSelectionChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the SelectionChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element)
        {
            return (element.HasSelectionChangedListeners);
        }


        /// <summary>
        /// Extracts the SelectionChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractSelectionChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the SelectionChanged event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderSelectionChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

                // Revise the event behavior
                eventBehaviorType = this.ReviseSelectionChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "selection", "eOpts" },
                            () => {
                                // Create the server callback code.SelectionChanged

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "SelectionChanged", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rows : VT_GetSelectedRowIndeces(view,selection), cells : VT_GetSelectedCellsIndeces(selection)}}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('selectionchange', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the SelectionChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderSelectionChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "selectionchange");
            }
        }

        /// <summary>
        /// Renders the CellMouseDown event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderCellMouseDownEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseCellMouseDownEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts" },
                                    () => {
                                        // Create the server callback code.CellMouseDown

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellMouseDown", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{cellIndex : VT_GetActualCellIndex(view,cellIndex), rowIndex : rowIndex , X : e.getX(), Y : e.getY(), button : e.button , delta : e.getWheelDelta() ,ctrlKey : e.ctrlKey, shiftKey : e.shiftKey}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("cellmousedown", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of CellMouseDown event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellMouseDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the CellMouseDown event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element)
        {
            return true;
        }


        /// <summary>
        /// Extracts the CellMouseDown event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractCellMouseDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellMouseEventArgs(args.Value<System.Int32>("cellIndex"), args.Value<System.Int32>("rowIndex"), args.Value<System.Int32>("X"), args.Value<System.Int32>("Y"), new MouseEventArgs(GridElementExtJSRenderer.ReviseMouseButton(args.Value<System.Int32>("button")), 0, 0, 0, args.Value<System.Int32>("delta")), args.Value<System.Boolean>("ctrlKey"), args.Value<System.Boolean>("shiftKey"));
        }


        /// <summary>
        /// Renders the CellMouseDown event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderCellMouseDownEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellMouseDownEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts" },
                            () => {
                                // Create the server callback code.CellMouseDown

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellMouseDown", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{cellIndex : VT_GetActualCellIndex(view,cellIndex), rowIndex : rowIndex , X : e.getX(), Y : e.getY(), button : e.button , delta : e.getWheelDelta() ,ctrlKey : e.ctrlKey, shiftKey : e.shiftKey}}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('cellmousedown', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the CellMouseDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderCellMouseDownEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "cellmousedown");
            }
        }

        /// <summary>
        /// Renders the CellkeyDown event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellkeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderCellkeyDownEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseCellkeyDownEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts" },
                                    () => {
                                        // Create the server callback code.CellkeyDown

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellkeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey(),CellIndex : cellIndex,RowIndex : rowIndex ,dataMember : this.widgetDataMember}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("cellkeydown", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of CellkeyDown event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellkeyDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the CellkeyDown event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellkeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element)
        {
            return (element.HasCellkeyDownListeners);
        }


        /// <summary>
        /// Extracts the CellkeyDown event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractCellkeyDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridElementCellEventArgs((Keys)args.Value<System.Int32>("key"), args.Value<System.Int32>("CellIndex"), args.Value<System.Int32>("RowIndex"), args.Value<System.String>("dataMember"));
        }


        /// <summary>
        /// Renders the CellkeyDown event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellkeyDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderCellkeyDownEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellkeyDownEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts" },
                            () => {
                                // Create the server callback code.CellkeyDown

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellkeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey(),CellIndex : cellIndex,RowIndex : rowIndex ,dataMember : this.widgetDataMember}}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('cellkeydown', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the CellkeyDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellkeyDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderCellkeyDownEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "cellkeydown");
            }
        }






        /// <summary>
        /// Renders the Afterrender event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderAfterrenderEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Queued;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseAfterrenderEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "grid" },
                                    () => {
                                        javaScriptBuilder.AppendFormattedRaw(@"VT_GridAfterRender(this, '{0}')", WindowElementExtJSRenderer.ReviseContextMenuStripID(element));
                                        javaScriptBuilder.AppendSemicolon();

                                        // Create the server callback code.Afterrender

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Afterrender", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("afterrender", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of Afterrender event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAfterrenderEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the Afterrender event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return true;
        }


        /// <summary>
        /// Extracts the Afterrender event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractAfterrenderCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the Afterrender event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Afterrender event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderAfterrenderEvent(context, element))
            {

            }
        }

        /// <summary>
        /// Renders the WidgetControlEditChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlEditChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderWidgetControlEditChangedEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseWidgetControlEditChangedEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "obj", "value", "dataIndex" },
                                    () => {
                                        // Create the server callback code.WidgetControlEditChanged

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlEditChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value, dataindex : dataIndex }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("widgetControlEditChanged", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of WidgetControlEditChanged event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlEditChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the WidgetControlEditChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlEditChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (element.HasWidgetControlEditChangedListeners);
        }


        /// <summary>
        /// Extracts the WidgetControlEditChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractWidgetControlEditChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellEventArgs(args.Value<System.String>("newValue"), args.Value<System.String>("dataindex"));
        }


        /// <summary>
        /// Renders the WidgetControlEditChanged event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlEditChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlEditChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseWidgetControlEditChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "obj", "value", "dataIndex" },
                            () => {
                                // Create the server callback code.WidgetControlEditChanged

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "WidgetControlEditChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value, dataindex : dataIndex }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('widgetControlEditChanged', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the WidgetControlEditChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlEditChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlEditChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "widgetControlEditChanged");
            }
        }

        /// <summary>
        /// Renders the WidgetControlItemChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlItemChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderWidgetControlItemChangedEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseWidgetControlItemChangedEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "obj", "value", "dataIndex" },
                                    () => {
                                        // Create the server callback code.WidgetControlItemChanged

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlItemChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowIndex: obj ,newValue : value, dataindex : dataIndex }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("widgetControlItemChanged", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of WidgetControlItemChanged event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlItemChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the WidgetControlItemChanged event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlItemChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (element.HasWidgetControlItemChangedListeners);
        }


        /// <summary>
        /// Extracts the WidgetControlItemChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractWidgetControlItemChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellEventArgs(args.Value<System.String>("newValue"), args.Value<System.String>("dataindex"), args.Value<object>("rowIndex"));
        }


        /// <summary>
        /// Renders the WidgetControlItemChanged event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlItemChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlItemChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseWidgetControlItemChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "obj", "value", "dataIndex" },
                            () => {
                                // Create the server callback code.WidgetControlItemChanged

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "WidgetControlItemChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowIndex:obj , newValue : value, dataindex : dataIndex }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('widgetControlItemChanged', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the WidgetControlItemChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlItemChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlItemChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "widgetControlItemChanged");
            }
        }

        /// <summary>
        /// Renders the WidgetControlFocusEnter event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlFocusEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderWidgetControlFocusEnterEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseWidgetControlFocusEnterEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "obj", "value", "dataIndex" },
                                    () => {
                                        // Create the server callback code.WidgetControlFocusEnter

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlFocusEnter", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowIndex: obj ,newValue : value, dataindex : dataIndex }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("widgetControlFocusEnter", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of WidgetControlFocusEnter event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlFocusEnterEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the WidgetControlFocusEnter event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlFocusEnterEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (element.HasWidgetControlFocusEnterListeners);
        }


        /// <summary>
        /// Extracts the WidgetControlFocusEnter event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractWidgetControlFocusEnterCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellEventArgs(args.Value<System.String>("newValue"), args.Value<System.String>("dataindex"), args.Value<object>("rowIndex"));
        }


        /// <summary>
        /// Renders the WidgetControlFocusEnter event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlFocusEnterAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlFocusEnterEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseWidgetControlFocusEnterEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "obj", "value", "dataIndex" },
                            () => {
                                // Create the server callback code.WidgetControlFocusEnter

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "WidgetControlFocusEnter", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowIndex:obj ,newValue : value, dataindex : dataIndex }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('widgetControlFocusEnter', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the WidgetControlFocusEnter event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlFocusEnterRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlFocusEnterEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "widgetControlFocusEnter");
            }
        }

        /// <summary>
        /// Renders the WidgetControlDoubleClick event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderWidgetControlDoubleClickEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseWidgetControlDoubleClickEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "obj", "value", "dataIndex" },
                                    () => {
                                        // Create the server callback code.WidgetControlDoubleClick

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value, dataindex : dataIndex }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("widgetControlDoubleClick", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of WidgetControlDoubleClick event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlDoubleClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the WidgetControlDoubleClick event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (element.HasWidgetControlDoubleClickListeners);
        }


        /// <summary>
        /// Extracts the WidgetControlDoubleClick event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractWidgetControlDoubleClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellEventArgs(args.Value<System.String>("newValue"), args.Value<System.String>("dataindex"));
        }


        /// <summary>
        /// Renders the WidgetControlDoubleClick event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlDoubleClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlDoubleClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseWidgetControlDoubleClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "obj", "value", "dataIndex" },
                            () => {
                                // Create the server callback code.WidgetControlDoubleClick

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "WidgetControlDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value, dataindex : dataIndex }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('widgetControlDoubleClick', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the WidgetControlDoubleClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlDoubleClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlDoubleClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "widgetControlDoubleClick");
            }
        }

        /// <summary>
        /// Renders the WidgetControlClick event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachWidgetControlClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderWidgetControlClickEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseWidgetControlClickEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { "view", "obj", "value", "dataIndex" },
                                    () => {
                                        // Create the server callback code.WidgetControlClick

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value, dataindex : dataIndex }}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("widgetControlClick", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of WidgetControlClick event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the WidgetControlClick event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderWidgetControlClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (element.HasWidgetControlClickListeners);
        }


        /// <summary>
        /// Extracts the WidgetControlClick event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractWidgetControlClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridCellEventArgs(args.Value<System.String>("newValue"), args.Value<System.String>("dataindex"));
        }


        /// <summary>
        /// Renders the WidgetControlClick event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseWidgetControlClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { "view", "obj", "value", "dataIndex" },
                            () => {
                                // Create the server callback code.WidgetControlClick

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "WidgetControlClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value, dataindex : dataIndex }}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('widgetControlClick', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the WidgetControlClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderWidgetControlClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "widgetControlClick");
            }
        }

        /// <summary>
        /// Renders the  Add Listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Choose event
            switch (eventName)
            {
                case "SelectionChanged":
                    this.RenderSelectionChangedAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "CellMouseDown":
                    this.RenderCellMouseDownAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "CellkeyDown":
                    this.RenderCellkeyDownAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlEditChanged":
                    this.RenderWidgetControlEditChangedAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlItemChanged":
                    this.RenderWidgetControlItemChangedAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlFocusEnter":
                    this.RenderWidgetControlFocusEnterAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlDoubleClick":
                    this.RenderWidgetControlDoubleClickAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlClick":
                    this.RenderWidgetControlClickAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlSpecialKey":
                    this.RenderWidgetControlSpecialKeyAddListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }


        /// <summary>
        /// Renders the WidgetControlSpecialKey event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWidgetControlSpecialKeyEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderWidgetControlSpecialKeyEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseWidgetControlSpecialKeyEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                        .AppendAnonymousFunction(
                            new string[]{
                    "view",
                    "td",
                    "cellIndex",
                    "record",
                    "tr",
                    "rowIndex",
                    "e",
                    "eOpts"
                        },
                            () => {
                        // Create the server callback code.WidgetControlSpecialKey

                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WidgetControlSpecialKey", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey(),CellIndex : cellIndex,RowIndex : rowIndex ,dataMember : this.widgetDataMember}}"));

                            });
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                        jsonWriter.WriteProperty("widgetControlSpecialKey", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }

        /// <summary>
        /// Revise the event behavior type of WidgetControlSpecialKey event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWidgetControlSpecialKeyEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }

        /// <summary>
        /// Indicates if we should render the WidgetControlSpecialKey event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWidgetControlSpecialKeyEvent(RenderingContext context, System.Web.VisualTree.Elements.WidgetGridElement element)
        {
            return (element.HasWidgetControlSpecialKeyListeners);
        }

        /// <summary>
        /// Extracts the WidgetControlSpecialKey event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected virtual EventArgs ExtractWidgetControlSpecialKeyCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new GridElementCellEventArgs((Keys)args.Value<System.Int32>("key"), args.Value<System.Int32>("CellIndex"), args.Value<System.Int32>("RowIndex"), args.Value<System.String>("dataMember"));
        }

        /// <summary>
        /// Renders the WidgetControlSpecialKey event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlSpecialKeyAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlSpecialKeyEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseWidgetControlSpecialKeyEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                .AppendAnonymousFunction(
                    new string[]{
            "view",
            "td",
            "cellIndex",
            "record",
            "tr",
            "rowIndex",
            "e",
            "eOpts"
                },
                    () => {
                // Create the server callback code.WidgetControlSpecialKey

                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "WidgetControlSpecialKey", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey(),CellIndex : cellIndex,RowIndex : rowIndex ,dataMember : this.widgetDataMember}}"));
                    });
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('widgetControlSpecialKey', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));

            }
        }

        /// <summary>
        /// Renders the WidgetControlSpecialKey event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetControlSpecialKeyRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderWidgetControlSpecialKeyEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "widgetControlSpecialKey");
            }
        }


        /// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Choose event
            switch (eventName)
            {
                case "SelectionChanged":
                    this.RenderSelectionChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "CellMouseDown":
                    this.RenderCellMouseDownRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "CellkeyDown":
                    this.RenderCellkeyDownRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlEditChanged":
                    this.RenderWidgetControlEditChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlItemChanged":
                    this.RenderWidgetControlItemChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlFocusEnter":
                    this.RenderWidgetControlFocusEnterRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlDoubleClick":
                    this.RenderWidgetControlDoubleClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlClick":
                    this.RenderWidgetControlClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "WidgetControlSpecialKey":
                    this.RenderWidgetControlSpecialKeyRemoveListener(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
        /// <summary>
        /// Renders the WidgetTemplate property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWidgetTemplateProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the WidgetTemplate property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetTemplatePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the WidgetTemplateModel property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWidgetTemplateModelProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the WidgetTemplateModel property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetTemplateModelPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the WidgetTemplateWidth property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWidgetTemplateWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the WidgetTemplateWidth property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetTemplateWidthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the WidgetTemplateItemsCount property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWidgetTemplateItemsCountProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the WidgetTemplateItemsCount property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetTemplateItemsCountPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the WidgetTemplateDataMembers property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWidgetTemplateDataMembersProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the WidgetTemplateDataMembers property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetTemplateDataMembersPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the WidgetTemplateDataAttachMethod property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWidgetTemplateDataAttachMethodProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the WidgetTemplateDataAttachMethod property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWidgetTemplateDataAttachMethodPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Extract the method callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
            // Choose method
            switch (methodName)
            {
                case "SetFocus":
                    return this.ExtractSetFocusMethodCallbackData(context, eventObject);
                case "SetEnabled":
                    return this.ExtractSetEnabledMethodCallbackData(context, eventObject);
                case "SetBackColor":
                    return this.ExtractSetBackColorMethodCallbackData(context, eventObject);
                case "RemoveFromViewModelContrlos":
                    return this.ExtractRemoveFromViewModelContrlosMethodCallbackData(context, eventObject);
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Choose method
            switch (methodName)
            {
                case "SetFocus":
                    this.RenderSetFocusMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                case "SetEnabled":
                    this.RenderSetEnabledMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                case "SetBackColor":
                    this.RenderSetBackColorMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                case "RemoveFromViewModelContrlos":
                    this.RenderRemoveFromViewModelContrlosMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the SetFocus method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetFocusMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
                    builder.AppendFormattedRawValue(@"VT_widgetgridfocuscontrol({0},{1},{2},{3});", new JavaScriptCode(componentVariableName), data.SelectedRowIndex, data.SelectedColumnIndex, data.DataMember);
                }
            }

        }


        /// <summary>
        /// Gets the method SetFocus callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetFocusMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


        /// Renders the SetEnabled method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetEnabledMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
                    builder.AppendFormattedRawValue(@"VT_widgetgridenablecontrol({0},{1},{2},{3},{4});", new JavaScriptCode(componentVariableName), data.SelectedRowIndex, data.SelectedColumnIndex, data.DataMember, data.Enabled);
                }
            }

        }


        /// <summary>
        /// Gets the method SetEnabled callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetEnabledMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


        /// Renders the SetBackColor method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetBackColorMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
                    builder.AppendFormattedRawValue(@"VT_widgetgridbackcolorcontrol({0},{1},{2},{3},{4});", new JavaScriptCode(componentVariableName), data.SelectedRowIndex, data.SelectedColumnIndex, data.DataMember, data.BackColor.Name);
                }
            }

        }


        /// <summary>
        /// Gets the method SetBackColor callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetBackColorMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


        /// Renders the RemoveFromViewModelContrlos method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderRemoveFromViewModelContrlosMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.WidgetGridElement element = visualElement as System.Web.VisualTree.Elements.WidgetGridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
                    builder.AppendFormattedRawValue(@"{0}.removeFromViewModelContrlos({1});", new JavaScriptCode(componentVariableName), data.SelectedRowIndex);
                }
            }

        }


        /// <summary>
        /// Gets the method RemoveFromViewModelContrlos callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractRemoveFromViewModelContrlosMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }


    }
}