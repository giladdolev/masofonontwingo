﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(WidgetColumnMultiComboBoxElement), "GridMultiComboBoxElementExtJSRenderer", "", "")]
    [ExtJSEventDescription("WidgetControlEditChanged", ClientEventName = "change", ClientEventHandlerParameterNames = "view,  newValue,oldValue, eOpts", ClientCode = "<# resource: WidgetControlFireEditChangedEvent.js #>")]
   
    public class WidgetColumnMultiComboBoxElementExtJSRenderer : WidgetColumnMultiComboBoxElementExtJSRendererBase
    {
       
    }
}