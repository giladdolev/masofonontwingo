﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridComboBoxColumn), "GridColumnExtJSRenderer", "gridcolumn", "Ext.grid.column.Column")]
    [ExtJSPropertyDescription("ReadOnly", PropertyChangeCode = " this.getEditor().setReadOnly( <#= JavaScriptCode.GetBool(element.ReadOnly) #>);")]
    public class GridComboBoxColumnExtJSRenderer : GridComboBoxColumnExtJSRendererBase
    {

        /// <summary>
        /// Renders the ComboBox store.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="comboListItems">The combo list items.</param>
        internal static void RenderComboBoxStore(Newtonsoft.Json.JsonTextWriter jsonWriter, string comboListItems)
        {
            if (!String.IsNullOrEmpty(comboListItems))
            {
                // Render store object property
                jsonWriter.WritePropertyName("store");
                RenderComboBoxStoreBody(jsonWriter, comboListItems);
            }
        }

        /// <summary>
        /// Renders the ComboBox store body.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="comboListItems">The combo list items.</param>
        internal static void RenderComboBoxStoreBody(Newtonsoft.Json.JsonTextWriter jsonWriter, string comboListItems)
        {
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
            {
                jsonWriter.WritePropertyName("fields");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    jsonWriter.WriteRaw("'comboName', 'comboValue'");
                }
                jsonWriter.WritePropertyName("data");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    if (!string.IsNullOrEmpty(comboListItems))
                    {
                        string[] comboItems = comboListItems.Split(',');
                        foreach (string comboItem in comboItems)
                        {
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                string[] itemParts = comboItem.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                if (itemParts.Length > 0)
                                {
                                    string itemValue = itemParts.Length > 1 ? itemParts[1] : itemParts[0];
                                    jsonWriter.WriteProperty("comboName", itemParts[0]);
                                    jsonWriter.WriteProperty("comboValue", itemValue);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the type of the editor x.
        /// </summary>
        /// <value>
        /// The type of the editor x.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return "combo";
            }
        }

        /// <summary>
        /// Renders the content of the editor.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <param name="gridColumn">The grid column.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderEditorContent(GridElement gridElement, GridColumn gridColumn, JsonTextWriter jsonWriter)
        {
            base.RenderEditorContent(gridElement, gridColumn, jsonWriter);
            StringBuilder stringRenderer = new StringBuilder();
            if (gridElement.ComboBoxList != null && gridElement.ComboBoxList.Count > 0)
            {
                if (gridElement.ComboBoxList.ContainsKey(gridColumn.ID))
                {
                    RenderComboBoxStore(jsonWriter, gridElement.ComboBoxList[gridColumn.ID]);
                }
            }
            jsonWriter.WriteProperty("displayField", "comboName");
            jsonWriter.WriteProperty("valueField", "comboValue");

            // Adding 'ComboCellSelectedIndexChanged' event
            if (gridElement != null)
            {
                if (gridElement.HasComboCellSelectedIndexChangedListeners)
                {
                    stringRenderer.Append(@" {change: function (view,newValue,oldValue,eOpts) {");
                    stringRenderer.AppendFormat("VT_raiseEvent('{0}', 'ComboCellSelectedIndexChanged' ,{{cellIndex : VT_GetWidgetColumnIndex(VT_GetCmp('{1}').columns,'{2}'),rowIndex:VT_GetWidgetRowIndex(view) , value : newValue, index: VT_GetComboboxSelectedIndex(view,view.findRecord(view.valueField || view.displayField, newValue))}});}}}}", VisualElement.GetElementId(gridElement), VisualElement.GetElementId(gridElement), gridColumn.DataMember);
                    JavaScriptCode javaSCriptCode = new JavaScriptCode(stringRenderer.ToString());
                    jsonWriter.WriteProperty("listeners", javaSCriptCode);
                }
            }


            RenderListenersProperty(gridColumn as GridComboBoxColumn, jsonWriter);
            // Write AutoComplete properties . 
            RenderAutoCompleteProperty(gridColumn as GridComboBoxColumn, jsonWriter);

        }

        /// <summary>
        /// Render AutoComplete properties 
        /// </summary>
        /// <param name="gridColumn">The grid column.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderAutoCompleteProperty(GridComboBoxColumn gridColumn, JsonTextWriter jsonWriter)
        {
            if (gridColumn != null)
            {
                if (gridColumn.AutoCompleteMode != AutoCompleteMode.None)
                {
                    jsonWriter.WriteProperty("minChars", "1");
                    jsonWriter.WriteProperty("typeAhead", GridComboBoxColumnExtJSRenderer.ReviseTypeAheadProperty(gridColumn as GridComboBoxColumn));
                    jsonWriter.WriteProperty("queryMode", "local");
                    if (gridColumn.ForceSelection == true)
                    {
                        jsonWriter.WriteProperty("forceSelection", JavaScriptCode.GetBool(gridColumn.ForceSelection));
                    }
                }
            }
        }

        /// <summary>
        /// Render Listeners property 
        /// </summary>
        /// <param name="gridColumn">The grid column.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderListenersProperty(GridComboBoxColumn gridColumn, JsonTextWriter jsonWriter)
        {
            if (gridColumn != null)
            {
                jsonWriter.WritePropertyName("listeners");
                jsonWriter.WriteStartObject();
                if (gridColumn.ExpandOnFocus)
                {
                    jsonWriter.WritePropertyWithRawValue("focus", "function(combo){combo.expand();}");
                }
                jsonWriter.WriteEndObject();
            }
        }

        /// <summary>
        /// Revises the typeAhead property.
        /// </summary>
        /// <param name="GridComboBoxColumn">The GridComboBoxColumn instance being rendered.</param>
        /// <returns>auto complete mode .</returns>
        internal static bool ReviseTypeAheadProperty(GridComboBoxColumn columnElement)
        {
            // Default ExtJS value according to Sencha Docs
            bool typeAhead = false;

            if (columnElement != null)
            {
                if (columnElement.AutoCompleteMode == AutoCompleteMode.Append || columnElement.AutoCompleteMode == AutoCompleteMode.SuggestAppend)
                {
                    typeAhead = true;
                }
            }
            return typeAhead;
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
            StringBuilder stringRendere = new StringBuilder();
            stringRendere.Append(@"function (value, metaData, record, rowIndex, colIndex, store, view, comboName) {
var combo = metaData.column.getEditor();
							if (value && combo && combo.store && combo.displayField) {
								var index = combo.store.findExact(combo.valueField, value);
								if (index >= 0) {
									return combo.store.getAt(index).get(combo.displayField);
								}
							}
							return value;            
            }");
            JavaScriptCode js = new JavaScriptCode(stringRendere.ToString());

            jsonWriter.WriteProperty("renderer", js);
        }
    }
}
