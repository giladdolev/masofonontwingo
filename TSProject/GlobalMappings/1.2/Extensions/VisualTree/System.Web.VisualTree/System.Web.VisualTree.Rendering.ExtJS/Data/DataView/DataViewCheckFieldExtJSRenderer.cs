﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewCheckField), "DataViewFieldExtJSRenderer", "checkcolumn", "Ext.grid.column.Check")]
    public class DataViewCheckFieldExtJSRenderer : DataViewCheckFieldExtJSRendererBase
    {
        /// <summary>
        /// Gets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return "checkbox";
            }
        }
    }
}
