﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    public class GridEditorColumnBaseExtJSRenderer : ControlElementExtJSRenderer
    {

        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.grid.column.Column";
            }
        }


        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "gridcolumn";
            }
        }

        /// <summary>
        /// Gets the type of the editor x.
        /// </summary>
        /// <value>
        /// The type of the editor x.
        /// </value>
        public virtual string EditorXType
        {
            get
            {
                return "textfield";
            }
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(VisualTree.Common.RenderingContext renderingContext, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);

            RenderEditorProperty(renderingContext, visualElement, jsonWriter);
        }
        /// <summary>
        /// Renders the editor property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderEditorProperty(VisualTree.Common.RenderingContext renderingContext, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            GridColumn gridColumn = visualElement as GridColumn;
            GridElement gridElement = gridColumn.Parent as GridElement;

            if (!(gridColumn is WidgetColumn))
            {
                if (gridElement != null && gridColumn != null)
                {
                    if (!gridElement.ReadOnly)
                    {
                        jsonWriter.WritePropertyName("editor");
                        jsonWriter.WriteStartObject();
                        string format = gridColumn.Format;
                        GridDateTimePickerColumn grdDate = gridColumn as GridDateTimePickerColumn;
                        if (!string.IsNullOrEmpty(format))
                        {
                            if (grdDate != null)
                            {
                                jsonWriter.WriteProperty("xtype", "datefield");
                            }
                            else
                            {
                                jsonWriter.WriteProperty("xtype", "formattedtext");
                            }
                        }
                        else
                        {
                            jsonWriter.WriteProperty("xtype", this.EditorXType);
                        }

                        RenderEditorContent(gridElement, gridColumn, jsonWriter);

                        if (gridElement.FrozenCols > 0 && gridColumn.Index < gridElement.FrozenCols)
                        {
                            jsonWriter.WriteProperty("locked", true);
                        }
                        if (gridColumn.NumberField == true && this.EditorXType.Equals("textfield"))
                        {
                            jsonWriter.WriteProperty("inputType", "number");
                        }
                        if (!string.IsNullOrEmpty(format))
                        {
                            if (grdDate != null)
                            {
                                jsonWriter.WriteProperty("format", DateTimePickerUtils.ConvertToClientCustomFormat(format));

                            }
                            else
                            {
                                jsonWriter.WritePropertyWithRawValue("formatter", format);

                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                }
            }
        }



        /// <summary>
        /// Renders the content of the editor.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <param name="gridColumn">The grid column.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderEditorContent(GridElement gridElement, GridColumn gridColumn, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            if (gridColumn != null)
            {
                if (gridColumn.ReadOnly != false)
                {
                    jsonWriter.WriteProperty("readOnly", gridColumn.ReadOnly);
                }
            }
        }
    }
}
