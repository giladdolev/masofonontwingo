﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewImageField), "DataViewFieldExtJSRenderer", "actioncolumn", "Ext.grid.column.Action")]
    [ExtJSPropertyDescription("Image", ClientConfigName = "items")]
    public class DataViewImageFieldExtJSRenderer : DataViewImageFieldExtJSRendererBase
    {
        protected override void RenderImageProperty(ExtJSRenderingContext context, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            DataViewImageField element = visualElement as DataViewImageField;
            if (element != null)
            {
                jsonWriter.WritePropertyName("items");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("icon", element.Image);
                    }
                }
            }
        }
        
    }
}
