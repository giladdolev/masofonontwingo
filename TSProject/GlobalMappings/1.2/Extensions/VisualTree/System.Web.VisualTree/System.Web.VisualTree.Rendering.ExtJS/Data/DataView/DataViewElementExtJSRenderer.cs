﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewElement), "ControlElementExtJSRenderer", "vtdataview", "Ext.ux.VTDataView")]
    public class DataViewElementExtJSRenderer : DataViewElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Render data source
            this.RenderDataSourceProperty(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the Bands property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBandsProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the data view
            DataViewElement dataViewElement = visualElement as DataViewElement;

            // If there is a valid data view
            if(dataViewElement != null)
            {
                // Get the bands
                DataViewBandCollection bands = dataViewElement.Bands;

                // Render bands
                RenderTypedBandsProperty(context, jsonWriter, bands, "headers", DataViewBandType.Header);
                RenderTypedBandsProperty(context, jsonWriter, bands, "details", DataViewBandType.Details, false);
                RenderTypedBandsProperty(context, jsonWriter, bands, "summary", DataViewBandType.Summary);
                RenderTypedBandsProperty(context, jsonWriter, bands, "footers", DataViewBandType.Footer);
            }
        }

        /// <summary>
        /// Renders the typed bands property.
        /// </summary>
        /// <param name="jsonWriter">The JSON writer.</param>
        /// <param name="bands">The bands.</param>
        /// <param name="propertyName">The property name.</param>
        /// <param name="bandType">The band type.</param>
        /// <param name="renderBandList">if set to <c>true</c> render band list.</param>
        private static void RenderTypedBandsProperty(RenderingContext context, JsonTextWriter jsonWriter, DataViewBandCollection bands, string propertyName, DataViewBandType bandType, bool renderBandList = true)
        {
            // First header indicator flag
            bool firstHeader = true;

            // Flag indicating if we need to close array
            bool closeArray = false;

            // Loop all header bands
            foreach (DataViewBand headerBand in bands.Where((band) => band.Type == bandType))
            {
                // Get renderer
                VisualElementExtJSRenderer renderer = VisualTreeManager.GetRenderer(context, headerBand) as VisualElementExtJSRenderer;

                // If there is a valid renderer
                if (renderer != null)
                {
                    // If is the first header
                    if (firstHeader)
                    {
                        // Render property   
                        jsonWriter.WritePropertyName(propertyName);

                        // If we should render list
                        if (renderBandList)
                        {
                            // Write start array
                            jsonWriter.WriteStartArray();

                            closeArray = true;
                        }
                    }

                    // Render the band
                    renderer.Render(context, headerBand, jsonWriter);

                    

                    // Not the first header
                    firstHeader = false;
                }
            }

            // If we should close the array
            if (closeArray)
            {
                // Write end array
                jsonWriter.WriteEndArray();
            }
        }
    }
}
