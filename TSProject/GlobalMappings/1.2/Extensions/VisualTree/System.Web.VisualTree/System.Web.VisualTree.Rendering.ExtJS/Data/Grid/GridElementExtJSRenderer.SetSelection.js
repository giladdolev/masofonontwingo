﻿try{
var store = this && this.getStore && this.getStore(),
    selectedIndex = <#= element.SelectedRowIndex #>;

if(store && (store.isLoading() || store.getCount() <= selectedIndex || store.loadTimer)){
    
    store.on('load', function(){
        if(store.getCount() > selectedIndex){
            this.getSelectionModel().select(selectedIndex);
            this.getView().focusRow(selectedIndex);
        }        

    }, this,  {single: true})
} else {
    this.getSelectionModel().select(selectedIndex);
    this.getView().focusRow(selectedIndex);
}

}catch(err){
    console.log("error :- " + err.message);
}