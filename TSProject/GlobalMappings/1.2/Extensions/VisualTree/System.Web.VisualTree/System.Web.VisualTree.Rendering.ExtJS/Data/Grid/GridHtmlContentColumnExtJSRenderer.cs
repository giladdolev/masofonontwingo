﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridHtmlContentColumn), "GridColumnExtJSRenderer", "grid-column", "VT.ux.VTGridColumn")]
    [ExtJSPropertyDescription("HtmlContent", PropertyInitializeCode = "this.htmlContent= <#=element.HtmlContent#>", PropertyChangeCode = "this.setHtmlContent(<#=element.HtmlContent#>)")]
    [ExtJSPropertyDescription("flex",PropertyInitializeCode = "flex = 1")]
    public class GridHtmlContentColumnExtJSRenderer : GridHtmlContentColumnExtJSRendererBase
    {
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
            GridHtmlContentColumn column = visualElement as GridHtmlContentColumn;
            if (column != null)
            {
                jsonWriter.WriteProperty("htmlContent", column.HtmlContent);
                jsonWriter.WriteProperty("dataMembers", column.DataMember);
                jsonWriter.WriteProperty("text", column.HeaderText);
                jsonWriter.WriteProperty("width", column.Width);
                //jsonWriter.WriteProperty("flex", "1");
            }
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderContentProperties(renderingContext, visualElement, jsonWriter);


            GridHtmlContentColumn column = visualElement as GridHtmlContentColumn;
            if (!string.IsNullOrEmpty(Convert.ToString(column.HtmlContent)))
            {
                StringBuilder stringRendere = new StringBuilder();
                stringRendere.Append("'GetRendere'");
                JavaScriptCode js = new JavaScriptCode(stringRendere.ToString());

                jsonWriter.WriteProperty("renderer", js);
            }
        }
    }
}
