﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ListViewElement), "ControlElementExtJSRenderer", "vtlistview", "Vt.ux.ListView")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "bodyStyle.backgroundColor = <#= System.Drawing.ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != System.Drawing.Color.Empty", PropertyChangeCode = "this.setBodyStyle('backgroundColor','<#= System.Drawing.ColorTranslator.ToHtml(element.BackColor) #>');")]
    [ExtJSPropertyDescription("CheckBoxes", PropertyInitializeCode = "showCheckboxes = <#= element.CheckBoxes #>", PropertyChangeCode = "this.setCheckboxies(<#=JavaScriptCode.GetBool( element.CheckBoxes )#>);")]
    [ExtJSPropertyDescription("columnLines", PropertyInitializeCode = "columnLines = false")]
    [ExtJSPropertyDescription("FullRowSelect", PropertyInitializeCode = "selModel = <#= ListViewElementExtJSRenderer.ReviseSelModel(element) #>", ClientNotDefaultCode = "element.FullRowSelect != false")]
    [ExtJSPropertyDescription("rowLines", PropertyInitializeCode = "rowLines = false")]
    [ExtJSPropertyDescription("View", PropertyInitializeCode = "hideHeaders = <#= JavaScriptCode.GetBool(element.HeadersVisible) #>", PropertyChangeCode = "this.setHideHeaders(<#= JavaScriptCode.GetBool(!element.HeadersVisible) #>)", ClientNotDefaultCode = "element.HeadersVisible != true")]
    [ExtJSPropertyDescription("MultiSelect", PropertyInitializeCode = "multiSelect = <#= element.MultiSelect #>", PropertyChangeCode = "this.setMultiSelect(<#=JavaScriptCode.GetBool( element.MultiSelect )#>);")]

    [ExtJSMethodDescription("SetItemForeColor", MethodCode = "this.setCellForeColor(<#=data.ForeColor#> , <#=data.RowIndex#> , <#=data.ColumnIndex#>);", MethodDataType = typeof(ListViewItemData))]

    [ExtJSEventDescription("DoubleClick", ClientEventName = "celldblclick", ClientEventHandlerParameterNames = " view, td, cellIndex, record, tr, rowIndex, e, eOpts")]
    [ExtJSEventDescription("ItemChecked", ClientEventName = "select", ClientEventHandlerParameterNames = " view, record, index, eOpts", ClientHandlerCallbackData = "value = index", ServerEventArgsCreateCode = "new ItemCheckEventArgs(this.value.ToInt32())")]
    [ExtJSEventDescription("ItemUnchecked", ClientEventName = "deselect", ClientEventHandlerParameterNames = " view, record, index, eOpts", ClientHandlerCallbackData = "value = index", ServerEventArgsCreateCode = "new ItemCheckEventArgs(this.value.ToInt32())")]
    [ExtJSEventDescription("ItemClick", ClientEventName = "itemclick", ClientEventHandlerParameterNames = "view,record,item, index, e, eOpts", ClientHandlerCallbackData = "value = view.store.indexOf(view.getSelectionModel().getSelection()[0])", ServerEventArgsCreateCode = "new ValueChangedArgs<int>(this.value.ToInt32())")]
    [ExtJSEventDescription("MouseEnter", ClientEventName = "itemmouseenter", ClientEventHandlerParameterNames = "view , record , item , index , e , eOpts", ClientCode = "<#resource:HotTrackingMouseEnter.js#>")]
    [ExtJSEventDescription("MouseLeave", ClientEventName = "itemmouseleave", ClientEventHandlerParameterNames = "view , record , item , index , e , eOpts", ClientCode = " <#resource:HotTrackingMouseLeave.js#>")]

    [ExtJSEventDescription("SelectionChanged", ClientEventName = "selectionchange", ClientEventHandlerParameterNames = " view , selected , eOpts", ClientHandlerCallbackData = " valuesArr = VT_GetListViewSelectedIndexes(view,selected)", ServerEventArgsCreateCode = " new ListViewItemSelectionChangedEventArgs(this.valuesArr.Split(',').Select(s => int.Parse(s)).ToArray())")]

    public class ListViewElementExtJSRenderer : ListViewElementExtJSRendererBase
    {


        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Render data source
            this.RenderDataSourceProperty(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <exception cref="System.ArgumentNullException">jsonWriter</exception>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ListViewElement listViewElement = visualElement as ListViewElement;
            if (jsonWriter == null)
            {
                throw new ArgumentNullException("jsonWriter");
            }

            if (listViewElement != null)
            {
                // if the listview is set to checkboxes true, the first column must be editable
                // so we have to add the plugins
                if (listViewElement.CheckBoxes)
                {
                    jsonWriter.WritePropertyName("plugins");
                    jsonWriter.WriteRawValue("[Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 1 })]");
                }
                RenderColumns(listViewElement, jsonWriter);
            }
        }








        /// <summary>
        /// Gets the name of the item identifier.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetItemIdName()
        {
            return "itemId";
        }



        /// <summary>
        /// Renders the columns.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderColumns(ListViewElement listViewElement, JsonTextWriter jsonWriter)
        {
            if (listViewElement.Columns != null && listViewElement.Columns.Count > 0)
            {
                jsonWriter.WritePropertyName("columns");

                RenderColumnsArray(listViewElement, jsonWriter);
            }
        }

        /// <summary>
        /// Renders the columns array.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <returns></returns>
        protected string RenderColumnsArray(ListViewElement listViewElement)
        {

            // Create the json string writer
            Text.StringBuilder stringBuilder = new Text.StringBuilder();

            // Create the json writer
            using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(stringBuilder))
            {
                // Render columns
                RenderColumnsArray(listViewElement, jsonWriter);

                // Flush columns
                jsonWriter.Flush();

            }

            // Return the string
            return stringBuilder.ToString();

        }


        /// <summary>
        /// Renders the columns array.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderColumnsArray(ListViewElement listViewElement, JsonTextWriter jsonWriter)
        {
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
            {
                RenderColumnsInArray(listViewElement, jsonWriter);
            }
        }

        /// <summary>
        /// Renders the columns in array.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderColumnsInArray(ListViewElement listViewElement, JsonTextWriter jsonWriter)
        {
            string[] fields = GetFields(listViewElement);

            // Render image column
            if (HasImageColumn(listViewElement))
            {
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteProperty("header", "");
                    jsonWriter.WriteProperty("dataIndex", "Image");
                    jsonWriter.WriteProperty("xtype", "templatecolumn");

                    // Render image column width
                    ImageList imageList = listViewElement.SmallImageList;
                    if (imageList != null)
                    {
                        int height = imageList.ImageSize.Height;
                        int width = imageList.ImageSize.Width;
                        if (imageList.ImageSize.IsEmpty)
                            width = height = 16;

                        // Add Extjs template column padding. 10px for each side.
                        jsonWriter.WriteProperty("width", width + 20);

                        jsonWriter.WriteProperty("tpl", string.Format(CultureInfo.InvariantCulture, "<img src=\"{{Image}}\"  height=\"{0}\" width=\"{1}\"/>", height, width));
                    }
                }
            }



            // Render all columns
            int columnIndex = 0;
            foreach (ColumnHeader columnHeader in listViewElement.Columns)
            {
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteProperty("header", columnHeader.Text);
                    jsonWriter.WriteProperty("width", columnHeader.Width);
                    jsonWriter.WriteProperty("dataIndex", columnHeader.ClientID);
                    if (listViewElement.MenuDisabled != false)
                    {
                        jsonWriter.WriteProperty("menuDisabled", true);
                    }
                    // if the listview is set to checkboxes true, the first column must be editable
                    if (listViewElement.CheckBoxes && columnIndex == 1)
                    {
                        jsonWriter.WriteProperty("editor", "textfield");
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether list view has image column.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <returns></returns>
        private static bool HasImageColumn(ListViewElement listViewElement)
        {
            ImageList imageList = listViewElement.SmallImageList;
            if (imageList != null && imageList.Images.Count > 0)
            {
                ListViewItemCollection items = listViewElement.Items;
                if (items != null)
                    return items.Any(item => item.ImageIndex >= 0);
            }

            return false;
        }

        /// <summary>
        /// Gets the fields.
        /// </summary>
        /// <param name="listViewElement">The list view element.</param>
        /// <returns></returns>
        protected virtual string[] GetFields(ListViewElement listViewElement)
        {
            ColumnHeaderCollection columnHeaders = listViewElement.Columns;

            if (columnHeaders != null)
                return columnHeaders.Select((column, columnIndex) => string.IsNullOrEmpty(column.ID) ? "col_" + columnIndex : column.ID).ToArray();

            return new string[] { };
        }


        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            if (property == "$refresh")
            {
                RenderRefresh(renderingContext, visualElement, builder, componentVariable);
            }
            else
            {
                base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
            }
        }

        /// <summary>
        /// Renders the refresh.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRefresh(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get the ListViewElement element
            ListViewElement listViewElement = visualElement as ListViewElement;

            // If there is a valid listview element
            if (listViewElement == null)
            {
                return;
            }

            // Render the reconfigure script
            builder.AppendStartInvocation(string.Concat(componentVariable, ".reconfigure"));



            builder.AppendRawValue(RenderColumnsArray(listViewElement));


            builder.AppendEndInvocation(true);


        }

        /// <summary>
        /// Determine whether the selection should include subitems or not
        /// </summary>
        /// <param name="visualElement"></param>
        /// <returns></returns>
        internal static string ReviseSelModel(VisualElement visualElement)
        {
            ListViewElement element = visualElement as ListViewElement;
            string selModel = "cellmodel";
            if (element != null)
            {
                if (element.FullRowSelect)
                    selModel = "rowmodel";
            }
            return selModel;
        }
    }
}
