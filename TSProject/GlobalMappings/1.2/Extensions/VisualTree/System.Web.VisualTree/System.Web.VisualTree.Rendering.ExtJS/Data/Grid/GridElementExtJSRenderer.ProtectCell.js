try{
    var row = <#= data.RowIndex #>;
    var col = <#= data.ColumnIndex #>;
    var protect = <#= data.Protect #>;
    this.getView().getCell(row, this.getColumns()[col]).Protect = protect;
}catch(err)
{
    console.log("error :- grid setProtect "+err.message);
}