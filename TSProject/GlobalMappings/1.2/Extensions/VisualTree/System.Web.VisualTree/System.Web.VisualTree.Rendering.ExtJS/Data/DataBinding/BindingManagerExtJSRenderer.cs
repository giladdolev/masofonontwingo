﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(BindingManager), "VisualElementExtJSRenderer", "storage", "Ext.data.Storage")]
    [ExtJSPropertyDescription("IsAutoSynchronize", ClientConfigName = "autoSync")]
    [ExtJSPropertyDescription("AllowFilter", PropertyInitializeCode = "statefulFilters = false", PropertyChangeCode = "this.setStatefulFilters(true)")]
    [ExtJSPropertyDescription("GroupField", PropertyInitializeCode = "groupField = <#=element.GroupField#>")]

    [ExtJSEventDescription("DataChanged", ClientEventName = "metachange", ClientEventHandlerParameterNames = " view, meta, eOpts")]
    public class BindingManagerExtJSRenderer : BindingManagerExtJSRendererBase
    {

        /// <summary>
        /// Renders the identifier property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderItemIdProperty(VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the x type property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderXTypeProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Store does not need an X type when embedded in configuration.
        }

        /// <summary>
        /// Renders the component variable.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariableName">Name of the component variable.</param>
        /// <param name="visualElement">The visual element.</param>
        protected override void RenderComponentVariable(JavaScriptBuilder builder, string componentVariableName, VisualElement visualElement)
        {
            // Get parent element id
            string parentElementId = (visualElement != null ? VisualElement.GetElementId(visualElement.ParentElement) : null);
            //gilad 

            if (!string.IsNullOrEmpty(parentElementId))
            {
                // Get component 
                builder.AppendVariableWithInvocationInitializer(componentVariableName, "VT_GetCmp", parentElementId);

                // Get component store
                builder.AppendAssignmentStatement(componentVariableName, new JavaScriptCode(string.Concat(componentVariableName, "?", componentVariableName, ".store : null")));
            }
        }

        /// <summary>
        /// The indexer column name
        /// </summary>
        private const string ITEMID_COLUMN_NAME = "id";

        /// <summary>
        /// The identifier escape prefix
        /// </summary>
        private const string IDX_ESCAPE_PREFIX = "_c_";

        /// <summary>
        /// Renders the Columns property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderColumnsProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get binding manager
            BindingManager bindingManager = visualElement as BindingManager;

            // If there is a valid binding manage
            if (bindingManager != null)
            {
                // Write field property
                jsonWriter.WritePropertyName("fields");
                jsonWriter.WriteStartArray();

                // Loop all columns
                foreach (IBindingDataColumn column in bindingManager.Columns)
                {

                    // No need to render record for grid row .
                    if (column is GridRowHeader)
                    {
                        break;
                    }
                    // Write field from column
                    jsonWriter.WriteStartObject();
                    jsonWriter.WriteProperty("name", column.Name);
                    jsonWriter.WriteProperty("type", RenderingUtils.ConvertToScriptType(column.DataTypeName));

                    switch (column.SortType)
                    {
                        case ColumnSortMode.Ascending:
                            jsonWriter.WriteProperty("sortType", "asDate");
                            break;
                        case ColumnSortMode.StringAscending:
                            jsonWriter.WriteProperty("sortType", "asText");
                            break;
                        case ColumnSortMode.NumericAscending:
                            jsonWriter.WriteProperty("sortType", "asFloat");
                            break;
                        default:

                            break;
                    }
                    if (column is GridCheckBoxColumn)
                    {
                        jsonWriter.WriteProperty("type", (column as GridCheckBoxColumn).ThreeState ? "threeStateBool" : RenderingUtils.ConvertToScriptType(column.DataTypeName));
                    }
                    else
                    {
                        jsonWriter.WriteProperty("type", RenderingUtils.ConvertToScriptType(column.DataTypeName));
                    }


                    // If is an index column
                    if (column.IsIndex)
                    {
                        jsonWriter.WriteProperty("critical", true);
                    }


                    jsonWriter.WriteEndObject();
                }

                jsonWriter.WriteEndArray();
            }
        }

        /// <summary>
        /// Renders the Items property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderItemsProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get binding manager element
            BindingManager bindingManager = visualElement as BindingManager;

            // If there is a valid binding manager element
            if (bindingManager != null)
            {
                // If data source is editable or is deferred loading
                if (bindingManager.IsEditable || bindingManager.IsDeferred)
                {
                    // Create proxy definition
                    jsonWriter.WritePropertyName("proxy");
                    jsonWriter.WriteStartObject();
                    jsonWriter.WriteProperty("type", "vt");

                    // Get parent element
                    VisualElement parentElement = bindingManager.ParentElement;

                    // If there is a valid parent element
                    if (parentElement != null)
                    {
                        // Set the server ID
                        jsonWriter.WriteProperty("serverId", VisualElement.GetElementId(parentElement));
                    }

                    jsonWriter.WriteEndObject();
                }

                // If should deferred 
                if (bindingManager.IsDeferred)
                {
                    // Indicate we should load the store
                    jsonWriter.WriteProperty("autoLoad", true);
                }
                else
                {

                    // Write data property
                    jsonWriter.WritePropertyName("data");

                    // Render items
                    this.RenderItemsArray(context, bindingManager, jsonWriter);
                }
            }
        }

        /// <summary>
        /// Renders the update client method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">Name of the component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder.</param>
        protected override void RenderUpdateClientMethod(RenderingContext context, IVisualElementMethodInvoker methodInvoker, VisualElement visualElement, object methodData, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get binding manager
            BindingManager bindingManager = visualElement as BindingManager;
            if (bindingManager != null)
            {
                // Flag indicating if we should refresh
                bool refresh = false;

                // If is a view model binding
                if (bindingManager.IsViewModel)
                {
                    // Render view model client update
                    refresh = RenderUpdateViewModelClientMethod(context, bindingManager, componentVariableName, getJavaScriptBuilder);
                }
                else
                {
                    // Refresh data
                    refresh = true;
                }

                // If we should reload data
                if (refresh)
                {
                    // Get script builder
                    JavaScriptBuilder scriptBuilder = getJavaScriptBuilder(true);

                    // If there is a valid script builder
                    if (scriptBuilder != null)
                    {
                        // Reload the 
                        scriptBuilder.AppendInvocationStatement(string.Concat(componentVariableName, ".reload"));

                        // Clear changes on server
                        bindingManager.ClearChanges();
                    }
                }

                ControlElement ctrl = bindingManager.ParentElement as ControlElement;
                if (ctrl != null)
                {
                    ctrl.RaiseUpdateClient();
                }
            }
        }

        /// <summary>
        /// Renders the update view model client method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="bindingManager">The binding manager.</param>
        /// <param name="componentVariableName">Name of the component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder.</param>
        private bool RenderUpdateViewModelClientMethod(RenderingContext context, BindingManager bindingManager, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            // If there is a valid binding manager
            if (bindingManager != null)
            {
                // Get columns change container
                IVisualElementCollectionChangeActionsContainer columnsChangeActionsContainer = bindingManager.Columns as IVisualElementCollectionChangeActionsContainer;
                if (columnsChangeActionsContainer != null)
                {
                    // If columns changed
                    if (columnsChangeActionsContainer.ChangeActions.Any())
                    {
                        // Resend all data
                        return true;
                    }
                }

                // Get items change container
                IVisualElementCollectionChangeActionsContainer itemsChangeActionsContainer = bindingManager.Items as IVisualElementCollectionChangeActionsContainer;
                if (itemsChangeActionsContainer != null && itemsChangeActionsContainer.ChangeActions.Count() > 0)
                {
                    // Get script builder
                    JavaScriptBuilder scriptBuilder = getJavaScriptBuilder(true);

                    // If there is a valid script builder
                    if (scriptBuilder != null)
                    {
                        // Get data reader
                        IDataReader dataReader = bindingManager.CreateReader(GetUpdatableItems(itemsChangeActionsContainer.ChangeActions));

                        // Loop all change actions
                        foreach (IVisualElementCollectionChangeAction changeAction in itemsChangeActionsContainer.ChangeActions)
                        {
                            switch (changeAction.Type)
                            {
                                case VisualElementCollectionChangeActionType.ClearItems:
                                    scriptBuilder.AppendDebuggerStatement();
                                    scriptBuilder.AppendInvocationStatement(string.Concat(componentVariableName, ".removeAll"));
                                    if (!string.IsNullOrEmpty(changeAction.Msg))
                                    {
                                        scriptBuilder.AppendLoggerStatement("ClearItems: " + changeAction.Msg);
                                    }
                                    break;
                                case VisualElementCollectionChangeActionType.SetItem:
                                    bool SetItem = true;
                                    if (SetItem)//gilad
                                    {
                                        scriptBuilder.AppendMultipleInvocationStatement(componentVariableName, changeAction.Index, RenderItem(bindingManager, dataReader));
                                        scriptBuilder.AppendRawValue(string.Format("{0}.parent.ownerGrid.viewModelContrlos[{1}].setData({0}.getAt({1}).data);", componentVariableName, changeAction.Index));
                                    }
                                    else
                                    {

                                        scriptBuilder.AppendInvocationStatement(string.Concat(componentVariableName, ".remove"), changeAction.Index);
                                        //scriptBuilder.AppendDebuggerStatement();
                                        scriptBuilder.AppendInvocationStatement(string.Concat(componentVariableName, ".insert"), changeAction.Index, RenderItem(bindingManager, dataReader));
                                    }
                                    break;
                                case VisualElementCollectionChangeActionType.InsertItem:
                                    scriptBuilder.AppendInvocationStatement(string.Concat(componentVariableName, ".insert"), changeAction.Index, RenderItem(bindingManager, dataReader));
                                    break;
                                case VisualElementCollectionChangeActionType.RemoveItem:
                                    scriptBuilder.AppendInvocationStatement(string.Concat(componentVariableName, ".removeAt"), changeAction.Index);
                                    //scriptBuilder.AppendRawValue("debugger;if (cmp.ownerGrid != null) {var widgetGrid = cmp.ownerGrid; if (widgetGrid.xtype = 'VTWidgetGrid') {");
                                    //scriptBuilder.AppendFormattedRawValue("widgetGrid.removeFromViewModelContrlos({1});", new JavaScriptCode(changeAction.Index.ToString()));
                                    //scriptBuilder.AppendRawValue("}}");  

                                    break;
                            }
                        }
                    }

                    // Clear update flags
                    itemsChangeActionsContainer.ClearChangeActions();
                }
            }

            // Return flag indicating if to resend data
            return false;
        }
        /// <summary>
        /// Gets the updatable items.
        /// </summary>
        /// <param name="enumerable">The enumerable.</param>
        /// <returns></returns>
        private IVisualElement[] GetUpdatableItems(IEnumerable<IVisualElementCollectionChangeAction> changeActions)
        {
            List<IVisualElement> items = new List<IVisualElement>();

            // If there are valid actions
            if (changeActions != null)
            {
                // Loop all change actions
                foreach (IVisualElementCollectionChangeAction changeAction in changeActions)
                {
                    // Check change type
                    switch (changeAction.Type)
                    {
                        case VisualElementCollectionChangeActionType.SetItem:
                            items.Add(changeAction.Item);
                            break;
                        case VisualElementCollectionChangeActionType.InsertItem:
                            items.Add(changeAction.Item);
                            break;
                    }
                }
            }

            return items.ToArray();
        }

        /// <summary>
        /// Renders the items array.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="bindingManager">The binding manager.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        private void RenderItemsArray(RenderingContext context, BindingManager bindingManager, JsonTextWriter jsonWriter)
        {
            // Write start array
            jsonWriter.WriteStartArray();

            // If there is a valid data source
            if (bindingManager != null)
            {
                // Create the binding reader
                IDataReader bindingReader = bindingManager.CreateReader();

                // Loop all items
                while (bindingReader.Read())
                {
                    RenderItem(bindingManager, jsonWriter, bindingReader);
                }
            }

            // Write end array
            jsonWriter.WriteEndArray();
        }

        /// <summary>
        /// Renders the item.
        /// </summary>
        /// <param name="bindingManager">The binding manager.</param>
        /// <param name="bindingReader">The binding reader.</param>
        private static object RenderItem(BindingManager bindingManager, IDataReader bindingReader)
        {
            // Create string writer
            StringWriter stringWriter = new StringWriter();

            // Create JSON writer
            JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(stringWriter);

            // If there is a valid JSON writer
            if (jsonWriter != null)
            {
                // If there is a valid binding reader
                if (bindingReader != null && bindingReader.Read())
                {
                    // Render item to writer
                    RenderItem(bindingManager, jsonWriter, bindingReader);
                }
                else
                {
                    // Write empty object
                    jsonWriter.WriteStartObject();
                    jsonWriter.WriteEndObject();

                }

                jsonWriter.Flush();
            }

            // Return code
            return new JavaScriptCode(stringWriter.ToString());
        }

        /// <summary>
        /// Renders the item.
        /// </summary>
        /// <param name="bindingManager">The binding manager.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="bindingReader">The binding reader.</param>
        private static void RenderItem(BindingManager bindingManager, JsonTextWriter jsonWriter, IDataReader bindingReader)
        {
            // Start item object
            jsonWriter.WriteStartObject();

            // If is an editable data source
            if (bindingManager.IsEditable)
            {
                // Get item id
                object objItemId = bindingReader.GetValue(int.MinValue);

                // If there is a valid item id
                if (objItemId != null)
                {
                    // Set the index column
                    jsonWriter.WritePropertyWithRawValue(ITEMID_COLUMN_NAME, JsonConvert.SerializeObject(objItemId));
                }
            }



            // Loop all columns
            for (int columnOrdinal = 0; columnOrdinal < bindingReader.FieldCount; columnOrdinal++)
            {
                string value = JavaScriptBuilder.GetValueCode(bindingReader.GetValue(columnOrdinal));
                string tempVlaue = value.ToLowerInvariant();
                if (tempVlaue.Equals("true") || tempVlaue.Equals("\"true\""))
                {
                    value = "true";
                }
                else if (tempVlaue.Equals("false") || tempVlaue.Equals("\"false\""))
                {
                    value = "false";
                }
                string dateTypeName = bindingReader.GetDataTypeName(columnOrdinal);
                if (dateTypeName.Equals("DateTime"))
                {

                    value = FormatHelper.GetDateValue(Convert.ToString(bindingReader.GetValue(columnOrdinal)));
                }
                if (dateTypeName.Equals("Double"))
                {
                    string doubleValue = Convert.ToString(bindingReader.GetValue(columnOrdinal));
                    if (!string.IsNullOrEmpty(doubleValue))
                    {
                        value = Convert.ToDouble(doubleValue).ToString();
                    }

                }
                // Get column name 
                string columnName = bindingReader.GetName(columnOrdinal);

                if (!String.IsNullOrEmpty(columnName))
                {
                    // Write field value from column                        
                    jsonWriter.WritePropertyWithRawValue(GetColumnClientName(columnName), value);
                }

            }

            // End the item object
            jsonWriter.WriteEndObject();
        }

        /// <summary>
        /// Gets the name of the client column.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string GetColumnClientName(string name)
        {
            // If is id column
            if (string.Equals(name, ITEMID_COLUMN_NAME, StringComparison.Ordinal))
            {
                return string.Concat(IDX_ESCAPE_PREFIX, name);
            }

            return name;
        }

        /// <summary>
        /// Gets the name of the server column.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string GetColumnServerName(string name)
        {
            // If is escaped column
            if (name != null && name.StartsWith(IDX_ESCAPE_PREFIX))
            {
                return name.Substring(IDX_ESCAPE_PREFIX.Length);
            }

            return name;
        }

        /// <summary>
        /// The data source response
        /// </summary>
        private class DataSourceResponse
        {
            /// <summary>
            /// The callback
            /// </summary>
            private readonly string _callback;

            /// <summary>
            /// The writer
            /// </summary>
            private readonly StringBuilder _writer = new StringBuilder();

            /// <summary>
            /// The exception
            /// </summary>
            private Exception _exception = null;

            /// <summary>
            /// Initializes a new instance of the <see cref="DataSourceResponse"/> class.
            /// </summary>
            /// <param name="callback">The callback.</param>
            public DataSourceResponse(string callback)
            {
                // Set the callback
                this._callback = callback;
            }

            /// <summary>
            /// Creates the writer.
            /// </summary>
            /// <returns></returns>
            public JsonTextWriter CreateWriter()
            {
                return RenderingUtils.GetJsonWriter(_writer);
            }

            /// <summary>
            /// Gets the callback.
            /// </summary>
            /// <value>
            /// The callback.
            /// </value>
            public string Callback
            {
                get { return _callback; }
            }


            /// <summary>
            /// Gets or sets the response.
            /// </summary>
            /// <value>
            /// The response.
            /// </value>
            public string Response
            {
                get
                {
                    // If there is an exception
                    if (_exception != null)
                    {
                        StringBuilder exceptionCode = new StringBuilder();

                        // Create the exception result
                        using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(exceptionCode))
                        {
                            jsonWriter.WriteStartObject();
                            jsonWriter.WriteProperty("error", _exception.Message);
                            jsonWriter.WriteEndObject();
                            jsonWriter.Flush();
                        }

                        // Return exception code
                        return exceptionCode.ToString();
                    }
                    else
                    {
                        // Return content
                        return _writer.ToString();
                    }
                }
            }

            /// <summary>
            /// Gets or sets the exception.
            /// </summary>
            /// <value>
            /// The exception.
            /// </value>
            public Exception Exception
            {
                get
                {
                    return _exception;
                }
                set
                {
                    _exception = value;
                }
            }
        }

        /// <summary>
        /// Handles the data source request.
        /// </summary>
        /// <param name="bindingManager">The binding manager element.</param>
        /// <param name="eventObject">The event object.</param>
        internal void HandleDataSourceRequest(RenderingContext renderingContext, BindingManager bindingManager, JObject eventObject)
        {
            // Get the callback id
            string callback = eventObject.Value<string>("callback");

            // Get the request params
            JObject parameters = eventObject.GetValue("params") as JObject;

            // Create the response 
            DataSourceResponse response = new DataSourceResponse(callback);

            try
            {
                // Create the JSON writer
                using (JsonTextWriter jsonWriter = response.CreateWriter())
                {
                    // Check action
                    switch (eventObject.Value<string>("action"))
                    {
                        case "read":

                            // Handle read request
                            this.HandleDataSourceReadRequest(renderingContext, bindingManager, parameters, jsonWriter);
                            break;

                        case "update":

                            // Get data 
                            JObject data = eventObject.GetValue("data") as JObject;
                            JArray dataArray = eventObject.GetValue("data") as JArray;
                            // If there is a valid data
                            if (data != null)
                            {
                                // Handle update
                                this.HandleDataSourceUpdateRequest(renderingContext, bindingManager, parameters, data, jsonWriter);
                            }
                            else if (dataArray != null)
                            {
                                foreach (JObject item in dataArray)
                                {
                                    // Handle update
                                    this.HandleDataSourceUpdateRequest(renderingContext, bindingManager, parameters, item, jsonWriter);
                                }
                            }
                            return;

                        case "create":
                        case "destroy":
                            return;

                        case "current":
                            this.HandleDataSourceCurrentRequest(renderingContext, bindingManager, parameters, jsonWriter);
                            return;

                        default:

                            // Write an empty object
                            jsonWriter.WriteStartObject();
                            jsonWriter.WriteEndObject();
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                // If there is a valid response
                if (response != null)
                {
                    // Set the exception
                    response.Exception = exception;
                }
            }

            // If there is a valid response
            if (response != null)
            {
                // Handle the response
                bindingManager.InvokeMethod("HandleDataSourceResponse", response);
            }
        }

        /// <summary>
        /// Handles the data source current request.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="bindingManager">The binding manager element.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        private void HandleDataSourceCurrentRequest(RenderingContext renderingContext, BindingManager bindingManager, JObject parameters, JsonTextWriter jsonWriter)
        {
            // If there are parameters
            if (parameters != null)
            {
                // Get item id token
                JToken itemIdToken = parameters.GetValue("itemId");

                // If there is a valid item id token
                if (itemIdToken != null)
                {
                    // Get item by item id
                    object item = bindingManager.GetItemByItemId(itemIdToken.ToObject(bindingManager.ItemIdType));

                    // If there is a valid item
                    if (item != null)
                    {
                        // Set current item
                        bindingManager.SetCurrent(item);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the data source update request.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="bindingManager">The binding manager element.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="data">The data.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        private void HandleDataSourceUpdateRequest(RenderingContext renderingContext, BindingManager bindingManager, JObject parameters, JObject data, JsonTextWriter jsonWriter)
        {
            // Get item id token
            JToken itemIdToken = data.GetValue(ITEMID_COLUMN_NAME);

            // If there is a valid item id token
            if (itemIdToken != null)
            {
                // Get item by item id
                object item = bindingManager.GetItemByItemId(itemIdToken.ToObject(bindingManager.ItemIdType));

                // If there is a valid item
                if (item != null)
                {
                    // Flag indicating if item was updated
                    bool itemUpdated = false;

                    // Try to update item through known interface
                    IBindingViewModelRecord record = item as IBindingViewModelRecord;
                    if (record != null)
                    {
                        itemUpdated = UpdateValues(data, item,
                            (key, valueToken) =>
                            {
                                // Get field to update
                                IBindingViewModelField field = record.GetField(key);
                                if (field != null)
                                {
                                    // Get value
                                    object value = valueToken.GetTypedObject(field.FieldType);

                                    using (bindingManager.SuspendServerSideTrackingBlock)
                                    {
                                        return field.SetValue(value);
                                    }
                                }

                                // No update made
                                return false;
                            });
                    }
                    else
                    {
                        // Fallback for types that are not implementing known interface
                        // Get the item type
                        Type itemType = item.GetType();

                        itemUpdated = UpdateValues(data, item,
                            (key, valueToken) =>
                            {
                                // Get item property from item type
                                PropertyInfo itemProperty = itemType.GetProperty(key);

                                // If there is a valid item property
                                if (itemProperty != null && itemProperty.CanWrite)
                                {
                                    using (bindingManager.SuspendServerSideTrackingBlock)
                                    {
                                        // Set value
                                        itemProperty.SetValue(item, valueToken.ToObject(itemProperty.PropertyType));
                                    }
                                    // Indicate item updated
                                    return true;
                                }

                                // No updates made
                                return false;
                            });
                    }

                    // If item was updated
                    if (itemUpdated)
                    {
                        // Update item
                        bindingManager.Update(item);

                        // Commit update
                        bindingManager.Commit();
                    }
                }
            }
        }

        /// <summary>
        /// Updates the values.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="item">The item.</param>
        /// <param name="updateField">The update field logic.</param>
        /// <returns></returns>
        private static bool UpdateValues(JObject data, object item, Func<string, JToken, bool> updateField)
        {
            bool itemUpdated = false;

            // Loop all values in data
            foreach (KeyValuePair<string, JToken> keyValue in data)
            {
                // Get data key
                string key = keyValue.Key;

                // If is not the item id column
                if (!string.Equals(ITEMID_COLUMN_NAME, keyValue.Key, StringComparison.Ordinal))
                {
                    // Get value token
                    JToken valueToken = keyValue.Value;

                    // If there is a valid value token
                    if (valueToken != null)
                    {
                        // Try to update field
                        itemUpdated = updateField(key, valueToken);
                    }
                }
            }
            return itemUpdated;
        }

        /// <summary>
        /// Handles the data source read request.
        /// </summary>
        /// <param name="bindingManager">The binding manager.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="callback">The callback.</param>
        private void HandleDataSourceReadRequest(RenderingContext renderingContext, BindingManager bindingManager, JObject parameters, JsonTextWriter jsonWriter)
        {
            RenderItemsArray(renderingContext, bindingManager, jsonWriter);
        }



        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext renderingContext, IVisualElementMethodInvoker methodInvoker, VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Check method name
            if (string.Equals(methodName, "HandleDataSourceResponse"))
            {
                // Get the response 
                DataSourceResponse response = methodData as DataSourceResponse;

                // If there is a valid response
                if (response != null)
                {
                    // Get the java script builder
                    JavaScriptBuilder jsBuilder = getJavaScriptBuilder(false);

                    // Invoke callback method
                    jsBuilder.AppendInvocationStatement("VT_invokeCallbackById", response.Callback, null, response.Exception == null, new JavaScriptCode(response.Response));
                }

                // Exit rendering
                return;
            }

            base.RenderMethod(renderingContext, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
        }
    }
}
