﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewLabelBaseField), "DataViewFieldExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("TemplateText", PropertyInitializeCode = "tpl = <#= element.TemplateText #>")]
    public class DataViewLabelBaseFieldExtJSRenderer : DataViewLabelBaseFieldExtJSRendererBase
    {
        /// <summary> 
        /// Gets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Renders the TemplateText property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTemplateTextProperty(ExtJSRenderingContext context, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // tpl property should be rendered always.

            // Get typed element
			System.Web.VisualTree.Elements.DataViewLabelBaseField element = visualElement as System.Web.VisualTree.Elements.DataViewLabelBaseField;

			// If there is a valid typed element
			if(element != null)
			{
				jsonWriter.WriteProperty("tpl",  element.TemplateText );				
			}
        }               
    }
}
