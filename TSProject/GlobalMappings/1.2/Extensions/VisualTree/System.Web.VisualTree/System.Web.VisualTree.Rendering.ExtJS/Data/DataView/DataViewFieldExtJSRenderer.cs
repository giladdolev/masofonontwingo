﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewField), "VisualElementExtJSRenderer", "column", "Ext.grid.column.Column")]
   [ExtJSPropertyDescription("Top", PropertyInitializeCode = "y = <#= element.PixelTop #>")]
    [ExtJSPropertyDescription("Left", PropertyInitializeCode = "x = <#= element.PixelLeft #>")]
    [ExtJSPropertyDescription("Width", PropertyInitializeCode = "width = <#= element.PixelWidth #>")]
    [ExtJSPropertyDescription("Height", PropertyInitializeCode = "height = <#= element.PixelHeight #>")]
    [ExtJSPropertyDescription("DataMember", PropertyInitializeCode = "member = <#= BindingManagerExtJSRenderer.GetColumnClientName(element.DataMember) #>")]
    [ExtJSPropertyDescription("ForeColor", PropertyInitializeCode = "fieldStyle.color = <#= DataViewFieldExtJSRenderer.ReviseForeColor(element) #>", PropertyChangeCode = "this.setFieldStyle('color:<#= DataViewFieldExtJSRenderer.ReviseForeColor(element) #>');")]
    [ExtJSPropertyDescription("Font", PropertyInitializeCode = "fieldStyle.font = <#= DataViewFieldExtJSRenderer.ReviseFont(element) #>", PropertyChangeCode = "this.setFieldStyle('font:<#= DataViewFieldExtJSRenderer.ReviseFont(element) #>)';")]
    [ExtJSPropertyDescription("WrapText", PropertyInitializeCode = "textWrap = <#= element.WrapText #>")]    
    public class DataViewFieldExtJSRenderer : DataViewFieldExtJSRendererBase
    { 
        /// <summary>
        /// Renders the identifier property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderItemIdProperty(VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            
        }

        /// <summary>
        /// Gets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public virtual string EditorXType
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Renders the x type property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderXTypeProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // If is not the default column xtype
            if (this.XType != "column")
            {
                // Render column xtype
                base.RenderXTypeProperty(renderingContext, visualElement, jsonWriter);
            }
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderContentProperties(RenderingContext context, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(context, visualElement, jsonWriter);
            this.RenderEditorProperty(context, visualElement, jsonWriter);
            this.RenderFieldRenderer(context, visualElement, jsonWriter);
        }

        public virtual void RenderFieldRenderer(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            DataViewField fieldElement = visualElement as DataViewField;
            if (this.ShouldRenderFieldRenderer(fieldElement))
            {
                // Render editor
                jsonWriter.WritePropertyName("renderer");
                jsonWriter.WriteRaw("function (value, metaData, record, rowIndex, colIndex)");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteRaw(String.Format(@"metaData.tdStyle = '{0}';",
                        GetBorderStyle(fieldElement)));

                    string value = String.IsNullOrEmpty(fieldElement.DataMember) ? "value" : String.Format("record.data['{0}']", fieldElement.DataMember);
                    jsonWriter.WriteRaw(String.Format(@"return {0};", GetFormattedValue(fieldElement, value)));
                }
            }
        }

        protected virtual string GetFormattedValue(DataViewField fieldElement, string value)
        {
            return value;
        }

        private string GetBorderStyle(DataViewField fieldElement)
        {
            string color = GetBorderColor(fieldElement);
            string borderWidth = GetBorderWidth(fieldElement);
            switch (fieldElement.BorderStyle)
            {
                case BorderStyle.FixedSingle:
                    return String.Format("border:{0} solid {1};", borderWidth, color);
                case BorderStyle.Outset:
                    return String.Format("border:{0} outset {1};", borderWidth, color);
                case BorderStyle.Fixed3D:
                case BorderStyle.Inset:
                    return String.Format("border:{0} inset {1};", borderWidth, color);
                case BorderStyle.Dotted:
                    return String.Format("border:{0} dotted {1};", borderWidth, color);
                case BorderStyle.Dashed:
                    return String.Format("border:{0} dashed {1};", borderWidth, color);
                case BorderStyle.Solid:
                    return String.Format("border:{0} solid {1};", borderWidth, color);
                case BorderStyle.Double:
                    return String.Format("border:{0} double {1};", borderWidth, color);
                case BorderStyle.Groove:
                    return String.Format("border:{0} groove {1};", borderWidth, color);
                case BorderStyle.Ridge:
                    return String.Format("border:{0} ridge {1};", borderWidth, color);
                case BorderStyle.ShadowBox:
                    return String.Format("border-bottom:{0} solid {1};border-right:thick  solid {1};", borderWidth, color);
                case BorderStyle.Underline:
                    return String.Format("border-bottom:{0} solid {1};", borderWidth, color);
            }
            return "";
        }

        private string GetBorderWidth(DataViewField fieldElement)
        {
            return "1px";
        }

        private string GetBorderColor(DataViewField fieldElement)
        {
            return "gray";
        }

        protected virtual bool ShouldRenderFieldRenderer(DataViewField fieldElement)
        {
            return (fieldElement != null) &&
                fieldElement.BorderStyle  != BorderStyle.None;
        }

        /// <summary>
        /// Renders the editor property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEditorProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            DataViewField dataViewField = visualElement as DataViewField;

            // If we should render editor 
            if (dataViewField != null && !dataViewField.ReadOnly)
            {
                // Render editor
                jsonWriter.WritePropertyName("editor");
                jsonWriter.WriteStartObject();
                jsonWriter.WriteProperty("xtype", this.EditorXType);
                this.RenderEditorProperties(renderingContext, visualElement, jsonWriter);
                jsonWriter.WriteEndObject();
            }
        }

        /// <summary>
        /// Should the render editor property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        protected virtual bool ShouldRenderEditorProperty(VisualElement visualElement)
        {
            return !string.IsNullOrWhiteSpace(this.EditorXType);
        }

        /// <summary>
        /// Renders the editor properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEditorProperties(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            
        }

        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Render id property
            base.RenderProperties(renderingContext, visualElement, jsonWriter);
            // Write style property
            RenderFieldStyleProperty(renderingContext, visualElement, jsonWriter);            
        }

        public static string ReviseForeColor(VisualElement visualElement)
        {
            DataViewField element = visualElement as DataViewField;

            if(element != null && element.ForeColor != Color.Empty)
            {
                return ColorTranslator.ToHtml(element.ForeColor);
            }

            return "inherit";
        }

        /// <summary>
        /// Renders the style font property.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseFont(DataViewField dataViewField)
        {
            string fontStyle = "";
            
            if (dataViewField != null)
            {
                Font font = dataViewField.Font;
                if (font != null)
                {
                    if (font.Bold)
                        fontStyle = "bold ";                    

                    if (font.Size > 0)
                        fontStyle += font.Size + "pt ";

                    if (font.FontFamily != null && !string.IsNullOrEmpty(font.FontFamily.Name))
                        fontStyle += font.FontFamily.Name;
                }
            }

            return fontStyle;
        }
    }
}
