setTimeout(function(){
    var colIndex = <#= data.ColumnIndex #>;
    var rowIndex = <#= data.RowIndex #>;
    var selectedIndex = <#= data.SelectedIndex#>;
    var grid = this;

    try {
        if(grid != null){
            // spreadsheet mode 
            if(grid.getSelectionModel().type == 'spreadsheet'){
                colIndex++;
            }

            var plugin = grid.getPlugin('cellEditing');
            var editor = grid.getColumns()[colIndex].getEditor();
            var itemsCount = 0;
            // If ther is a plugin
            if(plugin && editor)
            {

                // Get the total items count inside the picker
                itemsCount = editor.getPickerStore().totalCount;
                if(selectedIndex < itemsCount)
                {

                    // Get the record we want to modify from store
                    var rec = grid.getStore().getAt(rowIndex);

                    // Get the new value field from the given selected index
                    var newValue = editor.getPickerStore().data.items[selectedIndex].data[editor.displayField];

                    //Update the record value
                    rec.data[editor.dataIndex] = newValue;

                    // Get the column header
                    var colHeader = grid.view.getHeaderCt().getHeaderAtIndex(colIndex);

                    // Get the context of the plugin
                    var context = plugin.getEditingContext(rec,colHeader);

                    // Edit and refresh view
                    plugin.startEdit(rec,colHeader);
                    plugin.completeEdit();
                    grid.view.refresh();

                }
            }
           
        }

    } catch (e) {
        console.log('GridColumn.SetSelectedIndex error : ' + e.message);
    }
},300);

