﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridElement), "ControlElementExtJSRenderer", "VTgridpanel", "VT.ux.VTGridPanel")]
    [ExtJSPropertyDescription("AllowColumnSelection", PropertyChangeCode = "this.selModel.setColumnSelect(<#= JavaScriptCode.GetBool(element.AllowColumnSelection)#>);")]
    [ExtJSPropertyDescription("AllowTrackMouseOver", PropertyInitializeCode = "viewConfig.trackOver = <#=JavaScriptCode.GetBool(element.AllowTrackMouseOver) #>", ClientNotDefaultCode = "element.AllowTrackMouseOver != true")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "BodyStyle.backgroundColor = <#= ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != Color.Empty", PropertyChangeCode = "this.setBodyStyle('background-color','<#= ColorTranslator.ToHtml(element.BackColor) #>');")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "title = <#= element.Text #>", ClientNotDefaultCode = "!String.IsNullOrEmpty(element.Text)", PropertyChangeCode = "this.setTitle('<#= element.Text #>')")]
    [ExtJSPropertyDescription("CanSelect", PropertyInitializeCode = "disableSelection = <#= !element.CanSelect #>", ClientNotDefaultCode = "!element.CanSelect", PropertyChangeCode = "this.getSelectionModel().setLocked(<#=JavaScriptCode.GetBool(!element.CanSelect)#>)")]
    [ExtJSPropertyDescription("Cols", " <#= element.Cols #>")]
    [ExtJSPropertyDescription("ShowAutoFilterColumnItem", PropertyChangeCode = "this.getPlugin('filterplugin').setVisible(<#= JavaScriptCode.GetBool(element.ShowAutoFilterColumnItem) #>)")]
    [ExtJSPropertyDescription("ColumnFilterInfo", PropertyChangeCode = "this.store.filter('<#=element.ColumnFilterInfo.FilterInfoKey#>', '<#=element.ColumnFilterInfo.FilterInfoValue#>');")]
    [ExtJSPropertyDescription("ColumnHeadersVisible", PropertyInitializeCode = "hideHeaders = <#= !element.ColumnHeadersVisible #>", ClientNotDefaultCode = "!element.ColumnHeadersVisible")]
    [ExtJSPropertyDescription("Content", PropertyInitializeCode = "html = <#= Convert.ToString(element.Content) #>")]
    [ExtJSPropertyDescription("EnableLocking", PropertyInitializeCode = "enableLocking = <#=element.EnableLocking #>", ClientNotDefaultCode = "element.FrozenCols > 0 && element.EnableLocking != false")]
    [ExtJSPropertyDescription("FrozenCols", ClientNotDefaultCode = "element.FrozenCols != 0", PropertyChangeCode = "<#resource: SetFrozenCols.js #>")]
    [ExtJSPropertyDescription("UseFixedHeaders", PropertyInitializeCode = "fixedHeaders = <#= element.UseFixedHeaders #>", PropertyChangeCode = "this.setFixedHeaders(<#= element.UseFixedHeaders #>)")]
    [ExtJSPropertyDescription("MultiSelect", PropertyChangeCode = "multiSelect =<#= element.MultiSelect #>")]
    [ExtJSPropertyDescription("SortableColumns", PropertyInitializeCode = "sortableColumns = <#= JavaScriptCode.GetBool(element.SortableColumns)#>", ClientNotDefaultCode = "element.SortableColumns == false")]
    [ExtJSPropertyDescription("SearchPanel", PropertyInitializeCode = "searchPanel = <#= element.SearchPanel #>", ClientNotDefaultCode = "element.SearchPanel !=false", PropertyChangeCode = "this.setSearchPanel(<#= JavaScriptCode.GetBool(element.SearchPanel) #>);")]
    [ExtJSPropertyDescription("CellBorderStyle", PropertyInitializeCode = "rowLines = <#= GridElementExtJSRenderer.ReviseCellBorderStyleRow(element.CellBorderStyle) #>, columnLines = <#= GridElementExtJSRenderer.ReviseCellBorderStyleColumn(element.CellBorderStyle) #>", ClientNotDefaultCode = "element.CellBorderStyle != GridCellBorderStyle.SingleVertical")]


    [ExtJSMethodDescription("ClearFilter", MethodCode = "this.store.clearFilter();")]
    [ExtJSMethodDescription("EditCellByPosition", MethodCode = "<#resource:EditCell.js#> ", MethodDataType = typeof(GridIndexesData))]
    [ExtJSMethodDescription("SetCellBackgroundColor", MethodCode = "Ext.getElementById(this.getView().getCell(<#= data.RowIndex #>, this.getColumns()[<#= data.ColumnIndex #>]).id).style.backgroundColor = <#= ColorTranslator.ToHtml(data.BackgroundColor) #>", MethodDataType = typeof(GridCellBackGroundData))]
    [ExtJSMethodDescription("SetComboboxSelectedIndex", MethodCode = "<#resource:SetSelectedIndex.js#>", MethodDataType = typeof(GridComboBoxCellSelectionData))]
    [ExtJSMethodDescription("SelectCell", MethodCode = "<# resource: SelectCell.js #>", MethodDataType = typeof(GridIndexesData))]
    [ExtJSMethodDescription("SetCellStyle", MethodCode = "<#resource:SetCellStyle.js#>", MethodDataType = typeof(GridCellInfo))]
    [ExtJSMethodDescription("SetRowVisibility", MethodCode = "<#resource:SetRowVisibility.js#>", MethodDataType = typeof(GridRowData))]
    [ExtJSMethodDescription("SetRowHeaderBackgroundColor", MethodCode = "Ext.getElementById(this.getView().getCell(<#= data.RowIndex #>, this.getColumns()[<#= data.ColumnIndex #>]).id).style.backgroundColor = <#= ColorTranslator.ToHtml(data.BackgroundColor) #>", MethodDataType = typeof(GridCellBackGroundData))]
    [ExtJSMethodDescription("SetRowHeaderCornerIcon", MethodCode = "this.getColumns()[0].items.items[0].setSrc(<#= FormatHelper.FormatResource(context, element.HeaderColumns[0].Icon) #>);", MethodDataType = typeof(GridCellBackGroundData))]
    [ExtJSMethodDescription("SetRowHeaderBackgroundImage", MethodCode = "this.getView().getCell(<#= data.RowIndex #>, this.getColumns()[0]).setStyle(<#= data.RowIndex #> , this.getColumns()[0]).setStyle('background', 'url(<#= FormatHelper.FormatResource(context, data.BackGroundImage) #>) no-repeat center');", MethodDataType = typeof(GridCellBackGroundData))]

    [ExtJSMethodDescription("SetProtectCell", MethodCode = "<# resource: ProtectCell.js #>", MethodDataType = typeof(GridIndexesData))]
    [ExtJSMethodDescription("SetSelection", MethodCode = "<#resource:SetSelection.js#>")]
    [ExtJSMethodDescription("Sort", MethodCode = "<#resource:SortColumn.js#>", MethodDataType = typeof(GridColumnData))]
    [ExtJSMethodDescription("SwapColumns", MethodCode = "<#resource:SwapColumns.js#>", MethodDataType = typeof(GridColumnData))]
    [ExtJSMethodDescription("ScrollToRow", MethodCode = "<#resource:ScrollToRow.js#>", MethodDataType = typeof(GridRowData))]
    [ExtJSMethodDescription("SetCellStyle", MethodCode = "<#resource:SetCellStyle.js#>", MethodDataType = typeof(GridCellInfo))]
    [ExtJSMethodDescription("SetWidgetStyle", MethodCode = "<#resource:SetWidgetStyle.js#>", MethodDataType = typeof(GridCellInfo))]
    [ExtJSMethodDescription("SetProtectCell", MethodCode = "<# resource: ProtectCell.js #>", MethodDataType = typeof(GridIndexesData))]

    [ExtJSEventDescription("Afterrender", "afterrender", ClientEventHandlerParameterNames = "grid", ClientCode = "VT_GridAfterRender(this, '<#= WindowElementExtJSRenderer.ReviseContextMenuStripID(element) #>')", RenderAlways = true, ClientEventBehaviorType = ExtJSEventBehaviorType.Queued)]
    [ExtJSEventDescription("dataChanged", ClientEventName = "dataChanged")]
    [ExtJSEventDescription("CancelEdit", "canceledit")]
    [ExtJSEventDescription("CellContentClick", ClientEventName = "itemclick", ClientEventHandlerParameterNames = " view, record, item, index, e, eOpts", ClientHandlerCallbackData = "rowInedx = index,columnIndex = VT_GetColumnFromItemClickEvent(e)", ServerEventArgsCreateCode = "new GridElementCellEventArgs(this.columnIndex.ToInt32(),this.rowInedx.ToInt32())")]
    [ExtJSEventDescription("CellDoubleClick", "celldblclick", "list, td, cellIndex, record, tr, rowIndex", "rowIndex = rowIndex, cellIndex = cellIndex", "new GridElementCellEventArgs(this.cellIndex.ToInt32(), this.rowIndex.ToInt32())")]
    [ExtJSEventDescription("CellEndEdit", ClientEventName = "edit",ClientCode = "if(this.xtype == 'VTgridpanel'){if(!this.hasDirtyCell){return;}}", ClientEventHandlerParameterNames = " view,editor, context, eOpts ", ClientHandlerCallbackData = "newValue = (editor)?editor.value:null,rowIndex = editor.rowIdx,dataindex = editor.field", ServerEventArgsCreateCode = "new GridCellEventArgs(this.newValue,this.dataindex,this.rowIndex")]
    [ExtJSEventDescription("CellMouseDown", ClientEventName = "cellmousedown", ClientEventHandlerParameterNames = " view , td , cellIndex , record , tr , rowIndex , e , eOpts ", ClientHandlerCallbackData = "cellIndex = VT_GetActualCellIndex(view,cellIndex), rowIndex = rowIndex , X = e.getX(), Y = e.getY(), button = e.button , delta = e.getWheelDelta() ,ctrlKey = e.ctrlKey, shiftKey = e.shiftKey", ServerEventArgsCreateCode = "new GridCellMouseEventArgs(this.cellIndex.ToInt32(), this.rowIndex.ToInt32(), this.X.ToInt32(), this.Y.ToInt32(), new MouseEventArgs( GridElementExtJSRenderer.ReviseMouseButton(this.button.ToInt32()),0,0,0,this.delta.ToInt32()),this.ctrlKey.ToBoolean(),this.shiftKey.ToBoolean())", ClientEventBehaviorType = ExtJSEventBehaviorType.Delayed, RenderAlways = true)]
    [ExtJSEventDescription("ColumnHeaderMouseClick", ClientEventName = "headerclick", ClientEventHandlerParameterNames = "ct , column , e , t , eOpts", ClientHandlerCallbackData = "columnIndex = VT_GridActualColumnIndex(column, column.getIndex())", ServerEventArgsCreateCode = "new GridElementCellEventArgs(this.columnIndex.ToInt32(), -1)")]
    [ExtJSEventDescription("ColumnHeaderMouseRightClick", "headercontextmenu", ClientEventHandlerParameterNames = " view , column , e , t , eOpts ", ClientHandlerCallbackData = "columnIndex = VT_GridActualColumnIndex(column, column.getIndex())", ServerEventArgsCreateCode = "new GridElementCellEventArgs(this.columnIndex.ToInt32(), -1)")]
    [ExtJSEventDescription("ColumnDisplayIndexChanged", "columnmove", ClientEventHandlerParameterNames = "ct , column , fromIdx , toIdx , eOpts", ClientHandlerCallbackData = "fromIndex = fromIdx , toIndex = column.fullColumnIndex ", ServerEventArgsCreateCode = "new GridColumnMoveEventArgs(this.fromIndex.ToInt32(), this.toIndex.ToInt32())")]
    [ExtJSEventDescription("ContextMenuClick", ClientEventName = "cellcontextmenu", ClientEventHandlerParameterNames = " view, td, cellIndex, record, tr, rowIndex, e, eOpts ", ClientHandlerCallbackData = "value = e.getXY().toString()", ServerEventArgsCreateCode = "new GridContextMenuCellEventArgs(this.value)")]
    [ExtJSEventDescription("KeyDown", ClientEventName = "itemkeydown", ClientEventHandlerParameterNames = "grid, e, ar, bt, code ", ClientHandlerCallbackData = "key = code.keyCode", ServerEventArgsCreateCode = "new KeyDownEventArgs(this.key.ToInt32())")]
    [ExtJSEventDescription("CellkeyDown", ClientEventName = "cellkeydown", ClientEventHandlerParameterNames = "view, td , cellIndex , record , tr , rowIndex , e , eOpts", ClientHandlerCallbackData = "key = e.getKey(),CellIndex = cellIndex,RowIndex = rowIndex ", ServerEventArgsCreateCode = "new GridElementCellEventArgs((Keys)this.key.ToInt32(),this.CellIndex.ToInt32(),this.RowIndex.ToInt32(),this.widgetDataMember)")]


    [ExtJSMethodDescription("RaiseUpdateClient", MethodCode = "<# resource: RaiseUpdateClient.js #>")]
    [ExtJSEventDescription("MouseRightClick", "containercontextmenu", ClientEventHandlerParameterNames = " view, e, eOpts")]
    [ExtJSEventDescription("RowHeaderMouseRightClick", ClientEventName = "cellcontextmenu", ClientEventHandlerParameterNames = "view , td , cellIndex , record , tr , rowIndex , e , eOpts ", ClientHandlerCallbackData = "colIndex = cellIndex, rowIndex = rowIndex", ServerEventArgsCreateCode = "new GridElementCellEventArgs(this.colIndex.ToInt32(), this.rowIndex.ToInt32())")]
    [ExtJSEventDescription("SelectionChanged", ClientEventName = "selectionchange", ClientEventHandlerParameterNames = " view, selection , eOpts ", ClientHandlerCallbackData = "rows = VT_GetSelectedRowIndeces(view,selection), cells = VT_GetSelectedCellsIndeces(selection)", ClientEventBehaviorType = ExtJSEventBehaviorType.Delayed)]
    [ExtJSMethodDescription("UpdateCellValue", MethodCode = "VT_GridUpdateCellValue(this,<#=data[0]#>,<#=data[1]#>,<#=data[2]#>)", MethodDataType = typeof(ArrayList))]
    [ExtJSMethodDescription("CompleteEdit", MethodCode = "VT_GridCompleteEdit(this)")]
    [ExtJSEventDescription("ClientEvent", "clientEvent", ClientEventHandlerParameterNames = " view,value ", ClientHandlerCallbackData = "newValue = value", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.newValue)")]


    public class GridElementExtJSRenderer : GridElementExtJSRedererBase
    {

        /// <summary>
        /// Revises the XTYPE property value.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <returns></returns>
        protected override string ReviseXTypePropertyValue(VisualElement visualElement, string xtype)
        {
            // Get GridElement .
            GridElement grid = visualElement as GridElement;

            // If there is a valid GridElement
            if (grid != null)
            {
                // If there is text
                if (grid.SearchPanel == true || grid.UseFixedHeaders)
                {
                    // Return title bar XTYPE
                    return "vt-gridpanel";
                }
            }

            return xtype;
        }


        /// <summary>
        /// Get sort order .
        /// </summary>
        /// <param name="sortOrder">sort order . </param>
        public static string ReviseSortOrder(SortOrder sortOrder)
        {
            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    return "ASC";
                case SortOrder.Descending:
                    return "DESC";
            }
            return "";
        }

        /// <summary>
        /// Revises the MouseButton.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static MouseButtons ReviseMouseButton(int button)
        {
            switch (button)
            {
                case 0:
                    return MouseButtons.Left;
                case 1:
                    return MouseButtons.Middle;
                case 2:
                    return MouseButtons.Right;
            }
            return MouseButtons.None;
        }


        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, VisualElement visualElement, JsonTextWriter writer)
        {
            GridElement grd = visualElement as GridElement;
            if (grd != null)
            {
                if (grd.SelectionMode == CellSelectionMode.Row)
                {
                    grd.ClientCellMouseDown = "if(cellIndex > 0) {view.addRowCls(rowIndex, 'rowstyle');}";
                }
            }

            grd.ClientSelectionChanged = "  var grid = this; if(grid != null){ var rowIndex =grid.getSelectedRowIndex();var cellIndex = grid.getSelectedColumnIndex();var clm=grid.getColumns()[cellIndex];if(null!=clm&&'SPGridColumnwidgetcolumn'==clm.xtype){var row=clm.Rows[rowIndex];if(null!=row)for(i=0;i<row.length;i++)if('label'!=row[i].xtype){row[i].focus();break;}}}";

            base.RenderEvents(context, visualElement, writer);


        }
        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            RenderSelModelProperty(renderingContext, visualElement, jsonWriter);

            // Write the data source
            RenderDataSourceProperty(renderingContext, visualElement, jsonWriter);
        }


        /// <summary>
        /// Renders the RowHeaderVisble property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        private void RenderSelModelProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the grid element 
            GridElement objGridElement = visualElement as GridElement;
            if (objGridElement != null)
            {
                // Write style
                jsonWriter.WritePropertyName("selModel");

                // Open object scope
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    string cellSelectValue = "true";
                    string rowSelectValue = "false";
                    string selType = GridElementExtJSRenderer.ReviseSelModel(objGridElement);
                    jsonWriter.WriteProperty("type", selType);
                    if (selType.Equals("spreadsheet"))
                    {
                        if (objGridElement.SelectionMode == CellSelectionMode.Row)
                        {
                            cellSelectValue = "false";
                            rowSelectValue = "true";
                        }
                        jsonWriter.WritePropertyWithRawValue("cellSelect", cellSelectValue);
                        jsonWriter.WritePropertyWithRawValue("rowSelect", rowSelectValue);
                    }


                    if (objGridElement.AllowColumnSelection == true)
                    {
                        jsonWriter.WriteProperty("columnSelect", JavaScriptCode.GetBool(objGridElement.AllowColumnSelection));
                    }
                }
            }
        }

        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, IVisualElementMethodInvoker methodInvoker, VisualElement visualElement,
            string methodName, object methodData, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            switch (methodName)
            {
                case "ContextMenuShow":
                    this.RenderContextMenuShowMethod(context, methodInvoker, methodData as string, getJavaScriptBuilder, methodInvoker.Owner as VisualElement);
                    break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }



        /// <summary>
        /// Renders the context menu show method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder.</param>
        /// <param name="obj">The object.</param>
        private void RenderContextMenuShowMethod(RenderingContext context, IVisualElementMethodInvoker methodInvoker, string methodData, Func<bool, JavaScriptBuilder> getJavaScriptBuilder, VisualElement obj)
        {
            // Get JsBuilder
            JavaScriptBuilder builder = getJavaScriptBuilder(true);

            // If builder is valid
            if (builder != null)
            {
                builder.AppendFormattedRawValue("VT_GetCmp({0},true).showAt({1},{2});", methodData, ((GridElement)obj).contextMenuXPosition, ((GridElement)obj).contextMenuYPosition);
            }
        }



        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);

            // Get grid element
            GridElement gridElement = visualElement as GridElement;

            // If there is a valid grid element 
            if (gridElement != null)
            {
                // Render scroll property
                jsonWriter.WriteProperty("autoScroll", gridElement.AutoScroll);

                //Render cell editing plugin
                jsonWriter.WritePropertyName("plugins");
                jsonWriter.WriteStartArray();
                if (gridElement.ShowAutoFilterColumnItem == true)
                {
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("ptype", "filterbar");
                        jsonWriter.WriteProperty("id", "filterplugin");
                    }
                }

                jsonWriter.WriteRawValue("'clipboard'");
                if (gridElement.ReadOnly != true)
                {
                    jsonWriter.WriteRawValue("Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 1, id: 'cellEditing',listeners: { beforeedit: function(editor, event) { var protect = event.grid.ownerGrid.getView().getCell(event.rowIdx,event.grid.ownerGrid.getColumns()[event.colIdx]).Protect; if(protect) return false; }}})");
                }

                if (gridElement.EnableExport != false)
                {
                    jsonWriter.WriteRawValue("'gridexporter'");
                    jsonWriter.WriteEndArray();

                    //Render the header
                    jsonWriter.WritePropertyName("header");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("itemPosition", 1);
                        jsonWriter.WritePropertyName("items");
                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                        {
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                //Render export menu button
                                jsonWriter.WriteProperty("ui", "default-toolbar");
                                jsonWriter.WriteProperty("xtype", "button");
                                jsonWriter.WriteProperty("text", "Export to...");
                                jsonWriter.WritePropertyName("menu");
                                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                                {
                                    jsonWriter.WritePropertyName("items");
                                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                                    {

                                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                                        {
                                            jsonWriter.WriteProperty("text", "Excel xlsx");
                                            jsonWriter.WriteClientFunction("handler", new string[] { }, "VT_GetCmp('" + VisualElement.GetElementId(gridElement) + "').saveDocumentAs({type: 'xlsx', fileName:'myExport.xlsx' });");
                                        }

                                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                                        {
                                            jsonWriter.WriteProperty("text", "CSV");
                                            jsonWriter.WriteClientFunction("handler", new string[] { }, "VT_GetCmp('" + VisualElement.GetElementId(gridElement) + "').saveDocumentAs({type: 'csv', fileName:'myExport.csv' });");
                                        }

                                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                                        {
                                            jsonWriter.WriteProperty("text", "HTML");
                                            jsonWriter.WriteClientFunction("handler", new string[] { }, "VT_GetCmp('" + VisualElement.GetElementId(gridElement) + "').saveDocumentAs({type: 'html', fileName:'myExport.html' });");

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    jsonWriter.WriteEndArray();
                }

                // check if the grid is a grouping grid
                if (gridElement.IsGroupingGrid == true)
                {
                    jsonWriter.WritePropertyName("features");
                    jsonWriter.WriteRawValue("[{ ftype: 'grouping' }]");
                }

                List<GridColumn> _tempColumns = new List<GridColumn>();
                // if the grid in spreadsheet mode add to the column collection the rowheader columns. 
                if (gridElement.RowHeaderVisible)
                {

                    foreach (GridColumn item in gridElement.HeaderColumns)
                    {
                        _tempColumns.Add(item);
                    }
                }
                _tempColumns.AddRange(gridElement.Columns.ToList());
                // Write dummy columns
                RenderPlainItemsProperty(renderingContext, _tempColumns, jsonWriter, "columns", (element => element != null && ((GridColumn)element).Visible));

            }
        }

        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            if (property == "$refresh")
            {
                RenderRefresh(renderingContext, visualElement, builder, componentVariable);
            }
            else if (property == "selModel")
            {
                RenderSelModelPropertyChange(renderingContext, visualElement, builder, componentVariable);
            }
            else
            {
                base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
            }
        }

        /// <summary>
        /// Renders the SelectionMode property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        private void RenderSelModelPropertyChange(RenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            // If there is a valid typed element
            if (element != null)
            {
                string selModelType = GridElementExtJSRenderer.ReviseSelModel(element);

                //view.updateLayout();
                switch (selModelType)
                {
                    case "cellmodel":
                        builder.AppendAssignmentStatement("cmp.selModel", new JavaScriptCode(string.Format("Ext.create(\'Ext.selection.CellModel\',{{selType: \'cellmodel\'}});")));
                        break;
                    case "rowmodel":
                        builder.AppendAssignmentStatement("cmp.selModel", new JavaScriptCode(string.Format("Ext.create(\'Ext.selection.RowModel\',{{selType: \'rowmodel\'}});")));
                        break;
                    default:
                        return;
                }
                builder.AppendFormattedRaw("{0}.updateLayout();", new JavaScriptCode(componentVariable));
            }
        }

        /// <summary>
        /// Renders the refresh.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRefresh(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get the GridElement element
            GridElement gridElement = visualElement as GridElement;

            // If there is a valid listview element
            if (gridElement == null)
            {
                return;
            }

            // Render the reconfigure script
            builder.AppendStartInvocation(string.Concat(componentVariable, ".reconfigure"));

            // Create the json string writer
            Text.StringBuilder stringBuilder = new Text.StringBuilder();

            // Create the json writer
            using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(stringBuilder))
            {

                RenderPlainItemsPropertyValue(renderingContext, gridElement.Columns, jsonWriter, (element => element != null && ((GridColumn)element).Visible && !(element is GridRowHeader)));

                // Flush columns
                jsonWriter.Flush();

                builder.AppendRawValue(stringBuilder.ToString());
            }


            builder.AppendEndInvocation(true);

            if (gridElement.UseFixedHeaders == true)
            {
                builder.AppendFormattedRaw(@"{0}.addFixedHeadersButtons({0});", componentVariable);
            }

            if (gridElement.ShowAutoFilterColumnItem == true)
            {

                builder.AppendFormattedRaw(@"{0}.getPlugin('filterplugin').setVisible(true);", componentVariable);
            }

        }

        /// <summary>
        /// Revises the selection mode.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>return selModel type. </returns>
        internal static string ReviseSelModel(GridElement element)
        {
            string selModelType = "rowmodel";
            if (element != null)
            {
                if (element.RowHeaderVisible == true)
                {
                    return "spreadsheet";
                }
                else
                {
                    switch (element.SelectionMode)
                    {
                        case CellSelectionMode.Cell:
                            return "cellmodel";
                    }
                }
            }
            return selModelType;
        }

        /// <summary>
        /// Indicates if we should render the CellEndEdit event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <returns></returns>
        protected override bool ShouldRenderCellEndEditEvent(RenderingContext context, GridElement element)
        {
            // Do not render event for RO grid
            if (element.ReadOnly)
            {
                return false;
            }


            return true;
        }

        /// <summary>
        /// Extracts the SelectionChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractSelectionChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            //    var selectedRows = args.Value<Newtonsoft.Json.Linq.JArray>("rows").Values<System.Int32>().ToArray();
            //    var cells = args.Value<Newtonsoft.Json.Linq.JArray>("cells");
            //    var selectedCells = cells.Select(obj => new GridIndexesData((int)obj[0], (int)obj[1])).ToArray();

            //    return new GridElementSelectionChangeEventArgs(selectedRows, selectedCells);

            GridElement grd = visualElement as GridElement;
            List<int> lstRows = new List<int>() { grd.SelectedRowIndex };
            List<GridIndexesData> lstCells = new List<GridIndexesData>() { new GridIndexesData(grd.SelectedRowIndex, grd.SelectedColumnIndex) };
            GridElementSelectionChangeEventArgs newArgs = new GridElementSelectionChangeEventArgs(lstRows, lstCells);

            return new GridElementSelectionChangeEventArgs(lstRows, lstCells);
        }

        /// <summary>
        /// Renders the Click event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachClickEvent(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

            StringBuilder stringRenderer = new StringBuilder();
            GridElement grd = visualElement as GridElement;
            if (grd != null)
            {
                if (grd.HasClickListeners)
                {

                    stringRenderer.Append(@" element: 'el',click: function () {");
                    stringRenderer.AppendFormat("VT_raiseEvent('{0}', 'Click' ,{{}});}},", VisualElement.GetElementId(grd));
                    JavaScriptCode javaSCriptCode = new JavaScriptCode(stringRenderer.ToString());
                    jsonWriter.WriteRaw(javaSCriptCode.ToString());
                }
            }
        }



        /// <summary>
        ///Extract The Event Args from client's Event Args
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The event's name</param>
        /// <param name="args">Client's args</param>
        /// <returns></returns>
        protected override EventArgs ExtractComboCellSelectedIndexChangedCallbackEventArgs(RenderingContext context, VisualElement visualElement, string serverEventName, JObject args)
        {
            return new GridComboBoxColumnEventArgs(Convert.ToInt32(args.GetValue("cellIndex")), Convert.ToInt32(args.GetValue("rowIndex")), args.GetValue("value").ToString(), Convert.ToInt32(args.GetValue("index")));
        }

        /// <summary>
        /// Gets the cell border style for the DataGridView(value for Row).
        /// </summary>
        /// <param name="cellBorderStyle">sort order . </param>
        public static bool ReviseCellBorderStyleRow(GridCellBorderStyle cellBorderStyle)
        {
            bool cellStyle = true;
            switch (cellBorderStyle)
            {
                case GridCellBorderStyle.Single:
                case GridCellBorderStyle.SingleHorizontal:
                    cellStyle = true;
                    break;
                case GridCellBorderStyle.None:
                case GridCellBorderStyle.SingleVertical:
                    cellStyle = false;
                    break;
            }
            return cellStyle;
        }

        /// <summary>
        /// Gets the cell border style for the DataGridView(value for Column).
        /// </summary>
        /// <param name="cellBorderStyle">sort order . </param>
        public static bool ReviseCellBorderStyleColumn(GridCellBorderStyle cellBorderStyle)
        {
            bool cellStyle = true;
            switch (cellBorderStyle)
            {
                case GridCellBorderStyle.Single:
               case GridCellBorderStyle.SingleVertical:
                    cellStyle = true;
                    break;
                case GridCellBorderStyle.None:
                case GridCellBorderStyle.SingleHorizontal:
                    cellStyle = false;
                    break;
              
            }
            return cellStyle;
        }
    }
}
