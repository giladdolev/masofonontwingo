﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [RendererDescription(typeof(BindingSourceElement))]
	public class BindingSourceElementExtJSRenderer : DefaultComponentExtJSRenderer
    {
    }
}
