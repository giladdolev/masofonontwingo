﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewNumericField), "DataViewTextFieldExtJSRenderer", "numbercolumn", "Ext.grid.column.Number")]
    public class DataViewNumericFieldExtJSRenderer : DataViewNumericFieldExtJSRendererBase
    {
        protected override string GetFormattedValue(DataViewField fieldElement, string value)
        {
            DataViewTextField textField = fieldElement as DataViewTextField;
            if (textField != null && !String.IsNullOrEmpty(textField.Format))
            {
                return String.Format("Ext.util.Format.number({0}, '{1}');", value, GetSenchaFormat(textField));
            }
            return base.GetFormattedValue(fieldElement, value);
        }

        private string GetSenchaFormat(DataViewTextField textField)
        {
            string format = String.Empty;
            if (textField != null)
            {
                format = textField.Format;
                int separatorIndex = format.IndexOf(';');
                if (separatorIndex >= 0)
                {
                    return format.Substring(0, separatorIndex);
                }
            }

            return format;
        }
    }
}
