﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridButtonButtonColumn), "GridColumnExtJSRenderer", "widgetcolumn", "Ext.grid.column.Widget")]


    public class GridButtonButtonColumnExtJSRenderer : GridButtonButtonColumnExtJSRendererBase
    {

        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderProperties(context, visualElement, jsonWriter);
            StringBuilder stringRenderer = new StringBuilder();
            // Render GridTextButtonColumn widget .
            GridButtonButtonColumn gridButtonButtonColumn = visualElement as GridButtonButtonColumn;

            if (gridButtonButtonColumn != null)
            {
                GridElement grd = gridButtonButtonColumn.Parent as GridElement;
                jsonWriter.WritePropertyName("widget");
                // Render chart object
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteProperty("xtype", "container");
                    jsonWriter.WritePropertyName("layout");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("type", "hbox");
                    }
                    jsonWriter.WritePropertyName("items");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WriteProperty("xtype", "button");
                        if (grd.ReadOnly == true)
                        {
                            jsonWriter.WriteProperty("disabled", JavaScriptCode.GetBool(grd.ReadOnly));
                        }
                        jsonWriter.WriteProperty("text", gridButtonButtonColumn.Button1Text);
                        if (grd != null)
                        {
                            if (gridButtonButtonColumn.HasButton1ClickListeners)
                            {
                                stringRenderer.Append(@" {click: VT_CreateMarkedListener(function () {");
                                stringRenderer.AppendFormat("VT_raiseEvent('{0}', 'Button1Click' ,{{}});}})}}", VisualElement.GetElementId(gridButtonButtonColumn));
                                JavaScriptCode javaSCriptCode = new JavaScriptCode(stringRenderer.ToString());
                                jsonWriter.WriteProperty("listeners", javaSCriptCode);
                            }
                        }
                        jsonWriter.WriteEndObject();
                        jsonWriter.WriteStartObject();
                        jsonWriter.WriteProperty("xtype", "button");
                        if (grd.ReadOnly == true)
                        {
                            jsonWriter.WriteProperty("disabled", JavaScriptCode.GetBool(grd.ReadOnly));
                        }
                        jsonWriter.WriteProperty("text", gridButtonButtonColumn.Button2Text);
                        if (grd != null)
                        {
                            stringRenderer = new StringBuilder();
                            if (gridButtonButtonColumn.HasButton2ClickListeners)
                            {
                                stringRenderer.Append(@" {click: VT_CreateMarkedListener(function () {");
                                stringRenderer.AppendFormat("VT_raiseEvent('{0}', 'Button2Click' ,{{}});}})}}", VisualElement.GetElementId(gridButtonButtonColumn));
                                JavaScriptCode javaSCriptCode = new JavaScriptCode(stringRenderer.ToString());
                                jsonWriter.WriteProperty("listeners", javaSCriptCode);
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                }
            }
        }
    }
}
