﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewComboField), "DataViewFieldExtJSRenderer", "column", "Ext.grid.column.ColumnView")]
    [ExtJSPropertyDescription("ValueMember", PropertyInitializeCode = "valueField = <#= element.ValueMember #>")]
    [ExtJSPropertyDescription("DisplayMember", PropertyInitializeCode = "displayField = <#= element.DisplayMember #>")]    
    [ExtJSPropertyDescription("QueryMode", PropertyInitializeCode = "queryMode = \"local\"")]
    public class DataViewComboFieldExtJSRenderer : DataViewComboFieldExtJSRendererBase
    {
        /// <summary>
        /// Gets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return "combobox";
            }
        }

        /// <summary>
        /// Renders the editor properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderEditorProperties(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderEditorProperties(context, visualElement, jsonWriter);

            jsonWriter.WriteProperty("__copy", new JavaScriptCode("['valueField','displayField','store','queryMode']"));
        }


        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Render data source
            this.RenderDataSourceProperty(renderingContext, visualElement, jsonWriter);
        }
    }
}
