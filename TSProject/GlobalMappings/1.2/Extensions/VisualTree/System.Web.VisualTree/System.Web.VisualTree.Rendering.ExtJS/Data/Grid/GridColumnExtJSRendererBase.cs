using System.ComponentModel.Composition;
using System.Globalization;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class GridColumnExtJSRendererBase : GridEditorColumnBaseExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.grid.column.Column";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "gridcolumn";
			}
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "HeaderText":
					this.RenderHeaderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SortMode":
					this.RenderSortModePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ReadOnly":
					this.RenderReadOnlyPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MaxInputLength":
					this.RenderMaxInputLengthPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MenuDisabled":
					this.RenderMenuDisabledPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DisplayIndex":
					this.RenderDisplayIndexPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Icon":
					this.RenderIconPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "TabIndex":
					this.RenderTabIndexPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AutoSizeMode":
					this.RenderAutoSizeModePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "DataMember":
					this.RenderDataMemberPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Format":
					this.RenderFormatPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnFilter":
					this.RenderColumnFilterPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MaxWidth":
					this.RenderMaxWidthPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Locked":
					this.RenderLockedPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderHeaderTextProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSortModeProperty(renderingContext, visualElement, jsonWriter);
			this.RenderReadOnlyProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMaxInputLengthProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMenuDisabledProperty(renderingContext, visualElement, jsonWriter);
			this.RenderDisplayIndexProperty(renderingContext, visualElement, jsonWriter);
			this.RenderIconProperty(renderingContext, visualElement, jsonWriter);
			this.RenderAutoSizeModeProperty(renderingContext, visualElement, jsonWriter);
			this.RenderDataMemberProperty(renderingContext, visualElement, jsonWriter);
			this.RenderFormatProperty(renderingContext, visualElement, jsonWriter);
			this.RenderColumnFilterProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMaxWidthProperty(renderingContext, visualElement, jsonWriter);
			this.RenderLockedProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the Resize event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachResizeEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderResizeEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseResizeEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "_width", "_height", "_oldWidth", "_oldHeight", "eOpts"}, 
									() => {  
										// Create the server callback code.Resize
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Resize", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{oldWidth : _oldWidth,oldHeight : _oldHeight,width : _width, height : _height }}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("resize", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Resize event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseResizeEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Resize event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderResizeEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasResizeListeners);
		}


		/// <summary>
        /// Extracts the Resize event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractResizeCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new ResizeEventArgs(args.Value<System.Int32>("width"),args.Value<System.Int32>("height"),args.Value<System.Int32>("oldWidth"),args.Value<System.Int32>("oldHeight"));
		}


		/// <summary>
        /// Renders the Resize event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderResizeAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderResizeEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseResizeEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "_width", "_height", "_oldWidth", "_oldHeight", "eOpts"}, 
                            () => {  
										// Create the server callback code.Resize
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Resize", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{oldWidth : _oldWidth,oldHeight : _oldHeight,width : _width, height : _height }}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('resize', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the Resize event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderResizeRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderResizeEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "resize");
            }
        }

		/// <summary>
        /// Renders the HeaderText property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderHeaderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.HeaderText))
					{
						jsonWriter.WriteProperty("text",  element.HeaderText );
					}
				}
			}

		}

		/// <summary>
        /// Renders the HeaderText property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderHeaderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setText('{1}');", new JavaScriptCode(componentVariable),  element.HeaderText );
			}

		}


		/// <summary>
        /// Renders the SortMode property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSortModeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If SortMode value is not default
					if (!(element is GridRowHeader))
					{
						jsonWriter.WriteProperty("sortable",  GridColumnExtJSRenderer.ReviseSortMode(element));
					}
				}
			}

		}

		/// <summary>
        /// Renders the SortMode property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSortModePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.sortable = {1}", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool( GridColumnExtJSRenderer.ReviseSortMode(element) ) );
			}

		}


		/// <summary>
        /// Renders the ReadOnly property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderReadOnlyProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ReadOnly property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderReadOnlyPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MaxInputLength property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaxInputLengthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the MaxInputLength property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaxInputLengthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@" {0}.setEditor( {{editable : {1}}});", new JavaScriptCode(componentVariable),  element.MaxInputLength );
			}

		}


		/// <summary>
        /// Renders the MenuDisabled property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMenuDisabledProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MenuDisabled value is not default
					if (element.MenuDisabled == true && !(element is GridRowHeader))
					{
						jsonWriter.WriteProperty("menuDisabled",  element.MenuDisabled );
					}
				}
			}

		}

		/// <summary>
        /// Renders the MenuDisabled property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMenuDisabledPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the DisplayIndex property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDisplayIndexProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the DisplayIndex property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDisplayIndexPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Icon property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderIconProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Icon value is not default
					if (element.Icon != null && element.Icon.Source != String.Empty || element.HeaderText != String.Empty )
					{
						jsonWriter.WriteProperty("text",  GridColumnExtJSRenderer.ReviseIconProperty(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Icon property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderIconPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setText(""{1}"");", new JavaScriptCode(componentVariable),  GridColumnExtJSRenderer.ReviseIconProperty(element) );
			}

		}


		/// <summary>
        /// Renders the Width property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Width value is not default
					if (!(element is GridRowHeader))
					{
						jsonWriter.WriteProperty("width",  (element.PixelWidth == 0 ? 100 : element.PixelWidth) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the TabIndex property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTabIndexProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If TabIndex value is not default
					if (element.TabStop && element.TabIndex >= 0 && !(element is GridRowHeader))
					{
						jsonWriter.WriteProperty("tabIndex",  element.TabIndex );
					}
				}
			}

		}

		/// <summary>
        /// Renders the TabIndex property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTabIndexPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setTabIndex({1});", new JavaScriptCode(componentVariable),  ControlElementExtJSRenderer.ReviseTabIndex(element));
			}

		}


		/// <summary>
        /// Renders the Resizable property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderResizableProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Resizable value is not default
					if (!(element is GridRowHeader))
					{
						jsonWriter.WriteProperty("resizable",  bool.Parse(Convert.ToString(element.Resizable)) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the AutoSizeMode property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAutoSizeModeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If AutoSizeMode value is not default
					if (element.AutoSizeMode == GridAutoSizeColumnMode.Fill)
					{
						jsonWriter.WriteProperty("flex",  1 );
					}
				}
			}

		}

		/// <summary>
        /// Renders the AutoSizeMode property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAutoSizeModePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the DataMember property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDataMemberProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If DataMember value is not default
					if (!(element is GridRowHeader))
					{
						// If property is valid
						if(ValidateProperty(element.DataMember))
						{
							jsonWriter.WriteProperty("dataIndex",  element.DataMember );
						}
					}
				}
			}

		}

		/// <summary>
        /// Renders the DataMember property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataMemberPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Format property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderFormatProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Format value is not default
					if (!String.IsNullOrEmpty(element.Format))
					{
						// If property is valid
						if(ValidateProperty(element.Format))
						{
							jsonWriter.WriteProperty("formatter",  element.Format );
						}
					}
				}
			}

		}

		/// <summary>
        /// Renders the Format property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderFormatPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.formatter = '{1}';", new JavaScriptCode(componentVariable),  element.Format );
			}

		}


		/// <summary>
        /// Renders the ColumnFilter property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderColumnFilterProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If ColumnFilter value is not default
					if (element.ColumnFilter == true)
					{
						jsonWriter.WriteProperty("filter", new JavaScriptCode(String.Format(@"{{xtype:'textfield',filterName:'{0}'}}",  element.ClientID)));
					}
				}
			}

		}

		/// <summary>
        /// Renders the ColumnFilter property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnFilterPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MaxWidth property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaxWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If MaxWidth value is not default
					if (element.MaxWidth != 500)
					{
						jsonWriter.WriteProperty("maxWidth",  element.MaxWidth );
					}
				}
			}

		}

		/// <summary>
        /// Renders the MaxWidth property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaxWidthPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the Locked property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderLockedProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridColumn element = visualElement as System.Web.VisualTree.Elements.GridColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Locked value is not default
					if (element.Index >-1 && element.Index < element.GridElement.FrozenCols)
					{
						jsonWriter.WritePropertyWithRawValue("locked", @"true");
					}
				}
			}

		}

		/// <summary>
        /// Renders the Locked property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLockedPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


	}
}