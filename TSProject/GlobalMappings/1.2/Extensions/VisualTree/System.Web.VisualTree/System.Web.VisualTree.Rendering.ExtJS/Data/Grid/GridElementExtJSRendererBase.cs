using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class GridElementExtJSRedererBase : ControlElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "VT.ux.VTGridPanel";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "VTgridpanel";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the CellDoubleClick event attach code.
			RenderAttachCellDoubleClickEvent(context, visualElement, writer); 
			// Renders the CellButtonClick event attach code.
			RenderAttachCellButtonClickEvent(context, visualElement, writer); 
			// Renders the SelectionChanged event attach code.
			RenderAttachSelectionChangedEvent(context, visualElement, writer); 
			// Renders the BeforeSelectionChanged event attach code.
			RenderAttachBeforeSelectionChangedEvent(context, visualElement, writer); 
			// Renders the ColumnHeaderMouseClick event attach code.
			RenderAttachColumnHeaderMouseClickEvent(context, visualElement, writer); 
			// Renders the CellContentClick event attach code.
			RenderAttachCellContentClickEvent(context, visualElement, writer); 
			// Renders the CellFormatting event attach code.
			RenderAttachCellFormattingEvent(context, visualElement, writer); 
			// Renders the ColumnStateChanged event attach code.
			RenderAttachColumnStateChangedEvent(context, visualElement, writer); 
			// Renders the DataBindingComplete event attach code.
			RenderAttachDataBindingCompleteEvent(context, visualElement, writer); 
			// Renders the AddedColumn event attach code.
			RenderAttachAddedColumnEvent(context, visualElement, writer); 
			// Renders the CurrentCellChanged event attach code.
			RenderAttachCurrentCellChangedEvent(context, visualElement, writer); 
			// Renders the CellBeginEdit event attach code.
			RenderAttachCellBeginEditEvent(context, visualElement, writer); 
			// Renders the CellClick event attach code.
			RenderAttachCellClickEvent(context, visualElement, writer); 
			// Renders the CellEndEdit event attach code.
			RenderAttachCellEndEditEvent(context, visualElement, writer); 
			// Renders the ColumnHeaderMouseRightClick event attach code.
			RenderAttachColumnHeaderMouseRightClickEvent(context, visualElement, writer); 
			// Renders the RowClick event attach code.
			RenderAttachRowClickEvent(context, visualElement, writer); 
			// Renders the CellMouseDown event attach code.
			RenderAttachCellMouseDownEvent(context, visualElement, writer); 
			// Renders the MouseRightClick event attach code.
			RenderAttachMouseRightClickEvent(context, visualElement, writer); 
			// Renders the BeforeContextMenuShow event attach code.
			RenderAttachBeforeContextMenuShowEvent(context, visualElement, writer); 
			// Renders the ColumnDisplayIndexChanged event attach code.
			RenderAttachColumnDisplayIndexChangedEvent(context, visualElement, writer); 
			// Renders the CellMouseUp event attach code.
			RenderAttachCellMouseUpEvent(context, visualElement, writer); 
			// Renders the RowHeaderMouseRightClick event attach code.
			RenderAttachRowHeaderMouseRightClickEvent(context, visualElement, writer); 
			// Renders the CancelEdit event attach code.
			RenderAttachCancelEditEvent(context, visualElement, writer); 
			// Renders the CellkeyDown event attach code.
			RenderAttachCellkeyDownEvent(context, visualElement, writer); 
			// Renders the ComboCellSelectedIndexChanged event attach code.
			RenderAttachComboCellSelectedIndexChangedEvent(context, visualElement, writer); 
			// Renders the ClientEvent event attach code.
			RenderAttachClientEventEvent(context, visualElement, writer); 
			// Renders the KeyDown event attach code.
			RenderAttachKeyDownEvent(context, visualElement, writer); 
			// Renders the dataChanged event attach code.
			RenderAttachdataChangedEvent(context, visualElement, writer); 
			// Renders the ContextMenuClick event attach code.
			RenderAttachContextMenuClickEvent(context, visualElement, writer); 
			// Renders the Afterrender event attach code.
			RenderAttachAfterrenderEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "CellDoubleClick":
					return this.ExtractCellDoubleClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellButtonClick":
					return this.ExtractCellButtonClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "SelectionChanged":
					return this.ExtractSelectionChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeSelectionChanged":
					return this.ExtractBeforeSelectionChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnHeaderMouseClick":
					return this.ExtractColumnHeaderMouseClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellContentClick":
					return this.ExtractCellContentClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellFormatting":
					return this.ExtractCellFormattingCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnStateChanged":
					return this.ExtractColumnStateChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "DataBindingComplete":
					return this.ExtractDataBindingCompleteCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "AddedColumn":
					return this.ExtractAddedColumnCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CurrentCellChanged":
					return this.ExtractCurrentCellChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellBeginEdit":
					return this.ExtractCellBeginEditCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellClick":
					return this.ExtractCellClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellEndEdit":
					return this.ExtractCellEndEditCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnHeaderMouseRightClick":
					return this.ExtractColumnHeaderMouseRightClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "RowClick":
					return this.ExtractRowClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellMouseDown":
					return this.ExtractCellMouseDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "MouseRightClick":
					return this.ExtractMouseRightClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "BeforeContextMenuShow":
					return this.ExtractBeforeContextMenuShowCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ColumnDisplayIndexChanged":
					return this.ExtractColumnDisplayIndexChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellMouseUp":
					return this.ExtractCellMouseUpCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "RowHeaderMouseRightClick":
					return this.ExtractRowHeaderMouseRightClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CancelEdit":
					return this.ExtractCancelEditCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "CellkeyDown":
					return this.ExtractCellkeyDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ComboCellSelectedIndexChanged":
					return this.ExtractComboCellSelectedIndexChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ClientEvent":
					return this.ExtractClientEventCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "KeyDown":
					return this.ExtractKeyDownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "dataChanged":
					return this.ExtractdataChangedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "ContextMenuClick":
					return this.ExtractContextMenuClickCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Afterrender":
					return this.ExtractAfterrenderCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the viewConfig content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderViewConfigContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderViewConfigContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			this.RenderAllowTrackMouseOverProperty(context, visualElement, styleWriter);
		}


		/// <summary>
        /// Renders the body style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderBodyStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderBodyStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}
			this.RenderBackColorProperty(context, visualElement, styleWriter);
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "ClientRowTemplate":
					this.RenderClientRowTemplatePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "UseFixedHeaders":
					this.RenderUseFixedHeadersPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "RowHeaderVisible":
					this.RenderRowHeaderVisiblePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SelectionMode":
					this.RenderSelectionModePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MultiSelect":
					this.RenderMultiSelectPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Content":
					this.RenderContentPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CanSelect":
					this.RenderCanSelectPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "FrozenCols":
					this.RenderFrozenColsPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnFilterInfo":
					this.RenderColumnFilterInfoPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SortableColumns":
					this.RenderSortableColumnsPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AllowColumnSelection":
					this.RenderAllowColumnSelectionPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "EnableLocking":
					this.RenderEnableLockingPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ShowAutoFilterColumnItem":
					this.RenderShowAutoFilterColumnItemPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AllowTrackMouseOver":
					this.RenderAllowTrackMouseOverPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SearchPanel":
					this.RenderSearchPanelPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellBorderStyle":
					this.RenderCellBorderStylePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BackColor":
					this.RenderBackColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Text":
					this.RenderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Cols":
					this.RenderColsPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnHeadersVisible":
					this.RenderColumnHeadersVisiblePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderClientRowTemplateProperty(renderingContext, visualElement, jsonWriter);
			this.RenderUseFixedHeadersProperty(renderingContext, visualElement, jsonWriter);
			this.RenderRowHeaderVisibleProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSelectionModeProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMultiSelectProperty(renderingContext, visualElement, jsonWriter);
			this.RenderContentProperty(renderingContext, visualElement, jsonWriter);
			this.RenderCanSelectProperty(renderingContext, visualElement, jsonWriter);
			this.RenderFrozenColsProperty(renderingContext, visualElement, jsonWriter);
			this.RenderColumnFilterInfoProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSortableColumnsProperty(renderingContext, visualElement, jsonWriter);
			this.RenderAllowColumnSelectionProperty(renderingContext, visualElement, jsonWriter);
			this.RenderEnableLockingProperty(renderingContext, visualElement, jsonWriter);
			this.RenderShowAutoFilterColumnItemProperty(renderingContext, visualElement, jsonWriter);
			this.RenderSearchPanelProperty(renderingContext, visualElement, jsonWriter);
			this.RenderCellBorderStyleProperty(renderingContext, visualElement, jsonWriter);
			this.RenderColsProperty(renderingContext, visualElement, jsonWriter);
			this.RenderColumnHeadersVisibleProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the CellDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderCellDoubleClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseCellDoubleClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"list", "td", "cellIndex", "record", "tr", "rowIndex"}, 
									() => {  
										// Create the server callback code.CellDoubleClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowIndex : rowIndex, cellIndex : cellIndex}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("celldblclick", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of CellDoubleClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellDoubleClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellDoubleClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellDoubleClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellDoubleClickListeners);
		}


		/// <summary>
        /// Extracts the CellDoubleClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellDoubleClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridElementCellEventArgs(args.Value<System.Int32>("cellIndex"), args.Value<System.Int32>("rowIndex"));
		}


		/// <summary>
        /// Renders the CellDoubleClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellDoubleClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellDoubleClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellDoubleClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"list", "td", "cellIndex", "record", "tr", "rowIndex"}, 
                            () => {  
										// Create the server callback code.CellDoubleClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellDoubleClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowIndex : rowIndex, cellIndex : cellIndex}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('celldblclick', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the CellDoubleClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellDoubleClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellDoubleClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "celldblclick");
            }
        }

		/// <summary>
        /// Renders the CellButtonClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellButtonClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellButtonClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellButtonClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellButtonClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellButtonClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellButtonClickListeners);
		}


		/// <summary>
        /// Extracts the CellButtonClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellButtonClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellButtonClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellButtonClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellButtonClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellButtonClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the SelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderSelectionChangedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

						// Revise the event behavior
						eventBehaviorType = this.ReviseSelectionChangedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "selection", "eOpts"}, 
									() => {  
										// Create the client callback code.SelectionChanged
										if (!String.IsNullOrEmpty(element.ClientSelectionChanged))
										{
											javaScriptBuilder.AppendRawValue(element.ClientSelectionChanged);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.SelectionChanged
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "SelectionChanged", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rows : VT_GetSelectedRowIndeces(view,selection), cells : VT_GetSelectedCellsIndeces(selection)}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("selectionchange", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of SelectionChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseSelectionChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the SelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientSelectionChanged) || element.HasSelectionChangedListeners);
		}


		/// <summary>
        /// Extracts the SelectionChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractSelectionChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the SelectionChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderSelectionChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

                // Revise the event behavior
                eventBehaviorType = this.ReviseSelectionChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "selection", "eOpts"}, 
                            () => {  
										// Create the client callback code.SelectionChanged
										if (!String.IsNullOrEmpty(element.ClientSelectionChanged))
										{
											javaScriptBuilder.AppendRawValue(element.ClientSelectionChanged);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.SelectionChanged
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "SelectionChanged", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rows : VT_GetSelectedRowIndeces(view,selection), cells : VT_GetSelectedCellsIndeces(selection)}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('selectionchange', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the SelectionChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderSelectionChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "selectionchange");
            }
        }

		/// <summary>
        /// Renders the BeforeSelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeSelectionChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeSelectionChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeSelectionChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeSelectionChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasBeforeSelectionChangedListeners);
		}


		/// <summary>
        /// Extracts the BeforeSelectionChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeSelectionChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeSelectionChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectionChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeSelectionChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeSelectionChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ColumnHeaderMouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnHeaderMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderColumnHeaderMouseClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseColumnHeaderMouseClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"ct", "column", "e", "t", "eOpts"}, 
									() => {  
										// Create the server callback code.ColumnHeaderMouseClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "ColumnHeaderMouseClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{columnIndex : VT_GridActualColumnIndex(column, column.getIndex())}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("headerclick", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of ColumnHeaderMouseClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnHeaderMouseClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnHeaderMouseClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnHeaderMouseClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ColumnHeaderMouseClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnHeaderMouseClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridElementCellEventArgs(args.Value<System.Int32>("columnIndex"), -1);
		}


		/// <summary>
        /// Renders the ColumnHeaderMouseClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderColumnHeaderMouseClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseColumnHeaderMouseClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"ct", "column", "e", "t", "eOpts"}, 
                            () => {  
										// Create the server callback code.ColumnHeaderMouseClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "ColumnHeaderMouseClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{columnIndex : VT_GridActualColumnIndex(column, column.getIndex())}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('headerclick', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the ColumnHeaderMouseClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderColumnHeaderMouseClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "headerclick");
            }
        }

		/// <summary>
        /// Renders the CellContentClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellContentClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderCellContentClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseCellContentClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "record", "item", "index", "e", "eOpts"}, 
									() => {  
										// Create the server callback code.CellContentClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellContentClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowInedx : index,columnIndex : VT_GetColumnFromItemClickEvent(e)}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("itemclick", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of CellContentClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellContentClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellContentClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellContentClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellContentClickListeners);
		}


		/// <summary>
        /// Extracts the CellContentClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellContentClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridElementCellEventArgs(args.Value<System.Int32>("columnIndex"),args.Value<System.Int32>("rowInedx"));
		}


		/// <summary>
        /// Renders the CellContentClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellContentClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellContentClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellContentClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "record", "item", "index", "e", "eOpts"}, 
                            () => {  
										// Create the server callback code.CellContentClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellContentClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{rowInedx : index,columnIndex : VT_GetColumnFromItemClickEvent(e)}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('itemclick', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the CellContentClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellContentClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellContentClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "itemclick");
            }
        }

		/// <summary>
        /// Renders the CellFormatting event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellFormattingEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellFormatting event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellFormattingEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellFormatting event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellFormattingEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellFormattingListeners);
		}


		/// <summary>
        /// Extracts the CellFormatting event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellFormattingCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellFormatting event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellFormattingAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellFormatting event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellFormattingRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ColumnStateChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnStateChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ColumnStateChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnStateChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnStateChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnStateChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasColumnStateChangedListeners);
		}


		/// <summary>
        /// Extracts the ColumnStateChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnStateChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ColumnStateChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnStateChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ColumnStateChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnStateChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the DataBindingComplete event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDataBindingCompleteEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of DataBindingComplete event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDataBindingCompleteEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the DataBindingComplete event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDataBindingCompleteEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasDataBindingCompleteListeners);
		}


		/// <summary>
        /// Extracts the DataBindingComplete event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDataBindingCompleteCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the DataBindingComplete event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataBindingCompleteAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the DataBindingComplete event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataBindingCompleteRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the AddedColumn event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachAddedColumnEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of AddedColumn event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAddedColumnEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the AddedColumn event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderAddedColumnEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasAddedColumnListeners);
		}


		/// <summary>
        /// Extracts the AddedColumn event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractAddedColumnCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the AddedColumn event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAddedColumnAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the AddedColumn event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAddedColumnRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CurrentCellChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCurrentCellChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CurrentCellChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCurrentCellChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CurrentCellChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCurrentCellChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCurrentCellChangedListeners);
		}


		/// <summary>
        /// Extracts the CurrentCellChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCurrentCellChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CurrentCellChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCurrentCellChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CurrentCellChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCurrentCellChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellBeginEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellBeginEditEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellBeginEdit event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellBeginEditEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellBeginEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellBeginEditEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the CellBeginEdit event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellBeginEditCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellBeginEdit event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellBeginEditAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellBeginEdit event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellBeginEditRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the CellClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellEndEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellEndEditEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderCellEndEditEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseCellEndEditEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "editor", "context", "eOpts"}, 
									() => {
                                        javaScriptBuilder.AppendFormattedRaw(@"if(this.xtype == 'VTgridpanel'){{if(!this.hasDirtyCell){{return;}}}}");
                                        javaScriptBuilder.AppendSemicolon();

                                        // Create the client callback code.CellEndEdit
                                        if (!String.IsNullOrEmpty(element.ClientCellEndEdit))
										{
											javaScriptBuilder.AppendRawValue(element.ClientCellEndEdit);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.CellEndEdit
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellEndEdit", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : (editor)?editor.value:null, rowIndex : editor.rowIdx, dataindex : editor.field}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("edit", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of CellEndEdit event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellEndEditEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellEndEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellEndEditEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (!String.IsNullOrEmpty(element.ClientCellEndEdit) || element.HasCellEndEditListeners);
		}


		/// <summary>
        /// Extracts the CellEndEdit event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellEndEditCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridCellEventArgs(args.Value<System.String>("newValue"), args.Value<System.String>("dataindex"), args.Value<System.Int32>("rowIndex"));
		}


		/// <summary>
        /// Renders the CellEndEdit event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellEndEditAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellEndEditEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellEndEditEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "editor", "context", "eOpts"}, 
                            () => {
                                javaScriptBuilder.AppendFormattedRaw(@"if(this.xtype == 'VTgridpanel'){{if(!this.hasDirtyCell){{return;}}}}");
                                javaScriptBuilder.AppendSemicolon();
                                // Create the client callback code.CellEndEdit
                                if (!String.IsNullOrEmpty(element.ClientCellEndEdit))
										{
											javaScriptBuilder.AppendRawValue(element.ClientCellEndEdit);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.CellEndEdit
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellEndEdit", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : (editor)?editor.value:null,rowIndex : editor.rowIdx, dataindex : editor.field}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('edit', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the CellEndEdit event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellEndEditRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellEndEditEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "edit");
            }
        }

		/// <summary>
        /// Renders the ColumnHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderColumnHeaderMouseRightClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseColumnHeaderMouseRightClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "column", "e", "t", "eOpts"}, 
									() => {  
										// Create the server callback code.ColumnHeaderMouseRightClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "ColumnHeaderMouseRightClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{columnIndex : VT_GridActualColumnIndex(column, column.getIndex())}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("headercontextmenu", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of ColumnHeaderMouseRightClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnHeaderMouseRightClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ColumnHeaderMouseRightClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnHeaderMouseRightClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridElementCellEventArgs(args.Value<System.Int32>("columnIndex"), -1);
		}


		/// <summary>
        /// Renders the ColumnHeaderMouseRightClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseRightClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderColumnHeaderMouseRightClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseColumnHeaderMouseRightClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "column", "e", "t", "eOpts"}, 
                            () => {  
										// Create the server callback code.ColumnHeaderMouseRightClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "ColumnHeaderMouseRightClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{columnIndex : VT_GridActualColumnIndex(column, column.getIndex())}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('headercontextmenu', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the ColumnHeaderMouseRightClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeaderMouseRightClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderColumnHeaderMouseRightClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "headercontextmenu");
            }
        }

		/// <summary>
        /// Renders the RowClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachRowClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of RowClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseRowClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the RowClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderRowClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasRowClickListeners);
		}


		/// <summary>
        /// Extracts the RowClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractRowClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the RowClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the RowClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the CellMouseDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderCellMouseDownEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

						// Revise the event behavior
						eventBehaviorType = this.ReviseCellMouseDownEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
									() => {  
										// Create the client callback code.CellMouseDown
										if (!String.IsNullOrEmpty(element.ClientCellMouseDown))
										{
											javaScriptBuilder.AppendRawValue(element.ClientCellMouseDown);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.CellMouseDown
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellMouseDown", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{cellIndex : VT_GetActualCellIndex(view,cellIndex), rowIndex : rowIndex , X : e.getX(), Y : e.getY(), button : e.button , delta : e.getWheelDelta() ,ctrlKey : e.ctrlKey, shiftKey : e.shiftKey}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("cellmousedown", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of CellMouseDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellMouseDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellMouseDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellMouseDownEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the CellMouseDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellMouseDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridCellMouseEventArgs(args.Value<System.Int32>("cellIndex"), args.Value<System.Int32>("rowIndex"), args.Value<System.Int32>("X"), args.Value<System.Int32>("Y"), new MouseEventArgs( GridElementExtJSRenderer.ReviseMouseButton(args.Value<System.Int32>("button")),0,0,0,args.Value<System.Int32>("delta")),args.Value<System.Boolean>("ctrlKey"),args.Value<System.Boolean>("shiftKey"));
		}


		/// <summary>
        /// Renders the CellMouseDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellMouseDownEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Delayed;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellMouseDownEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
                            () => {  
										// Create the client callback code.CellMouseDown
										if (!String.IsNullOrEmpty(element.ClientCellMouseDown))
										{
											javaScriptBuilder.AppendRawValue(element.ClientCellMouseDown);
											javaScriptBuilder.AppendSemicolon();
										}
										// Create the server callback code.CellMouseDown
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellMouseDown", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{cellIndex : VT_GetActualCellIndex(view,cellIndex), rowIndex : rowIndex , X : e.getX(), Y : e.getY(), button : e.button , delta : e.getWheelDelta() ,ctrlKey : e.ctrlKey, shiftKey : e.shiftKey}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('cellmousedown', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the CellMouseDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellMouseDownEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "cellmousedown");
            }
        }

		/// <summary>
        /// Renders the MouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderMouseRightClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseMouseRightClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "e", "eOpts"}, 
									() => {  
										// Create the server callback code.MouseRightClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "MouseRightClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("containercontextmenu", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of MouseRightClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseMouseRightClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the MouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasMouseRightClickListeners);
		}


		/// <summary>
        /// Extracts the MouseRightClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractMouseRightClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the MouseRightClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseRightClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderMouseRightClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseMouseRightClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "e", "eOpts"}, 
                            () => {  
										// Create the server callback code.MouseRightClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "MouseRightClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('containercontextmenu', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the MouseRightClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMouseRightClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderMouseRightClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "containercontextmenu");
            }
        }

		/// <summary>
        /// Renders the BeforeContextMenuShow event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachBeforeContextMenuShowEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of BeforeContextMenuShow event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseBeforeContextMenuShowEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the BeforeContextMenuShow event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderBeforeContextMenuShowEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasBeforeContextMenuShowListeners);
		}


		/// <summary>
        /// Extracts the BeforeContextMenuShow event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractBeforeContextMenuShowCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the BeforeContextMenuShow event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeContextMenuShowAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the BeforeContextMenuShow event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBeforeContextMenuShowRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ColumnDisplayIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachColumnDisplayIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderColumnDisplayIndexChangedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseColumnDisplayIndexChangedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"ct", "column", "fromIdx", "toIdx", "eOpts"}, 
									() => {  
										// Create the server callback code.ColumnDisplayIndexChanged
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "ColumnDisplayIndexChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{fromIndex : fromIdx , toIndex : column.fullColumnIndex }}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("columnmove", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of ColumnDisplayIndexChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseColumnDisplayIndexChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ColumnDisplayIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderColumnDisplayIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ColumnDisplayIndexChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractColumnDisplayIndexChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridColumnMoveEventArgs(args.Value<System.Int32>("fromIndex"), args.Value<System.Int32>("toIndex"));
		}


		/// <summary>
        /// Renders the ColumnDisplayIndexChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnDisplayIndexChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderColumnDisplayIndexChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseColumnDisplayIndexChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"ct", "column", "fromIdx", "toIdx", "eOpts"}, 
                            () => {  
										// Create the server callback code.ColumnDisplayIndexChanged
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "ColumnDisplayIndexChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{fromIndex : fromIdx , toIndex : column.fullColumnIndex }}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('columnmove', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the ColumnDisplayIndexChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnDisplayIndexChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderColumnDisplayIndexChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "columnmove");
            }
        }

		/// <summary>
        /// Renders the CellMouseUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellMouseUpEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of CellMouseUp event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellMouseUpEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellMouseUp event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellMouseUpEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellMouseUpListeners);
		}


		/// <summary>
        /// Extracts the CellMouseUp event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellMouseUpCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CellMouseUp event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseUpAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the CellMouseUp event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellMouseUpRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the RowHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachRowHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderRowHeaderMouseRightClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseRowHeaderMouseRightClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
									() => {  
										// Create the server callback code.RowHeaderMouseRightClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "RowHeaderMouseRightClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{colIndex : cellIndex, rowIndex : rowIndex}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("cellcontextmenu", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of RowHeaderMouseRightClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseRowHeaderMouseRightClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the RowHeaderMouseRightClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderRowHeaderMouseRightClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasRowHeaderMouseRightClickListeners);
		}


		/// <summary>
        /// Extracts the RowHeaderMouseRightClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractRowHeaderMouseRightClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridElementCellEventArgs(args.Value<System.Int32>("colIndex"), args.Value<System.Int32>("rowIndex"));
		}


		/// <summary>
        /// Renders the RowHeaderMouseRightClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowHeaderMouseRightClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderRowHeaderMouseRightClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseRowHeaderMouseRightClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
                            () => {  
										// Create the server callback code.RowHeaderMouseRightClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "RowHeaderMouseRightClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{colIndex : cellIndex, rowIndex : rowIndex}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('cellcontextmenu', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the RowHeaderMouseRightClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowHeaderMouseRightClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderRowHeaderMouseRightClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "cellcontextmenu");
            }
        }

		/// <summary>
        /// Renders the CancelEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCancelEditEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderCancelEditEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseCancelEditEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.CancelEdit
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CancelEdit", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("canceledit", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of CancelEdit event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCancelEditEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CancelEdit event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCancelEditEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCancelEditListeners);
		}


		/// <summary>
        /// Extracts the CancelEdit event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCancelEditCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the CancelEdit event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCancelEditAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCancelEditEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCancelEditEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.CancelEdit
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CancelEdit", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('canceledit', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the CancelEdit event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCancelEditRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCancelEditEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "canceledit");
            }
        }

		/// <summary>
        /// Renders the CellkeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachCellkeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderCellkeyDownEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseCellkeyDownEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
									() => {  
										// Create the server callback code.CellkeyDown
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "CellkeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey(),CellIndex : cellIndex,RowIndex : rowIndex }}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("cellkeydown", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of CellkeyDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseCellkeyDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the CellkeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderCellkeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasCellkeyDownListeners);
		}


		/// <summary>
        /// Extracts the CellkeyDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractCellkeyDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridElementCellEventArgs((Keys)args.Value<System.Int32>("key"),args.Value<System.Int32>("CellIndex"),args.Value<System.Int32>("RowIndex"),args.Value<System.String>("widgetDataMember"));
		}


		/// <summary>
        /// Renders the CellkeyDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellkeyDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellkeyDownEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseCellkeyDownEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
                            () => {  
										// Create the server callback code.CellkeyDown
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "CellkeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : e.getKey(),CellIndex : cellIndex,RowIndex : rowIndex }}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('cellkeydown', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the CellkeyDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellkeyDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderCellkeyDownEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "cellkeydown");
            }
        }

		/// <summary>
        /// Renders the ComboCellSelectedIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachComboCellSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of ComboCellSelectedIndexChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseComboCellSelectedIndexChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ComboCellSelectedIndexChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderComboCellSelectedIndexChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasComboCellSelectedIndexChangedListeners);
		}


		/// <summary>
        /// Extracts the ComboCellSelectedIndexChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractComboCellSelectedIndexChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the ComboCellSelectedIndexChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderComboCellSelectedIndexChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the ComboCellSelectedIndexChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderComboCellSelectedIndexChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the ClientEvent event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachClientEventEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderClientEventEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseClientEventEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "value"}, 
									() => {  
										// Create the server callback code.ClientEvent
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "ClientEvent", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("clientEvent", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of ClientEvent event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseClientEventEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ClientEvent event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderClientEventEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the ClientEvent event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractClientEventCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new ValueChangedArgs<string>(args.Value<System.String>("newValue"));
		}


		/// <summary>
        /// Renders the ClientEvent event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClientEventAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderClientEventEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseClientEventEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "value"}, 
                            () => {  
										// Create the server callback code.ClientEvent
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "ClientEvent", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{newValue : value}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('clientEvent', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the ClientEvent event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClientEventRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderClientEventEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "clientEvent");
            }
        }

		/// <summary>
        /// Renders the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderKeyDownEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseKeyDownEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"grid", "e", "ar", "bt", "code"}, 
									() => {  
										// Create the server callback code.KeyDown
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "KeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : code.keyCode}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("itemkeydown", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of KeyDown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseKeyDownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the KeyDown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderKeyDownEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the KeyDown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractKeyDownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new KeyDownEventArgs(args.Value<System.Int32>("key"));
		}


		/// <summary>
        /// Renders the KeyDown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyDownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderKeyDownEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseKeyDownEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"grid", "e", "ar", "bt", "code"}, 
                            () => {  
										// Create the server callback code.KeyDown
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "KeyDown", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{key : code.keyCode}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('itemkeydown', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the KeyDown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderKeyDownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderKeyDownEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "itemkeydown");
            }
        }

		/// <summary>
        /// Renders the dataChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachdataChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderdataChangedEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.RevisedataChangedEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.dataChanged
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "dataChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("dataChanged", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of dataChanged event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType RevisedataChangedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the dataChanged event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderdataChangedEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasdataChangedListeners);
		}


		/// <summary>
        /// Extracts the dataChanged event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractdataChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the dataChanged event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderdataChangedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderdataChangedEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.RevisedataChangedEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.dataChanged
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "dataChanged", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('dataChanged', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the dataChanged event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderdataChangedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderdataChangedEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "dataChanged");
            }
        }

		/// <summary>
        /// Renders the ContextMenuClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachContextMenuClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderContextMenuClickEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseContextMenuClickEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
									() => {  
										// Create the server callback code.ContextMenuClick
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "ContextMenuClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{value : e.getXY().toString()}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("cellcontextmenu", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of ContextMenuClick event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseContextMenuClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.GridElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the ContextMenuClick event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderContextMenuClickEvent(RenderingContext context, System.Web.VisualTree.Elements.GridElement element)
		{
			return (element.HasContextMenuClickListeners);
		}


		/// <summary>
        /// Extracts the ContextMenuClick event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractContextMenuClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new GridContextMenuCellEventArgs(args.Value<System.String>("value"));
		}


		/// <summary>
        /// Renders the ContextMenuClick event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderContextMenuClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderContextMenuClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseContextMenuClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{"view", "td", "cellIndex", "record", "tr", "rowIndex", "e", "eOpts"}, 
                            () => {  
										// Create the server callback code.ContextMenuClick
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "ContextMenuClick", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{value : e.getXY().toString()}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('cellcontextmenu', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the ContextMenuClick event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderContextMenuClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderContextMenuClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "cellcontextmenu");
            }
        }

		/// <summary>
        /// Renders the Afterrender event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderAfterrenderEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Queued;

						// Revise the event behavior
						eventBehaviorType = this.ReviseAfterrenderEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"grid"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"VT_GridAfterRender(this, '{0}')",  WindowElementExtJSRenderer.ReviseContextMenuStripID(element) );
										javaScriptBuilder.AppendSemicolon();

										// Create the server callback code.Afterrender
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Afterrender", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("afterrender", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Afterrender event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAfterrenderEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Afterrender event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the Afterrender event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractAfterrenderCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Afterrender event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Afterrender event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderAfterrenderEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "CellDoubleClick":
					this.RenderCellDoubleClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SelectionChanged":
					this.RenderSelectionChangedAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnHeaderMouseClick":
					this.RenderColumnHeaderMouseClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellContentClick":
					this.RenderCellContentClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellEndEdit":
					this.RenderCellEndEditAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnHeaderMouseRightClick":
					this.RenderColumnHeaderMouseRightClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellMouseDown":
					this.RenderCellMouseDownAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MouseRightClick":
					this.RenderMouseRightClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnDisplayIndexChanged":
					this.RenderColumnDisplayIndexChangedAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "RowHeaderMouseRightClick":
					this.RenderRowHeaderMouseRightClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CancelEdit":
					this.RenderCancelEditAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellkeyDown":
					this.RenderCellkeyDownAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ClientEvent":
					this.RenderClientEventAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "KeyDown":
					this.RenderKeyDownAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "dataChanged":
					this.RenderdataChangedAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ContextMenuClick":
					this.RenderContextMenuClickAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "CellDoubleClick":
					this.RenderCellDoubleClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "SelectionChanged":
					this.RenderSelectionChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnHeaderMouseClick":
					this.RenderColumnHeaderMouseClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellContentClick":
					this.RenderCellContentClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellEndEdit":
					this.RenderCellEndEditRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnHeaderMouseRightClick":
					this.RenderColumnHeaderMouseRightClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellMouseDown":
					this.RenderCellMouseDownRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MouseRightClick":
					this.RenderMouseRightClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ColumnDisplayIndexChanged":
					this.RenderColumnDisplayIndexChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "RowHeaderMouseRightClick":
					this.RenderRowHeaderMouseRightClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CancelEdit":
					this.RenderCancelEditRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "CellkeyDown":
					this.RenderCellkeyDownRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ClientEvent":
					this.RenderClientEventRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "KeyDown":
					this.RenderKeyDownRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "dataChanged":
					this.RenderdataChangedRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ContextMenuClick":
					this.RenderContextMenuClickRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the ClientRowTemplate property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderClientRowTemplateProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ClientRowTemplate property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderClientRowTemplatePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the UseFixedHeaders property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderUseFixedHeadersProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("fixedHeaders",  element.UseFixedHeaders );
				}
			}

		}

		/// <summary>
        /// Renders the UseFixedHeaders property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderUseFixedHeadersPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setFixedHeaders({1})", new JavaScriptCode(componentVariable),  element.UseFixedHeaders );
			}

		}


		/// <summary>
        /// Renders the RowHeaderVisible property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderRowHeaderVisibleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the RowHeaderVisible property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderRowHeaderVisiblePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the SelectionMode property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSelectionModeProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the SelectionMode property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSelectionModePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MultiSelect property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMultiSelectProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the MultiSelect property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMultiSelectPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"multiSelect ={0}",  element.MultiSelect );
			}

		}


		/// <summary>
        /// Renders the Content property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderContentProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.Content))
					{
						jsonWriter.WriteProperty("html",  Convert.ToString(element.Content) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Content property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderContentPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the CanSelect property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderCanSelectProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If CanSelect value is not default
					if (!element.CanSelect)
					{
						jsonWriter.WriteProperty("disableSelection",  !element.CanSelect );
					}
				}
			}

		}

		/// <summary>
        /// Renders the CanSelect property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCanSelectPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.getSelectionModel().setLocked({1})", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(!element.CanSelect));
			}

		}


		/// <summary>
        /// Renders the FrozenCols property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderFrozenColsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the FrozenCols property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderFrozenColsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"try{{
var index = 0 ;
if({1})
{{
    // start from the second column in client .
    // cause the first column is a row  header . 
    index = 1;
}}

for(i=0 ; i < {2} || {0}.getColumns()[i + index].locked == true ; i++){{
    if(i >= {3}){{
        {0}.unlock({0}.getColumns()[i + index]);
}}
else{{
        {0}.lock({0}.getColumns()[i + index]);
}}   
 
}}
}}catch(err){{
console.log(err.message);
}}

    ", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(element.RowHeaderVisible) ,  element.FrozenCols ,  element.FrozenCols );
			}

		}


		/// <summary>
        /// Renders the ColumnFilterInfo property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderColumnFilterInfoProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ColumnFilterInfo property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnFilterInfoPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.store.filter('{1}', '{2}');", new JavaScriptCode(componentVariable), element.ColumnFilterInfo.FilterInfoKey, element.ColumnFilterInfo.FilterInfoValue);
			}

		}


		/// <summary>
        /// Renders the SortableColumns property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSortableColumnsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If SortableColumns value is not default
					if (element.SortableColumns == false)
					{
						jsonWriter.WriteProperty("sortableColumns",  JavaScriptCode.GetBool(element.SortableColumns));
					}
				}
			}

		}

		/// <summary>
        /// Renders the SortableColumns property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSortableColumnsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the AllowColumnSelection property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAllowColumnSelectionProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the AllowColumnSelection property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAllowColumnSelectionPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.selModel.setColumnSelect({1});", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.AllowColumnSelection));
			}

		}


		/// <summary>
        /// Renders the EnableLocking property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnableLockingProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If EnableLocking value is not default
					if (element.FrozenCols > 0 && element.EnableLocking != false)
					{
						jsonWriter.WriteProperty("enableLocking", element.EnableLocking );
					}
				}
			}

		}

		/// <summary>
        /// Renders the EnableLocking property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnableLockingPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ShowAutoFilterColumnItem property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderShowAutoFilterColumnItemProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ShowAutoFilterColumnItem property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderShowAutoFilterColumnItemPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.getPlugin('filterplugin').setVisible({1})", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.ShowAutoFilterColumnItem) );
			}

		}


		/// <summary>
        /// Renders the AllowTrackMouseOver property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAllowTrackMouseOverProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.ViewConfig)
				{
					// If AllowTrackMouseOver value is not default
					if (element.AllowTrackMouseOver != true)
					{
						jsonWriter.WritePropertyWithRawValue("trackOver", JavaScriptCode.GetBool(element.AllowTrackMouseOver) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the AllowTrackMouseOver property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAllowTrackMouseOverPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the SearchPanel property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderSearchPanelProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If SearchPanel value is not default
					if (element.SearchPanel !=false)
					{
						jsonWriter.WriteProperty("searchPanel",  element.SearchPanel );
					}
				}
			}

		}

		/// <summary>
        /// Renders the SearchPanel property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderSearchPanelPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setSearchPanel({1});", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.SearchPanel) );
			}

		}


		/// <summary>
        /// Renders the CellBorderStyle property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderCellBorderStyleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If CellBorderStyle value is not default
					if (element.CellBorderStyle != GridCellBorderStyle.SingleVertical)
					{
						jsonWriter.WriteProperty("rowLines",  GridElementExtJSRenderer.ReviseCellBorderStyleRow(element.CellBorderStyle) );
					}
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If CellBorderStyle value is not default
					if (element.CellBorderStyle != GridCellBorderStyle.SingleVertical)
					{
						jsonWriter.WriteProperty("columnLines",  GridElementExtJSRenderer.ReviseCellBorderStyleColumn(element.CellBorderStyle) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the CellBorderStyle property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCellBorderStylePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the BackColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBackColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.BodyStyle)
				{
					// If BackColor value is not default
					if (element.BackColor != Color.Empty)
					{
						jsonWriter.WriteRaw(String.Format("backgroundColor:{0};", NormalizeStyleValue( ColorTranslator.ToHtml(element.BackColor) )));
					}
				}
			}

		}

		/// <summary>
        /// Renders the BackColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderBackColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setBodyStyle('background-color','{1}');", new JavaScriptCode(componentVariable),  ColorTranslator.ToHtml(element.BackColor) );
			}

		}


		/// <summary>
        /// Renders the Text property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Text value is not default
					if (!String.IsNullOrEmpty(element.Text))
					{
						// If property is valid
						if(ValidateProperty(element.Text))
						{
							jsonWriter.WriteProperty("title",  element.Text );
						}
					}
				}
			}

		}

		/// <summary>
        /// Renders the Text property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setTitle('{1}')", new JavaScriptCode(componentVariable),  element.Text );
			}

		}


		/// <summary>
        /// Renders the Cols property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderColsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
			}

		}

		/// <summary>
        /// Renders the Cols property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ColumnHeadersVisible property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderColumnHeadersVisibleProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If ColumnHeadersVisible value is not default
					if (!element.ColumnHeadersVisible)
					{
						jsonWriter.WriteProperty("hideHeaders",  !element.ColumnHeadersVisible );
					}
				}
			}

		}

		/// <summary>
        /// Renders the ColumnHeadersVisible property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderColumnHeadersVisiblePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "SetCellStyle":
					return this.ExtractSetCellStyleMethodCallbackData(context, eventObject);						
				case "SetComboboxSelectedIndex":
					return this.ExtractSetComboboxSelectedIndexMethodCallbackData(context, eventObject);
                case "UpdateCellValue":
                    return this.ExtractUpdateCellValueMethodCallbackData(context, eventObject);
                case "SwapColumns":
					return this.ExtractSwapColumnsMethodCallbackData(context, eventObject);						
				case "Sort":
					return this.ExtractSortMethodCallbackData(context, eventObject);						
				case "ScrollToRow":
					return this.ExtractScrollToRowMethodCallbackData(context, eventObject);						
				case "SetWidgetStyle":
					return this.ExtractSetWidgetStyleMethodCallbackData(context, eventObject);						
				case "SetProtectCell":
					return this.ExtractSetProtectCellMethodCallbackData(context, eventObject);						
				case "SelectCell":
					return this.ExtractSelectCellMethodCallbackData(context, eventObject);						
				case "SetRowVisibility":
					return this.ExtractSetRowVisibilityMethodCallbackData(context, eventObject);						
				case "EditCellByPosition":
					return this.ExtractEditCellByPositionMethodCallbackData(context, eventObject);						
				case "SetCellBackgroundColor":
					return this.ExtractSetCellBackgroundColorMethodCallbackData(context, eventObject);						
				case "SetRowHeaderBackgroundColor":
					return this.ExtractSetRowHeaderBackgroundColorMethodCallbackData(context, eventObject);						
				case "SetRowHeaderBackgroundImage":
					return this.ExtractSetRowHeaderBackgroundImageMethodCallbackData(context, eventObject);						
				case "SetRowHeaderCornerIcon":
					return this.ExtractSetRowHeaderCornerIconMethodCallbackData(context, eventObject);						
				case "SetButtonVisibility":
					return this.ExtractSetButtonVisibilityMethodCallbackData(context, eventObject);						
				case "ContextMenuShow":
					return this.ExtractContextMenuShowMethodCallbackData(context, eventObject);						
				case "ClearFilter":
					return this.ExtractClearFilterMethodCallbackData(context, eventObject);						
				case "SetSelection":
					return this.ExtractSetSelectionMethodCallbackData(context, eventObject);
                case "CompleteEdit":
                    return this.ExtractCompleteEditMethodCallbackData(context, eventObject);
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "SetCellStyle":
					this.RenderSetCellStyleMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                case "UpdateCellValue":
                    this.RenderUpdateCellValueMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                case "SetComboboxSelectedIndex":
					this.RenderSetComboboxSelectedIndexMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SwapColumns":
					this.RenderSwapColumnsMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "Sort":
					this.RenderSortMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "ScrollToRow":
					this.RenderScrollToRowMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetWidgetStyle":
					this.RenderSetWidgetStyleMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetProtectCell":
					this.RenderSetProtectCellMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SelectCell":
					this.RenderSelectCellMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowVisibility":
					this.RenderSetRowVisibilityMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "EditCellByPosition":
					this.RenderEditCellByPositionMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetCellBackgroundColor":
					this.RenderSetCellBackgroundColorMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowHeaderBackgroundColor":
					this.RenderSetRowHeaderBackgroundColorMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowHeaderBackgroundImage":
					this.RenderSetRowHeaderBackgroundImageMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetRowHeaderCornerIcon":
					this.RenderSetRowHeaderCornerIconMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetButtonVisibility":
					this.RenderSetButtonVisibilityMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "ContextMenuShow":
					this.RenderContextMenuShowMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "ClearFilter":
					this.RenderClearFilterMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetSelection":
					this.RenderSetSelectionMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                case "CompleteEdit":
                    this.RenderCompleteEditMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the SetCellStyle method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetCellStyleMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridCellInfo data = (System.Web.VisualTree.Elements.GridCellInfo)methodData;
					builder.AppendFormattedRawValue(@"var row={0};var col={1};var styleName={2};var append={3};try{{if(col>-1&&row>-1){{var _record=cmp.store.getAt(row);if((cmp.getColumns().length>0)&&(col<cmp.getColumns().length)&&(_record!=null)){{var _column=cmp.getColumns()[col];if(_column!=null&&_column.getView().getRow(_record)!=null){{var cell=cmp.getView().getCell(_record,_column);if(append===!1){{var oldCss=cell.dom.classList[cell.dom.classList.length-1];cell.removeCls(oldCss)}}
    cell.addCls(styleName)}}}}}}}}catch(err){{console.log(""error :- setCellStyle -> ""+err.message)}}", data.RowIndex , data.ColumnIndex , data.CssClass , data.AllowAppend );
				}
			}

		}


		/// <summary>
        /// Gets the method SetCellStyle callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetCellStyleMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}

        /// Renders the UpdateCellValue method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderUpdateCellValueMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Collections.ArrayList data = (System.Collections.ArrayList)methodData;
                    builder.AppendFormattedRawValue(@"VT_GridUpdateCellValue({0},{1},{2},{3})", new JavaScriptCode(componentVariableName), data[0], data[1], data[2]);
                }
            }

        }


        /// <summary>
        /// Gets the method UpdateCellValue callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractUpdateCellValueMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }



        /// Renders the SetComboboxSelectedIndex method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetComboboxSelectedIndexMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridComboBoxCellSelectionData data = (System.Web.VisualTree.Elements.GridComboBoxCellSelectionData)methodData;
					builder.AppendFormattedRawValue(@"setTimeout(function(){{
    var colIndex = {1};
    var rowIndex = {2};
    var selectedIndex = {3};
    var grid = {0};

    try {{
        if(grid != null){{
            // spreadsheet mode 
            if(grid.getSelectionModel().type == 'spreadsheet'){{
                colIndex++;
            }}

            var plugin = grid.getPlugin('cellEditing');
            var editor = grid.getColumns()[colIndex].getEditor();
            var itemsCount = 0;
            // If ther is a plugin
            if(plugin && editor)
            {{

                // Get the total items count inside the picker
                itemsCount = editor.getPickerStore().totalCount;
                if(selectedIndex < itemsCount)
                {{

                    // Get the record we want to modify from store
                    var rec = grid.getStore().getAt(rowIndex);

                    // Get the new value field from the given selected index
                    var newValue = editor.getPickerStore().data.items[selectedIndex].data[editor.displayField];

                    //Update the record value
                    rec.data[editor.dataIndex] = newValue;

                    // Get the column header
                    var colHeader = grid.view.getHeaderCt().getHeaderAtIndex(colIndex);

                    // Get the context of the plugin
                    var context = plugin.getEditingContext(rec,colHeader);

                    // Edit and refresh view
                    plugin.startEdit(rec,colHeader);
                    plugin.completeEdit();
                    grid.view.refresh();

                }}
            }}
           
        }}

    }} catch (e) {{
        console.log('GridColumn.SetSelectedIndex error : ' + e.message);
    }}
}},300);
", new JavaScriptCode(componentVariableName),  data.ColumnIndex ,  data.RowIndex ,  data.SelectedIndex);
				}
			}

		}


		/// <summary>
        /// Gets the method SetComboboxSelectedIndex callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetComboboxSelectedIndexMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SwapColumns method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSwapColumnsMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridColumnData data = (System.Web.VisualTree.Elements.GridColumnData)methodData;
					builder.AppendFormattedRawValue(@"var ColumnHeaderCT = {0}.columns[0].getRootHeaderCt();
if(ColumnHeaderCT){{
    ColumnHeaderCT.move({1}, {2});
}}
}}catch(err){{
    console.log(""error :- "" + err.message);
}}", new JavaScriptCode(componentVariableName),  data.PreviousDisplayIndex ,  data.NewDisplayIndex );
				}
			}

		}


		/// <summary>
        /// Gets the method SwapColumns callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSwapColumnsMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the Sort method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSortMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridColumnData data = (System.Web.VisualTree.Elements.GridColumnData)methodData;
					builder.AppendFormattedRawValue(@"try{{
var newSorter = [{{ property: {1}, direction: {2} }}];{0}.getStore().setSorters(newSorter);
}}catch(err){{
    console.log(""error :- "" + err.message);
}}", new JavaScriptCode(componentVariableName),  data.ColumnDataIndex.ToString(), GridElementExtJSRenderer.ReviseSortOrder(data.ColumnSortOrder));
				}
			}

		}


		/// <summary>
        /// Gets the method Sort callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSortMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the ScrollToRow method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderScrollToRowMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridRowData data = (System.Web.VisualTree.Elements.GridRowData)methodData;
					builder.AppendFormattedRawValue(@"try{{
    var row =  {1};

    // ToDo check of out of range 
    {0}.getView().focusRow(row);

}}catch(err){{
    console.log(""error :- "" + err.message);
}}", new JavaScriptCode(componentVariableName),  data.RowIndex );
				}
			}

		}


		/// <summary>
        /// Gets the method ScrollToRow callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractScrollToRowMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetWidgetStyle method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetWidgetStyleMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridCellInfo data = (System.Web.VisualTree.Elements.GridCellInfo)methodData;
					builder.AppendFormattedRawValue(@"try{{var row={0};var col={1};var styleName={2};var append={3};var dataMemberindex={4};if(cmp.getColumns()[0].xtype==""SPGridColumnwidgetcolumn""){{if(col>-1&&row>-1&&dataMemberindex>-1){{if(cmp.getColumns().length>0&&col<cmp.getColumns().length&&cmp.getColumns()[col].Rows.length>0&&dataMemberindex>-1&&dataMemberindex<cmp.getColumns()[col].Rows[row].length){{if(cmp.getColumns()[col].Rows[row][dataMemberindex].rendered==!1){{if(cmp.getColumns()[col].Rows[row][dataMemberindex].protoEl==null)
{{return}}}}
    if(cmp.getColumns()[col].Rows[row][dataMemberindex].rendered==!0){{if(cmp.getColumns()[col].Rows[row][dataMemberindex].el==null)
    {{return}}}}
    if(append==!1){{cmp.getColumns()[col].Rows[row][dataMemberindex].removeCls(styleName)}}
    cmp.getColumns()[col].Rows[row][dataMemberindex].addCls(styleName)}}}}}}}}catch(err){{console.log(""error :- ""+err.message)}}", data.RowIndex , data.ColumnIndex , data.CssClass , data.AllowAppend , data.DataMemberIndex );
				}
			}

		}


		/// <summary>
        /// Gets the method SetWidgetStyle callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetWidgetStyleMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetProtectCell method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetProtectCellMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridIndexesData data = (System.Web.VisualTree.Elements.GridIndexesData)methodData;
					builder.AppendFormattedRawValue(@"try{{
    var row = {1};
    var col = {2};
    var protect = {3};
    {0}.getView().getCell(row, {0}.getColumns()[col]).Protect = protect;
}}catch(err)
{{
    console.log(""error :- grid setProtect ""+err.message);
}}", new JavaScriptCode(componentVariableName),  data.RowIndex ,  data.ColumnIndex ,  data.Protect );
				}
			}

		}


		/// <summary>
        /// Gets the method SetProtectCell callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetProtectCellMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SelectCell method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSelectCellMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridIndexesData data = (System.Web.VisualTree.Elements.GridIndexesData)methodData;
					builder.AppendFormattedRawValue(@"try{{
    var row = {1};
    var col = {2};
    {0}.getSelectionModel().selectCells([col, row],[col, row]);
    // scroll to the selected column .

    var position = column.getPosition();
  
    {0}.scrollByDeltaX(position[0]);

}}catch(err){{
    console.log(""error :- "" + err.message);
}}
								", new JavaScriptCode(componentVariableName),  data.RowIndex ,  data.ColumnIndex );
				}
			}

		}


		/// <summary>
        /// Gets the method SelectCell callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSelectCellMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowVisibility method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowVisibilityMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridRowData data = (System.Web.VisualTree.Elements.GridRowData)methodData;
					builder.AppendFormattedRawValue(@"try{{
if({1})
{{
    {0}.getView().getRow({2}).style.display = 'none';
}}else{{
    {0}.getView().getRow({3}).style.display = '';
}}

}}catch(err){{
    console.log(""error :- "" + err.message);
}}", new JavaScriptCode(componentVariableName), !data.RowVisibility, data.RowIndex, data.RowIndex);
				}
			}

		}


		/// <summary>
        /// Gets the method SetRowVisibility callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowVisibilityMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the EditCellByPosition method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderEditCellByPositionMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridIndexesData data = (System.Web.VisualTree.Elements.GridIndexesData)methodData;
					builder.AppendFormattedRawValue(@"var rowIndex = {1};
var columnIndex = {2};
var plugin = {0}.getPlugin('cellEditing');
if(rowIndex >=0 && columnIndex >= 0)
{{
    if(plugin){{
        plugin.startEditByPosition({{row: rowIndex, column: columnIndex}});
    }}
}}
 ", new JavaScriptCode(componentVariableName),  data.RowIndex ,  data.ColumnIndex );
				}
			}

		}


		/// <summary>
        /// Gets the method EditCellByPosition callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractEditCellByPositionMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetCellBackgroundColor method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetCellBackgroundColorMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridCellBackGroundData data = (System.Web.VisualTree.Elements.GridCellBackGroundData)methodData;
					builder.AppendFormattedRawValue(@"Ext.getElementById({0}.getView().getCell({1}, {0}.getColumns()[{2}]).id).style.backgroundColor = {3}", new JavaScriptCode(componentVariableName),  data.RowIndex ,  data.ColumnIndex ,  ColorTranslator.ToHtml(data.BackgroundColor) );
				}
			}

		}


		/// <summary>
        /// Gets the method SetCellBackgroundColor callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetCellBackgroundColorMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowHeaderBackgroundColor method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowHeaderBackgroundColorMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridCellBackGroundData data = (System.Web.VisualTree.Elements.GridCellBackGroundData)methodData;
					builder.AppendFormattedRawValue(@"Ext.getElementById({0}.getView().getCell({1}, {0}.getColumns()[{2}]).id).style.backgroundColor = {3}", new JavaScriptCode(componentVariableName),  data.RowIndex ,  data.ColumnIndex ,  ColorTranslator.ToHtml(data.BackgroundColor) );
				}
			}

		}


		/// <summary>
        /// Gets the method SetRowHeaderBackgroundColor callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowHeaderBackgroundColorMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowHeaderBackgroundImage method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowHeaderBackgroundImageMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridCellBackGroundData data = (System.Web.VisualTree.Elements.GridCellBackGroundData)methodData;
					builder.AppendFormattedRawValue(@"{0}.getView().getCell({1}, {0}.getColumns()[0]).setStyle({2} , {0}.getColumns()[0]).setStyle('background', 'url({3}) no-repeat center');", new JavaScriptCode(componentVariableName),  data.RowIndex ,  data.RowIndex ,  FormatHelper.FormatResource(context, data.BackGroundImage) );
				}
			}

		}


		/// <summary>
        /// Gets the method SetRowHeaderBackgroundImage callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowHeaderBackgroundImageMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetRowHeaderCornerIcon method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetRowHeaderCornerIconMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.GridCellBackGroundData data = (System.Web.VisualTree.Elements.GridCellBackGroundData)methodData;
					builder.AppendFormattedRawValue(@"{0}.getColumns()[0].items.items[0].setSrc({1});", new JavaScriptCode(componentVariableName),  FormatHelper.FormatResource(context, element.HeaderColumns[0].Icon) );
				}
			}

		}


		/// <summary>
        /// Gets the method SetRowHeaderCornerIcon callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetRowHeaderCornerIconMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetButtonVisibility method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetButtonVisibilityMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method SetButtonVisibility callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetButtonVisibilityMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the ContextMenuShow method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderContextMenuShowMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method ContextMenuShow callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractContextMenuShowMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the ClearFilter method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderClearFilterMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"{0}.store.clearFilter();", new JavaScriptCode(componentVariableName));
				}
			}

		}


		/// <summary>
        /// Gets the method ClearFilter callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractClearFilterMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetSelection method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetSelectionMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"try{{
var store = {0} && {0}.getStore && {0}.getStore(),
    selectedIndex = {1};

if(store && (store.isLoading() || store.getCount() <= selectedIndex || store.loadTimer)){{
    
    store.on('load', function(){{
        if(store.getCount() > selectedIndex){{
            {0}.getSelectionModel().select(selectedIndex);
            {0}.getView().focusRow(selectedIndex);
        }}        

    }}, {0},  {{single: true}})
}} else {{
    {0}.getSelectionModel().select(selectedIndex);
    {0}.getView().focusRow(selectedIndex);
}}

}}catch(err){{
    console.log(""error :- "" + err.message);
}}", new JavaScriptCode(componentVariableName),  element.SelectedRowIndex );
				}
			}

		}


		/// <summary>
        /// Gets the method SetSelection callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetSelectionMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the RaiseUpdateClient method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected override void RenderRaiseUpdateClientMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"var grd = {0};
if (grd != null && grd.ownerGrid != null) {{
    setTimeout(function (grd) {{
        grd.ownerGrid.fireEvent(""dataChanged"", grd.ownerGrid);
    }},
                                   20,
                                   grd);
}}", new JavaScriptCode(componentVariableName));
				}
			}

		}

        /// Renders the CompleteEdit method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderCompleteEditMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get typed element
            System.Web.VisualTree.Elements.GridElement element = visualElement as System.Web.VisualTree.Elements.GridElement;

            // If there is a valid typed element
            if (element != null)
            {
                var builder = getJavaScriptBuilder(true);
                if (builder != null)
                {
                    System.Object data = (System.Object)methodData;
                    builder.AppendFormattedRawValue(@"VT_GridCompleteEdit({0})", new JavaScriptCode(componentVariableName));
                }
            }

        }


        /// <summary>
        /// Gets the method CompleteEdit callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractCompleteEditMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
            return null;
        }

        /// <summary>
        /// Gets the method RaiseUpdateClient callback data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractRaiseUpdateClientMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}