﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewDropDownField), "DataViewFieldExtJSRenderer", "column", "Ext.grid.column.ColumnView")]
    [ExtJSPropertyDescription("DropDownControl")]
    public class DataViewDropDownFieldExtJSRenderer : DataViewDropDownFieldExtJSRendererBase
    {

        /// <summary>
        /// Gets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return "dropdownfield";
            }
        }

        /// <summary>
        /// Renders the editor properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderEditorProperties(RenderingContext context, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderEditorProperties(context, visualElement, jsonWriter);
            jsonWriter.WriteProperty("matchFieldWidth", false);
            DataViewDropDownField dropDownField = visualElement as DataViewDropDownField;

            if (dropDownField != null)
            {
                jsonWriter.WriteProperty("valueMember", dropDownField.ValueMember);
                ControlElement dropDownControl = dropDownField.DropDownControl;

                if (dropDownControl != null)
                {
                    VisualElementExtJSRenderer renderer = VisualTreeManager.GetRenderer(context, dropDownControl) as VisualElementExtJSRenderer;

                    if (renderer != null)
                    {
                        jsonWriter.WritePropertyName("dropdownctl");

                        renderer.Render(context, dropDownControl, jsonWriter);
                    }


                }


            }
        }

    } 
}
