﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewDateField), "DataViewTextFieldExtJSRenderer", "column", "Ext.grid.column.ColumnView")]
    public class DataViewDateFieldExtJSRenderer : DataViewDateFieldExtJSRendererBase
    {

        /// <summary>
        /// Gets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public override string EditorXType
        {
            get { return "datefield"; }
        }

        protected override string GetFormattedValue(DataViewField fieldElement, string value)
        {
            DataViewTextField textField = fieldElement as DataViewTextField;
            if (textField != null && !String.IsNullOrEmpty(textField.Format))
            {
                return String.Format("Ext.util.Format.date({0}, '{1}');", value, textField.Format);
            }
            return base.GetFormattedValue(fieldElement, value);
        }
    }
}
