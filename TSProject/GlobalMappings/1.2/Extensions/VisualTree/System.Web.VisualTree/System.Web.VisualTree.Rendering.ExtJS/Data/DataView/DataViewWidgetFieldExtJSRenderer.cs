﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

    public class DataViewWidgetFieldExtJSRenderer : DataViewFieldExtJSRenderer
    {

        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.grid.column.Widget";
            }
        }



        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "widgetcolumn";
            }
        }

        public override void RenderProperties(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(context, visualElement, jsonWriter);

            RenderWidgetProperty(context, visualElement, jsonWriter);
        }

        private void RenderWidgetProperty(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            jsonWriter.WritePropertyName("widget");
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
            {
                jsonWriter.WriteProperty("xtype", WidgetXType);

                InitializeWidgetProperties(context, visualElement, jsonWriter);
            }
        }

        protected virtual void InitializeWidgetProperties(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
        }

        protected virtual string WidgetXType
        {
            get
            {
                return String.Empty;
            }
        }
    }
}