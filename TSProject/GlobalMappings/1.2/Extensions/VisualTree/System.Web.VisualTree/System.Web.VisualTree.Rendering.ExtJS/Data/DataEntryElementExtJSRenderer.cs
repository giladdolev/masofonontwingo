﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataEntryElement), "ControlElementExtJSRenderer", "form", "Ext.form.Panel")]
    [ExtJSPropertyDescription("DataSource")]
    [ExtJSEventDescription("Updated", ClientEventName = "fieldchange", ClientHandlerCallbackData = "values=form.getValues(true, true, true)", ClientEventHandlerParameterNames = "form")]
    public class DataEntryElementExtJSRenderer : DataEntryElementExtJSRendererBase
    {

        /// <summary>
        /// Indicates if we should render the Updated event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <returns></returns>
        protected override bool ShouldRenderUpdatedEvent(RenderingContext context, DataEntryElement element)
        {
            return true;
        }


        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderContentProperties(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(context, visualElement, jsonWriter);

            // Create the attributes to add to all fields and create 'fieldchange' custom event firing
            jsonWriter.WritePropertyName("defaults");
            jsonWriter.WriteStartObject();
            jsonWriter.WriteProperty("anchor", "100%");
            jsonWriter.WritePropertyName("listeners");
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyWithRawValue("change", "function(){var form = " + this.GetContainingFormScript() + "; if(form) form.fireEvent('fieldchange', form);}");
            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndObject();
        }

        /// <summary>
        /// Gets the containing form script.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetContainingFormScript()
        {
            return "this.findParentByType('form')";
        }

        /// <summary>
        /// Extracts the Updated event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractUpdatedCallbackEventArgs(RenderingContext context, VisualElement visualElement, string serverEventName, JObject args)
        {
            // Get values send from client
            string valuesQueryString = args.Value<string>("values");

            // If there are values send from client
            if (!string.IsNullOrEmpty(valuesQueryString))
            {
                // Get query string values
                NameValueCollection values = HttpUtility.ParseQueryString(valuesQueryString);

                // If there is a valid values collection
                if (values != null)
                {
                    // Get the data entry 
                    DataEntryElement dataEntry = visualElement as DataEntryElement;

                    // If there is a valid data entry 
                    if (dataEntry != null)
                    {
                        // Get item
                        object item = dataEntry.ItemSource;

                        // If there is a valid item
                        if (item != null)
                        {
                            // Loop all item properties
                            foreach (PropertyDescriptor property in dataEntry.ItemProperties)
                            {
                                // Get the property name
                                string propertyName = property.Name;

                                // If there is a valid property name
                                if (!string.IsNullOrEmpty(propertyName))
                                {
                                    // Get the property value 
                                    string propertyValue = values.Get(propertyName);

                                    // If there is a valid property value
                                    if (propertyValue != null)
                                    {
                                        // Get property type
                                        Type propertyType = property.PropertyType;

                                        // If there is a valid property type
                                        if (propertyType != null)
                                        {
                                            // Get property type converter
                                            TypeConverter propertyTypeConverter = TypeDescriptor.GetConverter(propertyType);

                                            // If there is a valid 
                                            if (propertyTypeConverter != null)
                                            {
                                                // Set value based on conversion
                                                property.SetValue(item, propertyTypeConverter.ConvertFromString(propertyValue));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Return empty arguments
            return EventArgs.Empty;
        }

        /// <summary>
        /// Revise the event behavior type of Updated event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        /// <returns></returns>
        protected override ExtJSEventBehaviorType ReviseUpdatedEventBehavior(RenderingContext context, DataEntryElement element, ExtJSEventBehaviorType eventBehaviorType)
        {
            return ExtJSEventBehaviorType.Queued;
        }



        /// <summary>
        /// Renders the DataSource property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderDataSourceProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the data entry 
            DataEntryElement dataEntry = visualElement as DataEntryElement;

            // If there is a valid data entry 
            if (dataEntry != null)
            {


                // Start items property
                jsonWriter.WritePropertyName("items");

                RenderFieldArray(jsonWriter, dataEntry);


            }
        }

        /// <summary>
        /// Renders the field array.
        /// </summary>
        /// <param name="dataEntry">The data entry.</param>
        /// <returns></returns>
        internal JavaScriptCode RenderFieldArray(DataEntryElement dataEntry)
        {
            // Create buffer for text
            StringBuilder buffer = new StringBuilder();

            // Render field array
            using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(buffer))
            {
                RenderFieldArray(jsonWriter, dataEntry);
                jsonWriter.Flush();
            }

            // Return java script code
            return new JavaScriptCode(buffer.ToString());
        }


        /// <summary>
        /// Renders the field array.
        /// </summary>
        /// <param name="jsonWriter">The JSON writer.</param>
        /// <param name="dataEntry">The data entry.</param>
        private void RenderFieldArray(JsonTextWriter jsonWriter, DataEntryElement dataEntry)
        {
            // Get item
            object item = dataEntry.ItemSource;

            // If there is a valid item
            if (item != null)
            {
                // Create items array 
                jsonWriter.WriteStartArray();

                // Loop all item properties
                foreach (PropertyDescriptor property in dataEntry.ItemProperties)
                {
                    // Write the start object
                    jsonWriter.WriteStartObject();

                    // Set label to property name
                    jsonWriter.WriteProperty(this.GetFieldLabelConfigName(), property.Name);

                    // Set name to property name
                    jsonWriter.WriteProperty("name", property.Name);

                    // Set the value property
                    WriteXTypeRelatedProperties(jsonWriter, property.GetValue(item), property.PropertyType);

                    // Write the end object
                    jsonWriter.WriteEndObject();
                }

                // Close the items array
                jsonWriter.WriteEndArray();
            }
        }

        /// <summary>
        /// Gets the name of the field label configuration.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetFieldLabelConfigName()
        {
            return "fieldLabel";
        }

        /// <summary>
        /// Writes the XTYPE related properties.
        /// </summary>
        /// <param name="jsonWriter">The JSON writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        private void WriteXTypeRelatedProperties(JsonTextWriter jsonWriter, object value, Type type)
        {
            jsonWriter.WritePropertyName("value");
            jsonWriter.WriteValue(value);

            string xtype = null;

            if (type == typeof(DateTime))
            {
                xtype = this.GetDateFieldXType();
            }
            else if (type == typeof(bool))
            {
                xtype = this.GetCheckBoxFieldXType();
            }
            else if (type == typeof(int))
            {
                xtype = this.GetNumberFieldXType();
            }
            else
            {
                xtype = this.GetTextFieldXType();
            }

            jsonWriter.WriteProperty("xtype", xtype);
        }

        /// <summary>
        /// Gets the type of the text field.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetTextFieldXType()
        {
            return "textfield";
        }

        /// <summary>
        /// Gets the type of the number field.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetNumberFieldXType()
        {
            return "numberfield";
        }

        /// <summary>
        /// Gets the type of the CheckBox field.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetCheckBoxFieldXType()
        {
            return "checkboxfield";
        }

        /// <summary>
        /// Gets the type of the date field.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetDateFieldXType()
        {
            return "datefield";
        }

        /// <summary>
        /// Renders the DataSource property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderDataSourcePropertyChange(ExtJSRenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.DataEntryElement element = visualElement as System.Web.VisualTree.Elements.DataEntryElement;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRawValue("{0}.removeAll();{0}.add({1});", new JavaScriptCode(componentVariable), this.RenderFieldArray(element));
            }

        }
    }
}
