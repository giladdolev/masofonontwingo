﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewTextField), "DataViewFieldExtJSRenderer", "column", "Ext.grid.column.Column")]
    public class DataViewTextFieldExtJSRenderer : DataViewTextFieldExtJSRendererBase
    {
        /// <summary>
        /// Gets the type of the editor.
        /// </summary>
        /// <value>
        /// The type of the editor.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return "textfield";
            }
        }        
       
        protected override bool ShouldRenderFieldRenderer(DataViewField fieldElement)
        {
            if (base.ShouldRenderFieldRenderer(fieldElement))
            {
                return true;
            }
            DataViewTextField textField = fieldElement as DataViewTextField;
            if (textField != null && !String.IsNullOrEmpty(textField.Format))
            {
                return true;
            }
            return false;

        }
    }
}
