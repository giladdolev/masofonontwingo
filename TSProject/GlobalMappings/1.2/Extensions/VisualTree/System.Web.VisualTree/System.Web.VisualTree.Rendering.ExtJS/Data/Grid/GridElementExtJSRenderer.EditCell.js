var rowIndex = <#= data.RowIndex #>;
var columnIndex = <#= data.ColumnIndex #>;
var plugin = this.getPlugin('cellEditing');
if(rowIndex >=0 && columnIndex >= 0)
{
    if(plugin){
        plugin.startEditByPosition({row: rowIndex, column: columnIndex});
    }
}
