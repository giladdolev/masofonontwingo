﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(WidgetGridElement), "ControlElementExtJSRenderer", "VTWidgetGrid", "VT.ux.VTWidgetGrid")]
    [ExtJSEventDescription("WidgetControlItemChanged", ClientEventName = "widgetControlItemChanged", ClientEventHandlerParameterNames = "view, obj , value, dataIndex", ClientHandlerCallbackData = "rowIndex = obj, newValue = value, dataindex = dataIndex ", ServerEventArgsCreateCode = "new GridCellEventArgs(this.newValue,this.dataindex,this.rowIndex)")]
    [ExtJSEventDescription("WidgetControlFocusEnter", ClientEventName = "widgetControlFocusEnter", ClientEventHandlerParameterNames = "view, obj , value, dataIndex", ClientHandlerCallbackData = "rowIndex = obj,newValue = value, dataindex = dataIndex ", ServerEventArgsCreateCode = "new GridCellEventArgs(this.newValue,this.dataindex,this.rowIndex)")]
    [ExtJSEventDescription("WidgetControlDoubleClick", ClientEventName = "widgetControlDoubleClick", ClientEventHandlerParameterNames = "view, obj , value, dataIndex", ClientHandlerCallbackData = "newValue = value, dataindex = dataIndex ", ServerEventArgsCreateCode = "new GridCellEventArgs(this.newValue,this.dataindex)")]
    [ExtJSEventDescription("WidgetControlClick", ClientEventName = "widgetControlClick", ClientEventHandlerParameterNames = "view, obj , value, dataIndex", ClientHandlerCallbackData = "newValue = value, dataindex = dataIndex ", ServerEventArgsCreateCode = "new GridCellEventArgs(this.newValue,this.dataindex)")]
    [ExtJSEventDescription("WidgetControlEditChanged", ClientEventName = "widgetControlEditChanged", ClientEventHandlerParameterNames = "view, obj , value, dataIndex", ClientHandlerCallbackData = "newValue = value, dataindex = dataIndex ", ServerEventArgsCreateCode = "new GridCellEventArgs(this.newValue,this.dataindex)")]

    [ExtJSEventDescription("WidgetControlSpecialKey", ClientEventName = "widgetControlSpecialKey", ClientEventHandlerParameterNames = "view, td , cellIndex , record , tr , rowIndex , e , eOpts", ClientHandlerCallbackData = "key = e.getKey(),CellIndex = cellIndex,RowIndex = rowIndex ,dataMember = this.widgetDataMember", ServerEventArgsCreateCode = "new GridElementCellEventArgs((Keys)this.key.ToInt32(),this.CellIndex.ToInt32(),this.RowIndex.ToInt32(),this.dataMember)")]

    [ExtJSMethodDescription("SetFocus", MethodCode = "VT_widgetgridfocuscontrol(this,<#= data.SelectedRowIndex#>,<#= data.SelectedColumnIndex#>,<#= data.DataMember#>);", MethodDataType = typeof(WidgetColumnData))]
    [ExtJSMethodDescription("SetEnabled", MethodCode = "VT_widgetgridenablecontrol(this,<#= data.SelectedRowIndex#>,<#= data.SelectedColumnIndex#>,<#= data.DataMember#>,<#= data.Enabled#>);", MethodDataType = typeof(WidgetColumnData))]
    [ExtJSMethodDescription("SetBackColor", MethodCode = "VT_widgetgridbackcolorcontrol(this,<#= data.SelectedRowIndex#>,<#= data.SelectedColumnIndex#>,<#= data.DataMember#>,<#= data.BackColor.Name #>);", MethodDataType = typeof(WidgetColumnData))]
    [ExtJSMethodDescription("RemoveFromViewModelContrlos", MethodCode = "this.removeFromViewModelContrlos(<#= data.SelectedRowIndex#>);", MethodDataType = typeof(WidgetColumnData))]
    


    [ExtJSEventDescription("Afterrender", "afterrender", ClientEventHandlerParameterNames = "grid", ClientCode = "VT_GridAfterRender(this, '<#= WindowElementExtJSRenderer.ReviseContextMenuStripID(element) #>')", RenderAlways = true, ClientEventBehaviorType = ExtJSEventBehaviorType.Queued)]
    [ExtJSEventDescription("CellkeyDown", ClientEventName = "cellkeydown", ClientEventHandlerParameterNames = "view, td , cellIndex , record , tr , rowIndex , e , eOpts", ClientHandlerCallbackData = "key = e.getKey(),CellIndex = cellIndex,RowIndex = rowIndex ,dataMember = this.widgetDataMember", ServerEventArgsCreateCode = "new GridElementCellEventArgs((Keys)this.key.ToInt32(),this.CellIndex.ToInt32(),this.RowIndex.ToInt32(),this.dataMember)")]
    [ExtJSEventDescription("CellMouseDown", ClientEventName = "cellmousedown", ClientEventHandlerParameterNames = " view , td , cellIndex , record , tr , rowIndex , e , eOpts ", ClientHandlerCallbackData = "cellIndex = VT_GetActualCellIndex(view,cellIndex), rowIndex = rowIndex , X = e.getX(), Y = e.getY(), button = e.button , delta = e.getWheelDelta() ,ctrlKey = e.ctrlKey, shiftKey = e.shiftKey", ServerEventArgsCreateCode = "new GridCellMouseEventArgs(this.cellIndex.ToInt32(), this.rowIndex.ToInt32(), this.X.ToInt32(), this.Y.ToInt32(), new MouseEventArgs( GridElementExtJSRenderer.ReviseMouseButton(this.button.ToInt32()),0,0,0,this.delta.ToInt32()),this.ctrlKey.ToBoolean(),this.shiftKey.ToBoolean())", ClientEventBehaviorType = ExtJSEventBehaviorType.Delayed, RenderAlways = true)]
    [ExtJSEventDescription("SelectionChanged", ClientEventName = "selectionchange", ClientEventHandlerParameterNames = " view, selection , eOpts ", ClientHandlerCallbackData = "rows = VT_GetSelectedRowIndeces(view,selection), cells = VT_GetSelectedCellsIndeces(selection)", ClientEventBehaviorType = ExtJSEventBehaviorType.Delayed)]

    public class WidgetGridElementExtJSRenderer : WidgetGridElementExtJSRendererBase
    {



        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            if (property == "WidgetBind")
            {
                RenderWidgetBind(renderingContext, visualElement, builder, componentVariable);
            }
            base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
        }

        private void RenderWidgetBind(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get the GridElement element
            WidgetGridElement gridElement = visualElement as WidgetGridElement;

            // If there is a valid WidgetGridElement element
            if (gridElement == null)
            {
                return;
            }

            builder.AppendRawValue("if(cmp.xtype == 'VTWidgetGrid'){cmp.resetViewModelContrlos();}");
        }
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            WidgetGridElement widgetgrid = visualElement as WidgetGridElement;
            if (widgetgrid != null)
            {
                jsonWriter.WritePropertyName("columns");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("width", widgetgrid.WidgetTemplateWidth);
                        jsonWriter.WriteProperty("xtype", "widgetcolumn");
                        jsonWriter.WritePropertyWithRawValue("cellFocusable", "false");

                        jsonWriter.WritePropertyName("widget");
                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                        {
                            jsonWriter.WriteProperty("xtype", widgetgrid.WidgetTemplate);
                            jsonWriter.WriteProperty("viewModel", widgetgrid.WidgetTemplateModel);
                        }
                        // Render the client event handler
                        jsonWriter.WritePropertyWithAnonymousFunction("onWidgetAttach", new string[] { "col", "widget", "rec" }, (javaScriptBuilder) =>
                        {
                            StringBuilder sb = new StringBuilder();

                            sb.Append("this,col, widget, rec");
                            // Create the callback code.
                            javaScriptBuilder.AppendInvocationStatement(widgetgrid.WidgetTemplateDataAttachMethod, new JavaScriptCode(sb.ToString()));

                        });
                    }
                }

                jsonWriter.WritePropertyName("store");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WritePropertyName("fields");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(widgetgrid.WidgetTemplateDataMembers)))
                        {
                            string[] array = widgetgrid.WidgetTemplateDataMembers.Split(',');
                            for (int i = 0; i < array.Length; i++)
                            {
                                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                                {
                                    jsonWriter.WriteProperty("name", array[i]);
                                }
                            }
                        }
                    }
                    jsonWriter.WritePropertyName("proxy");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("type", "vt");
                        jsonWriter.WriteProperty("serverId", VisualElement.GetElementId(widgetgrid));
                    }

                }
            }
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Extracts the SelectionChanged event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractSelectionChangedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            //    var selectedRows = args.Value<Newtonsoft.Json.Linq.JArray>("rows").Values<System.Int32>().ToArray();
            //    var cells = args.Value<Newtonsoft.Json.Linq.JArray>("cells");
            //    var selectedCells = cells.Select(obj => new GridIndexesData((int)obj[0], (int)obj[1])).ToArray();

            //    return new GridElementSelectionChangeEventArgs(selectedRows, selectedCells);

            GridElement grd = visualElement as GridElement;
            List<int> lstRows = new List<int>() { grd.SelectedRowIndex };
            List<GridIndexesData> lstCells = new List<GridIndexesData>() { new GridIndexesData(grd.SelectedRowIndex, grd.SelectedColumnIndex) };
            GridElementSelectionChangeEventArgs newArgs = new GridElementSelectionChangeEventArgs(lstRows, lstCells);

            return new GridElementSelectionChangeEventArgs(lstRows, lstCells);
        }

    }
}