﻿using System.ComponentModel.Composition;
using System.Globalization;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridColumn), "GridEditorColumnBaseExtJSRenderer", "gridcolumn", "Ext.grid.column.Column")]
    [ExtJSPropertyDescription("AutoSizeMode", PropertyInitializeCode = "flex = <#= 1 #>", ClientNotDefaultCode = "element.AutoSizeMode == GridAutoSizeColumnMode.Fill")]
    [ExtJSPropertyDescription("DataMember", PropertyInitializeCode = "dataIndex = <#= element.DataMember #>", ClientNotDefaultCode = "!(element is GridRowHeader)")]
    [ExtJSPropertyDescription("Format", PropertyInitializeCode = "formatter = <#= element.Format #>", ClientNotDefaultCode = "!String.IsNullOrEmpty(element.Format)", PropertyChangeCode = "this.formatter = '<#= element.Format #>';")]
    [ExtJSPropertyDescription("HeaderText", PropertyInitializeCode = "text = <#= element.HeaderText #>", PropertyChangeCode = "this.setText('<#= element.HeaderText #>');")]
    [ExtJSPropertyDescription("ColumnFilter", PropertyInitializeCode = "filter={xtype:'textfield',filterName:'<#= element.ClientID#>'}", ClientNotDefaultCode = "element.ColumnFilter == true")]
    [ExtJSPropertyDescription("Icon", PropertyInitializeCode = "text = <#= GridColumnExtJSRenderer.ReviseIconProperty(element) #>", ClientNotDefaultCode = "element.Icon != null && element.Icon.Source != String.Empty || element.HeaderText != String.Empty ", PropertyChangeCode = "this.setText(\"<#= GridColumnExtJSRenderer.ReviseIconProperty(element) #>\");")]
    [ExtJSPropertyDescription("MaxInputLength", ClientNotDefaultCode = "element.MaxInputLength != -1", PropertyChangeCode = " this.setEditor( {editable : <#= element.MaxInputLength #>});")]
    [ExtJSPropertyDescription("MaxWidth", PropertyInitializeCode = "maxWidth = <#= element.MaxWidth #>", ClientDefault = 500)]
    [ExtJSPropertyDescription("MenuDisabled", PropertyInitializeCode = "menuDisabled = <#= element.MenuDisabled #>", ClientNotDefaultCode = "element.MenuDisabled == true && !(element is GridRowHeader)")]
    [ExtJSPropertyDescription("ReadOnly")]
    [ExtJSPropertyDescription("Resizable", PropertyInitializeCode = "resizable = <#= bool.Parse(Convert.ToString(element.Resizable)) #>", ClientNotDefaultCode = "!(element is GridRowHeader)")]
    [ExtJSPropertyDescription("SortMode", PropertyInitializeCode = "sortable = <#= GridColumnExtJSRenderer.ReviseSortMode(element)#>", PropertyChangeCode = "this.sortable = <#= JavaScriptCode.GetBool( GridColumnExtJSRenderer.ReviseSortMode(element) ) #>", ClientNotDefaultCode = "!(element is GridRowHeader)")]
    [ExtJSPropertyDescription("TabIndex", PropertyInitializeCode = " tabIndex = <#= element.TabIndex #>", PropertyChangeCode = "this.setTabIndex(<#= ControlElementExtJSRenderer.ReviseTabIndex(element)#>);", ClientNotDefaultCode = "element.TabStop && element.TabIndex >= 0 && !(element is GridRowHeader)")]
    [ExtJSPropertyDescription("Width", PropertyInitializeCode = "width = <#= (element.PixelWidth == 0 ? 100 : element.PixelWidth) #>", ClientNotDefaultCode = "!(element is GridRowHeader)")]

    [ExtJSPropertyDescription("Locked", PropertyInitializeCode = "locked = true", ClientNotDefaultCode = "element.Index >-1 && element.Index < element.GridElement.FrozenCols")]   
    [ExtJSEventDescription("Resize", "resize", ClientEventHandlerParameterNames = "view, _width , _height , _oldWidth , _oldHeight , eOpts", ClientHandlerCallbackData = "oldWidth = _oldWidth,oldHeight = _oldHeight,width = _width, height = _height ", ServerEventArgsCreateCode = "new ResizeEventArgs(this.width.ToInt32(),this.height.ToInt32(),this.oldWidth.ToInt32(),this.oldHeight.ToInt32())")]

    
    public class GridColumnExtJSRenderer : GridColumnExtJSRendererBase
    {


        /// <summary>
        /// Renders the Icon property change.
        /// </summary>
        /// <param name="controlElement">The controlElement.</param>
        internal static string ReviseIconProperty(ControlElement controlElement)
        {
            // Get typed element
            GridColumn element = controlElement as GridColumn;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (element.Icon != null && !string.IsNullOrEmpty(element.Icon.Source))
                {
                    if (!string.IsNullOrEmpty(element.HeaderText))
                    {
                        return "<img src='" + element.Icon.Source + "'  height='25' width='25'/>  " + element.HeaderText;
                    }
                }
                return element.HeaderText;
            }
            return "";
        }


        /// <summary>
        /// Renders the identifier property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderItemIdProperty(VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            GridColumn grdColumn = visualElement as GridColumn;
            if (grdColumn != null)
            {
                if (grdColumn is GridRowHeader)
                {
                    return;
                }
                string itemId = grdColumn.ClientID;
                if (grdColumn.IsDirtyDataMember)
                {
                    itemId = grdColumn.DataMember;
                }
                jsonWriter.WriteProperty("itemId", itemId);
            }
        }





        /// <summary>
        /// Revises the MouseButton.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static bool ReviseSortMode(GridColumn column)
        {
            GridElement grid = column.Parent as GridElement;
            // If the grid is null .
            if (grid != null)
            {
                if (grid.SortableColumns == true)
                {
                    switch (column.SortMode)
                    {
                        case GridColumnSortMode.Automatic:
                        case GridColumnSortMode.Programmatic:
                            return true;
                        case GridColumnSortMode.NotSortable:
                            return false;
                        default:
                            return true;
                    }
                }
            }
            return false;
        }



        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            
            GridElement grd = visualElement.ParentElement as GridElement;
            if (grd != null)
            {
                //default is false ; not allow to reorder columns
                if (grd.AllowUserToOrderColumns == false)
                {
                    //gridColumn.AllowColumnReorder = false;
                    jsonWriter.WriteProperty("draggable", false);
                }
                if (grd.AllowUserToOrderColumns == true)
                {
                    //gridColumn.AllowColumnReorder = false;
                    jsonWriter.WriteProperty("draggable", true);
                }

            }
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the Format property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderFormatProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            GridColumn gridColumn = visualElement as GridColumn;
            string currencySign = "";

            if (gridColumn == null)
            {
                return;
            }
            // Write corresponding sencha code for image
            if (IsPicture(gridColumn))
            {
                jsonWriter.WritePropertyName("renderer");
                jsonWriter.WriteRawValue("function(value, metaData, record, row, col, store, gridView){return \"<img  width='25'  height='25' src='GetPictureHandler?path=\" + value + \"' />\";}");
                return;
            }

            if (gridColumn is GridCheckBoxColumn || gridColumn is GridDateTimePickerColumn || gridColumn is GridComboBoxColumn || gridColumn is GridRowHeader)
            {
                return;
            }

            switch (gridColumn.Format)
            {
                case "date":
                    jsonWriter.WritePropertyName("renderer");
                    jsonWriter.WriteRawValue("function(value, metaData, record, row, col, store, gridView){VT_Column_Style_renderer(value, metaData, record, row, col, store, gridView); return Ext.util.Format.date(value, 'm/d/Y');}");

                    return;
                case "currency":
                    currencySign = new RegionInfo(gridColumn.DefaultCellStyle.CountryCurrencyFormat.ToString()).CurrencySymbol;
                    jsonWriter.WritePropertyName("renderer");
                    jsonWriter.WriteRawValue("function(value, metaData, record, row, col, store, gridView){VT_Column_Style_renderer(value, metaData, record, row, col, store, gridView); return Ext.util.Format.number(value, '0,000.00 " + currencySign + "');}");

                    return;
               
                default:
                    jsonWriter.WritePropertyName("renderer");
                    jsonWriter.WriteRawValue("function(value, metaData, record, row, col, store, gridView){VT_Column_Style_renderer(value, metaData, record, row, col, store, gridView); return value;}");
                    break;
            }

        }

        internal static bool IsPicture(GridColumn gridColumn)
        {
            return (gridColumn is GridImageColumn || (gridColumn.DataType != null && gridColumn.DataType.FullName == "System.Byte[]"));
        }

    }
}
