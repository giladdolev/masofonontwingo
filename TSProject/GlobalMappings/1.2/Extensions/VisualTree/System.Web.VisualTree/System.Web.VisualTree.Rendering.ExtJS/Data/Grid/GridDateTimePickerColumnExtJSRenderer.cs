﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridDateTimePickerColumn), "GridColumnExtJSRenderer", "datecolumn", "Ext.grid.column.Date")]
    [ExtJSPropertyDescription("Format", PropertyInitializeCode = "format =<#= DateTimePickerUtils.ParseGridDateTimePickerColumnFormat(element) #>", PropertyChangeCode = "format='<#= DateTimePickerUtils.ParseGridDateTimePickerColumnFormat(element) #>'")]
    [ExtJSPropertyDescription("ReadOnly", PropertyInitializeCode = "this.setEditor( {readOnly : <#= JavaScriptCode.GetBool(element.ReadOnly) #>,editable : <#= JavaScriptCode.GetBool(!element.ReadOnly) #>,xtype: 'datefield',});", PropertyChangeCode = "this.setEditor( {readOnly : <#= JavaScriptCode.GetBool(element.ReadOnly) #>,editable : <#= JavaScriptCode.GetBool(!element.ReadOnly) #>,xtype: 'datefield',});", ClientNotDefaultCode = "element.ReadOnly != false")]
    public class GridDateTimePickerColumnExtJSRenderer : GridDateTimePickerColumnExtJSRendererBase
    {

        /// <summary>
        /// Renders the content of the editor.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <param name="gridColumn">The grid column.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderEditorContent(GridElement gridElement, GridColumn gridColumn, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            if (gridColumn != null)
            {
                // the format that been sent to the server 
                //jsonWriter.WriteProperty("submitformat", "d-m-Y G:H:s");
                //jsonWriter.WriteProperty("format", "d/m/y");
                //jsonWriter.WriteProperty("allowBlank", false);
            }

            base.RenderEditorContent(gridElement, gridColumn, jsonWriter);
        }

        /// <summary>
        /// Gets the type of the editor x.
        /// </summary>
        /// <value>
        /// The type of the editor x.
        /// </value>
        public override string EditorXType
        {
            get { return "datefield"; }
        }
    }
}
