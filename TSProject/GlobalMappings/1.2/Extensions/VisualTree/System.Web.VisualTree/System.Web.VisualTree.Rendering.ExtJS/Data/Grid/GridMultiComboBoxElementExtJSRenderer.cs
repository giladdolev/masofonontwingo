﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridMultiComboBoxElement), "ComboBoxElementExtJSRenderer", "gridMultiComboBox", "VT.ux.VTGridMultiComboBox")]
    [ExtJSPropertyDescription("DisplayMember", PropertyInitializeCode = "displayField = <#= element.DisplayMember #>")]
    [ExtJSPropertyDescription("ValueMember", PropertyInitializeCode = "valueField = <#=  element.ValueMember #>")]
   
    public class GridMultiComboBoxElementExtJSRenderer : GridMultiComboBoxElementExtJSRendererBase
    {
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
            GridMultiComboBoxElement combo = visualElement as GridMultiComboBoxElement;
            if (combo != null)
            {
                jsonWriter.WritePropertyName("columns");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    RenderColumns(combo, jsonWriter);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="combo"></param>
        /// <param name="jsonWriter"></param>
        private void RenderColumns(GridMultiComboBoxElement combo, JsonTextWriter jsonWriter)
        {
            foreach (var column in combo.Columns)
            {
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteProperty("xtype", "gridcolumn");
                    jsonWriter.WritePropertyName("editor");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("xtype", "textfield");
                    }
                    jsonWriter.WriteProperty("menuDisabled", "true");
                    jsonWriter.WriteProperty("text", column.Text);
                    jsonWriter.WriteProperty("hidden", !column.Visible);
                    jsonWriter.WriteProperty("dataIndex", column.DataMember);
                }
            }
        }
    }
}