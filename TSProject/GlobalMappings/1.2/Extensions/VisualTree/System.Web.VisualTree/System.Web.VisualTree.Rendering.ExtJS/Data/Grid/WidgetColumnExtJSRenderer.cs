﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(WidgetColumn), "GridColumnExtJSRenderer", "SPGridColumnwidgetcolumn", "VT.ux.SPGridColumn")]
    [ExtJSPropertyDescription("DataMembers", PropertyInitializeCode = "datamembers = <#= element.DataMembers #>", PropertyChangeCode = "this.setDataMembers(<#= element.DataMembers #>)")]
     [ExtJSMethodDescription("SetFocus", "<# resource: SetFocus.js #>", MethodDataType = typeof(WidgetColumnData))]
     [ExtJSMethodDescription("SetEnabled", "<# resource: SetEnabled.js #>", MethodDataType = typeof(WidgetColumnData))]
     [ExtJSMethodDescription("SetBackColor", "<# resource: SetBackColor.js #>", MethodDataType = typeof(WidgetColumnData))]
    [ExtJSMethodDescription("SetStore", "<# resource: SetStore.js #>", MethodDataType = typeof(WidgetColumnData))]
    public class WidgetColumnExtJSRenderer : WidgetColumnExtJSRendererBase
    {

        /// <summary>
        /// Gets the type of the editor x.
        /// </summary>
        /// <value>
        /// The type of the editor x.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return "";
            }
        }

        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);

            WidgetColumn widgetColumn = visualElement as WidgetColumn;
            if (widgetColumn != null)
            {
                jsonWriter.WritePropertyName("widget");
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteProperty("xtype", "container");
                    jsonWriter.WritePropertyWithRawValue("height", "55");


                    jsonWriter.WritePropertyName("layout");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("type", "absolute");
                    }
                    
              
                    RenderPlainItemsProperty(renderingContext, widgetColumn.Items, jsonWriter, "items", (element => element != null && ((ControlElement)element).Visible));
                }
            }
        }
    }
}

