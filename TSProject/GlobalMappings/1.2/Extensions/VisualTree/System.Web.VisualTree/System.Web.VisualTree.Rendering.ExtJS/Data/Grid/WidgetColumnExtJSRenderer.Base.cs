using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class WidgetColumnExtJSRendererBase : GridColumnExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "VT.ux.SPGridColumn";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "SPGridColumnwidgetcolumn";
			}
        }


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WidgetColumn element = visualElement as System.Web.VisualTree.Elements.WidgetColumn;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "DataMembers":
					this.RenderDataMembersPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderDataMembersProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the DataMembers property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDataMembersProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WidgetColumn element = visualElement as System.Web.VisualTree.Elements.WidgetColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.DataMembers))
					{
						jsonWriter.WriteProperty("datamembers",  element.DataMembers );
					}
				}
			}

		}

		/// <summary>
        /// Renders the DataMembers property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDataMembersPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WidgetColumn element = visualElement as System.Web.VisualTree.Elements.WidgetColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setDataMembers({1})", new JavaScriptCode(componentVariable),  element.DataMembers );
			}

		}


		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "SetFocus":
					return this.ExtractSetFocusMethodCallbackData(context, eventObject);						
				case "SetEnabled":
					return this.ExtractSetEnabledMethodCallbackData(context, eventObject);						
				case "SetBackColor":
					return this.ExtractSetBackColorMethodCallbackData(context, eventObject);						
				case "SetStore":
					return this.ExtractSetStoreMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "SetFocus":
					this.RenderSetFocusMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetEnabled":
					this.RenderSetEnabledMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetBackColor":
					this.RenderSetBackColorMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "SetStore":
					this.RenderSetStoreMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the SetFocus method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetFocusMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.WidgetColumn element = visualElement as System.Web.VisualTree.Elements.WidgetColumn;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
					builder.AppendFormattedRawValue(@"var rowIndex={0};var columnIndedx={1};var widgetIndex={2};var widgetDataMember={3};try{{if(rowIndex>-1){{var grd=VT_GetCmp({4});if(grd!=null&&grd.xtype==""VTgridpanel""){{if(grd.getColumns().length>columnIndedx){{if(grd.getColumns()[columnIndedx].Rows.length>rowIndex){{var comp=grd.getColumns()[columnIndedx].getwidgetControl(rowIndex,widgetDataMember);if(comp&&comp.rendered!='undefined'){{grd.getView().focusRow(rowIndex);comp.focus()}}}}}}}}}}}}catch(err){{console.log(""error :- ""+err.message)}}", data.SelectedRowIndex, data.SelectedColumnIndex, data.WidgetControlIndex, data.DataMember, Convert.ToString(System.Web.VisualTree.Elements.VisualElement.GetElementId(data.Grid)));
				}
			}

		}


		/// <summary>
        /// Gets the method SetFocus callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetFocusMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetEnabled method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetEnabledMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.WidgetColumn element = visualElement as System.Web.VisualTree.Elements.WidgetColumn;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
					builder.AppendFormattedRawValue(@"var rowIndex={0};var columnIndedx={1};var widgetIndex={2};var state={3};var widgetDataMember={4};try{{if(rowIndex>-1){{var grd=VT_GetCmp({5});if(grd!=null&&grd.xtype==""VTgridpanel""){{if(grd.getColumns().length>columnIndedx){{if(grd.getColumns()[columnIndedx].Rows.length>rowIndex){{var widgetControl=grd.getColumns()[columnIndedx].getwidgetControl(rowIndex,widgetDataMember);if(widgetControl){{if(widgetControl.rendered!='undefined'){{if(widgetControl.xtype==""textfield""){{widgetControl.setReadOnly(!state)}}else if(widgetControl.xtype==""gridMultiComboBox""){{widgetControl.setDisabled(!state)}}}}}}}}}}}}}}}}catch(err){{console.log(""error :- ""+err.message)}}", data.SelectedRowIndex, data.SelectedColumnIndex, data.WidgetControlIndex, data.Enabled, data.DataMember, Convert.ToString(System.Web.VisualTree.Elements.VisualElement.GetElementId(data.Grid)));
				}
			}

		}


		/// <summary>
        /// Gets the method SetEnabled callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetEnabledMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetBackColor method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetBackColorMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.WidgetColumn element = visualElement as System.Web.VisualTree.Elements.WidgetColumn;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
					builder.AppendFormattedRawValue(@"var rowIndex={0};var columnIndedx={1};var widgetIndex={2};var color={3};var widgetDataMember={4};try{{if(rowIndex>-1){{var grd=VT_GetCmp({5});if(grd!=null&&grd.xtype==""VTgridpanel""){{if(grd.getColumns().length>columnIndedx){{if(grd.getColumns()[columnIndedx].Rows.length>rowIndex){{var widgetControl=grd.getColumns()[columnIndedx].getwidgetControl(rowIndex,widgetDataMember);if(widgetControl&&widgetControl.rendered!='undefined'){{if(widgetControl.rendered!=false){{if(widgetControl.xtype==""textfield""){{widgetControl.setFieldStyle('background:'+color)}}else if(widgetControl.xtype==""gridMultiComboBox""){{}}}}}}}}}}}}}}}}catch(err){{console.log(""error :- ""+err.message)}}", data.SelectedRowIndex, data.SelectedColumnIndex, data.WidgetControlIndex, data.BackColor.Name, data.DataMember, Convert.ToString(System.Web.VisualTree.Elements.VisualElement.GetElementId(data.Grid)));
				}
			}

		}


		/// <summary>
        /// Gets the method SetBackColor callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetBackColorMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the SetStore method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderSetStoreMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.WidgetColumn element = visualElement as System.Web.VisualTree.Elements.WidgetColumn;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.WidgetColumnData data = (System.Web.VisualTree.Elements.WidgetColumnData)methodData;
					builder.AppendFormattedRawValue(@"var rowIndex={0};var columnIndedx={1};var widgetIndex={2};var widgetDataMember={3};var store={4};try{{if(rowIndex>-1){{var grd=VT_GetCmp({5});if(grd!=null&&grd.xtype==""VTgridpanel""){{if(grd.getColumns().length>columnIndedx){{if(grd.getColumns()[columnIndedx].Rows.length>rowIndex){{var widgetControl=grd.getColumns()[columnIndedx].getwidgetControl(rowIndex,widgetDataMember);if(widgetControl&&widgetControl.rendered!='undefined'){{if(widgetControl.rendered!=false){{if(widgetControl.xtype==""gridMultiComboBox""){{widgetControl.setStore(store)}}}}}}}}}}}}}}}}catch(err){{console.log(""error :- ""+err.message)}}", data.SelectedRowIndex, data.SelectedColumnIndex, data.WidgetControlIndex, data.DataMember, data.Store, Convert.ToString(System.Web.VisualTree.Elements.VisualElement.GetElementId(data.Grid)));
				}
			}

		}


		/// <summary>
        /// Gets the method SetStore callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractSetStoreMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}