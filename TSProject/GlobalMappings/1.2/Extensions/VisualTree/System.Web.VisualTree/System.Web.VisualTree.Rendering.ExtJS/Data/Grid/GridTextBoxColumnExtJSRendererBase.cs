using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class GridTextBoxColumnExtJSRendererBase : GridColumnExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.grid.column.Column";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "gridcolumn";
			}
        }


		/// <summary>
        /// Renders the ReadOnly property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderReadOnlyProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridTextBoxColumn element = visualElement as System.Web.VisualTree.Elements.GridTextBoxColumn;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If ReadOnly value is not default
					if (element.ReadOnly != false)
					{
						jsonWriter.WritePropertyWithRawValue("disabled", @"false");
					}
				}
			}

		}

		/// <summary>
        /// Renders the ReadOnly property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderReadOnlyPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GridTextBoxColumn element = visualElement as System.Web.VisualTree.Elements.GridTextBoxColumn;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setEditor( {{readOnly : {1},editable : {2},xtype: 'textfield'}});", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.ReadOnly) ,  JavaScriptCode.GetBool(!element.ReadOnly) );
			}

		}


	}
}