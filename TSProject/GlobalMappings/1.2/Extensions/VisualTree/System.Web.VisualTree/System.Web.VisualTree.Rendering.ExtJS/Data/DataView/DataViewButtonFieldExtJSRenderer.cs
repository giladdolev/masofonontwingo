﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DataViewButtonField), "DataViewWidgetFieldExtJSRenderer", "widgetcolumn", "Ext.grid.column.Widget")]
    public class DataViewButtonFieldExtJSRenderer : DataViewButtonFieldExtJSRendererBase
    {
        public override string ExtJSType
        {
            get
            {
                return "button";
            }
        }
        protected override void InitializeWidgetProperties(RenderingContext context, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            DataViewButtonField buttonFieldElement = visualElement as DataViewButtonField;
            if (buttonFieldElement != null)
            {
                jsonWriter.WriteProperty("text", buttonFieldElement.Text);
            }
        }
    }
}
