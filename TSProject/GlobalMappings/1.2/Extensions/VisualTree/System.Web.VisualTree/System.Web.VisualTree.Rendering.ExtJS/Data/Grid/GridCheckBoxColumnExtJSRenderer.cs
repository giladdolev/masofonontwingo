﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridCheckBoxColumn), "GridColumnExtJSRenderer", "threeStateCheckboxColumn", "VT.ux.ThreeStateCheckboxColumn")]
    [ExtJSPropertyDescription("ReadOnly", PropertyInitializeCode = "editable = <#= JavaScriptCode.GetBool(element.ReadOnly) #>, disabledCls = ''", ClientNotDefaultCode = "element.ReadOnly != true", PropertyChangeCode = " this.setEditable(<#= JavaScriptCode.GetBool(element.ReadOnly) #>);")]
    [ExtJSPropertyDescription("ThreeState", PropertyInitializeCode = "threeState = <#= element.ThreeState #>", PropertyChangeCode = "this.setThreeState(<#= JavaScriptCode.GetBool(element.ThreeState) #>);")]

    [ExtJSEventDescription("CheckedChange", ClientEventName = "checkchange", ClientEventHandlerParameterNames = "view, rowIndex , checked , record , e , eOpts", ClientHandlerCallbackData = "newValue = checked", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.newValue)")]
    public class GridCheckBoxColumnExtJSRenderer : GridCheckBoxColumnExtJSRendererBase
    {
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);
            GridElement grd = visualElement.ParentElement  as GridElement;
            if (grd != null)
            {
                if (grd.ReadOnly)
                {
                    jsonWriter.WriteProperty("disabled", JavaScriptCode.GetBool(grd.ReadOnly));
                }
            }
        }  

        /// <summary>
        /// Gets the type of the editor x.
        /// </summary>
        /// <value>
        /// The type of the editor x.
        /// </value>
        public override string EditorXType
        {
            get
            {
                return "checkboxfield";
            }
        }

    }
}
