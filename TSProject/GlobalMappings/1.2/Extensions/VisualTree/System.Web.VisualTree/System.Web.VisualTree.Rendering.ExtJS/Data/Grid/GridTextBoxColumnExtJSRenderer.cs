﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridTextBoxColumn), "GridColumnExtJSRenderer", "gridcolumn", "Ext.grid.column.Column")]
    [ExtJSPropertyDescription("ReadOnly", PropertyInitializeCode = "disabled = false", PropertyChangeCode = "this.setEditor( {readOnly : <#= JavaScriptCode.GetBool(element.ReadOnly) #>,editable : <#= JavaScriptCode.GetBool(!element.ReadOnly) #>,xtype: 'textfield'});" ,ClientNotDefaultCode = "element.ReadOnly != false")]
    public class GridTextBoxColumnExtJSRenderer : GridTextBoxColumnExtJSRendererBase
    {
        /// <summary>
        /// Renders the content of the editor.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <param name="gridColumn">The grid column.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderEditorContent(GridElement gridElement, GridColumn gridColumn, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            if (gridColumn.MaxInputLength != -1)
            {
                jsonWriter.WriteProperty("maxLength", gridColumn.MaxInputLength);
            }
            base.RenderEditorContent(gridElement, gridColumn, jsonWriter);
        }

    }
}
