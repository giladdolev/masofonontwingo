try{
var index = 0 ;
if(<#=JavaScriptCode.GetBool(element.RowHeaderVisible) #>)
{
    // start from the second column in client .
    // cause the first column is a row  header . 
    index = 1;
}

for(i=0 ; i < <#= element.FrozenCols #> || this.getColumns()[i + index].locked == true ; i++){
    if(i >= <#= element.FrozenCols #>){
        this.unlock(this.getColumns()[i + index]);
}
else{
        this.lock(this.getColumns()[i + index]);
}   
 
}
}catch(err){
console.log(err.message);
}

    
