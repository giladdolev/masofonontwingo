﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Elements;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering;

namespace System.Web.VisualTree.Data
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridRowHeader), "GridColumnExtJSRenderer", "rownumberer", "")]
    [ExtJSPropertyDescription("ShowRowNumber", PropertyInitializeCode = "renderer = <#resource:ShowRowNumber.js#>")]
    public class GridRowHeaderExtJSRenderer : GridRowHeaderExtJSRendererBase
    {


        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);

            GridRowHeader grdRow = visualElement as GridRowHeader;
            if (grdRow != null)
            {
                GridElement grd = grdRow.Parent as GridElement;
                if (grd != null)
                {
                    if (grdRow.Parent != null)
                    {
                        jsonWriter.WritePropertyName("items");
                        // Open object scope
                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                        {
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                jsonWriter.WriteProperty("xtype", "image");
                                jsonWriter.WriteProperty("id", grd.ID + "rowImageHeaderId");
                                jsonWriter.WriteProperty("src", Convert.ToString(FormatHelper.FormatResource(renderingContext, grdRow.Icon)));
                                jsonWriter.WriteProperty("height", "16px");
                                jsonWriter.WriteProperty("width", "20px");
                            }
                        }
                    }
                }

            }
        }
    }
}
