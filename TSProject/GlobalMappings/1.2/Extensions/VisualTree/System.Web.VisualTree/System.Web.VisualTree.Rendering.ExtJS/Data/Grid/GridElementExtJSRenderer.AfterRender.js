try {

    /*
    * Show the filterplugin
    */
    if (grid) {
        var plugin = grid.getPlugin('filterplugin');
        if (plugin) {
            plugin.setVisible(true);
        }

        // Update the selected row on load
        grid.store.on('load', function (store, records, successful, operation, eOpts) {
            var gridId = '<#=element.ID#>';
            var gridElement = VT_GetCmp(gridId);
            if (records.length > 0 && gridElement) {
                if (gridElement.xtype != 'Grid-Combo') {
                    gridElement.getSelectionModel().select(0);
                }
            }

        }, this, { single: true });

        // Update the selected row on endupdate
        grid.store.on('endupdate', function () {
            var gridId = '<#=element.ID#>';
            var gridElement = VT_GetCmp(gridId);
            if (gridElement) {

                // If there are records and there hasn't been any selection, then select the first record by default
                if (gridElement.store.totalCount > 0 && gridElement.selection == null) {
                    if (gridElement.xtype != 'Grid-Combo') {
                        gridElement.getSelectionModel().select(0);
                    }
                }
            }
        });


        this.el.on('contextmenu', function (e) {
            e.stopEvent();

            var menuCmp = VT_GetCmp('<#= (element.ContextMenuStrip != null)?element.ContextMenuStrip.ID:"" #>');
            if (menuCmp != null) {
                menuCmp.on('render', function (menu) {
                    menu.getEl().on('contextmenu', function (e) {
                        e.stopEvent();
                    });
                });
                menuCmp.enable();
                menuCmp.showBy(this, "tl?", [e.getX() - this.getX(), e.getY() - this.getY()]);
            }
        });

    }

} catch (e) {
    console.log('GridElement.AfterRender.js error : ' + e.message);
}
