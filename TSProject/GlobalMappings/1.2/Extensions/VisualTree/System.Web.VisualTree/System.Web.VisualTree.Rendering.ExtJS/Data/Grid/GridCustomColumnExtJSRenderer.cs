﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridCustomColumn), "GridColumnExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("ItemPath", PropertyInitializeCode = "itemPath = <#= System.Web.VisualTree.Elements.VisualElement.GetElementId(element) #>")]
    [ExtJSMethodDescription("RenderCellEditor", MethodCode = "<#resource:CellRenderer.js #>", MethodDataType = typeof(CellRenderingEventArgs))]
    [ExtJSEventDescription("BeforeCellRendering", null, ServerEventArgsCreateCode = "new CellRenderingEventArgs(this.rowIndex.ToInt32(), this.columnIndex.ToInt32(), this.newValue)")]
    [ExtJSEventDescription("ColumnAction", null, ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.newValue)")]
    class GridCustomColumnExtJSRenderer : GridCustomColumnExtJSRendererBase
    {
        protected override void RenderFormatProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            GridCustomColumn gridColumn = visualElement as GridCustomColumn;

            if (gridColumn == null)
            {
                return;
            }
            jsonWriter.WritePropertyName("renderer");
            string renderer = String.IsNullOrEmpty(gridColumn.Renderer) ? "VT_RenderCellEditor" : gridColumn.Renderer;
            jsonWriter.WriteRawValue(renderer);

        }

        /// <summary>
        /// Revises the type of the widget.
        /// </summary>
        /// <param name="data">The <see cref="CellRenderingEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        internal static string ReviseWidgetType(CellRenderingEventArgs data)
        {
            if (data != null)
            {
                switch (data.CellType)
                {
                    case GridColumnType.CheckBox:
                        return "checkbox";
                    case GridColumnType.ComboBox:
                        return "combo";
                    case GridColumnType.DateTimePicker:
                        return "datefield";
                }
            }
            return "textfield";
        }

        /// <summary>
        /// Renders the store.
        /// </summary>
        /// <param name="data">The <see cref="CellRenderingEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        internal static object RenderStore(CellRenderingEventArgs data)
        {
            if (data != null)
            {
                if (data.CellType == GridColumnType.ComboBox && !String.IsNullOrEmpty(data.ListItems))
                {
                    StringBuilder sb = new StringBuilder();
                    using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(sb))
                    {
                        GridComboBoxColumnExtJSRenderer.RenderComboBoxStoreBody(jsonWriter, data.ListItems);
                    }
                    return new JavaScriptCode(sb.ToString());
                }
            }
            return null;
        }

        /// <summary>
        /// Renders the change listener.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="data">The <see cref="CellRenderingEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        internal static object RenderChangeListener(GridCustomColumn column, CellRenderingEventArgs data)
        {
            if (column != null && (!String.IsNullOrEmpty(column.DataMember) || column.HasColumnActionListeners))
            {
                if (column != null && column.HasColumnActionListeners)
                {
                    string script = @"{{blur: function (view, e, eOpts) {{{0} {1}}}}}";
                    return new JavaScriptCode(String.Format(script, RenderStoreUpdate(column, data), RenderRaiseColumnAction(column, data)));
                }
            }
            return null;
        }
        /// <summary>
        /// Renders the store update.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="data">The <see cref="CellRenderingEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        private static object RenderStoreUpdate(GridCustomColumn column, CellRenderingEventArgs data)
        {
            if (column != null && !String.IsNullOrEmpty(column.DataMember))
            {
                string script = @"var grid = VT_GetCmp('{0}');
        if (grid != null) {{
            var record = grid.getStore().getAt({1});
            if (record != null) {{
                record.set('{2}', view.getValue());
            }}
        }}";
                return new JavaScriptCode(String.Format(script, column.GridElement.ID, data.RowIndex, column.DataMember));
            }
            return String.Empty;
        }
        /// <summary>
        /// Renders the raise column action.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="data">The <see cref="CellRenderingEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        private static object RenderRaiseColumnAction(GridCustomColumn column, CellRenderingEventArgs data)
        {
            if (column != null && column.HasColumnActionListeners)
            {
                string script = @"VT_raiseEvent('{0}', 'ColumnAction', {{ newValue: view.getValue() }});";
                return new JavaScriptCode(String.Format(script, VisualElement.GetElementId(column)));
            }
            return String.Empty;
        }
        /// <summary>
        /// Renders the value.
        /// </summary>
        /// <param name="data">The <see cref="CellRenderingEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        internal static string RenderValue(CellRenderingEventArgs data)
        {
            if (data != null && data.Value != null)
            {
                return data.Value.ToString();
            }

            return String.Empty;
        }
    }
}
