var wcmp = Ext.getCmp(<#= String.Format("w-{0}-{1}", element.ClientID, data.RowIndex) #>);
if (wcmp != null) {wcmp.destroy();}
wcmp = Ext.widget(<#= GridCustomColumnExtJSRenderer.ReviseWidgetType(data)#>, {
    id: <#= String.Format("w-{0}-{1}", element.ClientID, data.RowIndex) #>,
    renderTo: <#= String.Format("{0}-{1}", element.ClientID, data.RowIndex) #>,
    store: <#= GridCustomColumnExtJSRenderer.RenderStore(data)#>,
    text: <#= GridCustomColumnExtJSRenderer.RenderValue(data)#>,
    value: <#= GridCustomColumnExtJSRenderer.RenderValue(data)#>,
    displayField: "comboName",
    valueField: "comboValue",
    scale: 'small',
    hideLabel: true,

    listeners: <#= GridCustomColumnExtJSRenderer.RenderChangeListener(element, data)#>
    });

var grd = Ext.Component.fromElement(wcmp.renderTo, 'gridpanel');

if(grd){
    clearTimeout(grd.syncRowHeightTimer);
    grd.syncRowHeightTimer = setTimeout(function(){

        grd && grd.ownerGrid && grd.ownerGrid.syncRowHeights();
    }, 100);
    
}