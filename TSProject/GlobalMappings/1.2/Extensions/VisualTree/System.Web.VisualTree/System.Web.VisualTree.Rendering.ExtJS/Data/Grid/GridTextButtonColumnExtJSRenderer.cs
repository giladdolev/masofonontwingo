﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GridTextButtonColumn), "GridColumnExtJSRenderer", "widgetcolumn", "Ext.grid.column.Widget")]
    public class GridTextButtonColumnExtJSRenderer : GridTextButtonColumnExtJSRendererBase
    {

        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.RenderProperties(context, visualElement, jsonWriter);
            StringBuilder stringRenderer = new StringBuilder();
            // Render GridTextButtonColumn widget .
            GridTextButtonColumn gridTextButtonColumn = visualElement as GridTextButtonColumn;

            if (gridTextButtonColumn != null)
            {
                GridElement grd = gridTextButtonColumn.Parent as GridElement;
                jsonWriter.WritePropertyName("widget");
                // Render chart object
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteProperty("xtype", "container");
                    jsonWriter.WritePropertyName("layout");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("type", "hbox");
                    }
                    jsonWriter.WritePropertyName("items");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WriteProperty("xtype", "textfield");
                        jsonWriter.WriteProperty("flex", "1");
                        jsonWriter.WriteProperty("readOnly", gridTextButtonColumn.TextReadOnly);
                        ReviseTextRenderListeners(grd, gridTextButtonColumn, jsonWriter);
                        jsonWriter.WriteEndObject();
                        jsonWriter.WriteStartObject();
                        jsonWriter.WriteProperty("xtype", "button");
                        jsonWriter.WriteProperty("text", gridTextButtonColumn.ButtonText);
                        if (grd != null)
                        {
                            if (grd.HasCellButtonClickListeners)
                            {

                                stringRenderer.Append(@" {click: function () {");
                                stringRenderer.AppendFormat("VT_raiseEvent('{0}', 'CellButtonClick' ,{{}});}}}}", VisualElement.GetElementId(grd));
                                JavaScriptCode javaSCriptCode = new JavaScriptCode(stringRenderer.ToString());
                                jsonWriter.WriteProperty("listeners", javaSCriptCode);
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                }
                stringRenderer = new StringBuilder();
                // render data member . 
                stringRenderer.Append(@"function (col, widget, rec) {displayRef = widget.down('textfield');");
                stringRenderer.AppendFormat(" displayRef.setValue(rec.get('{0}'));", gridTextButtonColumn.DataMember);
                stringRenderer.Append(@"}");
                JavaScriptCode js = new JavaScriptCode(stringRenderer.ToString());

                jsonWriter.WriteProperty("onWidgetAttach", js);


            }
        }

        /// <summary>
        /// Render focus event and leave event .
        /// from focus event raise the cell mouse down with column index and row index paranms.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        private void ReviseTextRenderListeners(GridElement parentElement, GridTextButtonColumn visualElement, JsonTextWriter jsonWriter)
        {
            StringBuilder stringRenderer = new StringBuilder();
            if (parentElement != null)
            {
                stringRenderer.Append(@" {focus: function (view) {");
                stringRenderer.AppendFormat("VT_raiseEvent('{0}', 'CellMouseDown' ,{{cellIndex : VT_GetWidgetColumnIndex(VT_GetCmp('{0}').columns,'{1}'),rowIndex:VT_GetWidgetRowIndex(view)  }});}},", VisualElement.GetElementId(parentElement), visualElement.DataMember);
                stringRenderer.Append(@" blur: function () {");
                stringRenderer.AppendFormat("VT_raiseEvent('{0}', 'CellEndEdit' ,{{newValue : this.getValue()}});}}}}", VisualElement.GetElementId(parentElement));
                JavaScriptCode javaSCriptCode = new JavaScriptCode(stringRenderer.ToString());
                jsonWriter.WriteProperty("listeners", javaSCriptCode);
            }
        }

    }
}
