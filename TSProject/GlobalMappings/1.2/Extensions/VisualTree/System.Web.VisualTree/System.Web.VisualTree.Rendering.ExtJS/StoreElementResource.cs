﻿using Newtonsoft.Json;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    /// <summary>
    /// Provides support for rendering ExtJS stores as a resource
    /// </summary>
    public class StoreElementResource : VisualElementResource
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreElementResource" /> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public StoreElementResource(IVisualElement visualElement)
            : base(visualElement)
        {

        }

        /// <summary>
        /// Executes the json result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderJsonResource(RenderingContext renderingContext, JsonTextWriter jsonWriter)
        {
            base.RenderJsonResource(renderingContext, jsonWriter);
        }
    }
}
