﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Elements;
using Newtonsoft.Json.Linq;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Engine;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Drawing;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.UI.WebControls;
using System.IO;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    public abstract class VisualElementExtJSRenderer : VisualElementRenderer
    {
        protected static readonly string _defaultExtJSType = "Ext.form.Label";
        protected static readonly string _defaultXType = "label";


        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public abstract string ExtJSType
        {
            get;
        }

        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public abstract string XType
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is touch environment.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is touch environment; otherwise, <c>false</c>.
        /// </value>
        protected bool IsTouchEnvironment
        {
            get
            {
                return ApplicationElement.CurrentRenderingEnvironment == RenderingEnvironment.ExtJSTouch;
            }
        }


        /// <summary>
        /// Gets the display text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        protected virtual string GetDisplayText(string text)
        {
            return text;
        }

        /// <summary>
        /// Gets the client event callback method.
        /// </summary>
        /// <param name="eventBehaviorType">The event behavior type.</param>
        /// <returns></returns>
        protected string GetClientEventCallbackMethod(ExtJSEventBehaviorType eventBehaviorType)
        {
            switch (eventBehaviorType)
            {
                case ExtJSEventBehaviorType.Queued:
                    return "VT_queueEvent";
                case ExtJSEventBehaviorType.Delayed:
                    return "VT_raiseDelayedEvent";
                case ExtJSEventBehaviorType.Immediate:
                    return "VT_raiseEvent";
                default:
                    return "VT_raiseEvent";
            }
        }

        /// <summary>
        /// Gets the formatted text property.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        protected string GetFormatedTextProperty(ControlElement controlElement, string text)
        {
            if (RequiresFormatedText)
            {
                if (controlElement.ForeColor != Color.Empty)
                {
                    StringBuilder spanBuffer = new StringBuilder("<span style=\"");
                    spanBuffer.AppendFormat("color:{0};", ColorTranslator.ToHtml(controlElement.ForeColor));
                    spanBuffer.Append("\">");
                    spanBuffer.Append(HttpUtility.HtmlEncode(text));
                    spanBuffer.Append("</span>");
                    return spanBuffer.ToString();
                }
            }


            return text;
        }

        /// <summary>
        /// Gets a value indicating whether requires formatted text.
        /// </summary>
        /// <value>
        ///   <c>true</c> if requires formatted text; otherwise, <c>false</c>.
        /// </value>
        protected virtual bool RequiresFormatedText
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public virtual void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Render id property
            RenderItemIdProperty(visualElement, jsonWriter);

            // Render events
            RenderEventsProperty(renderingContext, visualElement, jsonWriter);

            // Render content properties
            RenderContentProperties(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public virtual void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the data source property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public virtual void RenderDataSourceProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the binding manager
            BindingManager bindingManager = BindingManager.GetBindingManager(visualElement);

            // If there is a valid binding manager
            if (bindingManager != null)
            {
                // Get renderer
                VisualElementExtJSRenderer renderer = VisualTreeManager.GetRenderer(renderingContext, bindingManager) as VisualElementExtJSRenderer;

                // If there is a valid rendere
                if (renderer != null)
                {
                    // Write the store property
                    jsonWriter.WritePropertyName("store");

                    // Render the store
                    renderer.Render(renderingContext, bindingManager, jsonWriter);
                }

            }
        }


        /// <summary>
        /// Gets the display text without commands.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        protected static string GetDisplayTextWithoutCommands(string text)
        {
            // Return client text
            return string.IsNullOrEmpty(text) ? string.Empty : text.Replace("&", string.Empty);
        }

        /// <summary>
        /// Renders the ViewConfig property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderViewConfigProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderviewConfigObjectProperty(renderingContext, visualElement, jsonWriter, "viewConfig");

        }

        private void RenderviewConfigObjectProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter, string extjsViewConfigType)
        {
            RenderBlockProperty(renderingContext, visualElement, jsonWriter, extjsViewConfigType, (rc, ve, jw) =>
            {
                // Write ViewConfig object properties
                RenderViewConfig(rc, ve, jw, extjsViewConfigType);  // Write ViewConfig object properties
            });
        }

        /// <summary>
        /// Renders the style.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonEventWriter">The json event writer.</param>
        /// <param name="extjsStyleType">Type of the extjs style.</param>
        private void RenderViewConfig(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonEventWriter, string extjsViewConfigType)
        {

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.ViewConfig);

            switch (extjsViewConfigType)
            {
                case "viewConfig":
                    renderingContext.RenderingType = ExtJSRenderingType.ViewConfig;
                    RenderViewConfigContentProperties(renderingContext, visualElement, jsonEventWriter);
                    break;
            }
        }

        /// <summary>
        /// Renders the style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderStyleProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderStyleObjectProperty(renderingContext, visualElement, jsonWriter, "style");

        }

        private void RenderStyleObjectProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter, string extjsStyleType)
        {
            RenderBlockProperty(renderingContext, visualElement, jsonWriter, extjsStyleType, (rc, ve, jw) =>
            {
                // Write style object properties
                RenderStyle(rc, ve, jw, extjsStyleType);  // Write style object properties
            });
        }

        /// <summary>
        /// Renders the style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderFieldStyleProperty(RenderingContext renderingContext, VisualElement visualElement,
            JsonTextWriter jsonWriter)
        {
            RenderStyleProperty(renderingContext, visualElement, jsonWriter, "fieldStyle");
        }

        /// <summary>
        /// Renders the style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderBodyStyleProperty(RenderingContext renderingContext, VisualElement visualElement,
            JsonTextWriter jsonWriter)
        {
            RenderStyleProperty(renderingContext, visualElement, jsonWriter, "bodyStyle");
        }


        /// <summary>
        /// Renders the style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderStyleProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter, string extjsStyleType)
        {
            StringBuilder sb = new StringBuilder();
            using (JsonTextWriter jsonEventWriter = RenderingUtils.GetJsonWriter(sb))
            {
                RenderStyle(renderingContext, visualElement, jsonEventWriter, extjsStyleType);
                jsonEventWriter.Flush();
            }

            string eventsJson = sb.ToString();

            // If listener had been generated
            if (!String.IsNullOrEmpty(eventsJson))
            {
                // Start the listeners object
                jsonWriter.WritePropertyName(extjsStyleType);
                jsonWriter.WriteRawValue(String.Concat("'", eventsJson, "'"));
            }
        }

        /// <summary>
        /// Renders the style.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonEventWriter">The json event writer.</param>
        /// <param name="extjsStyleType">Type of the extjs style.</param>
        private void RenderStyle(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonEventWriter, string extjsStyleType)
        {
            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Style);

            switch (extjsStyleType)
            {
                case "fieldStyle":
                    renderingContext.RenderingType = ExtJSRenderingType.FieldStyle;
                    RenderFieldStyleContentProperties(renderingContext, visualElement, jsonEventWriter);
                    break;
                case "bodyStyle":
                    renderingContext.RenderingType = ExtJSRenderingType.BodyStyle;
                    RenderBodyStyleContentProperties(renderingContext, visualElement, jsonEventWriter);
                    break;
                case "style":
                    RenderStyleObjectProperties(renderingContext, visualElement, jsonEventWriter);
                    break;
            }
        }

        /// <summary>
        /// Renders the style object properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        public virtual void RenderStyleObjectProperties(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter styleWriter)
        {
            RenderStyleContentProperties(renderingContext, visualElement, styleWriter);

            RenderStyleBackColorProperty(visualElement, styleWriter);
            RenderStyleForeColorProperty(visualElement, styleWriter);
            RenderStyleFontProperty(visualElement, styleWriter);
            RenderStyleBorderProperty(renderingContext, visualElement, styleWriter);
        }

        /// <summary>
        /// Normalizes the style value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public string NormalizeStyleValue(string value, params object[] parameters)
        {
            if (parameters != null & parameters.Length > 0)
            {
                return String.Format(value, parameters);
            }

            return value;
        }


        /// <summary>
        /// Renders the style back color property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        protected virtual void RenderStyleBackColorProperty(VisualElement visualElement, JsonTextWriter styleWriter)
        {

        }

        /// <summary>
        /// Renders the style fore color property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        protected virtual void RenderStyleForeColorProperty(VisualElement visualElement, JsonTextWriter styleWriter)
        {

        }

        /// <summary>
        /// Renders the style font property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        protected virtual void RenderStyleFontProperty(VisualElement visualElement, JsonTextWriter styleWriter)
        {

        }

        /// <summary>
        /// Renders the ViewConfig content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The ViewConfig writer.</param>
        public virtual void RenderViewConfigContentProperties(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter styleWriter)
        {

        }

        /// <summary>
        /// Renders the style border property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        protected virtual void RenderStyleBorderProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter styleWriter)
        {

        }

        /// <summary>
        /// Renders the style content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        public virtual void RenderStyleContentProperties(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter styleWriter)
        {
            this.RenderNativeStylesProperty(renderingContext, visualElement, styleWriter);
        }

        /// <summary>
        /// Renders the native styles property
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        protected virtual void RenderNativeStylesProperty(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter styleWriter)
        {
            // If there is a valid element
            if (visualElement != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Style)
                {
                    ReadOnlyDictionary<string, string> objNativeStylesDictionary = visualElement.NativeStyles;
                    if (objNativeStylesDictionary != null)
                    {
                        foreach (KeyValuePair<string, string> style in objNativeStylesDictionary)
                        {
                            styleWriter.WriteProperty(string.Format("'{0}'", style.Key), style.Value);

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Renders the native styles property changed
        /// </summary>
        /// <param name="renderingContext">The rendering context</param>
        /// <param name="visualElement">The visual element</param>
        /// <param name="builder">The builder</param>
        /// <param name="componentVariable">The component variable</param>
        protected virtual void RenderNativeStylesPropertyChange(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // If there is a valid element
            if (visualElement != null)
            {
                ReadOnlyDictionary<string, string> objNativeStylesDictionary = visualElement.NativeStyles;
                if (objNativeStylesDictionary != null)
                {
                    foreach (KeyValuePair<string, string> style in objNativeStylesDictionary)
                    {
                        builder.AppendFormattedRaw(@"{0}.setStyle({{'{1}': '{2}'}});",
                            new JavaScriptCode(componentVariable), style.Key, style.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Renders the style content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        public virtual void RenderFieldStyleContentProperties(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter styleWriter)
        {

        }

        /// <summary>
        /// Renders the style content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="styleWriter">The style writer.</param>
        public virtual void RenderBodyStyleContentProperties(ExtJSRenderingContext renderingContext, VisualElement visualElement, JsonTextWriter styleWriter)
        {

        }


        /// <summary>
        /// Renders the controls content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected static void RenderControlsContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderControlsContentProperties(renderingContext, visualElement, jsonWriter, true);
        }


        /// <summary>
        /// Renders the controls content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="blnReverseNonDockedControls">if set to <c>true</c> [BLN reverse non docked controls].</param>
        protected static void RenderControlsContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter, bool blnReverseNonDockedControls)
        {
            // Get the control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // If is a touch control
                if (RenderingEnvironment.GetRenderingEnvironment(renderingContext) == RenderingEnvironment.ExtJSTouch)
                {
                    // Render plain items
                    RenderTouchControlsContentProperties(renderingContext, visualElement, jsonWriter, controlElement.Controls);
                }
                else
                {
                    // Render controls content properties
                    RenderControlsContentProperties(renderingContext, jsonWriter, controlElement.Controls, blnReverseNonDockedControls);
                }
            }
        }

        /// <summary>
        /// Renders the controls content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="childControls">The child controls.</param>
        /// <param name="blnReverseNonDockedControls">if set to <c>true</c> [BLN reverse non docked controls].</param>
        protected static void RenderControlsContentProperties(RenderingContext renderingContext, JsonTextWriter jsonWriter, IEnumerable<ControlElement> childControls, bool blnReverseNonDockedControls)
        {
            Dictionary<PanelLayout, IEnumerable<ControlElement>> layoutsData = GetContainerLayout(childControls, blnReverseNonDockedControls);

            if (layoutsData != null && layoutsData.Count > 0)
            {
                if (layoutsData.Count == 1)
                {
                    KeyValuePair<PanelLayout, IEnumerable<ControlElement>> layoutData = layoutsData.First();

                    // Indicate an border layout
                    jsonWriter.WriteProperty("layout", layoutData.Key.ToString().ToLower());

                    // Render docked controls
                    RenderPlainItemsProperty(renderingContext, layoutData.Value, jsonWriter);

                }
                else
                {
                    // Indicate an absolute layout
                    jsonWriter.WriteProperty("layout", "absolute");

                    jsonWriter.WritePropertyName("items");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                    {
                        foreach (KeyValuePair<PanelLayout, IEnumerable<ControlElement>> layoutData in layoutsData)
                        {
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                jsonWriter.WriteProperty("xtype", "container");
                                jsonWriter.WriteProperty("layout", layoutData.Key.ToString().ToLower());
                                jsonWriter.WriteProperty("anchor", "100% 100%");
                                jsonWriter.WriteProperty("x", 0);
                                jsonWriter.WriteProperty("y", 0);
                                RenderPlainItemsProperty(renderingContext, layoutData.Value, jsonWriter);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the container layouts.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static IEnumerable<PanelLayout> GetControlLayout(VisualElement element)
        {
            ControlElement parentElement = element as ControlElement;
            if (parentElement != null)
            {
                Dictionary<PanelLayout, IEnumerable<ControlElement>> layoutData = GetContainerLayout(parentElement.Controls, false);

                return from data in layoutData
                       select data.Key;
            }

            return new PanelLayout[] { };
        }

        /// <summary>
        /// Gets the container layouts.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static PanelLayout GetParentLayout(VisualElement element)
        {
            ControlElement parentElement = element.ParentElement as ControlElement;
            if (parentElement != null)
            {
                Dictionary<PanelLayout, IEnumerable<ControlElement>> layoutData = GetContainerLayout(parentElement.Controls, false);

                return (from data in layoutData
                        where data.Value.Contains(element)
                        select data.Key).FirstOrDefault();
            }

            return PanelLayout.Auto;
        }

        /// <summary>
        /// Gets the container layout.
        /// </summary>
        /// <param name="childControls">The child controls.</param>
        /// <param name="blnReverseNonDockedControls">if set to <c>true</c> reverse non docked controls.</param>
        /// <returns></returns>
        private static Dictionary<PanelLayout, IEnumerable<ControlElement>> GetContainerLayout(IEnumerable<ControlElement> childControls, bool blnReverseNonDockedControls)
        {
            Dictionary<PanelLayout, IEnumerable<ControlElement>> layoutData = new Dictionary<PanelLayout, IEnumerable<ControlElement>>();
            // If there is a valid child controls enumerator
            if (childControls != null)
            {
                List<ControlElement> dockedControls = new List<ControlElement>(),
                                        anchoredControls = new List<ControlElement>(),
                                        fillDockControls = new List<ControlElement>();

                foreach (ControlElement controlElement in childControls)
                {
                    if (IsDockedControl(controlElement))
                    {
                        if (controlElement.Dock == Dock.Fill)
                        {
                            fillDockControls.Add(controlElement);
                        }
                        else
                        {
                            dockedControls.Add(controlElement);
                        }
                    }
                    else
                    {
                        anchoredControls.Add(controlElement);
                    }
                }


                // Reverse docked controls
                dockedControls.Reverse();

                // If should reverse non docked controls
                if (blnReverseNonDockedControls)
                {
                    anchoredControls.Reverse();
                }

                if (fillDockControls.Count > 1)
                {
                    layoutData.Add(PanelLayout.Center, fillDockControls);
                }
                else
                {
                    dockedControls.AddRange(fillDockControls);
                }

                if (dockedControls.Count > 0)
                {
                    layoutData.Add(PanelLayout.Border, dockedControls);
                }

                if (anchoredControls.Count > 0)
                {
                    layoutData.Add(PanelLayout.Absolute, anchoredControls);
                }
            }

            return layoutData;
        }

        /// <summary>
        /// Renders the touch controls content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The touch element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="elements">The elements.</param>
        protected static void RenderTouchControlsContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter, IEnumerable<VisualElement> elements)
        {
            // The default layout
            string layout = null;

            // Get the container element
            ITVisualContainerElement containerElement = visualElement as ITVisualContainerElement;

            // If there is a valid container element
            if (containerElement != null)
            {
                // Choose layout
                switch (containerElement.Layout)
                {
                    case TVisualLayout.HBox:
                        layout = "hbox";
                        break;
                    case TVisualLayout.VBox:
                        layout = "vbox";
                        break;
                    case TVisualLayout.Card:
                        layout = "card";
                        break;
                    case TVisualLayout.Float:
                        layout = "float";
                        break;
                    case TVisualLayout.Fit:
                        layout = "fit";
                        break;
                    case TVisualLayout.Dock:
                        layout = "hbox";
                        break;
                    default:
                        break;
                }
            }

            // If there is no predefined layout
            else
            {
                // Calculate touch controls render
                layout = CalculateTouchControlsLayout(elements);
            }

            // If there is a valid layout
            if (!string.IsNullOrEmpty(layout))
            {
                // Render the layout property
                //jsonWriter.WriteProperty("layout", layout);
                jsonWriter.WritePropertyName("layout");
                jsonWriter.WriteStartObject();
                jsonWriter.WriteProperty("type", layout);
                jsonWriter.WriteProperty("pack", "start");
                jsonWriter.WriteProperty("align", "stretch");

                jsonWriter.WriteEndObject();

            }

            // Render items
            RenderPlainItemsProperty(renderingContext, elements, jsonWriter);
        }

        /// <summary>
        /// Calculates the touch controls layout.
        /// </summary>
        /// <param name="elements">The elements.</param>
        /// <returns></returns>
        private static string CalculateTouchControlsLayout(IEnumerable<VisualElement> elements)
        {
            bool hasDocked = false;

            bool hasAnchored = false;

            // Loop all elements
            foreach (VisualElement element in elements)
            {
                // Get control
                ControlElement control = element as ControlElement;

                // If there is a valid control
                if (control != null)
                {
                    // If is docked
                    if (control.Dock != Dock.None)
                    {

                        // Indicate docked
                        hasDocked = true;
                    }
                    else
                    {
                        // Indicate anchored
                        hasAnchored = true;
                    }
                }
            }

            // If found both anchoring and docking
            if (hasAnchored && hasDocked)
            {
                //throw new Exception("Mobile cannot contain anchored and docked items under same parent.");
                return "hbox";
            }
            else if (hasDocked)
            {
                return "vbox";
            }
            else if (hasAnchored)
            {
                return "vbox";
            }
            else
            {
                return "fit";
            }
        }

        /// <summary>
        /// Renders the controls content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="childControls">The child controls.</param>
        protected static void RenderControlsContentProperties(RenderingContext renderingContext, JsonTextWriter jsonWriter, IEnumerable<ControlElement> childControls)
        {
            RenderControlsContentProperties(renderingContext, jsonWriter, childControls, true);
        }


        /// <summary>
        /// Determines whether the specified control is docked control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        private static bool IsDockedControl(ControlElement control)
        {
            if (control != null)
            {
                return control.Dock != Dock.None && !(control is ContextMenuStripElement);
            }

            return false;
        }

        /// <summary>
        /// Renders the store property.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="storeItems">The store items.</param>
        protected virtual void RenderStoreProperty(VisualElement element, JsonTextWriter jsonWriter, IEnumerable<object> storeItems)
        {
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            // If there are store items
            if (storeItems != null && storeItems.Count() > 0)
            {

                // Write the store property
                jsonWriter.WritePropertyName("store");

                // Write start array
                jsonWriter.WriteStartArray();

                // The item index
                int itemIndex = 0;

                // Loop all items
                foreach (object item in storeItems)
                {
                    RenderStoreItem(element, jsonWriter, itemIndex++, item);
                }
            }

            // Write the end array
            jsonWriter.WriteEndArray();
        }

        /// <summary>
        /// Renders the store item.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="itemIndex">Index of the item.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        protected virtual void RenderStoreItem(VisualElement element, JsonTextWriter jsonWriter, int itemIndex, object item)
        {
            IListItems elementWithItems = element as IListItems;

            if (elementWithItems != null)
            {
                // Write item as [index, text] array
                jsonWriter.WriteStartArray();
                object itemValue = elementWithItems.GetItemValue(item);
                if (itemValue == null)
                {
                    jsonWriter.WriteValue(itemIndex);
                }
                else
                {
                    jsonWriter.WriteValue(itemValue);
                }

                jsonWriter.WriteValue(elementWithItems.GetItemText(item));
                jsonWriter.WriteEndArray();
            }
        }




        /// <summary>
        /// Renders the plain items property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="items">The items.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="elementPredicate">The element predicate.</param>
        protected static void RenderPlainItemsProperty(RenderingContext renderingContext, IEnumerable<VisualElement> items, JsonTextWriter jsonWriter, string propertyName, Predicate<VisualElement> elementPredicate)
        {

            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            // If there is a valid visual element list
            if (items != null)
            {
                if (!string.IsNullOrEmpty(propertyName))
                {
                    jsonWriter.WritePropertyName(propertyName);
                }

                RenderPlainItemsPropertyValue(renderingContext, items, jsonWriter, elementPredicate);
            }
        }


        /// <summary>
        /// Renders the plain items property value.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="items">The items.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="elementPredicate">The element predicate.</param>
        protected static void RenderPlainItemsPropertyValue(RenderingContext renderingContext, IEnumerable<VisualElement> items, JsonTextWriter jsonWriter, Predicate<VisualElement> elementPredicate = null)
        {
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
            {
                if (items != null && items.Any())
                {
                    // Loop all control elements
                    foreach (VisualElement itemElement in items)
                    {
                        // If no predicate or valid predicate
                        if (elementPredicate == null || elementPredicate(itemElement))
                        {
                            // Get renderer
                            VisualElementExtJSRenderer objVisualElementExtJSRenderer = VisualTreeManager.GetRenderer(renderingContext, itemElement) as VisualElementExtJSRenderer;

                            // If there is a valid renderer
                            if (objVisualElementExtJSRenderer != null)
                            {
                                // Render object
                                objVisualElementExtJSRenderer.Render(renderingContext, itemElement, jsonWriter);
                            }
                        }
                    }
                }
            }
        }




        /// <summary>
        /// Renders the plain items property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="items">The items.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="propertyName">Name of the property.</param>
        protected static void RenderPlainItemsProperty(RenderingContext renderingContext, IEnumerable<VisualElement> items, JsonTextWriter jsonWriter, string propertyName)
        {
            RenderPlainItemsProperty(renderingContext, items, jsonWriter, propertyName, null);

        }

        /// <summary>
        /// Renders the plain items property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="items">The items.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected static void RenderPlainItemsProperty(RenderingContext renderingContext, IEnumerable<VisualElement> items, JsonTextWriter jsonWriter)
        {
            RenderPlainItemsProperty(renderingContext, items, jsonWriter, "items", null);

        }


        /// <summary>
        /// Renders the identifier property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderItemIdProperty(VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderIdProperty(visualElement, "itemId", jsonWriter);
        }

        /// <summary>
        /// Renders the identifier property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="idProperty">The identifier property.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected static void RenderIdProperty(VisualElement visualElement, string idProperty, JsonWriter jsonWriter)
        {
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            // If there is a valid visual element
            if (visualElement != null)
            {
                // Get client ID from visual element
                string clientId = visualElement.ClientID;

                // If there is a valid id
                if (!string.IsNullOrEmpty(clientId))
                {
                    WidgetColumn clm = visualElement.ParentElement as WidgetColumn;
                    if (clm != null)
                    {
                        return;
                    }
                    jsonWriter.WritePropertyName(idProperty);
                    jsonWriter.WriteValue(clientId);
                }
            }
        }
        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderEventsProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderBlockProperty(renderingContext, visualElement, jsonWriter, "listeners", RenderEvents);
        }

        /// <summary>
        /// Renders the block property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="propertyName">Name of the property.</param>
        protected void RenderBlockProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter, string propertyName, Action<RenderingContext, VisualElement, JsonTextWriter> renderBlock)
        {
            StringBuilder sb = new StringBuilder();
            using (JsonTextWriter jsonEventWriter = RenderingUtils.GetJsonWriter(sb))
            {
                renderBlock(renderingContext, visualElement, jsonEventWriter);
                jsonEventWriter.Flush();
            }

            string eventsJson = sb.ToString();

            // If listener had been generated
            if (!String.IsNullOrEmpty(eventsJson))
            {

                // Start the listeners object
                jsonWriter.WritePropertyName(propertyName);
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteRaw(eventsJson);
                }
            }
        }

        /// <summary>
        /// Renders the events.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="getWriter">The get writer.</param>
        protected virtual void RenderEvents(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter getWriter)
        {

        }

        /// <summary>
        /// Gets the action arguments from js.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement"></param>
        /// <param name="strActionName"></param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected internal override EventArgs GetEventArgs(RenderingContext renderingContext, VisualElement visualElement, string strActionName, JObject eventObject)
        {
            return this.ExtractCallbackEventArgs(renderingContext, visualElement, strActionName, eventObject);
        }

        /// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected virtual EventArgs ExtractCallbackEventArgs(RenderingContext renderingContext, VisualElement visualElement, string serverEventName, JObject clientEventArgs)
        {
            return new EventArgs();
        }

        /// <summary>
        /// Renders the specified object visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public virtual void Render(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            if (visualElement != null)
            {
                // Open element object
                jsonWriter.WriteStartObject();

                // Render xtype property
                RenderXTypeProperty(renderingContext, visualElement, jsonWriter);

                // Write the element properties
                RenderProperties(renderingContext, visualElement, jsonWriter);

                // Close element object
                jsonWriter.WriteEndObject();
            }
        }

        /// <summary>
        /// Renders the x type property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected virtual void RenderXTypeProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Write the xtype property
            RenderProperty(renderingContext, jsonWriter, "xtype", this.ReviseXTypePropertyValue(visualElement, XType));
        }

        /// <summary>
        /// Revises the XTYPE property value.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <returns></returns>
        protected virtual string ReviseXTypePropertyValue(VisualElement visualElement, string xtype)
        {
            return xtype;
        }

        /// <summary>
        /// Renders the update.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        internal virtual void RenderUpdate(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        internal void RenderMethod(RenderingContext renderingContext, IVisualElementMethodInvoker methodInvoker, JsonTextWriter jsonWriter)
        {
            // The component variable name
            const string componentVariableName = "cmp";

            // If there is a valid method invoker
            if (methodInvoker != null)
            {
                VisualElement ownerElement = methodInvoker.Owner as VisualElement;

                // If there is a valid owner element
                if (ownerElement != null)
                {
                    // The method content builder
                    JavaScriptBuilder builder = null;

                    bool componentIfAdded = false;

                    // Render the method content
                    this.RenderMethod(renderingContext, methodInvoker, ownerElement, methodInvoker.Name, methodInvoker.Data, componentVariableName, (getComponent) =>
                    {
                        // If we need to create the java script builder
                        if (builder == null)
                        {
                            // Create the java script builder
                            builder = new JavaScriptBuilder();

                            // Add the start anonymous function code
                            builder.AppendStartAnonymousFunction();

                            // If we should get component
                            if (getComponent)
                            {
                                // If we should render instance call
                                if (this.ShouldRenderInstanceCall(methodInvoker.Name))
                                {
                                    // Render the component variable
                                    this.RenderComponentVariable(builder, componentVariableName, ownerElement);

                                    // Indicate if was added
                                    componentIfAdded = true;

                                    // If there is a valid component
                                    builder.AppendStartIf(componentVariableName);
                                }
                            }
                        }

                        return builder;
                    });


                    // If we need to close the get element if statement
                    if (builder != null)
                    {
                        if (componentIfAdded)
                        {
                            // AllowAppend end if
                            builder.AppendEndIf();
                        }

                        // Add the end anonymous function code
                        builder.AppendEndAnonymousFunction();

                        // Write the anonymous function as an array value as in [fn,fn,fn...]
                        jsonWriter.WriteRawValue(builder.ToString());
                    }

                }
                else
                {
                    // The method content builder
                    JavaScriptBuilder builder = null;

                    // Render the method content
                    this.RenderStaticMethod(renderingContext, methodInvoker, methodInvoker.Name, methodInvoker.Data, (getComponent) =>
                    {
                        // If we need to create the java script builder
                        if (builder == null)
                        {
                            // Create the java script builder
                            builder = new JavaScriptBuilder();

                            // Add the start anonymous function code
                            builder.AppendStartAnonymousFunction();
                        }

                        return builder;
                    });


                    // If we need to close the get element if statement
                    if (builder != null)
                    {
                        // Add the end anonymous function code
                        builder.AppendEndAnonymousFunction();

                        // Write the anonymous function as an array value as in [fn,fn,fn...]
                        jsonWriter.WriteRawValue(builder.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Renders the component variable.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariableName">Name of the component variable.</param>
        /// <param name="visualElement">The visual element.</param>
        protected virtual void RenderComponentVariable(JavaScriptBuilder builder, string componentVariableName, VisualElement visualElement)
        {
            // Get element id
            string elementId = VisualElement.GetElementId(visualElement);

            // Get component 
            builder.AppendVariableWithInvocationInitializer(componentVariableName, "VT_GetCmp", elementId);
        }

        /// <summary>
        /// Shoulds the render instance call.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        protected virtual bool ShouldRenderInstanceCall(string methodName)
        {
            return true;
        }



        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public virtual void RenderMethod(RenderingContext renderingContext, IVisualElementMethodInvoker methodInvoker, VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            // If we have the property change method name
            if (string.Equals(methodName, VisualElement.PropertyChangeMethodName, StringComparison.OrdinalIgnoreCase))
            {
                // Get changed properties list
                HashSet<string> changedProperties = methodData as HashSet<string>;

                // If there is a valid changed properties list
                if (changedProperties != null)
                {
                    // Render property change
                    this.RenderPropertyChanges(renderingContext, visualElement, changedProperties, getJavaScriptBuilder(true), componentVariableName);
                }
            }
            else if (string.Equals(methodName, VisualElement.AddListenerMethodName, StringComparison.OrdinalIgnoreCase))
            {
                // Get event name
                string eventNameAdded = methodData as string;

                // If there is a valid changed properties list
                if (eventNameAdded != null)
                {
                    this.RenderAddListener(renderingContext, visualElement, eventNameAdded, getJavaScriptBuilder(true), componentVariableName);
                }

            }
            else if (string.Equals(methodName, VisualElement.RemoveListenerMethodName, StringComparison.OrdinalIgnoreCase))
            {
                // Get event name
                string eventNameRemoved = methodData as string;

                // If there is a valid changed properties list
                if (eventNameRemoved != null)
                {
                    this.RenderRemoveListener(renderingContext, visualElement, eventNameRemoved, getJavaScriptBuilder(true), componentVariableName);
                }
            }
        }


        protected virtual void RenderAddListener(RenderingContext renderingContext, VisualElement visualElement, string eventName, JavaScriptBuilder builder, string componentVariable)
        {

        }

        protected virtual void RenderRemoveListener(RenderingContext renderingContext, VisualElement visualElement, string eventName, JavaScriptBuilder builder, string componentVariable)
        {

        }



        /// <summary>
        /// Renders the method.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder.</param>
        public virtual void RenderStaticMethod(RenderingContext renderingContext, IVisualElementMethodInvoker methodInvoker, string methodName, object methodData, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {

        }

        /// <summary>
        /// Renders the action code.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="strActionName">The action name.</param>
        /// <param name="strEventName">The event name.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public virtual void RenderActionCode(RenderingContext renderingContext, VisualElement visualElement, string strActionName, string strEventName, JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Render the value to json value.
        /// </summary>
        /// <param name="objValue">The value to render.</param>
        /// <returns></returns>
        protected static string RenderValue(object objValue)
        {
            return JsonConvert.SerializeObject(objValue);
        }



        /// <summary>
        /// Renders the object value.
        /// </summary>
        /// <param name="arrNameValuePairs">The name value pairs.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">arrNameValuePairs</exception>
        protected static string RenderObjectValue(params string[] arrNameValuePairs)
        {
            // validate parameter
            if (arrNameValuePairs == null)
            {
                throw new ArgumentNullException("arrNameValuePairs");
            }

            // Create builder
            StringBuilder objBuilder = new StringBuilder();
            objBuilder.Append("{");

            // Loop all pairs
            for (int intIndex = 0; intIndex < arrNameValuePairs.Length; intIndex += 2)
            {
                // If we need to add comma
                if (intIndex > 0)
                {
                    // Add comma
                    objBuilder.Append(",");
                }

                // Add name from pair
                objBuilder.Append(arrNameValuePairs[intIndex]);
                objBuilder.Append(":");

                // If there is a valid value
                if (arrNameValuePairs.Length > intIndex + 1)
                {
                    // Add value
                    objBuilder.Append(arrNameValuePairs[intIndex + 1]);
                }
                else
                {
                    // Set default value
                    objBuilder.Append("null");
                }
            }

            objBuilder.Append("}");

            // Return object
            return objBuilder.ToString();
        }


        /// <summary>
        /// Renders visual element update.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer</param>
        /// <param name="objUpdatedProperties">The updated properties.</param>
        /// <returns>
        /// A flag indicating if full update was written (indicates we do not need to update nested visuals).
        /// </returns>
        internal virtual bool RenderUpdate(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter, List<string> objUpdatedProperties)
        {
            return false;
        }



        /// <summary>
        /// Renders the default text property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected static void RenderDefaultTextProperty(RenderingContext renderingContext, VisualElement visualElement, JsonWriter jsonWriter)
        {
            // Get the control element
            ControlElement controlElement = visualElement as ControlElement;

            // The default text
            string defaultText = "?";

            // If there is a valid control element
            if (controlElement != null)
            {
                // Return control element name
                defaultText = controlElement.GetType().Name;
            }

            // Render the height property
            RenderProperty(renderingContext, jsonWriter, "text", defaultText);
        }

        /// <summary>
        /// Renders the default style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected static void RenderDefaultStyleProperty(RenderingContext renderingContext, JsonWriter jsonWriter)
        {
            RenderProperty(renderingContext, jsonWriter, "style", "border:1px solid black");
        }


        /// <summary>
        /// Renders the change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="properties">The changed properties.</param>
        /// <param name="builder">The builder.</param>
        protected internal override void RenderChange(RenderingContext renderingContext, VisualElement visualElement, HashSet<string> properties, JavaScriptBuilder builder)
        {
            // validate parameter
            if (builder == null)
            {
                return;
            }

            // Get element id
            string elementId = VisualElement.GetElementId(visualElement);

            // Get component 
            builder.AppendVariableWithInvocationInitializer("cmp", "VT_GetCmp", elementId);

            // If there is a valid component
            builder.AppendStartIf("cmp");

            // Render cmp change
            RenderPropertyChanges(renderingContext, visualElement, properties, builder, "cmp");

            // AllowAppend end if
            builder.AppendEndIf();
        }

        /// <summary>
        /// Renders the change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="properties">The properties.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable identifier.</param>
        private void RenderPropertyChanges(RenderingContext renderingContext, VisualElement visualElement, HashSet<string> properties, JavaScriptBuilder builder, string componentVariable)
        {
            // If there is a valid properties
            if (properties != null)
            {
                foreach (string property in GetSortedByPriorityProperties(properties))
                {
                    RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                }
            }
        }

        private IEnumerable<string> GetSortedByPriorityProperties(IEnumerable<string> properties)
        {
            Dictionary<string, int> priorities = new Dictionary<string, int>()
            {
                {"$refresh", 20},
                {"DataSource", 10}
            };

            return properties
                        .Select(item => new { property = item, order = priorities.ContainsKey(item) ? priorities[item] : 0 })
                        .OrderByDescending(itm => itm.order)
                        .Select(itm => itm.property);
        }

        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            // Choose property
            switch (property)
            {
                case "DataSource":
                    RenderDataSourceChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "NativeStyles":
                    this.RenderNativeStylesPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
            }
        }

        /// <summary>
        /// Renders the data source change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        private void RenderDataSourceChange(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            string completeCellEditingCode = string.Format("{0}.completeCellEditing && {0}.completeCellEditing();", componentVariable);

            builder.AppendRawValue(completeCellEditingCode);

            string methodName = (renderingContext.Environment == RenderingEnvironment.ExtJSTouch) ? "load()" : "reload()";
            string reloadStoreCode = string.Format("{0}.getStore && {0}.getStore() && {0}.getStore().{1};", componentVariable, methodName);
            builder.AppendRawValue(reloadStoreCode);

        }

        internal enum PanelLayout
        {
            Auto,
            Border,
            Absolute,
            Center,
        }


    }
}
