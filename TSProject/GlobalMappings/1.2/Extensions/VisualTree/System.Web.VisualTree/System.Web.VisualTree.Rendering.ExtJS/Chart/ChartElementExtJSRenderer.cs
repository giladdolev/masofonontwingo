﻿using System.Drawing;
using System.Globalization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Common;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Text;
using System.IO;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(Chart), "ControlElementExtJSRenderer", "cartesian", "Ext.chart.series.Cartesian")]
    //[ExtJSEventDescription("Resize", "resize", ClientEventHandlerParameterNames = "cmp, width, height, oldWidth, oldHeight, eOpts",
    //    ClientHandlerCallbackData = "width: Number.isInteger(width) ? width : (width.width || 0), height: Number.isInteger(height) ? height : (width.height || 0)", ServerEventArgsCreateCode = "new ResizeEventArgs(this.width.ToInt32(), this.height.ToInt32())")]
    public class ChartElementExtJSRenderer : ChartElementExtJSRendererBase
    {
        private enum AxesPosition
        {
            /// <summary>
            /// The bottom
            /// </summary>
            Bottom,
            /// <summary>
            /// The left
            /// </summary>
            Left
        }


        /// <summary>
        /// Renders the specified object visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void Render(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get visual element as chart
            Chart chart = visualElement as Chart;

            // If there is a valid chart
            if (chart != null)
            {
                // If contains only one chart area
                if (ShouldRenderChartContainer(chart))
                {
                    // Render multiple charts
                    RenderCharts(renderingContext, chart, jsonWriter);
                }
                else
                {
                    // Call base render
                    base.Render(renderingContext, visualElement, jsonWriter);
                }
            }
        }


        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            // Choose property
            switch (property)
            {
                case "$refresh":
                    RenderRefreshChartCode(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "Rotate":
                    RenderReload(renderingContext, visualElement as Chart, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }

        }

        /// <summary>
        /// Renders the refresh chart code.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        private static void RenderRefreshChartCode(RenderingContext renderingContext, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get visual element as chart
            Chart chart = visualElement as Chart;

            // If there is a valid chart
            if (chart != null)
            {
                // If contains only one chart area
                if (ShouldRenderChartContainer(chart))
                {

                }
                else
                {
                    RenderUpdateStore(renderingContext, chart, builder, componentVariable);
                }
            }
        }


        /// <summary>
        /// Renders the charts.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderCharts(RenderingContext renderingContext, Chart chart, JsonTextWriter jsonWriter)
        {
            // Render object
            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
            {
                // Write container
                jsonWriter.WriteProperty("xtype", "panel");

                // Is absolute layout chart
                if (IsAbsoluteLayoutChart(chart))
                {
                    jsonWriter.WriteProperty("layout", "absolute");
                }
                else
                {
                    // Write layout
                    jsonWriter.WritePropertyName("layout");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        jsonWriter.WriteProperty("type", "vbox");
                        jsonWriter.WriteProperty("align", "stretch");
                    }
                }

                // Write chart properties
                RenderProperties(renderingContext, chart, jsonWriter);

                // Write items property
                jsonWriter.WritePropertyName("items");

                // Open items array scope
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    // Loop all chart areas
                    foreach (ChartArea chartArea in chart.ChartAreas)
                    {
                        if (chartArea != null)
                        {
                            // Render chart object
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                // Render chart type property
                                RenderXTypeProperty(renderingContext, chart, jsonWriter);

                                // Render chart area as chart content properties
                                RenderChartContentProperties(chart, chartArea, jsonWriter);

                                IEnumerable<Series> series = null;

                                if (chartArea.ID == null)
                                {
                                    series = new Series[] { chart.Series[chart.ChartAreas.IndexOf(chartArea)] };
                                }
                                else
                                {
                                    series = GetChartAreaSeries(chart, chartArea);
                                }

                                // Render chart
                                RenderChart(renderingContext, chart, jsonWriter, chartArea, series);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the chart
            Chart chart = visualElement as Chart;

            // If there is a valid chart with one chart area
            if (chart != null && !ShouldRenderChartContainer(chart))
            {
                // Render chart
                RenderChart(renderingContext, chart, jsonWriter, chart.ChartAreas.FirstOrDefault(), chart.Series);
            }
        }

        /// <summary>
        /// Renders the chart content properties.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="chartArea">The chart area.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private static void RenderChartContentProperties(Chart chart, ChartArea chartArea, JsonTextWriter jsonWriter)
        {
            // If there are valid chart and chart area
            if (chart != null && chartArea != null)
            {
                // Write item id property
                if (chartArea.ID != null)
                {
                    RenderIdProperty(chartArea, "itemId", jsonWriter);
                }
                else
                {
                    jsonWriter.WritePropertyName("itemId");

                    // Generate chart area id.
                    string id = string.Format(CultureInfo.InvariantCulture, "{0}_ChartArea{1}", chart.ID, chart.ChartAreas.IndexOf(chartArea));

                    jsonWriter.WriteValue(id);
                }

                // If chart area is hidden
                if (!chartArea.Visible)
                {
                    // Write hidden property
                    jsonWriter.WriteProperty("hidden", true);
                }

                // Render chart position
                RenderPosition(chart, chartArea.Position, jsonWriter);
            }
        }

        /// <summary>
        /// Renders the position.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="position">The position.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private static void RenderPosition(Chart chart, ElementPosition position, JsonTextWriter jsonWriter)
        {
            // If there is a valid position
            if (position != null)
            {
                // If is not an absolute layout chart.
                if (!IsAbsoluteLayoutChart(chart))
                {
                    jsonWriter.WriteProperty("flex", 1);
                }
                else
                {
                    // If there is a valid width
                    if (position.Width != 0)
                    {
                        // Write width
                        jsonWriter.WriteProperty("width", string.Format(CultureInfo.InvariantCulture, "{0}%", position.Width));
                    }

                    // If there is a valid height
                    if (position.Height != 0)
                    {
                        // Write height
                        jsonWriter.WriteProperty("height", (position.Height / 100) * chart.Size.Height);
                    }

                    // If there is a valid x
                    if (position.X != 0)
                    {
                        // Write x
                        jsonWriter.WriteProperty("x", (position.X / 100) * chart.Size.Width);
                    }

                    // If there is a valid y
                    if (position.Y != 0)
                    {
                        // Write y
                        jsonWriter.WriteProperty("y", (position.Y / 100) * chart.Size.Height);
                    }
                }
            }
        }

        /// <summary>
        /// Renders the chart.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="chartArea">The chart area.</param>
        /// <param name="series">The series.</param>
        private static void RenderChart(RenderingContext renderingContext, Chart chart, JsonTextWriter jsonWriter, ChartArea chartArea, IEnumerable<Series> series)
        {
            if (IsChartAreaRotated(chartArea))
            {
                jsonWriter.WriteProperty("flipXY", true);
            }

            jsonWriter.WriteProperty("animation", false);

            // Write store action reference
            if (ApplicationElement.CurrentRenderingEnvironment == RenderingEnvironment.ExtJS)
            {
                jsonWriter.WriteProperty("store", new ChartStoreResourceActionReference(chart, "store", GetFieldNames(chart, chart.Series)));
            }
            else
            {
                // Touch did not manage with proxy store
                jsonWriter.WritePropertyName("store");
                RenderStoreWithData(chart, jsonWriter);
            }

            // Fix for zooming
            jsonWriter.WritePropertyWithRawValue("componentCls", "\"vt-overFlowVisible\"");

            // Render legend
            RenderLegend(renderingContext, chart.Legend, jsonWriter);

            // Render series
            RenderSeries(renderingContext, chart, series, jsonWriter);

            // Render axes
            RenderAxes(chart, chartArea, GetFieldNames(chart, series), jsonWriter);
        }

        private static void RenderLegend(RenderingContext renderingContext, Legend legend, JsonTextWriter jsonWriter)
        {
            if (legend != null && legend.Enabled)
            {
                jsonWriter.WritePropertyName("legend");

                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WritePropertyName("docked");

                    switch (legend.Position)
                    {
                        case LegendPosition.Top:
                            jsonWriter.WriteValue("top");
                            break;
                        case LegendPosition.Left:
                            jsonWriter.WriteValue("left");
                            break;
                        case LegendPosition.Bottom:
                            jsonWriter.WriteValue("bottom");
                            break;
                        case LegendPosition.Right:
                        default:
                            jsonWriter.WriteValue("right");
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the field names.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="series">The series.</param>
        /// <returns></returns>
        internal static string[] GetFieldNames(Chart chart, IEnumerable<Series> series)
        {
            List<string> fieldNames = new List<string>();

            // If there is a valid series enumerable
            if (series != null)
            {
                // Get series list
                List<Series> seriesList = series.ToList();

                // Set x value field name
                fieldNames.Add(GetFieldName(null));

                // Loop all series
                for (int index = 0; index < seriesList.Count; index++)
                {
                    if (seriesList[index].Points.Any())
                    {
                        // Get field name
                        string strName = GetFieldName(seriesList[index]);

                        // Set field name
                        fieldNames.Add(strName);
                    }
                }
            }

            return fieldNames.ToArray();
        }

        /// <summary>
        /// Gets the name of the field.
        /// </summary>
        /// <param name="series">The series.</param>
        /// <returns></returns>
        internal static string GetFieldName(Series series)
        {
            if (series == null)
            {
                return "xValue";
            }

            string strName = series.ID;

            // If there is no valid name
            if (string.IsNullOrEmpty(strName))
            {
                Chart chart = series.ParentElement as Chart;

                if (chart != null)
                {
                    // Generate series name
                    strName = "Series" + chart.Series.IndexOf(series);
                }
            }

            return strName;
        }

        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="request">The http request.</param>
        /// <returns></returns>
        public override IVisualElementRenderer RenderResult(RenderingContext renderingContext, IVisualElement visualElement, HttpRequestBase request = null)
        {
            Chart chart = visualElement as Chart;

            if (chart != null)
            {
                return new ChartStoreElementResource(chart);
            }

            return null;
        }

        /// <summary>
        /// Renders the axes.
        /// </summary>
        /// <param name="chart">The visual element.</param>
        /// <param name="chartArea">The chart area.</param>
        /// <param name="fieldNames">The field names.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private static void RenderAxes(Chart chart, ChartArea chartArea, string[] fieldNames, JsonTextWriter jsonWriter)
        {
            // If there is a valid chart area
            if (chartArea != null)
            {
                // Write axes property
                jsonWriter.WritePropertyName("axes");

                // Open array scope
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    // Render x axis
                    RenderAxisContent(chart, chartArea.AxisX, jsonWriter, IsChartAreaRotated(chartArea) ? AxesPosition.Left : AxesPosition.Bottom, new string[] { fieldNames[0] });

                    // Render y axis
                    RenderAxisContent(chart, chartArea.AxisY, jsonWriter, IsChartAreaRotated(chartArea) ? AxesPosition.Bottom : AxesPosition.Left, fieldNames.Skip(1).ToArray());
                }
            }
        }

        /// <summary>
        /// Renders the content of the axis.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="axis">The axis.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="axesPosition">The axes position.</param>
        /// <param name="fieldNames">The field names.</param>
        private static void RenderAxisContent(Chart chart, Axis axis, JsonTextWriter jsonWriter, AxesPosition axesPosition, string[] fieldNames)
        {
            // If there is a valid axis
            if (axis != null)
            {
                // Start series object
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    // Render axis type
                    AxisType axisType = GetAxisType(chart, axis, axesPosition);
                    jsonWriter.WriteProperty("type", axisType.ToString().ToLowerInvariant());

                    // Render axis position
                    jsonWriter.WriteProperty("position", axesPosition.ToString().ToLowerInvariant());

                    // If there is a valid grid
                    if (axis.MinorGrid != null)
                    {
                        // If grid is enabled
                        if (axis.MinorGrid.Enabled)
                        {
                            // Write grid property
                            jsonWriter.WriteProperty("grid", true);
                        }
                    }

                    // If there is a label style
                    if (axis.LabelStyle != null)
                    {
                        // If label is disabled
                        if (!axis.LabelStyle.Enabled)
                        {
                            // Write label property
                            jsonWriter.WritePropertyName("label");

                            // Open label object scope
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                // Write hidden property
                                jsonWriter.WriteProperty("hidden", true);
                            }
                        }
                    }

                    // Loop fields
                    if (fieldNames != null && fieldNames.Any())
                    {
                        // Write fields property
                        jsonWriter.WritePropertyName("fields");

                        // Open array scope
                        using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                        {
                            // Loop field names
                            foreach (string fieldName in fieldNames)
                            {
                                // Write field name
                                jsonWriter.WriteValue(fieldName);
                            }
                        }
                    }

                    // If there is a valid title
                    if (axis.Title != null)
                    {
                        // If there no font
                        if (axis.TitleFont == null)
                        {
                            // Write title property
                            jsonWriter.WriteProperty("title", RenderingUtils.NormalizeText(axis.Title, "&"));
                        }
                        else
                        {
                            // Write title property
                            jsonWriter.WritePropertyName("title");

                            // Open object scope
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                // Write text property
                                jsonWriter.WriteProperty("text", axis.Title);

                                // Write font size property
                                jsonWriter.WriteProperty("fontSize", axis.TitleFont.SizeInPoints);

                                // If bold
                                if (axis.TitleFont.Bold)
                                {
                                    // Write font weight property
                                    jsonWriter.WriteProperty("fontWeight", "bold");
                                }
                            }
                        }
                    }

                    // If is numeric axis
                    if (axisType == AxisType.Numeric)
                    {
                        double minimum = axis.Minimum, maximum = axis.Maximum;

                        // If there are no minimum and maximum defined
                        if ((minimum == 0 && maximum == 0)
                            || (double.IsNaN(minimum) && double.IsNaN(maximum)))
                        {
                            // Set default
                            minimum = double.MaxValue;
                            maximum = double.MinValue;

                            // Get chart area series
                            IEnumerable<Series> seriesList = GetChartAreaSeries(chart, axis.ParentElement as ChartArea);

                            // If is x value
                            if (fieldNames.Contains("xValue"))
                            {
                                bool blnHasBarSeries = false;

                                // Loop all series
                                foreach (Series series in seriesList)
                                {
                                    blnHasBarSeries = blnHasBarSeries || series.ChartType == SeriesChartType.Bar;

                                    foreach (DataPoint point in series.Points)
                                    {
                                        try
                                        {
                                            double xValue = Convert.ToDouble(point.XValue);

                                            if (minimum > xValue)
                                                minimum = xValue;

                                            if (maximum < xValue)
                                                maximum = xValue;
                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                            else
                            {
                                bool blnHasBarSeries = false;

                                // Loop all series
                                foreach (Series series in seriesList)
                                {
                                    blnHasBarSeries = blnHasBarSeries || series.ChartType == SeriesChartType.Bar;

                                    // If there are valid points
                                    if (series.Points.Any())
                                    {
                                        // Get minimum
                                        minimum = blnHasBarSeries ? double.NaN : Math.Min(minimum, series.Points.Min(point => point.YValues.FirstOrDefault()));

                                        // Get maximum
                                        maximum = Math.Max(maximum, series.Points.Max(point => point.YValues.FirstOrDefault()));
                                    }
                                }
                            }
                        }

                        // If has maximum value
                        if (!double.IsNaN(maximum) && maximum != double.MinValue)
                        {
                            // Write maximum property
                            jsonWriter.WriteProperty("maximum", maximum);
                        }

                        // If has minimum value
                        if (!double.IsNaN(minimum) && minimum != double.MaxValue)
                        {
                            // Write minimum property
                            jsonWriter.WriteProperty("minimum", minimum);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the type of the axis.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="axis">The axis.</param>
        /// <param name="axesPosition">The axes position.</param>
        /// <returns></returns>
        private static AxisType GetAxisType(Chart chart, Axis axis, AxesPosition axesPosition)
        {
            if (axis != null)
            {
                if (axis.AxisType != AxisType.Default)
                {
                    return axis.AxisType;
                }

                if (chart != null)
                {
                    if ((!IsAxisRotated(axis) && axesPosition == AxesPosition.Bottom) || (IsAxisRotated(axis) && axesPosition == AxesPosition.Left))
                    {
                        IEnumerable<Series> seriesList = GetChartAreaSeries(chart, axis.ParentElement as ChartArea);

                        if (seriesList.Any(series => series.ChartType == SeriesChartType.Bar))
                        {
                            return AxisType.Category;
                        }
                    }
                }
            }

            return AxisType.Numeric;
        }

        /// <summary>
        /// Renders the series.
        /// </summary>
        /// <param name="chart">The visual element.</param>
        /// <param name="series">The series.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private static void RenderSeries(RenderingContext renderingContext, Chart chart, IEnumerable<Series> series, JsonTextWriter jsonWriter)
        {
            // If there is valid series
            if (series != null)
            {
                // Write series property
                jsonWriter.WritePropertyName("series");

                // Open series array scope
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    // Loop all series
                    foreach (Series seriesItem in series)
                    {
                        // If there is a valid series
                        if (seriesItem != null)
                        {
                            // Start series object
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                // Render a series content
                                RenderSeriesContent(renderingContext, chart, seriesItem, jsonWriter);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Renders the content of the series.
        /// </summary>
        /// <param name="chart">The visual element.</param>
        /// <param name="series">The series.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private static void RenderSeriesContent(RenderingContext renderingContext, Chart chart, Series series, JsonTextWriter jsonWriter)
        {
            // If there is a valid series
            if (series != null)
            {
                ExtJSChartType extJSChartType = GetExtJSChartType(series.ChartType);
                // Write series type
                jsonWriter.WriteProperty("type", extJSChartType.ToString().ToLowerInvariant());

                // Write x field name
                jsonWriter.WriteProperty("xField", GetFieldName(null));

                // Write y field name
                jsonWriter.WriteProperty("yField", GetFieldName(series));

                // Write series legend title
                if (!string.IsNullOrEmpty(series.Title))
                {
                    jsonWriter.WriteProperty("title", series.Title);
                }

                // If has color
                if (series.Color != default(Color) || series.BorderColor != default(Color))
                {
                    // Write style
                    jsonWriter.WritePropertyName("style");

                    // Open object scope
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        // Write stroke
                        jsonWriter.WriteProperty("stroke", string.Format(CultureInfo.InvariantCulture, "#{0:X2}{1:X2}{2:X2}", series.BorderColor.R, series.BorderColor.G, series.BorderColor.B));

                        // If is bar type
                        if (extJSChartType == ExtJSChartType.Bar)
                        {
                            // Write fill
                            jsonWriter.WriteProperty("fill", string.Format(CultureInfo.InvariantCulture, "#{0:X2}{1:X2}{2:X2}", series.Color.R, series.Color.G, series.Color.B));
                        }
                    }
                }

                // Render series label
                if (series.LabelStyle.Enabled)
                {
                    VisualElementExtJSRenderer labelRenderer = VisualTreeManager.GetRenderer(renderingContext, series.LabelStyle) as VisualElementExtJSRenderer;

                    if (labelRenderer != null)
                    {
                        labelRenderer.Render(renderingContext, series.LabelStyle, jsonWriter);
                    }
                }
            }
        }

        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public override IVisualElementRenderer RenderResourceResult(RenderingContext renderingContext, IVisualElement visualElement, string resourceId)
        {
            // If is a store resource id
            if (resourceId == "store")
            {
                // Return the tree store resource action result
                return new ChartStoreElementResource(visualElement as Chart);
            }

            return base.RenderResourceResult(renderingContext, visualElement, resourceId);
        }

        private enum ExtJSChartType
        {
            /// <summary>
            /// The bar
            /// </summary>
            Bar,
            /// <summary>
            /// The line
            /// </summary>
            Line,
            /// <summary>
            /// The scatter
            /// </summary>
            Scatter
        }

        /// <summary>
        /// Gets the ext js chart type.
        /// </summary>
        /// <param name="seriesChartType">The series chart type.</param>
        /// <returns></returns>
        private static ExtJSChartType GetExtJSChartType(SeriesChartType seriesChartType)
        {
            // Choose chart type
            switch (seriesChartType)
            {
                case SeriesChartType.Bar:
                case SeriesChartType.StackedBar:
                case SeriesChartType.StackedBar100:
                case SeriesChartType.Column:
                case SeriesChartType.StackedColumn:
                case SeriesChartType.StackedColumn100:
                case SeriesChartType.RangeBar:
                case SeriesChartType.RangeColumn:
                    return ExtJSChartType.Bar;

                case SeriesChartType.Point:
                    return ExtJSChartType.Scatter;

                case SeriesChartType.Line:
                case SeriesChartType.FastLine:
                case SeriesChartType.FastPoint:
                case SeriesChartType.Bubble:
                case SeriesChartType.Spline:
                case SeriesChartType.StepLine:
                case SeriesChartType.Area:
                case SeriesChartType.SplineArea:
                case SeriesChartType.StackedArea:
                case SeriesChartType.StackedArea100:
                case SeriesChartType.Pie:
                case SeriesChartType.Doughnut:
                case SeriesChartType.Stock:
                case SeriesChartType.Candlestick:
                case SeriesChartType.Range:
                case SeriesChartType.SplineRange:
                case SeriesChartType.Radar:
                case SeriesChartType.Polar:
                case SeriesChartType.ErrorBar:
                case SeriesChartType.Boxplot:
                case SeriesChartType.Renko:
                case SeriesChartType.ThreeLineBreak:
                case SeriesChartType.Kagi:
                case SeriesChartType.PointAndFigure:
                case SeriesChartType.Funnel:
                case SeriesChartType.Pyramid:
                default:
                    return ExtJSChartType.Line;
            }
        }

        /// <summary>
        /// Shoulds the render chart container.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <returns></returns>
        private static bool ShouldRenderChartContainer(Chart chart)
        {
            // If there are valid chart areas
            if (chart.ChartAreas != null)
            {
                // If there are multiple chart areas
                if (chart.ChartAreas.Count > 1)
                {
                    return true;
                }

                // Get chart area
                ChartArea chartArea = chart.ChartAreas.FirstOrDefault();

                // If there is a valid chart area
                if (chartArea != null)
                {
                    // Return if position is not empty
                    return !chartArea.Position.IsEmpty();
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified chart is absolute layout chart.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <returns></returns>
        private static bool IsAbsoluteLayoutChart(Chart chart)
        {
            return ShouldRenderChartContainer(chart) && chart.ChartAreas.Any(chartArea => !chartArea.Position.IsEmpty());
        }

        /// <summary>
        /// Gets the chart area series.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="chartArea">The chart area.</param>
        /// <returns></returns>
        private static IEnumerable<Series> GetChartAreaSeries(Chart chart, ChartArea chartArea)
        {
            if (chart != null && chartArea != null)
            {
                return chart.Series.Where(seriesItem => string.Equals(seriesItem.ChartAreaID, chartArea.ID));
            }

            return new Series[] { };
        }

        /// <summary>
        /// Reloads the specified rendering context.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="series">The series.</param>
        /// <param name="builder">The builder.</param>
        internal void RenderReload(RenderingContext renderingContext, ChartItemElement chartElement, JavaScriptBuilder builder, string componentVariable)
        {
            if (chartElement != null)
            {
                Chart chart = chartElement.ParentElement as Chart;

                if (chart != null)
                {
                    // Close if statement of series component. => if(cmp)
                    RenderSetChartAsComponent(renderingContext, chart, builder, componentVariable);

                    RenderReload(renderingContext, chart, builder, componentVariable);
                }
            }
        }

        /// <summary>
        /// Refreshes the specified chart.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="builder">The builder.</param>
        private void RenderReload(RenderingContext renderingContext, Chart chart, JavaScriptBuilder builder, string componentVariable)
        {
            if (chart != null)
            {
                /*
                    var container = chart.findParentByType('container');

                    if (container) {
                        var chartIndex = container.items.indexOf(cmp);
                        if(chartIndex >= 0)
                        {                                    
                        container.remove(cmp);
                        chart = Ext.create('Ext.chart.Chart', {0});

                        container.insert(chartIndex, chart);
                        container.doLayout();
                        }
                    }                  
                 */

                StringBuilder sb = new StringBuilder();
                using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(sb))
                {
                    this.Render(renderingContext, chart, jsonWriter);

                    jsonWriter.Flush();
                }

                builder.AppendVariableWithInvocationInitializer("container", "cmp.findParentByType", "container");
                builder.AppendStartIf("container");
                builder.AppendVariableWithInvocationInitializer("chartIndex", "container.items.indexOf", new JavaScriptCode("cmp"));
                builder.AppendStartIf("chartIndex >= 0");
                builder.AppendInvocationStatement("container.remove", new JavaScriptCode("cmp"));
                builder.AppendAssignmentStatement("cmp", new JavaScriptCode(string.Format("Ext.create('Ext.chart.Chart', {0})", sb)));
                builder.AppendInvocationStatement("container.insert", new JavaScriptCode("chartIndex"), new JavaScriptCode("cmp"));
                builder.AppendInvocationStatement("container.doLayout");
                builder.AppendEndIf();
                builder.AppendEndIf();
            }
        }

        /// <summary>
        /// Sets the CMP to chart component.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="builder">The builder.</param>
        internal static void RenderSetChartAsComponent(RenderingContext renderingContext, Chart chart, JavaScriptBuilder builder, string componentVariable)
        {
            /*                  
                             * This code already exists in the builder
                 
            var cmp = VT_GetCmp("{chart element id}");if(cmp) {
                  
                             * And this is what this function adds to the builder :
                             * 
                             * If the property is not the charts but a series / axis property (like Series.ChartType)
                             * The previous if is closed because cmp is null. 
                             * The series component is an inner component of the chart and cannot be found this way.                 
            }
            cmp = VT_GetCmp('{chart id}');
            if (cmp) {
             */

            if (chart != null)
            {
                builder.AppendEndIf();
                builder.AppendAssignmentStatement(componentVariable, new JavaScriptCode(string.Format("VT_GetCmp('{0}')", VisualElement.GetElementId(chart))));
                builder.AppendStartIf(componentVariable);
            }
        }

        /// <summary>
        /// Updates the store.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="chartElement">The chart element.</param>
        /// <param name="builder">The builder.</param>
        internal static void RenderUpdateStore(RenderingContext renderingContext, ChartItemElement chartElement, JavaScriptBuilder builder, string componentVariable)
        {
            if (chartElement != null)
            {
                Chart chart = chartElement.ParentElement as Chart;

                if (chart != null)
                {
                    // Close if statement of chart element component. => if(cmp)
                    RenderSetChartAsComponent(renderingContext, chart, builder, componentVariable);

                    // Add update store.
                    RenderUpdateStore(renderingContext, chart, builder, componentVariable);
                }
            }
        }

        /// <summary>
        /// Updates the store.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="builder">The builder.</param>
        private static void RenderUpdateStore(RenderingContext renderingContext, Chart chart, JavaScriptBuilder builder, string componentVariable)
        {
            if (chart != null)
            {
                if (ApplicationElement.CurrentRenderingEnvironment == RenderingEnvironment.ExtJS)
                {
                    builder.AppendInvocationStatement(string.Concat(componentVariable, ".store.reload"));
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(sb))
                    {
                        RenderStoreWithData(chart, jsonWriter);
                        jsonWriter.Flush();
                    }
                    builder.AppendInvocationStatement(string.Concat(componentVariable, ".setStore"), new JavaScriptCode(sb.ToString()));
                }
                //builder.AppendInvocationStatement(string.Concat(componentVariable, ".redraw"));
            }
        }

        /// <summary>
        /// Renders the store with data.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private static void RenderStoreWithData(Chart chart, JsonTextWriter jsonWriter)
        {
            if (chart != null)
            {
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WritePropertyName("fields");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                    {
                        string[] fields = GetFieldNames(chart, chart.Series);

                        foreach (string field in fields)
                        {
                            jsonWriter.WriteValue(field);
                        }
                    }

                    ChartStoreElementResource.RenderStore(chart, jsonWriter);
                }
            }
        }

        /// <summary>
        /// Determines whether the specified chart area is chart area rotated.
        /// </summary>
        /// <param name="chartArea">The chart area.</param>
        /// <returns></returns>
        internal static bool IsChartAreaRotated(ChartArea chartArea)
        {
            if (chartArea != null)
            {
                return chartArea.Orientation == Orientation.Horizontal;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified axis is axis rotated.
        /// </summary>
        /// <param name="axis">The axis.</param>
        /// <returns></returns>
        internal static bool IsAxisRotated(Axis axis)
        {
            if (axis != null)
            {
                return IsChartAreaRotated(axis.ParentElement as ChartArea);
            }

            return false;
        }
    }
}
