﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    public class ChartStoreResourceActionReference : StoreElementResourceReference
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartStoreResourceActionReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        public ChartStoreResourceActionReference(Chart chart, string resourceId, string[] fields)
            : base(chart, resourceId)
        {
            // Auto load tree
            AutoLoad = true;
            ProxyReaderRoot = "data";
            Fields = fields;
        }
    }
}
