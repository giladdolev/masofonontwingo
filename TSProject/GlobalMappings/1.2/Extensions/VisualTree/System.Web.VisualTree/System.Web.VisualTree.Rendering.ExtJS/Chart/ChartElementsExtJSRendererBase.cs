﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    public class ChartElementsExtJSRendererBase : DefaultComponentExtJSRenderer
    {
        /// <summary>
        /// Reloads the chart.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="builder">The builder.</param>
        protected void RenderReloadChart(RenderingContext renderingContext, ChartItemElement chartElement, JavaScriptBuilder builder, string componentVariable)
        {
            if (chartElement != null)
            {
                Chart chart = chartElement.ParentElement as Chart;

                if (chart != null)
                {
                    ChartElementExtJSRenderer chartRenderer = VisualTreeManager.GetRenderer(renderingContext, chart) as ChartElementExtJSRenderer;

                    if (chartRenderer != null)
                    {
                        chartRenderer.RenderReload(renderingContext, chartElement, builder, componentVariable);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the store.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="series">The series.</param>
        /// <param name="builder">The builder.</param>
        internal void RenderUpdateStore(RenderingContext renderingContext, ChartItemElement chartElement, JavaScriptBuilder builder, string componentVariable)
        {
            if (chartElement != null)
            {
                Chart chart = chartElement.ParentElement as Chart;

                if (chart != null)
                {
                    ChartElementExtJSRenderer.RenderUpdateStore(renderingContext, chartElement, builder, componentVariable);
                }
            }
        }
    }
}
