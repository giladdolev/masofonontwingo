﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(SeriesLabelStyle), "ChartElementsExtJSRendererBase", "", "")]
    public class ChartSeriesLabelStyleElementExtJSRenderer : ChartElementsExtJSRendererBase
    {
        /// <summary>
        /// Renders the specified object visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void Render(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            SeriesLabelStyle labelStyle = visualElement as SeriesLabelStyle;

            Series series = labelStyle.ParentElement as Series;
            if (series != null)
            {
                jsonWriter.WritePropertyName("label");

                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    jsonWriter.WriteProperty("field", series.ID);

                    jsonWriter.WriteProperty("orientation", "horizontal");

                    RenderDisplay(renderingContext, labelStyle, jsonWriter);
                }
            }
        }

        /// <summary>
        /// Renders the display.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="labelStyle">The label style.</param>
        /// <param name="jsonWriter">The json writer.</param>
        private void RenderDisplay(RenderingContext renderingContext, SeriesLabelStyle labelStyle, JsonTextWriter jsonWriter)
        {
            if (labelStyle != null)
            {
                string displayValue = "none";
                if (labelStyle.Enabled)
                {
                    displayValue = labelStyle.Position == SeriesLabelPosition.Top ? "outside" : "insideEnd";
                }
                jsonWriter.WriteProperty("display", displayValue);
            }
        }

        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            switch (property)
            {
                case "Enabled":
                case "Position":
                    UpdateDisplayPropertyChanged(renderingContext, visualElement as SeriesLabelStyle, builder);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }

        /// <summary>
        /// Updates the display property changed.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="labelStyle">The label style.</param>
        /// <param name="builder">The builder.</param>
        private void UpdateDisplayPropertyChanged(RenderingContext renderingContext, SeriesLabelStyle labelStyle, JavaScriptBuilder builder)
        {
            if (labelStyle != null)
            {
                Series series = labelStyle.ParentElement as Series;

                if (series != null)
                {
                    Chart chart = series.ParentElement as Chart;

                    if (chart != null)
                    {
                        int seriesIndex = chart.Series.IndexOf(series);

                        if (seriesIndex >= 0)
                        {
                            // Render update label display js code.
                            builder.AppendEndIf();
                            builder.AppendAssignmentStatement("cmp", new JavaScriptCode(string.Format("VT_GetCmp('{0}')", VisualElement.GetElementId(chart))));
                            builder.AppendStartIf("cmp");

                            StringBuilder sb = new StringBuilder();

                            using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(sb))
                            {
                                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                                {
                                    this.RenderDisplay(renderingContext, labelStyle, jsonWriter);
                                }

                                jsonWriter.Flush();
                            }

                            builder.AppendInvocationStatement(string.Format("cmp.series[{0}].setLabel", seriesIndex), new JavaScriptCode(sb.ToString()));
                            builder.AppendInvocationStatement("cmp.updateSeries");
                        }
                    }

                }
            }
        }
    }
}
