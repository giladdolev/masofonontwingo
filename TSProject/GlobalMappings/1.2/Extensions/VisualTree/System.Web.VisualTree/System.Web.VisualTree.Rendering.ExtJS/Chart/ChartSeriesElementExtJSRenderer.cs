﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(Series), "ChartElementsExtJSRendererBase", "", "")]
    public class ChartSeriesElementExtJSRenderer : ChartElementsExtJSRendererBase
    {
        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            switch (property)
            {
                case "ChartType":
                    RenderReloadChart(renderingContext, visualElement as Series, builder, componentVariable);
                    break;
                case "DataPointCollection":
                    RenderUpdateStore(renderingContext, visualElement as Series, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }
    }
}
