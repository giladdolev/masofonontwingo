﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    public class ChartStoreElementResource : StoreElementResource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartStoreElementResource" /> class.
        /// </summary>
        /// <param name="chart">The chart.</param>
        public ChartStoreElementResource(Chart chart)
            : base(chart)
        {

        }

        /// <summary>
        /// Executes the json result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderJsonResource(RenderingContext renderingContext, JsonTextWriter jsonWriter)
        {
            Chart chart = Element as Chart;

            if (chart != null)
            {
                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                {
                    RenderStore(chart, jsonWriter);
                }
            }
        }

        /// <summary>
        /// Renders the store.
        /// </summary>
        /// <param name="series">The series.</param>
        /// <param name="jsonWriter">The json writer.</param>
        internal static void RenderStore(Chart chart, JsonTextWriter jsonWriter)
        {
            string[] fieldNames = new string[] { "xValue" };

            // If there is a valid series object
            if (chart != null)
            {
                List<Series> seriesList = chart.Series.ToList();
                Dictionary<object, double[][]> dataPoints = new Dictionary<object, double[][]>();
                fieldNames = ChartElementExtJSRenderer.GetFieldNames(chart, seriesList);
                bool hasXLabel = true;

                // Loop all series
                for (int index = 0; index < seriesList.Count; index++)
                {
                    // Get series item
                    Series seriesItem = seriesList[index];

                    // If there is a valid series item
                    if (seriesItem != null)
                    {
                        // If first series
                        if (index == 0)
                        {
                            // Has x value
                            hasXLabel = seriesItem.Points.Any(dataPoint => !string.IsNullOrEmpty(dataPoint.AxisLabel));
                        }

                        object xValue = null;
                        double xDoubleValue = 0;

                        // Get the series data points
                        foreach (var dataPoint in seriesItem.Points)
                        {
                            xValue = null;
                            double[][] yValues;

                            // Get x value
                            if (seriesItem.XValueType == ChartValueType.String
                                || (seriesItem.XValueType == ChartValueType.Auto && hasXLabel))
                                xValue = dataPoint.AxisLabel ?? dataPoint.XValue.ToString();

                            if (xValue == null)
                                xValue = xDoubleValue = xDoubleValue + 1;

                            // Try get y values
                            if (!dataPoints.TryGetValue(xValue, out yValues))
                            {
                                dataPoints[xValue] = yValues = new double[seriesList.Count][];
                            }

                            // Set series y value
                            yValues[index] = dataPoint.YValues;
                        }
                    }
                }

                // If there is data
                if (dataPoints.Any())
                {
                    // Write the chart store

                    //// Write fields property
                    //jsonWriter.WritePropertyName("fields");

                    //// Write fields
                    //using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                    //{
                    //    // Loop field names
                    //    foreach (string fieldName in fieldNames)
                    //    {
                    //        // Write field name
                    //        jsonWriter.WriteValue(fieldName);
                    //    }
                    //}                    

                    // Write data property
                    jsonWriter.WritePropertyName("data");

                    // Write data array
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                    {
                        // Order the data points by the key
                        IEnumerable<KeyValuePair<object, double[][]>> orderedPoints = dataPoints.OrderBy(dataPoint => dataPoint.Key);

                        // If there are more than 5,000 items
                        if (orderedPoints.Count() > 5000)
                        {
                            orderedPoints = orderedPoints.Where((x, i) => i % 10 == 0);
                        }

                        // Loop data points
                        foreach (KeyValuePair<object, double[][]> dataItem in orderedPoints)
                        {
                            // Write data object
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                // write x value
                                jsonWriter.WritePropertyName(fieldNames[0]);
                                jsonWriter.WriteValue(dataItem.Key);

                                // Get y values
                                double[][] yValues = dataItem.Value;

                                // Loop all y values
                                for (int index = 0; index < yValues.Length; index++)
                                {
                                    // Get y value
                                    double[] yValue = yValues[index];

                                    // If there is a valid y value
                                    if (yValue != null)
                                    {
                                        // Write y value
                                        jsonWriter.WriteProperty(fieldNames[index + 1], yValue[0]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the specified value has value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private static bool HasValue(object value)
        {
            if (value == null)
                return false;

            if (value is string && string.IsNullOrEmpty(value as string))
                return false;

            if (value is short && (short)value == default(short))
                return false;

            if (value is ushort && (ushort)value == default(ushort))
                return false;

            if (value is int && (int)value == default(int))
                return false;

            if (value is uint && (uint)value == default(uint))
                return false;

            if (value is long && (long)value == default(long))
                return false;

            if (value is ulong && (ulong)value == default(ulong))
                return false;

            if (value is double && (double)value == default(double))
                return false;

            if (value is decimal && (decimal)value == default(decimal))
                return false;

            if (value is float && (float)value == default(float))
                return false;

            if (value is byte && (byte)value == default(byte))
                return false;

            if (value is sbyte && (sbyte)value == default(sbyte))
                return false;

            if (value is Guid && (Guid)value == default(Guid))
                return false;

            return true;
        }
    }
}
