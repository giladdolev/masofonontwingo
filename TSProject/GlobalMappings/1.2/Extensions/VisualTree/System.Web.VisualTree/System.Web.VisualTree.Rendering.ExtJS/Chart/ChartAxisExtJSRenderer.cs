﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(Axis), "ChartElementsExtJSRendererBase", "", "")]
    class ChartAxisExtJSRenderer : ChartElementsExtJSRendererBase
    {
        public override void Render(RenderingContext renderingContext, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            base.Render(renderingContext, visualElement, jsonWriter);
        }

        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            switch (property)
            {
                case "Maximum":
                    RenderMaximumChanged(renderingContext, visualElement as Axis, builder, componentVariable);
                    break;
                case "Minimum":
                    RenderMinimumChanged(renderingContext, visualElement as Axis, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;

            }
        }

        /// <summary>
        /// Renders the maximum changed.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="axis">The axis.</param>
        /// <param name="builder">The builder.</param>
        private void RenderMaximumChanged(RenderingContext renderingContext, Axis axis, JavaScriptBuilder builder, string componentVariable)
        {
            if (axis != null)
            {
                RenderGetAxisAsComponent(renderingContext, axis, builder, componentVariable);

                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setMaximum"), axis.Maximum);
            }
        }

        /// <summary>
        /// Renders the minimum changed.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="axis">The axis.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        private void RenderMinimumChanged(RenderingContext renderingContext, Axis axis, JavaScriptBuilder builder, string componentVariable)
        {
            if (axis != null)
            {
                RenderGetAxisAsComponent(renderingContext, axis, builder, componentVariable);

                builder.AppendInvocationStatement(string.Concat(componentVariable, ".setMinimum"), axis.Minimum);
            }
        }

        /// <summary>
        /// Renders the get axis as component.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="axis">The axis.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        private void RenderGetAxisAsComponent(RenderingContext renderingContext, Axis axis, JavaScriptBuilder builder, string componentVariable)
        {
            if (axis != null)
            {                
                Chart chart = VisualElement.GetParentElement<Chart>(axis);                
                
                if (chart != null)
                {
                    int axisIndex = axis.Index;

                    if (axisIndex >= 0)
                    {
                        ChartElementExtJSRenderer.RenderSetChartAsComponent(renderingContext, chart, builder, componentVariable);


                        builder.AppendEndIf();
                        builder.AppendAssignmentStatement(componentVariable, new JavaScriptCode(string.Format("{0}.getAxis({1})", componentVariable, axisIndex)));
                        builder.AppendStartIf(componentVariable);
                    }
                }
            }
        }       
    }
}
