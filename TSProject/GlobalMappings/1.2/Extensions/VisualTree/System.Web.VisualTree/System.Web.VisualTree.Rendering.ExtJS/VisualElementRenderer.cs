﻿using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Web.VisualTree.Elements;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Rendering
{
    /// <summary>
    /// Provides support for rendering visual elements
    /// </summary>
    public abstract class VisualElementRenderer : IVisualElementRenderer
	{

        /// <summary>
        /// Renders the property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="strName">The property name.</param>
        /// <param name="objUnit">The property value.</param>
        protected static void RenderProperty(RenderingContext renderingContext, JsonWriter jsonWriter, string strName, Unit objUnit)
		{
            RenderProperty(renderingContext, jsonWriter, strName, UnitTypeConverter.GetPixels(objUnit));
		}

        /// <summary>
        /// Renders the property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="strName">The property name.</param>
        /// <param name="intValue">The property value.</param>
        protected static void RenderProperty(RenderingContext renderingContext, JsonWriter jsonWriter, string strName, int intValue)
		{
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

			jsonWriter.WritePropertyName(strName);
			jsonWriter.WriteValue(intValue);
		}

        /// <summary>
        /// Renders the property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="strName">The property name.</param>
        /// <param name="strValue">The property value.</param>
        protected static void RenderProperty(RenderingContext renderingContext, JsonWriter jsonWriter, string strName, string strValue)
		{
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

			jsonWriter.WritePropertyName(strName);
			jsonWriter.WriteValue(strValue);
		}


        /// <summary>
        /// Renders the property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="strName">Name of the string.</param>
        /// <param name="blnValue">if set to <c>true</c> [BLN value].</param>
        protected static void RenderProperty(RenderingContext renderingContext, JsonWriter jsonWriter, string strName, bool blnValue)
		{
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

			jsonWriter.WritePropertyName(strName);
			jsonWriter.WriteValue(blnValue);
		}

        /// <summary>
        /// Renders the property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="strName">Name of the string.</param>
        /// <param name="dblValue">The double value.</param>
        protected static void RenderProperty(RenderingContext renderingContext, JsonWriter jsonWriter, string strName, double dblValue)
        {
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            jsonWriter.WritePropertyName(strName);
            jsonWriter.WriteValue(dblValue);
        }


        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="applicationElement">The application element.</param>
        /// <param name="context">The context.</param>
        internal virtual void RenderResult(RenderingContext renderingContext, ApplicationElement applicationElement, HttpContext context)
        {

        }

        /// <summary>
        /// Gets the action arguments from js.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="strActionName">Name of the string action.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected internal virtual EventArgs GetEventArgs(RenderingContext renderingContext, VisualElement visualElement, string strActionName, JObject eventObject)
		{
			return new EventArgs();
		}

        /// <summary>
        /// Provides support for generating json resource result
        /// </summary>
		protected class JsonResourceResultRenderer : VisualElementResource
		{

            /// <summary>
            /// The visual element renderer
            /// </summary>
			private VisualElementRenderer _visualElementRenderer;

            /// <summary>
            /// The resource identifier
            /// </summary>
			private readonly string _resourceId;

            /// <summary>
            /// Initializes a new instance of the <see cref="JsonResourceResultRenderer" /> class.
            /// </summary>
            /// <param name="renderingContext">The rendering context.</param>
            /// <param name="visualElement">The visual element.</param>
            /// <param name="visualElementRenderer">The visual element renderer.</param>
            /// <param name="resourceId">The resource identifier.</param>
            public JsonResourceResultRenderer(RenderingContext renderingContext, IVisualElement visualElement, IVisualElementRenderer visualElementRenderer, string resourceId)
                : base(visualElement)
			{
				
				_visualElementRenderer = visualElementRenderer as VisualElementRenderer;
				_resourceId = resourceId;
			}

            /// <summary>
            /// Executes the json result.
            /// </summary>
            /// <param name="renderingContext">The rendering context.</param>
            /// <param name="jsonWriter">The json writer.</param>
			protected override void RenderJsonResource(RenderingContext renderingContext, JsonTextWriter jsonWriter)
			{
				_visualElementRenderer.RenderResourceResult(renderingContext, Element, _resourceId, jsonWriter);
			}

		}

        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public virtual IVisualElementRenderer RenderResourceResult(RenderingContext renderingContext, IVisualElement visualElement, string resourceId)
		{
            return new JsonResourceResultRenderer(renderingContext, visualElement, this, resourceId);
		}

        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected internal virtual void RenderResourceResult(RenderingContext renderingContext, VisualElement visualElement, string resourceId, JsonTextWriter jsonWriter)
		{
			jsonWriter.WriteEmptyObject();
		}

        /// <summary>
        /// Gets the element resource URI.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
		public static string GetElementResourceUri(VisualElement visualElement, string resourceId)
		{
            return string.Concat("/VisualTree/ElementResource?appId=", HttpUtility.UrlEncode(VisualElement.GetApplicationId(visualElement)), "&elementId=", HttpUtility.UrlEncode(VisualElement.GetElementId(visualElement)), "&resourceId=", HttpUtility.UrlEncode(resourceId));
		}


        /// <summary>
        /// Renders the change.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="properties">The changed properties.</param>
        /// <param name="builder">The builder.</param>
        protected internal virtual void RenderChange(RenderingContext renderingContext, VisualElement visualElement, HashSet<string> properties, JavaScriptBuilder builder)
		{

		}

        /// <summary>
        /// Revises the renderer.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public virtual IVisualElementRenderer ReviseRenderer(RenderingContext renderingContext, IVisualElement visualElement)
        {
            return this;
        }

        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="renderTo">The render to.</param>
        /// <returns></returns>
        public virtual string Render(RenderingContext renderingContext, IVisualElement visualElement, string renderTo)
        {
            return null;
        }

        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="request">The http request.</param>
        /// <returns></returns>
        public virtual IVisualElementRenderer RenderResult(RenderingContext renderingContext, IVisualElement visualElement, HttpRequestBase request = null)
        {
            return null;
        }

        /// <summary>
        /// Enables processing of the result of an action method by a custom type that inherits from the <see cref="T:System.Web.Mvc.ActionResult" /> class.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="httpContext">The HTTP context in which the result is executed.</param>
        public virtual void ExecuteResult(RenderingContext renderingContext, HttpContextBase httpContext)
        {
        }

        /// <summary>
        /// Gets the method callback data.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        internal object GetMethodCallbackData(RenderingContext renderingContext, string methodName, JObject eventObject)
        {
            return this.ExtractMethodCallbackData(renderingContext, methodName, eventObject);
        }



        /// <summary>
        /// Extract the method callback data.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractMethodCallbackData(RenderingContext renderingContext, string methodName, JObject eventObject)
        {
            return null;
        }

        /// <summary>
        /// Validate if the property is empty string.
        /// </summary>
        /// <param name="propertyValue">The property value.</param>
        /// <returns></returns>
        protected virtual bool ValidateProperty(object propertyValue)
        {

            return propertyValue != null;
        }

        /// <summary>
        /// Validate if the property is empty string.
        /// </summary>
        /// <param name="propertyValue">The property value.</param>
        /// <returns></returns>
        protected virtual bool ValidateProperty(string propertyValue)
        {
            return !string.IsNullOrEmpty(propertyValue);
        }

        /// <summary>
        /// Validate if the property is empty array.
        /// </summary>
        /// <param name="propertyValue">The property value.</param>
        /// <returns></returns>
        protected virtual bool ValidateProperty(Array propertyValue)
        {
            return propertyValue.Length != 0;
        }

        /// <summary>
        /// Validate if the property is empty list.
        /// </summary>
        /// <param name="propertyValue">The property value.</param>
        /// <returns></returns>
        protected virtual bool ValidateProperty(ICollection propertyValue)
        {
            return propertyValue.Count == 0;
        }
    }
}
