﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    internal class FormatHelper
    {
        /// <summary>
        /// Formats the resource URL.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="resource">The resource.</param>
        /// <returns></returns>
        internal static string FormatResourceUrl(RenderingContext context, ResourceReference resource)
        {
            string url = FormatResource(context, resource);
            if (!String.IsNullOrEmpty(url))
            {
                url = String.Format("url('{0}')", url);
            }
            return url;
        }
        /// <summary>
        /// Formats the resource.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="resource">The resource.</param>
        /// <returns></returns>
        internal static string FormatResource(RenderingContext context, ResourceReference resource)
        {
            // The result
            string result = null;

            // If there is a valid result
            if (resource != null)
            {
                // Get source from result
                result = Convert.ToString(resource.Source);

                // If is designer type and there is no valid result
                if (context != null && context.IsDesignTime && !String.IsNullOrEmpty(result))
                {
                    // Get path
                    string path = String.Format("{0}{1}", context.RootSite, result.Replace("/", "\\"));

                    // If path exists
                    if (File.Exists(path))
                    {
                        // Return result as base 64
                        result = String.Format("data:image/png;base64,{0}", Convert.ToBase64String(File.ReadAllBytes(path)));
                    }
                }
            }

            // Return result
            return result;
        }

        /// <summary>
        /// Format the indexes of the checked items. 
        /// </summary>
        /// <param name="value">Selected items.</param>
        /// <returns>Selected item as array of int.</returns>
        internal static int[] FormatItemCheckedArgs(object value)
        {
            if (value == null)
            {
                return null;
            }
            int[] selectedItemsArray;
            //If the value represent one selected item.
            if (value as Newtonsoft.Json.Linq.JValue != null)
            {
                selectedItemsArray = new int[1];
                string str = value.ToString();
                int res = -1;
                if (int.TryParse(str, out res))
                {
                    selectedItemsArray[0] = res;
                }
                return selectedItemsArray;
            }
            // Otherwise, if the value represent more than one selected items
            else if (value as Newtonsoft.Json.Linq.JArray != null)
            {
                Newtonsoft.Json.Linq.JArray tempValue = value as Newtonsoft.Json.Linq.JArray;
                selectedItemsArray = new int[tempValue.Count];
                // Convert to int array 
                selectedItemsArray = JsonConvert.DeserializeObject<int[]>(Convert.ToString(value));
                return selectedItemsArray;
            }
            return null;
        }

        /// <summary>
        /// Format the index of the last checked/unchecked item. 
        /// </summary>
        /// <param name="newValue">New selected items.</param>
        /// <param name="oldValue">Old Selected items.</param>
        /// <returns>Index of the last checked/unchecked item.</returns>
        internal static int FormatIndexCheckedArgs(object newValue, object oldValue)
        {
            // array format of new selected items
            int[] newValueList = FormatItemCheckedArgs(newValue);
            // array format of old selected items
            int[] oldValueList = FormatItemCheckedArgs(oldValue);

            if (newValueList == null)
            {
                return oldValueList[0];
            }
            else if (oldValueList == null)
            {
                return newValueList[0];
            }

            // when checked was the last action
            if (newValueList.Length > oldValueList.Length)
            {
                // return the index of the last checked
                return newValueList.Except(oldValueList).FirstOrDefault();
            }
            // when unchecked was the last action 
            else
            {
                // return the index of the last unchecked
                return oldValueList.Except(newValueList).FirstOrDefault();
            }
        }

        /// <summary>
        /// Format the check state of the last checked/unchecked item.
        /// </summary>
        /// <param name="newValue">New selected items.</param>
        /// <param name="oldValue">Index of the last checked/unchecked item.</param>
        /// <returns>CheckState of the last checked/unchecked item.</returns>
        internal static CheckState FormatStateCheckedArgs(object newValue, object oldValue)
        {

            if (oldValue == null)
            {
                return CheckState.Checked;
            }
            else if (newValue == null)
            {
                return CheckState.Unchecked;

            }

            int[] newValueList = FormatItemCheckedArgs(newValue);
            int[] oldValueList = FormatItemCheckedArgs(oldValue);
            // when checked was the last action
            if (newValueList.Length > oldValueList.Length)
            {
                return CheckState.Checked;
            }
            // when unchecked was the last action 
            else
            {
                return CheckState.Unchecked;
            }
        }

        /// <summary>
        /// Get Date Value .
        /// </summary>
        /// <param name="dateValue">dateValue as string </param>
        public static string GetDateValue(string dateValue)
        {
            DateTime date;
            if (DateTime.TryParse(dateValue, out date))
            {
                return GetDate(date);
            }

            if (DateTime.TryParseExact(dateValue, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                return GetDate(date);
            }

            return JavaScriptBuilder.GetValueCode(string.Empty);
        }

        /// <summary>
        /// Get date according server format .
        /// </summary>
        /// <param name="date">the date with server format </param>
        /// <returns> the date</returns>
        private static string GetDate(DateTime date)
        {
            return JavaScriptBuilder.GetValueCode(date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
        }
    }
}
