﻿using Newtonsoft.Json;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    /// <summary>
    /// Provides a temporary place holder for components
    /// </summary>
	public class DefaultComponentExtJSRenderer : VisualElementExtJSRenderer
    {


        /// <summary>
        /// Renders the specified object visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void Render(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            
        }

        /// <summary>
        /// Gets the ext js type.
        /// </summary>
        /// <value>
        /// The ext js type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return string.Empty;
            }
        }


        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return string.Empty;
            }
        }
    }
}
