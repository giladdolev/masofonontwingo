﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ToolBarItem), "ControlElementExtJSRenderer", "button", "Ext.button.Button")]
    [ExtJSPropertyDescription("Image", PropertyInitializeCode = "icon = <#= element.Image #>", PropertyChangeCode = "this.setIcon('<#= element.Image.Source #>');")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "text = <#= GetDisplayTextWithoutCommands(element.Text) #>")]
    [ExtJSPropertyDescription("DropDownArrows", PropertyInitializeCode = "arrowVisible = <#= JavaScriptCode.GetBool(element.DropDownArrows) #>", PropertyChangeCode = "this.setArrowVisible(<#= JavaScriptCode.GetBool(element.DropDownArrows) #>); ")]
    [ExtJSPropertyDescription("ImageScaling", PropertyInitializeCode = "listeners= [{afterrender: function () {<#resource:imageScaling.js #>}}]", ClientNotDefaultCode = " element.ImageScaling != ToolStripItemImageScaling.SizeToFit && element.Image.Source != null", PropertyChangeCode = "<#resource:imageScaling.js#>")]
    [ExtJSPropertyDescription("TextImageRelation", PropertyInitializeCode = "iconAlign = <#= ToolBarItemExtJSRenderer.ReviseTextImageRelation(element) #>", ClientNotDefaultCode = "element.TextImageRelation != TextImageRelation.Overlay && element.Text != String.Empty && element.Image.Source != null", PropertyChangeCode = "this.setIconAlign('<#= ToolBarItemExtJSRenderer.ReviseTextImageRelation(element) #>');")]

    [ExtJSEventDescription("Click", ClientEventName = "click")]
    public class ToolBarItemExtJSRenderer : ToolBarItemExtJSRendererBase
    {
        /// <summary>
        /// Gets the item x type
        /// </summary>
        /// <value>
        /// The item x type.
        /// </value>
        protected virtual string ItemXType
        {
            get
            {
                return "";
            }
        }

        /// <summary>
        /// Renders the x type property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderXTypeProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // validate parameter
            if (jsonWriter == null || visualElement == null)
            {
                return;
            }

            // If is a toolbar base item
            if (visualElement.ParentElement is ToolBarElement && !(visualElement.ParentElement is ContextMenuStripElement))
            {
                // Render default
                base.RenderXTypeProperty(renderingContext, visualElement, jsonWriter);
            }
            else
            {
                // If there is a non empty item x type
                if (!string.IsNullOrEmpty(ItemXType))
                {
                    // Write the xtype property
                    RenderProperty(renderingContext, jsonWriter, "xtype", ItemXType);
                }
            }
        }

        /// <summary>
        /// Renders the Text property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the toolbar item
            ToolBarItem element = visualElement as ToolBarItem;

            // If the element is valid
            if (element != null && (element.DisplayStyle == ToolBarItemDisplayStyle.Text || element.DisplayStyle == ToolBarItemDisplayStyle.ImageAndText))
            {
                // Render text property
                base.RenderTextProperty(context, visualElement, jsonWriter);
            }
        }


        /// <summary>
        /// Renders the Image property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderImageProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the toolbaritem element
            ToolBarItem element = visualElement as ToolBarItem;

            // If the element is valid
            if (element != null && (element.DisplayStyle == ToolBarItemDisplayStyle.Image || element.DisplayStyle == ToolBarItemDisplayStyle.ImageAndText))
            {
                if (element.Image != null)
                {
                    base.RenderImageProperty(context, visualElement, jsonWriter);
                }
                else
                {
                    // try to get image from image list.
                    ControlElement parentCtrl = Utilities.Utilities.SeekFotImageListOnParents(element);

                    if (parentCtrl != null)
                    {
                        // Get Image list .
                        ImageList imageList = parentCtrl.ImageList;
                        if (imageList != null)
                        {
                            ResourceReference img = null;

                            foreach (KeyItem key in imageList.Keys)
                            {
                                if (key.ImageIndex == element.ImageIndex)
                                {
                                    element.Image = new ResourceReference(key.Key);
                                    base.RenderImageProperty(context, visualElement, jsonWriter);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        }



        /// <summary>
        /// Renders the tool tip text property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderToolTipTextProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the toolbar element
            ToolBarItem element = visualElement as ToolBarItem;

            // If the element is valid
            if (element != null && !String.IsNullOrEmpty(element.ToolTipText))
            {
                // Render tool tip property
                base.RenderToolTipTextProperty(context, visualElement, jsonWriter);

                // Write title type
                //jsonWriter.WriteProperty("tooltipType", "title");
            }
        }


        /// <summary>
        /// Revises the textImageRelation.
        /// </summary>    
        /// <param name="buttonBaseElement"the control.</param>
        /// <returns></returns>
        internal static string ReviseTextImageRelation(ToolBarItem toolBarItem)
        {
            switch (toolBarItem.TextImageRelation)
            {
                case TextImageRelation.ImageAboveText:
                    return "top";
                case TextImageRelation.ImageBeforeText:
                    return "left";
                case TextImageRelation.TextAboveImage:
                    return "bottom";
                case TextImageRelation.TextBeforeImage:
                    return "right";
            }
            return "center";
        }
    }
}
