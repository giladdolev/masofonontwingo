﻿using System.Web.VisualTree.Elements;
using Newtonsoft.Json;
using System.Web.VisualTree.Engine;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ToolBarControlHost), "ToolBarItemExtJSRenderer", "", "")]
    public class ToolBarControlHostExtJSRenderer : ToolBarControlHostExtJSRendererBase
    {
        /// <summary>
        /// Renders the specified object visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void Render(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ToolBarControlHost controlHost = visualElement as ToolBarControlHost;

            if(controlHost != null)
            {
                VisualElementExtJSRenderer controlRenderer = VisualTreeManager.GetRenderer(renderingContext, controlHost.Control) as VisualElementExtJSRenderer;

                if(controlRenderer != null)
                {
                    controlRenderer.Render(renderingContext, visualElement, jsonWriter);
                }
            }
        }
    }
}
