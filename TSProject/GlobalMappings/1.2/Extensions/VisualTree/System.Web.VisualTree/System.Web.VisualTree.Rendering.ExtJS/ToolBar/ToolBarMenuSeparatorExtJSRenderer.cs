﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
	[ExtJSRendererDescription(typeof(ToolBarMenuSeparator), "ToolBarItemExtJSRenderer", "menuseparator", "Ext.menu.Separator")]
    public class ToolBarMenuSeparatorExtJSRenderer : ToolBarMenuSeparatorExtJSRendererBase
    {
        /// <summary>
        /// Gets the item x type
        /// </summary>
        /// <value>
        /// The item x type.
        /// </value>
        protected override string ItemXType
        {
            get
            {
                return XType;
            }
        }
    }
}
