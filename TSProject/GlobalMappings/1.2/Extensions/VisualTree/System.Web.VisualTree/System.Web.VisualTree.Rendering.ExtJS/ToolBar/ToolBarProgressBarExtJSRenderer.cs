﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
	[ExtJSRendererDescription(typeof(ToolBarProgressBar), "ToolBarItemExtJSRenderer", "progressbar", "Ext.ProgressBar")]
    [ExtJSPropertyDescription("Value", PropertyInitializeCode = "value = <#= ToolBarProgressBarExtJSRenderer.FormatValue(element) #>",
                                        PropertyChangeCode = "this.updateProgress(<#= ToolBarProgressBarExtJSRenderer.FormatValue(element) #>, '<#= ToolBarProgressBarExtJSRenderer.FormatText(element) #>');")]      
    public class ToolBarProgressBarExtJSRenderer : ToolBarProgressBarExtJSRendererBase
    {

        /// <summary>
        /// Formats the text.
        /// </summary>
        /// <param name="progressbarElement">The progress bar element.</param>
        /// <returns></returns>
        public static string FormatText(ToolBarProgressBar progressbar)
        {
            return string.Concat(Math.Ceiling(FormatValue(progressbar) * 100), "%");
        }


        /// <summary>
        /// Formats the value.
        /// </summary>
        /// <param name="progressbarElement">The progress bar element.</param>
        /// <returns></returns>
        public static double FormatValue(ToolBarProgressBar progressbar)
        {
            double value = 0;

            if (progressbar != null)
            {
                if (progressbar.Min < progressbar.Max)
                {
                    // value should be between 0-1
                    value = (double)(progressbar.Value - progressbar.Min) / (double)(progressbar.Max - progressbar.Min);
                }
            }

            return value;
        }
    }
}
