﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ToolBarSeparator), "ToolBarItemExtJSRenderer", "tbseparator", "Ext.toolbar.Separator")]
    public class ToolBarSeparatorExtJSRenderer : ToolBarSeparatorExtJSRendererBase
    {
        /// <summary>
        /// Gets the item x type
        /// </summary>
        /// <value>
        /// The item x type.
        /// </value>
        protected override string ItemXType
        {
            get
            {
                return XType;
            }
        }
    }
}
