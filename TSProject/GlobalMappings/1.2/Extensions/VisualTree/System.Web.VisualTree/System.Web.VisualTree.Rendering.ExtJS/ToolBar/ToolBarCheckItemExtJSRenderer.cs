﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ToolBarCheckItem), "ToolBarDropDownItemExtJSRenderer", "menucheckitem", "Ext.menu.CheckItem")]
    public class ToolBarCheckItemExtJSRenderer : ToolBarCheckItemExtJSRendererBase
    {
        /// <summary>
        /// Gets the item x type
        /// </summary>
        /// <value>
        /// The item x type.
        /// </value>
        protected override string ItemXType
        {
            get
            {
                return XType;
            }
        }
    }
}
