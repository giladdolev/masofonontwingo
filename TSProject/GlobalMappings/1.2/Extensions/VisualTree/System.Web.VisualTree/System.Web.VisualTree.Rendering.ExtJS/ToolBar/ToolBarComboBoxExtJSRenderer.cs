﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ToolBarComboBox), "ToolBarItemExtJSRenderer", "combobox", "Ext.form.field.ComboBox")]
    public class ToolBarComboBoxExtJSRenderer : ToolBarComboBoxExtJSRendererBase
    {
        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The object json writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            // Try casting the element in to a list control element
            ToolBarComboBox objToolBarComboBoxElement = visualElement as ToolBarComboBox;

            // If casting is valid
            if (objToolBarComboBoxElement != null)
            {
                // Render the store property
                RenderStoreProperty(objToolBarComboBoxElement, jsonWriter, objToolBarComboBoxElement.Items);
            }
        }
    }
}
