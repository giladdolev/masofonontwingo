﻿using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ToolBarDropDownItem), "ToolBarItemExtJSRenderer", "button", "Ext.button.Button")]
    public class ToolBarDropDownItemExtJSRenderer : ToolBarDropDownItemExtJSRendererBase
    {

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ToolBarDropDownItem toolbarElement = visualElement as ToolBarDropDownItem;

            if (toolbarElement != null && toolbarElement.DropDownItems.Count > 0)
            {
                RenderPlainItemsProperty(renderingContext, toolbarElement.DropDownItems, jsonWriter, "menu");
            }
        }
    }
}
