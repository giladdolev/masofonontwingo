using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

    public class ToolBarItemExtJSRendererBase : ControlElementExtJSRenderer
    {
        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.button.Button";
            }
        }


        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "button";
            }
        }


        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element == null)
            {
                return;
            }

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            // Choose property
            switch (property)
            {
                case "Image":
                    this.RenderImagePropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "DropDownArrows":
                    this.RenderDropDownArrowsPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "BorderSides":
                    this.RenderBorderSidesPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "ImageScaling":
                    this.RenderImageScalingPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                case "TextImageRelation":
                    this.RenderTextImageRelationPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);

            // Set ExtJS rendering context
            ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);

            this.RenderImageProperty(renderingContext, visualElement, jsonWriter);
            this.RenderDropDownArrowsProperty(renderingContext, visualElement, jsonWriter);
            this.RenderBorderSidesProperty(renderingContext, visualElement, jsonWriter);
            this.RenderImageScalingProperty(renderingContext, visualElement, jsonWriter);
            this.RenderTextImageRelationProperty(renderingContext, visualElement, jsonWriter);
        }


        /// <summary>
        /// Renders the Click event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachClickEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid element
            if (element != null)
            {
                // Check if the event should be registered
                if (this.ShouldRenderClickEvent(context, element))
                {
                    // If there is a valid JSON writer
                    if (jsonWriter != null)
                    {
                        // The current event behavior
                        var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                        // Revise the event behavior
                        eventBehaviorType = this.ReviseClickEventBehavior(context, element, eventBehaviorType);

                        // Render the client event handler
                        JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
                        // Write anonymous function
                        javaScriptBuilder
                            .AppendAnonymousFunction(
                                new string[] { },
                                    () => {
                                        // Create the client callback code.Click
                                        if (!String.IsNullOrEmpty(element.ClientClick))
                                        {
                                            javaScriptBuilder.AppendRawValue(element.ClientClick);
                                            javaScriptBuilder.AppendSemicolon();
                                        }
                                        // Create the server callback code.Click

                                        javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Click", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));

                                    }
                            );
                        JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

                        javaScriptBuilder = new JavaScriptBuilder();
                        javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


                        jsonWriter.WriteProperty("click", new JavaScriptCode(javaScriptBuilder.ToString()));

                    }
                }
            }
        }


        /// <summary>
        /// Revise the event behavior type of Click event.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseClickEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
        {
            return eventBehaviorType;
        }


        /// <summary>
        /// Indicates if we should render the Click event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderClickEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return (!String.IsNullOrEmpty(element.ClientClick) || element.HasClickListeners);
        }


        /// <summary>
        /// Extracts the Click event event arguments.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
        protected override EventArgs ExtractClickCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
            return new System.EventArgs();
        }


        /// <summary>
        /// Renders the Click event add listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderClickAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {


            // Get the typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderClickEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseClickEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[] { },
                            () => {
                                // Create the client callback code.Click
                                if (!String.IsNullOrEmpty(element.ClientClick))
                                {
                                    javaScriptBuilder.AppendRawValue(element.ClientClick);
                                    javaScriptBuilder.AppendSemicolon();
                                }
                                // Create the server callback code.Click

                                jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Click", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
                            }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('click', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()));



            }
        }

        /// <summary>
        /// Renders the Click event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderClickRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

            // Get the typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            if (element != null && javaScriptBuilder != null && this.ShouldRenderClickEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "click");
            }
        }

        /// <summary>
        /// Renders the Image property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderImageProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    jsonWriter.WriteProperty("icon", element.Image);
                }
            }

        }

        /// <summary>
        /// Renders the Image property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderImagePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setIcon('{1}');", new JavaScriptCode(componentVariable), element.Image.Source);
            }

        }


        /// <summary>
        /// Renders the DropDownArrows property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderDropDownArrowsProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    jsonWriter.WriteProperty("arrowVisible", JavaScriptCode.GetBool(element.DropDownArrows));
                }
            }

        }

        /// <summary>
        /// Renders the DropDownArrows property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDropDownArrowsPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setArrowVisible({1}); ", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(element.DropDownArrows));
            }

        }


        /// <summary>
        /// Renders the BorderSides property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBorderSidesProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {

        }

        /// <summary>
        /// Renders the BorderSides property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBorderSidesPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {

        }


        /// <summary>
        /// Renders the ImageScaling property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderImageScalingProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If ImageScaling value is not default
                    if (element.ImageScaling != ToolStripItemImageScaling.SizeToFit && element.Image.Source != null)
                    {
                        jsonWriter.WriteProperty("listeners", new JavaScriptCode(String.Format(@"[{{afterrender: function () {{if({0}){{this.btnIconEl.setStyle('background-size', 'contain');}}else{{ this.btnIconEl.setStyle('background-size', 'initial');}}}}}}]", JavaScriptCode.GetBool(element.ImageScaling == ToolStripItemImageScaling.SizeToFit))));
                    }
                }
            }

        }

        /// <summary>
        /// Renders the ImageScaling property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderImageScalingPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"if({1}){{{0}.btnIconEl.setStyle('background-size', 'contain');}}else{{ {0}.btnIconEl.setStyle('background-size', 'initial');}}", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(element.ImageScaling == ToolStripItemImageScaling.SizeToFit));
            }

        }


        /// <summary>
        /// Renders the Text property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If property is valid
                    if (ValidateProperty(element.Text))
                    {
                        jsonWriter.WriteProperty("text", GetDisplayTextWithoutCommands(element.Text));
                    }
                }
            }

        }

        /// <summary>
        /// Renders the TextImageRelation property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTextImageRelationProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                // Check rendering type
                if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    // If TextImageRelation value is not default
                    if (element.TextImageRelation != TextImageRelation.Overlay && element.Text != String.Empty && element.Image.Source != null)
                    {
                        jsonWriter.WriteProperty("iconAlign", ToolBarItemExtJSRenderer.ReviseTextImageRelation(element));
                    }
                }
            }

        }

        /// <summary>
        /// Renders the TextImageRelation property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTextImageRelationPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.ToolBarItem element = visualElement as System.Web.VisualTree.Elements.ToolBarItem;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.setIconAlign('{1}');", new JavaScriptCode(componentVariable), ToolBarItemExtJSRenderer.ReviseTextImageRelation(element));
            }

        }


    }
}