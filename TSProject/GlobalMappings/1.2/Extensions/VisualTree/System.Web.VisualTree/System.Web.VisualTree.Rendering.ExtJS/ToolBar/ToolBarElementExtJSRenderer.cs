﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ToolBarElement), "ControlElementExtJSRenderer", "toolbar", "Ext.toolbar.Toolbar")]
    [ExtJSPropertyDescription("Vertical", PropertyInitializeCode = "vertical = <#=element.Vertical#>")]
	public class ToolBarElementExtJSRenderer : ToolBarElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ToolBarElement toolbarElement = visualElement as ToolBarElement;
            
            if(toolbarElement != null)
            {
                RenderPlainItemsProperty(renderingContext, toolbarElement.Items, jsonWriter, "items", a => true); 
            }
        }
        /// <summary>
        /// Renders the Controls property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderControlsPropertyChange(ExtJSRenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // Render the collection change actions
                VisualElementExtJSCollectionRenderer.RenderCollectionChangeActions<VisualElementExtJSNonReversedCollectionRenderer>(context, controlElement.Controls, builder, componentVariable);
            }
        }
        /// <summary>
        /// Determines whether is visible toolbar item.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        private bool IsVisibleToolbarItem(VisualElement visualElement)
        {
            // Get toolbar item
            ToolBarItem toolBarItem = visualElement as ToolBarItem;

            // If there is a valid toolbar item
            if(toolBarItem != null)
            {
                // If is a visible toolbar item
                return toolBarItem.Visible;
            }

            return false;
        }

    }
}
