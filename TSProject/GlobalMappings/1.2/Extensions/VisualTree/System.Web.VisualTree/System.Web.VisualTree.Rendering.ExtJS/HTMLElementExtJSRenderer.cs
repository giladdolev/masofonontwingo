﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Rendering.ExtJS;
using System.Web.VisualTree.Rendering;

namespace System.Web.VisualTree
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(HTMLElement), "PanelElementExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("Url", PropertyChangeCode = "this.update('<iframe width='100%' style='height:100vh;' src=<#= element.Url #>></iframe>');")]
    public class HTMLElementExtJSRendererRenderer : HTMLElementExtJSRendererBase
    {
        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
       public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderContentProperties(renderingContext, visualElement, jsonWriter);

            // Get Iframe element
            HTMLElement hTMLElement = visualElement as HTMLElement;

            // If there is a valid Iframe element 
            if (hTMLElement != null)
            {
                jsonWriter.WriteProperty("html", "<iframe width='100%' style='height:100vh;' src='" + hTMLElement.Url + "'></iframe>");
            }
        }

    }
}
