﻿using Newtonsoft.Json;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
	public class VisualElementOperationExtJSRenderer : VisualElementExtJSRenderer
    {
        /// <summary>
        /// Renders the update.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        internal override void RenderUpdate(VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            base.RenderUpdate(visualElement, jsonWriter);

            // Get message box operation
            VisualElementOperation objVisualElementOperation = visualElement as VisualElementOperation;

            // If there is a valid operation
            if (objVisualElementOperation != null)
            {
                // Create string buffer
                JavaScriptBuilder objBuffer = new JavaScriptBuilder();

                // Generate handler function
                objBuffer.AppendAnonymousFunction(() =>
                {
                    // Render operation code
                    RenderOperationCode(objVisualElementOperation, objBuffer);
                });

                // Render function value
                jsonWriter.WriteRawValue(objBuffer.ToString());
            }

        }

        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="objVisualElementOperation">The visual element operation.</param>
        /// <param name="objBuilder">The java script builder.</param>
        protected virtual void RenderOperationCode(VisualElementOperation objVisualElementOperation, JavaScriptBuilder objBuilder)
        {

        }




        /// <summary>
        /// Gets the ext js type.
        /// </summary>
        /// <value>
        /// The ext js type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.operatoin.Operation";
            }
        }

        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "operatoin";
            }
        }
    }
}
