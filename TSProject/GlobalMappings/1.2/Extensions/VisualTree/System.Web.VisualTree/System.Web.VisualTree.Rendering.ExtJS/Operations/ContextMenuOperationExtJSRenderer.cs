﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(ContextMenuOperation))]
	public class ContextMenuOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {


        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="visualElementOperation">The visual element operation.</param>
        /// <param name="jsBuilder">The java script builder.</param>
        protected override void RenderOperationCode(VisualElementOperation visualElementOperation, JavaScriptBuilder jsBuilder)
        {
            if (jsBuilder == null)
            {
                throw new ArgumentNullException("jsBuilder");
            }

            // Get message box operation
            ContextMenuOperation contextMenuOperation = visualElementOperation as ContextMenuOperation;

            // If there is a valid operation
            if (contextMenuOperation != null)
            {
                // create the menu variable
                jsBuilder.AppendRawValue("var mnu =");
                jsBuilder.AppendNewObjectCreationExpression("Ext.menu.Menu");
                jsBuilder.AppendStartObject();
                jsBuilder.AppendRawValue("items: ");

                // Create the json string writer
                StringWriter jsonStringWriter = new StringWriter(CultureInfo.InvariantCulture);

                // Create the json writer
                using (JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(jsonStringWriter))
                {


                    // Write start array
                    jsonWriter.WriteStartArray();

                    // Render each menu item each with a click event listener
                    foreach (ToolBarItem item in contextMenuOperation.DropDownItems)
                    {
                        // Get renderer
                        VisualElementExtJSRenderer objVisualElementExtJSRenderer = VisualTreeManager.GetRenderer(item) as VisualElementExtJSRenderer;

                        // If there is a valid renderer
                        if (objVisualElementExtJSRenderer != null)
                        {
                            // Render object
                            objVisualElementExtJSRenderer.Render(item, jsonWriter);
                        }
                    }

                    // End array
                    jsonWriter.WriteEndArray();

                    // Flush columns
                    jsonWriter.Flush();

                    jsBuilder.AppendRawValue(jsonStringWriter.ToString());

                    jsBuilder.AppendEndObject();
                    jsBuilder.AppendEndInvocation(true);

                    // Add hide attachment
                    jsBuilder.AppendInvocationStatement("mnu.on", "hide", new JavaScriptCode(string.Concat("function(){VT_raiseDelayedEvent('", VisualElement.GetElementId(contextMenuOperation), "', 'Callback', 1);}")));

                    // Get mouse position
                    Point mousePosition = ApplicationElement.MousePosition;

                    // Add show at invocation
                    jsBuilder.AppendInvocationStatement("mnu.showAt", new int[] { mousePosition.X, mousePosition.Y});


                }
            }
        }





    }
}
