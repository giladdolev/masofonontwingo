﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
    [RendererDescription(typeof(VisualElementChangedOperation))]
    public class VisualElementChangedOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {

        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="builder">The builder.</param>
        protected override void RenderOperationCode(VisualElementOperation operation, JavaScriptBuilder builder)
        {
            // Get the change operation
            VisualElementChangedOperation changeOperation = operation as VisualElementChangedOperation;

            // If there is a valid change operation
            if(changeOperation != null)
            {
                // Get element renderer
                VisualElementRenderer elementRenderer = VisualTreeManager.GetRenderer(changeOperation.Element) as VisualElementRenderer;

                // If there is a valid element renderer
                if(elementRenderer != null)
                {
                    // Render the change
                    elementRenderer.RenderChange(changeOperation.Element, changeOperation.Properties, builder);
                }
            }
        }
    }
}
