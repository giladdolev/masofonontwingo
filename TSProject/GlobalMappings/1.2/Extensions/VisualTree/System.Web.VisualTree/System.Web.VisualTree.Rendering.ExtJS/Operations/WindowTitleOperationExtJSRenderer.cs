﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(WindowTitleOperation))]
	public class WindowTitleOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {
        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="visualElementOperation">The visual element operation.</param>
        /// <param name="jsBuilder">The java script builder.</param>
        protected override void RenderOperationCode(VisualElementOperation visualElementOperation, JavaScriptBuilder jsBuilder)
        {
            // validate parameter
            if (jsBuilder == null)
            {
                return;
            }

            // Get window title operation
            WindowTitleOperation windowTitleOperation = visualElementOperation as WindowTitleOperation;

            // If there is a valid operation
            if (windowTitleOperation != null)
            {
                // Append assignment statement
                jsBuilder.AppendAssignmentStatement("document.title", windowTitleOperation.Text);
            }
        }
    }
}
