﻿using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(InputBoxElementOperation))]
	public class InputBoxElementOperationExtJSRenderer : MessageBoxElementOperationExtJSRenderer
    {



        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="objVisualElementOperation">The visual element operation.</param>
        /// <param name="objBuilder">The buffer.</param>
        protected override void RenderOperationCode(VisualElementOperation objVisualElementOperation, JavaScriptBuilder objBuilder)
        {
            // Get message box operation
            InputBoxElementOperation inputBoxOperation = objVisualElementOperation as InputBoxElementOperation;

            // If there is a valid operation
            if (inputBoxOperation != null)
            {
                if (objBuilder == null)
                {
                    throw new ArgumentNullException("objBuilder");
                }

                // Create message box invocation statement
                objBuilder.AppendStartInvocation("Ext.Msg.prompt");
                objBuilder.AppendValue(inputBoxOperation.Caption);
                objBuilder.AppendComma();
                objBuilder.AppendValue(inputBoxOperation.Text);
                objBuilder.AppendComma();
                objBuilder.AppendRawValue(GetHandlerCode(inputBoxOperation));
                objBuilder.AppendEndInvocation(true);
            }
        }

        /// <summary>
        /// Gets the handler code.
        /// </summary>
        /// <param name="inputBoxOperation">The input box operation.</param>
        /// <returns></returns>
        private static string GetHandlerCode(InputBoxElementOperation inputBoxOperation)
        {
            return string.Concat("function(btn, text){", GetHandlerCodeBody(inputBoxOperation), "}");
        }

        /// <summary>
        /// Gets the handler code body.
        /// </summary>
        /// <param name="inputBoxOperation">The input box operation.</param>
        /// <returns></returns>
        private static string GetHandlerCodeBody(InputBoxElementOperation inputBoxOperation)
        {
            return string.Concat("VT_raiseEvent('", VisualElement.GetElementId(inputBoxOperation), "', 'Callback', {value:btn, text:text});");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IVisualElementRenderer))]
    [RendererTempEventDescription(typeof(InputBoxElementOperation), "Callback")]
    public class InputCallbackEventExtJSRenderer : CallbackEventExtJSRenderer
    {

        /// <summary>
        /// Gets the action arguments from js.
        /// </summary>
        /// <param name="visualElement"></param>
        /// <param name="strActionName"></param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected internal override EventArgs GetEventArgs(VisualElement visualElement, string strActionName, JObject eventObject)
        {
            ValueChangedArgs<DialogResult> dialogArgs = base.GetEventArgs(visualElement, strActionName, eventObject) as ValueChangedArgs<DialogResult>;

            if (dialogArgs != null && dialogArgs.Value == DialogResult.OK)
            {
                return new ValueChangedArgs<string>(eventObject.GetPropertyAsString("text", string.Empty));
            }
            else
            {
                return new ValueChangedArgs<string>(string.Empty);
            }
        }

    }
}
