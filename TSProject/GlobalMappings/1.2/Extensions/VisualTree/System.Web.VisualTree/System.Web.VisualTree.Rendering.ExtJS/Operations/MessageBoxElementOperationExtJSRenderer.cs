﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(MessageBoxElementOperation))]
	public class MessageBoxElementOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {




        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="objVisualElementOperation">The visual element operation.</param>
        /// <param name="objBuilder">The buffer.</param>
        protected override void RenderOperationCode(VisualElementOperation objVisualElementOperation, JavaScriptBuilder objBuilder)
        {
            if (objBuilder == null)
            {
                throw new ArgumentNullException("objBuilder");
            }

            // Get message box operation
            MessageBoxElementOperation messageBoxOperation = objVisualElementOperation as MessageBoxElementOperation;

            // If there is a valid operation
            if (messageBoxOperation != null)
            {
                // Create message box invocation statement
                objBuilder.AppendStartInvocation("Ext.Msg.show");

                // Create text writer to write in the messagebox properties
				
                StringWriter textWriter = new StringWriter(CultureInfo.InvariantCulture);

                // Create the json writer
				using (JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(textWriter))
				{

					// Write the start object
					jsonWriter.WriteStartObject();

					// Add the title property
					jsonWriter.WriteProperty("title", messageBoxOperation.Caption);

					// Add the msg property
					jsonWriter.WriteProperty("msg", messageBoxOperation.Text);

					// Add the buttons property
					jsonWriter.WriteProperty("buttons", new JavaScriptCode(GetMessageBoxButtons(messageBoxOperation.Buttons)));

					// If there is a callback
					if (messageBoxOperation.Callback != null)
					{
						// Add the handler code
						jsonWriter.WriteProperty("fn", new JavaScriptCode(GetHandlerCode(messageBoxOperation)));
					}

					// Write the end object
					jsonWriter.WriteEndObject();

					// Flush result
					jsonWriter.Flush();

					// Add the generated raw code
					objBuilder.AppendRawValue(textWriter.ToString());

					// Close the invocation
					objBuilder.AppendEndInvocation(true);
				}
            }
        }

        /// <summary>
        /// Gets the handler code.
        /// </summary>
        /// <param name="messageBoxOperation">The message box operation.</param>
        /// <returns></returns>
        private static string GetHandlerCode(MessageBoxElementOperation messageBoxOperation)
        {
            return string.Concat("function(btn){", GetHandlerCodeBody(messageBoxOperation), "}");
        }

        /// <summary>
        /// Gets the handler code body.
        /// </summary>
        /// <param name="messageBoxOperation">The message box operation.</param>
        /// <returns></returns>
        private static string GetHandlerCodeBody(MessageBoxElementOperation messageBoxOperation)
        {            
            return string.Concat("VT_raiseEvent('", VisualElement.GetElementId(messageBoxOperation), "', 'Callback', {value:btn}, '", VisualElement.GetElementId(messageBoxOperation.Owner) , "');");
        }

        /// <summary>
        /// Gets the message box buttons.
        /// </summary>
        /// <param name="messageBoxButtons">The message box buttons.</param>
        /// <returns></returns>
        private static string GetMessageBoxButtons(MessageBoxButtons messageBoxButtons)
        {
            switch (messageBoxButtons)
            {
                
                case MessageBoxButtons.OKCancel:
                    return "Ext.Msg.OKCANCEL";
                case MessageBoxButtons.AbortRetryIgnore:
                    return "Ext.Msg.OK";
                case MessageBoxButtons.YesNoCancel:
                    return "Ext.Msg.YESNOCANCEL";
                case MessageBoxButtons.YesNo:
                    return "Ext.Msg.YESNO";
                case MessageBoxButtons.RetryCancel:
                    return "Ext.Msg.YESNO";                    
                case MessageBoxButtons.OK:
                default:
                    return "Ext.Msg.OK";
            }
        }

        
    }

    /// <summary>
    /// Provides support for handling messagebox callbacks
    /// </summary>
    [Export(typeof(IVisualElementRenderer))]
    [RendererTempEventDescription(typeof(MessageBoxElementOperation), "Callback")]
    public class CallbackEventExtJSRenderer : VisualElementEventExtJSRenderer
    {




        /// <summary>
        /// Gets the action arguments from js.
        /// </summary>
        /// <param name="visualElement"></param>
        /// <param name="strActionName"></param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected internal override EventArgs GetEventArgs(VisualElement visualElement, string strActionName, JObject eventObject)
        {
            // Set default result
            DialogResult dialogResult = DialogResult.None;

            // Determine the actual result
            switch (eventObject.GetPropertyAsString("value", string.Empty))
            {
                case "no":
                    dialogResult = DialogResult.No;
                    break;
                case "ok":
                    dialogResult = DialogResult.OK;
                    break;
                case "cancel":
                    dialogResult = DialogResult.Cancel;
                    break;
                case "yes":
                    dialogResult = DialogResult.Yes;
                    break;
                case "retry":
                    dialogResult = DialogResult.Retry;
                    break;
                case "ignore":
                    dialogResult = DialogResult.Ignore;
                    break;
            }

            // Return value changed args
            return new ValueChangedArgs<DialogResult>(dialogResult);
        }

    }
}
