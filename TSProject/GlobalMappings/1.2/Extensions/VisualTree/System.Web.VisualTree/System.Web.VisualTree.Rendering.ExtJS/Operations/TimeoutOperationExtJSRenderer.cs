﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(TimeoutOperation))]
	public class TimeoutOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {
        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="visualElementOperation">The visual element operation.</param>
        /// <param name="jsBuilder">The java script builder.</param>
        protected override void RenderOperationCode(VisualElementOperation visualElementOperation, JavaScriptBuilder jsBuilder)
        {
            if (jsBuilder == null)
            {
                return;
            }

            // Get timeout operation
            TimeoutOperation timeoutOperation = visualElementOperation as TimeoutOperation;

            // If there is a valid enabled operation
            if (timeoutOperation != null && timeoutOperation.Enabled)
            {                
                // Append deleyed raise statement
                jsBuilder.AppendInvocationStatement("VT_raiseDelayedEvent", VisualElement.GetElementId(timeoutOperation), "Callback", timeoutOperation.Interval);
            }
        }
    }
}
