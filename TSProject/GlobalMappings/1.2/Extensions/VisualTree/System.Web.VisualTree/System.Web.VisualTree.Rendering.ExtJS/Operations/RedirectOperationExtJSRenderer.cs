﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(RedirectOperation))]
	public class RedirectOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {
        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="visualElementOperation">The visual element operation.</param>
        /// <param name="jsBuilder">The java script builder.</param>
        protected override void RenderOperationCode(VisualElementOperation visualElementOperation, JavaScriptBuilder jsBuilder)
        {
            if (jsBuilder == null)
            {
                return;
            }

            // Get redirect operation
            RedirectOperation redirectOperation = visualElementOperation as RedirectOperation;

            // If there is a valid operation
            if (redirectOperation != null)
            {
                // Append redirect script
                jsBuilder.AppendAssignmentStatement("document.location.href", redirectOperation.Url);
            }
        }
    }
}
