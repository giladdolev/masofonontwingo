﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(BrowserOperation))]
	public class BrowserOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {
        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="visualElementOperation">The visual element operation.</param>
        /// <param name="jsBuilder">The java script builder.</param>
        protected override void RenderOperationCode(VisualElementOperation visualElementOperation, JavaScriptBuilder jsBuilder)
        {
            if (jsBuilder == null)
            {
                throw new ArgumentNullException("jsBuilder");
            }

            // Get browser operation
            BrowserOperation browserOperation = visualElementOperation as BrowserOperation;

            // If there is a valid operation
            if (browserOperation != null)
            {
                // Append window open invocation
                jsBuilder.AppendInvocationStatement("window.open", browserOperation.Url);
            }
        }
    }
}
