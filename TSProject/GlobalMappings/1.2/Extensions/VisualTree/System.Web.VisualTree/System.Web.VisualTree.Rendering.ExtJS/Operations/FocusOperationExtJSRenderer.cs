﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(FocusOperation))]
	public class FocusOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {
                /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="visualElementOperation">The visual element operation.</param>
        /// <param name="objBuilder">The buffer.</param>
        protected override void RenderOperationCode(VisualElementOperation visualElementOperation, JavaScriptBuilder objBuilder)
        {
            if (objBuilder == null)
            {
                throw new ArgumentNullException("objBuilder");
            }

            // Get focused operation
            FocusOperation focusOperation = visualElementOperation as FocusOperation;

            // If there is a valid focus
            if(focusOperation != null)
            {
                // Get the focused element
                objBuilder.AppendVariableWithInvocationInitializer("cmp", "VT_GetCmp", VisualElement.GetElementId(focusOperation.FocusedElement));

                // If there is a valid component
                objBuilder.AppendStartIf("cmp");

                // Call the focus method
                objBuilder.AppendInvocationStatement("cmp.focus");
                objBuilder.AppendEndIf();
            }
        }
    }
}
