﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Operation;

namespace System.Web.VisualTree.Rendering.ExtJS.Operations
{
    [Export(typeof(IVisualElementRenderer))]
    [RendererDescription(typeof(WindowElementOperation))]
	public class WindowElementOperationExtJSRenderer : VisualElementOperationExtJSRenderer
    {

        /// <summary>
        /// Renders the operation code.
        /// </summary>
        /// <param name="objVisualElementOperation">The visual element operation.</param>
        /// <param name="objBuilder">The buffer.</param>
        protected override void RenderOperationCode(VisualElementOperation objVisualElementOperation, JavaScriptBuilder objBuilder)
        {
            // validate parameter
            if (objBuilder == null)
            {
                return;
            }

            // Get window operation element
            WindowElementOperation windowElementOperation = objVisualElementOperation as WindowElementOperation;

            // If there is a valid window operation element
            if (windowElementOperation != null)
            {
                // If is an open window operation 
                if (windowElementOperation.OperationType == WindowElementOperationType.Open || windowElementOperation.OperationType == WindowElementOperationType.OpenModal)
                {
                    // Get window renderer
                    VisualElementExtJSRenderer windowRenderer = VisualTreeManager.GetRenderer(windowElementOperation.Window) as VisualElementExtJSRenderer;

                    // If there is a valid window renderer
                    if (windowRenderer != null)
                    {
						
						// Create the json string writer
                        StringWriter jsonStringWriter = new StringWriter(CultureInfo.InvariantCulture);

                        // Create the json writer
						using (JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(jsonStringWriter))
						{

							// Render window config
							windowRenderer.Render(windowElementOperation.Window, jsonWriter);

							// Flush content
							jsonWriter.Flush();
							jsonStringWriter.Flush();

							// Add open window code
							objBuilder.AppendVariableWithInvocationInitializer("win", "Ext.create", "widget.window", new JavaScriptCode(jsonStringWriter.ToString()));

							// If is an open modal
							if (windowElementOperation.OperationType == WindowElementOperationType.OpenModal)
							{
								// Set window as modal
								objBuilder.AppendAssignmentStatement("win.modal", true);
							}

							// Show the window
							objBuilder.AppendInvocationStatement("VT_ShowWindow", new JavaScriptCode("win"));
						}
                    }
                }
                else
                {
                    // Get component and distroy it if valid.
                    objBuilder.AppendVariableWithInvocationInitializer("win", "VT_GetCmp", windowElementOperation.ClientID);
                    objBuilder.AppendInvocationStatement("if (win) win.destroy");
                }
            }
        }
    }
}
