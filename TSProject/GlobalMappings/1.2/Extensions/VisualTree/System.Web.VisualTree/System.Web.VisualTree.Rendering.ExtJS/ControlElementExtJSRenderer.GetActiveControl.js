
setTimeout(function(){
    try {
        var activeControl =Ext.get(Ext.Element.getActiveElement());
        console.log(activeControl);
        var activeControlId = "";
        if (activeControl != null) {
            activeControlId = activeControl.component.itemId;
        }
        <#callback:{value:activeControlId}#>;
    } catch (e) {
        console.log("GetActiveControl.js error : " + e.message);
    }
},100);