﻿using System.Drawing;
using System.Globalization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Common;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Text;
using System.IO;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ContextMenuStripElement), "ControlElementExtJSRenderer", "menu", "Ext.menu.Menu")]
    class ContextMenuStripExtJSRenderer : ContextMenuStripExtJSRendererBase
    {
        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <exception cref="System.ArgumentNullException">jsonWriter</exception>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            ContextMenuStripElement contextMenuStrip = visualElement as ContextMenuStripElement;

            if (contextMenuStrip != null)
            {
                RenderPlainItemsProperty(renderingContext, contextMenuStrip.Items, jsonWriter, "items", IsVisibleToolbarItem);
            }
        }

        protected override void RenderPropertyChange(RenderingContext renderingContext, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            switch (property)
            {
                case "Items":
                    RenderItemsPropertyChange(renderingContext, visualElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }

        private void RenderItemsPropertyChange(RenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            ContextMenuStripElement contextMenuStrip = visualElement as ContextMenuStripElement;

            // If there is a valid control element
            if (contextMenuStrip != null)
            {
                // Render the collection change actions
                VisualElementExtJSCollectionRenderer.RenderCollectionChangeActions<VisualElementExtJSNonReversedCollectionRenderer>(context, contextMenuStrip.Items, builder, componentVariable);
            }
        }

        /// <summary>
        /// Determines whether is visible toolbar item.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        static bool IsVisibleToolbarItem(VisualElement visualElement)
        {
            // Get toolbar item
            ToolBarItem toolBarItem = visualElement as ToolBarItem;

            // If there is a valid toolbar item
            if (toolBarItem != null)
            {
                // If is a visible toolbar item
                return toolBarItem.Visible;
            }

            return false;
        }
    }
}
