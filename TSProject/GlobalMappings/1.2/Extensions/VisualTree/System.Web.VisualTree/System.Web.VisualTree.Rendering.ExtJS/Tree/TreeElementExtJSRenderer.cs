﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TreeElement), "ControlElementExtJSRenderer", "treepanel", "Ext.tree.Panel")]
    [ExtJSPropertyDescription("SelectedItem", PropertyInitializeCode = null, PropertyChangeCode = "this.selectPath('<#=TreeElementExtJSRenderer.FormatSelected(element)#>', 'itemId');")]
    [ExtJSPropertyDescription("RootVisible", PropertyInitializeCode = "rootVisible = false")]
    [ExtJSPropertyDescription("Store", PropertyInitializeCode = "store = <#= TreeElementExtJSRenderer.FormatValue(element) #>")]
    [ExtJSPropertyDescription("SearchPanel", PropertyInitializeCode = "searchPanel = <#= element.SearchPanel #>", PropertyChangeCode = "this.setSearchPanel(<#= JavaScriptCode.GetBool(element.SearchPanel) #>);")]

    [ExtJSMethodDescription("ApplyFilter", MethodCode = "<#resource: FilterNodes.js#>")]
    [ExtJSMethodDescription("CollapseAll",MethodCode="this.collapseAll()")]
    [ExtJSMethodDescription("Refresh", MethodCode = "this.reconfigure(this.store); this.store.reload();")]
    [ExtJSEventDescription("NodeMouseClick", ClientEventName = "itemclick", ClientEventHandlerParameterNames = "view, record, item, index, e, eOpts", ClientHandlerCallbackData = "node= VT_getItemIdPath(record)", ServerEventArgsCreateCode = "new TreeMouseClickEventArgs(visualElement.GetVisualElementByPath(this.node) as TreeItem)")]
    [ExtJSEventDescription("NodeMouseDoubleClick", ClientEventName = "itemdblclick", ClientEventHandlerParameterNames = "view, record, item, index, e, eOpts", ClientHandlerCallbackData = "node= VT_getItemIdPath(record)", ServerEventArgsCreateCode = "new TreeMouseClickEventArgs(visualElement.GetVisualElementByPath(this.node) as TreeItem)")]

    [ExtJSEventDescription("AfterExpand", ClientEventName = "afteritemexpand", ClientEventHandlerParameterNames = "node, index, item, eOpts", ClientHandlerCallbackData = "node= node.get('itemId')", ServerEventArgsCreateCode = "new TreeEventArgs(visualElement.GetVisualElementByPath(this.node) as TreeItem)")]
    [ExtJSEventDescription("BeforeExpand", ClientEventName = "beforeitemexpand", ClientEventHandlerParameterNames = "node, eOpts", ClientHandlerCallbackData = "newNode = node.get('itemId')", ServerEventArgsCreateCode = "new TreeEventArgs(visualElement.GetVisualElementByPath(this.newNode) as TreeItem)")]
    [ExtJSEventDescription("BeforeCollapse", ClientEventName = "beforeitemcollapse", ClientEventHandlerParameterNames = "node, eOpts", ClientHandlerCallbackData = "newNode =  node.get('itemId')", ServerEventArgsCreateCode = "new TreeEventArgs(visualElement.GetVisualElementByPath(this.newNode) as TreeItem)")]
    [ExtJSEventDescription("BeforeCheck", ClientEventName = "beforecheckchange", ClientEventHandlerParameterNames = "node, checked, e, eOpts", ClientHandlerCallbackData = "newNode = node.get('itemId'), newState = checked", ServerEventArgsCreateCode = "new TreeEventArgs(visualElement.GetVisualElementByPath(this.newNode) as TreeItem){ Checked = Convert.ToBoolean(this.newState)}")]
   
    public class TreeElementExtJSRenderer : TreeElementExtJSRendererBase
    {

        /// <summary>
        /// Revises the XTYPE property value.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <returns></returns>
        protected override string ReviseXTypePropertyValue(VisualElement visualElement, string xtype)
        {
            // Get TreeElement .
            TreeElement tree = visualElement as TreeElement;

            // If there is a valid TreeElement .
            if (tree != null)
            {
                // If there is text
                if (tree.SearchPanel == true)
                {
                    // Return title bar XTYPE
                    return "searchTree";
                }
            }

            return xtype;
        }


        /// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            TreeElement treeElement = visualElement as TreeElement;

            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);
            if (treeElement.Columns.Count > 0)
            {
                RenderPlainItemsProperty(context, treeElement.Columns, jsonWriter, "columns", (element => element != null && ((TreeColumn)element).Visible));
            }
            
        }

        /// <summary>
        /// Renders the Store property.
        /// </summary>
        /// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderStoreProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Get typed element
            System.Web.VisualTree.Elements.TreeElement element = visualElement as System.Web.VisualTree.Elements.TreeElement;

            // If there is a valid typed element
            if (element != null)
            {
                if (element.DataSource != null)
                {
                    RenderTreeDataSourceProperty(renderingContext, element, jsonWriter);
                }
                // Check rendering type
                else if (renderingContext.RenderingType == ExtJSRenderingType.Property)
                {
                    jsonWriter.WriteProperty("store", TreeElementExtJSRenderer.FormatValue(element));
                }
            }

        }
        /// <summary>
        /// Renders the tree data source property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public virtual void RenderTreeDataSourceProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get the binding manager
            BindingManager bindingManager = BindingManager.GetBindingManager(visualElement);

            // If there is a valid binding manager
            if (bindingManager != null)
            {
                // Get renderer
                VisualElementExtJSRenderer renderer = VisualTreeManager.GetRenderer(renderingContext, bindingManager) as VisualElementExtJSRenderer;

                // If there is a valid renderer
                if (renderer != null)
                {
                    // Write the store property
                    jsonWriter.WritePropertyName("store");
                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                    {
                        TreeElement treeElement = visualElement as TreeElement;
                        if (treeElement != null)
                        {
                            // render fields 

                            jsonWriter.WritePropertyName("fields");
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                            {
                                foreach (TreeColumn column in treeElement.Columns)
                                {
                                    using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                                    {
                                        jsonWriter.WriteProperty("name", column.DataMember);
                                    }
                                }

                            }

                            jsonWriter.WritePropertyName("root");
                            using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Object))
                            {
                                RenderChildrenProperty(renderingContext, treeElement.Items, jsonWriter);

                            }

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Formats the value.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static TreeStoreElementResourceReference FormatValue(TreeElement element)
        {
            return new TreeStoreElementResourceReference(element, "store");
        }


        /// <summary>
        /// Formats the selected.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        internal static string FormatSelected(TreeElement element)
        {
            return TreeItem.GetItemPath(element.SelectedItem);
        }

        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public override IVisualElementRenderer RenderResourceResult(RenderingContext renderingContext, IVisualElement visualElement, string resourceId)
        {
            // If is a store resource id
            if (resourceId == "store")
            {
                // Return the tree store resource action result
                return new TreeStoreElementResource(visualElement as VisualElement);
            }

            return base.RenderResourceResult(renderingContext, visualElement, resourceId);
        }

        /// <summary>
        /// Renders the children property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="items">The items.</param>
        /// <param name="jsonWriter">The json writer.</param>
        internal static void RenderChildrenProperty(RenderingContext renderingContext, TreeItemCollection items, JsonTextWriter jsonWriter)
        {
            // If there are valid items
            if (items != null && items.Count > 0)
            {
                // Render expanded property
                jsonWriter.WriteProperty("expanded", true);
                // set a custom class for the icon

                // Write children array property
                jsonWriter.WriteStartArrayProperty("children");

                // Render the children array property
                RenderChildrenArrayItemsProperty(renderingContext, items, jsonWriter);

                // End array
                jsonWriter.WriteEndArray();
            }
        }

        /// <summary>
        /// Renders the children array items property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="items">The items.</param>
        /// <param name="jsonWriter">The json writer.</param>
        internal static void RenderChildrenArrayItemsProperty(RenderingContext renderingContext, TreeItemCollection items, JsonTextWriter jsonWriter)
        {
            // Loop all tree items
            foreach (TreeItem item in items)
            {
                // Get element renderer
                VisualElementExtJSRenderer elementRenderer = VisualTreeManager.GetRenderer(renderingContext, item) as VisualElementExtJSRenderer;

                // If there is a valid element renderer
                if (elementRenderer != null)
                {
                    // Render object
                    elementRenderer.Render(renderingContext, item, jsonWriter);
                }
            }
        }

    }
}
