var store = this.getStore();
store.clearFilter();
var filterFn = function (record) {
        return record.data.visible;
    };
store.filterBy(filterFn);
