﻿using Newtonsoft.Json;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    public class TreeStoreElementResource : StoreElementResource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeStoreElementResource" /> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public TreeStoreElementResource(IVisualElement visualElement)
            : base(visualElement)
        {

        }

        /// <summary>
        /// Executes the json result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderJsonResource(RenderingContext renderingContext, JsonTextWriter jsonWriter)
        {
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            // Get the tree element 
            TreeElement treeElement = Element as TreeElement;

            // If there is a valid tree element
            if (treeElement != null)
            {
                jsonWriter.WriteStartArray();
                TreeElementExtJSRenderer.RenderChildrenArrayItemsProperty(renderingContext, treeElement.Items, jsonWriter);
                jsonWriter.WriteEndArray();
            }
            else
            {
                jsonWriter.WriteEmptyObject();
            }
        }
    }
}
