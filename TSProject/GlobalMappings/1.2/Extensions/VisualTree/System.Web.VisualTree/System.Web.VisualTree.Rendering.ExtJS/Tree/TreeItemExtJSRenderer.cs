﻿using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TreeItem), "VisualElementExtJSRenderer", "<#= empty #>")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "text = <#= element.Text #>")]
    [ExtJSPropertyDescription("IsLeaf", PropertyInitializeCode = "leaf = <#= element.IsLeaf #>")]
    [ExtJSPropertyDescription("IsExpanded", PropertyInitializeCode = "expanded = <#= element.IsExpanded #>", PropertyChangeCode = "(<#=JavaScriptCode.GetBool( element.IsExpanded )#> ? this.expand(): this.collapse());")]
    [ExtJSPropertyDescription("Icon", "icon = <#= FormatHelper.FormatResource(renderingContext, element.Icon) #>")]
    [ExtJSPropertyDescription("Visible", PropertyInitializeCode = "visible = <#= element.Visible #>", PropertyChangeCode = "this.data.visible = <#= JavaScriptCode.GetBool(element.Visible) #>")]
    [ExtJSPropertyDescription("ImageKey", PropertyInitializeCode = "icon = <#= element.ParentTree.ImageList.Images[element.ImageKey].Source  #>", ClientNotDefaultCode = "element.ImageKey!=null && element.ParentTree !=null && element.ParentTree.ImageList != null && element.ParentTree.ImageList.Images[element.ImageKey] !=null ")]

    [ExtJSMethodDescription("Check", MethodCode = "<# resource:CheckDynamically.js#>")]
    public class TreeItemExtJSRenderer : TreeItemExtJSRendererBase
    {

        /// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Renders the x type property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderXTypeProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {

        }



        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get tree item
            TreeItem treeItem = visualElement as TreeItem;

            if (treeItem.ParentTree != null && treeItem.ParentTree.CheckBoxes == true)
            {
                jsonWriter.WriteProperty("checked", false);
            }

            // If there is a valid tree item
            if (treeItem != null)
            {

                //render columns
                for (int index = 0; index < treeItem.ParentTree.Columns.Count; index++)
                {
                    jsonWriter.WriteProperty(treeItem.ParentTree.Columns[index].DataMember, treeItem.Values[index]);
                }
                // Render children property
                TreeElementExtJSRenderer.RenderChildrenProperty(renderingContext, treeItem.Items, jsonWriter);

            }
        }
        protected override void RenderItemIdProperty(VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            jsonWriter.WriteProperty("itemId", TreeItem.GetItemId(visualElement as TreeItem));
        }

        /// <summary>
        /// Renders the Visible property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderVisiblePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
            System.Web.VisualTree.Elements.TreeItem element = visualElement as System.Web.VisualTree.Elements.TreeItem;

            // If there is a valid typed element
            if (element != null)
            {
                builder.AppendFormattedRaw(@"{0}.data.visible = {1};}}else{{{0} = VT_GetCmp('{2}/{3}/{4}'); if({0}){0}.data.visible = {1};", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(element.Visible), GetWindowID(element), element.ParentTree.ClientID, element.ClientID);
            }

        }

        /// <summary>
        /// Gets the window's ID.
        /// </summary>
        /// <param name="element">The element.</param>
        private string GetWindowID(VisualElement element)
        {
            if (element.ParentElement is WindowElement)
                return element.ParentElement.ClientID;
            else
                return GetWindowID(element.ParentElement);   
        }

    }
}
