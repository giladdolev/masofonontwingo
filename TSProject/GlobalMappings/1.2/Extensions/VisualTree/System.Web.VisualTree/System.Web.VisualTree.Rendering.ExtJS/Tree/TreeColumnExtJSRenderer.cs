﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TreeColumn), "ControlElementExtJSRenderer", "gridcolumn", "Ext.grid.column.Column")]
    [ExtJSPropertyDescription("DataMember", PropertyInitializeCode = "dataIndex = <#= element.DataMember #>")]
    [ExtJSPropertyDescription("HeaderText", PropertyInitializeCode = "text = <#= GetFormatedTextProperty(element, GetDisplayText(element.HeaderText)) #>", PropertyChangeCode = "this.setText('<#= GetDisplayText(element.HeaderText) #>');")]
    public class TreeColumnExtJSRenderer : TreeColumnExtJSRendererBase
    {

        /// <summary>
        /// Renders the x type property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderXTypeProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            TreeColumn treeColumn = visualElement as TreeColumn;
            TreeElement treeElement = treeColumn.Parent as TreeElement;
            if (treeColumn != null && treeElement != null)
            {
                int columnIndex = treeElement.Columns.IndexOf(treeColumn);
                if(columnIndex == 0)
                {
                    // Write the xtype property
                    RenderProperty(renderingContext, jsonWriter, "xtype", this.ReviseXTypePropertyValue(visualElement, "treecolumn"));
                }
                else
                {
                    // Write the xtype property
                    RenderProperty(renderingContext, jsonWriter, "xtype", this.ReviseXTypePropertyValue(visualElement, XType));
                }
            }
        }

    }
}
