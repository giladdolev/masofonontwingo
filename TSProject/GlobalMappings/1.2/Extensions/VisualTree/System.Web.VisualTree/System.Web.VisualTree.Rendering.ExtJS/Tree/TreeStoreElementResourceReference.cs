﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    public class TreeStoreElementResourceReference : StoreElementResourceReference
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeStoreElementResourceReference"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        public TreeStoreElementResourceReference(VisualElement visualElement, string resourceId)
            : base(visualElement, resourceId, "store:tree")
        {

        }
    }
}
