﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(LineElement), "ControlElementExtJSRenderer", "label", "Ext.form.Label")]
    [ExtJSPropertyDescription("BorderStyle", PropertyInitializeCode = "Style.border = <#= ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) #>,Style.borderRight=<#= ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) #>,Style.borderBottom=<#= ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) #>", PropertyChangeCode = "this.setStyle({<#= ControlElementExtJSRenderer.ReviseBorderStyle(element) #>});")]
    public class LineElementExtJSRenderer : LineElementExtJSRendererBase
    {

    }
}