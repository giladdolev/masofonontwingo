﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    /// <summary>
    /// Provides support for rendering collection actions
    /// </summary>
    public class VisualElementExtJSCollectionRenderer
    {
        /// <summary>
        /// Renders the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="changeActionsContainer">The change actions container.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        private void RenderCollection(RenderingContext context, IVisualElementCollectionChangeActionsContainer changeActionsContainer, JavaScriptBuilder builder, string componentVariable)
        {
            // If there is a valid change actions container
            if (changeActionsContainer != null)
            {
                // Loop all change actions
                foreach (IVisualElementCollectionChangeAction changeAction in changeActionsContainer.ChangeActions)
                {
                    // If there is a valid change action
                    if (changeAction != null)
                    {
                        // Create item renderer helper
                        Func<string> itemRenderer = () =>
                        {
                            // Get item renderer
                            VisualElementExtJSRenderer renderer = VisualTreeManager.GetRenderer(context, changeAction.Item) as VisualElementExtJSRenderer;

                            // If there is a valid renderer
                            if (renderer != null)
                            {
                                // Create the text writer
                                StringBuilder stringBuilder = new StringBuilder();

                                // Create JSON writer 
                                using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(stringBuilder))
                                {
                                    // Get item as visual element
                                    VisualElement itemVisualElement = changeAction.Item as VisualElement;

                                    // If there is a valid item 
                                    if (itemVisualElement != null)
                                    {
                                        // Render the store
                                        renderer.Render(context, itemVisualElement, jsonWriter);
                                    }
                                    else
                                    {
                                        // Return null
                                        jsonWriter.WriteValue((string)null);
                                    }

                                    jsonWriter.Flush();
                                }

                                // Return the JSON string
                                return stringBuilder.ToString();
                            }

                            // Return null string
                            return "null";
                        };

                        // Render the collection change action
                        this.RenderCollectionChangeAction(builder, componentVariable, changeAction, itemRenderer);
                    }
                }



                // Clear the change actions
                changeActionsContainer.ClearChangeActions();
            }
        }

        /// <summary>
        /// Renders the collection change action.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <param name="changeAction">The change action.</param>
        /// <param name="itemRenderer">The item renderer.</param>
        protected virtual void RenderCollectionChangeAction(JavaScriptBuilder builder, string componentVariable, IVisualElementCollectionChangeAction changeAction, Func<string> itemRenderer)
        {
            // Render based on change action
            switch (changeAction.Type)
            {
                case VisualElementCollectionChangeActionType.ClearItems:
                    this.RenderCollectionClearItemsAction(builder, componentVariable);
                    break;
                case VisualElementCollectionChangeActionType.SetItem:
                    this.RenderCollectionSetItemAction(builder, componentVariable, itemRenderer, GetNormalizedIndex(changeAction));
                    break;
                case VisualElementCollectionChangeActionType.InsertItem:                    
                    this.RenderCollectionInsertItemAction(builder, componentVariable, itemRenderer, GetNormalizedIndex(changeAction));
                    break;
                case VisualElementCollectionChangeActionType.RemoveItem:
                    this.RenderCollectionRemoveItemAction(builder, componentVariable, GetNormalizedIndex(changeAction));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Renders the collection clear items action.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderCollectionClearItemsAction(JavaScriptBuilder builder, string componentVariable)
        {
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".delayLayout"));
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".removeAll"), true);
        }

        /// <summary>
        /// Renders the collection set item action.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <param name="itemRenderer">The item renderer.</param>
        /// <param name="index">The index.</param>
        protected virtual void RenderCollectionSetItemAction(JavaScriptBuilder builder, string componentVariable, Func<string> itemRenderer, int index)
        {
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".delayLayout"));
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".insert"), index - 1, itemRenderer);
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".remove"), index);
        }

        /// <summary>
        /// Renders the collection insert item action.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <param name="itemRenderer">The item renderer.</param>
        /// <param name="index">The index.</param>
        protected virtual void RenderCollectionInsertItemAction(JavaScriptBuilder builder, string componentVariable, Func<string> itemRenderer, int index)
        {
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".delayLayout"));
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".insert"), index, itemRenderer);
        }

        /// <summary>
        /// Renders the collection remove item action.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        /// <param name="index">The index.</param>
        protected virtual void RenderCollectionRemoveItemAction(JavaScriptBuilder builder, string componentVariable, int index)
        {
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".delayLayout"));
            builder.AppendInvocationStatement(string.Concat(componentVariable, ".remove"), index);
        }

        /// <summary>
        /// Gets the normalized index.
        /// </summary>
        /// <param name="changeAction">The change action.</param>
        /// <returns></returns>
        protected virtual int GetNormalizedIndex(IVisualElementCollectionChangeAction changeAction)
        {
            // If should reverse client indices
            if (this.IsReversedClientIndices)
            {
                return changeAction.Length - changeAction.Index;
            }
            else
            {
                return changeAction.Index;
            }
        }


        /// <summary>
        /// Gets a value indicating whether this instance is reversed client indices.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is reversed client indices; otherwise, <c>false</c>.
        /// </value>
        protected virtual bool IsReversedClientIndices
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Renders the specified context.
        /// </summary>
        /// <typeparam name="TRenderer">The type of the renderer.</typeparam>
        /// <param name="context">The context.</param>
        /// <param name="container">The container.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        internal static void RenderCollectionChangeActions<TRenderer>(RenderingContext context, IVisualElementCollectionChangeActionsContainer container, JavaScriptBuilder builder, string componentVariable)
            where TRenderer : VisualElementExtJSCollectionRenderer, new()
        {
            // Create collection renderer
            TRenderer collectionRenderer = new TRenderer();

            // Render the collection
            collectionRenderer.RenderCollection(context, container, builder, componentVariable);
        }
    }

    /// <summary>
    /// Provides support for rendering collection actions with no reversing of indices
    /// </summary>
    public class VisualElementExtJSNonReversedCollectionRenderer : VisualElementExtJSCollectionRenderer
    {
        protected override bool IsReversedClientIndices
        {
            get
            {
                return false;
            }
        }
    }
}
