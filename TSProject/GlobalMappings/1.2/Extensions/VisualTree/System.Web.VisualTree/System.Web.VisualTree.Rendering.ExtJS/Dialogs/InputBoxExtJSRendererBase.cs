using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Dialogs
{

	public abstract class InputBoxExtJSRendererBase :  System.Web.VisualTree.Rendering.ExtJS.DefaultComponentExtJSRenderer
	{
		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "Show":
					return this.ExtractShowMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderStaticMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, string methodName, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "Show":
					this.RenderShowMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
                default:
                    base.RenderStaticMethod(context, methodInvoker, methodName, methodData, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the Show method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderShowMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
							var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.InputBoxData data = (System.Web.VisualTree.Elements.InputBoxData)methodData;
				builder.AppendFormattedRawValue(@"Ext.Msg.prompt({0},{1}, function(btn, text) {{        
    VT_raiseEvent({2}, 'Callback', {{value:text}});
}});",  data.Caption ,  data.Text ,  System.Web.VisualTree.Elements.VisualElement.GetElementId(methodInvoker) );
				}

		}


		/// <summary>
        /// Gets the method Show callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractShowMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return eventObject.Value<System.String>("value");
		}


	}
}