﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Dialogs
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(MessageBox), "System.Web.VisualTree.Rendering.ExtJS.DefaultComponentExtJSRenderer", "", "")]
    [ExtJSMethodDescription("ShowMessageBox", MethodCode = "<#resource:ShowMessageBox.js#>", MethodDataType = typeof(MessageBoxData), ServerCallbackDataCode = "MessageBoxExtJSRenderer.GetDialogResult(this.value.ToString())")] 
    public class MessageBoxExtJSRenderer : MessageBoxExtJSRendererBase
    {


        /// <summary>
        /// Gets the dialog result.
        /// </summary>
        /// <param name="clientResult">The client result.</param>
        /// <returns></returns>
        public static DialogResult GetDialogResult(string clientResult)
        {
            // Set default result
            DialogResult dialogResult = DialogResult.None;

            // Determine the actual result
            switch (clientResult)
            {
                case "no":
                    dialogResult = DialogResult.No;
                    break;
                case "ok":
                    dialogResult = DialogResult.OK;
                    break;
                case "cancel":
                    dialogResult = DialogResult.Cancel;
                    break;
                case "yes":
                    dialogResult = DialogResult.Yes;
                    break;
                case "retry":
                    dialogResult = DialogResult.Retry;
                    break;
                case "ignore":
                    dialogResult = DialogResult.Ignore;
                    break;
            }

            // Return dialog result
            return dialogResult;
        }

        public static JavaScriptCode GetMessageBoxIcon(MessageBoxIcon messageBoxIcon)
        {
            string icon = "''";
            switch (messageBoxIcon)
            {
                case MessageBoxIcon.None:
                case MessageBoxIcon.Hand:
                    break;
                case MessageBoxIcon.Question:
                    icon = "Ext.Msg.QUESTION";
                    break;
                case MessageBoxIcon.Asterisk:
                    break;
                case MessageBoxIcon.Stop:
                case MessageBoxIcon.Error:
                    icon = "Ext.Msg.ERROR";
                    break;
                case MessageBoxIcon.Exclamation:
                case MessageBoxIcon.Warning:
                    icon = "Ext.Msg.WARNING";
                    break;
                case MessageBoxIcon.Information:
                    icon = "Ext.Msg.INFO";
                    break;
            }
            return new JavaScriptCode(icon);
        }
        /// <summary>
        /// Gets the message box buttons.
        /// </summary>
        /// <param name="messageBoxButtons">The message box buttons.</param>
        /// <returns></returns>
        public static JavaScriptCode GetMessageBoxButtons(MessageBoxButtons messageBoxButtons)
        {
            string buttons = null;

            switch (messageBoxButtons)
            {
                case MessageBoxButtons.OKCancel:
                    buttons = "Ext.Msg.OKCANCEL";
                    break;
                case MessageBoxButtons.AbortRetryIgnore:
                    buttons = "Ext.Msg.OK";
                    break;
                case MessageBoxButtons.YesNoCancel:
                    buttons = "Ext.Msg.YESNOCANCEL";
                    break;
                case MessageBoxButtons.YesNo: 
                    buttons = "Ext.Msg.YESNO";
                    break;
                case MessageBoxButtons.RetryCancel:
                    buttons = "Ext.Msg.YESNO";
                    break;

                case MessageBoxButtons.OK:
                default:
                    buttons = "Ext.Msg.OK";
                    break;
            }

            return new JavaScriptCode(buttons);
        }

     
        /// <summary>
        /// checks which buttons are on the message box and returns the default button id according to default button requested(first, second or third)
        /// </summary>
        /// <param name="messageBoxData"></param>
        /// <returns></returns>
        public static JavaScriptCode GetMessageBoxDefaultButton(MessageBoxData messageBoxData)
        {
            string defaultButton = "";

            switch (messageBoxData.DefaultButton)
            {
                case MessageBoxDefaultButton.Button1:
                    if (messageBoxData.Buttons == MessageBoxButtons.AbortRetryIgnore)
                        defaultButton = "'Abort'";
                    else if (messageBoxData.Buttons == MessageBoxButtons.RetryCancel)
                        defaultButton = "'Retry'";
                    else if (messageBoxData.Buttons == MessageBoxButtons.OKCancel || messageBoxData.Buttons == MessageBoxButtons.OK)
                        defaultButton = "'Ok'";
                    else if (messageBoxData.Buttons == MessageBoxButtons.YesNo || messageBoxData.Buttons == MessageBoxButtons.YesNoCancel)
                        defaultButton = "'Yes'";
                    break;
                case MessageBoxDefaultButton.Button2:
                    if (messageBoxData.Buttons == MessageBoxButtons.AbortRetryIgnore)
                        defaultButton = "'Retry'";
                    else if (messageBoxData.Buttons == MessageBoxButtons.RetryCancel || messageBoxData.Buttons == MessageBoxButtons.OKCancel)
                        defaultButton = "'Cancel'";
                    else if (messageBoxData.Buttons == MessageBoxButtons.YesNo || messageBoxData.Buttons == MessageBoxButtons.YesNoCancel)
                        defaultButton = "'No'";
                    break;
                case MessageBoxDefaultButton.Button3:
                    if (messageBoxData.Buttons == MessageBoxButtons.AbortRetryIgnore)
                        defaultButton = "'Ignore'";
                    else if (messageBoxData.Buttons == MessageBoxButtons.YesNoCancel)
                        defaultButton = "'Cancel'";
                    break;
            }
            return new JavaScriptCode(defaultButton);
        }
        /// <summary>
        /// Gets the MessageBox Class
        /// </summary>
        /// <param name="data">The data</param>
        /// <returns>The Class</returns>
        public static JavaScriptCode GetMessageBoxClass(MessageBoxData data)
        {
            string strClass = "''";
            MessageBoxDirection direction = data.Direction;

            switch (direction)
            {
                case MessageBoxDirection.Rtl:
                    strClass = "'rtl'";
                    break;
                default:
                    strClass = "'ltr'";
                    break;
            }

            return new JavaScriptCode(strClass);
        }
    }
}
