﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Dialogs
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(InputBox), " System.Web.VisualTree.Rendering.ExtJS.DefaultComponentExtJSRenderer", "", "")]
    [ExtJSMethodDescription("Show", MethodCode = "<#resource:Show.js#>", MethodDataType = typeof(InputBoxData), ServerCallbackDataCode = "this.value.ToString()")]
    public class InputBoxExtJSRenderer : InputBoxExtJSRendererBase
    {

    }
}
