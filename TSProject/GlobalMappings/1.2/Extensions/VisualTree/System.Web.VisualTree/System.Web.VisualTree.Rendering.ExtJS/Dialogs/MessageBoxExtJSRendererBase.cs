using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Dialogs
{

	public abstract class MessageBoxExtJSRendererBase : System.Web.VisualTree.Rendering.ExtJS.DefaultComponentExtJSRenderer
	{
		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "ShowMessageBox":
					return this.ExtractShowMessageBoxMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderStaticMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, string methodName, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "ShowMessageBox":
					this.RenderShowMessageBoxMethod(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
                default:
                    base.RenderStaticMethod(context, methodInvoker, methodName, methodData, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the ShowMessageBox method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderShowMessageBoxMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
							var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Web.VisualTree.Elements.MessageBoxData data = (System.Web.VisualTree.Elements.MessageBoxData)methodData;
				builder.AppendFormattedRawValue(@"var messageBox = Ext.create('Ext.window.MessageBox', {{
    buttonText: {{
         ok: '�����',
        yes: '��',
        no: '��',
        cancel: '�����'
            
    }},
    alwaysOnTop:true
}});


messageBox.show({{
    title: {0},
    message: {1},
    buttons: {2},
    defaultFocus: {3},
    icon: {4},
    cls: {5},
    fn: function(btn) {{        
        VT_raiseEvent({6}, 'Callback', {{value:btn}});
        }}
}});",  data.Caption ,  JavaScriptUtilities.ParseString(data.Text) ,  MessageBoxExtJSRenderer.GetMessageBoxButtons(data.Buttons) ,  MessageBoxExtJSRenderer.GetMessageBoxDefaultButton(data) ,  MessageBoxExtJSRenderer.GetMessageBoxIcon(data.Icon) ,  MessageBoxExtJSRenderer.GetMessageBoxClass(data) ,  System.Web.VisualTree.Elements.VisualElement.GetElementId(methodInvoker) );
				}

		}


		/// <summary>
        /// Gets the method ShowMessageBox callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractShowMessageBoxMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return MessageBoxExtJSRenderer.GetDialogResult(eventObject.Value<System.String>("value"));
		}


	}
}