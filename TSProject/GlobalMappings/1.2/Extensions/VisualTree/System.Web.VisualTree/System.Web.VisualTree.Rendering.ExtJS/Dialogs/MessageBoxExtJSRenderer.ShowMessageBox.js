var messageBox = Ext.create('Ext.window.MessageBox', {
    buttonText: {
        ok: '�����',
        yes: '��',
        no: '��',
        cancel: '�����'
            
    },
    alwaysOnTop:true
});


messageBox.show({
    title: <#= data.Caption #>,
    message: <#= JavaScriptUtilities.ParseString(data.Text) #>,
    buttons: <#= MessageBoxExtJSRenderer.GetMessageBoxButtons(data.Buttons) #>,
    defaultFocus: <#= MessageBoxExtJSRenderer.GetMessageBoxDefaultButton(data) #>,
    icon: <#= MessageBoxExtJSRenderer.GetMessageBoxIcon(data.Icon) #>,
    cls: <#= MessageBoxExtJSRenderer.GetMessageBoxClass(data) #>,
    fn: function(btn) {        
        <#callback:{value:btn}#>
        }
});