﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(StatusElement), "ToolBarElementExtJSRenderer", "statusbar", "Ext.ux.StatusBar")]
    public class StatusElementExtJSRenderer : StatusElementExtJSRendererBase
    {
    }
}
