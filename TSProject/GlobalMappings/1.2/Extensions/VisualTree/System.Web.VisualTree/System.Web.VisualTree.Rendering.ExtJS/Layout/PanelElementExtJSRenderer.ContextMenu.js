cmp.el.addListener('contextmenu', function (e) {
    e.stopEvent();
    var menuCmp = VT_GetCmp('<#= Convert.ToString(element.ContextMenuStrip.ID) #>');
    menuCmp.on('render', function (menu) {
        menu.getEl().on('contextmenu', function (e) {
            e.stopEvent();
        });
    });
    menuCmp.enable();
    menuCmp.showBy(cmp, "tl?", [e.getX() - cmp.getX(), e.getY() - cmp.getY()]);
});

