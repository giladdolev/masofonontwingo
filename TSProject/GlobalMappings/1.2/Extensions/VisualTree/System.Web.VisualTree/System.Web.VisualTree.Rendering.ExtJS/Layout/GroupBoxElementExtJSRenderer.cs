﻿using Newtonsoft.Json;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Elements;
using System.Web.UI;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(GroupBoxElement), "ScrollableControlExtJSRenderer", "fieldset", "Ext.form.FieldSet")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "title = <#= GroupBoxElementExtJSRenderer.ReviseTitle(element) #>",
                                            PropertyChangeCode = "this.setTitle('<#= GroupBoxElementExtJSRenderer.ReviseTitle(element) #>');")]
    [ExtJSPropertyDescription("ForeColor", PropertyChangeCode = "this.setTitle('<#= GroupBoxElementExtJSRenderer.ReviseTitle(element) #>');")]
    [ExtJSPropertyDescription("Font", PropertyChangeCode = "this.setTitle('<#= GroupBoxElementExtJSRenderer.ReviseTitle(element) #>');")]
    public class GroupBoxElementExtJSRenderer : GroupBoxElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderControlsContentProperties(renderingContext, visualElement, jsonWriter);
        }


        /// <summary>
        /// Renders the text property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">controlElement</exception>
        internal static string ReviseTitle(VisualElement visualElement)
        {
            GroupBoxElement controlElement = visualElement as GroupBoxElement;

            if (controlElement != null)
            {

                // If text is valid
                if (!string.IsNullOrEmpty(controlElement.Text) && controlElement.BorderStyle != BorderStyle.None)
                {

                    // Render the title  property as html in order to add style
                    StringWriter stringwriter = new StringWriter(CultureInfo.InvariantCulture);
                    HtmlTextWriter writer = new HtmlTextWriter(stringwriter);


                    // by default
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Padding, "0 3px;");
                    ReviseFormattedText(controlElement, writer);

                    return stringwriter.ToString();
                }

            }
            return String.Empty;
        }
    }
}
