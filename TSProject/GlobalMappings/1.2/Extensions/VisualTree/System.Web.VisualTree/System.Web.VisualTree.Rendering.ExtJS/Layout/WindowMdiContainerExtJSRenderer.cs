﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(WindowMdiContainer), "ControlElementExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("Layout", PropertyInitializeCode = "layout = card")]
    public class WindowMdiContainerExtJSRenderer : WindowMdiContainerExtJSRendererBase
    {

        /// <summary>
        /// Gets the ext js type.
        /// </summary>
        /// <value>
        /// The ext js type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                return "Ext.panel.Panel";
            }
        }

        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                return "panel";
            }
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // Set card layout 
                jsonWriter.WriteProperty("layout", "card");

                // Render controls
                RenderPlainItemsProperty(renderingContext, controlElement.Controls, jsonWriter);
            }
        }
    }
}
