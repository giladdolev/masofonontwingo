﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.Web.VisualTree.Elements;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Interfaces;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    public abstract class WindowElementBaseExtJSRenderer : ControlContainerElementExtJSRenderer, IVisualElementRendererApp
    {
        /// <summary>
        /// Renders the application.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="windowElement">The window element.</param>
        /// <param name="objWriter">The writer.</param>
        /// <param name="renderTo">The render to.</param>
        public  virtual void RenderApp(RenderingContext renderingContext, IVisualElement visualElement, StringWriter objWriter, string renderTo)
        {
            WindowElement windowElement = (visualElement != null)? visualElement as WindowElement : null;

            if (renderingContext != null && renderingContext.SettingAlternativeMainWindow)
            {
                RenderInitialization(renderingContext, windowElement, objWriter, renderTo);
            }
            else
            {
                objWriter.WriteLine("<script type=\"text/javascript\">");

                // If there is a valid visual element
                if (visualElement != null)
                {
                    // Write the visual tree application ID
                    objWriter.WriteLine(string.Format("VT_APPID = '{0}';", visualElement.ApplicationId));
                }

                objWriter.WriteLine("Ext.Loader.setPath('VT.ux', 'Scripts/vt/ux');");
                objWriter.WriteLine("Ext.require('VT.ux.MultiLineTabs');");
                objWriter.WriteLine("Ext.onReady(function() {");

                // Call VT_onInitialize to hook events and such
                objWriter.WriteLine("VT_onInitialize();");

                // Invoke timer if needed
                windowElement.Application.InvokeTimers();

                // Initialize content
                RenderInitialization(renderingContext, windowElement, objWriter, renderTo);

                //Render all method invocations that occurs in initialization time
                RenderInitMethodsCalls(renderingContext, windowElement, objWriter);                
               
                // Call VT_onAfterInitialize to hook events and such after initialization
                objWriter.WriteLine("VT_onAfterInitialize();");

                objWriter.WriteLine("});");


                objWriter.WriteLine("</script>");
            }
        }



        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="windowElement">The window element.</param>
        /// <param name="objWriter">The object writer.</param>
        /// <param name="renderTo">The render to.</param>
        internal void RenderInitialization(RenderingContext renderingContext, WindowElement windowElement, StringWriter objWriter, string renderTo)
        {
            // If there is a valid render to
            if (!string.IsNullOrEmpty(renderTo))
            {
                // Render internal panel
                objWriter.Write(string.Format(CultureInfo.InvariantCulture, "window.VT_RootComponent = Ext.create('{0}', ", this.ExtJSType));
            }
            else
            {
                // Render as viewport
                objWriter.Write(string.Format(CultureInfo.InvariantCulture, "window.VT_RootComponent = Ext.create('Ext.container.Viewport', "));
            }

            // Create the json writer
            // dispose by JsonTextWriter when call close, but call is missing, can use using for the JsonTextWriter, no wanning for fxcop for this.. 
            JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(objWriter);

            // Write start object
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyName("renderTo");

            // If there is a valid render to
            if (!string.IsNullOrEmpty(renderTo))
            {
                // Get element to render to
                jsonWriter.WriteRawValue(string.Format(CultureInfo.InvariantCulture, "Ext.getElementById(\"{0}\")", renderTo));
            }
            else
            {
                // Render to body
                jsonWriter.WriteRawValue("Ext.getBody()");
            }

            if (windowElement != null && windowElement.RightToLeft == RightToLeft.Yes)
            {
                jsonWriter.WriteStartArrayProperty("requires");
                jsonWriter.WriteRawValue("'Ext.rtl.*'");
                jsonWriter.WriteEndArray();
            }

            // Render visual element properties
            this.RenderProperties(renderingContext, windowElement, jsonWriter);

            jsonWriter.WritePropertyWithRawValue("autoShow", "true");
            // Write the end object
            jsonWriter.WriteEndObject();

            // Flush json writer
            jsonWriter.Flush();

            objWriter.WriteLine(");");
        }

        /// <summary>
        /// Render all method invocations that occurs in initialization time
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="windowElement">The window element.</param>
        /// <param name="objWriter">The object writer.</param>
        internal void RenderInitMethodsCalls(RenderingContext renderingContext, WindowElement windowElement, StringWriter objWriter)
        {
            // Get the method invoker container
            IVisualElementMethodInvokerContainer methodInvokerContainer = windowElement.ParentElement as IVisualElementMethodInvokerContainer;

            // If there is a valid method invoker container
            if (methodInvokerContainer != null)
            {
                // The current method invoker
                IVisualElementMethodInvoker methodInvoker = null;
                JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(objWriter);
                
                objWriter.Write("VT_callback(");
                jsonWriter.WriteStartArray();
                // De-queue all method invokers
                while ((methodInvoker = methodInvokerContainer.Dequeue()) != null)
                {
                    // Get the visual element renderer
                    VisualElementExtJSRenderer objVisualElementRenderer = VisualTreeManager.GetRenderer(renderingContext, methodInvoker) as VisualElementExtJSRenderer;

                    // If there is a valid visual element renderer
                    if (objVisualElementRenderer != null)
                    {
                        // Render the operation
                        objVisualElementRenderer.RenderMethod(renderingContext, methodInvoker, jsonWriter);
                    }
                }

                // End the current response
                methodInvokerContainer.Clear();
                jsonWriter.WriteEndArray();
                jsonWriter.Flush();
                objWriter.WriteLine(");");
                

                
            }
            
        }
    }
}
