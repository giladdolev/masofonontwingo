try {
    var panelId = '<#= element.ID #>';
    var type = '<#= element.BorderStyle #>';
    var panel = VT_GetCmp(panelId);

    if(panel != null)
    {
        var domPanel = panel.el.dom;
        // Get the Base Css class
        var domPanelStyle = domPanel.getElementsByClassName('x-panel-body-default');
        if(domPanelStyle)
        {
            if(type == 'None')
            {
                // Change the default border-style to none
                domPanelStyle[0].style.setProperty('border-style','none');
            }
            else
            {
                // Restore the default border-style 
                domPanelStyle[0].style.setProperty('border-style','solid');
            }   
        }
    }

    this.setStyle({<#= ControlElementExtJSRenderer.ReviseBorderStyle(element) #>});
  
} catch (e) {
    console.log('Error occurred in UpdatePanelBorder.js : ' + e.message);
}