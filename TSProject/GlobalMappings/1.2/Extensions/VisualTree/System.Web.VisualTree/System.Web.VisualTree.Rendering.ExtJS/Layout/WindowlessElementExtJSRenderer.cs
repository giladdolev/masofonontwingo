﻿using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(WindowElement), "WindowElementBaseExtJSRenderer", "panel", "Ext.panel.Panel", IsAlternative = true)]
    public class WindowlessElementExtJSRenderer : WindowlessElementExtJSRendererBase
    {

        /// <summary>
        /// The instance
        /// </summary>
        public static WindowlessElementExtJSRenderer Instance
        {
            get { return new WindowlessElementExtJSRenderer(); }
        }




        /// <summary>
        /// Renders the invoke window action method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">Name of the component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder.</param>
        protected override void RenderInvokeWindowActionMethod(RenderingContext context, IVisualElementMethodInvoker methodInvoker, VisualElement visualElement, object methodData, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get the java script builder
            JavaScriptBuilder builder = getJavaScriptBuilder(false);

            // If there is a valid java script builder
            if (builder != null)
            {
                // Get window action type
                WindowActionType windowMethodData = (WindowActionType)methodData;

                // If context is valid
                if (context != null && context.SettingAlternativeMainWindow)
                {
                    // If is an open window action 
                    if (windowMethodData == WindowActionType.OpenAsMain)
                    {
                        // Render the window element
                        string applicationClientCode = VisualTreeManager.RenderElement(context, visualElement);

                        // If the generated code is valid
                        if (!String.IsNullOrWhiteSpace(applicationClientCode))
                        {
                            // AllowAppend the code to the response
                            builder.AppendRawValue(applicationClientCode);
                        }
                    }
                }
            }
        }




        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            WindowElement windowElement = visualElement as WindowElement;
            if (windowElement != null)
            {
                // Render controls content properties
                base.RenderContentProperties(renderingContext, visualElement, jsonWriter);



                if (IsInnerWindow(visualElement))
                {
                    jsonWriter.WriteProperty("resizable", true);
                    jsonWriter.WriteProperty("draggable", true);
                    //jsonWriter.WriteProperty("floating", true);
                    //jsonWriter.WriteProperty("closable", true);                    
                }
            }
        }

        /// <summary>
        /// Determines whether the specified visual element is inner window.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        internal static bool IsInnerWindow(VisualElement visualElement)
        {
            if (visualElement is WindowElement)
            {
                return !(visualElement.ParentElement is ApplicationElement);
            }

            return false;
        }
    }
}
