﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Text;
using System.Linq;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Collections.Generic;
using System.Drawing;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(PanelElement), "ScrollableControlExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "title = <#= element.ShowHeader ? RenderingUtils.NormalizeText(element.Text, \"&\"): string.Empty #>")]
    [ExtJSPropertyDescription("BorderStyle", PropertyInitializeCode = "Style.border = <#= ControlElementExtJSRenderer.ReviseBorderInitializeStyle(element) #>,Style.borderRight=<#= ControlElementExtJSRenderer.ReviseBorderRightInitializeStyle(element) #>,Style.borderBottom=<#= ControlElementExtJSRenderer.ReviseBorderBottomInitializeStyle(element) #>", PropertyChangeCode = "<#resource:UpdatePanelBorder.js#>")]
    [ExtJSPropertyDescription("Content", PropertyInitializeCode = "html = <#= Convert.ToString(element.Content) #>", PropertyChangeCode = "this.setHtml('<#= Convert.ToString(element.Content) #>');")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "bodyStyle.backgroundColor = <#= ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != Color.Empty", PropertyChangeCode = "this.setBodyStyle('backgroundColor','<#= ColorTranslator.ToHtml(element.BackColor) #>');")]

    [ExtJSPropertyDescription("BackgroundImage", PropertyInitializeCode = "bodyStyle.backgroundImage = <#= ControlElementExtJSRenderer.ReviseBackground(renderingContext, element) #>", ClientNotDefaultCode = "element.BackgroundImage != null", PropertyChangeCode = "this.setBodyStyle('backgroundImage','<#= ControlElementExtJSRenderer.ReviseBackground(context, element) #>');")]
    [ExtJSPropertyDescription("BackgroundImageLayout", PropertyInitializeCode = "bodyStyle.backgroundSize = <#= ControlElementExtJSRenderer.ReviseBackgroundSize(element.BackgroundImageLayout) #>, bodyStyle.backgroundRepeat = <#= ControlElementExtJSRenderer.ReviseBackgroundRepeat(element.BackgroundImageLayout) #> ,bodyStyle.backgroundPosition = <#= ControlElementExtJSRenderer.ReviseBackgroundPosition(element.BackgroundImageLayout) #> ", ClientNotDefaultCode = "element.BackgroundImage != null", PropertyChangeCode = " this.setBodyStyle('backgroundSize','<#= ControlElementExtJSRenderer.ReviseBackgroundSize(element.BackgroundImageLayout) #>');this.setBodyStyle('backgroundRepeat','<#= ControlElementExtJSRenderer.ReviseBackgroundRepeat(element.BackgroundImageLayout) #>');this.setBodyStyle('backgroundPosition','<#= ControlElementExtJSRenderer.ReviseBackgroundPosition(element.BackgroundImageLayout) #>');")]
    [ExtJSPropertyDescription("Collapsed", PropertyInitializeCode = "collapsed = <#=element.Collapsed #>", ClientNotDefaultCode = "element.Collapsed", PropertyChangeCode = "this.setCollapsed(<#=JavaScriptCode.GetBool(element.Collapsed) #>);")]
    [ExtJSPropertyDescription("Collapsible", PropertyInitializeCode = "collapsible =<#=element.Collapsible #>, titleCollapse =<#=element.Collapsible #>", ClientNotDefaultCode = "element.Collapsible", PropertyChangeCode = "collapsible=<#= JavaScriptCode.GetBool(element.Collapsible)#>, titleCollapse=<#= JavaScriptCode.GetBool(element.Collapsible)#>")]
    [ExtJSEventDescription("BeforeCollapse", ClientEventName = "beforecollapse")]
    [ExtJSEventDescription("BeforeExpand", ClientEventName = "beforeexpand")]
    [ExtJSEventDescription("Afterrender", ClientEventName = "afterrender", ClientEventHandlerParameterNames = "cmp", ClientCode = "<#resource:ContextMenu.js#>")]
    [ExtJSPropertyDescription("MinWidth", PropertyInitializeCode = "minWidth = <#= element.MinWidth #>", PropertyChangeCode = "this.setMinWidth(<#= element.MinWidth #>);")]
    [ExtJSPropertyDescription("MinHeight", PropertyInitializeCode = "minHeight = <#= element.MinHeight #>", PropertyChangeCode = "this.setMinHeight(<#= element.MinHeight #>);")]

    public class PanelElementExtJSRenderer : PanelElementExtJSRendererBase
    {

        
        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            RenderControlsContentProperties(renderingContext, visualElement, jsonWriter);
        }


        /// <summary>
        /// Renders the Controls property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderControlsPropertyChange(ExtJSRenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // Render the collection change actions
                VisualElementExtJSCollectionRenderer.RenderCollectionChangeActions<VisualElementExtJSNonReversedCollectionRenderer>(context, controlElement.Controls, builder, componentVariable);
            }
        }



        /// <summary>
        /// Renders the property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, VisualElement visualElement, string property, JavaScriptBuilder builder, string componentVariable)
        {
            switch (property)
            {
                case "ControlElementCollection":
                    RenderControlsPropertyUpdate(context, visualElement as PanelElement, builder, componentVariable);
                    break;
                default:
                    base.RenderPropertyChange(context, visualElement, property, builder, componentVariable);
                    break;
            }

        }





        /// <summary>
        /// Renders the controls property update.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="panelElement">The panel element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        private void RenderControlsPropertyUpdate(RenderingContext context, PanelElement panelElement, JavaScriptBuilder builder, string componentVariable)
        {
            if (panelElement != null)
            {
                StringBuilder sb = new StringBuilder();
                using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(sb))
                {
                    RenderPlainItemsPropertyValue(context, panelElement.Controls, jsonWriter);
                    jsonWriter.Flush();
                }
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".removeAll"));
                builder.AppendInvocationStatement(string.Concat(componentVariable, ".add"), new JavaScriptCode(sb.ToString()));
            }
        }




        /// <summary>
        /// Renders the HeaderText property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            PanelElement panelElement = visualElement as PanelElement;
            if (panelElement != null && panelElement.ShowHeader && !string.IsNullOrEmpty(panelElement.Text))
            {
                base.RenderTextProperty(context, visualElement, jsonWriter);
            }
        }
    }
}
