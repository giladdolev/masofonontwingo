﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    
    [Export(typeof(IVisualElementRenderer))]
	[RendererDescription(typeof(TablePanel))]
	public class TablePanelExtJSRenderer : PanelElementExtJSRenderer
    {


        /// <summary>
        /// Renders the style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderStyleProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            base.RenderStyleProperty(renderingContext, visualElement, jsonWriter);

            // Get the window element
            TablePanel tablePanelElement = visualElement as TablePanel;

            // write body style If there is a valid table panel element
            RenderingUtils.WriteBodyStyle(jsonWriter, tablePanelElement);
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        /// <exception cref="System.ArgumentNullException">jsonWriter</exception>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            if (jsonWriter == null)
            {
                throw new ArgumentNullException("jsonWriter");
            }

            // Get table panel
            TablePanel tableElement = visualElement as TablePanel;

            // If there is a valid table
            if (tableElement == null)
            {
                // Render layout property
                RenderProperty(renderingContext, jsonWriter, "layout", "table");
                return;
            }


            // Get the rows count
            int rowCount = tableElement.RowCount;

            // Get the column count
            int columnCount = tableElement.ColumnCount;

            // Render layout configuration
            jsonWriter.WritePropertyName("layout");
            jsonWriter.WriteStartObject();
            jsonWriter.WriteProperty("type", "table");
            jsonWriter.WriteProperty("columns", columnCount + 1);
            jsonWriter.WriteStartObjectProperty("tableAttrs");
            jsonWriter.WriteStartObjectProperty("style");
            jsonWriter.WriteProperty("width", "100%");
            jsonWriter.WriteProperty("height", "100%");
            jsonWriter.WriteProperty("tableLayout", "fixed");
            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndObject();


            // Create items property
            jsonWriter.WritePropertyName("items");
            jsonWriter.WriteStartArray();

            // Loop all control elements
            foreach (VisualElement itemElement in NormalizeTableItems(tableElement, tableElement.Controls, rowCount, columnCount))
            {
                // If there is no valid item
                if (itemElement == null)
                {
                    // Write empty object
                    jsonWriter.WriteObjectWithProperty("xtype", "image");
                    return;
                }

                SizingElement sizingElement = itemElement as SizingElement;
                if (sizingElement != null)
                {
                    

                    jsonWriter.WriteStartObject();
                    jsonWriter.WriteStartObjectProperty("tdAttrs");
                    if (!string.IsNullOrEmpty(sizingElement.Width))
                    {
                        jsonWriter.WriteProperty("width", sizingElement.Width);
                    }

                    if (!string.IsNullOrEmpty(sizingElement.Height))
                    {
                        jsonWriter.WriteProperty("height", sizingElement.Height);
                    }
                    jsonWriter.WriteEndObject();
                    jsonWriter.WriteProperty("xtype", "image");
                    jsonWriter.WriteEndObject();
                    return;
                }

                // Get renderer
                VisualElementExtJSRenderer objVisualElementExtJSRenderer = VisualTreeManager.GetRenderer(renderingContext, itemElement) as VisualElementExtJSRenderer;

                // If there is a valid renderer
                if (objVisualElementExtJSRenderer != null)
                {
                    // Render object
                    objVisualElementExtJSRenderer.Render(renderingContext, itemElement, jsonWriter);
                }
                else
                {
                    // Write empty object
                    jsonWriter.WriteEmptyObject();
                }
            }

            // End array
            jsonWriter.WriteEndArray();
        }

        
        private class SizingElement : VisualElement
        {

            /// <summary>
            /// Gets or sets the width.
            /// </summary>
            /// <value>
            /// The width.
            /// </value>
            public string Width { get; set; }

            /// <summary>
            /// Gets or sets the height.
            /// </summary>
            /// <value>
            /// The height.
            /// </value>
            public string Height { get; set; }
        }

        /// <summary>
        /// Normalizes the table items.
        /// </summary>
        /// <param name="tableElement">The table element.</param>
        /// <param name="items">The items.</param>
        /// <param name="rowCount">The row count.</param>
        /// <param name="columnCount">The column count.</param>
        /// <returns></returns>
        private static IEnumerable<VisualElement> NormalizeTableItems(TablePanel tableElement, IEnumerable<VisualElement> items, int rowCount, int columnCount)
        {
            // If there is a valid row/column count
            if (rowCount > 0 && columnCount > 0)
            {
                // Create ordered elements matrix
                VisualElement[,] orderedElements = new VisualElement[columnCount + 1, rowCount + 1];

                // If there are valid items
                if (items != null)
                {
                    // Loop all items
                    foreach (VisualElement item in items)
                    {
                        // Get control from item
                        ControlElement control = item as ControlElement;

                        // If there is a valid control
                        if (control != null)
                        {
                            // Get the table layout
                            ControlElementTableLayout tableLayout = control.ControlLayout as ControlElementTableLayout;

                            // If there is a valid table layout
                            if (tableLayout != null)
                            {
                                // Get control layout spans
                                int fromColumn = Math.Min(Math.Max(tableLayout.Column, 0), columnCount - 1);
                                int toColumn = Math.Min(Math.Max(fromColumn + (tableLayout.ColumnSpan - 1), fromColumn), columnCount - 1);
                                int fromRow = Math.Min(Math.Max(tableLayout.Row, 0), rowCount - 1);
                                int toRow = Math.Min(Math.Max(fromRow + (tableLayout.RowSpan - 1), fromRow), rowCount - 1);

                                bool blnIsFirst = true;

                                // Loop all columns
                                for (int columnIndex = fromColumn; columnIndex <= toColumn; columnIndex++)
                                {
                                    // Loop all rows
                                    for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++)
                                    {
                                        // If is the first cell
                                        if (blnIsFirst)
                                        {
                                            // Indicate not first
                                            blnIsFirst = false;

                                            // Set control
                                            orderedElements[columnIndex + 1, rowIndex + 1] = control;
                                        }
                                        else
                                        {
                                            // Set table to indicate not empty cell
                                            orderedElements[columnIndex + 1, rowIndex + 1] = tableElement;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Create the top left cell
                    orderedElements[0, 0] = new SizingElement() { Width = "1px", Height = "1px" };

                    // The column index
                    int columnSizingIndex = 1;

                    // Loop all cells
                    foreach (ColumnStyle columnStyle in tableElement.ColumnStyles)
                    {
                        // If column index is in range
                        if (columnSizingIndex <= columnCount)
                        {
                            // If there is a valid column style
                            if (columnStyle != null)
                            {
                                switch (columnStyle.SizeType)
                                {
                                    case SizeType.Absolute:
                                        orderedElements[columnSizingIndex, 0] = new SizingElement() { Width = string.Concat(columnStyle.Size, "px") };
                                        break;
                                    case SizeType.Percent:
                                        orderedElements[columnSizingIndex, 0] = new SizingElement() { Width = string.Concat(columnStyle.Size, "%") };
                                        break;
                                    case SizeType.AutoSize:
                                    default:
                                        break;
                                }
                            }

                            columnSizingIndex++;
                        }
                    }

                    // The row index
                    int rowSizingIndex = 1;

                    // Loop all rows
                    foreach (RowStyle rowStyle in tableElement.RowStyles)
                    {
                        // If row index is in range
                        if (rowSizingIndex <= rowCount)
                        {
                            // If there is a valid row style
                            if (rowStyle != null)
                            {
                                switch (rowStyle.SizeType)
                                {
                                    case SizeType.Absolute:
                                        orderedElements[0, rowSizingIndex] = new SizingElement() { Height = string.Concat(rowStyle.Size, "px") };
                                        break;
                                    case SizeType.Percent:
                                        orderedElements[0, rowSizingIndex] = new SizingElement() { Height = string.Concat(rowStyle.Size, "%") };
                                        break;
                                    case SizeType.AutoSize:
                                    default:
                                        break;
                                }
                            }

                            rowSizingIndex++;
                        }
                    }
                    // create elements list to render
                    List<VisualElement> elementsToRender = new List<VisualElement>();

                    // Loop all rows
                    for (int rowIndex = 0; rowIndex < rowCount + 1; rowIndex++)
                    {
                        // Loop all columns
                        for (int columnIndex = 0; columnIndex < columnCount + 1; columnIndex++)
                        {
                            // Get ordered element at position
                            VisualElement orderedElement = orderedElements[columnIndex, rowIndex];

                            // If is null
                            if (orderedElement == null)
                            {
                                // Add a place hoder (generates an empty cell)
                                elementsToRender.Add(null);
                            }
                            // If is not a place holder for span
                            else if (orderedElement != tableElement)
                            {
                                // Add elements to render
                                elementsToRender.Add(orderedElement);
                            }
                        }
                    }

                    // Return elements to render
                    return elementsToRender;
                }
            }

            // Return an empty array
            return VisualElement.EmptyArray;
        }


    }
}
