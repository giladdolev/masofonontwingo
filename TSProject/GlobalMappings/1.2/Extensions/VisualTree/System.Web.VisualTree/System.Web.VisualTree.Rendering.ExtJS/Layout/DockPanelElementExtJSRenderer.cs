﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DockPanelElement), "PanelElementExtJSRenderer", "tabpanel", "Ext.tab.Panel")]
    [ExtJSPropertyDescription("SelectedPanel", PropertyInitializeCode = "",
        PropertyChangeCode = "this.setActiveTab(<#= element.SelectedPanelID #>);")]
    public class DockPanelElementExtJSRenderer : DockPanelElementExtJSRendererBase
    {

    }
}
