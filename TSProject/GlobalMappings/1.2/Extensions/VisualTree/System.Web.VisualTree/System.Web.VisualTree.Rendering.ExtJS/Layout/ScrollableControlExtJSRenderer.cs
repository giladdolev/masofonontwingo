﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(ScrollableControl), "ControlElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSPropertyDescription("AutoScroll", PropertyInitializeCode = "scrollable = <#= element.AutoScroll #>", ClientNotDefaultCode = "element.AutoScroll", PropertyChangeCode = "this.setScrollable(<#= JavaScriptCode.GetBool( element.AutoScroll )#>)")]
    public class ScrollableControlExtJSRenderer : ScrollableControlExtJSRendererBase
    {
    }
}
