﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TabElement), "ControlContainerElementExtJSRenderer", "", "")]
    [ExtJSPropertyDescription("SelectedIndex", PropertyInitializeCode = "activeTab = <#= element.SelectedIndex #>",
                                                    PropertyChangeCode = "this.setActiveTab(<#= element.SelectedIndex #>);")]
    [ExtJSEventDescription("Click", ClientEventName = "<# empty #>")]
    [ExtJSEventDescription("Selecting", ClientEventName = "beforetabchange", ClientEventHandlerParameterNames = " view , newTab , oldTab , eOpts ", ClientHandlerCallbackData = " currentTabItem = newTab.title, prevTabItem = oldTab.title, index = view.items.indexOf(newTab)", ServerEventArgsCreateCode = "new TabViewCancelEventArgs( new TabItem(this.currentTabItem),new TabItem(this.prevTabItem),this.index.ToInt32(),false)")]
    [ExtJSEventDescription("Selected", ClientEventName = "tabchange", ClientEventHandlerParameterNames = " view , newTab , oldTab , eOpts ", ClientHandlerCallbackData = "prevTabItem = VT_GetTabTitle(oldTab)  , currentTabItem = newTab.title, index = view.items.indexOf(newTab)", ServerEventArgsCreateCode = "new TabControlEventArgs(new TabItem(this.currentTabItem), new TabItem(this.prevTabItem), this.index.ToInt32(), TabControlAction.Selected)")]
    public class TabControlElementExtJSRenderer : TabControlElementExtJSRendererBase
    {


        /// <summary>
        /// The is multi line tabs
        /// </summary>
        bool isMultiLineTabs = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabControlElementExtJSRenderer"/> class.
        /// </summary>
        public TabControlElementExtJSRenderer()
        {
        }

        /// <summary>
        /// Gets the ext js type.
        /// </summary>
        /// <value>
        /// The ext js type.
        /// </value>
        public override string ExtJSType
        {
            get
            {
                if (isMultiLineTabs)
                    return "VT.ux.MultiLineTabs";
                return "Ext.tab.Panel";
            }
        }

        /// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get
            {
                if (isMultiLineTabs)
                    return "multiline-tabs";

                return "tabpanel";
            }
        }



        /// <summary>
        /// Renders the layout properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderLayoutProperties(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {
            // Important to render the base layout properties first
            base.RenderLayoutProperties(renderingContext, controlElement, jsonWriter);

            TabElement tabControl = controlElement as TabElement;

            if (tabControl != null)
            {
                TabAlignment alignment = tabControl.Alignment;

                string position = alignment.ToString().ToLowerInvariant();

                RenderProperty(renderingContext, jsonWriter, GetTabPositionConfigName(), position);
            }
        }

        /// <summary>
        /// Gets the name of the tab position configuration.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetTabPositionConfigName()
        {
            return "tabPosition";
        }



        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            if (jsonWriter == null)
            {
                return;
            }

            base.RenderProperties(renderingContext, visualElement, jsonWriter);

            this.RenderRequiresConfig(jsonWriter);
        }

        /// <summary>
        /// Renders the TabElement Controls property change.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderControlsPropertyChange(ExtJSRenderingContext context, VisualElement visualElement, JavaScriptBuilder builder, string componentVariable)
        {
            // Get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // Render the collection change actions
                VisualElementExtJSCollectionRenderer.RenderCollectionChangeActions<VisualElementExtJSNonReversedCollectionRenderer>(context, controlElement.Controls, builder, componentVariable);
            }
        }

        /// <summary>
        /// Renders the requires configuration.
        /// </summary>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderRequiresConfig(JsonTextWriter jsonWriter)
        {
            if (isMultiLineTabs)
            {
                jsonWriter.WritePropertyName("requires");

                using (jsonWriter.OpenScope(RenderingUtils.JsonTextWriterScopeType.Array))
                {
                    jsonWriter.WriteValue("VT.ux.MultiLineTabs");
                }
            }
        }



        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Render tabs
            RenderPlainItemsProperty(renderingContext, ((TabElement)visualElement).Controls, jsonWriter);
        }




    }
}
