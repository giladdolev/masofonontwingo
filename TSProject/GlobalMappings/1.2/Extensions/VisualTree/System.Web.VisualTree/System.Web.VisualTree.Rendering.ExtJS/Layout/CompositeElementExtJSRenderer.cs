﻿using System.Web.VisualTree.Rendering.ExtJS.Common;
using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using System.Web.VisualTree.Common.Interfaces;


namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(CompositeElement), "PanelElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSEventDescription("Unloading", ClientEventName = "beforeremove", ClientEventHandlerParameterNames = " view , component, eOpts ")]
    [ExtJSMethodDescription("RemoveComposite", MethodCode = "this.destroy();")]
    public class CompositeElementExtJSRenderer : CompositeElementExtJSRendererBase, IVisualElementRendererApp
    {
        /// <summary>
        /// Renders the application.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="windowElement">The window element.</param>
        /// <param name="objWriter">The writer.</param>
        /// <param name="renderTo">The render to.</param>
        public virtual void RenderApp(RenderingContext renderingContext, IVisualElement visualElement, StringWriter objWriter, string renderTo)
        {
            CompositeElement compositeElement = visualElement != null ? visualElement as CompositeElement : null;

            if (renderingContext != null && renderingContext.SettingAlternativeMainWindow)
            {
                RenderInitialization(renderingContext, compositeElement, objWriter, renderTo);
            }
            else
            {
                objWriter.WriteLine("<script type=\"text/javascript\">");
                objWriter.WriteLine("Ext.onReady(function() {");

                // Call VT_onInitialize to hook events and such
                objWriter.WriteLine("VT_onInitialize();");

                // Initialize content
                RenderInitialization(renderingContext, compositeElement, objWriter, renderTo);

                // Call VT_onAfterInitialize to hook events and such after initialization
                objWriter.WriteLine("VT_onAfterInitialize();");

                objWriter.WriteLine("});");


                objWriter.WriteLine("</script>");
            }
        }



        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="windowElement">The window element.</param>
        /// <param name="objWriter">The object writer.</param>
        /// <param name="renderTo">The render to.</param>
        internal void RenderInitialization(RenderingContext renderingContext, CompositeElement compositeElement, StringWriter objWriter, string renderTo)
        {
            if (!string.IsNullOrEmpty(renderTo))
            {
                // Render internal panel
                objWriter.Write(string.Format(CultureInfo.InvariantCulture, "window.VT_RootComponent = Ext.create('{0}', ", this.ExtJSType));
            }
            else
            {
                // Render as viewport
                objWriter.Write(string.Format(CultureInfo.InvariantCulture, "window.VT_RootComponent = Ext.create('Ext.container.Viewport', "));
            }

            // Create the json writer
            // dispose by JsonTextWriter when call close, but call is missing, can use using for the JsonTextWriter, no wanning for fxcop for this.. 
            JsonTextWriter jsonWriter = JsonExtensions.CreateWriter(objWriter);

            // Write start object
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyName("renderTo");

            // If there is a valid render to
            if (!string.IsNullOrEmpty(renderTo))
            {
                // Get element to render to
                jsonWriter.WriteRawValue(string.Format(CultureInfo.InvariantCulture, "Ext.getElementById(\"{0}\")", renderTo));
            }
            else
            {
                // Render to body
                jsonWriter.WriteRawValue("Ext.getBody()");
            }

            if (compositeElement != null && compositeElement.RightToLeft == RightToLeft.Yes)
            {
                jsonWriter.WriteStartArrayProperty("requires");
                jsonWriter.WriteRawValue("'Ext.rtl.*'");
                jsonWriter.WriteEndArray();
            }

            // Render visual element properties
            this.RenderProperties(renderingContext, compositeElement, jsonWriter);

            // Write the end object
            jsonWriter.WriteEndObject();

            // Flush json writer
            jsonWriter.Flush();

            objWriter.WriteLine(");");
        }
    }
}
