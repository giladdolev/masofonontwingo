var panel = view.items.items[0];
var panel1 = view.items.items[1];
if (panel != null && panel1 !=null) {
    if (panel.region == 'west') {
        if (oldWidth) {
            if (oldWidth != width) {
                var relativeResize = (oldWidth / width) * 1.0;
                if (view.fixedPanel == 1 || view.fixedPanel == 0) {
                    view.items.items[2].setSize(width - panel, view.items.items[2].height);
                    this.savedWidth = view.items.items[2].getWidth() + panel1.getWidth() + 2
                }
                if (view.fixedPanel == 0) {
                    panel.setSize(panel.width / relativeResize, panel.height);
                }
                else if (view.fixedPanel == 2) {
                    panel.setSize(width - this.savedWidth, panel.height);
                }
            }
            else {
                this.savedWidth = view.items.items[2].getWidth() + panel1.getWidth() + 2
            }

        }
    }

    else {
        if (oldHeight) {
            if (oldHeight != height) {
                var relativeResize = (oldHeight / height) * 1.0;
                if (view.fixedPanel == 1 || view.fixedPanel == 0) {
                    view.items.items[2].setSize(view.items.items[2].getWidth(), height - panel);
                    this.savedHeight = view.items.items[2].getHeight() + panel1.getHeight() + 2
                }
                if (view.fixedPanel == 0) {
                    panel.setSize(panel.getWidth(), panel.getHeight() / relativeResize);
                }
                else if (view.fixedPanel == 2) {
                    console.log(this.savedHeight);
                    panel.setSize(panel.getWidth(), height - this.savedHeight);
                }
            }
            else {
                this.savedHeight = view.items.items[2].getHeight() + panel1.getHeight() + 2
            }

        }
    }
}