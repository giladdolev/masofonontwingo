using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.Web.VisualTree.Elements;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Touch;
using System.Web.VisualTree.Common;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Engine;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class WindowElementExtJSRendererBase : WindowElementBaseExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.Container";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "container";
			}
        }


		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
			// Renders the WindowClosing event attach code.
			RenderAttachWindowClosingEvent(context, visualElement, writer); 
			// Renders the Shown event attach code.
			RenderAttachShownEvent(context, visualElement, writer); 
			// Renders the Deactivate event attach code.
			RenderAttachDeactivateEvent(context, visualElement, writer); 
			// Renders the Activated event attach code.
			RenderAttachActivatedEvent(context, visualElement, writer); 
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
				case "WindowClosing":
					return this.ExtractWindowClosingCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Shown":
					return this.ExtractShownCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Deactivate":
					return this.ExtractDeactivateCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
				case "Activated":
					return this.ExtractActivatedCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


		/// <summary>
        /// Renders the style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			this.RenderBackColorProperty(context, visualElement, styleWriter);
		}


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "StartPosition":
					this.RenderStartPositionPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "TopMost":
					this.RenderTopMostPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "WindowState":
					this.RenderWindowStatePropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MaximizeBox":
					this.RenderMaximizeBoxPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "MinimizeBox":
					this.RenderMinimizeBoxPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "EnableEsc":
					this.RenderEnableEscPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BackColor":
					this.RenderBackColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Text":
					this.RenderTextPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "AutoScroll":
					this.RenderAutoScrollPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "ControlBox":
					this.RenderControlBoxPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Icon":
					this.RenderIconPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderStartPositionProperty(renderingContext, visualElement, jsonWriter);
			this.RenderTopMostProperty(renderingContext, visualElement, jsonWriter);
			this.RenderWindowStateProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMaximizeBoxProperty(renderingContext, visualElement, jsonWriter);
			this.RenderMinimizeBoxProperty(renderingContext, visualElement, jsonWriter);
			this.RenderEnableEscProperty(renderingContext, visualElement, jsonWriter);
			this.RenderAutoScrollProperty(renderingContext, visualElement, jsonWriter);
			this.RenderControlBoxProperty(renderingContext, visualElement, jsonWriter);
			this.RenderIconProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the WindowClosing event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachWindowClosingEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderWindowClosingEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseWindowClosingEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.WindowClosing
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "WindowClosing", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("beforeclose", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of WindowClosing event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseWindowClosingEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the WindowClosing event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderWindowClosingEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasWindowClosingListeners);
		}


		/// <summary>
        /// Extracts the WindowClosing event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractWindowClosingCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new WindowClosingEventArgs();
		}


		/// <summary>
        /// Renders the WindowClosing event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWindowClosingAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderWindowClosingEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseWindowClosingEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.WindowClosing
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "WindowClosing", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('beforeclose', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the WindowClosing event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWindowClosingRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderWindowClosingEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "beforeclose");
            }
        }

		/// <summary>
        /// Renders the Shown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachShownEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of Shown event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseShownEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Shown event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderShownEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasShownListeners);
		}


		/// <summary>
        /// Extracts the Shown event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractShownCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Shown event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderShownAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Shown event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderShownRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the Deactivate event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachDeactivateEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderDeactivateEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseDeactivateEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{}, 
									() => {  
										// Create the server callback code.Deactivate
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Deactivate", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("deactivate", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Deactivate event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseDeactivateEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Deactivate event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderDeactivateEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasDeactivateListeners);
		}


		/// <summary>
        /// Extracts the Deactivate event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractDeactivateCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Deactivate event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDeactivateAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

 
            // Get the typed element
            System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDeactivateEvent(context, element))
            {

                // The current event behavior
                var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

                // Revise the event behavior
                eventBehaviorType = this.ReviseDeactivateEventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        new string[]{}, 
                            () => {  
										// Create the server callback code.Deactivate
										
										jsBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "Deactivate", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('deactivate', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
        }

        /// <summary>
        /// Renders the Deactivate event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderDeactivateRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderDeactivateEvent(context, element))
            {
                javaScriptBuilder.AppendInvocationStatement("VT_RemoveListener", System.Web.VisualTree.Elements.VisualElement.GetElementId(visualElement), "deactivate");
            }
        }

		/// <summary>
        /// Renders the Activated event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAttachActivatedEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
		}


		/// <summary>
        /// Revise the event behavior type of Activated event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected virtual System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseActivatedEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Activated event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected virtual bool ShouldRenderActivatedEvent(RenderingContext context, System.Web.VisualTree.Elements.WindowElement element)
		{
			return (element.HasActivatedListeners);
		}


		/// <summary>
        /// Extracts the Activated event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected virtual EventArgs ExtractActivatedCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Activated event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderActivatedAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Activated event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderActivatedRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
        }

		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "WindowClosing":
					this.RenderWindowClosingAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Deactivate":
					this.RenderDeactivateAddListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Choose event
            switch (eventName)
            {
				 case "WindowClosing":
					this.RenderWindowClosingRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Deactivate":
					this.RenderDeactivateRemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
		/// <summary>
        /// Renders the StartPosition property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderStartPositionProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("defaultAlign",   WindowElementExtJSRenderer.ReviseStartPositionProperty(element) );
				}
			}

		}

		/// <summary>
        /// Renders the StartPosition property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderStartPositionPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the TopMost property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderTopMostProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If TopMost value is not default
					if (element.TopMost)
					{
						jsonWriter.WriteProperty("alwaysOnTop", element.TopMost);
					}
				}
			}

		}

		/// <summary>
        /// Renders the TopMost property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderTopMostPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setAlwaysOnTop({1});", new JavaScriptCode(componentVariable), JavaScriptCode.GetBool(element.TopMost));
			}

		}


		/// <summary>
        /// Renders the WindowState property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderWindowStateProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("maximized",  WindowElementExtJSRenderer.ReviseWindowStateProperty(element) );
				}
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("minimized",  WindowElementExtJSRenderer.ReviseWindowStateProperty(element) );
				}
			}

		}

		/// <summary>
        /// Renders the WindowState property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderWindowStatePropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the MaximizeBox property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMaximizeBoxProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("maximizable",  element.MaximizeBox );
				}
			}

		}

		/// <summary>
        /// Renders the MaximizeBox property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMaximizeBoxPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"if({1}){{
    {0}.tools.maximize.show();
}}else{{
    {0}.tools.maximize.hide();
}}", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.MaximizeBox) );
			}

		}


		/// <summary>
        /// Renders the MinimizeBox property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderMinimizeBoxProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("minimizable",  element.MinimizeBox );
				}
			}

		}

		/// <summary>
        /// Renders the MinimizeBox property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderMinimizeBoxPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"if({1}){{
    {0}.tools.minimize.show();
}}else{{
    {0}.tools.minimize.hide();
}}", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.MinimizeBox) );
			}

		}


		/// <summary>
        /// Renders the EnableEsc property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderEnableEscProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If EnableEsc value is not default
					if ( element.EnableEsc != true)
					{
						jsonWriter.WritePropertyWithRawValue("onEsc", @"Ext.emptyFn");
					}
				}
			}

		}

		/// <summary>
        /// Renders the EnableEsc property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderEnableEscPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the BackColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderBackColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If BackColor value is not default
					if (element.BackColor != Color.Empty)
					{
						jsonWriter.WriteProperty("backgroundColor",  ColorTranslator.ToHtml(element.BackColor) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the BackColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderBackColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('background-color','{1}');", new JavaScriptCode(componentVariable),  ColorTranslator.ToHtml(element.BackColor) );
			}

		}


		/// <summary>
        /// Renders the Width property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderWidthProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("width",  WindowElementExtJSRenderer.ReviseWidthProperty(element) );
				}
			}

		}

		/// <summary>
        /// Renders the Text property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.Text))
					{
						jsonWriter.WriteProperty("title",  RenderingUtils.NormalizeText(element.Text,"&") );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Text property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setTitle ('{1}');", new JavaScriptCode(componentVariable),  RenderingUtils.NormalizeText(element.Text,"&") );
			}

		}


		/// <summary>
        /// Renders the Height property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderHeightProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("height",  WindowElementExtJSRenderer.ReviseHeightProperty(element) );
				}
			}

		}

		/// <summary>
        /// Renders the Resizable property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderResizableProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("resizable",  WindowElementExtJSRenderer.ReviseResizable(element) );
				}
			}

		}

		/// <summary>
        /// Renders the AutoScroll property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderAutoScrollProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WritePropertyWithRawValue("autoScroll", @"false");
				}
			}

		}

		/// <summary>
        /// Renders the AutoScroll property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderAutoScrollPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


		/// <summary>
        /// Renders the ControlBox property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderControlBoxProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("closable",  element.ControlBox );
				}
			}

		}

		/// <summary>
        /// Renders the ControlBox property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderControlBoxPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"if({1}){{
    {0}.tools.close.show();
}}else{{
    {0}.tools.close.hide();
}}", new JavaScriptCode(componentVariable),  JavaScriptCode.GetBool(element.ControlBox) );
			}

		}


		/// <summary>
        /// Renders the Icon property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderIconProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If Icon value is not default
					if (element.Icon != null)
					{
						jsonWriter.WriteProperty("icon", element.Icon);
					}
				}
			}

		}

		/// <summary>
        /// Renders the Icon property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderIconPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setIcon('{1}');", new JavaScriptCode(componentVariable), (element.Icon.Source));
			}

		}


		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
				case "RaiseClosedEvent":
					return this.ExtractRaiseClosedEventMethodCallbackData(context, eventObject);						
				case "InvokeWindowAction":
					return this.ExtractInvokeWindowActionMethodCallbackData(context, eventObject);						
				case "Activate":
					return this.ExtractActivateMethodCallbackData(context, eventObject);						
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
				case "RaiseClosedEvent":
					this.RenderRaiseClosedEventMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "InvokeWindowAction":
					this.RenderInvokeWindowActionMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
				case "Activate":
					this.RenderActivateMethod(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


        /// Renders the RaiseClosedEvent method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderRaiseClosedEventMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"{0}.destroy();", new JavaScriptCode(componentVariableName));
				}
			}

		}


		/// <summary>
        /// Gets the method RaiseClosedEvent callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractRaiseClosedEventMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the InvokeWindowAction method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderInvokeWindowActionMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			
		}


		/// <summary>
        /// Gets the method InvokeWindowAction callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractInvokeWindowActionMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


        /// Renders the Activate method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        protected virtual void RenderActivateMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
						// Get typed element
			System.Web.VisualTree.Elements.WindowElement element = visualElement as System.Web.VisualTree.Elements.WindowElement;

			// If there is a valid typed element
			if(element != null)
			{
				var builder = getJavaScriptBuilder(true);
				if (builder != null) {
				System.Object data = (System.Object)methodData;
					builder.AppendFormattedRawValue(@"{0}.toFront();", new JavaScriptCode(componentVariableName));
				}
			}

		}


		/// <summary>
        /// Gets the method Activate callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected virtual object ExtractActivateMethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			return null;
		}


	}
}