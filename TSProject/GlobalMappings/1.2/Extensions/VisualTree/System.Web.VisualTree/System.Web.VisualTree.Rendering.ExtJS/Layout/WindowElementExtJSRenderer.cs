﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Drawing;
using System.Web.VisualTree.Elements;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Rendering.ExtJS.Touch;
using System.Web.VisualTree.Common;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.Rendering.ExtJS
{

    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(WindowElement), "WindowElementBaseExtJSRenderer", "container", "Ext.Container")]
    [ExtJSPropertyDescription("AutoScroll", PropertyInitializeCode = "autoScroll = false")]
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "Style.backgroundColor = <#= ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != Color.Empty", PropertyChangeCode = "this.setStyle('background-color','<#= ColorTranslator.ToHtml(element.BackColor) #>');")]
    [ExtJSPropertyDescription("ControlBox", PropertyInitializeCode = "closable = <#= element.ControlBox #>", PropertyChangeCode = "<#resource:ShowHideClosable.js#>")]
    [ExtJSPropertyDescription("EnableEsc", PropertyInitializeCode = "onEsc  = Ext.emptyFn", ClientNotDefaultCode = " element.EnableEsc != true")]
    [ExtJSPropertyDescription("Height", PropertyInitializeCode = "height = <#= WindowElementExtJSRenderer.ReviseHeightProperty(element) #>")]
    [ExtJSPropertyDescription("MaximizeBox", PropertyInitializeCode = "maximizable = <#= element.MaximizeBox #>", PropertyChangeCode = "<#resource:ShowHideMaximizeBox.js#>")]
    [ExtJSPropertyDescription("MinimizeBox", PropertyInitializeCode = "minimizable  = <#= element.MinimizeBox #>", PropertyChangeCode = "<#resource:ShowHideMinimizeBox.js#>")]
    [ExtJSPropertyDescription("Resizable", PropertyInitializeCode = "resizable = <#= WindowElementExtJSRenderer.ReviseResizable(element) #>")]
    [ExtJSPropertyDescription("StartPosition", PropertyInitializeCode = "defaultAlign = <#=  WindowElementExtJSRenderer.ReviseStartPositionProperty(element) #>")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "title = <#= RenderingUtils.NormalizeText(element.Text,\"&\") #>", PropertyChangeCode = "this.setTitle ('<#= RenderingUtils.NormalizeText(element.Text,\"&\") #>');")]
    [ExtJSPropertyDescription("TopMost", PropertyInitializeCode = "alwaysOnTop = <#=element.TopMost#>", PropertyChangeCode = "this.setAlwaysOnTop(<#=JavaScriptCode.GetBool(element.TopMost)#>);", ClientNotDefaultCode = "element.TopMost")]
    [ExtJSPropertyDescription("Width", PropertyInitializeCode = "width = <#= WindowElementExtJSRenderer.ReviseWidthProperty(element) #>")]
    [ExtJSPropertyDescription("WindowState", PropertyInitializeCode = "maximized  = <#= WindowElementExtJSRenderer.ReviseWindowStateProperty(element) #>, minimized  = <#= WindowElementExtJSRenderer.ReviseWindowStateProperty(element) #>")]
    [ExtJSPropertyDescription("EnableEsc", PropertyInitializeCode = "onEsc  = Ext.emptyFn", ClientNotDefaultCode = " element.EnableEsc != true")]
    [ExtJSPropertyDescription("Icon", PropertyInitializeCode = "icon = <#=element.Icon#>", PropertyChangeCode = "this.setIcon('<#=(element.Icon.Source)#>');", ClientNotDefaultCode = "element.Icon != null")]

    [ExtJSMethodDescription("InvokeWindowAction")]

    [ExtJSEventDescription("WindowClosed", ClientEventName = "close", ServerEventArgsCreateCode = "new WindowClosedEventArgs()")]

    [ExtJSMethodDescription("Activate", MethodCode = "this.toFront();")]
    [ExtJSMethodDescription("InvokeWindowAction")]
    [ExtJSMethodDescription("RaiseClosedEvent", MethodCode = "this.destroy();")]

    [ExtJSEventDescription("Deactivate", "deactivate")]
   
    [ExtJSEventDescription("WindowClosing", ClientEventName = "beforeclose", ServerEventArgsCreateCode = "new WindowClosingEventArgs()")]
    public class WindowElementExtJSRenderer : WindowElementExtJSRendererBase
    {
        /// <summary>
        /// Renders the identifier property.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderItemIdProperty(VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // Windows should have a global id
            RenderIdProperty(visualElement, "id", jsonWriter);
        }



        /// <summary>
        /// Renders the invoke window action method.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">Name of the component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder.</param>
        protected override void RenderInvokeWindowActionMethod(RenderingContext renderingContext, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, VisualElement visualElement, object methodData, string componentVariableName, Func<bool, JavaScriptBuilder> getJavaScriptBuilder)
        {
            // Get the java script builder
            JavaScriptBuilder builder = getJavaScriptBuilder(false);

            // If there is no a valid javascript builder
            if (builder == null)
            {
                return;
            }


            // Get window action type
            WindowActionType actionType = methodData != null ? (WindowActionType)methodData : WindowActionType.Open;

            // If is an open window action 
            if (actionType == WindowActionType.Open || actionType == WindowActionType.OpenModal)
            {

                // Create the JSON string writer
                Text.StringBuilder stringBuilder = new Text.StringBuilder();

                // Create the JSON writer
                using (JsonTextWriter jsonWriter = RenderingUtils.GetJsonWriter(stringBuilder))
                {

                    // Render window
                    this.Render(renderingContext, visualElement, jsonWriter);

                    // Flush content
                    jsonWriter.Flush();

                    string getOrCreateWindowCode = string.Format("VT_GetCmp('{0}') || Ext.create", VisualElement.GetElementId(visualElement));
                    // Add open window code
                    builder.AppendVariableWithInvocationInitializer("win", getOrCreateWindowCode, "widget.window", new JavaScriptCode(stringBuilder.ToString()));

                    // If is an open modal
                    if (actionType == WindowActionType.OpenModal)
                    {
                        // Set window as modal
                        builder.AppendAssignmentStatement("win.modal", true);
                    }

                    // Show the window
                    builder.AppendInvocationStatement("VT_ShowWindow", new JavaScriptCode("win"));
                }

            }
            else
            {
                // Get component and destroy it if valid.
                builder.AppendVariableWithInvocationInitializer("win", "VT_GetCmp", VisualElement.GetElementId(visualElement));
                builder.AppendInvocationStatement("if (win) win.destroy");
            }

        }


        /// <summary>
        /// Revises the renderer.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public override IVisualElementRenderer ReviseRenderer(RenderingContext renderingContext, IVisualElement visualElement)
        {
            // Get window element 
            WindowElement objWindowElement = visualElement as WindowElement;

            // If there is a valid window element
            if (objWindowElement != null)
            {
                // If is a window less window
                if (objWindowElement.IsWindowless)
                {
                    // Render windowless window
                    return WindowlessElementExtJSRenderer.Instance;
                }
            }
            return base.ReviseRenderer(renderingContext, visualElement);
        }


        /// <summary>
        /// Renders the height property.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static int ReviseHeightProperty(ControlElement controlElement)
        {
            // If height is valid
            if (controlElement.Height != Unit.Empty)
            {
                int height = controlElement.PixelHeight + 30;

                WindowElement windowElement = controlElement as WindowElement;

                if (windowElement != null)
                {
                    MenuElement mainMenu = windowElement.Menu;

                    if (mainMenu != null && mainMenu.Dock == Dock.Top)
                    {
                        height += 40;
                    }
                }

                return height;
            }

            return 0;
        }


        internal static string ReviseStartPositionProperty(ControlElement controlElement)
        {
            WindowElement element = controlElement as WindowElement;

            if (element == null || element.StartPosition == WindowStartPosition.WindowsDefaultLocation
                                || element.StartPosition == WindowStartPosition.WindowsDefaultBounds
                                || element.StartPosition == WindowStartPosition.Manual

                )
            {
                return "tl-bl";
            }

            return "c-c"; // default Center position
        }

        /// <summary>
        /// Renders the width property.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static int ReviseWidthProperty(ControlElement controlElement)
        {
            // If width is valid
            if (controlElement.Width != Unit.Empty)
            {
                return controlElement.PixelWidth + 15;
            }

            return 0;
        }

        /// <summary>
        /// Revises the resizable.
        /// </summary>
        /// <param name="windowElement">The control element.</param>
        /// <returns></returns>
        internal static bool ReviseResizable(WindowElement windowElement)
        {
            // If the window Resizable false .
            // return false . 
            if (windowElement.Resizable == false)
            {
                return false;
            }
            switch (windowElement.WindowBorderStyle)
            {
                case WindowBorderStyle.FixedSingle:
                case WindowBorderStyle.FixedDouble:
                case WindowBorderStyle.FixedDialog:
                case WindowBorderStyle.FixedToolWindow:
                    return false;
            }

            return true;
        }
        /// <summary>
        /// Revise the WindowState property
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <returns></returns>
        internal static string ReviseWindowStateProperty(ControlElement controlElement)
        {
            WindowElement windowElement = controlElement as WindowElement;
            if (windowElement != null)
            {
                switch (windowElement.WindowState)
                {
                    case WindowState.Maximized:
                        return "false";
                    case WindowState.Minimized:
                        return "true";
                    default:
                        return string.Empty;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext context, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            WindowElement windowElement = visualElement as WindowElement;
            if (windowElement != null)
            {
                List<ControlElement> controls = new List<ControlElement>();

                // Add main menu to window controls
                if (windowElement.Menu != null)
                {
                    // Add menu
                    controls.Add(windowElement.Menu);
                }

                // Add controls after menu
                controls.AddRange(windowElement.Controls);

                // If is a touch visual element
                if (this.IsTouchEnvironment)
                {
                    // Render plain items
                    RenderTouchControlsContentProperties(context, visualElement, jsonWriter, controls);
                }
                else
                {
                    // Render controls content properties
                    base.RenderContentProperties(context, visualElement, jsonWriter);
                }
            }
        }


        /// <summary>
        /// Renders the style property.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderStyleProperty(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            // validate parameter
            if (jsonWriter == null)
            {
                return;
            }

            base.RenderStyleProperty(renderingContext, visualElement, jsonWriter);

            // Get the window element
            WindowElement windowElement = visualElement as WindowElement;

            // write body style If there is a valid window element
            RenderingUtils.WriteBodyStyle(jsonWriter, windowElement);
        }
    }
}
