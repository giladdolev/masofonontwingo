using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class SplitContainerExtJSRendererBase : ControlElementExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.panel.Panel";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "panel";
			}
        }


		/// <summary>
        /// Renders the style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderStyleContentProperties(context, visualElement, styleWriter);

			// Get typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			this.RenderBackColorProperty(context, visualElement, styleWriter);
		}


		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            // Get typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid typed element
			if(element == null)
			{
				return;
			}

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
				 case "FixedPanel":
					this.RenderFixedPanelPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "BackColor":
					this.RenderBackColorPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
				 case "Layout":
					this.RenderLayoutPropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			this.RenderFixedPanelProperty(renderingContext, visualElement, jsonWriter);
			this.RenderLayoutProperty(renderingContext, visualElement, jsonWriter);
		}


		/// <summary>
        /// Renders the Resize event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachResizeEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderResizeEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseResizeEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "width", "height", "oldWidth", "oldHeight", "eOpts"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"var panel = view.items.items[0];
var panel1 = view.items.items[1];
if (panel != null && panel1 !=null) {{
    if (panel.region == 'west') {{
        if (oldWidth) {{
            if (oldWidth != width) {{
                var relativeResize = (oldWidth / width) * 1.0;
                if (view.fixedPanel == 1 || view.fixedPanel == 0) {{
                    view.items.items[2].setSize(width - panel, view.items.items[2].height);
                    this.savedWidth = view.items.items[2].getWidth() + panel1.getWidth() + 2
                }}
                if (view.fixedPanel == 0) {{
                    panel.setSize(panel.width / relativeResize, panel.height);
                }}
                else if (view.fixedPanel == 2) {{
                    panel.setSize(width - this.savedWidth, panel.height);
                }}
            }}
            else {{
                this.savedWidth = view.items.items[2].getWidth() + panel1.getWidth() + 2
            }}

        }}
    }}

    else {{
        if (oldHeight) {{
            if (oldHeight != height) {{
                var relativeResize = (oldHeight / height) * 1.0;
                if (view.fixedPanel == 1 || view.fixedPanel == 0) {{
                    view.items.items[2].setSize(view.items.items[2].getWidth(), height - panel);
                    this.savedHeight = view.items.items[2].getHeight() + panel1.getHeight() + 2
                }}
                if (view.fixedPanel == 0) {{
                    panel.setSize(panel.getWidth(), panel.getHeight() / relativeResize);
                }}
                else if (view.fixedPanel == 2) {{
                    console.log(this.savedHeight);
                    panel.setSize(panel.getWidth(), height - this.savedHeight);
                }}
            }}
            else {{
                this.savedHeight = view.items.items[2].getHeight() + panel1.getHeight() + 2
            }}

        }}
    }}
}}");
										javaScriptBuilder.AppendSemicolon();

										// Create the server callback code.Resize
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Resize", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{width : width , height: height}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("resize", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Resize event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseResizeEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Resize event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderResizeEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return (element.HasResizeListeners);
		}


		/// <summary>
        /// Extracts the Resize event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractResizeCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new ResizeEventArgs(args.Value<System.Int32>("width"),args.Value<System.Int32>("height"));
		}


		/// <summary>
        /// Renders the Resize event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderResizeAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Resize event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderResizeRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderResizeEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the Afterrender event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderAttachAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
 
			// Get the typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRenderAfterrenderEvent(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType.Immediate;

						// Revise the event behavior
						eventBehaviorType = this.ReviseAfterrenderEventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								new string[]{"view", "eOpts"}, 
									() => {  
										javaScriptBuilder.AppendFormattedRaw(@"this.savedWidth = this.items.items[2].getWidth() + this.items.items[1].getWidth() + 2;
this.savedHeight = this.items.items[2].getHeight() + this.items.items[1].getHeight() + 2;");
										javaScriptBuilder.AppendSemicolon();

										// Create the server callback code.Afterrender
										
										javaScriptBuilder.AppendInvocationStatement(GetClientEventCallbackMethod(eventBehaviorType), System.Web.VisualTree.Elements.VisualElement.GetElementId(element), "Afterrender", System.Web.VisualTree.Rendering.JavaScriptCode.Format(@"{{}}"));
									 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("afterlayout", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
		}


		/// <summary>
        /// Revise the event behavior type of Afterrender event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        protected override System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType ReviseAfterrenderEventBehavior(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the Afterrender event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        protected override bool ShouldRenderAfterrenderEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
		{
			return true;
		}


		/// <summary>
        /// Extracts the Afterrender event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		protected override EventArgs ExtractAfterrenderCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return new System.EventArgs();
		}


		/// <summary>
        /// Renders the Afterrender event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderAddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

        }

        /// <summary>
        /// Renders the Afterrender event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAfterrenderRemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
 
            // Get the typed element
            System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

            if(element != null && javaScriptBuilder != null && this.ShouldRenderAfterrenderEvent(context, element))
            {
                
            }
        }

		/// <summary>
        /// Renders the FixedPanel property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderFixedPanelProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WriteProperty("fixedPanel", Convert.ToInt32(element.FixedPanel));
				}
			}

		}

		/// <summary>
        /// Renders the FixedPanel property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderFixedPanelPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.fixedPanel = {1};", new JavaScriptCode(componentVariable), Convert.ToInt32(element.FixedPanel));
			}

		}


		/// <summary>
        /// Renders the BackColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderBackColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Style)
				{
					// If BackColor value is not default
					if (element.BackColor != Color.Empty)
					{
						jsonWriter.WriteProperty("background",  ColorTranslator.ToHtml(element.BackColor) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the BackColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderBackColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setStyle('background','{1}');", new JavaScriptCode(componentVariable),  ColorTranslator.ToHtml(element.BackColor) );
			}

		}


		/// <summary>
        /// Renders the Layout property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected virtual void RenderLayoutProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.SplitContainer element = visualElement as System.Web.VisualTree.Elements.SplitContainer;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					jsonWriter.WritePropertyWithRawValue("layout", @"""border""");
				}
			}

		}

		/// <summary>
        /// Renders the Layout property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected virtual void RenderLayoutPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			
		}


	}
}