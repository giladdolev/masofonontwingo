﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;
using System.Drawing;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(SplitContainer), "ControlElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSPropertyDescription("Layout", PropertyInitializeCode = "layout = \"border\" ")]
    [ExtJSPropertyDescription("FixedPanel", PropertyInitializeCode = "fixedPanel = <#=Convert.ToInt32(element.FixedPanel)#>", PropertyChangeCode = "this.fixedPanel = <#=Convert.ToInt32(element.FixedPanel)#>;")]
    [ExtJSEventDescription("Resize", ClientEventName = "resize", ClientEventHandlerParameterNames = "view, width, height, oldWidth, oldHeight, eOpts", ClientHandlerCallbackData = "width = width , height= height", ServerEventArgsCreateCode = "new ResizeEventArgs(this.width.ToInt32(),this.height.ToInt32())", ClientCode = "<#resource:Resize.js#>")]
    [ExtJSEventDescription("Afterrender", ClientEventName = "afterlayout", ClientEventHandlerParameterNames = " view , eOpts", ClientCode = "<#resource:SaveDimnesions.js#>",RenderAlways = true)] 
    [ExtJSPropertyDescription("BackColor", PropertyInitializeCode = "Style.background = <#= ColorTranslator.ToHtml(element.BackColor) #>", ClientNotDefaultCode = "element.BackColor != Color.Empty", PropertyChangeCode = "this.setStyle('background','<#= ColorTranslator.ToHtml(element.BackColor) #>');")]

    public class SplitContainerExtJSRenderer : SplitContainerExtJSRendererBase
    {
        /// <summary>
        /// Renders the content properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        public override void RenderContentProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            SplitContainer splitContainer = visualElement as SplitContainer;

            if (splitContainer != null)
            {
                // Render docked controls
                RenderControlsContentProperties(renderingContext, jsonWriter, new ControlElement[] { splitContainer.Panel1, splitContainer.Panel2 });
            }
        }

        /// <summary>
        /// Indicates if we should render the Resize event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        protected override bool ShouldRenderResizeEvent(RenderingContext context, System.Web.VisualTree.Elements.ControlElement element)
        {
            return true;
        }
    }
}
