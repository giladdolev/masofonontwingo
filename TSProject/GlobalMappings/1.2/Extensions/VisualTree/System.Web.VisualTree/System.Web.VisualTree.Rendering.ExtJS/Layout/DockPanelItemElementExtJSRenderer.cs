﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(DockPanelItemElement), "PanelElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSPropertyDescription("Closable", PropertyInitializeCode = "closable = true")]
    [ExtJSEventDescription("Closed", ClientEventName = "Close", ServerEventArgsCreateCode = "EventArgs.Empty")]
    public class DockPanelItemElementExtJSRenderer : DockPanelItemElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the header text property.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext context, VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            DockPanelItemElement dockPanelItemElement = visualElement as DockPanelItemElement;

            if (dockPanelItemElement != null)
            {
                jsonWriter.WriteProperty("title", dockPanelItemElement.Text);

            }

        }
    }
}
