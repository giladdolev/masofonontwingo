﻿using Newtonsoft.Json;
using System.ComponentModel.Composition;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(AccordionElement), "TabControlElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSEventDescription("SelectedIndexChanged", ClientEventName =  "itemExpanded", ClientEventHandlerParameterNames= "currentItemId", ClientHandlerCallbackData = "itemId=currentItemId", ServerEventArgsCreateCode = "new ValueChangedArgs<string>(this.itemId.ToString())")]
    public class AccordionElementExtJSRenderer : AccordionElementExtJSRendererBase
    { 
         
        public AccordionElementExtJSRenderer()
        {
            
        }

        public override void RenderProperties(RenderingContext renderingContext, VisualElement visualElement, JsonTextWriter jsonWriter)
        {
            jsonWriter.WritePropertyWithRawValue("defaults", "{listeners:{expand: function() {this.findParentByType('panel').fireEvent('itemExpanded', this.getItemId());}}}");
            base.RenderProperties(renderingContext, visualElement, jsonWriter);
        }

        /// <summary>
        /// Renders the layout properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="jsonWriter">The json writer.</param>
        protected override void RenderLayoutProperties(RenderingContext renderingContext, ControlElement controlElement, JsonTextWriter jsonWriter)
        {
            // Important to render the base layout properties first
            base.RenderLayoutProperties(renderingContext, controlElement, jsonWriter);

            jsonWriter.WriteProperty("layout", "accordion");
            jsonWriter.WritePropertyWithRawValue("layoutConfig", "{titleCollapse:true, animate:true, activeOnTop:true}");
            
        }


    }
}
