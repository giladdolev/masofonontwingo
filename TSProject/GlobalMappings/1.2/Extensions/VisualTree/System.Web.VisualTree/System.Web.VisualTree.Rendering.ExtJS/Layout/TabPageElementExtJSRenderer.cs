﻿using System.Runtime.InteropServices.ComTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.Rendering.ExtJS
{
    [Export(typeof(IVisualElementRenderer))]
    [ExtJSRendererDescription(typeof(TabItem), "PanelElementExtJSRenderer", "panel", "Ext.panel.Panel")]
    [ExtJSEventDescription("TabActivated", ClientEventName = "activate", ClientEventHandlerParameterNames = "view, eOpts", ClientHandlerCallbackData = "value: this.id", ServerEventArgsCreateCode = "EventArgs.Empty", ClientEventBehaviorType = ExtJSEventBehaviorType.Delayed)]
    [ExtJSPropertyDescription("Visible", PropertyInitializeCode = "hidden = <#= !element.Visible #>", ClientNotDefaultCode = "!element.Visible", PropertyChangeCode = "this.tab.setHidden(<#=JavaScriptCode.GetBool( !element.Visible )#>);")]
    [ExtJSPropertyDescription("Text", PropertyInitializeCode = "title= <#= RenderingUtils.NormalizeText(GetDisplayTextWithoutCommands(element.Text),\"&\") #>", PropertyChangeCode = "this.setTitle('<#= RenderingUtils.NormalizeText(GetDisplayTextWithoutCommands(element.Text),\"&\") #>');")]
    [ExtJSPropertyDescription("AutoScroll", PropertyInitializeCode = "scrollable = <#= element.AutoScroll #>", ClientNotDefaultCode = "element.AutoScroll")]
    public class TabPageElementExtJSRenderer : TabPageElementExtJSRendererBase
    {

        /// <summary>
        /// Renders the properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);

            //Get the tab item element 
            TabItem tabItem = visualElement as TabItem;
            if (tabItem != null)
            {
                // Get the parent element of tab item control
                TabElement tabElement = tabItem.ParentElement as TabElement;
                if (tabElement != null)
                {
                    ImageList imageList = tabElement.ImageList;

                    //check if the tab element have a imagelist 
                    if (imageList != null)
                    {
                        ImageElement img = null;

                        foreach (KeyItem key in imageList.Keys)
                        {
                            //To be sure we dont render items with no imageindex
                            if (tabItem.ImageIndex > -1)
                            {
                                if (key.ImageIndex == tabItem.ImageIndex)
                                {
                                    WriteHeaderProperties(jsonWriter, key.Key);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// rendere header property
        /// </summary>
        /// <param name="jsonWriter">jsonWriter</param>
        /// <param name="iconSource">icon path</param>
        private void WriteHeaderProperties(JsonTextWriter jsonWriter, string iconSource)
        {
            jsonWriter.WriteProperty("titlePosition", 0);
            jsonWriter.WriteProperty("icon", iconSource);
        }



        /// <summary>
        /// Renders the header object properties.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="headerWriter">The style writer.</param>
        public override void RenderHeaderProperties(RenderingContext renderingContext, VisualElement controlElement, JsonTextWriter headerWriter)
        {
            TabItem tab = controlElement as TabItem;
            if (tab != null)
            {
                if (headerWriter != null)
                {
                    if (tab.Image != null)
                    {
                        WriteHeaderProperties(headerWriter, tab.Image.Source);
                    }
                }
            }
        }



        /// <summary>
        /// Indicates if we should render the TabActivated event attach code.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The visual element.</param>
        /// <returns></returns>
        protected override bool ShouldRenderTabActivatedEvent(RenderingContext context, TabItem element)
        {
            // If is not the accordion element
            if (element.Parent is AccordionElement)
            {
                return false;
            }
            else
            {
                return base.ShouldRenderTabActivatedEvent(context, element);
            }
        }
    }
}
