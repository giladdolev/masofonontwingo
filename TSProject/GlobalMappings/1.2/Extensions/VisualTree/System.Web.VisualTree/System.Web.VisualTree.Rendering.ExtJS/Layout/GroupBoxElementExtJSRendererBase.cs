using Newtonsoft.Json;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Elements;
using System.Web.UI;
using System.ComponentModel.Composition;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.Rendering.ExtJS
{

	public class GroupBoxElementExtJSRendererBase : ScrollableControlExtJSRenderer
	{
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "Ext.form.FieldSet";
			}
        }


		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "fieldset";
			}
        }


		/// <summary>
        /// Renders the Font property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderFontProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the Font property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderFontPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GroupBoxElement element = visualElement as System.Web.VisualTree.Elements.GroupBoxElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setTitle('{1}');", new JavaScriptCode(componentVariable),  GroupBoxElementExtJSRenderer.ReviseTitle(element) );
			}

		}


		/// <summary>
        /// Renders the ForeColor property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderForeColorProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			
		}

		/// <summary>
        /// Renders the ForeColor property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderForeColorPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GroupBoxElement element = visualElement as System.Web.VisualTree.Elements.GroupBoxElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setTitle('{1}');", new JavaScriptCode(componentVariable),  GroupBoxElementExtJSRenderer.ReviseTitle(element) );
			}

		}


		/// <summary>
        /// Renders the Text property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        protected override void RenderTextProperty(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GroupBoxElement element = visualElement as System.Web.VisualTree.Elements.GroupBoxElement;

			// If there is a valid typed element
			if(element != null)
			{
				// Check rendering type
				if (renderingContext.RenderingType == ExtJSRenderingType.Property)
				{
					// If property is valid
					if(ValidateProperty(element.Text))
					{
						jsonWriter.WriteProperty("title",  GroupBoxElementExtJSRenderer.ReviseTitle(element) );
					}
				}
			}

		}

		/// <summary>
        /// Renders the Text property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderTextPropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			// Get typed element
			System.Web.VisualTree.Elements.GroupBoxElement element = visualElement as System.Web.VisualTree.Elements.GroupBoxElement;

			// If there is a valid typed element
			if(element != null)
			{
				builder.AppendFormattedRaw(@"{0}.setTitle('{1}');", new JavaScriptCode(componentVariable),  GroupBoxElementExtJSRenderer.ReviseTitle(element) );
			}

		}


	}
}