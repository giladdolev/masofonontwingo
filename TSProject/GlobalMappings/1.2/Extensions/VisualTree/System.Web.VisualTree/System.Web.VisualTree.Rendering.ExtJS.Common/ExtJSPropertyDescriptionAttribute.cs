﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    
    public class ExtJSPropertyDescriptionAttribute : ExtJSMemberDescriptionAttribute
    {
        /// <summary>
        /// The property change code
        /// </summary>
        private string _propertyChangeCode;

        /// <summary>
        /// The property initialize code
        /// </summary>
        private string _propertyInitializeCode;

        /// <summary>
        /// The client side default value check code
        /// </summary>
        private string _clientNotDefaultCode;

        /// <summary>
        /// Gets or sets the client default value.
        /// </summary>
        private object _clientDefault;

        /// <summary>
        /// The style initialize code
        /// </summary>
        private string _styleInitializeCode;        

        /// <summary>
        /// The client configuration name
        /// </summary>
        private string _clientConfigName;

        /// <summary>
        /// The client setter name
        /// </summary>
        private string _clientSetterName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <param name="propertyInitializeCode">The property initialize code.</param>
        /// <param name="propertyChangeCode">The property change code.</param>
        public ExtJSPropertyDescriptionAttribute(string propertyName, string propertyInitializeCode, string propertyChangeCode)
            : base(propertyName)
        {
            _propertyChangeCode = propertyChangeCode;
            _propertyInitializeCode = propertyInitializeCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <param name="propertyInitializeCode">The property initialize code.</param>        
        public ExtJSPropertyDescriptionAttribute(string propertyName, string propertyInitializeCode)
            : this(propertyName, propertyInitializeCode, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        public ExtJSPropertyDescriptionAttribute(string propertyName)
            : this(propertyName, null, null)
        {

        }

 
        /// <summary>
        /// Gets or sets the name of the client configuration.
        /// </summary>
        /// <value>
        /// The name of the client configuration.
        /// </value>
        public string ClientConfigName
        {
            get
            {
                return _clientConfigName;
            }
            set
            {
                _clientConfigName = value;
                this.PropertyInitializeCode = string.Format("{0}=<#=element.{1}#>", _clientConfigName, this.Name);
            }
        }

        /// <summary>
        /// Gets or sets the name of the client setter.
        /// </summary>
        /// <value>
        /// The name of the client setter.
        /// </value>
        public string ClientSetterName
        {
            get
            {
                return _clientSetterName;
            }
            set
            {
                _clientSetterName = value;
                this.PropertyChangeCode = string.Format("this.{0}(<#=element.{1}#>); ", _clientSetterName, this.Name);
            }
        }

        /// <summary>
        /// Gets or sets the client default value.
        /// </summary>
        /// <value>
        /// The client default.
        /// </value>
        public object ClientDefault
        {
            get { return _clientDefault; }
            set
            {
                _clientDefault = value;
                this.ClientNotDefaultCode = string.Format("element.{0} != {1}", this.Name, Utilities.CodeFromValue(_clientDefault));
            }
        }
        

        /// <summary>
        /// Gets the property change code.
        /// </summary>
        /// <value>
        /// The property change code.
        /// </value>
        public string PropertyChangeCode
        {
            get { return _propertyChangeCode; }
            set { _propertyChangeCode = value;  }
        }


        /// <summary>
        /// Gets the property initialize code.
        /// </summary>
        /// <value>
        /// The property initialize code.
        /// </value>
        public string PropertyInitializeCode
        {
            get { return _propertyInitializeCode; }
            set { _propertyInitializeCode = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is inherited; otherwise, <c>false</c>.
        /// </value>
        public bool IsInherited { get; set; }

        /// <summary>
        /// The client side default value check code
        /// </summary>
        /// <value>
        /// The client default code.
        /// </value>
        public string ClientNotDefaultCode
        {
            get { return _clientNotDefaultCode; }
            set { _clientNotDefaultCode = value; }
        }
        

        /// <summary>
        /// Gets or sets the style code.
        /// </summary>
        /// <value>
        /// The style code.
        /// </value>
        public string StyleCode 
        {
            get { return _styleInitializeCode; }
            set { _styleInitializeCode = value; }
        }
    }
}
