﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    internal class Utilities
    {
        /// <summary>
        /// Codes from value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal static object CodeFromValue(object value)
        {
            if (value == null)
            {
                return "null";
            }
            Type type = value.GetType();
            if (type == typeof(String))
            {
                return String.Format("\"{0}\"", value);
            }
            if (type.IsEnum)
            {
                return String.Format("{0}.{1}", type.FullName, value);
            }
            return value;
        }
    }
}
