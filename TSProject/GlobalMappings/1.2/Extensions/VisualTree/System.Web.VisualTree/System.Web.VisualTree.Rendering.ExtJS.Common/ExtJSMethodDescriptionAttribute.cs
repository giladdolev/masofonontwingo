﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    /// <summary>
    /// Provides support for rendering methods
    /// </summary>
    public class ExtJSMethodDescriptionAttribute : ExtJSMemberDescriptionAttribute    
    {
        /// <summary>
        /// The method code
        /// </summary>
        private string _methodCode;

        /// <summary>
        /// The method data type
        /// </summary>
        private Type _methodDataType;

        /// <summary>
        /// The method data type name
        /// </summary>
        private string _methodDataTypeName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSMethodDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="actionName">The method name.</param>
        public ExtJSMethodDescriptionAttribute(string actionName)
            : this(actionName, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSMethodDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="actionName">The method name.</param>
        public ExtJSMethodDescriptionAttribute(string actionName, string methodCode)
            : base(actionName)
        {
            this._methodCode = methodCode;
        }



        /// <summary>
        /// Gets or sets the method code.
        /// </summary>
        /// <value>
        /// The method code.
        /// </value>
        public string MethodCode
        {
            get { return _methodCode; }
            set { _methodCode = value; }
        }

        /// <summary>
        /// Gets or sets the method data type.
        /// </summary>
        /// <value>
        /// The method data type.
        /// </value>
        public Type MethodDataType
        {
            get
            {
                return _methodDataType;
            }
            set
            {
                _methodDataType = value;

                if(value != null)
                {
                    this.MethodDataTypeName = value.FullName;
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the method data type.
        /// </summary>
        /// <value>
        /// The name of the method data type.
        /// </value>
        public string MethodDataTypeName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_methodDataTypeName))
                {
                    return _methodDataTypeName;
                }
                else
                {
                    return "System.Object";
                }
            }
            set
            {
                _methodDataTypeName = value;
            }
        }

        /// <summary>
        /// Gets or sets the server callback data code.
        /// </summary>
        /// <value>
        /// The server callback data code.
        /// </value>
        public string ServerCallbackDataCode { get; set; }
    }
}
