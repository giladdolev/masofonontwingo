﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    /// <summary>
    /// 
    /// </summary>
    public enum ExtJSRenderingType
    {
        /// <summary>
        /// Render as property .
        /// </summary>
        Property,
        /// <summary>
        /// Render as style object .
        /// </summary>
        Style,
        /// <summary>
        /// Render as field style property .
        /// </summary>
        FieldStyle,
        /// <summary>
        /// Render as body style object .
        /// </summary>
        BodyStyle,
        /// <summary>
        /// Render as ViewConfig property object .
        /// </summary>
        ViewConfig
    }
}
