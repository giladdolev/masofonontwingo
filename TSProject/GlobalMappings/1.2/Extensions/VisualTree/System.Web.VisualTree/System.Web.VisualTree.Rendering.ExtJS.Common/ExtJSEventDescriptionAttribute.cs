﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    public class ExtJSEventDescriptionAttribute : ExtJSMemberDescriptionAttribute
    {
        /// <summary>
        /// The client event name
        /// </summary>
        private string _clientEventName;

        /// <summary>
        /// The client handler parameter names
        /// </summary>
        private string[] _clientHandlerParameterNames;

        /// <summary>
        /// The client handler callback data
        /// </summary>
        private string _clientHandlerCallbackData;

        /// <summary>
        /// The server event arguments type
        /// </summary>
        private string _serverEventArgsCreateCode;

        /// <summary>
        /// The client event behavior type
        /// </summary>
        private ExtJSEventBehaviorType _clientEventBehaviorType;

        /// <summary>
        /// The client event filter type
        /// </summary>
        private ExtJSEventFilterType _clientEventFilterType;


        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="eventName">The server event name.</param>
        /// <param name="clientEventName">The client event name.</param>
        /// <param name="clientHandlerParameterNames">The client handler parameter names.</param>
        /// <param name="clientHandlerCallbackData">The client handler callback data.</param>
        public ExtJSEventDescriptionAttribute(string eventName)
            : this(eventName, null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="eventName">The server event name.</param>
        /// <param name="clientEventName">The client event name.</param>
        /// <param name="clientHandlerParameterNames">The client handler parameter names.</param>
        /// <param name="clientHandlerCallbackData">The client handler callback data.</param>
        public ExtJSEventDescriptionAttribute(string eventName, string clientEventName)
            : this(eventName, clientEventName, null, null, null)
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="eventName">The server event name.</param>
        /// <param name="clientEventName">The client event name.</param>
        /// <param name="clientHandlerParameterNames">The client handler parameter names.</param>
        /// <param name="clientHandlerCallbackData">The client handler callback data.</param>
        public ExtJSEventDescriptionAttribute(string eventName, string clientEventName, string clientHandlerParameterNames)
            : this(eventName, clientEventName, clientHandlerParameterNames, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="eventName">The server event name.</param>
        /// <param name="clientEventName">The client event name.</param>
        /// <param name="clientHandlerParameterNames">The client handler parameter names.</param>
        /// <param name="clientHandlerCallbackData">The client handler callback data.</param>
        public ExtJSEventDescriptionAttribute(string eventName, string clientEventName, string clientHandlerParameterNames, string clientHandlerCallbackData)
            : this(eventName, clientEventName, clientHandlerParameterNames, clientHandlerCallbackData, null)
        {

        }

        

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSPropertyDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="eventName">The server event name.</param>
        /// <param name="clientEventName">The client event name.</param>
        /// <param name="clientHandlerParameterNames">The client handler parameter names.</param>
        /// <param name="clientHandlerCallbackData">The client handler callback data.</param>
        /// <param name="serverEventArgsCreateCode">The server event arguments create code.</param>
        public ExtJSEventDescriptionAttribute(string eventName, string clientEventName, string clientHandlerParameterNames, string clientHandlerCallbackData, string serverEventArgsCreateCode)
            : base(eventName)
        {
            // Set the client name
            this.ClientEventName = clientEventName;

            // Set client handler names
            this.ClientEventHandlerParameterNames = clientHandlerParameterNames;

            // Set client handler data
            this.ClientHandlerCallbackData = clientHandlerCallbackData;

            // Set the server event args create code
            this.ServerEventArgsCreateCode = serverEventArgsCreateCode;
        }





        /// <summary>
        /// Gets or sets the type of the client event behavior.
        /// </summary>
        /// <value>
        /// The type of the client event behavior.
        /// </value>
        public ExtJSEventBehaviorType ClientEventBehaviorType
        {
            get { return _clientEventBehaviorType; }
            set { _clientEventBehaviorType = value; }
        }





        /// <summary>
        /// Gets or sets the type of the client event filter.
        /// </summary>
        /// <value>
        /// The type of the client event filter.
        /// </value>
        public ExtJSEventFilterType ClientEventFilterType
        {
            get { return _clientEventFilterType; }
            set { _clientEventFilterType = value; }
        }





        /// <summary>
        /// Gets the type of the server event create code.
        /// </summary>
        /// <value>
        /// The type of the server event create code.
        /// </value>
        public string ServerEventArgsCreateCode
        {
            get 
            { 
                return _serverEventArgsCreateCode; 
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this._serverEventArgsCreateCode = value;
                }
                else
                {
                    this._serverEventArgsCreateCode = "new System.EventArgs()";
                }
            }
        } 





        /// <summary>
        /// Gets the client event name.
        /// </summary>
        /// <value>
        /// The client event name.
        /// </value>
        public string ClientEventName
        {
            get
            {
                return _clientEventName;
            }
            set
            {
                _clientEventName = value;
            }
        }





        /// <summary>
        /// Gets the client event handler parameter names.
        /// </summary>
        /// <value>
        /// The client event handler parameter names.
        /// </value>
        public IEnumerable<string> ClientEventHandlerParameters
        {
            get
            {
                return _clientHandlerParameterNames;
            }        
        }




        /// <summary>
        /// Gets or sets the client event handler parameter names.
        /// </summary>
        /// <value>
        /// The client event handler parameter names.
        /// </value>
        public string ClientEventHandlerParameterNames
        {
            get
            {
                return string.Join(",",  _clientHandlerParameterNames.ToArray());
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this._clientHandlerParameterNames = value.Split(new char[] { ' ', ',', '\'', '"' }, StringSplitOptions.RemoveEmptyEntries);
                }
                else
                {
                    this._clientHandlerParameterNames = new string[] { };
                }
            }
        }




        /// <summary>
        /// Gets the client handler callback data.
        /// </summary>
        /// <value>
        /// The client handler callback data.
        /// </value>
        public string ClientHandlerCallbackData
        {
            get 
            { 
                return _clientHandlerCallbackData; 
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    // TODO: GENERATOR-TOREVIEW (should be more robust algorithm that replaces only specific equals)
                    this._clientHandlerCallbackData = value.Replace('=', ':');
                }
                else
                {
                    this._clientHandlerCallbackData = string.Empty;
                }
            }
        }

        /// <summary>
        /// Client code property
        /// </summary>
        public string ClientCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether render always.
        /// </summary>
        /// <value>
        ///   <c>true</c> if render always; otherwise, <c>false</c>.
        /// </value>
        public bool RenderAlways { get; set; }
    }




    /// <summary>
    /// Indicates when the event is populated.
    /// </summary>
    public enum ExtJSEventBehaviorType
    {
        /// <summary>
        /// The immediate
        /// </summary>
        Immediate,

        /// <summary>
        /// The queued
        /// </summary>
        Queued,

        /// <summary>
        /// The delayed
        /// </summary>
        Delayed,
    }




    /// <summary>
    /// Indicates what kind of events should be filtered out.
    /// </summary>
    public enum ExtJSEventFilterType
    {
        /// <summary>
        /// The none
        /// </summary>
        None,

        /// <summary>
        /// The filter doubles
        /// </summary>
        FilterDoubles
    }
}

