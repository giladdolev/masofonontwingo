﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    public class ExtJSRenderingContext : RenderingContext
    {
        private ExtJSRenderingType _renderingType = ExtJSRenderingType.Property;

        public ExtJSRenderingType RenderingType
        {
            get { return _renderingType; }
            set { _renderingType = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRenderingContext"/> class.
        /// </summary>
        /// <param name="renderingEnvironment">The rendering environment.</param>
        /// <param name="isDesignTime">if set to <c>true</c> is design time.</param>
        public ExtJSRenderingContext(RenderingEnvironment renderingEnvironment, bool isDesignTime)
            : this(renderingEnvironment, isDesignTime, null, null)
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRenderingContext"/> class.
        /// </summary>
        /// <param name="renderingEnvironment">The rendering environment.</param>
        /// <param name="isDesignTime">if set to <c>true</c> is design time.</param>
        /// <param name="rootSite">The root site.</param>
        /// <param name="rootSiteAssemblyReferences">The root site assembly references.</param>
        public ExtJSRenderingContext(RenderingEnvironment renderingEnvironment, bool isDesignTime, string rootSite, HashSet<Assembly> rootSiteAssemblyReferences)
            : base(renderingEnvironment, isDesignTime, rootSite, rootSiteAssemblyReferences)
        {

        }

        /// <summary>
        /// Sets the context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="renderingType">Type of the rendering.</param>
        /// <returns></returns>
        public static ExtJSRenderingContext SetContext(RenderingContext context, ExtJSRenderingType renderingType)
        {
            ExtJSRenderingContext wrappedContext = context as ExtJSRenderingContext;
            if (wrappedContext == null && context != null)
            {
                wrappedContext = new ExtJSRenderingContext(context.Environment, context.IsDesignTime, context.RootSite, context.RootSiteAssemblyReferences);
            }
            if (wrappedContext != null)
            {
                wrappedContext.RenderingType = renderingType;

            }
            return wrappedContext;
        }
    }
}
