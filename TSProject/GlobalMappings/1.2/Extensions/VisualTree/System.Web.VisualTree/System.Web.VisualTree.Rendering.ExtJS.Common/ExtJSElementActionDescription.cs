﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    public class ExtJSElementActionDescription : ExtJSPropertyDescriptionAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSElementActionDescription" /> class.
        /// </summary>
        /// <param name="actionName">The action name.</param>
        /// <param name="actionCode">The action code.</param>
        public ExtJSElementActionDescription(string actionName, string actionCode)
            : base(string.Concat("$", actionName), null, actionCode)
        {

        }
    }
}
