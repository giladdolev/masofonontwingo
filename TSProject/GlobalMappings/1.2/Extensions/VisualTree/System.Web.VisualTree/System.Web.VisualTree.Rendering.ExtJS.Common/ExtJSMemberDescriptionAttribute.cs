﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ExtJSMemberDescriptionAttribute : Attribute
    {
        /// <summary>
        /// The name
        /// </summary>
        private readonly string _name;



        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSMemberDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public ExtJSMemberDescriptionAttribute(string name)
        {
            this._name = name;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return _name; }
        } 

    }
}
