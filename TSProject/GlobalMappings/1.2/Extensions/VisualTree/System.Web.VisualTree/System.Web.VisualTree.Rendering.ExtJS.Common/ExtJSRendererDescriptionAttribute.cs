﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS;

namespace System.Web.VisualTree.Rendering.ExtJS.Common
{
    /// <summary>
    /// Provides support for registering ExtJS renderers
    /// </summary>
    public class ExtJSRendererDescriptionAttribute : RendererDescriptionAttribute
    {
        /// <summary>
        /// The XTYPE
        /// </summary>
        private readonly string _xtype;


        /// <summary>
        /// The EXT JS type
        /// </summary>
        private readonly string _extJsType;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="name">The description.</param>
        public ExtJSRendererDescriptionAttribute(Type visualElementType, string rendererBaseType, string xtype, string extJsType)
            : base(visualElementType, RenderingEnvironmentId.ExtJS, rendererBaseType, null)
        {
            _xtype = xtype;
            _extJsType = extJsType;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="name">The description.</param>
        public ExtJSRendererDescriptionAttribute(Type visualElementType, Type rendererBaseType, string xtype, string extJsType)
            : base(visualElementType, RenderingEnvironmentId.ExtJS, GetTypeFullName(rendererBaseType), null)
        {
            _xtype = xtype;
            _extJsType = extJsType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="name">The description.</param>
        public ExtJSRendererDescriptionAttribute(Type visualElementType, string xtype, string extJsType)
            : base(visualElementType, RenderingEnvironmentId.ExtJS, "System.Web.VisualTree.Rendering.ExtJS.VisualElementExtJSRenderer", null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="renderingEnvironmentId">The rendering environment identifier.</param>
        /// <param name="rendererBaseType">Type of the renderer base.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <param name="extJsType">The ext JS type.</param>
        protected ExtJSRendererDescriptionAttribute(Type visualElementType, RenderingEnvironmentId renderingEnvironmentId, string rendererBaseType, string xtype, string extJsType)
            : base(visualElementType, renderingEnvironmentId, rendererBaseType, null)
        {
            _xtype = xtype;
            _extJsType = extJsType;
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="renderingEnvironmentId">The rendering environment identifier.</param>
        /// <param name="rendererBaseType">Type of the renderer base.</param>
        /// <param name="xtype">The XTYPE.</param>
        /// <param name="extJsType">The ext JS type.</param>
        protected ExtJSRendererDescriptionAttribute(Type visualElementType, RenderingEnvironmentId renderingEnvironmentId, Type rendererBaseType, string xtype, string extJsType)
            : base(visualElementType, renderingEnvironmentId, GetTypeFullName(rendererBaseType), null)
        {
            _xtype = xtype;
            _extJsType = extJsType;
        }



        /// <summary>
        /// Gets the full name of the type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private static string GetTypeFullName(Type type)
        {
            if (type != null)
            {
                return type.FullName;
            }

            return null;
        }





        /// <summary>
        /// Gets the XType
        /// </summary>
        /// <value>
        /// The XType
        /// </value>
        public string XType
        {
            get { return _xtype; }
        }

        /// <summary>
        /// Gets the EXTJS type
        /// </summary>
        /// <value>
        /// The EXTJS type
        /// </value>
        public string ExtJsType
        {
            get { return _extJsType; }
        }


    }


    /// <summary>
    /// Provides support for registering ExtJS touch renderers
    /// </summary>
    public class ExtJSTouchRendererDescriptionAttribute : ExtJSRendererDescriptionAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSTouchRendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="rendererBaseType">The renderer base type.</param>
        /// <param name="xtype">The XTYPE</param>
        /// <param name="extJsType">The Ext JS type</param>
        public ExtJSTouchRendererDescriptionAttribute(Type visualElementType, string rendererBaseType, string xtype, string extJsType)
            : base(visualElementType, RenderingEnvironmentId.ExtJSTouch, rendererBaseType, xtype, extJsType)
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSTouchRendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="rendererBaseType">The renderer base type.</param>
        /// <param name="xtype">The XTYPE</param>
        /// <param name="extJsType">The Ext JS type</param>
        public ExtJSTouchRendererDescriptionAttribute(Type visualElementType, Type rendererBaseType, string xtype, string extJsType)
            : base(visualElementType, RenderingEnvironmentId.ExtJSTouch, rendererBaseType, xtype, extJsType)
        {

        }
    }
}
