﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;


namespace System.Web.VisualTree
{
    public static class ResourcesExtensions
    {
        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <returns></returns>
        public static ActionResult GetResult(this ResourceReference reference)
        {
            ImageReference imageReference = reference as ImageReference;
            if (imageReference != null)
            {
                return imageReference.GetResult();
            }
            IconReference iconReference = reference as IconReference;
            if (iconReference != null)
            {
                return iconReference.GetResult();
            }
            return null;
        }
        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <returns></returns>
        public static ActionResult GetResult(this IconReference reference)
        {
            Icon _icon = reference;
            // If there is a valid image
            if (_icon != null)
            {
                // Get stream from image
                MemoryStream stream = new MemoryStream();
                _icon.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);

                // Return file stream result
                return new FileStreamResult(stream, "image/icon");
            }

            // Return empty result
            return new EmptyResult();
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <returns></returns>
        public static ActionResult GetResult(this ImageReference reference)
        {
            Image _image = reference;

            // If there is a valid image
            if (_image != null)
            {
                // Get stream from image
                MemoryStream stream = new MemoryStream();
                _image.Save(stream, ImageFormat.Png);
                stream.Seek(0, SeekOrigin.Begin);

                // Return file stream result
                return new FileStreamResult(stream, "image/gif");
            }

            // Return empty result
            return new EmptyResult();
        }
    }
}
