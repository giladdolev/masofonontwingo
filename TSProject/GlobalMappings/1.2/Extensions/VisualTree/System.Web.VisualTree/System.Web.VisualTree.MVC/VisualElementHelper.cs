﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.VisualTree.Elements;
using System.Diagnostics;
using System.Web.UI;
using System.Reflection;
using System.ComponentModel;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.MVC
{
    public class VisualElementHelper
    {
        /// <summary>
        /// Reload view.
        /// </summary>
        /// <typeparam name="TElementType">The element type.</typeparam>
        /// <param name="controllerName">The controller name.</param>
        /// <exception cref="System.NullReferenceException">Invalid http context.
        /// or
        /// Invalid http response.
        /// or
        /// Invalid http request.</exception>
        public static void ReloadView<TElementType>(TElementType viewModel, string controllerName)
            where TElementType : VisualElement
        {
            ReloadView<TElementType>(viewModel, controllerName, null, null);
        }

        /// <summary>
        /// Reload view.
        /// </summary>
        /// <typeparam name="TElementType">The element type.</typeparam>
        /// <param name="controllerName">The controller name.</param>
        /// <param name="actionName">The view name.</param>
        /// <exception cref="System.NullReferenceException">Invalid http context.
        /// or
        /// Invalid http response.
        /// or
        /// Invalid http request.</exception>
        public static void ReloadView<TElementType>(TElementType viewModel, string controllerName, string actionName)
            where TElementType : VisualElement
        {
            ReloadView<TElementType>(viewModel, controllerName, actionName, null);
        }

        /// <summary>
        /// Reload view.
        /// </summary>
        /// <typeparam name="TElementType">The element type.</typeparam>
        /// <param name="controllerName">The controller name.</param>
        /// <param name="actionName">The view name.</param>
        /// <exception cref="System.ArgumentNullException">controllerName</exception>
        /// <exception cref="System.NullReferenceException">Invalid http context.
        /// or
        /// Invalid http response.
        /// or
        /// Invalid http request.</exception>
        public static void ReloadView<TElementType>(TElementType viewModel, string controllerName, string actionName, string area)
            where TElementType : VisualElement
        {
            try
            {
                // If there is a invalid controller name
                if (controllerName == null)
                {
                    throw new ArgumentNullException("controllerName");
                }

                // If there is an invalid action name
                if (string.IsNullOrWhiteSpace(actionName))
                {
                    // Set the controller name as the action name.
                    actionName = controllerName;
                }

                // Get HTTP context
                HttpContext httpContext = RenderingContext.CurrentHttpContext;

                // Ensure valid http context
                if (httpContext == null)
                {
                    throw new NullReferenceException("Invalid http context.");
                }

                // Get HTTP response
                HttpResponse httpResponse = httpContext.Response;

                // Ensure valid http response
                if (httpResponse == null)
                {
                    throw new NullReferenceException("Invalid http response.");
                }

                // Get HTTP request
                HttpRequest httpRequest = httpContext.Request;

                // Ensure valid http request
                if (httpRequest == null)
                {
                    throw new NullReferenceException("Invalid http request.");
                }

                // Get the current controller builder
                ControllerBuilder _controllerBuilder = ControllerBuilder.Current;

                // If there is a valid controller builder
                if (_controllerBuilder != null)
                {
                    // Get the controller factory
                    IControllerFactory _controllerFactory = _controllerBuilder.GetControllerFactory();

                    // If there is a valid controller factory
                    if (_controllerFactory != null)
                    {
                        // Get route data
                        RouteData routeData = GetRouteData(controllerName, actionName, area, new Dictionary<string, object>());

                        // Create temp request context
                        RequestContext tempRequestContext = new RequestContext(httpRequest.RequestContext.HttpContext, routeData);

                        // Get controller instance from name
                        Controller controllerInstance = _controllerFactory.CreateController(tempRequestContext, controllerName) as Controller;

                        // If there is a valid control instance
                        if (controllerInstance != null)
                        {

                            // Store the original output writer
                            TextWriter originalOutputWriter = httpResponse.Output;

                            try
                            {
                                // Ensure current context
                                RenderingContext.EnsureHttpContext();

                                // Create dummy writer
                                httpResponse.Output = new StringWriter(CultureInfo.InvariantCulture);

                                // Create controller context
                                ControllerContext controllerContext = new ControllerContext(httpRequest.RequestContext.HttpContext, routeData, controllerInstance);

                                // Set the value provider
                                controllerInstance.ValueProvider = new RouteDataValueProvider(controllerContext);

                                // Indicate create view mode
                                controllerInstance.TempData[RenderingConstants.VT_CREATE_VIEW] = true;

                                // Set the view model
                                controllerInstance.ViewData.Model = viewModel;

                                // Create view result
                                ViewResult viewResult = new ViewResult()
                                {
                                    ViewName = null,
                                    MasterName = null,
                                    ViewData = controllerInstance.ViewData,
                                    TempData = controllerInstance.TempData,
                                    ViewEngineCollection = controllerInstance.ViewEngineCollection
                                };

                                // Get control element model from view model
                                ControlElement controlModel = viewModel as ControlElement;

                                // If there is a valid control
                                if(controlModel != null)
                                {
                                    // Clear control
                                    controlModel.Controls.Clear();
                                }

                                // Execute result to reload view
                                viewResult.ExecuteResult(controllerContext);
                            }
                            finally
                            {
                                // Restore the original writer
                                httpResponse.Output = originalOutputWriter;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new VisualElementActionException(String.Format("Failed to reload view for '{0}' action of controller '{1}'.", actionName, controllerName), ex);
            }
        }

        /// <summary>
        /// Creates element from view.
        /// </summary>
        /// <typeparam name="TElementType">The element type.</typeparam>
        /// <param name="controllerName">The controller name.</param>
        /// <param name="actionName">The view name.</param>
        /// <param name="area">The area name.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="properties">The properties.</param>
        /// <returns>
        /// An instance of the element required.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">controllerName</exception>
        /// <exception cref="System.NullReferenceException">Invalid http context.
        /// or
        /// Invalid http response.
        /// or
        /// Invalid http request.</exception>
        public static TElementType CreateFromView<TElementType>(string controllerName, string actionName,string area, IDictionary<string, object> arguments, IDictionary<string, object> properties)
            where TElementType : VisualElement
        {
            try
            {
                // If there is a invalid controller name
                if (controllerName == null)
                {
                    throw new ArgumentNullException("controllerName");
                }

                // If there is an invalid action name
                if (string.IsNullOrWhiteSpace(actionName))
                {
                    // Set the controller name as the action name.
                    actionName = controllerName;
                }

                // Get HTTP context
                HttpContext httpContext = RenderingContext.CurrentHttpContext;

                // Ensure valid http context
                if (httpContext == null)
                {
                    throw new NullReferenceException("Invalid http context.");
                }

                // Get HTTP response
                HttpResponse httpResponse = httpContext.Response;

                // Ensure valid http response
                if (httpResponse == null)
                {
                    throw new NullReferenceException("Invalid http response.");
                }

                // Get HTTP request
                HttpRequest httpRequest = httpContext.Request;

                // Ensure valid http request
                if (httpRequest == null)
                {
                    throw new NullReferenceException("Invalid http request.");
                }




                // Get the current controller builder
                ControllerBuilder _controllerBuilder = ControllerBuilder.Current;

                // If there is a valid controller builder
                if (_controllerBuilder != null)
                {
                    // Get the controller factory
                    IControllerFactory _controllerFactory = _controllerBuilder.GetControllerFactory();

                    // If there is a valid controller factory
                    if (_controllerFactory != null)
                    {
                        // Get route data
                        RouteData routeData = GetRouteData(controllerName, actionName, area, arguments);

                        RequestContext tempRequestContext = new RequestContext(httpRequest.RequestContext.HttpContext, routeData);

                        // Get controller instance from name
                        Controller controllerInstance = _controllerFactory.CreateController(tempRequestContext, controllerName) as Controller;

                        // If there is a valid control instance
                        if (controllerInstance != null)
                        {
                            // Get action invoker
                            IActionInvoker actionInvoker = controllerInstance.ActionInvoker;

                            // If there is a valid action invoker
                            if (actionInvoker != null)
                            {
                                // Store the original output writer
                                TextWriter originalOutputWriter = httpResponse.Output;

                                try
                                {
                                    // Ensure current context
                                    RenderingContext.EnsureHttpContext();

                                    // Create dummy writer
                                    httpResponse.Output = new StringWriter(CultureInfo.InvariantCulture);
                                    
                                    // Create controller context
                                    ControllerContext controllerContext = new ControllerContext(httpRequest.RequestContext.HttpContext, routeData, controllerInstance);

                                    // Set the value provider
                                    controllerInstance.ValueProvider = new RouteDataValueProvider(controllerContext);

                                    // Indicate create view mode
                                    controllerInstance.TempData[RenderingConstants.VT_CREATE_VIEW] = true;

                                    // Invoke the action and get the _vt_container
                                    if (actionInvoker.InvokeAction(controllerContext, actionName))
                                    {
                                        // If there are valid properties 
                                        if (properties != null && properties.Count > 0)
                                        {
                                            // Get the control from the controller instance
                                            Control control = controllerInstance.TempData[RenderingConstants.VT_CONTAINER_CONTROL] as Control;

                                            // If there is a valid control
                                            if (control != null)
                                            {
                                                // Get the control type
                                                Type controlType = control.GetType();

                                                // Loop all control properties
                                                foreach (KeyValuePair<string, object> propertyEntry in properties)
                                                {
                                                    // Get property info
                                                    PropertyInfo propertyInfo = controlType.GetProperty(propertyEntry.Key);

                                                    // If there is a valid property info
                                                    if (propertyInfo != null)
                                                    {
                                                        try
                                                        {
                                                            // Get type converter
                                                            TypeConverter typeConverter = TypeDescriptor.GetConverter(propertyInfo.PropertyType);

                                                            // If there is a valid type converter
                                                            if (typeConverter != null)
                                                            {
                                                                // Check if we can convert from string
                                                                if (typeConverter.CanConvertFrom(typeof(string)))
                                                                {
                                                                    // Get property value
                                                                    object propertyValue = typeConverter.ConvertFromString(propertyEntry.Value as string);

                                                                    // Set the property value
                                                                    propertyInfo.SetValue(control, propertyValue);
                                                                }
                                                            }

                                                        }
                                                        catch (Exception exception)
                                                        {
                                                            // TODO: LOG???
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        return controllerInstance.TempData[RenderingConstants.VT_CONTAINER] as TElementType;
                                    }
                                }
                                finally
                                {
                                    // Restore the original writer
                                    httpResponse.Output = originalOutputWriter;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new VisualElementActionException(String.Format("Failed to create view for '{0}' action of controller '{1}'.", actionName, controllerName), ex);
            }


            return null;
        }

        private static RouteData GetRouteData(string controllerName, string actionName, string area, IDictionary<string, object> arguments)
        {
            // Create the rout data
            RouteData routeData = new RouteData();
            routeData.Values["controller"] = controllerName;
            routeData.Values["action"] = actionName;
            if (area != null)
            {
                routeData.DataTokens["area"] = area;
            }

            // If there is a valid arguments dictionary
            if (arguments != null)
            {
                // Loop all arguments
                foreach (KeyValuePair<string, object> argument in arguments)
                {
                    // Set the rout data values
                    routeData.Values[argument.Key] = argument.Value;
                }
            }
            return routeData;
        }


        /// <summary>
        /// Creates element from view.
        /// </summary>
        /// <typeparam name="TElementType">The element type.</typeparam>
        /// <param name="controllerName">The controller name.</param>
        /// <param name="area">The area.</param>
        /// <param name="arguments">The arguments.</param>        
        /// <param name="properties">The properties.</param>
        /// <returns>
        /// An instance of the element required.
        /// </returns>
        public static TElementType CreateFromView<TElementType>(string controllerName,string area, IDictionary<string, object> arguments, IDictionary<string, object> properties)
            where TElementType : VisualElement
        {
            return CreateFromView<TElementType>(controllerName, null,area, arguments, properties);
        }



        /// <summary>
        /// Creates from view.
        /// </summary>
        /// <typeparam name="TElementType">The type of the element type.</typeparam>
        /// <param name="area">The area.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="properties">The properties.</param>
        /// <returns></returns>
        public static TElementType CreateFromView<TElementType>(string area, IDictionary<string, object> arguments, IDictionary<string, object> properties)
            where TElementType : VisualElement
        {
            return CreateFromView<TElementType>(typeof(TElementType).Name,area, arguments, properties);
        }


        /// <summary>
        /// Creates from view.
        /// </summary>
        /// <typeparam name="TElementType">The type of the element type.</typeparam>
        /// <param name="controllerName">Name of the controller.</param>
        /// <returns></returns>
        public static TElementType CreateFromView<TElementType>(string controllerName)
             where TElementType : VisualElement
        {
            return CreateFromView<TElementType>(controllerName, controllerName);
        }


        /// <summary>
        /// Creates from view.
        /// </summary>
        /// <typeparam name="TElementType">The type of the element type.</typeparam>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <returns></returns>
        public static TElementType CreateFromView<TElementType>(string controllerName, string actionName)
             where TElementType : VisualElement
        {
            return CreateFromView<TElementType>(controllerName, actionName, null, null, null);
        }

        /// <summary>
        /// Creates from view.
        /// </summary>
        /// <typeparam name="TElementType">The type of the element type.</typeparam>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="area">The area.</param>
        /// <returns></returns>
        public static TElementType CreateFromView<TElementType>(string controllerName, string actionName, string area)
             where TElementType : VisualElement
        {
            return CreateFromView<TElementType>(controllerName, actionName, area, null, null);
        }
    }
}
