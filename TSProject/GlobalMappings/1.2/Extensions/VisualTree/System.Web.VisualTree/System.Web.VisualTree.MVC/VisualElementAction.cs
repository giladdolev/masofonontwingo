﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.Routing;
using System.Web.Mvc;

namespace System.Web.VisualTree.MVC
{
    /// <summary>
    /// Provides support for handling VisualTree events as MVC controller actions.
    /// </summary>
    /// <typeparam name="TActionArgs">The type of the action arguments.</typeparam>
    public class VisualElementAction<TActionArgs> : IVisualElementAction<TActionArgs>
    {
        /// <summary>
        /// The controller name
        /// </summary>
        private string _controllerName;

        /// <summary>
        /// The action
        /// </summary>
        private string _actionName;

        /// <summary>
        /// The element
        /// </summary>
        private IVisualElement _element;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementAction{TActionArgs}" /> class.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        public VisualElementAction(IVisualElement visualElement, string controllerName, string actionName)
        {
            _controllerName = controllerName;
            _actionName = actionName;
            _element = visualElement;
        }


        /// <summary>
        /// Invokes the specified sender.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The arguments.</param>
        public void Invoke(object sender, TActionArgs args)
        {
            DoInvokeAction(sender, args);
        }

        /// <summary>
        /// Invokes the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public void Invoke(TActionArgs args)
        {
            DoInvokeAction(_element, args);
        }

        /// <summary>
        /// Does the invoke action.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The arguments.</param>
        /// <exception cref="System.Web.VisualTree.MVC.VisualElementActionException"></exception>
        private void DoInvokeAction(object sender, TActionArgs args)
        {
            try
            {
                Controller controllerInstance = ControllerExtensions.GetOrCreateController(_controllerName, VisualElement.GetContainerElement(sender as VisualElement));

                if (controllerInstance != null)
                {
                    // Get the action method
                    MethodInfo actionMethodInfo = controllerInstance.GetType().GetMethod(_actionName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                    // If there is a valid action method
                    if (actionMethodInfo != null)
                    {
                        // Get parameters
                        ParameterInfo[] parameters = actionMethodInfo.GetParameters();

                        // If there is a valid array of parameters
                        if (parameters != null)
                        {
                            switch (parameters.Length)
                            {
                                case 0:
                                    // Invoke the action method
                                    actionMethodInfo.Invoke(controllerInstance, new object[] { });
                                    break;
                                case 1:
                                    // Invoke the action method
                                    actionMethodInfo.Invoke(controllerInstance, new object[] { args });
                                    break;
                                case 2:
                                    // Invoke the action method
                                    actionMethodInfo.Invoke(controllerInstance, new object[] { sender, args });
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                // Raise a visual element action exception.
                throw new VisualElementActionException(string.Format(CultureInfo.InvariantCulture, "Failed to invoke '{0}' action on '{1}' controller.", _actionName, _controllerName), objException);
            }
        }
    }

    
    [SerializableAttribute] 
    public class VisualElementActionException : ApplicationException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementActionException"/> class.
        /// </summary>
        public VisualElementActionException()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementActionException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public VisualElementActionException(string message): base(message)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementActionException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public VisualElementActionException(string message, Exception innerException)
            : base(message, innerException)
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementActionException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected VisualElementActionException(SerializationInfo serializationInfo, StreamingContext streamingContext):base(serializationInfo,streamingContext)
        {
            
        }
    }
}
