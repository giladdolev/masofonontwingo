﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.MVC
{

    public static class ControllerExtensions
    {


        /// <summary>
        /// Gets the window model by its name.
        /// </summary>
        /// <typeparam name="TWindowElement">The type of the window element.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public static TWindowElement GetWindowModelByName<TWindowElement>(this Controller controller, string viewName) where TWindowElement : class, IWindowElement
        {
            // Get the window element
            IWindowElement windowElement = ApplicationElement.GetWindowModelByName<TWindowElement>(viewName);

            // Return the empty result
            return windowElement as TWindowElement;
        }


        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="initializerAction">The initializer action.</param>
        /// <param name="viewName">The view name.</param>
        /// <param name="masterName">The master name.</param>
        /// <returns></returns>
        public static ActionResult CreateView<TVisualElement>(this Controller controller, Action<WindowElement> initializerAction, string viewName, string masterName)
            where TVisualElement : VisualElement, new()
        {
            return controller.CreateView<TVisualElement>(initializerAction, viewName, masterName, new TVisualElement());
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="TModelElement">The type of the model element.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="initializerAction">The initializer action.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="masterName">Name of the master.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns></returns>
        public static ActionResult CreateView<TModelElement>(this Controller controller, Action<WindowElement> initializerAction, string viewName, string masterName, object viewModel)
            where TModelElement : new()
        {
            // If there is a valid controller
            if (controller != null)
            {
                // Set the view model
                controller.ViewData.Model = viewModel;

                // If there is a valid initializer action
                if (initializerAction != null)
                {
                    // Set main form to ensure loading the form
                    controller.TempData["_vt_initialize"] = initializerAction;
                }


                // Create the view result
                ViewResult result = new ViewResult();
                result.ViewName = viewName;
                result.MasterName = masterName;
                result.ViewData = controller.ViewData;
                result.TempData = controller.TempData;
                result.ViewEngineCollection = controller.ViewEngineCollection;
                return result;
            }

            return null;
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="initializerAction">The initializer action.</param>
        /// <param name="viewName">The view name.</param>
        /// <returns></returns>
        public static ActionResult CreateView<TVisualElement>(this Controller controller, Action<WindowElement> initializerAction, string viewName)
            where TVisualElement : VisualElement, new()
        {
            return CreateView<TVisualElement>(controller, initializerAction, viewName, null);
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="initializerAction">The initializer action.</param>
        /// <returns></returns>
        public static ActionResult CreateView<TVisualElement>(this Controller controller, Action<WindowElement> initializerAction)
            where TVisualElement : VisualElement, new()
        {
            return CreateView<TVisualElement>(controller, initializerAction, null, null);
        }

        /// <summary>
        /// Creates the view.
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">The view name.</param>
        /// <param name="masterName">The master name.</param>
        /// <returns></returns>
        public static ActionResult CreateView<TVisualElement>(this Controller controller, string viewName, string masterName)
            where TVisualElement : VisualElement, new()
        {
            return controller.CreateView<TVisualElement>(null, viewName, masterName);
        }


        /// <summary>
        /// Creates an internal view (can only be created with in the app code - not navigated from client).
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">The view name.</param>
        /// <param name="masterName">The master name.</param>
        /// <param name="model">The Model</param>
        /// <returns></returns>
        public static ActionResult CreateInternalView<TVisualElement>(this Controller controller, string viewName, string masterName, TVisualElement model)
            where TVisualElement : VisualElement
        {
            // If there is a valid controller
            if (controller != null)
            {
                if (controller.TempData[RenderingConstants.VT_CREATE_VIEW] != null)
                {
                    // Set the view model
                    controller.ViewData.Model = model;

                    // Create the view result
                    ViewResult result = new ViewResult();
                    result.ViewName = viewName;
                    result.MasterName = masterName;
                    result.ViewData = controller.ViewData;
                    result.TempData = controller.TempData;
                    result.ViewEngineCollection = controller.ViewEngineCollection;
                    return result;
                }
                return new RedirectResult("/");
            }

            return null;

        }

        /// <summary>
        /// Creates an internal view (can only be created with in the app code - not navigated from client).
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">The view name.</param>
        /// <param name="masterName">The master name.</param>
        /// <returns></returns>
        public static ActionResult CreateInternalView<TVisualElement>(this Controller controller, string viewName, string masterName)
            where TVisualElement : VisualElement, new()
        {
            return controller.CreateInternalView(viewName, masterName, new TVisualElement());
        }



        /// <summary>
        /// Creates an internal view (can only be created with in the app code - not navigated from client).
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">The view name.</param>
        /// <returns></returns>
        public static ActionResult CreateInternalView<TVisualElement>(this Controller controller, string viewName)
            where TVisualElement : VisualElement, new()
        {
            return controller.CreateInternalView(viewName, null, new TVisualElement());


        }

        /// <summary>
        /// Creates an internal view (can only be created with in the app code - not navigated from client).
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="model">The model</param>
        /// <returns></returns>
        public static ActionResult CreateInternalView<TVisualElement>(this Controller controller, TVisualElement model)
            where TVisualElement : VisualElement, new()
        {
            return controller.CreateInternalView(null, null, model);

        }

        /// <summary>
        /// Creates an internal view (can only be created with in the app code - not navigated from client).
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <returns></returns>
        public static ActionResult CreateInternalView<TVisualElement>(this Controller controller)
            where TVisualElement : VisualElement, new()
        {
            return controller.CreateInternalView(null, null, new TVisualElement());

        }



        /// <summary>
        /// Gets the window element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <returns></returns>
        public static VisualElement GetRootVisualElement(this Controller controller)
        {
            if (controller != null)
            {
                TempDataDictionary tempData = controller.TempData;
                if (tempData != null)
                {
                    // Return the visual tree container
                    return tempData[RenderingConstants.VT_CONTAINER] as VisualElement;
                }
            }

            return null;
        }


        /// <summary>
        /// Gets the visual element by id.
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="controller">The controller.</param>
        /// <param name="visualElementID">The visual element id.</param>
        /// <returns></returns>
        public static TVisualElement GetVisualElementById<TVisualElement>(this Controller controller, string visualElementID)
            where TVisualElement : VisualElement
        {
            // If there is a valid controller
            if (controller != null)
            {
                // Get the temp data
                TempDataDictionary tempData = controller.TempData;

                // If there is a valid temp data
                if (tempData != null)
                {
                    // Get the visual tree container
                    VisualElement visualElement = tempData[RenderingConstants.VT_CONTAINER] as VisualElement;

                    // If there is a valid visual element container
                    if (visualElement != null)
                    {
                        // Get visual element by id
                        return visualElement.GetVisualElementById<TVisualElement>(visualElementID);
                    }
                }
            }

            return null;
        }

        public static List<TVisualElement> GetVisualElementsById<TVisualElement>(this Controller controller, string visualElementID)
    where TVisualElement : VisualElement
        {
            // If there is a valid controller
            if (controller != null)
            {
                // Get the temp data
                TempDataDictionary tempData = controller.TempData;

                // If there is a valid temp data
                if (tempData != null)
                {
                    // Get the visual tree container
                    VisualElement visualElement = tempData[RenderingConstants.VT_CONTAINER] as VisualElement;

                    // If there is a valid visual element container
                    if (visualElement != null)
                    {
                        // Get visual element by id
                        return visualElement.GetVisualElementsById<TVisualElement>(visualElementID);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the restorable view result.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="appId">The application identifier.</param>
        /// <param name="appIdParameterName">The application identifier parameter name.</param>
        /// <returns></returns>
        public static ActionResult GetRestorableViewResult(this Controller controller, string appId, string appIdParameterName, Func<WindowElement> createModel = null)
        {
            // The current application element
            ApplicationElement applicationElement = null;

            // If there is no valid app ID
            if (string.IsNullOrWhiteSpace(appId))
            {
                // Create new application element
                applicationElement = ApplicationElement.CreateApplicationElement();

                // If there is a valid application element
                if (applicationElement != null)
                {
                    RouteValueDictionary values = null;

                    // Get route data
                    RouteData routeData = controller.RouteData;

                    // If there is a valid route data
                    if (routeData != null)
                    {
                        // Get values
                        values = new RouteValueDictionary(routeData.Values);
                    }

                    // If there are no valid values
                    if (values == null)
                    {
                        // set new route values
                        values = new RouteValueDictionary();
                    }

                    // Set the app ID
                    values[appIdParameterName] = applicationElement.ClientID;
                    
                    // Re-route to include app ID in URL
                    return new RedirectToRouteResult(values);
                }

            }
            else
            {
                // Get existing application element by ID
                applicationElement = ApplicationElement.RestoreApplicationElement(appId, true);

                // Get the main window
                WindowElement mainWindowElement = ApplicationElement.MainWindow;

                // If there is a valid main window
                if (mainWindowElement != null)
                {
                    // Indicate create view mode
                    controller.TempData[RenderingConstants.VT_RESTORE_VIEW] = true;

                    // Set main window
                    controller.ViewData.Model = mainWindowElement;

                    // Perform view restored
                    mainWindowElement.PerformViewRestored();
                }
            }

            if (controller.ViewData.Model == null && createModel != null)
            {
                    controller.ViewData.Model = createModel.Invoke();
            }

            // Create view result to display view
            ViewResult viewResult = new ViewResult()
            {
                ViewName = null,
                MasterName = null,
                ViewData = controller.ViewData,
                TempData = controller.TempData,
                ViewEngineCollection = controller.ViewEngineCollection
            };

            // Return view result
            return viewResult;

        }

        /// <summary>
        /// Gets the events visual result.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="appId">The application identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">controller</exception>
        public static ActionResult GetEventsVisualResult(this Controller controller, string appId)
        {
            // Create the synchronization session
            using (RenderingContext.CreateSynchronizationSession())
            {
                // validate parameter
                if (controller == null)
                {
                    throw new ArgumentNullException("controller");
                }

                try
                {
                    // Ensure application element is restored
                    ApplicationElement.RestoreApplicationElement(appId);
                }
                catch (TimeoutException)
                {
                    Logout();
                }

                // Increment revision
                ApplicationElement.IncrementRevision();

                // Create visual element render
                IVisualElementRenderer objVisualElementRenderer = VisualTreeManager.GetRenderer(ApplicationElement.CurrentRenderingEnvironment, typeof(ApplicationElement));

                // If there is a valid visual element renderer
                if (objVisualElementRenderer != null)
                {
                    // Render the current application element
                    return new ActionResultWrapper(objVisualElementRenderer.RenderResult(ApplicationElement.CurrentRenderingContext, ApplicationElement.Current, controller.Request));
                }

                return null;

            }

        }

        /// <summary>
        /// Logouts this instance.
        /// </summary>
        private static void Logout()
        {
            FormsAuthentication.SignOut();

            ApplicationElement.CreateApplicationElement();

            if (!String.IsNullOrEmpty(FormsAuthentication.LoginUrl))
            {
                BrowserUtils.NavigateUrl(FormsAuthentication.LoginUrl);
            }
            else
            {
                BrowserUtils.Reload();
            }
        }
        /// <summary>
        /// Gets the element resource result.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="appId">The application identifier.</param>
        /// <param name="elementId">The element identifier.</param>
        /// <param name="resourceId">The resourceId identifier.</param>
        /// <returns></returns>
        public static ActionResult GetElementResourceResult(this Controller controller, string appId, string elementId, string resourceId)
        {
            // Create the synchronization session
            using (RenderingContext.CreateSynchronizationSession())
            {
                // Restore and get application element
                ApplicationElement objApplicationElememnt = null;
                try
                {
                    // Ensure application element is restored
                    objApplicationElememnt = ApplicationElement.RestoreApplicationElement(appId);
                }
                catch (TimeoutException)
                {
                    Logout();
                }

                // If there is a valid application element
                if (objApplicationElememnt != null)
                {
                    // Get the visual element
                    VisualElement visualElement = objApplicationElememnt.GetVisualElementByPath(elementId);

                    if (visualElement != null)
                    {
                        // Create visual element render
                        IVisualElementRenderer visualElementRenderer = VisualTreeManager.GetRenderer(null, visualElement);

                        // If there is a valid visual element renderer
                        if (visualElementRenderer != null)
                        {
                            // Render the current application element
                            return new ActionResultWrapper(visualElementRenderer.RenderResourceResult(ApplicationElement.CurrentRenderingContext, visualElement, resourceId));
                        }
                    }
                }

                // Return the empty result
                return new EmptyResult();
            }
        }



        /// <summary>
        /// Gets the visual result.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="appId">The application identifier.</param>
        /// <returns></returns>
        public static ActionResult GetVisualResult(this Controller controller, RenderingContext renderingContext, string appId)
        {
            // Restore the application element
            try
            {
                // Ensure application element is restored
                ApplicationElement.RestoreApplicationElement(appId);
            }
            catch (TimeoutException)
            {
                Logout();
            }

            // Create visual element render
            IVisualElementRenderer objVisualElementRenderer = VisualTreeManager.GetRenderer(RenderingEnvironment.GetRenderingEnvironment(renderingContext), typeof(ApplicationElement));

            // If there is a valid visual element renderer
            if (objVisualElementRenderer != null)
            {
                // Render the current application element
                return new ActionResultWrapper(objVisualElementRenderer.RenderResult(null, ApplicationElement.Current));
            }

            return null;
        }

        /// <summary>
        /// Gets the or create controller.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static Controller GetOrCreateController(string controllerName, object model)
        {
            Controller controllerInstance = null;
            Tuple<object, string> key = GetControllerKey(controllerName, model);

            Dictionary<Tuple<object, string>, Controller> objControllerByModel = GetControllerByModelCollection();

            if (objControllerByModel != null && !objControllerByModel.TryGetValue(key, out controllerInstance))
            {
                controllerInstance = CreateController(controllerName);

                if (controllerInstance != null)
                {
                    controllerInstance.RegisterModel(model);
                }
            }

            return controllerInstance;
        }

        /// <summary>
        /// Gets the or create controller.
        /// </summary>
        /// <typeparam name="TController">The type of the controller.</typeparam>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static TController GetOrCreateController<TController>(this Controller controller, object model)
            where TController : Controller
        {
            string controllerName = GetControllerName(typeof(TController));

            Controller controllerInstance = GetOrCreateController(controllerName, model);

            if (controllerInstance is TController)
            {
                return (TController)controllerInstance;
            }

            return null;
        }

        /// <summary>
        /// Creates the controller.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <returns></returns>
        private static Controller CreateController(string controllerName)
        {
            ControllerBuilder controllerBuilder = ControllerBuilder.Current;

            if (controllerBuilder != null)
            {
                IControllerFactory controllerFactory = controllerBuilder.GetControllerFactory();

                if (controllerFactory != null)
                {
                    HttpContext objHttpContext = RenderingContext.CurrentHttpContext;
                    if (objHttpContext != null)
                    {
                        return controllerFactory.CreateController(objHttpContext.Request.RequestContext, controllerName) as Controller;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the name of the controller.
        /// </summary>
        /// <param name="controllerType">Type of the controller.</param>
        /// <returns></returns>
        private static string GetControllerName(Type controllerType)
        {
            string controllerName = controllerType.Name;

            if (controllerName.EndsWith("Controller"))
            {
                controllerName = controllerName.Remove(controllerName.Length - "Controller".Length);
            }

            return controllerName;
        }

        /// <summary>
        /// Gets the controller key.
        /// </summary>
        /// <param name="controllerType">Type of the controller.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        private static Tuple<object, string> GetControllerKey(string controllerName, object model)
        {
            return new Tuple<object, string> (model, controllerName);
        }

        /// <summary>
        /// Registers the model.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="model">The model.</param>
        internal static void RegisterModel(this Controller controller, object model)
        {
            if (controller != null)
            {
                controller.TempData[RenderingConstants.VT_CONTAINER] = model;

                Tuple<object, string> key = GetControllerKey(GetControllerName(controller.GetType()), model);

                if (key != null)
                {
                    Dictionary<Tuple<object, string>, Controller> objControllerByModel = GetControllerByModelCollection();

                    if (objControllerByModel != null)
                    {
                        objControllerByModel[key] = controller;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the controller by model collection.
        /// </summary>
        /// <returns></returns>
        private static Dictionary<Tuple<object, string>, Controller> GetControllerByModelCollection()
        {
            Dictionary<Tuple<object, string>, Controller> objControllerByModel = null;

            HttpContext objHttpContext = RenderingContext.CurrentHttpContext;
            if (objHttpContext != null)
            {
                if (objHttpContext.Items.Contains(RenderingConstants.VT_CONTROLLERS))
                {
                    objControllerByModel = objHttpContext.Items[RenderingConstants.VT_CONTROLLERS] as Dictionary<Tuple<object, string>, Controller>;
                }
                else
                {
                    objHttpContext.Items[RenderingConstants.VT_CONTROLLERS] = objControllerByModel = new Dictionary<Tuple<object, string>, Controller>();
                }
            }

            return objControllerByModel;
        }
    }
}
