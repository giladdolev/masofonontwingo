﻿using System.Web.Mvc;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;

namespace System.Web.VisualTree.MVC
{

    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// Renders the specified object helper.
        /// </summary>
        /// <param name="objHelper">The helper.</param>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public static MvcHtmlString Render(this HtmlHelper objHelper, RenderingContext renderingContext, VisualElement visualElement)
        {
            if (renderingContext == null)
            {
                renderingContext = new RenderingContext(RenderingEnvironment.Default, false);
            }

            return Render(objHelper, renderingContext, visualElement, null);
        }

        /// <summary>
        /// Renders the specified object helper.
        /// </summary>
        /// <param name="objHelper">The helper.</param>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="renderTo">The id render to (if not specified the body is used).</param>
        /// <returns></returns>
        public static MvcHtmlString Render(this HtmlHelper objHelper, RenderingContext renderingContext, VisualElement visualElement, string renderTo)
        {

            // If there is a valid visual element
            if (visualElement != null)
            {
                // Get the current control container
                VisualElement objControlContainer = ApplicationElement.CurrentContainer;

                // If there is a valid control container
                if (objControlContainer != null)
                {
                    // Add the current visual element
                    objControlContainer.Add(visualElement, false);
                }

                // Get control element
                ControlElement controlElement = visualElement as ControlElement;

                // If there is a valid control element
                if (controlElement != null)
                {
                    // Ensure control is created
                    controlElement.CreateControl();
                }

                // Render element code
                string strCode = VisualTreeManager.RenderElement(renderingContext, visualElement, renderTo);


                // Return html string
                return new MvcHtmlString(strCode);
            }

            return null;

        }

    }
}
