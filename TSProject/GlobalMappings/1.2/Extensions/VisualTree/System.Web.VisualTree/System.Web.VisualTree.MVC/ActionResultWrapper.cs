﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.MVC
{
    public class ActionResultWrapper : ActionResult
    {
        private IVisualElementRenderer _visualElement;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionResultWrapper"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public ActionResultWrapper(IVisualElementRenderer visualElement)
        {
            this._visualElement = visualElement;
        }
        /// <summary>
        /// Enables processing of the result of an action method by a custom type that inherits from the <see cref="T:System.Web.Mvc.ActionResult" /> class.
        /// </summary>
        /// <param name="context">The context in which the result is executed. The context information includes the controller, HTTP content, request context, and route data.</param>
        public override void ExecuteResult(ControllerContext context)
        {
            // validate parameter
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            // Execute result
            this._visualElement.ExecuteResult(ApplicationElement.CurrentRenderingContext, context.HttpContext);
        }
    }
}
