﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.MVC
{
    [Export(typeof(IVisualElementProvider))]
    public class VisualElementProvider : IVisualElementProvider
    {
        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="windowControl">The window control.</param>
        /// <param name="element">The element.</param>
        /// <param name="preventRendering">if set to <c>true</c> prevent rendering.</param>
        /// <param name="preventSync">if set to <c>true</c> prevent synchronize.</param>
        /// <returns></returns>
        public IVisualElement OnInitializeWindow(Control windowControl, IVisualElement element, ref bool preventRendering, ref bool preventSynchronize)
        {
            // Get the view page
            ViewPage viewPage = windowControl.Page as ViewPage;

            // If there is a valid view page
            if (viewPage != null)
            {
                // If is in create view mode
                if (viewPage.TempData != null)
                {
                    // If is create view
                    if (viewPage.TempData[RenderingConstants.VT_CREATE_VIEW] != null)
                    {
                        // We should prevent rendering
                        preventRendering = true;
                    }

                    // If we are restoring view
                    else if (viewPage.TempData[RenderingConstants.VT_RESTORE_VIEW] != null)
                    {
                        // We should prevent synchronizing 
                        preventSynchronize = true;
                    }
                }

                // Set the model view element as the VT container control
                viewPage.TempData[RenderingConstants.VT_CONTAINER_CONTROL] = windowControl;

                // Get window element
                VisualElement modelWindowElement = viewPage.Model as VisualElement;

                // Get the window element
                VisualElement windowElement = element as VisualElement;

                // If there is a valid model window element
                if (modelWindowElement != null)
                {
                    // Set the model view element as the VT container
                    viewPage.TempData[RenderingConstants.VT_CONTAINER] = modelWindowElement;
                    
                    return modelWindowElement;
                }
                // If there is a valid window
                else if(windowElement != null)
                {
                    // Set the model view element as the VT container
                    viewPage.TempData[RenderingConstants.VT_CONTAINER] = windowElement;

                    // This is not a restore mode
                    preventSynchronize = false;

                    return windowElement;
                }
            }

            return null;
        }
        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="windowControl">The window control.</param>
        /// <param name="windowElement">The window element.</param>
        public void InitializeWindow(Control windowControl, IVisualElement windowElement)
        {
            // Get the view page
            ViewPage viewPage = windowControl.Page as ViewPage;

            // If there is a valid view page
            if (viewPage != null)
            {
                // Try to get window initializer method
                Action<WindowElement> windowInitializer = viewPage.TempData["_vt_initialize"] as Action<WindowElement>;

                // If there is a valid window initializer
                if (windowInitializer != null)
                {
                    // Initialize window
                    windowInitializer(windowElement as WindowElement);
                }
            }
        }

        /// <summary>
        /// Creates the action.
        /// </summary>
        /// <typeparam name="TActionArgs">The type of the action arguments.</typeparam>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <returns></returns>
        public IVisualElementAction<TActionArgs> CreateAction<TActionArgs>(IVisualElement visualElement, string controllerName, string actionName)
        {
            return new VisualElementAction<TActionArgs>(visualElement, controllerName, actionName);
        }
    }
}
