﻿using System.Web.UI.WebControls;

namespace System.Web.VisualTree
{
    public static class WebExtensions
    {
        /// <summary>
        /// To the pixels.
        /// </summary>
        /// <param name="objUnit">The unit.</param>
        /// <returns></returns>
        public static int ToPixels(this Unit objUnit)
        {
            // If is a pixel type
            if(objUnit.Type == UnitType.Pixel)
            {
                // Return value as integer
                return (int)objUnit.Value;
            }

            return 0;
        }
    }
}
