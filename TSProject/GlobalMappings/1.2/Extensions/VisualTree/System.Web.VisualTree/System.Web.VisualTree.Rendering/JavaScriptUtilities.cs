﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Rendering
{
    public static class JavaScriptUtilities
    {
        /// <summary>
        /// Replaces '\n' with '<br>' 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ParseString(string text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                if (text.Contains("\n"))
                {
                    text = text.Replace("\n", "<br>");
                }
                if (text.Contains("\r\n"))
                {
                    text = text.Replace("\r\n", "<br>");
                }

                text = text.Replace("\\", "\\\\");
            }
           
            return text;
        }
    }
}
