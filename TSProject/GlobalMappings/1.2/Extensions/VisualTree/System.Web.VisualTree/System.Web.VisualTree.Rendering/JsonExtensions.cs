﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.IO;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Rendering
{
    public static class JsonExtensions
    {
        /// <summary>
        /// Creates the writer.
        /// </summary>
        /// <returns></returns>
        public static JsonTextWriter CreateWriter(TextWriter writer)
        {
            Formatting formatting = Formatting.None;
            string formattingCfg = System.Configuration.ConfigurationManager.AppSettings["jsonFormatting"];

            if (formattingCfg == "Indented")
            {
                formatting = Formatting.Indented;
            }


            // Create the json writer
            JsonTextWriter jsonWriter = new JsonTextWriter(writer);

            // Indicate that we should not quote names
            jsonWriter.QuoteName = false;
            jsonWriter.Indentation = 1;
            jsonWriter.IndentChar = '\t';
            jsonWriter.Formatting = formatting;

            return jsonWriter;
        }

        /// <summary>
        /// Gets a JObject property as string.
        /// </summary>
        /// <param name="jsObject">The js object.</param>
        /// <param name="propertyName">The property name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string GetPropertyAsString(this JObject jsObject, string propertyName, string defaultValue)
        {
            // The value
            string value = defaultValue;


            // The value token
            JToken valueToken = null;

            // Get token from property name
            if (jsObject != null && jsObject.TryGetValue(propertyName, out valueToken))
            {
                // If there is a valid value token
                if (valueToken != null)
                {
                    // Set the string value
                    value = valueToken.ToString();
                }
            }

            return value;
        }


        /// <summary>
        /// Gets a JObject property as int.
        /// </summary>
        /// <param name="jsObject">The js object.</param>
        /// <param name="propertyName">The property name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static int GetPropertyAsInt(this JObject jsObject, string propertyName, int defaultValue)
        {
            // The value
            int value = defaultValue;


            // The value token
            JToken valueToken = null;

            // Get token from property name
            if (jsObject != null && jsObject.TryGetValue(propertyName, out valueToken))
            {
                // If there is a valid value token
                if (valueToken != null)
                {
                    // Get value

                    if (int.TryParse(valueToken.ToString(), out value))
                    {
                        return value;
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Gets the property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsObject">The object.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static T GetProperty<T>(this JObject jsObject, string propertyName, T defaultValue)
        {
            // The value
            T value = defaultValue;


            // The value token
            JToken valueToken = null;

            // Get token from property name
            if (jsObject != null && jsObject.TryGetValue(propertyName, out valueToken))
            {
                // If there is a valid value token
                if (valueToken == null) return value;
                try
                {
                    // Get value
                    value = (T)Convert.ChangeType(valueToken.ToString(), typeof(T));

                }
                catch (ArgumentException)
                {
                    return value;
                }
                catch (InvalidCastException)
                {
                    return value;
                }
            }
            return value;
        }

        /// <summary>
        /// Converts a long representation of time since the unix epoch to a DateTime.
        /// </summary>
        /// <param name="epoch">The number of seconds since Jan 1, 1970.</param>
        /// <returns>A DateTime representing the time since the epoch.</returns>
        public static DateTime FromEpoch(long epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(epoch).ToLocalTime();
        }

        /// <summary>
        /// Gets the typed object.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static object GetTypedObject(this JToken token, Type type)
        {
                if (type == typeof(DateTime))
                {
                    string value = token.ToString();
                    long epochTime = 0;
                    Int64.TryParse(value, out epochTime);
                    return FromEpoch(epochTime);
                }

                return token.ToObject(type);
        }
        /// <summary>
        /// Writes the empty object.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        public static void WriteEmptyObject(this JsonTextWriter jsonWriter)
        {
            jsonWriter.WriteStartObject();
            jsonWriter.WriteEndObject();
        }

        /// <summary>
        /// Writes the object with property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteObjectWithProperty(this JsonTextWriter jsonWriter, string name, string value)
        {
            jsonWriter.WriteStartObject();
            jsonWriter.WriteProperty(name, value);
            jsonWriter.WriteEndObject();
        }

        /// <summary>
        /// Writes a property with an object value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        public static void WriteStartObjectProperty(this JsonTextWriter jsonWriter, string name)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteStartObject();
        }

        /// <summary>
        /// Writes a property with an array value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        public static void WriteStartArrayProperty(this JsonTextWriter jsonWriter, string name)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteStartArray();
        }


        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, bool value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }


        /// <summary>
        /// Writes the formatted property.
        /// </summary>
        /// <param name="jsonWriter">The JSON writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="valueParams">The value parameters.</param>
        public static void WriteFormattedProperty(this JsonTextWriter jsonWriter, string name, string value, params object[] valueParams)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteRawValue(string.Format(value, JavaScriptBuilder.GetValueCode(valueParams)));
        }

        /// <summary>
        /// Writes the property object.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, object value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteRawValue(JavaScriptBuilder.GetValueCode(value));
        }

        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="scriptCode">The script code.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, JavaScriptCode scriptCode)
        {
            // Write the property name
            jsonWriter.WritePropertyName(name);

            // If there is a valid script code
            if (scriptCode != null)
            {
                // Write raw value
                jsonWriter.WriteRawValue(scriptCode.ToString());
            }
            else
            {
                // Write null value
                jsonWriter.WriteValue((object)null);
            }
        }


        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="valueWriter">The value writer.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, JsonValueWriter valueWriter)
        {
            jsonWriter.WritePropertyName(name);
            valueWriter.WriteValue(jsonWriter);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, byte value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, byte[] value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, char value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }


        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, DateTime value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }


        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, DateTimeOffset value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, IResourceReference value)
        {
            // If there is a valid image reference
            if (value != null)
            {
                // Return the source url
                jsonWriter.WriteProperty(name, value.Source);
            }
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, decimal value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, double value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, float value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, Guid value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, int value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, long value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, sbyte value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, short value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }


        /// <summary>
        /// Writes a property with raw value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WritePropertyWithRawValue(this JsonTextWriter jsonWriter, string name, string value)
        {
            jsonWriter.WriteProperty(name, new JavaScriptCode(value));
        }

        /// <summary>
        /// Writes a property with raw value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WritePropertyWithAnonymousFunction(this JsonTextWriter jsonWriter, string name, Action<JavaScriptBuilder> value)
        {
            WritePropertyWithAnonymousFunction(jsonWriter, name, new string[] { }, value);
        }


        /// <summary>
        /// Writes a property with raw value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WritePropertyWithAnonymousFunction(this JsonTextWriter jsonWriter, string name, string[] parameters, Action<JavaScriptBuilder> value)
        {
            // If there is a valid value
            if (value != null)
            {
                // Create java script builder
                JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();

                // Write anonymous function
                javaScriptBuilder.AppendAnonymousFunction(parameters, () => { value(javaScriptBuilder); });

                // Add the property
                jsonWriter.WriteProperty(name, new JavaScriptCode(javaScriptBuilder.ToString()));
            }
            else
            {
                jsonWriter.WriteProperty(name, (string)null);
            }
        }

        /// <summary>
        /// Writes a client function.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteClientFunction(this JsonTextWriter jsonWriter, string name, string[] parameters, string value)
        {
            // If there is a valid value
            if (value != null)
            {
                // Create java script builder
                JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();

                // Write client function
                javaScriptBuilder.AppendClientFunction(parameters, value);

                // Add the property
                jsonWriter.WriteProperty(name, new JavaScriptCode(javaScriptBuilder.ToString()));
            }
            else
            {
                jsonWriter.WriteProperty(name, (string)null);
            }
        }

        /// <summary>
        /// Writes a property with raw value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WritePropertyWithRawValue(this JsonTextWriter jsonWriter, string name, Action<JavaScriptBuilder> value)
        {
            // If there is a valid value
            if (value != null)
            {
                // Create java script builder
                JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();

                // Fill javascript
                value(javaScriptBuilder);

                // Add the property
                jsonWriter.WriteProperty(name, new JavaScriptCode(javaScriptBuilder.ToString()));
            }
            else
            {
                jsonWriter.WriteProperty(name, (string)null);
            }
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, string value)
        {
            // Check field name and value validity
            if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(value))
            {
                jsonWriter.WritePropertyName(name);
                jsonWriter.WriteValue(value);
            }
        }


        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="parameters">The parameters.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, string value, params object[] parameters)
        {
            WriteProperty(jsonWriter, name, String.Format(value, parameters));
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, TimeSpan value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, uint value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, ulong value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, Uri value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }

        /// <summary>
        /// Writes a property.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void WriteProperty(this JsonTextWriter jsonWriter, string name, ushort value)
        {
            jsonWriter.WritePropertyName(name);
            jsonWriter.WriteValue(value);
        }


    }

    /// <summary>
    /// Provides support for writing json values
    /// </summary>
    public abstract class JsonValueWriter
    {

        /// <summary>
        /// Writes the value.
        /// </summary>
        /// <param name="jsonWriter">The json writer.</param>
        public abstract void WriteValue(JsonTextWriter jsonWriter);
    }
}
