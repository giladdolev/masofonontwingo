﻿using Newtonsoft.Json;
using System.Text;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Rendering
{
    public class JavaScriptBuilder
    {
        // the builder
        private StringBuilder objBuilder = new StringBuilder();

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return objBuilder.ToString();
        }

        /// <summary>
        /// Renders the object property.
        /// </summary>
        /// <param name="objBuilder">The object builder.</param>
        /// <param name="strPropertyName">The property name.</param>
        /// <param name="strPropertyValue">The property value.</param>
        public void AppendObjectProperty(string strPropertyName, string strPropertyValue)
        {
            // Add name object pair
            objBuilder.Append(strPropertyName);
            objBuilder.Append(":");
            objBuilder.Append(strPropertyValue);
        }

        /// <summary>
        /// Appends the semicolon.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        public void AppendSemicolon()
        {
            objBuilder.Append(";");
        }

        /// <summary>
        /// Appends the comma.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        public void AppendComma()
        {
            objBuilder.Append(",");
        }

        /// <summary>
        /// Appends the start object.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        public void AppendStartObject()
        {
            objBuilder.Append("{");
        }


        /// <summary>
        /// Appends the end object.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        public void AppendEndObject()
        {
            objBuilder.Append("}");
        }

        /// <summary>
        /// Appends the start invocation.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        /// <param name="strTarget">The target.</param>
        public void AppendStartInvocation(string strTarget)
        {
            objBuilder.Append(strTarget);
            objBuilder.Append("(");
        }

        /// <summary>
        /// Appends the start anonymous function.
        /// </summary>
        public void AppendStartAnonymousFunction(params object[] parameters)
        {
            objBuilder.Append("function (");

            AppendParameters(parameters);

            objBuilder.Append(") {");
        }

        /// <summary>
        /// Appends the end anonymous function.
        /// </summary>
        public void AppendEndAnonymousFunction()
        {
            objBuilder.Append("}");
        }

        /// <summary>
        /// Appends the end invocation.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        public void AppendEndInvocation()
        {
            AppendEndInvocation(false);
        }

        /// <summary>
        /// Appends the end invocation.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        /// <param name="blnIsStatement">if set to <c>true</c> is statement.</param>
        public void AppendEndInvocation(bool blnIsStatement)
        {
            objBuilder.Append(")");

            if (blnIsStatement)
            {
                objBuilder.Append(";");
            }
        }



        /// <summary>
        /// Renders the invocation expression.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        /// <param name="strTarget">The target.</param>
        /// <param name="arrParameters">The parameters.</param>
        public void AppendInvocationExpression(string strTarget, params object[] arrParameters)
        {
            AppendInvocation(false, strTarget, arrParameters);
        }

        /// <summary>
        /// Renders the anonymous function.
        /// </summary>
        /// <param name="objBuilder">The string builder.</param>
        /// <param name="bodyAction">The body action.</param>
        public void AppendAnonymousFunction(Action bodyAction)
        {
            AppendAnonymousFunction(null, bodyAction);
        }


        /// <summary>
        /// Renders the anonymous function.
        /// </summary>
        /// <param name="objBuilder">The string builder.</param>
        /// <param name="bodyAction">The body action.</param>
        public void AppendAnonymousFunction(string[] parameterNames, Action bodyAction)
        {
            // Generate function start
            objBuilder.Append("function(");

            // If there are parameters
            if (parameterNames != null && parameterNames.Length > 0)
            {
                // Indicates first parameter
                bool firstParameter = true;

                // Loop all parameter names
                foreach (string parameterName in parameterNames)
                {
                    // If is not first parameter
                    if (!firstParameter)
                    {
                        // Add separator
                        objBuilder.Append(",");
                    }
                    else
                    {
                        // Indicate not first parameter
                        firstParameter = false;
                    }

                    // Add parameter name
                    objBuilder.Append(parameterName);
                }
            }

            // Add function starting section
            objBuilder.Append("){");

            // If there is a valid body action
            if (bodyAction != null)
            {
                // Generate body
                bodyAction();
            }

            // Generate function end
            objBuilder.Append("}");
        }

        /// <summary>
        /// Renders the client function.
        /// </summary>
        /// <param name="objBuilder">The string builder.</param>
        /// <param name="bodyAction">The body js code.</param>
        public void AppendClientFunction(string[] parameterNames, string bodyAction)
        {
            Action action = null;
            if (bodyAction != null)
            {
                action = () => objBuilder.Append(bodyAction);
            }
            AppendAnonymousFunction(parameterNames, action);
        }

        /// <summary>
        /// Renders the invocation statement.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        /// <param name="strTarget">The target.</param>
        /// <param name="arrParameters">The parameters.</param>
        public void AppendVariableWithInvocationInitializer(string strVariable, string strTarget, params object[] arrParameters)
        {
            objBuilder.Append("var ");
            objBuilder.Append(strVariable);
            objBuilder.Append(" = ");
            AppendInvocationStatement(strTarget, arrParameters);
        }


        /// <summary>
        /// Appends the new object creation expression.
        /// </summary>
        /// <param name="type">The type.</param>
        public void AppendNewObjectCreationExpression(string type)
        {
            objBuilder.Append("new ");
            AppendStartInvocation(type);
        }


        /// <summary>
        /// Renders the invocation statement.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        /// <param name="strTarget">The target.</param>
        /// <param name="arrParameters">The parameters.</param>
        public void AppendInvocationStatement(string strTarget, params object[] arrParameters)
        {
            AppendInvocation(true, strTarget, arrParameters);
        }

        //gilad check
        public void AppendMultipleInvocationStatement(string strTarget, int index, params object[] arrParameters)
        {
            AppendMultipleInvocation(true, strTarget, index, arrParameters);
        }

        /// <summary>
        /// Appends the debugger statement.
        /// </summary>
        public void AppendDebuggerStatement()
        {
        }

        /// <summary>
        /// Append log statement
        /// </summary>
        public void AppendLoggerStatement(string msg)
        {
            //gilad
            //if (!string.IsNullOrEmpty(msg))
            //{
            //    objBuilder.Append(string.Format("console.log(\"{0}\");", msg));
            //}
        }

        /// <summary>
        /// Renders the invocation.
        /// </summary>
        /// <param name="objBuilder">The builder.</param>
        /// <param name="blnIsStatement">if set to <c>true</c> is statement.</param>
        /// <param name="strTarget">The target.</param>
        /// <param name="arrParameters">The parameters.</param>
        public void AppendInvocation(bool blnIsStatement, string strTarget, params object[] arrParameters)
        {
            // Add invocation target
            objBuilder.Append(strTarget);
            objBuilder.Append("(");

            AppendParameters(arrParameters);

            objBuilder.Append(")");

            // If is statement render
            if (blnIsStatement)
            {
                objBuilder.Append(";");
            }
        }

        //gilad
        public void AppendMultipleInvocation(bool blnIsStatement, string strTarget, int index, params object[] arrParameters)
        {

            string str = string.Format("var rec = {0}.getAt({1});", strTarget, index);
            foreach (object objParameter in arrParameters)
            {
                if (objParameter.ToString().Contains(":"))
                {
                    string[] arr = objParameter.ToString().Replace("{", "").Replace("}", "").Trim().Split(new char[] { ',', ':' });
                    for (int i = 0; i < arr.Length; i += 2)
                    {
                        str += ("rec.set");
                        str += ("('");
                        str += (arr[i]);
                        str += ("',");
                        str += (arr[i + 1]);
                        str += (")");

                        if (blnIsStatement)
                        {
                            str += (";");
                        }
                    }
                }
            }

            objBuilder.Append(str);
        }



        /// <summary>
        /// Appends the parameters.
        /// </summary>
        /// <param name="arrParameters">The parameters array.</param>
        private void AppendParameters(object[] arrParameters)
        {
            bool blnIsFirst = true;

            // Loop all parameters
            foreach (object objParameter in arrParameters)
            {
                // If is first parameter
                if (blnIsFirst)
                {
                    blnIsFirst = false;
                }
                else
                {
                    // Add comma separator
                    objBuilder.Append(", ");
                }

                AppendValue(objParameter);
            }
        }




        /// <summary>
        /// Append the json value.
        /// </summary>
        /// <param name="value">The value to append.</param>
        /// <returns></returns>
        public void AppendValue(object value)
        {
            // Add raw script code
            objBuilder.Append(GetValueCode(value));
        }

        /// <summary>
        /// Gets the value code.
        /// </summary>
        /// <param name="value">The value.</param>
        internal static string[] GetValueCode(object[] value)
        {
            if (value != null && value.Length > 0)
            {
                string[] codes = new string[value.Length];

                for (int i = 0; i < value.Length; i++)
                {
                    codes[i] = GetValueCode(value[i]);
                }

                return codes;
            }

            return new string[] { };
        }


        /// <summary>
        /// Gets the value code.
        /// </summary>
        /// <param name="value">The value.</param>
        public static string GetValueCode(object value)
        {
            // Get script code
            JavaScriptCode scriptCode = value as JavaScriptCode;

            // If there is a valid script code
            if (scriptCode != null)
            {
                // Add raw script code
                return scriptCode.ToString();
            }
            else
            {
                // Get string function
                Func<string> scriptFunction = value as Func<string>;

                // If there is a valid script function
                if (scriptFunction != null)
                {
                    // Return script function return value
                    return scriptFunction();
                }
                else
                {
                    // Try to get resource reference
                    IResourceReference resourceReference = value as IResourceReference;

                    // If there is a valid resource reference
                    if (resourceReference != null)
                    {
                        // Return string of URL example: "http://localhost/temp.jpg"
                        return JsonConvert.SerializeObject(resourceReference.Source);
                    }
                    else
                    {
                        // Add JavaScript value
                        return JsonConvert.SerializeObject(value);
                    }
                }
            }
        }

        /// <summary>
        /// Appends the raw value.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        public void AppendRawValue(string strValue)
        {
            objBuilder.Append(strValue);
        }


        /// <summary>
        /// Appends the formatted raw value.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        /// <param name="parameters">The parameters.</param>
        public void AppendFormattedRawValue(string strValue, params object[] parameters)
        {
            objBuilder.AppendFormat(strValue, GetValueCode(parameters));
        }

        /// <summary>
        /// Appends the formatted raw.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        /// <param name="paramers">The parameters.</param>
        public void AppendFormattedRaw(string strValue, params object[] parameters)
        {
            SanitizeValues(parameters);
            objBuilder.AppendFormat(strValue, parameters);
        }

        private static void SanitizeValues(object[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i] is string)
                {
                    parameters[i] = Convert.ToString(parameters[i]).Replace("\'", "\\u0027");
                }
            }
        }


        /// <summary>
        /// Appends the assignment statement.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">The value.</param>
        public void AppendAssignmentStatement(string target, object value)
        {
            objBuilder.Append(target);
            objBuilder.Append(" = ");
            AppendValue(value);
            objBuilder.AppendLine(";");
        }

        /// <summary>
        /// Appends the start if.
        /// </summary>
        /// <param name="condition">The condition.</param>
        public void AppendStartIf(string condition)
        {
            objBuilder.Append("if(");
            objBuilder.Append(condition);
            objBuilder.AppendLine(") {");


        }

        /// <summary>
        /// Appends the end if.
        /// </summary>
        public void AppendEndIf()
        {
            objBuilder.AppendLine("}");
        }

        /// <summary>
        /// Appends the start if.
        /// </summary>
        public void AppendStartDelayedExecution()
        {
            objBuilder.Append("setTimeout(function () {");
        }

        /// <summary>
        /// Appends the end if.
        /// </summary>
        public void AppendEndDelayedExecution(int delay = 1)
        {
            objBuilder.AppendLine(string.Concat("}, ", delay, ");"));
        }


        /// <summary>
        /// Appends the else.
        /// </summary>
        public void AppendElse()
        {
            objBuilder.AppendLine("} else {");
        }


        /// <summary>
        /// Appends the array start character.
        /// </summary>
        public void AppendArrayStart()
        {
            objBuilder.Append("[");
        }

        /// <summary>
        /// Appends the array end character.
        /// </summary>
        public void AppendArrayEnd()
        {
            objBuilder.Append("]");
        }

        /// <summary>
        /// Appends return statement.
        /// </summary>
        public void AppendReturnStatement(object expression)
        {
            objBuilder.Append("return ");
            AppendValue(expression);
            objBuilder.AppendLine(";");
        }
    }


    /// <summary>
    /// Provides support for adding raw javascript code
    /// </summary>
    public class JavaScriptCode
    {
        /// <summary>
        /// The code
        /// </summary>
        private string _code;

        /// <summary>
        /// Initializes a new instance of the <see cref="JavaScriptCode"/> class.
        /// </summary>
        /// <param name="code">The code.</param>
        public JavaScriptCode(string code)
        {
            _code = code;
        }

        /// <summary>
        /// Formats the specified code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static JavaScriptCode Format(string code, params object[] parameters)
        {
            // Set the code with the parameters evaluated
            return new JavaScriptCode(string.Format(code, JavaScriptBuilder.GetValueCode(parameters)));
        }

        /// <summary>
        /// Gets the javascript boolean.
        /// </summary>
        /// <param name="value">if set to <c>true</c> print "true" otherwise "false".</param>
        /// <returns></returns>
        public static string GetBool(bool value)
        {
            return value ? "true" : "false";
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return _code;
        }

    }
}
