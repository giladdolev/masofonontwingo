﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    internal class SynchronizedSynchronizationContext : SynchronizationContext
    {
        /// <summary>
        /// Provides support for SynchronizedSynchronizationContext session
        /// </summary>
        private class SynchronizationSession : IDisposable
        {
            /// <summary>
            /// The previous synchronization context
            /// </summary>
            private readonly SynchronizationContext mobjPreviousSynchronizationContext;

            /// <summary>
            /// The current synchronization context
            /// </summary>
            private readonly SynchronizedSynchronizationContext mobjCurrentSynchronizationContext;

            /// <summary>
            /// Initializes a new instance of the <see cref="SynchronizationSession"/> class.
            /// </summary>
            /// <param name="objPreviousSynchronizationContext">The previous synchronization context.</param>
            public SynchronizationSession(SynchronizationContext objPreviousSynchronizationContext)
            {
                mobjPreviousSynchronizationContext = objPreviousSynchronizationContext;
                mobjCurrentSynchronizationContext = new SynchronizedSynchronizationContext();
            }


            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            void IDisposable.Dispose()
            {
                mobjCurrentSynchronizationContext.OnSessionEnd();
                SynchronizationContext.SetSynchronizationContext(mobjPreviousSynchronizationContext);
            }

            /// <summary>
            /// Gets the context.
            /// </summary>
            /// <value>
            /// The context.
            /// </value>
            public SynchronizationContext Context
            {
                get
                {
                    return mobjCurrentSynchronizationContext;
                }
            }
        }


        /// <summary>
        /// The HTTP context
        /// </summary>
        private HttpContext mobjHttpContext = null;

        /// <summary>
        /// Prevents a default instance of the <see cref="SynchronizedSynchronizationContext"/> class from being created.
        /// </summary>
        private SynchronizedSynchronizationContext()
        {
            mobjHttpContext = HttpContext.Current;
        }


 
        /// <summary>
        /// Opens the session.
        /// </summary>
        /// <returns></returns>
        public static IDisposable OpenSession()
        {
            SynchronizationSession objVTSynchronizationSession = new SynchronizationSession(SynchronizationContext.Current);
            SynchronizationContext.SetSynchronizationContext(objVTSynchronizationSession.Context);
            return objVTSynchronizationSession;
        }


        /// <summary>
        /// When overridden in a derived class, creates a copy of the synchronization context.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Threading.SynchronizationContext" /> object.
        /// </returns>
        public override SynchronizationContext CreateCopy()
        {
            return this;
        }

        /// <summary>
        /// When overridden in a derived class, responds to the notification that an operation has started.
        /// </summary>
        public override void OperationStarted()
        {

        }

        /// <summary>
        /// When overridden in a derived class, responds to the notification that an operation has completed.
        /// </summary>
        public override void OperationCompleted()
        {
        }

        /// <summary>
        /// When overridden in a derived class, dispatches an asynchronous message to a synchronization context.
        /// </summary>
        /// <param name="d">The <see cref="T:System.Threading.SendOrPostCallback" /> delegate to call.</param>
        /// <param name="state">The object passed to the delegate.</param>
        public override void Post(SendOrPostCallback d, object state)
        {
            try
            {
                d(state);
            }
            catch(Exception objException)
            {
                RegisterException(objException);
            }
        }

        /// <summary>
        /// When overridden in a derived class, dispatches a synchronous message to a synchronization context.
        /// </summary>
        /// <param name="d">The <see cref="T:System.Threading.SendOrPostCallback" /> delegate to call.</param>
        /// <param name="state">The object passed to the delegate.</param>
        public override void Send(SendOrPostCallback d, object state)
        {
            try
            {
                d(state);
            }
            catch(Exception objException)
            {
                RegisterException(objException);

                
            }
        }

        /// <summary>
        /// Registers the exception.
        /// </summary>
        /// <param name="objException">The exception.</param>
        private static void RegisterException(Exception objException)
        {
            // Get active synchronization context
            SynchronizedSynchronizationContext objSynchronizedSynchronizationContext = SynchronizationContext.Current as SynchronizedSynchronizationContext;
            if (objSynchronizedSynchronizationContext != null)
            {
                // Get active callback session
                SynchronizationCallbackSession objSynchronizationCallbackSession = objSynchronizedSynchronizationContext.CallbackSession;
                if (objSynchronizationCallbackSession != null)
                {
                    // If there is no exception
                    if (objSynchronizationCallbackSession.Exception == null)
                    {
                        // Set callback exception
                        objSynchronizationCallbackSession.Exception = objException;
                    }
                    else
                    {
                        // Aggregate exception
                        objSynchronizationCallbackSession.Exception = new AggregateException(objSynchronizationCallbackSession.Exception, objException);
                    }
                }
            }
            
        }

        /// <summary>
        /// Gets the HTTP context.
        /// </summary>
        /// <value>
        /// The HTTP context.
        /// </value>
        public HttpContext HttpContext
        {
            get
            {
                return mobjHttpContext;
            }
        }

        /// <summary>
        /// Gets the current HTTP context.
        /// </summary>
        /// <value>
        /// The current HTTP context.
        /// </value>
        public static HttpContext CurrentHttpContext
        {
            get
            {
                // Get current HTTP context
                HttpContext objHttpContext = HttpContext.Current;
                if (objHttpContext == null)
                {
                    // Get HTTP context from synchronization context
                    SynchronizedSynchronizationContext objSynchronizedSynchronizationContext = SynchronizationContext.Current as SynchronizedSynchronizationContext;
                    if (objSynchronizedSynchronizationContext != null)
                    {
                        objHttpContext = objSynchronizedSynchronizationContext.HttpContext;
                    }
                }

                return objHttpContext;
            }
        }

        /// <summary>
        /// Gets or sets the callback session.
        /// </summary>
        /// <value>
        /// The callback session.
        /// </value>
        public SynchronizationCallbackSession CallbackSession { get; set; }
        
        /// <summary>
        /// Called when session end.
        /// </summary>
        private void OnSessionEnd()
        {
            mobjHttpContext = null;
        }
    }
}
