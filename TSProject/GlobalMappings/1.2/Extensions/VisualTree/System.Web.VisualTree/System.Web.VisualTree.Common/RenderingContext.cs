﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{

    public class RenderingContext
    {
        /// <summary>
        /// The _setting another main window flag
        /// </summary>
        private bool _settingAlternativeMainWindow = false;

        /// <summary>
        /// The is design time flag
        /// </summary>
        private bool _isDesignTime = false;

        /// <summary>
        /// The root site path
        /// </summary>
        private string _rootSite = null;

        /// <summary>
        /// The root site assembly references
        /// </summary>
        private HashSet<Assembly> _rootSiteAssemblyReferences;

        /// <summary>
        /// The rendering environment
        /// </summary>
        private readonly RenderingEnvironment _renderingEnvironment;


        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingContext" /> class.
        /// </summary>
        /// <param name="renderingEnvironment">The rendering environment.</param>
        /// <param name="isDesignTime">if set to <c>true</c> is design time.</param>
        public RenderingContext(RenderingEnvironment renderingEnvironment, bool isDesignTime)
            : this(renderingEnvironment, isDesignTime, null, null)
        {

        }



        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingContext" /> class.
        /// </summary>
        /// <param name="renderingEnvironment">The rendering environment.</param>
        /// <param name="isDesignTime">if set to <c>true</c> is design time.</param>
        /// <param name="rootSite">The root site.</param>
        /// <param name="rootSiteAssemblyReferences">The root site assembly references.</param>
        public RenderingContext(RenderingEnvironment renderingEnvironment, bool isDesignTime, string rootSite, HashSet<Assembly> rootSiteAssemblyReferences)
        {
            _isDesignTime = isDesignTime;
            _rootSite = rootSite;
            _rootSiteAssemblyReferences = rootSiteAssemblyReferences;
            _renderingEnvironment = renderingEnvironment;
        }

        /// <summary>
        /// Creates the synchronization session.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IDisposable CreateSynchronizationSession()
        {
            return SynchronizedSynchronizationContext.OpenSession();
        }

        /// <summary>
        /// Sets the synchronization callback result.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="taskSource">The task source.</param>
        /// <param name="result">The result.</param>
        /// <exception cref="System.SystemException"></exception>
        public static void SetSynchronizationCallbackResult<TResult>(TaskCompletionSource<TResult> taskSource, TResult result)
        {
            // Create callback session
            using (SynchronizationCallbackSession objSynchronizationCallbackSession = SynchronizationCallbackSession.Create())
            {
                // Handler result from message box
                taskSource.SetResult(result);

                // If there is a valid callback session
                if (objSynchronizationCallbackSession != null)
                {
                    // Try to get callback exception
                    Exception objCallbackException = objSynchronizationCallbackSession.Exception;
                    if (objCallbackException != null)
                    {
                        // Throw exception
                        throw new SystemException(objCallbackException.Message, objCallbackException);
                    }
                }
            }
        }


        /// <summary>
        /// The is view debugging context flag
        /// </summary>
        [ThreadStatic]
        private static bool _isViewDebuggingContext;


        /// <summary>
        /// Gets a value indicating whether this instance is view debugging context.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is view debugging context; otherwise, <c>false</c>.
        /// </value>
        public static bool IsViewDebuggingContext
        {
            get
            {
                return _isViewDebuggingContext;
            }
        }

        /// <summary>
        /// Creates the view debugging session.
        /// </summary>
        /// <returns></returns>
        public static IDisposable CreateViewDebuggingSession()
        {
            return new ViewDebuggingSession();
        }

        /// <summary>
        /// Provide support for debugging views
        /// </summary>
        private class ViewDebuggingSession : IDisposable
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ViewDebuggingSession"/> class.
            /// </summary>
            public ViewDebuggingSession()
            {
                _isViewDebuggingContext = true;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            void IDisposable.Dispose()
            {
                _isViewDebuggingContext = false;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether [setting alternative main window].
        /// </summary>
        /// <value>
        /// <c>true</c> if [setting alternative main window]; otherwise, <c>false</c>.
        /// </value>
        public bool SettingAlternativeMainWindow
        {
            get
            {
                return _settingAlternativeMainWindow;
            }
            set
            {
                _settingAlternativeMainWindow = value;
            }
        }



        /// <summary>
        /// Gets the environment.
        /// </summary>
        /// <value>
        /// The environment.
        /// </value>
        public RenderingEnvironment Environment
        {
            get { return _renderingEnvironment; }
        }




        /// <summary>
        /// Gets a value indicating whether this instance is design time.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is design time; otherwise, <c>false</c>.
        /// </value>
        public bool IsDesignTime
        {
            get
            {
                return _isDesignTime;
            }
        }


        /// <summary>
        /// Gets the root site.
        /// </summary>
        /// <value>
        /// The root site.
        /// </value>
        public string RootSite
        {
            get
            {
                return _rootSite;
            }
        }



        /// <summary>
        /// Gets the root site assembly references.
        /// </summary>
        /// <value>
        /// The root site assembly references.
        /// </value>
        public HashSet<Assembly> RootSiteAssemblyReferences
        {
            get
            {
                return _rootSiteAssemblyReferences;
            }
        }


        /// <summary>
        /// Gets the HTTP context.
        /// </summary>
        /// <value>
        /// The HTTP context.
        /// </value>
        public static HttpContext CurrentHttpContext
        {
            get
            {
                return SynchronizedSynchronizationContext.CurrentHttpContext;
            }
        }

        /// <summary>
        /// Ensures the HTTP context.
        /// </summary>
        public static void EnsureHttpContext()
        {
            // If there is an invalid http context
            if (HttpContext.Current == null)
            {
                // Set the http context
                HttpContext.Current = SynchronizedSynchronizationContext.CurrentHttpContext;
            }
        }

        
    }
}
