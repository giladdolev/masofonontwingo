﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class RenderingConstants
    {

        public const string VT_CREATE_VIEW = "_vt_createview";

        public const string VT_RESTORE_VIEW = "_vt_restoreview";

        public const string VT_CONTAINER_CONTROL = "_vt_container_control";

        public const string VT_CONTROLLERS = "_vt_controllers";

        public const string VT_CONTAINER = "_vt_container";
    }
}
