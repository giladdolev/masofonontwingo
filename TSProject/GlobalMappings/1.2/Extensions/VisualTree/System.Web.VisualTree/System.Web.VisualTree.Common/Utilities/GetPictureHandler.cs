﻿using System;
using System.IO;
using System.Web;
using System.Web.Caching;
namespace System.Web.VisualTree.Common
{
    /// <summary>
    /// Handler for bring picture according to path
    /// </summary>
    public class GetPictureHandler : IHttpHandler
    {
        // duration of cache
        private int _CacheExpirationDays = 30;
        // FileInfo of the image requested
        private FileInfo _ImageFile = null;

        // Override the ProcessRequest method.
        public void ProcessRequest(HttpContext context)
        {
            // the image path
            string imagePath;
            // the image type
            string imageType;

            HttpResponse response = context.Response;
            HttpRequest request = context.Request;

            if (context.Request.QueryString["path"] != null)
            {
                imagePath = context.Request.QueryString["path"];
            }
            else
            {
                throw new ArgumentException("No parameter specified");
            }

            string relativePath = GeServerPath(imagePath);
            // Check validity of image
            if (!String.IsNullOrEmpty(relativePath) && File.Exists(relativePath))
            {
                // Load archive info
                _ImageFile = new FileInfo(imagePath);
            }

            imageType = GetResourceType(imagePath);

            byte[] data = GetCache(context, imagePath);
            if (data != null)
            {
                // Serve cached content
                context.Response.BinaryWrite(data);
                return;
            }

            //if the file is exist
            if (File.Exists(relativePath))
            {
                // we don't find the image in the cahce so retrieve it
                using (FileStream fs = new FileStream(relativePath, FileMode.Open))
                {
                    context.Response.ContentType = imageType;
                    byte[] bytesRead = new byte[fs.Length];
                    fs.Read(bytesRead, 0, (int)fs.Length);
                    context.Response.BinaryWrite(bytesRead);

                    // Set cache headers
                    SetResponseCache(response);

                    // Store content in output cache
                    SetCache(context, imagePath, bytesRead);
                }
            }


        }

        /// <summary>
        /// get server path 
        /// </summary>
        /// <param name="imagePath">image path</param>
        /// <returns>true </returns>
        private string GeServerPath(string imagePath)
        {
            string relativePath = "";
            if (!string.IsNullOrEmpty(imagePath))
            {
               char firstCharacter = imagePath[0];
                switch (firstCharacter)
                {
                    case '/':
                        relativePath = "~" + imagePath;
                        break;
                    case '~':
                        relativePath = imagePath;
                        break;
                    default:
                        relativePath = "~/" + imagePath;
                        break;
                }
                string phisicalPath = RenderingContext.CurrentHttpContext.Server.MapPath(relativePath);
                if (File.Exists(phisicalPath))
                {
                    relativePath = phisicalPath;
                }
            }
            return relativePath;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Store content in cache.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="resourcePath">The resource path.</param>
        /// <param name="data">The data.</param>
        /// <param name="isCompressed">if set to <c>true</c> [is compressed].</param>
        private void SetCache(HttpContext context, string resourcePath, object data)
        {
            context.Cache.Insert(resourcePath, data, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(_CacheExpirationDays));
        }

        /// <summary>
        /// Sets the response cache headers for aggressive caching.
        /// </summary>
        /// <param name="context">The context.</param>
        private void SetResponseCache(HttpResponse response)
        {
            HttpCachePolicy cache = response.Cache;
            DateTime universalTime = _ImageFile.LastWriteTimeUtc;
            DateTime dateTime1 = DateTime.Now.ToUniversalTime().AddSeconds(-1);
            if (universalTime > dateTime1)
            {
                universalTime = dateTime1;
            }
            cache.SetLastModified(universalTime);
            cache.SetOmitVaryStar(true);
            cache.SetExpires(DateTime.UtcNow.AddDays(365));
            cache.SetMaxAge(TimeSpan.FromDays(365));
            cache.SetValidUntilExpires(true);
            cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            cache.SetCacheability(HttpCacheability.Public);
            cache.SetLastModifiedFromFileDependencies();
        }
        /// <summary>
        /// Get cached content
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="resourcePath">The resource path.</param>
        /// <param name="isCompressed">if set to <c>true</c> [is compressed].</param>
        /// <returns></returns>
        private byte[] GetCache(HttpContext context, string resourcePath)
        {
            // Look for cached content
            return context.Cache[resourcePath] as byte[];
        }




        /// <summary>
        /// Gets the type of the resource.
        /// </summary>
        /// <param name="requestedResourcePath">The requested resource path.</param>
        /// <returns></returns>
        private static string GetResourceType(string requestedResourcePath)
        {
            // Get required resource extension
            string resourceExtension = Path.GetExtension(requestedResourcePath).ToLower();
            switch (resourceExtension)
            {
                case ".js":
                    return "text/javascript";
                case ".css":
                    return "text/css";
                case ".swf":
                    return "application/x-shockwave-flash";
                case ".gif":
                    return "image/gif";
                case ".png":
                    return "image/png";
                case ".jpg":
                case ".jpeg":
                    return "image/jpg";
            }

            // unknown type
            return null;
        }
    }
}
