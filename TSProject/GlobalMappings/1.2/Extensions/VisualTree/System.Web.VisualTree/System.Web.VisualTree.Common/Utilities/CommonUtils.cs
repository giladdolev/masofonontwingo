﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    public static class CommonUtils
    {
        /// <summary>
        /// Copies the event handlers.
        /// </summary>
        /// <param name="sourceObject">The source object.</param>
        /// <param name="targetObject">The target object.</param>
        /// <param name="eventName">Name of the event.</param>
        public static void CopyEventHandlers(object sourceObject, object targetObject, string eventName)
        {
            if (sourceObject != null && targetObject != null)
            {
                FieldInfo fieldInfo = sourceObject.GetType().GetField(eventName, BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
                if (fieldInfo != null)
                {
                    Delegate eventHandlerList = fieldInfo.GetValue(sourceObject) as Delegate;
                    if (eventHandlerList != null)
                    {
                        Delegate[] eventHandlers = eventHandlerList.GetInvocationList();

                        EventInfo targetEvent = targetObject.GetType().GetEvent(eventName);
                        if (targetEvent != null)
                        {
                            foreach (Delegate eventHandler in eventHandlers)
                            {
                                targetEvent.AddEventHandler(targetObject, eventHandler);
                            }
                        }
                    }
                }
            }
        }
    }
}
