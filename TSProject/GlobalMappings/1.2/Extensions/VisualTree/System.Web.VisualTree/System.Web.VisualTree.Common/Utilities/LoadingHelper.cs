﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    public class LoadingHelper
    {
        /// <summary>
        /// Combines the catalog.
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="mainLoader">The main loader.</param>
        /// <param name="catalogFolder">The catalog folder.</param>
        public static void CombineCatalog(HashSet<Assembly> assemblies, object mainLoader, string catalogFolder = null)
        {
            // An aggregate catalog that combines multiple catalogs
            AggregateCatalog catalog = new AggregateCatalog();

            // Add Folder based catalog
            if (assemblies != null)
            {
                foreach (Assembly assembly in assemblies)
                {
                    catalog.Catalogs.Add(new AssemblyCatalog(assembly));
                }
            }
            else
            {
                // Get current assembly
                Assembly executingAssembly = Assembly.GetCallingAssembly();

                // Adds all the parts found in the same assembly as the Program class
                catalog.Catalogs.Add(new AssemblyCatalog(executingAssembly));

                // Build catalog folder full path
                string runtimeFolder = executingAssembly.CodeBase;
                runtimeFolder = runtimeFolder.Replace(@"file:///", String.Empty);
                runtimeFolder = Path.GetDirectoryName(runtimeFolder);
                if (!String.IsNullOrEmpty(catalogFolder))
                {
                    runtimeFolder = Path.Combine(runtimeFolder, catalogFolder);
                }

                // Add Folder based catalog
                Trace.WriteLine("Registering runtimeFolder " + runtimeFolder);
                catalog.Catalogs.Add(new DirectoryCatalog(runtimeFolder));
            }

            //Create the CompositionContainer with the parts in the catalog
            CompositionContainer container = new CompositionContainer(catalog);

            //Fill the imports of this object
            try
            {
                container.ComposeParts(mainLoader);
            }
            catch (Exception compositionException)
            {
                throw;
            }
        }
    }
}
