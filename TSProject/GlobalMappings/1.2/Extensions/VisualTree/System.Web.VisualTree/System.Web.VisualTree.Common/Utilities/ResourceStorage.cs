﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    /// <summary>
    /// Provides support for different resource storages
    /// </summary>
    internal abstract class ResourceStorage : List<ResourceStorageEntity>
    {
        /// <summary>
        /// The resource DLLS
        /// </summary>
        private static readonly string[] _assemblyNames = new string[] {
            "System.Web.VisualTree.Rendering.ExtJS"
        };

        /// <summary>
        /// Reads the entity bytes.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        internal abstract byte[] ReadEntityBytes(ResourceStorageEntity entity);

        /// <summary>
        /// Gets the last update time.
        /// </summary>
        /// <value>
        /// The last update time.
        /// </value>
        public abstract DateTime LastUpdateTime { get; }

        /// <summary>
        /// Loads the storages.
        /// </summary>
        /// <param name="rootDirectory">The root directory.</param>
        /// <param name="storages">The storages.</param>
        internal static void LoadStorages(string rootDirectory, List<ResourceStorage> storages)
        {
            // Loop all names
            foreach (string assemblyName in _assemblyNames)
            {
                // Get the assembly storage
                ResourceStorage storage = DllResourceStorage.Load(assemblyName);

                // If there is a valid storage
                if(storage != null)
                {
                    // Add to storages
                    storages.Add(storage);
                }
            }

            // Get the archives directory
            string archivesDirectory = Path.Combine(rootDirectory, "ResourceArchives");

            // If there is a valid archives directory
            if (Directory.Exists(archivesDirectory))
            {
                
            }
        }

        /// <summary>
        /// Gets the relative path.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        internal abstract string GetRelativePath(string name);
    }

    /// <summary>
    /// Provides support for resources storages
    /// </summary>
    internal class ZipResourceStorage : ResourceStorage
    {

        /// <summary>
        /// The archive file name
        /// </summary>
        private readonly string _archiveFileName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ZipResourceStorage"/> class.
        /// </summary>
        /// <param name="archiveFileName">The archive file name.</param>
        public ZipResourceStorage(string archiveFileName)
        {
            _archiveFileName = archiveFileName;
        }

        /// <summary>
        /// Gets the last update time.
        /// </summary>
        /// <value>
        /// The last update time.
        /// </value>
        public override DateTime LastUpdateTime
        {
            get
            {
                return File.GetLastWriteTimeUtc(_archiveFileName);
            }
        }

        /// <summary>
        /// Reads the entity bytes.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        internal override byte[] ReadEntityBytes(ResourceStorageEntity entity)
        {
            byte[] data = null;

            // Look for requested resource in archive
            using (FileStream zipToOpen = new FileStream(_archiveFileName, FileMode.Open, FileAccess.Read))
            {
                // Initialize archive stream
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Read))
                {
                    // Scan entries
                    foreach (ZipArchiveEntry archiveEntry in archive.Entries)
                    {
                        // Check if entry name match to requested resource
                        if (String.Equals(archiveEntry.FullName, entity.Name, StringComparison.OrdinalIgnoreCase))
                        {
                            // Get resource stream
                            using (Stream resourceStream = archiveEntry.Open())
                            {
                                // Prepare buffer for content
                                int contentLength = (int)archiveEntry.Length;
                                data = new byte[contentLength];

                                // Read resource content
                                resourceStream.Read(data, 0, contentLength);


                            }
                        }
                    }
                }
            }

            return data;
        }

        /// <summary>
        /// Gets the relative path.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        internal override string GetRelativePath(string name)
        {
            return name;
        }
    }

    /// <summary>
    /// Provides support for 
    /// </summary>
    internal class DllResourceStorage : ResourceStorage
    {
        /// <summary>
        /// The assembly
        /// </summary>
        private readonly Assembly _assembly = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="DllResourceStorage"/> class.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        public DllResourceStorage(Assembly assembly)
        {
            _assembly = assembly;
        }

        /// <summary>
        /// Gets the last update time.
        /// </summary>
        /// <value>
        /// The last update time.
        /// </value>
        public override DateTime LastUpdateTime
        {
            get
            {
                return File.GetLastWriteTimeUtc(_assembly.Location);
            }
        }

        /// <summary>
        /// Reads the entity bytes.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        internal override byte[] ReadEntityBytes(ResourceStorageEntity entity)
        {
            byte[] data = null;

            // Open resource stream
            using (Stream resourceStream = _assembly.GetManifestResourceStream(entity.Name))
            {
                // If there is a valid resource stream
                if (resourceStream != null)
                {
                    // Prepare buffer for content
                    int contentLength = (int)resourceStream.Length;
                    data = new byte[contentLength];

                    // Read resource content
                    resourceStream.Read(data, 0, contentLength);

                }

            }

            return data;
        }

        /// <summary>
        /// Gets the relative path.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        internal override string GetRelativePath(string name)
        {
            if(name != null)
            {
                AssemblyName assemblyName = _assembly.GetName();

                if(assemblyName != null)
                {
                    name = name.Substring(assemblyName.Name.Length);

                }


                string extension = Path.GetExtension(name);

                name = Path.GetFileNameWithoutExtension(name);

                name = name.Replace('.', '\\');

                return string.Concat(name, extension);
            }

            return null;
        }

        /// <summary>
        /// Provides entity for DLL entity
        /// </summary>
        private class DLLResourceStorageEntity : ResourceStorageEntity
        {
            /// <summary>
            /// The path
            /// </summary>
            private readonly string _path;

            /// <summary>
            /// Initializes a new instance of the <see cref="DLLResourceStorageEntity"/> class.
            /// </summary>
            /// <param name="storage">The storage.</param>
            /// <param name="name">The name.</param>
            public DLLResourceStorageEntity(ResourceStorage storage, string name, string path)
                : base(storage, name)
            {
                _path = path;
            }

            /// <summary>
            /// Gets the relative path.
            /// </summary>
            /// <value>
            /// The relative path.
            /// </value>
            public override string RelativePath
            {
                get
                {
                    return _path;
                }
            }

        }

        /// <summary>
        /// Loads the DLL storage.
        /// </summary>
        /// <param name="assemblyName">The assembly name.</param>
        /// <returns></returns>
        internal static ResourceStorage Load(string assemblyName)
        {
            DllResourceStorage resourceStorage = null;

            // Get the current DLL
            Assembly assembly = Assembly.Load(assemblyName);

            // If there is a valid assembly
            if (assembly != null)
            {
                // Create resource storage
                resourceStorage = new DllResourceStorage(assembly);

                // Get resource names
               HashSet<string> resourceNames = new HashSet<string>(assembly.GetManifestResourceNames());

                // The default namespace
                string defaultNamespace = null;

                // The resource assembly name
                const string resourceAssemblyName = ".resourceassembly.txt";

                // Loop all resource names
                foreach (string resourceName in resourceNames)
                {
                    // If is the resource assembly resource
                    if (resourceName.EndsWith(resourceAssemblyName))
                    {
                        // Set the default assembly name
                        defaultNamespace = string.Concat(resourceName.Replace(resourceAssemblyName, ""), ".");

                        break;
                    }
                }

                // If there is a valid namespace
                if (!string.IsNullOrEmpty(defaultNamespace))
                {
                    // The resource directory name
                    const string resourceDirectoryName = ".resourcedirectory.txt";

                    // Loop all resource names
                    foreach (string resourceName in resourceNames)
                    {
                        // If is a resource directory name
                        if (resourceName.EndsWith(resourceDirectoryName))
                        {
                            // Get the directory path
                            string directoryPath = resourceName.Substring(0, resourceName.Length - resourceDirectoryName.Length);

                            // Get the relative path
                            string relativePath = directoryPath.Replace(defaultNamespace, "").Replace('.','\\');

                            // Loop all resource names
                            foreach (string fileName in GetAssemblyResourceLines(assembly, resourceName))                            
                            {
                                // Get full file name
                                string fileResourceName = string.Concat(directoryPath, ".", fileName);

                                // If there is a valid resource
                                if(resourceNames.Contains(fileResourceName))
                                {
                                    // Add resource
                                    resourceStorage.Add(new DLLResourceStorageEntity(resourceStorage, fileResourceName, Path.Combine(relativePath, fileName)));
                                }
                            }
                        }
                    }

                    
                }
            }

            return resourceStorage;
        }

        /// <summary>
        /// Gets the assembly resource lines.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="resourceName">The resource name.</param>
        /// <returns></returns>
        private static string[] GetAssemblyResourceLines(Assembly assembly, string resourceName)
        {
            List<string> lines = new List<string>();

            // Get resource stream
            using(Stream resourceStream = assembly.GetManifestResourceStream(resourceName))
            {
                // Create stream reader
                StreamReader reader = new StreamReader(resourceStream);

                // The line variable
                string line = null;

                // Read all lines
                while((line = reader.ReadLine())!= null)
                {
                    // Add current line
                    lines.Add(line);
                }


            }

            return lines.ToArray();
        }
    }


    internal class FileSystemResourceStorage : ResourceStorage
    {

        /// <summary>
        /// The file storage
        /// </summary>
        private static FileSystemResourceStorage _fileStorage = new FileSystemResourceStorage();

        /// <summary>
        /// 
        /// </summary>
        private class FileSystemResourceStorageEntity : ResourceStorageEntity
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="FileSystemResourceStorageEntity" /> class.
            /// </summary>
            /// <param name="storage">The storage.</param>
            /// <param name="path">The path.</param>
            public FileSystemResourceStorageEntity(ResourceStorage storage, string path)
                : base(storage, path)
            {
            }

            /// <summary>
            /// Gets the last update time.
            /// </summary>
            /// <value>
            /// The last update time.
            /// </value>
            public override DateTime LastUpdateTime
            {
                get
                {
                    return File.GetLastWriteTimeUtc(this.Name);
                }
            }

            /// <summary>
            /// Reads the bytes.
            /// </summary>
            /// <returns></returns>
            internal override byte[] ReadBytes()
            {
                return File.ReadAllBytes(this.Name);
            }

            /// <summary>
            /// Gets the relative path.
            /// </summary>
            /// <value>
            /// The relative path.
            /// </value>
            public override string RelativePath
            {
                get
                {
                    return this.Name;
                }
            }
        }

        /// <summary>
        /// Reads the entity bytes.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        internal override byte[] ReadEntityBytes(ResourceStorageEntity entity)
        {
            return File.ReadAllBytes(entity.Name);
        }

        /// <summary>
        /// Gets the last update time.
        /// </summary>
        /// <value>
        /// The last update time.
        /// </value>
        public override DateTime LastUpdateTime
        {
            get
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Gets the relative path.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        internal override string GetRelativePath(string name)
        {
            return name;
        }

        /// <summary>
        /// Froms the file.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="virtualResourcePath">The virtual resource path.</param>
        /// <returns></returns>
        internal static ResourceStorageEntity FromFile(HttpContext context, string virtualResourcePath)
        {
            // If there is a valid context
            if(context != null && virtualResourcePath != null )
            {
                // Get actual file path
                string file = context.Server.MapPath(virtualResourcePath);

                // If file exists
                if(File.Exists(file))
                {
                    // Create storage entity
                    return new FileSystemResourceStorageEntity(_fileStorage, file);
                }
            }

            return null;
        }

    }
    /// <summary>
    /// Provides support for tracking storage resource entities
    /// </summary>
    internal abstract class ResourceStorageEntity
    {

        /// <summary>
        /// The storage
        /// </summary>
        private readonly ResourceStorage _storage = null;

        /// <summary>
        /// The name
        /// </summary>
        private readonly string _name = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceStorageEntity"/> class.
        /// </summary>
        /// <param name="storage">The storage.</param>
        /// <param name="name">The name.</param>
        public ResourceStorageEntity(ResourceStorage storage, string name)
        {
            this._storage = storage;
            this._name = name;
        }

        /// <summary>
        /// Gets the last update time.
        /// </summary>
        /// <value>
        /// The last update time.
        /// </value>
        public virtual DateTime LastUpdateTime
        {
            get
            {
                return _storage.LastUpdateTime;
            }
        }

        /// <summary>
        /// Reads the bytes.
        /// </summary>
        /// <returns></returns>
        internal virtual byte[] ReadBytes()
        {
            return _storage.ReadEntityBytes(this);
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _name;
            }
        }

        /// <summary>
        /// Gets the relative path.
        /// </summary>
        /// <value>
        /// The relative path.
        /// </value>
        public abstract string RelativePath
        {
            get;
        }

        
    }

    
}
