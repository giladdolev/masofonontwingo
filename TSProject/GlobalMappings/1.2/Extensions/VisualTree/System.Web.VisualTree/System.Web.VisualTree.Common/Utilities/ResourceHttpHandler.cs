﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Caching;

namespace System.Web.VisualTree.Common
{
    public class ResourceHttpHandler : IHttpHandler
    {

        /// <summary>
        /// The storages lock
        /// </summary>
        private static object _storagesLock = new object();

        /// <summary>
        /// The storages
        /// </summary>
        private static ResourceStorage[] _storages = null;

        /// <summary>
        /// The files to storage
        /// </summary>
        private static Dictionary<string, ResourceStorageEntity> _files = new Dictionary<string, ResourceStorageEntity>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// The compress resources
        /// </summary>
        private bool _compressResources = true;

        /// <summary>
        /// The cache expiration days
        /// </summary>
        private int _cacheExpirationDays = 90;

        private bool? _outputCacheDisabled;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceHttpHandler"/> class.
        /// </summary>
        public ResourceHttpHandler()
        {

        }

        public bool OutputCacheDisabled
        {
            get
            {
                if (!_outputCacheDisabled.HasValue)
                {
                    bool blnOutputCacheDisabled;

                    // disable cache if web.config key is defined and value set to "true"
                    bool.TryParse(ConfigurationManager.AppSettings["DisableResourceHttpHandlerOutputCache"], out blnOutputCacheDisabled);

                    _outputCacheDisabled = blnOutputCacheDisabled;
                }
                return _outputCacheDisabled.Value;
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void EnsureInitialized(HttpContext context)
        {
            // If we need to initialize storages
            if (_storages == null)
            {
                // Lock access to storages
                lock (_storagesLock)
                {
                    // If storages is still null
                    if (_storages == null)
                    {
                        // Ensure files are empty
                        _files.Clear();

                        // Create list of storages
                        List<ResourceStorage> storages = new List<ResourceStorage>();

                        // Get the root directory
                        string rootDirectory = context.Server.MapPath("/");

                        // Load the storages
                        ResourceStorage.LoadStorages(rootDirectory, storages);

                        // Create the 
                        _files = new Dictionary<string, ResourceStorageEntity>();

                        // Loop all storages
                        foreach (ResourceStorage storage in storages)
                        {
                            // If there is a valid storage
                            if (storage != null)
                            {
                                // Loop all storage entities
                                foreach (ResourceStorageEntity storageEntity in storage)
                                {
                                    // Get relative path
                                    string relativePath = storageEntity.RelativePath;

                                    // If there is a valid relative path
                                    if (!string.IsNullOrEmpty(relativePath))
                                    {
                                        relativePath = relativePath.Replace('\\', '/');

                                        if (!relativePath.StartsWith("/"))
                                        {
                                            relativePath = string.Concat("/", relativePath);
                                        }
                                        // Set the entity
                                        _files[relativePath] = storageEntity;
                                    }
                                }
                            }
                        }
                        // Set the storages
                        _storages = storages.ToArray();
                    }
                }
            }
        }

        #region IHttpHandler Members

        /// <summary>
        /// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler" /> instance.
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler" /> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext" /> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
        public virtual void ProcessRequest(HttpContext context)
        {
            // Ensure the handler is initialized
            this.EnsureInitialized(context);

            // Get the http response
            HttpResponse response = context.Response;

            // Get the http request 
            HttpRequest request = context.Request;

            // Get the virtual resource path
            string virtualResourcePath = request.Path;

            ResourceStorageEntity resourceStorageEntity = null;

            // Check that requested virtual resource is registered
            if (_files.TryGetValue(virtualResourcePath, out resourceStorageEntity))
            {
                // Write the resource entity
                this.WriteResourceEntity(context, response, request, virtualResourcePath, resourceStorageEntity);
            }
            else
            {
                // Write file if exists
                this.WriteResourceEntity(context, response, request, virtualResourcePath, FileSystemResourceStorage.FromFile(context, virtualResourcePath));
            }
        }

        /// <summary>
        /// Writes the resource entity.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="response">The response.</param>
        /// <param name="request">The request.</param>
        /// <param name="virtualResourcePath">The virtual resource path.</param>
        /// <param name="resourceStorageEntity">The resource storage entity.</param>
        private void WriteResourceEntity(HttpContext context, HttpResponse response, HttpRequest request, string virtualResourcePath, ResourceStorageEntity resourceStorageEntity)
        {
            // If there is a valid resource storage entity
            if (resourceStorageEntity != null)
            {
                // Check modification for cached file
                if (!IsSourceModified(request, resourceStorageEntity))
                {
                    // Respond with 304 - Not Modified
                    response.SuppressContent = true;
                    response.StatusCode = 304;
                    response.StatusDescription = "Not Modified";
                    response.AddHeader("Content-Length", "0");
                }
                else
                {
                    // Get requested resource type
                    string resourceType = GetResourceType(virtualResourcePath);

                    // If there is a valid resource type
                    if (!String.IsNullOrEmpty(resourceType))
                    {
                        // Check if compression configured and requested
                        bool isCompressionRequired = IsCompressionRequired(request, resourceType);

                        // Check for cached content
                        byte[] data = GetCache(context, virtualResourcePath, isCompressionRequired);

                        // If there is a valid data array
                        if (data != null)
                        {
                            // Serve cached content
                            WriteData(response, data, resourceType, isCompressionRequired);
                        }
                        else
                        {
                            // Read entity data
                            data = resourceStorageEntity.ReadBytes();

                            // If there is valid data
                            if (data != null)
                            {
                                // Serve retrieved content
                                WriteData(response, data, resourceType, isCompressionRequired);

                                // Set cache headers
                                SetResponseCache(response, resourceStorageEntity);

                                // Store content in output cache
                                SetCache(context, virtualResourcePath, data, isCompressionRequired);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether source modified since date sent by specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="resourceStorageEntity">The resource storage entity.</param>
        /// <returns></returns>
        private bool IsSourceModified(HttpRequest request, ResourceStorageEntity resourceStorageEntity)
        {
            DateTime universalTime;

            // Check for If-Modified-Since request header
            if (DateTime.TryParse(request.Headers["If-Modified-Since"] ?? string.Empty, out universalTime))
            {
                // Get archive modification date
                DateTime universalLastWriteTime = resourceStorageEntity.LastUpdateTime;

                // Compare date sent by request with archive modification date
                if (universalLastWriteTime <= universalTime || (universalLastWriteTime - universalTime) < TimeSpan.FromSeconds(1))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Get cached content
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="resourcePath">The resource path.</param>
        /// <param name="isCompressed">if set to <c>true</c> [is compressed].</param>
        /// <returns></returns>
        private byte[] GetCache(HttpContext context, string resourcePath, bool isCompressed)
        {
            // Check if output cache is disabled.
            if (this.OutputCacheDisabled)
            {
                return null;
            }
            else
            {
                // Look for cached content
                return context.Cache[GetCacheKey(resourcePath, isCompressed)] as byte[];
            }
        }

        /// <summary>
        /// Gets the cache key.
        /// </summary>
        /// <param name="fullResourcePath">The full resource path.</param>
        /// <param name="isCompressed">if set to <c>true</c> [is compressed].</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private string GetCacheKey(string fullResourcePath, bool isCompressed)
        {
            return fullResourcePath + (isCompressed ? "_gzip" : String.Empty);
        }

        /// <summary>
        /// Store content in cache.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="resourcePath">The resource path.</param>
        /// <param name="data">The data.</param>
        /// <param name="isCompressed">if set to <c>true</c> compressed.</param>
        private void SetCache(HttpContext context, string resourcePath, object data, bool isCompressed)
        {
            // Set Cached item if not disabled
            if (!this.OutputCacheDisabled)
            {
                context.Cache.Insert(GetCacheKey(resourcePath, isCompressed), data, null, Cache.NoAbsoluteExpiration, TimeSpan.FromDays(_cacheExpirationDays));
            }
        }

        /// <summary>
        /// Sets the response cache headers for aggressive caching.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="resourceStorageEntity">The resource storage entity.</param>
        private void SetResponseCache(HttpResponse response, ResourceStorageEntity resourceStorageEntity)
        {
            // Prevent Caching policy override when cache is disabled
            if (this.OutputCacheDisabled)
                return;

            HttpCachePolicy cache = response.Cache;
            DateTime universalTime = resourceStorageEntity.LastUpdateTime;
            DateTime dateTime1 = DateTime.Now.ToUniversalTime().AddSeconds(-1);
            if (universalTime > dateTime1)
            {
                universalTime = dateTime1;
            }
            cache.SetLastModified(universalTime);
            cache.SetOmitVaryStar(true);
            cache.SetExpires(DateTime.UtcNow.AddDays(365));
            cache.SetMaxAge(TimeSpan.FromDays(365));
            cache.SetValidUntilExpires(true);
            cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            cache.SetCacheability(HttpCacheability.Public);
            cache.SetLastModifiedFromFileDependencies();
        }

        /// <summary>
        /// Determines whether compression required for the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        private bool IsCompressionRequired(HttpRequest request, string resourceType)
        {
            // Check if handler configured for compression
            if (_compressResources)
            {
                switch (resourceType)
                {
                    // Compress text files only
                    case "text/javascript":
                    case "text/css":

                        // Check if compression supported and requested by client
                        return IsCompressionSupported(request);
                }
            }
            return false;
        }

        /// <summary>
        /// Writes the content to response.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="data">The data.</param>
        /// <param name="responseType">Type of the response.</param>
        /// <param name="requireCompression">if set to <c>true</c> require compression.</param>
        protected void WriteData(HttpResponse response, byte[] data, string responseType, bool requireCompression)
        {
            response.Charset = "utf-8";
            if (requireCompression)
            {
                // Set compression header
                response.AppendHeader("Content-Encoding", "gzip");
                data = GZip(data);
            }
            response.ContentType = responseType;
            response.OutputStream.Write(data, 0, data.Length);
        }
        /// <summary>
        /// Gets the type of the resource.
        /// </summary>
        /// <param name="requestedResourcePath">The requested resource path.</param>
        /// <returns></returns>
        private static string GetResourceType(string requestedResourcePath)
        {
            // Get required resource extension
            string resourceExtension = Path.GetExtension(requestedResourcePath);

            // Return mime type by extension
            switch (resourceExtension)
            {
                case ".js":
                    return "text/javascript";
                case ".css":
                    return "text/css";
                case ".swf":
                    return "application/x-shockwave-flash";
                case ".gif":
                    return "image/gif";
                case ".png":
                    return "image/png";
                case ".jpg":
                case ".jpeg":
                    return "image/jpg";
            }

            return null;
        }

        /// <summary>
        /// Determines whether compression supported and requested by client.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public static bool IsCompressionSupported(HttpRequest request)
        {
            if (request.Browser.IsBrowser("IE") && request.Browser.MajorVersion <= 6)
            {
                return false;
            }

            // Check if browser supports encoding
            string acceptEncoding = (request.Headers["Accept-Encoding"] ?? "").ToLowerInvariant();

            // Only gzip supported at this point
            return acceptEncoding.Contains("gzip"); // || acceptEncoding.Contains("deflate");
        }

        /// <summary>
        /// Compress data
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        public static byte[] GZip(byte[] instance)
        {
            MemoryStream memoryStream = new MemoryStream();
            GZipStream gZipStream = new GZipStream(memoryStream, CompressionMode.Compress);
            gZipStream.Write(instance, 0, (int)instance.Length);
            gZipStream.Close();
            return memoryStream.ToArray();
        }

        #endregion
    }
}
