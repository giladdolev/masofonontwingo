﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.CodeGenerator
{
    /// <summary>
    /// Provides support for generating code
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class GeneratorManager
    {
        /// <summary>
        /// The renderer generator providers
        /// </summary>
        private List<GeneratorProvider> _generatorProviders;

        /// <summary>
        /// The class name to generate
        /// </summary>
        private string _className;


        /// <summary>
        /// The class name space to generate
        /// </summary>
        private string _classNamespace;

        /// <summary>
        /// The class metadata to generate
        /// </summary>
        private object[] _metadata;

        /// <summary>
        /// The loaded flag indicating if we loaded the generators
        /// </summary>
        private bool _loaded;

        /// <summary>
        /// The type resolution service
        /// </summary>
        private ITypeResolutionService _typeResolutionService;


        /// <summary>
        /// The file code scope
        /// </summary>
        private List<string> _fileCodeScope;




        /// <summary>
        /// The the list of code generators
        /// </summary>
        private static string[] CODE_GENERATORS = new string[] {
            "System.Web.VisualTree.CodeGenerator.ExtJS.ExtJSRendererGenerator"
        };
        

        /// <summary>
        /// Generates the renderer code.
        /// </summary>
        /// <param name="classNamespace">The class namespace.</param>
        /// <param name="className">The class name.</param>
        /// <param name="metadata">The renderer class metadata.</param>
        /// <returns></returns>
        public GeneratorManager(ITypeResolutionService typeResolutionService, List<string> fileCodeScope, string classNamespace, string className, object[] metadata)
        {
            this._className = className;
            this._classNamespace = classNamespace;
            this._metadata = metadata;
            this._typeResolutionService = typeResolutionService;
            this._fileCodeScope = fileCodeScope;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            // If we need to load generators
            if (!_loaded)
            {
                // Indicate generators loaded
                _loaded = true;

                // Load generators
                this.LoadGenerators();
            }


            // If there are valid generators list
            if (_generatorProviders != null && _generatorProviders.Count > 0)
            {
                // Loop all renderer generator providers
                foreach (GeneratorProvider provider in _generatorProviders)
                {
                    // If there is a valid provider and it can generate code
                    if (provider != null && provider.CanGenerateCode(this._metadata))
                    {
                        // Generate code using provider
                        return provider.GenerateCode(this._fileCodeScope, this._classNamespace, this._className, this._metadata);
                    }
                }

                throw new GeneratorException("Cannot find specific renderer generator provider.");
            }
            else
            {
                throw new GeneratorException("Cannot find renderer generator providers.");
            }
        }

        /// <summary>
        /// Loads the generators.
        /// </summary>
        private void LoadGenerators()
        {
            // Create new list of providers
            _generatorProviders = new List<GeneratorProvider>();

            // If there is a valid type service
            if(_typeResolutionService != null)
            {
                // Loop all code generators
                foreach(string codeGeneratorType in CODE_GENERATORS)
                {
                    // Get the provider type
                    Type providerType = _typeResolutionService.GetType(codeGeneratorType, false);

                    // If there is a valid provider type
                    if (providerType != null)
                    {
                        // Get provider from service
                        GeneratorProvider provider = Activator.CreateInstance(providerType) as GeneratorProvider;

                        // If there is a valid provider
                        if (provider != null)
                        {
                            // Add provider
                            _generatorProviders.Add(provider);
                        }
                    }
                }
            }
        }
    }
}
