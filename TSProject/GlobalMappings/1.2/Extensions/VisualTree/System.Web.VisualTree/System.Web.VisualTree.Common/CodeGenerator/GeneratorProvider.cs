﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.CodeGenerator
{
    /// <summary>
    /// Provides support for rendering code based on metadata.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class GeneratorProvider
    {
        /// <summary>
        /// Determines whether this instance can generate code given the specific metadata.
        /// </summary>
        /// <param name="metadata">The metadata.</param>
        /// <returns></returns>
        public abstract bool CanGenerateCode(object[] metadata);

        /// <summary>
        /// Generate code given the specific metadata and class name.
        /// </summary>
        /// <param name="classNamespace">The class namespace.</param>
        /// <param name="className">The class name.</param>
        /// <param name="metadata">The class metadata.</param>
        /// <returns></returns>
        public abstract string GenerateCode(List<string> fileCodeScope, string classNamespace, string className, object[] metadata);
    }
}
