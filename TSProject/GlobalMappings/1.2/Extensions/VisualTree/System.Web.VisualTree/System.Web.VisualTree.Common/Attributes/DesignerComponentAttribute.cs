﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DesignerComponentAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets a value indicating whether to add partial modifier to generated control class.
        /// </summary>
        /// <value>
        ///   <c>true</c> if should add partial modifier to generated control class; otherwise, <c>false</c>.
        /// </value>
        public bool AddPartial { get; set; }
    }
}
