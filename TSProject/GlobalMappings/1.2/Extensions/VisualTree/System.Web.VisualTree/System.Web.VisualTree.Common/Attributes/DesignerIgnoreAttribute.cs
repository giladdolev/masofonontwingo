﻿namespace System.Web.VisualTree.Common.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public sealed class DesignerIgnoreAttribute : Attribute
    {

    }
}
