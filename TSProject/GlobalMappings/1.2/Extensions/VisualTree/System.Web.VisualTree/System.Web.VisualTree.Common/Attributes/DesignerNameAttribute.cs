﻿namespace System.Web.VisualTree.Common.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public sealed class DesignerNameAttribute : Attribute
    {
        /// <summary>
        /// The name
        /// </summary>
        private readonly string _name;



        /// <summary>
        /// Initializes a new instance of the <see cref="DesignerNameAttribute"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public DesignerNameAttribute(string name)
        {
            _name = name;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return _name; }
        } 

    }
}
