﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.Attributes
{
    [Obsolete("Get the fuck out...")]
    public class RendererTempEventDescriptionAttribute : RendererDescriptionAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RendererTempEventDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="name">The name.</param>
        public RendererTempEventDescriptionAttribute(Type visualElementType, string name)
            : base(visualElementType)
        {
            
        }
    }
}
