﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.Attributes
{
    /// <summary>
    /// Provides support for marking classes as visual tree renderers.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class RendererDescriptionAttribute : Attribute
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RendererDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        public RendererDescriptionAttribute(Type visualElementType)
            : this(visualElementType, RenderingEnvironmentId.Default, null, null)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="RendererDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        public RendererDescriptionAttribute(Type visualElementType, RenderingEnvironmentId renderingEnvironmentId)
            : this(visualElementType, renderingEnvironmentId, null, null)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="RendererDescriptionAttribute" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="name">The description.</param>
        public RendererDescriptionAttribute(Type visualElementType, RenderingEnvironmentId renderingEnvironmentId, string rendererBaseType, string name)
        {
            this.VisualElementType = visualElementType;
            this.RendererBaseType = rendererBaseType;
            this.RenderingEnvironmentId = renderingEnvironmentId;
        }


        /// <summary>
        /// Gets the rendering environment identifier.
        /// </summary>
        /// <value>
        /// The rendering environment identifier.
        /// </value>
        public RenderingEnvironmentId RenderingEnvironmentId { get; private set; }

        /// <summary>
        /// Gets the renderer base type.
        /// </summary>
        /// <value>
        /// The renderer base type.
        /// </value>
        public string RendererBaseType { get; private set; }


        /// <summary>
        /// Gets or sets the type of the visual element.
        /// </summary>
        /// <value>
        /// The type of the visual element.
        /// </value>
        public Type VisualElementType { get; private set; }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is alternative.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is alternative; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(false)]
        public bool IsAlternative
        {
            get;
            set;
        }


        /// <summary>
        /// Gets the renderer key.
        /// </summary>
        /// <value>
        /// The renderer key.
        /// </value>
        public Tuple<RenderingEnvironmentId, Type> RendererKey
        {
            get
            {
                return Tuple.Create(this.RenderingEnvironmentId, this.VisualElementType);
            }
        }

    }
}
