﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.Attributes
{
    /// <summary>
    /// Provides support for marking properties for rendering.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class RendererPropertyDescriptionAttribute : Attribute
    {
    }
}
