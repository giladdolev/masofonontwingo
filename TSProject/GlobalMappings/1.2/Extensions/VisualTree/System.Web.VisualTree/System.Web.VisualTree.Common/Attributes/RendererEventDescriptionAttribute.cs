﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Event)]
    public class RendererEventDescriptionAttribute : Attribute
    {
    }
}
