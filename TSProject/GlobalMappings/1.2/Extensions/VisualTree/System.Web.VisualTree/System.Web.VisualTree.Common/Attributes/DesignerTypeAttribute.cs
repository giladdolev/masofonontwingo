﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public sealed class DesignerTypeAttribute : Attribute
    {
        /// <summary>
        /// The type
        /// </summary>
        private readonly Type _type;



        /// <summary>
        /// Initializes a new instance of the <see cref="DesignerTypeAttribute"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public DesignerTypeAttribute(Type type)
        {
            _type = type;
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public Type Type
        {
            get { return _type; }
        } 
    }
}
