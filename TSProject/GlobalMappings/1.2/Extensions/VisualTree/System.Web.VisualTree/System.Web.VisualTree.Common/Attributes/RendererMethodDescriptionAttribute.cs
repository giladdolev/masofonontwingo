﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common.Attributes
{
    /// <summary>
    /// Provides support for publishing methods
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class RendererMethodDescriptionAttribute : Attribute
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RendererMethodDescriptionAttribute"/> class.
        /// </summary>
        public RendererMethodDescriptionAttribute()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RendererMethodDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="name">The method name.</param>
        public RendererMethodDescriptionAttribute(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RendererMethodDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="name">The method name.</param>
        /// <param name="dataType">The method data type.</param>
        public RendererMethodDescriptionAttribute(string name, Type dataType)
        {
            this.Name = name;
            this.DataType = dataType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RendererMethodDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="dataType">The method data type.</param>
        public RendererMethodDescriptionAttribute(Type dataType)
        {
            this.DataType = dataType;
        }

        /// <summary>
        /// Gets or sets the method name.
        /// </summary>
        /// <value>
        /// The action name.
        /// </value>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the method data type.
        /// </summary>
        /// <value>
        /// The method data type.
        /// </value>
        public Type DataType
        {
            get;
            set;
        }
    }
}
