﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    /// <summary>
    /// Provides support for defining the rendering environment context
    /// </summary>
    public class RenderingEnvironment
    {

        /// <summary>
        /// The ext JS
        /// </summary>
        public static readonly RenderingEnvironment ExtJS = new RenderingEnvironment(RenderingEnvironmentId.ExtJS);

        /// <summary>
        /// The default
        /// </summary>
        public static readonly RenderingEnvironment Default = RenderingEnvironment.ExtJS;

        /// <summary>
        /// The ext JS touch
        /// </summary>
        public static readonly RenderingEnvironment ExtJSTouch = new RenderingEnvironment(RenderingEnvironmentId.ExtJSTouch, RenderingEnvironment.ExtJS);

        /// <summary>
        /// The rendering environment
        /// </summary>
        private readonly RenderingEnvironment _parentRenderingEnvironment;

        /// <summary>
        /// The environment identifier
        /// </summary>
        private readonly RenderingEnvironmentId _environmentId;


        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingEnvironment" /> class.
        /// </summary>
        /// <param name="environmentId">The environment identifier.</param>
        /// <param name="parentRenderingEnvironment">The rendering environment.</param>
        public RenderingEnvironment(RenderingEnvironmentId environmentId, RenderingEnvironment parentRenderingEnvironment = null)
        {
            _parentRenderingEnvironment = parentRenderingEnvironment;
            _environmentId = environmentId;
        }

        /// <summary>
        /// Gets the parent environment.
        /// </summary>
        /// <value>
        /// The parent environment.
        /// </value>
        public RenderingEnvironment ParentEnvironment
        {
            get
            {
                return _parentRenderingEnvironment;
            }
        }


        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public RenderingEnvironmentId Id
        {
            get
            {
                return _environmentId;
            }
        }

        /// <summary>
        /// Gets the rendering environment by identifier.
        /// </summary>
        /// <param name="renderingEnvironmentId">The rendering environment identifier.</param>
        /// <returns></returns>
        public static RenderingEnvironment GetRenderingEnvironmentById(RenderingEnvironmentId renderingEnvironmentId)
        {
            // Choose environment by id
            switch (renderingEnvironmentId)
            {
                case RenderingEnvironmentId.ExtJS:
                    return RenderingEnvironment.ExtJS;
                case RenderingEnvironmentId.ExtJSTouch:
                    return RenderingEnvironment.ExtJSTouch;
                default:
                case RenderingEnvironmentId.Default:
                    return RenderingEnvironment.Default;
            }
        }

        /// <summary>
        /// Gets the rendering environment.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <returns></returns>
        public static RenderingEnvironment GetRenderingEnvironment(RenderingContext renderingContext)
        {
            if (renderingContext != null)
            {
                return renderingContext.Environment;
            }
            return RenderingEnvironment.Default;
        }

        /// <summary>
        /// Gets the rendering environment identifier.
        /// </summary>
        /// <param name="renderingEnvironment">The rendering environment.</param>
        /// <returns></returns>
        public static RenderingEnvironmentId GetRenderingEnvironmentId(RenderingEnvironment renderingEnvironment)
        {
            // If there is a valid rendering environment
            if (renderingEnvironment != null)
            {
                return renderingEnvironment.Id;
            }

            return RenderingEnvironmentId.Default;
        }


    }


    /// <summary>
    /// Provides support for referencing rendering environments
    /// </summary>
    public enum RenderingEnvironmentId
    {
        /// <summary>
        /// The default rendering environment
        /// </summary>
        Default,

        /// <summary>
        /// The ext JS rendering environment
        /// </summary>
        ExtJS,

        /// <summary>
        /// The ext JS touch rendering environment
        /// </summary>
        ExtJSTouch
    }
}
