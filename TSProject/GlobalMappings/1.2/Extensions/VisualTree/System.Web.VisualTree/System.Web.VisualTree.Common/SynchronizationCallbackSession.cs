﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    internal class SynchronizationCallbackSession : IDisposable
    {
        /// <summary>
        /// The synchronized synchronization context
        /// </summary>
        private readonly SynchronizedSynchronizationContext mobjSynchronizedSynchronizationContext = null;

        /// <summary>
        /// The previous synchronization callback session
        /// </summary>
        private readonly SynchronizationCallbackSession mobjPreviousSynchronizationCallbackSession = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationCallbackSession"/> class.
        /// </summary>
        /// <param name="objSynchronizedSynchronizationContext">The synchronized synchronization context.</param>
        internal SynchronizationCallbackSession(SynchronizedSynchronizationContext objSynchronizedSynchronizationContext)
        {
            mobjSynchronizedSynchronizationContext = objSynchronizedSynchronizationContext;
            mobjPreviousSynchronizationCallbackSession = objSynchronizedSynchronizationContext.CallbackSession;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        void IDisposable.Dispose()
        {
            mobjSynchronizedSynchronizationContext.CallbackSession = mobjPreviousSynchronizationCallbackSession;
        }

        /// <summary>
        /// Gets or sets the exception.
        /// </summary>
        /// <value>
        /// The exception.
        /// </value>
        public Exception Exception { get; set; }



        /// <summary>
        /// Creates the synchronization callback session.
        /// </summary>
        /// <returns></returns>
        internal static SynchronizationCallbackSession Create()
        {
            SynchronizedSynchronizationContext objSynchronizedSynchronizationContext = SynchronizationContext.Current as SynchronizedSynchronizationContext;

            if (objSynchronizedSynchronizationContext != null)
            {
                return objSynchronizedSynchronizationContext.CallbackSession = new SynchronizationCallbackSession(objSynchronizedSynchronizationContext);
            }

            return null;
        }
    }
}
