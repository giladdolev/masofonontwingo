﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    public interface IVisualElement
    {
        /// <summary>
        /// Gets the application identifier.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        string ApplicationId
        {
            get;
        }
    }

    /// <summary>
    /// Provides support for marking elements as internal implementation
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IInternalImplementationElement
    {

    }

    /// <summary>
    /// The visual element collection change target
    /// </summary>
    public interface IVisualElementCollectionChangeTarget
    {

        /// <summary>
        /// Notifies the change.
        /// </summary>
        /// <param name="actionsContainer">The actions container.</param>
        /// <param name="targetElement">The target element.</param>
        void NotifyChange(IVisualElementCollectionChangeActionsContainer actionsContainer, IVisualElement targetElement);
    }

    /// <summary>
    /// The visual element collection actions
    /// </summary>
    public interface IVisualElementCollectionChangeActionsContainer
    {
        /// <summary>
        /// Gets the change actions.
        /// </summary>
        /// <value>
        /// The change actions.
        /// </value>
        IEnumerable<IVisualElementCollectionChangeAction> ChangeActions
        {
            get;
        }

        /// <summary>
        /// Clears the change actions.
        /// </summary>
        void ClearChangeActions();
    }

    /// <summary>
    /// The visual element collection action
    /// </summary>
    public interface IVisualElementCollectionChangeAction
    {
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        VisualElementCollectionChangeActionType Type
        {
            get;
        }

        /// <summary>
        /// Message Getter
        /// </summary>
        string Msg { get; }

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        IVisualElement Item
        {
            get;
        }

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        int Index
        {
            get;
        }

        /// <summary>
        /// Gets the collection length.
        /// </summary>
        /// <value>
        /// The collection length.
        /// </value>
        int Length
        {
            get;
        }
    }

    /// <summary>
    /// The visual element collection change action type
    /// </summary>
    public enum VisualElementCollectionChangeActionType
    {
        ClearItems,

        SetItem,

        InsertItem,

        RemoveItem
    }
}
