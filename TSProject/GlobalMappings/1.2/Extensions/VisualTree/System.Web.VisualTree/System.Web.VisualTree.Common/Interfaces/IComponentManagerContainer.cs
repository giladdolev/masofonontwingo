﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    /// <summary>
    /// Provides support for marking an element as a client element
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IComponentManagerContainer
    {
        /// <summary>
        /// Registers the component.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        void RegisterComponent(IVisualElement visualElement);

        /// <summary>
        /// Gets the component elements.
        /// </summary>
        /// <value>
        /// The component elements.
        /// </value>
        IEnumerable<IVisualElement> Elements { get;}
    }
}
