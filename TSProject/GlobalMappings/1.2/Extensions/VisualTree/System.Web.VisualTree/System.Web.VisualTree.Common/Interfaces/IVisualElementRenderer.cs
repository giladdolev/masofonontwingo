﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    
    public interface IVisualElementRenderer
    {
        /// <summary>
        /// Revises the renderer.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        IVisualElementRenderer ReviseRenderer(RenderingContext renderingContext, IVisualElement visualElement);
        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="renderTo">The render to.</param>
        /// <returns></returns>
        string Render(RenderingContext renderingContext, IVisualElement visualElement, string renderTo = null);
        /// <summary>
        /// Renders the result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="request">The http request.</param>
        /// <returns></returns>
        IVisualElementRenderer RenderResult(RenderingContext renderingContext, IVisualElement visualElement, HttpRequestBase request = null);
        /// <summary>
        /// Renders the resource result.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        IVisualElementRenderer RenderResourceResult(RenderingContext renderingContext, IVisualElement visualElement, string resourceId);
        /// <summary>
        /// Enables processing of the result of an action method by a custom type that inherits from the <see cref="T:System.Web.Mvc.ActionResult" /> class.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="httpContext">The HTTP context in which the result is executed.</param>
        void ExecuteResult(RenderingContext renderingContext, HttpContextBase httpContext);

    }
}
