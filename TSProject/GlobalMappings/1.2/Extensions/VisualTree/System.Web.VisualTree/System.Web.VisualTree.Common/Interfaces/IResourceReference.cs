﻿using System;
namespace System.Web.VisualTree.Common
{
    
    public interface IResourceReference
    {
        /// <summary>
        /// Gets or sets the resource source moniker.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        string Source { get; set; }
    }
}
