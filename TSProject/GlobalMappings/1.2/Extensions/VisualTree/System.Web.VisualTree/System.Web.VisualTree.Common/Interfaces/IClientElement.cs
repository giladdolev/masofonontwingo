﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace System.Web.VisualTree.Common
{
    /// <summary>
    /// Provides support for marking an element as a client element
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IClientElement
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string ClientID
        {
            get;
        }

        /// <summary>
        /// Gets or sets the parent element.
        /// </summary>
        /// <value>
        /// The parent element.
        /// </value>
        IClientElement ParentElement
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this element is the application.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is the application; otherwise, <c>false</c>.
        /// </value>
        bool IsApplication { get; }

        /// <summary>
        /// Gets a value indicating whether this element is a container.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is a container; otherwise, <c>false</c>.
        /// </value>
        bool IsContainer { get; }
    }

    /// <summary>
    /// Provides support for marking an element as a client history element
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IClientHistoryElement : IClientElement
    {

        /// <summary>
        /// Navigates to this element.
        /// </summary>
        void Navigate();
    }

    /// <summary>
    /// Provides support for declaring a history element
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IClinetHistoryContainerElement : IClientElement
    {
        /// <summary>
        /// Gets the navigated element.
        /// </summary>
        /// <value>
        /// The navigated element.
        /// </value>
        IClientHistoryElement NavigatedElement
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether history enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if history enabled; otherwise, <c>false</c>.
        /// </value>
        bool HistoryEnabled { get; }
    }

    /// <summary>
    /// Provides API for history management
    /// </summary>
    public interface IClientHistoryManager
    {
        /// <summary>
        /// Navigates to the specific bookmark
        /// </summary>
        /// <param name="token">The token.</param>
        void Navigate(string token);
    }


    /// <summary>
    /// Provides API for history handler
    /// </summary>
    public interface IClientHistoryHandler
    {
        /// <summary>
        /// Navigates to the specific bookmark
        /// </summary>
        /// <param name="token">The token.</param>
        void Navigate(string token);
    }
}
