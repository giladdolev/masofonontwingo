﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    /// <summary>
    /// Provides support for invoking client methods.
    /// </summary>
    public interface IVisualElementMethodInvoker : IRootElement, IClientElement
    {


        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name
        {
            get;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        object Data
        {
            get;
            set;
        }


        /// <summary>
        /// Gets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        object Owner
        {
            get;
        }


        /// <summary>
        /// Gets or sets the callback.
        /// </summary>
        /// <value>
        /// The callback.
        /// </value>
        Action<object> Callback
        {
            get;
        }

        /// <summary>
        /// Performs the callback.
        /// </summary>
        /// <param name="result">The result.</param>
        void PerformCallback(object result);
    }

    /// <summary>
    /// Provides support for invoking client methods.
    /// </summary>
    public interface IVisualElementMethodInvokerContainer
    {
        /// <summary>
        /// De-queues a method invoker.
        /// </summary>
        /// <returns></returns>
        IVisualElementMethodInvoker Dequeue();


        /// <summary>
        /// Clears the method invoker state.
        /// </summary>
        void Clear();
    }
}
