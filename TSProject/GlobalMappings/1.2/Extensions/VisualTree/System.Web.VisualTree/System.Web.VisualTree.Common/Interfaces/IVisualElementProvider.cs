﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace System.Web.VisualTree.Common
{
    public interface IVisualElementProvider
    {

        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="windowControl">The window control.</param>
        /// <param name="element">The element.</param>
        /// <param name="preventRendering">if set to <c>true</c> prevent rendering.</param>
        /// <param name="preventSynchronize">if set to <c>true</c> prevent synchronize.</param>
        /// <returns></returns>
        IVisualElement OnInitializeWindow(Control windowControl, IVisualElement element, ref bool preventRendering, ref bool preventSynchronize);
        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="windowControl">The window control.</param>
        /// <param name="windowElement">The window element.</param>
        void InitializeWindow(Control windowControl, IVisualElement windowElement);
        /// <summary>
        /// Creates the action.
        /// </summary>
        /// <typeparam name="TActionArgs">The type of the action arguments.</typeparam>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <returns></returns>
        IVisualElementAction<TActionArgs> CreateAction<TActionArgs>(IVisualElement visualElement, string controllerName, string actionName);
    }
}
