﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace System.Web.VisualTree.Common.Interfaces
{
    public interface IVisualElementRendererApp
    {
        /// <summary>
        /// Renders the application.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="windowElement">The window element.</param>
        /// <param name="objWriter">The object writer.</param>
        /// <param name="renderTo">The render to.</param>
        void RenderApp(RenderingContext renderingContext, IVisualElement windowElement, StringWriter objWriter, string renderTo);

    }
}
