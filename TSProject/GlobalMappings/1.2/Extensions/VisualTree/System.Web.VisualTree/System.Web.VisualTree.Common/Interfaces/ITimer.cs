﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    /// <summary>
    /// Defines required interface to be implemented by a timer.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface ITimer
    {

        /// <summary>
        /// Gets the timer interval
        /// </summary>
        int Interval
        {
            get;
        }

        /// <summary>
        /// Checks if timer is enabled
        /// </summary>
        bool Enabled
        {
            get;
        }

        /// <summary>
        /// Invokes timer
        /// </summary>
        int InvokeTimer();

        /// <summary>
        /// Gets the time to the next invocation in milliseconds
        /// </summary>
        /// <param name="lngCurrentTicks"></param>
        /// <returns></returns>
        int GetNextInvokation(long lngCurrentTicks);
    }

    /// <summary>
    /// Summary description for ITimerHandler.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface ITimerHandler
    {
        /// <summary>
        /// Adds the timer.
        /// </summary>
        /// <param name="timer">The timer.</param>
        void AddTimer(ITimer timer);

        /// <summary>
        /// Removes the timer.
        /// </summary>
        /// <param name="timer">The timer.</param>
        void RemoveTimer(ITimer timer);


        /// <summary>
        /// Gets a value indicating whether this instance has timers.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has timers; otherwise, <c>false</c>.
        /// </value>
        bool HasTimers
        {
            get;
        }


        /// <summary>
        /// Invoke all required timers
        /// </summary>
        /// <param name="currentTicks"></param>
        int InvokeTimers(long currentTicks);
    }


}
