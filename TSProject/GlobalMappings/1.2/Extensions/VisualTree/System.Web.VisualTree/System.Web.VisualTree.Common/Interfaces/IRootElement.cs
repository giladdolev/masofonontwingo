﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IRootElement
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string ID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the parent element.
        /// </summary>
        /// <value>
        /// The parent element.
        /// </value>
        IVisualElement ParentElement
        {
            get;
            set;
        }
    }
}
