﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Common
{
    
    public interface IRenderingEngine
    {
        /// <summary>
        /// Renders the specified visual element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="renderTo">The render to.</param>
        void Render(RenderingContext renderingContext, IVisualElement visualElement, TextWriter writer, string renderTo);
    }
}
