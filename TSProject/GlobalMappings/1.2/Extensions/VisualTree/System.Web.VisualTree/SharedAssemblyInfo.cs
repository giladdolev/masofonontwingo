﻿using System.Reflection;

[assembly: AssemblyCompany("Gizmox Transposition")]
[assembly: AssemblyProduct("Visual Tree")]
[assembly: AssemblyCopyright("Copyright © 2005-2016 Gizmox Transposition")]

[assembly: AssemblyVersion("1.8.0")]
[assembly: AssemblyFileVersion("1.8.0")]
