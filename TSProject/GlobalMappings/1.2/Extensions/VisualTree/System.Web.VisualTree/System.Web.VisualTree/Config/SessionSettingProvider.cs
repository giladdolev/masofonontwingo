﻿using System.Collections.Specialized;
using System.Configuration;

namespace System.Web.VisualTree.Config
{
    public class SessionSettingProvider : SettingsProvider
    {
        /// <summary>
        /// The MSTR application name
        /// </summary>
        private string mstrApplicationName;

        /// <summary>
        /// Gets or sets the name of the currently running application.
        /// </summary>
        /// <returns>A <see cref="T:System.String" /> that contains the application's shortened name, which does not contain a full path or extension, for example, SimpleAppSettings.</returns>
        public override string ApplicationName
        {
            get { return mstrApplicationName; }
            set { mstrApplicationName = value; }
        }


        /// <summary>
        /// Initializes the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="values">The values.</param>
        public override void Initialize(string name, NameValueCollection values)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "SessionSettingProvider";
            }
            base.Initialize(name, values);
        }

 

 


        public override string Name
        {
            get
            {
                return base.Name;
            }
        }

        public override string Description
        {
            get
            {
                return base.Description;
            }
        }

        /// <summary>
        /// Gets the property values.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="objSettingProperties">The setting properties.</param>
        /// <returns></returns>
        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection objSettingProperties)
        {
            if (objSettingProperties == null)
            {
                throw new ArgumentNullException("objSettingProperties");
            }

            SettingsPropertyValueCollection objPropertyValues = new SettingsPropertyValueCollection();

            foreach(SettingsProperty objProperty in objSettingProperties)
            {
                objPropertyValues.Add(new SettingsPropertyValue(objProperty));
            }

            return objPropertyValues;
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
        {
            
        }
    }
}
