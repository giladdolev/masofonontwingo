﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for extending the visual element
    /// </summary>
    public interface IVisualElementExtender
    {
        /// <summary>
        /// Gets the extender.
        /// </summary>
        /// <value>
        /// The extender.
        /// </value>
        IVisualElementExtender Extender
        {
            get;
            set;
        }

        /// <summary>
        /// Adds the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        void AddExtender(IVisualElementExtender objExtender);

        /// <summary>
        /// Removes the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        void RemoveExtender(IVisualElementExtender objExtender);

    }
}
