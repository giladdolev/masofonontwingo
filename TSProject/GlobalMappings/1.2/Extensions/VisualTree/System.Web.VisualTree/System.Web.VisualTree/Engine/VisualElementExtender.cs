﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for extending elements
    /// </summary>
    public abstract class VisualElementExtender : IVisualElementExtender
    {

        /// <summary>
        /// The next extender
        /// </summary>
        private IVisualElementExtender mobjExtender;


        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementExtender"/> class.
        /// </summary>
        public VisualElementExtender()
        {
        }

        /// <summary>
        /// Gets or sets the extender.
        /// </summary>
        /// <value>
        /// The extender.
        /// </value>
        public IVisualElementExtender Extender
        {
            get { return mobjExtender; }
            set { mobjExtender = value; }
        }

        /// <summary>
        /// Removes the extender.
        /// </summary>
        public void RemoveExtender()
        {
            // Remove the current extender
            RemoveExtender(this);
        }


        /// <summary>
        /// Adds the extender.
        /// </summary>
        /// <param name="objCurrent">The current.</param>
        /// <param name="objExtender">The extender.</param>
        internal static void AddExtender(IVisualElementExtender objCurrent, IVisualElementExtender objExtender)
        {
            // If there is a valid current element and a valid extender
            if (objCurrent != null && objExtender != null)
            {
                // Get the next extender
                IVisualElementExtender objNext = objCurrent.Extender;

                // If there is a next extender
                if (objNext != null)
                {
                    // Add extender to the next extender
                    AddExtender(objNext, objExtender);
                }
                else
                {
                    // Set the next extender
                    objCurrent.Extender = objExtender;
                }
            }
        }
        
        /// <summary>
        /// Removes the extender.
        /// </summary>
        /// <param name="objCurrent">The current.</param>
        /// <param name="objExtender">The extender.</param>
        internal static void RemoveExtender(IVisualElementExtender objCurrent, IVisualElementExtender objExtender)
        {
            // If there is a valid current element and a valid extender
            if (objCurrent != null && objExtender != null)
            {

                // Get the next extender
                IVisualElementExtender objNext = objCurrent.Extender;

                // If there is a next extender
                if (objNext != null)
                {
                    // If the next extender is the extender to remove
                    if (objNext == objExtender)
                    {
                        // Connect current extender and the next of the remove extender
                        objCurrent.Extender = objExtender.Extender;
                    }
                    else
                    {
                        // Remove extender from next extender
                        RemoveExtender(objNext, objExtender);
                    }
                }
            }
        }

        /// <summary>
        /// Removes the extender by type.
        /// </summary>
        /// <typeparam name="T1">The type of the extender.</typeparam>
        /// <param name="objCurrent">The current extender.</param>
        internal static void RemoveExtender<T1>(IVisualElementExtender objCurrent) where T1 : class, IVisualElementExtender
        {
            // If there is a valid current element and a valid extender
            if (objCurrent != null)
            {
                // Get the next extender
                IVisualElementExtender objNext = objCurrent.Extender;

                // If there is a next extender
                if (objNext != null)
                {
                    // If the next extender is the extender to remove
                    if (objNext is T1)
                    {
                        // Connect current extender and the next of the remove extender
                        objCurrent.Extender = objNext.Extender;
                    }
                    else
                    {
                        // Remove extender from next extender
                        RemoveExtender<T1>(objNext);
                    }
                }
            }
        }


        /// <summary>
        /// Gets the extenders.
        /// </summary>
        /// <typeparam name="T1">The extender type.</typeparam>
        /// <param name="objCurrent">The current.</param>
        /// <returns></returns>
        internal static IEnumerable<T1> GetExtenders<T1>(IVisualElementExtender objCurrent) where T1 : class, IVisualElementExtender
        {
            // If there is a valid current extender
            if (objCurrent != null)
            {
                // If current is a T1 type
                if (objCurrent is T1)
                {
                    // Return current extender as the required extender
                    yield return (T1)objCurrent;
                }

                // Try to get extender from the next extender
                foreach (T1 objExtender in GetExtenders<T1>(objCurrent.Extender))
                {
                    // Return the next extender
                    yield return objExtender;
                }
            }
        }


        /// <summary>
        /// Gets the extender.
        /// </summary>
        /// <typeparam name="T1">The element extender type.</typeparam>
        /// <param name="objVisualElementExtender">The object visual element extender.</param>
        /// <param name="objCreateFucntion">The create extender function.</param>
        /// <returns></returns>
        internal static T1 GetOrCreateExtender<T1>(IVisualElementExtender objVisualElementExtender, Func<T1> objCreateFucntion) where T1 : class, IVisualElementExtender
        {
            // Get or create the extender
            return GetOrCreateExtender<T1>(objVisualElementExtender, delegate(object objExtender)
            {

                // If there is a valid create function
                if (objCreateFucntion != null)
                {
                    return objCreateFucntion();
                }

                return null;
            });
        }

        /// <summary>
        /// Gets the extender.
        /// </summary>
        /// <typeparam name="T1">The element extender type.</typeparam>
        /// <param name="objVisualElementExtender">The object visual element extender.</param>
        /// <param name="objCreateFucntion">The create extender function.</param>
        /// <returns></returns>
        public static T1 GetOrCreateExtender<T1>(IVisualElementExtender objVisualElementExtender, Func<object, T1> objCreateFucntion) where T1 : class, IVisualElementExtender
        {
            T1 objExtender = null;

            // If there is a valid element
            if (objVisualElementExtender != null)
            {
                // Get existing extender
                objExtender = GetExtender<T1>(objVisualElementExtender);

                // If we need and can create a new extender
                if (objExtender == null && objCreateFucntion != null)
                {
                    // If we need and can create a new extender
                    if (objExtender == null)
                    {
                        // Create extender 
                        objExtender = objCreateFucntion(objVisualElementExtender);

                        // If there is a valid created exception
                        if (objExtender != null)
                        {
                            // Add the element extender to the element
                            objVisualElementExtender.AddExtender(objExtender);
                        }
                    }
                }
            }

            return objExtender;
        }

        /// <summary>
        /// Determines if there is an extender of type T1.
        /// </summary>
        /// <typeparam name="T1">The element extender type.</typeparam>
        /// <param name="objCurrent">The current.</param>
        /// <returns></returns>
        internal static bool HasExtender<T1>(IVisualElementExtender objCurrent) where T1 : class, IVisualElementExtender
        {
            return GetExtender<T1>(objCurrent) != null;
        }

        /// <summary>
        /// Gets the extender.
        /// </summary>
        /// <typeparam name="T1">The element extender type.</typeparam>
        /// <param name="objCurrent">The current extender.</param>
        /// <returns></returns>
        public static T1 GetExtender<T1>(IVisualElementExtender objCurrent) where T1 : class, IVisualElementExtender
        {
            // If there is a valid current extender
            while (objCurrent != null)
            {
                // If current is a T1 type
                if (objCurrent is T1)
                {
                    // Return current extender as the required extender
                    return (T1)objCurrent;
                }
                else
                {
                    // Try to get extender from the next extender
                    objCurrent = objCurrent.Extender;
                }
            }

            return null;
        }





        /// <summary>
        /// Adds the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        public void AddExtender(IVisualElementExtender objExtender)
        {
            AddExtender(this, objExtender);
        }

        /// <summary>
        /// Removes the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        public void RemoveExtender(IVisualElementExtender objExtender)
        {
            RemoveExtender(this, objExtender);
        }


    }
}
