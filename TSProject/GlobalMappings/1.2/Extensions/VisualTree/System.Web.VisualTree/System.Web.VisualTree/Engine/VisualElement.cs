using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Web.Routing;
using System.IO;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements.Touch;
using System.ComponentModel;
using System.Collections;
using System.Collections.ObjectModel;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Engine;
using System.Text.RegularExpressions;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides base implementation for visual tree elements
    /// </summary>
    public abstract class VisualElement : IVisualElement, IVisualElementExtender, IClientElement
    {

        private string _id;
        private string _clientId;
        private VisualElement _parentElement;
        private int _elementRevision;

        /// <summary>
        /// The property change method name
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal const string PropertyChangeMethodName = "$PropertyChanged";

        /// <summary>
        /// The add listener method name
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal const string AddListenerMethodName = "$AddListener";

        /// <summary>
        /// The remove listener method name
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal const string RemoveListenerMethodName = "$RemoveListener";

        private static VisualElement[] _EmptyVisualElementArray = new VisualElement[] { };

        /// <summary>
        /// The batch processing flag
        /// </summary>
        private SafeFlag _batchProcessing = new SafeFlag();

        /// <summary>
        /// Gets a value indicating whether batch processing is on.
        /// </summary>
        /// <value>
        ///   <c>true</c> if batch processing is on; otherwise, <c>false</c>.
        /// </value>
        public bool BatchProcessing { get { return _batchProcessing.Flag; } }

        /// <summary>
        /// Starts the batch.
        /// </summary>
        /// <returns></returns>
        public IDisposable BatchBlock
        {
            get
            {
                return _batchProcessing.Start();
            }
        }


        /// <summary>
        /// Gets the window target.
        /// </summary>
        /// <param name="windowTargetID">The window target identifier.</param>
        /// <returns></returns>
        internal IWindowTarget GetWindowTarget(string windowTargetID)
        {
            // Try to get this window as target
            IWindowTarget windowTarget = this as IWindowTarget;

            // If there is a valid window target
            if (windowTarget != null)
            {
                // Check window target id
                if (string.Equals(windowTarget.WindowTargetID, windowTargetID, StringComparison.Ordinal))
                {
                    // Return window target
                    return windowTarget;
                }
            }
            else
            {
                // If this is a window target container
                if (this is IWindowTargetContainer)
                {
                    // Get children
                    IEnumerable<VisualElement> children = null;

                    // If is an application element
                    if (this is ApplicationElement)
                    {
                        // Get list of open windows
                        children = ((ApplicationElement)this).GetOpenWindows();
                    }
                    else
                    {
                        // Set children
                        children = this.Children;
                    }

                    // Loop all children
                    foreach (VisualElement child in children)
                    {
                        // If there is a valid child
                        if (child != null)
                        {
                            // Try to get window target
                            windowTarget = child.GetWindowTarget(windowTargetID);

                            // If there is a valid window target
                            if (windowTarget != null)
                            {
                                return windowTarget;
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the empty array.
        /// </summary>
        /// <value>
        /// The empty array.
        /// </value>
        public static VisualElement[] EmptyArray
        {
            get
            {
                return _EmptyVisualElementArray;
            }
        }

        /// <summary>
        /// Ensures the visible.
        /// </summary>
        /// <param name="element">The element to ensure visible.</param>
        public virtual void EnsureVisibleChild(VisualElement element)
        {

        }

        /// <summary>
        /// Ensures the visible.
        /// </summary>
        /// <param name="recursive">if set to <c>true</c> recursive.</param>
        public virtual void EnsureVisible(bool recursive)
        {
            // If is recursive
            if (recursive)
            {
                // Get parent 
                VisualElement parent = this.ParentElement;

                // If there is a valid parent
                if (parent != null)
                {
                    // Ensure this child is visible
                    parent.EnsureVisibleChild(this);

                    // Ensure the element parent element is visible 
                    parent.EnsureVisible(true);
                }
            }
        }

        /// <summary>
        /// Gets the binding manager.
        /// </summary>
        /// <value>
        /// The binding manager.
        /// </value>
        public BindingManager BindingManager
        {
            get
            {
                // Get the binding manager
                BindingManager bindingManager = BindingManager.GetBindingManager(this);

                // If there is a valid binding manager
                if (bindingManager != null)
                {
                    return bindingManager;
                }

                // If there is a valid parent element
                if (_parentElement != null)
                {
                    // Return binding manager from parent element
                    return _parentElement.BindingManager;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>
        /// The data source.
        /// </value>        
        public virtual object DataSource
        {
            get
            {
                // Get the binding manager
                BindingManager bindingManager = BindingManager.GetBindingManager(this);

                // If there is a valid binding manager
                if (bindingManager != null)
                {
                    // Return the binding manager context
                    return bindingManager.DataSource;
                }

                // If there is a valid parent element
                if (_parentElement != null)
                {
                    // Return data source from parent element
                    return _parentElement.DataSource;
                }

                return null;
            }
            set
            {

                // Get the binding manager
                BindingManager bindingManager = BindingManager.CreateBindingManager(this);

                // If there is a valid binding context
                if (bindingManager != null)
                {

                    // Set the binding manager data source
                    bindingManager.DataSource = value;

                    // Handle data source changed
                    FireDataSourceChanged(new BindingManagerChangedEventArgs(bindingManager));
                }


                // Notify data source had changed
                OnPropertyChanged("DataSource");
            }
        }

        /// <summary>
        /// Gets the user data.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public object GetAttribute(string key)
        {
            // Get the extender
            UserDataExtender extender = VisualElementExtender.GetExtender<UserDataExtender>(this);

            object value;
            // If there is a valid extender
            if (extender != null)
            {
                if (extender.TryGetValue(key, out value))
                {
                    return value;
                }
            }

            if (this.ParentElement != null)
            {
                return this.ParentElement.GetAttribute(key);
            }

            return null;
        }

        /// <summary>
        /// Sets the user data.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void SetAttribute(string key, object value)
        {
            UserDataExtender extender = VisualElementExtender.GetOrCreateExtender<UserDataExtender>(this, () => new UserDataExtender());
            extender[key] = value;
        }
        /// <summary>
        /// Fires the data source changed.
        /// </summary>
        /// <param name="args">The <see cref="BindingManagerChangedEventArgs"/> instance containing the event data.</param>
        private void FireDataSourceChanged(BindingManagerChangedEventArgs args)
        {
            // If there are valid arguments
            if (args != null)
            {
                // Raise the data source changed event
                this.OnDataSourceChanged(args);

                // Loop all child nodes
                foreach (VisualElement child in this.Children)
                {
                    // Get child data source
                    BindingManager childDataSource = VisualElementExtender.GetExtender<BindingManager>(child);

                    // If there is a valid data source
                    if (childDataSource != null)
                    {
                        // Set parent data source
                        childDataSource.SetParentDataSource(args.BidingManager);
                    }

                    // Loop all data bindings
                    foreach (Binding dataBinding in Binding.GetDataBindings(child))
                    {
                        // If there is a valid data binding
                        if (dataBinding != null && dataBinding.IsParentBinding)
                        {
                            // Get the binding manager
                            BindingManager bindingManager = args.BidingManager;

                            // If there is a valid binding manager
                            if (bindingManager != null)
                            {
                                // Bind the data source to it's target
                                bindingManager.Bind(dataBinding);
                            }
                        }
                    }

                    // Indicate data source change
                    child.FireDataSourceChanged(args);
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:DataSourceChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BindingManagerChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnDataSourceChanged(BindingManagerChangedEventArgs args)
        {

        }

        /// <summary>
        /// Gets a value indicating whether children order reverted.
        /// </summary>
        /// <value>
        /// <c>true</c> if children order reverted; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool ChildrenOrderReverted
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the revision.
        /// </summary>
        /// <value>
        /// The revision.
        /// </value>
        internal int Revision
        {
            get { return _elementRevision; }
            set { _elementRevision = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the visual element is loaded to client.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the visual element is loaded to client; otherwise, <c>false</c>.
        /// </value>
        internal bool IsLoadedToClient
        {
            get { return Revision != ApplicationElement.GlobalRevision; }
        }

        /// <summary>
        /// Gets the application.
        /// </summary>
        /// <value>
        /// The application.
        /// </value>
        public virtual ApplicationElement Application
        {
            get
            {
                // Get the visual parent
                VisualElement objParent = ParentElement;

                // If there is a valid parent
                if (objParent != null)
                {
                    // Return application
                    return objParent.Application;
                }

                return ApplicationElement.Current;
            }
        }

        /// <summary>
        /// Gets the application identifier.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        public string ApplicationId
        {
            get
            {
                // Get the application 
                ApplicationElement applicationElement = this.Application;

                // If there is a valid application
                if (applicationElement != null)
                {
                    // Return the client ID
                    return applicationElement.ClientID;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public virtual IEnumerable<VisualElement> Children
        {
            get
            {
                yield break;
            }
        }

        /// <summary>
        /// Gets or sets the visual element identifier.
        /// </summary>
        /// <value>
        /// The visual element identifier.
        /// </value>        
        [RendererPropertyDescription]
        public string ID
        {
            get { return _id; }
            set { ReviseClientId(_id = value); }
        }

        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        /// <value>
        /// The client identifier.
        /// </value>
        public virtual string ClientID
        {
            get
            {
                // The client id
                return _clientId;
            }
        }

        /// <summary>
        /// Gets the parent element.
        /// </summary>
        /// <value>
        /// The parent element.
        /// </value>
        public VisualElement ParentElement
        {
            get
            {
                // Return the parent element
                return _parentElement;
            }
            internal set
            {
                // Set parent element
                _parentElement = value;

                // Revise client id
                ReviseClientId(ID);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool IsContainer
        {
            get { return false; }
        }


        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> _nativeStyles = new Dictionary<string, string>();


        /// <summary>
        /// Gets or sets the NativeStyles
        /// </summary>
        /// <value>
        /// The NativeStyles.
        /// </value>
        [RendererPropertyDescription]
        public virtual ReadOnlyDictionary<string, string> NativeStyles
        {
            get { return new ReadOnlyDictionary<string, string>(_nativeStyles); }
        }


        /// <summary>
        /// Setting native style for the element. 
        /// For Example, you can set a custom html style for the element. 
        /// E.g. "box-shadow", "0 0 5px rgba(0, 0, 0, 0.3)"
        /// </summary>
        /// <param name="styleName">The name of the style.</param>
        /// <param name="styleValue">The value of the style.</param>
        public virtual void SetNativeStyle(string styleName, string styleValue)
        {
            if (_nativeStyles != null)
            {
                string existingStyleValue = null;
                //if the style is already set
                if (_nativeStyles.TryGetValue(styleName, out existingStyleValue))
                {
                    if (existingStyleValue == styleValue)
                    {
                        //No Change
                        return;
                    }
                    //Style changed
                    _nativeStyles[styleName] = styleValue;
                }
                else
                {
                    _nativeStyles.Add(styleName, styleValue);
                }
                // Indicate changes in native styles
                OnPropertyChanged("NativeStyles");
            }
        }

        /// <summary>
        /// Setting a collection of native styles for the element.
        /// if a native style exists it will be changed, if not it will be added.
        /// For Example, you can set a custom html style for the element 
        /// </summary>
        /// <param name="stylesDictionary">The styles dictionary</param>
        public virtual void SetNativeStyles(IDictionary<string, string> stylesDictionary)
        {
            if (stylesDictionary != null)
            {
                foreach (KeyValuePair<string, string> style in stylesDictionary)
                {
                    this.SetNativeStyle(style.Key, style.Value);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this element is an application.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is an application; otherwise, <c>false</c>.
        /// </value>
        internal virtual bool IsApplication
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether in design mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if in design mode; otherwise, <c>false</c>.
        /// </value>
        protected bool DesignMode { get; private set; }

        /// <summary>
        /// Gets the extender.
        /// </summary>
        /// <value>
        /// The extender.
        /// </value>
        IVisualElementExtender IVisualElementExtender.Extender
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string IClientElement.ClientID
        {
            get
            {
                return this.ClientID;
            }
        }

        /// <summary>
        /// Gets or sets the parent element.
        /// </summary>
        /// <value>
        /// The parent element.
        /// </value>
        IClientElement IClientElement.ParentElement
        {
            get
            {
                return this.ParentElement;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this element is the application.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is the application; otherwise, <c>false</c>.
        /// </value>
        bool IClientElement.IsApplication
        {
            get
            {
                return this.IsApplication;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this element is a container.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is a container; otherwise, <c>false</c>.
        /// </value>
        bool IClientElement.IsContainer
        {
            get
            {
                return this.IsContainer;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElement"/> class.
        /// </summary>
        protected VisualElement()
        {
            // Set the current element revision
            _elementRevision = ApplicationElement.GlobalRevision;
        }

        /// <summary>
        /// Invokes the method but if a method with this name and owner was already queued for invocation it updates it's data.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public void InvokeMethodOnce(string methodName, object methodData = null, Action<object> methodCallback = null)
        {
            // Get application 
            ApplicationElement applicationElement = this.Application;

            // If there is a valid application 
            if (applicationElement != null)
            {
                // Invoke unique method
                applicationElement.InvokeMethodOnce(methodName, this, methodData, methodCallback);
            }
        }

        /// <summary>
        /// Invokes the method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        public void InvokeMethod(string methodName, object methodData = null)
        {
            // Get application 
            ApplicationElement applicationElement = this.Application;

            // If there is a valid application 
            if (applicationElement != null)
            {
                // Invoke action
                applicationElement.InvokeMethod(methodName, this, methodData);
            }
        }

        /// <summary>
        /// Invokes the method with Callback.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public void InvokeMethod(string methodName, object methodData = null, Action<object> methodCallback = null)
        {
            // Get application 
            ApplicationElement applicationElement = this.Application;

            // If there is a valid application 
            if (applicationElement != null)
            {
                // Invoke action
                applicationElement.InvokeMethod(methodName, this, methodData, methodCallback);
            }
        }



        /// <summary>
        /// Gets the visual element by identifier.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="visualElementID">The visual element identifier.</param>
        /// <returns></returns>
        public TVisualElement GetVisualElementById<TVisualElement>(string visualElementID) where TVisualElement : VisualElement
        {
            // If found a matching visual element
            if (IsMatchingVisualElement<TVisualElement>(this, visualElementID))
            {
                // Set matching element
                return (TVisualElement)this;
            }

            // Get the child visual element
            foreach (VisualElement visualElement in this.Children)
            {
                // Get visual element by matching id
                TVisualElement matchingVisualElement = visualElement.GetVisualElementById<TVisualElement>(visualElementID);

                // If there is a valid visual element
                if (matchingVisualElement != null)
                {
                    // Return the visual element
                    return matchingVisualElement;
                }
            }

            // As last resort, check if the requested element is one of the ghost elements
            return GetComponentManagerElementById<TVisualElement>(this, visualElementID);
        }

        /// <summary>
        /// Gets the component manager element by identifier.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="visualElementID">The visual element identifier.</param>
        /// <returns></returns>
        private TVisualElement GetComponentManagerElementById<TVisualElement>(VisualElement visualElement, string visualElementID)
             where TVisualElement : VisualElement
        {
            // Get component manager container
            IComponentManagerContainer componentManagerContainer = visualElement as IComponentManagerContainer;

            // If there is a valid component manager container
            if (componentManagerContainer != null)
            {
                // Get component manager container elements
                IEnumerable<IVisualElement> componentManagerElements = componentManagerContainer.Elements;

                // If there are valid component manager container elements
                if (componentManagerElements != null)
                {
                    // Loop all component manager container elements
                    foreach (IVisualElement componentManagerElement in componentManagerElements)
                    {
                        // Get actual element
                        VisualElement actualComponentManagerElement = componentManagerElement as VisualElement;

                        // If there is a valid actual element
                        if (actualComponentManagerElement != null)
                        {
                            // Get element from component manager element
                            TVisualElement foundElement = actualComponentManagerElement.GetVisualElementById<TVisualElement>(visualElementID);

                            // If we found an element
                            if (foundElement != null)
                            {
                                // Return that element
                                return foundElement;
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the visual element by path.
        /// </summary>
        /// <param name="visualElementPath">The visual element path.</param>
        /// <returns></returns>
        public VisualElement GetVisualElementByPath(string visualElementPath)
        {
            // If there is a valid visual element path
            if (!string.IsNullOrEmpty(visualElementPath))
            {
                // If we have the "process" id
                if (visualElementPath == "__process")
                {
                    // Return the application
                    return ApplicationElement.Current;
                }
                else
                {
                    // The current element
                    VisualElement currentElement = this;

                    // Loop all element path part 
                    foreach (string visualElementPathPart in visualElementPath.Split('/'))
                    {
                        // Get next element
                        currentElement = currentElement.GetChildVisualElement<VisualElement>(visualElementPathPart);

                        // If there is no valid current element
                        if (currentElement == null)
                        {
                            return null;
                        }
                    }

                    // Return current element
                    return currentElement;
                }
            }

            return null;
        }

        /// <summary>
        /// Determines whether is visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="visualElementID">The visual element identifier.</param>
        /// <returns></returns>
        static internal bool IsMatchingVisualElement<TVisualElement>(VisualElement visualElement, string visualElementID) where TVisualElement : VisualElement
        {
            // If there is a valid visual element
            if (visualElement != null && visualElement is TVisualElement)
            {
                // If found a matching visual element
                if (string.Equals(visualElement.ClientID, visualElementID, StringComparison.Ordinal))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="visualElementID">The visual element identifier.</param>
        /// <returns></returns>
        internal virtual TVisualElement GetVisualElement<TVisualElement>(string visualElementID) where TVisualElement : VisualElement
        {
            // If found a matching visual element
            if (IsMatchingVisualElement<TVisualElement>(this, visualElementID))
            {
                // Set matching element
                return (TVisualElement)this;
            }

            // Get the child visual element
            return GetChildVisualElement<TVisualElement>(visualElementID);
        }

        /// <summary>
        /// Gets the child visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="strVisualElementID">The string visual element identifier.</param>
        /// <returns></returns>
        internal virtual TVisualElement GetChildVisualElement<TVisualElement>(string strVisualElementID) where TVisualElement : VisualElement
        {
            // Loop all child controls
            foreach (VisualElement visualElement in this.Children)
            {
                if (visualElement.IsContainer)
                {
                    // If there is a valid child control
                    if (IsMatchingVisualElement<TVisualElement>(visualElement, strVisualElementID))
                    {
                        // Return element
                        return visualElement as TVisualElement;
                    }
                }
                else
                {
                    // Get child visual element
                    TVisualElement childVisualElement = visualElement.GetVisualElement<TVisualElement>(strVisualElementID);

                    // If there is a valid child visual element
                    if (childVisualElement != null)
                    {
                        return childVisualElement;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Called when property changed.
        /// </summary>
        /// <param name="propertyName">The property changed name.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            // If we need to update the client 
            if (ApplicationElement.GlobalRevision != Revision)
            {
                // Register update
                ApplicationElement.RegisterChange(this, propertyName);
            }
        }

        /// <summary>
        /// Called when event handler attached.
        /// </summary>
        /// <param name="eventName">Name of the event.</param>
        protected virtual void OnEventHandlerAttached(string eventName)
        {
            // Get application 
            ApplicationElement applicationElement = this.Application;

            // If there is a valid application 
            if (applicationElement != null && ApplicationElement.GlobalRevision != Revision)
            {
                // Invoke unique method
                applicationElement.InvokeMethod(AddListenerMethodName, this, eventName);
            }
        }
        protected virtual void OnEventHandlerDeattached(string eventName)
        {
            // Get application 
            ApplicationElement applicationElement = this.Application;

            // If there is a valid application 
            if (applicationElement != null && ApplicationElement.GlobalRevision != Revision)
            {
                // Invoke unique method
                applicationElement.InvokeMethod(RemoveListenerMethodName, this, eventName);
            }
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public virtual void Add(VisualElement visualElement, bool insert)
        {
            // Try to get binding manager
            BindingManager bindingManager = visualElement as BindingManager;

            // If there is a valid binding manager
            if (bindingManager != null)
            {
                // Set the current binding manager
                BindingManager.SetBindingManager(this, bindingManager);

                // Handle data source changed
                FireDataSourceChanged(new BindingManagerChangedEventArgs(bindingManager));
            }
            else
            {
                // Get visual settings
                VisualSettings objVisualSettings = visualElement as VisualSettings;

                // If there is a valid visual settings
                if (objVisualSettings != null)
                {
                    // Set the visual ID and client id
                    ID = objVisualSettings.ID;
                }
            }
        }

        /// <summary>
        /// Removes the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public virtual void Remove(VisualElement visualElement)
        {

        }

        /// <summary>
        /// Revises the client identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        private void ReviseClientId(string id)
        {
            // If id is invalid
            if (string.IsNullOrWhiteSpace(id))
            {
                // Set default id
                id = "_id";
            }
            // If there is a valid id
            else if (id != null && id.Length > 0)
            {
                // Check if id starts with a digit
                if (char.IsDigit(id[0]))
                {
                    // Add prefix to prevent incorrect naming
                    id = string.Concat("_id_", id);
                }
            }


            // If there is a valid parent
            if (_parentElement != null && !_parentElement.BatchProcessing)
            {
                // Index used to enumerate identifiers
                int identifierIndex = 1;

                // The current id
                string currentId = id;

                // Loop while no unique id
                while (!IsUniqueId(_parentElement, this, currentId))
                {
                    // Increment index
                    identifierIndex++;

                    // Set the next id
                    currentId = string.Concat(id, "_", identifierIndex);
                }

                // Set the current id
                id = currentId;
            }

            // Set client id
            _clientId = id;
        }

        /// <summary>
        /// Determines whether is unique identifier.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="currentElement">The current element.</param>
        /// <param name="uniqueId">The unique identifier.</param>
        /// <returns></returns>
        private static bool IsUniqueId(VisualElement parentElement, VisualElement currentElement, string uniqueId)
        {
            // Get visual element
            VisualElement namedElement = parentElement.GetVisualElement<VisualElement>(uniqueId);

            // If no name or is same element
            if (namedElement == null || namedElement == currentElement)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return base.ToString();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {

        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Creates visual element instance using type name.
        /// </summary>
        /// <param name="typeName">The visual element type name.</param>
        /// <returns></returns>
        public static VisualElement CreateInstance(string typeName)
        {
            return (VisualElement)Activator.CreateInstance(Type.GetType(typeName, true));
        }

        /// <summary>
        /// Gets the application identifier.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public static string GetApplicationId(VisualElement visualElement)
        {
            // If there is a valid visual element
            if (visualElement != null)
            {
                // Get application from visual element
                ApplicationElement applicationElement = visualElement.Application;

                // If there is a valid application element
                if (applicationElement != null)
                {
                    // Return the application element
                    return applicationElement.ClientID;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the element identifier.
        /// </summary>
        /// <param name="clientElement">The visual element.</param>
        /// <returns></returns>
        public static string GetElementId(IClientElement clientElement)
        {
            // If there is a visual element
            if (clientElement != null)
            {
                string elementId = "";
                // if (withClientId)
                // {
                // Get element id
                elementId = clientElement.ClientID;

                // If there is a valid element id
                if (!string.IsNullOrEmpty(elementId))
                {
                    // Create path builder
                    StringBuilder pathBuilder = new StringBuilder();

                    // Fill container path
                    FillContainerPath(clientElement.ParentElement, pathBuilder);

                    // Add the element id
                    pathBuilder.Append(elementId);

                    // Return the path
                    return pathBuilder.ToString();
                }
                //}
                //else
                //{
                //    // Create path builder
                //    StringBuilder pathBuilder = new StringBuilder();

                //    // Fill container path
                //    FillContainerPath(clientElement.ParentElement, pathBuilder);

                //    // Add the element id
                //    pathBuilder.Append(elementId);
                //    pathBuilder.Remove(pathBuilder.Length - 1, 1);
                //    // Return the path
                //    return pathBuilder.ToString();
                //}


            }

            return null;
        }

        /// <summary>
        /// Fills the container path.
        /// </summary>
        /// <param name="clientElement">The client element.</param>
        /// <param name="pathBuilder">The path builder.</param>
        private static void FillContainerPath(IClientElement clientElement, StringBuilder pathBuilder)
        {
            // If there is a valid client non application element
            if (clientElement != null && !clientElement.IsApplication)
            {

                // Fill parent containers
                FillContainerPath(clientElement.ParentElement, pathBuilder);

                // If there is a valid container
                if (clientElement.IsContainer)
                {
                    // Get element id
                    string elementId = clientElement.ClientID;

                    // If there is a valid element id
                    if (!string.IsNullOrEmpty(elementId))
                    {
                        pathBuilder.Append(elementId);
                        pathBuilder.Append("/");
                    }
                    else
                    {
                        pathBuilder.Append("?/");
                    }
                }
            }
        }

        /// <summary>
        /// Gets the perform event method.
        /// </summary>
        /// <param name="clientElement">The client element.</param>
        /// <param name="eventName">The event name.</param>
        /// <returns></returns>
        public static MethodInfo GetPerformEventMethod(IClientElement clientElement, string eventName)
        {
            // The method info
            MethodInfo objMethodInfo = null;

            // If there is a valid client element
            if (clientElement != null && !string.IsNullOrEmpty(eventName))
            {
                // Get client element type
                Type visualElementType = clientElement.GetType();

                // If there is a valid client element type
                if (visualElementType != null)
                {
                    // Add the perform method prefix
                    string preformEventName = string.Concat("Perform", eventName);

                    // Get property info
                    while (visualElementType != null && ((objMethodInfo = visualElementType.GetMethod(preformEventName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)) == null))
                    {
                        // Get base type
                        visualElementType = visualElementType.BaseType;
                    }
                }
            }

            // Return the method info
            return objMethodInfo;
        }

        /// <summary>
        /// Adds the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        void IVisualElementExtender.AddExtender(IVisualElementExtender objExtender)
        {
            AddExtender(objExtender);
        }

        /// <summary>
        /// Adds the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        protected void AddExtender(IVisualElementExtender objExtender)
        {
            VisualElementExtender.AddExtender(this, objExtender);
        }

        /// <summary>
        /// Removes the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        void IVisualElementExtender.RemoveExtender(IVisualElementExtender objExtender)
        {
            RemoveExtender(objExtender);
        }

        /// <summary>
        /// Removes the extender.
        /// </summary>
        /// <param name="objExtender">The extender.</param>
        protected void RemoveExtender(IVisualElementExtender objExtender)
        {
            VisualElementExtender.RemoveExtender(this, objExtender);
        }

        /// <summary>
        /// Gets the container element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public static VisualElement GetContainerElement(VisualElement visualElement)
        {
            if (visualElement != null)
            {
                if (visualElement is CompositeElement)
                {
                    return visualElement;
                }
                else if (visualElement is WindowElement)
                {
                    return visualElement;
                }
                // support root element of controller in control view
                else if (visualElement is PanelElement && visualElement.ParentElement == null)
                {
                    return visualElement;
                }
                else
                {
                    return GetContainerElement(visualElement.ParentElement);
                }

            }

            return null;
        }

        /// <summary>
        /// Gets the parent element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public static T GetParentElement<T>(VisualElement visualElement)
            where T : VisualElement
        {
            if (visualElement == null)
                return null;

            if (visualElement.ParentElement is T)
                return visualElement.ParentElement as T;

            return GetParentElement<T>(visualElement.ParentElement);
        }

        /// <summary>
        /// Called when data source item updated.
        /// </summary>
        /// <param name="item">The item.</param>
        public virtual void OnDataSourceItemUpdated(object item)
        {

        }

        /// <summary>
        /// Called when data source item deleted.
        /// </summary>
        /// <param name="item">The item.</param>
        internal protected virtual void OnDataSourceItemDeleted(object item)
        {

        }

        /// <summary>
        /// Called when data source item created.
        /// </summary>
        /// <param name="item">The item.</param>
        internal protected virtual void OnDataSourceItemCreated(object item)
        {

        }

        /// <summary>
        /// Called when data source current item changed.
        /// </summary>
        /// <param name="item">The item.</param>
        internal protected virtual void OnDataSourceCurrentItemChanged(object item)
        {

        }


        /// <summary>
        /// Initializes the binding manager.
        /// </summary>
        /// <param name="bindingManager">The binding manager.</param>
        public virtual void InitializeBindingManager(BindingManager bindingManager)
        {
        }

        /// <summary>
        /// Gets the visual elements by identifier.
        /// Mimic control array functionality.
        /// Expect elements id in format _id_index (e.g. _button_0, _button_1).
        /// Index dont have to be sequential
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="visualElementID">The visual element identifier.</param>
        /// <returns></returns>
        public List<TVisualElement> GetVisualElementsById<TVisualElement>(string visualElementID)
            where TVisualElement : VisualElement
        {
            // Initialize search pattern
            Regex regEx = new Regex(String.Concat("^_", visualElementID.Replace(".", "\\."), "_", "(?<index>\\d+)$"), RegexOptions.None);
            List<TVisualElement> controlArray = new List<TVisualElement>();

            // Search children recursively
            GetVisualElementsById(this.Children, controlArray, regEx);

            return (controlArray.Count > 0) ? controlArray : null;
        }

        /// <summary>
        /// Gets the visual element list by identifier.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="controls">The control array.</param>
        /// <param name="controlArray">The control array.</param>
        /// <param name="regEx">The reg ex.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void GetVisualElementsById<TVisualElement>(IEnumerable<VisualElement> controls, List<TVisualElement> controlArray, Regex regEx)
            where TVisualElement : VisualElement
        {
            // Scan controls collection
            foreach (VisualElement item in controls)
            {
                // check if elements id match control array pattern
                Match idMatch = regEx.Match(item.ID);
                if (idMatch.Success)
                {
                    // Prase controls index in array
                    string indexString = idMatch.Groups["index"].Value;
                    int index = -1;
                    if (Int32.TryParse(indexString, out index))
                    {
                        TVisualElement control = item as TVisualElement;
                        if (control != null)
                        {
                            // Fill array with placeholders
                            while (controlArray.Count <= index)
                            {
                                controlArray.Add(null);
                            }
                        }
                        // Add control at designated index
                        controlArray[index] = control;
                    }
                }
            }

            // If nothing found at this level, proceed recurcively
            if (controlArray.Count == 0)
            {
                foreach (VisualElement item in controls)
                {
                    // Search children
                    GetVisualElementsById(item.Children, controlArray, regEx);
                    if (controlArray.Count > 0)
                    {
                        break;
                    }
                }
            }
        }
    }
}
