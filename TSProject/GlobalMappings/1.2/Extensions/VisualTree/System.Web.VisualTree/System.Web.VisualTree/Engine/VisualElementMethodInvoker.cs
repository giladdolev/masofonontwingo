﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for invoking client methods
    /// </summary>
    internal class VisualElementMethodInvoker : IVisualElementMethodInvoker
    {
        private readonly string _methodName;
        private readonly object _methodOwner;
        private object _methodData;
        private Action<object> _methodCallback;
        private IVisualElement _parentElement;
        private string _id;


        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementMethodInvoker" /> class.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public VisualElementMethodInvoker(string methodName, object methodOwner, object methodData, Action<object> methodCallback)
        {
            _methodName = methodName;
            _methodOwner = methodOwner;
            _methodData = methodData;
            _methodCallback = methodCallback;
        }



        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return _methodName; }
        }




        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public object Data
        {
            get { return _methodData; }
            set { _methodData = value;  }
        }




        /// <summary>
        /// Gets or sets the callback.
        /// </summary>
        /// <value>
        /// The callback.
        /// </value>
        public Action<object> Callback
        {
            get { return _methodCallback; }
            set { _methodCallback = value; }
        }




        /// <summary>
        /// Performs the callback.
        /// </summary>
        /// <param name="result">The result.</param>
        public void PerformCallback(object result)
        {
            // Get the application element
            ApplicationElement applicationElement = this.ParentElement as ApplicationElement;

            // If there is a valid application element
            if(applicationElement != null)
            {
                // Release the method invoker
                applicationElement.ReleaseMethodInvoker(this);
            }

            // If there is a valid method callback
            if(_methodCallback != null)
            {
                // Call the callback
                _methodCallback(result);
            }
        }




        /// <summary>
        /// Gets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        public object Owner
        {
            get { return _methodOwner; }
        }




        /// <summary>
        /// Gets the name of the owner type.
        /// </summary>
        /// <value>
        /// The name of the owner type.
        /// </value>
        private string OwnerTypeName
        {
            get
            {
                // If there is a valid owner
                if (_methodOwner != null)
                {
                    // Get owner as type
                    Type methodOwnerType = _methodOwner as Type;

                    // If there is a valid owner as type
                    if (methodOwnerType != null)
                    {
                        // Get type name
                        return methodOwnerType.Name;
                    }
                    else
                    {
                        // Get type name of instance
                        return _methodOwner.GetType().Name;
                    }
                }

                return string.Empty;
            }
        }




        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string IRootElement.ID
        {
            get
            {
                // If we need to initialize id
                if (string.IsNullOrWhiteSpace(_id))
                {
                    // Calculate the meaningful name
                    _id = string.Concat(this.OwnerTypeName, "_", this.Name);
                }

            

                return _id;
            }
            set
            {
                _id = value;
            }
        }




        /// <summary>
        /// Gets or sets the parent element.
        /// </summary>
        /// <value>
        /// The parent element.
        /// </value>
        public IVisualElement ParentElement
        {
            get
            {
                return _parentElement;
            }
            set
            {
                _parentElement = value;
            }
        }




        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string IClientElement.ClientID
        {
            get
            {
                return ((IRootElement)this).ID;
            }
        }




        /// <summary>
        /// Gets or sets the parent element.
        /// </summary>
        /// <value>
        /// The parent element.
        /// </value>
        IClientElement IClientElement.ParentElement
        {
            get
            {
                return _parentElement as IClientElement;
            }
        }




        /// <summary>
        /// Gets a value indicating whether this element is the application.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is the application; otherwise, <c>false</c>.
        /// </value>
        bool IClientElement.IsApplication
        {
            get
            {
                return false;
            }
        }




        /// <summary>
        /// Gets a value indicating whether this element is a container.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is a container; otherwise, <c>false</c>.
        /// </value>
        bool IClientElement.IsContainer
        {
            get
            {
                return false;
            }
        }
    }

}
