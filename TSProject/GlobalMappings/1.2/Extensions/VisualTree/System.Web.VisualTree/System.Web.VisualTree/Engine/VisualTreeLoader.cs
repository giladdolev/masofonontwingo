﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Engine
{
    internal class VisualTreeLoader
    {
        /// <summary>
        /// The element renderers
        /// </summary>
        [ImportMany(typeof(IVisualElementRenderer))]
        private List<IVisualElementRenderer> _elementRenderers;


        /// <summary>
        /// Initializes the renderers.
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="elementRenderers">The element renderers.</param>
        internal void InitializeRenderers(HashSet<Assembly> assemblies, Dictionary<Tuple<RenderingEnvironmentId, Type>, IVisualElementRenderer> elementRenderers)
        {
            LoadRenderers(assemblies);

            // If there is a valid list of renderers
            if (_elementRenderers != null)
            {
                // Loop all renderers
                foreach (IVisualElementRenderer elementRenderer in _elementRenderers)
                {
                    // Get all renderer descriptions
                    IEnumerable<RendererDescriptionAttribute> rendererDescriptionAttributes = elementRenderer.GetType().GetCustomAttributes<RendererDescriptionAttribute>(false);

                    // If there is a valid list of renderer descriptions
                    if (rendererDescriptionAttributes != null)
                    {
                        // Loop all renderer descriptions
                        foreach (RendererDescriptionAttribute rendererDescriptionAttribute in rendererDescriptionAttributes)
                        {
                            if (!rendererDescriptionAttribute.IsAlternative)
                            {
                                // Get the renderer key
                                Tuple<RenderingEnvironmentId, Type> rendererKey =
                                    rendererDescriptionAttribute.RendererKey;

                                // If there is a valid renderer key
                                if (rendererKey != null)
                                {
                                    // If this is a unique renderer per environment and type
                                    if (!elementRenderers.ContainsKey(rendererKey))
                                    {
                                        // Register renderer
                                        elementRenderers[rendererKey] = elementRenderer;
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }

                    }
                }
            }

        }




        /// <summary>
        /// Loads the renderers.
        /// </summary>
        private void LoadRenderers(HashSet<Assembly> assemblies)
        {
            // Load renderers using MEF
            LoadingHelper.CombineCatalog(assemblies, this);
        }
    }
}
