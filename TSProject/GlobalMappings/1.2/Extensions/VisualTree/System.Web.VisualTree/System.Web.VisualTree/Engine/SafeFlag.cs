﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    public class SafeFlag
    {
        /// <summary>
        /// 
        /// </summary>
        /// <seealso cref="System.IDisposable" />
        public class Block : IDisposable
        {
            /// <summary>
            /// The parent
            /// </summary>
            private SafeFlag _parent;

            /// <summary>
            /// Initializes a new instance of the <see cref="Block"/> class.
            /// </summary>
            /// <param name="parent">The parent.</param>
            protected Block(SafeFlag parent)
            {
                _parent = parent;
                _parent._flag = true;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                _parent._flag = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <seealso cref="System.Web.VisualTree.Elements.SafeFlag.Block" />
        private class InternalBlocks : Block
        {
            public InternalBlocks(SafeFlag parent)
                : base(parent)
            {
            }
        }

        /// <summary>
        /// The flag
        /// </summary>
        private bool _flag;

        /// <summary>
        /// Gets a value indicating whether this <see cref="SafeFlag"/> is flag.
        /// </summary>
        /// <value>
        ///   <c>true</c> if flag; otherwise, <c>false</c>.
        /// </value>
        public bool Flag
        {
            get { return _flag; }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        /// <returns></returns>
        public Block Start ()
        {
            return new InternalBlocks(this);
        }
    }
} 
