﻿using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{
    /// <summary>
    /// Provides support for visual element collections
    /// </summary>
    /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
    public abstract class VisualElementCollection<TVisualElement> : Collection<TVisualElement>, IVisualElementCollectionChangeActionsContainer
        where TVisualElement : VisualElement
    {

        #region Change Actions 
        
        /// <summary>
        /// Provides support for tracking changes
        /// </summary>
        private abstract class ChangeAction : IEnumerable<ChangeAction>, IVisualElementCollectionChangeAction
        {

            public string Msg { get; set; }

            public ChangeAction(string msg = null)
            {
                this.Msg = msg;
            }

            /// <summary>
            /// Gets or sets the next.
            /// </summary>
            /// <value>
            /// The next.
            /// </value>
            public ChangeAction Next
            {
                get;
                set;
            }

           
            /// <summary>
            /// Gets the actions.
            /// </summary>
            /// <value>
            /// The actions.
            /// </value>
            private IEnumerable<ChangeAction> Actions
            {
                get
                {
                    // Set this as current
                    ChangeAction current = this;

                    // If there is a current action
                    while (current != null)
                    {
                        // Return the current action
                        yield return current;

                        // Set next action as current
                        current = current.Next;
                    }
                }
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>
            /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
            /// </returns>
            IEnumerator<ChangeAction> IEnumerable<ChangeAction>.GetEnumerator()
            {
                return this.Actions.GetEnumerator();
            }

            /// <summary>
            /// Returns an enumerator that iterates through a collection.
            /// </summary>
            /// <returns>
            /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
            /// </returns>
            Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
            {
                return this.Actions.GetEnumerator();
            }

            /// <summary>
            /// Appends the specified _changes.
            /// </summary>
            /// <param name="changes">The changes.</param>
            /// <param name="changeAction">The change action.</param>
            /// <param name="lastChange">The last change.</param>
            internal static void Append(ref ChangeAction changes, ChangeAction changeAction, ref ChangeAction lastChange)
            {
                // Get the clear action
                ClearChangeAction clearAction = changeAction as ClearChangeAction;

                // If there is a valid clear action
                if (clearAction != null)
                {
                    // Set new action as all the changes (all previous changes are redundant).
                    changes = clearAction;
                    lastChange = changeAction;
                }
                else
                {
                    // If there are no changes
                    if (changes == null)
                    {
                        // Set current change
                        changes = changeAction;
                        lastChange = changeAction;
                    }
                    else
                    {
                        switch (changeAction.ActionType)
                        {
                            case VisualElementCollectionChangeActionType.ClearItems:
                                changes = changeAction;
                                        return;
                            case VisualElementCollectionChangeActionType.SetItem:
                                foreach (ChangeAction change in changes)
                                {
                                    if (change.Item == changeAction.Item)
                                    {
                                        // Do nothing, item already has updated values throw item reference
                                        return;
                                    }
                                }
                                break;
                            case VisualElementCollectionChangeActionType.InsertItem:
                                break;
                            case VisualElementCollectionChangeActionType.RemoveItem:
                                if (changes.Item == changeAction.Item && changes.Item!=null)
                                {
                                    changeAction.Next = changes.Next;
                                    if (changes == lastChange)
                                    {
                                        lastChange = changeAction;
                                    }
                                    changes = changeAction;
                                    return;
                                }
                                ChangeAction prevChange = changes;
                                ChangeAction nextChange = prevChange.Next;
                                while (nextChange != null)
                                {
                                    if (nextChange.Item == changeAction.Item)
                                    {
                                        changeAction.Next = nextChange.Next;
                                        prevChange.Next = changeAction;
                                        if (nextChange == lastChange)
                                        {
                                            lastChange = changeAction;
                                        }
                                        return;
                                    }
                                    prevChange = prevChange.Next;
                                    nextChange = prevChange.Next;
                                }

                                break;
                            default:
                                break;
                        }

                        // If there is a valid last change
                        if (lastChange != null)
                        {
                            // Set the change as the next change after the last one
                            lastChange.Next = changeAction;
                        }
                        lastChange = changeAction;
                    }
                }
            }



            /// <summary>
            /// Gets the type.
            /// </summary>
            /// <value>
            /// The type.
            /// </value>
            VisualElementCollectionChangeActionType IVisualElementCollectionChangeAction.Type
            {
                get { return this.ActionType; }
            }

            /// <summary>
            /// Gets the type of the action.
            /// </summary>
            /// <value>
            /// The type of the action.
            /// </value>
            protected abstract VisualElementCollectionChangeActionType ActionType { get; }


            /// <summary>
            /// Gets the item.
            /// </summary>
            /// <value>
            /// The item.
            /// </value>
            IVisualElement IVisualElementCollectionChangeAction.Item
            {
                get
                {
                    return this.Item;
                }
            }

            /// <summary>
            /// Gets the index.
            /// </summary>
            /// <value>
            /// The index.
            /// </value>
            int IVisualElementCollectionChangeAction.Index
            {
                get
                {
                    return this.Index;
                }
            }

            /// <summary>
            /// Gets the item.
            /// </summary>
            /// <value>
            /// The item.
            /// </value>
            protected abstract IVisualElement Item { get; }

            /// <summary>
            /// Gets the index.
            /// </summary>
            /// <value>
            /// The index.
            /// </value>
            protected abstract int Index { get; }

            /// <summary>
            /// Gets the collection length.
            /// </summary>
            /// <value>
            /// The collection length.
            /// </value>
            protected abstract int Length { get; }

            /// <summary>
            /// Gets the collection length.
            /// </summary>
            /// <value>
            /// The collection length.
            /// </value>
            int IVisualElementCollectionChangeAction.Length
            {
                get 
                { 
                    return this.Length;
                }
            }
        }

        /// <summary>
        /// Provides support clearing items
        /// </summary>
        private class ClearChangeAction : ChangeAction
        {
            public ClearChangeAction(string reason = null)
            {
                this.Msg = reason;
            }

            /// <summary>
            /// Gets the index.
            /// </summary>
            /// <value>
            /// The index.
            /// </value>
            protected override int Index
            {
                get
                {
                    return 0;
                }
            }

            /// <summary>
            /// Gets the length.
            /// </summary>
            /// <value>
            /// The length.
            /// </value>
            protected override int Length
            {
                get
                {
                    return 0;
                }
            }

            /// <summary>
            /// Gets the item.
            /// </summary>
            /// <value>
            /// The item.
            /// </value>
            protected override IVisualElement Item
            {
                get
                {
                    return null;
                }
            }

            /// <summary>
            /// Gets the type of the action.
            /// </summary>
            /// <value>
            /// The type of the action.
            /// </value>
            protected override VisualElementCollectionChangeActionType ActionType
            {
                get
                {
                    return VisualElementCollectionChangeActionType.ClearItems;
                }
            }
        }

        /// <summary>
        /// Provides support for updating items
        /// </summary>
        private class UpdateChangeAction : ItemChangeAction
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="IndexChangeAction" /> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="length">The length.</param>
            /// <param name="item">The item.</param>
            public UpdateChangeAction(int index, int length, IVisualElement item)
                : base(index, length, item)
            {

            }

            /// <summary>
            /// Gets the type of the action.
            /// </summary>
            /// <value>
            /// The type of the action.
            /// </value>
            protected override VisualElementCollectionChangeActionType ActionType
            {
                get
                {
                    return VisualElementCollectionChangeActionType.SetItem;
                }
            }
        }

        /// <summary>
        /// Provides support for index based change action
        /// </summary>
        private abstract class IndexChangeAction : ChangeAction
        {
            /// <summary>
            /// The index
            /// </summary>
            private readonly int _index;

            /// <summary>
            /// The length
            /// </summary>
            private readonly int _length;

            /// <summary>
            /// Initializes a new instance of the <see cref="IndexChangeAction" /> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="length">The length.</param>
            public IndexChangeAction(int index, int length)
            {
                _index = index;
                _length = length;
            }

            /// <summary>
            /// Gets the index.
            /// </summary>
            /// <value>
            /// The index.
            /// </value>
            protected override int Index
            {
                get { return _index; }
            }

            /// <summary>
            /// Gets the length.
            /// </summary>
            /// <value>
            /// The length.
            /// </value>
            protected override int Length
            {
                get
                {
                    return _length;
                }
            }

        }

        /// <summary>
        /// Provides support for index and item based change action
        /// </summary>
        private abstract class ItemChangeAction : IndexChangeAction
        {
            /// <summary>
            /// The item
            /// </summary>
            private readonly IVisualElement _item;

            /// <summary>
            /// Initializes a new instance of the <see cref="ItemChangeAction" /> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="length">The length.</param>
            /// <param name="item">The item.</param>
            public ItemChangeAction(int index, int length, IVisualElement item)
                : base(index, length)
            {
                _item = item;
            }


            /// <summary>
            /// Gets the item.
            /// </summary>
            /// <value>
            /// The item.
            /// </value>
            protected override IVisualElement Item
            {
                get { return _item; }
            }
        }

        /// <summary>
        /// Provides support setting items
        /// </summary>
        private class SetItemChangeAction : ItemChangeAction
        {


            /// <summary>
            /// Initializes a new instance of the <see cref="SetItemChangeAction" /> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="length">The length.</param>
            /// <param name="item">The item.</param>
            public SetItemChangeAction(int index, int length, TVisualElement item)
                : base(index, length, item)
            {

            }

            /// <summary>
            /// Gets the type of the action.
            /// </summary>
            /// <value>
            /// The type of the action.
            /// </value>
            protected override VisualElementCollectionChangeActionType ActionType
            {
                get
                {
                    return VisualElementCollectionChangeActionType.SetItem;
                }
            }
        }

        /// <summary>
        /// Provides support inserting items
        /// </summary>
        private class InsertItemChangeAction : ItemChangeAction
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="InsertItemChangeAction" /> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="length">The length.</param>
            /// <param name="item">The item.</param>
            public InsertItemChangeAction(int index, int length, TVisualElement item)
                : base(index, length, item)
            {

            }

            /// <summary>
            /// Gets the type of the action.
            /// </summary>
            /// <value>
            /// The type of the action.
            /// </value>
            protected override VisualElementCollectionChangeActionType ActionType
            {
                get
                {
                    return VisualElementCollectionChangeActionType.InsertItem;
                }
            }

        }

        /// <summary>
        /// Provides support removing items
        /// </summary>
        private class RemoveItemChangeAction : ItemChangeAction
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="RemoveItemChangeAction" /> class.
            /// </summary>
            /// <param name="index">The index.</param>
            /// <param name="length">The length.</param>
            /// <param name="item">The item.</param>
            public RemoveItemChangeAction(int index, int length, TVisualElement item)
                : base(index, length, item)
            {

            }

            /// <summary>
            /// Gets the type of the action.
            /// </summary>
            /// <value>
            /// The type of the action.
            /// </value>
            protected override VisualElementCollectionChangeActionType ActionType
            {
                get
                {
                    return VisualElementCollectionChangeActionType.RemoveItem;
                }
            }

        }
        
        #endregion

        #region Change Targets

        /// <summary>
        /// Provides support for change target implementation
        /// </summary>
        protected abstract class ChangeTarget : IVisualElementCollectionChangeTarget
        {

            /// <summary>
            /// Notifies the change.
            /// </summary>
            /// <param name="actionsContainer">The actions container.</param>
            /// <param name="targetElement">The target element.</param>
            public abstract void NotifyChange(IVisualElementCollectionChangeActionsContainer actionsContainer, IVisualElement targetElement);
        }

        /// <summary>
        /// Provides support for property change
        /// </summary>
        protected class CollectionPropertyChangeTarget : ChangeTarget
        {
            /// <summary>
            /// The property name
            /// </summary>
            private string _propertyName;

            /// <summary>
            /// Initializes a new instance of the <see cref="CollectionPropertyChangeTarget"/> class.
            /// </summary>
            /// <param name="propertyName">The property name.</param>
            public CollectionPropertyChangeTarget(string propertyName)
            {
                _propertyName = propertyName;
            }

            /// <summary>
            /// Notifies the change.
            /// </summary>
            /// <param name="actionsContainer">The actions container.</param>
            /// <param name="targetElement">The target element.</param>
            public override void NotifyChange(IVisualElementCollectionChangeActionsContainer actionsContainer, IVisualElement targetElement)
            {
                // Get the visual element
                VisualElement visualElement = targetElement as VisualElement;

                // If there is a valid visual element
                if (visualElement != null)
                {
                    // Handle property change
                    ApplicationElement.RegisterChange(visualElement, _propertyName);
                }
       
            }


            /// <summary>
            /// Creates a property change target
            /// </summary>
            /// <param name="propertyName">Name of the property.</param>
            /// <returns></returns>
            internal static IVisualElementCollectionChangeTarget Create(string propertyName)
            {
                // If there is a valid property name
                if (!string.IsNullOrEmpty(propertyName))
                {
                    // Set the change target
                    return new CollectionPropertyChangeTarget(propertyName);
                }

                return null;
            }
        }

        /// <summary>
        /// Provides support for changes in items bindings
        /// </summary>
        protected class CollectionBindingItemsChangeTarget : ChangeTarget
        {


            /// <summary>
            /// Initializes a new instance of the <see cref="CollectionBindingItemsChangeTarget"/> class.
            /// </summary>
            public CollectionBindingItemsChangeTarget()
            {
            }

            /// <summary>
            /// Notifies the change.
            /// </summary>
            /// <param name="actionsContainer">The actions container.</param>
            /// <param name="targetElement">The target element.</param>
            public override void NotifyChange(IVisualElementCollectionChangeActionsContainer actionsContainer, IVisualElement targetElement)
            {
                // Get the binding manager
                BindingViewModelManager bindingViewModelManager = BindingManager.GetBindingManager(targetElement as VisualElement) as BindingViewModelManager;

                // If there is a valid visual element
                if (bindingViewModelManager != null)
                {
                    // Update the client on model changes
                    bindingViewModelManager.UpdateClient();
                }

            }
        }

        /// <summary>
        /// Provides support for changes in columns bindings
        /// </summary>
        protected class CollectionBindingColumnsChangeTarget : ChangeTarget
        {


            /// <summary>
            /// Initializes a new instance of the <see cref="CollectionBindingColumnsChangeTarget"/> class.
            /// </summary>
            public CollectionBindingColumnsChangeTarget()
            {
            }

            /// <summary>
            /// Notifies the change.
            /// </summary>
            /// <param name="actionsContainer">The actions container.</param>
            /// <param name="targetElement">The target element.</param>
            public override void NotifyChange(IVisualElementCollectionChangeActionsContainer actionsContainer, IVisualElement targetElement)
            {
                // Get the binding manager
                BindingViewModelManager bindingViewModelManager = BindingManager.GetBindingManager(targetElement as VisualElement) as BindingViewModelManager;

                // If there is a valid visual element
                if (bindingViewModelManager != null)
                {
                    // Update the client on model changes
                    bindingViewModelManager.UpdateClient();
                }

            }
        }

        #endregion

        /// <summary>
        /// The parent element
        /// </summary>
        private readonly VisualElement _parentElement;

        /// <summary>
        /// The changes
        /// </summary>
        private ChangeAction _changes = null;

        /// <summary>
        /// The Last Change
        /// </summary>
        private ChangeAction _lastChange = null;

        /// <summary>
        /// The change target
        /// </summary>
        private readonly IVisualElementCollectionChangeTarget _changeTarget = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementCollection{TVisualElement}"/> class.
        /// </summary>
        internal VisualElementCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementCollection{TVisualElement}"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">Name of the property.</param>
        internal VisualElementCollection(VisualElement parentElement, string propertyName)
            : this(parentElement, CollectionPropertyChangeTarget.Create(propertyName))
        {
                     
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementCollection{TVisualElement}"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="changeTarget">The change target.</param>
        public VisualElementCollection(VisualElement parentElement, IVisualElementCollectionChangeTarget changeTarget)
            : this(parentElement)
        {
            // Set the change target
            _changeTarget = changeTarget;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementCollection{TVisualElement}"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        public VisualElementCollection(VisualElement parentElement)
        {
            // Set the parent element
            _parentElement = parentElement;
        }

        /// <summary>
        /// Gets a value indicating whether track changes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if track changes; otherwise, <c>false</c>.
        /// </value>
        protected virtual bool TrackChanges
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the current change.
        /// </summary>
        /// <value>
        /// The current change.
        /// </value>
        protected IVisualElementCollectionChangeAction CurrentChange
        {
            get
            {
                return _changes;
            }
        }

        /// <summary>
        /// Gets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        internal VisualElement Owner
        {
            get
            {
                return _parentElement;
            }
        }

        /// <summary>
        /// Determines whether is there loop
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="childElement">The child element.</param>
        /// <returns>If there is loop</returns>
        protected bool IsThereLoop(VisualElement visualElement, VisualElement childElement)
        {
            // If one of parameters is null - there is no loop
            if (childElement == null || visualElement == null)
            {
                return false;
            }
            // If child is the same as the element - there is loop
            if (childElement == visualElement)
            {
                return true;
            }
            // Check next parent
            bool blnIsThereLoop = IsThereLoop(visualElement.ParentElement, childElement);
            return blnIsThereLoop;
        }


        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="elements">The elements.</param>
        public void AddRange(IEnumerable<TVisualElement> elements)
        {
            // If there are valid controls
            if (elements != null)
            {
                // Loop all controls
                foreach (TVisualElement controlElement in elements)
                {
                    // If there is loop continue
                    if(IsThereLoop(this._parentElement, controlElement))
                    {
                        continue;
                    }
                    // If the collection already contains such element
                    if (this.Contains(controlElement))
                    {
                        continue;
                    }
                    // Add the current control
                    Add(controlElement);
                }
            }
        }

        /// <summary>
        /// Adds the index of the with.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        protected int AddWithIndex(TVisualElement element)
        {
            // Add the element
            Add(element);

            // Return the index
            return Count - 1;
        }

        /// <summary>
        /// Gets the <see cref="TVisualElement"/> with the specified name.
        /// </summary>
        /// <value>
        /// The <see cref="TVisualElement"/>.
        /// </value>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public virtual TVisualElement this[string name]
        {
            get
            {
                // Loop all visual elements
                foreach (TVisualElement visualElement in this)
                {
                    // If there is a valid element and it's name is a match
                    if (visualElement != null && string.Equals(visualElement.ID, name, StringComparison.Ordinal))
                    {
                        return visualElement;
                    }
                }
                return null;
            }

        }

        /// <summary>
        /// Inserts the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InsertItem(int index, TVisualElement item)
        {
            // If there is a valid item
            if (item != null)
            {
                // Set parent item
                item.ParentElement = _parentElement;

                // Insert item
                base.InsertItem(index, item);

                // If we should track changes
                if (this.TrackChangesRequired(item))
                {
                    // Notify collection changed
                    OnCollectionChanged(new InsertItemChangeAction(index, this.Count, item));
                }
            }
        }

        /// <summary>
        /// Sets the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void SetItem(int index, TVisualElement item)
        {
            // If there is a valid item
            if (item != null)
            {
                // Set the parent item
                item.ParentElement = _parentElement;

                // Set item
                base.SetItem(index, item);

                SetItemChanged(index, item);
            }
        }

        /// <summary>
        /// Sets the item changed.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        private void SetItemChanged(int index, TVisualElement item)
        {
                // If we should track changes
            if (this.TrackChangesRequired(item))
            {
                OnItemChanged(index, item);
            }
        }

        /// <summary>
        /// Called when item changed.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        private void OnItemChanged(int index, TVisualElement item)
                {
                    // Notify collection changed
                    OnCollectionChanged(new SetItemChangeAction(index, this.Count, item));
                }

        /// <summary>
        /// Updates the item.
        /// </summary>
        /// <param name="item">The item.</param>
        public virtual void UpdateItem(TVisualElement item)
        {
            // If there is a valid item
            if (this.TrackChangesRequired(item))
            {
                int index = this.IndexOf(item);
                if (index >= 0)
                {
                    OnItemChanged(index, item);
                }
            }
        }

        /// <summary>
        /// Check if change tracking should be updated
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private bool TrackChangesRequired(TVisualElement item)
        {
            // If there is a valid item
            if (item != null)
            {
                // Check if change tracking suspended
                return TrackChangesRequired();
            }
            return false;
        }

        /// <summary>
        /// Change tracking required.
        /// </summary>
        /// <returns></returns>
        private bool TrackChangesRequired()
        {
            // Check if collection support change tracking
            if (this.TrackChanges)
            {
                // Access parent element
                VisualElement parent = this._parentElement;
                if (parent != null)
                {
                    // Try to get binding manager
                    BindingManager bindingManager = parent.BindingManager;
                    if (bindingManager != null)
                    {
                        // Check if change tracking suspended
                        return !bindingManager.SuspendServerSideTracking;
                    }
                    else
                    {
                        // require tracking for unbound collections
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Removes the element at the specified index of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        protected override void RemoveItem(int index)
        {
            TVisualElement element = this[index];
            base.RemoveItem(index);

            // If we should track changes
            if (this.TrackChangesRequired())
            {
                // Notify collection changed
                OnCollectionChanged(new RemoveItemChangeAction(index, this.Count, element));
            }
        }

        /// <summary>
        /// Removes items with comment
        /// </summary>
        protected virtual void ClearItems(string reason)
        {
            base.ClearItems();

            // If we should track changes
            if (this.TrackChangesRequired())
            {
                // Notify collection changed
                OnCollectionChanged(new ClearChangeAction(reason));
            }
        }

        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        protected override void ClearItems()
        {
            base.ClearItems();

            // If we should track changes
            if (this.TrackChangesRequired())
            {
                // Notify collection changed
                OnCollectionChanged(new ClearChangeAction());
            }
        }

        /// <summary>
        /// Called when collection changed.
        /// </summary>
        /// <param name="changeAction">The change action.</param>
        private void OnCollectionChanged(ChangeAction changeAction)
        {
            // If there is a valid change target
            if (_changeTarget != null)
            {
                // If we need to update the client 
                if (ApplicationElement.GlobalRevision != this._parentElement.Revision)
                {
                    // Handle change action append
                    this.OnAppendChangeAction(changeAction);

                    // Notify collection changed
                    this.OnCollectionChanged();
                }
            }
        }

        /// <summary>
        /// Called when append change action.
        /// </summary>
        /// <param name="changeAction">The change action.</param>
        protected virtual void OnAppendChangeAction(IVisualElementCollectionChangeAction changeAction)
        {
            // AllowAppend change action
            ChangeAction.Append(ref _changes, changeAction as ChangeAction, ref _lastChange);

            
        }

        /// <summary>
        /// Called when collection changed.
        /// </summary>
        protected virtual void OnCollectionChanged()
        {
            // If there is a valid change target
            if (_changeTarget != null)
            {
                // Notify parent change
                _changeTarget.NotifyChange(this, _parentElement);
            }
        }

        /// <summary>
        /// Gets the change actions.
        /// </summary>
        /// <value>
        /// The change actions.
        /// </value>
        IEnumerable<IVisualElementCollectionChangeAction> IVisualElementCollectionChangeActionsContainer.ChangeActions
        {
            get
            { 
                // If there are changes
                if (_changes != null)
                {
                    // Loop all changes 
                    foreach (ChangeAction change in _changes)
                    {
                        // Return change
                        yield return change;
                    }
                }
            }
        }

        /// <summary>
        /// Clears the change actions.
        /// </summary>
        void IVisualElementCollectionChangeActionsContainer.ClearChangeActions()
        {
            _changes = null;
            _lastChange = null;
        }
    }

}
