﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Common;
using System.Diagnostics;

namespace System.Web.VisualTree.Engine
{


    public class VisualTreeManager
    {

        /// <summary>
        /// The element renderers
        /// </summary>
        private static Dictionary<Tuple<RenderingEnvironmentId, Type>, IVisualElementRenderer> _elementRenderers = new Dictionary<Tuple<RenderingEnvironmentId, Type>, IVisualElementRenderer>();




        /// <summary>
        /// Renders the element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public static string RenderElement(RenderingContext renderingContext, VisualElement visualElement)
        {
            return RenderElement(renderingContext, visualElement, null);
        }




        /// <summary>
        /// Renders the element.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="renderTo">The render to.</param>
        /// <returns></returns>
        public static string RenderElement(RenderingContext renderingContext, VisualElement visualElement, string renderTo)
        {
            // If there is a valid visual element
            if (visualElement != null)
            {
                // Get renderer
                IVisualElementRenderer objVisualElementRenderer = GetRenderer(RenderingEnvironment.GetRenderingEnvironment(renderingContext), typeof(ApplicationElement));

                // If there is a valid renderer
                if (objVisualElementRenderer != null)
                {
                    // Render visual element
                    return objVisualElementRenderer.Render(renderingContext, visualElement, renderTo);
                }
            }

            return null;
        }




        /// <summary>
        /// Gets the renderer.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="methodInvoker">The method invoker.</param>
        /// <returns></returns>
        public static IVisualElementRenderer GetRenderer(RenderingContext renderingContext, IVisualElementMethodInvoker methodInvoker)
        {
            // If there is a valid action
            if (methodInvoker != null)
            {
                // Get owner 
                object methodOwner = methodInvoker.Owner;

                // If there is a valid owner
                if (methodOwner != null)
                {
                    // Get method owner type
                    Type methodTypeOwner = methodOwner as Type;

                    // If there is a valid owner type
                    if (methodTypeOwner != null)
                    {
                        // Get renderer by the owner type
                        return GetRenderer(RenderingEnvironment.GetRenderingEnvironment(renderingContext), methodTypeOwner);
                    }
                    else
                    {
                        // Get owner as element
                        IVisualElement methodElementOwner = methodOwner as IVisualElement;

                        // If there is a valid owner element
                        if (methodElementOwner != null)
                        {
                            // Get renderer by element
                            return GetRenderer(renderingContext, methodElementOwner);
                        }
                    }
                }
            }

            return null;
        }




        /// <summary>
        /// Gets the renderer.
        /// </summary>
        /// <param name="renderingContext">The rendering context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public static IVisualElementRenderer GetRenderer(RenderingContext renderingContext, IVisualElement visualElement)
        {
            // If there is a valid visual element
            if (visualElement != null)
            {
                // Get renderer
                IVisualElementRenderer objVisualElementRenderer = GetRenderer(RenderingEnvironment.GetRenderingEnvironment(renderingContext), visualElement.GetType());

                // If there is a valid visual renderer
                if (objVisualElementRenderer != null)
                {
                    // Revise and return renderer
                    return objVisualElementRenderer.ReviseRenderer(renderingContext, visualElement);
                }
            }

            return null;
        }




        /// <summary>
        /// Gets the renderer.
        /// </summary>
        /// <param name="renderingEnvironment">The rendering environment.</param>
        /// <param name="elementType">Type of the object element.</param>
        /// <returns></returns>
        public static IVisualElementRenderer GetRenderer(RenderingEnvironment renderingEnvironment, Type elementType)
        {
            // The visual element renderer
            IVisualElementRenderer visualElementRenderer = null;

            // If there is a valid element type
            if (elementType != null)
            {
                // Get the visual element renderer
                if (!TryGetRenderer(renderingEnvironment, elementType, out visualElementRenderer))
                {
                    // Get renderer from base type
                    visualElementRenderer = GetRenderer(renderingEnvironment, elementType.BaseType);
                }
            }

            // Return element renderer
            return visualElementRenderer;
        }





        /// <summary>
        /// Tries the get renderer.
        /// </summary>
        /// <param name="renderingEnvironment">The rendering environment.</param>
        /// <param name="elementType">The element type.</param>
        /// <param name="visualElementRenderer">The visual element renderer.</param>
        /// <returns></returns>
        private static bool TryGetRenderer(RenderingEnvironment renderingEnvironment, Type elementType, out IVisualElementRenderer visualElementRenderer)
        {
            // Set default return value
            visualElementRenderer = null;

            // If there are element renderers
            if (_elementRenderers != null)
            {
                // Get the renderer key
                Tuple<RenderingEnvironmentId, Type> rendererKey = Tuple.Create(RenderingEnvironment.GetRenderingEnvironmentId(renderingEnvironment), elementType);

                // Try to get renderer
                if (_elementRenderers.TryGetValue(rendererKey, out visualElementRenderer))
                {
                    return true;
                }
                else if (renderingEnvironment != null)
                {
                    // Get parent environment
                    RenderingEnvironment parentRenderingEnvironment = renderingEnvironment.ParentEnvironment;

                    // If there is a valid parent environment
                    if (parentRenderingEnvironment != null)
                    {
                        // Get renderer using parent environment
                        return TryGetRenderer(parentRenderingEnvironment, elementType, out visualElementRenderer);
                    }
                }
            }

            return false;
        }





        /// <summary>
        /// Initializes the specified assemblies.
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        public static void Initialize(HashSet<Reflection.Assembly> assemblies)
        {
            VisualTreeLoader loader = new VisualTreeLoader();
            loader.InitializeRenderers(assemblies, _elementRenderers);
        }
    }
}
