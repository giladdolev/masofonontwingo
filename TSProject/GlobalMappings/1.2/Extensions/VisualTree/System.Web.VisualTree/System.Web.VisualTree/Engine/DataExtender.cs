﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Engine
{
    /// <summary>
    /// Provides support for extending elements bindings
    /// </summary>
    public class DataExtender<T> : IVisualElementExtender
    {

        /// <summary>
        /// Gets the extender.
        /// </summary>
        /// <value>
        /// The extender.
        /// </value>
        IVisualElementExtender IVisualElementExtender.Extender
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// Adds the extender.
        /// </summary>
        /// <param name="extender">The extender.</param>
        void IVisualElementExtender.AddExtender(IVisualElementExtender extender)
        {
            VisualElementExtender.AddExtender(this, extender);
        }

        /// <summary>
        /// Removes the extender.
        /// </summary>
        /// <param name="extender">The extender.</param>
        void IVisualElementExtender.RemoveExtender(IVisualElementExtender extender)
        {
            VisualElementExtender.RemoveExtender(this, extender);
        }
    }
}
