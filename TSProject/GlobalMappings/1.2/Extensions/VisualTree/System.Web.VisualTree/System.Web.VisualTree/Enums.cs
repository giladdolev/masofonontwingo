using System.Drawing;
using System.Runtime.InteropServices;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{

    public enum Dock
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// The top
        /// </summary>
        Top = 1,
        /// <summary>
        /// The bottom
        /// </summary>
        Bottom = 2,
        /// <summary>
        /// The left
        /// </summary>
        Left = 3,
        /// <summary>
        /// The right
        /// </summary>
        Right = 4,
        /// <summary>
        /// The fill
        /// </summary>
        Fill = 5
    }


    [Flags()]
    public enum Anchor
    {
        /// <summary>
        /// The bottom
        /// </summary>
        Bottom = 2,
        /// <summary>
        /// The left
        /// </summary>
        Left = 4,
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// The right
        /// </summary>
        Right = 8,
        /// <summary>
        /// The top
        /// </summary>
        Top = 1
    }


    public enum DialogResult
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The ok
        /// </summary>
        OK,
        /// <summary>
        /// The cancel
        /// </summary>
        Cancel,
        /// <summary>
        /// The abort
        /// </summary>
        Abort,
        /// <summary>
        /// The retry
        /// </summary>
        Retry,
        /// <summary>
        /// The ignore
        /// </summary>
        Ignore,
        /// <summary>
        /// The yes
        /// </summary>
        Yes,
        /// <summary>
        /// The no
        /// </summary>
        No
    }


    public enum DialogMode
    {
        /// <summary>
        /// The modal
        /// </summary>
        Modal,

        /// <summary>
        /// The modeless mode
        /// </summary>
        Modelles
    }


    public enum DrawMode
    {
        /// <summary>
        /// The normal
        /// </summary>
        Normal,
        /// <summary>
        /// The owner draw fixed
        /// </summary>
        OwnerDrawFixed,
        /// <summary>
        /// The owner draw variable
        /// </summary>
        OwnerDrawVariable
    }


    public enum ToolBarTextDirection
    {
        /// <summary>
        /// The inherit
        /// </summary>
        Inherit,
        /// <summary>
        /// The horizontal
        /// </summary>
        Horizontal,
        /// <summary>
        /// The vertical90
        /// </summary>
        Vertical90,
        /// <summary>
        /// The vertical270
        /// </summary>
        Vertical270
    }


    public enum TextImageRelation
    {
        /// <summary>
        /// The overlay
        /// </summary>
        Overlay,
        /// <summary>
        /// The image above text
        /// </summary>
        ImageAboveText = 1,
        /// <summary>
        /// The image before text
        /// </summary>
        ImageBeforeText = 4,
        /// <summary>
        /// The text before image
        /// </summary>
        TextBeforeImage = 8,
        /// <summary>
        /// The text above image
        /// </summary>
        TextAboveImage
    }


    public enum RightToLeft
    {
        /// <summary>
        /// The no
        /// </summary>
        No,
        /// <summary>
        /// The yes
        /// </summary>
        Yes,
        /// <summary>
        /// The inherit
        /// </summary>
        Inherit
    }


    public enum Orientation
    {
        /// <summary>
        /// The horizontal
        /// </summary>
        Horizontal,
        /// <summary>
        /// The vertical
        /// </summary>
        Vertical
    }



    public enum SortOrder
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The ascending
        /// </summary>
        Ascending,
        /// <summary>
        /// The descending
        /// </summary>
        Descending
    }


    public enum Day
    {
        /// <summary>
        /// The Sunday
        /// </summary>
        Sunday,
        /// <summary>
        /// The Monday
        /// </summary>
        Monday,
        /// <summary>
        /// The Tuesday
        /// </summary>
        Tuesday,
        /// <summary>
        /// The Wednesday
        /// </summary>
        Wesdneday,
        /// <summary>
        /// The Thursday
        /// </summary>
        Thursday,
        /// <summary>
        /// The friday
        /// </summary>
        Friday,
        /// <summary>
        /// The Saturday
        /// </summary>
        Saturday
    }



    public enum SizeType
    {
        /// <summary>
        /// The automatic size
        /// </summary>
        AutoSize,
        /// <summary>
        /// The absolute
        /// </summary>
        Absolute,
        /// <summary>
        /// The percent
        /// </summary>
        Percent
    }



    public enum BorderStyle
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The fixed single
        /// </summary>
        FixedSingle,
        /// <summary>
        /// The fixed3 d
        /// </summary>
        Fixed3D,
        /// <summary>
        /// The dotted
        /// </summary>
        Dotted,
        /// <summary>
        /// The dashed
        /// </summary>
        Dashed,
        /// <summary>
        /// The solid
        /// </summary>
        Solid,
        /// <summary>
        /// The double
        /// </summary>
        Double,
        /// <summary>
        /// The groove
        /// </summary>
        Groove,
        /// <summary>
        /// The ridge
        /// </summary>
        Ridge,
        /// <summary>
        /// The inset
        /// </summary>
        Inset,
        /// <summary>
        /// The outset
        /// </summary>
        Outset,
        /// <summary>
        /// The shadow box
        /// </summary>
        ShadowBox,
        /// <summary>
        /// The underline
        /// </summary>
        Underline,
        /// <summary>
        /// The inherit
        /// </summary>
        NotSet
    }


    public enum SizeGripStyle
    {
        /// <summary>
        /// The automatic
        /// </summary>
        Auto,
        /// <summary>
        /// The show
        /// </summary>
        Show,
        /// <summary>
        /// The hide
        /// </summary>
        Hide
    }


    public enum SelectionMode
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The one
        /// </summary>
        One,
        /// <summary>
        /// The multi simple
        /// </summary>
        MultiSimple,
        /// <summary>
        /// The multi extended
        /// </summary>
        MultiExtended
    }

    /// <summary>
    /// Provides support for masking key events
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class KeyMaskAttribute : Attribute
    {
        /// <summary>
        /// The key masks
        /// </summary>
        private readonly Keys[] _keyMasks;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyMaskAttribute" /> class.
        /// </summary>
        /// <param name="keyMasks">The key masks.</param>
        public KeyMaskAttribute(params Keys[] keyMasks)
        {
            _keyMasks = keyMasks;
        }


        /// <summary>
        /// Gets the key masks.
        /// </summary>
        /// <value>
        /// The key masks.
        /// </value>
        public Keys[] KeyMasks
        {
            get { return _keyMasks; }
        }


    }


    public enum Keys
    {
        /// <summary>
        /// The bitmask to extract a key code from a key value.
        /// </summary>
        KeyCode = 65535,
        /// <summary>
        /// The bitmask to extract modifiers from a key value.
        /// </summary>
        Modifiers = -65536,
        /// <summary>
        /// No key pressed.
        /// </summary>
        None = 0,
        /// <summary>
        /// The left mouse button.
        /// </summary>
        LButton = 1,
        /// <summary>
        /// The right mouse button.
        /// </summary>
        RButton = 2,
        /// <summary>
        /// The CANCEL key.
        /// </summary>
        Cancel = 3,
        /// <summary>
        /// The middle mouse button (three-button mouse).
        /// </summary>
        MButton = 4,
        /// <summary>
        /// The first x mouse button (five-button mouse).
        /// </summary>
        XButton1 = 5,
        /// <summary>
        /// The second x mouse button (five-button mouse).
        /// </summary>
        XButton2 = 6,
        /// <summary>
        /// The BACKSPACE key.
        /// </summary>
        Back = 8,
        /// <summary>
        /// The TAB key.
        /// </summary>
        Tab = 9,
        /// <summary>
        /// The LINEFEED key.
        /// </summary>
        Linefeed = 10,
        /// <summary>
        /// The CLEAR key.
        /// </summary>
        Clear = 12,
        /// <summary>
        /// The RETURN key.
        /// </summary>
        Return = 13,
        /// <summary>
        /// The ENTER key.
        /// </summary>
        Enter = 13,
        /// <summary>
        /// The SHIFT key.
        /// </summary>
        ShiftKey = 16,
        /// <summary>
        /// The CTRL key.
        /// </summary>
        ControlKey = 17,
        /// <summary>
        /// The ALT key.
        /// </summary>
        Menu = 18,
        /// <summary>
        /// The PAUSE key.
        /// </summary>
        Pause = 19,
        /// <summary>
        /// The CAPS LOCK key.
        /// </summary>
        Capital = 20,
        /// <summary>
        /// The CAPS LOCK key.
        /// </summary>
        CapsLock = 20,
        /// <summary>
        /// The IME Kana mode key.
        /// </summary>
        KanaMode = 21,
        /// <summary>
        /// The IME Hanguel mode key. (maintained for compatibility; use HangulMode)
        /// </summary>
        HanguelMode = 21,
        /// <summary>
        /// The IME Hangul mode key.
        /// </summary>
        HangulMode = 21,
        /// <summary>
        /// The IME Junja mode key.
        /// </summary>
        JunjaMode = 23,
        /// <summary>
        /// The IME final mode key.
        /// </summary>
        FinalMode = 24,
        /// <summary>
        /// The IME Hanja mode key.
        /// </summary>
        HanjaMode = 25,
        /// <summary>
        /// The IME Kanji mode key.
        /// </summary>
        KanjiMode = 25,
        /// <summary>
        /// The ESC key.
        /// </summary>
        Escape = 27,
        /// <summary>
        /// The IME convert key.
        /// </summary>
        IMEConvert = 28,
        /// <summary>
        /// The IME nonconvert key.
        /// </summary>
        IMENonconvert = 29,
        /// <summary>
        /// The IME accept key, replaces <see cref="F:System.Windows.Forms.Keys.IMEAceept" />.
        /// </summary>
        IMEAccept = 30,
        /// <summary>
        /// The IME accept key. Obsolete, use <see cref="F:System.Windows.Forms.Keys.IMEAccept" /> instead.
        /// </summary>
        IMEAceept = 30,
        /// <summary>
        /// The IME mode change key.
        /// </summary>
        IMEModeChange = 31,
        /// <summary>
        /// The SPACEBAR key.
        /// </summary>
        Space = 32,
        /// <summary>
        /// The PAGE UP key.
        /// </summary>
        Prior = 33,
        /// <summary>
        /// The PAGE UP key.
        /// </summary>
        PageUp = 33,
        /// <summary>
        /// The PAGE DOWN key.
        /// </summary>
        Next = 34,
        /// <summary>
        /// The PAGE DOWN key.
        /// </summary>
        PageDown = 34,
        /// <summary>
        /// The END key.
        /// </summary>
        End = 35,
        /// <summary>
        /// The HOME key.
        /// </summary>
        Home = 36,
        /// <summary>
        /// The LEFT ARROW key.
        /// </summary>
        Left = 37,
        /// <summary>
        /// The UP ARROW key.
        /// </summary>
        Up = 38,
        /// <summary>
        /// The RIGHT ARROW key.
        /// </summary>
        Right = 39,
        /// <summary>
        /// The DOWN ARROW key.
        /// </summary>
        Down = 40,
        /// <summary>
        /// The SELECT key.
        /// </summary>
        Select = 41,
        /// <summary>
        /// The PRINT key.
        /// </summary>
        Print = 42,
        /// <summary>
        /// The EXECUTE key.
        /// </summary>
        Execute = 43,
        /// <summary>
        /// The PRINT SCREEN key.
        /// </summary>
        Snapshot = 44,
        /// <summary>
        /// The PRINT SCREEN key.
        /// </summary>
        PrintScreen = 44,
        /// <summary>
        /// The INS key.
        /// </summary>
        Insert = 45,
        /// <summary>
        /// The DEL key.
        /// </summary>
        Delete = 46,
        /// <summary>
        /// The HELP key.
        /// </summary>
        Help = 47,
        /// <summary>
        /// The 0 key.
        /// </summary>
        D0 = 48,
        /// <summary>
        /// The 1 key.
        /// </summary>
        D1 = 49,
        /// <summary>
        /// The 2 key.
        /// </summary>
        D2 = 50,
        /// <summary>
        /// The 3 key.
        /// </summary>
        D3 = 51,
        /// <summary>
        /// The 4 key.
        /// </summary>
        D4 = 52,
        /// <summary>
        /// The 5 key.
        /// </summary>
        D5 = 53,
        /// <summary>
        /// The 6 key.
        /// </summary>
        D6 = 54,
        /// <summary>
        /// The 7 key.
        /// </summary>
        D7 = 55,
        /// <summary>
        /// The 8 key.
        /// </summary>
        D8 = 56,
        /// <summary>
        /// The 9 key.
        /// </summary>
        D9 = 57,
        /// <summary>
        /// The A key.
        /// </summary>
        A = 65,
        /// <summary>
        /// The B key.
        /// </summary>
        B = 66,
        /// <summary>
        /// The C key.
        /// </summary>
        C = 67,
        /// <summary>
        /// The D key.
        /// </summary>
        D = 68,
        /// <summary>
        /// The E key.
        /// </summary>
        E = 69,
        /// <summary>
        /// The F key.
        /// </summary>
        F = 70,
        /// <summary>
        /// The G key.
        /// </summary>
        G = 71,
        /// <summary>
        /// The H key.
        /// </summary>
        H = 72,
        /// <summary>
        /// The I key.
        /// </summary>
        I = 73,
        /// <summary>
        /// The J key.
        /// </summary>
        J = 74,
        /// <summary>
        /// The K key.
        /// </summary>
        K = 75,
        /// <summary>
        /// The L key.
        /// </summary>
        L = 76,
        /// <summary>
        /// The M key.
        /// </summary>
        M = 77,
        /// <summary>
        /// The N key.
        /// </summary>
        N = 78,
        /// <summary>
        /// The O key.
        /// </summary>
        O = 79,
        /// <summary>
        /// The P key.
        /// </summary>
        P = 80,
        /// <summary>
        /// The Q key.
        /// </summary>
        Q = 81,
        /// <summary>
        /// The R key.
        /// </summary>
        R = 82,
        /// <summary>
        /// The S key.
        /// </summary>
        S = 83,
        /// <summary>
        /// The T key.
        /// </summary>
        T = 84,
        /// <summary>
        /// The U key.
        /// </summary>
        U = 85,
        /// <summary>
        /// The V key.
        /// </summary>
        V = 86,
        /// <summary>
        /// The W key.
        /// </summary>
        W = 87,
        /// <summary>
        /// The X key.
        /// </summary>
        X = 88,
        /// <summary>
        /// The Y key.
        /// </summary>
        Y = 89,
        /// <summary>
        /// The Z key.
        /// </summary>
        Z = 90,
        /// <summary>
        /// The left Windows logo key (Microsoft Natural Keyboard).
        /// </summary>
        LWin = 91,
        /// <summary>
        /// The right Windows logo key (Microsoft Natural Keyboard).
        /// </summary>
        RWin = 92,
        /// <summary>
        /// The application key (Microsoft Natural Keyboard).
        /// </summary>
        Apps = 93,
        /// <summary>
        /// The computer sleep key.
        /// </summary>
        Sleep = 95,
        /// <summary>
        /// The 0 key on the numeric keypad.
        /// </summary>
        NumPad0 = 96,
        /// <summary>
        /// The 1 key on the numeric keypad.
        /// </summary>
        NumPad1 = 97,
        /// <summary>
        /// The 2 key on the numeric keypad.
        /// </summary>
        NumPad2 = 98,
        /// <summary>
        /// The 3 key on the numeric keypad.
        /// </summary>
        NumPad3 = 99,
        /// <summary>
        /// The 4 key on the numeric keypad.
        /// </summary>
        NumPad4 = 100,
        /// <summary>
        /// The 5 key on the numeric keypad.
        /// </summary>
        NumPad5 = 101,
        /// <summary>
        /// The 6 key on the numeric keypad.
        /// </summary>
        NumPad6 = 102,
        /// <summary>
        /// The 7 key on the numeric keypad.
        /// </summary>
        NumPad7 = 103,
        /// <summary>
        /// The 8 key on the numeric keypad.
        /// </summary>
        NumPad8 = 104,
        /// <summary>
        /// The 9 key on the numeric keypad.
        /// </summary>
        NumPad9 = 105,
        /// <summary>
        /// The multiply key.
        /// </summary>
        Multiply = 106,
        /// <summary>
        /// The add key.
        /// </summary>
        Add = 107,
        /// <summary>
        /// The separator key.
        /// </summary>
        Separator = 108,
        /// <summary>
        /// The subtract key.
        /// </summary>
        Subtract = 109,
        /// <summary>
        /// The decimal key.
        /// </summary>
        Decimal = 110,
        /// <summary>
        /// The divide key.
        /// </summary>
        Divide = 111,
        /// <summary>
        /// The F1 key.
        /// </summary>
        F1 = 112,
        /// <summary>
        /// The F2 key.
        /// </summary>
        F2 = 113,
        /// <summary>
        /// The F3 key.
        /// </summary>
        F3 = 114,
        /// <summary>
        /// The F4 key.
        /// </summary>
        F4 = 115,
        /// <summary>
        /// The F5 key.
        /// </summary>
        F5 = 116,
        /// <summary>
        /// The F6 key.
        /// </summary>
        F6 = 117,
        /// <summary>
        /// The F7 key.
        /// </summary>
        F7 = 118,
        /// <summary>
        /// The F8 key.
        /// </summary>
        F8 = 119,
        /// <summary>
        /// The F9 key.
        /// </summary>
        F9 = 120,
        /// <summary>
        /// The F10 key.
        /// </summary>
        F10 = 121,
        /// <summary>
        /// The F11 key.
        /// </summary>
        F11 = 122,
        /// <summary>
        /// The F12 key.
        /// </summary>
        F12 = 123,
        /// <summary>
        /// The F13 key.
        /// </summary>
        F13 = 124,
        /// <summary>
        /// The F14 key.
        /// </summary>
        F14 = 125,
        /// <summary>
        /// The F15 key.
        /// </summary>
        F15 = 126,
        /// <summary>
        /// The F16 key.
        /// </summary>
        F16 = 127,
        /// <summary>
        /// The F17 key.
        /// </summary>
        F17 = 128,
        /// <summary>
        /// The F18 key.
        /// </summary>
        F18 = 129,
        /// <summary>
        /// The F19 key.
        /// </summary>
        F19 = 130,
        /// <summary>
        /// The F20 key.
        /// </summary>
        F20 = 131,
        /// <summary>
        /// The F21 key.
        /// </summary>
        F21 = 132,
        /// <summary>
        /// The F22 key.
        /// </summary>
        F22 = 133,
        /// <summary>
        /// The F23 key.
        /// </summary>
        F23 = 134,
        /// <summary>
        /// The F24 key.
        /// </summary>
        F24 = 135,
        /// <summary>
        /// The NUM LOCK key.
        /// </summary>
        NumLock = 144,
        /// <summary>
        /// The SCROLL LOCK key.
        /// </summary>
        Scroll = 145,
        /// <summary>
        /// The left SHIFT key.
        /// </summary>
        LShiftKey = 160,
        /// <summary>
        /// The right SHIFT key.
        /// </summary>
        RShiftKey = 161,
        /// <summary>
        /// The left CTRL key.
        /// </summary>
        LControlKey = 162,
        /// <summary>
        /// The right CTRL key.
        /// </summary>
        RControlKey = 163,
        /// <summary>
        /// The left ALT key.
        /// </summary>
        LMenu = 164,
        /// <summary>
        /// The right ALT key.
        /// </summary>
        RMenu = 165,
        /// <summary>
        /// The browser back key (Windows 2000 or later).
        /// </summary>
        BrowserBack = 166,
        /// <summary>
        /// The browser forward key (Windows 2000 or later).
        /// </summary>
        BrowserForward = 167,
        /// <summary>
        /// The browser refresh key (Windows 2000 or later).
        /// </summary>
        BrowserRefresh = 168,
        /// <summary>
        /// The browser stop key (Windows 2000 or later).
        /// </summary>
        BrowserStop = 169,
        /// <summary>
        /// The browser search key (Windows 2000 or later).
        /// </summary>
        BrowserSearch = 170,
        /// <summary>
        /// The browser favorites key (Windows 2000 or later).
        /// </summary>
        BrowserFavorites = 171,
        /// <summary>
        /// The browser home key (Windows 2000 or later).
        /// </summary>
        BrowserHome = 172,
        /// <summary>
        /// The volume mute key (Windows 2000 or later).
        /// </summary>
        VolumeMute = 173,
        /// <summary>
        /// The volume down key (Windows 2000 or later).
        /// </summary>
        VolumeDown = 174,
        /// <summary>
        /// The volume up key (Windows 2000 or later).
        /// </summary>
        VolumeUp = 175,
        /// <summary>
        /// The media next track key (Windows 2000 or later).
        /// </summary>
        MediaNextTrack = 176,
        /// <summary>
        /// The media previous track key (Windows 2000 or later).
        /// </summary>
        MediaPreviousTrack = 177,
        /// <summary>
        /// The media Stop key (Windows 2000 or later).
        /// </summary>
        MediaStop = 178,
        /// <summary>
        /// The media play pause key (Windows 2000 or later).
        /// </summary>
        MediaPlayPause = 179,
        /// <summary>
        /// The launch mail key (Windows 2000 or later).
        /// </summary>
        LaunchMail = 180,
        /// <summary>
        /// The select media key (Windows 2000 or later).
        /// </summary>
        SelectMedia = 181,
        /// <summary>
        /// The start application one key (Windows 2000 or later).
        /// </summary>
        LaunchApplication1 = 182,
        /// <summary>
        /// The start application two key (Windows 2000 or later).
        /// </summary>
        LaunchApplication2 = 183,
        /// <summary>
        /// The OEM Semicolon key on a US standard keyboard (Windows 2000 or later).
        /// </summary>
        OemSemicolon = 186,
        /// <summary>
        /// The OEM 1 key.
        /// </summary>
        Oem1 = 186,
        /// <summary>
        /// The OEM plus key on any country/region keyboard (Windows 2000 or later).
        /// </summary>
        Oemplus = 187,
        /// <summary>
        /// The OEM comma key on any country/region keyboard (Windows 2000 or later).
        /// </summary>
        Oemcomma = 188,
        /// <summary>
        /// The OEM minus key on any country/region keyboard (Windows 2000 or later).
        /// </summary>
        OemMinus = 189,
        /// <summary>
        /// The OEM period key on any country/region keyboard (Windows 2000 or later).
        /// </summary>
        OemPeriod = 190,
        /// <summary>
        /// The OEM question mark key on a US standard keyboard (Windows 2000 or later).
        /// </summary>
        OemQuestion = 191,
        /// <summary>
        /// The OEM 2 key.
        /// </summary>
        Oem2 = 191,
        /// <summary>
        /// The OEM tilde key on a US standard keyboard (Windows 2000 or later).
        /// </summary>
        Oemtilde = 192,
        /// <summary>
        /// The OEM 3 key.
        /// </summary>
        Oem3 = 192,
        /// <summary>
        /// The OEM open bracket key on a US standard keyboard (Windows 2000 or later).
        /// </summary>
        OemOpenBrackets = 219,
        /// <summary>
        /// The OEM 4 key.
        /// </summary>
        Oem4 = 219,
        /// <summary>
        /// The OEM pipe key on a US standard keyboard (Windows 2000 or later).
        /// </summary>
        OemPipe = 220,
        /// <summary>
        /// The OEM 5 key.
        /// </summary>
        Oem5 = 220,
        /// <summary>
        /// The OEM close bracket key on a US standard keyboard (Windows 2000 or later).
        /// </summary>
        OemCloseBrackets = 221,
        /// <summary>
        /// The OEM 6 key.
        /// </summary>
        Oem6 = 221,
        /// <summary>
        /// The OEM singled/double quote key on a US standard keyboard (Windows 2000 or later).
        /// </summary>
        OemQuotes = 222,
        /// <summary>
        /// The OEM 7 key.
        /// </summary>
        Oem7 = 222,
        /// <summary>
        /// The OEM 8 key.
        /// </summary>
        Oem8 = 223,
        /// <summary>
        /// The OEM angle bracket or backslash key on the RT 102 key keyboard (Windows 2000 or later).
        /// </summary>
        OemBackslash = 226,
        /// <summary>
        /// The OEM 102 key.
        /// </summary>
        Oem102 = 226,
        /// <summary>
        /// The PROCESS KEY key.
        /// </summary>
        ProcessKey = 229,
        /// <summary>
        /// Used to pass Unicode characters as if they were keystrokes. The Packet key value is the low word of a 32-bit virtual-key value used for non-keyboard input methods.
        /// </summary>
        Packet = 231,
        /// <summary>
        /// The ATTN key.
        /// </summary>
        Attn = 246,
        /// <summary>
        /// The CRSEL key.
        /// </summary>
        Crsel = 247,
        /// <summary>
        /// The EXSEL key.
        /// </summary>
        Exsel = 248,
        /// <summary>
        /// The ERASE EOF key.
        /// </summary>
        EraseEof = 249,
        /// <summary>
        /// The PLAY key.
        /// </summary>
        Play = 250,
        /// <summary>
        /// The ZOOM key.
        /// </summary>
        Zoom = 251,
        /// <summary>
        /// A constant reserved for future use.
        /// </summary>
        NoName = 252,
        /// <summary>
        /// The PA1 key.
        /// </summary>
        Pa1 = 253,
        /// <summary>
        /// The CLEAR key.
        /// </summary>
        OemClear = 254,
        /// <summary>
        /// The SHIFT modifier key.
        /// </summary>
        Shift = 65536,
        /// <summary>
        /// The CTRL modifier key.
        /// </summary>
        Control = 131072,
        /// <summary>
        /// The ALT modifier key.
        /// </summary>
        Alt = 262144,

        /// <summary>
        /// The ALT modifier key.
        /// </summary>
        Menu9,
        /// <summary>
        /// The menu f5
        /// </summary>
        MenuF5,
        /// <summary>
        /// The menu f7
        /// </summary>
        MenuF7,
        /// <summary>
        /// The menu f8
        /// </summary>
        MenuF8,
        /// <summary>
        /// The menu F11
        /// </summary>
        MenuF11,
        /// <summary>
        /// The menu F12
        /// </summary>
        MenuF12,
        /// <summary>
        /// The menu shift f7
        /// </summary>
        MenuShiftF7,
        /// <summary>
        /// The menu shift f8
        /// </summary>
        MenuShiftF8,
        /// <summary>
        /// The menu shift F11
        /// </summary>
        MenuShiftF11,
        /// <summary>
        /// The menu shift F12
        /// </summary>
        MenuShiftF12,
        /// <summary>
        /// The menu control f5
        /// </summary>
        MenuCtrlF5,
        /// <summary>
        /// The menu control f7
        /// </summary>
        MenuCtrlF7,
        /// <summary>
        /// The menu control e
        /// </summary>
        MenuCtrlE,
        /// <summary>
        /// The menu control l
        /// </summary>
        MenuCtrlL,
        /// <summary>
        /// The menu control t
        /// </summary>
        MenuCtrlT,
        /// <summary>
        /// The menu control u
        /// </summary>
        MenuCtrlU,
        /// <summary>
        /// The menu control w
        /// </summary>
        MenuCtrlW
    }


    public enum ImeMode
    {
        /// <summary>
        /// The inherit
        /// </summary>
        Inherit = -1,
        /// <summary>
        /// The no control
        /// </summary>
        NoControl,
        /// <summary>
        /// The on
        /// </summary>
        On,
        /// <summary>
        /// The off
        /// </summary>
        Off,
        /// <summary>
        /// The disable
        /// </summary>
        Disable,
        /// <summary>
        /// The hiragana
        /// </summary>
        Hiragana,
        /// <summary>
        /// The katakana
        /// </summary>
        Katakana,
        /// <summary>
        /// The katakana half
        /// </summary>
        KatakanaHalf,
        /// <summary>
        /// The alpha full
        /// </summary>
        AlphaFull,
        /// <summary>
        /// The alpha
        /// </summary>
        Alpha,
        /// <summary>
        /// The hangul full
        /// </summary>
        HangulFull,
        /// <summary>
        /// The hangul
        /// </summary>
        Hangul,
        /// <summary>
        /// The close
        /// </summary>
        Close,
        /// <summary>
        /// The on half
        /// </summary>
        OnHalf
    }


    public enum HorizontalAlignment
    {
        /// <summary>
        /// The left
        /// </summary>
        Left,
        /// <summary>
        /// The right
        /// </summary>
        Right,
        /// <summary>
        /// The center
        /// </summary>
        Center
    }


    public enum WindowStartPosition
    {
        /// <summary>
        /// The position of the form is determined by the <see cref="P:System.Windows.Forms.Control.Location" /> property.
        /// </summary>
        Manual,

        /// <summary>
        /// The form is centered on the current display, and has the dimensions specified in the form's size.
        /// </summary>
        CenterScreen,

        /// <summary>
        /// The form is positioned at the Windows default location and has the dimensions specified in the form's size.
        /// </summary>
        WindowsDefaultLocation,

        /// <summary>
        /// The form is positioned at the Windows default location and has the bounds determined by Windows default.
        /// </summary>
        WindowsDefaultBounds,

        /// <summary>
        /// The form is centered within the bounds of its parent form.
        /// </summary>
        CenterParent
    }


    [Flags()]
    public enum DrawItemState
    {
        /// <summary>
        /// The item is checked. Only menu controls use this value.
        /// </summary>
        Checked = 8,
        /// <summary>
        /// The item is the editing portion of a <see cref="T:System.Windows.Forms.ComboBox" />.
        /// </summary>
        ComboBoxEdit = 4096,
        /// <summary>
        /// The item is in its default visual state.
        /// </summary>
        Default = 32,
        /// <summary>
        /// The item is unavailable.
        /// </summary>
        Disabled = 4,
        /// <summary>
        /// The item has focus.
        /// </summary>
        Focus = 16,
        /// <summary>
        /// The item is grayed. Only menu controls use this value.
        /// </summary>
        Grayed = 2,
        /// <summary>
        /// The item is being hot-tracked, that is, the item is highlighted as the mouse pointer passes over it.
        /// </summary>
        HotLight = 64,
        /// <summary>
        /// The item is inactive.
        /// </summary>
        Inactive = 128,
        /// <summary>
        /// The item displays without a keyboard accelerator.
        /// </summary>
        NoAccelerator = 256,
        /// <summary>
        /// The item displays without the visual cue that indicates it has focus.
        /// </summary>
        NoFocusRect = 512,
        /// <summary>
        /// The item is selected.
        /// </summary>
        Selected = 1,
        /// <summary>
        /// The item currently has no state.
        /// </summary>
        None = 0
    }


    public enum AccessibleRole
    {
        /// <summary>
        /// A system-provided role.
        /// </summary>
        Default = -1,

        /// <summary>
        /// No role.
        /// </summary>
        None,

        /// <summary>
        /// A title or caption bar for a window.
        /// </summary>
        TitleBar,

        /// <summary>
        /// A menu bar, usually beneath the title bar of a window, from which users can select menus.
        /// </summary>
        MenuBar,

        /// <summary>
        /// A vertical or horizontal scroll bar, which can be either part of the client area or used in a control.
        /// </summary>
        ScrollBar,

        /// <summary>
        /// A special mouse pointer, which allows a user to manipulate user interface elements such as a window. For example, a user can click and drag a sizing grip in the lower-right corner of a window to resize it.
        /// </summary>
        Grip,

        /// <summary>
        /// A system sound, which is associated with various system events.
        /// </summary>
        Sound,

        /// <summary>
        /// A mouse pointer.
        /// </summary>
        Cursor,

        /// <summary>
        /// A caret, which is a flashing line, block, or bitmap that marks the location of the insertion point in a window's client area.
        /// </summary>
        Caret,

        /// <summary>
        /// An alert or condition that you can notify a user about. Use this role only for objects that embody an alert but are not associated with another user interface element, such as a message box, graphic, text, or sound.
        /// </summary>
        Alert,

        /// <summary>
        /// A window frame, which usually contains child objects such as a title bar, client, and other objects typically contained in a window.
        /// </summary>
        Window,

        /// <summary>
        /// A window's user area.
        /// </summary>
        Client,

        /// <summary>
        /// A menu, which presents a list of options from which the user can make a selection to perform an action. All menu types must have this role, including drop-down menus that are displayed by selection from a menu bar, and shortcut menus that are displayed when the right mouse button is clicked.
        /// </summary>
        MenuPopup,

        /// <summary>
        /// A menu item, which is an entry in a menu that a user can choose to carry out a command, select an option, or display another menu. Functionally, a menu item can be equivalent to a push button, radio button, check box, or menu.
        /// </summary>
        MenuItem,

        /// <summary>
        /// A tool tip, which is a small rectangular pop-up window that displays a brief description of the purpose of a button.
        /// </summary>
        ToolTip,

        /// <summary>
        /// The main window for an application.
        /// </summary>
        Application,

        /// <summary>
        /// A document window, which is always contained within an application window. This role applies only to multiple-document interface (MDI) windows and refers to an object that contains the MDI title bar.
        /// </summary>
        Document,

        /// <summary>
        /// A separate area in a frame, a split document window, or a rectangular area of the status bar that can be used to display information. Users can navigate between panes and within the contents of the current pane, but cannot navigate between items in different panes. Thus, panes represent a level of grouping lower than frame windows or documents, but above individual controls. Typically, the user navigates between panes by pressing TAB, F6, or CTRL+TAB, depending on the context.
        /// </summary>
        Pane,

        /// <summary>
        /// A graphical image used to represent data.
        /// </summary>
        Chart,

        /// <summary>
        /// A dialog box or message box.
        /// </summary>
        Dialog,

        /// <summary>
        /// A window border. The entire border is represented by a single object, rather than by separate objects for each side.
        /// </summary>
        Border,

        /// <summary>
        /// The objects grouped in a logical manner. There can be a parent-child relationship between the grouping object and the objects it contains.
        /// </summary>
        Grouping,

        /// <summary>
        /// A space divided visually into two regions, such as a separator menu item or a separator dividing split panes within a window.
        /// </summary>
        Separator,

        /// <summary>
        /// A toolbar, which is a grouping of controls that provide easy access to frequently used features.
        /// </summary>
        ToolBar,

        /// <summary>
        /// A status bar, which is an area typically at the bottom of an application window that displays information about the current operation, state of the application, or selected object. The status bar can have multiple fields that display different kinds of information, such as an explanation of the currently selected menu command in the status bar.
        /// </summary>
        StatusBar,

        /// <summary>
        /// A table containing rows and columns of cells and, optionally, row headers and column headers.
        /// </summary>
        Table,

        /// <summary>
        /// A column header, which provides a visual label for a column in a table.
        /// </summary>
        ColumnHeader,

        /// <summary>
        /// A row header, which provides a visual label for a table row.
        /// </summary>
        RowHeader,

        /// <summary>
        /// A column of cells within a table.
        /// </summary>
        Column,

        /// <summary>
        /// A row of cells within a table.
        /// </summary>
        Row,

        /// <summary>
        /// A cell within a table.
        /// </summary>
        Cell,

        /// <summary>
        /// A link, which is a connection between a source document and a destination document. This object might look like text or a graphic, but it acts like a button.
        /// </summary>
        Link,

        /// <summary>
        /// A Help display in the form of a ToolTip or Help balloon, which contains buttons and labels that users can click to open custom Help topics.
        /// </summary>
        HelpBalloon,

        /// <summary>
        /// A cartoon-like graphic object, such as Microsoft Office Assistant, which is typically displayed to provide help to users of an application.
        /// </summary>
        Character,

        /// <summary>
        /// A list box, which allows the user to select one or more items.
        /// </summary>
        List,

        /// <summary>
        /// An item in a list box or the list portion of a combo box, drop-down list box, or drop-down combo box.
        /// </summary>
        ListItem,

        /// <summary>
        /// An outline or tree structure, such as a tree view control, which displays a hierarchical list and usually allows the user to expand and collapse branches.
        /// </summary>
        Outline,

        /// <summary>
        /// An item in an outline or tree structure.
        /// </summary>
        OutlineItem,

        /// <summary>
        /// A property page that allows a user to view the attributes for a page, such as the page's title, whether it is a home page, or whether the page has been modified. Normally, the only child of this control is a grouped object that contains the contents of the associated page.
        /// </summary>
        PageTab,

        /// <summary>
        /// A property page, which is a dialog box that controls the appearance and the behavior of an object, such as a file or resource. A property page's appearance differs according to its purpose.
        /// </summary>
        PropertyPage,

        /// <summary>
        /// An indicator, such as a pointer graphic, that points to the current item.
        /// </summary>
        Indicator,

        /// <summary>
        /// A picture.
        /// </summary>
        Graphic,

        /// <summary>
        /// The read-only text, such as in a label, for other controls or instructions in a dialog box. Static text cannot be modified or selected.
        /// </summary>
        StaticText,

        /// <summary>
        /// The selectable text that can be editable or read-only.
        /// </summary>
        Text,

        /// <summary>
        /// A push button control, which is a small rectangular control that a user can turn on or off. A push button, also known as a command button, has a raised appearance in its default off state and a sunken appearance when it is turned on.
        /// </summary>
        PushButton,

        /// <summary>
        /// A check box control, which is an option that can be turned on or off independent of other options.
        /// </summary>
        CheckButton,

        /// <summary>
        /// An option button, also known as a radio button. All objects sharing a single parent that have this attribute are assumed to be part of a single mutually exclusive group. You can use grouped objects to divide option buttons into separate groups when necessary.
        /// </summary>
        RadioButton,

        /// <summary>
        /// A combo box, which is an edit control with an associated list box that provides a set of predefined choices.
        /// </summary>
        ComboBox,

        /// <summary>
        /// A drop-down list box. This control shows one item and allows the user to display and select another from a list of alternative choices.
        /// </summary>
        DropList,

        /// <summary>
        /// A progress bar, which indicates the progress of a lengthy operation by displaying colored lines inside a horizontal rectangle. The length of the lines in relation to the length of the rectangle corresponds to the percentage of the operation that is complete. This control does not take user input.
        /// </summary>
        ProgressBar,

        /// <summary>
        /// A dial or knob. This can also be a read-only object, like a speedometer.
        /// </summary>
        Dial,

        /// <summary>
        /// A hot-key field that allows the user to enter a combination or sequence of keystrokes to be used as a hot key, which enables users to perform an action quickly. A hot-key control displays the keystrokes entered by the user and ensures that the user selects a valid key combination.
        /// </summary>
        HotkeyField,

        /// <summary>
        /// A control, sometimes called a trackbar, that enables a user to adjust a setting in given increments between minimum and maximum values by moving a slider. The volume controls in the Windows operating system are slider controls.
        /// </summary>
        Slider,

        /// <summary>
        /// A spin box, also known as an up-down control, which contains a pair of arrow buttons. A user clicks the arrow buttons with a mouse to increment or decrement a value. A spin button control is most often used with a companion control, called a buddy window, where the current value is displayed.
        /// </summary>
        SpinButton,

        /// <summary>
        /// A graphical image used to diagram data.
        /// </summary>
        Diagram,

        /// <summary>
        /// An animation control, which contains content that is changing over time, such as a control that displays a series of bitmap frames, like a filmstrip. Animation controls are usually displayed when files are being copied, or when some other time-consuming task is being performed.
        /// </summary>
        Animation,

        /// <summary>
        /// A mathematical equation.
        /// </summary>
        Equation,

        /// <summary>
        /// A button that drops down a list of items.
        /// </summary>
        ButtonDropDown,

        /// <summary>
        /// A button that drops down a menu.
        /// </summary>
        ButtonMenu,

        /// <summary>
        /// A button that drops down a grid.
        /// </summary>
        ButtonDropDownGrid,

        /// <summary>
        /// A blank space between other objects.
        /// </summary>
        WhiteSpace,

        /// <summary>
        /// A container of page tab controls.
        /// </summary>
        PageTabList,

        /// <summary>
        /// A control that displays the time.
        /// </summary>
        Clock,

        /// <summary>
        /// A toolbar button that has a drop-down list icon directly adjacent to the button.
        /// </summary>
        SplitButton,

        /// <summary>
        /// A control designed for entering Internet Protocol (IP) addresses.
        /// </summary>
        IpAddress,

        /// <summary>
        /// A control that navigates like an outline item.
        /// </summary>
        OutlineButton
    }


    [ComVisible(true)]
    public enum Appearance
    {
        /// <summary>
        /// The default appearance defined by the control class.
        /// </summary>
        Normal,

        /// <summary>
        /// The appearance of a Windows button.
        /// </summary>
        Button
    }


    public enum AutoScaleMode
    {
        /// <summary>
        /// Automatic scaling is disabled.
        /// </summary>
        None,

        /// <summary>
        /// Controls scale relative to the dimensions of the font the classes are using, which is typically the system font.
        /// </summary>
        Font,

        /// <summary>
        /// Controls scale relative to the display resolution. Common resolutions are 96 and 120 DPI.
        /// </summary>
        Dpi,

        /// <summary>
        /// Controls scale according to the classes' parent's scaling mode. If there is no parent, automatic scaling is disabled.
        /// </summary>
        Inherit
    }


    public enum AutoSizeMode
    {
        /// <summary>
        /// The control grows or shrinks to fit its contents. The control cannot be resized manually.
        /// </summary>
        GrowAndShrink,

        /// <summary>
        /// The control grows as much as necessary to fit its contents but does not shrink smaller than the value of its <see cref="P:System.Windows.Forms.Control.Size" />   property. The form can be resized, but cannot be made so small that any of its contained controls are hidden.
        /// </summary>
        GrowOnly
    }


    public enum CheckState
    {
        /// <summary>
        /// The control is unchecked.
        /// </summary>
        Unchecked,

        /// <summary>
        /// The control is checked.
        /// </summary>
        Checked,

        /// <summary>
        /// The control is indeterminate. An indeterminate control generally has a shaded appearance.
        /// </summary>
        Indeterminate
    }


    public enum CloseReason
    {
        /// <summary>
        /// The cause of the closure was not defined or could not be determined.
        /// </summary>
        None,

        /// <summary>
        /// The operating system is closing all applications before shutting down.
        /// </summary>
        WindowsShutdown,

        /// <summary>
        /// The parent form of this multiple document interface (MDI) form is closing.
        /// </summary>
        MdiFormClosing,

        /// <summary>
        /// The user is closing the form through the user interface (UI), for example by clicking the Close button on the form window, selecting Close from the window's control menu, or pressing ALT+F4.
        /// </summary>
        UserClosing,

        /// <summary>
        /// The Microsoft Windows Task Manager is closing the application.
        /// </summary>
        TaskManagerClosing,

        /// <summary>
        /// The owner form is closing.
        /// </summary>
        FormOwnerClosing,

        /// <summary>
        /// The <see cref="M:System.Windows.Forms.Application.Exit" /> method of the <see cref="T:System.Windows.Forms.Application" /> class was invoked.
        /// </summary>
        ApplicationExitCall,
        
        /// <summary>
        /// The close operation called from Code
        /// </summary>
        Code
    }


    [Flags()]
    public enum TextFormatFlags
    {
        // default value
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// Aligns the text on the bottom of the bounding rectangle. Applied only when the text is a single line.
        /// </summary>
        Bottom = 8,
        /// <summary>
        /// Removes the end of trimmed lines, and replaces them with an ellipsis.
        /// </summary>
        EndEllipsis = 32768,
        /// <summary>
        /// Expands tab characters. The default number of characters per tab is eight. The <see cref="F:System.Windows.Forms.TextFormatFlags.WordEllipsis" />, <see cref="F:System.Windows.Forms.TextFormatFlags.PathEllipsis" />, and <see cref="F:System.Windows.Forms.TextFormatFlags.EndEllipsis" /> values cannot be used with <see cref="F:System.Windows.Forms.TextFormatFlags.ExpandTabs" />.
        /// </summary>
        ExpandTabs = 64,
        /// <summary>
        /// Includes the font external leading in line height. Typically, external leading is not included in the height of a line of text.
        /// </summary>
        ExternalLeading = 512,
        /// <summary>
        /// Applies the default formatting, which is left-aligned.
        /// </summary>
        Default = 0,
        /// <summary>
        /// Applies to Windows 2000 and Windows XP only:
        /// </summary>
        HidePrefix = 1048576,
        /// <summary>
        /// Centers the text horizontally within the bounding rectangle.
        /// </summary>
        HorizontalCenter = 1,
        /// <summary>
        /// Uses the system font to calculate text metrics.
        /// </summary>
        Internal = 4096,
        /// <summary>
        /// Aligns the text on the left side of the clipping area.
        /// </summary>
        Left = 0,
        /// <summary>
        /// Has no effect on the drawn text.
        /// </summary>
        ModifyString = 65536,
        /// <summary>
        /// Allows the overhanging parts of glyphs and unwrapped text reaching outside the formatting rectangle to show.
        /// </summary>
        NoClipping = 256,
        /// <summary>
        /// Turns off processing of prefix characters. Typically, the ampersand (&amp;) mnemonic-prefix character is interpreted as a directive to underscore the character that follows, and the double-ampersand (&amp;&amp;) mnemonic-prefix characters as a directive to print a single ampersand. By specifying <see cref="F:System.Windows.Forms.TextFormatFlags.NoPrefix" />, this processing is turned off. For example, an input string of "A&amp;bc&amp;&amp;d" with <see cref="F:System.Windows.Forms.TextFormatFlags.NoPrefix" /> applied would result in output of "A&amp;bc&amp;&amp;d".
        /// </summary>
        NoPrefix = 2048,
        /// <summary>
        /// Applies to Windows 98, Windows Me, Windows 2000, or Windows XP only:
        /// </summary>
        NoFullWidthCharacterBreak = 524288,
        /// <summary>
        /// Removes the center of trimmed lines and replaces it with an ellipsis.
        /// </summary>
        PathEllipsis = 16384,
        /// <summary>
        /// Applies to Windows 2000 or Windows XP only:
        /// </summary>
        PrefixOnly = 2097152,
        /// <summary>
        /// Aligns the text on the right side of the clipping area.
        /// </summary>
        Right = 2,
        /// <summary>
        /// Displays the text from right to left.
        /// </summary>
        RightToLeft = 131072,
        /// <summary>
        /// Displays the text in a single line.
        /// </summary>
        SingleLine = 32,
        /// <summary>
        /// Specifies the text should be formatted for display on a <see cref="T:System.Windows.Forms.TextBox" /> control.
        /// </summary>
        TextBoxControl = 8192,
        /// <summary>
        /// Aligns the text on the top of the bounding rectangle.
        /// </summary>
        Top = 0,
        /// <summary>
        /// Centers the text vertically, within the bounding rectangle.
        /// </summary>
        VerticalCenter = 4,
        /// <summary>
        /// Breaks the text at the end of a word.
        /// </summary>
        Wordbreak = 16,
        /// <summary>
        /// Trims the line to the nearest word and an ellipsis is placed at the end of a trimmed line.
        /// </summary>
        WordEllipsis = 262144,
        /// <summary>
        /// Preserves the clipping specified by a <see cref="T:System.Drawing.Graphics" /> object. Applies only to methods receiving an <see cref="T:System.Drawing.IDeviceContext" /> that is a <see cref="T:System.Drawing.Graphics" />.
        /// </summary>
        PreserveGraphicsClipping = 16777216,
        /// <summary>
        /// Preserves the transformation specified by a <see cref="T:System.Drawing.Graphics" />. Applies only to methods receiving an <see cref="T:System.Drawing.IDeviceContext" /> that is a <see cref="T:System.Drawing.Graphics" />.
        /// </summary>
        PreserveGraphicsTranslateTransform = 33554432,
        /// <summary>
        /// Adds padding to the bounding rectangle to accommodate overhanging glyphs.
        /// </summary>
        GlyphOverhangPadding = 0,
        /// <summary>
        /// Does not add padding to the bounding rectangle.
        /// </summary>
        NoPadding = 268435456,
        /// <summary>
        /// Adds padding to both sides of the bounding rectangle.
        /// </summary>
        LeftAndRightPadding = 536870912
    }


    [Flags(), ComVisible(true)]
    public enum MouseButtons
    {
        /// <summary>
        /// The left mouse button was pressed.
        /// </summary>
        Left = 1048576,
        /// <summary>
        /// No mouse button was pressed.
        /// </summary>
        None = 0,
        /// <summary>
        /// The right mouse button was pressed.
        /// </summary>
        Right = 2097152,
        /// <summary>
        /// The middle mouse button was pressed.
        /// </summary>
        Middle = 4194304,
        /// <summary>
        /// The first XButton was pressed.
        /// </summary>
        XButton1 = 8388608,
        /// <summary>
        /// The second XButton was pressed.
        /// </summary>
        XButton2 = 16777216
    }


    public enum MessageBoxButtons
    {
        /// <summary>
        /// The message box contains an OK button.
        /// </summary>
        OK,

        /// <summary>
        /// The message box contains OK and Cancel buttons.
        /// </summary>
        OKCancel,

        /// <summary>
        /// The message box contains Abort, Retry, and Ignore buttons.
        /// </summary>
        AbortRetryIgnore,

        /// <summary>
        /// The message box contains Yes, No, and Cancel buttons.
        /// </summary>
        YesNoCancel,

        /// <summary>
        /// The message box contains Yes and No buttons.
        /// </summary>
        YesNo,

        /// <summary>
        /// The message box contains Retry and Cancel buttons.
        /// </summary>
        RetryCancel
    }


    public enum MdiLayout
    {
        /// <summary>
        /// All MDI child windows are cascaded within the client region of the MDI parent form.
        /// </summary>
        Cascade,

        /// <summary>
        /// All MDI child windows are tiled horizontally within the client region of the MDI parent form.
        /// </summary>
        TileHorizontal,

        /// <summary>
        /// All MDI child windows are tiled vertically within the client region of the MDI parent form.
        /// </summary>
        TileVertical,

        /// <summary>
        /// All MDI child icons are arranged within the client region of the MDI parent form.
        /// </summary>
        ArrangeIcons
    }


    public enum DragDropEffects
    {
        /// <summary>
        /// The drop target does not accept the data.
        /// </summary>
        None = 0,
        /// <summary>
        /// The data from the drag source is copied to the drop target.
        /// </summary>
        Copy = 1,
        /// <summary>
        /// The data from the drag source is moved to the drop target.
        /// </summary>
        Move = 2,
        /// <summary>
        /// The data from the drag source is linked to the drop target.
        /// </summary>
        Link = 4,
        /// <summary>
        /// The target can be scrolled while dragging to locate a drop position that is not currently visible in the target.
        /// </summary>
        Scroll = -2147483648,
        /// <summary>
        /// The combination of the <see cref="F:System.Windows.DragDropEffects.Copy" />, <see cref="F:System.Windows.Forms.DragDropEffects.Move" />, and <see cref="F:System.Windows.Forms.DragDropEffects.Scroll" /> effects.
        /// </summary>
        All = -2147483645
    }


    public enum DataSourceUpdateMode
    {
        /// <summary>
        /// Data source is updated when the control property is validated,
        /// </summary>
        OnValidation,

        /// <summary>
        /// Data source is updated whenever the value of the control property changes.
        /// </summary>
        OnPropertyChanged,

        /// <summary>
        /// Data source is never updated and values entered into the control are not parsed, validated or re-formatted.
        /// </summary>
        Never
    }


    public enum MessageBoxIcon
    {
        /// <summary>
        /// The message box contain no symbols.
        /// </summary>
        None,
        /// <summary>
        /// The message box contains a symbol consisting of a white X in a circle with a red background.
        /// </summary>
        Hand,
        /// <summary>
        /// The message box contains a symbol consisting of a question mark in a circle. The question-mark message icon is no longer recommended because it does not clearly represent a specific type of message and because the phrasing of a message as a question could apply to any message type. In addition, users can confuse the message symbol question mark with Help information. Therefore, do not use this question mark message symbol in your message boxes. The system continues to support its inclusion only for backward compatibility.
        /// </summary>
        Question,
        /// <summary>
        /// The message box contains a symbol consisting of an exclamation point in a triangle with a yellow background.
        /// </summary>
        Exclamation,
        /// <summary>
        /// The message box contains a symbol consisting of a lowercase letter i in a circle.
        /// </summary>
        Asterisk,
        /// <summary>
        /// The message box contains a symbol consisting of white X in a circle with a red background.
        /// </summary>
        Stop,
        /// <summary>
        /// The message box contains a symbol consisting of white X in a circle with a red background.
        /// </summary>
        Error,
        /// <summary>
        /// The message box contains a symbol consisting of an exclamation point in a triangle with a yellow background.
        /// </summary>
        Warning,
        /// <summary>
        /// The message box contains a symbol consisting of a lowercase letter i in a circle.
        /// </summary>
        Information
    }


    public enum ItemsView
    {
        /// <summary>
        /// The large icon
        /// </summary>
        LargeIcon,
        /// <summary>
        /// The details
        /// </summary>
        Details,
        /// <summary>
        /// The small icon
        /// </summary>
        SmallIcon,
        /// <summary>
        /// The list
        /// </summary>
        List,
        /// <summary>
        /// The tile
        /// </summary>
        Tile
    }


    public enum PictureBoxSizeMode
    {
        /// <summary>
        /// The normal
        /// </summary>
        Normal,
        /// <summary>
        /// The stretch image
        /// </summary>
        StretchImage,
        /// <summary>
        /// The automatic size
        /// </summary>
        AutoSize,
        /// <summary>
        /// The center image
        /// </summary>
        CenterImage,
        /// <summary>
        /// The zoom
        /// </summary>
        Zoom
    }


    public enum AnchorStyles
    {
        /// <summary>
        /// The bottom
        /// </summary>
        Bottom = 2,
        /// <summary>
        /// The left
        /// </summary>
        Left = 4,
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// The right
        /// </summary>
        Right = 8,
        /// <summary>
        /// The top
        /// </summary>
        Top = 1
    }


    public enum ScrollBars
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The horizontal
        /// </summary>
        Horizontal,
        /// <summary>
        /// The vertical
        /// </summary>
        Vertical,
        /// <summary>
        /// The both
        /// </summary>
        Both,
        /// <summary>
        /// The large change
        /// </summary>
        LargeChange,
        /// <summary>
        /// The maximum
        /// </summary>
        Maximum,
        /// <summary>
        /// The minimum
        /// </summary>
        Minimum,
        /// <summary>
        /// The small change
        /// </summary>
        SmallChange,
        /// <summary>
        /// The value
        /// </summary>
        Value
    }


    public enum FlatStyle
    {
        /// <summary>
        /// The flat
        /// </summary>
        Flat,
        /// <summary>
        /// The popup
        /// </summary>
        Popup,
        /// <summary>
        /// The standard
        /// </summary>
        Standard,
        /// <summary>
        /// The system
        /// </summary>
        System
    }


    public enum ImageLayout
    {
        /// <summary>
        /// The none
        /// </summary>
        None,
        /// <summary>
        /// The tile
        /// </summary>
        Tile,
        /// <summary>
        /// The center
        /// </summary>
        Center,
        /// <summary>
        /// The stretch
        /// </summary>
        Stretch,
        /// <summary>
        /// The zoom
        /// </summary>
        Zoom
    }


    public enum DateTimePickerFormat
    {
        /// <summary>
        /// The default value
        /// </summary>
        Default = 0,
        /// <summary>
        /// The custom
        /// </summary>
        Custom = 8,
        /// <summary>
        /// The long
        /// </summary>
        Long = 1,
        /// <summary>
        /// The short
        /// </summary>
        Short = 2,
        /// <summary>
        /// The time
        /// </summary>
        Time = 4,
        /// <summary>
        /// The Line
        /// </summary>
        Line = 5
    }


    public enum AppearanceConstants
    {
        // default value
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// The CC3 d
        /// </summary>
        cc3D = 1,
        /// <summary>
        /// The cc flat
        /// </summary>
        ccFlat = 2
    }


    public enum ListLabelEditConstants
    {
        /// <summary>
        /// The automatic
        /// </summary>
        Automatic,
        /// <summary>
        /// The manual
        /// </summary>
        Manual
    }


    public enum AlignConstants
    {
        /// <summary>
        /// The align none
        /// </summary>
        AlignNone = 0,
        /// <summary>
        /// The align top
        /// </summary>
        AlignTop = 1,
        /// <summary>
        /// The align bottom
        /// </summary>
        AlignBottom = 2,
        /// <summary>
        /// The align left
        /// </summary>
        AlignLeft = 3,
        /// <summary>
        /// The align right
        /// </summary>
        AlignRight = 4
    }


    public enum PictureTypeConstants
    {
        /// <summary>
        /// The pic type none
        /// </summary>
        PicTypeNone,
        /// <summary>
        /// The pic type bitmap
        /// </summary>
        PicTypeBitmap,
        /// <summary>
        /// The pic type metafile
        /// </summary>
        PicTypeMetafile,
        /// <summary>
        /// The pic type icon
        /// </summary>
        PicTypeIcon,
        /// <summary>
        /// The pic type e metafile
        /// </summary>
        PicTypeEMetafile
    }

    /// <summary>
    /// Returns an Integer specifying the current Input Method Editor (IME) mode of Microsoft Windows; available in Far East versions only (Japanese, Chinies,  Korean locale)
    /// </summary>
    public enum IMEStatus
    {
        /// <summary>
        /// No IME installed
        /// </summary>
        IMENoOP,

        /// <summary>
        /// IME on
        /// </summary>
        IMEOn,

        /// <summary>
        /// IME off
        /// </summary>
        IMEOff,

        /// <summary>
        /// IME disable
        /// </summary>
        IMEDisable,

        /// <summary>
        /// Hiragana double-byte characters (DBC)
        /// </summary>
        IMEHiragana,

        /// <summary>
        /// Hiragana double-byte characters (DBC)
        /// </summary>
        IMEModeHiragana,

        /// <summary>
        /// Katakana DBC
        /// </summary>
        IMEKatakanaDbl,

        /// <summary>
        /// Katakana single-byte characters (SBC)
        /// </summary>
        IMEKatakanaSng,

        /// <summary>
        /// Alphanumeric DBC
        /// </summary>
        IMEAlphaDbl,

        /// <summary>
        /// Alphanumeric SBC
        /// </summary>
        IMEAlphaSng,

        /// <summary>
        /// The IME mode alpha
        /// </summary>
        IMEModeAlpha,

        /// <summary>
        /// The IME mode alpha full
        /// </summary>
        IMEModeAlphaFull,

        /// <summary>
        /// The IME mode alpha
        /// </summary>
        IMEModeHangul,

        /// <summary>
        /// The IME mode alpha full
        /// </summary>
        IMEModeHangulFull,

        /// <summary>
        /// atakana half
        /// </summary>
        IMEModeKatakanaHalf,

        /// <summary>
        /// katakana
        /// </summary>
        IMEModeKatakana,

        /// <summary>
        /// The  IME mode no control
        /// </summary>
        IMEModeNoControl,

        /// <summary>
        /// The  IME mode off
        /// </summary>
        IMEModeOff,

        /// <summary>
        /// The  IME mode on
        /// </summary>
        IMEModeOn
    }

    /// <summary>
    /// Shift enumeration
    /// </summary>
    public enum ShiftEnum
    {
        /// <summary>
        /// Alt mask
        /// </summary>
        AltMask,

        /// <summary>
        /// Ctrl mask
        /// </summary>
        CtrlMask,

        /// <summary>
        /// Shift mask
        /// </summary>
        ShiftMask,

        /// <summary>
        /// None
        /// </summary>
        None
    }

    /// <summary>
    /// Call types
    /// </summary>
    public enum CallType
    {
        /// <summary>
        /// The get
        /// </summary>
        Get,
        /// <summary>
        /// The let
        /// </summary>
        Let,
        /// <summary>
        /// The method
        /// </summary>
        Method,
        /// <summary>
        /// The set
        /// </summary>
        Set
    }

    /// <summary>
    /// Specifies the location and behavior of a pop-up menu, as described in Setting
    /// </summary>
    [Flags()]
    public enum PopupMenuFlags
    {
        /// <summary>
        /// (Default) The left side of the pop-up menu is located at x.
        /// </summary>
        PopupMenuLeftAlign,

        /// <summary>
        /// (Default) An item on the pop-up menu reacts to a mouse click only when you use the left mouse button.
        /// </summary>
        PopupMenuLeftButton,

        /// <summary>
        /// An item on the pop-up menu reacts to a mouse click when you use either the right or the left mouse button.
        /// </summary>
        PopupMenuRightButton,

        /// <summary>
        /// The pop-up menu is centered at x.
        /// </summary>
        PopupMenuCenterAlign,

        /// <summary>
        /// The right side of the pop-up menu is located at x.
        /// </summary>
        PopupMenuRightAlign,
    }

    public enum MessageBoxDefaultButton
    {
        /// <summary>
        /// first button on messageBox is the default button
        /// </summary>
        Button1 = 0,

        /// <summary>
        /// second button on messageBox is the default button
        /// </summary>
        Button2 = 256,

        /// <summary>
        /// third button on messageBox is the default button
        /// </summary>
        Button3 = 512,
    }

    public enum MessageBoxOptions
    {
        DefaultDesktopOnly,

        RightAlign,

        RtlReading,

        ServiceNotification
    }

    [ComVisible(true)]
    [Flags]
    public enum ToolBarStatusLabelBorderSides
    {
        All = 15,
        Bottom = 8,
        Left = 1,
        Right = 4,
        Top = 2,
        None = 0,
    }

    /// <summary>
    /// Specifies the case of characters in a <see cref="T:TextBoxElement"/> control.
    /// </summary>
    public enum CharacterCasing
    {
        Normal,
        Upper,
        Lower,
    }

    [Flags]
    [Serializable]
    public enum DockAreas
    {
        Float = 1,
        DockLeft = 2,
        DockRight = 4,
        DockTop = 8,
        DockBottom = 16,
        Document = 32,
    }
    public enum LineStyle
    {
        None = 0,
        DashDotDot = 1,
        DashDot = 2,
        Dot = 3,
        Dash = 4,
        SlantDashDot = 5,
        Continous = 6,
        Continuous = 6,
        Double = 7,
    }
    public enum UnderlineStyle
    {
        None,
        Single,
        Double,
        SingleAccounting,
        DoubleAccounting,
    }
    public enum BordersIndex
    {
        DiagonalDown,
        DiagonalUp,
        EdgeBottom,
        EdgeLeft,
        EdgeRight,
        EdgeTop,
        InsideHorizontal,
        InsideVertical,
    }

    public enum BorderWeight
    {
        Hairline,
        Thin,
        Medium,
        Thick,
    }
    public enum PageOrientation
    {
        Landscape,
        Portrait,
    }
    public enum PrintLocation
    {
        InPlace,
        NoComments,
        SheetEnd,
    }

    public enum PrintErrors
    {
        AsDisplayed,
        AsBlank,
        AsDash,
        AsNA,
    }

    public enum AutoCompleteMode
    {
        /// <summary>
        /// Appends the remainder of the most likely candidate string to the existing characters, highlighting the appended characters.
        /// </summary>
        Append,
        /// <summary>
        /// Disables the automatic completion feature for the ComboBox and TextBox controls.
        /// </summary>
        None,
        /// <summary>
        /// Displays the auxiliary drop-down list associated with the edit control. This drop-down is populated with one or more suggested completion strings.
        /// </summary>
        Suggest,
        /// <summary>
        /// Applies both Suggest and AllowAppend options.
        /// </summary>
        SuggestAppend,
    }


    public enum MessageBoxDirection
    {
        Ltr,
        Rtl
    }
    
    public enum CurrencyCountry
    {
        /// <summary>
        /// US currency ($).
        /// </summary>
        US,
        /// <summary>
        /// European Union currency (�).
        /// </summary>
        ES,
        /// <summary>
        /// Israel currency (�).
        /// </summary>
        IL,
        /// <summary>
        /// UK currency (�).
        /// </summary>
        GB


    }

    public enum DateInterval
    {
         /// <summary>
        /// Day Interval
        /// </summary>
        Day,
        /// <summary>
        /// Month Interval
        /// </summary>
        Month,
        /// <summary>
        /// Year Interval
        /// </summary>
        Year
    }

    public enum ScaleConstants
    {
        /// <summary>
        /// Small Scale
        /// </summary>
        Small,
        /// <summary>
        /// Medium Scale
        /// </summary>
        Medium,
        /// <summary>
        /// Large Scale
        /// </summary>
        Large
    }


    /// <summary>
    /// 
    /// </summary>
    public enum ColumnSortMode
    {
        None = 0,
        Ascending ,
        Descending,
        NumericAscending ,
        NumericDescending ,
        StringNoCaseAsending ,
        NoCaseDescending ,
        StringAscending ,
        StringDescending ,
        Custom 
    }


    //
    // Summary:
    //     Specifies that SplitContainer.Panel1, SplitContainer.Panel2,
    //     or neither panel is fixed.
    public enum FixedPanel
    {
        //
        // Summary:
        //     Specifies that neither SplitContainer.Panel1, Panel2
        //     is fixed. A Resize event affects both panels.
        None = 0,
        //
        // Summary:
        //     Specifies that SplitContainer.Panel1 is fixed. A Resize
        //     event affects only SplitContainer.Panel2.
        Panel1 = 1,
        //
        // Summary:
        //     Specifies that Panel2 is fixed. A Resize
        //     event affects only Panel1.
        Panel2 = 2
    }

    /// <summary>
    /// Gets or sets a value indicating whether an image on a ToolBarItem is automatically resized to fit in a container.
    /// If AutoSize is true (the default) and ToolStripItemImageScaling is SizeToFit, no image scaling occurs, and the ToolStrip size is that of the largest item, or a prescribed minimum size.
    /// If AutoSize is false and ToolStripItemImageScaling is None, neither image nor ToolStrip scaling occurs.
    /// </summary>
    public enum ToolStripItemImageScaling 
    {
        //Specifies that the size of the image on a ToolStripItem is not automatically adjusted to fit on a ToolStrip.
        None = 0,

        //Specifies that the size of the image on a ToolStripItem is automatically adjusted to fit on a ToolStrip.
        SizeToFit = 1
    }
}


