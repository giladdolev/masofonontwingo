﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.FileSystem
{
    /// <summary>
    /// The virtual drive
    /// </summary>
    public class VirtualDrive : VirtualDirectory
    {
        /// <summary>
        /// The drive
        /// </summary>
        private readonly string _drive;

        /// <summary>
        /// The name
        /// </summary>
        private readonly string _name;


        /// <summary>
        /// Initializes a new instance of the <see cref="VirtualDrive" /> class.
        /// </summary>
        /// <param name="drive">The drive.</param>
        /// <param name="name">The name.</param>
        /// <param name="folder">The folder.</param>
        public VirtualDrive(string drive, string name, string folder)
            : base(null, folder)
        {
            _drive = drive;
            _name = name;
        }



        /// <summary>
        /// Gets the drive.
        /// </summary>
        /// <value>
        /// The drive.
        /// </value>
        public string Drive
        {
            get { return _drive; }
        }




        /// <summary>
        /// Gets the virtual path.
        /// </summary>
        /// <value>
        /// The virtual path.
        /// </value>
        public override string Name
        {
            get
            {
                return _name;
            }
        }



        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }




        /// <summary>
        /// Determines whether the specified value is match.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal override bool IsMatch(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return string.Equals(_drive, value.Trim(':', '\\', '/', ' '), StringComparison.OrdinalIgnoreCase);
            }

            return false;
        }




        /// <summary>
        /// Gets the virtual directory.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        internal VirtualDirectory GetVirtualDirectory(string path)
        {
            VirtualDirectory currentDirectory = null;

            // If there is a valid path
            if (!string.IsNullOrEmpty(path))
            {
                // Get the directory path
                string directoryPath = Path;

                // If path starts with directory
                if (path.StartsWith(directoryPath, StringComparison.OrdinalIgnoreCase))
                {
                    // Cut the initial path
                    path = path.Substring(directoryPath.Length);

                    // Set current directory to this as the starting point
                    currentDirectory = this;
                }

                // Loop all path parts
                foreach (string pathPart in path.Split(new char[] { '\\', '/', ':' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    // If there is no current directory
                    if (currentDirectory == null)
                    {
                        // Check the drive as the root
                        if (IsMatch(pathPart))
                        {
                            // Set current directory
                            currentDirectory = this;

                            // Continue to next part
                            continue;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        // Flag indicating if directory was found
                        bool foundDirectory = false;

                        // Loop all sub directories
                        foreach (VirtualDirectory subDirectory in currentDirectory.GetDirectories())
                        {
                            // If is a matching sub directory
                            if (subDirectory != null && subDirectory.IsMatch(pathPart))
                            {
                                // Set current sub directory
                                currentDirectory = subDirectory;

                                // Indicate we found the directory
                                foundDirectory = true;

                                // Stop searching
                                break;
                            }
                        }

                        // If directory was found continue to the next path part
                        if (foundDirectory)
                        {
                            continue;
                        }

                        // Stop searching
                        return null;
                    }
                }
            }

            return currentDirectory;
        }
    }
}
