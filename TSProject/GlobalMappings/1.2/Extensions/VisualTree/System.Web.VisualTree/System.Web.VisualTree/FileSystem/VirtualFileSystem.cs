﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.FileSystem
{
    public class VirtualFileSystem
    {
        private const string DEFAULT_VIRTUAL_FILE_SYSTEM_ID = "DefaultFileSystem";
        private string _id = DEFAULT_VIRTUAL_FILE_SYSTEM_ID;
        private Dictionary<string, string> _driveDefinitions = null;
        /// <summary>
        /// The virtual root
        /// </summary>
        private string _virtualRoot = "~/App_Data";

        /// <summary>
        /// The user token provider
        /// </summary>
        private Func<string> _userTokenProvider = null;

        /// <summary>
        /// Gets or sets the virtual root.
        /// </summary>
        /// <value>
        /// The virtual root.
        /// </value>
        public string VirtualRoot
        {
            get { return _virtualRoot; }
            set { _virtualRoot = value; }
        }

        /// <summary>
        /// Gets the virtual drives.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<VirtualDrive> GetVirtualDrives()
        {
            return GetVirtualDrives(GetCurrentUserToken());
        }

        /// <summary>
        /// Gets the current user token.
        /// </summary>
        /// <returns></returns>
        private string GetCurrentUserToken()
        {
            string userToken = null;

            // If there is a valid token provider
            if (_userTokenProvider != null)
            {
                // Get user token
                userToken = _userTokenProvider();
            }

            // If there is no valid token
            if (string.IsNullOrWhiteSpace(userToken))
            {
                return "anonymous";
            }
            else
            {
                return userToken;
            }
        }

        /// <summary>
        /// Registers the user token provider.
        /// </summary>
        /// <param name="userTokenProvider">The user token provider.</param>
        public void RegisterUserTokenProvider(Func<string> userTokenProvider)
        {
            _userTokenProvider = userTokenProvider;
        }

        /// <summary>
        /// Gets the virtual drive.
        /// </summary>
        /// <param name="driveId">The drive identifier.</param>
        /// <returns></returns>
        internal VirtualDrive GetVirtualDrive(string driveId)
        {
            return GetVirtualDrives().FirstOrDefault(item => String.Equals(item.Drive, driveId, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Gets the virtual drives.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        internal IEnumerable<VirtualDrive> GetVirtualDrives(string userId)
        {
            if (_driveDefinitions == null)
            {
                _driveDefinitions = InitializeDriveDefinitions();
            }
            List<VirtualDrive> virtualDrives = new List<VirtualDrive>();
            foreach (string driveID in _driveDefinitions.Keys)
            {
                yield return GetVirtualDrive(userId, driveID, _driveDefinitions[driveID]);
            }
        }

        /// <summary>
        /// Initializes the drive definitions.
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> InitializeDriveDefinitions()
        {
            Dictionary<string, string> driveDefinitions = new Dictionary<string,string>();
            
            // TODO: Read from configuration using id

            if (driveDefinitions.Count == 0)
            {
                driveDefinitions.Add("C", "C:");
                driveDefinitions.Add("D", "D:");
            }
            return driveDefinitions;
        }


        /// <summary>
        /// Gets the virtual drive.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="driveId">The drive identifier.</param>
        /// <param name="driveName">Name of the drive.</param>
        /// <returns></returns>
        private VirtualDrive GetVirtualDrive(string userId, string driveId, string driveName)
        {
            // Create virtual user directory
            string directory = GetVirtualUserDirectory(userId, driveId, true);

            // Return virtual drive 
            return new VirtualDrive(driveId, driveName, directory);
        }

        /// <summary>
        /// Gets the virtual user directory.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="directoryId">The directory identifier.</param>
        /// <param name="createIfMissing">if set to <c>true</c> create if missing.</param>
        /// <returns></returns>
        private string GetVirtualUserDirectory(string userId, string directoryId, bool createIfMissing)
        {
            // If there is a valid context
            HttpContext context = RenderingContext.CurrentHttpContext;

            // If there is a valid context
            if (context != null)
            {
                // Get app data with user data
                string path = Path.Combine(context.Server.MapPath(_virtualRoot), "users", userId, directoryId);

                // If there is no valid directory
                if (!Directory.Exists(path) && createIfMissing)
                {
                    // Create directory
                    Directory.CreateDirectory(path);
                }

                // Return the path
                return path;
            }

            return null;
        }


        /// <summary>
        /// Gets the virtual directory.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        internal VirtualDirectory GetVirtualDirectory(string path)
        {
            VirtualDirectory directory = null;

            // Loop all virtual drives
            foreach (VirtualDrive drive in GetVirtualDrives())
            {
                // Get virtual directory from drive
                directory = drive.GetVirtualDirectory(path);

                // If there is a valid directory
                if (directory != null)
                {
                    break;
                }
            }

            return directory;
        }




        /// <summary>
        /// Gets the virtual path or self.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public string GetVirtualPathOrSelf(string path)
        {
            // Get virtual directory 
            VirtualDirectory directory = GetVirtualDirectory(path);

            // If there is a valid directory
            if (directory != null)
            {
                return directory.VirtualPath;
            }

            return path;
        }

        /// <summary>
        /// Gets the default.
        /// </summary>
        /// <value>
        /// The default.
        /// </value>
        public static VirtualFileSystem Default
        {
            get
            {
                return GetFileSystem();
            }
        }

        /// <summary>
        /// Ins the initialize default file system.
        /// </summary>
        /// <returns></returns>
        private static VirtualFileSystem InInitializeDefaultFileSystem()
        {
            return new VirtualFileSystem();
        }

        /// <summary>
        /// Gets the file system.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static VirtualFileSystem GetFileSystem(string id = DEFAULT_VIRTUAL_FILE_SYSTEM_ID)
        {
            if (String.IsNullOrEmpty(id))
            {
                id = DEFAULT_VIRTUAL_FILE_SYSTEM_ID;
            }
            VirtualFileSystem defaultFileSystem = ApplicationElement.GetOrCreateInstance<VirtualFileSystem>(InInitializeDefaultFileSystem, id);
            return defaultFileSystem;
        }
    }
}
