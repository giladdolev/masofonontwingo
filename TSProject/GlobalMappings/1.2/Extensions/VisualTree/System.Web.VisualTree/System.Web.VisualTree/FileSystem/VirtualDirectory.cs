﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.FileSystem
{
    /// <summary>
    /// The virtual directory
    /// </summary>
    public class VirtualDirectory : VirtualItem
    {
        /// <summary>
        /// The folder
        /// </summary>
        private readonly string _folder;


        /// <summary>
        /// Initializes a new instance of the <see cref="VirtualDirectory" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="folder">The folder.</param>
        public VirtualDirectory(VirtualDirectory parent, string folder)
            : base(parent)
        {
            _folder = folder;
        }



        /// <summary>
        /// Gets the virtual path.
        /// </summary>
        /// <value>
        /// The virtual path.
        /// </value>
        public override string Name
        {
            get
            {
                return IO.Path.GetFileName(_folder);
            }
        }




        /// <summary>
        /// Gets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public override string Path
        {
            get
            {
                return _folder;
            }
        }




        /// <summary>
        /// Determines whether the specified value is match.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal virtual bool IsMatch(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return string.Equals(Name, value.Trim(':', '\\', '/', ' '), StringComparison.OrdinalIgnoreCase);
            }

            return false;
        }




        /// <summary>
        /// Gets the directories.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VirtualDirectory> GetDirectories()
        {
            // Loop sub directories
            foreach (string subFolder in Directory.GetDirectories(_folder))
            {
                // Create directory
                yield return new VirtualDirectory(this, subFolder);
            }
        }




        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VirtualFile> GetFiles()
        {
            // Loop files
            foreach (string file in Directory.GetFiles(_folder))
            {
                // Create file
                yield return new VirtualFile(this, file);
            }
        }
    }
}
