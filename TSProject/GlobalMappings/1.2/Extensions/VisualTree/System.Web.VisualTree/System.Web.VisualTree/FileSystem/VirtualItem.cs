﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.FileSystem
{
    /// <summary>
    /// The virtual item.
    /// </summary>
    public abstract class VirtualItem
    {
        private readonly VirtualDirectory _parent;


        /// <summary>
        /// Initializes a new instance of the <see cref="VirtualItem"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public VirtualItem(VirtualDirectory parent)
        {
            _parent = parent;
        }



        /// <summary>
        /// Gets the virtual path.
        /// </summary>
        /// <value>
        /// The virtual path.
        /// </value>
        public abstract string Name
        {
            get;
        }



        /// <summary>
        /// Gets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public abstract string Path
        {
            get;
        }



        public string VirtualPath
        {
            get
            {
                StringBuilder buffer = new StringBuilder();
                FillVirtualPath(buffer);
                return buffer.ToString();
            }
        }



        /// <summary>
        /// Fills the virtual path.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        protected virtual void FillVirtualPath(StringBuilder buffer)
        {
            // If there is a valid parent
            if (_parent != null)
            {
                // Fill parent path
                _parent.FillVirtualPath(buffer);

                // Add separator
                buffer.Append("/");
            }

            // Add name
            buffer.Append(Name);
        }
    }
}
