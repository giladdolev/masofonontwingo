﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.FileSystem
{
    /// <summary>
    /// The virtual file
    /// </summary>
    public class VirtualFile : VirtualItem
    {
        /// <summary>
        /// The file
        /// </summary>
        private readonly string _file;



        /// <summary>
        /// Initializes a new instance of the <see cref="VirtualFile" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="file">The file.</param>
        public VirtualFile(VirtualDirectory parent, string file)
            : base(parent)
        {
            _file = file;
        }



        /// <summary>
        /// Gets the virtual path.
        /// </summary>
        /// <value>
        /// The virtual path.
        /// </value>
        public override string Name
        {
            get
            {
                return IO.Path.GetFileName(_file);
            }
        }



        /// <summary>
        /// Gets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public override string Path
        {
            get
            {
                return _file;
            }
        }
    }
}
