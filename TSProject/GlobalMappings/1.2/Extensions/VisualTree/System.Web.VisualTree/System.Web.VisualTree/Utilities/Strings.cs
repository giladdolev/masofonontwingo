﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Utilities
{
    /// <summary>
    /// Contains utilities methods for string
    /// </summary>
    internal static class Strings
    {
        /// <summary>
        /// Safes the compare strings.
        /// </summary>
        /// <param name="string1">The string1.</param>
        /// <param name="string2">The string2.</param>
        /// <param name="ignoreCase">if set to <c>true</c> ignore case.</param>
        /// <returns></returns>
        public static bool SafeCompareStrings(string string1, string string2, bool ignoreCase)
        {
            if ((string1 == null) || (string2 == null))
            {
                return false;
            }
            if (string1.Length != string2.Length)
            {
                return false;
            }
            return (string.Compare(string1, string2, ignoreCase, CultureInfo.InvariantCulture) == 0);
        }
    }
}
