using System.Drawing;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Utilities
{
    public static class ClipboardUtils
    {
        private static string _copiedText = "";

        /// <summary>
        /// Clear clipboard text.
        /// </summary>
        public static void Clear()
        {
            _copiedText = "";
        }


        /// <summary>
        /// set clipboard text.
        /// </summary>
        /// <param name="copiedValue">the copied text .</param>
        [RendererMethodDescription]
        public static void CopyText(string copiedValue)
        {

            _copiedText = copiedValue;
            ApplicationElement.InvokeMethod("CopyText", typeof(ClipboardUtils), _copiedText);
        }


        /// <summary>
        /// return copied text .
        /// </summary>
        /// <returns>string copied text.</returns>
        public static string GetText()
        {
            return _copiedText;
        }

    }
}
