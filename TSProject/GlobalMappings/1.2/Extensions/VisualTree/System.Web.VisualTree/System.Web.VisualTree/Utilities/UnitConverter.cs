﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace System.Web.VisualTree.Utilities
{

    public class UnitTypeConverter
    {
        /// <summary>
        /// Gets the pixel per.
        /// </summary>
        /// <param name="unitType">Type of the unit.</param>
        /// <returns></returns>
        private static double GetPixelPer(UnitType unitType)
        {
            switch (unitType)
            {
                case UnitType.Cm:
                    return GetPixelPer(UnitType.Inch) / 2.54F;
                case UnitType.Inch:
                    return 96;
                case UnitType.Mm:
                    return GetPixelPer(UnitType.Cm) / 10;
                default:
                    return 1;
            }
        }




        /// <summary>
        /// Converts to.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="toUnitType">Type of to unit.</param>
        /// <returns></returns>
        public static Unit ConvertTo(Unit from, UnitType toUnitType)
        {
            if (from.Type == toUnitType)
            {
                return from;
            }

            return new Unit((from.Value * GetPixelPer(from.Type)) /
                            GetPixelPer(toUnitType), toUnitType);
        }




        /// <summary>
        /// Gets the pixels.
        /// </summary>
        /// <param name="from">From.</param>
        public static int GetPixels(Unit from)
        {
            Unit pixelUnit = ConvertTo(from, UnitType.Pixel);
            return (int)Math.Round(pixelUnit.Value);
        }
    }
}
