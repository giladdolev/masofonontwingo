﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Utilities
{
    public static class BrowserUtils
    {
        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        [RendererMethodDescription]
        public static void Reload()
        {
            ApplicationElement.InvokeMethod("Refresh", typeof(BrowserUtils));
        }

        /// <summary>
        /// Navigates the URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        [RendererMethodDescription]
        public static void NavigateUrl(string url)
        {
            ApplicationElement.InvokeMethod("NavigateUrl" , typeof(BrowserUtils), url);
        }

        /// <summary>
        /// Opens the window.
        /// </summary>
        /// <param name="url">The URL.</param>
        [RendererMethodDescription]
        public static void OpenWindow(string url)
        {
            ApplicationElement.InvokeMethod("OpenWindow", typeof(BrowserUtils), url);
        }




        /// <summary>
        /// Sets the document title.
        /// </summary>
        /// <param name="title">The title.</param>
        [RendererMethodDescription]
        internal static void SetDocumentTitle(string title)
        {
            ApplicationElement.InvokeMethod("SetDocumentTitle", typeof(BrowserUtils), title);
        }

        /// <summary>
        /// Adds the history token.
        /// </summary>
        /// <param name="token">The token.</param>
        [RendererMethodDescription]
        public static void AddHistoryToken(string token)
        {
            ApplicationElement.InvokeMethod("AddHistoryToken", typeof(BrowserUtils), token);
        }

        /// <summary>
        /// Raises the delayed events.
        /// </summary>
        /// <param name="milliseconds">The milliseconds.</param>
        [RendererMethodDescription]
        public static void RaiseDelayedEvents(int milliseconds)
        {
            ApplicationElement.InvokeMethodOnce("RaiseDelayedEvents", typeof(BrowserUtils), milliseconds);
        }
    }
}
