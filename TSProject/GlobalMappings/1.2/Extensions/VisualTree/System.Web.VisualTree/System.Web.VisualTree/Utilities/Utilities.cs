﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Utilities
{
    public static class Utilities
    {
        /// <summary>
        /// find image list on parents elements .
        /// </summary>
        /// <param name="element">The element . </param>
        /// <returns>true if one of the parent of this element has filled  image list, otherwise false. </returns>
        public static ControlElement SeekFotImageListOnParents(ControlElement element)
        {
            if (element.Parent == null)
            {
                return null;
            }
            if (element.ImageList != null)
            {
                return element;
            }
            return SeekFotImageListOnParents(element.Parent);
        }

        /// <summary>
        /// Convert Date to .Net format   
        /// </summary>
        /// <param name="dateTime">The dateTime .</param>
        /// <param name="dateTimePickerFormat">The dateTimePicker format.</param>
        /// <param name="customFormat">The dateTimePickercustom format </param>
        /// <returns></returns>
        internal static string ConvertDateToString(string dateTime, DateTimePickerFormat dateTimePickerFormat, string customFormat)
        {

            if (string.IsNullOrEmpty(dateTime))
            {
                return "";
            }

            DateTime date = DateTime.Parse(dateTime);

            if (date == null)
            {
                return "";
            }

            if (dateTimePickerFormat == DateTimePickerFormat.Custom &&string.IsNullOrEmpty(customFormat))
            {
                return "";
            }
            switch (dateTimePickerFormat)
            {
                case DateTimePickerFormat.Custom:
                    return date.ToString(customFormat,CultureInfo.InvariantCulture);

                case DateTimePickerFormat.Long:
                    return date.ToString("D", CultureInfo.InvariantCulture);

                case DateTimePickerFormat.Time:
                    return date.ToString("T", CultureInfo.InvariantCulture);

                case DateTimePickerFormat.Line:
                    break;
                case DateTimePickerFormat.Default:
                    return date.ToString("d", CultureInfo.InvariantCulture);

                case DateTimePickerFormat.Short:
                default:
                    return date.ToString(customFormat, CultureInfo.InvariantCulture);
            }
            return "";
        }
    }
}
