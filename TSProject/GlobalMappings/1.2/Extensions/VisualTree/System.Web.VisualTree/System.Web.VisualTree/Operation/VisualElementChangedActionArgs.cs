﻿using System.Collections.Generic;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    public class VisualElementChangedActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// The updated properties
        /// </summary>
        private readonly HashSet<string> _properties = new HashSet<string>();

        /// <summary>
        /// The visual element
        /// </summary>
        private readonly VisualElement _visualElement;


        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementChangedActionArgs"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public VisualElementChangedActionArgs(VisualElement visualElement)
        {
            _visualElement = visualElement;
        }


        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public VisualElement Element
        {
            get { return _visualElement; }
        } 


        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <value>
        /// The properties.
        /// </value>
        public HashSet<string> Properties
        {
            get { return _properties; }
        } 

    }

    
}
