﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    public class BrowserActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// The redirect url
        /// </summary>
        private readonly string _url;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrowserActionArgs"/> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        public BrowserActionArgs(string url)
        {
            _url = url;
        }


        /// <summary>
        /// Gets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url
        {
            get { return _url; }
        } 



        public static void OpenWindow(string url)
        {
            ApplicationElement.EnqueueAction(new BrowserActionArgs(url));
        }
    }
}

