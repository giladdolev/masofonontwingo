﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    /// <summary>
    /// The focus operation
    /// </summary>
    public class FocusActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// Gets or sets the focused element.
        /// </summary>
        /// <value>
        /// The focused element.
        /// </value>
        public VisualElement FocusedElement
        {
            get;
            set;
        }
    }
}
