﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    /// <summary>
    /// Provides support for executing an action
    /// </summary>
    public class VisualElementActionArgs
    {
        /// <summary>
        /// The owner
        /// </summary>
        private VisualElement _owner;

        /// <summary>
        /// The owner type
        /// </summary>
        private Type _ownerType;

        /// <summary>
        /// The id
        /// </summary>
        private string _id;


        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementActionArgs"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="ownerType">The owner type.</param>
        public VisualElementActionArgs(string id, Type ownerType)
        {
            _id = id;
            _ownerType = ownerType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementActionArgs"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="owner">The owner.</param>
        public VisualElementActionArgs(string id, VisualElement owner)
        {
            // Set the owner element
            _owner = owner;

            // If there is a valid element
            if (owner != null)
            {
                // Set the owner type
                _ownerType = owner.GetType();
            }
        }

        /// <summary>
        /// Gets or sets the action identifier.
        /// </summary>
        /// <value>
        /// The action identifier.
        /// </value>
        internal string ID
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        /// Gets or sets the owner type.
        /// </summary>
        /// <value>
        /// The owner type.
        /// </value>
        internal Type OwnerType
        {
            get
            {
                return _ownerType;
            }
        }

        /// <summary>
        /// Gets or sets the owner element.
        /// </summary>
        /// <value>
        /// The owner element.
        /// </value>
        internal VisualElement OwnerElement
        {
            get
            {
                return _owner;
            }
        }
    }
}
