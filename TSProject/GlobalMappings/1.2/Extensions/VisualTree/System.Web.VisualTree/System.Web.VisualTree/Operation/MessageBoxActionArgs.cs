﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    public   class MessageBoxActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// The text
        /// </summary>
        private readonly string mstrText;


        /// <summary>
        /// The caption
        /// </summary>
        private readonly string mstrCaption;

        /// <summary>
        /// The callback
        /// </summary>
        private EventHandler _callback;

        /// <summary>
        /// The id of the virtual window
        /// </summary>
        private string _id = "msgbox";

        /// <summary>
        /// The buttons
        /// </summary>
        private readonly MessageBoxButtons _buttons; 

        /// <summary>
        /// The icon
        /// </summary>
        private readonly MessageBoxIcon _icon;

        /// <summary>
        /// The dialog result
        /// </summary>
        private DialogResult _dialogResult = DialogResult.None;


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxActionArgs"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        public MessageBoxActionArgs(string text, string caption, MessageBoxButtons buttons , MessageBoxIcon icon, EventHandler callback)
        {
            mstrText = text;
            mstrCaption = caption;
            _callback = callback;
            _buttons = buttons;
            _icon = icon;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxActionArgs"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        public MessageBoxActionArgs(string text, string caption, MessageBoxButtons buttons , MessageBoxIcon icon ) :this(text, caption, buttons, icon, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxActionArgs"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        public MessageBoxActionArgs(string text, string caption, MessageBoxButtons buttons )
            : this(text, caption, buttons, MessageBoxIcon.None, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxActionArgs"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        public MessageBoxActionArgs(string text, string caption)
            : this(text, caption, MessageBoxButtons.OK, MessageBoxIcon.None, null)
        {

        }




        /// <summary>
        /// Gets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        public MessageBoxIcon Icon
        {
            get { return _icon; }
        } 

        /// <summary>
        /// Gets the buttons.
        /// </summary>
        /// <value>
        /// The buttons.
        /// </value>
        public MessageBoxButtons Buttons
        {
            get { return _buttons; }
        } 


        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption
        {
            get { return mstrCaption; }
        }

        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text
        {
            get { return mstrText; }
        }

        /// <summary>
        /// Gets the callback.
        /// </summary>
        /// <value>
        /// The callback.
        /// </value>
        public EventHandler Callback
        {
            get
            {
                return _callback;
            }
        }

      

        

        
    }
}
