﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    /// <summary>
    /// Provides support for window operations
    /// </summary>
    public class WindowActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// The window element
        /// </summary>
        private readonly WindowElement _windowElement;


        /// <summary>
        /// The operation type
        /// </summary>
        private readonly WindowActionType _actionType;

        /// <summary>
        /// The clinet identifier
        /// </summary>
        private readonly string _clinetId;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowActionArgs"/> class.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        /// <param name="actionType">The action type.</param>
        public WindowActionArgs(WindowElement windowElement, WindowActionType actionType)
        {
            _windowElement = windowElement;
            _actionType = actionType;

            // validate parameter
            if (windowElement == null)
            {
                throw new ArgumentNullException("windowElement");
            }
            _clinetId = windowElement.ClientID;
        }

        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        /// <value>
        /// The client identifier.
        /// </value>
        public string ClientID
        {
            get
            {
                return _clinetId;
            }
        }

        /// <summary>
        /// Gets the window.
        /// </summary>
        /// <value>
        /// The window.
        /// </value>
        public WindowElement Window
        {
            get { return _windowElement; }
        }

        /// <summary>
        /// Gets the type of the action.
        /// </summary>
        /// <value>
        /// The type of the action.
        /// </value>
        public WindowActionType ActionType
        {
            get { return _actionType; }
        } 



    }

    /// <summary>
    /// The window action type
    /// </summary>
    public enum WindowActionType
    {
        /// <summary>
        /// The open
        /// </summary>
        Open,

        /// <summary>
        /// Open a modal dialog
        /// </summary>
        OpenModal,

        /// <summary>
        /// The close
        /// </summary>
        Close
    }
}
