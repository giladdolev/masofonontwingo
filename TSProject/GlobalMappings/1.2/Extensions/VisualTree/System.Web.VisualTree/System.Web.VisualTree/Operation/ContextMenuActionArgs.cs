﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    public sealed class ContextMenuActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// The menu item
        /// </summary>
        private ToolBarMenuItem _menuItem = null;

        /// <summary>
        /// The callback
        /// </summary>
        private EventHandler _callback = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="ContextMenuActionArgs"/> class.
        /// </summary>
        /// <param name="menuItem">The menu item.</param>
        /// <param name="controlElement">The control element.</param>
        /// <param name="callback">The callback.</param>
        public ContextMenuActionArgs(ToolBarMenuItem menuItem, ControlElement controlElement, EventHandler callback)
        {
            _menuItem = menuItem;
            _callback = callback;
        }




        /// <summary>
        /// Gets the drop down items.
        /// </summary>
        /// <value>
        /// The drop down items.
        /// </value>
        public ToolBarItemCollection DropDownItems
        {
            get
            {
                if (_menuItem != null)
                {
                    return _menuItem.DropDownItems;
                }
                return null;
            }
        }


    }
}
