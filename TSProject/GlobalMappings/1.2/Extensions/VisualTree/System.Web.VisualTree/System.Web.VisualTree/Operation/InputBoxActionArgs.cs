﻿namespace System.Web.VisualTree.Operation
{
    /// <summary>
    /// Handles the input box
    /// </summary>
    public class InputBoxActionArgs : MessageBoxActionArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InputBoxActionArgs"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="callback">The callback.</param>
        public InputBoxActionArgs(string text, string caption, EventHandler callback ) 
            :base(text, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.None, callback)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InputBoxActionArgs"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="callback">The callback.</param>
        public InputBoxActionArgs(string text, string caption)
            : this(text, caption, null)
        {
            
        }



    }
}
