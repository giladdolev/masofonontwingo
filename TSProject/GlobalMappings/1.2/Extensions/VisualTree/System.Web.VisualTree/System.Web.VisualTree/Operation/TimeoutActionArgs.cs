﻿using System.Diagnostics;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    public sealed  class TimeoutActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// The operation manager
        /// </summary>
        private readonly ITimeoutActionManager _actionManager;

        /// <summary>
        /// The operation creation
        /// </summary>
        private long _operationCreation;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeoutActionArgs" /> class.
        /// </summary>
        /// <param name="operationManager">The operation manager.</param>
        /// <param name="operationCreation">The operation creation.</param>
        public TimeoutActionArgs(ITimeoutActionManager operationManager, long operationCreation)
        {
            _actionManager = operationManager;
            _operationCreation = operationCreation;
        }

        /// <summary>
        /// Performs specific actions of the operator in case of a do events call
        /// </summary>
        protected internal void DoEvents()
        {
            // If the timer is enabled
            if (Enabled)
            {
                // Get the current timestamp
                long currentTimestamp = Stopwatch.GetTimestamp();

                // Check if it's time to perform the callback operation
                if (TimeSpan.FromTicks(currentTimestamp - _operationCreation).Milliseconds >= Interval)
                {
                    try
                    {
                        // Perform the callback operation of the timer
                        //PerformCallback(EventArgs.Empty);
                    }
                    finally
                    {
                        // Set current time as the new operation creation time
                        _operationCreation = currentTimestamp;
                    }

                }
            }
        }

        /// <summary>
        /// Gets the action creation.
        /// </summary>
        /// <value>
        /// The action creation.
        /// </value>
        public long ActionCreation
        {
            get
            {
                return _operationCreation;
            }
        }

        /// <summary>
        /// Gets the action manager.
        /// </summary>
        /// <value>
        /// The action manager.
        /// </value>
        public ITimeoutActionManager ActionManager
        {
            get { return _actionManager; }
        }

        /// <summary>
        /// Gets the interval.
        /// </summary>
        /// <value>
        /// The interval.
        /// </value>
        public int Interval
        {
            get
            {
                return _actionManager.Interval;
            }
        }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled
        {
            get
            {
                return _actionManager.Enabled;
            }
        }


    }

    /// <summary>
    /// Provides support for generating controlling timeout operations
    /// </summary>
    public interface ITimeoutActionManager
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string ID { get; set; }

        /// <summary>
        /// Gets the interval.
        /// </summary>
        /// <value>
        /// The interval.
        /// </value>
        int Interval { get; }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool Enabled { get; }

        /// <summary>
        /// Performs the callback.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        void PerformCallback(EventArgs e);
    }
}
