﻿using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Operation
{
    /// <summary>
    /// Provides support for redirection
    /// </summary>
    public class RedirectActionArgs : VisualElementActionArgs
    {
        /// <summary>
        /// The redirect url
        /// </summary>
        private readonly string _url;

        /// <summary>
        /// Initializes a new instance of the <see cref="RedirectActionArgs"/> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        public RedirectActionArgs(string url)
        {
            _url = url;
        }


        /// <summary>
        /// Gets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url
        {
            get { return _url; }
        } 



        public static void Redirect(string url)
        {
            ApplicationElement.EnqueueAction(new RedirectActionArgs(url));
        }
    }
}
