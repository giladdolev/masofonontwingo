using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class CheckBoxElement : ButtonBaseElement
    {
        private bool _isChecked = false;
        private CheckState _checkState;
        private ContentAlignment _checkAlign = ContentAlignment.MiddleLeft;
        private bool _threeState = false;
        private Appearance _appearance = Appearance.Normal;
        private bool _autoCheck = true;


        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBoxElement"/> class.
        /// </summary>
        public CheckBoxElement()
        {

        }


        /// <summary>
        /// Gets or set a value indicating whether the Checked or CheckState values and the CheckBox's appearance are automatically changed when the CheckBox is clicked.
        /// </summary>
        /// <value>
        /// <c>true</c>if the Checked value or CheckState value and the appearance of the control are automatically changed on the Click event; otherwise,
        // /    <c>false</c>. The default value is <c>true</c>.
        /// </value>
        [DefaultValue(true)]
        [RendererPropertyDescription]
        public bool AutoCheck
        {
            get { return _autoCheck; }
            set
            {
                // change the value according the autoCheck property
                _autoCheck = value;

                OnPropertyChanged("AutoCheck");
            }
        }

        /// <summary>
        /// Gets or sets the appearance.
        /// </summary>
        /// <value>
        /// The appearance.
        /// </value>
        [DefaultValue(Appearance.Normal)]
        public Appearance Appearance
        {
            get { return _appearance; }
            set
            {
                _appearance = value;

                // Notify property Appearance changed
                OnPropertyChanged("Appearance");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the CheckBox will allow three check states rather than two.
        /// </summary>
        /// <value>
        ///   <c>true</c> if three state; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool ThreeState
        {
            get { return _threeState; }
            set
            {
                _threeState = value;

                // Notify property CheckAlign changed
                OnPropertyChanged("ThreeState");
            }
        }

        /// <summary>
        /// Gets or sets the horizontal and vertical alignment of the check mark on a
        //     System.Web.VisualTree CheckBox control.
        /// </summary>
        /// <value>
        /// One of the System.Drawing.ContentAlignment values. The default value is MiddleLeft.
        /// </value>
        [DefaultValue(ContentAlignment.MiddleLeft)]
        public ContentAlignment CheckAlign
        {
            get { return _checkAlign; }
            set
            {
                _checkAlign = value;

                // Notify property CheckAlign changed
                OnPropertyChanged("CheckAlign");
            }
        }




        /// <summary>
        /// Gets or sets the state of the System.Web.VisualTree CheckBox.
        /// </summary>
        /// <value>
        /// One of the System.Web.VisualTree.CheckState enumeration values. The default
        ///     value is Unchecked.
        /// </value>
        public CheckState CheckState
        {
            get { return _checkState; }
            set
            {
                _checkState = value;
                _isChecked = _checkState == CheckState.Checked;

                // Notify property CheckState changed
                OnPropertyChanged("IsChecked");
            }
        }




        /// <summary>
        /// Gets or set a value indicating whether the System.Web.VisualTree CheckBox
        ///     is in the checked state.
        /// </summary>
        /// <value>
        /// <c>true</c> if the System.Web.VisualTree.CheckBox is in the checked state; otherwise,
        // /    <c>false</c>. The default value is <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                if (_isChecked != value)
                {
                    // change the value according the isChecked property
                    _isChecked = value;
                    _checkState = value ? CheckState.Checked : CheckState.Unchecked;

                    OnPropertyChanged("IsChecked");
                }                
            }
        }




        [DesignerIgnore]
        public int IsCheckedInt
        {
            get
            {
                if (_isChecked)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                // change the value according the isChecked property
                if (value == 1)
                {
                    _isChecked = true;
                }
                else
                {
                    _isChecked = false;
                }

                OnPropertyChanged("IsChecked");
            }
        }




        /// <summary>
        /// Performs the <see cref="E:Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        internal void PerformClick(EventArgs args)
        {

            // Raise the Click event.
            this.OnClick(args);
        }


        /// <summary>
        /// CheckStateChanged event handler.
        /// </summary>
        private event EventHandler _CheckStateChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove CheckStateChanged action.
        /// </summary>
        public event EventHandler CheckStateChanged
        {
            add
            {
                bool needNotification = (_CheckStateChanged == null);

                _CheckStateChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CheckStateChanged");
                }

            }
            remove
            {
                _CheckStateChanged -= value;

                if (_CheckStateChanged == null)
                {
                    OnEventHandlerDeattached("CheckStateChanged");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:CheckStateChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CheckStateChanged"/> instance containing the event data.</param>
        protected virtual void OnCheckStateChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_CheckStateChanged != null)
            {
                _CheckStateChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CheckStateChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformCheckStateChanged(EventArgs args)
        {

            // Try to get string value changed arguments
            ValueChangedArgs<string> cellclickValueChangedArgs = args as ValueChangedArgs<string>;

            // If there is a valid string value changed arguments
            if (cellclickValueChangedArgs != null)
            {
                // Set text value
                switch (cellclickValueChangedArgs.Value)
                {
                    case "False":
                        this._checkState = CheckState.Unchecked;
                        PerformCheckedChanged(new ValueChangedArgs<bool>(false));
                        break;
                    case "True":
                        this._checkState = CheckState.Checked;
                        PerformCheckedChanged(new ValueChangedArgs<bool>(true));
                        break;
                    default:
                        this._checkState = CheckState.Indeterminate;
                        break;
                }
            }

            // Raise the SelectionChangeCommitted event.
            OnCheckStateChanged(args);
        }


        /// <summary>
        /// CheckedChanged event handler.
        /// </summary>
        private event EventHandler _CheckedChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove CheckedChanged action.
        /// </summary>
        public event EventHandler CheckedChanged
        {
            add
            {
                bool needNotification = (_CheckedChanged == null);

                _CheckedChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CheckedChanged");
                }

            }
            remove
            {
                _CheckedChanged -= value;

                if (_CheckedChanged == null)
                {
                    OnEventHandlerDeattached("CheckedChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CheckedChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCheckedChangedListeners
        {
            get { return _CheckedChanged != null || _CheckStateChanged == null; }
        }

        /// <summary>
        /// Raises the <see cref="E:CheckedChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CheckedChanged"/> instance containing the event data.</param>
        protected virtual void OnCheckedChanged(EventArgs args)
        {
          
            // Check if there are listeners.
            if (_CheckedChanged != null)
            {
                _CheckedChanged(this, args);
            }
            //Raise click event
            this.PerformClick(args);
        }


        /// <summary>
        /// Performs the <see cref="E:CheckedChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCheckedChanged(EventArgs args)
        {
            // If there is a valid changed arguments
            ValueChangedArgs<bool> changedArgs = args as ValueChangedArgs<bool>;

            // If is a checked argument
            if (changedArgs != null)
            {
                _isChecked = changedArgs.Value;

                OnCheckedChanged(args);
            }
           
        }
    }
}
