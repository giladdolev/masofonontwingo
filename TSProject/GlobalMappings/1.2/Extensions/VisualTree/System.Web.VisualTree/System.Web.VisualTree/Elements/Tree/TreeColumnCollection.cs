﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class TreeColumnCollection :  VisualElementCollection<TreeColumn>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnCollection" /> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">The property name</param>
        public TreeColumnCollection(VisualElement parentElement, string propertyName)
            : base(parentElement, propertyName)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnCollection"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        internal TreeColumnCollection(VisualElement parentElement)
            : base(parentElement, new CollectionBindingColumnsChangeTarget())
        {

        }

        /// <summary>
        /// Adds a <see cref="T:System.Web.VisualTree.Elements.GridColumn" /> with the given column name and column header text to the collection.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        /// <returns></returns>
        public virtual int Add(string columnName, string headerText)
        {
            TreeColumn column = new TreeColumn(columnName, headerText, typeof(string));
            this.Add(column);

            return this.IndexOf(column);
        }

        /// <summary>
        /// Inserts the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InsertItem(int index, TreeColumn item)
        {
            base.InsertItem(index, item);

            TreeElement tree = this.Owner as TreeElement;
            if (tree != null)
            {
                // Initialize DataMember for auto-generated columns
                if (String.IsNullOrEmpty(item.DataMember))
                {
                    item.DataMember = item.ID;
                }
                //tree.PerformAddedColumn(new ValueChangedArgs<int>(index));
            }
        }

        /// <summary>
        ///  Gets the index of the given string in the collection.    
        /// </summary>
        /// <param name="colName">The string to return the index of</param>
        /// <returns>The index of the given column name.</returns>
        public int IndexOf(string colName)
        {
            foreach (TreeColumn col in this)        
            {
                if (col.ID == colName)
                    return this.Items.IndexOf(col);
            }
            return -1;
        }

        /// <summary>
        /// Adds range of GridColumn.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">items</exception>
        public void AddRange(TreeColumn[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            foreach (TreeColumn item in items)
            {
                // Add the item
                if (!this.Contains(item))
                {
                    Add(item);
                }
            }
        }
        /// <summary>
        /// Determines whether the collection contains the column referred to by the given name.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        /// true if the column is contained in the collection; otherwise, false.
        /// </returns>
        /// <exception cref="ArgumentNullException">columnName is empty</exception>
        /// <exception cref="System.ArgumentNullException">columnName is null.</exception>
        public bool Contains(string columnName)
        {
            if (columnName == null)
                throw new ArgumentNullException("columnName is empty");
            foreach (TreeColumn col in this)
            {
                if (col.ID == columnName)
                    return true;
            }
            return false;
        }
    }
}
