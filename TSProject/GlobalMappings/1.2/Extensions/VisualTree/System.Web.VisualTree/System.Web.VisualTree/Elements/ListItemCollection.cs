﻿using System.Collections.ObjectModel;

namespace System.Web.VisualTree.Elements
{
    public class ListItemCollection : VisualElementCollection<ListItem>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListItemCollection" /> class without change tracking.
        /// </summary>
        internal ListItemCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualElementCollection" /> class without change tracking.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">Property ignored.</param>
        internal ListItemCollection(ListControlElement parentElement, string propertyName)
            : base(parentElement)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ListItemCollection" /> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        internal ListItemCollection(ListControlElement parentElement)
            : base(parentElement, new CollectionBindingItemsChangeTarget())
        {
        }

        /// <summary>
        /// Adds range of items.
        /// </summary>
        /// <param name="items">The items.</param>
        public void AddRange(params object[] items)
        {
            // If there are valid controls
            if (items != null)
            {
                // Loop all controls
                foreach (object controlElement in items)
                {
                    ListItem listItem = ListItem.GetItem(controlElement);
                    // Add the current control
                    Add(listItem);
                }
            }
        }

        /// <summary>
        /// Add new item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Add(object item)
        {
            return Add(item, false);
        }


        /// <summary>
        /// Add new item
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="isChecked">if set to <c>true</c> [is checked].</param>
        /// <returns></returns>
        public int Add(object item, bool isChecked)
        {
            ListItem listItem = ListItem.GetItem(item);
            base.Add(listItem);

            return IndexOf(listItem);
        }

        /// <summary>
        /// Add new item
        /// </summary>
        /// <param name="item">The object to insert. The value can be null for reference types.</param>
        /// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
        /// <returns></returns>
        public int Add(object item, int index)
        {
            ListItem listItem = ListItem.GetItem(item);
            InsertItem(index, listItem);

            return IndexOf(listItem);
        }

        /// <summary>
        ///  Inserts an item into the collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index location where the item is inserted</param>
        /// <param name="item">An string representing the item to insert</param>
        public void Insert(int index, string item)
        {
            ListItem listItem = new ListItem(item);
            InsertItem(index, listItem);
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public ListItem this[int index]
        {
            get
            {
                return base[index] as ListItem;
            }
            set
            {
                base[index] = value;
            }
        }

        /// <summary>
        ///  Gets the index of the given string in the collection.    
        /// </summary>
        /// <param name="listItem">The string to return the index of</param>
        /// <returns>The index of the given list item .</returns>
        public int IndexOf(ListItem listItem)
        {
            if (listItem != null)
            {
                int index = 0;
                foreach (ListItem item in this)
                {
                    if (!String.IsNullOrEmpty(item.ID))
                    {
                        if (string.Equals(item.ID, listItem.ID))
                        {
                            return index;
                        }
                    }
                    else
                    {
                        if (item.Value is String && !String.IsNullOrEmpty((string)item.Value))
                        {
                            if (string.Equals(item.Value, listItem.Value))
                            {
                                return index;
                            }
                        }
                    }   
                    index++;
                }
            }
            return -1;
        }

    }
}
