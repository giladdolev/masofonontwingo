using System.Data;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{GridRow}" />
    public class GridRowCollection : VisualElementCollection<GridRow>
    {


        private int _maxRows = Int32.MaxValue;
        /// <summary>
        /// Initializes a new instance of the <see cref="GridRowCollection"/> class.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        /// <param name="propertyName">The property name.</param>
        public GridRowCollection(GridElement gridElement, string propertyName)
            : base(gridElement, propertyName)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridRowCollection"/> class.
        /// </summary>
        /// <param name="gridElement">The grid element.</param>
        internal GridRowCollection(GridElement gridElement)
            : base(gridElement, new CollectionBindingItemsChangeTarget())
        {
        }



        /// <summary>
        /// Updated event handler.
        /// </summary>
        private event EventHandler<GridRowUpdatedEventArgs> _Updated;

        /// <summary>
        /// Add or remove Updated action.
        /// </summary>
        public event EventHandler<GridRowUpdatedEventArgs> Updated
        {
            add
            {
                _Updated += value;

            }
            remove
            {
                _Updated -= value;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Updated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Updated"/> instance containing the event data.</param>
        public void OnUpdated(GridRowUpdatedEventArgs args)
        {

            // Check if there are listeners.
            if (_Updated != null)
            {
                _Updated(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Updated" /> event.
        /// </summary>
        /// <param name="index">The updated row index.</param>
        /// <param name="action">The action name (such as 'Add', 'Remove'.. ext.)</param>
        /// <param name="row">The Affected Row</param>
        public void PerformUpdated(int index, string action, DataRow row)
        {
            var args = new GridRowUpdatedEventArgs(index, action, row);
            // Raise the Updated event.
            OnUpdated(args);
        }

        /// <summary>
        /// Adds a new row to the collection, and populates the cells with the specified objects.
        /// </summary>
        /// <param name="values">A variable number of objects that populate the cells of the new System.Web.VisualTree.Elements.GridRow.</param>
        /// <returns>The index of the new row.</returns>
        public int Add(params object[] values)
        {
            if (this.Count >= MaxRows)
            {
                throw new ArgumentException(Resource.MaxRows);
            }
            GridRow grdRow = new GridRow();
            base.Add(grdRow);
            WidgetColumn wColumn = null;
            GridElement grd = this.Owner as GridElement;
            if (grd != null)
            {
                if (grd.Columns.Count > 0)
                {
                    wColumn = grd.Columns[0] as WidgetColumn;
                }

            }

            int columnIndex = 0;
            foreach (var item in values)
            {

                // jedaan: - wrong behavior ! 
                // need to remove this code .
                if (grdRow.Cells.Count == 0)
                {
                    GridContentCell grdCell = new GridContentCell();

                    grdCell.Value = GetValueByType(item, columnIndex);
                    grdCell.Value = grdCell.Value == "null" ? null : grdCell.Value;
                    grdRow.Cells.Add(grdCell);
                }
                else
                {

                    grdRow.Cells[columnIndex].Value = GetValueByType(item, columnIndex);
                    grdRow.Cells[columnIndex].Value = grdRow.Cells[columnIndex].Value == "null" ? null : grdRow.Cells[columnIndex].Value;
                }
                if (wColumn != null)
                {
                    if (wColumn.Items.Count > -1 && wColumn.Items.Count > columnIndex)
                    {
                        grdRow.WidgetControls.Add(GetControl(wColumn.Items[columnIndex], grdRow));
                    }
                }
                columnIndex++;
            }
            base.Add(grdRow);
            return Count - 1;
        }

        public void SetWidgetControls(int rowIndex)
        {
            WidgetColumn wColumn = null;
            GridElement grd = this.Owner as GridElement;
            if (grd != null)
            {
                if (grd.Columns.Count > 0)
                {
                    wColumn = grd.Columns[0] as WidgetColumn;
                }

            }
            if (wColumn != null)
            {
                int columnIndex = 0;
                foreach (var item in wColumn.Items)
                {
                    if (wColumn.Items.Count > columnIndex)
                    {
                        this[rowIndex].WidgetControls.Add(GetControl(wColumn.Items[columnIndex], this[rowIndex]));
                    }
                    columnIndex++;
                }
            }
        }



        private ControlElement GetControl(ControlElement controlElement, GridRow grdRow)
        {
            int index = this.IndexOf(grdRow);
            if (controlElement is LabelElement)
            {
                LabelElement lbl = new LabelElement();
                lbl = controlElement as LabelElement;
                return lbl;
            }
            else if (controlElement is WidgetColumnTextBoxElement)
            {
                WidgetColumnTextBoxElement txt = new WidgetColumnTextBoxElement();
                txt = controlElement as WidgetColumnTextBoxElement;

                return txt;
            }
            else if (controlElement is WidgetColumnMultiComboBoxElement)
            {
                WidgetColumnMultiComboBoxElement mlcombo = new WidgetColumnMultiComboBoxElement();
                mlcombo = controlElement as WidgetColumnMultiComboBoxElement;
                return mlcombo;
            }

            return null;
        }

        /// <summary>
        /// Get value by the type 
        /// </summary>
        /// <param name="item">the item </param>
        /// <param name="columnIndex">column index</param>
        /// <returns></returns>
        private object GetValueByType(object item, int columnIndex)
        {
            GridElement parentGrid = this.Owner as GridElement;

            if (parentGrid != null && parentGrid.Columns.Count > 0)
            {
                switch (Convert.ToString(parentGrid.Columns[columnIndex].DataTypeName))
                {

                    case "Boolean":
                        GridCheckBoxColumn column = parentGrid.Columns[columnIndex] as GridCheckBoxColumn;
                        if (column.ThreeState == true)
                        {
                            if (item == null || Convert.ToString(item) == "Indeterminate")
                            {
                                return "null";
                            }
                        }
                        if (bool.Parse(Convert.ToString(item)) == true || Convert.ToString(item) == "Checked")
                        {
                            return "true";
                        }
                        else
                        {
                            return "false";
                        }

                    case "string":
                    default:
                        return Convert.ToString(item);

                }
            }
            return "";
        }

        /// <summary>
        /// Adds this instance.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException"></exception>
        public virtual int Add()
        {
            if (this.Count >= MaxRows)
            {
                throw new ArgumentException(Resource.MaxRows);
            }
            GridRow row = new GridRow();
            Add(row);
            return Count - 1;
        }

        /// <summary>
        /// Inserts the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InsertItem(int index, GridRow item)
        {
            if (!Owner.BatchProcessing)
            {
                MarkAsOldRow();
            }

            // Mark row as new 
            item.IsNewRow = true;

            // Insert item
            base.InsertItem(index, item);

            // Get parent grid reference
            GridElement grid = item.ParentElement as GridElement;
            if (grid != null)
            {
                int cellIndex = 0;
                if (item.Cells.Count == 0)
                {
                   /* WidgetGridElement widgetGrid = grid as WidgetGridElement;
                    if (widgetGrid != null)
                    {
                        for (cellIndex = 0; cellIndex < widgetGrid.WidgetColumnCollection.Count; cellIndex++)
                        {
                            GridContentCell grdCell = new GridContentCell();
                            grdCell.OwningColumn = widgetGrid.WidgetColumnCollection[cellIndex];
                            item.Cells.Add(grdCell);
                        }
                    }
                    else
                    {*/
                        for (cellIndex = 0; cellIndex < grid.Columns.Count; cellIndex++)
                        {
                            GridContentCell grdCell = new GridContentCell();
                            grdCell.OwningColumn = grid.Columns[cellIndex];
                            item.Cells.Add(grdCell);
                        }
                   // }
                }
                else
                {
                    foreach (GridCellElement cell in item.Cells)
                    {
                        // Update disconnected cell owner
                        cell.OwningColumn = grid.Columns[cellIndex++];
                    }
                }
            }
            if (!Owner.BatchProcessing)
            {
                DataRow DR = null;
                if (grid.DataSource != null)
                {
                    //Bulid a data row from the grid row
                    DR = (grid.DataSource as DataTable).NewRow(); // equals to: DataRow x = new DataRow(); 
                    object[] data = new object[item.Cells.Count];
                    foreach (var c in item.Cells)
                    {
                        data[c.ColumnIndex] = c.Value;
                    }
                    DR.ItemArray = data;
                }
                PerformUpdated(index, "Add", DR);
            }
        }

        /// <summary>
        /// Adds the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException"></exception>
        public int Add(GridRow row)
        {

            if (this.Count >= MaxRows)
            {
                throw new ArgumentException(Resource.MaxRows);
            }
            base.Add(row);

            return Count - 1;
        }

        /// <summary>
        /// Adds the specified count.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException"></exception>
        public virtual int Add(int count)
        {
            if (this.Count >= MaxRows)
            {
                throw new ArgumentException(Resource.MaxRows);
            }
            for (int i = 0; i < count; i++)
            {
                Add();
            }

            return Count - 1;
        }

        /// <summary>
        /// Set or get maximum rows allowed
        /// </summary>
        public int MaxRows
        {
            get
            {
                return _maxRows;
            }
            set
            {
                _maxRows = value;
            }
        }

        /// <summary>
        /// Assigned true to the IsNewRow Property of the new added , otherwise false  
        /// </summary>
        private void MarkAsOldRow()
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].IsNewRow == true)
                {
                    this[i].IsNewRow = false;
                }
            }
        }


        /// <summary>
        /// Adds range of GridRow.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">items</exception>
        public void AddRange(GridRow[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            foreach (GridRow item in items)
            {
                // Add the item
                if (!this.Contains(item))
                {
                    Add(item);
                }
            }
        }

        /// <summary>
        /// Remove GridRow by index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <exception cref="System.IndexOutOfRangeException">index</exception>
        public void RemoveAt(int index)
        {
            if (index < 0 || index > this.Count)
            {
                throw new IndexOutOfRangeException();
            }
            base.RemoveAt(index);
            PerformUpdated(index, "Remove", null);
            WidgetGridElement widgetGrid = this.Owner as WidgetGridElement;
            if (widgetGrid != null)
            {
                widgetGrid.RemoveFromViewModelContrlos(index);
            }
        }

        /// <summary>
        /// Clears with a message
        /// </summary>
        /// <param name="msg"></param>
        public void ClearEX(string msg = "")
        {
            this.ClearItems(msg);
        }
    }
}
