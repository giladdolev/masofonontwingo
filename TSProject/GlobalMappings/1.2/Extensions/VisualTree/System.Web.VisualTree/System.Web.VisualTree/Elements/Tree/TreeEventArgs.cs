namespace System.Web.VisualTree.Elements
{
	public class TreeEventArgs : EventArgs
	{

		/// <summary>
		/// The tree item
		/// </summary>
		private readonly TreeItem _treeItem = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="TreeEventArgs"/> class.
        /// </summary>
        /// <param name="treeItem">The tree item.</param>
        public TreeEventArgs(TreeItem treeItem)
		{
			_treeItem = treeItem;
		}

        /// <summary>
        /// Set,get checkedValue .
        /// </summary>
        public bool Checked
        {
            get;
            set;
        }

		/// <summary>
		/// Gets the node.
		/// </summary>
		/// <value>
		/// The node.
		/// </value>
		public TreeItem Item
		{
			get { return _treeItem; }
		}
	}
}
