namespace System.Web.VisualTree.Elements
{
	public class WindowClosingEventArgs : EventArgs
	{
        private CloseReason _closeReason;
        private bool _cancel;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowClosingEventArgs"/> class.
        /// </summary>
        public WindowClosingEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowClosingEventArgs"/> class.
        /// </summary>
        /// <param name="closeReason">The close reason.</param>
        /// <param name="cancel">if set to <c>true</c> cancel.</param>
        public WindowClosingEventArgs(CloseReason closeReason, bool cancel)
        {
            _cancel = cancel;
            _closeReason = closeReason;
        }



        /// <summary>
        /// Gets the close reason.
        /// </summary>
        /// <value>
        /// The close reason.
        /// </value>
		public CloseReason CloseReason
		{
			get
            {
                return _closeReason;
            }
		}



        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="WindowClosingEventArgs"/> is cancel.
        /// </summary>
        /// <value>
        ///   <c>true</c> if cancel; otherwise, <c>false</c>.
        /// </value>
        public bool Cancel
		{
			get
            {
                return _cancel;
            }
			set
            {
                _cancel = value;
            }
		}
	}
}
