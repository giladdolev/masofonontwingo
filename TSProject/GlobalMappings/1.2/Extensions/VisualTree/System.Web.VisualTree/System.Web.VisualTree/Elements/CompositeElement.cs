using System.Collections.Generic;
using System.ComponentModel;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class CompositeElement : CompositeElementBase, IComponentManagerContainer
    {
        /// <summary>
        /// The component manager elements (also knows as ghost elements)
        /// </summary>
        private List<VisualElement> componentManagerElements = new List<VisualElement>();



        /// <summary>
        /// Registers the component.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        void IComponentManagerContainer.RegisterComponent(IVisualElement visualElement)
        {
            // Get actual visual element
            VisualElement actualVisualElement = visualElement as VisualElement;

            // If there is a valid actual visual element
            if (actualVisualElement != null)
            {
                // Add to component manager elements
                componentManagerElements.Add(actualVisualElement);
                actualVisualElement.ParentElement = this;
            }
        }


        /// <summary>
        /// Gets the component elements.
        /// </summary>
        /// <value>
        /// The component elements.
        /// </value>
        IEnumerable<IVisualElement> IComponentManagerContainer.Elements
        {
            get
            {
                return componentManagerElements;
            }
        }


        /// <summary>
        /// Unloading event handler.
        /// </summary>
        private event EventHandler<CancelEventArgs> _Unloading;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Unloading action.
        /// </summary>
        public event EventHandler<CancelEventArgs> Unloading
        {
            add
            {
                bool needNotification = (_Unloading == null);

                _Unloading += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Unloading");
                }

            }
            remove
            {
                _Unloading -= value;

                if (_Unloading == null)
                {
                    OnEventHandlerDeattached("Unloading");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Unloading listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasUnloadingListeners
        {
            get { return _Unloading != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Unloading" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Unloading"/> instance containing the event data.</param>
        protected virtual void OnUnloading(CancelEventArgs args)
        {

            // Check if there are listeners.
            if (_Unloading != null)
            {
                _Unloading(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Unloading" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformUnloading(CancelEventArgs args)
        {
            // Raise the Unloading event.
            OnUnloading(args);
            if (!args.Cancel)
            {
                PerformUnload(args);
            }
        }


        /// <summary>
        /// Unload event handler.
        /// </summary>
        private event EventHandler<CancelEventArgs> _Unload;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Unload action.
        /// </summary>
        public event EventHandler<CancelEventArgs> Unload
        {
            add
            {
                bool needNotification = (_Unload == null);

                _Unload += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Unload");
                }

            }
            remove
            {
                _Unload -= value;

                if (_Unload == null)
                {
                    OnEventHandlerDeattached("Unload");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Unload listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasUnloadListeners
        {
            get { return _Unload != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Unload" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Unload"/> instance containing the event data.</param>
        protected virtual void OnUnload(CancelEventArgs args)
        {

            // Check if there are listeners.
            if (_Unload != null)
            {
                _Unload(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Unload" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformUnload(CancelEventArgs args)
        {
            if(!args.Cancel)
            {
                 // Raise the Unload event.
                OnUnload(args);

                RemoveComposite();
            }
           
        }

        /// <summary>
        /// Remove Composite element .
        /// </summary>
        [RendererMethodDescription]
        public void RemoveComposite()
        {
            this.InvokeMethodOnce("RemoveComposite");
        }

    }
}
