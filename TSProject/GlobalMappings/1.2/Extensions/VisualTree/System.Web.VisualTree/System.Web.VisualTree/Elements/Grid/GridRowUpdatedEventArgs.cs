﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace System.Web.VisualTree
{
    public class GridRowUpdatedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or Sets the Affected Row.
        /// </summary>
        public DataRow AffectedRow { get; set; }

        /// <summary>
        /// Gets or Sets the index of the updated row.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or Sets the Action Name (for examp. "Remove").
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridRowUpdatedEventArgs"/> class with the specified settings.
        /// </summary>
        /// <param name="index"> 
        /// An integer value that specifies the index of the updated row.
        /// </param>
        /// <param name="action"> 
        /// A String value that specifies the update Action  (for examp. "Add").
        /// </param>
        ///  <param name="row"> 
        /// The AffectedRow.
        /// </param>
        public GridRowUpdatedEventArgs(int index, string action, DataRow row)
        {
            Index = index;
            ActionName = action;
            AffectedRow = row;
        }
    }
}
