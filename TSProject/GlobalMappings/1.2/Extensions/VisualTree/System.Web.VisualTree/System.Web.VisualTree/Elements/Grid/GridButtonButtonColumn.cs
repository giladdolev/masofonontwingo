﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class GridButtonButtonColumn : GridColumn
    {
        private EventHandler _ellipsesButtonClick;
        private string _button1Text = "";
        private string _button2Text = "";
        private bool _textReadOnly = false;
        private bool _button1Visible = false;
        private bool _button2Visible = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn"/> class.
        /// </summary>
        public GridButtonButtonColumn()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridButtonButtonColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(string))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn" /> class.
        /// We used a default constructor how use 3 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        /// /// <param name="dataType">The dataType .</param>
        public GridButtonButtonColumn(string columnName, string headerText, System.Type dataType)
            : base(columnName, headerText, dataType)
        {

        }


        /// <summary>
        /// Gets or sets the text of the button.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [DefaultValue("")]
        public string Button1Text
        {
            get { return _button1Text; }
            set
            {
                // If the new value is not same as the old value
                if (_button1Text != value)
                {
                    // Set the text value
                    _button1Text = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the text of the button.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [DefaultValue("")]
        public string Button2Text
        {
            get { return _button2Text; }
            set
            {
                // If the new value is not same as the old value
                if (_button2Text != value)
                {
                    // Set the text value
                    _button2Text = value;
                }
            }
        }

        [DefaultValue(false)]
        public bool Button1Clicked
        {
            get;
            set;
        }

        [DefaultValue(false)]
        public bool Button2Clicked
        {
            get;
            set;
        }



        /// <summary>
        /// Button1Click event handler.
        /// </summary>
        private event EventHandler<EventArgs> _button1Click;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Button1Click action.
        /// </summary>
        public event EventHandler<EventArgs> Button1Click
        {
            add
            {
                bool needNotification = (_button1Click == null);

                _button1Click += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Button1Click");
                }

            }
            remove
            {
                _button1Click -= value;

                if (_button1Click == null)
                {
                    OnEventHandlerDeattached("Button1Click");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Button1Click listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasButton1ClickListeners
        {
            get { return true; }
        }

        /// <summary>
        /// Raises the <see cref="E:Button1Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Button1Click"/> instance containing the event data.</param>
        protected virtual void OnButton1Click(EventArgs args)
        {

            // Check if there are listeners.
            if (_button1Click != null)
            {
                _button1Click(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Button1Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformButton1Click(EventArgs args)
        {

            GridElement grd = this.Parent as GridElement;
            if (grd != null)
            {
                ButtonsColumnClickEventsArgs newArgs = new ButtonsColumnClickEventsArgs() { ClickedButtonText = this.Button1Text, RowIndex = grd.SelectedRowIndex, ColumnIndex = grd.SelectedColumnIndex };
                grd.PerformButtonsColumnClick(newArgs);
            }
            // Raise the Button1Click event.
            OnButton1Click(args);
        }



        /// <summary>
        /// Button2Click event handler.
        /// </summary>
        private event EventHandler<EventArgs> _button2Click;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Button2Click action.
        /// </summary>
        public event EventHandler<EventArgs> Button2Click
        {
            add
            {
                bool needNotification = (_button2Click == null);

                _button2Click += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Button2Click");
                }

            }
            remove
            {
                _button2Click -= value;

                if (_button2Click == null)
                {
                    OnEventHandlerDeattached("Button2Click");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Button2Click listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasButton2ClickListeners
        {
            get { return true; }
        }

        /// <summary>
        /// Raises the <see cref="E:Button2Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Button2Click"/> instance containing the event data.</param>
        protected virtual void OnButton2Click(EventArgs args)
        {

            // Check if there are listeners.
            if (_button2Click != null)
            {
                _button2Click(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Button2Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformButton2Click(EventArgs args)
        {

            GridElement grd = this.Parent as GridElement;
            if (grd != null)
            {

                ButtonsColumnClickEventsArgs newArgs = new ButtonsColumnClickEventsArgs() { ClickedButtonText = this.Button2Text, RowIndex = grd.SelectedRowIndex, ColumnIndex = grd.SelectedColumnIndex };
                grd.PerformButtonsColumnClick(newArgs);
            }
            // Raise the Button2Click event.
            OnButton2Click(args);
        }
    }
}
