using System.Collections;

namespace System.Web.VisualTree.Elements
{
    public class ListViewItemCollection : VisualElementCollection<ListViewItem>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItemCollection"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        public ListViewItemCollection(ListViewElement parentElement)
            : this(parentElement, false)
        {

        }

        /// <summary>
        /// Initializes a new instance of the System.Windows.Forms.ListView.ListViewItemCollection class.
        /// </summary>
        /// <param name="parentElement">The ListView that owns the collection.</param>
        /// <param name="applyChangeTarget">if set to <c>true</c> [apply change target].</param>
        internal ListViewItemCollection(ListViewElement parentElement, bool applyChangeTarget)
            : base(parentElement, applyChangeTarget ? new CollectionBindingItemsChangeTarget() : null)
        {

        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public virtual ListViewItem Add(string key, string text)
        {
            return Add(key, text, null);
        }



        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageKey">The image key.</param>
        /// <returns></returns>
        public virtual ListViewItem Add(string key, string text, string imageKey)
        {
            if (!string.IsNullOrEmpty(imageKey))
            {
                ListViewElement listViewElement = Owner as ListViewElement;

                if (listViewElement != null)
                {
                    ImageList imageList = listViewElement.SmallImageList;

                    if (imageList != null)
                    {
                        return Add(key, text, imageList.Images.IndexOfKey(imageKey));
                    }
                }
            }

            return Add(key, text, -1);
        }



        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageIndex">Index of the image.</param>
        /// <returns></returns>
        internal virtual ListViewItem Add(string key, string text, int imageIndex)
        {
            ListViewItem item = new ListViewItem() { Text = text, ImageIndex = imageIndex, Key = key };

            Add(item);

            return item;
        }


        /// <summary>
        /// Adds range of items.
        /// </summary>
        /// <param name="items">The items.</param>
        public virtual void AddRange(params object[] items)
        {
            // If there are valid controls
            if (items != null)
            {
                // Loop all controls
                foreach (object controlElement in items)
                {
                    ListViewItem listItem = ListViewItem.GetItem(controlElement);
                    // Add the current control
                    if (!this.Contains(listItem))
                    {
                        Add(listItem);
                    }
                }
            }
        }

        /// <summary>
        /// Adds range of items.
        /// </summary>
        /// <param name="items">The items.</param>
        public virtual void AddRange(ListViewItemCollection items)
        {
            // If there are valid controls
            if (items != null)
            {
                // Loop all controls
                foreach (ListViewItem controlElement in items)
                {
                    ListViewItem listItem = ListViewItem.GetItem(controlElement);
                    // Add the current control
                    Add(listItem);
                }
            }
        }

        /// <summary>
        /// Adds the item with given key (name).
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public virtual ListViewItem Add(string key)
        {
            ListViewItem item = new ListViewItem() { Key = key };

            Add(item);

            return item;
        }

        internal void NotifyUpdate(ListViewItem listViewItem, ListViewSubitem subItem, string propertyName)
        {

        }


        /// <summary>
        /// Determines whether the collection contains key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if the specified key contains key; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool ContainsKey(string key)
        {
            return default(bool);
        }


        /// <summary>
        /// Removes the by key.
        /// </summary>
        /// <param name="key">The key.</param>
        public virtual void RemoveByKey(string key)
        {
        }



        public virtual void RemoveByIndex(int index)
        {

        }



        /// <summary>
        /// Searches for Controls by their Name property, builds up an array of all the controls that match. 
        /// </summary>
        /// <param name="key">The item key.</param>
        /// <param name="searchAllSubItems">if set to <c>true</c> [search all sub items].</param>
        /// <returns></returns>
        public ListViewItem[] Find(string key, bool searchAllSubItems)
        {
            ArrayList foundItems = FindInternal(key, searchAllSubItems, this, new ArrayList());

            ListViewItem[] stronglyTypedFoundItems = new ListViewItem[foundItems.Count];
            foundItems.CopyTo(stronglyTypedFoundItems, 0);

            return stronglyTypedFoundItems;
        }

        /// <devdoc>
        ///     <para>Searches for Controls by their Name property, builds up an arraylist
        ///           of all the controls that match.
        ///     </para> 
        /// </devdoc>
        /// <internalonly/> 
        private ArrayList FindInternal(string key, bool searchAllSubItems, ListViewItemCollection listViewItems, ArrayList foundItems)
        {
            if ((listViewItems == null) || (foundItems == null))
            {
                return null;  // 
            }

            for (int i = 0; i < listViewItems.Count; i++)
            {

                if (string.Compare(listViewItems[i].Key, key, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    foundItems.Add(listViewItems[i]);
                }
                else
                {
                    if (searchAllSubItems)
                    {
                        // start from 1, as we've already compared subitems[0]
                        for (int j = 1; j < listViewItems[i].Subitems.Count; j++)
                        {
                            if (string.Compare(listViewItems[i].Key, key, StringComparison.OrdinalIgnoreCase) != 0)
                            {
                                foundItems.Add(listViewItems[i]);
                                break;
                            }
                        }
                    }
                }
            }

            return foundItems;
        }

       
    }
}
