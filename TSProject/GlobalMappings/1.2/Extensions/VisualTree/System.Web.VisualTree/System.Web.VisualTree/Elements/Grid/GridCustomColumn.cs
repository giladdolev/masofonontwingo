﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class GridCustomColumn : GridColumn
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridCustomColumn"/> class.
        /// </summary>
        public GridCustomColumn()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="GridCustomColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridCustomColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(string))
        {
        }

        /// <summary>
        /// Gets or sets the renderer.
        /// </summary>
        /// <value>
        /// The renderer.
        /// </value>
        [RendererPropertyDescription]
        public string Renderer { get; set; }

        
        /// <summary>
        /// ColumnAction event handler.
        /// </summary>
        private event EventHandler<EventArgs> _ColumnAction;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove ColumnAction action.
        /// </summary>
        public event EventHandler<EventArgs> ColumnAction
        {
            add
            {
                bool needNotification = (_ColumnAction == null);

                _ColumnAction += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ColumnAction");
                }

            }
            remove
            {
                _ColumnAction -= value;

                if (_ColumnAction == null)
                {
                    OnEventHandlerDeattached("ColumnAction");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ColumnAction listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasColumnActionListeners
        {
            get { return _ColumnAction != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ColumnAction" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ColumnAction"/> instance containing the event data.</param>
        protected virtual void OnColumnAction(EventArgs args)
        {

            // Check if there are listeners.
            if (_ColumnAction != null)
            {
                _ColumnAction(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ColumnAction" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformColumnAction(EventArgs args)
        {

            // Raise the ColumnAction event.
            OnColumnAction(args);

            // Use CellEndEdit To Update The Value . 
            GridElement grd = this.Parent as GridElement;
            if (grd != null)
            {
                ValueChangedArgs<string> newArgs = args as ValueChangedArgs<string>;
                if (newArgs != null)
                {
                    grd.PerformCellEndEdit(newArgs);
                }
            }
        }


        /// <summary>
        /// Gets a value indicating whether this instance has BeforeCellRendering listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeCellRenderingListeners
        {
            get { return _BeforeCellRendering != null; }
        }

        /// <summary>
        /// BeforeCellRendering event handler.
        /// </summary>
        private event EventHandler<CellRenderingEventArgs> _BeforeCellRendering;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove BeforeCellRendering action.
        /// </summary>
        public event EventHandler<CellRenderingEventArgs> BeforeCellRendering
        {
            add
            {
                bool needNotification = (_BeforeCellRendering == null);

                _BeforeCellRendering += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeCellRendering");
                }

            }
            remove
            {
                _BeforeCellRendering -= value;

                if (_BeforeCellRendering == null)
                {
                    OnEventHandlerDeattached("BeforeCellRendering");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeCellRendering" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeCellRendering"/> instance containing the event data.</param>
        protected virtual void OnBeforeCellRendering(CellRenderingEventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeCellRendering != null)
            {
                _BeforeCellRendering(this, args);
                InvokeMethod("RenderCellEditor", args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeCellRendering" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeCellRendering(CellRenderingEventArgs args)
        {

            // Raise the BeforeCellRendering event.
            OnBeforeCellRendering(args);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public enum GridColumnType
    {
        CheckBox,
        ComboBox,
        DateTimePicker,
        TextBox
    }

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class CellRenderingEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CellRenderingEventArgs"/> class.
        /// </summary>
        public CellRenderingEventArgs()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CellRenderingEventArgs"/> class.
        /// </summary>
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <param name="value">The value.</param>
        public CellRenderingEventArgs(int rowIndex, int columnIndex, object value)
        {
            this.RowIndex = rowIndex;
            this.ColumnIndex = columnIndex;
            this.Value = value;
        }

        /// <summary>
        /// The cell type
        /// </summary>
        private GridColumnType _CellType = GridColumnType.TextBox;

        /// <summary>
        /// Gets or sets the type of the cell.
        /// </summary>
        /// <value>
        /// The type of the cell.
        /// </value>
        public GridColumnType CellType
        {
            get { return _CellType; }
            set { _CellType = value; }
        }


        /// <summary>
        /// Gets or sets the list items (coma separated list of text|values.
        /// </summary>
        /// <value>
        /// The list items.
        /// </value>
        public string ListItems { get; set; }

        /// <summary>
        /// Gets or sets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int RowIndex { get; set; }

        /// <summary>
        /// Gets or sets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the column.
        /// </value>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value { get; set; }
    }
}
