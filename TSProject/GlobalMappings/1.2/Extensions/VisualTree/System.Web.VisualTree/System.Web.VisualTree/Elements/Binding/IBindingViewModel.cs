﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{

    /// <summary>
    /// Provides support for binding view model
    /// </summary>
    public interface IBindingViewModel
    {


        /// <summary>
        /// Binds the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        void Bind(IDataReader dataReader);

        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        IEnumerable<IBindingDataColumn> Columns { get; }

        /// <summary>
        /// Gets the records.
        /// </summary>
        /// <value>
        /// The records.
        /// </value>
        IEnumerable<IBindingViewModelRecord> Records { get; }
    }

}
