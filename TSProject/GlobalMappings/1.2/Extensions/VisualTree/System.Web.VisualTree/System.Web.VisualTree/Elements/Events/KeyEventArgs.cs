using System.ComponentModel;
namespace System.Web.VisualTree.Elements
{
    public class KeyEventArgs : CancelEventArgs
	{
        /// <summary>
        /// The key data
        /// </summary>
        public  Keys _keyData;


        /// <summary>
        /// Initializes a new instance of the <see cref="KeyEventArgs"/> class.
        /// </summary>
        public KeyEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyEventArgs"/> class.
        /// </summary>
        /// <param name="keyData">The key data.</param>
        public KeyEventArgs(Keys keyData)
		{
            _keyData = keyData;
		}



        /// <summary>
        /// Gets a value indicating whether alt is pressed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if alt is pressed; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Alt
        {
            get
            {
                return ((_keyData & Keys.Alt) == Keys.Alt);
            }
        }
 




        /// <summary>
        /// Gets a value indicating whether control.
        /// </summary>
        /// <value>
        ///   <c>true</c> if control; otherwise, <c>false</c>.
        /// </value>
        public bool Control
        {
            get
            {
                return ((_keyData & Keys.Control) == Keys.Control);
            }
        }





        /// <summary>
        /// Gets or sets a value indicating whether event was handled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if was handled; otherwise, <c>false</c>.
        /// </value>
		public bool Handled
		{
			get;
			set;
		}



        /// <summary>
        /// Gets the key code.
        /// </summary>
        /// <value>
        /// The key code.
        /// </value>
        public  Keys KeyCode
        {
            get
            {
                Keys keys = _keyData & Keys.KeyCode;
                if (!Enum.IsDefined(typeof(Keys), (int)keys))
                {
                    return Keys.None;
                }
                return keys;
            }
            set
            {

            }
        }







        /// <summary>
        /// Gets the key value.
        /// </summary>
        /// <value>
        /// The key value.
        /// </value>
        public int KeyValue
        {
            get
            {
                return (((int)_keyData) & 0xffff);
            }
        }


        /// <summary>
        /// Gets or sets the key character.
        /// </summary>
        /// <value>
        /// The key character.
        /// </value>
        public char KeyChar
        {
            get
            {
                unchecked
                {
                    return (char)KeyValue;
                }
            }
        }




        /// <summary>
        /// Gets the key data.
        /// </summary>
        /// <value>
        /// The key data.
        /// </value>
		public Keys KeyData
		{
			get
            {
                return _keyData;
            }
		}



        /// <summary>
        /// Gets the modifiers.
        /// </summary>
        /// <value>
        /// The modifiers.
        /// </value>
        public Keys Modifiers
        {
            get
            {
                return (_keyData & ~Keys.KeyCode);
            }
        }







        /// <summary>
        /// Gets a value indicating whether shift.
        /// </summary>
        /// <value>
        ///   <c>true</c> if shift; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Shift
        {
            get
            {
                return ((_keyData & Keys.Shift) == Keys.Shift);
            }
        }







        /// <summary>
        /// Gets or sets a value indicating whether suppress key press.
        /// </summary>
        /// <value>
        ///   <c>true</c> if suppress key press; otherwise, <c>false</c>.
        /// </value>
		public bool SuppressKeyPress
		{
			get;
			set;
		}
	}
}
