using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
	public class RangeElement : ControlElement
	{

         /// <summary>
        /// The maximum value for the Slider.
        /// </summary>
        private int _maxValue;


        /// <summary>
        /// The minimum value for the Slider.
        /// </summary>
        private int _minValue;


        /// <summary>
        /// How many units to change the slider when adjusting by drag and drop.
        /// </summary>
        private int _increment;


        /// <summary>
        /// Array of Number values with which to initalize the slider.
        /// A separate slider thumb will be created for each value in this array.
        /// </summary>
        private int[] _valuesRange = new int[] {0,0};



        [RendererPropertyDescription]
        /// <summary>
        /// range Description.
        /// </summary>
        public int[] ValuesRange
        {
            get
            {
                return _valuesRange;
            }
            set
            {
                if (_valuesRange != value)
                {
                    _valuesRange = value;

                    OnPropertyChanged("ValuesRange");
                }
            }
        }



        [RendererPropertyDescription]
        /// <summary>
        /// increment Description.
        /// </summary>
        public int Increment
        {
            get
            {
                return _increment;
            }
            set
            {
                if (_increment != value)
                {
                    _increment = value;

                    OnPropertyChanged("Increment");
                }
            }
        }


        [DefaultValue(0)]
        [RendererPropertyDescription]
        /// <summary>
        /// minValue Description.
        /// </summary>
        public int MinValue
        {
            get
            {
                return _minValue;
            }
            set
            {
                if (_minValue != value)
                {
                    _minValue = value;

                    OnPropertyChanged("MinValue");
                }
            }
        }


        [DefaultValue(0)]
        [RendererPropertyDescription]
        /// <summary>
        /// maxValue Description.
        /// </summary>
        public int MaxValue
        {
            get
            {
                return _maxValue;
            }
            set
            {
                if (_maxValue != value)
                {
                    _maxValue = value;

                    OnPropertyChanged("MaxValue");
                }
            }
        }


      
    

        /// <summary>
        /// Scroll event handler.
        /// </summary>
        private event EventHandler _Scroll;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Scroll action.
        /// </summary>
        public event EventHandler Scroll
        {
            add
            {
                bool needNotification = (_Scroll == null);

                _Scroll += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Scroll");
                }

            }
            remove
            {
                _Scroll -= value;

                if (_Scroll == null)
                {
                    OnEventHandlerDeattached("Scroll");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Scroll listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasScrollListeners
        {
            get { return _Scroll != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Scroll" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Scroll"/> instance containing the event data.</param>
        protected virtual void OnScroll(EventArgs args)
        {

            // Check if there are listeners.
            if (_Scroll != null)
            {
                _Scroll(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Scroll" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformScroll(EventArgs args)
        {

            // Raise the Scroll event.
            OnScroll(args);
        }

	}
}
