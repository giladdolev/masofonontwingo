using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class TabItem : PanelElement, IClientHistoryElement
    {
        /// <summary>
        /// TabActivated event handler.
        /// </summary>
        private event EventHandler _TabActivated;
        private int _imageIndex = -1;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove TabActivated action.
        /// </summary>
        public event EventHandler TabActivated
        {
            add
            {
                bool needNotification = (_TabActivated == null);

                _TabActivated += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("TabActivated");
                }

            }
            remove
            {
                _TabActivated -= value;

                if (_TabActivated == null)
                {
                    OnEventHandlerDeattached("TabActivated");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:TabActivated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="TabActivated"/> instance containing the event data.</param>
        protected virtual void OnTabActivated(EventArgs args)
        {

            // Check if there are listeners.
            if (_TabActivated != null)
            {
                _TabActivated(this, args);
            }
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="TabItem"/> class.
        /// </summary>
        public TabItem()
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="TabItem"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        public TabItem(string title)
        {
            this.Text = title;
        }



        /// <summary>
        /// Gets or sets a value indicating whether [use visual style back color].
        /// </summary>
        /// <value>
        /// <c>true</c> if [use visual style back color]; otherwise, <c>false</c>.
        /// </value>
        public bool UseVisualStyleBackColor
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the size of the item.
        /// </summary>
        /// <value>
        /// The size of the item.
        /// </value>
        public System.Drawing.SizeF ItemSize
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        [RendererPropertyDescription]
        public virtual ResourceReference Image
        {
            get;
            set;
        }



        /// <summary>
        /// Performs the <see cref="E:TabActivated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformTabActivated(EventArgs args)
        {
            // Get the tab element containing this tab item
            TabElement tabElement = Parent as TabElement;

            // If there is a valid tab item
            if (tabElement != null)
            {
                // Set selected tab item
                tabElement.SetSelectedTab(this, true);

                // To emulate desktop like event we do the click event
                tabElement.PerformClick(args);

                OnTabActivated(args);
            }
        }


        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // Get the tag page settings
            TabItemSettings objTabPageSettings = visualElement as TabItemSettings;

            // If there is a valid tab page settings
            if (objTabPageSettings != null)
            {

            }

            base.Add(visualElement, insert);
        }



        /// <summary>
        /// tab image index
        /// </summary>
        public int ImageIndex
        {
            get { return _imageIndex; }
            set { _imageIndex = value; }
        }

        /// <summary>
        /// Navigates to this element.
        /// </summary>
        void IClientHistoryElement.Navigate()
        {
            // Get parent tab
            TabElement tabElement = this.Parent as TabElement;

            // If there is a valid tab
            if (tabElement != null)
            {
                // Suspend history
                using (tabElement.CreateSuspendHistorySession())
                {
                    // Set the selected tab
                    tabElement.SelectedTab = this;
                }
            }
        }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override int Index
        {
            get
            {
                TabElement tabelement = this.Parent as TabElement;
                if (tabelement != null)
                {
                    return tabelement.TabItems.IndexOf(this);
                }
                return -1;
            }
        }
    }
}
