﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class RibbonPageElement : PanelElement
    {

          public RibbonPageElement()
          {
              ShowHeader = true;
          }

          /// <summary>
          /// Gets Groups.
          /// </summary>
          /// <value>
          /// The Groups.
          /// </value>
          public RibbonPageGroupCollection Groups
          {
              get { return Controls as RibbonPageGroupCollection; }
          }

          /// <summary>
          /// Creates the RibbonPageGroup element collection.
          /// </summary>
          /// <returns></returns>
          protected override ControlElementCollection CreateControlElementCollection()
          {
              return new RibbonPageGroupCollection(this, "Groups");
          }
       

    }
}
