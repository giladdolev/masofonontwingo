namespace System.Web.VisualTree.Elements
{
	public class TreeMouseClickEventArgs : EventArgs
	{		/// <summary>
		/// The tree item
		/// </summary>
		private readonly TreeItem _node = null;


		/// <summary>
		/// Initializes a new instance of the <see cref="TreeMouseClickEventArgs"/> class.
		/// </summary>
		/// <param name="node">The item.</param>
        public TreeMouseClickEventArgs(TreeItem node)
		{
			_node = node;
		}



		/// <summary>
		/// Gets the node.
		/// </summary>
		/// <value>
		/// The node.
		/// </value>
		public TreeItem Node
		{
			get { return _node; }
		}
	}
}
