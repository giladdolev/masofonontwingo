﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// The data view band type
    /// </summary>
    public enum DataViewBandType
    {
        /// <summary>
        /// The none
        /// </summary>
        None,

        /// <summary>
        /// The header
        /// </summary>
        Header,

        /// <summary>
        /// The details
        /// </summary>
        Details,

        /// <summary>
        /// The footer
        /// </summary>
        Footer,

        /// <summary>
        /// The summary
        /// </summary>
        Summary
    }
}
