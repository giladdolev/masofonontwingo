﻿using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class CursorElement : VisualElement, IDisposable, ISerializable
    {
        private byte[] cursorData;
        private static Size cursorSize = Size.Empty;
        private IntPtr handle;
        private bool ownHandle;
        private int resourceId;
        private string cursorName;


        public string CursorName
        {
            get
            {
                return cursorName;
            }
        }


        // Properties
        public static Rectangle Clip { get; set; }


        internal static Rectangle ClipInternal { get; set; }


        public static CursorElement Current { get; set; }


        internal static CursorElement CurrentInternal { get; set; }


        public static Point Position { get; set; }


        public Size Size
        {
            get
            {
                if (cursorSize.IsEmpty)
                {
                    cursorSize = new Size(13, 14);
                }
                return cursorSize;
            }
        }


        [DesignerType(typeof(string))]
        public object Tag { get; set; }


        public IntPtr Handle
        {
            get
            {
                if (handle == IntPtr.Zero)
                {
                    throw new ObjectDisposedException(base.GetType().Name);
                }
                return handle;
            }
        }




        private static void LoadPicture(Stream stream)
        {
            return;
        }




        internal CursorElement(SerializationInfo info, StreamingContext context)
        {
            handle = IntPtr.Zero;
            ownHandle = true;
            SerializationInfoEnumerator enumerator = info.GetEnumerator();
            if (enumerator != null)
            {
                while (enumerator.MoveNext())
                {
                    if (string.Equals(enumerator.Name, "CursorData", StringComparison.OrdinalIgnoreCase))
                    {
                        cursorData = (byte[])enumerator.Value;
                        if (cursorData != null)
                        {
                            LoadPicture(new MemoryStream(this.cursorData));
                        }
                    }
                    else if (string.Compare(enumerator.Name, "CursorResourceId", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        LoadFromResourceId((int)enumerator.Value);
                    }
                }
            }
        }



        internal CursorElement(string resource)
        {
            cursorName = resource;

        }



        public IntPtr CopyHandle()
        {
            return Handle;
        }



        private void DestroyHandle()
        {
            if (ownHandle)
            {
                // DestroyCursor(new HandleRef(this, this.handle));
            }
        }



        protected override void Dispose(bool disposing)
        {
            if (handle != IntPtr.Zero)
            {
                DestroyHandle();
                handle = IntPtr.Zero;
            }
        }



        public override bool Equals(object obj)
        {
            CursorElement cursotElement = obj as CursorElement;
            return cursotElement != null && this == cursotElement;
        }




        ~CursorElement()
        {
            Dispose(false);
        }



        public override int GetHashCode()
        {
            return (int)handle;
        }



        private Size GetIconSize(IntPtr iconHandle)
        {
            Size size = Size;

            return size;
        }



        public static void Hide()
        {
            return;
            //IntSecurity.AdjustCursorClip.Demand();
            // ShowCursor(false);
        }



        private void LoadFromResourceId(int nResourceId)
        {
            ownHandle = false;
            resourceId = nResourceId;
        }



        public static void Show()
        {
            return;
            //UnsafeNativeMethods.ShowCursor(true);
        }



        void ISerializable.GetObjectData(SerializationInfo si, StreamingContext context)
        {
            // validate parameter
            if (si == null)
            {
                return;
            }

            if (cursorData != null)
            {
                si.AddValue("CursorData", cursorData, typeof(byte[]));
            }
            else
            {
                if (resourceId == 0)
                {
                    throw new SerializationException();
                }
                si.AddValue("CursorResourceId", resourceId, typeof(int));
            }
        }   
    }
}
