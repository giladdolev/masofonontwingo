using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.VisualTree.Common.Attributes;
using System.Web.UI;
using System.Data;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents a Visual Tree control to display a list of items.
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.InputElement" />
    /// <seealso cref="System.Web.VisualTree.Elements.IListItems" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingViewModel" />
    [DesignTimeVisible(false)]
    public class ListControlElement : InputElement, IListItems, IBindingViewModel
    {
        private string _displayMember = "text";
        private string _valueMember = "value";
        private int _SelectedIndex = -1;
        private readonly ListItemCollection _items = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="ListBoxElement"/> class.
        /// </summary>
        public ListControlElement()
        {
            _items = new ListItemCollection(this);
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                foreach (VisualElement item in _items)
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// Binds the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        void IBindingViewModel.Bind(IDataReader dataReader)
        {
            // Clear the list items
            _items.Clear();

            try
            {
                // If there is a valid data reader
                if (dataReader != null)
                {
                    // Get display member ordinal
                    int intDisplayMember = dataReader.GetOrdinal(this.DisplayMember);

                    // If there is a valid display member ordinal
                    if (intDisplayMember > -1)
                    {
                        // Get value member ordinal
                        int intValueMember = dataReader.GetOrdinal(this.ValueMember);

                        // If there is a valid value member ordinal
                        if (intValueMember > -1)
                        {
                            using (this.BatchBlock)
                            {
                                int i = 0;
                                // Read all records
                                while (dataReader.Read())
                                {

                                    ListItem item = new ListItem(dataReader.GetValue(intValueMember),
                                        Convert.ToString(dataReader.GetValue(intDisplayMember)))
                                    {
                                        ID = String.Format("{0}_item{1}", this.ID, i)

                                    };
                                    // fill the data row 
                                    for (int j = 0; j < dataReader.FieldCount; j++)
                                    {
                                       item.DataRow.Add(new ListItem(Convert.ToString(dataReader.GetValue(j))));
                                    }

                                    // Add items
                                    _items.Add(item);
                                    
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        IEnumerable<IBindingDataColumn> IBindingViewModel.Columns
        {
            get
            {

                yield return new BindingDataColumn(typeof(ListItem).GetProperty("Value"));
                yield return new BindingDataColumn(typeof(ListItem).GetProperty("Text"));
            }
        }

        /// <summary>
        /// Gets the records.
        /// </summary>
        /// <value>
        /// The records.
        /// </value>
        IEnumerable<IBindingViewModelRecord> IBindingViewModel.Records
        {
            get
            {
                return this._items;
            }
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ListItem>))]
        public ListItemCollection Items
        {
            get { return _items; }
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            ListItem listItem = ListItem.GetItem(visualElement);
            Items.Add(listItem);
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="index"></param>
        public void Add(object visualElement, int index = 0)
        {
            ListItem listItem = ListItem.GetItem(visualElement);
            Items.Add(listItem);
        }

        /// <summary>
        /// Gets or sets the display member.
        /// </summary>
        /// <value>
        /// The display member.
        /// </value>
        [RendererPropertyDescription]
        public string DisplayMember
        {
            get
            {
                return _displayMember;
            }
            set
            {
                _displayMember = value;
                OnPropertyChanged("DisplayMember");

                this.Bind();
            }
        }

        /// <summary>
        /// Binds to data
        /// </summary>
        private void Bind()
        {
            // Re-bind the data source
            this.DataSource = this.DataSource;
        }

        /// <summary>
        /// Gets or sets the value member.
        /// </summary>
        /// <value>
        /// The value member.
        /// </value>
        [RendererPropertyDescription]
        public string ValueMember
        {
            get
            {
                return _valueMember;
            }
            set
            {
                _valueMember = value;
                OnPropertyChanged("ValueMember");

                this.Bind();
            }
        }



        public bool FormattingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the item data.
        /// </summary>
        /// <value>
        /// The item data.
        /// </value>
        public int ItemData
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count
        {
            get { return Items == null ? 0 : Items.Count(); }
        }

        /// <summary>
        /// Gets or sets the index of the selected.
        /// </summary>
        /// <value>
        /// The index of the selected.
        /// </value>
        [DefaultValue(-1)]
        public virtual int SelectedIndex
        {
            get
            {
                return _SelectedIndex;
            }
            set
            {
                _SelectedIndex = value;
            }
        }

        
        /// <summary>
        /// Gets or sets the selected value.
        /// </summary>
        /// <value>
        /// The selected value.
        /// </value>
        public virtual object SelectedValue
        {

            get
            {
                if (!ValidateValues())
                {
                    return Items[SelectedIndex].Value;
                }
                return null;
            }
            set
            {
                ListItem listItem = this.Items.FirstOrDefault(item => (item.Value != null && item.Value.Equals(value)));
                if (listItem != null && this.DataSource != null)
                {
                    SelectedIndex = this.Items.IndexOf(listItem);
                    ValueChangedArgs<string> valueChangedValueChangedArgs = new ValueChangedArgs<string>(listItem.Value.ToString());
                    // Raise the SelectedValueChanged event.
                    PerformSelectedValueChanged(valueChangedValueChangedArgs);
                }
            }
        }

        /// <summary>
        /// Validate the selected index and datasource properties
        /// </summary>
        /// <returns>true if valid, false otherwise</returns>
        internal bool ValidateValues()
        {
            return (SelectedIndex > -1 && Items.Count > SelectedIndex && this.DataSource != null);
        }

        /// <summary>
        /// Gets or sets the selected text.
        /// </summary>
        /// <value>
        /// The selected text.
        /// </value>
        public virtual object SelectedText
        {

            get
            {
                if (SelectedIndex > -1 && Items.Count > SelectedIndex)
                {
                    return Items[SelectedIndex].Text;
                }

                return null;
            }
            set
            {
                ListItem listItem = this.Items.FirstOrDefault(item => (item.Text.Equals(value)));
                if (listItem != null)
                {
                    SelectedIndex = this.Items.IndexOf(listItem);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public virtual object SelectedItem
        {
            get
            {
                return (SelectedIndex > -1) ? Items[SelectedIndex] : null;
            }
            set
            {
                if (value != null && value is ListItem)
                {
                    int newIndex = Items.IndexOf((ListItem)value);
                    if(newIndex > -1)
                    {
                        SelectedIndex = newIndex;
                    }
                }
            }
        }

        private bool _Sorted;
        /// <summary>
        /// Gets or sets the Sorted.
        /// </summary>
        /// <value>
        /// The Sorted.
        /// </value>
        [RendererPropertyDescription]
        [Category("Behavior")]
        public bool Sorted
        {
            get { return _Sorted; }
            set
            {
                if (_Sorted != value)
                {
                    _Sorted = value;

                    OnPropertyChanged("Sorted");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether combo dropdown is multi column.
        /// </summary>
        /// <value>
        ///   <c>true</c> if combo dropdown is multi column; otherwise, <c>false</c>.
        /// </value>
        public virtual bool MultiColumn
        {
            get { return false; }
            set { }
        }


        /// <summary>
        /// Gets the item data for a given item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public int GetItemData(int index)
        {
            ListItem item = Items[index];
            if (item != null)
            {
                return item.ItemData;
            }

            return 0;
        }

        /// <summary>
        /// SelectedIndexChanged event handler.
        /// </summary>
        private event EventHandler _SelectedIndexChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove SelectedIndexChanged action.
        /// </summary>
        public event EventHandler SelectedIndexChanged
        {
            add
            {
                bool needNotification = (_SelectedIndexChanged == null);

                _SelectedIndexChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedIndexChanged");
                }

            }
            remove
            {
                _SelectedIndexChanged -= value;

                if (_SelectedIndexChanged == null)
                {
                    OnEventHandlerDeattached("SelectedIndexChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has SelectedIndexChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasSelectedIndexChangedListeners
        {
            get { return _SelectedIndexChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedIndexChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectedIndexChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_SelectedIndexChanged != null)
            {
                _SelectedIndexChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelectedIndexChanged(EventArgs args)
        {

            // Raise the SelectedIndexChanged event.
            OnSelectedIndexChanged(args);
        }

        /// <summary>
        /// Sets the item data for a given item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="value">The value.</param>
        public void SetItemData(int index, int value)
        {
            ListItem item = Items[index];
            if (item != null)
            {
                item.ItemData = value;
            }
        }

        /// <summary>
        /// SelectedValueChanged event handler.
        /// </summary>
        private event EventHandler _SelectedValueChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove SelectedValueChanged action.
        /// </summary>
        public event EventHandler SelectedValueChanged
        {
            add
            {
                bool needNotification = (_SelectedValueChanged == null);

                _SelectedValueChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedValueChanged");
                }

            }
            remove
            {
                _SelectedValueChanged -= value;

                if (_SelectedValueChanged == null)
                {
                    OnEventHandlerDeattached("SelectedValueChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has SelectedValueChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasSelectedValueChangedListeners
        {
            get { return _SelectedValueChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectedValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedValueChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectedValueChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_SelectedValueChanged != null)
            {
                _SelectedValueChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectedValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelectedValueChanged(EventArgs args)
        {

            // Try to get string value changed arguments
            ValueChangedArgs<string> valueChangedValueChangedArgs = args as ValueChangedArgs<string>;

            // If there is a valid string value changed arguments
            if (valueChangedValueChangedArgs != null)
            {
               // this.SelectedValue = this.Items[valueChangedValueChangedArgs.Value];  
                ListItem listItem = this.Items.FirstOrDefault(item => (Convert.ToString(item.Value).Equals(valueChangedValueChangedArgs.Value)));

                if (listItem == null)
                {
                    return;
                }
                this.SelectedIndex = this.Items.IndexOf(listItem);
            }

            // Raise the SelectedValueChanged event.
            OnSelectedValueChanged(args);

            // Raise the SelectedIndexChanged event
            if (_SelectedIndexChanged != null)
            {
                PerformSelectedIndexChanged(args);
            }
        }



        /// <summary>
        /// Gets the item text.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public string GetItemText(object item)
        {
            if (item != null)
            {
                ListItem listItem = item as ListItem;

                if (listItem == null)
                {
                    return item.ToString();
                }
                return listItem.Text;
            }

            return String.Empty;
        }

        /// <summary>
        /// Gets the item text.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public object GetItemValue(object item)
        {
            if (item != null)
            {
                ListItem listItem = item as ListItem;

                if (listItem == null)
                {
                    return item.ToString();
                }
                return listItem.Value;
            }

            return null;
        }

        /// <summary>
        /// Clear data source filter 
        /// </summary>
        [RendererMethodDescription]
        public void ClearItems(string reason = null)
        {
            _items.Clear();
            this.InvokeMethodOnce("ClearItems");
        }
    }
}
