﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Panel Bevel constants
    /// </summary>
    public enum PanelBevelConstants
    {
        /// <summary>
        /// No bevel
        /// </summary>
        NoBevel,

        /// <summary>
        /// Insert
        /// </summary>
        Inset,

        /// <summary>
        /// Raised
        /// </summary>
        Raised
    }
}
