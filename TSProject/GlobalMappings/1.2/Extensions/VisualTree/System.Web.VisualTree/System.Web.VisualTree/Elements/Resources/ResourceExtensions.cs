﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Elements
{
    public static class ResourceExtensions
    {
        /// <summary>
        /// Gets the image list streamer.
        /// </summary>
        /// <param name="resourceManager">The resource manager.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static ImageListStreamer GetImageListStreamer(this ComponentResourceManager resourceManager, string name)
        {
            // If there is a valid resource manager
            if (resourceManager != null)
            {
                // Get the resource index
                string resourceNames = resourceManager.GetObject(name) as string;

                // If there is a valid resource index
                if (!string.IsNullOrEmpty(resourceNames))
                {
                    // Create list of image references
                    List<ImageReference> imageReferences = new List<ImageReference>();

                    // Loop all resource names
                    foreach (string resourceName in resourceNames.Split(';'))
                    {
                        // Get image reference by name
                        ImageReference imageReference = resourceManager.GetResourceReference(resourceName) as ImageReference;

                        // If there is a valid image reference
                        if (imageReference != null)
                        {
                            // Add image reference
                            imageReferences.Add(imageReference);
                        }
                        else
                        {
                            // Add blank image reference
                            imageReferences.Add(ImageReference.Blank);
                        }
                    }

                    // Return image list streamer based on image references
                    return new ImageListStreamer(imageReferences);
                }


            }

            // Return blank image list streamer
            return ImageListStreamer.Blank;
        }




        /// <summary>
        /// Gets the resource reference.
        /// </summary>
        /// <param name="resourceManager">The resource manager.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static ResourceReference GetResourceReference(this ComponentResourceManager resourceManager, string name)
        {
            // The image reference
            ResourceReference resourceReference = null;

            // If there is a valid resource manager
            if (resourceManager != null)
            {
                // Generate resource id
                string resourceId = string.Concat(resourceManager.BaseName, "::", name);

                // Resolve or create image resource reference
                resourceReference = ResourceReference.Resolve<ResourceReference>(resourceId, () =>
                {
                    // Get the resource object
                    object resourceObject = resourceManager.GetObject(name);

                    // If there is a valid resource object
                    if (resourceObject != null)
                    {
                        // Get image from resource
                        Image image = resourceObject as Image;

                        // If there is a valid image
                        if (image != null)
                        {
                            // Return the image reference
                            return new ImageReference(string.Concat("/VisualTree/Resource?resourceId=", HttpUtility.UrlEncode(resourceId)), image);
                        }
                        else
                        {
                            // Get icon from resource
                            Icon icon = resourceObject as Icon;

                            // If there is a valid icon
                            if (icon != null)
                            {
                                // Return icon reference
                                return new IconReference(string.Concat("/VisualTree/Resource?resourceId=", HttpUtility.UrlEncode(resourceId)), icon);
                            }
                        }
                    }

                    return null;

                });
            }

            // Return image reference
            return resourceReference;
        }
    }
}
