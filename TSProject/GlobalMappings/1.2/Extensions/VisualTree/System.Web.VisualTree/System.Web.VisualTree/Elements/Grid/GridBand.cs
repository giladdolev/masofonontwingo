using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.GridItemElement" />
    public class GridBand : GridItemElement
    {
        /// <summary>
        /// The  default cell style
        /// </summary>
        private GridCellStyle _defaultCellStyle = new GridCellStyle();


        /// <summary>
        /// The _resizable state
        /// </summary>
        private GridTristate _resizable = GridTristate.True;



        /// <summary>
        /// Gets or sets a value indicating whether band is read only.
        /// </summary>
        /// <value>
        ///   <c>true</c> if read only; otherwise, <c>false</c>.
        /// </value>
        public virtual bool ReadOnly
        {
            get { return false; }
            set { }
        }



        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="GridBand"/> is selected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if selected; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Selected
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the default cell style.
        /// </summary>
        /// <value>
        /// The default cell style.
        /// </value>
        [DesignerIgnore]
        public virtual GridCellStyle DefaultCellStyle
        {
            get
            {
                return _defaultCellStyle;
            }
            set
            {
                if (value != null)
                {
                    _defaultCellStyle = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the resizable.
        /// </summary>
        /// <value>
        /// The resizable.
        /// </value>
        public virtual GridTristate Resizable
        {
            get { return this._resizable; }
            set { this._resizable = value; }
        }



        /// <summary>
        /// Gets the inherited style.
        /// </summary>
        /// <value>
        /// The inherited style.
        /// </value>
        public virtual GridCellStyle InheritedStyle
        {
            get { return default(GridCellStyle); }
        }
    }
}
