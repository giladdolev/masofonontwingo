namespace System.Web.VisualTree.Elements
{
	public class CursorEventArgs : EventArgs
	{

        private readonly ChartArea _chartArea;
        private readonly Axis _axis;
        private double _newPosition;

        public CursorEventArgs()
        {

        }

        public CursorEventArgs(ChartArea chartArea, Axis axis, double newPosition)
        {
            _chartArea = chartArea;
            _axis = axis;
            _newPosition = newPosition;
        }

		/// <summary>
		///Gets or sets the position of a cursor. Depending on the event, also sets the cursor's position.
		/// </summary>
		public double NewPosition
		{
			get
            {
                return _newPosition;
            }
			set
            {
                _newPosition = value;
            }
		}


		/// <summary>
		///Gets the T:System.Windows.Forms.DataVisualization.Charting.ChartArea object that a cursor or range selection belongs to.
		/// </summary>
		public ChartArea ChartArea
		{
			get
            {
                return _chartArea;
            }
		}
		/// <summary>
		///Gets the T:System.Windows.Forms.DataVisualization.Charting.Axis object that a cursor or range selection belongs to.
		/// </summary>
		public Axis Axis
		{
			get
            {
                return _axis;
            }
		}
	}
}
