﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public class GridHtmlContentColumn : GridColumn
    {

        private string _htmlContent = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCheckBoxColumn"/> class.
        /// </summary>
        public GridHtmlContentColumn()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCheckBoxColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridHtmlContentColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(bool))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCheckBoxColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        ///  /// <param name="htmlContent">The html Content .</param>
        public GridHtmlContentColumn(string columnName, string headerText, string htmlContent)
            : base(columnName, headerText, typeof(bool))
        {
            this.HtmlContent = htmlContent;
        }

        /// <summary>
        /// Get , set html content .
        /// </summary>
        public string HtmlContent
        {
            get { return _htmlContent; }
            set
            {
                this._htmlContent = value;
                SetHtmlContent();
            }
        }



        [RendererMethodDescription]
        private void SetHtmlContent()
        {
            this.InvokeMethod("SetHtmlContent", null);
        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        [DesignerIgnore]
        public override string DataTypeName
        {
            get
            {
                return "HtmlContent";
            }
        }

        public new string DataMember
        {
            get;
            set;
        }
    }
}
